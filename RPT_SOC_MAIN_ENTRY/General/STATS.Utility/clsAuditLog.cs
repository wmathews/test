﻿using System;
using System.IO;
using System.Security;
using System.Security.Principal;

namespace STATS.Utility
{
    public class clsAuditLog
    {
        private ClsProperty objProperties = ClsProperty.GetAccess();
        private string strHomeTeam = "";
        private string strAwayTeam = "";
        private string opt = "";

        //THIS WILL HAVE ALL USER INTERFACE ACTION SET.
        public enum AuditOperation
        {
            MenuItemSelected,
            ComboBoxSelected,
            ButtonClicked,
            ListBoxClicked,
            ListViewItemClicked,
            ListViewItemDoubleClicked,
            FormClosing,
            TextBoxEdited,
            GridRowSelected,
            GameSelected,
            DataGridChecked,
            DataGridUnChecked,
            ContextMenuItemSelected,
            ListBoxRightClicked,
            CheckBoxSelected,
            RadioButtonClicked,
            MouseClicked,
            LinkLabelClicked,
            CheckBoxUnSelected,
            ApplicationLoaded,
            FieldClicked,
            FieldMarksRemoved,
            PictureBoxClicked,
            DragDrop,
            Formname,
            Stack,
            Error,
            temp,
            KeyDown
        }

        //BELOW FUNCTION WILL CREATE THE AUDIT LOG STRING
        public void AuditLog(string HomeTeamAbbrev, string AwayTeamAbbrev, AuditOperation Operation, string ControlDescription, int NewLineBefore, int NewLineAfter)
        {
            try
            {
                strHomeTeam = HomeTeamAbbrev;
                strAwayTeam = AwayTeamAbbrev;
                string strAuditLogString = "";
                string OpStr = GetAuditLogString(Operation);
                
                if (Operation != STATS.Utility.clsAuditLog.AuditOperation.temp)
                {
                    strAuditLogString = OpStr + ControlDescription + " at local time " + DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss tt");
                }
                else
                {
                    strAuditLogString = OpStr + ControlDescription + "";
                }
                if (NewLineBefore == 1)
                {
                    strAuditLogString = System.Environment.NewLine + strAuditLogString;
                }

                if (NewLineBefore == 2)
                {
                    strAuditLogString = System.Environment.NewLine + System.Environment.NewLine + strAuditLogString;
                }

                if (NewLineAfter == 1)
                {
                    strAuditLogString = strAuditLogString + System.Environment.NewLine;
                }

                WriteToAuditLog(strAuditLogString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //BELOW FUNCTION WILL CREATE THE AUDIT LOG STRING
        public void AuditLog(string HomeTeamAbbrev, string AwayTeamAbbrev, AuditOperation Operation, string ControlValue, string ControlDescription, int NewLineBefore, int NewLineAfter)
        {
            try
            {
                strHomeTeam = HomeTeamAbbrev;
                strAwayTeam = AwayTeamAbbrev;
                string strAuditLogString = "";
                string OpStr = GetAuditLogString(Operation);
                string temp = ControlValue;
                opt = OpStr;

                if (temp != "System.Data.DataRowView")
                {
                    if (ControlDescription == null)
                    {
                        ControlDescription = null;
                    }
                    else
                    {
                        ControlDescription = "(For" + ControlDescription + ")";
                    }
                    if (temp != null && OpStr != "CLICKED LISTVIEW ITEM:      ")
                    {
                        strAuditLogString = OpStr + ControlValue + "" + ControlDescription + " at local time " + DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss tt");
                    }
                    else
                    {
                        strAuditLogString = OpStr + ControlValue + "";
                    }
                    //' strAuditLogString = OpStr + ControlValue + " (For " + ControlDescription + ")" + " at local time " + DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss tt");
                    if (NewLineBefore == 1)
                    {
                        strAuditLogString = System.Environment.NewLine + strAuditLogString;
                    }
                    if (NewLineAfter == 1)
                    {
                        strAuditLogString = strAuditLogString + System.Environment.NewLine;
                    }

                    if (NewLineBefore == 2)
                    {
                        strAuditLogString = System.Environment.NewLine + System.Environment.NewLine + strAuditLogString;
                    }

                    if ((NewLineBefore == 1) && (NewLineAfter == 1))
                    {
                        strAuditLogString = System.Environment.NewLine + strAuditLogString + System.Environment.NewLine;
                    }

                    WriteToAuditLog(strAuditLogString);
                }
                OpStr = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //BELOW FUNCTION WILL WRITE THE AUDIT LOG STRING INTO THE FILE
        public void WriteToAuditLog(string strAuditLogString)
        {
            //CREATE THE DIRECTORY PATH
            try
            {
                string strAuditTrailDirPath = CreateAuditLogDirectory();

                //SET THE DIRECTORY PATH IN PROPERTY CLASS
                //objProperty.AuditDirectory = strAuditTrailDirPath;

                //CREATE THE AUDIT LOG FILE NAME.
                string strFileName = CreateAuditLogFileName(strAuditTrailDirPath);

                // SET THE AUDIT LOG FILE NAME TO PROPERTY CLASS
                //objProperty.AuditFileName = strFileName;

                StreamWriter sw = null;
                //if( )
                //{
                //}
                if (opt == null)
                {
                    strAuditLogString = " ";
                }
                //CHECK FOR THE EXISTANCE OF A FILE OTHERWISE CREATE THE FILE.
                if (File.Exists(strFileName) == false)
                {
                    //CREATE THE NEW AUDIT LOG FILE
                    sw = File.CreateText(strFileName);
                    sw.Write(strAuditLogString);
                    sw.Close();
                    CreateHeader();
                }
                else
                {
                    sw = File.AppendText(strFileName);
                    sw.Write(strAuditLogString);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //BELOW FUNCTION WILL GET THE ACTION STRING FOR AN AUDIT OPERTATION
        public string GetAuditLogString(AuditOperation Operation)
        {
            try
            {
                string OpStr = "";
                switch (Operation)
                {
                    case AuditOperation.ButtonClicked:
                        OpStr = "CLICKED BUTTON:             ";
                        break;

                    case AuditOperation.DragDrop:
                        OpStr = "FORMATION DRAG DROP:         ";
                        break;

                    case AuditOperation.ComboBoxSelected:
                        OpStr = "SELECTED COMBO ITEM:        ";
                        break;

                    case AuditOperation.GridRowSelected:
                        OpStr = "SELECTED GRID ROW:          ";
                        break;

                    case AuditOperation.ListBoxClicked:
                        OpStr = "CLICKED LIST BOX:           ";
                        break;

                    case AuditOperation.ListViewItemClicked:
                        OpStr = "CLICKED LISTVIEW ITEM:      ";
                        break;

                    case AuditOperation.MenuItemSelected:
                        OpStr = "SELECTED MENU ITEM:         ";
                        break;

                    case AuditOperation.FormClosing:
                        OpStr = "CLOSING FORM:               ";
                        break;

                    case AuditOperation.TextBoxEdited:
                        OpStr = "ENTERED/MODIFIED TEXT:      ";
                        break;

                    case AuditOperation.GameSelected:
                        OpStr = "SELECTED GAME:              ";
                        break;

                    case AuditOperation.DataGridChecked:
                        OpStr = "CHECKED DATAGRID:           ";
                        break;

                    case AuditOperation.DataGridUnChecked:
                        OpStr = "UNCHECKED DATAGRID:         ";
                        break;

                    case AuditOperation.ContextMenuItemSelected:
                        OpStr = "SELECTED CONTEXTMENU ITEM:  ";
                        break;

                    case AuditOperation.ListBoxRightClicked:
                        OpStr = "RIGHT CLICKED LISTBOX:      ";
                        break;

                    case AuditOperation.CheckBoxSelected:
                        OpStr = "SELECTED CHECKBOX:          ";
                        break;

                    case AuditOperation.RadioButtonClicked:
                        OpStr = "CLICKED RADIOBUTTON:        ";
                        break;

                    case AuditOperation.MouseClicked:
                        OpStr = "CLICKED MOUSE:              ";
                        break;

                    case AuditOperation.LinkLabelClicked:
                        OpStr = "CLICKED LINKLABEL:          ";
                        break;

                    case AuditOperation.CheckBoxUnSelected:
                        OpStr = "UNSELECTED CHECKBOX:        ";
                        break;

                    case AuditOperation.ApplicationLoaded:
                        OpStr = "LOADED APPLICATION:         ";
                        break;

                    case AuditOperation.ListViewItemDoubleClicked:
                        OpStr = "DOUBLECLICKED LISTVIEWITEM: ";
                        break;

                    case AuditOperation.FieldClicked:
                        OpStr = "CLICKED FIELD:              ";
                        break;

                    case AuditOperation.FieldMarksRemoved:
                        OpStr = "REMOVED FIELD MARKS:        ";
                        break;

                    case AuditOperation.PictureBoxClicked:
                        OpStr = "CLICKED PICTURE BOX:        ";
                        break;

                    case AuditOperation.Formname:
                        OpStr = "Form Load:                  ";
                        break;

                    case AuditOperation.Error:
                        OpStr = "Error Occurred:             ";
                        break;

                    case AuditOperation.Stack:
                        OpStr = "Stack:                      ";
                        break;
                    case AuditOperation.KeyDown:
                        OpStr = "KEY DOWN:                   ";
                        break;
                }

                return OpStr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //BELOW FUNCTION WILL CREATE THE AUDIT LOG DIRECTORY
        public string CreateAuditLogDirectory()
        {
            try
            {
                string Path = System.AppDomain.CurrentDomain.BaseDirectory.Substring(0, 3);
                Path = Path + "\\SoccerDataCollection\\AuditTrail";
                if (!Directory.Exists(Path))
                {
                    DirectoryInfo DirectoryPath = Directory.CreateDirectory(Path);
                }
                return Path;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //BELOW FUNCTION WILL CREATE THE AUDIT LOG FILE NAME
        public string CreateAuditLogFileName(string Dirpath)
        {
            string fileName = "";                          
            string strMachineHash = Math.Abs((System.Net.Dns.GetHostName() + "\\" + Environment.UserName).GetHashCode()).ToString().Substring(0, 8);
            
            try
            {
                string strCurrentDate = DateTime.Now.ToShortDateString().Replace("/", "-");
                fileName = fileName + Dirpath + "\\";

                if (strHomeTeam == "")
                {
                    string strAuditLogFileName = "AT ";
                    fileName = fileName + strAuditLogFileName + strCurrentDate + "_" + strMachineHash + ".txt";
                }
                else
                {
                    string strAuditLogFileName = "AT " + strHomeTeam + " @ " + strAwayTeam;
                    fileName = fileName + strAuditLogFileName + "(" + strCurrentDate + ")" + "_" + objProperties.UserID + "_" + strMachineHash + ".txt";
                }

                return fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                fileName = null;
            }
        }

        //CODE ADDED BY GOPAL TO DELETE EXPIRED(2 WEEKS) AUDIT LOG FILES FROM REPORTER'S M/C
        public void DeleteExpiredAuditLogs()
        {
            try
            {

                //points to default system folder(My Documents)
                string strAuditTrailPath = "";

                strAuditTrailPath = System.AppDomain.CurrentDomain.BaseDirectory.Substring(0, 3);
                strAuditTrailPath = strAuditTrailPath + "\\SoccerDataCollection\\AuditTrail";
                DirectoryInfo dri = new DirectoryInfo(strAuditTrailPath);
                if (dri.Exists)
                {
                    //2 weeks before
                    DateTime dtDays = Convert.ToDateTime(DateTime.Now.AddDays(-14)).Date;
                    foreach (FileInfo file in dri.GetFiles("*.txt"))
                    {
                        DateTime MyStamp = file.CreationTime;
                        string strDate = Convert.ToDateTime(MyStamp).ToShortDateString();
                        if (Convert.ToDateTime(strDate).Date <= dtDays)
                        {
                            file.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Below function will get the Operating system and Machine Name
        public void CreateHeader()
        {
            try
            {
                string logString = "";
                string header = "Soccer Data Collection" + System.Environment.NewLine;
                header = header + "Release: " + System.Windows.Forms.Application.ProductVersion.ToString() + System.Environment.NewLine + System.Environment.NewLine;
                header = header + "AUDIT TRAIL" + System.Environment.NewLine;
                logString = new string('=', 71);
                header = header + logString + System.Environment.NewLine;
                header = header + "OS: " + GetOperatingSystem() + " " + System.Environment.NewLine;
                header = header + "Machine Name: " + Environment.MachineName + System.Environment.NewLine + System.Environment.NewLine;

                WriteToAuditLog(header);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To get the OS Name
        private static string GetOperatingSystem()
        {
            try
            {
                System.OperatingSystem os = System.Environment.OSVersion;
                string osName = "UN";
                switch (os.Platform)
                {
                    case System.PlatformID.Win32Windows:
                        switch (os.Version.Minor)
                        {
                            case 0:
                                osName = "Windows 95";
                                break; // TODO: might not be correct. Was : Exit Select

                                break;

                            case 10:
                                osName = "Windows98";
                                break; // TODO: might not be correct. Was : Exit Select

                                break;

                            case 90:
                                osName = "Windows ME";
                                break; // TODO: might not be correct. Was : Exit Select

                                break;
                        }
                        break; // TODO: might not be correct. Was : Exit Select

                        break;

                    case System.PlatformID.Win32NT:
                        switch (os.Version.Major)
                        {
                            case 3:
                                osName = "Windows NT";
                                break; // TODO: might not be correct. Was : Exit Select

                                break;

                            case 4:
                                osName = "Windows NT";
                                break; // TODO: might not be correct. Was : Exit Select

                                break;

                            case 5:
                                if (os.Version.Minor == 0)
                                {
                                    osName = "Windows 2000";
                                }
                                else if (os.Version.Minor == 1)
                                {
                                    osName = "Windows XP";
                                }
                                else if (os.Version.Minor == 2)
                                {
                                    osName = "Windows 2003";
                                }
                                break; // TODO: might not be correct. Was : Exit Select

                                break;

                            case 6:
                                osName = "Windows Vista";
                                if (os.Version.Minor == 0)
                                {
                                    osName = "Windows Vista";
                                }
                                else if (os.Version.Minor == 1)
                                {
                                    osName = "Windows 7";
                                }
                                break; // TODO: might not be correct. Was : Exit Select

                                break;
                        }
                        break; // TODO: might not be correct. Was : Exit Select

                        break;
                }
                return osName;
            }
            catch (Exception ex)
            {
                return "Permission Denied";
            }
        }

        //to get the Maching Hash string
        private static string GetWindowsIdentity()
        {
            try
            {
                return WindowsIdentity.GetCurrent().Name;
            }
            catch (SecurityException)
            {
                return "Permission Denied";
            }
        }
    }
}