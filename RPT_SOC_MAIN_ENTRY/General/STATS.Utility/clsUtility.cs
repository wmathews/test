#region " Using "

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Linq;
#endregion " Using "

namespace STATS.Utility
{
    #region "Comments"

    /*-------------------------------------------------------------------------------
     Class Name        : clsUtility
     Author            : Sandeep Kumar Nanda
     Created Date      : 06 May 2008
     Description       : Contains methods to load different Data Controls
    ----------------------------------------------------------------------------------
    ---------------------------Modification Log---------------------------------------
    ----------------------------------------------------------------------------------
    ID             Modified By         Modified date           Description
    ----------------------------------------------------------------------------------
     1.           Ravi Krishna G        22/04/09               Added comments & Desc
    ----------------------------------------------------------------------------------*/

    #endregion "Comments"

    public class clsUtility
    {
        /// <summary>
        /// Represents the Select Type option to be displayed in the Combo
        /// </summary>
        public enum SelectTypeDisplay
        {
            ALL,
            NONE,
            ALLNONE,
            BLANK
        }

        #region "Shared Methods"

        /// <summary>
        /// Loads the Combo with the data from the Data Table
        /// </summary>
        /// <param name="Combo">A Combo that should be populated with the data</param>
        /// <param name="Type">Any one Select Type from (ALL/NONE/BLANK/ALLNONE)</param>
        /// <param name="Table">A DataTable which contains the data</param>
        /// <param name="DisplayColumnName">A Datacolumn representing the text displayed in the combo</param>
        /// <param name="DataColumnName">A Datacolumn representing the underlying data binded to the combo</param>
        public static void LoadControl(ref ComboBox Combo, SelectTypeDisplay Type, DataTable Table, String DisplayColumnName, String DataColumnName)
        {
            try
            {
                DataTable dtLoadData = new DataTable();
                DataRow drResultSet;

                if (Table != null)
                {
                    if (Table.Rows.Count > 0)
                    {
                        if (Table.Rows.Count == 1)
                        {
                            if (Type == SelectTypeDisplay.ALL)
                            {
                                Type = SelectTypeDisplay.BLANK;
                            }
                            else if (Type == SelectTypeDisplay.ALLNONE)
                            {
                                Type = SelectTypeDisplay.NONE;
                            }
                        }

                        switch (Type)
                        {
                            case SelectTypeDisplay.ALL:
                                dtLoadData = Table.Copy();
                                drResultSet = dtLoadData.NewRow();
                                drResultSet[DataColumnName] = 0;
                                drResultSet[DisplayColumnName] = "All";
                                Combo.DataSource = dtLoadData;
                                dtLoadData.Rows.InsertAt(drResultSet, 0);
                                Combo.ValueMember = dtLoadData.Columns[DataColumnName].ToString();
                                Combo.DisplayMember = dtLoadData.Columns[DisplayColumnName].ToString();
                                break;

                            case SelectTypeDisplay.ALLNONE:
                                dtLoadData = Table.Copy();
                                drResultSet = dtLoadData.NewRow();
                                drResultSet[DataColumnName] = 0;
                                drResultSet[DisplayColumnName] = "";
                                dtLoadData.Rows.InsertAt(drResultSet, 0);
                                drResultSet = dtLoadData.NewRow();
                                drResultSet[DataColumnName] = 0;
                                drResultSet[DisplayColumnName] = "ALL";
                                Combo.DataSource = dtLoadData;
                                dtLoadData.Rows.InsertAt(drResultSet, 1);
                                Combo.ValueMember = dtLoadData.Columns[DataColumnName].ToString();
                                Combo.DisplayMember = dtLoadData.Columns[DisplayColumnName].ToString();
                                break;

                            case SelectTypeDisplay.NONE:
                                dtLoadData = Table.Copy();
                                drResultSet = dtLoadData.NewRow();
                                drResultSet[DataColumnName] = 0;
                                drResultSet[DisplayColumnName] = "";
                                Combo.DataSource = dtLoadData;
                                dtLoadData.Rows.InsertAt(drResultSet, 0);
                                Combo.ValueMember = dtLoadData.Columns[DataColumnName].ToString();
                                Combo.DisplayMember = dtLoadData.Columns[DisplayColumnName].ToString();
                                break;

                            case SelectTypeDisplay.BLANK:
                                Combo.DataSource = Table;
                                Combo.ValueMember = Table.Columns[DataColumnName].ToString();
                                Combo.DisplayMember = Table.Columns[DisplayColumnName].ToString();
                                break;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Loads a group of Combos with the data from the Data Source
        /// </summary>
        /// <param name="Combo">A set of Combo's that should be populated with the data</param>
        /// <param name="Type">Any one Select Type from (ALL/NONE/BLANK/ALLNONE)</param>
        /// <param name="Table">A DataTable which contains the data</param>
        /// <param name="DisplayColumnName">A Column representing the text displayed in the combo</param>
        /// <param name="DataColumnName">A Column representing the underlying data binded to the combo</param>
        public static void LoadControl(ref ComboBox[] Combo, SelectTypeDisplay Type, DataTable Table, String DisplayColumnName, String DataColumnName)
        {
            try
            {
                DataTable dtLoadData = new DataTable();
                DataRow drResultSet;

                if (Table.Rows.Count < 1)
                {
                    Type = SelectTypeDisplay.BLANK;
                }
                else if (Table.Rows.Count == 1)
                {
                    if (Type == SelectTypeDisplay.ALL)
                    {
                        Type = SelectTypeDisplay.BLANK;
                    }
                    else if (Type == SelectTypeDisplay.ALLNONE)
                    {
                        Type = SelectTypeDisplay.NONE;
                    }
                }

                foreach (ComboBox cBox in Combo)
                {
                    switch (Type)
                    {
                        case SelectTypeDisplay.ALL:
                            dtLoadData = Table.Copy();
                            drResultSet = dtLoadData.NewRow();
                            drResultSet[DataColumnName] = 0;
                            drResultSet[DisplayColumnName] = "All";
                            cBox.DataSource = dtLoadData;
                            dtLoadData.Rows.InsertAt(drResultSet, 0);
                            cBox.ValueMember = dtLoadData.Columns[DataColumnName].ToString();
                            cBox.DisplayMember = dtLoadData.Columns[DisplayColumnName].ToString();
                            break;

                        case SelectTypeDisplay.ALLNONE:
                            dtLoadData = Table.Copy();
                            drResultSet = dtLoadData.NewRow();
                            drResultSet[DataColumnName] = 0;
                            drResultSet[DisplayColumnName] = "";
                            dtLoadData.Rows.InsertAt(drResultSet, 0);
                            drResultSet = dtLoadData.NewRow();
                            drResultSet[DataColumnName] = 0;
                            drResultSet[DisplayColumnName] = "ALL";
                            cBox.DataSource = dtLoadData;
                            dtLoadData.Rows.InsertAt(drResultSet, 1);
                            cBox.ValueMember = dtLoadData.Columns[DataColumnName].ToString();
                            cBox.DisplayMember = dtLoadData.Columns[DisplayColumnName].ToString();
                            break;

                        case SelectTypeDisplay.NONE:
                            dtLoadData = Table.Copy();
                            drResultSet = dtLoadData.NewRow();
                            drResultSet[DataColumnName] = 0;
                            drResultSet[DisplayColumnName] = "";
                            cBox.DataSource = dtLoadData;
                            dtLoadData.Rows.InsertAt(drResultSet, 0);
                            cBox.ValueMember = dtLoadData.Columns[DataColumnName].ToString();
                            cBox.DisplayMember = dtLoadData.Columns[DisplayColumnName].ToString();
                            break;

                        case SelectTypeDisplay.BLANK:
                            //dsLoadData = ResultSet.Copy();
                            //cBox.DataSource = dtLoadData;
                            cBox.DataSource = Table;
                            cBox.ValueMember = Table.Columns[DataColumnName].ToString();
                            cBox.DisplayMember = Table.Columns[DisplayColumnName].ToString();
                            break;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Loads the CheckedListBox with the data from the Data Table
        /// </summary>
        /// <param name="CLBox">A CheckedListBox that should be populated with the data</param>
        /// <param name="table">A DataTable which contains the data</param>
        /// <param name="displayColumnName">A Datacolumn representing the text displayed in the CheckedListbox</param>
        /// <param name="dataColumnName">A Datacolumn representing the underlying data binded to the CheckedListbox</param>
        public static void LoadControl(ref CheckedListBox CLBox, DataTable table, String displayColumnName, String dataColumnName)
        {
            try
            {
                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        CLBox.DataSource = table;
                        CLBox.ValueMember = table.Columns[dataColumnName].ToString();
                        CLBox.DisplayMember = table.Columns[displayColumnName].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Loads the ListBox with the data from the Data Table
        /// </summary>
        /// <param name="LBox">A ListBox that should be populated with the data</param>
        /// <param name="table">A DataTable which contains the data</param>
        /// <param name="displayColumnName">A Datacolumn representing the text displayed in the CheckedListbox</param>
        /// <param name="dataColumnName">A Datacolumn representing the underlying data binded to the CheckedListbox</param>
        public static void LoadControl(ref ListBox LBox, DataTable table, String displayColumnName, String dataColumnName)
        {
            try
            {
                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        LBox.DataSource = table;
                        LBox.ValueMember = table.Columns[dataColumnName].ToString();
                        LBox.DisplayMember = table.Columns[displayColumnName].ToString();
                        LBox.SelectedIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public static void LoadControl(ref ListBox listBoxcontrol, List<Object> listvalues, String displayColumnName, String dataColumnName, string tagPbpColumn, int listSelectValue)
        {
            try
            {
                listBoxcontrol.DataSource = null;
                listBoxcontrol.Visible = true;
                listBoxcontrol.Tag = tagPbpColumn;

                if (listvalues != null && listvalues.Count > 0)
                {
                    listBoxcontrol.DataSource = listvalues;
                    listBoxcontrol.ValueMember = dataColumnName;
                    listBoxcontrol.DisplayMember = displayColumnName;
                    if (listSelectValue == -1 || (dataColumnName == "PlayerId" && listSelectValue == 0)) //High Light Goalie
                    {
                        listBoxcontrol.SelectedIndex = listSelectValue;
                    }
                    else if (dataColumnName == "PlayerId")
                    {
                        listBoxcontrol.SelectedValue = Convert.ToInt32(listSelectValue); // (ActualType)(listSelectValue);
                    }
                    else
                    {
                        listBoxcontrol.SelectedValue = Convert.ToInt16(listSelectValue); // (ActualType)(listSelectValue);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public static void LoadControl(ref ListBox listBoxcontrol,  DataTable table, String displayColumnName, String dataColumnName, string tagPbpColumn)
        {
            try
            {
                listBoxcontrol.DataSource = null;
                listBoxcontrol.Visible = true;
                listBoxcontrol.Tag = tagPbpColumn;

                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        listBoxcontrol.DataSource = table;
                        listBoxcontrol.ValueMember = table.Columns[dataColumnName].ToString();
                        listBoxcontrol.DisplayMember = table.Columns[displayColumnName].ToString();
                        listBoxcontrol.SelectedIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        /// <summary>
        /// Checks/Unchecks all the entries in the CheckedListBox
        /// </summary>
        /// <param name="CLBox">A CheckedListBox populated with the data</param>
        /// <param name="Select">A Boolean value representing Check (1) / Uncheck (0) </param>
        public static void SelectAllItems(ref CheckedListBox CLBox, Boolean Select)
        {
            try
            {
                for (int itemIndex = 0; itemIndex < CLBox.Items.Count; itemIndex++)
                {
                    CLBox.SetItemChecked(itemIndex, Select);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Converts given input to Minutes, Seconds & Milliseconds
        /// </summary>
        /// <param name="dblTime">A +ve value which needs to be converted</param>
        /// <param name="blnIsMilliSecond">Represents whether the output should include Millisecond</param>
        /// <returns>A string in a given format representing the time in Minutes, Seconds & Milliseconds(optional)</returns>
        public static string ConvertSecondToMinute(double dblTime, bool blnIsMilliSecond)
        {
            try
            {
                int dblMinute;
                double dblSecond;
                double dblMilliSecond = 0;
                string strMinuteVal;
                string strSecondVal;

                dblMinute = (int)(dblTime / 60);
                dblSecond = dblTime - ((int)dblMinute * 60);

                if (dblMinute < 10)
                {
                    strMinuteVal = "0" + dblMinute;
                }
                else
                {
                    strMinuteVal = dblMinute.ToString();
                }

                if (dblSecond < 10)
                {
                    strSecondVal = "0" + dblSecond;
                }
                else
                {
                    strSecondVal = dblSecond.ToString();
                }

                if (blnIsMilliSecond == true)
                {
                    return strMinuteVal + ":" + strSecondVal + ":" + dblMilliSecond;
                }
                else
                {
                    return strMinuteVal + ":" + strSecondVal;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Converts given Time  to 24 hr format
        /// </summary>
        /// <param name="strTime">A +ve value which needs to be converted to 24 hrs format</param>
        /// <returns>A string which represents the time in 24 hr format</returns>

        public static string ConvertTimeTo24HourFormat(string strTime)
        {
            string strTime24hrFormat = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strTime))
                {
                    Array arr = strTime.Split(System.Convert.ToChar(":"));
                    if (strTime.IndexOf("PM") == -1 && strTime.IndexOf("AM") == -1)
                    {
                        return strTime;
                    }
                    if (strTime.Contains("PM"))
                    {
                        strTime24hrFormat = Convert.ToString((Convert.ToInt32(arr.GetValue(0).ToString()) + 12));
                        strTime24hrFormat += ":" + arr.GetValue(1).ToString().Replace("PM", "");
                        return strTime24hrFormat;
                    }
                    else
                    {
                        return strTime.Replace("AM", "");
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Converts given input to Seconds from Minutes
        /// </summary>
        /// <param name="strTime">A +ve value which needs to be converted to Seconds</param>
        /// <returns>A string which represents the time in seconds</returns>
        ///

        public double ConvertMinuteToSecond(string strTime)
        {
            try
            {
                string strMinuteVal;
                string strSecondVal;
                double dblSecondVal;

                if (strTime.Length >= 5)
                {
                    Array arr = strTime.Split(System.Convert.ToChar(":"));
                    strMinuteVal = System.Convert.ToString(arr.GetValue(0)).Trim() == string.Empty ? "0" : System.Convert.ToString(arr.GetValue(0)).Trim();
                    strSecondVal = System.Convert.ToString(arr.GetValue(1)).Trim() == string.Empty ? "0" : System.Convert.ToString(arr.GetValue(1)).Trim();
                    if (strMinuteVal != "   ")
                    {
                        dblSecondVal = double.Parse(strMinuteVal) * 60 + double.Parse(strSecondVal);
                    }
                    else
                    {
                        dblSecondVal = double.Parse(strSecondVal);
                    }
                    return dblSecondVal;
                }
                else
                {
                    dblSecondVal = 0;
                    return dblSecondVal;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ConvertMinuteToMillisecond(string strTime)
        {
            try
            {
                string strMinuteVal;
                string strSecondVal;
                string strMillisecVal;
                double dblMilliSecondVal;

                if (strTime.Length >= 5)
                {
                    Array arr = strTime.Split(System.Convert.ToChar(":"));
                    strMinuteVal = System.Convert.ToString(arr.GetValue(0)).Trim() == string.Empty ? "0" : System.Convert.ToString(arr.GetValue(0)).Trim();
                    strSecondVal = System.Convert.ToString(arr.GetValue(1)).Trim() == string.Empty ? "0" : System.Convert.ToString(arr.GetValue(1)).Trim();
                    strMillisecVal = System.Convert.ToString(arr.GetValue(2)).Trim() == string.Empty? "0" : System.Convert.ToString(arr.GetValue(2)).Trim();

                    if (strMinuteVal != "")
                    {
                        dblMilliSecondVal = (double.Parse(strMinuteVal) * 60000) + (double.Parse(strSecondVal) * 1000) + double.Parse(strMillisecVal) ;
                    }
                    else
                    {
                        dblMilliSecondVal = double.Parse(strSecondVal) * 1000 + double.Parse(strMillisecVal);
                    }
                    return dblMilliSecondVal;
                }
                else
                {
                    dblMilliSecondVal = 0;
                    return dblMilliSecondVal;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Returns the full path of the Image Location
        /// </summary>
        /// <param name="DisplayImage">String representing the name of the Image to be displayed</param>
        /// <returns>If contains, returns the location of the image, else returns null</returns>
        public string ReturnImage(string DisplayImage)
        {
            DirectoryInfo objDirectoryInfo;
            try
            {
                string strReturnImage = "";
                string strPath = Application.StartupPath + "\\Resources\\";
                string strFileName = DisplayImage;
                objDirectoryInfo = new DirectoryInfo(strPath);
                if (objDirectoryInfo.Exists)
                {
                    foreach (FileInfo objFileInfo in objDirectoryInfo.GetFiles(strFileName))
                    {
                        string strName = objFileInfo.Name;
                        if (strFileName == strName)
                        {
                            strReturnImage = objFileInfo.FullName;
                        }
                    }
                    if (strReturnImage == "")
                    {
                        strReturnImage = null;
                    }
                }
                else
                {
                    strReturnImage = null;
                }
                return strReturnImage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDirectoryInfo = null;
            }
        }

        /// <summary>
        /// Exports the data from the Datagridview to Excel sheet in the given format
        /// </summary>
        /// <param name="objDataGridView">Represents the DataGridView which contains the data to be exported to excel</param>
        /// <param name="FileName">String representing the Filename</param>
        /// <param name="FileExtension">String representing the filename extension for the excel sheet</param>
        /// <param name="FilePath">String representing the Relative path where the file should be saved</param>
        /// <returns>A Boolean value which denotes the status of the Export</returns>
        public Boolean ExportToExcel(ref DataGridView objDataGridView, string FileName, string FileExtension, string FilePath)
        {
            // CHOOSE THE PATH, NAME AND EXTENSION FOR THE EXCEL FILE
            string strFile = FilePath + "\\" + FileName + FileExtension;
            System.IO.StreamWriter objStreamWriter = new System.IO.StreamWriter(strFile, false);

            try
            {
                // OPEN THE FILE AND WRITE THE HEADERS
                objStreamWriter.WriteLine("<?xml version=\"1.0\"?>");
                objStreamWriter.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
                objStreamWriter.WriteLine("<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">");

                // CREATE THE STYLES FOR THE WORKSHEET
                objStreamWriter.WriteLine(" <ss:Styles>");

                // STYLE FOR THE COLUMN HEADERS
                objStreamWriter.WriteLine(" <ss:Style ss:ID=\"1\">");
                objStreamWriter.WriteLine(" <ss:Font ss:Bold=\"1\"/>");
                objStreamWriter.WriteLine(" <ss:Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\" " + "ss:WrapText=\"1\"/>");
                objStreamWriter.WriteLine(" <ss:Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
                objStreamWriter.WriteLine(" </ss:Style>");

                // STYLE FOR THE COLUMN INFORMATION
                objStreamWriter.WriteLine(" <ss:Style ss:ID=\"2\">");
                objStreamWriter.WriteLine(" <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                objStreamWriter.WriteLine(" </ss:Style>");
                objStreamWriter.WriteLine(" </ss:Styles>");

                // WRITE THE WORKSHEET CONTENTS
                objStreamWriter.WriteLine("<ss:Worksheet ss:Name=\"Sheet1\">");
                objStreamWriter.WriteLine(" <ss:Table>");

                int intColumn = 0;
                for (intColumn = 0; intColumn <= objDataGridView.Columns.Count - 1; intColumn++)
                {
                    objStreamWriter.WriteLine(string.Format(" <ss:Column ss:Width=\"{0}\"/>", objDataGridView.Columns[intColumn].Width));
                }

                objStreamWriter.WriteLine(" <ss:Row>");

                int intRow = 0;
                for (intRow = 0; intRow <= objDataGridView.Columns.Count - 1; intRow++)
                {
                    objStreamWriter.WriteLine(string.Format(" <ss:Cell ss:StyleID=\"1\">" + "<ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", objDataGridView.Columns[intRow].HeaderText));
                }
                objStreamWriter.WriteLine(" </ss:Row>");

                int intSubtractBy;
                string strCellText;
                if (objDataGridView.AllowUserToAddRows == true)
                    intSubtractBy = 2;
                else
                    intSubtractBy = 1;

                // WRITE CONTENTS FOR EACH CELL
                int intRowCell = 0;
                for (intRowCell = 0; intRowCell <= objDataGridView.RowCount - intSubtractBy; intRowCell++)
                {
                    objStreamWriter.WriteLine(string.Format(" <ss:Row ss:Height=\"{0}\">", objDataGridView.Rows[intRowCell].Height));
                    int intColCell = 0;
                    for (intColCell = 0; intColCell <= objDataGridView.Columns.Count - 1; intColCell++)
                    {
                        strCellText = objDataGridView[intColCell, intRowCell].Value.ToString();

                        // CHECK FOR NULL CELL AND CHANGE IT TO EMPTY TO AVOID ERROR

                        if (strCellText == string.Empty)
                            strCellText = "";
                        objStreamWriter.WriteLine(string.Format(" <ss:Cell ss:StyleID=\"2\">" + "<ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", strCellText.ToString()));
                    }
                    objStreamWriter.WriteLine(" </ss:Row>");
                }

                // CLOSE THE DOCUMENT
                objStreamWriter.WriteLine(" </ss:Table>");
                objStreamWriter.WriteLine("</ss:Worksheet>");
                objStreamWriter.WriteLine("</ss:Workbook>");

                return true;
            }
            catch (IOException ex)
            {
                throw ex;
            }
            finally
            {
                objStreamWriter.Close();
            }
        }

        public static string ConvertColorToHexadecimalString(System.Drawing.Color ColorToConvert)
        {
            try
            {
                string strR = null;
                string strG = null;
                string strB = null;

                strR = ColorToConvert.R.ToString("X").ToLower().PadRight(2, '0').ToUpper().ToString();
                strG = ColorToConvert.G.ToString("X").ToLower().PadRight(2, '0').ToUpper().ToString();
                strB = ColorToConvert.B.ToString("X").ToLower().PadRight(2, '0').ToUpper().ToString();

                return "#" + strR + strG + strB;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static System.Drawing.Color ConvertHexadecimalStringToColor(string HexadecimalString)
        {
            try
            {
                System.Drawing.Color clrConverted = default(System.Drawing.Color);
                Int32 intR = default(Int32);
                Int32 intG = default(Int32);
                Int32 intB = default(Int32);

                intR = Convert.ToInt32(HexadecimalString.Substring(1, 2), 16);
                intG = Convert.ToInt32(HexadecimalString.Substring(3, 2), 16);
                intB = Convert.ToInt32(HexadecimalString.Substring(5, 2), 16);

                clrConverted = System.Drawing.Color.FromArgb(intR, intG, intB);
                return clrConverted;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static System.Drawing.Color GetTextColor(System.Drawing.Color BackColor)
        {
            try
            {
                int intBackColor = BackColor.ToArgb();
                System.Drawing.Color TextColor = System.Drawing.Color.Black;

                int intR = BackColor.R;
                int intG = BackColor.G;
                int intB = BackColor.B;

                if (intR * 0.3 + intG * 0.59 + intB * 0.11 > 128)
                {
                    TextColor = System.Drawing.Color.Black;
                }
                else
                {
                    TextColor = System.Drawing.Color.White;
                }
                return TextColor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Highlight event,team and player buttons when clicked..
        public static void HighlightGoldButton(Button buttonHighlight)
        {
            buttonHighlight.BackColor = Color.Gold;
            buttonHighlight.ForeColor = GetTextColor(Color.Gold);
        }

        //set button color
        public static void SetButtonColor(Button buttonHighlight, Color setBackColor)
        {
            buttonHighlight.BackColor = setBackColor;
            buttonHighlight.ForeColor = GetTextColor(setBackColor);
        }

        //set button color
        public static void SetButtonColor(Button buttonHighlight, Color setBackColor, Color setForeColor)
        {
            buttonHighlight.BackColor = setBackColor;
            buttonHighlight.ForeColor = setForeColor;
        }

        //set button color
        public static void ClearPanelButtonText(Panel panelClear)
        {
            foreach (Button buttonPanel in panelClear.Controls.OfType<Button>())
            {
                buttonPanel.Text = string.Empty;
                buttonPanel.Tag = 0;
                buttonPanel.Enabled = false;
            }
        }

        //Clear Panel TEXTBOX  values
        public static void ClearPanelLabelText(Panel panelClear)
        {
            foreach (Label labelPanel in panelClear.Controls.OfType<Label>())
            {
                labelPanel.Text = string.Empty;
            }
        }

        //Set the panel button color
        public static void SetPanelButtonColor(Panel panel,Color setBackColor, Color setForeColor)
        {
            foreach (Button buttonPanel in panel.Controls.OfType<Button>())
            {
                buttonPanel.BackColor = setBackColor;
                buttonPanel.ForeColor = setForeColor;
            }
        }


        #endregion "Shared Methods"
    }
}