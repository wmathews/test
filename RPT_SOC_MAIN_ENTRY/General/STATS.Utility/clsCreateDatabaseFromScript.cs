#region "Using"

using Microsoft.Win32;
using System;
using System.IO;

#endregion "Using"

namespace STATS.Utility
{
    #region"Comments"
    /*-------------------------------------------------------------------------
     Class Name        : clsUtility
     Author            : Sandeep Kumar Nanda
     Created Date      : 30 May 2008
     Description       :
    ---------------------------------------------------------------------------
    ---------------------------Modification Log--------------------------------
    ---------------------------------------------------------------------------
    ID             Modified By         Modified date           Description
    ---------------------------------------------------------------------------
    1.          RAVI KRISHNA G          22/04/09        Added Comments & Desc
    ---------------------------------------------------------------------------*/
    #endregion

    public class clsCreateDatabaseFromScript
    {
        /// <summary>
        /// To create the Database in the local system based on the script file provided as input
        /// </summary>
        /// <param name="frm">A Form passed as a reference to which the focus control should be passed back after the database creation</param>
        /// <param name="ApplicationPath">A String representing the Applicationpath</param>
        /// <param name="ScriptName">A String representing the Filename of the batch file</param>
        /// <param name="Database">A String representing the Name of the Database</param>
        /// <returns>A Boolean value representing the status of the database creation</returns>
        public static bool CreateDatabase(string ApplicationPath, string ScriptName, string Database)
        {
            System.Diagnostics.ProcessStartInfo objStartProcess;
            try
            {
                string strFilepath = "";
                //System.Diagnostics.ProcessStartInfo objStartProcess;
                System.Diagnostics.Process pStart1 = new System.Diagnostics.Process();
                strFilepath = ApplicationPath;
                strFilepath += "\\Resources\\" + ScriptName + ".bat";
                string strRegistryPath;
                //CHECK FOR THE SQL EXIST OR NOT.
                strRegistryPath = Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SQL Server\\SQLEXPRESS\\Setup", "SQLPath", null).ToString();
                //CREATE A BACKUP FILE FOR SQL.
                strRegistryPath += "\\BackUp";
                if ((Directory.Exists(strRegistryPath)))
                {
                    FileInfo objFileInfo = new FileInfo(strRegistryPath);
                    System.DateTime dtRegistryDate;
                    System.DateTime dtRegistryTime;
                    System.DateTime dtTodayDate;
                    dtRegistryDate = objFileInfo.CreationTime.Date;
                    dtRegistryTime = objFileInfo.CreationTime;
                    dtTodayDate = System.DateTime.Today;
                    // IF THE SQL FILE EXIST IN TODAYS NAME THEN DELETE THE SQL FILE AND RECREATE SAME
                    if ((dtRegistryDate == System.DateTime.Today))
                    {
                        SQLScriptDelete(Database);
                    }
                }

                RegistryKey NPkey;
                RegistryKey TCPkey;
                // CHECK FOR SQL EXISTANCE IN WHICH SUB FOLDER.
                if ((Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib\\NP", true) != null))
                {
                    NPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib\\NP", true);
                    string strNPKey = NPkey.GetValue("Enabled").ToString();
                    NPkey.SetValue("Enabled", 1);
                    TCPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib\\Tcp", true);
                    if ((TCPkey != null))
                    {
                        string strTCPKey = TCPkey.GetValue("Enabled").ToString();
                        TCPkey.SetValue("Enabled", 1);
                    }
                }
                else if ((Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.2\\MSSQLServer\\SuperSocketNetLib\\NP", true) != null))
                {
                    NPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.2\\MSSQLServer\\SuperSocketNetLib\\NP", true);
                    string str = NPkey.GetValue("Enabled").ToString();
                    NPkey.SetValue("Enabled", 1);
                    TCPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.2\\MSSQLServer\\SuperSocketNetLib\\Tcp", true);
                    if ((TCPkey != null))
                    {
                        string str1 = TCPkey.GetValue("Enabled").ToString();
                        TCPkey.SetValue("Enabled", 1);
                    }
                }
                else if ((Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.3\\MSSQLServer\\SuperSocketNetLib\\NP", true) != null))
                {
                    NPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.3\\MSSQLServer\\SuperSocketNetLib\\NP", true);
                    string str = NPkey.GetValue("Enabled").ToString();
                    NPkey.SetValue("Enabled", 1);
                    TCPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.3\\MSSQLServer\\SuperSocketNetLib\\Tcp", true);
                    if ((TCPkey != null))
                    {
                        string str1 = TCPkey.GetValue("Enabled").ToString();
                        TCPkey.SetValue("Enabled", 1);
                    }
                }
                else if ((Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.4\\MSSQLServer\\SuperSocketNetLib\\NP", true) != null))
                {
                    NPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.4\\MSSQLServer\\SuperSocketNetLib\\NP", true);
                    string str = NPkey.GetValue("Enabled").ToString();
                    NPkey.SetValue("Enabled", 1);
                    TCPkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.4\\MSSQLServer\\SuperSocketNetLib\\Tcp", true);
                    if ((TCPkey != null))
                    {
                        string str1 = TCPkey.GetValue("Enabled").ToString();
                        TCPkey.SetValue("Enabled", 1);
                    }
                }

                //RUN THE BATCH FILE TO CREATE DATABASE SCHEMA.
                objStartProcess = new System.Diagnostics.ProcessStartInfo(strFilepath);
                objStartProcess.CreateNoWindow = true;    //for splash
                objStartProcess.UseShellExecute = false; //for splash
                pStart1.StartInfo = objStartProcess;
                System.Windows.Forms.Application.DoEvents();
                //frm.Focus();
                System.Windows.Forms.Application.DoEvents();
                pStart1.Start();
                do
                {
                    System.Windows.Forms.Application.DoEvents();
                    System.Threading.Thread.Sleep(250);
                }
                while (pStart1.HasExited);
                System.Windows.Forms.Application.DoEvents();
                pStart1.WaitForExit();
                pStart1.Close();
                File.Delete(strFilepath);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        /// <summary>
        /// To delete the SQL Database files from the local system
        /// Will delete the .mdf and .ldf files from the local system
        /// </summary>
        /// <param name="Database">A string representing the name of the Database</param>
        public static void SQLScriptDelete(string Database)
        {
            string strDatabasePathMDF;
            string strDatabasePathLDF;

            try
            {
                string strRegistryPath;
                strRegistryPath = Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SQL Server\\SQLEXPRESS\\Setup", "SQLPath", null).ToString();
                strRegistryPath += "\\Data";
                if (Directory.Exists(strRegistryPath))
                {
                    if (Database != "")
                    {
                        strDatabasePathMDF = strRegistryPath + "\\" + Database + ".mdf";
                        strDatabasePathLDF = strRegistryPath + "\\" + Database + "_log.LDF";
                        if (System.IO.File.Exists(strDatabasePathMDF))
                        {
                            System.IO.File.Delete(strDatabasePathMDF);
                        }
                        if (System.IO.File.Exists(strDatabasePathLDF))
                        {
                            System.IO.File.Delete(strDatabasePathLDF);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}