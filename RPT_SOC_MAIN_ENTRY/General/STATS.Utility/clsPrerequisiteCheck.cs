#region "Using"

using Microsoft.Win32;
using System;
using System.Drawing.Printing;
using System.IO;

#endregion "Using"

namespace STATS.Utility
{
    #region "Comments"

    /*---------------------------------------------------------------------------
     Class Name        : clsUtility
     Author            : Sandeep Kumar Nanda
     Created Date      : 29 May 2008
     Description       :
    -----------------------------------------------------------------------------
    ---------------------------Modification Log----------------------------------
    -----------------------------------------------------------------------------
    ID             Modified By         Modified date           Description
    -----------------------------------------------------------------------------
    1.            RAVI KRISHNA G         22/04/09        Added Comments & Desc
    -----------------------------------------------------------------------------*/

    #endregion "Comments"

    public class clsPrerequisiteCheck
    {
        /// <summary>
        /// Enumerated values representing the type of the printers available
        /// </summary>
        public enum Printer
        {
            AllPrinter,
            DefaultPrinter
        }

        /// <summary>
        /// To check for the availability of the Printer
        /// </summary>
        /// <param name="PrintType">A Printer variable, where 0 represents the AllPrinters and 1 represents the Default Printer</param>
        /// <returns>A string denoting the list of printers names</returns>
        public static string CheckForPrinter(Printer PrintType)
        {
            try
            {
                PrintDocument prtdoc = new PrintDocument();
                string strDefaultPrinter = "";
                //ACCORDING TO PRINT TYPE WE NEED TO RETURN EITHER DEFAULT PRINTER OR LIST OF ALL PRINTERS.
                switch (PrintType)
                {
                    case Printer.DefaultPrinter:
                        //THIS WILL RETURN DEFAULT PRINTER
                        strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;
                        break;

                    case Printer.AllPrinter:
                        //THIS WILL RETURN LIST OF ALL PRINTER
                        foreach (string strPrinter in PrinterSettings.InstalledPrinters)
                        {
                            strDefaultPrinter = strDefaultPrinter + strPrinter + "\n";
                        }
                        break;
                }
                return strDefaultPrinter;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To check for the existence of a given Font name
        /// </summary>
        /// <param name="FontName">A String representing the Font name</param>
        /// <returns>A Boolean value denoting True or False based on the font availability</returns>
        public static bool IsFontAvailable(string FontName)
        {
            try
            {
                string strDirectory;
                strDirectory = (string)System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).Substring(0, 1);
                bool blnFontRetType = false;
                if (Directory.Exists(strDirectory + ":\\WINDOWS\\Fonts"))
                {
                    string[] Files = Directory.GetFiles(strDirectory + ":\\WINDOWS\\Fonts");
                    //LOOP THROUGH EACH OF THE FONT IN THE LIST. MATCH WITH FONT NAME GIVEN IF MATCH RETURN TRUE ELSE FALSE
                    foreach (string Filename in Files)
                    {
                        if (Filename == strDirectory + ":\\WINDOWS\\Fonts\\" + FontName)
                        {
                            blnFontRetType = true;
                        }
                    }
                }
                else
                {
                    blnFontRetType = false;
                }
                return blnFontRetType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //SANDEEP CREATED THIS FUNCTION TO GET ALL THE AVAILABLE FONTS
        public static string[] GetAvailableFont()
        {
            try
            {
                string[] Fonts = null;
                if (Directory.Exists("C:\\WINDOWS\\Fonts"))
                {
                    string[] Files = Directory.GetFiles("C:\\WINDOWS\\Fonts");
                    //LOOP THROUGH LIST OF ALL FONT AND RETURN ALL THE FONTS AS A LIST
                    foreach (string Filename in Files)
                    {
                        Fonts = Files;
                    }
                }
                return Fonts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Enumerated values representing the type of the Software to be validated
        /// </summary>
        public enum ExportFileType
        {
            Acrobat,
            Excel
        }

        /// <summary>
        /// To check for the existence of the software in the local system
        /// </summary>
        /// <param name="FileType">ExportFileType Enumerated value denoting the type of the software</param>
        /// <returns>A string denoting the status of the software present in the local system</returns>
        public static string CheckForApplicationType(ExportFileType FileType)
        {
            try
            {
                string strFileType = "Software doesn't exist";
                RegistryKey key;
                //ACCORDING TO FILE TYPE WE NEED TO FIND OUT WHETHER THE SW EXIST OR NOT.
                switch (FileType)
                {
                    case ExportFileType.Acrobat:
                        key = Registry.ClassesRoot.OpenSubKey(".pdf");
                        strFileType = "Acrobat Reader exists";
                        break;

                    case ExportFileType.Excel:
                        key = Registry.ClassesRoot.OpenSubKey(".xls");
                        strFileType = "Microsoft Excel exists";
                        break;
                }
                return strFileType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}