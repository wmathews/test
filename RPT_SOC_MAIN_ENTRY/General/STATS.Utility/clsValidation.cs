#region "Using"

using System;
using System.Globalization;
using System.Text.RegularExpressions;

#endregion "Using"

namespace STATS.Utility
{
    #region "Comments"

    /*--------------------------------------------------------------------------
     CLASS NAME        : clsValidation
     AUTHOR            : SASIKALA
     CREATED DATE      : 23 MAY 2008
     DESCRIPTION       : CONTAINS GENERAL VALIDATION FUNCTIONS
    ----------------------------------------------------------------------------
    ---------------------------Modification Log---------------------------------
    ----------------------------------------------------------------------------
    ID             Modified By         Modified date           Description
    ----------------------------------------------------------------------------
    1.            RAVI KRISHNA G         22/04/09        Added Comments & Desc
    ----------------------------------------------------------------------------*/

    #endregion "Comments"

    public class clsValidation
    {
        #region "Shared Methods"

        /// <summary>
        /// Checks the input string to be a valid Email
        /// </summary>
        /// <param name="Email">String representing the email address</param>
        /// <returns>A Boolean value which denotes True or Fales based on the validity</returns>
        public static Boolean ValidateEmail(string Email)
        {
            try
            {
                string strMatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|2{1}[0-5]{2})\.([0-1]?[0-9]{1,2}|2{1}[0-5]{2})\."
                + @"([0-1]?[0-9]{1,2}|2{1}[0-5]{2})\.([0-1]?[0-9]{1,2}|2{1}[0-5]{2})){1}|"
                + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

                return Regex.IsMatch(Email, strMatchEmailPattern);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks the input string to be a valid Numerical Value
        /// </summary>
        /// <param name="Value">String representing the input value</param>
        /// <returns>A Boolean value which denotes True or Fales based on the validity</returns>
        public static Boolean ValidateNumeric(string Value)
        {
            try
            {
                string strMatchNumeral = "^([0-9]+)$";
                //string strMatchNumeral = "[0-9]";
                //string strMatchString = "^\\d+$";
                return Regex.IsMatch(Value, strMatchNumeral);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Boolean ValidateTime(string Value)
        {
            try
            {
                string strMatchNumeral = "^([0-9][0-9]*[0-9]*|)\\:([0-5][0-9]*)$";

                return Regex.IsMatch(Value, strMatchNumeral);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks the input string to be a valid Alphabet set
        /// </summary>
        /// <param name="Value">String representing the Alphabet set</param>
        /// <returns>A Boolean value which denotes True or Fales based on the validity</returns>
        public static Boolean ValidateAlphabets(string Value)
        {
            try
            {
                string strMatchString = "[a-zA-Z]";
                //string strMatchString = "^[a-zA-Z]+$";
                return Regex.IsMatch(Value, strMatchString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks the input string to be a valid Alphanumeric
        /// </summary>
        /// <param name="Value">String representing the Alphanumeric set</param>
        /// <returns>A Boolean value which denotes True or False based on the validity</returns>
        public static Boolean ValidateAlphaNumeric(string Value)
        {
            try
            {
                string strMatchAlphaNumeric = "[a-zA-Z0-9-_'.]";
                return Regex.IsMatch(Value, strMatchAlphaNumeric);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validates the length of a string with a given integer value
        /// </summary>
        /// <param name="Value">String representing the value to find the length for</param>
        /// <param name="Length">Integer Value representing the length of the string</param>
        /// <returns>A Boolean value which denotes True or False based on the validity</returns>
        public static Boolean ValidateLength(string Value, int Length)
        {
            try
            {
                Value = Value.ToString().Trim();
                if (Value.Length == Length)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Represents the Enumerated values used for ValidateRange(RangeValidation, string, int, int) method
        /// </summary>
        public enum RangeValidation
        {
            INTEGER,
            DECIMAL
        }

        /// <summary>
        /// Validates a given Integer or Decimal value with a given range
        /// </summary>
        /// <param name="RangeType">Contains two values, 0 denoting Integer and 1 denoting Decimal, representing the value to be passed as an input</param>
        /// <param name="Value">A String representing the value to be validated</param>
        /// <param name="MinValue">An Integer value representing the Minimum value to be validated</param>
        /// <param name="MaxValue">An Integer value representing the Maximum value to be validated</param>
        /// <returns>A Boolean value which denotes True or False based on the validity</returns>
        public static Boolean ValidateRange(RangeValidation RangeType, string Value, int MinValue, int MaxValue)
        {
            int intValue;
            double dbValue;

            try
            {
                switch (RangeType)
                {
                    case RangeValidation.INTEGER:
                        intValue = Convert.ToInt16(Value);
                        if (intValue >= MinValue && intValue <= MaxValue)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case RangeValidation.DECIMAL:
                        int getPos = Value.IndexOf(".");
                        Value = Value.Substring(0, getPos + 3);

                        dbValue = Convert.ToDouble(Value);

                        if (dbValue >= MinValue && dbValue <= MaxValue)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //FUNCTION RETURNS TRUE FOR NON-EMPTY STRING
        public Boolean ValidateEmpty(string Text)
        {
            try
            {
                if (Text.Trim() == "")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static decimal GetUSFormatDecimal(string Value)
        {
            try
            {
                decimal decValue;
                NumberFormatInfo numFormat;

                numFormat = new CultureInfo("en-US", false).NumberFormat;

                decValue = Convert.ToDecimal(Value, numFormat);

                return decValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion "Shared Methods"
    }
}