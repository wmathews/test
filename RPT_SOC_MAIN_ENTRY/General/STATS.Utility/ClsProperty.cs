﻿namespace STATS.Utility
{
    public class ClsProperty
    {
        private static ClsProperty Instance;
        public int m_intUserID;

        public static ClsProperty GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT
            if (Instance == null)
            {
                Instance = new ClsProperty();
            }
            return Instance;
        }

        public int UserID
        {
            get
            {
                return m_intUserID;
            }
            set
            {
                m_intUserID = value;
            }
        }
    }
}