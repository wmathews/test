﻿using System.IO;

//using System.Linq;
using System.Net;

namespace STATS.Utility
{
    /// <summary>
    /// Represents a helper class for transfer of files via FTP.
    /// </summary>
    public static class FTPHelper
    {
        #region Fields

        private static FtpWebRequest ftpRequest;
        private static Stream fileStream;

        #endregion Fields

        #region Properties

        /// <summary>
        /// A string representing the Hostname or IP address (Ex: ftp://hostname or ftp://127.0.0.1).
        /// </summary>
        public static string Host
        {
            get;
            set;
        }

        /// <summary>
        /// A string representing the User Name.
        /// </summary>
        public static string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// A string representing the password.
        /// </summary>
        public static string Password
        {
            get;
            set;
        }

        #endregion Properties

        #region Enums

        /// <summary>
        /// A Yes/No Value denoting whether the File should be deleted once tranferred successfully to the Server.
        /// </summary>
        public enum FileDel
        {
            Yes, No
        }

        #endregion Enums

        #region Shared Methods

        /// <summary>
        /// To upload the files in a given directory to the FTP Server.
        /// </summary>
        /// <param name="sourceDirectory">A string representing the directory full path.</param>
        /// <param name="fDel">An enumerated value denoting the deletion of files after successfull transfer of files.</param>
        /// <returns>A Boolean value representing the success/failure of File transfer.</returns>
        public static bool FileUpload(string sourceDirectory, FileDel fDel)
        {
            try
            {
                bool fileUploaded = false;
                DirectoryInfo dirInfo = new DirectoryInfo(sourceDirectory);
                if (dirInfo.Exists)
                {
                    foreach (FileInfo fileInfo in dirInfo.GetFiles())
                    {
                        try
                        {
                            byte[] uploadFile = System.IO.File.ReadAllBytes(fileInfo.FullName);
                            ftpRequest = (FtpWebRequest)System.Net.WebRequest.Create(Host + @"/" + fileInfo.Name);
                            ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                            ftpRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                            ftpRequest.Timeout = 180000;
                            fileStream = ftpRequest.GetRequestStream();
                            fileStream.Write(uploadFile, 0, uploadFile.Length);
                            fileUploaded = true;
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                        catch
                        {
                            fileUploaded = false;
                        }
                        finally
                        {
                        }
                    }
                    if (fileUploaded == true)
                    {
                        if (fDel == FileDel.Yes)
                        {
                            foreach (FileInfo fileInfo in dirInfo.GetFiles())
                            {
                                fileInfo.Delete();
                            }
                        }
                    }
                }
                if (fileUploaded == false)
                    return false;
                else
                    return true;
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// To upload a given file in the directory to the FTP Server.
        /// </summary>
        /// <param name="sourceDirectory">A string representing the directory full path.</param>
        /// <param name="fileName">A string representing the file name that has to be transferred.</param>
        /// <param name="fDel">An enumerated value denoting the deletion of file after successfull transfer of file.</param>
        /// <returns>A Boolean value representing the success/failure of File transfer.</returns>
        public static bool FileUpload(string sourceDirectory, string fileName, FileDel fDel)
        {
            try
            {
                bool fileUploaded = false;
                DirectoryInfo dirInfo = new DirectoryInfo(sourceDirectory);
                if (dirInfo.Exists)
                {
                    try
                    {
                        byte[] uploadFile = System.IO.File.ReadAllBytes(sourceDirectory + "\\" + fileName);
                        ftpRequest = (FtpWebRequest)System.Net.WebRequest.Create(Host + @"/" + fileName);
                        ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                        ftpRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                        ftpRequest.Timeout = 180000;
                        fileStream = ftpRequest.GetRequestStream();
                        fileStream.Write(uploadFile, 0, uploadFile.Length);
                        fileUploaded = true;
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch
                    {
                        fileUploaded = false;
                    }
                    finally
                    {
                    }
                    if (fileUploaded == true)
                    {
                        if (fDel == FileDel.Yes)
                        {
                            File.Delete(sourceDirectory + "\\" + fileName);
                        }
                    }
                }
                if (fileUploaded == false)
                    return false;
                else
                    return true;
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// To create a folder and upload a given file in the directory to the FTP Server.
        /// </summary>
        /// <param name="sourceDirectory">A string representing the directory full path.</param>
        /// <param name="fileName">A string representing the file name that has to be transferred.</param>
        /// <param name="fDel">An enumerated value denoting the deletion of file after successfull transfer of file.</param>
        /// <param name="fileDirectory">A string representing the folder name that has to be created in the server.</param>
        /// <returns>A Boolean value representing the success/failure of File transfer.</returns>
        public static bool FileUpload(string sourceDirectory, string fileName, FileDel fDel, string fileDirectory)
        {
            try
            {
                bool fileUploaded = false;
                DirectoryInfo dirInfo = new DirectoryInfo(sourceDirectory);
                if (dirInfo.Exists)
                {
                    try
                    {
                        FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(Host + @"/" + fileDirectory);
                        ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                        ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                        try
                        {
                            FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                            ftpRequest.Abort();
                        }
                        catch
                        {
                            ftpRequest = (FtpWebRequest)FtpWebRequest.Create(Host + @"/" + fileDirectory);
                            ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                            ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                            FtpWebResponse FTPRes = (FtpWebResponse)ftpRequest.GetResponse();
                            ftpRequest.Abort();
                        }
                        byte[] uploadFile = System.IO.File.ReadAllBytes(sourceDirectory + "\\" + fileName);
                        ftpRequest = (FtpWebRequest)System.Net.WebRequest.Create(Host + @"/" + fileDirectory + "\\" + fileName);
                        ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                        ftpRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                        ftpRequest.Timeout = 180000;
                        fileStream = ftpRequest.GetRequestStream();
                        fileStream.Write(uploadFile, 0, uploadFile.Length);
                        fileUploaded = true;
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch
                    {
                        fileUploaded = false;
                    }
                    finally
                    {
                    }
                    if (fileUploaded == true)
                    {
                        if (fDel == FileDel.Yes)
                        {
                            File.Delete(sourceDirectory + "\\" + fileName);
                        }
                    }
                }
                if (fileUploaded == false)
                    return false;
                else
                    return true;
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// To create a folder and upload a given file in the directory to the FTP Server.
        /// </summary>
        /// <param name="sourceDirectory">A string representing the directory full path.</param>
        /// <param name="fileName">A string representing the file name that has to be transferred.</param>
        /// <param name="fDel">An enumerated value denoting the deletion of file after successfull transfer of file.</param>
        /// <param name="fileDirectory">A string representing the folder name that has to be created in the server.</param>
        /// <param name="fileSubDirectory">A string representing the Sub folder name that has to be created in the server.</param>
        /// <returns>A Boolean value representing the success/failure of File transfer.</returns>
        public static bool FileUpload(string sourceDirectory, string fileName, FileDel fDel, string fileDirectory, string fileSubDirectory)
        {
            try
            {
                bool fileUploaded = false;
                DirectoryInfo dirInfo = new DirectoryInfo(sourceDirectory);
                if (dirInfo.Exists)
                {
                    try
                    {
                        //Directory existence check
                        ftpRequest = (FtpWebRequest)WebRequest.Create(Host + @"/" + fileDirectory);
                        ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                        ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                        try
                        {
                            FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                            ftpRequest.Abort();
                            ftpRequest = (FtpWebRequest)WebRequest.Create(Host + @"/" + fileDirectory + @"/" + fileSubDirectory);
                            ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                            try
                            {
                                FtpWebResponse responseSub = (FtpWebResponse)ftpRequest.GetResponse();
                                ftpRequest.Abort();
                            }
                            catch
                            {
                                //Sub directory creation
                                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(Host + @"/" + fileDirectory + @"/" + fileSubDirectory);
                                ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                                FtpWebResponse FTPResSub = (FtpWebResponse)ftpRequest.GetResponse();
                                ftpRequest.Abort();
                            }
                        }
                        catch
                        {
                            //Main directory creation
                            ftpRequest = (FtpWebRequest)FtpWebRequest.Create(Host + @"/" + fileDirectory);
                            ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                            ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                            FtpWebResponse FTPRes = (FtpWebResponse)ftpRequest.GetResponse();
                            ftpRequest.Abort();

                            //Sub directory creation
                            ftpRequest = (FtpWebRequest)FtpWebRequest.Create(Host + @"/" + fileDirectory + @"/" + fileSubDirectory);
                            ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                            ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                            FtpWebResponse FTPResSub = (FtpWebResponse)ftpRequest.GetResponse();
                            ftpRequest.Abort();
                        }
                        //File upload
                        //ftpRequestSub.Abort();
                        byte[] uploadFile = System.IO.File.ReadAllBytes(sourceDirectory + "\\" + fileName);
                        ftpRequest = (FtpWebRequest)System.Net.WebRequest.Create(Host + @"/" + fileDirectory + @"/" + fileSubDirectory + "\\" + fileName);
                        ftpRequest.Credentials = new NetworkCredential(UserName, Password);
                        ftpRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                        ftpRequest.Timeout = 180000;
                        fileStream = ftpRequest.GetRequestStream();
                        fileStream.Write(uploadFile, 0, uploadFile.Length);
                        fileUploaded = true;
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch
                    {
                        fileUploaded = false;
                    }
                    finally
                    {
                    }
                    if (fileUploaded == true)
                    {
                        if (fDel == FileDel.Yes)
                        {
                            File.Delete(sourceDirectory + "\\" + fileName);
                        }
                    }
                }
                if (fileUploaded == false)
                    return false;
                else
                    return true;
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion Shared Methods
    }
}