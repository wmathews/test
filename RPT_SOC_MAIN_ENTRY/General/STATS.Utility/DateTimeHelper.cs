﻿using System;
using System.Globalization;

namespace STATS.Utility
{
    public class DateTimeHelper
    {
        public enum InputFormat
        {
            /// <summary>
            /// yyyy/MM/dd hh:mm:ss
            /// </summary>
            ISOFormat,

            /// <summary>
            /// MM/dd/yyyy hh:mm:ss
            /// </summary>
            CurrentFormat,

            /// <summary>
            /// Current format of the system
            /// </summary>
            RegionalSettings
        }

        public enum OutputFormat
        {
            /// <summary>
            /// yyyy/MM/dd hh:mm:ss
            /// </summary>
            ISOFormat,

            /// <summary>
            /// MM/dd/yyyy hh:mm:ss
            /// </summary>
            CurrentFormat,

            /// <summary>
            /// Current format of the system
            /// </summary>
            RegionalSettings
        }

        //public enum InputFormat
        //{
        //    ISOFormat,  // yyyy/MM/dd hh:mm:ss
        //    CurrentFormat, // MM/dd/yyyy hh:mm:ss
        //    RegionalSettings // current system format
        //}

        //public enum OutputFormat
        //{
        //    ISOFormat,  // yyyy/MM/dd hh:mm:ss
        //    CurrentFormat, // MM/dd/yyyy hh:mm:ss
        //    RegionalSettings // current system format
        //}

        //CustomFormat    // developer preferred format

        /// <summary>
        /// Gets the DateTime object
        /// </summary>
        /// <param name="DateString">String parameter should contain datetime value</param>
        /// <param name="DateFormat">Enumerator which represents datetime format of the DateString</param>
        /// <returns>DateTime object</returns>
        public static DateTime GetDate(string DateString, InputFormat DateFormat)
        {
            try
            {
                DateTime objDateTime;
                string[] arrDate;

                string strDate = "";

                switch (DateFormat)
                {
                    case InputFormat.ISOFormat:
                        // yyyy/MM/dd hh:mm:tt
                        objDateTime = DateTime.Parse(DateString);
                        break;

                    case InputFormat.CurrentFormat:
                        // MM/dd/yyyy hh:mm:tt
                        if (DateString.IndexOf("/") > 0)
                        {
                            arrDate = DateString.Split('/');
                            strDate = arrDate[2].Split(' ')[0] + "/" + arrDate[0] + "/" + arrDate[1];
                            strDate = strDate + " " + arrDate[2].Split(' ')[1]; //time portion
                            if (arrDate[2].Split(' ').Length > 2)
                            {
                                strDate = strDate + " " + arrDate[2].Split(' ')[2]; // am / pm
                            }
                            objDateTime = DateTime.Parse(strDate);
                        }
                        else
                        {
                            arrDate = DateString.Split('-');
                            strDate = arrDate[2].Split(' ')[0] + "-" + arrDate[0] + "-" + arrDate[1];
                            strDate = strDate + " " + arrDate[2].Split(' ')[1]; //time portion
                            if (arrDate[2].Split(' ').Length > 2)
                            {
                                strDate = strDate + " " + arrDate[2].Split(' ')[2]; // am / pm
                            }
                            objDateTime = DateTime.Parse(strDate);
                        }
                        break;

                    case InputFormat.RegionalSettings:
                        //objDateTime = DateTime.Parse(DateString);
                        objDateTime = DateTime.Parse(String.Format("{0:" + CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern + "}", DateTime.Parse(DateString)));
                        break;

                    default:
                        objDateTime = DateTime.Parse(DateString);
                        break;
                }

                return objDateTime;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///// <summary>
        ///// Gets the DateTime object
        ///// </summary>
        ///// <param name="DateString">string parameter should contain datetime value</param>
        ///// <param name="InputDateFormat">string parameter which represents datetime format of the DateString</param>
        ///// <returns>DateTime</returns>
        //public static DateTime GetDate(string DateString, string InputDateFormat)
        //{
        //    try
        //    {
        //        DateTime objDateTime;
        //        string strDate = "";
        //        string[] arrDate;
        //        string DD = "";
        //        string MM = "";
        //        string YYYY = "";
        //        string TIME = "";

        //        //arrDate = DateString.Split('/');

        //        //DateString = arrDate[0].Length == 1 ? "0" + arrDate[0] : arrDate[0];
        //        //DateString = DateString + "/" + (arrDate[1].Length == 1 ? "0" + arrDate[1] : arrDate[1]);
        //        //DateString = DateString + "/" + (arrDate[2].Length == 1 ? "0" + arrDate[2] : arrDate[2]);

        //        //DateString = String.Format("{0:" + InputDateFormat + "}", DateString);

        //        for (int i = 0; i < InputDateFormat.Length - 1; i++)
        //        {
        //            if (InputDateFormat.Substring(i, 1) != "/")
        //            {
        //                if (InputDateFormat.Substring(i, 2) == "MM")
        //                {
        //                    MM = DateString.Substring(i, 2);
        //                }
        //                else if (InputDateFormat.Substring(i, 2).ToUpper() == "DD")
        //                {
        //                    DD = DateString.Substring(i, 2);
        //                }
        //                else if (InputDateFormat.Substring(i, 4).ToUpper() == "YYYY")
        //                {
        //                    YYYY = DateString.Substring(i, 4);
        //                }
        //            }
        //            else if (InputDateFormat.Substring(i, 1) == " ")
        //            {
        //                TIME = DateString.Substring(i);
        //            }
        //        }

        //        strDate = YYYY + "/" + MM + "/" + DD;

        //        strDate = strDate + " " + TIME.Trim();

        //        objDateTime = Convert.ToDateTime(String.Format("{0:" + InputDateFormat + "}", DateString));

        //        return objDateTime;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Gets the DateTime value as a string
        /// </summary>
        /// <param name="ObjDateTime">System.DateTime object</param>
        /// <param name="DateFormat">Enumerator represents the output date format you want</param>
        /// <returns></returns>
        public static string GetDateString(DateTime ObjDateTime, OutputFormat DateFormat)
        {
            try
            {
                string strDate = "";

                switch (DateFormat)
                {
                    case OutputFormat.ISOFormat:
                        // yyyy/MM/dd hh:mm:tt
                        strDate = ObjDateTime.Year + "/" + ObjDateTime.Month + "/" + ObjDateTime.Day;
                        strDate = strDate + " " + ObjDateTime.Hour + ":" + ObjDateTime.Minute + ":" + ObjDateTime.Second;
                        if (ObjDateTime.Hour < 12)
                        {
                            strDate = strDate + " " + "AM";
                        }
                        else
                        {
                            strDate = strDate + " " + "PM";
                        }
                        break;

                    case OutputFormat.CurrentFormat:
                        // MM/dd/yyyy hh:mm:tt
                        strDate = ObjDateTime.Month + "/" + ObjDateTime.Day + "/" + ObjDateTime.Year;
                        strDate = strDate + " " + ObjDateTime.Hour + ":" + ObjDateTime.Minute + ":" + ObjDateTime.Second;
                        if (ObjDateTime.Hour < 12)
                        {
                            strDate = strDate + " " + "AM";
                        }
                        else
                        {
                            strDate = strDate + " " + "PM";
                        }
                        break;

                    case OutputFormat.RegionalSettings:
                        strDate = String.Format("{0:" + CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern + "}", ObjDateTime);
                        break;

                    default:
                        strDate = ObjDateTime.ToString();
                        break;
                }

                return strDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the DateTime value as a string
        /// </summary>
        /// <param name="ObjDateTime">System.DateTime object</param>
        /// <param name="OutputFormat">string represents the output date format you want</param>
        /// <returns></returns>
        public static string GetDateString(DateTime ObjDateTime, string OutputFormat)
        {
            try
            {
                string strDate = "";

                OutputFormat = OutputFormat.Trim();

                strDate = String.Format("{0:" + OutputFormat + "}", ObjDateTime);
                char splitChar = Convert.ToChar(" ");

                if (strDate.Split(splitChar).Length > 2 && OutputFormat.Split(splitChar).Length > 2)
                {
                    string[] str = strDate.Split(splitChar);
                    strDate = str[0] + " " + str[1];

                    if (ObjDateTime.Hour < 12)
                    {
                        strDate = strDate + " " + "AM";
                    }
                    else
                    {
                        strDate = strDate + " " + "PM";
                    }
                }

                //string separetor  = CultureInfo.CurrentCulture.DateTimeFormat.DateSeparator;

                return strDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the DateTime value as a string
        /// </summary>
        /// <param name="ObjDateTime">System.DateTime object</param>
        /// <param name="OutputFormat">string represents the output date format you want</param>
        /// <returns></returns>
        //public static string GetDateString(DateTime ObjDateTime, string OutputFormat)
        //{
        //    try
        //    {
        //        string strDate = "";

        //        strDate = String.Format("{0:" + OutputFormat + "}", ObjDateTime);

        //        return strDate;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}