#region "using"
using System;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OracleClient;
using System.Data;
#endregion

namespace STATS.DataAccess
{
    #region "Comments"
    /*--------------------------------------------------------------------------
     Class Name        : clsDataAccess
     Author            : Sandeep Kumar Nanda
     Created Date      : 06 May 2008
     Description       :
    ----------------------------------------------------------------------------
    ---------------------------Modification Log---------------------------------
    ----------------------------------------------------------------------------
    ID             Modified By         Modified date           Description
    ----------------------------------------------------------------------------
    1.          RAVI KRISHNA G          22/04/09        Added Comments & Desc
    ----------------------------------------------------------------------------*/
    #endregion

    public class clsDataAccess
    {

        #region "Constants & Variables"
        //SHARED FUNCTION CAN ONLY ACCESS SHARED MEMBER
        private static clsDataAccess Instance;
        public SqlConnection sqlConnection;

        #endregion

        #region "Constructor"
        //PROTECTED CONSTRUCTOR
        protected clsDataAccess()
        {

        }

        #endregion

        #region "Shared Methods"

        /// <summary>
        /// To create and initialize a new SQL Connection Object
        /// </summary>
        /// <returns>A SQL Connection object</returns>
        public SqlConnection GetSQLConnection()
        {
            try
            {
                string strConnectionString;
                strConnectionString = System.Configuration.ConfigurationManager.AppSettings["strSqlCon"];
                sqlConnection = new SqlConnection(strConnectionString);
                sqlConnection.Open();
                return sqlConnection;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (sqlConnection.State == ConnectionState.Open)
                //{
                //    //sqlConnection.Close();
                //}
            }
        }

        /// <summary>
        /// To create a new singleton instance of the class
        /// </summary>
        /// <returns>A singleton static object</returns>
        public static clsDataAccess GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT
            if (Instance == null)
            {
                Instance = new clsDataAccess();
            }
            return Instance;
        }

        #endregion
    }
}
