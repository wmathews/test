﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddVenu
' Author        : Shravani
' Created Date  : 20th Aug,2009
' Description   : Business Logic for Add Team
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddVenue

#Region " Constants & Variables "
    Shared m_myInstance As clsAddVenue
    Private m_objAddVenue As SoccerDAL.clsAddVenue = SoccerDAL.clsAddVenue.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsAddVenue
        If m_myInstance Is Nothing Then
            m_myInstance = New clsAddVenue
        End If
        Return m_myInstance
    End Function
#End Region


    Public Function GetLeague() As DataSet
        Try
            Return m_objAddVenue.GetLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub InsertAddVenue(ByVal LeagueID As Integer, ByVal FieldName As String, ByVal FieldNickName As String, ByVal City As String, ByVal Capacity As Integer, ByVal Grass As Char, ByVal Dome As Char)
        Try
            m_objAddVenue.InsertAddVenue(LeagueID, FieldName, FieldNickName, City, Capacity, Grass, Dome)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
  
    Public Sub UpdateAddVenue(ByVal LeagueID As Integer, ByVal FieldID As Integer, ByVal FieldName As String, ByVal FieldNickName As String, ByVal City As String, ByVal Capacity As Integer, ByVal Grass As Char, ByVal Dome As Char)
        Try
            m_objAddVenue.UpdateAddVenue(LeagueID, FieldID, FieldName, FieldNickName, City, Capacity, Grass, Dome)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DeleteAddVenue(ByVal FieldID As Integer, ByVal LeagueID As Integer)
        Try
            m_objAddVenue.DeleteAddVenue(FieldID, LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function SelectAddVenue() As DataSet
        Try
            Return m_objAddVenue.SelectAddVenue()
        Catch ex As Exception
            Throw ex
        End Try
    End Function



End Class
