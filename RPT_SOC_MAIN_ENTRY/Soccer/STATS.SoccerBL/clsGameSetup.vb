﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"

Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsGameSetUp
' Author        : Shravani
' Created Date  : 28th April,2009
' Description   : Business Logic GameSetup
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsGameSetup

#Region " Constants & Variables "
    Private m_objDataAccess As New STATS.SoccerDAL.clsGameSetup
    Private m_dsGameSetup As New DataSet
    Shared m_myInstance As clsGameSetup
#End Region

    Public Shared Function GetInstance() As clsGameSetup
        If m_myInstance Is Nothing Then
            m_myInstance = New clsGameSetup
        End If
        Return m_myInstance
    End Function

    ''' <summary>
    ''' Gets the GameSetup Details assigned to the reporter
    ''' </summary>
    ''' <param name="LeagueID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectGameSetup(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal LeagueID As Integer, ByVal AwayTeamID As Integer, ByVal HomeTeamID As Integer, ByVal LanguageID As Int32, ByVal Tier As Int32, ByVal Modu As Int32) As DataSet
        m_dsGameSetup = m_objDataAccess.SeletGameSetup(GameCode, FeedNumber, LeagueID, AwayTeamID, HomeTeamID, LanguageID, Tier, Modu)
        Return m_dsGameSetup
    End Function

    ''' <summary>
    ''' Inserts or Updates the Gamesetup info
    ''' </summary>
    ''' <param name="GameSetup"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertUpdateGameSetup(ByVal GameSetup As DataSet) As Integer
        Try
            Dim strGameSetUpXmal As String = GameSetup.GetXml()
            Return m_objDataAccess.InsertUpdateGameSetup(strGameSetUpXmal)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Selects Official and Match info and displays in the Module1PBP form
    ''' </summary>
    ''' <param name="GameCode">An integer represents the GameCode</param>
    ''' <param name="FeedNumber">An integer represents the FeedNumber</param>
    ''' <param name="LeagueID">An integer represents the LeagueID</param>
    ''' <returns>A Dataset which contains Official and Match information</returns>
    ''' <remarks></remarks>
    Public Function GetOfficialAndMatchInfo(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal LeagueID As Integer) As DataSet
        m_dsGameSetup = m_objDataAccess.GetOfficialAndMatchInfo(GameCode, FeedNumber, LeagueID)
        Return m_dsGameSetup
    End Function

End Class
