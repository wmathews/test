﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region


#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmPenaltyShootoutScore
' Author        : Shravani
' Created Date  : 23rd April 2010
' Description   : 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsPenaltyShootoutScore

#Region "Constants & Variables"
    Shared m_myInstance As clsPenaltyShootoutScore
    Private m_objShootoutscore As SoccerDAL.clsPenaltyShootoutScore = SoccerDAL.clsPenaltyShootoutScore.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsPenaltyShootoutScore
        If m_myInstance Is Nothing Then
            m_myInstance = New clsPenaltyShootoutScore
        End If
        Return m_myInstance
    End Function
#End Region


#Region " User Methods "

    Public Function InsertPBPDataToSQL(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal langID As Integer) As DataSet
        Try
            Return m_objShootoutscore.InsertPBPDataToSQL(PBPXmlData, GameCode, FeedNumber, ReporterRole, langID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdatePenaltyShootoutScore(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal langID As Integer) As DataSet
        Try
            Return m_objShootoutscore.UpdatePenaltyShootoutScore(GameCode, FeedNumber, ReporterRole, langID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'GetPBPData
    Public Function GetPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objShootoutscore.GetPBPData(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateTeamGamePenaltyShootoutScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String) As Integer
        Try
            m_objShootoutscore.UpdateTeamGamePenaltyShootoutScores(GameCode, FeedNumber, ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLastEventDetails(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objShootoutscore.GetLastEventDetails(GameCode, FeedNumber)
    End Function

#End Region

End Class
