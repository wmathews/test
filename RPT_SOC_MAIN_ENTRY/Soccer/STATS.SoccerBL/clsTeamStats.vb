﻿
#Region "Imports"
Imports System
Imports STATS.SoccerDAL

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsTeamStats
' Author        : Shravani
' Created Date  : 07-AUG-09
' Description   : Business login for Team Statistics
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class clsTeamStats

#Region " Constatnts and Variable "
    Shared m_myInstance As clsTeamStats
    Private m_objTeamStats As SoccerDAL.clsTeamStats = SoccerDAL.clsTeamStats.GetAccess()


#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsTeamStats
        If m_myInstance Is Nothing Then
            m_myInstance = New clsTeamStats
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Defined Functions"
    ''' <summary>
    ''' GETS GAME STATISTICS
    ''' </summary>
    ''' <param name="GameCode">An integer represents GameCode</param>
    ''' <param name="FeedNumber">An integer represents Feeednumber</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTeamStats(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objTeamStats.GetTeamStats(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="feedNumber"></param>
    ''' <remarks></remarks>
    Public Sub InsertTeamStastoSummary(ByVal GameCode As Integer, ByVal feedNumber As Integer, ByVal ModuleID As Integer, ByVal CoverageLevel As Integer)
        Try
            m_objTeamStats.InsertTeamStatstoSummary(GameCode, feedNumber, ModuleID, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetTeamStastoselect(ByVal GameCode As Integer, ByVal feedNumber As Integer) As DataSet
        Try
            Return m_objTeamStats.GetTeamStastoselect(GameCode, feedNumber)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub InsertTeamStas(ByVal intGameCode As Integer, ByVal intFeedNumber As Integer, ByVal TEAM_ID As Integer, ByVal PERIOD As Integer, ByVal SHOTS As Integer, ByVal SHOTS_ON_GOAL As Integer, ByVal WOODWORK As Integer, ByVal OFFSIDES As Integer, ByVal CORNER_KICKS As Integer, ByVal FOULS_COMMITTED As Integer)
        Try
            m_objTeamStats.InsertTeamStas(intGameCode, intFeedNumber, TEAM_ID, PERIOD, SHOTS, SHOTS_ON_GOAL, WOODWORK, OFFSIDES, CORNER_KICKS, FOULS_COMMITTED)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub InsertTeampossion(ByVal intGameCode As Integer, ByVal intFeedNumber As Integer, ByVal TEAM_ID As Integer, ByVal PERIOD As Integer, ByVal Possession As Integer)
        Try
            m_objTeamStats.InsertTeampossion(intGameCode, intFeedNumber, TEAM_ID, PERIOD, Possession)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function Getperiodcount(ByVal intGameCode As Integer) As Int32
        Try
            Return m_objTeamStats.Getperiodcount(intGameCode)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


#End Region
End Class
