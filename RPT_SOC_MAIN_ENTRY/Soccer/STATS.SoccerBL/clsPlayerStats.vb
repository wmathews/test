﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsPlayerStats
' Author        : Shravani
' Created Date  : 10-NOV-09
' Description   : Business login for Player Statistics
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsPlayerStats

#Region " Constatnts and Variable "
    Shared m_myInstance As clsPlayerStats
    Private m_objPlayerStats As SoccerDAL.clsPlayerStats = SoccerDAL.clsPlayerStats.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsPlayerStats
        If m_myInstance Is Nothing Then
            m_myInstance = New clsPlayerStats
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Defined Functions"
    ''' <summary>
    ''' GETS GAME STATISTICS
    ''' </summary>
    ''' <param name="GameCode">An integer represents GameCode</param>
    ''' <param name="FeedNumber">An integer represents Feeednumber</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPlayerStats(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Integer) As DataSet
        Try
            Return m_objPlayerStats.GetPlayerStats(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="feedNumber"></param>
    ''' <remarks></remarks>
    Public Sub InsertPlayerStatsToSummary(ByVal GameCode As Integer, ByVal feedNumber As Integer, ByVal Time As Integer, ByVal CoverageLevel As Integer)
        Try
            m_objPlayerStats.InsertPlayerStatstoSummary(GameCode, feedNumber, Time, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To get the TimeElapsed of Last event of the game - to display time duration in playerstats report for Module 2
    ''' </summary>
    ''' <param name="GameCode"></param>
    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal period As Integer) As Integer
        Try
            Return m_objPlayerStats.GetMaxTimeElapsed(GameCode, period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
End Class
