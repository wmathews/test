﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsModule2Score
' Author        : Shravani
' Created Date  : 19-Mar-2010
' Description   : Class used for Business Logic of Module12Score
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsModule2Score


#Region "Constants & Variables"
    Shared m_myInstance As clsModule2Score
    Private m_objModule2Score As SoccerDAL.clsModule2Score = STATS.SoccerDAL.clsModule2Score.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsModule2Score
        If m_myInstance Is Nothing Then
            m_myInstance = New clsModule2Score
        End If
        Return m_myInstance
    End Function
#End Region

#Region " User Methods "
    Public Function GetTeams(ByVal GameCode As Integer) As DataSet
        Try
            Return (m_objModule2Score.GetTeams(GameCode))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objModule2Score.GetScores(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetUniqueID(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objModule2Score.GetUniqueID(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function InsertPBPDataToSQL(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal IsTimeEdited As Boolean, ByVal IsInsertBefore As Boolean, ByVal m_decInsertSeqNum As Decimal, ByVal IsFoulAssociated As Boolean, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule2Score.InsertPBPDataToSQL(PBPXmlData, GameCode, FeedNumber, ReporterRole, IsTimeEdited, IsInsertBefore, m_decInsertSeqNum, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMultipleGameDetails(ByVal LanguageID As Integer, ByVal UserType As Integer) As DataSet
        Dim m_dsGameDetails As New DataSet
        m_dsGameDetails = m_objModule2Score.GetMultipleGameDetails(LanguageID, UserType)
        Return m_dsGameDetails
    End Function
    ''' <summary>
    ''' To insert current game data
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <param name="intReporterID"></param>
    ''' <param name="strReporterRole"></param>
    ''' <remarks></remarks>
    Public Sub InsertCurrentGame(ByVal intGameCode As Int32, ByVal intOldGameCode As Integer, ByVal intFeedNumber As Int32, ByVal intReporterID As Int32, ByVal strReporterRole As String, ByVal intModuleID As Integer, ByVal TimeElapsed As Integer, ByVal SysLocalTime As String, ByVal Type As String)
        Try
            m_objModule2Score.InsertCurrentGame(intGameCode, intOldGameCode, intFeedNumber, intReporterID, strReporterRole, intModuleID, TimeElapsed, SysLocalTime, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'GetPBPData
    Public Function GetPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Integer) As DataSet
        Try
            Return m_objModule2Score.GetPBPData(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objModule2Score.GetMaxTimeElapsed(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>SHIRLEY
    ''' FETCHING PBP RECORDS BASED ON SEQUENCE NUMBER FROM DATABASE WHICH IS USED FOR EDITING PURPOSE
    ''' </summary> 
    ''' <param name="GameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <param name="Sequence_Number"></param> ' SELECTED FROM THE LISTBOX OF MAIN SCREEN
    ''' <param name="Type"></param> ' WHICH QUERY TO GET EXECUTED (EX : EQUAL,LESS THAN ETC)
    ''' <returns></returns>
    ''' <remarks></remarks> 'A DATASET WHICH CONTAINS THE SELECTED EVENT PBP RECORD
    Public Function GetPBPRecordBasedOnSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objModule2Score.GetPBPRecordBasedOnSeqNumber(GameCode, FeedNumber, Sequence_Number, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Deletes the selected event 
    ''' </summary>
    ''' <param name="SequenceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPrevSeq(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objModule2Score.GetPrevSeq(GameCode, FeedNumber, SequenceNumber, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Deletes the selected event 
    ''' </summary>
    ''' <param name="SequenceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeletePBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule2Score.DeletePBPData(GameCode, FeedNumber, ReporterRole, SequenceNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPBPMainEventData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal str As String) As DataSet
        Try
            Return m_objModule2Score.GetPBPMainEventData(GameCode, FeedNumber, SequenceNumber, str)
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Function UpdatePairedPBPEvents(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal Type As String, ByVal langauge_id As Integer) As DataSet
        Try
            Return m_objModule2Score.UpdatePairedPBPEvents(PBPXmlData, GameCode, FeedNumber, ReporterRole, SequenceNumber, Type, langauge_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function TimeEditOnPBPEvents(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objModule2Score.TimeEditOnPBPEvents(PBPXmlData, GameCode, FeedNumber, ReporterRole, SequenceNumber, LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPBPRecordBasedOnTime(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ElapsedTime As Integer, ByVal Period As Integer, ByVal Type As String) As DataSet
        Try
            Return m_objModule2Score.GetPBPRecordBasedOnTime(GameCode, FeedNumber, ElapsedTime, Period, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetLastEventDetails(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objModule2Score.GetLastEventDetails(GameCode, FeedNumber)
    End Function

#End Region
End Class
