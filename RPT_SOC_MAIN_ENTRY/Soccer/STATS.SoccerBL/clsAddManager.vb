﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddManager
' Author        : Shravani
' Created Date  : 06th July,2009
' Description   : Business Logic for AddManager Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddManager

#Region " Constants & Variables "
    Shared Instance As clsAddManager
    Private m_objAddManager As STATS.SoccerDAL.clsAddManager = STATS.SoccerDAL.clsAddManager.GetAccess()
#End Region


    Public Function SelectAddManager(ByVal GameCode As Integer, ByVal TeamID As Integer, ByVal DemoData As Char) As DataSet
        Try
            Return m_objAddManager.SelectAddManager(GameCode, TeamID, DemoData)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertManager(ByVal NupID As Integer, ByVal TeamID As Integer, ByVal Moniker As String, ByVal LastName As String, ByVal ReporterRole As String, ByVal LeagueID As Integer, ByVal DemoData As Char, ByVal Type As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objAddManager.InsertManager(NupID, TeamID, Moniker, LastName, ReporterRole, LeagueID, DemoData, Type, GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class
