﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region



Public Class clsManagerExpulsion


#Region "Constants & Variables"
    Shared m_myInstance As clsManagerExpulsion
    Private m_objManagerExpulsion As SoccerDAL.clsManagerExpulsion = SoccerDAL.clsManagerExpulsion.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsManagerExpulsion
        If m_myInstance Is Nothing Then
            m_myInstance = New clsManagerExpulsion
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Functions"

    'Public Function GetScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
    '    Return m_objValidateScore.GetScores(GameCode, FeedNumber)
    'End Function

    Public Function GetFormationCoach(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ManagerID As Integer, ByVal Type As String, ByVal langauge_id As Integer) As DataSet
        Try
            Return m_objManagerExpulsion.GetFormationCoach(GameCode, FeedNumber, ManagerID, Type, langauge_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetReasonData(ByVal languageid As Integer) As DataSet
        Try
            Return m_objManagerExpulsion.GetReasonData(languageid)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class




