﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddTeam
' Author        : Shravani
' Created Date  : 20th Aug,2009
' Description   : Business Logic for Add Team
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddTeam

#Region "Constants & Variables"
    Shared m_myInstance As clsAddTeam
    Private m_objAddTeam As SoccerDAL.clsAddTeam = SoccerDAL.clsAddTeam.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsAddTeam
        If m_myInstance Is Nothing Then
            m_myInstance = New clsAddTeam
        End If
        Return m_myInstance
    End Function

    Public Function GetLeague() As DataSet
        Try
            Return m_objAddTeam.GetLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="LeagueID"></param>
    ''' <param name="TeamName"></param>
    ''' <param name="TeamNickName"></param>
    ''' <param name="TeamAbbrev"></param>
    ''' <remarks></remarks>
    Public Sub InsertAddTeam(ByVal LeagueID As Integer, ByVal TeamName As String, ByVal TeamNickName As String, ByVal TeamAbbrev As String)
        Try
            m_objAddTeam.InsertAddTeam(LeagueID, TeamName, TeamNickName, TeamAbbrev)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <param name="LeagueID"></param>
    ''' <param name="TeamName"></param>
    ''' <param name="TeamNickName"></param>
    ''' <param name="TeamAbbrev"></param>
    ''' <remarks></remarks>
    Public Sub UpdateAddTeam(ByVal TeamID As Integer, ByVal LeagueID As Integer, ByVal TeamName As String, ByVal TeamNickName As String, ByVal TeamAbbrev As String)
        Try
            m_objAddTeam.UpdateAddTeam(TeamID, LeagueID, TeamName, TeamNickName, TeamAbbrev)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Public Function DeleteAddTeam(ByVal TeamID As Integer) As DataSet
        Try
            Return m_objAddTeam.DeleteAddTeam(TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAddTeam() As DataSet
        Try
            Return m_objAddTeam.SelectAddTeam()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class
