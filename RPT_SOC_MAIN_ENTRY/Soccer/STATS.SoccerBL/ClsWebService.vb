﻿Imports System.Configuration
Public Class ClsWebService
#Region "Constants & Variables"
    Shared _myInstance As ClsWebService
    Public _objWebSoc As New SoccerWS.Service
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsWebService
        If _myInstance Is Nothing Then
            _myInstance = New ClsWebService
        End If
        Return _myInstance
    End Function
    Sub New()
        _objWebSoc.Url = (ConfigurationSettings.AppSettings(ConfigurationSettings.AppSettings("Environment")))
    End Sub

#End Region



End Class
