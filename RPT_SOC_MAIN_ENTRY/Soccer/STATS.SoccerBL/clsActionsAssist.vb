﻿Public Class ClsActionsAssist

#Region "Constants & Variables"
    Shared _myInstance As ClsActionsAssist
    Private _objActionsAssist As SoccerDAL.clsActionsAssist = SoccerDAL.clsActionsAssist.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsActionsAssist
        If _myInstance Is Nothing Then
            _myInstance = New ClsActionsAssist
        End If
        Return _myInstance
    End Function
#End Region

#Region "User Functions"

    Public Function UpdatePbpRefresh(ByVal uniqueIds As String) As Integer
        Try
            Return _objActionsAssist.UpdatePbpRefresh(uniqueIds)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function EditPlaybyPlaydata(ByVal pbpXmlData As String, ByVal SequenceNumber As Decimal, ByVal IsTimeEdited As Char, ByVal InsertMode As Char) As DataSet
        Try
            Return _objActionsAssist.EditPlaybyPlaydata(pbpXmlData, SequenceNumber, IsTimeEdited, InsertMode)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function InsertDeleteKeyMomentData(ByVal SequenceNumber As Decimal, ByVal ReporterRole As String, ByVal KeyType As String) As Integer
        Try
            Return _objActionsAssist.InsertDeleteKeyMomentData(SequenceNumber, ReporterRole, KeyType)
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region

End Class