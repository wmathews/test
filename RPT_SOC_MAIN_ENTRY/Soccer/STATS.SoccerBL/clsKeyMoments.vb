﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsKeyMoments
' Author        : Ravi Krishna
' Created Date  : 18-06-09
' Description   : Class used for Business Logic of Key Moments Screen
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsKeyMoments

#Region "Constants & Variables"
    Shared m_myInstance As clsKeyMoments
    Private m_objKeyMoments As SoccerDAL.clsKeyMoments = SoccerDAL.clsKeyMoments.GetAccess()
#End Region

#Region " Shared methods "

    Public Shared Function GetInstance() As clsKeyMoments
        If m_myInstance Is Nothing Then
            m_myInstance = New clsKeyMoments
        End If
        Return m_myInstance
    End Function

#End Region

#Region "User Functions"

    ''' <summary>
    ''' To Insert the KeyMoments Id for the respective events in the local SQL Server
    ''' </summary>
    ''' <param name="FromUniqueID">An Integer representing the Unique Id from where the KeyMoment starts</param>
    ''' <param name="ToUniqueID">An Integer representing the Unique Id to where the KeyMoment Ends</param>
    ''' <param name="KeyMomentID">An Integer representing the KeyMoment ID</param>
    ''' <param name="GameCode">An Integer representing the game code</param>
    ''' <param name="FeedNumber">An integer representing the feed number</param>
    ''' <returns>A dataset containing the list of PBP Events associated with the unique Keymoment ID</returns>
    ''' <remarks>Updates the Unique KeyMoment ID in the Key_Moment_Id column of the local SQL PBP table for the associated set of PBP Events</remarks>
    Public Function InsertKeyMomentsToPBP(ByVal FromUniqueID As Decimal, ByVal ToUniqueID As Decimal, ByVal KeyMomentID As Integer, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Reporter_Role As String) As DataSet
        Try
            Return m_objKeyMoments.InsertKeyMomentsToPBP(FromUniqueID, ToUniqueID, KeyMomentID, GameCode, FeedNumber, Reporter_Role)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To Retrieve an unique Keymoment ID from the PBP table
    ''' </summary>
    ''' <param name="GameCode">An Integer representing the game code</param>
    ''' <param name="FeedNumber">An integer representing the feed number</param>
    ''' <returns>A Dataset containing the unique keymoment id</returns>
    ''' <remarks>Calculates the highest Key Moment ID from the local SQL PBP Table and updates the value by 1 and returns</remarks>
    Public Function GetKeyMomentID(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objKeyMoments.GetKeyMomentID(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To Delete the KeyMoment associated with a set of PBP Events in the SQL PBP Table
    ''' </summary>
    ''' <param name="FromUniqueID">An Integer representing the Unique Id from where the KeyMoment starts</param>
    ''' <param name="ToUniqueID">An Integer representing the Unique Id to where the KeyMoment Ends</param>
    ''' <param name="KeyMomentID">An Integer representing the KeyMoment ID</param>
    ''' <param name="GameCode">An Integer representing the game code</param>
    ''' <param name="FeedNumber">An integer representing the feed number</param>
    ''' <returns>An Integer value representing the number of rows updated</returns>
    ''' <remarks>Updates the Key_Moment_ID column in the local SQL PBP Table with NULL</remarks>
    Public Function DeleteKeyMoments(ByVal FromUniqueID As Decimal, ByVal ToUniqueID As Decimal, ByVal KeyMomentID As Integer, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Reporter_Role As String) As Integer
        Try
            Return m_objKeyMoments.DeleteKeyMoments(FromUniqueID, ToUniqueID, KeyMomentID, GameCode, FeedNumber, Reporter_Role)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To Update the KeyMoment in the local SQL Server
    ''' </summary>
    ''' <param name="FromUniqueID">An Integer representing the Unique Id from where the KeyMoment starts</param>
    ''' <param name="ToUniqueID">An Integer representing the Unique Id to where the KeyMoment Ends</param>
    ''' <param name="KeyMomentID">An Integer representing the KeyMoment ID</param>
    ''' <param name="GameCode">An Integer representing the game code</param>
    ''' <param name="FeedNumber">An integer representing the feed number</param>
    ''' <returns>A dataset containing the list of PBP Events associated with the unique existing Keymoment ID</returns>
    ''' <remarks>Updates the Unique KeyMoment ID in the Key_Moment_Id column of the local SQL PBP table for the associated set of PBP Events</remarks>
    Public Function UpdateKeyMomentsToPBP(ByVal FromUniqueID As Decimal, ByVal ToUniqueID As Decimal, ByVal KeyMomentID As Integer, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Reporter_Role As String) As DataSet
        Try
            Return m_objKeyMoments.UpdateKeyMomentsToPBP(FromUniqueID, ToUniqueID, KeyMomentID, GameCode, FeedNumber, Reporter_Role)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To Get keymoment data
    ''' </summary>
    ''' <param name="GameCode">An Integer representing the game code</param>
    ''' <param name="FeedNumber">An integer representing the feed number</param>
    ''' <returns>A dataset containing the list of Keymoments</returns>
    Public Function GetKeymomentsData(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objKeyMoments.GetKeymomentsData(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


#End Region

End Class
