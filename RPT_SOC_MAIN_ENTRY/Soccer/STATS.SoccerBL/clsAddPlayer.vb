﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddPlayer
' Author        : Shravani
' Created Date  : 16th June,2009
' Description   : Business Logic for AddPlayer Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddPlayer

#Region " Constants & Variables "
    Shared Instance As clsAddPlayer
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Private m_objAddPlayer As STATS.SoccerDAL.clsAddPlayer = STATS.SoccerDAL.clsAddPlayer.GetAccess()
#End Region

#Region " User Methods "
    Public Shared Function GetInstance() As clsAddPlayer
        If Instance Is Nothing Then
            Instance = New clsAddPlayer
        End If
        Return Instance
    End Function

    Public Function LoadPosition() As DataSet
        Try
            Return m_objAddPlayer.LoadPosition()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub InsertUpdatePlayer(ByVal PersonID As Long, ByVal LeagueID As Integer, ByVal TeamID As Integer, ByVal LastName As String, ByVal FirstName As String, ByVal UniformNumber As String, ByVal DisplayUniform As String, ByVal PositionID As Integer, ByVal DemoData As Char)
        Try
            m_objAddPlayer.InsertUpdatePlayer(PersonID, LeagueID, TeamID, LastName, FirstName, UniformNumber, DisplayUniform, PositionID, GetCurrentYear(LeagueID), DemoData)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function SelectAddPlayer(ByVal GameCode As Integer, ByVal LeagueID As Integer, ByVal SortOrder As String, ByVal DemoData As Char) As DataSet
        Try
            Return m_objAddPlayer.SelectAddPlayer(GameCode, LeagueID, SortOrder, DemoData)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteAddPlayer(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal PlayerID As Long, ByVal LeagueID As Integer, ByVal TeamID As Integer, ByVal DemoData As Char, ByVal CoverageLevel As Integer) As Integer
        Try
            Return m_objAddPlayer.DeleteAddPlayer(GameCode, FeedNumber, PlayerID, LeagueID, TeamID, DemoData, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetCurrentYear(ByVal LeagueId As Integer) As Integer
        Dim GetSeasonyear As DataSet
        GetSeasonyear = m_objAddPlayer.GetCurrentYear(LeagueId)
        If (GetSeasonyear.Tables(0).Rows.Count > 0) Then
            Return CInt(GetSeasonyear.Tables(0).Rows(0).Item(0).ToString())
        End If
    End Function

    Public Function GetUnusedUniformNumber(ByVal TeamID As Integer) As DataSet
        Try
            Return m_objAddPlayer.GetUnusedUniformNumber(TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FetchPlayerIDFromOracle() As Long
        Try
            Return m_objWebSoc._objWebSoc.FetchLiveIdSequence(System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetPlayerCountInTouches(ByVal Gamecode As Integer, ByVal PlayerId As Long) As Integer
        Try
            Return m_objWebSoc._objWebSoc.GetTouchesPlayerCntByPlayerId(Gamecode, PlayerId, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function


#End Region

End Class
