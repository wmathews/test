﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddDemoPlayer
' Author        : Shravani
' Created Date  : 24th Aug,2009
' Description   : Business Logic for clsAddDemoPlayer
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddDemoPlayer

#Region " Constants & Variables "
    Shared m_myInstance As clsAddDemoPlayer
    Private m_objAddPlayer As SoccerDAL.clsAddDemoPlayer = SoccerDAL.clsAddDemoPlayer.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsAddDemoPlayer
        If m_myInstance Is Nothing Then
            m_myInstance = New clsAddDemoPlayer
        End If
        Return m_myInstance
    End Function
#End Region


    Public Function GetLeague() As DataSet
        Try
            Return m_objAddPlayer.GetLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub InsertAddPlayer(ByVal PersonID As Integer, ByVal Moniker As String, ByVal LastName As String, ByVal LeagueID As Integer, ByVal TeamID As Integer, ByVal Uniform As String, ByVal DisplayUniform As String, ByVal PositionID As Integer)
        Try
            m_objAddPlayer.InsertAddPlayer(PersonID, Moniker, LastName, LeagueID, TeamID, Uniform, DisplayUniform, PositionID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UpdateAddPlayer(ByVal TeamID As Integer, ByVal LeagueID As Integer, ByVal Moniker As String, ByVal LastName As String, ByVal PersonId As Integer, ByVal DisplayUniform As String, ByVal PositionID As Integer)
        Try
            m_objAddPlayer.UpdateAddPlayer(TeamID, LeagueID, Moniker, LastName, PersonId, DisplayUniform, PositionID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Function DeleteAddPlayer(ByVal PlayerID As Integer, ByVal TeamID As Integer, ByVal LeagueID As Integer) As Integer
        Try
            Return m_objAddPlayer.DeleteAddPlayer(PlayerID, TeamID, LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAddPlayer() As DataSet
        Try
            Return m_objAddPlayer.SelectAddPlayer()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTeamAndPosition(ByVal LeagueId As Integer) As DataSet
        Try
            Return m_objAddPlayer.GetTeamAndPosition(LeagueId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
