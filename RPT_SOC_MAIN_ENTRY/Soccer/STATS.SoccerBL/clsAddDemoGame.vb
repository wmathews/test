﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddDemoGame
' Author        : Shravani
' Created Date  : 20th Aug,2009
' Description   : Business Logic for Add Demo Game
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class clsAddDemoGame

#Region " Constants & Variables "
    Private m_objAddDemoGame As New STATS.SoccerDAL.clsAddDemoGame()
#End Region

    Public Function GetSamepleGameDisplay() As DataSet
        Try
            Return m_objAddDemoGame.GetSampleGameDisplay()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLeague() As DataSet
        Try
            Return m_objAddDemoGame.GetLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAllComboData(ByVal LeagueID As Integer) As DataSet
        Try
            Return m_objAddDemoGame.GetAllComboData(LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCntPlayer(ByVal strQuery As String) As DataSet
        Try
            Return m_objAddDemoGame.GetCntPlayer(strQuery)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertAddGame(ByVal GameDate As DateTime, ByVal LeagueID As Integer, ByVal AwayTeamID As Integer, ByVal HomeTeamID As Integer, ByVal VenueID As Integer, ByVal CoverageLevel As Integer, ByVal ModuleID As Integer) As Integer
        Try
            Return m_objAddDemoGame.InsertAddGame(GameDate, LeagueID, AwayTeamID, HomeTeamID, VenueID, CoverageLevel, ModuleID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateAddGame(ByVal GameCode As Integer, ByVal GameDate As DateTime, ByVal LeagueID As Integer, ByVal AwayTeamID As Integer, ByVal HomeTeamID As Integer, ByVal VenueID As Integer, ByVal CoverageLevel As Integer, ByVal ModuleID As Integer) As Integer
        Try
            Return m_objAddDemoGame.UpdateAddGame(GameCode, GameDate, LeagueID, AwayTeamID, HomeTeamID, VenueID, CoverageLevel, ModuleID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteAddGame(ByVal GameCode As Integer) As Integer
        Try
            Return m_objAddDemoGame.DeleteAddGame(GameCode)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
