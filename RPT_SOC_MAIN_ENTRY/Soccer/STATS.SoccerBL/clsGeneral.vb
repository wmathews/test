﻿Imports System.Windows.Forms

#Region "Imports"
Imports System
Imports STATS.SoccerDAL
Imports System.IO
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsGeneral
' Author        : Dijo Davis
' Created Date  : 08-05-09
' Description   : This is the business logic for general functions
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsGeneral

#Region "Constants & Variables"
    Shared m_myInstance As clsGeneral
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Private m_objclsGeneral As STATS.SoccerDAL.clsGeneral = STATS.SoccerDAL.clsGeneral.GetAccess()
    Private m_dsUserDetails As New DataSet
    Private m_blnNetConection As Boolean = False

#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsGeneral
        If m_myInstance Is Nothing Then
            m_myInstance = New clsGeneral
        End If
        Return m_myInstance
    End Function
#End Region

#Region "General"
    ''' <summary>
    ''' Function to return team logos
    ''' </summary>
    ''' <param name="intLeagueID"></param>
    ''' <param name="intTeamID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTeamLogo(ByVal intLeagueID As Int32, ByVal intTeamID As Int32) As MemoryStream
        Try
            Dim dsLogo As New DataSet
            Dim v_memLogo As MemoryStream = Nothing
            dsLogo = m_objclsGeneral.GetTeamLogo(intLeagueID, intTeamID)
            If dsLogo IsNot Nothing Then
                If dsLogo.Tables.Count > 0 Then
                    If dsLogo.Tables(0).Rows.Count > 0 Then
                        v_memLogo = New MemoryStream(DirectCast(dsLogo.Tables(0).Rows(0).Item(0), Byte()))

                    End If
                End If
            End If
            Return v_memLogo
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' To get unprocessed records in all modules
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUnprocessedRecords(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal ModuleID As Integer) As Object
        Try
            Return m_objclsGeneral.GetUnprocessedRecords(intGameCode, intFeedNumber, ModuleID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMaxTimeElapsedTouches(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objclsGeneral.GetMaxTimeElapsedTouches(GameCode, FeedNumber)
    End Function

    ''' <summary>
    ''' Function to fetch Home and Away team rosters
    ''' </summary>
    ''' <param name="intTeamID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTeamRostersforCommentaryandTouches(ByVal intTeamID As Int32, ByVal language_id As Integer) As DataSet
        Try
            Dim dsTeamRosters As DataSet = m_objclsGeneral.GetTeamRostersforCommentaryandTouches(intTeamID, language_id)
            dsTeamRosters.Tables(0).TableName = "tblTeamRosters"
            Return dsTeamRosters
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Function to fetch Home and Away team rosters by LineUps
    ''' </summary>
    ''' <param name="intTeamID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTeamRostersforCommentaryandTouchesByLineUps(ByVal intTeamID As Int32, ByVal language_id As Integer) As DataSet
        Try
            Dim dsTeamRosters As DataSet = m_objclsGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(intTeamID, language_id)
            dsTeamRosters.Tables(0).TableName = "tblTeamRosters"
            Return dsTeamRosters
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FETCHES SCORES FROM PBP TABLE TO DISPLAY THE LAST RECORD SCORE
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objclsGeneral.GetScores(GameCode, FeedNumber)
    End Function

    ''' <summary>
    ''' SPECIFIC FOR MODULE 2 - FETCHES THE TIME ELAPSED VALUE SO AS TO DISPLAY THE DIFF IN CLOCK TIME
    ''' WHEN THE REPORTER SWITCHES BETWEEN THE GAMES
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Function GetTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objclsGeneral.GetTimeElapsed(GameCode, FeedNumber)
    End Function

    Public Sub UpdateTeamColor(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal IsHomeTeam As Boolean, ByVal TeamColor As Integer)
        Try
            m_objclsGeneral.UpdateTeamColor(GameCode, FeedNumber, IsHomeTeam, TeamColor)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' CALLS THE WEB METHOD WHICH DELETES THE EXISTANCE OF THE REPORTER FROM LIVE_REPORTER_LOGINS TABLE
    ''' </summary>
    ''' <param name="ReporterID"></param>
    ''' <remarks></remarks>
    Public Sub DeleteReporterExistance(ByVal ReporterID As Integer)
        Try
            m_objWebSoc._objWebSoc.DeleteReporterExistance(ReporterID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetUniqueID(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objclsGeneral.GetUniqueID(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function getEndGameCount(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objclsGeneral.getEndGameCount(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' REFRESHING THE GAME DATA INFOR - MULTI REPORTER COMM
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <param name="IsEditMode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RefreshGameData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ModuleID As Integer, ByVal IsEditMode As Boolean, ByVal language_id As Int32) As DataSet
        Try
            Return m_objclsGeneral.RefreshGameData(GameCode, FeedNumber, ModuleID, IsEditMode, language_id)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetLastClockEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetLastClockEvent(GameCode, FeedNumber, Period)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function UpdateLastEntryStatus(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal IsEntryHappened As Boolean) As Boolean
        Try
            If m_objclsGeneral.UpdateLastEntryStatus(GameCode, FeedNumber, IsEntryHappened) > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetPeriodEndCount(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetPeriodEndCount(GameCode, FeedNumber, Period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPeriodEndCount_Touches(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetPeriodEndCount_Touches(GameCode, FeedNumber, Period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function PzGetPeriodEndCount(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer) As DataSet
        Try
            Return m_objclsGeneral.PzEndPeriodCount(GameCode, FeedNumber, Period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' GETS THE EVENTS STORED IN PBP TABLE SO AS TO DISPLAY IN THE MAIN SCREEN.
    ''' EVENTS SUCH AS SUB,BOOKINGS,FOULS ARE FETCHED FROM DATABASE
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FetchPBPEventsToDisplay(ByVal intGameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Dim DsMainEvents As DataSet
            DsMainEvents = m_objclsGeneral.FetchMainScreenEvents(intGameCode, FeedNumber)
            Return DsMainEvents
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FetchSubsforCommAndTouches(ByVal intGameCode As Integer) As DataSet
        Try
            Dim DsMainEvents As DataSet
            DsMainEvents = m_objclsGeneral.FetchSubsforCommAndTouches(intGameCode)
            Return DsMainEvents
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function RefreshPlayers(ByVal UniqueId As Integer, ByVal intGameCode As Integer) As DataSet
        Try
            Dim DsRefPlayers As DataSet
            DsRefPlayers = m_objclsGeneral.RefreshPlayers(UniqueId, intGameCode)
            Return DsRefPlayers
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' FETCHES THE GAMES ASSIGNED TO THE REPORTER SPECIFIED WITHIN +/- 24 HRS
    ''' </summary>
    ''' <param name="LanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetGameDetails(ByVal LanguageID As Integer, ByVal ReporterID As Integer) As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objclsGeneral.GetGameDetails(LanguageID, ReporterID)
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub InsertCurrentGame(ByVal intGameCode As Int32, ByVal intOldGameCode As Integer, ByVal intFeedNumber As Int32, ByVal intReporterID As Int32, ByVal strReporterRole As String, ByVal intModuleID As Integer, ByVal TimeElapsed As Integer, ByVal SysLocalTime As String, ByVal Type As String)
        Try
            m_objclsGeneral.InsertCurrentGame(intGameCode, intOldGameCode, intFeedNumber, intReporterID, strReporterRole, intModuleID, TimeElapsed, SysLocalTime, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetTouchesHomeStartSide(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal LeagueID As Integer) As DataSet
        Try
            Return m_objclsGeneral.getTouchesHomeStartSide(GameCode, FeedNumber, LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetSportVUFeed(ByVal GameCode As Integer) As Integer
        Try
            Return m_objclsGeneral.SportVUFeedAvailability(GameCode)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

    Public Function GetPBPreport(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objclsGeneral.GetPBPreport(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetTouchesreport(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetTouchesreport(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPZActionreport(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal GameClock As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetPZActionreport(GameCode, FeedNumber, GameClock)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function getDataForEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer, ByVal EvtID As Integer, ByVal ModuleId As Integer) As DataSet
        Try
            Return m_objclsGeneral.getDataForEvent(GameCode, FeedNumber, Period, EvtID, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function CheckNetConnection() As Boolean
        Try
            m_blnNetConection = m_objWebSoc._objWebSoc.NetConnection(System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))

            If m_blnNetConection = True Then
                Return True
            ElseIf m_blnNetConection = False Then
                Return False
            End If
            Return True
        Catch ex As Exception
            m_blnNetConection = False
            Return False
        End Try
    End Function

    Public Function FetchFouls(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer, ByVal TeamID As Integer, ByVal timeElapsed As Integer) As DataSet
        Try
            Return m_objclsGeneral.FetchFouls(GameCode, FeedNumber, Period, TeamID, timeElapsed)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetRosterInfo(ByVal GameCode As Integer, ByVal LeagueID As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objclsGeneral.GetRosterInfo(GameCode, LeagueID, "NAME", language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPenaltyShootoutScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetPenaltyShootoutScores(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPenaltyShootoutScoresPz(ByVal gameCode As Integer, ByVal feedNumber As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetPenaltyShootoutScoresPz(gameCode, feedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMultipleGameDetails(ByVal LanguageID As Integer, ByVal ReporterID As Integer) As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objclsGeneral.GetMultipleGameDetails(LanguageID, ReporterID)
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetRulesData(ByVal LeagueID As Integer, ByVal CoverageLevel As Integer) As DataSet
        Try
            Dim dsRules As New DataSet
            dsRules = m_objclsGeneral.GetRulesData(LeagueID, CoverageLevel)

            'TOSOCRS-212 -------------------------begin
            'If Not dsRules Is Nothing Then
            '    If dsRules.Tables(0).Rows.Count <= 0 Then
            '        If CoverageLevel = 5 Then
            '            dsRules = m_objclsGeneral.GetRulesData(39, 5)
            '            For i As Integer = 0 To dsRules.Tables(0).Rows.Count - 1
            '                dsRules.Tables(0).Rows(i).BeginEdit()
            '                dsRules.Tables(0).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(0).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(1).Rows.Count - 1
            '                dsRules.Tables(1).Rows(i).BeginEdit()
            '                dsRules.Tables(1).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(1).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(2).Rows.Count - 1
            '                dsRules.Tables(2).Rows(i).BeginEdit()
            '                dsRules.Tables(2).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(2).Rows(i).EndEdit()
            '            Next

            '        ElseIf CoverageLevel = 4 Then
            '            dsRules = m_objclsGeneral.GetRulesData(39, 4)
            '            For i As Integer = 0 To dsRules.Tables(0).Rows.Count - 1
            '                dsRules.Tables(0).Rows(i).BeginEdit()
            '                dsRules.Tables(0).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(0).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(1).Rows.Count - 1
            '                dsRules.Tables(1).Rows(i).BeginEdit()
            '                dsRules.Tables(1).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(1).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(2).Rows.Count - 1
            '                dsRules.Tables(2).Rows(i).BeginEdit()
            '                dsRules.Tables(2).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(2).Rows(i).EndEdit()
            '            Next

            '        ElseIf CoverageLevel = 3 Then
            '            dsRules = m_objclsGeneral.GetRulesData(50, 3)
            '            For i As Integer = 0 To dsRules.Tables(0).Rows.Count - 1
            '                dsRules.Tables(0).Rows(i).BeginEdit()
            '                dsRules.Tables(0).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(0).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(1).Rows.Count - 1
            '                dsRules.Tables(1).Rows(i).BeginEdit()
            '                dsRules.Tables(1).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(1).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(2).Rows.Count - 1
            '                dsRules.Tables(2).Rows(i).BeginEdit()
            '                dsRules.Tables(2).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(2).Rows(i).EndEdit()
            '            Next

            '        ElseIf CoverageLevel = 2 Then
            '            dsRules = m_objclsGeneral.GetRulesData(70, 2)

            '            For i As Integer = 0 To dsRules.Tables(0).Rows.Count - 1
            '                dsRules.Tables(0).Rows(i).BeginEdit()
            '                dsRules.Tables(0).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(0).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(1).Rows.Count - 1
            '                dsRules.Tables(1).Rows(i).BeginEdit()
            '                dsRules.Tables(1).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(1).Rows(i).EndEdit()
            '            Next

            '            For i As Integer = 0 To dsRules.Tables(2).Rows.Count - 1
            '                dsRules.Tables(2).Rows(i).BeginEdit()
            '                dsRules.Tables(2).Rows(i).Item("LeagueID") = LeagueID
            '                dsRules.Tables(2).Rows(i).EndEdit()
            '            Next

            '            dsRules.AcceptChanges()
            '        End If
            '    End If
            'End If
            'dsRules.AcceptChanges()
            'TOSOCRS-212 -------------------------end

            Return dsRules
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetOnfieldValue(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal intPeriod As Integer) As Integer
        Try
            Return m_objclsGeneral.GetOnfieldValue(GameCode, FeedNumber, intPeriod)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetNextOnfieldValue(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal IntTeamID As Integer, ByVal intPeriod As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetNextOnfieldValue(GameCode, FeedNumber, IntTeamID, intPeriod)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'GetPBPData
    Public Function GetPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objclsGeneral.GetPBPData(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertPBPDataToSQL(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal IsEdited As String, ByVal languageid As Integer) As DataSet
        Try
            Return m_objclsGeneral.InsertPBPDataToSQL(PBPXmlData, GameCode, FeedNumber, IsEdited, languageid)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdatePairedPBPEvents(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal Type As String, ByVal language_id As Int32) As DataSet
        Try
            Return m_objclsGeneral.UpdatePairedPBPEvents(PBPXmlData, GameCode, FeedNumber, ReporterRole, SequenceNumber, Type, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPBPRecordBasedOnSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objclsGeneral.GetPBPRecordBasedOnSeqNumber(GameCode, FeedNumber, Sequence_Number, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function LanguageTranslate(ByVal LanguageID As Integer) As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objclsGeneral.LanguageTranslate(LanguageID)
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function
     Public Function GetMultilingual(ByVal languageId As Integer) As DataSet
        Try
            Return  m_objclsGeneral.GetMultilingual(LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetTeamColors(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetTeamColors(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetGoalieChangeEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer, ByVal CoverageLevel As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetGoalieChangeEvent(GameCode, FeedNumber, TeamID, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetLastEventDetails(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetLastEventDetails(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertTeamGameForLowTiers(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String) As Integer
        Try
            m_objclsGeneral.InsertTeamGameForLowTiers(GameCode, FeedNumber, ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objclsGeneral.GetMaxTimeElapsed(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetKeymomentsData(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objclsGeneral.GetKeymomentsData(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function UpdatehomesideET(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer) As DataSet
        Try
            Return m_objclsGeneral.UpdatehomesideET(GameCode, FeedNumber, Period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function getAssignmentForCommentary(ByVal GameCode As Integer, ByVal RepID As Integer, ByVal ModuleID As Integer, ByVal SerialNO As Integer) As DataSet
        Try
            Return m_objclsGeneral.getAssignmentForCommentary(GameCode, RepID, ModuleID, SerialNO)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteCurrentGame(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterID As Integer) As DataSet
        Try
            Return m_objclsGeneral.DeleteCurrentGame(GameCode, FeedNumber, ReporterID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UpdateTouchesDone(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String)
        Try
            m_objWebSoc._objWebSoc.TouchesFinalized(GameCode, FeedNumber, ReporterRole, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetAllRosters(ByVal gameCode As Integer, ByVal languageId As Integer, Optional ByVal sequenceNumber As Decimal = 0) As DataSet
        Try
            Return m_objclsGeneral.GetAllRosters(gameCode, languageId, sequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "Demo"
    Public Function GetDemoGameDetails() As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objclsGeneral.GetDemoGameDetails()
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidateUserDetails(ByVal UserName As String, ByVal Password As String, ByVal language_id As Integer) As DataSet
        Dim intDaysFrom, intDaysTo As Integer
        Try
            intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            m_dsUserDetails = m_objWebSoc._objWebSoc.GetUserInfo(UserName, Password, intDaysFrom, intDaysTo, language_id, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            If (m_dsUserDetails.Tables.Count > 0) Then
                m_dsUserDetails.Tables(0).TableName = "UserDetails"
                m_dsUserDetails.Tables(1).TableName = "Status"
                m_dsUserDetails.Tables(2).TableName = "Games"
            End If
            Return m_dsUserDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "Prozone"
    Public Sub FillBenchPlayers(ByVal playerList As List(Of ClsPlayerData.PlayerData), pnlSub As Panel, buttonName As String)
        Try
            Dim buttonNumber As Integer = 1
            Dim btnplayer As New Button
            For Each player In playerList
                btnplayer = CType(pnlSub.Controls.Item(buttonName & buttonNumber), Button)
                btnplayer.Enabled = True
                btnplayer.Text = player.Player
                btnplayer.Tag = player.PlayerId
                buttonNumber = buttonNumber + 1
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function LoadPlaybyPlaydata(ByVal gameCode As Integer, ByVal feedNumber As Integer, ByVal languageId As Integer, ByVal gameClockEachPeriod As Integer, ByVal refreshedOnly As String, ByVal ReporterSLNo As Integer, Optional ByVal teamFilter As Integer = 0) As DataSet  'teamFilter = '0' Both teams
        Try
            Return m_objclsGeneral.LoadPlaybyPlaydata(gameCode, feedNumber, languageId, gameClockEachPeriod, refreshedOnly, ReporterSLNo, teamFilter)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function LoadPrimaryRepInsertedPBPData(ByVal gameCode As Integer, ByVal feedNumber As Integer, ByVal languageId As Integer, ByVal gameClockEachPeriod As Integer) As DataSet  'teamFilter = '0' Both teams
        Try
            Return m_objclsGeneral.LoadPrimaryRepInsertedPBPData(gameCode, feedNumber, languageId, gameClockEachPeriod)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function RefreshPZPbpData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Int32, ByVal DefaultGameClock As Integer, ByVal LoadFullPBP As Boolean, ByVal ReporterSLNo As Integer, Optional ByVal teamFilter As Integer = 0) As DataSet
        Try
            Return m_objclsGeneral.RefreshPZPbpData(GameCode, FeedNumber, language_id, DefaultGameClock, LoadFullPBP, ReporterSLNo, teamFilter)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function RefreshPZGameInfoData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objclsGeneral.RefreshPZGameInfoData(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetPlayerCntforYellowCard(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal EventID As Integer, ByVal PlayerID As Integer, ByVal SequenceNumber As Decimal) As Integer
        Try
            Return m_objclsGeneral.GetPlayerCntforYellowCard(GameCode, FeedNumber, EventID, PlayerID, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeletePlaybyPlayData(ByVal SequenceNumber As Decimal, ByVal ReporterRoleSerial As Integer)
        Try
            Return m_objclsGeneral.DeletePlaybyPlayData(SequenceNumber, ReporterRoleSerial)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetActionsMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objclsGeneral.GetActionsMaxTimeElapsed(GameCode, FeedNumber)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetInjuryTimePeriodCount(ByVal gameCode As Integer, ByVal feedNumber As Integer, ByVal period As Integer) As Integer
        Try
            Return m_objclsGeneral.GetInjuryTimePeriodCount(gameCode, feedNumber, period)
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region

End Class