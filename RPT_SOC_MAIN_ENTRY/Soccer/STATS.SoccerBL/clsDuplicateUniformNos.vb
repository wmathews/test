﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsDuplicateUniformNos
' Author        : Shravani
' Created Date  : 15th April,2010
' Description   : Business Logic for DuplicateUniformNos screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsDuplicateUniformNos

#Region " Constants & Variables "
    Shared Instance As clsDuplicateUniformNos
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Private m_objDuplicateUN As STATS.SoccerDAL.clsDuplicateUniformNumbers = STATS.SoccerDAL.clsDuplicateUniformNumbers.GetAccess()
#End Region


    Public Shared Function GetInstance() As clsDuplicateUniformNos
        If Instance Is Nothing Then
            Instance = New clsDuplicateUniformNos
        End If
        Return Instance
    End Function


    Public Function GetAllRosters(ByVal GameCode As Integer, ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objDuplicateUN.GetAllRosters(GameCode, LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UpdateRosters(ByVal GameCode As Integer, ByVal Players As String)
        Try
            m_objDuplicateUN.UpdateRosterInfo(GameCode, Players)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



End Class
