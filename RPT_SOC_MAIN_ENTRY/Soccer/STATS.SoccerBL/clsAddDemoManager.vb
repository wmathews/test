﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddDemoManager
' Author        : Shravani
' Created Date  : 31th Aug,2009
' Description   : Business Logic for AddManager(Demo) Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddDemoManager

#Region " Constants & Variables "
    Shared Instance As clsAddDemoManager
    Private m_objAddManager As STATS.SoccerDAL.clsAddDemoManager = STATS.SoccerDAL.clsAddDemoManager.GetAccess()
#End Region

    Public Shared Function GetInstance() As clsAddDemoManager
        If Instance Is Nothing Then
            Instance = New clsAddDemoManager
        End If
        Return Instance
    End Function

    Public Function GetLeague() As DataSet
        Try
            Return m_objAddManager.GetLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAddManager() As DataSet
        Try
            Return m_objAddManager.SelectAddManager()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetSeasonID(ByVal LeagueID As Integer) As Integer
        Try
            Return m_objAddManager.GetSeasonID(LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertUpdateManager(ByVal NupID As Integer, ByVal SeasonID As Integer, ByVal TeamID As Integer, ByVal Sequence As Integer, ByVal Moniker As String, ByVal LastName As String, ByVal ReporterRole As String) As Integer
        Try
            Return m_objAddManager.InsertUpdateManager(NupID, SeasonID, TeamID, Sequence, Moniker, LastName, ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetSequence() As Integer
        Try
            Return m_objAddManager.GetSequence()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeletetManager(ByVal CoachID As Integer, ByVal SeasonID As Integer) As Integer
        Try
            Return m_objAddManager.DeletetManager(CoachID, SeasonID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTeam(ByVal LeagueID As Integer) As DataSet
        Try
            Return m_objAddManager.GetTeam(LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
