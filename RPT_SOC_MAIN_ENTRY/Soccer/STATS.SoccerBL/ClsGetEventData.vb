﻿#Region " Options "
Option Explicit Off
Option Strict Off

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddTeam
' Author        : Wilson Mathews
' Created Date  : 12th Dec,2015
' Description   : Get the Rules Data for the given event
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

#Region "Imports"
#End Region

Public Class ClsGetEventData

#Region "Constants & Variables"
    Shared _myInstance As ClsGetEventData
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsGetEventData
        If _myInstance Is Nothing Then
            _myInstance = New ClsGetEventData
        End If
        Return _myInstance
    End Function
#End Region

    'Event List box load
    Public Function GetEventData(gtSoccerRulesEventLBs As DataTable, gtSoccerRulesEventLbData As DataTable, leagueId As Integer, coverageLevel As Integer, eventId As Integer) As List(Of EventData)
        Return (From eventLb In gtSoccerRulesEventLBs.AsEnumerable()
               Let lbData = (From eventLbData In gtSoccerRulesEventLbData.AsEnumerable()
                             Where eventLbData.Field(Of Byte)("AttributeID") = eventLb.Field(Of Byte)("AttributeID") _
                             AndAlso eventLbData.Field(Of Int16)("EventID") = eventLb.Field(Of Int16)("EventID") _
                             AndAlso eventLbData.Field(Of Int16)("LeagueID") = eventLb.Field(Of Int16)("LeagueID") _
                             AndAlso eventLbData.Field(Of Byte)("CoverageLevel") = eventLb.Field(Of Byte)("CoverageLevel")
                        Order By eventLbData.Field(Of Int16?)("SortOrder")
                              Select New EventDataValues(eventLbData.Field(Of Int16)("AttributeDataID"), eventLbData.Field(Of String)("AttributeData")))
      Where eventLb.Field(Of Int16)("LeagueID") = leagueId _
        AndAlso eventLb.Field(Of Byte)("CoverageLevel") = coverageLevel _
        AndAlso eventLb.Field(Of Int16)("EventID") = eventId
        Select (New EventData(eventLb.Field(Of Byte)("AttributeID"),
                             eventLb.Field(Of String)("AttributeDesc"),
                 New List(Of EventDataValues)(lbData.ToList()), eventLb.Field(Of String)("PBPColumnName"), eventLb.Field(Of Int16?)("Listbox_Default_Selection")
                             ))).ToList()
    End Function
    'Event List box data selection to Hide and show player List
    Public Function GetEventDependencyData(selectedListbox As Integer, selectedListboxData As Integer, gtSoccerRulesEventLBs As DataTable, gtSoccerRulesEventLbDependency As DataTable, leagueId As Integer, coverageLevel As Integer, eventId As Integer) As List(Of EventData)
        'AttributeEntityID   selectedListbox , AttributeEntityDetailID (Box should be disbaled),AttributeEntityDataID (selectedListbox index ),AttributeData(FALSE,FIELD OFF,Power), AttributeDataID(-100, +100)
        Return (From eventLbDependency In gtSoccerRulesEventLbDependency.AsEnumerable()
                 Join eventLb In gtSoccerRulesEventLBs.AsEnumerable() _
                  On eventLb.Field(Of Int16)("EventID") Equals (eventLbDependency.Field(Of Int16)("EventID")) _
                 And eventLb.Field(Of Byte)("AttributeID") Equals (eventLbDependency.Field(Of Byte)("AttributeEntityDetailID")) _
                 And eventLb.Field(Of Byte)("CoverageLevel") Equals (eventLbDependency.Field(Of Byte)("CoverageLevel")) _
                 And eventLb.Field(Of Int16)("LeagueID") Equals (eventLbDependency.Field(Of Int16)("LeagueID"))
                 Where
                         eventLbDependency.Field(Of Int16)("LeagueID") = leagueId _
                 AndAlso eventLbDependency.Field(Of Byte)("CoverageLevel") = coverageLevel _
                 AndAlso eventLbDependency.Field(Of Int16)("EventID") = eventId _
                 AndAlso eventLbDependency.Field(Of Byte)("AttributeEntityID") = selectedListbox _
                 AndAlso eventLbDependency.Field(Of Int16)("AttributeDataID") < 0
        Select (New EventData(eventLbDependency.Field(Of Byte)("AttributeEntityDetailID"), eventLbDependency.Field(Of Int16)("AttributeDataID"), eventLbDependency.Field(Of String)("AttributeData"), eventLbDependency.Field(Of Byte)("AttributeEntityID"), eventLbDependency.Field(Of Int16)("AttributeEntityDataID"), eventLb.Field(Of String)("AttributeDesc"), eventLb.Field(Of String)("PBPColumnName")))).ToList()
    End Function
    'Event List box data selection to reset  back  the  player data
    Public Function GetEventAttributeData(selectedListbox As Integer, gtSoccerRulesEventLbData As DataTable, leagueId As Integer, coverageLevel As Integer, eventId As Integer) As List(Of EventDataValues)
        Return (From eventLbData In gtSoccerRulesEventLbData.AsEnumerable()
                Where eventLbData.Field(Of Int16)("LeagueID") = leagueId _
                AndAlso eventLbData.Field(Of Byte)("CoverageLevel") = coverageLevel _
                AndAlso eventLbData.Field(Of Int16)("EventID") = eventId _
                AndAlso eventLbData.Field(Of Byte)("AttributeID") = selectedListbox
         Select New EventDataValues(eventLbData.Field(Of Int16)("AttributeDataID"), eventLbData.Field(Of String)("AttributeData"))).ToList()
    End Function

    'Event List box data selection to Fill  other List box values
    Public Function GetEventDependencyListData(selectedListbox As Integer, selectedListboxData As Integer, gtSoccerRulesEventLbDependency As DataTable, leagueId As Integer, coverageLevel As Integer, eventId As Integer) As List(Of EventData)
        'AttributeEntityID   selectedListbox , AttributeEntityDetailID (Box should be disbaled),AttributeEntityDataID (selectedListbox index ),AttributeData(FALSE,FIELD OFF,Power), AttributeDataID(-100, +100)
        Dim distinctDependency = (From eventLbDependency In gtSoccerRulesEventLbDependency
                                 Where eventLbDependency.Field(Of Int16)("LeagueID") = leagueId _
               AndAlso eventLbDependency.Field(Of Byte)("CoverageLevel") = coverageLevel _
               AndAlso eventLbDependency.Field(Of Int16)("EventID") = eventId _
               AndAlso eventLbDependency.Field(Of Byte)("AttributeEntityID") = selectedListbox _
               AndAlso eventLbDependency.Field(Of Int16)("AttributeDataID") > 0
              Select
                   leagueIdD = eventLbDependency.Field(Of Int16)("LeagueID"),
                   coverageLevelD = eventLbDependency.Field(Of Byte)("CoverageLevel"),
                   eventIdD = eventLbDependency.Field(Of Int16)("EventID"),
                   attributeEntityIdD = eventLbDependency.Field(Of Byte)("AttributeEntityID"),
                   attributeEntityDetailIdD = eventLbDependency.Field(Of Byte)("AttributeEntityDetailID"),
                   attributeEntityDataIdD = eventLbDependency.Field(Of Int16)("AttributeEntityDataID")
                   Distinct
                  ).ToList()

        Return (From eventLbDependency In distinctDependency
                 Let data1 = (From eventLbData In gtSoccerRulesEventLbDependency.AsEnumerable()
                   Where eventLbData.Field(Of Int16)("LeagueID") = eventLbDependency.leagueIdD _
                   AndAlso eventLbData.Field(Of Byte)("CoverageLevel") = eventLbDependency.coverageLevelD _
                   AndAlso eventLbData.Field(Of Int16)("EventID") = eventLbDependency.eventIdD _
                   AndAlso eventLbData.Field(Of Byte)("AttributeEntityID") = eventLbDependency.attributeEntityIdD _
                   AndAlso eventLbData.Field(Of Byte)("AttributeEntityDetailID") = eventLbDependency.attributeEntityDetailIdD
          Select New EventDataValues(eventLbData.Field(Of Int16)("AttributeDataID"), eventLbData.Field(Of String)("AttributeData")))
          Select (New EventData(eventLbDependency.attributeEntityIdD, eventLbDependency.attributeEntityDetailIdD, eventLbDependency.attributeEntityDataIdD, New List(Of EventDataValues)(data1.ToList())))).ToList()
    End Function
    Public Function GetEventDependency(gtSoccerRulesEventLBs As DataTable, gtSoccerRulesEventLbDependency As DataTable, leagueId As Integer, coverageLevel As Integer, eventId As Integer) As List(Of Byte)
        Return (From eventLbDependency In gtSoccerRulesEventLbDependency.AsEnumerable()
                 Join eventLb In gtSoccerRulesEventLBs.AsEnumerable() _
                  On eventLb.Field(Of Int16)("EventID") Equals (eventLbDependency.Field(Of Int16)("EventID")) _
                 And eventLb.Field(Of Byte)("AttributeID") Equals (eventLbDependency.Field(Of Byte)("AttributeEntityDetailID")) _
                 And eventLb.Field(Of Byte)("CoverageLevel") Equals (eventLbDependency.Field(Of Byte)("CoverageLevel")) _
                 And eventLb.Field(Of Int16)("LeagueID") Equals (eventLbDependency.Field(Of Int16)("LeagueID"))
                 Where
                         eventLbDependency.Field(Of Int16)("LeagueID") = leagueId _
                 AndAlso eventLbDependency.Field(Of Byte)("CoverageLevel") = coverageLevel _
                 AndAlso eventLbDependency.Field(Of Int16)("EventID") = eventId
        Select attributeEntityDetailId = eventLbDependency.Field(Of Byte)("AttributeEntityID") Distinct
         Order By attributeEntityDetailId Descending).ToList()
    End Function
    Public Function GetEventPbpColumns(gtSoccerRulesEventLBs As DataTable, eventId As Integer) As List(Of EventDataValues)
        Return (From eventLb In gtSoccerRulesEventLBs.AsEnumerable()
                 Where eventLb.Field(Of Int16)("EventID") = eventId _
                 Select New EventDataValues(eventLb.Field(Of Byte)("AttributeID"), eventLb.Field(Of String)("PBPColumnName"))).ToList()
    End Function
    Public Function GetEventDependencyInc(selectedListbox As Integer, selectedListboxData As Integer, gtSoccerRulesEventLBs As DataTable, gtSoccerRulesEventLbDependency As DataTable, leagueId As Integer, coverageLevel As Integer, eventId As Integer) As List(Of EventData)
        'AttributeEntityID   selectedListbox , AttributeEntityDetailID (Box should be disbaled),AttributeEntityDataID (selectedListbox index ),AttributeData(FALSE,FIELD OFF,Power), AttributeDataID(-100, +100)
        Return (From eventLbDependency In gtSoccerRulesEventLbDependency.AsEnumerable()
                 Join eventLb In gtSoccerRulesEventLBs.AsEnumerable() _
                  On eventLb.Field(Of Int16)("EventID") Equals (eventLbDependency.Field(Of Int16)("EventID")) _
                 And eventLb.Field(Of Byte)("AttributeID") Equals (eventLbDependency.Field(Of Byte)("AttributeEntityDetailID")) _
                 And eventLb.Field(Of Byte)("CoverageLevel") Equals (eventLbDependency.Field(Of Byte)("CoverageLevel")) _
                 And eventLb.Field(Of Int16)("LeagueID") Equals (eventLbDependency.Field(Of Int16)("LeagueID"))
                 Where
                         eventLbDependency.Field(Of Int16)("LeagueID") = leagueId _
                 AndAlso eventLbDependency.Field(Of Byte)("CoverageLevel") = coverageLevel _
                 AndAlso eventLbDependency.Field(Of Int16)("EventID") = eventId _
                 AndAlso eventLbDependency.Field(Of Object)("AttributeEntityDataID") = selectedListboxData _
                 AndAlso eventLbDependency.Field(Of Byte)("AttributeEntityID") = selectedListbox _
                 AndAlso eventLbDependency.Field(Of Int16)("AttributeDataID") < 0
        Select (New EventData(eventLbDependency.Field(Of Byte)("AttributeEntityDetailID"), eventLbDependency.Field(Of Int16)("AttributeDataID"), eventLbDependency.Field(Of String)("AttributeData"), eventLbDependency.Field(Of Byte)("AttributeEntityID"), eventLbDependency.Field(Of Int16)("AttributeEntityDataID"), eventLb.Field(Of String)("AttributeDesc"), eventLb.Field(Of String)("PBPColumnName")))).ToList()
    End Function

    Public Class EventData
        Public Property AttributeId() As Byte
            Get
                Return _attributeId
            End Get
            Set(value As Byte)
                _attributeId = value
            End Set
        End Property
        Private _attributeId As Integer
        Public Property AttributeDesc() As String
            Get
                Return _attributeDesc
            End Get
            Set(value As String)
                _attributeDesc = value
            End Set
        End Property
        Private _attributeDesc As String
        Public Property EventDataValue() As List(Of EventDataValues)
            Get
                Return _eventDataValue
            End Get
            Set(value As List(Of EventDataValues))
                _eventDataValue = value
            End Set
        End Property
        Private _eventDataValue As List(Of EventDataValues)
        Public Property PbpColumnName() As String
            Get
                Return _pbpColumnName
            End Get
            Set(value As String)
                _pbpColumnName = value
            End Set
        End Property
        Private _pbpColumnName As String
        Private _attributeEntityDetailId As String
        Public Property AttributeEntityDetailId() As String
            Get
                Return _attributeEntityDetailId
            End Get
            Set(value As String)
                _attributeEntityDetailId = value
            End Set
        End Property
        Private _attributeEntityId As Byte
        Public Property AttributeEntityId() As Byte
            Get
                Return _attributeEntityId
            End Get
            Set(value As Byte)
                _attributeEntityId = value
            End Set
        End Property
        Private _attributeEntityDataId As Int16
        Public Property AttributeEntityDataId() As Int16
            Get
                Return _attributeEntityDataId
            End Get
            Set(value As Int16)
                _attributeEntityDataId = value
            End Set
        End Property
        Private _attributeDataId? As Int16
        Public Property AttributeDataId() As Nullable(Of Int16)
            Get
                Return _attributeDataId
            End Get
            Set(value As Nullable(Of Int16))
                _attributeDataId = value
            End Set
        End Property
        Private _attributeData As String
        Public Property AttributeData() As String
            Get
                Return _attributeData
            End Get
            Set(value As String)
                _attributeData = value
            End Set
        End Property
        Private _defaultSelection? As Int16
        Public Property DefaultSelection() As Nullable(Of Int16)
            Get
                Return _defaultSelection
            End Get
            Set(value As Nullable(Of Int16))
                _defaultSelection = value
            End Set
        End Property

        Private _eventId As Int16
        Public Property EventId() As Nullable(Of Int16)
            Get
                Return _eventId
            End Get
            Set(value As Nullable(Of Int16))
                _eventId = value
            End Set
        End Property

        Public Sub New()
        End Sub
        Public Sub New(pAttributeId As Byte, pListBoxDesc As String, pEventDataValue As List(Of EventDataValues), pPbpColumnName As String, pDefaultSelection As Int16?)
            AttributeId = pAttributeId
            AttributeDesc = pListBoxDesc
            EventDataValue = pEventDataValue
            PbpColumnName = pPbpColumnName
            DefaultSelection = pDefaultSelection
        End Sub

        Public Sub New(pAttributeEntityId As Byte, pAttributeEntityDetailId As String, pAttributeEntityDataId As Int16, pEventDataValue As List(Of EventDataValues))
            AttributeEntityId = pAttributeEntityId
            AttributeEntityDetailId = pAttributeEntityDetailId
            AttributeEntityDataId = pAttributeEntityDataId
            EventDataValue = pEventDataValue
        End Sub

        Public Sub New(pAttributeEntityDetailId As Byte, pDataId? As Int16, pDataDesc As String, pAttributeEntityId As Byte, pAttributeEntityDataId As Int16, pAttributeDesc As String, pPbpColumnName As String)
            AttributeEntityDetailId = pAttributeEntityDetailId
            AttributeData = pDataDesc
            AttributeDataId = pDataId
            AttributeEntityId = pAttributeEntityId
            AttributeEntityDataId = pAttributeEntityDataId
            AttributeDesc = pAttributeDesc
            PbpColumnName = pPbpColumnName
        End Sub

        Public Sub New(pEventId As Integer, pEnableListPbpColumnName As String, pDependenceList As List(Of EventDataValues))
            EventId = pEventId
            PbpColumnName = pEnableListPbpColumnName
            EventDataValue = pDependenceList
        End Sub

    End Class

    Public Class EventDataValues
        Private _dataId? As Int16
        Public Property DataId() As Nullable(Of Int16)
            Get
                Return _dataId
            End Get
            Set(value As Nullable(Of Int16))
                _dataId = value
            End Set
        End Property
        Private _dataDesc As String
        Public Property DataDesc() As String
            Get
                Return _dataDesc
            End Get
            Set(value As String)
                _dataDesc = value
            End Set
        End Property
        Public Sub New()
        End Sub
        Public Sub New(pDataId? As Int16, pDataDesc As String)
            DataDesc = pDataDesc
            DataId = pDataId
        End Sub
    End Class

    Public Class EventDataDependecy
        Private _eventId As Int16
        Public Property EventId() As Int16
            Get
                Return _eventId
            End Get
            Set(value As Int16)
                _eventId = value
            End Set
        End Property

        Private _boxEnable As Int16
        Public Property BoxEnable() As Int16
            Get
                Return _boxEnable
            End Get
            Set(value As Int16)
                _boxEnable = value
            End Set
        End Property

        Private _boxDependency1 As Int16
        Public Property BoxDependency1() As Int16
            Get
                Return _boxDependency1
            End Get
            Set(value As Int16)
                _boxDependency1 = value
            End Set
        End Property

        Private _boxDependencyData1 As Int16
        Public Property BoxDependencyData1() As Int16
            Get
                Return _boxDependencyData1
            End Get
            Set(value As Int16)
                _boxDependencyData1 = value
            End Set
        End Property

        Private _boxDependency2 As Int16
        Public Property BoxDependency2() As Int16
            Get
                Return _boxDependency2
            End Get
            Set(value As Int16)
                _boxDependency2 = value
            End Set
        End Property

        Private _boxDependencyData2 As Int16
        Public Property BoxDependencyData2() As Int16
            Get
                Return _boxDependencyData2
            End Get
            Set(value As Int16)
                _boxDependencyData2 = value
            End Set
        End Property
        Public Sub New()
        End Sub
        Public Sub New(pEventId As Int16, pBoxEnable As Int16, pBoxDependency1 As Int16, pBoxDependencyData1 As Int16, pBoxDependency2 As Int16, pBoxDependencyData2 As Int16)
            EventId = pEventId
            BoxEnable = pBoxEnable
            BoxDependency1 = pBoxDependency1
            BoxDependencyData1 = pBoxDependencyData1
            BoxDependency2 = pBoxDependency2
            BoxDependencyData2 = pBoxDependencyData2
        End Sub
    End Class

End Class