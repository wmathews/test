﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region


#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsGameDetails
' Author        : Ravi Krishna
' Created Date  : 24 April 2009
' Description   : This class contains information specific to the Games assigned for the user.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region


Public Class clsValidateScore

#Region "Constants & Variables"
    Shared m_myInstance As clsValidateScore
    Private m_objValidateScore As SoccerDAL.clsValidateScore = SoccerDAL.clsValidateScore.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsValidateScore
        If m_myInstance Is Nothing Then
            m_myInstance = New clsValidateScore
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Functions"

    Public Function GetScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objValidateScore.GetScores(GameCode, FeedNumber)
    End Function

#End Region

End Class
