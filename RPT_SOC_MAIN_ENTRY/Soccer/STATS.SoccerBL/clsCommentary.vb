﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsCommentary
' Author        : Dijo Davis
' Created Date  : 29-04-09
' Description   : This is the business logic for commentary module.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsCommentary
#Region "Constants & Variables"
    Shared m_myInstance As clsCommentary
    Private m_objclsCommentary As STATS.SoccerDAL.clsCommentary = STATS.SoccerDAL.clsCommentary.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsCommentary
        If m_myInstance Is Nothing Then
            m_myInstance = New clsCommentary
        End If
        Return m_myInstance
    End Function
#End Region

#Region "Commentary"
    ''' <summary>
    ''' Function to fetch available languages
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLanguages() As DataSet
        Try
            Return m_objclsCommentary.GetLanguages()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

   
    ''' <summary>
    ''' Functrion to save commentry
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <param name="intLanguageID"></param>
    ''' <param name="intPeriod"></param>
    ''' <param name="intTimeElapsed"></param>
    ''' <param name="strheadLine"></param>
    ''' <param name="strComment"></param>
    ''' <remarks></remarks>
    Public Sub AddCommentary(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intLanguageID As Int32, ByVal intPeriod As Int32, ByVal intTimeElapsed As Int32, ByVal strheadLine As String, ByVal strComment As String, ByVal intCommentType As Integer, ByVal strSystemTime As String, ByVal Reporter_Slno As Integer)
        Try
            m_objclsCommentary.AddCommentary(intGameCode, intFeedNumber, intLanguageID, intPeriod, intTimeElapsed, strheadLine, strComment, intCommentType, strSystemTime, Reporter_Slno)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Function to fetch commentary data
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCommentary(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32) As DataSet
        Try
            Return m_objclsCommentary.GetCommentary(intGameCode, intFeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Function to delete commentary data
    ''' </summary>
    ''' <param name="decSequenceNumber"></param>
    ''' <remarks></remarks>
    Public Sub DeleteCommentary(ByVal decSequenceNumber As Decimal)
        Try
            m_objclsCommentary.DeleteCommentary(decSequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Function to Edit Commentary Data
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <param name="intLanguageID"></param>
    ''' <param name="intPeriod"></param>
    ''' <param name="intTimeElapsed"></param>
    ''' <param name="strheadLine"></param>
    ''' <param name="strComment"></param>
    ''' <param name="decSequenceNumber"></param>
    ''' <remarks></remarks>
    Public Sub UpdateCommentary(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intLanguageID As Int32, ByVal intPeriod As Int32, ByVal intTimeElapsed As Int32, ByVal strheadLine As String, ByVal strComment As String, ByVal intCommentType As Integer, ByVal decSequenceNumber As Decimal, ByVal Reporter_Slno As Integer)
        Try
            m_objclsCommentary.UpdateCommentary(intGameCode, intFeedNumber, intLanguageID, intPeriod, intTimeElapsed, strheadLine, strComment, intCommentType, decSequenceNumber, Reporter_Slno)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' To get PBP string
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intLanguageID"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPBPString(ByVal intGameCode As Int32, ByVal intLanguageID As Int32, ByVal intFeedNumber As Int32)
        Try
            Return m_objclsCommentary.GetPBPString(intGameCode, intLanguageID, intFeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Arindam 23-Aug
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intLanguageID"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCommType(ByVal intLanguageID As Int32, ByVal clientID As Integer) As DataSet
        Try
            Return m_objclsCommentary.GetCommType(intLanguageID, clientID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To get PBP data for commentary
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPBPDataForCommentary(ByVal intGameCode As Int32, ByVal language_id As Integer) As DataSet
        Try
            Return m_objclsCommentary.GetPBPDataForCommentary(intGameCode, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To get Coach info for commentary
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCoachInfoForCommentary(ByVal intGameCode As Int32, ByVal language_id As Integer) As DataSet
        Try
            Return m_objclsCommentary.GetCoachInfoForCommentary(intGameCode, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetInitialCoachInfoForCommentary(ByVal intTeamID As Int32, ByVal language_id As Integer) As DataSet
        Try
            Return m_objclsCommentary.GetInitialCoachInfoForCommentary(intTeamID, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCommRecordBasedOnSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objclsCommentary.GetPBPRecordBasedOnSeqNumber(GameCode, FeedNumber, Sequence_Number, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region
End Class
