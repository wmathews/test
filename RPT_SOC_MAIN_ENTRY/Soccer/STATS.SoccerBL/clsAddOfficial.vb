﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddOfficial
' Author        : Shravani
' Created Date  : 12th June,2009
' Description   : Business Logic for AddOfficial Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsAddOfficial

#Region " Constants & Variables "
    Shared Instance As clsAddOfficial
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Private m_objAddOfficial As STATS.SoccerDAL.clsAddOfficial = STATS.SoccerDAL.clsAddOfficial.GetAccess()
#End Region

#Region " User Methods "
    Public Shared Function GetInstance() As clsAddOfficial
        If Instance Is Nothing Then
            Instance = New clsAddOfficial
        End If
        Return Instance
    End Function

    Public Sub InsertUpdateOfficial(ByVal PersonID As Integer, ByVal LeagueID As Integer, ByVal UniformNumber As String, ByVal ReporterRole As String, ByVal LastName As String, ByVal FirstName As String, ByVal DemoData As Char, ByVal GameCode As Integer, ByVal FeedNumber As Integer)
        Try
            m_objAddOfficial.InsertUpdateOfficial(PersonID, LeagueID, UniformNumber, ReporterRole, LastName, FirstName, DemoData, GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function SelectAddOfficial(ByVal LeagueID As Integer, ByVal SortOrder As String) As DataSet
        Try
            Return m_objAddOfficial.SelectAddOfficial(LeagueID, SortOrder)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteAddOfficial(ByVal RefereeID As Integer, ByVal LeagueID As Integer, ByVal DemoData As Char, ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objAddOfficial.DeleteAddOfficial(RefereeID, LeagueID, DemoData, GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetUniform() As Integer
        Try
            Return m_objAddOfficial.GetUniform()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Get League for Demo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLeague() As DataSet
        Try
            Return m_objAddOfficial.GetLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectAddOfficialDemo(ByVal SortOrder As String) As DataSet
        Try
            Return m_objAddOfficial.SelectAddOfficialDemo(SortOrder)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FetchMaxRefereeNUPID() As Long
        Try
            Return m_objWebSoc._objWebSoc.FetchMaxRefereeNupid(System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

End Class
