﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsTeamSetup
' Author        : Shirley Ranjini
' Created Date  : 11th May,2009
' Description   : Business Logic for TeamSetup Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsTeamSetup

#Region " Constants & Variables "

    Private m_objDLTeamSetup As STATS.SoccerDAL.clsTeamSetup = STATS.SoccerDAL.clsTeamSetup.GetAccess()

#End Region

#Region " User Methods "
    ''' <summary>
    ''' Fetching Rosters for Home and Away Teams by passing Gamecode and LeagueId as Parameters
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="LeagueID"></param>
    ''' <returns>Dataset which Contains Home and Away Team Players</returns>
    ''' <remarks></remarks>
    Public Function GetRosterInfo(ByVal GameCode As Integer, ByVal LeagueID As Integer, ByVal LanguageID As Int32) As DataSet
        Try
            Return m_objDLTeamSetup.GetRosterInfo(GameCode, LeagueID, "NAME", LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UpdateRosterInfo(ByVal GameCode As Integer, ByVal strXmlTeamSetupData As String)
        Try
            m_objDLTeamSetup.UpdateRosterInfo(GameCode, strXmlTeamSetupData)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function GetFormation() As DataSet
        Try
            Return m_objDLTeamSetup.GetFormation()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCoach(ByVal TeamID As Integer) As DataSet
        Try
            Return m_objDLTeamSetup.GetCoach(TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertUpdateTeamGame(ByVal TeamGame As DataSet) As Integer
        Try
            Dim strXmlTeamGameData As String = TeamGame.GetXml()
            m_objDLTeamSetup.InsertUpdateTeamGame(strXmlTeamGameData)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
   

    Public Function GetFormationCoach(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer) As DataSet
        Try
            Return m_objDLTeamSetup.GetFormationCoach(GameCode, FeedNumber, TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLineupEventCnt(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ModuleId As Integer) As Integer
        Try
            Return m_objDLTeamSetup.GetLineUpEventCnt(GameCode, FeedNumber, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetInjuredSuspendedPlayers(ByVal TeamID As Integer) As DataSet
        Try
            Return m_objDLTeamSetup.GetInjuredSuspendedPlayers(TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteInjuredOrSuspendedPlayer(ByVal LeagueID As Integer, ByVal TeamID As Integer, ByVal PlayerID As Integer) As Integer
        Try
            m_objDLTeamSetup.DeleteInjuredOrSuspended(LeagueID, TeamID, PlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetFormationSpecification(ByVal FormationID As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objDLTeamSetup.GetFormationSpecification(FormationID, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetUniformPictures(ByVal TeamID As Integer) As DataSet
        Try
            Return m_objDLTeamSetup.GetUniformPictures(TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetUniformColors(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objDLTeamSetup.GetUniformColors(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetplayerCount(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal PlayerID As Integer) As Integer
        Try
            Return m_objDLTeamSetup.GetPlayerCount(GameCode, FeedNumber, PlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function UpdateExisPBPPlayerWithNewPlayer(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal ExisPlayerID As Integer, ByVal NewPlayerID As Integer) As Integer
        Try
            m_objDLTeamSetup.UpdateExisPBPPlayerWithNewPlayer(GameCode, FeedNumber, ReporterRole, ExisPlayerID, NewPlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetShirtandShortColors(ByVal KitNo As Integer, ByVal TeamID As Integer) As DataSet
        Try
            Return m_objDLTeamSetup.GetShirtandShortColors(KitNo, TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetEndGame(ByVal Gamecode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objDLTeamSetup.GetEndGame(Gamecode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPeriodEndCount(ByVal Gamecode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer) As Integer
        Try
            Return m_objDLTeamSetup.GetPeriodEndCount(Gamecode, FeedNumber, Period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertOnfield(ByVal Gamecode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal Playerdata As String, ByVal formationdata As String) As Integer
        Try
            Return m_objDLTeamSetup.InsertPBPOnfield(Gamecode, FeedNumber, ReporterRole, Playerdata, formationdata)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateLineupEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal EditedTime As String) As Integer
        Try
            Return m_objDLTeamSetup.UpdateLineupEvent(GameCode, FeedNumber, ReporterRole, EditedTime)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub UpdateLineupsAndOverwriteExisPlayerData(ByVal GameCode As Integer, ByVal strXmlTeamSetupData As String, ByVal ReporterRole As String)
        Try
            m_objDLTeamSetup.UpdateLineupsAndOverwriteExisPlayerData(GameCode, strXmlTeamSetupData, ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
#End Region
End Class
