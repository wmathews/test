﻿#Region " Options "
Option Explicit Off
Option Strict Off
#End Region

Imports System.Windows.Forms
Imports System.Drawing

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : ClsPbpTree
' Author        : Wilson Mathews
' Created Date  : 8th Feb,2016
' Description   : Get the Player Data
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class ClsPbpTree
#Region "Constants & Variables"
    Shared _myInstance As ClsPbpTree
    Private _objActionsAssist As ClsActionsAssist = ClsActionsAssist.GetInstance()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsPbpTree
        If _myInstance Is Nothing Then
            _myInstance = New ClsPbpTree
        End If
        Return _myInstance
    End Function
#End Region

#Region "User Functions"

    Public Sub PbpTreeLoad(ByVal pbpData As DataTable, ByVal dgvPbp As DataGridView, ByVal fullLoad As Boolean, Optional ByVal primaryReporter As Boolean = False, Optional ByVal fetchPrimaryInsertedData As Boolean = False)
        Try

            If fullLoad Then
                dgvPbp.DataSource = Nothing
            End If

            Dim dgPbpDataSource As DataTable
            dgPbpDataSource = dgvPbp.DataSource

            Dim dgvRowBackcolor As Color
            Dim indexSelected As Integer = 0
            Dim sequenceNumber As Decimal = 0
            If (dgvPbp.SelectedRows.Count = 1) Then
                indexSelected = dgvPbp.SelectedRows(0).Index
                If Not IsDBNull(dgvPbp.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value) Then
                    sequenceNumber = CDec(dgvPbp.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value)
                End If
                dgvRowBackcolor = dgvPbp.SelectedRows(0).DefaultCellStyle.SelectionBackColor
            End If

            'LoadFullPbP from DB
            If dgPbpDataSource Is Nothing Then
                'Binding other than Deleted record (SEQUENCE_NUMBER" = -1)
                Dim pbpDataRows = (From pbp In pbpData.AsEnumerable().OfType(Of DataRow)().Where(Function(row) row.Field(Of Decimal?)("SEQUENCE_NUMBER") <> -1))
                If pbpDataRows.Any() Then
                    dgvPbp.DataSource = pbpDataRows.CopyToDataTable()
                End If

            ElseIf pbpData.Rows.Count > 0 Then
                'Insert/append a record in PBP tree
                Dim recordsnew = (From pbp In pbpData Where pbp.Field(Of Int32?)("EDIT_UID") = 0 AndAlso pbp.Field(Of String)("CONTINUATION") = "F").ToList()
                For Each r In recordsnew
                    dgPbpDataSource.NewRow()
                    dgPbpDataSource.ImportRow(r)
                Next

                ''Edit/update the existing record in PBP tree
                Dim recordsEdit = (From pbp In pbpData Where pbp.Field(Of Int32?)("EDIT_UID") > 0 AndAlso pbp.Field(Of Decimal)("SEQUENCE_NUMBER") <> -1).AsEnumerable()
                Dim recordsEdits = From recodEdit In recordsEdit
                                   Join dgPbp In dgPbpDataSource.AsEnumerable()
                                   On dgPbp.Field(Of Decimal?)("SEQUENCE_NUMBER") Equals (recodEdit.Field(Of Decimal?)("SEQUENCE_NUMBER")) _
                                   Select recodEdit, dgPbp

                For Each r In recordsEdits
                    r.dgPbp.ItemArray() = r.recodEdit.ItemArray()
                Next

                'WIN/LOSS & Time Edit based in Continuation flag
                'edit_uid = 0 added for win/loss player X/Y edits
                Dim recordsInbetween = (From pbp In pbpData Where pbp.Field(Of String)("CONTINUATION") = "T" AndAlso pbp.Field(Of Int32?)("EDIT_UID") = 0 AndAlso pbp.Field(Of Decimal?)("SEQUENCE_NUMBER") <> -1).ToList()
                For Each r In recordsInbetween
                    Dim recordIndex = (From theRow As DataGridViewRow In dgvPbp.Rows _
                                Where Not IsDBNull(theRow.Cells("SEQUENCE_NUMBER").Value) AndAlso theRow.Cells("SEQUENCE_NUMBER").Value < r.Field(Of Decimal?)("SEQUENCE_NUMBER")
                                Select theRow.Index).LastOrDefault()
                    Dim drrecordsInbetween As DataRow = dgPbpDataSource.NewRow()
                    drrecordsInbetween.ItemArray() = r.ItemArray()
                    dgPbpDataSource.Rows.InsertAt(drrecordsInbetween, recordIndex + 1)
                Next

                If Not fetchPrimaryInsertedData Then
                    'Delete record in PBP tree
                    Dim blnDelete As Boolean = False
                    Dim recordsDelete = (From pbp In pbpData Where pbp.Field(Of Decimal)("SEQUENCE_NUMBER") = -1).AsEnumerable()
                    ''Delete/remove the existing record in PBP tree
                    Dim recordsDeletes = From recodDelete In recordsDelete
                                       Join dgPbp In dgPbpDataSource.AsEnumerable()
                                       On dgPbp.Field(Of Int32?)("UNIQUE_ID") Equals (recodDelete.Field(Of Int32?)("EDIT_UID")) _
                                       Select recodDelete, dgPbp
                    For Each r In recordsDeletes
                        r.dgPbp.Delete()
                        blnDelete = True
                    Next
                    'same record updation
                    If Not blnDelete Then
                        ''Time edit in A2 and same time A1 changing the event - duplicating records.
                        Dim recordsDel = From recDelete In recordsDelete
                                          Join dgPbp In dgPbpDataSource.AsEnumerable()
                                          On dgPbp.Field(Of Int32?)("UNIQUE_ID") Equals (recDelete.Field(Of Int32?)("UNIQUE_ID")) _
                                          Select recDelete, dgPbp
                        For Each r In recordsDel
                            r.dgPbp.Delete()
                        Next
                    End If
                End If


                Dim firstDisplayedIndex As Integer = 0
                If dgvPbp.FirstDisplayedCell IsNot Nothing Then
                    firstDisplayedIndex = dgvPbp.FirstDisplayedCell.RowIndex
                End If

                If fetchPrimaryInsertedData Then
                    dgPbpDataSource.Rows(dgPbpDataSource.Rows.Count - 1).AcceptChanges()
                Else
                    dgPbpDataSource.AcceptChanges()
                End If

                If sequenceNumber > 0 And indexSelected > 0 Then
                    Dim recordSelected = dgPbpDataSource.AsEnumerable().
                              Select(Function(r, i) New With {.Row = r, .Index = i}).
                                  Where(Function(x) Not IsDBNull(x.Row.Field(Of Decimal?)("SEQUENCE_NUMBER")) AndAlso x.Row.Field(Of Decimal?)("SEQUENCE_NUMBER") = sequenceNumber)
                    If recordSelected.Any() Then
                        'map the action based on SNO and select the action.
                        indexSelected = recordSelected.First().Index
                        dgvPbp.Rows(indexSelected).Selected = True
                        dgvPbp.Rows(indexSelected).DefaultCellStyle.SelectionBackColor = dgvRowBackcolor
                        dgvPbp.Rows(indexSelected).DefaultCellStyle.SelectionForeColor = Color.Black
                    Else
                        'if the selected timeline is deleted by another reporter then set index = 0 to clear the grid selection
                        ''firstDisplayedIndex = indexSelected
                        indexSelected = 0
                    End If
                Else
                    If primaryReporter Then
                        'in-memory record save - we delete the in-memory record and index is set to 0, so we need to take the last action index
                        indexSelected = (From theRow As DataGridViewRow In dgvPbp.Rows _
                                        Where IsDBNull(theRow.Cells("SEQUENCE_NUMBER").Value)
                                        Select theRow.Index).FirstOrDefault()
                    End If
                End If


                If indexSelected > 0 Then
                    dgvPbp.Rows(indexSelected).Selected = True
                    If firstDisplayedIndex = indexSelected Then
                        dgvPbp.FirstDisplayedScrollingRowIndex = indexSelected
                    Else
                        dgvPbp.FirstDisplayedScrollingRowIndex = firstDisplayedIndex
                    End If
                    dgvPbp.Rows(indexSelected).DefaultCellStyle.SelectionBackColor = dgvRowBackcolor
                    dgvPbp.Rows(indexSelected).DefaultCellStyle.SelectionForeColor = Color.Black
                Else
                    'clear selection if index = 0
                    dgvPbp.ClearSelection()
                    dgvPbp.FirstDisplayedScrollingRowIndex = firstDisplayedIndex
                End If
            End If

            'Hide grid column
            For columnIndex As Integer = 9 To dgvPbp.Columns.Count - 1
                dgvPbp.Columns(columnIndex).Visible = False
            Next
            ' Disable dgvPBP.Columns Sortable
            For Each Column In dgvPbp.Columns
                Column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub PbpDataUpdateRefreshed(ByVal pbpData As DataTable)
        'update the record REFRESHED = 'Y'
        If pbpData.Rows.Count > 0 Then
            Dim pbpDataDs As New DataSet
            pbpDataDs.Tables.Add(pbpData.Copy())
            _objActionsAssist.UpdatePbpRefresh(pbpDataDs.GetXml())
        End If
    End Sub
#End Region
    Public Class ListBoxColumnData
        Private _lstEvent As ListView
        Public Property ListEvent() As ListView
            Get
                Return _lstEvent
            End Get
            Set(value As ListView)
                _lstEvent = value
            End Set
        End Property
        Private _column1 As String
        Public Property Column1() As String
            Get
                Return _column1
            End Get
            Set(value As String)
                _column1 = value
            End Set
        End Property

        Private _column2 As String
        Public Property Column2() As String
            Get
                Return _column2
            End Get
            Set(value As String)
                _column2 = value
            End Set
        End Property

        Private _column3 As String
        Public Property Column3() As String
            Get
                Return _column3
            End Get
            Set(value As String)
                _column3 = value
            End Set
        End Property

        Public Sub New()

        End Sub
        Public Sub New(pListEvent As ListView, pColumn1 As String, pColumn2 As String, pColumn3 As String)
            ListEvent = pListEvent
            Column1 = pColumn1
            Column2 = pColumn2
            Column3 = pColumn3
        End Sub
    End Class

End Class