﻿Imports System.IO.Compression
Imports System.IO

Public Class GZipHelper
    Public Shared Function Decompress(ByVal data As [Byte]()) As DataSet
        Dim mem As New MemoryStream(data)
        Dim zip As New GZipStream(mem, CompressionMode.Decompress)
        Dim dataset As New DataSet()
        dataset.ReadXml(zip, XmlReadMode.ReadSchema)
        zip.Close()
        mem.Close()
        Return dataset
    End Function
    Public Shared Function Compressdataset(ByVal dataset As DataSet) As Byte()
        Dim data As [Byte]()
        Dim mem As New MemoryStream()
        Dim zip As New GZipStream(mem, CompressionMode.Compress)
        dataset.WriteXml(zip, XmlWriteMode.WriteSchema)
        zip.Close()
        data = mem.ToArray()
        mem.Close()
        Return data
    End Function
End Class
