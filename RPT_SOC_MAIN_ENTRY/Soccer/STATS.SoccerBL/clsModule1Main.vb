﻿
#Region "Imports"
Imports System
Imports STATS.SoccerDAL

Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsModule1Main
' Author        : Shirley Ranjini
' Created Date  : 01-06-09
' Description   : Class used for Business Logic of Module1 Main Screen
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class clsModule1Main

#Region "Constants & Variables"
    Shared m_myInstance As clsModule1Main
    Private m_objModule1Main As SoccerDAL.clsModule1Main = SoccerDAL.clsModule1Main.GetAccess()
    Private m_objGameDetails As clsGameSetup = clsGameSetup.GetInstance()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsModule1Main
        If m_myInstance Is Nothing Then
            m_myInstance = New clsModule1Main
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Defined Functions"

    Public Function GetUniqueID(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objModule1Main.GetUniqueID(GameCode, FeedNumber)
    End Function

    Public Function GetScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objModule1Main.GetScores(GameCode, FeedNumber)
    End Function

    Public Function GetGameCodeMod2() As DataSet
        Return m_objModule1Main.GetGameCodeMod2()
    End Function
    Public Function InsertPBPDataToSQL(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal IsEdited As String, ByVal languageid As Integer, ByVal ModuleId As Integer) As DataSet
        Try
            Return m_objModule1Main.InsertPBPDataToSQL(PBPXmlData, GameCode, FeedNumber, IsEdited, languageid, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FETCHES THE MODULE 2 GAMES ASSIGNED TO THE REPORTER SPECIFIED WITHIN +/- 24 HRS
    ''' </summary>
    ''' <param name="LanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMultipleGameDetails(ByVal LanguageID As Integer, ByVal ReporterID As Integer) As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objModule1Main.GetMultipleGameDetails(LanguageID, ReporterID)
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' GETS THE EVENTS STORED IN PBP TABLE SO AS TO DISPLAY IN THE MAIN SCREEN.
    ''' EVENTS SUCH AS SUB,BOOKINGS,FOULS ARE FETCHED FROM DATABASE
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FetchPBPEventsToDisplay(ByVal intGameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Dim DsMainEvents As DataSet
            DsMainEvents = m_objModule1Main.FetchMainScreenEvents(intGameCode, FeedNumber)
            Return DsMainEvents
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' GETS THE MASTER TABLES WHCIH ARE REQUIRED TO DISPLAY THE DATA IN THE MAIN SCREEN
    ''' EX. REASON_ID IS FILLED FOR A SUB EVENTS..TO DISPLAY IN MAIN SCREEN, WE ARE FETCHING FROM MASTER TABLES TO 
    ''' DISPLAY THE DESC...
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FetchMainScreenMasterData(ByVal LanguageID As Integer) As DataSet
        Try
            Dim DsMaster As DataSet

            DsMaster = m_objModule1Main.FetchMainScreenMasterData(LanguageID)
            DsMaster.Tables(0).TableName = "BOOKING_REASON"
            DsMaster.Tables(1).TableName = "BOOKING_TYPE"
            DsMaster.Tables(2).TableName = "SUB_REASON"
            DsMaster.Tables(3).TableName = "SHOT_TYPE"
            DsMaster.Tables(4).TableName = "EVENT_CODE"
            Return DsMaster

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To insert current game data
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <param name="intReporterID"></param>
    ''' <param name="strReporterRole"></param>
    ''' <remarks></remarks>
    Public Sub InsertCurrentGame(ByVal intGameCode As Int32, ByVal intOldGameCode As Integer, ByVal intFeedNumber As Int32, ByVal intReporterID As Int32, ByVal strReporterRole As String, ByVal intModuleID As Integer, ByVal TimeElapsed As Integer, ByVal SysLocalTime As String, ByVal Type As String)
        Try
            m_objModule1Main.InsertCurrentGame(intGameCode, intOldGameCode, intFeedNumber, intReporterID, strReporterRole, intModuleID, TimeElapsed, SysLocalTime, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Fetching Rosters for Home and Away Teams by passing Gamecode and LeagueId as Parameters
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="LeagueID"></param>
    ''' <returns>Dataset which Contains Home and Away Team Players</returns>
    ''' <remarks></remarks>
    Public Function GetRosterInfo(ByVal GameCode As Integer, ByVal LeagueID As Integer, ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objModule1Main.GetRosterInfo(GameCode, LeagueID, "LINEUP", LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' GETS GAME STATISTICS
    ''' </summary>
    ''' <param name="GameCode">An integer represents GameCode</param>
    ''' <param name="FeedNumber">An integer represents Feeednumber</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTeamStats(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objModule1Main.GetTeamStats(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="feedNumber"></param>
    ''' <remarks></remarks>
    Public Sub InsertTeamStastoSummary(ByVal GameCode As Integer, ByVal feedNumber As Integer, ByVal CoverageLevel As Integer)
        Try
            m_objModule1Main.InsertTeamStatstoSummary(GameCode, feedNumber, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTeamColors(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objModule1Main.GetTeamColors(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetFormationCoach(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ManagerID As Integer, ByVal Type As String, ByVal languageid As Integer) As DataSet
        Try
            Return m_objModule1Main.GetFormationCoach(GameCode, FeedNumber, ManagerID, Type, languageid)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetUniformColors(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objModule1Main.GetUniformColors(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetGameDetails(ByVal LanguageID As Integer, ByVal UserType As Integer) As DataSet
        Dim m_dsGameDetails As New DataSet
        m_dsGameDetails = m_objModule1Main.GetGameDetails(LanguageID, UserType)
        Return m_dsGameDetails
    End Function
    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objModule1Main.GetMaxTimeElapsed(GameCode, FeedNumber)
    End Function
    Public Sub UpdateActiveGK(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer, ByVal langID As Integer, ByVal CoverageLevel As Integer)
        Try
            m_objModule1Main.UpdateActiveGK(GameCode, FeedNumber, TeamID, langID, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Demo"
    Public Function GetDemoGameDetails() As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objModule1Main.GetDemoGameDetails()
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#End Region
    

End Class
