﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient
Imports STATS.SoccerBL
Imports System.Globalization

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsUpComingAssignments
' Author        : Shravani
' Created Date  : 16th June,2009
' Description   : Business Logic for UpComingAssignments Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsUpComingAssignments

#Region " Constants & Variables "
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Shared Instance As clsUpComingAssignments
    Private m_objAssignments As STATS.SoccerDAL.clsUpComingAssignments = STATS.SoccerDAL.clsUpComingAssignments.GetAccess()
    Public dsGetLeaguedata As New DataSet
#End Region

#Region " User Methods "
    Public Shared Function GetInstance() As clsUpComingAssignments
        If Instance Is Nothing Then
            Instance = New clsUpComingAssignments
        End If
        Return Instance
    End Function

    Public Function LoadLeague() As DataSet
        Try
            Return m_objAssignments.LoadLeague()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DownloadUpComingAssignments(ByVal ReporterID As Integer, ByVal Days As Integer, ByVal LeagueID As Integer) As DataSet
        Try
            Return m_objWebSoc._objWebSoc.DownloadUpComingAssignments(ReporterID, Days, LeagueID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function getLeaguedata() As DataSet
        Try
            Return m_objWebSoc._objWebSoc.GetLeaguedata(System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetReporterSettings(ByVal reporterid As Integer) As DataSet
        Try
            Return m_objWebSoc._objWebSoc.GetReporterSettings(reporterid, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
