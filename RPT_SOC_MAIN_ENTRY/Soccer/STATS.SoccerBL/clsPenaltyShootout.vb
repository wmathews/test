﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsPenaltyShootout
' Author        : Shirley Ranjini
' Created Date  : 01-06-09
' Description   : Class used for Business Logic
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsPenaltyShootout

#Region "Constants & Variables"
    Shared m_myInstance As clsPenaltyShootout
    Private m_objPenaltyShootout As SoccerDAL.clsPenaltyShootout = SoccerDAL.clsPenaltyShootout.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsPenaltyShootout
        If m_myInstance Is Nothing Then
            m_myInstance = New clsPenaltyShootout
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Defined Functions"

    Public Function FetchShootoutInputs(ByVal GameCode As Integer, ByVal LanguageID As Integer) As DataSet
        Try
            Dim DsInputs As DataSet
            DsInputs = m_objPenaltyShootout.FetchShootoutInputs(GameCode, LanguageID)
            If DsInputs.Tables.Count > 0 Then
                DsInputs.Tables(0).TableName = "Team"
                DsInputs.Tables(1).TableName = "Goal_Zone"
                DsInputs.Tables(2).TableName = "Keeper_Zone"
                DsInputs.Tables(3).TableName = "Shot_Res"
                DsInputs.Tables(4).TableName = "Shot_Desc" ''Arindam_penalty
            End If
            Dim dtResult As New DataTable
            dtResult.TableName = "Result"
            dtResult.Columns.Add("RESULT_ID")
            dtResult.Columns.Add("RESULT_DESC")
            dtResult.Rows.Add()
            dtResult.Rows(0).Item("RESULT_ID") = 1
            dtResult.Rows(0).Item("RESULT_DESC") = "Scored"
            dtResult.Rows.Add()
            dtResult.Rows(1).Item("RESULT_ID") = 2
            dtResult.Rows(1).Item("RESULT_DESC") = "Missed"
            DsInputs.Tables.Add(dtResult)

            'Dim dtType As New DataTable
            'dtType.TableName = "Shot_Desc"
            'dtType.Columns.Add("SHOT_DESC_ID")
            'dtType.Columns.Add("SHOT_DESC")
            'dtType.Rows.Add()
            ''dtType.Rows(0).Item("TYPE_ID") = 1
            'dtType.Rows(0).Item("TYPE_DESC") = "Power"
            'dtType.Rows.Add()
            'dtType.Rows(1).Item("TYPE_ID") = 2
            'dtType.Rows(1).Item("TYPE_DESC") = "Placed"
            'DsInputs.Tables.Add(dtType)

            DsInputs.AcceptChanges()
            Return DsInputs
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FetchShootoutInputsPz(ByVal gameCode As Integer, ByVal languageId As Integer) As DataSet
        Try
            Return m_objPenaltyShootout.FetchPzShootoutInputs(GameCode, LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertPBPDataToSQL(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal langID As Integer) As DataSet
        Try
            Return m_objPenaltyShootout.InsertPBPDataToSQL(PBPXmlData, GameCode, FeedNumber, ReporterRole, langID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetShootoutPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objPenaltyShootout.GetShootoutPBPData(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
     Public Function GetShootoutPzpbpData(ByVal gameCode As Integer, ByVal feedNumber As Integer, ByVal languageId As Int32) As DataSet
        Try
            Return m_objPenaltyShootout.GetShootoutPzpbpData(GameCode, FeedNumber,languageId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPBPRecordBasedOnSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objPenaltyShootout.GetPBPRecordBasedOnSeqNumber(GameCode, FeedNumber, Sequence_Number, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function UpdatePBP(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal Type As String, ByVal langauge_id As Integer) As DataSet
        Try
            Return m_objPenaltyShootout.UpdatePBPData(PBPXmlData, GameCode, FeedNumber, ReporterRole, SequenceNumber, "EDIT", langauge_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function EditScoreEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal Type As String) As Integer
        Try
            Return m_objPenaltyShootout.EditScoreEvent(GameCode, FeedNumber, SequenceNumber, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteScoreEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As Integer
        Try
            Return m_objPenaltyShootout.DeleteScoreEvent(GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeletePBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal language_id As Int32) As Integer
        Try
            Return m_objPenaltyShootout.DeletePBPData(GameCode, FeedNumber, ReporterRole, SequenceNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal langauge_id As Integer) As DataSet
        Try
            Return m_objPenaltyShootout.GetPBPData(GameCode, FeedNumber, langauge_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateTeamGamePenaltyShootoutScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String) As Integer
        Try
            m_objPenaltyShootout.UpdateTeamGamePenaltyShootoutScores(GameCode, FeedNumber, ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class