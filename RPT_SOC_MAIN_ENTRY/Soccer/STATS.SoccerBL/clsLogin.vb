﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"

Imports System.Configuration
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsLogin
' Author        : Shravani
' Created Date  : 28th April,2009
' Description   : Business Logic for Login/Download
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsLogin

#Region " Constants & Variables "
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Private m_objDataAccess As New STATS.SoccerDAL.clsLogin
    Private m_dsGameDetails As New DataSet
    Private m_dsUserDetails As New DataSet
    Private m_dsLanguage As New DataSet
    Private m_dsReporterData As New DataSet
    Private m_dsTeamLogo As New DataSet
    Private m_dsCoachSeason As New DataSet
    Private m_dsSocReferee As New DataSet

#End Region

#Region " User Methods "

    Public Function dropDatabase(ByVal strQuery As String) As Integer
        Try
            Return m_objDataAccess.ExecuteNonQuery(strQuery)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Gets the language data from the Oracle and inserts into local Sql Table
    ''' </summary>
    ''' <returns>A Datset contains Language Data</returns>
    ''' <remarks>Calls from WebService </remarks>
    Public Function GetLanguage() As DataSet
        Try
            m_dsLanguage = GZipHelper.Decompress(m_objWebSoc._objWebSoc.GetLanguage(System.Configuration.ConfigurationManager.AppSettings("SecurityToken")))
            If (m_dsLanguage.Tables.Count > 0) Then
                If (m_dsLanguage.Tables(0).Rows.Count > 0) Then
                    'DELETE GLB_LANGUAGE TABLE DATA BEFORE INSERT INTO SQL
                    'm_objDataAccess.DeleteLanguageTableData()
                    'INSERT DATA INTO LOCAL SQL
                    FillLanguageTable(m_dsLanguage)
                End If
            End If
            Return m_dsLanguage
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Fills language table - GLB_LANGUAGE
    ''' </summary>
    ''' <param name="Language">A Dataset contains language table data </param>
    ''' <remarks></remarks>
    '''
    Private Sub FillLanguageTable(ByVal Language As DataSet)
        Try
            m_objDataAccess.FillLanguageTable(Language)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' Fills demo game table -

    Public Function FillDemoGames(ByVal FromDate As String, ByVal ToDate As String, ByVal Leageid As Int16) As DataSet
        Try

            Dim m_dsUserDetails As DataSet = m_objWebSoc._objWebSoc.GetDemoGames(FromDate, ToDate, Leageid, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            If (m_dsUserDetails.Tables.Count > 0) Then
                m_dsUserDetails.Tables(0).TableName = "UserDetails"
                m_dsUserDetails.Tables(1).TableName = "Status"
                m_dsUserDetails.Tables(2).TableName = "Games"
            End If
            Return m_dsUserDetails
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Validates the userdetails
    ''' </summary>
    ''' <param name="UserName">A String represents username</param>
    ''' <param name="Password">A String represents password</param>
    ''' <returns>A DataSet which contains tables, userdetails and status </returns>
    ''' <remarks>Status =0 - If Reporter logs for first time and games are assigned</remarks>
    ''' <remarks1>Status =2 - If No games are assigned to the reporter</remarks1>
    ''' <remarks2>Status =3 - If Reporter logs after the 1st time</remarks2>
    Public Function ValidateUserDetails(ByVal UserName As String, ByVal Password As String, ByVal language_id As Integer, ByVal NoofDays As Integer) As DataSet
        Dim intDaysFrom, intDaysTo As Integer
        Try
            
            Select Case ConfigurationSettings.AppSettings("Environment")
                Case "TESTRAC"
                    intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
                Case "PRODUCTION"
                    intDaysFrom = NoofDays
                Case Else
                    intDaysFrom = NoofDays
            End Select

            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            m_dsUserDetails = m_objWebSoc._objWebSoc.GetUserInfo(UserName, Password, intDaysFrom, intDaysTo, language_id, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            If (m_dsUserDetails.Tables.Count > 0) Then
                m_dsUserDetails.Tables(0).TableName = "UserDetails"
                m_dsUserDetails.Tables(1).TableName = "Status"
                m_dsUserDetails.Tables(2).TableName = "Games"
            End If
            Return m_dsUserDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function iso8859_unicode(ByVal src As String) As String
        Dim iso As Encoding = Encoding.GetEncoding("iso8859-1")
        Dim unicode As Encoding = Encoding.UTF8
        Dim isoBytes As Byte() = iso.GetBytes(src)
        Return unicode.GetString(isoBytes)
    End Function

    Public Function DownloadSoccerData(ByVal ReporterdID As Integer, ByVal LanguageID As Integer, ByVal GameCode As String, ByVal FeedNumber As Integer, ByVal ModuleID As Integer, ByVal ReporterRole As String, ByVal LeagueId As Integer, ByVal CovergaLevel As Integer) As Boolean
        Dim intDaysFrom, intDaysTo As Integer
        Dim strXmlSoccerData As String
        Dim dsUniformImages As New DataSet
        Dim ReporterSerialNo? As Integer
        Dim dsTeamLogo As New DataSet
        Try
            intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            ReporterSerialNo = CType(ReporterRole.Substring(ReporterRole.Length - 1), Integer?)
            m_objWebSoc._objWebSoc.Timeout = 800000
            m_objWebSoc._objWebSoc.PreAuthenticate = True

            'm_dsReporterData = m_objWebSoc.ImportSoccerData(ReporterdID, LanguageID, intDaysFrom, intDaysTo, CStr(GameCode), FeedNumber, ModuleID, ReporterRole, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            m_dsReporterData = GZipHelper.Decompress(m_objWebSoc._objWebSoc.ImportSoccerData(ReporterdID, LanguageID, intDaysFrom, intDaysTo, CStr(GameCode), FeedNumber, ModuleID, ReporterRole, System.Configuration.ConfigurationManager.AppSettings("SecurityToken")))
            If m_dsReporterData.Tables.Count > 0 Then

                If ModuleID = 1 Then
                    m_dsReporterData.Tables("LIVE_TOUCHES").Rows.Clear()
                    m_dsReporterData.Tables("LIVE_COMMENTARY").Rows.Clear()
                ElseIf ModuleID = 3 Then
                    m_dsReporterData.Tables("LIVE_COMMENTARY").Rows.Clear()
                    'm_dsReporterData.Tables("PBP").Rows.Clear()
                ElseIf ModuleID = 4 Then
                    m_dsReporterData.Tables("PBP").Rows.Clear()
                    m_dsReporterData.Tables("LIVE_TOUCHES").Rows.Clear()
                End If

                dsTeamLogo.Tables.Add(m_dsReporterData.Tables("TEAM_LOGO").Copy)
                dsUniformImages.Tables.Add(m_dsReporterData.Tables("SOC_UNIFORMIMAGE").Copy)

                m_dsReporterData.Tables.Remove("TEAM_LOGO")
                m_dsReporterData.Tables.Remove("SOC_UNIFORMIMAGE")

                strXmlSoccerData = m_dsReporterData.GetXml()
                m_dsReporterData.ReadXml(New System.IO.StringReader(iso8859_unicode(strXmlSoccerData)))

                'DELETE ALL TABLE DATA BEFORE INSERT
                m_objDataAccess.DeleteAllTableData()
                 m_objDataAccess.InsertDownLoaddata(strXmlSoccerData, LanguageID, CInt(ReporterSerialNo))

                FillTeamLogo(dsTeamLogo)

                If dsUniformImages IsNot Nothing Then
                    If dsUniformImages.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To dsUniformImages.Tables(0).Rows.Count - 1
                            If dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR") IsNot DBNull.Value Then
                                If Not dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR").ToString.Contains("#") Then
                                    dsUniformImages.Tables(0).Rows(i).BeginEdit()
                                    dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR") = "#" & dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR").ToString.ToUpper()
                                    dsUniformImages.Tables(0).Rows(i).EndEdit()
                                End If
                            End If

                            If dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR") IsNot DBNull.Value Then
                                If Not dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR").ToString.Contains("#") Then
                                    dsUniformImages.Tables(0).Rows(i).BeginEdit()
                                    dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR") = "#" & dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR").ToString.ToUpper()
                                    dsUniformImages.Tables(0).Rows(i).EndEdit()
                                End If
                            End If
                        Next
                        dsUniformImages.Tables(0).AcceptChanges()
                    End If
                End If
                FillUniformImages(dsUniformImages)
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function DownloadSoccerDataDemo(ByVal GameCode As String, ByVal ModuleID As Integer, ByVal CovergaLevel As Integer, ByVal LeagueId As Integer, ByVal LanguageID As Integer) As Boolean
        Dim intDaysFrom, intDaysTo As Integer
        Dim strXmlSoccerData As String
        Dim dsUniformImages As New DataSet
        Dim dsTeamLogo As New DataSet
        Try
            intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            m_objWebSoc._objWebSoc.Timeout = 800000
            m_objWebSoc._objWebSoc.PreAuthenticate = True
            m_dsReporterData = m_objWebSoc._objWebSoc.ImportSoccerDataDemo(CStr(GameCode), ModuleID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            If m_dsReporterData.Tables.Count > 0 Then

                Dim dr() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Select(" LEAGUE_ID = " & LeagueId)
                If dr.Length = 0 Then
                    If CovergaLevel = 6 Then
                        Dim drLB() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 6")
                        For i As Integer = 0 To drLB.Length - 1
                            drLB(i).BeginEdit()
                            drLB(i).Item("LEAGUE_ID") = LeagueId
                            drLB(i).EndEdit()
                        Next

                        Dim drLBData() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 6")
                        For i As Integer = 0 To drLBData.Length - 1
                            drLBData(i).BeginEdit()
                            drLBData(i).Item("LEAGUE_ID") = LeagueId
                            drLBData(i).EndEdit()
                        Next

                        Dim drLBDEP() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 6")
                        For i As Integer = 0 To drLBDEP.Length - 1
                            drLBDEP(i).BeginEdit()
                            drLBDEP(i).Item("LEAGUE_ID") = LeagueId
                            drLBDEP(i).EndEdit()
                        Next
                    ElseIf CovergaLevel = 5 Then
                        Dim drLB() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 5")
                        For i As Integer = 0 To drLB.Length - 1
                            drLB(i).BeginEdit()
                            drLB(i).Item("LEAGUE_ID") = LeagueId
                            drLB(i).EndEdit()
                        Next

                        Dim drLBData() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 5")
                        For i As Integer = 0 To drLBData.Length - 1
                            drLBData(i).BeginEdit()
                            drLBData(i).Item("LEAGUE_ID") = LeagueId
                            drLBData(i).EndEdit()
                        Next

                        Dim drLBDEP() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 5")
                        For i As Integer = 0 To drLBDEP.Length - 1
                            drLBDEP(i).BeginEdit()
                            drLBDEP(i).Item("LEAGUE_ID") = LeagueId
                            drLBDEP(i).EndEdit()
                        Next
                    ElseIf CovergaLevel = 4 Then
                        Dim drLB() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 4")
                        For i As Integer = 0 To drLB.Length - 1
                            drLB(i).BeginEdit()
                            drLB(i).Item("LEAGUE_ID") = LeagueId
                            drLB(i).EndEdit()
                        Next

                        Dim drLBData() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 4")
                        For i As Integer = 0 To drLBData.Length - 1
                            drLBData(i).BeginEdit()
                            drLBData(i).Item("LEAGUE_ID") = LeagueId
                            drLBData(i).EndEdit()
                        Next

                        Dim drLBDEP() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Select("LEAGUE_ID = 39 AND COVERAGE_LEVEL = 4")
                        For i As Integer = 0 To drLBDEP.Length - 1
                            drLBDEP(i).BeginEdit()
                            drLBDEP(i).Item("LEAGUE_ID") = LeagueId
                            drLBDEP(i).EndEdit()
                        Next
                    ElseIf CovergaLevel = 3 Then
                        Dim drLB() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Select("LEAGUE_ID = 50 AND COVERAGE_LEVEL = 3")
                        Dim dtLB As DataTable
                        dtLB = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Clone
                        For Each drs As DataRow In drLB
                            dtLB.ImportRow(drs)
                        Next
                        For i As Integer = 0 To dtLB.Rows.Count - 1
                            dtLB.Rows(i).BeginEdit()
                            dtLB.Rows(i).Item("LEAGUE_ID") = LeagueId
                            dtLB.Rows(i).EndEdit()
                        Next
                        m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Merge(dtLB)
                        dtLB.Dispose()

                        Dim drLBData() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Select("LEAGUE_ID = 50 AND COVERAGE_LEVEL = 3")
                        Dim dtLBData As DataTable
                        dtLBData = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Clone()
                        For Each drs As DataRow In drLBData
                            dtLBData.ImportRow(drs)
                        Next
                        For i As Integer = 0 To dtLBData.Rows.Count - 1
                            dtLBData.Rows(i).BeginEdit()
                            dtLBData.Rows(i).Item("LEAGUE_ID") = LeagueId
                            dtLBData.Rows(i).EndEdit()
                        Next
                        m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Merge(dtLBData)
                        dtLBData.Dispose()

                        Dim drLBDEP() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Select("LEAGUE_ID = 50 AND COVERAGE_LEVEL = 3")
                        Dim dtLBDEP As DataTable
                        dtLBDEP = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Clone()
                        For Each drs As DataRow In drLBDEP
                            dtLBDEP.ImportRow(drs)
                        Next

                        For i As Integer = 0 To dtLBDEP.Rows.Count - 1
                            dtLBDEP.Rows(i).BeginEdit()
                            dtLBDEP.Rows(i).Item("LEAGUE_ID") = LeagueId
                            dtLBDEP.Rows(i).EndEdit()
                        Next
                        m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Merge(dtLBDEP)
                        dtLBDEP.Dispose()

                    ElseIf CovergaLevel = 2 Then

                        Dim drLB() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Select("LEAGUE_ID = 70 AND COVERAGE_LEVEL = 2")
                        Dim dtLB As DataTable
                        dtLB = m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Clone
                        For Each drs As DataRow In drLB
                            dtLB.ImportRow(drs)
                        Next
                        For i As Integer = 0 To dtLB.Rows.Count - 1
                            dtLB.Rows(i).BeginEdit()
                            dtLB.Rows(i).Item("LEAGUE_ID") = LeagueId
                            dtLB.Rows(i).EndEdit()
                        Next
                        m_dsReporterData.Tables("SOC_EVENT_LB_CURSOR").Merge(dtLB)
                        dtLB.Dispose()


                        Dim drLBData() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Select("LEAGUE_ID = 70 AND COVERAGE_LEVEL = 2")
                        Dim dtLBData As DataTable
                        dtLBData = m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Clone()
                        For Each drs As DataRow In drLBData
                            dtLBData.ImportRow(drs)
                        Next
                        For i As Integer = 0 To dtLBData.Rows.Count - 1
                            dtLBData.Rows(i).BeginEdit()
                            dtLBData.Rows(i).Item("LEAGUE_ID") = LeagueId
                            dtLBData.Rows(i).EndEdit()
                        Next
                        m_dsReporterData.Tables("SOC_EVENT_LB_DATA_CURSOR").Merge(dtLBData)
                        dtLBData.Dispose()

                        Dim drLBDEP() As DataRow = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Select("LEAGUE_ID = 70 AND COVERAGE_LEVEL = 2")
                        Dim dtLBDEP As DataTable
                        dtLBDEP = m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Clone()
                        For Each drs As DataRow In drLBDEP
                            dtLBDEP.ImportRow(drs)
                        Next

                        For i As Integer = 0 To dtLBDEP.Rows.Count - 1
                            dtLBDEP.Rows(i).BeginEdit()
                            dtLBDEP.Rows(i).Item("LEAGUE_ID") = LeagueId
                            dtLBDEP.Rows(i).EndEdit()
                        Next
                        m_dsReporterData.Tables("SOC_EVENT_LB_DEP_CURSOR").Merge(dtLBDEP)
                        dtLBDEP.Dispose()
                    End If
                    m_dsReporterData.AcceptChanges()
                End If

                dsTeamLogo.Tables.Add(m_dsReporterData.Tables("TEAM_LOGO").Copy)
                dsUniformImages.Tables.Add(m_dsReporterData.Tables("SOC_UNIFORMIMAGE").Copy)

                m_dsReporterData.Tables.Remove("TEAM_LOGO")
                m_dsReporterData.Tables.Remove("SOC_UNIFORMIMAGE")

                strXmlSoccerData = m_dsReporterData.GetXml()
                m_dsReporterData.ReadXml(New System.IO.StringReader(iso8859_unicode(strXmlSoccerData)))

                'DELETE ALL TABLE DATA BEFORE INSERT
                m_objDataAccess.DeleteAllTableData()
                m_objDataAccess.InsertDownLoaddata(strXmlSoccerData, LanguageID, 0)

                FillTeamLogo(dsTeamLogo)

                If dsUniformImages IsNot Nothing Then
                    If dsUniformImages.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To dsUniformImages.Tables(0).Rows.Count - 1
                            If dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR") IsNot DBNull.Value Then
                                If Not dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR").ToString.Contains("#") Then
                                    dsUniformImages.Tables(0).Rows(i).BeginEdit()
                                    dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR") = "#" & dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR").ToString.ToUpper()
                                    dsUniformImages.Tables(0).Rows(i).EndEdit()
                                End If
                            End If

                            If dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR") IsNot DBNull.Value Then
                                If Not dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR").ToString.Contains("#") Then
                                    dsUniformImages.Tables(0).Rows(i).BeginEdit()
                                    dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR") = "#" & dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR").ToString.ToUpper()
                                    dsUniformImages.Tables(0).Rows(i).EndEdit()
                                End If
                            End If
                        Next
                        dsUniformImages.Tables(0).AcceptChanges()
                    End If
                End If
                FillUniformImages(dsUniformImages)
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertReporterData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterID As Integer, ByVal CoumputerID As String, ByVal ReporterName As String) As Boolean
        Try
            m_objWebSoc._objWebSoc.InsertReporterData(GameCode, FeedNumber, ReporterID, CoumputerID, ReporterName, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Function DeleteReporterData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterID As Integer, ByVal CoumputerID As String) As Boolean
        Try
            m_objWebSoc._objWebSoc.DeleteReporterData(GameCode, FeedNumber, ReporterID, CoumputerID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Function GetReporterData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterID As Integer, ByVal CoumputerID As String) As DataSet
        Try
            Return m_objWebSoc._objWebSoc.GetReporterData(GameCode, FeedNumber, ReporterID, CoumputerID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DownloadSoccermultilingualData(ByVal LanguageID As Integer, ByVal ModuleID As Integer, ByVal CoverageLevel As Integer, ByVal Gamecode As String, ByVal ReporterID As Integer) As Boolean
        Dim intDaysFrom, intDaysTo As Integer
        Dim strXmlSoccerData As String
        Dim dsUniformImages As New DataSet
        Dim dsTeamLogo As New DataSet
        Try
            intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            m_objWebSoc._objWebSoc.Timeout = 800000
            m_objWebSoc._objWebSoc.PreAuthenticate = True


            'm_dsReporterData = m_objWebSoc.ImportSoccerData(ReporterdID, LanguageID, intDaysFrom, intDaysTo, CStr(GameCode), FeedNumber, ModuleID, ReporterRole)
            m_dsReporterData = GZipHelper.Decompress(m_objWebSoc._objWebSoc.ImportSoccerDatamultilingual(LanguageID, intDaysFrom, intDaysTo, ModuleID, CoverageLevel, Gamecode, ReporterID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken")))
            If m_dsReporterData.Tables.Count > 0 Then
                m_dsReporterData.Tables("COMMENT_XLATE").Rows.Clear()
                strXmlSoccerData = m_dsReporterData.GetXml()
                m_dsReporterData.ReadXml(New System.IO.StringReader(iso8859_unicode(strXmlSoccerData)))
                'DELETE ALL TABLE DATA BEFORE INSERT
                m_objDataAccess.InsertDownMultilingualdata(strXmlSoccerData, LanguageID)
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Fills TeamLogo table - GLB_TEAM_LOGO
    ''' </summary>
    ''' <param name="TeamLogo">A Dataset which contains TeamLogo data</param>
    ''' <remarks></remarks>
    Private Sub FillTeamLogo(ByVal TeamLogo As DataSet)
        Try
            m_objDataAccess.FillTeamLogo(TeamLogo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Gets the gamedetails assigned to the reporter
    ''' </summary>
    ''' <param name="LanguageID">An Integer represents the LanguageID</param>
    ''' <param name="UserType">An Integer represetns the UserType</param>
    ''' <returns>A Dataset contains Gamedetails</returns>
    ''' <remarks>UserType is 0 for "REPORTER"</remarks>
    ''' <remarks1>UserType is 1 for "OPS"</remarks1>
    Public Function GetGameDetails(ByVal LanguageID As Integer, ByVal UserType As Integer) As DataSet
        m_dsGameDetails = m_objDataAccess.GetGameDetails(LanguageID, UserType)
        Return m_dsGameDetails
    End Function

    ''' <summary>
    ''' To insert current game data
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <param name="intReporterID"></param>
    ''' <param name="strReporterRole"></param>
    ''' <remarks></remarks>
    Public Sub InsertCurrentGame(ByVal intGameCode As Int32, ByVal intOldGameCode As Integer, ByVal intFeedNumber As Int32, ByVal intReporterID As Int32, ByVal strReporterRole As String, ByVal intModuleID As Integer, ByVal TimeElapsed As Integer, ByVal SysLocalTime As String, ByVal Type As String)
        Try
            m_objDataAccess.InsertCurrentGame(intGameCode, intOldGameCode, intFeedNumber, intReporterID, strReporterRole, intModuleID, TimeElapsed, SysLocalTime, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' INSERTING MODULE 2 MULTPLE GAMES INTO CURRENT GAME TABLE
    ''' </summary>
    ''' <param name="Ds"></param>
    ''' <param name="intGamecode"></param>
    ''' <param name="ReporterID"></param>
    ''' <remarks></remarks>
    Public Sub InsertMultipleGames(ByVal Ds As DataSet, ByVal intGamecode As Integer, ByVal ReporterID As Integer)
        Try
            Dim strXML As String = ""
            Ds.DataSetName = "Mod2Games"
            Ds.Tables(0).TableName = "MultipleGames"
            'NEW COLUMNS ARE ADDED WHICH IS UESED FOR THE XML TO READ THE DATA EASILY
            Ds.Tables("MultipleGames").Columns.Add("REPORTER_ROLECODE")
            Ds.Tables("MultipleGames").Columns.Add("REPORTER_ID")
            Ds.Tables("MultipleGames").Columns.Add("ACTIVE_GAME")
            Ds.Tables("MultipleGames").Columns.Add("DEMO_DATA")
            'ASSIGNING THE VALUES
            For intRow As Integer = 0 To Ds.Tables("MultipleGames").Rows.Count - 1
                Ds.Tables("MultipleGames").Rows(intRow).Item("REPORTER_ROLECODE") = Ds.Tables("MultipleGames").Rows(intRow).Item("REPORTER_ROLE").ToString + Ds.Tables("MultipleGames").Rows(intRow).Item("SERIAL_NO").ToString
                Ds.Tables("MultipleGames").Rows(intRow).Item("REPORTER_ID") = ReporterID
                Ds.Tables("MultipleGames").Rows(intRow).Item("DEMO_DATA") = "N"
                If CInt(Ds.Tables("MultipleGames").Rows(intRow).Item("GAME_CODE")) = intGamecode Then
                    'THE GAME SELECTED FROM SELECT GAME IS GIVE ACTIVE STATUS "Y"
                    Ds.Tables("MultipleGames").Rows(intRow).Item("ACTIVE_GAME") = "Y"
                Else
                    'OTHER GAMES STATUS ARE NOT ACTIVE
                    Ds.Tables("MultipleGames").Rows(intRow).Item("ACTIVE_GAME") = "N"
                End If
            Next
            'CONVERTING INTO XML
            strXML = Ds.GetXml()
            strXML = strXML.Replace("'", "''")
            'INSERTING THE XML DATA INTO CURRENT_GAME SQL TABLE
            m_objDataAccess.InsertMultipleGames(strXML)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHES THE MODULE 2 GAMES ASSIGNED TO THE REPORTER SPECIFIED WITHIN +/- 24 HRS
    ''' </summary>
    ''' <param name="LanguageID"></param>
    ''' <param name="UserType"></param>
    ''' <param name="GameDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMultipleGameDetails(ByVal LanguageID As Integer, ByVal UserType As Integer, ByVal GameDate As Date) As DataSet
        Try
            Dim m_dsGameDetails As DataSet
            m_dsGameDetails = m_objDataAccess.GetMultipleGameDetails(LanguageID, UserType, GameDate)
            Return m_dsGameDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteCurrentGameData(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Boolean
        Try
            m_objWebSoc._objWebSoc.DeleteCurrentGameData(GameCode, FeedNumber, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            m_objDataAccess.DeleteLiveTables(GameCode, FeedNumber)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    ''' <summary>
    ''' GETS THE COUNT OF DATA FROM LOCAL SQL FOR THE SELECTED GAME DURING RESTART
    ''' MAINLY FOR T1 WORKFLOW
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="ModuleId"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Function GetLocalSqlDataCountForRestart(ByVal GameCode As String, ByVal FeedNumber As Integer, ByVal ModuleId As Integer) As DataSet
        Try
            Return m_objDataAccess.GetLocalSQLDataCountForRestart(GameCode, FeedNumber, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetLocalSqlDataForRestart(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ModuleId As Integer) As DataSet
        Try
            Return m_objDataAccess.GetLocalSQLDataForRestart(GameCode, FeedNumber, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Fills TeamLogo table - SOCCER_UNIFORM_IMAGES
    ''' </summary>
    ''' <param name="UniformImages">A Dataset which contains UniformImages data</param>
    ''' <remarks></remarks>
    Private Sub FillUniformImages(ByVal UniformImages As DataSet)
        Try
            m_objDataAccess.FillUniformImages(UniformImages)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetFeedCoverageInfo(ByVal GameCode As Integer, ByVal ReporterID As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objDataAccess.GetFeedCoverageInfo(GameCode, ReporterID, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetServerName() As String
        Try
            Dim ServerPath As String = My.MySettings.[Default]("STATS_SoccerBL_SoccerWS_Service").ToString()
            Return ServerPath
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 'Insert a dashboard alert once the reporter has logged-in and seleted the game
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="LeagueID"></param>
    ''' <param name="ReporterID"></param>
    ''' <param name="ReporterRole"></param>
    ''' <remarks></remarks>
    Public Sub InsertDashboardAlert(ByVal GameCode As Integer, ByVal LeagueID As Integer, ByVal ReporterID As Integer, ByVal ReporterRole As String)
        Try
            m_objWebSoc._objWebSoc.InsertDashboardAlert(GameCode, LeagueID, ReporterID, ReporterRole, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub DeleteReporterExistance(ByVal ReporterID As Integer)
        Try
            m_objWebSoc._objWebSoc.DeleteReporterExistance(ReporterID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'FPLRS-139	
    Public Function GetSqlLanguage() As DataSet
        Try
            Return m_objDataAccess.GetSqlLanguage()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ValidateReporter(ByVal LoginId As String) As Integer
        Try
            Return m_objDataAccess.ValidateReporter(LoginId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetReporterDetails(ByVal LoginId As String) As DataSet
        Try
            Return m_objDataAccess.GetReporterDetails(LoginId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetGameDetails() As DataSet
        Try
            Return m_objDataAccess.GetGameDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region " DEMO "
    ''' <summary>
    ''' Downloads the Soccer Demo Data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DownloadSoccerDemoData() As Boolean
        Dim strXmlSoccerData As String
        Try
            m_objWebSoc._objWebSoc.Timeout = 400000
            m_objWebSoc._objWebSoc.PreAuthenticate = True
            m_dsReporterData = m_objWebSoc._objWebSoc.ImportSoccerDemoData(System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            If m_dsReporterData.Tables.Count > 0 Then
                strXmlSoccerData = m_dsReporterData.GetXml()
                strXmlSoccerData = strXmlSoccerData.Replace("'", "''")
                'DELETE ALL TABLE DATA BEFORE INSERT
                m_objDataAccess.DeleteAllTableData()
                'INSERTION OF SAMPLE GAME DATA
                m_objDataAccess.InsertDemoData(strXmlSoccerData)
                InsertSampleGame()
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetSamepleGameDisplay() As DataSet
        Try
            Return m_objDataAccess.GetSampleGameDisplay()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertSampleGame() As DataSet
        Try
            Return m_objDataAccess.InsertSampleGame()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class
