﻿
#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsTeamStats
' Author        : Shravani
' Created Date  : 07-AUG-09
' Description   : Business login for Team Statistics
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class clsPlayerTouches

#Region " Constatnts and Variable "
    Shared m_myInstance As clsPlayerTouches
    Private m_objPlayerTouches As SoccerDAL.clsPlayerTouches = SoccerDAL.clsPlayerTouches.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsPlayerTouches
        If m_myInstance Is Nothing Then
            m_myInstance = New clsPlayerTouches
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Defined Functions"
    ''' <summary>
    ''' GETS GAME STATISTICS
    ''' </summary>
    ''' <param name="GameCode">An integer represents GameCode</param>
    ''' <param name="FeedNumber">An integer represents Feeednumber</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPlayerTouches(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal langauge_id As Integer, ByVal serial_no As Integer, ByVal hTeam As Integer, ByVal aTeam As Integer, ByVal Period As Integer) As DataSet
        Try
            Return m_objPlayerTouches.GetPlayerTouches(GameCode, FeedNumber, langauge_id, serial_no, hTeam, aTeam, Period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' GETS GAME STATISTICS
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTouchTypes() As DataSet
        Try
            Return m_objPlayerTouches.GetTouchTypes()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' GETS GAME STATISTICS
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRosters(ByVal GameCode As Integer, ByVal feedNumber As Integer, ByVal teamID As Integer) As DataSet
        Try
            Return m_objPlayerTouches.GetRosters(GameCode, feedNumber, teamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="feedNumber"></param>
    ''' <remarks></remarks>
    Public Sub InsertPlayerTouchestoSummary(ByVal GameCode As Integer, ByVal feedNumber As Integer, ByVal ModuleID As Integer)
        Try
            m_objPlayerTouches.InsertPlayerTouchestoSummary(GameCode, feedNumber, ModuleID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
