﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsSelectGame
' Author        : Shravani
' Created Date  : 01-DEC-2009
' Description   : Business Logic for Selecting game for OPS
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsSelectGame

#Region " Constants & Variables "
    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Private m_objDataAccess As New STATS.SoccerDAL.clsSelectGame
    Private m_dsUserDetails As New DataSet
    Shared Instance As clsSelectGame

    Private m_dsLanguage As New DataSet
    Private m_dsReporterData As New DataSet
    Private m_dsTeamLogo As New DataSet
    Private m_dsCoachSeason As New DataSet
    Private m_dsSocReferee As New DataSet
#End Region

    Public Shared Function GetInstance() As clsSelectGame
        If Instance Is Nothing Then
            Instance = New clsSelectGame
        End If
        Return Instance
    End Function

    Public Function ValidateUserDetails(ByVal UserName As String, ByVal Password As String, ByVal langaue_id As Integer) As DataSet
        Dim intDaysFrom, intDaysTo As Integer
        Try
            intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            m_dsUserDetails = m_objWebSoc._objWebSoc.GetUserInfo(UserName, Password, intDaysFrom, intDaysTo, langaue_id, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
            If (m_dsUserDetails.Tables.Count > 0) Then
                m_dsUserDetails.Tables(0).TableName = "UserDetails"
                m_dsUserDetails.Tables(1).TableName = "Status"
                m_dsUserDetails.Tables(2).TableName = "Games"
            End If
            Return m_dsUserDetails
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'INSERTION OF CURRENT GAME IN DATABASE ONCE THE GAME IS SELECTED
    Public Function GetAllLeagues() As DataSet
        Try
            Return m_objWebSoc._objWebSoc.GetLeaguedata(System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLocalSqlDataCountForRestart(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ModuleId As Integer) As DataSet
        Try
            Return m_objDataAccess.GetLocalSQLDataCountForRestart(GameCode, FeedNumber, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetLocalSqlDataForRestart(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ModuleId As Integer) As DataSet
        Try
            Return m_objDataAccess.GetLocalSQLDataForRestart(GameCode, FeedNumber, ModuleId)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub InsertCurrentGame(ByVal intGameCode As Int32, ByVal intOldGameCode As Integer, ByVal intFeedNumber As Int32, ByVal intReporterID As Int32, ByVal strReporterRole As String, ByVal intModuleID As Integer, ByVal TimeElapsed As Integer, ByVal SysLocalTime As String, ByVal Type As String)
        Try
            m_objDataAccess.InsertCurrentGame(intGameCode, intOldGameCode, intFeedNumber, intReporterID, strReporterRole, intModuleID, TimeElapsed, SysLocalTime, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function FetchLclGameTable(ByVal intGameCode As Int32, ByVal intFeedNumber As Integer) As DataSet
        Try
            Return m_objDataAccess.FetchLclGameTableT1(intGameCode, intFeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Shared Function iso8859_unicode(ByVal src As String) As String
        Dim iso As Encoding = Encoding.GetEncoding("iso8859-1")
        Dim unicode As Encoding = Encoding.UTF8
        Dim isoBytes As Byte() = iso.GetBytes(src)
        Return unicode.GetString(isoBytes)
    End Function
    Public Function DownloadSoccerData(ByVal ReporterdID As Integer, ByVal LanguageID As Integer, ByVal GameCode As String, ByVal FeedNumber As Integer, ByVal ModuleID As Integer, ByVal ReporterRole As String, ByVal CovergaLevel As Integer, ByVal LeagueId As Integer) As Boolean
        Dim intDaysFrom, intDaysTo As Integer
        Dim strXmlSoccerData As String
        Dim dsUniformImages As New DataSet
        Dim dsTeamLogo As New DataSet
        Dim ReporterSerialNo? As Integer
        Try
            intDaysFrom = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysFrom"))
            intDaysTo = CInt(System.Configuration.ConfigurationManager.AppSettings("DaysTo"))
            ReporterSerialNo = CType(ReporterRole.Substring(ReporterRole.Length - 1), Integer?)
            m_objWebSoc._objWebSoc.Timeout = 800000
            m_objWebSoc._objWebSoc.PreAuthenticate = True
            m_dsReporterData = GZipHelper.Decompress(m_objWebSoc._objWebSoc.ImportSoccerData(ReporterdID, LanguageID, intDaysFrom, intDaysTo, CStr(GameCode), FeedNumber, ModuleID, ReporterRole, System.Configuration.ConfigurationManager.AppSettings("SecurityToken")))
            If m_dsReporterData.Tables.Count > 0 Then

                If ModuleID = 1 Then
                    m_dsReporterData.Tables("LIVE_TOUCHES").Rows.Clear()
                    m_dsReporterData.Tables("LIVE_COMMENTARY").Rows.Clear()
                ElseIf ModuleID = 3 Then
                    m_dsReporterData.Tables("LIVE_COMMENTARY").Rows.Clear()
                    m_dsReporterData.Tables("PBP").Rows.Clear()
                ElseIf ModuleID = 4 Then
                    m_dsReporterData.Tables("PBP").Rows.Clear()
                    m_dsReporterData.Tables("LIVE_TOUCHES").Rows.Clear()
                End If
                dsTeamLogo.Tables.Add(m_dsReporterData.Tables("TEAM_LOGO").Copy)
                dsUniformImages.Tables.Add(m_dsReporterData.Tables("SOC_UNIFORMIMAGE").Copy)

                m_dsReporterData.Tables.Remove("TEAM_LOGO")
                m_dsReporterData.Tables.Remove("SOC_UNIFORMIMAGE")

                strXmlSoccerData = m_dsReporterData.GetXml()
                m_dsReporterData.ReadXml(New System.IO.StringReader(iso8859_unicode(strXmlSoccerData)))
                'DELETE ALL TABLE DATA BEFORE INSERT
                m_objDataAccess.DeleteAllTableData()
                m_objDataAccess.InsertDownLoaddata(strXmlSoccerData, LanguageID, CInt(ReporterSerialNo))

                FillTeamLogo(dsTeamLogo)
                If dsUniformImages IsNot Nothing Then
                    If dsUniformImages.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To dsUniformImages.Tables(0).Rows.Count - 1
                            If dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR") IsNot DBNull.Value Then
                                dsUniformImages.Tables(0).Rows(i).BeginEdit()
                                dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR") = "#" & dsUniformImages.Tables(0).Rows(i).Item("SHIRT_COLOR").ToString.ToUpper()
                                dsUniformImages.Tables(0).Rows(i).EndEdit()
                            End If

                            If dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR") IsNot DBNull.Value Then
                                dsUniformImages.Tables(0).Rows(i).BeginEdit()
                                dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR") = "#" & dsUniformImages.Tables(0).Rows(i).Item("SHORT_COLOR").ToString.ToUpper()
                                dsUniformImages.Tables(0).Rows(i).EndEdit()
                            End If
                        Next
                        dsUniformImages.Tables(0).AcceptChanges()
                    End If
                End If
                FillUniformImages(dsUniformImages)
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Fills TeamLogo table - GLB_TEAM_LOGO
    ''' </summary>
    ''' <param name="TeamLogo">A Dataset which contains TeamLogo data</param>
    ''' <remarks></remarks>
    Private Sub FillTeamLogo(ByVal TeamLogo As DataSet)
        Try
            m_objDataAccess.FillTeamLogo(TeamLogo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FillUniformImages(ByVal UniformImages As DataSet)
        Try
            m_objDataAccess.FillUniformImages(UniformImages)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' INSERTING MODULE 2 MULTPLE GAMES INTO CURRENT GAME TABLE
    ''' </summary>
    ''' <param name="Ds"></param>
    ''' <param name="intGamecode"></param>
    ''' <param name="ReporterID"></param>
    ''' <remarks></remarks>
    Public Sub InsertMultipleGames(ByVal Ds As DataSet, ByVal intGamecode As Integer, ByVal ReporterID As Integer)
        Try
            Dim strXML As String = ""
            Ds.DataSetName = "Mod2Games"
            Ds.Tables(0).TableName = "MultipleGames"
            'NEW COLUMNS ARE ADDED WHICH IS UESED FOR THE XML TO READ THE DATA EASILY
            Ds.Tables("MultipleGames").Columns.Add("REPORTER_ROLECODE")
            Ds.Tables("MultipleGames").Columns.Add("REPORTER_ID")
            Ds.Tables("MultipleGames").Columns.Add("ACTIVE_GAME")
            Ds.Tables("MultipleGames").Columns.Add("DEMO_DATA")
            'ASSIGNING THE VALUES 
            For intRow As Integer = 0 To Ds.Tables("MultipleGames").Rows.Count - 1
                Ds.Tables("MultipleGames").Rows(intRow).Item("REPORTER_ROLECODE") = Ds.Tables("MultipleGames").Rows(intRow).Item("REPORTER_ROLE").ToString + Ds.Tables("MultipleGames").Rows(intRow).Item("SERIAL_NO").ToString
                Ds.Tables("MultipleGames").Rows(intRow).Item("REPORTER_ID") = ReporterID
                Ds.Tables("MultipleGames").Rows(intRow).Item("DEMO_DATA") = "N"
                If CInt(Ds.Tables("MultipleGames").Rows(intRow).Item("GAME_CODE")) = intGamecode Then
                    'THE GAME SELECTED FROM SELECT GAME IS GIVE ACTIVE STATUS "Y"
                    Ds.Tables("MultipleGames").Rows(intRow).Item("ACTIVE_GAME") = "Y"
                Else
                    'OTHER GAMES STATUS ARE NOT ACTIVE
                    Ds.Tables("MultipleGames").Rows(intRow).Item("ACTIVE_GAME") = "N"
                End If
            Next
            'CONVERTING INTO XML
            strXML = Ds.GetXml()
            strXML = strXML.Replace("'", "''")
            'INSERTING THE XML DATA INTO CURRENT_GAME SQL TABLE
            m_objDataAccess.InsertMultipleGames(strXML)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Public Function GetGameDetails(ByVal LanguageID As Integer, ByVal UserType As Integer) As DataSet
    '    Dim m_dsGameDetails As New DataSet
    '    m_dsGameDetails = m_objDataAccess.GetGameDetails(LanguageID, UserType)
    '    Return m_dsGameDetails
    'End Function

    Public Function GetFeedCoverageInfo(ByVal GameCode As Integer, ByVal ReporterID As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objDataAccess.GetFeedCoverageInfo(GameCode, ReporterID, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
