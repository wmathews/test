﻿
#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsTeamSetup
' Author        : Shirley Ranjini
' Created Date  : 11th May,2009
' Description   : Business Logic for TeamSetup Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsSubFormations
    Shared m_myInstance As clsSubFormations
    Private m_objFormations As STATS.SoccerDAL.clsSubFormations = STATS.SoccerDAL.clsSubFormations.GetAccess()

#Region " Shared methods "
    Public Shared Function GetInstance() As clsSubFormations
        If m_myInstance Is Nothing Then
            m_myInstance = New clsSubFormations
        End If
        Return m_myInstance
    End Function
#End Region

    Public Function GetFormationCoach(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer) As DataSet
        Try
            Return m_objFormations.GetFormationCoach(GameCode, FeedNumber, TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetFormationSpecification(ByVal FormationID As Integer, ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objFormations.GetFormationSpecification(FormationID, LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetOnfieldValue(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal IntTeamID As Integer, ByVal intPeriod As Integer) As DataSet
        Try
            Return m_objFormations.GetOnfieldValue(GameCode, FeedNumber, IntTeamID, intPeriod)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
