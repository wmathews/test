﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsComments
' Author        : Shravani
' Created Date  : 15th July,2009
' Description   : Business Logic for Comments Screen
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsComments

#Region " Constants & Variables "
    Shared Instance As clsComments
    Private m_objClsComments As STATS.SoccerDAL.clsComments = STATS.SoccerDAL.clsComments.GetAccess()

#End Region

#Region " User Methods "

    Public Shared Function GetInstance() As clsComments
        If Instance Is Nothing Then
            Instance = New clsComments
        End If
        Return Instance
    End Function

    'GetPBPData
    Public Function GetPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Integer) As DataSet
        Try
            Return m_objClsComments.GetPBPData(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objClsComments.GetMaxTimeElapsed(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetLanguage() As DataSet
        Try
            Return m_objClsComments.GetLanguage("SELECT LANGUAGE_ID, LANGUAGE_DESC FROM GLB_LANGUAGE WHERE COMMENTS_LANG = 'T' ")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPredefinedComments(ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objClsComments.GetPredefinedComments("SELECT COMMENT_ID, COMMENT_DESC FROM SOCCER_COMMENT_XLATE WHERE LANGUAGE_ID = " & LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPredefinedCommentsforDelay(ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objClsComments.GetPredefinedComments("SELECT COMMENT_ID, COMMENT_DESC FROM SOCCER_COMMENT_XLATE WHERE COMMENT_ID IN (14,15,16,17,18) AND LANGUAGE_ID = " & LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function getDelayData(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objClsComments.getDelayData(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function getGameSetupData(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Return m_objClsComments.getGameSetupData(GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function updateDelayTime(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal dateTim As Date) As DataSet
        Try
            Return m_objClsComments.updateDelayTime(GameCode, FeedNumber, dateTim)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertUpdateGameSetupDelay(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal dateTim As Date) As DataSet
        Try
            Return m_objClsComments.updateDelayTime(GameCode, FeedNumber, dateTim)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertUpdateGameSetupDelay(ByVal GameSetup As DataSet) As Integer
        Try
            Dim strGameSetUpXmal As String = GameSetup.GetXml()
            Return m_objClsComments.InsertUpdateGameSetupDelay(strGameSetUpXmal)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region


End Class
