﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region


#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsHomeStart
' Author        : Shravani
' Created Date  : 07th Jan 2010
' Description   : 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsHomeStart
#Region "Constants & Variables"
    Shared m_myInstance As clsHomeStart
    Private m_objHomeStart As SoccerDAL.clsHomeStart = SoccerDAL.clsHomeStart.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsHomeStart
        If m_myInstance Is Nothing Then
            m_myInstance = New clsHomeStart
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Functions"

    ''' <summary>
    ''' Inserts or Updates the Gamesetup info (HomeStatr)
    ''' </summary>
    ''' <param name="GameSetup"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertUpdateGameSetup(ByVal gamecode As Integer, ByVal feednumber As Integer, ByVal leagueId As Integer, ByVal HomeStart As String, ByVal HomeStartET As String) As Integer
        Try
            Return m_objHomeStart.InsertUpdateHomeStart(gamecode, feednumber, leagueId, HomeStart, HomeStartET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
End Class
