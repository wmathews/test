﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsModule1PBP
' Author        : Ravi Krishna
' Created Date  : 18-05-09
' Description   : Class used for Business Logic of Module1 PBP Screen
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsModule1PBP

#Region "Constants & Variables"
    Shared m_myInstance As clsModule1PBP
    Private m_objModule1PBP As SoccerDAL.clsModule1PBP = SoccerDAL.clsModule1PBP.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsModule1PBP
        If m_myInstance Is Nothing Then
            m_myInstance = New clsModule1PBP
        End If
        Return m_myInstance
    End Function
#End Region

#Region "User Functions"

    Public Sub AddPBP()

    End Sub

    Public Function GetUniqueID(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objModule1PBP.GetUniqueID(GameCode, FeedNumber)
    End Function

    Public Function GetTableCount(ByVal TableName As String, ByVal ColumnName As String) As Integer

        Try
            If (TableName = "") Or (ColumnName = "") Then
                Return -1
            Else
                Return m_objModule1PBP.GetTableCount(TableName, ColumnName)
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetSortOrder(ByVal TableName As String, ByVal ColumnName As String, ByVal ColumnValue As Integer) As Integer

        Try
            If (TableName = "") Or (ColumnName = "") Then
                Return -1
            Else
                Return m_objModule1PBP.GetSortOrder(TableName, ColumnName, ColumnValue)
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetMaxEventID() As Integer
        Return m_objModule1PBP.GetMaxEventID()
    End Function

    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objModule1PBP.GetMaxTimeElapsed(GameCode, FeedNumber)
    End Function

    Public Function GetMaxTimeElapsedMain(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objModule1PBP.GetMaxTimeElapsed(GameCode, FeedNumber)
    End Function

    Public Function checkForHalfTimeEvents(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal) As Integer
        Return m_objModule1PBP.checkForHalfTimeEvents(GameCode, FeedNumber, Sequence_Number)
    End Function


    Public Function GetLastEventDetails(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objModule1PBP.GetLastEventDetails(GameCode, FeedNumber)
    End Function


    Public Function GetScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Return m_objModule1PBP.GetScores(GameCode, FeedNumber)
    End Function

    Public Function InsertPBPDataToSQL(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal IsTimeEdited As Boolean, ByVal IsInsertBefore As Boolean, ByVal m_decInsertSeqNum As Decimal, ByVal IsFoulAssociated As Boolean, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule1PBP.InsertPBPDataToSQL(PBPXmlData, GameCode, FeedNumber, ReporterRole, IsTimeEdited, IsInsertBefore, m_decInsertSeqNum, IsFoulAssociated, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateSubstitutePlayer(ByVal LeagueID As Integer, ByVal PlayerInID As Integer, ByVal PlayerOutID As Integer) As Integer
        Try
            Return m_objModule1PBP.UpdateSubstitutePlayer(LeagueID, PlayerInID, PlayerOutID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>SHIRLEY
    ''' FETCHING PBP RECORDS BASED ON SEQUENCE NUMBER FROM DATABASE WHICH IS USED FOR EDITING PURPOSE
    ''' </summary> 
    ''' <param name="GameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <param name="Sequence_Number"></param> ' SELECTED FROM THE LISTBOX OF MAIN SCREEN
    ''' <param name="Type"></param> ' WHICH QUERY TO GET EXECUTED (EX : EQUAL,LESS THAN ETC)
    ''' <returns></returns>
    ''' <remarks></remarks> 'A DATASET WHICH CONTAINS THE SELECTED EVENT PBP RECORD
    Public Function GetPBPRecordBasedOnSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objModule1PBP.GetPBPRecordBasedOnSeqNumber(GameCode, FeedNumber, Sequence_Number, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Public Function GetPBPRecordBasedOnTime(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ElapsedTime As Integer, ByVal Period As Integer, ByVal Type As String) As DataSet
        Try
            Return m_objModule1PBP.GetPBPRecordBasedOnTime(GameCode, FeedNumber, ElapsedTime, Period, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    'GetPBPData
    Public Function GetPBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule1PBP.GetPBPData(GameCode, FeedNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function getArea(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SeqNum As Decimal) As DataSet
        Try
            Return m_objModule1PBP.getArea(GameCode, FeedNumber, SeqNum)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Deletes the selected event 
    ''' </summary>
    ''' <param name="SequenceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeletePBPData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule1PBP.DeletePBPData(GameCode, FeedNumber, ReporterRole, SequenceNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Deletes the selected event 
    ''' </summary>
    ''' <param name="SequenceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteBookings(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule1PBP.DeleteBookings(GameCode, FeedNumber, ReporterRole, SequenceNumber, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Deletes the selected event 
    ''' </summary>
    ''' <param name="SequenceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPrevSeq(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objModule1PBP.GetPrevSeq(GameCode, FeedNumber, SequenceNumber, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPlayerCount(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal PlayerID As Integer) As Integer
        Try
            Return m_objModule1PBP.GetPlayerCount(GameCode, FeedNumber, SequenceNumber, PlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPlayerForEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal EventID As Integer, ByVal PlayerID As Integer) As Integer
        Try
            Return m_objModule1PBP.GetPlayerForEvent(GameCode, FeedNumber, EventID, PlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Adjusts the socre if the score event is deleted
    ''' </summary>
    ''' <param name="SequenceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteScoreEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As Integer
        Try
            Return m_objModule1PBP.DeleteScoreEvent(GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function EditScoreEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal Type As String) As Integer
        Try
            Return m_objModule1PBP.EditScoreEvent(GameCode, FeedNumber, SequenceNumber, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ReplaceScoreEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As Integer
        Try
            Return m_objModule1PBP.ReplaceScoreEvent(GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Public Function GetTeamIDBasedonSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As DataSet
        Try
            Return m_objModule1PBP.GetTeamIDBasedonSeqNumber(GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FetchSeqNumBasedOnTime(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal timeElapsed As Integer, ByVal Period As Integer, ByVal Type As String) As DataSet
        Try
            Return m_objModule1PBP.FetchSeqNumBasedOnTime(GameCode, FeedNumber, timeElapsed, Period, Type)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function checkEventHalftime(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer, ByVal evtCode As Integer) As DataSet
        Try
            Return m_objModule1PBP.checkEventHalftime(GameCode, FeedNumber, Period, evtCode)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function InsertScoreEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal) As Integer
        Try
            Return m_objModule1PBP.InsertScoreEvent(GameCode, FeedNumber, ReporterRole, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPlayerInOut(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As DataSet
        Try
            Return m_objModule1PBP.GetPlayerInOut(GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetPBPDataForEdit(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As DataSet
        Try
            Return m_objModule1PBP.GetPBPDataForEdit(GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdatePBP(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal) As DataSet
        Try
            Return m_objModule1PBP.UpdatePBPData(PBPXmlData, GameCode, FeedNumber, SequenceNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdatePairedPBPEvents(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal Type As String, ByVal language_id As Int32) As DataSet
        Try
            Return m_objModule1PBP.UpdatePairedPBPEvents(PBPXmlData, GameCode, FeedNumber, ReporterRole, SequenceNumber, Type, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetFormationCoach(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ManagerID As Integer, ByVal Type As String, ByVal langauge_id As Integer) As DataSet
        Try
            Return m_objModule1PBP.GetFormationCoach(GameCode, FeedNumber, ManagerID, Type, langauge_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Fetching Rosters for Home and Away Teams by passing Gamecode and LeagueId as Parameters
    ''' </summary>
    ''' <param name="GameCode"></param>
    ''' <param name="LeagueID"></param>
    ''' <returns>Dataset which Contains Home and Away Team Players</returns>
    ''' <remarks></remarks>
    Public Function GetRosterInfo(ByVal GameCode As Integer, ByVal LeagueID As Integer, ByVal LanguageID As Int32) As DataSet
        Try
            Return m_objModule1PBP.GetRosterInfo(GameCode, LeagueID, "LINEUP", LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' GETS THE EVENTS STORED IN PBP TABLE SO AS TO DISPLAY IN THE MAIN SCREEN.
    ''' EVENTS SUCH AS SUB,BOOKINGS,FOULS ARE FETCHED FROM DATABASE
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="FeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FetchPBPEventsToDisplay(ByVal intGameCode As Integer, ByVal FeedNumber As Integer) As DataSet
        Try
            Dim DsMainEvents As DataSet
            DsMainEvents = m_objModule1PBP.FetchMainScreenEvents(intGameCode, FeedNumber)
            Return DsMainEvents
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function TimeEditOnPBPEvents(ByVal PBPXmlData As String, ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal SequenceNumber As Decimal, ByVal LanguageID As Integer, ByVal IsEventModified As Char) As DataSet
        Try
            Return m_objModule1PBP.TimeEditOnPBPEvents(PBPXmlData, GameCode, FeedNumber, ReporterRole, SequenceNumber, LanguageID, IsEventModified)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetPBPMainEventData(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal SequenceNumber As Decimal, ByVal str As String) As DataSet
        Try
            Return m_objModule1PBP.GetPBPMainEventData(GameCode, FeedNumber, SequenceNumber, str)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetPBPEvents(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Period As Integer, ByVal TimeElapsed As Integer) As Integer
        Try
            Return m_objModule1PBP.GetPBPEvents(GameCode, FeedNumber, Period, TimeElapsed)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetKeyMomentStartSNO(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Key_MomentID As Integer) As DataSet
        Try
            Return m_objModule1PBP.GetKeyMomentStartSNO(GameCode, FeedNumber, Key_MomentID)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetGoalieChangeEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer) As DataSet
        Return m_objModule1PBP.GetGoalieChangeEvent(GameCode, FeedNumber, TeamID)
    End Function

    Public Sub UpdateActiveGK(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer, ByVal langID As Integer)
        Try
            m_objModule1PBP.UpdateActiveGK(GameCode, FeedNumber, TeamID, langID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class
