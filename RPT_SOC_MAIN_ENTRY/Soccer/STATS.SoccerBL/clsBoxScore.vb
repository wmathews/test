﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports System.Data
Imports System.Text
Imports STATS.SoccerDAL
Imports System.Data.SqlClient
#End Region


#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsBoxScore
' Author        : Asif
' Created Date  : 16th july, 2009
' Description   : Business Logic for BoxScore
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region


Public Class clsBoxScore

#Region "Constants & Variables"
    Shared m_myInstance As clsBoxScore
    Private m_objclsBoxScore As STATS.SoccerDAL.clsBoxScore = STATS.SoccerDAL.clsBoxScore.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsBoxScore
        If m_myInstance Is Nothing Then
            m_myInstance = New clsBoxScore
        End If
        Return m_myInstance
    End Function
#End Region


    Public Function GetVenueDetails(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intLeagueID As Int32, ByVal Languageid As Int32) As DataSet
        Try
            Return m_objclsBoxScore.GetVenueDetails(intGameCode, intFeedNumber, intLeagueID, Languageid)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetBoxScoreReport(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objclsBoxScore.GetBoxScoreReport(intGameCode, intFeedNumber, LanguageID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetNewBoxScoreReport(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal LanguageID As Integer, ByVal CoverageLevel As Integer) As DataSet
        Try
            Return m_objclsBoxScore.GetNewBoxScoreReport(intGameCode, intFeedNumber, LanguageID, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPlayerSeconds(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intLeagueID As Int32, ByVal intTeamID As Int32, ByVal intPlayerID As Int32) As Integer
        Try
            Return m_objclsBoxScore.GetPlayerSeconds(intGameCode, intFeedNumber, intLeagueID, intTeamID, intPlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FetchPlayerSubsitutionAndRedCardEvents(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32) As DataSet
        Try
            Return m_objclsBoxScore.FetchSubsitutionAndRedCardEvents(intGameCode, intFeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPenaltyShootoutScores(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal CoverageLevel As Integer) As DataSet
        Try
            Return m_objclsBoxScore.GetPenaltyShootoutScores(GameCode, FeedNumber, CoverageLevel)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class
