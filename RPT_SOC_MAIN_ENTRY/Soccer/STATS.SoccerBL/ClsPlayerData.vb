﻿#Region " Options "
Option Explicit Off
Option Strict Off
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsAddTeam
' Author        : Wilson Mathews
' Created Date  : 9th Feb,2016
' Description   : Get the Player Data
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class ClsPlayerData

#Region "Constants & Variables"
    Shared _myInstance As ClsPlayerData
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsPlayerData
        If _myInstance Is Nothing Then
            _myInstance = New ClsPlayerData
        End If
        Return _myInstance
    End Function
#End Region

    Public Function FillPlayer(players As DataTable) As List(Of PlayerData)
        Return (From player In players.AsEnumerable()
                             Select New PlayerData(player.Field(Of Int32)("PLAYER_ID"), player.Field(Of String)("PLAYER"), _
                                                   player.Field(Of Int32?)("STARTING_POSITION"),
                                                   player.Field(Of Int16?)("LINEUP_POSITION"),
                                                   player.Field(Of Boolean)("SUBSTITUTE"),
                                                   player.Field(Of Int32)("TEAM"), _
                                                   player.Field(Of Byte?)("PLAYER_STATUS_ID"), player.Field(Of String)("DISPLAY_UNIFORM_NUMBER"), _
                                                   player.Field(Of String)("UNIFORM"), player.Field(Of Int32)("TEAM_ID"), _
                                                   player.Field(Of String)("LAST_NAME_MONIKER_FIRST")
                                                    )).ToList()
    End Function

    Public Function GetEventPlayerData(playerslist As List(Of PlayerData), teamOrder As Int32, attributeDesc As String) As List(Of PlayerData)
        Dim lineUp? As Boolean
        Dim bench? As Boolean
        Dim home? As Boolean
        Dim away? As Boolean
        Dim defensiveTeam? As Boolean
        Dim singleTeam As Boolean
        Dim playerStatus() As Nullable(Of Byte) = {1, 2, 3, 4}
        Dim otherteam As Boolean
        Dim redCard As Boolean

        defensiveTeam = Nothing
        If attributeDesc = "ALL" Then
            lineUp = True
            bench = Nothing
            home = True
            away = True
        ElseIf attributeDesc = "Field" Then
            lineUp = True
            home = True
            away = True
            bench = Nothing
        ElseIf attributeDesc = "ALL OFF" Then
            lineUp = True
            bench = True
            singleTeam = True
            playerStatus = {0}  '{3,4} included 'injured
        ElseIf attributeDesc = "FIELD OFF" Then
            singleTeam = True
            lineUp = True
            bench = Nothing
        ElseIf attributeDesc = "FIELD DEF" Then
            singleTeam = True
            lineUp = True
            bench = Nothing
            defensiveTeam = True
        ElseIf attributeDesc = "FIELD ALL" Then
            lineUp = True
            home = True
            away = True
            otherteam = True
        ElseIf attributeDesc = "BENCH OFF" Then
            bench = True
            lineUp = Nothing
            singleTeam = True
        ElseIf attributeDesc = "BENCH DEF" Then
            lineUp = True
            bench = Nothing
            singleTeam = True
            defensiveTeam = True
        ElseIf attributeDesc = "ALL REDCARD" Then
            lineUp = True
            bench = Nothing
            singleTeam = True
            defensiveTeam = True
            redCard = True
        End If

        If defensiveTeam Then  ''make defensiveTeam true
            If teamOrder = 1 Then
                teamOrder = 2
            ElseIf teamOrder = 2 Then
                teamOrder = 1
            End If
        End If

        If singleTeam Then
            If teamOrder = 1 Then
                home = True
                away = Nothing
            Else
                away = True
                home = Nothing
            End If
        End If

        If redCard Then
            Return (From players In playerslist
              Where (players.LineUp = lineUp Or players.StartingPosition = 0) _
              AndAlso (players.Home = home Or players.Away = away)
              Order By players.LineupPosition Ascending
        Select New PlayerData(players.PlayerId, players.Player, players.StartingPosition)).ToList()
        Else
            Return (From players In playerslist
               Let sortOrder = (players.TeamType = teamOrder)
              Where (players.LineUp = lineUp Or players.Bench = bench) _
              AndAlso (players.Home = home Or players.Away = away)
              Order By sortOrder Ascending
        Select New PlayerData(players.PlayerId, players.Player, players.StartingPosition)).ToList()
        End If


        'AndAlso Not playerStatus.Contains(players.PlayerStatusId)
    End Function
    Public Function GetStaringPlayers(playerslist As List(Of PlayerData), playerteamId As Int32) As List(Of PlayerData)
        Dim lineup As Boolean? = True
        Dim staringPlayers = (From staringPlayer In playerslist
                            Where staringPlayer.LineUp = lineup AndAlso staringPlayer.TeamId = playerteamId
                           Select New PlayerData(pPlayerId:=staringPlayer.PlayerId, pPlayer:=CStr(staringPlayer.DisplayUniformNumber) & Chr(13) & staringPlayer.LastNameMonikerFirst, pStartPosition:=staringPlayer.StartingPosition))
        Return staringPlayers.ToList()
    End Function
    Public Function GetBenchPlayers(playerslist As List(Of PlayerData), playerteamId As Int32) As List(Of PlayerData)
        Dim bench As Boolean? = True
        Dim benchPlayers = (From benchPlayer In playerslist
                  Where benchPlayer.Bench = bench AndAlso benchPlayer.TeamId = playerteamId
                 Select New PlayerData(pPlayerId:=benchPlayer.PlayerId, pPlayer:=CStr(benchPlayer.DisplayUniformNumber) & Chr(13) & benchPlayer.LastNameMonikerFirst, pStartPosition:=benchPlayer.StartingPosition))
        Return benchPlayers.ToList()
    End Function
    Public Function GetPlayerName(playerslist As List(Of PlayerData), playerId As Int32) As String
        Return (From player In playerslist Where player.PlayerId = playerId
                Select player.Player).First
    End Function
    Public Function GetPlayerStartingPosition(playerslist As List(Of PlayerData), playerId As Int32) As String
        Return (From player In playerslist Where player.PlayerId = playerId
                Select player.StartingPosition).First
    End Function

    Public Class PlayerData

#Region "Constants & Variables"
        Shared _myInstance As PlayerData
#End Region

#Region "Shared methods "

        Public Shared Function GetInstance() As PlayerData
            If _myInstance Is Nothing Then
                _myInstance = New PlayerData
            End If
            Return _myInstance
        End Function
#End Region

        Private _playerId As Int32
        Public Property PlayerId() As Nullable(Of Int32)
            Get
                Return _playerId
            End Get
            Set(value As Nullable(Of Int32))
                _playerId = value
            End Set
        End Property
        Private _player As String
        Public Property Player() As String
            Get
                Return _player
            End Get
            Set(value As String)
                _player = value
            End Set
        End Property

        Private _lineUp? As Boolean
        Public Property LineUp() As Nullable(Of Boolean)
            Get
                Return _lineUp
            End Get
            Set(value As Nullable(Of Boolean))
                _lineUp = value
            End Set
        End Property

        Private _bench? As Boolean
        Public Property Bench() As Nullable(Of Boolean)
            Get
                Return _bench
            End Get
            Set(value As Nullable(Of Boolean))
                _bench = value
            End Set
        End Property

        Private _home? As Boolean
        Public Property Home() As Nullable(Of Boolean)
            Get
                Return _home
            End Get
            Set(value As Nullable(Of Boolean))
                _home = value
            End Set
        End Property

        Private _away? As Boolean
        Public Property Away() As Nullable(Of Boolean)
            Get
                Return _away
            End Get
            Set(value As Nullable(Of Boolean))
                _away = value
            End Set
        End Property

        Private _startingPosition? As Int16
        Public Property StartingPosition() As Nullable(Of Int16)
            Get
                Return _startingPosition
            End Get
            Set(value As Nullable(Of Int16))
                _startingPosition = value
            End Set
        End Property

        Private _teamType? As Int32
        Public Property TeamType() As Nullable(Of Int32)
            Get
                Return _teamType
            End Get
            Set(value As Nullable(Of Int32))
                _teamType = value
            End Set
        End Property

        Private _playerStatusId? As Byte
        Public Property PlayerStatusId() As Nullable(Of Byte)
            Get
                Return _playerStatusId
            End Get
            Set(value As Nullable(Of Byte))
                _playerStatusId = value
            End Set
        End Property

        Private _displayUniformNumber As String
        Public Property DisplayUniformNumber() As String
            Get
                Return _displayUniformNumber
            End Get
            Set(value As String)
                _displayUniformNumber = value
            End Set
        End Property

        Private _uniformNumber As String
        Public Property UniformNumber() As String
            Get
                Return _uniformNumber
            End Get
            Set(value As String)
                _uniformNumber = value
            End Set
        End Property



        Private _teamId As Int32
        Public Property TeamId() As Nullable(Of Int32)
            Get
                Return _teamId
            End Get
            Set(value As Nullable(Of Int32))
                _teamId = value
            End Set
        End Property

        Private _lastNameMonikerFirst As String
        Public Property LastNameMonikerFirst() As String
            Get
                Return _lastNameMonikerFirst
            End Get
            Set(value As String)
                _lastNameMonikerFirst = value
            End Set
        End Property
        Private _substitute? As Boolean
        Public Property Substitute() As Nullable(Of Boolean)
            Get
                Return _substitute
            End Get
            Set(value As Nullable(Of Boolean))
                _substitute = value
            End Set
        End Property
        Private _redcard? As Boolean
        Public Property RedCard() As Nullable(Of Boolean)
            Get
                Return _redcard
            End Get
            Set(value As Nullable(Of Boolean))
                _redcard = value
            End Set
        End Property

        Private _lineupPosition? As Byte
        Public Property LineupPosition() As Nullable(Of Byte)
            Get
                Return _lineupPosition
            End Get
            Set(value As Nullable(Of Byte))
                _lineupPosition = value
            End Set
        End Property
        Public Sub New()

        End Sub

        Public Sub New(pPlayerId As Int32, pPlayer As String, pStartingPosition? As Int16, pLineupPosition? As Int16, pSubstitute As Boolean, pTeamType As Int32, pPlayerStatusId? As Byte, pdisplayUniformNumber As String, pUniformNumber As String, pTeamId As Int32, pLastNameMonikerFirst As String)
            PlayerId = pPlayerId
            Player = pPlayer
            StartingPosition = pStartingPosition
            TeamType = pTeamType
            PlayerStatusId = pPlayerStatusId

            If pStartingPosition < 12 And pStartingPosition > 0 Then
                LineUp = True
            Else
                LineUp = False
            End If
            If (pStartingPosition > 11) Then
                Bench = True
            Else
                Bench = False
            End If
            If (pStartingPosition = 0) Then
                RedCard = True
            Else
                RedCard = False
            End If
            Substitute = pSubstitute

            If pTeamType = 1 Then
                Home = True
            Else
                Away = True
            End If
            DisplayUniformNumber = pdisplayUniformNumber
            UniformNumber = pUniformNumber
            TeamId = pTeamId
            LastNameMonikerFirst = pLastNameMonikerFirst
            LineupPosition = pLineupPosition
        End Sub
        Public Sub New(pPlayerId As Int32, pPlayer As String, pStartPosition? As Int16)
            PlayerId = pPlayerId
            Player = pPlayer
            StartingPosition = pStartPosition
        End Sub

    End Class
End Class