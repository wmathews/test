﻿Public Class clsSportVU

#Region "Constants & Variables"
    Shared myInstance As clsSportVU
    Private objSportVU As SoccerDAL.clsSportVU = SoccerDAL.clsSportVU.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsSportVU
        If myInstance Is Nothing Then
            myInstance = New clsSportVU
        End If
        Return myInstance
    End Function
#End Region

    Public Function GetOpticalPBPData(ByVal GameCode As Integer, ByVal TeamID As Integer, ByVal EventID As Integer) As DataSet
        Try

            Return objSportVU.GetOpticalData(GameCode, TeamID, EventID)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetOpticalEvent(ByVal GameCode As Integer, ByVal UniqueID As Integer) As DataSet
        Try

            Return objSportVU.GetOpticalEvent(GameCode, UniqueID)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateProcessedOpticalPBP(ByVal GameCode As Integer, ByVal UniqueIDs As String, ByVal SaveIgnore As Char) As Boolean
        Try
            If objSportVU.UpdateProcessedOpticalPBP(GameCode, UniqueIDs, SaveIgnore) > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetOpticalTouchData(ByVal GameCode As Integer, ByVal PlayerID As Integer, ByVal TeamID As Integer) As DataSet
        Try
            Return objSportVU.GetOpticalTouchData(GameCode, PlayerID, TeamID)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function UpdateProcessedOpticalTouch(ByVal GameCode As Integer, ByVal UniqueIDs As String) As Boolean
        Try
            If objSportVU.UpdateProcessedOpticalTouch(GameCode, UniqueIDs) > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetOpticalTouchEvent(ByVal GameCode As Integer, ByVal UniqueID As Integer) As DataSet
        Try

            Return objSportVU.GetOpticalTouchEvent(GameCode, UniqueID)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAllSportVUData(ByVal GameCode As Integer, ByVal ModuleID As Integer) As DataSet
        Try
            Return objSportVU.GetAllSportVUData(GameCode, ModuleID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetMaxUniqueID(ByVal ModuleID As Integer) As DataSet
        Try
            Return objSportVU.GetMaxUniqueID(ModuleID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetFieldDimensions(ByVal GameCode As Integer) As DataSet
        Try
            Return objSportVU.GetFieldDimensions(GameCode)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
