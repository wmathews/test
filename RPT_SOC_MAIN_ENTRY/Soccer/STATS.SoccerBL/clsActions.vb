﻿Public Class ClsActions

#Region "Constants & Variables"
    Shared _myInstance As ClsActions
    Private _objActions As SoccerDAL.clsActions = SoccerDAL.clsActions.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsActions
        If _myInstance Is Nothing Then
            _myInstance = New ClsActions
        End If
        Return _myInstance
    End Function
#End Region

#Region "User Functions"

    Public Function InsertEditActionsPBPData(ByVal pbpXmlData As String, ByVal SeqNumber As Decimal) As DataSet
        Try
            Return _objActions.InsertEditActionsPBPData(pbpXmlData, SeqNumber)
        Catch ex As Exception
            Throw
        End Try
    End Function

  

#End Region

End Class