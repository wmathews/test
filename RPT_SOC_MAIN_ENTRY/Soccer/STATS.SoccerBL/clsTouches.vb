﻿#Region "Imports"
Imports System
Imports STATS.SoccerDAL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsTouches
' Author        : Dijo Davis
' Created Date  : 03-06-09
' Description   : This is the business logic for touches module.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region



Public Class clsTouches

#Region "Constants & Variables"
    Shared m_myInstance As clsTouches
    Private m_objclsTouches As STATS.SoccerDAL.clsTouches = STATS.SoccerDAL.clsTouches.GetAccess()
#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsTouches
        If m_myInstance Is Nothing Then
            m_myInstance = New clsTouches
        End If
        Return m_myInstance
    End Function
#End Region

#Region "Touches"
    ''' <summary>
    ''' To save touches
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <param name="intTimeElapsed"></param>
    ''' <param name="intPeriod"></param>
    ''' <param name="intPlayerID"></param>
    ''' <param name="intTouchTypeID"></param>
    ''' <param name="strReporterRole"></param>
    ''' <param name="intXFieldZone"></param>
    ''' <param name="intYFieldZone"></param>
    ''' <remarks></remarks>
    Public Sub AddTouches(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intTimeElapsed As Int32, ByVal intPeriod As Int32, ByVal intPlayerID As Int32, ByVal intTouchTypeID As Int32, ByVal strReporterRole As String, ByVal intXFieldZone As Integer, ByVal intYFieldZone As Integer)
        Try
            m_objclsTouches.AddTouches(intGameCode, intFeedNumber, intTimeElapsed, intPeriod, intPlayerID, intTouchTypeID, strReporterRole, intXFieldZone, intYFieldZone)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub AddTouches(ByVal TouchesData As DataSet)
        Try
            m_objclsTouches.AddTouches(TouchesData)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetGoalieChangeEvent(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal TeamID As Integer) As DataSet
        Try
            Return m_objclsTouches.GetGoalieChangeEvent(GameCode, FeedNumber, TeamID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To get touches data
    ''' </summary>
    ''' <param name="intGameCode"></param>
    ''' <param name="intFeedNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTouches(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intlanguage_id As Int32, ByVal intSerialNo As Int32, ByVal touchHTeamID As Integer, ByVal touchATeamID As Integer, ByVal period As Integer) As DataSet
        Try
            Return m_objclsTouches.GetTouches(intGameCode, intFeedNumber, intlanguage_id, intSerialNo, touchHTeamID, touchATeamID, period)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetTouchesAll(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intlanguage_id As Int32, ByVal intSerialNo As Int32) As DataSet
        Try
            Return m_objclsTouches.GetTouchesAll(intGameCode, intFeedNumber, intlanguage_id, intSerialNo)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLastEventDetails(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32) As DataSet
        Try
            Return m_objclsTouches.GetLastEventDetails(intGameCode, intFeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function RefreshTouchesData(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intlanguage_id As Int32, ByVal Sequence_Number As Decimal, ByVal SerialNo As Integer) As DataSet
        Try
            Return m_objclsTouches.RefreshTouchesData(intGameCode, intFeedNumber, intlanguage_id, Sequence_Number, SerialNo)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTouchesRefresh(ByVal intGameCode As Int32, ByVal intFeedNumber As Int32, ByVal intlanguage_id As Int32, ByVal SerialNo As Integer) As DataSet
        Try
            Return m_objclsTouches.GetTouchesRefresh(intGameCode, intFeedNumber, intlanguage_id, SerialNo)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateTouches(ByVal TouchesData As DataSet, ByVal decSequenceNumber As Decimal, ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Try
            Return m_objclsTouches.UpdateTouches(TouchesData, decSequenceNumber, GameCode, FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetTouchesRecordBasedOnSeqNumber(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal Sequence_Number As Decimal, ByVal Type As String) As DataSet
        Try
            Return m_objclsTouches.GetPBPRecordBasedOnSeqNumber(GameCode, FeedNumber, Sequence_Number, Type)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetMaxTimeElapsed(ByVal GameCode As Integer, ByVal FeedNumber As Integer) As Integer
        Return m_objclsTouches.GetMaxTimeElapsed(GameCode, FeedNumber)
    End Function

    Public Function GetPBPDataForTouches(ByVal intGameCode As Int32, ByVal language_id As Integer) As DataSet
        Try
            Return m_objclsTouches.GetPBPDataForTouches(intGameCode, language_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To delete touches data
    ''' </summary>
    ''' <param name="decSequenceNumber"></param>
    ''' <remarks></remarks>
    Public Function DeleteTouches(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal decSequenceNumber As Decimal, ByVal Reporter_Role As String) As Integer
        Try
            Return m_objclsTouches.DeleteTouches(GameCode, FeedNumber, decSequenceNumber, Reporter_Role)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetplayerCountInTouches(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal PlayerID As Integer) As Integer
        Try
            Return m_objclsTouches.GetPlayerCountInTouches(GameCode, FeedNumber, PlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function UpdateExisPBPPlayerWithNewPlayer(ByVal GameCode As Integer, ByVal FeedNumber As Integer, ByVal ReporterRole As String, ByVal ExisPlayerID As Integer, ByVal NewPlayerID As Integer) As Integer
        Try
            m_objclsTouches.UpdateExisTouchesPlayerWithNewPlayer(GameCode, FeedNumber, ReporterRole, ExisPlayerID, NewPlayerID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
End Class
