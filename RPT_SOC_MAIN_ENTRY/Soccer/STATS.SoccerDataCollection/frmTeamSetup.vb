﻿
#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmTeamSetup
' Author        : Shirley Ranjini S
' Created Date  : 11-05-09
' Description   : This form is used for the selection of starting Lineups players for Home and Away Team
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmTeamSetup

#Region "Constants & Variables"

    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objBLTeamSetup As New STATS.SoccerBL.clsTeamSetup
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_objModule1Main As New frmModule1Main
    Private m_objUtilAudit As New clsAuditLog
    Private m_objUtility As New clsUtility
    Private m_HomeCoach As Integer = 0
    Private m_HometeamID As Integer = 0
    Private m_AwayteamID As Integer = 0
    Private m_HomeFormation As Integer
    Private m_AwayCoach As Integer = 0
    Private m_AwayFormation As Integer
    Private m_HomeFormationSpec As Integer
    Private m_AwayFormationSpec As Integer
    Private m_sendToClient As Boolean
    Private BoolIsLineupsUpdated As Boolean = False

    Private intTeamID As Integer
    Private DsRostersInfo As DataSet = Nothing
    Private dsTeamGame As DataSet = Nothing
    'Dim dsTeamGame As New DataSet
    Dim dsSelectedData As New DataSet
    Dim dsForCoach As DataSet = Nothing
    Private m_blnIsFormationComboLoaded As Boolean = False
    Private m_blnIsCoachComboLoaded As Boolean = False

    Private m_blnAutoFired As Boolean = False
    Private m_HomeKit As Integer = -1
    Private m_AwayKit As Integer = -1
    Dim StrTeamChecked As String
    Private m_blnCheck As Boolean = False
    Private m_DsPlayersDesel As DataSet
    Private dsCoachKitInfo As DataSet
    Private lsupport As New languagesupport

    Dim strmessage As String = "Please click on Apply/Reset to proceed"
    Dim strmessage2 As String = "The same uniform number exists for"
    Dim strmessage3 As String = "Player already selected in Bench Lineups"
    Dim strmessage4 As String = "Player already selected in Starting Lineups"
    Dim strmessage5 As String = "Please click on Formation Apply/Reset button for the changes to take effect"
    Dim strmessage6 As String = "Are teams done and ready to send to clients?"

    Dim strmessage7 As String = "Formation will be lost - are you sure?"
    Dim strmessage8 As String = "Do you want to Overwrite the players with the players dragged in formation ?"
    Dim strmessage9 As String = "Please select a player to delete"
    Dim strmessage10 As String = "Are you sure to delete"
    Dim strmessage11 As String = "Players are updated successfully."
    Dim strmessage12 As String = "Ratings entered should be between 1 and 10 ."
    Dim strmessage13 As String = "This Player is not Eligible for Rating!!!."
    Dim strmessage14 As String = "Player is not selected.Please select a player and enter ratings!!."
    Dim strmessage15 As String = "Ratings are not given for the players who were subbed in .Do you want to proceed ?"
    Dim strmessage16 As String = "Ratings are not given for Lineup players .Do you want to proceed ?"
    'Dim strmessage17 As String = "Are teams done and ready to send to clients?"
    'Dim strmessage18 As String = "Player is not selected.Please select a player and enter ratings!!."
    Dim strmessage19 As String = "You have not selected Coach - want to proceed?"
    Dim strmessage20 As String = "You have not selected Kit - want to proceed?"
    Dim strmessage21 As String = "Do you want to add"
    Dim strmessage22 As String = "in lineups"
    Dim strmessage23 As String = "You are not allowed to Insert/Update Events - Primary Reporter is Down!"
    Dim strmessage24 As String = "Last Entry in Process, Please Wait..."

#End Region

#Region "Event Handling"

    Private Sub frmTeamSetup_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            'EXITS FROM THE FORM
            'AUDIT TRIAL
            UpdatelabelColor()
            getCoachKitInfo()
            'If m_AwayCoach = 0 And m_HomeCoach = 0 Then
            '    If essageDialog.Show("You have not selected Coach/Kit - want to proceed?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
            '        Exit Sub
            '    End If
            'ElseIf m_HomeCoach = 0 Or m_HomeKit = -1 Then
            '    If m_AwayCoach = 0 Or m_AwayKit = -1 Then
            '        If essageDialog.Show("You have not selected Coach/Kit - want to proceed?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
            '            Exit Sub
            '        End If
            '    Else
            '        If essageDialog.Show("You have not selected HOME Coach/Kit - want to proceed?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
            '            Exit Sub
            '        End If
            '    End If
            'ElseIf m_AwayCoach = 0 Or m_AwayKit = -1 Then
            '    If essageDialog.Show("You have not selected AWAY Coach/Kit - want to proceed?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
            '        Exit Sub
            '    End If
            'End If

            If m_AwayKit = -1 Or m_HomeKit = -1 Then

                If dsCoachKitInfo.Tables(4).Rows.Count > 0 Then
                    Dim homeKitcount() As DataRow
                    Dim awayKitcount() As DataRow

                    homeKitcount = dsCoachKitInfo.Tables(4).Select("TEAM_ID = " & m_HometeamID & "")

                    awayKitcount = dsCoachKitInfo.Tables(4).Select("TEAM_ID = " & m_AwayteamID & "")

                    'If dr.Length > 0 Then
                    If homeKitcount.Length > 0 And awayKitcount.Length > 0 Then
                        If MessageDialog.Show(strmessage20, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    ElseIf (homeKitcount.Length > 0 And m_HomeKit = -1) Or (awayKitcount.Length > 0 And m_AwayKit = -1) Then
                        If MessageDialog.Show(strmessage20, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If

                End If
            End If
            If m_AwayCoach = 0 Or m_HomeCoach = 0 Then
                If MessageDialog.Show(strmessage19, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmTeamSetup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmTeamSetup", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Dim TEMLENINT As Integer
            'ASSIGNING HOME AND AWAY TEAM NAMES TO THE RADIO BUTTONS
            dsSelectedData = CreateColumn()
            radHomeTeam.Text = m_objclsGameDetails.HomeTeam
            radAwayTeam.Text = m_objclsGameDetails.AwayTeam
            'm_objTeamSetup.HomeShirtColor = Color.Empty
            'm_objTeamSetup.AwayShirtColor = Color.Empty
            'm_objTeamSetup.HomeShortColor = Color.Empty
            'm_objTeamSetup.AwayShortColor = Color.Empty
            ''BY DEFAULT SELECT THE HOME TEAM RADIO BUTTON
            If (m_objGameDetails.CoverageLevel <> 6) Then ' TOPZ-1116
                btnShirtNoColor.Visible = False
                btnClearShirtNoColor.Visible = False
                Label7.Visible = False
                lblShirtNoColor.Visible = False
            End If
            If StrTeamChecked = "Home" Then
                radHomeTeam.Checked = True
                radAwayTeam.Checked = False
                lblTeam.Text = radHomeTeam.Text.Trim
            ElseIf StrTeamChecked = "Away" Then
                radHomeTeam.Checked = False
                radAwayTeam.Checked = True
                lblTeam.Text = radAwayTeam.Text.Trim
            End If

            If dgInjuriesSuspensions.Rows.Count > 0 Then
                dgInjuriesSuspensions.ClearSelection()
            End If
            If m_objBLTeamSetup.GetLineupEventCnt(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID) = 2 Then
                lblStartLineSent.Text = "YES"
                lblStartLineSent.ForeColor = Color.Green
            Else
                lblStartLineSent.Text = "NO"
                lblStartLineSent.ForeColor = Color.OrangeRed
            End If
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                Label1.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label1.Text)
                Label2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label2.Text)
                radHomeTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radHomeTeam.Text)
                radAwayTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radAwayTeam.Text)
                lblInjuriesSuspensions.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblInjuriesSuspensions.Text)
                lblCoach.Text = lblCoach.Text.Replace(":", "")
                lblCoach.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblCoach.Text)
                lblCoach.Text = lblCoach.Text & ":"
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                lblFormation.Text = lblFormation.Text.Replace(":", "")
                TEMLENINT = lblFormation.Text.Length + 3
                lblFormation.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblFormation.Text)
                linebreak(lblFormation, TEMLENINT)
                lblFormation.Text = lblFormation.Text & ":"
                Label3.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label3.Text)
                Label8.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Kit:")
                TEMLENINT = Label4.Text.Length - 4
                Label4.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label4.Text)
                linebreak(Label4, TEMLENINT)
                Label5.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label5.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnAddPlayer.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAddPlayer.Text)
                btnManager.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnManager.Text)
                lblTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTeam.Text)
                Label15.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label15.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)
                strmessage8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage8)
                strmessage9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage9)
                strmessage10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage10)
                strmessage11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage11)
                strmessage12 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage12)
                strmessage13 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage13)
                strmessage14 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage14)
                strmessage15 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage15)
                strmessage16 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage16)
                strmessage19 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage19)
                strmessage20 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage20)
                strmessage21 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage21)
                strmessage22 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage22)
                strmessage23 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage23)
                strmessage24 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage24)
                For i As Integer = 0 To dgvStartingLineup.Columns.Count - 1

                    dgvStartingLineup.Columns(i).HeaderText = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), dgvStartingLineup.Columns(i).HeaderText).Replace(" ", "")
                Next

                For i As Integer = 0 To dgvBench.Columns.Count - 1
                    dgvBench.Columns(i).HeaderText = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), dgvBench.Columns(i).HeaderText).Replace(" ", "")
                Next
                For i As Integer = 0 To dgInjuriesSuspensions.Columns.Count - 1
                    dgInjuriesSuspensions.Columns(i).HeaderText = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), dgInjuriesSuspensions.Columns(i).HeaderText).Replace(" ", "")
                Next

            End If



            btnSave.Enabled = True



        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub
    Public Sub linebreak(ByVal lbtemp As Label, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub
    Private Sub radHomeTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radHomeTeam.CheckedChanged
        Try
            'LOGO,TEAM NAMES AND PLAYERS ARE LOADED BASED ON TEAM SELECTION
            If radHomeTeam.Checked = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radHomeTeam.Text, 1, 0)
                If DsRostersInfo IsNot Nothing Then
                    'UPDATE LINUP INFO FOR THE SELECTED PLAYERS IN THE DATAGRIDVIEW
                    If UpdatePlayerLineupInfo() = False Then
                        'radHomeTeam.Checked = False
                        Exit Sub
                    End If
                    'shirley sep 28 validate uniform number
                    If ValidateUniformNumber() = False Then
                        Exit Sub
                    End If

                    'If AreRatingsEntered() = False Then
                    '    Exit Try
                    'End If
                    If m_blnCheck = False Then
                        If ArelineupsEntered() = False Then
                            m_blnCheck = False
                            Exit Sub
                        End If
                    Else
                        m_blnCheck = False
                        Exit Sub
                    End If

                End If


                lblTeam.Text = radHomeTeam.Text.Trim
                FetchTeamID(radHomeTeam.Text)
                FetchTeamLogo(intTeamID)
                GetRostersInfo()
                GetUniformImages()
                UpdatelabelColor()
                GetFormationCoach()
                'IF THE PLAYERS ARE FILLED ALREADY AND IF THIS TEAM IS SELECTED AGAIN
                'POPULATE THE ROSTERS FROM THE LOCAL DATASET
                SetPlayersInEachComboCell()

                RetriveRostersFromLocal()

                GetInjuredSuspendedPlayers()

                'DISBALING THE SAVE BUTTON AS SAVE BUTTON SHOULD BE CLICKED ONCE 
                'VISITING TEAM PLAYERS ARE SELECTED
                'btnSave.Enabled = False
                RefreshFormationDisplay()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally

        End Try
    End Sub

    Private Sub radAwayTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAwayTeam.CheckedChanged
        Try

            'LOGO , TEAM NAMES AND PLAYERS ARE LOADED BASED ON TEAM SELECTION
            If radAwayTeam.Checked = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radAwayTeam.Text, 1, 0)

                'UPDATE LINUP INFO FOR THE SELECTED PLAYERS IN THE DATAGRIDVIEW
                If DsRostersInfo IsNot Nothing Then
                    If UpdatePlayerLineupInfo() = False Then
                        'radAwayTeam.Checked = False
                        Exit Sub
                    End If
                    If ValidateUniformNumber() = False Then
                        Exit Sub
                    End If
                    'If AreRatingsEntered() = False Then
                    '    radHomeTeam.Checked = True
                    '    Exit Try
                    'End If
                    If m_blnCheck = False Then
                        If ArelineupsEntered() = False Then
                            m_blnCheck = False
                            Exit Sub
                        End If
                    Else
                        m_blnCheck = False
                        Exit Sub
                    End If

                End If

                lblTeam.Text = radAwayTeam.Text.Trim
                FetchTeamID(radAwayTeam.Text)
                'LOGO TO DISPLAY
                FetchTeamLogo(intTeamID)
                GetRostersInfo()
                UpdatelabelColor()
                GetUniformImages()
                UpdatelabelColor()
                GetFormationCoach()

                SetPlayersInEachComboCell()
                'IF THE PLAYERS ARE FILLED ALREADY AND IF THIS TEAM IS SELECTED AGAIN
                'POPULATE THE ROSTERS FROM THE LOCAL DATASET
                RetriveRostersFromLocal()

                GetInjuredSuspendedPlayers()

                'ENABLING THE SAVE BUTTON AS SAVE BUTTON SHOULD BE CLICKED ONCE 
                'VISITING TEAM PLAYERS ARE SELECTED
                RefreshFormationDisplay()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally

        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try


            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, "Team Setup", 1, 0)
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub dgvStartingLineup_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStartingLineup.CellContentClick, dgvBench.CellContentClick
        If dgvStartingLineup.CurrentCell.ColumnIndex = 4 Or dgvBench.CurrentCell.ColumnIndex = 4 Then
            'Dim DR() As DataRow
            Dim StrTableName As String = ""
            For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                dgvStartingLineup.Rows(i).Cells(4).Value = False
            Next
            For i As Integer = 0 To dgvBench.Rows.Count - 1
                dgvBench.Rows(i).Cells(4).Value = False
            Next

            If radHomeTeam.Checked Then
                m_objGameDetails.HomeTeamCaptainId = IIf(sender.Rows(e.RowIndex).Cells(4).EditedFormattedValue, sender.Rows(e.RowIndex).Cells(2).Value, 0)
            ElseIf radAwayTeam.Checked Then
                m_objGameDetails.AwayTeamCaptainId = IIf(sender.Rows(e.RowIndex).Cells(4).EditedFormattedValue, sender.Rows(e.RowIndex).Cells(2).Value, 0)
            End If

        End If
    End Sub

    Private Sub dgvStartingLineup_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStartingLineup.CellValueChanged
        Try
            'TRIGGERS formation display if there is a change in uniform number
            If dgvStartingLineup.CurrentCell.ColumnIndex = 1 Then
                'updates the starting position's selected
                If Not dgvStartingLineup.CurrentRow.Cells(1).Value Is Nothing Then
                    If dgvStartingLineup.CurrentRow.Cells(1).Value.ToString <> "" Then
                        If UdcSoccerFormation1.btnApply.Enabled = True Then
                            'ApplyChangesToDatagridview()
                            MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Exit Sub
                        End If
                        FillStLineupPlayerCombo()
                        'If ValidateSLUniformNumber() = False Then
                        '    Exit Sub
                        'End If
                        UpdatePlayerLineupInfo()
                        RefreshFormationDisplay()
                    End If
                End If
            ElseIf dgvStartingLineup.CurrentCell.ColumnIndex = 3 Then
                TextBoxSLValidated(sender, e)
            ElseIf dgvStartingLineup.CurrentCell.ColumnIndex = 2 Then
                '1610
                If CLng(dgvStartingLineup.Rows(e.RowIndex).Cells(2).Value) <> Nothing Then
                    If radHomeTeam.Checked Then
                        m_objGameDetails.HomeTeamCaptainId = IIf(dgvStartingLineup.Rows(e.RowIndex).Cells(4).Value, dgvStartingLineup.Rows(e.RowIndex).Cells(2).Value, m_objGameDetails.HomeTeamCaptainId)
                    ElseIf radAwayTeam.Checked Then
                        m_objGameDetails.AwayTeamCaptainId = IIf(dgvStartingLineup.Rows(e.RowIndex).Cells(4).Value, dgvStartingLineup.Rows(e.RowIndex).Cells(2).Value, m_objGameDetails.AwayTeamCaptainId)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub combo_DropDown(ByVal sender As Object, ByVal e As EventArgs)
        Try
            DirectCast(sender, ComboBox).BackColor = Color.White
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub combo_KeyPress(ByVal sender As Object, ByVal e As EventArgs)
        Try
            DirectCast(sender, ComboBox).DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub Textbox_BackColorChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            DirectCast(sender, TextBox).BackColor = Color.White
            dgvStartingLineup.CurrentCell.Style.BackColor = Color.White
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub dgvStartingLineup_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvStartingLineup.DataError
        Try

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub dgvStartingLineup_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvStartingLineup.EditingControlShowing
        Try

            'EVENT IS TRIGGERED AND FILLS THE UNIFORM NUMBER, ONCE PLAYER IS SELECTED FROM THE COMBO.
            If dgvStartingLineup.CurrentCell.ColumnIndex = 2 Then

                If TypeOf e.Control Is ComboBox Then
                    Dim cb As ComboBox = CType(e.Control, ComboBox)
                    cb.DropDownStyle = ComboBoxStyle.DropDown
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.AutoCompleteSource = AutoCompleteSource.ListItems
                    cb.SelectAll()
                End If

                ' first remove event handler to keep from attaching multiple: 
                RemoveHandler DirectCast(e.Control, ComboBox).SelectedIndexChanged, AddressOf FillSLUniformNo
                ' now attach the event handler 
                m_blnAutoFired = True

                AddHandler DirectCast(e.Control, ComboBox).SelectedIndexChanged, AddressOf FillSLUniformNo

                'AddHandler DirectCast(e.Control, ComboBox).SelectionChangeCommitted, AddressOf FillSLUniformNo

                RemoveHandler DirectCast(e.Control, ComboBox).DropDown, AddressOf combo_DropDown
                AddHandler DirectCast(e.Control, ComboBox).DropDown, AddressOf combo_DropDown


                RemoveHandler DirectCast(e.Control, ComboBox).KeyPress, AddressOf combo_KeyPress
                AddHandler DirectCast(e.Control, ComboBox).KeyPress, AddressOf combo_KeyPress

            ElseIf dgvStartingLineup.CurrentCell.ColumnIndex = 1 Or dgvStartingLineup.CurrentCell.ColumnIndex = 3 Then

                RemoveHandler DirectCast(e.Control, TextBox).Validated, AddressOf TextBoxSLValidated
                AddHandler DirectCast(e.Control, TextBox).Validated, AddressOf TextBoxSLValidated

                RemoveHandler DirectCast(e.Control, TextBox).KeyPress, AddressOf TextBoxKeyPress
                AddHandler DirectCast(e.Control, TextBox).KeyPress, AddressOf TextBoxKeyPress

                If dgvStartingLineup.CurrentCell.ColumnIndex = 1 Then
                    If Not dgvStartingLineup.CurrentRow.Cells(1).Value Is Nothing Then
                        If dgvStartingLineup.CurrentRow.Cells(1).Value.ToString <> "" Then
                            If UdcSoccerFormation1.btnApply.Enabled = True Then
                                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Exit Sub
                            End If
                            RefreshFormationDisplay()
                        End If
                    End If
                End If


                RemoveHandler DirectCast(e.Control, TextBox).BackColorChanged, AddressOf Textbox_BackColorChanged
                AddHandler DirectCast(e.Control, TextBox).BackColorChanged, AddressOf Textbox_BackColorChanged
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub TextBoxSLValidated(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Not IsDBNull(dgvStartingLineup.CurrentRow.Cells(3).Value) And Not dgvStartingLineup.CurrentRow.Cells(3).Value Is Nothing Then
                'Check whether the player has been selected
                If Not dgvStartingLineup.CurrentRow.Cells(3).Value.ToString = "" Then
                    If IsDBNull(dgvStartingLineup.CurrentRow.Cells(2).Value) Or dgvStartingLineup.CurrentRow.Cells(2).Value Is Nothing Then
                        MessageDialog.Show(strmessage14, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        dgvStartingLineup.CurrentRow.Cells(3).Value = ""
                        Exit Sub
                    End If
                End If


                If Not dgvStartingLineup.CurrentRow.Cells(3).Value.ToString = "" Then
                    If CInt(dgvStartingLineup.CurrentRow.Cells(3).Value) > 10 Or CInt(dgvStartingLineup.CurrentRow.Cells(3).Value) <= 0 Then
                        MessageDialog.Show(strmessage12, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        dgvStartingLineup.CurrentRow.Cells(3).Value = ""
                    End If
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Function AreRatingsEntered() As Boolean
        Try
            'check whether the ratings are entered for starting lineups players

            Dim DRS() As DataRow = DsRostersInfo.Tables(GetTableName).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION <12")
            If DRS.Length > 0 Then
                For Each drRow As DataRow In DRS
                    If drRow.Item("Rating").ToString = "" Then
                        If MessageDialog.Show(strmessage16, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                            Return True
                        Else
                            Return False
                        End If
                    End If
                Next
            End If

            'check for bench players
            Dim DsSubEvents As DataSet
            DsSubEvents = m_objGeneral.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If DsSubEvents.Tables.Count > 0 Then
                Dim DRSubevents() As DataRow = DsSubEvents.Tables(0).Select("EVENT_CODE_ID = 22 AND TEAM_ID =" & intTeamID & "")
                If DRSubevents.Length > 0 Then
                    Dim DR() As DataRow = DsRostersInfo.Tables(GetTableName).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION >11")
                    If DR.Length > 0 Then
                        For Each drRow As DataRow In DR
                            If IsRatingAllowedForPlayer(CInt(drRow.Item("PLAYER_ID"))) = False Then
                                If drRow.Item("Rating").ToString = "" Then
                                    If MessageDialog.Show(strmessage15, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                                        Return True
                                    Else
                                        Return False
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function IsRatingAllowedForPlayer(ByVal PlayerID As Integer) As Boolean
        Try
            Dim DsSubEvents As DataSet
            DsSubEvents = m_objGeneral.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If DsSubEvents.Tables.Count > 0 Then
                Dim DRS() As DataRow = DsSubEvents.Tables(0).Select("EVENT_CODE_ID = 22 AND OFFENSIVE_PLAYER_ID = " & PlayerID & "")
                If DRS.Length > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub TextBoxBenchValidated(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Not IsDBNull(dgvBench.CurrentRow.Cells(3).Value) And Not dgvBench.CurrentRow.Cells(3).Value Is Nothing Then
                'Check whether the player has been selected
                If Not dgvBench.CurrentRow.Cells(3).Value.ToString = "" Then
                    If IsDBNull(dgvBench.CurrentRow.Cells(2).Value) Or dgvBench.CurrentRow.Cells(2).Value Is Nothing Then
                        MessageDialog.Show(strmessage14, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        dgvBench.CurrentRow.Cells(3).Value = ""
                        Exit Sub
                    End If
                End If

                'FPLRS-271 Soccer Reporter Software - Player Ratings for Substitutes with Red Cards
                ' bench player if given a red card and sub (Color.IndianRed,Color.PaleGreen) should be allowed to rate as per the new requirement
                If (dgvBench.CurrentRow.DefaultCellStyle.BackColor.Name = "0") Then
                    MessageDialog.Show(strmessage13, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    dgvBench.CurrentRow.Cells(3).Value = ""
                    Exit Sub
                End If


                'validation for Ratings
                If Not dgvBench.CurrentRow.Cells(3).Value.ToString = "" Then
                    If CInt(dgvBench.CurrentRow.Cells(3).Value) > 10 Or CInt(dgvBench.CurrentRow.Cells(3).Value) <= 0 Then
                        MessageDialog.Show(strmessage12, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        dgvBench.CurrentRow.Cells(3).Value = ""
                    End If
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub TextBoxKeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Try
            If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) And (Asc(e.KeyChar) < 47 Or Asc(e.KeyChar) > 57) Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub



    Private Sub dgvBench_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBench.CellValueChanged
        Try
            If dgvBench.CurrentCell.ColumnIndex = 1 Then
                If Not dgvBench.CurrentRow.Cells(1).Value Is Nothing Then
                    If dgvBench.CurrentRow.Cells(1).Value.ToString <> "" Then
                        'If UdcSoccerFormation1.btnApply.Enabled = True Then
                        '    essageDialog.Show("Please click on Formation Apply/Reset button for the changes to take effect", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        '    Exit Sub
                        'End If

                        If m_blnCheck = False Then
                            If ArelineupsEntered() = False Then
                                m_blnCheck = False
                                Exit Sub
                            End If
                        Else
                            m_blnCheck = False
                            Exit Sub
                        End If
                        FillBenchPlayerCombo()

                        'If ValidateBenchUniformNumber() = False Then
                        '    Exit Sub
                        'End If

                        'updates the starting position's selected
                        UpdatePlayerLineupInfo()
                    End If
                End If
                'ElseIf dgvStartingLineup.CurrentCell.ColumnIndex = 2 Then
                '    If dgvBench.CurrentRow.Cells(1).Value.ToString = "" Then
                '        'If Not dgvBench.CurrentRow.Cells(2).Value Is Nothing Then
                '        '    If dgvBench.CurrentRow.Cells(2).Value.ToString <> "" Then
                '        '        RefreshFormationDisplay()
                '        '    End If
                '        'End If
                '    End If


            ElseIf dgvStartingLineup.CurrentCell.ColumnIndex = 3 Then
                If Not dgvBench.CurrentRow.Cells(3).Value Is Nothing Then
                    If dgvBench.CurrentRow.Cells(3).Value.ToString <> "" Then
                        TextBoxBenchValidated(sender, e)
                    End If
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgvBench_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBench.DataError
        Try

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub dgvBench_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBench.EditingControlShowing
        Try
            'EVENT IS TRIGGERED AND FILLS THE UNIFORM NUMBER, ONCE PLAYER IS SELECTED FROM THE COMBO.
            If dgvBench.CurrentCell.ColumnIndex = 2 Then

                If TypeOf e.Control Is ComboBox Then
                    Dim cb As ComboBox = CType(e.Control, ComboBox)
                    cb.DropDownStyle = ComboBoxStyle.DropDown
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.AutoCompleteSource = AutoCompleteSource.ListItems
                    cb.SelectAll()
                End If

                ' first remove event handler to keep from attaching multiple: 
                RemoveHandler DirectCast(e.Control, ComboBox).SelectedIndexChanged, AddressOf FillBenchUniformNo
                ' now attach the event handler 
                m_blnAutoFired = True
                AddHandler DirectCast(e.Control, ComboBox).SelectedIndexChanged, AddressOf FillBenchUniformNo

                RemoveHandler DirectCast(e.Control, ComboBox).DropDown, AddressOf combo_DropDown
                AddHandler DirectCast(e.Control, ComboBox).DropDown, AddressOf combo_DropDown


                RemoveHandler DirectCast(e.Control, ComboBox).KeyPress, AddressOf combo_KeyPress
                AddHandler DirectCast(e.Control, ComboBox).KeyPress, AddressOf combo_KeyPress

            ElseIf dgvBench.CurrentCell.ColumnIndex = 1 Or dgvBench.CurrentCell.ColumnIndex = 3 Then

                RemoveHandler DirectCast(e.Control, TextBox).Validated, AddressOf TextBoxBenchValidated
                AddHandler DirectCast(e.Control, TextBox).Validated, AddressOf TextBoxBenchValidated

                RemoveHandler DirectCast(e.Control, TextBox).KeyPress, AddressOf TextBoxKeyPress

                AddHandler DirectCast(e.Control, TextBox).KeyPress, AddressOf TextBoxKeyPress


            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Function IsLineupsModified() As Boolean
        Try
            Dim DsExistingPlayers As DataSet
            Dim drsExisting() As DataRow
            Dim drsNew() As DataRow

            'fetching existing players from loacl DB
            DsExistingPlayers = m_objBLTeamSetup.GetRosterInfo(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.languageid)
            If DsExistingPlayers.Tables.Count > 0 Then
                For intTableCnt As Integer = 0 To DsExistingPlayers.Tables.Count - 1
                    drsExisting = DsExistingPlayers.Tables(intTableCnt).Select("Starting_Position is not null", "Starting_Position")
                    If drsExisting.Length > 0 Then
                        'fetching new players which are currently used
                        drsNew = DsRostersInfo.Tables(intTableCnt).Select("Starting_Position is not null", "Starting_Position")
                        If drsNew.Length > 0 Then
                            For intRowCount As Integer = 0 To drsExisting.Length - 1
                                If CInt(intRowCount) > CInt(drsNew.Length - 1) Then
                                    'issue occured when players are deselected and set to blanks
                                    Continue For
                                End If
                                'shirley July 20 for updating the lineups that is alreday sent to clients
                                If drsNew.Length <> drsExisting.Length Then
                                    BoolIsLineupsUpdated = True
                                End If

                                If CInt(drsExisting(intRowCount).Item("Starting_Position")) = CInt(drsNew(intRowCount).Item("Starting_position")) Then
                                    'if lineups are matching, then check for player match
                                    If CLng(drsExisting(intRowCount).Item("Player_id")) <> CLng(drsNew(intRowCount).Item("Player_id")) Then
                                        Dim drsCheck() As DataRow
                                        drsCheck = DsRostersInfo.Tables(intTableCnt).Select("Starting_Position is not null and Player_id = " & CLng(drsExisting(intRowCount).Item("Player_id")) & "")
                                        If drsCheck.Length > 0 Then
                                            '--players positions are changed
                                            BoolIsLineupsUpdated = True
                                        Else
                                            '-- check in pbp whether the player exists 
                                            BoolIsLineupsUpdated = True
                                            Dim intPlayerCount As Integer = m_objBLTeamSetup.GetplayerCount(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CInt(drsExisting(intRowCount).Item("Player_id")))
                                            If intPlayerCount = 0 Then
                                            Else
                                                'if player played any events, he should be updated with the new player
                                                'If MessageDialog.Show(drsExisting(intRowCount).Item("Last_Name").ToString & " " & drsExisting(intRowCount).Item("Moniker").ToString & " has existing Play by Play records. Are you sure to modify the lineup from " & drsExisting(intRowCount).Item("Player").ToString & "  to " & drsNew(intRowCount).Item("Player").ToString & " ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                                                If MessageDialog.Show(drsExisting(intRowCount).Item("Last_Name").ToString & " " & drsExisting(intRowCount).Item("Moniker").ToString & " has existing Play by Play records. Are you sure to modify the lineup from " & drsExisting(intRowCount).Item("Player").ToString & "  to " & drsNew(intRowCount).Item("Player").ToString & " ?" & vbNewLine & "Click Yes to update the Play-by-Play records. Click no to receive further option to cancel this change or to keep the current Play-by-Play records.", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                                                    ' code to update all the players with the new player
                                                    m_objBLTeamSetup.UpdateExisPBPPlayerWithNewPlayer(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.ReporterRole, CInt(drsExisting(intRowCount).Item("Player_id")), CInt(drsNew(intRowCount).Item("Player_id")))
                                                Else
                                                    If MessageDialog.Show(strmessage21 + " " & drsNew(intRowCount).Item("Player").ToString & strmessage22 + " ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                                        Return True
                                                    Else
                                                        Return False
                                                    End If
                                                    'false coming out of the function 
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next
            End If




            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function


    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            'LINEUP'S ARE ASSINGNED FOR AWAY TEAM PLAYERS 
            If UpdatePlayerLineupInfo() = False Then
                Exit Sub
            End If

            'shirley sep 28 validate un
            If ValidateUniformNumber() = False Then
                Exit Sub
            End If

            'VALIDATE THE DATAGRIDVIEW CELLS ONCE THE PLAYERS ARE SELECTED FOR STARTING LINEUPS
            If ValidateCellsBeforeSave() = False Then
                m_sendToClient = False
                Exit Sub
            End If

            If IsLineupsModified() = False Then
                'is lineup's modified then update the player id with the new one
                m_sendToClient = False
                Exit Sub
            End If

            'ASSIGNING THE VALUES TO THE PROPERTY DATASET
            If m_objTeamSetup.RosterInfo Is Nothing Then
                Dim Ds As New DataSet
                m_objTeamSetup.RosterInfo = Ds
            Else
                m_objTeamSetup.RosterInfo.Tables.Clear()
            End If

            'CREATES AN EMPTY TABLE WITH SCHEMA FROM DSRSOTERSINFO.
            m_objTeamSetup.RosterInfo = DsRostersInfo.Clone()

            If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                For intTableCount As Integer = 0 To m_objTeamSetup.RosterInfo.Tables.Count - 1
                    'ASSIGNING THE ROWS TO THE PROPERTY DATASET
                    If intTableCount <= 1 Then
                        Dim drs() As DataRow
                        drs = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL", "STARTING_POSITION")
                        For Each row As DataRow In drs
                            'IMPORTING THE ROWS FROM ONE TABLE TO ANOTHER
                            m_objTeamSetup.RosterInfo.Tables(intTableCount).ImportRow(row)
                        Next
                    End If
                Next
            End If
            m_objTeamSetup.RosterInfo.AcceptChanges()

            'XML CONVERTION AND UPDATION INTO DB
            Dim strXmlTeamSetupData As String = ""
            If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                Dim DsPlayers As DataSet
                'ASSIGNING THE DATA TO A NEW DATASET
                DsPlayers = m_objTeamSetup.RosterInfo.Copy()
                'MERGING BOTH HOME AND AWAY TEAM
                DsPlayers.Tables(0).Merge(DsPlayers.Tables(1))
                DsPlayers.Tables.RemoveAt(1)
                DsPlayers.DataSetName = "LineupData"
                'CONVERTING INTO XML DATA
                strXmlTeamSetupData = DsPlayers.GetXml()
                strXmlTeamSetupData = strXmlTeamSetupData.Replace("'", "''")
                'UPDATING THE LOCAL SQL
                If m_objGameDetails.ModuleID = 3 Then
                    m_objBLTeamSetup.UpdateLineupsAndOverwriteExisPlayerData(m_objclsGameDetails.GameCode, strXmlTeamSetupData, m_objGameDetails.ReporterRole)
                Else
                    m_objBLTeamSetup.UpdateRosterInfo(m_objclsGameDetails.GameCode, strXmlTeamSetupData)
                End If
                If m_sendToClient Then
                    m_objModule1Main.InsertPlayByPlayData(clsGameDetails.HomeStartingLineups, "")
                    m_objModule1Main.InsertPlayByPlayData(clsGameDetails.AwatStartingLineups, "")
                End If
                If m_sendToClient = False And BoolIsLineupsUpdated = True Then
                    If m_objBLTeamSetup.GetLineupEventCnt(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objGameDetails.ModuleID) > 0 Then
                        Dim dtTimeDiff As Date = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                        Dim StrTime As String = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
                        m_objBLTeamSetup.UpdateLineupEvent(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objGameDetails.ReporterRole, StrTime)
                    End If
                End If

            End If
            m_sendToClient = False
            BoolIsLineupsUpdated = False

            'INSERT FORMATION AND COACH DATA TO SQL
            Dim DsFormationCoach As New DataSet
            DsFormationCoach = dsTeamGame.Clone()

            For intTableCount As Integer = 0 To dsTeamGame.Tables.Count - 1
                Dim drs() As DataRow
                If intTableCount = 0 Then
                    drs = dsTeamGame.Tables(intTableCount).Select("HOME IS NOT NULL OR AWAY IS NOT NULL")
                ElseIf intTableCount = 1 Then
                    drs = dsTeamGame.Tables(intTableCount).Select("SELECTED IS NOT NULL ")
                ElseIf intTableCount = 3 Then
                    drs = dsTeamGame.Tables(intTableCount).Select("HOME IS NOT NULL OR AWAY IS NOT NULL")
                End If

                For Each row As DataRow In drs
                    'IMPORTING THE ROWS FROM ONE TABLE TO ANOTHER
                    DsFormationCoach.Tables(intTableCount).ImportRow(row)
                Next
            Next


            For inttablecount = 0 To DsFormationCoach.Tables.Count - 4
                Dim drSelecteDataTeamSetup As DataRow
                drSelecteDataTeamSetup = dsSelectedData.Tables(0).NewRow()
                drSelecteDataTeamSetup("GAME_CODE") = m_objclsGameDetails.GameCode
                drSelecteDataTeamSetup("FEED_NUMBER") = m_objclsGameDetails.FeedNumber
                drSelecteDataTeamSetup("SHOOTOUT_SCORE") = DBNull.Value
                'drSelecteDataTeamSetup("SHIRT_COLOR") = DBNull.Value
                'drSelecteDataTeamSetup("SHORT_COLOR") = DBNull.Value
                drSelecteDataTeamSetup("REPORTER_ROLE") = m_objclsGameDetails.ReporterRole
                drSelecteDataTeamSetup("PROCESSED") = "N"
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    drSelecteDataTeamSetup("DEMO_DATA") = "N"
                Else
                    drSelecteDataTeamSetup("DEMO_DATA") = "Y"
                End If


                If inttablecount = 0 Then
                    drSelecteDataTeamSetup("TEAM_ID") = m_objclsGameDetails.HomeTeamID

                    'drSelecteDataTeamSetup("SHIRT_COLOR") = m_objTeamSetup.HomeShirtColor.ToArgb
                    'drSelecteDataTeamSetup("SHORT_COLOR") = m_objTeamSetup.HomeShortColor.ToArgb

                    If Not m_objTeamSetup.HomeShirtColor = Color.Empty Then
                        drSelecteDataTeamSetup("SHIRT_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShirtColor)
                    End If
                    ' drSelecteDataTeamSetup("SHIRT_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShirtColor)
                    If Not m_objTeamSetup.HomeShortColor = Color.Empty Then
                        drSelecteDataTeamSetup("SHORT_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShortColor)
                    End If
                    'drSelecteDataTeamSetup("SHORT_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShortColor)
                    ''TOPZ-1116
                    If Not m_objTeamSetup.HomeShirtColorNo = Color.Empty Then
                        drSelecteDataTeamSetup("SHIRT_NO_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShirtColorNo)
                    End If

                    If m_HomeKit = -1 Then
                        drSelecteDataTeamSetup("KIT_NO") = DBNull.Value
                    Else
                        drSelecteDataTeamSetup("KIT_NO") = m_HomeKit
                    End If


                    Dim drFormation() As DataRow = DsFormationCoach.Tables(0).Select("HOME = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                    If drFormation.Length > 0 Then
                        'INSERTION OF FORMATION ID
                        drSelecteDataTeamSetup("START_FORMATION_ID") = drFormation(0).Item("FORMATION_ID")

                        Me.m_objGameDetails.HomeFormationId = CInt(drSelecteDataTeamSetup("START_FORMATION_ID").ToString)

                        'INSERTION OF FORMATION SPECIFICATION
                        Dim drForSpec() As DataRow = DsFormationCoach.Tables(3).Select("HOME = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                        If drForSpec.Length > 0 Then
                            drSelecteDataTeamSetup("FORMATION_SPEC_ID") = drForSpec(0).Item("FORMATION_SPEC_ID")
                        End If


                        Dim drCoach() As DataRow = DsFormationCoach.Tables(1).Select("TEAM_ID = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                        If drCoach.Length > 0 Then
                            'insertion of coach id
                            drSelecteDataTeamSetup("COACH_ID") = drCoach(0).Item("COACH_ID")
                        End If
                    Else
                        Dim drCoach() As DataRow = DsFormationCoach.Tables(1).Select("TEAM_ID = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                        If drCoach.Length > 0 Then
                            'insertion of coach id
                            drSelecteDataTeamSetup("COACH_ID") = drCoach(0).Item("COACH_ID")
                        End If
                    End If
                    drSelecteDataTeamSetup("CAPTAIN_ID") = IIf(m_objGameDetails.HomeTeamCaptainId = 0, Nothing, m_objGameDetails.HomeTeamCaptainId)
                Else
                    drSelecteDataTeamSetup("TEAM_ID") = m_objclsGameDetails.AwayTeamID

                    If Not m_objTeamSetup.AwayShirtColor = Color.Empty Then
                        drSelecteDataTeamSetup("SHIRT_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.AwayShirtColor)
                    End If
                    If Not m_objTeamSetup.AwayShortColor = Color.Empty Then
                        drSelecteDataTeamSetup("SHORT_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.AwayShortColor)
                    End If
                    ''TOPZ-1116
                    If Not m_objTeamSetup.AwayShirtColorNo = Color.Empty Then
                        drSelecteDataTeamSetup("SHIRT_NO_COLOR") = clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.AwayShirtColorNo)
                    End If
                    If m_AwayKit = -1 Then
                        drSelecteDataTeamSetup("KIT_NO") = DBNull.Value
                    Else
                        drSelecteDataTeamSetup("KIT_NO") = m_AwayKit
                    End If


                    Dim drFormation() As DataRow = DsFormationCoach.Tables(0).Select("AWAY = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                    If drFormation.Length > 0 Then
                        'INSERTION OF FORMATION ID
                        drSelecteDataTeamSetup("START_FORMATION_ID") = CInt(drFormation(0).Item("FORMATION_ID").ToString)

                        Me.m_objGameDetails.AwayFormationId = CInt(drSelecteDataTeamSetup("START_FORMATION_ID").ToString)


                        'INSERTION OF FORMATION SPECIFICATION
                        Dim drForSpec() As DataRow = DsFormationCoach.Tables(3).Select("AWAY = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                        If drForSpec.Length > 0 Then
                            drSelecteDataTeamSetup("FORMATION_SPEC_ID") = drForSpec(0).Item("FORMATION_SPEC_ID")
                        End If

                        Dim drCoach() As DataRow = DsFormationCoach.Tables(1).Select("TEAM_ID = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                        If drCoach.Length > 0 Then
                            'insertion of coach id
                            drSelecteDataTeamSetup("COACH_ID") = drCoach(0).Item("COACH_ID")
                        End If
                    Else
                        Dim drCoach() As DataRow = DsFormationCoach.Tables(1).Select("TEAM_ID = " & CInt(drSelecteDataTeamSetup("TEAM_ID")) & "")
                        If drCoach.Length > 0 Then
                            'insertion of coach id
                            drSelecteDataTeamSetup("COACH_ID") = drCoach(0).Item("COACH_ID")
                        End If
                    End If
                    drSelecteDataTeamSetup("CAPTAIN_ID") = IIf(m_objGameDetails.AwayTeamCaptainId = 0, Nothing, m_objGameDetails.AwayTeamCaptainId)
                End If


                dsSelectedData.Tables(0).Rows.Add(drSelecteDataTeamSetup)
            Next


            m_objBLTeamSetup.InsertUpdateTeamGame(dsSelectedData)

            ''Added by shirley on Aug 31 2010 for Multi rep comm
            'If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1"  And m_objGameDetails.ReporterRole <> "OPS" Then
            '    m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, True)
            '    m_objGameDetails.AssistersLastEntryStatus = False
            'End If

            'insert of onfield data
            '=========================================================
            'Dim strXmlTeamGameData As String = dsSelectedData.GetXml()
            'Dim StrXMlPlayerdata As String = m_objTeamSetup.RosterInfo.GetXml()
            'strXmlTeamSetupData = strXmlTeamSetupData.Replace("'", "''")
            'StrXMlPlayerdata = StrXMlPlayerdata.Replace("'", "''")
            'm_objBLTeamSetup.InsertOnfield(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, StrXMlPlayerdata, strXmlTeamGameData)
            ''=========================================================

            'CREATING TWO MORE TABLES WHICH IS USED TO HOLD THE CURRENT PLAYERS 
            'PLAYING IN THE FIELD
            Dim DsCurrentPlayer As DataSet
            DsCurrentPlayer = m_objTeamSetup.RosterInfo.Copy()
            DsCurrentPlayer.Tables(0).TableName = "HomeCurrent"
            DsCurrentPlayer.Tables(1).TableName = "AwayCurrent"
            m_objTeamSetup.RosterInfo.Merge(DsCurrentPlayer.Tables(0))
            m_objTeamSetup.RosterInfo.Merge(DsCurrentPlayer.Tables(1))
            m_objTeamSetup.RosterInfo.AcceptChanges()

            GetCurrentPlayersAfterRestart()

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 0)

            MessageDialog.Show(strmessage11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

            'DISBALING THE SAVE BUTTON AS SAVE BUTTON SHOULD BE CLICKED ONCE 
            'VISITING TEAM PLAYERS ARE SELECTED
            btnSave.Enabled = True

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                'Else
                '    frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                '    frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                '    frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)

                'End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            dsSelectedData.Tables(0).Rows.Clear()
        End Try
    End Sub

    Private Sub GetCurrentPlayersAfterRestart()
        Try
            'Getting the current players from red card and sub events
            Dim DsSubEvents As DataSet = m_objGeneral.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If DsSubEvents.Tables(0).Rows.Count > 0 Then
                For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                    Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                        Case 7, 22
                            For intTableCount As Integer = 2 To m_objTeamSetup.RosterInfo.Tables.Count - 1
                                Dim DrPlayerOut() As DataRow = m_objTeamSetup.RosterInfo.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) & "")
                                If DrPlayerOut.Length > 0 Then
                                    If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = 7 Then
                                        'RED CARD
                                        DrPlayerOut(0).BeginEdit()
                                        DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                        DrPlayerOut(0).EndEdit()
                                        DrPlayerOut(0).AcceptChanges()
                                    Else
                                        'SUBSTITUTION
                                        If IsDBNull(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) = False Then
                                            Dim DrPlayerIn() As DataRow = m_objTeamSetup.RosterInfo.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) & "")
                                            If DrPlayerIn.Length > 0 Then
                                                DrPlayerIn(0).BeginEdit()
                                                DrPlayerIn(0).Item("STARTING_POSITION") = DrPlayerOut(0).Item("STARTING_POSITION")
                                                DrPlayerIn(0).EndEdit()

                                                DrPlayerOut(0).BeginEdit()
                                                DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                                DrPlayerOut(0).EndEdit()

                                                DrPlayerIn(0).AcceptChanges()
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                    End Select
                Next
            End If

            m_objTeamSetup.RosterInfo.AcceptChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''reordering current players so as to place goal keepers at Top
    'Private Function ReOrderCurrentPlayers(ByVal DsCurrentPlayer As DataSet) As DataSet
    '    Try
    '        Dim DsCurrent As DataSet = DsCurrentPlayer.Clone()
    '        If DsCurrentPlayer.Tables.Count > 0 Then
    '            For intTableCount As Integer = 0 To DsCurrentPlayer.Tables.Count - 1
    '                Dim Drs() As DataRow
    '                Drs = DsCurrentPlayer.Tables(intTableCount).Select("POSITION_ID_1 = 1")
    '                For Each DR As DataRow In Drs
    '                    DsCurrent.Tables(intTableCount).ImportRow(DR)
    '                Next
    '                Drs = DsCurrentPlayer.Tables(intTableCount).Select("POSITION_ID_1 <> 1")
    '                For Each DR As DataRow In Drs
    '                    DsCurrent.Tables(intTableCount).ImportRow(DR)
    '                Next
    '            Next
    '        End If
    '        DsCurrent.AcceptChanges()
    '        Return DsCurrent

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Sub btnAddPlayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPlayer.Click
        Try
            'TOPZ-1474 ----begin
            Dim gr = CreateGraphics()
            Dim strText As String = ""
            Dim strMoniker As String = ""
            Dim strLastName As String = ""
            Dim strPositionAbbrev As String = ""
            Dim intPreferredWidth As Integer = 130
            'TOPZ-1474 ----end

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAddPlayer.Text, 1, 0)

            If DsRostersInfo IsNot Nothing Then
                'UPDATE LINUP INFO FOR THE SELECTED PLAYERS IN THE DATAGRIDVIEW
                UpdatePlayerLineupInfo()
            End If

            Dim StrTeam As String = ""
            If radAwayTeam.Checked = True Then
                StrTeam = "Away"
            Else
                StrTeam = "Home"
            End If
            Dim objAddPlayer As New frmAddPlayer(StrTeam)
            objAddPlayer.ShowDialog()

            'HOME AND AWAY TEAM ROSTERS ARE FETCHED ONCE A PLAYER IS NEWLY ADDED
            If DsRostersInfo IsNot Nothing Then
                Dim DsPlayersSel As DataSet
                DsPlayersSel = DsRostersInfo.Copy()
                DsRostersInfo.Tables.Clear()

                DsRostersInfo = m_objBLTeamSetup.GetRosterInfo(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.languageid)
                If DsRostersInfo.Tables.Count > 0 Then
                    DsRostersInfo.Tables(0).TableName = "Home"
                    DsRostersInfo.Tables(1).TableName = "Away"

                    'TOPZ-1474 ----begin
                    'trim long names so that they fit well in starting lineups and formations

                    'loop both teams
                    For tbl As Integer = 0 To 1
                        'loop all players
                        For rw As Integer = 0 To DsRostersInfo.Tables(tbl).Rows.Count - 1
                            strText = DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER")
                            If gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth Then
                                strMoniker = IIf(IsDBNull(DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER")), "", DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER"))
                                strLastName = DsRostersInfo.Tables(tbl).Rows(rw)("LAST_NAME")
                                strPositionAbbrev = DsRostersInfo.Tables(tbl).Rows(rw)("POSITION_ABBREV")

                                While gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth
                                    If strMoniker.Length > 1 Then
                                        strMoniker = strMoniker.Substring(0, strMoniker.Length - 1)
                                    Else
                                        strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                    End If
                                    strText = strLastName + ", " + strMoniker
                                    If Not (IsNothing(strPositionAbbrev) Or strPositionAbbrev = "") Then
                                        strText = strText + " (" + strPositionAbbrev + ")"
                                    End If
                                End While
                                DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER") = strText
                            End If
                        Next
                    Next
                    'TOPZ-1474 ----end

                    AddDgviewColumns(dgvStartingLineup)
                    AddDgviewRows("Starting Lineups")
                    AddDgviewColumns(dgvBench)
                    AddDgviewRows("Bench")
                    Dim DsFetPlayers As DataSet
                    DsFetPlayers = DsRostersInfo.Copy()
                    Dim DsNewPlayer As DataSet

                    DsNewPlayer = UpdateSelectedPlayer(DsFetPlayers, DsPlayersSel)
                    DsRostersInfo.Tables.Clear()
                    DsRostersInfo = DsNewPlayer.Copy()
                    SetPlayersInEachComboCell()
                    RetriveRostersFromLocal()
                    'clear the resouces
                    DsFetPlayers.Dispose()
                    DsNewPlayer.Dispose()
                    DsPlayersSel.Dispose()
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
        End Try
    End Sub

    Private Sub KitClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles picKit1.Click, picKit2.Click, picKit3.Click, picKit4.Click, picKit5.Click, picKit6.Click, picKit7.Click, picKit8.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                'ApplyChangesToDatagridview()
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            pnlHighlight.Visible = True
            pnlHighlight.Location = New Point(DirectCast(sender, PictureBox).Location.X - 1, DirectCast(sender, PictureBox).Location.Y - 1)
            If m_objclsGameDetails.HomeTeamID = intTeamID Then
                'm_HomeKit = CInt(DirectCast(sender, PictureBox).Tag)
                If DirectCast(sender, PictureBox).Tag Is Nothing Then
                    m_HomeKit = -1
                Else
                    m_HomeKit = CInt(DirectCast(sender, PictureBox).Tag)
                End If
                If m_HomeKit <> -1 Then
                    GetShirtAndShortColor(m_HomeKit, m_objclsGameDetails.HomeTeamID)
                    RefreshFormationDisplay()
                End If
            Else
                If DirectCast(sender, PictureBox).Tag Is Nothing Then
                    m_AwayKit = -1
                Else
                    m_AwayKit = CInt(DirectCast(sender, PictureBox).Tag)
                End If

                If m_AwayKit <> -1 Then
                    GetShirtAndShortColor(m_AwayKit, m_objclsGameDetails.AwayTeamID)
                    If UdcSoccerFormation1.btnApply.Enabled = True Then
                        'ApplyChangesToDatagridview()
                        MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Exit Sub
                    End If
                    RefreshFormationDisplay()
                End If
            End If


        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub


    Private Sub cmbFormation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFormation.SelectedIndexChanged
        Try
            If m_blnIsFormationComboLoaded = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbFormation.Text, lblFormation.Text, 1, 0)
                If cmbFormation.Items.Count > 0 Then
                    If cmbFormation.SelectedValue IsNot Nothing Then
                        LoadFormationSpecification(CInt(cmbFormation.SelectedValue.ToString))
                    End If
                End If

                RefreshFormationDisplay()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbFormationSpecification_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFormationSpecification.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbFormationSpecification.Text, lblFormation.Text, 1, 0)
            If m_blnCheck = True Then
                If UdcSoccerFormation1.btnApply.Enabled = True Then
                    'ApplyChangesToDatagridview()
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
            End If
            RefreshFormationDisplay()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbCoach_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCoach.SelectedIndexChanged
        Try
            If m_blnIsCoachComboLoaded = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbCoach.Text, lblCoach.Text, 1, 0)
                m_blnIsCoachComboLoaded = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' HANDLES THE EVENT OF ADD NEW MANAGER BUTTON
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnManager_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnManager.Click

        Try
            UpdatePlayerLineupInfo()
            Dim DSNew As New DataSet
            Dim StrTeam As String = ""
            If radAwayTeam.Checked = True Then
                StrTeam = "Away"
            Else
                StrTeam = "Home"
            End If

            Dim objAddManager As New frmAddManager(StrTeam)
            If objAddManager.ShowDialog() = Windows.Forms.DialogResult.Yes Then
                DSNew = m_objBLTeamSetup.GetFormationCoach(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTeamID)
                DSNew.Tables(1).TableName = "Coach"
                DSNew.Tables("Coach").Columns.Add("Selected")


                For intRowCnt As Integer = 0 To dsTeamGame.Tables("Coach").Rows.Count - 1
                    Dim drCoachSel() As DataRow = dsTeamGame.Tables("Coach").Select("Selected IS NOT NULL")
                    If drCoachSel.Length > 0 Then
                        For Each drSelData As DataRow In drCoachSel
                            Dim drNewCoach() As DataRow = DSNew.Tables("Coach").Select("Coach_ID = " & CInt(drSelData.Item("Coach_ID")) & "")
                            drNewCoach(0).BeginEdit()
                            drNewCoach(0).Item("Coach_ID") = drSelData.Item("Coach_ID")
                            drNewCoach(0).Item("Coach_Name") = drSelData.Item("Coach_Name")
                            drNewCoach(0).Item("Team_ID") = drSelData.Item("Team_ID")
                            drNewCoach(0).Item("Selected") = drSelData.Item("Selected")
                            drNewCoach(0).EndEdit()
                        Next
                    End If
                Next

                dsTeamGame.Tables("Coach").Clear()
                For Each dr As DataRow In DSNew.Tables("Coach").Rows
                    dsTeamGame.Tables("Coach").ImportRow(dr)
                Next

                UpdatePlayerLineupInfo()
                LoadCoach(intTeamID)
                SetPlayersInEachComboCell()
                RetriveRostersFromLocal()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''  Deletes the injured or Suspended players (Player_Status_id updated as Bench player in roster table
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'TOPZ-1474 ----begin
            Dim gr = CreateGraphics()
            Dim strText As String = ""
            Dim strMoniker As String = ""
            Dim strLastName As String = ""
            Dim strPositionAbbrev As String = ""
            Dim intPreferredWidth As Integer = 130
            'TOPZ-1474 ----end

            Dim intPlayerID As Integer
            UpdatePlayerLineupInfo()
            If dgInjuriesSuspensions.Rows.Count > 0 Then
                If dgInjuriesSuspensions.SelectedRows.Count = 1 Then
                    If MessageDialog.Show(strmessage10 + " ' " & CType(dgInjuriesSuspensions.SelectedRows.Item(0), DataGridViewRow).Cells(2).Value.ToString & " ' " & vbNewLine & "     from Injuries and Suspensions ?", lblInjuriesSuspensions.Text.Replace(":", " ") & " - Delete ", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, lblInjuriesSuspensions.Text, 1, 0)


                        intPlayerID = CInt(CType(dgInjuriesSuspensions.SelectedRows.Item(0), DataGridViewRow).Cells(0).Value)
                        m_objBLTeamSetup.DeleteInjuredOrSuspendedPlayer(m_objclsGameDetails.LeagueID, intTeamID, intPlayerID)
                        dgInjuriesSuspensions.Rows.Remove(dgInjuriesSuspensions.SelectedRows(0))

                        If DsRostersInfo IsNot Nothing Then
                            Dim DsPlayersSel As DataSet
                            DsPlayersSel = DsRostersInfo.Copy()
                            DsRostersInfo.Tables.Clear()

                            DsRostersInfo = m_objBLTeamSetup.GetRosterInfo(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.languageid)
                            If DsRostersInfo.Tables.Count > 0 Then
                                DsRostersInfo.Tables(0).TableName = "Home"
                                DsRostersInfo.Tables(1).TableName = "Away"

                                'TOPZ-1474 ----begin
                                'trim long names so that they fit well in starting lineups and formations

                                'loop both teams
                                For tbl As Integer = 0 To 1
                                    'loop all players
                                    For rw As Integer = 0 To DsRostersInfo.Tables(tbl).Rows.Count - 1
                                        strText = DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER")
                                        If gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth Then
                                            strMoniker = IIf(IsDBNull(DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER")), "", DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER"))
                                            strLastName = DsRostersInfo.Tables(tbl).Rows(rw)("LAST_NAME")
                                            strPositionAbbrev = DsRostersInfo.Tables(tbl).Rows(rw)("POSITION_ABBREV")

                                            While gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth
                                                If strMoniker.Length > 1 Then
                                                    strMoniker = strMoniker.Substring(0, strMoniker.Length - 1)
                                                Else
                                                    strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                                End If
                                                strText = strLastName + ", " + strMoniker
                                                If Not (IsNothing(strPositionAbbrev) Or strPositionAbbrev = "") Then
                                                    strText = strText + " (" + strPositionAbbrev + ")"
                                                End If
                                            End While
                                            DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER") = strText
                                        End If
                                    Next
                                Next
                                'TOPZ-1474 ----end

                                AddDgviewColumns(dgvStartingLineup)
                                AddDgviewRows("Starting Lineups")
                                AddDgviewColumns(dgvBench)
                                AddDgviewRows("Bench")
                                Dim DsFetPlayers As DataSet
                                DsFetPlayers = DsRostersInfo.Copy()
                                Dim DsNewPlayer As DataSet
                                DsNewPlayer = UpdateSelectedPlayer(DsFetPlayers, DsPlayersSel)
                                DsRostersInfo.Tables.Clear()
                                DsRostersInfo = DsNewPlayer.Copy()
                                SetPlayersInEachComboCell()

                                RetriveRostersFromLocal()
                                'clear the resouces
                                DsFetPlayers.Dispose()
                                DsNewPlayer.Dispose()
                                DsPlayersSel.Dispose()
                                RefreshFormationDisplay()
                            End If
                        End If
                    End If
                Else
                    MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnShirtColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShirtColor.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                ' ApplyChangesToDatagridview()
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If cdlgShirtShort.ShowDialog = Windows.Forms.DialogResult.OK Then
                lblShirtColor.BackColor = cdlgShirtShort.Color
                lblShirtColor.Text = ""
                If radHomeTeam.Checked Then
                    m_objTeamSetup.HomeShirtColor = cdlgShirtShort.Color
                Else
                    m_objTeamSetup.AwayShirtColor = cdlgShirtShort.Color
                End If

                RefreshFormationDisplay()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CheckPlayersCount()
        Try
            Dim DrsRosters() As DataRow
            If UdcSoccerFormation1.USFStartingLineup.Rows.Count > 0 Then
                DrsRosters = DsRostersInfo.Tables(GetTableName).Select("STARTING_POSITION IS NOT NULL")
                If UdcSoccerFormation1.USFStartingLineup.Rows.Count <> DrsRosters.Length Then
                    If MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                        'automatically all players in formation will be overwritten
                    Else 'No is clicked

                    End If

                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub ApplyChangesToDatagridview()
        Try
            Dim DrsRosters() As DataRow
            Dim drs() As DataRow
            If Not UdcSoccerFormation1.USFStartingLineup Is Nothing Then
                If UdcSoccerFormation1.USFStartingLineup.Rows.Count > 0 Then
                    'filtering only those players selected in formations
                    drs = UdcSoccerFormation1.USFStartingLineup.Select("LINEUP IS NOT NULL", "LINEUP")
                    If drs.Length > 0 Then
                        'fetch each row form user control and update the roster dataset
                        For Each DR As DataRow In drs
                            DrsRosters = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = " & CInt(DR.Item("PLAYER_ID")) & "")
                            If DrsRosters.Length > 0 Then
                                DrsRosters(0).BeginEdit()
                                DrsRosters(0).Item("STARTING_POSITION") = DR.Item("LINEUP")
                                DrsRosters(0).EndEdit()
                            End If
                        Next
                        DsRostersInfo.Tables(GetTableName).AcceptChanges()

                        SetPlayersInEachComboCell()
                        If UdcSoccerFormation1.btnApply.Enabled = True Then
                            UdcSoccerFormation1.btnApply.Enabled = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnShortColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShortColor.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                'ApplyChangesToDatagridview()
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If cdlgShirtShort.ShowDialog = Windows.Forms.DialogResult.OK Then

                lblShortColor.BackColor = cdlgShirtShort.Color
                lblShortColor.Text = ""
                If radHomeTeam.Checked Then
                    m_objTeamSetup.HomeShortColor = cdlgShirtShort.Color
                Else
                    m_objTeamSetup.AwayShortColor = cdlgShirtShort.Color
                End If

                RefreshFormationDisplay()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClearShirtColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearShirtColor.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                'ApplyChangesToDatagridview()
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            lblShirtColor.BackColor = Nothing
            lblShirtColor.Text = "(None)"

            If radHomeTeam.Checked Then
                m_objTeamSetup.HomeShirtColor = Color.Empty
            Else
                m_objTeamSetup.AwayShirtColor = Color.Empty
            End If
            RefreshFormationDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    'TOPZ-1116
    Private Sub btnShirtNoColor_Click(sender As Object, e As EventArgs) Handles btnShirtNoColor.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If cdlgShirtShort.ShowDialog = Windows.Forms.DialogResult.OK Then
                lblShirtNoColor.BackColor = cdlgShirtShort.Color
                lblShirtNoColor.Text = ""
                If radHomeTeam.Checked Then
                    m_objTeamSetup.HomeShirtColorNo = cdlgShirtShort.Color
                Else
                    m_objTeamSetup.AwayShirtColorNo = cdlgShirtShort.Color
                End If

                RefreshFormationDisplay()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    'TOPZ-1116
    Private Sub btnClearShirtNoColor_Click(sender As Object, e As EventArgs) Handles btnClearShirtNoColor.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            lblShirtNoColor.BackColor = Nothing
            lblShirtNoColor.Text = "(None)"

            If radHomeTeam.Checked Then
                m_objTeamSetup.HomeShirtColorNo = Color.Empty
            Else
                m_objTeamSetup.AwayShirtColorNo = Color.Empty
            End If
            RefreshFormationDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClearShortColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearShortColor.Click
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                'ApplyChangesToDatagridview()
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            lblShortColor.BackColor = Nothing
            lblShortColor.Text = "(None)"

            If radHomeTeam.Checked Then
                m_objTeamSetup.HomeShortColor = Nothing
            Else
                m_objTeamSetup.AwayShortColor = Nothing
            End If
            RefreshFormationDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerFormation1_USFApplyClick(ByVal sender As Object, ByVal e As SFEventArgs) Handles UdcSoccerFormation1.USFApplyClick
        Try
            'write code to refresh starting lineup grid with data from the datatable e.USFStartingLineup here...
            '......
            'If e.USFStartingLineup.Rows.Count > 0 Then
            '    'UpdatePlayerLineupInfo()
            '    'AddGridViewColumnsAndRows()
            '    Dim drs() As DataRow = e.USFStartingLineup.Select("LINEUP IS NOT NULL")
            '    If drs.Length > 0 Then
            '        Dim intLineupNo As Integer = 1
            '        For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
            '            drs = e.USFStartingLineup.Select("LINEUP = " & intLineupNo & "")
            '            If drs.Length > 0 Then
            '                dgvStartingLineup.Rows(i).Cells(1).Value = drs(0).Item("UNIFORM")
            '                dgvStartingLineup.Rows(i).Cells(2).Value = drs(0).Item("PLAYER_ID")
            '            End If
            '            intLineupNo += 1
            '        Next
            '    End If
            'End If
            Dim DrsRosters() As DataRow
            Dim drs() As DataRow
            If e.USFStartingLineup.Rows.Count > 0 Then
                'filtering only those players selected in formations
                drs = e.USFStartingLineup.Select("LINEUP IS NOT NULL", "LINEUP")
                UpdateRostersLineuptoNull()
                If drs.Length > 0 Then
                    'fetch each row form user control and update the roster dataset
                    For Each DR As DataRow In drs
                        DrsRosters = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = " & CInt(DR.Item("PLAYER_ID")) & "")
                        If DrsRosters.Length > 0 Then
                            DrsRosters(0).BeginEdit()
                            DrsRosters(0).Item("STARTING_POSITION") = DR.Item("LINEUP")
                            DrsRosters(0).EndEdit()
                        End If
                    Next
                    DsRostersInfo.Tables(GetTableName).AcceptChanges()

                    SetPlayersInEachComboCell()
                    ''assigning the selected players to the lineups
                    'Dim intLineupNo As Integer = 1
                    'For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                    '    drs = e.USFStartingLineup.Select("LINEUP = " & intLineupNo & "")
                    '    If drs.Length > 0 Then
                    '        dgvStartingLineup.Rows(i).Cells(1).Value = drs(0).Item("UNIFORM")
                    '        dgvStartingLineup.Rows(i).Cells(2).Value = drs(0).Item("PLAYER_ID")
                    '    End If
                    '    intLineupNo += 1
                    'Next
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    ''' <summary>
    ''' fetching Rosters from database and storing it in a Dataset
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AddGridViewColumnsAndRows()
        Try
            'TOPZ-1474 ----begin
            Dim gr = CreateGraphics()
            Dim strText As String = ""
            Dim strMoniker As String = ""
            Dim strLastName As String = ""
            Dim strPositionAbbrev As String = ""
            Dim intPreferredWidth As Integer = 130
            'TOPZ-1474 ----end

            If DsRostersInfo IsNot Nothing Then
                'IF ROSTERS ARE FILLED ALREADY THEN BLANK ROWS ARE CREATED IN THE DATAGRIDVIEW 
                AddDgviewColumns(dgvStartingLineup)
                AddDgviewRows("Starting Lineups")
            Else
                'HOME AND AWAY TEAM ROSTERS AND FETCHED AND STORED IN A DATASET
                DsRostersInfo = m_objBLTeamSetup.GetRosterInfo(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.languageid)
                If DsRostersInfo.Tables.Count > 0 Then
                    DsRostersInfo.Tables(0).TableName = "Home"
                    DsRostersInfo.Tables(1).TableName = "Away"

                    'TOPZ-1474 ----begin
                    'trim long names so that they fit well in starting lineups and formations

                    'loop both teams
                    For tbl As Integer = 0 To 1
                        'loop all players
                        For rw As Integer = 0 To DsRostersInfo.Tables(tbl).Rows.Count - 1
                            strText = DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER")
                            If gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth Then
                                strMoniker = IIf(IsDBNull(DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER")), "", DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER"))
                                strLastName = DsRostersInfo.Tables(tbl).Rows(rw)("LAST_NAME")
                                strPositionAbbrev = DsRostersInfo.Tables(tbl).Rows(rw)("POSITION_ABBREV")

                                While gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth
                                    If strMoniker.Length > 1 Then
                                        strMoniker = strMoniker.Substring(0, strMoniker.Length - 1)
                                    Else
                                        strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                    End If
                                    strText = strLastName + ", " + strMoniker
                                    If Not (IsNothing(strPositionAbbrev) Or strPositionAbbrev = "") Then
                                        strText = strText + " (" + strPositionAbbrev + ")"
                                    End If
                                End While
                                DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER") = strText
                            End If
                        Next
                    Next
                    'TOPZ-1474 ----end

                    AddDgviewColumns(dgvStartingLineup)
                    AddDgviewRows("Starting Lineups")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub getCoachKitInfo()
        Try
            'Dim dsCoachKitInfo As DataSet
            dsCoachKitInfo = m_objBLTeamSetup.GetFormationCoach(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTeamID)
            If dsCoachKitInfo.Tables(2).Rows.Count = 0 Then
                m_HomeCoach = 0
                m_AwayCoach = 0
                m_HomeKit = -1
                m_AwayKit = -1
            End If

            If dsCoachKitInfo.Tables(2).Rows.Count > 0 Then

                If CInt(dsCoachKitInfo.Tables(2).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                    m_HometeamID = m_objclsGameDetails.HomeTeamID
                    If dsCoachKitInfo.Tables(2).Rows(0).Item("COACH_ID").ToString() = "" Then
                        m_HomeCoach = 0
                    Else
                        m_HomeCoach = CInt(dsCoachKitInfo.Tables(2).Rows(0).Item("COACH_ID"))
                    End If

                    If dsCoachKitInfo.Tables(2).Rows(0).Item("KIT_NO").ToString() = "" Then
                        m_HomeKit = -1
                    Else
                        m_HomeKit = CInt(dsCoachKitInfo.Tables(2).Rows(0).Item("KIT_NO"))
                    End If


                    If dsCoachKitInfo.Tables(2).Rows.Count > 1 Then
                        m_AwayteamID = m_objclsGameDetails.AwayTeamID
                        If dsCoachKitInfo.Tables(2).Rows(1).Item("COACH_ID").ToString() = "" Then
                            m_AwayCoach = 0
                        Else
                            m_AwayCoach = CInt(dsCoachKitInfo.Tables(2).Rows(1).Item("COACH_ID"))
                        End If


                        If dsCoachKitInfo.Tables(2).Rows(1).Item("KIT_NO").ToString() = "" Then
                            m_AwayKit = -1
                        Else
                            m_AwayKit = CInt(dsCoachKitInfo.Tables(2).Rows(1).Item("KIT_NO"))
                        End If

                    End If
                Else
                    m_AwayteamID = m_objclsGameDetails.AwayTeamID
                    If dsCoachKitInfo.Tables(2).Rows(0).Item("COACH_ID").ToString() = "" Then
                        m_AwayCoach = 0
                    Else
                        m_AwayCoach = CInt(dsCoachKitInfo.Tables(2).Rows(0).Item("COACH_ID"))
                    End If

                    If dsCoachKitInfo.Tables(2).Rows(0).Item("KIT_NO").ToString() = "" Then
                        m_AwayKit = -1
                    Else
                        m_AwayKit = CInt(dsCoachKitInfo.Tables(2).Rows(0).Item("KIT_NO"))
                    End If


                    If dsCoachKitInfo.Tables(2).Rows.Count > 1 Then
                        m_HometeamID = m_objclsGameDetails.HomeTeamID
                        If dsCoachKitInfo.Tables(2).Rows(1).Item("COACH_ID").ToString() = "" Then
                            m_HomeCoach = 0
                        Else
                            m_HomeCoach = CInt(dsCoachKitInfo.Tables(2).Rows(1).Item("COACH_ID"))
                        End If

                        If dsCoachKitInfo.Tables(2).Rows(1).Item("KIT_NO").ToString() = "" Then
                            m_HomeKit = -1
                        Else
                            m_HomeKit = CInt(dsCoachKitInfo.Tables(2).Rows(1).Item("KIT_NO"))
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub GetShirtAndShortColor(ByVal intKitNo As Integer, ByVal TeamId As Integer)
        Try
            Dim dsColors As New DataSet
            dsColors = m_objBLTeamSetup.GetShirtandShortColors(intKitNo, TeamId)
            If dsColors IsNot Nothing Then
                If dsColors.Tables(0).Rows.Count > 0 Then
                    Dim drHome() As DataRow
                    drHome = dsColors.Tables(0).Select("Team_ID = " & m_objGameDetails.HomeTeamID)
                    If drHome.Length > 0 Then
                        If drHome(0).Item("SHIRT_COLOR").ToString <> "" Then
                            If drHome(0).Item("SHIRT_COLOR").ToString.Length < 7 Then
                                m_objTeamSetup.HomeShirtColor = Color.Empty
                                lblShirtColor.BackColor = Color.Empty
                                lblShirtColor.Text = "(None)"
                            Else
                                m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(drHome(0).Item("SHIRT_COLOR").ToString)
                                lblShirtColor.BackColor = m_objTeamSetup.HomeShirtColor
                                lblShirtColor.Text = ""
                            End If
                        Else
                            m_objTeamSetup.HomeShirtColor = Color.Empty
                            lblShirtColor.BackColor = Color.Empty
                            lblShirtColor.Text = "(None)"
                        End If
                        If drHome(0).Item("SHORT_COLOR").ToString <> "" Then
                            If drHome(0).Item("SHORT_COLOR").ToString.Length < 7 Then
                                m_objTeamSetup.HomeShortColor = Color.Empty
                                lblShortColor.BackColor = Color.Empty
                                lblShortColor.Text = "(None)"
                            Else
                                m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(drHome(0).Item("SHORT_COLOR").ToString)
                                lblShortColor.BackColor = m_objTeamSetup.HomeShortColor
                                lblShortColor.Text = ""
                            End If
                        Else
                            m_objTeamSetup.HomeShortColor = Color.Empty
                            lblShortColor.BackColor = Color.Empty
                            lblShortColor.Text = "(None)"
                        End If
                    End If

                    Dim drAway() As DataRow
                    drAway = dsColors.Tables(0).Select("Team_ID = " & m_objGameDetails.AwayTeamID)
                    If drAway.Length > 0 Then
                        If drAway(0).Item("SHIRT_COLOR").ToString <> "" Then
                            If drAway(0).Item("SHIRT_COLOR").ToString.Length < 7 Then
                                m_objTeamSetup.AwayShirtColor = Color.Empty
                                lblShirtColor.BackColor = Color.Empty
                                lblShirtColor.Text = "(None)"
                            Else
                                m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(drAway(0).Item("SHIRT_COLOR").ToString)
                                lblShirtColor.BackColor = m_objTeamSetup.AwayShirtColor
                                lblShirtColor.Text = ""
                            End If
                        Else
                            m_objTeamSetup.AwayShirtColor = Color.Empty
                            lblShirtColor.BackColor = Color.Empty
                            lblShirtColor.Text = "(None)"
                        End If
                        If drAway(0).Item("SHORT_COLOR").ToString <> "" Then
                            If drAway(0).Item("SHORT_COLOR").ToString.Length < 7 Then
                                m_objTeamSetup.AwayShortColor = Color.Empty
                                lblShortColor.BackColor = Color.Empty
                                lblShortColor.Text = "(None)"
                            Else
                                m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(drAway(0).Item("SHORT_COLOR").ToString)
                                lblShortColor.BackColor = m_objTeamSetup.AwayShortColor
                                lblShortColor.Text = ""
                            End If
                        Else
                            m_objTeamSetup.AwayShortColor = Color.Empty
                            lblShortColor.BackColor = Color.Empty
                            lblShortColor.Text = "(None)"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UdcSoccerFormation1_USFDragDrop(ByVal sender As Object, ByVal e As SFDDEventArgs) Handles UdcSoccerFormation1.USFDragDrop
        Try
            'FETCHING UNIFORM NUMBER SO AS TO DISPLAY IN DATAGRISVIEW
            Dim m_strTeamAbbrev As String
            Dim dr() As DataRow
            DR = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = " & e.USFPlayerId & "")
            If DR.Length > 0 Then
                m_strTeamAbbrev = dr(0).Item("TEAM_ABBREV").ToString
            End If
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.DragDrop, e.USFSource.ToString() & " -> " & e.USFDestination.ToString() & " : " & e.USFPlayerName & " (" & m_strTeamAbbrev & ")" & " (" & e.USFPlayerId & ")", 1, 0)
        Catch

        End Try
    End Sub

    Private Sub UdcSoccerFormation1_USFResetClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles UdcSoccerFormation1.USFResetClick
        Try
            If MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                RefreshFormationDisplay()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'Private Sub dgvStartingLineup_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStartingLineup.CellEndEdit
    '    Try
    '        If dgvStartingLineup.Columns(e.ColumnIndex).Index = 2 Then
    '            If dgvStartingLineup.CurrentRow.Cells(2).Value Is Nothing Then
    '                essageDialog.Show("Starting Lineup Player cannot be empty", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
    '                Exit Sub
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub dgvStartingLineup_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStartingLineup.CellValidated
    '    Try
    '        'VALIDATION FOR THE CELLS 
    '        If dgvStartingLineup.Columns(e.ColumnIndex).Index = 2 Then
    '            If dgvStartingLineup.CurrentRow.Cells(2).Value IsNot Nothing AndAlso String.IsNullOrEmpty(dgvStartingLineup.CurrentRow.Cells(2).Value.ToString()) Then
    '                essageDialog.Show("Starting Lineup Player cannot be empty", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub dgvStartingLineup_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvStartingLineup.CellValidating
    '    Try
    '        'VALIDATION FOR THE CELLS 
    '        If dgvStartingLineup.Columns(e.ColumnIndex).Index = 2 Then
    '            If e.FormattedValue IsNot Nothing AndAlso String.IsNullOrEmpty(e.FormattedValue.ToString()) Then
    '                essageDialog.Show("Starting Lineup Player cannot be empty", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub dgvBench_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvBench.CellValidating
    '    Try
    '        'VALIDATION FOR THE CELLS 
    '        If dgvBench.Columns(e.ColumnIndex).Index = 2 Then
    '            If e.FormattedValue IsNot Nothing AndAlso String.IsNullOrEmpty(e.FormattedValue.ToString()) And CInt(dgvBench.CurrentRow.Cells(0).Value) < 18 Then
    '                essageDialog.Show("Player Name must not be empty", Me.Text)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub



#End Region

#Region "User Defined Functions"

    ''' <summary>
    ''' FETCHING TEAM ID BY PASSING TEAM NAME AS PARAMETER
    ''' </summary>
    ''' <param name="Teamname"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamID(ByVal Teamname As String)
        Try
            If Teamname = m_objclsGameDetails.HomeTeam Then
                intTeamID = m_objclsGameDetails.HomeTeamID
            Else
                intTeamID = m_objclsGameDetails.AwayTeamID
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' UPDATES THE SELECTED PLAYER , BEFORE MOVING TO ADD PLAYER SCREEN
    ''' </summary>
    ''' <param name="DsNewPlayer"></param>
    ''' <param name="DsPlayerSel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Private Function UpdateSelectedPlayer(ByVal DsNewPlayer As DataSet, ByVal DsPlayerSel As DataSet) As DataSet
        Try
            For intTableCount As Integer = 0 To DsPlayerSel.Tables.Count - 1
                Dim drPlayerSel() As DataRow = DsPlayerSel.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL")
                If drPlayerSel.Length > 0 Then
                    For Each drSelData As DataRow In drPlayerSel
                        Dim drNewPlayer() As DataRow = DsNewPlayer.Tables(intTableCount).Select("PLAYER_ID <> " & CInt(drSelData.Item("PLAYER_ID")) & "AND STARTING_POSITION = " & CInt(drSelData.Item("STARTING_POSITION")) & "")
                        If drNewPlayer.Length > 0 Then 'MISMATCH - need to change starting position for both players!
                            'Find current posion of 'selected' player in 'fetched' dataset
                            Dim drSelPl() As DataRow = DsPlayerSel.Tables(intTableCount).Select("PLAYER_ID = " & CInt(drNewPlayer(0).Item("PLAYER_ID")) & "")

                            If drSelPl.Length > 0 Then
                                drNewPlayer(0).BeginEdit()
                                drNewPlayer(0).Item("STARTING_POSITION") = drSelPl(0).Item("STARTING_POSITION")
                                drNewPlayer(0).Item("RATING") = drSelPl(0).Item("RATING")
                                drNewPlayer(0).Item("PLAYER_STATUS_ID") = drSelPl(0).Item("PLAYER_STATUS_ID")
                                drNewPlayer(0).EndEdit()
                            Else
                                drNewPlayer(0).BeginEdit()
                                drNewPlayer(0).Item("STARTING_POSITION") = System.DBNull.Value
                                drNewPlayer(0).EndEdit()
                            End If

                            Dim drFetchPl() As DataRow = DsNewPlayer.Tables(intTableCount).Select("PLAYER_ID = " & CInt(drSelData.Item("PLAYER_ID")) & "")
                            If drFetchPl.Length > 0 Then
                                drFetchPl(0).BeginEdit()
                                drFetchPl(0).Item("STARTING_POSITION") = drSelData.Item("STARTING_POSITION")
                                drFetchPl(0).Item("RATING") = drSelData.Item("RATING")
                                drFetchPl(0).Item("PLAYER_STATUS_ID") = drSelData.Item("PLAYER_STATUS_ID")
                                drFetchPl(0).EndEdit()
                            End If
                            'Else
                            '    drNewPlayer(0).BeginEdit()
                            '    drNewPlayer(0).Item("STARTING_POSITION") = drSelData.Item("STARTING_POSITION")
                            '    drNewPlayer(0).Item("RATING") = drSelData.Item("RATING")
                            '    drNewPlayer(0).Item("PLAYER_STATUS_ID") = drSelData.Item("PLAYER_STATUS_ID")
                            '    drNewPlayer(0).EndEdit()
                        Else
                            Dim drNewPlayer1() As DataRow = DsNewPlayer.Tables(intTableCount).Select("PLAYER_ID = " & CInt(drSelData.Item("PLAYER_ID")) & "")
                            If drNewPlayer1.Length > 0 Then
                                If drNewPlayer1.Length > 0 Then
                                    drNewPlayer1(0).BeginEdit()
                                    drNewPlayer1(0).Item("STARTING_POSITION") = drSelData.Item("STARTING_POSITION")
                                    drNewPlayer1(0).Item("RATING") = drSelData.Item("RATING")
                                    drNewPlayer1(0).Item("PLAYER_STATUS_ID") = drSelData.Item("PLAYER_STATUS_ID")
                                    drNewPlayer1(0).EndEdit()
                                End If
                            End If

                        End If

                    Next
                End If
            Next
            DsNewPlayer.AcceptChanges()
            Return DsNewPlayer
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    'HANDLING OF SUBSTITUTE PLAYERS AND REORDERING THEM IN THE DATASET
    Private Sub ReorderPlayers()
        Try
            Dim intEvent As Integer = 22
            Dim Player_inID As Long
            Dim Player_OutID As Long
            Dim DR() As DataRow
            Dim DRS() As DataRow

            If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                m_objTeamSetup.RosterInfo.Tables(2).Columns.Add("SUB")
                m_objTeamSetup.RosterInfo.Tables(3).Columns.Add("SUB")
                DR = m_objTeamSetup.RosterInfo.Tables(2).Select("PLAYER_ID = " & Player_inID & "")
                If DR.Length > 0 Then
                    DR(0).BeginEdit()
                    DRS = m_objTeamSetup.RosterInfo.Tables(2).Select("PLAYER_ID = " & Player_OutID & "")
                    DR(0).Item("SUB") = DRS(0).Item("STARTING_POSITION")
                    DR(0).EndEdit()

                    DRS(0).BeginEdit()
                    DRS(0).Item("SUB") = DR(0).Item("STARTING_POSITION")
                    DRS(0).EndEdit()
                Else
                    DR = m_objTeamSetup.RosterInfo.Tables(3).Select("PLAYER_ID = " & Player_inID & "")
                    If DR.Length > 0 Then
                        DR(0).BeginEdit()
                        DRS = m_objTeamSetup.RosterInfo.Tables(2).Select("PLAYER_ID = " & Player_OutID & "")
                        DR(0).Item("SUB") = DRS(0).Item("STARTING_POSITION")
                        DR(0).EndEdit()

                        DRS(0).BeginEdit()
                        DRS(0).Item("SUB") = DR(0).Item("STARTING_POSITION")
                        DRS(0).EndEdit()
                    End If
                End If
            End If
            m_objTeamSetup.RosterInfo.AcceptChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'automatically fills the players if unifrom number is entered
    Private Sub FillStLineupPlayerCombo()
        Try
            Dim dr() As DataRow
            Dim StrTableName As String = ""
            StrTableName = GetTableName()
            If dgvStartingLineup.CurrentRow.Cells(1).Value.ToString <> "" Then
                'OCt 6 2010 shirley
                dr = DsRostersInfo.Tables(StrTableName).Select("DISPLAY_UNIFORM_NUMBER = '" & dgvStartingLineup.CurrentRow.Cells(1).Value.ToString & "' and STARTING_POSITION IS NULL")
                If dr.Length > 0 Then
                    If Not dgvStartingLineup.CurrentRow.Cells(2).Value Is Nothing Then
                        If dgvStartingLineup.CurrentRow.Cells(2).Value.ToString <> "" Then
                            If dgvStartingLineup.CurrentRow.Cells(2).Value.ToString <> dr(0).Item("PLAYER_ID").ToString Then
                                If MessageDialog.Show("Click YES to change the display uniform number for player " & dgvStartingLineup.CurrentRow.Cells(2).FormattedValue.ToString & " or Click NO to select " & dgvStartingLineup.CurrentRow.Cells(1).Value.ToString & " - " & CStr(dr(0).Item("PLAYER")), Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                                Else
                                    dgvStartingLineup.CurrentRow.Cells(2).Value = dr(0).Item("PLAYER_ID")
                                End If
                            End If
                        Else
                            dgvStartingLineup.CurrentRow.Cells(2).Value = dr(0).Item("PLAYER_ID")
                        End If
                    Else
                        dgvStartingLineup.CurrentRow.Cells(2).Value = dr(0).Item("PLAYER_ID")
                    End If
                End If
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'automatically fills the players if unifrom number is entered
    Private Sub FillBenchPlayerCombo()
        Try
            Dim dr() As DataRow
            Dim StrTableName As String = ""
            StrTableName = GetTableName()
            If dgvBench.CurrentRow.Cells(1).Value.ToString <> "" Then
                dr = DsRostersInfo.Tables(StrTableName).Select("DISPLAY_UNIFORM_NUMBER = '" & dgvBench.CurrentRow.Cells(1).Value.ToString & "' and STARTING_POSITION IS NULL")
                If dr.Length > 0 Then
                    dgvBench.CurrentRow.Cells(2).Value = dr(0).Item("PLAYER_ID")
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' UPDATES PLAYER LINEUP NUMBER IN THE DATASET FOR THE SELCETED PLAYER IN THE DATAGRIDVIEW
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function UpdatePlayerLineupInfo() As Boolean
        Try
            Dim DR() As DataRow
            Dim StrTableName As String = ""

            ''VALIDATION FOR POSITION (GK SHOULD BE ONLY ONE FOR A TEAM)
            'Dim intPosCount As Integer = 0
            'Dim intPlayerCount As Integer = 0
            'For intRowCount As Integer = 0 To dgvStartingLineup.Rows.Count - 1
            '    If CInt(dgvStartingLineup.Rows(intRowCount).Cells(4).Value) = 1 Then
            '        intPosCount += 1
            '    ElseIf CInt(dgvStartingLineup.Rows(intRowCount).Cells(2).Value) > 0 Then
            '        intPlayerCount += 1
            '    End If
            'Next
            'If intPosCount > 1 Then
            '    essageDialog.Show("More than one GoalKeeper is selected. Please Check !! ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
            '    Return False
            '    Exit Function
            'ElseIf intPosCount = 0 And intPlayerCount > 0 Then
            '    If essageDialog.Show("GoalKeeper is not selected for " & lblTeam.Text & " Team. Press YES to select a GoalKeeper. Press NO to Continue!! ", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            '        Return False
            '        Exit Function
            '    End If
            'End If

            UpdateRostersLineuptoNull()

            'If UdcSoccerFormation1.btnApply.Enabled = True Then
            '    If essageDialog.Show("Do you want to proceed ?", Me.Text, MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then

            '    Else
            '        Return False
            '    End If
            'End If

            'GETTING THE TABLE NAME FROM THE DATASET
            StrTableName = GetTableName()
            'UPDATING SLNO(LINEUPS NUMBER) FOR THE PLAYERS  SELECTED IN DGSTARTINGLINEUPS
            For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                If CLng(dgvStartingLineup.Rows(i).Cells(2).Value) <> Nothing Then
                    DR = DsRostersInfo.Tables(StrTableName).Select("PLAYER_ID = " & dgvStartingLineup.Rows(i).Cells(2).Value.ToString & "")
                    If DR.Length > 0 Then
                        Dim drs() As DataRow
                        drs = DsRostersInfo.Tables(StrTableName).Select("STARTING_POSITION = " & dgvStartingLineup.Rows(i).Cells(0).Value.ToString & "")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("STARTING_POSITION") = System.DBNull.Value
                            drs(0).Item("RATING") = System.DBNull.Value
                            drs(0).Item("PLAYER_STATUS_ID") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If
                        DR(0).BeginEdit()
                        DR(0).Item("STARTING_POSITION") = dgvStartingLineup.Rows(i).Cells(0).Value.ToString
                        DR(0).Item("DISPLAY_UNIFORM_NUMBER") = IIf(dgvStartingLineup.Rows(i).Cells(1).Value Is Nothing, DBNull.Value, dgvStartingLineup.Rows(i).Cells(1).Value)
                        ' DR(0).Item("RATING") = IIf(dgvStartingLineup.Rows(i).Cells(3).Value Is Nothing, DBNull.Value, dgvStartingLineup.Rows(i).Cells(3).Value)
                        If dgvStartingLineup.Rows(i).Cells(3).Value Is Nothing Then
                            DR(0).Item("RATING") = DBNull.Value
                        ElseIf dgvStartingLineup.Rows(i).Cells(3).Value.ToString = "" Then
                            DR(0).Item("RATING") = DBNull.Value
                        Else
                            DR(0).Item("RATING") = dgvStartingLineup.Rows(i).Cells(3).Value
                        End If

                        DR(0).Item("PLAYER_STATUS_ID") = 1
                        DR(0).EndEdit()
                    End If
                End If
            Next

            'UPDATING SLNO(LINEUPS NUMBER) FOR THE PLAYERS SELECTED ON DGBENCH
            For i As Integer = 0 To dgvBench.Rows.Count - 1
                If CLng(dgvBench.Rows(i).Cells(2).Value) <> Nothing Then
                    DR = DsRostersInfo.Tables(StrTableName).Select("PLAYER_ID = " & dgvBench.Rows(i).Cells(2).Value.ToString & "")
                    If DR.Length > 0 Then
                        Dim drs() As DataRow
                        drs = DsRostersInfo.Tables(StrTableName).Select("STARTING_POSITION = " & dgvBench.Rows(i).Cells(0).Value.ToString & "")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("STARTING_POSITION") = System.DBNull.Value
                            drs(0).Item("RATING") = System.DBNull.Value
                            DR(0).Item("PLAYER_STATUS_ID") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If
                        DR(0).BeginEdit()
                        DR(0).Item("STARTING_POSITION") = dgvBench.Rows(i).Cells(0).Value.ToString
                        DR(0).Item("DISPLAY_UNIFORM_NUMBER") = IIf(dgvBench.Rows(i).Cells(1).Value Is Nothing, DBNull.Value, dgvBench.Rows(i).Cells(1).Value)
                        If dgvBench.Rows(i).Cells(3).Value Is Nothing Then
                            DR(0).Item("RATING") = DBNull.Value
                        ElseIf dgvBench.Rows(i).Cells(3).Value.ToString = "" Then
                            DR(0).Item("RATING") = DBNull.Value
                        Else
                            DR(0).Item("RATING") = dgvBench.Rows(i).Cells(3).Value
                        End If
                    End If
                    DR(0).Item("PLAYER_STATUS_ID") = 2
                    DR(0).EndEdit()
                End If
            Next

            'UPDATION OF FORMATION IN THE DATASET
            If CInt(cmbFormation.SelectedValue) <> 0 Then
                DR = dsTeamGame.Tables("Formation").Select("FORMATION_ID = " & CInt(cmbFormation.SelectedValue) & "")
                If DR.Length > 0 Then
                    Dim drs() As DataRow
                    If intTeamID = m_objGameDetails.HomeTeamID Then
                        drs = dsTeamGame.Tables("Formation").Select("HOME = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("HOME") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If

                        DR(0).BeginEdit()
                        DR(0).Item("FORMATION_ID") = CInt(cmbFormation.SelectedValue)
                        DR(0).Item("HOME") = intTeamID
                        DR(0).EndEdit()
                        m_HomeFormation = CInt(DR(0).Item("FORMATION_ID"))
                    Else
                        drs = dsTeamGame.Tables("Formation").Select("AWAY = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("AWAY") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If
                        DR(0).BeginEdit()
                        DR(0).Item("FORMATION_ID") = CInt(cmbFormation.SelectedValue)
                        DR(0).Item("AWAY") = intTeamID
                        DR(0).EndEdit()
                        m_AwayFormation = CInt(DR(0).Item("FORMATION_ID"))
                    End If
                End If
            Else
                Dim drs() As DataRow
                If intTeamID = m_objGameDetails.HomeTeamID Then
                    drs = dsTeamGame.Tables("Formation").Select("HOME = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("HOME") = System.DBNull.Value
                        drs(0).EndEdit()
                        m_HomeFormation = 0
                    End If
                Else
                    drs = dsTeamGame.Tables("Formation").Select("AWAY = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("AWAY") = System.DBNull.Value
                        drs(0).EndEdit()
                        m_AwayFormation = 0
                    End If
                End If

            End If

            'UPDATION OF FORMATION SPECIFICATION
            If CInt(cmbFormationSpecification.SelectedValue) <> 0 Then
                DR = dsTeamGame.Tables("FormationSpec").Select("FORMATION_ID = " & CInt(cmbFormation.SelectedValue) & "AND FORMATION_SPEC_ID= " & CInt(cmbFormationSpecification.SelectedValue) & "")
                If DR.Length > 0 Then
                    Dim drs() As DataRow
                    If intTeamID = m_objGameDetails.HomeTeamID Then
                        drs = dsTeamGame.Tables("FormationSpec").Select("HOME = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("HOME") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If

                        DR(0).BeginEdit()
                        'DR(0).Item("FORMATION_SPEC_ID") = CInt(cmbFormationSpecification.SelectedValue)
                        DR(0).Item("HOME") = intTeamID
                        DR(0).EndEdit()
                        m_HomeFormationSpec = CInt(DR(0).Item("FORMATION_SPEC_ID"))
                    Else
                        drs = dsTeamGame.Tables("FormationSpec").Select("AWAY = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("AWAY") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If
                        DR(0).BeginEdit()
                        'DR(0).Item("FORMATION_SPEC_ID") = CInt(cmbFormation.SelectedValue)
                        DR(0).Item("AWAY") = intTeamID
                        DR(0).EndEdit()
                        m_AwayFormationSpec = CInt(DR(0).Item("FORMATION_SPEC_ID"))
                    End If
                End If
            Else
                Dim drs() As DataRow
                If intTeamID = m_objGameDetails.HomeTeamID Then
                    drs = dsTeamGame.Tables("FormationSpec").Select("HOME = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("HOME") = System.DBNull.Value
                        drs(0).EndEdit()
                        m_HomeFormationSpec = 0
                    End If
                Else
                    drs = dsTeamGame.Tables("FormationSpec").Select("AWAY = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("AWAY") = System.DBNull.Value
                        drs(0).EndEdit()
                        m_AwayFormationSpec = 0
                    End If
                End If

            End If


            'UPDATION OF KIT NO
            If picKit1.Tag IsNot Nothing Or picKit2.Tag IsNot Nothing Or picKit3.Tag IsNot Nothing Or picKit4.Tag IsNot Nothing Or picKit5.Tag IsNot Nothing Or picKit6.Tag IsNot Nothing Or picKit7.Tag IsNot Nothing Or picKit8.Tag IsNot Nothing Then
                If m_objclsGameDetails.HomeTeamID = intTeamID Then
                    DR = dsTeamGame.Tables("UniformImages").Select("KIT_NO = " & m_HomeKit & " AND TEAM_ID =" & intTeamID & "")
                Else
                    DR = dsTeamGame.Tables("UniformImages").Select("KIT_NO = " & m_AwayKit & " AND TEAM_ID =" & intTeamID & "")
                End If

                If DR.Length > 0 Then
                    Dim drs() As DataRow
                    If intTeamID = m_objGameDetails.HomeTeamID Then
                        drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("Selected") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If

                        DR(0).BeginEdit()
                        'DR(0).Item("FORMATION_SPEC_ID") = CInt(cmbFormationSpecification.SelectedValue)
                        DR(0).Item("Selected") = intTeamID
                        DR(0).EndEdit()
                        m_HomeKit = CInt(DR(0).Item("KIT_NO"))
                    Else
                        drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("Selected") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If
                        DR(0).BeginEdit()
                        'DR(0).Item("FORMATION_SPEC_ID") = CInt(cmbFormation.SelectedValue)
                        DR(0).Item("Selected") = intTeamID
                        DR(0).EndEdit()
                        m_AwayKit = CInt(DR(0).Item("KIT_NO"))
                    End If
                End If
            Else
                Dim drs() As DataRow
                If intTeamID = m_objGameDetails.HomeTeamID Then
                    drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("Selected") = System.DBNull.Value
                        drs(0).EndEdit()
                        m_HomeKit = -1
                    End If
                Else
                    drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("Selected") = System.DBNull.Value
                        drs(0).EndEdit()
                        m_AwayKit = -1
                    End If
                End If

            End If

            'UPDATION OF COACH IN THE DATASET
            If cmbCoach.Items.Count > 0 Then
                If CInt(cmbCoach.SelectedValue) <> 0 Then
                    DR = dsTeamGame.Tables("Coach").Select("Coach_ID = " & CInt(cmbCoach.SelectedValue) & "")
                    If DR.Length > 0 Then
                        Dim drs() As DataRow
                        drs = dsTeamGame.Tables("Coach").Select("Selected = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("Selected") = System.DBNull.Value
                            drs(0).EndEdit()
                        End If
                        DR(0).BeginEdit()
                        DR(0).Item("Coach_ID") = CInt(cmbCoach.SelectedValue)
                        DR(0).Item("Selected") = intTeamID
                        DR(0).EndEdit()
                        If intTeamID = m_objclsGameDetails.HomeTeamID Then
                            m_HomeCoach = CInt(DR(0).Item("Coach_ID"))
                        Else
                            m_AwayCoach = CInt(DR(0).Item("Coach_ID"))
                        End If
                    End If
                Else
                    Dim drs() As DataRow
                    drs = dsTeamGame.Tables("Coach").Select("Selected = '" & intTeamID & "'")
                    If drs.Length > 0 Then
                        drs(0).BeginEdit()
                        drs(0).Item("Selected") = System.DBNull.Value
                        drs(0).EndEdit()
                        If intTeamID = m_objclsGameDetails.HomeTeamID Then
                            m_HomeCoach = 0
                        Else
                            m_AwayCoach = 0
                        End If
                    End If
                End If

                m_objGameDetails.OffensiveCoachID = m_HomeCoach
                m_objGameDetails.DefensiveCoachID = m_AwayCoach


                'DATASET IS UPDATED WITH THE CHANGES
                DsRostersInfo.AcceptChanges()
                dsTeamGame.AcceptChanges()
            End If

            'If UdcSoccerFormation1.btnApply.Enabled = True Then
            '    If essageDialog.Show("Do you want to proceed ?", Me.Text, MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then

            '    Else
            '        If intTeamID = m_objclsGameDetails.HomeTeamID Then
            '            radHomeTeam.Checked = True
            '        Else
            '            radAwayTeam.Checked = True
            '        End If
            '        Return False
            '    End If
            'End If

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Private Function ValidateUniformNumber() As Boolean
        Try
            For intTableCount As Integer = 0 To DsRostersInfo.Tables.Count - 1
                Dim dr() As DataRow
                Dim drs() As DataRow
                dr = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL AND DISPLAY_UNIFORM_NUMBER IS NOT NULL", "STARTING_POSITION")
                For Each drRow As DataRow In dr
                    drs = DsRostersInfo.Tables(intTableCount).Select("DISPLAY_UNIFORM_NUMBER = '" & CInt(drRow.Item("DISPLAY_UNIFORM_NUMBER")) & "' AND STARTING_POSITION <> " & CInt(drRow.Item("STARTING_POSITION")) & "")
                    If drs.Length > 0 Then
                        MessageDialog.Show("The same uniform number " & drRow.Item("DISPLAY_UNIFORM_NUMBER").ToString & " exists for Players : " & drRow.Item("PLAYER").ToString & " And " & drs(0).Item("PLAYER").ToString & ".Please resolve the duplicates to proceed further.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Return False
                    End If
                Next
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' VALIDATES THE STARTING LINEUPS AND BENCH PLAYERS SELECTED IN DATAGRIDVIEW ONCE SAVE BUTTON IS CLICKED
    ''' </summary>
    ''' <returns>A BOOLEAN VALUE TRUE/FALSE</returns>
    ''' <remarks></remarks>
    Private Function ValidateCellsBeforeSave() As Boolean

        Dim strMeassage As String = String.Empty
        Try
            'For i As Integer = 0 To DsRostersInfo.Tables.Count - 1
            '    Dim drs() As DataRow
            '    drs = DsRostersInfo.Tables(i).Select("LINEUP IS NOT NULL")
            '    If drs.Length = 0 Then
            '        essageDialog.Show("Select atleast one player for a team", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
            '        Return False
            '        Exit Function
            '    End If
            'Next

            ''VALIDATION FOR POSITION (GK SHOULD BE ONLY ONE FOR A TEAM)         
            'For intTableCount As Integer = 0 To DsRostersInfo.Tables.Count - 1
            '    Dim intPosCount As Integer = 0
            '    Dim intPlayerCount As Integer = 0
            '    Dim strMessage As String = String.Empty
            '    Dim drPos() As DataRow = DsRostersInfo.Tables(intTableCount).Select("LINEUP IS NOT NULL")
            '    For Each dr As DataRow In drPos
            '        If CInt(dr.Item("LINEUP")) < 12 Then
            '            intPlayerCount += 1
            '            If CInt(dr.Item("POSITION_ID_1")) = 1 Then
            '                intPosCount += 1
            '            End If
            '        End If
            '    Next

            '    If intPosCount > 1 Then
            '        If essageDialog.Show("More than one GoalKeeper is selected for " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString & " Team. Do you want to Continue ? ", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
            '            If DsRostersInfo.Tables(intTableCount).TableName = "Home" Then
            '                radHomeTeam.Checked = True
            '            Else
            '                radAwayTeam.Checked = True
            '            End If
            '            Return False
            '            Exit For
            '        Else
            '            Return True
            '        End If
            '    End If
            'Next
            If m_blnCheck = False Then
                If ArelineupsEntered() = False Then
                    m_blnCheck = False
                    Exit Function
                End If
            Else
                m_blnCheck = False
                Exit Function
            End If


            For intTableCount As Integer = 0 To DsRostersInfo.Tables.Count - 1
                Dim dr() As DataRow
                Dim drGoalkeeper() As DataRow

                dr = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION <12")
                If dr.Length = 0 Then
                    strMeassage = strMeassage & "Empty Roster!!!!"
                    Exit For
                End If
                Dim intPosCount As Integer = 0
                For Each drs As DataRow In dr
                    If Not IsDBNull(drs.Item("POSITION_ID_1")) Then
                        If CInt(drs.Item("POSITION_ID_1")) = 1 Then
                            intPosCount += 1
                        End If
                    End If
                Next
                If intPosCount > 1 Then
                    strMeassage = strMeassage & "More than one GoalKeeper is selected for " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString.ToUpper & "" & "" & vbNewLine
                Else
                    strMeassage = strMeassage & ""
                End If

                If intPosCount = 0 Then
                    strMeassage = strMeassage & "No GoalKeeper selected for " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString.ToUpper & "" & "" & vbNewLine
                Else
                    drGoalkeeper = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION =1 AND POSITION_ID_1=1")
                    If drGoalkeeper.Length = 0 Then
                        If m_objclsGameDetails.CoverageLevel = 3 Then
                            strMeassage = strMeassage & "First Player Should be Goalkeeper for " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString.ToUpper & "" & vbNewLine
                        Else
                            strMeassage = strMeassage & ""
                        End If
                    End If
                    'strMeassage = strMeassage & ""
                End If

                If dr.Length < 11 Then
                    strMeassage = strMeassage & "All 11 players are not filled up for " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString.ToUpper & "" & vbNewLine
                Else
                    strMeassage = strMeassage & ""
                End If


                'ratings warnings should appear once the game started.

                If m_objGameDetails.CurrentPeriod > 0 Then
                    If m_objBLTeamSetup.GetEndGame(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber) > 0 Then
                        dgvStartingLineup.Columns(3).ReadOnly = False
                        Dim DRSLineups() As DataRow = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION <12")
                        If DRSLineups.Length > 0 Then
                            For Each drRow As DataRow In DRSLineups
                                If drRow.Item("Rating").ToString = "" Then
                                    strMeassage = strMeassage & "Ratings are not entered for all Lineup players of " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString.ToUpper & "" & vbNewLine
                                    Exit For
                                End If
                            Next
                        End If

                        'check for bench players
                        Dim DsSubEvents As DataSet
                        Dim StrPlayerName As String = ""
                        DsSubEvents = m_objGeneral.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        If DsSubEvents.Tables.Count > 0 Then
                            Dim DRSubevents() As DataRow = DsSubEvents.Tables(0).Select("EVENT_CODE_ID = 22 AND TEAM_ID =" & CInt(DsRostersInfo.Tables(intTableCount).Rows(0).Item("Team_id")) & "")
                            If DRSubevents.Length > 0 Then
                                Dim DRbench() As DataRow = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION >11")
                                If dr.Length > 0 Then
                                    For Each drRow As DataRow In DRbench
                                        If IsRatingAllowedForPlayer(CInt(drRow.Item("PLAYER_ID"))) = True Then
                                            If drRow.Item("Rating").ToString = "" Then
                                                'getting all player names for display
                                                StrPlayerName = StrPlayerName & ", " & drRow.Item("Last_Name").ToString & " " & drRow.Item("Moniker").ToString
                                            End If
                                        End If
                                    Next
                                    If StrPlayerName <> "" Then
                                        strMeassage = strMeassage & "Ratings are not entered for bench players ( " & StrPlayerName.Substring(2) & " ) of " & DsRostersInfo.Tables(intTableCount).Rows(0).Item("TEAM_ABBREV").ToString.ToUpper & "" & vbNewLine & vbNewLine
                                    Else
                                        'give a new line
                                        strMeassage = strMeassage & vbNewLine
                                    End If
                                Else
                                    strMeassage = strMeassage & vbNewLine
                                End If
                            Else
                                strMeassage = strMeassage & vbNewLine
                            End If
                        Else
                            strMeassage = strMeassage & vbNewLine
                        End If
                    Else
                        dgvStartingLineup.Columns(3).ReadOnly = True
                    End If
                End If

            Next

            'Sep 15 2010 Lineup events are saved into DB, without any players 
            Dim boolLineup As Boolean = False
            Dim drsLineup() As DataRow
            For intTableCount As Integer = 0 To DsRostersInfo.Tables.Count - 1
                drsLineup = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NOT NULL AND STARTING_POSITION <12")
                If drsLineup.Length > 0 Then
                    boolLineup = True
                    If intTableCount = 0 Then
                        boolLineup = False
                    End If
                Else
                    boolLineup = False
                    Exit For
                End If
            Next

            'Captain Not selected
            If m_objGameDetails.HomeTeamCaptainId = 0 Or m_objGameDetails.AwayTeamCaptainId = 0 Then
                strMeassage = strMeassage & "You have not selected a captain." & vbNewLine
            End If

            If strMeassage <> "" Then
                If (MessageDialog.Show(strMeassage & vbNewLine & "Do you want to proceed? ", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    If boolLineup = True Then
                        If m_objBLTeamSetup.GetLineupEventCnt(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objGameDetails.ModuleID) = 0 Then
                            If MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                'Insert lineups to pbp with eventids 23,24
                                'Check the the lineups inserted or not
                                m_sendToClient = True
                                lblStartLineSent.Text = "YES"
                                lblStartLineSent.ForeColor = Color.Green
                            End If
                        Else
                            lblStartLineSent.Text = "YES"
                            lblStartLineSent.ForeColor = Color.Green
                            Return True
                            Exit Function
                        End If
                    End If

                Else
                    'Return False
                    Exit Function
                End If
            Else
                If boolLineup = True Then
                    If m_objBLTeamSetup.GetLineupEventCnt(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.ModuleID) = 0 Then
                        If MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            'Insert lineups to pbp with eventids 23,24
                            'Check the the lineups inserted or not
                            'm_objModule1Main.InsertPlayByPlayData(clsGameDetails.HomeStartingLineups, "")
                            'm_objModule1Main.InsertPlayByPlayData(clsGameDetails.AwatStartingLineups, "")
                            m_sendToClient = True
                            lblStartLineSent.Text = "YES"
                            lblStartLineSent.ForeColor = Color.Green
                        End If
                        'm_objModule1Main.InsertPlayByPlayData(clsGameDetails.HomeStartingLineups, "")
                        'm_objModule1Main.InsertPlayByPlayData(clsGameDetails.AwatStartingLineups, "")
                    Else
                        lblStartLineSent.Text = "YES"
                        lblStartLineSent.ForeColor = Color.Green
                    End If
                End If

            End If


            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Fetching logo for the selected Team
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamLogo(ByVal TeamID As Integer)
        Try
            Dim v_memLogo As MemoryStream
            picTeamLogo.Image = Nothing
            v_memLogo = m_objGeneral.GetTeamLogo(m_objclsGameDetails.LeagueID, TeamID)
            If v_memLogo IsNot Nothing Then
                picTeamLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else 'TOSOCRS-338
                picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' fetching Rosters from database and storing it in a Dataset
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetRostersInfo()
        Try
            'TOPZ-1474 ----begin
            Dim gr = CreateGraphics()
            Dim strText As String = ""
            Dim strMoniker As String = ""
            Dim strLastName As String = ""
            Dim strPositionAbbrev As String = ""
            Dim intPreferredWidth As Integer = 130
            'TOPZ-1474 ----end

            If DsRostersInfo IsNot Nothing Then
                'IF ROSTERS ARE FILLED ALREADY THEN BLANK ROWS ARE CREATED IN THE DATAGRIDVIEW 
                AddDgviewColumns(dgvStartingLineup)
                AddDgviewRows("Starting Lineups")
                AddDgviewColumns(dgvBench)
                AddDgviewRows("Bench")
            Else
                'HOME AND AWAY TEAM ROSTERS AND FETCHED AND STORED IN A DATASET
                DsRostersInfo = m_objBLTeamSetup.GetRosterInfo(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.languageid)
                If DsRostersInfo.Tables.Count > 0 Then
                    DsRostersInfo.Tables(0).TableName = "Home"
                    DsRostersInfo.Tables(1).TableName = "Away"

                    'TOPZ-1474 ----begin
                    'trim long names so that they fit well in starting lineups and formations

                    'loop both teams
                    For tbl As Integer = 0 To 1
                        'loop all players
                        For rw As Integer = 0 To DsRostersInfo.Tables(tbl).Rows.Count - 1
                            strText = DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER")
                            If gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth Then
                                'strMoniker = DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER")
                                strMoniker = IIf(IsDBNull(DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER")), "", DsRostersInfo.Tables(tbl).Rows(rw)("MONIKER"))
                                strLastName = DsRostersInfo.Tables(tbl).Rows(rw)("LAST_NAME")
                                strPositionAbbrev = DsRostersInfo.Tables(tbl).Rows(rw)("POSITION_ABBREV")

                                While gr.MeasureString(strText, dgvStartingLineup.Font).Width > intPreferredWidth
                                    If strMoniker.Length > 1 Then
                                        strMoniker = strMoniker.Substring(0, strMoniker.Length - 1)
                                    Else
                                        strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                    End If
                                    strText = strLastName + ", " + strMoniker
                                    If Not (IsNothing(strPositionAbbrev) Or strPositionAbbrev = "") Then
                                        strText = strText + " (" + strPositionAbbrev + ")"
                                    End If
                                End While
                                DsRostersInfo.Tables(tbl).Rows(rw)("PLAYER") = strText
                            End If
                        Next
                    Next
                    'TOPZ-1474 ----end

                    AddDgviewColumns(dgvStartingLineup)
                    AddDgviewRows("Starting Lineups")
                    AddDgviewColumns(dgvBench)
                    AddDgviewRows("Bench")
                End If
            End If

            'RETRIVEING THE DATA IF THE PROPERTY DATASET IS ALREADY FILLES
            'If m_objTeamSetup.RosterInfo IsNot Nothing Then
            '    If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
            '        'RETRIEVE THE DATA STORED IN THE DAYASET AND DISPLAY IN DATAGRIDVIEW
            '        RetriveRostersFromGlobal()
            '        ''UPDATE LINEUPS AS NULL
            '        'UpdateRostersLineup()
            '    End If
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' UPDATING ROSTER LINEUP NUMBERS TO NULL WHILE EDITING THE STARTING LINEUPS
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateRostersLineuptoNull()
        Try
            Dim dr() As DataRow
            Dim StrTablename As String = GetTableName()
            If DsRostersInfo.Tables.Count > 0 Then
                If DsRostersInfo.Tables(StrTablename).Rows.Count > 0 Then
                    dr = DsRostersInfo.Tables(StrTablename).Select("STARTING_POSITION IS NOT NULL")
                    If dr.Length > 0 Then
                        For intRow As Integer = 0 To dr.Length - 1
                            dr(intRow).BeginEdit()
                            dr(intRow).Item("STARTING_POSITION") = System.DBNull.Value
                            dr(intRow).Item("RATING") = System.DBNull.Value
                            dr(intRow).Item("PLAYER_STATUS_ID") = System.DBNull.Value
                            dr(intRow).EndEdit()
                        Next
                        DsRostersInfo.Tables(StrTablename).AcceptChanges()
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' ROSTERS ARE RETRIEVED FROM THE LOCAL DATASET AND DISPLAYED IN THE DATAGRIDVIEW FOR EDITING/VIEWING PURPOSE
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RetriveRostersFromLocal()
        Try
            Dim DsTeamPlayers As DataSet
            Dim StrTableName As String = ""

            If DsRostersInfo.Tables.Count > 0 Then
                DsTeamPlayers = DsRostersInfo.Copy()
                Dim drs() As DataRow
                'GETTING THE TABLE NAME FROM THE DATASET
                StrTableName = GetTableName()

                drs = DsTeamPlayers.Tables(StrTableName).Select("STARTING_POSITION IS NOT NULL")
                If drs.Length > 0 Then
                    'DISPLAY THE SELECTED TEAM ROSTERS STORED IN LOCAL DATASET
                    Dim intLineupNo As Integer = 1
                    For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                        drs = DsTeamPlayers.Tables(StrTableName).Select("STARTING_POSITION = " & intLineupNo & "")
                        If drs.Length > 0 Then
                            dgvStartingLineup.Rows(i).Cells(1).Value = drs(0).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvStartingLineup.Rows(i).Cells(2).Value = drs(0).Item("PLAYER_ID")
                            dgvStartingLineup.Rows(i).Cells(3).Value = drs(0).Item("RATING")
                            dgvStartingLineup.Rows(i).Cells(4).Value = IIf(drs(0).Item("PLAYER_ID") = IIf(StrTableName = "Home", m_objGameDetails.HomeTeamCaptainId, m_objGameDetails.AwayTeamCaptainId), True, False)

                            'Else
                            '    dgvStartingLineup.Rows(i).Cells(1).Value = ""
                            '    dgvStartingLineup.Rows(i).Cells(2).Value = ""
                            '    dgvStartingLineup.Rows(i).Cells(3).Value = ""
                        End If
                        intLineupNo += 1
                    Next
                    'DISPLAY OF BENCH PLAYERS
                    intLineupNo = 12
                    For i = 0 To dgvBench.Rows.Count - 1
                        drs = DsTeamPlayers.Tables(StrTableName).Select("STARTING_POSITION = " & intLineupNo & "")
                        If drs.Length > 0 Then
                            dgvBench.Rows(i).Cells(1).Value = drs(0).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvBench.Rows(i).Cells(2).Value = drs(0).Item("PLAYER_ID")
                            dgvBench.Rows(i).Cells(3).Value = drs(0).Item("RATING")
                            dgvBench.Rows(i).Cells(4).Value = IIf(drs(0).Item("PLAYER_ID") = IIf(StrTableName = "Home", m_objGameDetails.HomeTeamCaptainId, m_objGameDetails.AwayTeamCaptainId), True, False)

                            'Else
                            '    dgvBench.Rows(i).Cells(1).Value = ""
                            '    dgvBench.Rows(i).Cells(2).Value = ""
                            '    dgvBench.Rows(i).Cells(3).Value = ""
                        End If
                        intLineupNo += 1
                    Next
                End If
            End If

            If dsTeamGame IsNot Nothing Then
                If dsTeamGame.Tables.Count > 0 Then
                    Dim drs() As DataRow
                    Dim drForSpec() As DataRow
                    Dim drKit() As DataRow
                    If Not dsTeamGame.Tables("Formation") Is Nothing Then
                        If m_objGameDetails.HomeTeamID = intTeamID Then
                            drs = dsTeamGame.Tables("Formation").Select("HOME = '" & intTeamID & "'")
                            If drs.Length > 0 Then
                                cmbFormation.SelectedValue = drs(0).Item("Formation_ID")
                            End If
                            drForSpec = dsTeamGame.Tables("FormationSpec").Select("HOME = '" & intTeamID & "'")
                            If drForSpec.Length > 0 Then
                                cmbFormationSpecification.SelectedValue = drForSpec(0).Item("FORMATION_SPEC_ID")
                            End If

                        Else
                            drs = dsTeamGame.Tables("Formation").Select("AWAY = '" & intTeamID & "'")
                            If drs.Length > 0 Then
                                cmbFormation.SelectedValue = drs(0).Item("Formation_ID")
                            End If

                            drForSpec = dsTeamGame.Tables("FormationSpec").Select("AWAY = '" & intTeamID & "'")
                            If drForSpec.Length > 0 Then
                                cmbFormationSpecification.SelectedValue = drForSpec(0).Item("FORMATION_SPEC_ID")
                            End If

                        End If

                        drKit = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "' AND TEAM_ID =" & intTeamID & "")
                        If drKit.Length > 0 Then
                            For Each ctrl As Control In Me.pnlKit.Controls
                                If ctrl.GetType().ToString = "System.Windows.Forms.PictureBox" Then
                                    If CInt(CType(ctrl, PictureBox).Tag) = CInt(drKit(0).Item("KIT_NO")) Then
                                        pnlHighlight.Visible = True
                                        pnlHighlight.Location = New Point(CType(ctrl, PictureBox).Location.X - 1, CType(ctrl, PictureBox).Location.Y - 1)
                                        Exit For
                                    End If
                                End If
                            Next
                        Else
                            pnlHighlight.Visible = False
                            'For Each ctrl As Control In Me.pnlKit.Controls
                            '    If ctrl.GetType().ToString = "System.Windows.Forms.PictureBox" Then
                            '        Panel1.Location = New Point(CType(ctrl, PictureBox).Location.X, CType(ctrl, PictureBox).Location.Y)
                            '        Panel1.BackColor = 
                            '    End If
                            'Next
                        End If

                        drs = dsTeamGame.Tables("Coach").Select("Selected =  '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            cmbCoach.SelectedValue = drs(0).Item("Coach_ID")
                        End If
                    End If
                End If
            End If

            'UpdatelabelColor()

            If m_objclsGameDetails.HomeTeamID = intTeamID Then
                If m_objTeamSetup.HomeShirtColor.Name = "0" Then
                    m_objTeamSetup.HomeShirtColor = Nothing
                    lblShirtColor.Text = "(None)"
                Else
                    lblShirtColor.Text = ""
                End If
                lblShirtColor.BackColor = m_objTeamSetup.HomeShirtColor
                If m_objTeamSetup.HomeShortColor.Name = "0" Then
                    m_objTeamSetup.HomeShortColor = Nothing
                    lblShortColor.Text = "(None)"
                Else
                    lblShortColor.Text = ""
                End If
                lblShortColor.BackColor = m_objTeamSetup.HomeShortColor
                'TOPZ-1116
                If m_objTeamSetup.HomeShirtColorNo.Name = "0" Then
                    m_objTeamSetup.HomeShirtColorNo = Nothing
                    lblShirtNoColor.Text = "(None)"
                Else
                    lblShirtNoColor.Text = ""
                End If
                lblShirtNoColor.BackColor = m_objTeamSetup.HomeShirtColorNo
            Else
                If m_objTeamSetup.AwayShirtColor.Name = "0" Then
                    m_objTeamSetup.AwayShirtColor = Nothing
                    lblShirtColor.Text = "(None)"
                Else
                    lblShirtColor.Text = ""
                End If
                lblShirtColor.BackColor = m_objTeamSetup.AwayShirtColor
                If m_objTeamSetup.AwayShortColor.Name = "0" Then
                    m_objTeamSetup.AwayShortColor = Nothing
                    lblShortColor.Text = "(None)"
                Else
                    lblShortColor.Text = ""
                End If
                lblShortColor.BackColor = m_objTeamSetup.AwayShortColor
                ' TOPZ-1116
                If m_objTeamSetup.AwayShirtColorNo.Name = "0" Then
                    m_objTeamSetup.AwayShirtColorNo = Nothing
                    lblShirtNoColor.Text = "(None)"
                Else
                    lblShirtNoColor.Text = ""
                End If
                lblShirtNoColor.BackColor = m_objTeamSetup.AwayShirtColorNo
            End If

            'UpdatelabelColor()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' ROSTERS ARE RETRIEVED FROM THE PROPERTY DATASET AND DISPLAYED IN THE DATAGRIDVIEW FOR EDITING/VIEWING PURPOSE
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RetriveRostersFromGlobal()
        Try
            Dim DsTeamPlayers As DataSet
            If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                DsTeamPlayers = m_objTeamSetup.RosterInfo.Copy()
                If intTeamID = CInt(DsTeamPlayers.Tables("Home").Rows(0).Item("TEAM_ID")) Then
                    'DISPLAY THE HOME TEAM ROSTERS STORED IN PROPERTY DATASET
                    For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                        If DsTeamPlayers.Tables("Home").Rows.Count > i Then
                            dgvStartingLineup.Rows(i).Cells(0).Value = DsTeamPlayers.Tables("Home").Rows(i).Item("STARTING_POSITION")
                            dgvStartingLineup.Rows(i).Cells(1).Value = DsTeamPlayers.Tables("Home").Rows(i).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvStartingLineup.Rows(i).Cells(2).Value = DsTeamPlayers.Tables("Home").Rows(i).Item("PLAYER_ID")
                        End If
                    Next
                    'DISPLAY OF BENCH PLAYERS
                    Dim drs() As DataRow
                    Dim intLineupNo As Integer = 12
                    For i = 0 To dgvBench.Rows.Count - 1
                        drs = DsTeamPlayers.Tables("Home").Select("STARTING_POSITION = " & intLineupNo & "")
                        If drs.Length > 0 Then
                            dgvBench.Rows(i).Cells(0).Value = DsTeamPlayers.Tables("Home").Rows(intLineupNo - 1).Item("STARTING_POSITION")
                            dgvBench.Rows(i).Cells(1).Value = DsTeamPlayers.Tables("Home").Rows(intLineupNo - 1).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvBench.Rows(i).Cells(2).Value = DsTeamPlayers.Tables("Home").Rows(intLineupNo - 1).Item("PLAYER_ID")
                            intLineupNo += 1
                        End If
                    Next
                Else
                    'DISPLAY THE AWAY TEAM ROSTERS STORED IN PROPERTY DATASET
                    For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                        If DsTeamPlayers.Tables("Away").Rows.Count > i Then
                            dgvStartingLineup.Rows(i).Cells(0).Value = DsTeamPlayers.Tables("Away").Rows(i).Item("STARTING_POSITION")
                            dgvStartingLineup.Rows(i).Cells(1).Value = DsTeamPlayers.Tables("Away").Rows(i).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvStartingLineup.Rows(i).Cells(2).Value = DsTeamPlayers.Tables("Away").Rows(i).Item("PLAYER_ID")
                        End If
                    Next
                    'DISPLAY OF BENCH PLAYERS
                    Dim drs() As DataRow
                    Dim intLineupNo As Integer = 12
                    For i = 0 To dgvBench.Rows.Count - 1
                        drs = DsTeamPlayers.Tables("Away").Select("STARTING_POSITION = " & intLineupNo & "")
                        If drs.Length > 0 Then
                            dgvBench.Rows(i).Cells(0).Value = DsTeamPlayers.Tables("Away").Rows(intLineupNo - 1).Item("STARTING_POSITION")
                            dgvBench.Rows(i).Cells(1).Value = DsTeamPlayers.Tables("Away").Rows(intLineupNo - 1).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvBench.Rows(i).Cells(2).Value = DsTeamPlayers.Tables("Away").Rows(intLineupNo - 1).Item("PLAYER_ID")
                            intLineupNo += 1
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' DATASET HAS 2 TABLES - FETCHING THE SELECTED TEAM TABLE FROM THE DATASET
    ''' </summary>
    ''' <returns>TABLE NAME AN STRING VALUE</returns>
    ''' <remarks></remarks>
    Private Function GetTableName() As String
        Try
            Dim drs() As DataRow
            drs = DsRostersInfo.Tables(0).Select("TEAM_ID = " & intTeamID & "")
            'If drs.Length > 0 Then
            '    Return "Home"
            'Else
            '    Return "Away"
            'End If

            If intTeamID = m_objGameDetails.HomeTeamID Then
                Return "Home"
            Else
                Return "Away"
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' ADDING ROWS TO THE DATAGRIDVIEW (FOR STARTING LINEUPS MAX PLAYERS WILL BE 11 ) AND REMAINING COMES UNDER BENCH
    ''' </summary>
    ''' <param name="Str"></param>
    ''' <remarks></remarks>
    Private Sub AddDgviewRows(ByVal Str As String)
        Try
            'Dim asNoIm As String = Application.StartupPath() + "\" + "Resources\" + "Blank.gif"
            Select Case Str
                Case "Starting Lineups"
                    dgvStartingLineup.Rows.Clear()
                    dgvStartingLineup.Rows.Add(11)
                    For i As Integer = 1 To 11
                        dgvStartingLineup.Rows(i - 1).Cells(0).Value = i
                        'dgvStartingLineup.Rows(i - 1).Cells(4).Value = Image.FromFile(asNoIm)
                    Next
                Case "Bench"
                    Dim intRowcount As Integer
                    dgvBench.Rows.Clear()
                    intRowcount = DsRostersInfo.Tables(GetTableName).Rows.Count

                    If intRowcount - 11 > 0 Then
                        If intRowcount > 41 Then
                            dgvBench.Rows.Add(30)
                        Else
                            dgvBench.Rows.Add(intRowcount - 11)
                        End If
                        'dgvBench.Rows.Add(7)
                        For i = 1 To dgvBench.Rows.Count
                            dgvBench.Rows(i - 1).Cells(0).Value = dgvStartingLineup.Rows.Count + i
                            'dgvBench.Rows(i - 1).Cells(4).Value = Image.FromFile(asNoIm)
                        Next
                    End If

            End Select

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    '''  CREATING COLUMNS FOR THE DATAGRIDVIEW SLNO,UNIFORM NUMBER AND PLAYER NAME
    ''' </summary>
    ''' <param name="dgv"></param>
    ''' <remarks></remarks>
    Private Sub AddDgviewColumns(ByVal dgv As DataGridView)
        Try
            If dgv.Columns.Count > 0 Then
                dgv.Columns.Clear()
            End If
            Dim ColSlno As New DataGridViewTextBoxColumn()
            ColSlno.HeaderText = "#"
            ColSlno.Width = 20
            ColSlno.ReadOnly = True
            ColSlno.SortMode = DataGridViewColumnSortMode.NotSortable
            dgv.Columns.Add(ColSlno)



            Dim ColUN As New DataGridViewTextBoxColumn()
            'set column's DataPropertyName, HeaderText, Name 
            ColUN.HeaderText = "UN"
            ColUN.Width = 29
            ColUN.ReadOnly = False
            ColUN.MaxInputLength = 3
            ColUN.SortMode = DataGridViewColumnSortMode.NotSortable
            dgv.Columns.Add(ColUN)

            With dgv
                ' Set DataGridView Combo Column for playerID field   
                Dim ColPlayer As New DataGridViewComboBoxColumn
                'DataGridView Combo ValueMember field has name "playerID"  
                'DataGridView Combo DisplayMember field has name "last_name"  
                Dim DsPlayers As DataSet = DsRostersInfo.Clone()
                Dim drs() As DataRow
                Dim DR As DataRow
                'fetching only the players who are not used in Lineups
                drs = DsRostersInfo.Tables(GetTableName).Select("starting_position is null")
                For Each DR In drs
                    DsPlayers.Tables(GetTableName).ImportRow(DR)
                Next

                DR = DsPlayers.Tables(GetTableName).NewRow()
                DR("PLAYER_ID") = 0
                DR("PLAYER") = ""
                DsPlayers.Tables(GetTableName).Rows.InsertAt(DR, 0)
                DsPlayers.Tables(GetTableName).AcceptChanges()

                With ColPlayer
                    .DataPropertyName = "PLAYER_ID"
                    .HeaderText = "Player"
                    .Width = 150
                    ' Bind ColPlayer to Home and Away Team table  
                    .DataSource = DsPlayers.Tables(GetTableName)
                    .ValueMember = "PLAYER_ID"
                    .DisplayMember = "PLAYER"
                    .AutoComplete = True
                End With
                .Columns.Add(ColPlayer)
            End With


            Dim ColRK As New DataGridViewTextBoxColumn()
            ColRK.HeaderText = "RK"


            'If m_objBLTeamSetup.GetEndGame(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber) > 0 Then
            '    ColRK.ReadOnly = False
            'Else
            '    ColRK.ReadOnly = True
            'End If

            If m_objBLTeamSetup.GetPeriodEndCount(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod) >= 2 Then
                ColRK.ReadOnly = False
            ElseIf m_objGameDetails.CurrentPeriod >= 2 Then
                ColRK.ReadOnly = False
            ElseIf m_objGameDetails.IsEndGame = True Then
                ColRK.ReadOnly = False
            Else
                ColRK.ReadOnly = True
            End If
            ColRK.MaxInputLength = 2
            ColRK.SortMode = DataGridViewColumnSortMode.NotSortable

            If dgv.Name = "dgvStartingLineup" Then
                ColRK.Width = 26
                dgv.Columns.Add(ColRK)
            Else
                ColRK.Width = 26
                dgv.Columns.Add(ColRK)
            End If
            'If dgv.Name = "dgvStartingLineup" Then
            Dim colCap As New DataGridViewCheckBoxColumn()
            colCap.HeaderText = "C"
            colCap.Width = 25
            colCap.SortMode = DataGridViewColumnSortMode.NotSortable
            If dgv.Name = "dgvStartingLineup" Then
                colCap.ReadOnly = False
            Else
                colCap.ReadOnly = True
            End If

            dgv.Columns.Add(colCap)
            'End If
            'Dim ColDisplayImage As New DataGridViewImageColumn()
            'ColDisplayImage.HeaderText = "IM"
            'ColDisplayImage.Width = 30
            'ColDisplayImage.ReadOnly = True
            'dgv.Columns.Add(ColDisplayImage)

            'Dim ColColor As New DataGridViewTextBoxColumn()
            'ColColor.HeaderText = "RK"
            'ColColor.Width = 10
            'ColColor.ReadOnly = True
            'dgv.Columns.Add(ColColor)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    ''' <summary>
    ''' ONCE PLAYER IS SELECTED FORM BENCH COMBOBOX, THIS FUNCTION AUTOMATICALLY FILLS THE UNIFORM NUMBER COLUMN
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FillBenchUniformNo(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If m_blnAutoFired = True Then


                If dgvBench.CurrentCell.ColumnIndex = 2 Then
                    If DirectCast(sender, ComboBox).SelectedIndex > 0 Then
                        Dim DR() As DataRow
                        If DirectCast(sender, ComboBox).SelectedValue.ToString <> "System.Data.DataRowView" Then
                            If UdcSoccerFormation1.btnApply.Enabled = True Then
                                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                dgvBench.CurrentRow.Cells(1).Value = ""
                                DirectCast(sender, ComboBox).SelectedIndex = -1
                                Exit Sub
                            End If

                            'IF BLANK IS SELECTED IN COMBO,CLEAR THE UNIFORM NO IF ALREDAY FILLED IN
                            If CInt(DirectCast(sender, ComboBox).SelectedValue) = 0 Then
                                dgvBench.CurrentRow.Cells(1).Value = ""
                                Exit Sub
                            End If

                            'VALIDATION FOR PLAYERS IF HE IS SELECTED IN STARTING lINEUPS
                            For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                                If CLng(dgvStartingLineup.Rows(i).Cells(2).Value) > 0 Then
                                    If (DirectCast(sender, ComboBox).SelectedValue.ToString = dgvStartingLineup.Rows(i).Cells(2).Value.ToString) Then
                                        MessageDialog.Show(strmessage4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        dgvBench.CurrentRow.Cells(1).Value = ""
                                        DirectCast(sender, ComboBox).SelectedIndex = -1
                                        Exit Sub
                                    End If
                                End If
                            Next

                            'VALIDATION FOR PLAYERS IF HE IS SELECTED IN BENCH LINEUPS
                            For i As Integer = 0 To dgvBench.Rows.Count - 1
                                If CLng(dgvBench.Rows(i).Cells(2).Value) > 0 Then
                                    If (DirectCast(sender, ComboBox).SelectedValue.ToString = dgvBench.Rows(i).Cells(2).Value.ToString) And (dgvBench.CurrentRow.Index <> i) Then
                                        MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        dgvBench.CurrentRow.Cells(1).Value = ""
                                        DirectCast(sender, ComboBox).SelectedIndex = -1
                                        Exit Sub
                                    End If
                                End If
                            Next

                            'If ValidateBenchUniformNumber() = False Then
                            '    Exit Sub
                            'End If

                            'FETCHING THE UNIFORM NUMBER FOR THE SELECTED BENCH PLAYER
                            DR = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = " & DirectCast(sender, ComboBox).SelectedValue.ToString() & "")
                            If DR.Length > 0 Then
                                dgvBench.CurrentRow.Cells(1).Value = DR(0).Item("DISPLAY_UNIFORM_NUMBER")
                                dgvBench.CurrentRow.Cells(2).Value = DR(0).Item("PLAYER_ID")
                                'AUDIT TRIAL
                                If radHomeTeam.Checked = True Then
                                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, "Bench : " & DirectCast(sender, ComboBox).Text, m_objclsGameDetails.HomeTeam, 1, 0)
                                Else
                                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, "Bench : " & DirectCast(sender, ComboBox).Text, m_objclsGameDetails.AwayTeam, 1, 0)
                                End If
                            End If
                            RefreshFormationDisplay()

                            'loads the players for the next combo cell
                            LoadBenchCombos(sender, e)

                            'sets the players which were filled already in each combo cell
                            SetPlayersInEachComboCell()
                        End If
                    Else

                        If dgvBench.CurrentRow.Cells(1).Value Is "" Or DirectCast(sender, ComboBox).SelectedIndex = 0 Then
                            dgvBench.CurrentRow.Cells(1).Value = ""
                            dgvBench.CurrentRow.Cells(2).Value = Nothing
                        End If

                        UpdatePlayerLineupInfo()

                        LoadBenchCombos(sender, e)
                        'setting the alreday filled players in each combo cell
                        SetPlayersInEachComboCell()

                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally

        End Try

    End Sub

    ''' <summary>
    ''' ONCE PLAYER IS SELECTED FORM STARTING LINEUPS COMBOBOX, THIS FUNCTION AUTOMATICALLY FILLS THE UNIFORM NUMBER COLUMN
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FillSLUniformNo(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If m_blnAutoFired = True Then
                If dgvStartingLineup.CurrentCell.ColumnIndex = 2 Then
                    If DirectCast(sender, ComboBox).SelectedIndex > 0 Then
                        Dim DR() As DataRow
                        If DirectCast(sender, ComboBox).SelectedValue.ToString <> "System.Data.DataRowView" Then

                            If UdcSoccerFormation1.btnApply.Enabled = True Then
                                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                                DirectCast(sender, ComboBox).SelectedIndex = -1
                                Exit Sub
                            End If

                            'IF BLANK IS SELECTED IN COMBO,CLEAR THE UNIFORM NO IF ALREDAY FILLED IN
                            If CInt(DirectCast(sender, ComboBox).SelectedValue) = 0 Then
                                dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                                Exit Sub
                            End If

                            'VALIDATION FOR PLAYERS IN STARTING lINEUPS
                            For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                                If CLng(dgvStartingLineup.Rows(i).Cells(2).Value) > 0 Then
                                    If (DirectCast(sender, ComboBox).SelectedValue.ToString = dgvStartingLineup.Rows(i).Cells(2).Value.ToString) And (dgvStartingLineup.CurrentRow.Index <> i) Then
                                        MessageDialog.Show(strmessage4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                                        DirectCast(sender, ComboBox).SelectedIndex = -1
                                        Exit Sub
                                    End If
                                End If
                            Next

                            'VALIDATION FOR PLAYERS IF HE IS SELECTED IN BENCH lINEUPS
                            For i As Integer = 0 To dgvBench.Rows.Count - 1
                                If CLng(dgvBench.Rows(i).Cells(2).Value) > 0 Then
                                    If (DirectCast(sender, ComboBox).SelectedValue.ToString = dgvBench.Rows(i).Cells(2).Value.ToString) Then
                                        MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                                        DirectCast(sender, ComboBox).SelectedIndex = -1
                                        Exit Sub
                                    End If
                                End If
                            Next



                            'FETCHING UNIFORM NUMBER SO AS TO DISPLAY IN DATAGRISVIEW
                            DR = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = " & DirectCast(sender, ComboBox).SelectedValue.ToString() & "")
                            If DR.Length > 0 Then
                                dgvStartingLineup.CurrentRow.Cells(1).Value = DR(0).Item("DISPLAY_UNIFORM_NUMBER")
                                dgvStartingLineup.CurrentRow.Cells(2).Value = DR(0).Item("PLAYER_ID")
                                'AUDIT TRIAL
                                If radHomeTeam.Checked = True Then
                                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, DirectCast(sender, ComboBox).Text, m_objclsGameDetails.HomeTeam, 1, 0)
                                Else
                                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, DirectCast(sender, ComboBox).Text, m_objclsGameDetails.AwayTeam, 1, 0)
                                End If
                            End If

                            RefreshFormationDisplay()

                            'loads the players for the next combo cell
                            LoadCombos(sender, e)

                            'setting the alreday filled players in each combo cell
                            SetPlayersInEachComboCell()

                            'If ValidateSLUniformNumber() = False Then
                            '    Exit Sub
                            'End If

                        End If
                    Else
                        ''dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                        If dgvStartingLineup.CurrentRow.Cells(1).Value Is "" Or DirectCast(sender, ComboBox).SelectedIndex = 0 Then
                            dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                            dgvStartingLineup.CurrentRow.Cells(2).Value = Nothing
                        End If
                        UpdatePlayerLineupInfo()

                        'RefreshFormationDisplay()
                        LoadCombos(sender, e)
                        'setting the alreday filled players in each combo cell
                        SetPlayersInEachComboCell()

                    End If
                End If
            End If


        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            'm_blnAutoFired = False
        End Try

    End Sub

    Private Function ValidateSLUniformNumber() As Boolean
        Try
            Dim drs() As DataRow = Nothing

            If Not dgvStartingLineup.CurrentRow.Cells(1).Value Is Nothing And Not dgvStartingLineup.CurrentRow.Cells(2).Value Is Nothing Then
                Dim StrUniform As String = dgvStartingLineup.CurrentRow.Cells(1).Value.ToString
                If (Not StrUniform Is Nothing) Then
                    If StrUniform <> "" Then
                        If (StrUniform.Trim.Length = 1) Then
                            If (StrUniform = "0") Then
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                            Else
                                Dim strUniform1 As String = "0" & StrUniform.Trim()
                                Dim strUniform2 As String = "00" & StrUniform.Trim
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND (DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "')")
                            End If
                        ElseIf (StrUniform.Trim.Length = 2) Then
                            If (StrUniform = "00") Then
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                            Else
                                If (StrUniform.Substring(0, 1) = "0") Then
                                    Dim strUniform1 As String = StrUniform.Substring(1, 1)
                                    Dim strUniform2 As String = "00" & StrUniform.Substring(1)
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                Else
                                    Dim strUniform2 As String = "0" & StrUniform
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                End If
                            End If
                        ElseIf (StrUniform.Trim.Length = 3) Then
                            If (StrUniform.Substring(0, 1) = "0") Then
                                If (StrUniform.Substring(0, 2) = "00") Then
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & StrUniform.Substring(1) & "' ")
                                Else
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(1) & "' ")
                                End If
                            Else
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' ")
                            End If
                        End If
                        If (drs.Length >= 1) Then

                            MessageDialog.Show(strmessage2 + ": " & drs(0).Item("PLAYER").ToString & ". Please resolve the duplicate uniform number: " & drs(0).Item("DISPLAY_UNIFORM_NUMBER").ToString & ". ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Dim dr() As DataRow = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = '" & CStr(dgvStartingLineup.CurrentRow.Cells(2).Value) & "'")
                            If dr.Length > 0 Then
                                dgvStartingLineup.CurrentRow.Cells(1).Value = dr(0).Item("DISPLAY_UNIFORM_NUMBER")
                            Else
                                dgvStartingLineup.CurrentRow.Cells(1).Value = ""
                            End If

                            Return False
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Function ValidateBenchUniformNumber() As Boolean
        Try
            Dim drs() As DataRow = Nothing

            If Not dgvBench.CurrentRow.Cells(1).Value Is Nothing And Not dgvBench.CurrentRow.Cells(2).Value Is Nothing Then
                Dim StrUniform As String = dgvBench.CurrentRow.Cells(1).Value.ToString
                If (Not StrUniform Is Nothing) Then
                    If StrUniform <> "" Then
                        If (StrUniform.Trim.Length = 1) Then
                            If (StrUniform = "0") Then
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                            Else
                                Dim strUniform1 As String = "0" & StrUniform.Trim()
                                Dim strUniform2 As String = "00" & StrUniform.Trim
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND (DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "')")
                            End If
                        ElseIf (StrUniform.Trim.Length = 2) Then
                            If (StrUniform = "00") Then
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                            Else
                                If (StrUniform.Substring(0, 1) = "0") Then
                                    Dim strUniform1 As String = StrUniform.Substring(1, 1)
                                    Dim strUniform2 As String = "00" & StrUniform.Substring(1)
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                Else
                                    Dim strUniform2 As String = "0" & StrUniform
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                End If
                            End If
                        ElseIf (StrUniform.Trim.Length = 3) Then
                            If (StrUniform.Substring(0, 1) = "0") Then
                                If (StrUniform.Substring(0, 2) = "00") Then
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & StrUniform.Substring(1) & "' ")
                                Else
                                    drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(1) & "' ")
                                End If
                            Else
                                drs = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID <> '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' ")
                            End If

                        End If
                        If (drs.Length >= 1) Then
                            MessageDialog.Show(strmessage2 + " " & drs(0).Item("PLAYER").ToString & ".Please resolve the duplicate uniform number " & drs(0).Item("DISPLAY_UNIFORM_NUMBER").ToString & ". ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Dim dr() As DataRow = DsRostersInfo.Tables(GetTableName).Select("PLAYER_ID = '" & CStr(dgvBench.CurrentRow.Cells(2).Value) & "'")
                            If dr.Length > 0 Then
                                dgvBench.CurrentRow.Cells(1).Value = dr(0).Item("DISPLAY_UNIFORM_NUMBER")
                            Else
                                dgvBench.CurrentRow.Cells(1).Value = ""
                            End If

                            Return False
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'TOPZ-1526 Player Ratings for Substitutes
    Private Sub ApplyStylesForPlayerRatings()
        Try
            Dim objClsPlayerData As ClsPlayerData = ClsPlayerData.GetInstance()
            Dim playerTable As New List(Of ClsPlayerData.PlayerData)
            playerTable = objClsPlayerData.FillPlayer(m_objGeneral.GetAllRosters(m_objGameDetails.GameCode, m_objGameDetails.languageid).Tables(0))
            HighlightPlyrsInvolvedInSubsRedCard(dgvStartingLineup, playerTable, Color.LightSalmon)
            HighlightPlyrsInvolvedInSubsRedCard(dgvBench, playerTable, Color.PaleGreen)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub HighlightPlyrsInvolvedInSubsRedCard(ByVal playersDataGrid As DataGridView, ByVal playerList As List(Of ClsPlayerData.PlayerData), ByVal SetColor As Color)
        Try
            Dim highlightRows = From dgvStartingrow As DataGridViewRow In playersDataGrid.Rows
                                 Join player In playerList.AsEnumerable()
                                 On player.PlayerId Equals (CInt(dgvStartingrow.Cells(2).Value))
                                 Where player.Substitute Or player.RedCard
                                 Select player, dgvStartingrow
            For Each highlightRow In highlightRows
                If highlightRow.player.StartingPosition = 0 Then 'red card
                    highlightRow.dgvStartingrow.DefaultCellStyle.BackColor = Color.IndianRed
                Else
                    highlightRow.dgvStartingrow.DefaultCellStyle.BackColor = SetColor
                    highlightRow.dgvStartingrow.Cells(4).ReadOnly = False
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub SetPlayersInEachComboCell()
        Try
            'Dim asplayerout As String = Application.StartupPath() + "\" + "Resources\" + "outHome.gif"
            'Dim asplayerin As String = Application.StartupPath() + "\" + "Resources\" + "inHome.gif"
            'Dim asNoIm As String = Application.StartupPath() + "\" + "Resources\" + "Blank.gif"
            Dim DsTeamPlayers As DataSet
            Dim DsMainEvents As DataSet


            DsMainEvents = m_objGeneral.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            Dim StrTableName As String = ""

            If DsRostersInfo.Tables.Count > 0 Then
                DsTeamPlayers = DsRostersInfo.Copy()
                Dim drs() As DataRow
                'GETTING THE TABLE NAME FROM THE DATASET
                StrTableName = GetTableName()

                drs = DsTeamPlayers.Tables(StrTableName).Select("STARTING_POSITION IS NOT NULL")
                If drs.Length > 0 Then
                    Dim intLineupNo As Integer = 1

                    For i As Integer = 0 To dgvStartingLineup.Rows.Count - 1
                        'pointing to each row in the combo box cell
                        Dim ColPlayer As New DataGridViewComboBoxCell
                        ColPlayer = CType(dgvStartingLineup.Rows(i).Cells(2), DataGridViewComboBoxCell)

                        Dim DsRoster As DataSet = DsRostersInfo.Clone()
                        Dim DR As DataRow
                        'fetching only the players who are not used in Lineups
                        drs = DsRostersInfo.Tables(GetTableName).Select("starting_position is null")
                        For Each DR In drs
                            DsRoster.Tables(GetTableName).ImportRow(DR)
                        Next
                        'inserting the selected player in the dataset to bind
                        drs = DsRostersInfo.Tables(StrTableName).Select("STARTING_POSITION = " & intLineupNo & "")
                        For Each DR In drs
                            DsRoster.Tables(GetTableName).ImportRow(DR)
                        Next
                        'sorted based on last name
                        Dim dsPlayers As DataSet
                        dsPlayers = DsRostersInfo.Clone()
                        drs = DsRoster.Tables(StrTableName).Select("", "last_name")
                        For Each DR In drs
                            dsPlayers.Tables(GetTableName).ImportRow(DR)
                        Next

                        'inserting a blank row to the top 
                        DR = dsPlayers.Tables(GetTableName).NewRow()
                        DR("PLAYER_ID") = 0
                        DR("PLAYER") = ""
                        dsPlayers.Tables(GetTableName).Rows.InsertAt(DR, 0)
                        dsPlayers.Tables(GetTableName).AcceptChanges()

                        'finally binding it to the Datagridview cell
                        m_blnAutoFired = False
                        ColPlayer.DataSource = dsPlayers.Tables(GetTableName)
                        ColPlayer.ValueMember = "PLAYER_ID"
                        ColPlayer.DisplayMember = "PLAYER"
                        m_blnAutoFired = True

                        drs = DsTeamPlayers.Tables(StrTableName).Select("STARTING_POSITION = " & intLineupNo & "")
                        If drs.Length > 0 Then
                            dgvStartingLineup.Rows(i).Cells(1).Value = drs(0).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvStartingLineup.Rows(i).Cells(2).Value = drs(0).Item("PLAYER_ID")
                            dgvStartingLineup.Rows(i).Cells(3).Value = drs(0).Item("RATING")


                            If DsMainEvents.Tables(0).Rows.Count > 0 Then
                                Dim drMain() As DataRow = DsMainEvents.Tables(0).Select("PLAYER_OUT_ID =  " & CInt(drs(0).Item("PLAYER_ID")) & "")
                                If drMain.Length > 0 Then
                                    If CInt(drMain(0).Item("EVENT_CODE_ID")) = 7 Then
                                        dgvStartingLineup.Rows(i).DefaultCellStyle.BackColor = Color.IndianRed
                                    Else
                                        dgvStartingLineup.Rows(i).DefaultCellStyle.BackColor = Color.LightSalmon
                                    End If
                                Else
                                    dgvStartingLineup.Rows(i).DefaultCellStyle.BackColor = Color.White
                                End If
                            Else
                                dgvStartingLineup.Rows(i).DefaultCellStyle.BackColor = Color.White
                            End If
                        End If
                        intLineupNo += 1
                    Next

                    'DISPLAY OF BENCH PLAYERS
                    intLineupNo = 12
                    For i = 0 To dgvBench.Rows.Count - 1

                        'pointing to each row in the combo box cell
                        Dim ColPlayer As New DataGridViewComboBoxCell
                        ColPlayer = CType(dgvBench.Rows(i).Cells(2), DataGridViewComboBoxCell)

                        Dim DsRoster As DataSet = DsRostersInfo.Clone()
                        Dim DR As DataRow
                        'fetching only the players who are not used in Lineups
                        drs = DsRostersInfo.Tables(GetTableName).Select("starting_position is null")
                        For Each DR In drs
                            DsRoster.Tables(GetTableName).ImportRow(DR)
                        Next
                        'inserting the selected player in the dataset to bind
                        drs = DsRostersInfo.Tables(StrTableName).Select("STARTING_POSITION = " & intLineupNo & "")
                        For Each DR In drs
                            DsRoster.Tables(GetTableName).ImportRow(DR)
                        Next
                        'sorted based on last name
                        Dim dsPlayers As DataSet
                        dsPlayers = DsRostersInfo.Clone()
                        drs = DsRoster.Tables(StrTableName).Select("", "last_name")
                        For Each DR In drs
                            dsPlayers.Tables(GetTableName).ImportRow(DR)
                        Next

                        'inserting a blank row to the top 
                        DR = dsPlayers.Tables(GetTableName).NewRow()
                        DR("PLAYER_ID") = 0
                        DR("PLAYER") = ""
                        dsPlayers.Tables(GetTableName).Rows.InsertAt(DR, 0)
                        dsPlayers.Tables(GetTableName).AcceptChanges()

                        'finally binding it to the Datagridview cell
                        m_blnAutoFired = False
                        ColPlayer.DataSource = dsPlayers.Tables(GetTableName)
                        ColPlayer.ValueMember = "PLAYER_ID"
                        ColPlayer.DisplayMember = "PLAYER"
                        m_blnAutoFired = True


                        drs = DsTeamPlayers.Tables(StrTableName).Select("STARTING_POSITION = " & intLineupNo & "")
                        If drs.Length > 0 Then
                            dgvBench.Rows(i).Cells(1).Value = drs(0).Item("DISPLAY_UNIFORM_NUMBER")
                            dgvBench.Rows(i).Cells(2).Value = drs(0).Item("PLAYER_ID")
                            dgvBench.Rows(i).Cells(3).Value = drs(0).Item("RATING")
                            If DsMainEvents.Tables(0).Rows.Count > 0 Then
                                Dim drMain() As DataRow = DsMainEvents.Tables(0).Select("OFFENSIVE_PLAYER_ID =  " & CInt(drs(0).Item("PLAYER_ID")) & "")
                                If drMain.Length > 0 Then
                                    'dgvBench.Rows(i).Cells(4).Value = Image.FromFile(asplayerin)
                                    dgvBench.Rows(i).DefaultCellStyle.BackColor = Color.PaleGreen
                                Else
                                    'dgvBench.Rows(i).Cells(4).Value = Image.FromFile(asNoIm)
                                End If
                            Else
                                'dgvBench.Rows(i).Cells(4).Value = Image.FromFile(asNoIm)
                            End If
                        End If
                        intLineupNo += 1
                    Next
                End If

                If m_objGameDetails.ModuleID = 3 Then
                    ApplyStylesForPlayerRatings()
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadCombos(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If DirectCast(sender, ComboBox).SelectedIndex > 0 Then
                'updates the starting position's selected
                UpdatePlayerLineupInfo()
                'exit from Function
                If dgvStartingLineup.CurrentRow.Index > 9 Then Exit Sub
                If CInt(dgvStartingLineup.Rows(dgvStartingLineup.CurrentRow.Index + 1).Cells(2).Value) > 0 Then Exit Sub

                'pointing to next combobox cell
                Dim ColPlayer As New DataGridViewComboBoxCell
                ColPlayer = CType(dgvStartingLineup.Rows(dgvStartingLineup.CurrentRow.Index + 1).Cells(2), DataGridViewComboBoxCell)

                Dim DsPlayers As DataSet = DsRostersInfo.Clone()
                Dim drs() As DataRow
                Dim DR As DataRow
                'fetching only the players who are not used in Lineups
                drs = DsRostersInfo.Tables(GetTableName).Select("starting_position is null")
                For Each DR In drs
                    DsPlayers.Tables(GetTableName).ImportRow(DR)
                Next
                'inserting a blank row to the top 
                DR = DsPlayers.Tables(GetTableName).NewRow()
                DR("PLAYER_ID") = 0
                DR("PLAYER") = ""
                DsPlayers.Tables(GetTableName).Rows.InsertAt(DR, 0)
                DsPlayers.Tables(GetTableName).AcceptChanges()

                'finally binding it to the Datagridview cell
                m_blnAutoFired = False
                ColPlayer.DataSource = DsPlayers.Tables(GetTableName)
                ColPlayer.ValueMember = "PLAYER_ID"
                ColPlayer.DisplayMember = "PLAYER"
                m_blnAutoFired = True
            End If

        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Sub

    Private Sub LoadBenchCombos(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If DirectCast(sender, ComboBox).SelectedIndex > 0 Then
                'updates the starting position's selected
                UpdatePlayerLineupInfo()
                'exit from the Function
                If dgvBench.CurrentRow.Index > 9 Then Exit Sub
                If dgvBench.CurrentRow.Index > dgvBench.Rows.Count - 1 Then
                    If CInt(dgvBench.Rows(dgvBench.CurrentRow.Index + 1).Cells(2).Value) > 0 Then Exit Sub
                Else
                    Exit Sub
                End If
                'pointing to the next combo box cell
                Dim ColPlayer As New DataGridViewComboBoxCell
                ColPlayer = CType(dgvBench.Rows(dgvBench.CurrentRow.Index + 1).Cells(2), DataGridViewComboBoxCell)
                Dim DsPlayers As DataSet = DsRostersInfo.Clone()
                Dim drs() As DataRow
                Dim DR As DataRow
                drs = DsRostersInfo.Tables(GetTableName).Select("starting_position is null")
                For Each DR In drs
                    DsPlayers.Tables(GetTableName).ImportRow(DR)
                Next
                'inserting a blank row in Dataset
                DR = DsPlayers.Tables(GetTableName).NewRow()
                DR("PLAYER_ID") = 0
                DR("PLAYER") = ""
                DsPlayers.Tables(GetTableName).Rows.InsertAt(DR, 0)
                DsPlayers.Tables(GetTableName).AcceptChanges()

                m_blnAutoFired = False
                'finally binding it to the cell
                ColPlayer.DataSource = DsPlayers.Tables(GetTableName)
                ColPlayer.ValueMember = "PLAYER_ID"
                ColPlayer.DisplayMember = "PLAYER"
                m_blnAutoFired = True
            End If

        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Sub

    ''' <summary>
    ''' LOADS THE DATA INTO CONTROL
    ''' </summary>
    ''' <param name="Combo">Name of the Combo Box</param>
    ''' <param name="GameSetup">Name of the Data Table</param>
    ''' <param name="DisplayColumnName">A string represents the Column Name to be displayed</param>
    ''' <param name="DataColumnName">A string represents value of the column</param>
    ''' <remarks></remarks>
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' LOADS FORMATION COMBO BOX
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadFormation()
        Try
            Dim dtFormation As New DataTable
            'Dim dsFormation As New DataSet
            'dsFormation = m_objBLTeamSetup.GetFormation()
            'If Not dsTeamGame.Tables(0) Is Nothing Then
            dtFormation = dsTeamGame.Tables("FORMATION").Copy
            LoadControl(cmbFormation, dtFormation, "FORMATION", "FORMATION_ID")
            m_blnIsFormationComboLoaded = True
            cmbFormation.SelectedIndex = 0
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' LOADS COACH COMBO
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub LoadCoach(ByVal TeamID As Integer)
        Try
            Dim dtCoach As New DataTable
            Dim dsCoach As New DataSet
            'If Not dsTeamGame.Tables("COACH") Is Nothing Then
            dtCoach.Columns.Add("COACH_ID")
            dtCoach.Columns.Add("COACH_NAME")

            Dim DR() As DataRow
            DR = dsTeamGame.Tables("COACH").Select("TEAM_ID = " & TeamID)

            For Each drs As DataRow In DR
                dtCoach.ImportRow(drs)
            Next
            LoadControl(cmbCoach, dtCoach, "COACH_NAME", "COACH_ID")
            m_blnIsCoachComboLoaded = True
            cmbCoach.SelectedIndex = 0
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' CREATES THE DATASET WITH COLUMUNS OF LIVE_SOC_TEAM_GAME TO INSERT FORMATION AND COACH DATA
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateColumn() As DataSet
        Try

            Dim dsTempData As New DataSet
            dsTempData.Tables.Add()

            dsTempData.Tables(0).Columns.Add("GAME_CODE")
            dsTempData.Tables(0).Columns.Add("FEED_NUMBER")
            dsTempData.Tables(0).Columns.Add("TEAM_ID")
            dsTempData.Tables(0).Columns.Add("COACH_ID")
            dsTempData.Tables(0).Columns.Add("SHOOTOUT_SCORE")
            dsTempData.Tables(0).Columns.Add("SHIRT_COLOR")
            dsTempData.Tables(0).Columns.Add("SHIRT_NO_COLOR") 'TOPZ-1116
            dsTempData.Tables(0).Columns.Add("SHORT_COLOR")
            dsTempData.Tables(0).Columns.Add("START_FORMATION_ID")
            dsTempData.Tables(0).Columns.Add("FORMATION_SPEC_ID")
            dsTempData.Tables(0).Columns.Add("KIT_NO")
            dsTempData.Tables(0).Columns.Add("REPORTER_ROLE")
            dsTempData.Tables(0).Columns.Add("PROCESSED")
            dsTempData.Tables(0).Columns.Add("DEMO_DATA")
            dsTempData.Tables(0).Columns.Add("CAPTAIN_ID")
            Return dsTempData

        Catch ex As Exception
            Throw ex
        End Try
    End Function



    ''' <summary>
    ''' DISPLAYS THE FORMATION AND COACH INFORMATION WHEN SWITCHING TEAM RADIO BUTTONS
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetFormationCoach()
        Try
            Dim homeCaptainId As Integer = 0
            Dim awayCaptainId As Integer = 0
            If dsForCoach Is Nothing Then
                dsForCoach = m_objBLTeamSetup.GetFormationCoach(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTeamID)
                dsTeamGame = m_objBLTeamSetup.GetFormationCoach(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTeamID)

                If dsForCoach.Tables(2).Rows.Count > 0 Then

                    If CInt(dsForCoach.Tables(2).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                        m_objGameDetails.HomeTeamCaptainId = IIf(IsDBNull(dsForCoach.Tables(2).Rows(0).Item("CAPTAIN_ID")), 0, dsForCoach.Tables(2).Rows(0).Item("CAPTAIN_ID"))
                        If dsForCoach.Tables(2).Rows(0).Item("COACH_ID").ToString() = "" Then
                            m_HomeCoach = 0
                        Else
                            m_HomeCoach = CInt(dsForCoach.Tables(2).Rows(0).Item("COACH_ID"))
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("START_FORMATION_ID").ToString() = "" Then
                            m_HomeFormation = 0
                        Else
                            m_HomeFormation = CInt(dsForCoach.Tables(2).Rows(0).Item("START_FORMATION_ID"))
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("FORMATION_SPEC_ID").ToString() = "" Then
                            m_HomeFormationSpec = 0
                        Else
                            m_HomeFormationSpec = CInt(dsForCoach.Tables(2).Rows(0).Item("FORMATION_SPEC_ID"))
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("KIT_NO").ToString() = "" Then
                            m_HomeKit = -1
                        Else
                            m_HomeKit = CInt(dsForCoach.Tables(2).Rows(0).Item("KIT_NO"))
                        End If


                        If dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString() = "" Then
                            m_objTeamSetup.HomeShirtColor = Color.Empty
                        Else
                            m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString)

                        End If
                        'TOPZ-1116
                        If dsForCoach.Tables(2).Rows(0).Item("SHIRT_NO_COLOR").ToString() = "" Then
                            m_objTeamSetup.HomeShirtColorNo = Color.Empty
                        Else
                            m_objTeamSetup.HomeShirtColorNo = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHIRT_NO_COLOR").ToString)

                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString() = "" Then
                            m_objTeamSetup.HomeShortColor = Color.Empty
                        Else
                            m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString)
                            'If dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString <> "#000000" Then
                            '    m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString)
                            'Else
                            '    m_objTeamSetup.HomeShortColor = Color.Empty
                            'End If
                        End If


                        If dsForCoach.Tables(2).Rows.Count > 1 Then
                            m_objGameDetails.AwayTeamCaptainId = IIf(IsDBNull(dsForCoach.Tables(2).Rows(1).Item("CAPTAIN_ID")), 0, dsForCoach.Tables(2).Rows(1).Item("CAPTAIN_ID"))
                            If dsForCoach.Tables(2).Rows(1).Item("COACH_ID").ToString() = "" Then
                                m_AwayCoach = 0
                            Else
                                m_AwayCoach = CInt(dsForCoach.Tables(2).Rows(1).Item("COACH_ID"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("START_FORMATION_ID").ToString() = "" Then
                                m_AwayFormation = 0
                            Else
                                m_AwayFormation = CInt(dsForCoach.Tables(2).Rows(1).Item("START_FORMATION_ID"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("FORMATION_SPEC_ID").ToString() = "" Then
                                m_AwayFormationSpec = 0
                            Else
                                m_AwayFormationSpec = CInt(dsForCoach.Tables(2).Rows(1).Item("FORMATION_SPEC_ID"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("KIT_NO").ToString() = "" Then
                                m_AwayKit = -1
                            Else
                                m_AwayKit = CInt(dsForCoach.Tables(2).Rows(1).Item("KIT_NO"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString() = "" Then
                                m_objTeamSetup.AwayShirtColor = Color.Empty
                            Else
                                m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString)
                            End If
                            'TOPZ-1116
                            If dsForCoach.Tables(2).Rows(1).Item("SHIRT_NO_COLOR").ToString() = "" Then
                                m_objTeamSetup.AwayShirtColorNo = Color.Empty
                            Else
                                m_objTeamSetup.AwayShirtColorNo = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHIRT_NO_COLOR").ToString)
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString() = "" Then
                                m_objTeamSetup.AwayShortColor = Color.Empty
                            Else
                                m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString)
                            End If
                        End If
                    Else
                        m_objGameDetails.AwayTeamCaptainId = IIf(IsDBNull(dsForCoach.Tables(2).Rows(0).Item("CAPTAIN_ID")), 0, dsForCoach.Tables(2).Rows(0).Item("CAPTAIN_ID"))
                        If dsForCoach.Tables(2).Rows(0).Item("COACH_ID").ToString() = "" Then
                            m_AwayCoach = 0
                        Else
                            m_AwayCoach = CInt(dsForCoach.Tables(2).Rows(0).Item("COACH_ID"))
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("START_FORMATION_ID").ToString() = "" Then
                            m_AwayFormation = 0
                        Else
                            m_AwayFormation = CInt(dsForCoach.Tables(2).Rows(0).Item("START_FORMATION_ID"))
                        End If


                        If dsForCoach.Tables(2).Rows(0).Item("FORMATION_SPEC_ID").ToString() = "" Then
                            m_AwayFormationSpec = 0
                        Else
                            m_AwayFormationSpec = CInt(dsForCoach.Tables(2).Rows(0).Item("FORMATION_SPEC_ID"))
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("KIT_NO").ToString() = "" Then
                            m_AwayKit = -1
                        Else
                            m_AwayKit = CInt(dsForCoach.Tables(2).Rows(0).Item("KIT_NO"))
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString() = "" Then
                            m_objTeamSetup.AwayShirtColor = Color.Empty
                        Else
                            m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString)
                        End If

                        If dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString() = "" Then
                            m_objTeamSetup.AwayShortColor = Color.Empty
                        Else
                            m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString)
                        End If


                        If dsForCoach.Tables(2).Rows.Count > 1 Then
                            m_objGameDetails.HomeTeamCaptainId = IIf(IsDBNull(dsForCoach.Tables(2).Rows(1).Item("CAPTAIN_ID")), 0, dsForCoach.Tables(2).Rows(1).Item("CAPTAIN_ID"))
                            If dsForCoach.Tables(2).Rows(1).Item("COACH_ID").ToString() = "" Then
                                m_HomeCoach = 0
                            Else
                                m_HomeCoach = CInt(dsForCoach.Tables(2).Rows(1).Item("COACH_ID"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("START_FORMATION_ID").ToString() = "" Then
                                m_HomeFormation = 0
                            Else
                                m_HomeFormation = CInt(dsForCoach.Tables(2).Rows(1).Item("START_FORMATION_ID"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("FORMATION_SPEC_ID").ToString() = "" Then
                                m_HomeFormationSpec = 0
                            Else
                                m_HomeFormationSpec = CInt(dsForCoach.Tables(2).Rows(1).Item("FORMATION_SPEC_ID"))
                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("KIT_NO").ToString() = "" Then
                                m_HomeKit = -1
                            Else
                                m_HomeKit = CInt(dsForCoach.Tables(2).Rows(1).Item("KIT_NO"))
                            End If



                            If dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString() = "" Then
                                m_objTeamSetup.HomeShirtColor = Color.Empty
                            Else
                                m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString)

                            End If

                            If dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString() = "" Then
                                m_objTeamSetup.HomeShortColor = Color.Empty
                            Else
                                m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString)

                            End If
                        End If
                    End If

                    m_objGameDetails.OffensiveCoachID = m_HomeCoach
                    m_objGameDetails.DefensiveCoachID = m_AwayCoach
                End If
                dsTeamGame.Tables(0).TableName = "Formation"
                dsTeamGame.Tables(1).TableName = "Coach"
                dsTeamGame.Tables(3).TableName = "FormationSpec"
                dsTeamGame.Tables(4).TableName = "UniformImages"


                dsTeamGame.Tables("Formation").Columns.Add("HOME")
                dsTeamGame.Tables("Formation").Columns.Add("AWAY")
                dsTeamGame.Tables("Coach").Columns.Add("Selected")
                dsTeamGame.Tables("FormationSpec").Columns.Add("HOME")
                dsTeamGame.Tables("FormationSpec").Columns.Add("AWAY")
                dsTeamGame.Tables("UniformImages").Columns.Add("Selected")
                'dsTeamGame.Tables("UniformImages").Columns.Add("AWAY")

            End If
            UpdateKitNo()

            LoadFormation()
            LoadCoach(intTeamID)

            UpdateFormationCoach()


            If dsForCoach.Tables(2).Rows.Count > 0 Then
                Dim DR() As DataRow
                DR = dsForCoach.Tables(2).Select("TEAM_ID = " & intTeamID)

                If DR.Length > 0 Then
                    If intTeamID = m_objclsGameDetails.HomeTeamID Then
                        cmbFormation.SelectedValue = m_HomeFormation
                        cmbFormationSpecification.SelectedValue = m_HomeFormationSpec
                        cmbCoach.SelectedValue = m_HomeCoach

                        For Each ctrl As Control In Me.pnlKit.Controls
                            If ctrl.GetType().ToString = "System.Windows.Forms.PictureBox" Then
                                If CInt(CType(ctrl, PictureBox).Tag) = m_HomeKit Then
                                    pnlHighlight.Visible = True
                                    pnlHighlight.Location = New Point(CType(ctrl, PictureBox).Location.X - 1, CType(ctrl, PictureBox).Location.Y - 1)
                                    Exit For
                                End If
                            End If
                        Next

                    Else
                        cmbFormation.SelectedValue = m_AwayFormation
                        cmbFormationSpecification.SelectedValue = m_AwayFormationSpec
                        cmbCoach.SelectedValue = m_AwayCoach

                        For Each ctrl As Control In Me.pnlKit.Controls
                            If ctrl.GetType().ToString = "System.Windows.Forms.PictureBox" Then
                                If CInt(CType(ctrl, PictureBox).Tag) = m_AwayKit Then
                                    pnlHighlight.Visible = True
                                    pnlHighlight.Location = New Point(CType(ctrl, PictureBox).Location.X - 1, CType(ctrl, PictureBox).Location.Y - 1)
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            'If dsForCoach.Tables(2).Rows.Count > 0 Then
            '    For introwCnt As Integer = 0 To dsForCoach.Tables(2).Rows.Count - 1
            '        If m_objGameDetails.HomeTeamID = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("TEAM_ID").ToString) Then
            '            m_HomeFormation = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("START_FORMATION_ID").ToString)
            '            m_HomeFormationSpec = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("FORMATION_SPEC_ID").ToString)
            '            m_HomeCoach = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("COACH_ID").ToString)
            '            'cmbFormation.SelectedValue = m_HomeFormation
            '            'cmbFormationSpecification.SelectedValue = m_HomeFormationSpec
            '            'cmbCoach.SelectedValue = m_HomeCoach
            '        Else
            '            m_AwayFormation = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("START_FORMATION_ID").ToString)
            '            m_AwayFormationSpec = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("FORMATION_SPEC_ID").ToString)
            '            m_AwayCoach = CInt(dsForCoach.Tables(2).Rows(introwCnt).Item("COACH_ID").ToString)
            '            'cmbFormation.SelectedValue = m_AwayFormation
            '            'cmbFormationSpecification.SelectedValue = m_AwayFormationSpec
            '            'cmbCoach.SelectedValue = m_AwayCoach

            '        End If
            '    Next
            'End If

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Private Sub UpdateKitNo()
        Try
            If dsTeamGame.Tables(2).Rows.Count > 0 Then
                Dim DR() As DataRow
                'UPDATION OF KIT NO
                If picKit1.Tag IsNot Nothing Or picKit2.Tag IsNot Nothing Or picKit3.Tag IsNot Nothing Or picKit4.Tag IsNot Nothing Or picKit5.Tag IsNot Nothing Or picKit6.Tag IsNot Nothing Or picKit7.Tag IsNot Nothing Or picKit8.Tag IsNot Nothing Then
                    If m_objclsGameDetails.HomeTeamID = intTeamID Then
                        DR = dsTeamGame.Tables("UniformImages").Select("KIT_NO = " & m_HomeKit & " AND TEAM_ID =" & intTeamID & "")
                    Else
                        DR = dsTeamGame.Tables("UniformImages").Select("KIT_NO = " & m_AwayKit & " AND TEAM_ID =" & intTeamID & "")
                    End If

                    If DR.Length > 0 Then
                        Dim drs() As DataRow
                        If intTeamID = m_objGameDetails.HomeTeamID Then
                            drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                            If drs.Length > 0 Then
                                drs(0).BeginEdit()
                                drs(0).Item("Selected") = System.DBNull.Value
                                drs(0).EndEdit()
                            End If

                            DR(0).BeginEdit()
                            'DR(0).Item("FORMATION_SPEC_ID") = CInt(cmbFormationSpecification.SelectedValue)
                            DR(0).Item("Selected") = intTeamID
                            DR(0).EndEdit()
                            m_HomeKit = CInt(DR(0).Item("KIT_NO"))
                        Else
                            drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                            If drs.Length > 0 Then
                                drs(0).BeginEdit()
                                drs(0).Item("Selected") = System.DBNull.Value
                                drs(0).EndEdit()
                            End If
                            DR(0).BeginEdit()
                            'DR(0).Item("FORMATION_SPEC_ID") = CInt(cmbFormation.SelectedValue)
                            DR(0).Item("Selected") = intTeamID
                            DR(0).EndEdit()
                            m_AwayKit = CInt(DR(0).Item("KIT_NO"))
                        End If
                    End If
                Else
                    Dim drs() As DataRow
                    If intTeamID = m_objGameDetails.HomeTeamID Then
                        drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("Selected") = System.DBNull.Value
                            drs(0).EndEdit()
                            m_HomeKit = -1
                        End If
                    Else
                        drs = dsTeamGame.Tables("UniformImages").Select("Selected = '" & intTeamID & "'")
                        If drs.Length > 0 Then
                            drs(0).BeginEdit()
                            drs(0).Item("Selected") = System.DBNull.Value
                            drs(0).EndEdit()
                            m_AwayKit = -1
                        End If
                    End If

                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UpdateFormationCoach()
        Try

            Dim DR() As DataRow
            DR = dsTeamGame.Tables("Formation").Select("FORMATION_ID = " & m_HomeFormation)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("HOME") = m_objGameDetails.HomeTeamID
                DR(0).EndEdit()
            End If

            DR = dsTeamGame.Tables("Formation").Select("FORMATION_ID = " & m_AwayFormation)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("AWAY") = m_objGameDetails.AwayTeamID
                DR(0).EndEdit()
            End If

            DR = dsTeamGame.Tables("FormationSpec").Select("FORMATION_SPEC_ID=" & m_HomeFormationSpec)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("HOME") = m_objGameDetails.HomeTeamID
                DR(0).EndEdit()
            End If

            DR = dsTeamGame.Tables("FormationSpec").Select("FORMATION_SPEC_ID=" & m_AwayFormationSpec)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("AWAY") = m_objGameDetails.AwayTeamID
                DR(0).EndEdit()
            End If


            DR = dsTeamGame.Tables("Coach").Select("COACH_ID=" & m_HomeCoach & "AND TEAM_ID = " & m_objGameDetails.HomeTeamID)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("Selected") = m_objGameDetails.HomeTeamID
                DR(0).EndEdit()
            End If


            DR = dsTeamGame.Tables("Coach").Select("COACH_ID=" & m_AwayCoach & "AND TEAM_ID = " & m_objGameDetails.AwayTeamID)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("Selected") = m_objGameDetails.AwayTeamID
                DR(0).EndEdit()
            End If


            dsTeamGame.AcceptChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub GetInjuredSuspendedPlayers()
        Try

            Dim ds As New DataSet
            ds = m_objBLTeamSetup.GetInjuredSuspendedPlayers(intTeamID)
            dgInjuriesSuspensions.DataSource = ds.Tables(0).DefaultView
            dgInjuriesSuspensions.Columns(0).Width = 0
            dgInjuriesSuspensions.Columns(0).Visible = False
            dgInjuriesSuspensions.Columns(1).HeaderText = "DATE"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DeselectSelectedItemFromCombo()
        Try
            Dim drs() As DataRow
            m_DsPlayersDesel = DsRostersInfo.Clone()
            For intTableCount As Integer = 0 To DsRostersInfo.Tables.Count - 1
                drs = DsRostersInfo.Tables(intTableCount).Select("STARTING_POSITION IS NULL")
                For Each DR As DataRow In drs
                    m_DsPlayersDesel.Tables(intTableCount).ImportRow(DR)
                Next
            Next



        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub RefreshFormationDisplay()
        Try
            UdcSoccerFormation1.USFFormation = cmbFormation.Text
            'UdcSoccerFormation1.USFFormationSpecification = cmbFormationSpecification.Text

            If cmbFormationSpecification.Text <> Nothing And cmbFormationSpecification.Text <> "" Then
                UdcSoccerFormation1.USFFormationSpecification = cmbFormationSpecification.SelectedValue.ToString
            End If



            If radHomeTeam.Checked Then
                UdcSoccerFormation1.USFShirtColor = m_objTeamSetup.HomeShirtColor
                UdcSoccerFormation1.USFShortColor = m_objTeamSetup.HomeShortColor
                UdcSoccerFormation1.USFRoster = DsRostersInfo.Tables("Home")
            Else
                UdcSoccerFormation1.USFShirtColor = m_objTeamSetup.AwayShirtColor
                UdcSoccerFormation1.USFShortColor = m_objTeamSetup.AwayShortColor
                UdcSoccerFormation1.USFRoster = DsRostersInfo.Tables("Away")
            End If

            Dim dtStartingLineup As New DataTable
            Dim drStartingLineup As DataRow
            dtStartingLineup.Columns.Add("LINEUP")
            dtStartingLineup.Columns(0).DataType = GetType(Integer)
            dtStartingLineup.Columns.Add("PLAYER_ID")
            dtStartingLineup.Columns(1).DataType = GetType(Integer)
            dtStartingLineup.Columns.Add("PLAYER")
            dtStartingLineup.Columns.Add("UNIFORM")
            dtStartingLineup.Columns.Add("LAST_NAME")
            dtStartingLineup.Columns.Add("MONIKER")
            dtStartingLineup.Columns.Add("POSITION_ABBREV")

            For Each dgvr As DataGridViewRow In dgvStartingLineup.Rows
                If Not dgvr.Cells(2).Value Is Nothing Then
                    drStartingLineup = dtStartingLineup.NewRow
                    drStartingLineup("LINEUP") = dgvr.Cells(0).Value
                    drStartingLineup("PLAYER_ID") = dgvr.Cells(2).Value
                    drStartingLineup("PLAYER") = dgvr.Cells(2).FormattedValue
                    drStartingLineup("UNIFORM") = dgvr.Cells(1).Value
                    dtStartingLineup.Rows.Add(drStartingLineup)
                End If
            Next

            Dim dtBench As New DataTable
            Dim drBench As DataRow
            dtBench.Columns.Add("LINEUP")
            dtBench.Columns(0).DataType = GetType(Integer)
            dtBench.Columns.Add("PLAYER_ID")
            dtBench.Columns(1).DataType = GetType(Integer)
            dtBench.Columns.Add("PLAYER")
            dtBench.Columns.Add("UNIFORM")
            dtBench.Columns.Add("LAST_NAME")
            dtBench.Columns.Add("MONIKER")
            dtBench.Columns.Add("POSITION_ABBREV")

            For Each dgvr As DataGridViewRow In dgvBench.Rows
                If Not dgvr.Cells(2).Value Is Nothing Then
                    drBench = dtBench.NewRow
                    drBench("LINEUP") = dgvr.Cells(0).Value
                    drBench("PLAYER_ID") = dgvr.Cells(2).Value
                    drBench("PLAYER") = dgvr.Cells(2).FormattedValue
                    drBench("UNIFORM") = dgvr.Cells(1).Value
                    dtBench.Rows.Add(drBench)
                End If
            Next

            UdcSoccerFormation1.USFStartingLineup = dtStartingLineup
            UdcSoccerFormation1.USFBench = dtBench

            UdcSoccerFormation1.USFRefreshFormationDisplay()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadFormationSpecification(ByVal FormationID As Integer)
        Try
            Dim dsFormationspec As New DataSet
            dsFormationspec = m_objBLTeamSetup.GetFormationSpecification(FormationID, m_objGameDetails.languageid)
            LoadControl(cmbFormationSpecification, dsFormationspec.Tables(0), "FORMATION_SPEC_ID", "FORMATION_SPEC")
            If cmbFormationSpecification.Items.Count - 1 > 0 Then
                If FormationID = 0 Then
                    cmbFormationSpecification.SelectedIndex = 0
                Else
                    cmbFormationSpecification.SelectedIndex = 1
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub GetUniformImages()
        Try
            Dim dsUniform As New DataSet
            dsUniform = m_objBLTeamSetup.GetUniformPictures(intTeamID)

            clearPictureBox()
            Dim v_memUniform As MemoryStream
            If dsUniform.Tables(0).Rows.Count > 0 Then
                For introwcnt As Integer = 0 To dsUniform.Tables(0).Rows.Count - 1
                    v_memUniform = New MemoryStream(DirectCast(dsUniform.Tables(0).Rows(introwcnt).Item("UNIFORM_IMAGE"), Byte()))
                    CType(Me.pnlKit.Controls("picKit" & introwcnt + 1), PictureBox).Image = New Bitmap(v_memUniform)
                    CType(Me.pnlKit.Controls("picKit" & introwcnt + 1), PictureBox).Tag = dsUniform.Tables(0).Rows(introwcnt).Item("KIT_NO")
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub clearPictureBox()
        Try
            picKit1.Image = Nothing
            picKit2.Image = Nothing
            picKit3.Image = Nothing
            picKit4.Image = Nothing
            picKit5.Image = Nothing
            picKit6.Image = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function IsPBPEntryAllowed() As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                Return True
            Else
                If m_objGameDetails.IsPrimaryReporterDown Then
                    MessageDialog.Show(strmessage23 + " ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                End If
                If m_objGameDetails.AssistersLastEntryStatus = False And m_objGameDetails.ModuleID = 1 Then
                    MessageDialog.Show(strmessage24 + " ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                Else
                    Return True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''Private Sub UpdatePictureBoxTags(ByVal intTag As Integer)
    ''    Try
    ''        For i As Integer = 0 To pnlKit.Controls.Count - 1
    ''            If CInt(pnlKit.Controls(i).Tag) <> intTag Then
    ''                pnlKit.Controls(i).Tag = Nothing
    ''            End If
    ''        Next
    ''    Catch ex As Exception
    ''        Throw ex
    ''    End Try
    ''End Sub


    Private Sub UpdatelabelColor()
        Try
            Dim DS As New DataSet
            DS = m_objBLTeamSetup.GetUniformColors(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)
            Dim drs() As DataRow
            If DS.Tables(1).Rows.Count > 0 Then
                drs = DS.Tables(1).Select("TEAM_ID=" & m_objclsGameDetails.HomeTeamID & "")
                If drs.Length > 0 Then
                    If drs(0).Item("SHIRT_COLOR").ToString = "" Then
                        'm_objTeamSetup.HomeShirtColor = Color.Empty
                    End If

                    If drs(0).Item("SHIRT_COLOR").ToString IsNot "" Then
                        If drs(0).Item("SHIRT_COLOR").ToString <> clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShirtColor) Then
                            ' m_objTeamSetup.HomeShirtColor = Color.Empty
                        Else
                            m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(drs(0).Item("SHIRT_COLOR").ToString)
                        End If
                    End If

                    If drs(0).Item("SHORT_COLOR").ToString = "" Then
                        ' m_objTeamSetup.HomeShortColor = Color.Empty
                    End If

                    If drs(0).Item("SHORT_COLOR").ToString IsNot "" Then
                        If drs(0).Item("SHORT_COLOR").ToString <> clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.HomeShortColor) Then
                            'm_objTeamSetup.HomeShortColor = Color.Empty
                        Else
                            m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(drs(0).Item("SHORT_COLOR").ToString)
                        End If
                    End If
                End If

                drs = DS.Tables(1).Select("TEAM_ID=" & m_objclsGameDetails.AwayTeamID & "")
                If drs.Length > 0 Then
                    If drs(0).Item("SHIRT_COLOR").ToString = "" Then
                        'm_objTeamSetup.AwayShirtColor = Color.Empty
                    End If

                    If drs(0).Item("SHIRT_COLOR").ToString IsNot "" Then
                        If drs(0).Item("SHIRT_COLOR").ToString <> clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.AwayShirtColor) Then
                            'm_objTeamSetup.AwayShirtColor = Color.Empty
                        Else
                            m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(drs(0).Item("SHIRT_COLOR").ToString)
                        End If
                    End If

                    If drs(0).Item("SHORT_COLOR").ToString = "" Then
                        'm_objTeamSetup.AwayShortColor = Color.Empty
                    End If

                    If drs(0).Item("SHORT_COLOR").ToString IsNot "" Then
                        If drs(0).Item("SHORT_COLOR").ToString <> clsUtility.ConvertColorToHexadecimalString(m_objTeamSetup.AwayShortColor) Then
                            'm_objTeamSetup.AwayShortColor = Color.Empty
                        Else
                            m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(drs(0).Item("SHORT_COLOR").ToString)
                        End If
                    End If
                End If
            Else
                'm_objTeamSetup.HomeShirtColor = Color.Empty
                'm_objTeamSetup.HomeShortColor = Color.Empty
                'm_objTeamSetup.AwayShirtColor = Color.Empty
                'm_objTeamSetup.AwayShortColor = Color.Empty
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'reordering current players so as to place goal keepers at Top
    Private Function ReOrderCurrentPlayers(ByVal DsCurrentPlayer As DataSet) As DataSet
        Try
            Dim DsCurrent As DataSet = DsCurrentPlayer.Clone()
            If DsCurrentPlayer.Tables.Count > 0 Then
                For intTableCount As Integer = 0 To DsCurrentPlayer.Tables.Count - 1
                    Dim Drs() As DataRow
                    Drs = DsCurrentPlayer.Tables(intTableCount).Select("POSITION_ID_1 = 1")
                    For Each DR As DataRow In Drs
                        DsCurrent.Tables(intTableCount).ImportRow(DR)
                    Next
                    Drs = DsCurrentPlayer.Tables(intTableCount).Select("POSITION_ID_1 <> 1")
                    For Each DR As DataRow In Drs
                        DsCurrent.Tables(intTableCount).ImportRow(DR)
                    Next
                Next
            End If
            DsCurrent.AcceptChanges()
            Return DsCurrent

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ArelineupsEntered() As Boolean
        Try
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                If MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Cancel Then
                    m_blnCheck = False
                    Return True
                    Exit Function
                Else
                    If intTeamID = m_objclsGameDetails.HomeTeamID Then
                        m_blnCheck = True
                        radHomeTeam.Checked = True
                        Exit Function
                    Else
                        m_blnCheck = True
                        radAwayTeam.Checked = True
                        Exit Function
                    End If
                End If
            Else
                m_blnCheck = False
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function




#End Region

    Public Sub New(ByVal TeamChecked As String) ' = "Home")
        StrTeamChecked = TeamChecked
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub


End Class