﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlayerTouches
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picTopBar2 = New System.Windows.Forms.PictureBox()
        Me.picReporterBar2 = New System.Windows.Forms.PictureBox()
        Me.stssMain2 = New System.Windows.Forms.StatusStrip()
        Me.btnSave2 = New System.Windows.Forms.Button()
        Me.btnPrint2 = New System.Windows.Forms.Button()
        Me.btnRefresh2 = New System.Windows.Forms.Button()
        Me.btnClose2 = New System.Windows.Forms.Button()
        Me.PrintDialog2 = New System.Windows.Forms.PrintDialog()
        Me.SaveFileDialog2 = New System.Windows.Forms.SaveFileDialog()
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument()
        Me.pnlReport2 = New System.Windows.Forms.Panel()
        Me.wbReport2 = New System.Windows.Forms.WebBrowser()
        Me.picButtonBar2 = New System.Windows.Forms.Panel()
        CType(Me.picTopBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picReporterBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlReport2.SuspendLayout()
        Me.picButtonBar2.SuspendLayout()
        Me.SuspendLayout()
        '
        'picTopBar2
        '
        Me.picTopBar2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTopBar2.BackColor = System.Drawing.Color.GhostWhite
        Me.picTopBar2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar2.Location = New System.Drawing.Point(0, -1)
        Me.picTopBar2.Name = "picTopBar2"
        Me.picTopBar2.Size = New System.Drawing.Size(877, 15)
        Me.picTopBar2.TabIndex = 0
        Me.picTopBar2.TabStop = False
        '
        'picReporterBar2
        '
        Me.picReporterBar2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picReporterBar2.BackColor = System.Drawing.Color.GhostWhite
        Me.picReporterBar2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar2.Location = New System.Drawing.Point(0, 11)
        Me.picReporterBar2.Name = "picReporterBar2"
        Me.picReporterBar2.Size = New System.Drawing.Size(877, 21)
        Me.picReporterBar2.TabIndex = 1
        Me.picReporterBar2.TabStop = False
        '
        'stssMain2
        '
        Me.stssMain2.AutoSize = False
        Me.stssMain2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.stssMain2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stssMain2.Location = New System.Drawing.Point(0, 532)
        Me.stssMain2.Name = "stssMain2"
        Me.stssMain2.Size = New System.Drawing.Size(877, 18)
        Me.stssMain2.TabIndex = 271
        Me.stssMain2.Text = "StatusStrip1"
        '
        'btnSave2
        '
        Me.btnSave2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave2.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave2.ForeColor = System.Drawing.Color.White
        Me.btnSave2.Location = New System.Drawing.Point(13, 5)
        Me.btnSave2.Name = "btnSave2"
        Me.btnSave2.Size = New System.Drawing.Size(75, 25)
        Me.btnSave2.TabIndex = 272
        Me.btnSave2.Text = "Save..."
        Me.btnSave2.UseVisualStyleBackColor = False
        '
        'btnPrint2
        '
        Me.btnPrint2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint2.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnPrint2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnPrint2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint2.ForeColor = System.Drawing.Color.White
        Me.btnPrint2.Location = New System.Drawing.Point(94, 5)
        Me.btnPrint2.Name = "btnPrint2"
        Me.btnPrint2.Size = New System.Drawing.Size(75, 25)
        Me.btnPrint2.TabIndex = 273
        Me.btnPrint2.Text = "Print..."
        Me.btnPrint2.UseVisualStyleBackColor = False
        '
        'btnRefresh2
        '
        Me.btnRefresh2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh2.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(20, Byte), Integer))
        Me.btnRefresh2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnRefresh2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh2.ForeColor = System.Drawing.Color.White
        Me.btnRefresh2.Location = New System.Drawing.Point(175, 5)
        Me.btnRefresh2.Name = "btnRefresh2"
        Me.btnRefresh2.Size = New System.Drawing.Size(75, 25)
        Me.btnRefresh2.TabIndex = 276
        Me.btnRefresh2.Text = "Refresh..."
        Me.btnRefresh2.UseVisualStyleBackColor = False
        '
        'btnClose2
        '
        Me.btnClose2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose2.ForeColor = System.Drawing.Color.White
        Me.btnClose2.Location = New System.Drawing.Point(790, 5)
        Me.btnClose2.Name = "btnClose2"
        Me.btnClose2.Size = New System.Drawing.Size(75, 25)
        Me.btnClose2.TabIndex = 274
        Me.btnClose2.Text = "Close"
        Me.btnClose2.UseVisualStyleBackColor = False
        '
        'PrintDialog2
        '
        Me.PrintDialog2.UseEXDialog = True
        '
        'pnlReport2
        '
        Me.pnlReport2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlReport2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReport2.Controls.Add(Me.wbReport2)
        Me.pnlReport2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlReport2.Location = New System.Drawing.Point(13, 49)
        Me.pnlReport2.Name = "pnlReport2"
        Me.pnlReport2.Size = New System.Drawing.Size(852, 429)
        Me.pnlReport2.TabIndex = 275
        '
        'wbReport2
        '
        Me.wbReport2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbReport2.Location = New System.Drawing.Point(0, 0)
        Me.wbReport2.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbReport2.Name = "wbReport2"
        Me.wbReport2.Size = New System.Drawing.Size(850, 427)
        Me.wbReport2.TabIndex = 0
        '
        'picButtonBar2
        '
        Me.picButtonBar2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picButtonBar2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar2.Controls.Add(Me.btnClose2)
        Me.picButtonBar2.Controls.Add(Me.btnSave2)
        Me.picButtonBar2.Controls.Add(Me.btnRefresh2)
        Me.picButtonBar2.Controls.Add(Me.btnPrint2)
        Me.picButtonBar2.Location = New System.Drawing.Point(0, 499)
        Me.picButtonBar2.Name = "picButtonBar2"
        Me.picButtonBar2.Size = New System.Drawing.Size(877, 39)
        Me.picButtonBar2.TabIndex = 277
        '
        'frmPlayerTouches
        '
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(877, 550)
        Me.Controls.Add(Me.pnlReport2)
        Me.Controls.Add(Me.stssMain2)
        Me.Controls.Add(Me.picReporterBar2)
        Me.Controls.Add(Me.picTopBar2)
        Me.Controls.Add(Me.picButtonBar2)
        Me.Name = "frmPlayerTouches"
        Me.Text = "Player Stats"
        CType(Me.picTopBar2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picReporterBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlReport2.ResumeLayout(False)
        Me.picButtonBar2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picCompanyLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents picButtonBar As System.Windows.Forms.PictureBox
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents pnlReport As System.Windows.Forms.Panel
    Friend WithEvents wbReport As System.Windows.Forms.WebBrowser
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents picTopBar2 As System.Windows.Forms.PictureBox
    Friend WithEvents picReporterBar2 As System.Windows.Forms.PictureBox
    Friend WithEvents wbReport2 As System.Windows.Forms.WebBrowser
    Friend WithEvents stssMain2 As System.Windows.Forms.StatusStrip
    Friend WithEvents btnSave2 As System.Windows.Forms.Button
    Friend WithEvents btnPrint2 As System.Windows.Forms.Button
    Friend WithEvents btnRefresh2 As System.Windows.Forms.Button
    Friend WithEvents btnClose2 As System.Windows.Forms.Button
    Friend WithEvents PrintDialog2 As System.Windows.Forms.PrintDialog
    Friend WithEvents SaveFileDialog2 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents pnlReport2 As System.Windows.Forms.Panel
    Friend WithEvents picButtonBar2 As System.Windows.Forms.Panel
End Class
