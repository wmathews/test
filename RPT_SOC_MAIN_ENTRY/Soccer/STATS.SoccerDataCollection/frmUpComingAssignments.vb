﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmUpComingAssignments
' Author        : Shravani
' Created Date  : 16-05-09
' Description   : Displays Games Assigned to the Reporter
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmUpComingAssignments

#Region " Constants & Variables "
    Private m_Assignments As New clsUpComingAssignments
    Private m_intReporterID As Integer
    Dim objUtil As New clsUtility
    Private m_strReporterName As String
    Private m_intDay As Integer
    Private m_dsLeague As New DataSet
    Private m_blnIsComboLoaded As Boolean = False
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private lsupport As New languagesupport
    Dim strmessage As String = "No games are assigned to"
    Dim strmessage1 As String = "Please choose duration"
    Dim strmessage2 As String = "Please choose league"
    Dim strmessage3 As String = "Please choose league and duration to generate upcoming assignments"
#End Region

#Region "Event Handler"

    Public Sub New(ByVal ReporterID As Integer, ByVal ReporterName As String)
        m_intReporterID = ReporterID
        m_strReporterName = ReporterName
        InitializeComponent()
    End Sub

    Private Sub frmUpcomingAssignments_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmUpComingAssignments", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Me.Text = "Upcoming Assignments"
            lblUserName.Text = "Reporter: " & m_strReporterName
            If CInt(m_objLoginDetails.LanguageID) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                lblUserName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblUserName.Text)
                lblLeagueId.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLeagueId.Text)
                lblDuration.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblDuration.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)

                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Game Date")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Game")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "League")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Arena")
                lvwUpcomingAssignment.Columns(0).Text = g4
                lvwUpcomingAssignment.Columns(1).Text = g5
                lvwUpcomingAssignment.Columns(2).Text = g6
                lvwUpcomingAssignment.Columns(3).Text = g7
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)


            End If
            cmbDay.Items.Add("Today")
            cmbDay.Items.Add("1 Day")
            cmbDay.Items.Add("2 Day")
            cmbDay.Items.Add("3 Day")
            cmbDay.Items.Add("4 Day")
            cmbDay.Items.Add("5 Day")
            cmbDay.Items.Add("6 Day")
            cmbDay.Items.Add("1 Week")
            cmbDay.Items.Add("2 Week")
            cmbDay.Items.Add("3 Week")
            cmbDay.Items.Add("4 Week")
            cmbDay.Items.Add("1 Month")
            cmbDay.Items.Add("2 Month")
            cmbDay.Items.Add("3 Month")
            cmbDay.Items.Add("4 Month")
            LoadLeagueCombo()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

    Private Sub cboDay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDay.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbDay.Text, Nothing, 1, 0)
            Try
                Select Case cmbDay.SelectedItem.ToString
                    Case "Today"
                        m_intDay = 0
                    Case "1 Day"
                        m_intDay = 1
                    Case "2 Day"
                        m_intDay = 2
                    Case "3 Day"
                        m_intDay = 3
                    Case "4 Day"
                        m_intDay = 4
                    Case "5 Day"
                        m_intDay = 5
                    Case "6 Day"
                        m_intDay = 6
                    Case "1 Week"
                        m_intDay = 7
                    Case "2 Week"
                        m_intDay = 14
                    Case "3 Week"
                        m_intDay = 21
                    Case "4 Week"
                        m_intDay = 28
                    Case "1 Month"
                        m_intDay = 30
                    Case "2 Month"
                        m_intDay = 60
                    Case "3 Month"
                        m_intDay = 90
                    Case "4 Month"
                        m_intDay = 120

                End Select
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnGenerate.Text, Nothing, 1, 0)
            If cboLeague.SelectedIndex = -1 And cmbDay.SelectedIndex = -1 Then
                MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If cboLeague.SelectedIndex = -1 Then
                MessageDialog.Show(strmessage2 + " ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If cmbDay.SelectedIndex = -1 Then
                MessageDialog.Show(strmessage1 + " ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor
            Dim dsUpcomingAssignment As New DataSet
            Dim StrGameDate As String = ""

            dsUpcomingAssignment = m_Assignments.DownloadUpComingAssignments(m_intReporterID, m_intDay, CInt(cboLeague.SelectedValue))
            lvwUpcomingAssignment.Items.Clear()
            If (dsUpcomingAssignment.Tables(0).Rows.Count > 0) Then
                Dim introwCount As Integer
                For introwCount = 0 To dsUpcomingAssignment.Tables(0).Rows.Count - 1
                    'Dim ls As New ListViewItem(dsUpcomingAssignment.Tables(0).Rows(introwCount).Item("GAME_DATE").ToString)
                    'Dec 07 2009 - Shirley modified to display based on TimeZone Mode
                    Dim ls As New ListViewItem
                    Dim Dt As DateTime = DateTimeHelper.GetDate(dsUpcomingAssignment.Tables(0).Rows(introwCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                    If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                        StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                        ls = New ListViewItem(StrGameDate)
                    Else
                        StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                        ls = New ListViewItem(StrGameDate)
                    End If
                    ls.SubItems.Add(dsUpcomingAssignment.Tables(0).Rows(introwCount).Item("Game").ToString)
                    ls.SubItems.Add(dsUpcomingAssignment.Tables(0).Rows(introwCount).Item("LEAGUE_ABBREV").ToString)
                    ls.SubItems.Add(dsUpcomingAssignment.Tables(0).Rows(introwCount).Item("FIELD_NAME").ToString)
                    lvwUpcomingAssignment.Items.Add(ls)
                Next
            Else
                MessageDialog.Show(strmessage + " " & m_strReporterName & ".", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub LoadLeagueCombo()
        Try
            m_dsLeague = m_Assignments.getLeaguedata()
            'm_dsLeague = m_Assignments.LoadLeague()
            Dim dtLeague As New DataTable
            dtLeague = m_dsLeague.Tables(0).Copy
            If m_dsLeague.Tables.Count > 0 Then
                If m_dsLeague.Tables(0).Rows.Count > 0 Then
                    clsUtility.LoadControl(cboLeague, clsUtility.SelectTypeDisplay.ALL, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
                    cboLeague.SelectedIndex = -1
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cboLeague_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeague.SelectedIndexChanged
        Try
            If (cboLeague.Text <> "" And cboLeague.Text <> "System.Data.DataRowView") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cboLeague.Text, Nothing, 1, 0)
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class