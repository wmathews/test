﻿
#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : udcSoccerField
' Author        : Fiaz Ahmed
' Created Date  : 21st May 2009
' Description   : This user control represents a soccer field.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               | Fiaz                | 15/6/2015         | Changes for TOSOCRS-174: Smaller XY Location Markers
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Imports System.ComponentModel

Public Class udcSoccerField

#Region " Constants & Variables "
    Private Const FIELD_MARGIN_HORIZONTAL As Integer = 3
    Private Const FIELD_MARGIN_VERTICAL As Integer = 2
    Private Const FIELD_HEIGHT As Integer = 70
    Private Const CENTER_CIRCLE_DIAMETER As Double = 18.3
    Private Const PENALTY_BOX_HEIGHT As Double = 40.32
    Private Const PENALTY_BOX_WIDTH As Double = 16.5
    Private Const GOAL_BOX_HEIGHT As Double = 18.32
    Private Const GOAL_BOX_WIDTH As Double = 5.5
    Private Const GOAL_POST_HEIGHT As Double = 7.32
    Private Const GOAL_POST_WIDTH As Double = 2.5
    Private Const POINT_WIDTH_HEIGHT As Integer = 1
    Private Const PENALTY_POINT_DISTANCE As Integer = 11
    Private Const PENALTY_ARC_HEIGHT As Double = 18.3
    Private Const CORNER_ARC_HEIGHT As Double = 2
    Private Const OPTIONAL_MARK_DISTANCE As Double = 10.15

    Private m_blnHomeTeamOnLeft As Boolean = True
    Private m_intFieldX As Integer
    Private m_intFieldY As Integer
    Private m_intFieldWidth As Integer
    Private m_intFieldHeight As Integer
    Private m_intFieldMarginHorizontal As Integer
    Private m_intFieldMarginVertical As Integer
    Private m_blnDisplayArrow As Boolean = True

    Private m_blnDisplayTeamName As Boolean = True
    Private m_strHomeTeamName As String = ""
    Private m_strAwayTeamName As String = ""

    Private m_blnDisplayTouchInstructions As Boolean = False
    Private m_strEventText As String = ""

    Private m_dtFieldGrid As DataTable

    Public Event USFClick(ByVal sender As Object, ByVal e As USFEventArgs)
    Public Event USFRightClick(ByVal sender As Object, ByVal e As USFEventArgs)
    Public Event USFMarkRemoved(ByVal sender As Object, ByVal e As USFEventArgs)
    Public Event USFMouseMove(ByVal sender As Object, ByVal e As USFEventArgs)

    Private m_dtFieldMarks As DataTable
    Private m_intNextMark As Integer = 1
    Private m_intMarkCount As Integer = 0
    Private m_intMaximumMarksAllowed As Integer = 2
    Private m_blnEnableOneClickRelocation As Boolean = False
    Private m_blnConfineToLeftPenaltyBox As Boolean = False
    Private m_blnConfineToRightPenaltyBox As Boolean = False

    Private m_rctLeftGoalPost As Rectangle
    Private m_rctRightGoalPost As Rectangle
    Private m_ptLeftPenaltyPoint As Point
    Private m_ptRightPenaltyPoint As Point
    Private m_rctLeftPenaltyBox As Rectangle
    Private m_rctRightPenaltyBox As Rectangle

    Private m_objUtility As New STATS.Utility.clsUtility

    Public Enum enMarkLocation
        RightPost
        LeftPost
        Crossbar
        SidelineAndGoalline
        Sideline
        PenaltyMarkLeftHalf
        PenaltyMarkRightHalf
    End Enum

#End Region

#Region " Event handlers "

    ''' <summary>
    ''' Occurs when the user control loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub udcSoccerField_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'create a datatable with 2 rows to hold data corresponding to 2 marks
            Dim drMark As DataRow

            m_dtFieldMarks = New DataTable
            m_dtFieldMarks.Columns.Add("Mark", System.Type.GetType("System.Int16"))         '1 or 2 representing 1st or 2nd mark
            m_dtFieldMarks.Columns.Add("FieldArea", System.Type.GetType("System.String"))   'A sring value corresponding to a cell in virtual 36 X 24 Grid where the mark is located
            m_dtFieldMarks.Columns.Add("X", System.Type.GetType("System.Int16"))            'x-coordinate of the mark
            m_dtFieldMarks.Columns.Add("Y", System.Type.GetType("System.Int16"))            'y-coordinate of the mark

            'add row for the first mark
            drMark = m_dtFieldMarks.NewRow
            drMark(0) = 1
            m_dtFieldMarks.Rows.Add(drMark)

            'add row for the second mark
            drMark = m_dtFieldMarks.NewRow
            drMark(0) = 2
            m_dtFieldMarks.Rows.Add(drMark)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Occurs when user control is resized.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub udcSoccerField_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Try
            Dim intHeight As Integer
            Dim intWidth As Integer

            'When developer drags the user control on form,
            'ensure that the resize always maintains 2:3 ratio for height:width
            If Me.Height / 2 > Me.Width / 3 Then
                intHeight = CInt(2 * (Me.Width / 3))
                While Not intHeight Mod 2 = 0
                    intHeight = intHeight + 1
                End While
                Me.Height = intHeight
                Me.Width = CInt(intHeight / 2 * 3) ''''
            Else
                intWidth = CInt(3 * (Me.Height / 2))
                While Not intWidth Mod 3 = 0
                    intWidth = intWidth + 1
                End While
                Me.Width = intWidth
                Me.Height = CInt(intWidth / 3 * 2) '''''
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Occurs when the picture box is clicked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub picSoccerField_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picSoccerField.Click
        Try
            Dim evargMouse As MouseEventArgs
            Dim strFieldArea As String
            Dim intX As Integer
            Dim intY As Integer

            evargMouse = CType(e, MouseEventArgs)

            Dim MyEventArgs As USFEventArgs

            'fire the event only for left mouse click
            If Not evargMouse.Button = Windows.Forms.MouseButtons.Left Then
                MyEventArgs = New USFEventArgs("", 0, 0, Nothing)
                RaiseEvent USFRightClick(sender, MyEventArgs)
                Exit Try
            End If

            'fire the event only if user clicks within the field boundary
            If evargMouse.X < m_intFieldMarginHorizontal Or evargMouse.Y < m_intFieldMarginVertical Then
                Exit Try
            End If
            If evargMouse.X > m_intFieldMarginHorizontal + m_intFieldWidth Or evargMouse.Y > m_intFieldMarginVertical + m_intFieldHeight Then
                Exit Try
            End If

            'exit if user clicks ouside left half penalty box and USFConfineToLeftPenaltyBox = true (TOPZ-933)
            If m_blnConfineToLeftPenaltyBox = True And Not m_rctLeftPenaltyBox.Contains(evargMouse.X, evargMouse.Y) Then
                Exit Try
            End If

            'exit if user clicks ouside right half penalty box and USFConfineToRightPenaltyBox = true (TOPZ-933)
            If m_blnConfineToRightPenaltyBox = True And Not m_rctRightPenaltyBox.Contains(evargMouse.X, evargMouse.Y) Then
                Exit Try
            End If

            'if one clikc relocation is needed, clear existing mark and force set the value of m_intMaximumMarksAllowed to 1
            'this functionalit is implemented especially for touch module where user should be allowed to chose a new mark location with one click
            If m_blnEnableOneClickRelocation = True Then
                USFClearMarks()
                m_intMaximumMarksAllowed = 1
            End If

            If m_intMarkCount >= m_intMaximumMarksAllowed Then
                Exit Try
            End If

            'get the field area corresponding to click location
            strFieldArea = GetFieldArea(evargMouse.X, evargMouse.Y)

            'calculate the x and y corresponding to original field size
            intX = CInt((evargMouse.X - m_intFieldMarginHorizontal) / m_intFieldHeight * FIELD_HEIGHT)
            intY = CInt((evargMouse.Y - m_intFieldMarginVertical) / m_intFieldHeight * FIELD_HEIGHT)

            'update mark details in the datatable
            If m_intNextMark = 1 Then
                m_dtFieldMarks.Rows(0).Item("FieldArea") = strFieldArea
                m_dtFieldMarks.Rows(0).Item("X") = intX
                m_dtFieldMarks.Rows(0).Item("Y") = intY
            ElseIf m_intNextMark = 2 Then
                m_dtFieldMarks.Rows(1).Item("FieldArea") = strFieldArea
                m_dtFieldMarks.Rows(1).Item("X") = intX
                m_dtFieldMarks.Rows(1).Item("Y") = intY
            End If

            'place the mark on user control
            SetMark(evargMouse.X, evargMouse.Y)



            'raise the user control's USFClick event containing
            'the current mouse x, y, field area
            'and the datatable with mark details
            MyEventArgs = New USFEventArgs(strFieldArea, intX, intY, m_dtFieldMarks)
            RaiseEvent USFClick(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Occurs when mouse is moved over the picture box.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub picSoccerField_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picSoccerField.MouseMove
        Try
            Dim evargMouse As MouseEventArgs
            Dim strFieldArea As String
            Dim intX As Integer
            Dim intY As Integer

            evargMouse = CType(e, MouseEventArgs)

            'fire the event only if user clicks within the field boundary
            If evargMouse.X < m_intFieldMarginHorizontal Or evargMouse.Y < m_intFieldMarginVertical Or evargMouse.X > m_intFieldMarginHorizontal + m_intFieldWidth Or evargMouse.Y > m_intFieldMarginVertical + m_intFieldHeight Then
                strFieldArea = ""
                intX = Nothing
                intY = Nothing
            Else
                strFieldArea = GetFieldArea(evargMouse.X, evargMouse.Y)
                intX = CInt((evargMouse.X - m_intFieldMarginHorizontal) / m_intFieldHeight * FIELD_HEIGHT)
                intY = CInt((evargMouse.Y - m_intFieldMarginVertical) / m_intFieldHeight * FIELD_HEIGHT)
            End If

            Dim MyEventArgs As USFEventArgs

            MyEventArgs = New USFEventArgs(strFieldArea, intX, intY, m_dtFieldMarks)

            'raise the user control's USFMouseMove event containing
            'the current mouse x, y, field area
            'and the datatable with existing mark details
            RaiseEvent USFMouseMove(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Occurs when the picture box is painted.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub picSoccerField_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles picSoccerField.Paint
        Try
            DrawField(e)        'draw the soccer field
            DefineFieldArea()   'define the virtual 36 X 24 Grid for picking field area values
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


#End Region

#Region " Private methods "

    ''' <summary>
    ''' Draws the soccer field.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub DrawField(ByVal e As System.Windows.Forms.PaintEventArgs)
        Try
            Dim rctField As Rectangle
            Dim rctCenterCircle As Rectangle
            Dim rctLeftPenaltyBox As Rectangle
            Dim rctRightPenaltyBox As Rectangle
            Dim rctLeftGoalBox As Rectangle
            Dim rctRightGoalBox As Rectangle
            Dim rctLeftGoalPost As Rectangle
            Dim rctRightGoalPost As Rectangle
            Dim rctLeftPenaltyArc As Rectangle
            Dim rctRightPenaltyArc As Rectangle

            Dim pnDrawing As Pen
            Dim clrPenColor As Color
            Dim intPenWidth As Integer = 2

            Dim pnTemp As Pen
            pnTemp = New Pen(Color:=Color.Red, Width:=intPenWidth)
            Dim pnTemp2 As Pen
            pnTemp2 = New Pen(Color:=Color.Blue, Width:=intPenWidth)

            Dim dblCircleDiameter As Double
            Dim dblPenaltyBoxHeight As Double
            Dim dblPenaltyBoxWidth As Double
            Dim dblGoalBoxHeight As Double
            Dim dblGoalBoxWidth As Double
            Dim dblGoalPostHeight As Double
            Dim dblGoalPostWidth As Double
            Dim dblPenaltyPointDistance As Double
            Dim dblPenaltyArcHeight As Double

            Dim strLeftTeamName As String = ""
            Dim strRightTeamName As String = ""

            m_intFieldMarginHorizontal = CInt(FIELD_MARGIN_HORIZONTAL / FIELD_HEIGHT * Me.Height)
            While Not m_intFieldMarginHorizontal Mod 3 = 0
                m_intFieldMarginHorizontal = m_intFieldMarginHorizontal + 1
            End While
            m_intFieldMarginVertical = CInt(m_intFieldMarginHorizontal / 3 * 2)
            'dblFieldMarginVertical = FIELD_MARGIN_VERTICAL / FIELD_HEIGHT * Me.Height
            clrPenColor = Color.White
            pnDrawing = New Pen(Color:=clrPenColor, Width:=intPenWidth)

            'draw field boundaries
            rctField = New Rectangle(x:=m_intFieldMarginHorizontal, y:=m_intFieldMarginVertical, Width:=CInt(picSoccerField.Width - (m_intFieldMarginHorizontal * 2)), Height:=CInt(picSoccerField.Height - (m_intFieldMarginVertical * 2)))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctField)
            m_intFieldX = rctField.X
            m_intFieldY = rctField.Y
            m_intFieldWidth = rctField.Width
            m_intFieldHeight = rctField.Height

            'draw center line
            e.Graphics.DrawLine(pen:=pnDrawing, x1:=CInt(m_intFieldWidth / 2) + m_intFieldX, y1:=m_intFieldY, x2:=CInt(m_intFieldWidth / 2) + m_intFieldX, y2:=m_intFieldHeight + m_intFieldY)

            'draw center point
            e.Graphics.DrawEllipse(pen:=pnDrawing, x:=CInt(m_intFieldWidth / 2) + m_intFieldX - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), y:=CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), width:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight), height:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight))
            e.Graphics.FillEllipse(Brushes.White, x:=CInt(m_intFieldWidth / 2) + m_intFieldX - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), y:=CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), width:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight), height:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight))

            'draw center circle
            dblCircleDiameter = CENTER_CIRCLE_DIAMETER / FIELD_HEIGHT * m_intFieldHeight
            rctCenterCircle = New Rectangle(x:=CInt(CInt(m_intFieldWidth / 2) + m_intFieldX - (dblCircleDiameter / 2)), y:=CInt(CInt(m_intFieldHeight / 2) + m_intFieldY - (dblCircleDiameter / 2)), Width:=CInt(dblCircleDiameter), Height:=CInt(dblCircleDiameter))
            e.Graphics.DrawEllipse(pnDrawing, rctCenterCircle)

            'draw penalty boxes
            dblPenaltyBoxHeight = PENALTY_BOX_HEIGHT / FIELD_HEIGHT * m_intFieldHeight
            dblPenaltyBoxWidth = PENALTY_BOX_WIDTH / FIELD_HEIGHT * m_intFieldHeight
            rctLeftPenaltyBox = New Rectangle(x:=m_intFieldX, y:=CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblPenaltyBoxWidth), Height:=CInt(dblPenaltyBoxHeight))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctLeftPenaltyBox)
            rctRightPenaltyBox = New Rectangle(x:=CInt(m_intFieldX + m_intFieldWidth - dblPenaltyBoxWidth), y:=CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblPenaltyBoxWidth), Height:=CInt(dblPenaltyBoxHeight))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctRightPenaltyBox)

            m_rctLeftPenaltyBox = rctLeftPenaltyBox
            m_rctRightPenaltyBox = rctRightPenaltyBox

            'draw penalty points
            dblPenaltyPointDistance = PENALTY_POINT_DISTANCE / FIELD_HEIGHT * m_intFieldHeight
            e.Graphics.DrawEllipse(pen:=pnDrawing, x:=CInt(dblPenaltyPointDistance) + m_intFieldX, y:=CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), width:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight), height:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight))
            e.Graphics.FillEllipse(Brushes.White, x:=CInt(dblPenaltyPointDistance) + m_intFieldX, y:=CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), width:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight), height:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight))
            e.Graphics.DrawEllipse(pen:=pnDrawing, x:=m_intFieldX + m_intFieldWidth - CInt(dblPenaltyPointDistance), y:=CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), width:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight), height:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight))
            e.Graphics.FillEllipse(Brushes.White, x:=m_intFieldX + m_intFieldWidth - CInt(dblPenaltyPointDistance), y:=CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), width:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight), height:=CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight))

            m_ptLeftPenaltyPoint = New Point(CInt(dblPenaltyPointDistance) + m_intFieldX + CInt(CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight) / 2), CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2) + CInt(CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight) / 2))
            m_ptRightPenaltyPoint = New Point(m_intFieldX + m_intFieldWidth - CInt(dblPenaltyPointDistance) + CInt(CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight) / 2), CInt(m_intFieldHeight / 2) + m_intFieldY - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2) + CInt(CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight) / 2))

            'draw penalty arcs
            dblPenaltyArcHeight = PENALTY_ARC_HEIGHT / FIELD_HEIGHT * m_intFieldHeight
            rctLeftPenaltyArc = New Rectangle(x:=CInt(dblPenaltyPointDistance) + m_intFieldX, y:=CInt((m_intFieldHeight / 2 - dblPenaltyArcHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblPenaltyArcHeight / 2), Height:=CInt(dblPenaltyArcHeight))
            e.Graphics.DrawArc(pen:=pnDrawing, rect:=rctLeftPenaltyArc, startAngle:=275, sweepAngle:=170)
            rctRightPenaltyArc = New Rectangle(x:=m_intFieldX + m_intFieldWidth - CInt(dblPenaltyPointDistance) - CInt(dblPenaltyArcHeight / 2) - CInt(POINT_WIDTH_HEIGHT / FIELD_HEIGHT * m_intFieldHeight / 2), y:=CInt((m_intFieldHeight / 2 - dblPenaltyArcHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblPenaltyArcHeight / 2), Height:=CInt(dblPenaltyArcHeight))
            e.Graphics.DrawArc(pen:=pnDrawing, rect:=rctRightPenaltyArc, startAngle:=95, sweepAngle:=170)

            'draw goal boxes
            dblGoalBoxHeight = GOAL_BOX_HEIGHT / FIELD_HEIGHT * m_intFieldHeight
            dblGoalBoxWidth = GOAL_BOX_WIDTH / FIELD_HEIGHT * m_intFieldHeight
            rctLeftGoalBox = New Rectangle(x:=m_intFieldX, y:=CInt((m_intFieldHeight / 2 - dblGoalBoxHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblGoalBoxWidth), Height:=CInt(dblGoalBoxHeight))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctLeftGoalBox)
            rctRightGoalBox = New Rectangle(x:=CInt(m_intFieldX + m_intFieldWidth - dblGoalBoxWidth), y:=CInt((m_intFieldHeight / 2 - dblGoalBoxHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblGoalBoxWidth), Height:=CInt(dblGoalBoxHeight))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctRightGoalBox)

            'draw goal posts
            dblGoalPostHeight = GOAL_POST_HEIGHT / FIELD_HEIGHT * m_intFieldHeight
            dblGoalPostWidth = GOAL_POST_WIDTH / FIELD_HEIGHT * m_intFieldHeight
            rctLeftGoalPost = New Rectangle(x:=CInt(m_intFieldX - dblGoalPostWidth), y:=CInt((m_intFieldHeight / 2 - dblGoalPostHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblGoalPostWidth), Height:=CInt(dblGoalPostHeight))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctLeftGoalPost)
            rctRightGoalPost = New Rectangle(x:=CInt(m_intFieldX + m_intFieldWidth), y:=CInt((m_intFieldHeight / 2 - dblGoalPostHeight / 2) + m_intFieldMarginVertical), Width:=CInt(dblGoalPostWidth), Height:=CInt(dblGoalPostHeight))
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctRightGoalPost)

            m_rctLeftGoalPost = rctLeftGoalPost
            m_rctRightGoalPost = rctRightGoalPost

            'display team names
            If m_blnDisplayTeamName = True Then
                Dim intHomeLength As Integer = CInt(IIf(m_strHomeTeamName.Length < 16, m_strHomeTeamName.Length, 16))
                Dim intAwayLength As Integer = CInt(IIf(m_strAwayTeamName.Length < 16, m_strAwayTeamName.Length, 16))

                If m_blnHomeTeamOnLeft = True Then
                    strLeftTeamName = CStr(IIf(m_strHomeTeamName = "", "", m_strHomeTeamName.Substring(0, intHomeLength) & " -->>"))
                    strRightTeamName = CStr(IIf(m_strAwayTeamName = "", "", "<<-- " & m_strAwayTeamName.Substring(0, intAwayLength)))
                Else
                    strLeftTeamName = CStr(IIf(m_strAwayTeamName = "", "", m_strAwayTeamName.Substring(0, intAwayLength) & " -->>"))
                    strRightTeamName = CStr(IIf(m_strHomeTeamName = "", "", "<<-- " & m_strHomeTeamName.Substring(0, intHomeLength)))
                End If

                Dim rectLeft As New Rectangle(rctField.X, rctField.Y + m_intFieldHeight - (CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) + m_intFieldMarginVertical)), CInt(rctField.Width / 2), CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) + m_intFieldMarginVertical))
                Dim rectRight As New Rectangle(CInt(m_intFieldWidth / 2) + m_intFieldX, rctField.Y + m_intFieldHeight - (CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) + m_intFieldMarginVertical)), CInt(rctField.Width / 2), CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) + m_intFieldMarginVertical))

                Dim stringFormat As New StringFormat()
                stringFormat.Alignment = StringAlignment.Center
                stringFormat.LineAlignment = StringAlignment.Far

                Dim br As New SolidBrush(Color.FromArgb(255, 191, 234, 210))
                e.Graphics.DrawString(strLeftTeamName, New Font("Arial Unicode MS", CSng(3 / FIELD_HEIGHT * m_intFieldHeight), FontStyle.Bold), br, rectLeft, stringFormat)
                e.Graphics.DrawString(strRightTeamName, New Font("Arial Unicode MS", CSng(3 / FIELD_HEIGHT * m_intFieldHeight), FontStyle.Bold), br, rectRight, stringFormat)
            End If

            'display touch instructions
            If m_blnDisplayTouchInstructions = True Then
                Dim brIns As New SolidBrush(Color.FromArgb(255, 255, 252, 185))
                Dim stringFormatIns As New StringFormat()
                stringFormatIns.Alignment = StringAlignment.Center
                stringFormatIns.LineAlignment = StringAlignment.Far
                Dim rectInstr1 As New Rectangle(rctField.X, CInt(rctField.Y + rctField.Height / 2 + rctCenterCircle.Height / 2), rctField.Width, CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2)))
                e.Graphics.DrawString("Enter Touch Type At Right" & vbLf & "Press Space for Next Touch", New Font("Arial Unicode MS", CSng(3 / FIELD_HEIGHT * m_intFieldHeight), FontStyle.Regular), brIns, rectInstr1, stringFormatIns)
            End If

            'display event text
            If m_strEventText.Trim <> "" Then
                Dim brEv As New SolidBrush(Color.FromArgb(255, 255, 252, 185))
                Dim intEvLength As Integer = CInt(IIf(m_strEventText.Length < 45, m_strEventText.Length, 45))
                Dim stringFormatEv As New StringFormat()
                stringFormatEv.Alignment = StringAlignment.Center
                stringFormatEv.LineAlignment = StringAlignment.Far
                Dim rectEv As New Rectangle(rctField.X, CInt(m_intFieldMarginVertical * 2), rctField.Width, CInt((m_intFieldHeight / 2 - dblPenaltyBoxHeight / 2) / 2))
                e.Graphics.DrawString(m_strEventText.Substring(0, intEvLength), New Font("Arial Unicode MS", CSng(3 / FIELD_HEIGHT * m_intFieldHeight), FontStyle.Regular), brEv, rectEv, stringFormatEv)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Defines the virtual 36 X 24 Grid for picking field area values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DefineFieldArea()
        Try
            Dim dblGridSquareHeight As Double
            Dim dblXValue As Double = 0
            Dim dblYValue As Double = 0
            Dim intXCount As Integer = 0
            Dim intYCount As Integer = 0

            Dim drFieldGrid As DataRow

            Dim strLeftPrefix As String
            Dim strRightPrefix As String

            m_dtFieldGrid = New DataTable

            If m_blnHomeTeamOnLeft = True Then
                strLeftPrefix = "H"
                strRightPrefix = "V"
            Else
                strLeftPrefix = "V"
                strRightPrefix = "H"
            End If

            'Create a datatable representing the 36 X 24 Grid - see sample below
            '---------------------------------------
            'X1maxX | X1maxY | X1value | X2maxX | X1 and X2 represent 1st and 2nd column in grid. maxX and maxY represent the bottom-right point
            '-------|--------|---------|--------|---
            '  12   |    8   |   HA1   | 12.175 | this row represents Y1 (1st row in grid)
            '-------|--------|---------|--------|----
            '  12   |  8.23  |   HB1   | 12.175 | this row represents Y2 (2nd row in grid)
            '-------|--------|---------|--------|----
            '       |        |         |        |

            'Add 36 X 3 columns
            For i As Integer = 1 To 36
                m_dtFieldGrid.Columns.Add("X" & i.ToString & "maxX")
                m_dtFieldGrid.Columns.Add("X" & i.ToString & "maxY")
                m_dtFieldGrid.Columns.Add("X" & i.ToString & "value")
            Next

            'determine the height/width of each square in grid
            dblGridSquareHeight = m_intFieldHeight / 24

            'fill grid
            dblYValue = m_intFieldMarginVertical + dblGridSquareHeight  'start from bottom-right corner of first cell
            intYCount = 1
            For intYCount = 1 To 24
                dblXValue = m_intFieldMarginHorizontal + dblGridSquareHeight    'start from bottom-right corner of first cell
                intXCount = 1
                drFieldGrid = m_dtFieldGrid.NewRow
                For intXCount = 1 To 36
                    drFieldGrid("X" & intXCount & "maxX") = dblXValue
                    drFieldGrid("X" & intXCount & "maxY") = dblYValue

                    If intXCount <= 18 Then
                        drFieldGrid("X" & intXCount & "value") = strLeftPrefix & Chr(64 + intYCount) & intXCount.ToString
                    Else
                        drFieldGrid("X" & intXCount & "value") = strRightPrefix & Chr(64 + 24 - (intYCount - 1)) & (36 - (intXCount - 1)).ToString
                    End If

                    dblXValue = dblXValue + dblGridSquareHeight
                Next
                m_dtFieldGrid.Rows.Add(drFieldGrid)

                dblYValue = dblYValue + dblGridSquareHeight
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Returns field area value corresponding to the X and Y coordinates.
    ''' </summary>
    ''' <param name="X">X-coordinate</param>
    ''' <param name="Y">Y-coordinate</param>
    ''' <returns>Field area as string.</returns>
    ''' <remarks></remarks>
    Private Function GetFieldArea(ByVal X As Integer, ByVal Y As Integer) As String
        Try
            For i As Integer = 0 To 23
                For j As Integer = 1 To 36
                    If X <= CType(m_dtFieldGrid.Rows(i).Item("X" & j & "maxX"), Double) And Y <= CType(m_dtFieldGrid.Rows(i).Item("X" & j & "maxY"), Double) Then
                        Return m_dtFieldGrid.Rows(i).Item("X" & j & "value").ToString
                    End If
                Next
            Next

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Place the mark on user control
    ''' </summary>
    ''' <param name="X">X-Coordinate</param>
    ''' <param name="Y">Y-Coordinate</param>
    ''' <remarks></remarks>
    Private Sub SetMark(ByVal X As Integer, ByVal Y As Integer, Optional ByVal Blink As Boolean = False)
        Try
            Dim pic As New PictureBox
            Dim ptXY As Point

            If m_intNextMark = 1 Then
                If Blink = True Then
                    pic.Image = Image.FromFile(m_objUtility.ReturnImage("GreenPinBlink.gif"))
                Else
                    pic.Image = Image.FromFile(m_objUtility.ReturnImage("GreenPinSmall.gif")) 'TOSOCRS-174
                End If

                pic.Name = "GreenMark"
                m_intMarkCount = m_intMarkCount + 1
                If m_intMaximumMarksAllowed > 1 Then
                    m_intNextMark = 2
                End If
            ElseIf m_intNextMark = 2 Then
                If Blink = True Then

                Else
                    pic.Image = Image.FromFile(m_objUtility.ReturnImage("OrangePinSmall.gif")) 'TOSOCRS-174
                End If
                pic.Name = "OrangeMark"
                m_intMarkCount = m_intMarkCount + 1
                If m_intMaximumMarksAllowed > 1 Then
                    m_intNextMark = 1
                End If
            Else
                Exit Try
            End If

            pic.SizeMode = PictureBoxSizeMode.AutoSize
            picSoccerField.Controls.Add(pic)
            AddHandler pic.Click, AddressOf Me.ClickMark

            ptXY.X = X - 3 'TOSOCRS-174
            ptXY.Y = Y - 3 'TOSOCRS-174
            pic.Location = ptXY
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Fires when any of the marks is clicked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ClickMark(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'delete the mark clicked by user
            Dim picdel As PictureBox
            picdel = CType(sender, PictureBox)
            If picdel.Name = "GreenMark" Then
                m_dtFieldMarks.Rows(0).Item("FieldArea") = DBNull.Value
                m_dtFieldMarks.Rows(0).Item("X") = DBNull.Value
                m_dtFieldMarks.Rows(0).Item("Y") = DBNull.Value
                m_intMarkCount = m_intMarkCount - 1
                m_intNextMark = 1
            Else
                m_dtFieldMarks.Rows(1).Item("FieldArea") = DBNull.Value
                m_dtFieldMarks.Rows(1).Item("X") = DBNull.Value
                m_dtFieldMarks.Rows(1).Item("Y") = DBNull.Value
                m_intMarkCount = m_intMarkCount - 1

                If m_intMaximumMarksAllowed > 1 Then
                    If m_dtFieldMarks.Rows(0).Item("FieldArea").ToString = "" Then
                        m_intNextMark = 1
                    Else
                        m_intNextMark = 2
                    End If
                Else
                    m_intNextMark = 2
                End If

            End If
            picdel.Dispose()


            Dim MyEventArgs As USFEventArgs

            'raise the user control's USFMarkRemoved event containing
            'the datatable with existing mark details if any
            MyEventArgs = New USFEventArgs(Nothing, Nothing, Nothing, m_dtFieldMarks)
            RaiseEvent USFMarkRemoved(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetBackground()
        Try
            If Me.DesignMode Then Exit Sub

            If m_blnDisplayArrow = False Then
                picSoccerField.BackgroundImage = Nothing
                Exit Try
            End If

            If m_blnHomeTeamOnLeft = True Then
                picSoccerField.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LeftToRight.gif")
            ElseIf m_blnHomeTeamOnLeft = False Then
                picSoccerField.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\RighttoLeft.gif")
            Else
                picSoccerField.BackgroundImage = Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Public properties "
    Public Property USFHomeTeamOnLeft() As Boolean
        Get
            Return m_blnHomeTeamOnLeft
        End Get
        Set(ByVal value As Boolean)
            m_blnHomeTeamOnLeft = value
            picSoccerField.Refresh()
            SetBackground()
        End Set
    End Property

    Public Property USFDisplayArrow() As Boolean
        Get
            Return m_blnDisplayArrow
        End Get
        Set(ByVal value As Boolean)
            m_blnDisplayArrow = value
            SetBackground()
        End Set
    End Property

    Public Property USFDisplayTeamName() As Boolean
        Get
            Return m_blnDisplayTeamName
        End Get
        Set(ByVal value As Boolean)
            m_blnDisplayTeamName = value
        End Set
    End Property

    Public Property USFDisplayTouchInstructions() As Boolean
        Get
            Return m_blnDisplayTouchInstructions
        End Get
        Set(ByVal value As Boolean)
            m_blnDisplayTouchInstructions = value
        End Set
    End Property

    Public Property USFEventText() As String
        Get
            Return m_strEventText
        End Get
        Set(ByVal value As String)
            m_strEventText = value
        End Set
    End Property

    Public Property USFHomeTeamName() As String
        Get
            Return m_strHomeTeamName
        End Get
        Set(ByVal value As String)
            m_strHomeTeamName = value
        End Set
    End Property

    Public Property USFAwayTeamName() As String
        Get
            Return m_strAwayTeamName
        End Get
        Set(ByVal value As String)
            m_strAwayTeamName = value
        End Set
    End Property

    Public Property USFNextMark() As Integer
        Get
            Return m_intNextMark
        End Get
        Set(ByVal value As Integer)
            m_intNextMark = value
        End Set
    End Property

    Public Property USFMaximumMarksAllowed() As Integer
        Get
            Return m_intMaximumMarksAllowed
        End Get
        Set(ByVal value As Integer)
            m_intMaximumMarksAllowed = value
        End Set
    End Property

    Public WriteOnly Property USFEnableOneClickRelocation() As Boolean
        Set(ByVal value As Boolean)
            m_blnEnableOneClickRelocation = value
        End Set
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USFConfineToLeftPenaltyBox As Boolean
        Get
            Return m_blnConfineToLeftPenaltyBox
        End Get
        Set(ByVal value As Boolean)
            m_blnConfineToLeftPenaltyBox = value
        End Set
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USFConfineToRightPenaltyBox As Boolean
        Get
            Return m_blnConfineToRightPenaltyBox
        End Get
        Set(ByVal value As Boolean)
            m_blnConfineToRightPenaltyBox = value
        End Set
    End Property
#End Region

#Region " Public Methods "

    ''' <summary>
    ''' Clears all the marks on soccer-field user control.
    ''' </summary>
    ''' <remarks>Call this method to clear all marks during Save or Cancel operation.</remarks>
    Public Sub USFClearMarks()
        Try
            For i As Integer = 0 To picSoccerField.Controls.Count - 1
                For Each cnt As Control In picSoccerField.Controls
                    If cnt.Name = "GreenMark" Or cnt.Name = "OrangeMark" Then
                        cnt.Dispose()
                        m_dtFieldMarks.Rows(0).Item("FieldArea") = DBNull.Value
                        m_dtFieldMarks.Rows(0).Item("X") = DBNull.Value
                        m_dtFieldMarks.Rows(0).Item("Y") = DBNull.Value
                        m_dtFieldMarks.Rows(1).Item("FieldArea") = DBNull.Value
                        m_dtFieldMarks.Rows(1).Item("X") = DBNull.Value
                        m_dtFieldMarks.Rows(1).Item("Y") = DBNull.Value
                    End If
                Next
            Next
            m_intMarkCount = 0
            m_intNextMark = 1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Places the mark on soccer field.
    ''' </summary>
    ''' <param name="X">X-Coordinate of the mark.</param>
    ''' <param name="Y">Y-Coordinate of the mark.</param>
    ''' <remarks>Call this method to place marks on soccer field during Edit operations.</remarks>
    Public Sub USFSetMark(ByVal X As Integer, ByVal Y As Integer, Optional ByVal Blink As Boolean = False)
        Try
            Dim intX As Integer
            Dim intY As Integer

            'calculate the location scaled to the user control size
            intX = CInt(X / FIELD_HEIGHT * m_intFieldHeight) + m_intFieldMarginHorizontal
            intY = CInt(Y / FIELD_HEIGHT * m_intFieldHeight) + m_intFieldMarginVertical

            'update mark details in the datatable
            If m_intNextMark = 1 Then
                m_dtFieldMarks.Rows(0).Item("FieldArea") = GetFieldArea(X, Y)
                m_dtFieldMarks.Rows(0).Item("X") = X
                m_dtFieldMarks.Rows(0).Item("Y") = Y
            ElseIf m_intNextMark = 2 Then
                m_dtFieldMarks.Rows(1).Item("FieldArea") = GetFieldArea(X, Y)
                m_dtFieldMarks.Rows(1).Item("X") = X
                m_dtFieldMarks.Rows(1).Item("Y") = Y
            End If

            'place the mark
            SetMark(intX, intY, Blink)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Places the mark at specific spot/region in field (TOPZ-933)
    ''' </summary>
    ''' <param name="MarkLocation">Specific spot/region in field like right post, sideline, penalty spot etc.</param>
    ''' <param name="X">X-Coordinate of the mark clicked by user</param>
    ''' <param name="Y">Y-Coordinate of the mark clicked by user</param>
    ''' <param name="Blink"></param>
    ''' <returns>The new X and Y corresponding to mark location</returns>
    ''' <remarks>Call this method to place mark on soccer field at specific spot/region based on event/action</remarks>
    Public Function USFSetMark(ByVal MarkLocation As enMarkLocation, ByVal X As Integer, ByVal Y As Integer, Optional ByVal Blink As Boolean = False) As Point
        Try
            Dim intX As Integer
            Dim intY As Integer
            Dim blnOnLine As Boolean = False
            Dim intNearestY As Integer = 0
            Dim intNearestX As Integer = 0
            Dim strNearestSideline As String = ""
            Dim strNearestGoaline As String = ""

            'calculate the location scaled to the user control size
            intX = CInt(X / FIELD_HEIGHT * m_intFieldHeight) + m_intFieldMarginHorizontal
            intY = CInt(Y / FIELD_HEIGHT * m_intFieldHeight) + m_intFieldMarginVertical

            Select Case MarkLocation
                Case enMarkLocation.Crossbar, enMarkLocation.LeftPost, enMarkLocation.RightPost
                    If X <= CInt(FIELD_HEIGHT * (3 / 2)) / 2 Then  'left half of the field
                        X = 0
                        intX = 0 + m_intFieldMarginHorizontal
                        If MarkLocation = enMarkLocation.Crossbar Then
                            If Not (intY > m_rctLeftGoalPost.Y And intY < m_rctLeftGoalPost.Y + m_rctLeftGoalPost.Height) Then
                                intY = m_rctLeftGoalPost.Y + CInt(m_rctLeftGoalPost.Height / 2)
                            End If
                        ElseIf MarkLocation = enMarkLocation.RightPost Then
                            intY = m_rctLeftGoalPost.Y
                        ElseIf MarkLocation = enMarkLocation.LeftPost Then
                            intY = m_rctLeftGoalPost.Y + m_rctLeftGoalPost.Height
                        End If
                    Else  'right half of the field
                        X = CInt(FIELD_HEIGHT * (3 / 2))
                        intX = m_intFieldWidth + m_intFieldMarginHorizontal
                        If MarkLocation = enMarkLocation.Crossbar Then
                            If Not (intY > m_rctRightGoalPost.Y And intY < m_rctRightGoalPost.Y + m_rctRightGoalPost.Height) Then
                                intY = m_rctRightGoalPost.Y + CInt(m_rctRightGoalPost.Height / 2)
                            End If
                        ElseIf MarkLocation = enMarkLocation.RightPost Then
                            intY = m_rctRightGoalPost.Y + m_rctRightGoalPost.Height
                        ElseIf MarkLocation = enMarkLocation.LeftPost Then
                            intY = m_rctRightGoalPost.Y
                        End If
                    End If

                    Y = CInt(((intY - m_intFieldMarginVertical) / m_intFieldHeight) * FIELD_HEIGHT)
                Case enMarkLocation.PenaltyMarkLeftHalf
                    intX = m_ptLeftPenaltyPoint.X
                    intY = m_ptLeftPenaltyPoint.Y
                    X = CInt(((intX - m_intFieldMarginHorizontal) / m_intFieldHeight) * FIELD_HEIGHT)
                    Y = CInt(((intY - m_intFieldMarginVertical) / m_intFieldHeight) * FIELD_HEIGHT)
                Case enMarkLocation.PenaltyMarkRightHalf
                    intX = m_ptRightPenaltyPoint.X
                    intY = m_ptRightPenaltyPoint.Y
                    X = CInt(((intX - m_intFieldMarginHorizontal) / m_intFieldHeight) * FIELD_HEIGHT)
                    Y = CInt(((intY - m_intFieldMarginVertical) / m_intFieldHeight) * FIELD_HEIGHT)
                Case enMarkLocation.Sideline
                    blnOnLine = False
                    'if the user clicked point is on top sideline
                    If ((m_intFieldX < intX And intX < (m_intFieldX + m_intFieldWidth)) And m_intFieldY = intY) Or _
                                   ((m_intFieldX > intX And intX > (m_intFieldX + m_intFieldWidth)) And m_intFieldY = intY) Then
                        blnOnLine = True
                    End If
                    'if the user clicked point is on bottom sideline
                    If ((m_intFieldX < intX And intX < (m_intFieldX + m_intFieldWidth)) And (m_intFieldY + m_intFieldHeight) = intY) Or _
                                   ((m_intFieldX > intX And intX > (m_intFieldX + m_intFieldWidth)) And (m_intFieldY + m_intFieldHeight) = intY) Then
                        blnOnLine = True
                    End If

                    'if the point is not on any sidelines
                    'choose the nearest sideline
                    If blnOnLine = False Then
                        If intY < m_intFieldHeight / 2 Then
                            intY = m_intFieldY
                        Else
                            intY = m_intFieldY + m_intFieldHeight
                        End If
                        Y = CInt(((intY - m_intFieldMarginVertical) / m_intFieldHeight) * FIELD_HEIGHT)
                    End If
                Case enMarkLocation.SidelineAndGoalline
                    blnOnLine = False
                    'if the user clicked point is on top sideline
                    If ((m_intFieldX < intX And intX < (m_intFieldX + m_intFieldWidth)) And m_intFieldY = intY) Or _
                                   ((m_intFieldX > intX And intX > (m_intFieldX + m_intFieldWidth)) And m_intFieldY = intY) Then
                        blnOnLine = True
                    End If
                    'if the user clicked point is on bottom sideline
                    If ((m_intFieldX < intX And intX < (m_intFieldX + m_intFieldWidth)) And (m_intFieldY + m_intFieldHeight) = intY) Or _
                                   ((m_intFieldX > intX And intX > (m_intFieldX + m_intFieldWidth)) And (m_intFieldY + m_intFieldHeight) = intY) Then
                        blnOnLine = True
                    End If
                    'if the user clicked point is on left goalline
                    If (m_intFieldX = intX And (m_intFieldY < intY And intY < (m_intFieldY + m_intFieldHeight))) Or _
                        (m_intFieldX = intX And (m_intFieldY > intY And intY > (m_intFieldY + m_intFieldHeight))) Then
                        blnOnLine = True
                    End If
                    'if the user clicked point is on right goalline
                    If ((m_intFieldX + m_intFieldWidth) = intX And (m_intFieldY < intY And intY < (m_intFieldY + m_intFieldHeight))) Or _
                        ((m_intFieldX + m_intFieldWidth) = intX And (m_intFieldY > intY And intY > (m_intFieldY + m_intFieldHeight))) Then
                        blnOnLine = True
                    End If

                    'if the point is not on any sidelines or goallines
                    'choose the nearest sideline or goalline
                    If blnOnLine = False Then
                        intNearestY = intY - m_intFieldY
                        strNearestSideline = "topSideline"
                        If intNearestY > ((m_intFieldY + m_intFieldHeight) - intY) Then
                            intNearestY = ((m_intFieldY + m_intFieldHeight) - intY)
                            strNearestSideline = "bottomSideline"
                        End If
                        intNearestX = intX - m_intFieldX
                        strNearestGoaline = "leftGoalline"
                        If intNearestX > ((m_intFieldX + m_intFieldWidth) - intX) Then
                            intNearestX = ((m_intFieldX + m_intFieldWidth) - intX)
                            strNearestGoaline = "rightGoalline"
                        End If
                        'if point is closer to goalline
                        If intNearestX < intNearestY Then
                            If strNearestGoaline = "leftGoalline" Then
                                intX = m_intFieldX
                            Else
                                intX = m_intFieldX + m_intFieldWidth
                            End If
                            X = CInt(((intX - m_intFieldMarginHorizontal) / m_intFieldHeight) * FIELD_HEIGHT)
                        Else
                            If strNearestSideline = "topSideline" Then
                                intY = m_intFieldY
                            Else
                                intY = m_intFieldY + m_intFieldHeight
                            End If
                            Y = CInt(((intY - m_intFieldMarginVertical) / m_intFieldHeight) * FIELD_HEIGHT)
                        End If
                    End If

            End Select

            'update mark details in the datatable
            If m_intNextMark = 1 Then
                m_dtFieldMarks.Rows(0).Item("FieldArea") = GetFieldArea(X, Y)
                m_dtFieldMarks.Rows(0).Item("X") = X
                m_dtFieldMarks.Rows(0).Item("Y") = Y
            ElseIf m_intNextMark = 2 Then
                m_dtFieldMarks.Rows(1).Item("FieldArea") = GetFieldArea(X, Y)
                m_dtFieldMarks.Rows(1).Item("X") = X
                m_dtFieldMarks.Rows(1).Item("Y") = Y
            End If

            'place the mark
            SetMark(intX, intY, Blink)

            Return New Point(X, Y)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class

Public Class USFEventArgs
    Inherits System.EventArgs

#Region " Constants & Variables "
    Private m_FieldArea As String
    Private m_intX As Integer
    Private m_intY As Integer
    Private m_dtMarks As DataTable
#End Region

#Region " Public properties "
    Public ReadOnly Property USFFieldArea() As String
        Get
            Return m_FieldArea
        End Get
    End Property

    Public ReadOnly Property USFx() As Integer
        Get
            Return m_intX
        End Get
    End Property

    Public ReadOnly Property USFy() As Integer
        Get
            Return m_intY
        End Get
    End Property

    Public ReadOnly Property USFMarks() As DataTable
        Get
            Return m_dtMarks
        End Get
    End Property
#End Region

#Region " Public Methods "
    Public Sub New(ByVal FieldArea As String, ByVal X As Integer, ByVal Y As Integer, ByVal FieldMarks As DataTable)
        Try
            m_FieldArea = FieldArea
            m_intX = X
            m_intY = Y
            m_dtMarks = FieldMarks
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class
