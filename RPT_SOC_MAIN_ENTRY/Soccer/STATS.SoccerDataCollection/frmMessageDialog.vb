﻿#Region " Comments "
'------------------------------------------------------------------------------------------------------------------------------------------------------
' Form name     :   frmMessageDialog
' Author        :   Fiaz Ahmed
' Created on    :   30th July, 2008
' Description   :   This form displays generic and exception related messages.
'                   Usage example:
'
'                   Private MessageDialog As New frmMessageDialog
'
'                   (a) For generic messages,
'                       Dim Result As DialogResult
'                       Result = MessageDialog.Show("You did not enter a server name. Cancel this operation?", _
'                                "Football Data Collection", MessageDialogButtons.YesNo, MessageDialogIcon.Question)
'                       If Result = Windows.Forms.DialogResult.Yes Then
'                           ...................
'                           ...................
'
'                   (b) For exception based messages,
'                       Try
'                           ...................
'                           ...................
'                       Catch ex As Exception
'                           MessageDialog.Show(ex, "Football Data Collection")
'                       End Try
'------------------------------------------------------------------------------------------------------------------------------------------------------
' MODIFICATION LOG
'------------------------------------------------------------------------------------------------------------------------------------------------------
' ID           |Modified by            |Modified on     |Description
'--------------|-----------------------|----------------|----------------------------------------------------------------------------------------------
' MD0001       |Fiaz                   |22nd Jan, 2009  |Modified design and code to include Info text box that will be used to display
'              |                       |                |game details like covergae level, teams, reporter role, user name etc. Changes were
'              |                       |                |also made to clsUtility.cs and all forms calling message dialog as part of this
'              |                       |                |requirement.
'              |                       |                |
' MD0002       |Fiaz                   |19th Jan, 2010  |Modified load code to expand the dialog to show details with error radiobutton auto-selected.
'------------------------------------------------------------------------------------------------------------------------------------------------------
#End Region

#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region " Imports "
Imports System.Windows.Forms
Imports STATS.Utility

#End Region

''' <summary>
''' Displays a generic or exception based message dialog that can contain text, buttons and symbols that inform and instruct the user.
''' </summary>
''' <remarks>To display a message dialog, call the method Show. The title, message, buttons, and icons displayed in the dialog are determined by parameters that you pass to this method.</remarks>
Public Class frmMessageDialog

#Region " Constants & Variables "
    Private strDialogType As String
    Private strDialogText As String
    Private strDialogCaption As String
    Private arrDialogButtons() As String
    Private imgDialogIcon As System.Drawing.Image = Nothing
    Private intReturnValue As DialogResult
    Private strErrorText As String
    Private strStackText As String
    Private strTargetText As String
    Private strInfoText As String   '(Modification ID: MD0001)
    Private m_blnHideDetailsOnLoad As Boolean = False   '(Modification ID: MD0002)
    Private m_objUtilAudit As New clsAuditLog
    Private Const FORM_WIDTH As Integer = 470
    Private Const FORM_HEIGHT As Integer = 154
    Private Const FORM_HEIGHT_EXCLUDING_MESSAGE As Integer = 139        'the factor by which form has to shrink or expand based on message length
    'Private Const ERR_FORM_HEIGHT_EXCLUDING_MESSAGE As Integer = 256    'same as above - for detailed error dialog (Modification ID: MD0001)
    Private Const ERR_FORM_HEIGHT_EXCLUDING_MESSAGE As Integer = 281   'same as above - for detailed error dialog
    'Private Const ERR_PANEL_TOP_FROM_FORM_BOTTOM As Integer = 188       'panel top - based on form height (Modification ID: MD0001)
    Private Const ERR_PANEL_TOP_FROM_FORM_BOTTOM As Integer = 213       'panel top - based on form height
    Private Const BUTTON_WIDTH As Integer = 75
    Private Const ERROR_BUTTON_WIDTH As Integer = 95
    Private Const DISTANCE_BETWEEN_BUTTONS As Integer = 6
    Private Const DISTANCE_FROM_RIGHT_EDGE As Integer = 20
    Private Const ICON_X As Integer = 12
    Private Const MESSAGE_LABEL_X As Integer = 63
    Private Const DISTANCE_BETWEEN_ICON_AND_MESSAGE As Integer = 14
    Private Const DISTANCE_AT_TWO_ENDS As Integer = 12

    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private lsupport As New languagesupport
#End Region

#Region " Shared methods "

    ''' <summary>
    ''' Displays an exception message dialog with the specified caption and exception details.
    ''' </summary>
    ''' <param name="Ex">The exception for which to display the details in the message dialog.</param>
    ''' <param name="Caption">The text to display in the title bar of the exception message dialog.</param>
    ''' <returns>One of the DialogResult values.</returns>
    ''' <remarks>This method has to be invoked in a catch block.</remarks>
    Public Shadows Function Show(ByVal Ex As Exception, ByVal Caption As String) As DialogResult
        'Public Shadows Function Show(ByVal Ex As Exception, ByVal Caption As String) As DialogResult   (Modification ID: MD0001)
        strDialogType = "Error"

        If TypeOf Ex Is SqlClient.SqlException Then
            If Ex.Message.Contains("Timeout") Then
                strDialogText = "SERVER TIME OUT OCCURED. PLEASE TRY THE OPERATION AGAIN"
            Else
                strDialogText = "An error in Sql Server occured. Could not proceed further, Please contact your operations desk."
            End If
        ElseIf TypeOf Ex Is System.Net.WebException Then
            strDialogText = "An error in network occured. Could not proceed further, Please contact your operations desk."
        Else
            strDialogText = "An Error occured,could not Proceed further. Please contact your operations desk."
        End If

        strErrorText = Ex.Message
        strStackText = Ex.StackTrace
        strTargetText = Ex.TargetSite.ToString
        'Praveen modified this code to add date time with info text --09-07-09

        strInfoText = m_objGameDetails.CoverageLevel & m_objGameDetails.AwayTeamAbbrev & m_objGameDetails.ReporterRole & m_objGameDetails.HomeTeamAbbrev & m_objLoginDetails.UserLoginName & Date.Now.Date & "v" & Application.ProductVersion.ToString & "*" & Date.Now.Month

        strDialogCaption = Caption
        imgDialogIcon = System.Drawing.SystemIcons.Error.ToBitmap
        ReDim arrDialogButtons(1)
        arrDialogButtons(0) = "OK"
        arrDialogButtons(1) = "Show Details >>"

        MyBase.ShowDialog()
        Return intReturnValue
        Me.Dispose()
    End Function

    ''' <summary>
    ''' Displays a message dialog with the specified text, caption, buttons and icon.
    ''' </summary>
    ''' <param name="Text">The text to display in the message dialog.</param>
    ''' <param name="Caption">The text to display in the title bar of the message dialog.</param>
    ''' <param name="Buttons">One of the MessageDialogButtons values that specifies which buttons to display in the message dialog.</param>
    ''' <param name="Icon">One of the MessageDialogIcon values that specifies which icon to display in the message dialog.</param>
    ''' <returns>One of the DialogResult values.</returns>
    ''' <remarks>You can have a maximum of three buttons on the message dialog.</remarks>
    Public Shadows Function Show(ByVal Text As String, ByVal Caption As String, ByVal Buttons As MessageDialogButtons, ByVal Icon As MessageDialogIcon) As DialogResult
        strDialogType = "Non-Error"
        strDialogText = Text
        strDialogCaption = Caption

        Select Case Buttons
            Case MessageDialogButtons.AbortRetryIgnore
                ReDim arrDialogButtons(2)
                arrDialogButtons(0) = "Abort"
                arrDialogButtons(1) = "Retry"
                arrDialogButtons(2) = "Ignore"
            Case MessageDialogButtons.OK
                ReDim arrDialogButtons(0)
                arrDialogButtons(0) = "OK"
            Case MessageDialogButtons.OKCancel
                ReDim arrDialogButtons(1)
                arrDialogButtons(0) = "OK"
                arrDialogButtons(1) = "Cancel"
            Case MessageDialogButtons.RetryCancel
                ReDim arrDialogButtons(1)
                arrDialogButtons(0) = "Retry"
                arrDialogButtons(1) = "Cancel"
            Case MessageDialogButtons.YesNo
                ReDim arrDialogButtons(1)
                arrDialogButtons(0) = "Yes"
                arrDialogButtons(1) = "No"
            Case MessageDialogButtons.YesNoCancel
                ReDim arrDialogButtons(2)
                arrDialogButtons(0) = "Yes"
                arrDialogButtons(1) = "No"
                arrDialogButtons(2) = "Cancel"
        End Select

        Select Case Icon
            Case MessageDialogIcon.Asterisk
                imgDialogIcon = System.Drawing.SystemIcons.Asterisk.ToBitmap
            Case MessageDialogIcon.Error
                imgDialogIcon = System.Drawing.SystemIcons.Error.ToBitmap
            Case MessageDialogIcon.Exclamation
                imgDialogIcon = System.Drawing.SystemIcons.Exclamation.ToBitmap
            Case MessageDialogIcon.Hand
                imgDialogIcon = System.Drawing.SystemIcons.Hand.ToBitmap
            Case MessageDialogIcon.Information
                imgDialogIcon = System.Drawing.SystemIcons.Information.ToBitmap
            Case MessageDialogIcon.None
                imgDialogIcon = Nothing
            Case MessageDialogIcon.Question
                imgDialogIcon = System.Drawing.SystemIcons.Question.ToBitmap
            Case MessageDialogIcon.Warning
                imgDialogIcon = System.Drawing.SystemIcons.Warning.ToBitmap
        End Select

        MyBase.ShowDialog()
        Return intReturnValue
        Me.Dispose()
    End Function
#End Region

    ''' <summary>
    ''' Displays a message dialog with the specified text, caption, buttons and icon.
    ''' </summary>
    ''' <param name="Text">The text to display in the message dialog.</param>
    ''' <param name="Caption">The text to display in the title bar of the message dialog.</param>
    ''' <param name="Buttons">One of the MessageDialogButtons values that specifies which buttons to display in the message dialog.</param>
    ''' <param name="Icon">One of the MessageDialogIcon values that specifies which icon to display in the message dialog.</param>
    ''' <returns>One of the DialogResult values.</returns>
    ''' <remarks>You can have a maximum of three buttons on the message dialog.</remarks>
    Public Shadows Function Show(ByVal languageId As Integer, ByVal Text As String, ByVal Caption As String, ByVal Buttons As MessageDialogButtons, ByVal Icon As MessageDialogIcon) As DialogResult
        strDialogType = "Non-Error"

        If (languageId <> 1) Then  'Multilingual for messages
            strDialogText = lsupport.MultilingualDictionary(Text)
            strDialogCaption = lsupport.MultilingualDictionary(Caption)
        Else
            strDialogText = Text
            strDialogCaption = Caption
        End If

        Select Case Buttons
            Case MessageDialogButtons.AbortRetryIgnore
                ReDim arrDialogButtons(2)
                arrDialogButtons(0) = "Abort"
                arrDialogButtons(1) = "Retry"
                arrDialogButtons(2) = "Ignore"
            Case MessageDialogButtons.OK
                ReDim arrDialogButtons(0)
                arrDialogButtons(0) = "OK"
            Case MessageDialogButtons.OKCancel
                ReDim arrDialogButtons(1)
                arrDialogButtons(0) = "OK"
                arrDialogButtons(1) = "Cancel"
            Case MessageDialogButtons.RetryCancel
                ReDim arrDialogButtons(1)
                arrDialogButtons(0) = "Retry"
                arrDialogButtons(1) = "Cancel"
            Case MessageDialogButtons.YesNo
                ReDim arrDialogButtons(1)
                arrDialogButtons(0) = "Yes"
                arrDialogButtons(1) = "No"
            Case MessageDialogButtons.YesNoCancel
                ReDim arrDialogButtons(2)
                arrDialogButtons(0) = "Yes"
                arrDialogButtons(1) = "No"
                arrDialogButtons(2) = "Cancel"
        End Select

        Select Case Icon
            Case MessageDialogIcon.Asterisk
                imgDialogIcon = System.Drawing.SystemIcons.Asterisk.ToBitmap
            Case MessageDialogIcon.Error
                imgDialogIcon = System.Drawing.SystemIcons.Error.ToBitmap
            Case MessageDialogIcon.Exclamation
                imgDialogIcon = System.Drawing.SystemIcons.Exclamation.ToBitmap
            Case MessageDialogIcon.Hand
                imgDialogIcon = System.Drawing.SystemIcons.Hand.ToBitmap
            Case MessageDialogIcon.Information
                imgDialogIcon = System.Drawing.SystemIcons.Information.ToBitmap
            Case MessageDialogIcon.None
                imgDialogIcon = Nothing
            Case MessageDialogIcon.Question
                imgDialogIcon = System.Drawing.SystemIcons.Question.ToBitmap
            Case MessageDialogIcon.Warning
                imgDialogIcon = System.Drawing.SystemIcons.Warning.ToBitmap
        End Select

        MyBase.ShowDialog()
        Return intReturnValue
        Me.Dispose()
    End Function

#Region " Private methods "

    ''' <summary>
    ''' Sets the button text, spacing and position based on the dialog type and buttons opted
    ''' </summary>
    ''' <param name="Buttons">Array of buttons to be displayed in the dialog</param>
    ''' <remarks></remarks>
    Private Sub SetButtons(ByVal Buttons() As String)
        Try
            Dim intButtonCount As Integer = Buttons.Length
            Dim intGapsBetweenButtons As Integer

            intGapsBetweenButtons = intButtonCount - 1

            Button1.Visible = True
            Button2.Visible = True
            Button3.Visible = True
            Button1.Width = BUTTON_WIDTH

            Select Case strDialogType
                Case "Non-Error"
                    Button2.Width = BUTTON_WIDTH
                Case "Error"
                    Button2.Width = ERROR_BUTTON_WIDTH
            End Select

            Button3.Width = BUTTON_WIDTH
            Button1.Anchor = AnchorStyles.Bottom
            Button2.Anchor = AnchorStyles.Bottom
            Button3.Anchor = AnchorStyles.Bottom

            Select Case intButtonCount
                Case 3
                    Button1.Text = Buttons(0)
                    Button2.Text = Buttons(1)
                    Button3.Text = Buttons(2)
                    Button1.Left = CType(((Me.ClientRectangle.Width - ((BUTTON_WIDTH * intButtonCount) + (DISTANCE_BETWEEN_BUTTONS * intGapsBetweenButtons))) / 2), Integer)
                    Button2.Left = Button1.Left + BUTTON_WIDTH + DISTANCE_BETWEEN_BUTTONS
                    Button3.Left = Button2.Left + BUTTON_WIDTH + DISTANCE_BETWEEN_BUTTONS
                Case 2
                    Button3.Visible = False
                    Button1.Text = Buttons(0)
                    Button2.Text = Buttons(1)

                    Select Case strDialogType
                        Case "Non-Error"
                            Button1.Left = CType(((Me.ClientRectangle.Width - ((BUTTON_WIDTH * intButtonCount) + (DISTANCE_BETWEEN_BUTTONS * intGapsBetweenButtons))) / 2), Integer)
                            Button2.Left = Button1.Left + BUTTON_WIDTH + DISTANCE_BETWEEN_BUTTONS
                        Case "Error"
                            Button1.Left = Me.ClientRectangle.Width - (BUTTON_WIDTH + ERROR_BUTTON_WIDTH + DISTANCE_BETWEEN_BUTTONS + DISTANCE_FROM_RIGHT_EDGE)
                            Button2.Left = Button1.Left + BUTTON_WIDTH + DISTANCE_BETWEEN_BUTTONS
                    End Select

                Case 1
                    Button2.Visible = False
                    Button3.Visible = False
                    Button1.Text = Buttons(0)
                    Button1.Left = CType(((Me.ClientRectangle.Width - ((BUTTON_WIDTH * intButtonCount) + (DISTANCE_BETWEEN_BUTTONS * intGapsBetweenButtons))) / 2), Integer)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Sets the return value for dialog based on the button clicked by user
    ''' </summary>
    ''' <param name="sender">The button clicked by user</param>
    ''' <remarks></remarks>
    Private Sub SetReturnValue(ByVal sender As System.Object)
        Try
            Dim btn As Button = CType(sender, Button)
            Select Case btn.Text
                Case "Abort"
                    intReturnValue = Windows.Forms.DialogResult.Abort
                Case "Retry"
                    intReturnValue = Windows.Forms.DialogResult.Retry
                Case "Ignore"
                    intReturnValue = Windows.Forms.DialogResult.Ignore
                Case "OK"
                    intReturnValue = Windows.Forms.DialogResult.OK
                Case "Cancel"
                    intReturnValue = Windows.Forms.DialogResult.Cancel
                Case "Yes"
                    intReturnValue = Windows.Forms.DialogResult.Yes
                Case "No"
                    intReturnValue = Windows.Forms.DialogResult.No
                Case Else
                    intReturnValue = Windows.Forms.DialogResult.None
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Event handlers "

    Private Sub frmMessageDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Width = FORM_WIDTH
            Me.Height = FORM_HEIGHT
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmmessagedialog", Nothing, 1, 0)

            pnlErrorDetails.Visible = False

            Select Case strDialogType
                Case "Non-Error"
                    Me.Icon = Nothing
                Case "Error"
                    Me.Icon = frmMain.Icon
            End Select

            Me.Text = strDialogCaption
            picIcon.BackgroundImage = imgDialogIcon
            picIcon.BackgroundImageLayout = ImageLayout.Center

            picIcon.Left = ICON_X
            If imgDialogIcon Is Nothing Then
                picIcon.Visible = False
                lblMessage.Left = ICON_X
                lblMessage.MaximumSize = New System.Drawing.Size(Me.ClientRectangle.Width - (DISTANCE_AT_TWO_ENDS * 2), 0)
            Else
                picIcon.Visible = True
                lblMessage.Left = MESSAGE_LABEL_X
                lblMessage.MaximumSize = New System.Drawing.Size(Me.ClientRectangle.Width - (picIcon.Width + DISTANCE_BETWEEN_ICON_AND_MESSAGE + (DISTANCE_AT_TWO_ENDS * 2)), 0)
            End If

            lblMessage.Text = strDialogText
            If strDialogType = "Error" Then
                txtError.Text = strErrorText
                txtStack.Text = strStackText
                txtTarget.Text = strTargetText
                txtInfo.Text = strInfoText      '(Modification ID: MD0001)
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Error, txtError.Text, Nothing, 1, 0)
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Stack, txtStack.Text, Nothing, 1, 0)
            End If

            picIcon.Top = CInt(lblMessage.Top + (lblMessage.Height / 2)) - CInt(picIcon.Height / 2)
            Me.Height = lblMessage.Height + FORM_HEIGHT_EXCLUDING_MESSAGE

            SetButtons(arrDialogButtons)

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, strDialogText, Nothing, 1, 0)

            '(Modification ID: MD0002) --- begin
            If m_blnHideDetailsOnLoad = False Then
                If strDialogType = "Error" Then
                    If Button2.Text = "Show Details >>" Then
                        DialogButtonClicked(Button2, e)
                        radError.Checked = True
                    End If
                End If
            End If
            '(Modification ID: MD0002) --- end
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DialogButtonClicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click, Button3.Click
        Try
            Dim btn As Button = CType(sender, Button)
            If btn.Name = "Button2" Then
                Select Case strDialogType
                    Case "Non-Error"
                        SetReturnValue(sender)
                        Me.Focus()
                        Me.Close()
                    Case "Error"
                        If Button2.Text = "Show Details >>" Then
                            pnlErrorDetails.Visible = True
                            radError.Checked = False
                            radStack.Checked = False
                            radTarget.Checked = False
                            radInfo.Checked = False     '(Modification ID: MD0001)
                            Me.Height = lblMessage.Height + ERR_FORM_HEIGHT_EXCLUDING_MESSAGE
                            pnlErrorDetails.Top = Me.Height - ERR_PANEL_TOP_FROM_FORM_BOTTOM
                            Button2.Text = "Hide Details <<"
                        Else
                            pnlErrorDetails.Visible = False
                            lblMessage.Text = strDialogText
                            Me.Height = lblMessage.Height + FORM_HEIGHT_EXCLUDING_MESSAGE
                            Button2.Text = "Show Details >>"
                        End If
                End Select
            Else
                SetReturnValue(sender)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Private Sub ErrorDetailSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radError.CheckedChanged, radStack.CheckedChanged, radTarget.CheckedChanged (Modification ID: MD0001)
    Private Sub ErrorDetailSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radError.CheckedChanged, radStack.CheckedChanged, radTarget.CheckedChanged, radInfo.CheckedChanged
        Try
            Dim strDisplayText As String = ""

            If radError.Checked = True Then
                strDisplayText = strErrorText
            ElseIf radStack.Checked = True Then
                strDisplayText = strStackText
            ElseIf radTarget.Checked = True Then
                strDisplayText = strTargetText
            ElseIf radInfo.Checked = True Then      '(Modification ID: MD0001)
                strDisplayText = strInfoText
            End If

            lblMessage.Text = strDialogText & Chr(13) & Chr(13) & "[" & strDisplayText & "]"
            Me.Height = lblMessage.Height + ERR_FORM_HEIGHT_EXCLUDING_MESSAGE
            pnlErrorDetails.Top = Me.Height - ERR_PANEL_TOP_FROM_FORM_BOTTOM
            Me.StartPosition = FormStartPosition.CenterParent
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

#Region " Public properties "
    '(Modification ID: MD0002) --- begin
    Public Property HideDetailsOnLoad() As Boolean
        Get
            Return m_blnHideDetailsOnLoad
        End Get
        Set(ByVal value As Boolean)
            m_blnHideDetailsOnLoad = value
        End Set
    End Property
    '(Modification ID: MD0002) --- end
#End Region

End Class