﻿#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text
Imports System.IO

#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmTeamStats
' Author        : Wilson
' Created Date  : 12-Dec-09
' Description   : DIPLAYS THE TEAM PBP 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------

#End Region
Public Class frmPBPReport
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Dim m_strBuilder As StringBuilder
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_dsGameSetup As New DataSet
    Dim m_objclsTouches As STATS.SoccerBL.clsTouches = clsTouches.GetInstance()
    Private m_objTeamStats As clsTeamStats = clsTeamStats.GetInstance()
    Private m_objModule1PBP As clsModule1PBP = clsModule1PBP.GetInstance()
    Private m_Objrep As clsGeneral = clsGeneral.GetInstance()
    Private _objGeneral As clsGeneral = clsGeneral.GetInstance()
    Private _objClsGetEventData As ClsGetEventData = ClsGetEventData.GetInstance()
    Private m_NextElapsedTime As Integer
    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200
    Dim tempinit, tempinit1, tempinit2 As String
    Dim tempname, tempname1, tempname2 As String
    Dim tempindex, tempindex1, tempindex2 As Integer
    Dim tempnamecomm, tempnamecomm1, tempnamecomm2 As Integer
    Dim CurrPeriod As Integer
    Dim hgola As Integer = 0
    Dim agola As Integer = 0
    Dim dsPBP As New DataSet
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Dim ElapsedTime As Integer
    Private lsupport As New languagesupport
    Dim pencheck As String

    Dim strPlaybyplay As String = "Play by Play"
    Dim strFirstHalf As String = "First Half"
    Dim strSecondHalf As String = "Second Half"
    Dim str1stExtraTime As String = "1st Extra Time"
    Dim str2ndExtraTime As String = "2nd Extra Time"
    Dim strPenaltyShootouts As String = "Penalty Shootouts"
    Dim strend As String = "END OF"



    Private Sub frmPBPReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPBPReport", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)

            strPlaybyplay = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strPlaybyplay)
            strFirstHalf = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strFirstHalf)
            strSecondHalf = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strSecondHalf)
            str1stExtraTime = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), str1stExtraTime)
            str2ndExtraTime = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), str2ndExtraTime)
            strPenaltyShootouts = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strPenaltyShootouts)
            strend = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strend)
            If (m_objGameDetails.CoverageLevel = 6) Then
                BuildPlayByPlayPZ()
            Else
                BuildPlayByPlay()
            End If

            wbReport.Focus()
            ' BuildReports()
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnPrint.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPrint.Text)
                btnRefresh.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnRefresh.Text)

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim path As String
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 0)
            If wbReport.DocumentText = Nothing Then
                'MessageDialog.Show("No Document is Present", Me.Text)
                Exit Sub
            Else
                path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                SaveFileDialog1.InitialDirectory = path
                SaveFileDialog1.Filter = "HTML Files|*.htm"
                If SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fileName As String = SaveFileDialog1.FileName
                    Dim sw As StreamWriter
                    If File.Exists(fileName) = False Then
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    Else
                        File.Delete(fileName)
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            PrintDialog1.Document = PrintDocument1
            PrintDialog1.AllowSomePages = True
            PrintDocument1.DefaultPageSettings.Landscape = True
            If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim pritcopy As Integer = DirectCast(DirectCast(PrintDialog1, System.Windows.Forms.PrintDialog).PrinterSettings, System.Drawing.Printing.PrinterSettings).Copies
                For i As Integer = 0 To pritcopy - 1
                    wbReport.Print()
                Next
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            If (m_objGameDetails.CoverageLevel = 6) Then
                BuildPlayByPlayPZ()
            Else
                BuildPlayByPlay()
            End If
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh.Text, 1, 0)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3 Then
                    If CurrPeriod = 1 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                    ElseIf CurrPeriod = 2 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                    ElseIf CurrPeriod = 3 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                    ElseIf CurrPeriod = 4 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                    End If
                Else
                    If CurrPeriod = 1 Then
                        Return CStr(CInt(ElapsedTime / 60))
                    ElseIf CurrPeriod = 2 Then
                        Return CStr(CInt((FIRSTHALF + ElapsedTime) / 60))
                    ElseIf CurrPeriod = 3 Then
                        Return CStr(CInt((SECONDHALF + ElapsedTime) / 60))
                        'clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                    ElseIf CurrPeriod = 4 Then
                        Return CStr(CInt((FIRSTEXTRA + ElapsedTime) / 60))
                        'clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                    End If

                End If

            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub goalcall(ByVal tperiod As Integer)
        Dim exp1 As String = " team_id='" & m_objGameDetails.HomeTeamID & "' and  period <='" & tperiod & "'  And (event_code_id = '17' Or event_code_id = '28' or event_code_id = '11')  "
        Dim foundRows() As DataRow
        Dim foundRows1() As DataRow
        Dim newtable As New DataTable
        newtable = dsPBP.Tables(0)
        If (exp1 <> "") Then
            foundRows = newtable.Select(exp1)
            hgola = foundRows.Length
        End If
        Dim exp2 As String = " team_id=" & m_objGameDetails.AwayTeamID & " and  period <='" & tperiod & "'   And (event_code_id = '17' Or event_code_id = '28' or event_code_id = '11')  "
        If (exp2 <> "") Then
            foundRows1 = newtable.Select(exp2)
            agola = foundRows1.Length
        End If
    End Sub

    Private Sub BuildPlayByPlay()
        Try
            pencheck = ""
            m_strBuilder = New StringBuilder

            Dim StrTime As String
            Dim NextPeriod As Integer
            Dim temper As String
            Dim dd As New DataSet
            Dim strDate As String = String.Empty
            Dim dsGameStats As New DataSet

            dsGameStats = m_objTeamStats.GetTeamStats(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If (m_objGameDetails.ModuleID <> 3) Then
                dsPBP = m_Objrep.GetPBPreport(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            Else
                dsPBP = m_Objrep.GetTouchesreport(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            End If
            Dim Dt As DateTime
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                Dt = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
            Else
                Dt = CDate(m_objGameDetails.GameDate)
            End If
            If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                strDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
            Else
                strDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
            End If
            Dim strDates() As String = strDate.Split(CChar(" "))
            Dim teamabr As String
            m_strBuilder.Append("<html><body>")
            m_strBuilder.Append("<table width=800px style=""font-family:@Arial Unicode MS"" align=center >")
            Dim strfieldname As String = m_objGameDetails.FieldName
            If (strfieldname.Trim() <> "") Then
                strfieldname = "(" + strfieldname + ")"

            End If
            m_strBuilder.Append("<tr align=" & "center" & "><td style=font-size:12;><b>" & m_objGameDetails.HomeTeam & " vs " & m_objGameDetails.AwayTeam & "</b>  " & strfieldname & " </td></tr></table>")
            m_strBuilder.Append("<table><tr><td colspan=2 width=800px style=font-size:14><b>" & strPlaybyplay & "</b></td> <td colspan=2 width=200px style=font-size:13 align=" & "right" & "> " & strDates(0).ToString & "</td></tr></table><br>")
            Dim startSeqNo As Decimal = 0
            Dim EndSeqNo As Decimal = 0
            Dim startSeqNo1 As Decimal = 0
            Dim EndSeqNo1 As Decimal = 0

            Dim drStart() As DataRow
            Dim drEnd() As DataRow
            If dsPBP.Tables.Count > 1 Then
                If (dsPBP.Tables(1).Rows.Count = 0) Then

                    ' GET SEQUNCE NUMBERS, TO DISPLAY THE BOOKINGS AND SUBSTITUTIONS WHICH ARE IN BETWEEN END OF THE FIRST HALF AND START OF THE SECOND HALF.
                    drStart = dsPBP.Tables(0).Select("PERIOD = 2 AND EVENT_CODE_ID = 21")
                    drEnd = dsPBP.Tables(0).Select("PERIOD = 1 AND EVENT_CODE_ID = 13")
                    If drStart.Length > 0 Then
                        startSeqNo = CDec(drStart(0).Item("SEQUENCE_NUMBER"))
                    Else
                        If dsPBP.Tables(0).Rows.Count > 0 Then
                            startSeqNo = CDec(dsPBP.Tables(0).Rows(dsPBP.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER"))
                            startSeqNo = startSeqNo + 1
                        End If

                    End If
                    If drEnd.Length > 0 Then
                        EndSeqNo = CDec(drEnd(0).Item("SEQUENCE_NUMBER"))
                    End If
                    ' GET SEQUNCE NUMBERS, TO DISPLAY THE BOOKINGS AND SUBSTITUTIONS WHICH ARE IN BETWEEN END OF THE SECOND HALF AND START HALF.
                    Dim drStart1() As DataRow = dsPBP.Tables(0).Select("PERIOD = 3 AND EVENT_CODE_ID = 21")
                    Dim drEnd1() As DataRow = dsPBP.Tables(0).Select("PERIOD = 2 AND EVENT_CODE_ID = 13")
                    If drStart1.Length > 0 Then
                        startSeqNo1 = CDec(drStart1(0).Item("SEQUENCE_NUMBER"))
                    Else
                        If dsPBP.Tables(0).Rows.Count > 0 Then
                            startSeqNo1 = CDec(dsPBP.Tables(0).Rows(dsPBP.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER"))
                            startSeqNo1 = startSeqNo1 + 1
                        End If
                    End If
                    If drEnd1.Length > 0 Then
                        EndSeqNo1 = CDec(drEnd1(0).Item("SEQUENCE_NUMBER"))
                    End If
                End If
            End If


            For i As Integer = 0 To dsPBP.Tables(0).Rows.Count - 1

                If (dsPBP.Tables(0).Rows(i).Item("team_id").ToString = m_objGameDetails.HomeTeamID.ToString) Then
                    teamabr = m_objGameDetails.HomeTeamAbbrev.ToString.Trim
                ElseIf (dsPBP.Tables(0).Rows(i).Item("team_id").ToString = m_objGameDetails.AwayTeamID.ToString) Then
                    teamabr = m_objGameDetails.AwayTeamAbbrev.ToString.Trim
                Else
                    teamabr = ""
                End If

                If (teamabr.Length = 1) Then
                    teamabr = teamabr + "  "
                ElseIf (teamabr.ToString.Length = 2) Then
                    teamabr = teamabr + " "
                End If
                ElapsedTime = CInt(dsPBP.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                If m_objGameDetails.CoverageLevel <= 3 And ElapsedTime > 0 Then
                    ElapsedTime = ElapsedTime + 30
                End If

                m_NextElapsedTime = CInt(dsPBP.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                CurrPeriod = CInt(dsPBP.Tables(0).Rows(i).Item("PERIOD"))
                StrTime = CalculateTimeElapseAfter(m_NextElapsedTime, NextPeriod)
                StrTime = "(" + TimeAfterRegularInterval(ElapsedTime, CurrPeriod) + ")"
                tempinit = ""
                tempinit1 = ""
                tempinit2 = ""
                tempname = ""
                tempname1 = ""
                tempname2 = ""
                tempname2 = ""
                tempindex = 0
                tempindex1 = 0
                tempnamecomm = 0
                tempnamecomm1 = 0
                tempnamecomm2 = 0
                tempnamecomm2 = 0
                ' '' ''game start
                If (m_objGameDetails.ModuleID <> 3) Then
                    Dim L1, L2, l1l2 As String

                    If (dsPBP.Tables(0).Rows(i).Item("L1").ToString <> "") Then
                        L1 = " " + dsPBP.Tables(0).Rows(i).Item("L1").ToString
                    Else
                        L1 = ""
                    End If
                    If (dsPBP.Tables(0).Rows(i).Item("L2").ToString <> "") Then
                        L2 = " " + dsPBP.Tables(0).Rows(i).Item("L2").ToString
                    Else
                        L2 = ""
                    End If
                    If ((L1 <> "") And (L2 <> "")) Then
                        l1l2 = L1 + " " + L2
                    ElseIf ((L1 = "") And (L2 <> "") And ((dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "19") Or (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "20") Or (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "6"))) Then
                        l1l2 = "( )" + " " + L2
                    ElseIf ((L1 <> "") And (L2 = "") And ((dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "19") Or (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "20") Or (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "6"))) Then
                        l1l2 = L1 + " " + "( )"
                    ElseIf (L1 <> "" Or L2 <> "") Then
                        l1l2 = L1 + " " + L2
                    ElseIf ((L1 = "") And (L2 = "")) Then
                        l1l2 = ""
                    End If

                    If (temper <> dsPBP.Tables(0).Rows(i).Item("period").ToString) Then
                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "1") Then
                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & strFirstHalf & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        End If
                        '''''''
                        If DirectCast(DirectCast(dsPBP.Tables(0).Rows(i).Item("SEQUENCE_NUMBER"), System.Object), System.Decimal) < startSeqNo And DirectCast(DirectCast(dsPBP.Tables(0).Rows(i).Item("SEQUENCE_NUMBER"), System.Object), System.Decimal) > EndSeqNo Then
                            If (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "22") Then
                                namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                                namestrahy2(dsPBP.Tables(0).Rows(i).Item("player_out").ToString)

                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  for " & tempinit1 & "    " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("sub_reason").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                                Continue For
                                'booking And Expulsion
                            ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "2") Then
                                namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; Booking " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("Booking_type").ToString & " for " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("booking_reason").ToString & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                                Continue For
                                'Expulsion
                            ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "7") Then
                                namestrahy(dsPBP.Tables(0).Rows(i).Item("player_out").ToString)
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; Booking " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("Booking_type").ToString & " for " & tempname.ToString.Substring(0, tempindex + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("booking_reason").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                                Continue For
                            End If

                        End If
                        '''''''''''
                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "2") Then
                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & strSecondHalf & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        End If

                        If DirectCast(DirectCast(dsPBP.Tables(0).Rows(i).Item("SEQUENCE_NUMBER"), System.Object), System.Decimal) < startSeqNo1 And DirectCast(DirectCast(dsPBP.Tables(0).Rows(i).Item("SEQUENCE_NUMBER"), System.Object), System.Decimal) > EndSeqNo1 Then
                            If (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "22") Then
                                namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                                namestrahy2(dsPBP.Tables(0).Rows(i).Item("player_out").ToString)

                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  for " & tempinit1 & "    " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("sub_reason").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                                Continue For
                                'booking And Expulsion
                            ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "2") Then
                                namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; Booking " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("Booking_type").ToString & " for " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("booking_reason").ToString & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                                Continue For
                                'Expulsion
                            ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "7") Then
                                namestrahy(dsPBP.Tables(0).Rows(i).Item("player_out").ToString)
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; Booking " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("Booking_type").ToString & " for " & tempname.ToString.Substring(0, tempindex + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("booking_reason").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                                Continue For
                            End If

                        End If

                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "3") Then

                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & str1stExtraTime & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        End If
                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "4" And dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString <> "13") Then

                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & str2ndExtraTime & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        End If
                    End If
                    temper = dsPBP.Tables(0).Rows(i).Item("period").ToString

                    If (dsPBP.Tables(0).Rows(i).Item("orig_seq").ToString <> "NULL" And dsPBP.Tables(0).Rows(i).Item("continuation").ToString <> "F") Then
                        StrTime = "     "
                    End If
                    If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "1" And dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "13") Then
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11> </td><td width=5px></td><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & strend & "  " & strFirstHalf & "</b> </td></tr><tr><td>&nbsp;</td></tr></table>")
                        goalcall(1)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & m_objGameDetails.HomeTeam.ToString & "  " & hgola & " </b> </td> <tr><td style=font-size:11><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & m_objGameDetails.AwayTeam.ToString & "  " & agola & " </b> </td></tr></table><br><br>")
                    End If
                    If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "2" And dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "13") Then
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11> </td><td width=5px></td><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & strend & " " & strSecondHalf & "</b> </td></tr><tr><td>&nbsp;</td></tr></table>")
                        goalcall(2)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & m_objGameDetails.HomeTeam.ToString & "  " & hgola & " </b> </td> <tr><td style=font-size:11><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & m_objGameDetails.AwayTeam.ToString & "  " & agola & " </b> </td></tr></table><br><br>")
                    End If
                    If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "3" And dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "13") Then
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11> </td><td width=5px></td><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & strend & " " & str1stExtraTime & "</b> </td></tr><tr><td>&nbsp;</td></tr></table>")
                        goalcall(3)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & m_objGameDetails.HomeTeam.ToString & "  " & hgola & " </b> </td> <tr><td style=font-size:11><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & m_objGameDetails.AwayTeam.ToString & "  " & agola & " </b> </td></tr></table><br><br>")
                    End If
                    If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "4" And dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "13") Then
                        m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11> </td><td width=5px></td><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & strend & " " & str2ndExtraTime & "</b> </td></tr><tr><td>&nbsp;</td></tr></table>")
                        goalcall(4)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & m_objGameDetails.HomeTeam.ToString & "  " & hgola & " </b> </td> <tr><td style=font-size:11><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & m_objGameDetails.AwayTeam.ToString & "  " & agola & " </b> </td></tr></table><br><br>")
                    End If

                    'throw in
                    If (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "47") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  By  " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'goal kick
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "46") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  By  " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'offside
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "16") Then
                        Dim tempoffside As String
                        Dim tempoffside111 As String
                        tempoffside111 = dsPBP.Tables(0).Rows(i).Item("offside_desc").ToString()
                        If (dsPBP.Tables(0).Rows(i).Item("offside_desc").ToString() <> "Unknown/Other") Then
                            tempoffside = dsPBP.Tables(0).Rows(i).Item("offside_desc").ToString
                        Else
                            tempoffside = "Offside(Unknown)"
                        End If
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & tempoffside & "  By " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & " - " & dsPBP.Tables(0).Rows(i).Item("offside_result").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'substituation
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "22") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("player_out").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  for " & tempinit1 & "    " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("sub_reason").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'booking And Expulsion
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "2") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; Booking " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("Booking_type").ToString & " for " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("booking_reason").ToString & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'Expulsion
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "7") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("player_out").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; Booking " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("Booking_type").ToString & " for " & tempname.ToString.Substring(0, tempindex + 1) & "   reason " & dsPBP.Tables(0).Rows(i).Item("booking_reason").ToString & " " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'free kick
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "9") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("FOOT_desc").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("FK_Result").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'key Movement
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "52") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td><td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; key movement" & tempinit1 & "    " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & tempinit & "    " & tempname.ToString.Substring(0, tempindex + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "51") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td><td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; key movement" & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & "    " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "50") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("Assist_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td><td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; key movement" & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & "   " & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " " & tempinit1 & "    " & tempname1.ToString.Substring(0, tempindex1 + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "53") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px><td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td></td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; key movement" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   by " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        '10 yard
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "60") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td>  <td  style=font-size:11>&nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'back-pass
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "61") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td>  <td  style=font-size:11>&nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("BACK_PASS_DESC").ToString & "  " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'Goal thread
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "42") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; Goal threat" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  by  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("goal_threat_desc").ToString & " to " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & " " & dsPBP.Tables(0).Rows(i).Item("goal_threat_def").ToString & "" & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")

                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "43") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; Goal threat" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  by  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("goal_threat_desc").ToString & " to " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & " " & dsPBP.Tables(0).Rows(i).Item("goal_threat_def").ToString & " " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")

                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "44") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; Goal threat" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  by   " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("goal_threat_desc").ToString & " to " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & " " & dsPBP.Tables(0).Rows(i).Item("goal_threat_def").ToString & "" & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "45") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; Goal threat" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  by   " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("goal_threat_desc").ToString & " to " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & " " & dsPBP.Tables(0).Rows(i).Item("goal_threat_def").ToString & "" & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'def action 
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "49") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("def_action_type").ToString & "  by  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("def_action_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("def_action_result").ToString & "" & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'cross 
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "6") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        If (dsPBP.Tables(0).Rows(i).Item("corner_cross_type").ToString <> "Cross") Then
                            If (dsPBP.Tables(0).Rows(i).Item("corner_cross_RES").ToString <> "Cross") Then
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_type").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("corner_cross_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_res").ToString & "      " & dsPBP.Tables(0).Rows(i).Item("DEFENDED_TYPE").ToString & "" & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            Else
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_type").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("corner_cross_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_res").ToString & "   " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   " & dsPBP.Tables(0).Rows(i).Item("DEFENDED_TYPE").ToString & "" & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            End If
                        Else
                            If (dsPBP.Tables(0).Rows(i).Item("corner_cross_RES").ToString <> "Cross") Then
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_desc").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("corner_cross_res").ToString & "        " & dsPBP.Tables(0).Rows(i).Item("DEFENDED_TYPE").ToString & "" & l1l2 & "</td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            Else
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_desc").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("corner_cross_res").ToString & "   " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "    " & dsPBP.Tables(0).Rows(i).Item("DEFENDED_TYPE").ToString & "" & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            End If
                        End If
                        'corner 
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "5") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("FOOT_desc").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_type").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("corner_cross_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("corner_cross_res").ToString & "     " & tempinit1 & "   " & tempname1.ToString.Substring(0, tempindex1 + 1) & "  " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")

                        'ontarget
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "20") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=895px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;  On Target  " & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("shot_result").ToString & " " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("save_type").ToString & "  " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'offtarget
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "19") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=895px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>   &nbsp;&nbsp;&nbsp;  Off Target " & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & " " & dsPBP.Tables(0).Rows(i).Item("shot_result").ToString & "  " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "  " & dsPBP.Tables(0).Rows(i).Item("save_type").ToString & "  " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        'foul
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "8") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        If (dsPBP.Tables(0).Rows(i).Item("foul_type").ToString <> "Foul") Then
                            If (dsPBP.Tables(0).Rows(i).Item("foul_type").ToString <> "Obstruction") Then
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("foul_type").ToString & "  by  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  Resulted in " & dsPBP.Tables(0).Rows(i).Item("foul_result").ToString & "   " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            Else
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("foul_type").ToString & "  by " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  on  " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & " Resulted in " & dsPBP.Tables(0).Rows(i).Item("foul_result").ToString & "    " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            End If
                        Else
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>   &nbsp;&nbsp;&nbsp; " & dsPBP.Tables(0).Rows(i).Item("foul_type").ToString & " by " & tempinit & " " & tempname.ToString.Substring(0, tempindex + 1) & "  on  " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & " Resulted in " & dsPBP.Tables(0).Rows(i).Item("foul_result").ToString & "     " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        End If
                        'goal Normal
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "11") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("second_assister_id").ToString)
                        namestrahy3(dsPBP.Tables(0).Rows(i).Item("assist_player").ToString)
                        Dim goaltype As String = dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString
                        If (goaltype = "Goal") Then
                            goaltype = "Normal Goal"
                        End If
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=1200px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; <b> " & goaltype & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "    " & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & "      " & dsPBP.Tables(0).Rows(i).Item("situation").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("goalzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("keeperzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("celebration").ToString & "  " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   " & tempinit2 & "  " & tempname2.ToString.Substring(0, tempindex2 + 1) & "    " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   " & dsPBP.Tables(0).Rows(i).Item("Assist_type").ToString & " </b> " & l1l2 & " </td></tr><td align=left style=font-size:11 width=40px></td></table>")
                        'goal own
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "28") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        If (dsPBP.Tables(0).Rows(i).Item("second_assister_id").ToString <> "") Then

                            namestrahy2(dsPBP.Tables(0).Rows(i).Item("second_assister_id").ToString)
                        End If
                        Dim goaltype As String = dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=900px style=font-family:@Arial Unicode MS><b><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>    &nbsp;&nbsp;&nbsp; <b> " & goaltype & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "    " & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & "      " & dsPBP.Tables(0).Rows(i).Item("situation").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("goalzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("keeperzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("celebration").ToString & "           " & dsPBP.Tables(0).Rows(i).Item("Assist_type").ToString & " </b> " & l1l2 & "</td></tr></td><td align=left style=font-size:11 width=40px></td></tr></b></table>")
                        '    'penality  30,31,41                        
                        'penalty
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "17") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("caused_by_player").ToString)
                        namestrahy3(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        Dim goaltype As String = dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=1200px style=font-family:@Arial Unicode MS><b><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>   &nbsp;&nbsp;&nbsp;  <b> Penalty Goal Kick " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "   " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   " & tempinit2 & "  " & tempname2.ToString.Substring(0, tempindex2 + 1) & "    " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "   " & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & "      " & dsPBP.Tables(0).Rows(i).Item("situation").ToString & "  " & dsPBP.Tables(0).Rows(i).Item("goalzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("keeperzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("celebration").ToString & "    " & dsPBP.Tables(0).Rows(i).Item("Assist_type").ToString & " </b> " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></b></table>")
                        'free kick
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "10") Then
                        m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11> " & StrTime & " </td><td width=5px></td><td  style=font-size:11><b>" & dsPBP.Tables(0).Rows(i).Item("event_code_desc").ToString & " </B> " & l1l2 & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table></td></tr>")
                        'miss penalty
                    ElseIf (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "18") Then
                        namestrahy(dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString)
                        namestrahy2(dsPBP.Tables(0).Rows(i).Item("caused_by_player").ToString)
                        namestrahy3(dsPBP.Tables(0).Rows(i).Item("defensive_player").ToString)
                        Dim temp As String = dsPBP.Tables(0).Rows(i).Item("caused_by_player").ToString
                        If (temp <> "") Then
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=1000px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>      &nbsp;&nbsp;&nbsp; Miss Penalty" & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & " " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & " caused by  " & tempinit1 & " " & tempname1.ToString.Substring(0, tempindex1 + 1) & "  In goal  " & tempinit2 & "  " & tempname2.ToString.Substring(0, tempindex2 + 1) & "        " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("shot_result").ToString & "     " & dsPBP.Tables(0).Rows(i).Item("goalzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("keeperzone").ToString & "      " & dsPBP.Tables(0).Rows(i).Item("save_type").ToString & "   " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        Else
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=1000px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>    &nbsp;&nbsp;&nbsp; Miss Penalty" & dsPBP.Tables(0).Rows(i).Item("shot_type").ToString & "  " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "   " & tempinit1 & "  " & tempname1.ToString.Substring(0, tempindex1 + 1) & "  In goal   " & tempinit2 & "  " & tempname2.ToString.Substring(0, tempindex2 + 1) & "    " & dsPBP.Tables(0).Rows(i).Item("shot_desc").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("shot_result").ToString & "     " & dsPBP.Tables(0).Rows(i).Item("goalzone").ToString & "   " & dsPBP.Tables(0).Rows(i).Item("keeperzone").ToString & "      " & dsPBP.Tables(0).Rows(i).Item("save_type").ToString & "  " & l1l2 & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                        End If
                    End If
                    'goal calculation
                    If ((dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "17") Or (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "28") Or (dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString = "11")) Then
                        Dim expression As String = " team_id='" & m_objGameDetails.HomeTeamID & "' and sequence_number > 1.0000  and sequence_number <='" & dsPBP.Tables(0).Rows(i).Item("sequence_number").ToString & "'  And (event_code_id = '17' Or event_code_id = '28' or event_code_id = '11')  "
                        Dim foundRows() As DataRow
                        Dim foundRows1() As DataRow
                        Dim newtable As New DataTable
                        newtable = dsPBP.Tables(0)
                        Dim z As Integer = 0
                        If (expression <> "") Then
                            foundRows = newtable.Select(expression)
                            z = foundRows.Length
                        End If
                        Dim z1 As Integer = 0
                        Dim expression1 As String = " team_id=" & m_objGameDetails.AwayTeamID & "and sequence_number > 1.0000  and sequence_number <='" & dsPBP.Tables(0).Rows(i).Item("sequence_number").ToString & "'  And (event_code_id = '17' Or event_code_id = '28' or event_code_id = '11')  "
                        If (expression <> "") Then
                            foundRows1 = newtable.Select(expression1)
                            z1 = foundRows1.Length
                        End If
                        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <b>" & m_objGameDetails.HomeTeam & " " & z & "   &nbsp;&nbsp;   " & m_objGameDetails.AwayTeam & " " & z1 & "  </b></td></tr></table><br>")
                    End If

                Else
                    If (temper <> dsPBP.Tables(0).Rows(i).Item("period").ToString) Then
                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "1") Then
                            m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:14;color:black; style=font-family:@Arial Unicode MS align=center><b> 1st Quarter</b> </td></tr></tr></table><br>")

                        End If
                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "2") Then

                            m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:12;color:black; style=font-family:@Arial Unicode MS align=left><b> END OF QUARTER </b> </td></tr></tr></table><br>")
                            m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:14;color:black; style=font-family:@Arial Unicode MS align=center><b> 2nd Quarter</b> </td></tr></tr></table><br>")

                        End If
                        If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "3") Then

                            m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:12;color:black; style=font-family:@Arial Unicode MS align=left><b> END OF QUARTER </b> </td></tr></tr></table><br>")
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:14;color:black; style=font-family:@Arial Unicode MS align=center><b> 1st Extra Time </b> </td></tr></tr></table><br>")
                        End If
                        If (m_objGameDetails.ModuleID <> 3) Then
                            If (dsPBP.Tables(0).Rows(i).Item("period").ToString = "4" And dsPBP.Tables(0).Rows(i).Item("event_code_id").ToString <> "13") Then
                                m_strBuilder.Append("<br><table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:12;color:black; style=font-family:@Arial Unicode MS align=left><b> END OF QUARTER </b> </td></tr></tr></table><br>")
                                m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:14;color:black; style=font-family:@Arial Unicode MS align=center><b> 2nd Extra Time </b> </td></tr></tr></table><br>")
                            End If
                        End If
                    End If

                    namestrahy(dsPBP.Tables(0).Rows(i).Item("player").ToString)
                    Dim LOCATION As String
                    Dim touch, namepbp As String
                    If (dsPBP.Tables(0).Rows(i).Item("X_FIELD_ZONE").ToString <> "") Then
                        LOCATION = "(" + dsPBP.Tables(0).Rows(i).Item("X_FIELD_ZONE").ToString + " , " + dsPBP.Tables(0).Rows(i).Item("Y_FIELD_ZONE").ToString + ")" + " ;"
                    Else
                        LOCATION = "_ ;"
                    End If
                    If (dsPBP.Tables(0).Rows(i).Item("TOUCH_TYPE_DESC").ToString <> "") Then
                        touch = dsPBP.Tables(0).Rows(i).Item("TOUCH_TYPE_DESC").ToString + " "
                    Else
                        touch = "_ "
                    End If
                    If (tempname <> "") Then
                        namepbp = tempinit + " " + tempname.ToString.Substring(0, tempindex + 1) + " ;"
                    Else
                        namepbp = "_ ;"
                    End If
                    If (teamabr = "") Then
                        teamabr = "_ "
                    End If

                    temper = dsPBP.Tables(0).Rows(i).Item("period").ToString
                    m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=900px style=font-family:@Arial Unicode MS><b><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>    &nbsp;&nbsp;&nbsp;  " & namepbp & "  " & LOCATION & "  " & touch & "</td></tr></td><td align=left style=font-size:11 width=40px></td></tr></b></table>")
                End If
                'End Game
            Next
            If dsPBP.Tables.Count > 1 Then
                If (dsPBP.Tables(1).Rows.Count > 0) Then
                    m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & strPenaltyShootouts & "  </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                    For i As Integer = 0 To dsPBP.Tables(1).Rows.Count - 1
                        'penality  30,31,41
                        If (dsPBP.Tables(1).Rows(i).Item("event_code_id").ToString = "31") Then
                            namestrahy(dsPBP.Tables(1).Rows(i).Item("offensive_player").ToString)
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=900px style=font-family:@Arial Unicode MS><b><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>    &nbsp;&nbsp;&nbsp; " & dsPBP.Tables(1).Rows(i).Item("shot_desc").ToString & "  Missed   " & dsPBP.Tables(1).Rows(i).Item("shot_result").ToString & " " & dsPBP.Tables(1).Rows(i).Item("shot_type").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(1).Rows(i).Item("goalzone").ToString & "  " & dsPBP.Tables(1).Rows(i).Item("keeperzone").ToString & "                  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></b></table>")
                        ElseIf (dsPBP.Tables(1).Rows(i).Item("event_code_id").ToString = "41") Then

                            namestrahy(dsPBP.Tables(1).Rows(i).Item("offensive_player").ToString)
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=900px style=font-family:@Arial Unicode MS><b><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>    &nbsp;&nbsp;&nbsp; " & dsPBP.Tables(1).Rows(i).Item("event_code_desc").ToString & "  Missed  " & dsPBP.Tables(1).Rows(i).Item("shot_desc").ToString & "  " & dsPBP.Tables(1).Rows(i).Item("shot_result").ToString & "  " & dsPBP.Tables(1).Rows(i).Item("shot_type").ToString & "   " & tempinit & "  " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(1).Rows(i).Item("goalzone").ToString & "  " & dsPBP.Tables(1).Rows(i).Item("keeperzone").ToString & "                  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></b></table>")
                        ElseIf (dsPBP.Tables(1).Rows(i).Item("event_code_id").ToString = "30") Then
                            namestrahy(dsPBP.Tables(1).Rows(i).Item("offensive_player").ToString)
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=900px style=font-family:@Arial Unicode MS><b><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11>    &nbsp;&nbsp;&nbsp;" & dsPBP.Tables(1).Rows(i).Item("event_code_desc").ToString & "   Scored  " & dsPBP.Tables(1).Rows(i).Item("shot_desc").ToString & "   " & dsPBP.Tables(1).Rows(i).Item("shot_result").ToString & " " & dsPBP.Tables(1).Rows(i).Item("shot_type").ToString & "    " & tempinit & "   " & tempname.ToString.Substring(0, tempindex + 1) & "  " & dsPBP.Tables(1).Rows(i).Item("goalzone").ToString & "  " & dsPBP.Tables(1).Rows(i).Item("keeperzone").ToString & "     </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></b></table>")
                        End If
                    Next
                End If
            End If

            m_strBuilder.Append("</body></html>")
            wbReport.Refresh()


            wbReport.DocumentText = m_strBuilder.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function namestrahy(ByVal namestr As String) As String
        tempname = namestr.Trim()
        tempnamecomm = tempname.IndexOf(",")
        If (tempnamecomm <> -1) Then
            tempindex = tempname.IndexOf(",") - 1
            If (tempindex + 2 < tempname.Length) Then
                tempinit = tempname.Substring(tempindex + 2, 2).ToString + "."
            End If
        Else
            'tempinit = tempname
            tempindex = tempname.Length - 1

        End If
        Return tempinit
        'Dim tempname As String = dsPBP.Tables(0).Rows(i).Item("offensive_player").ToString
        'Dim tempnamecomm As Integer = tempname.IndexOf(",")
        'Dim tempindex As Integer = tempname.IndexOf(",") - 1
        'Dim tempinit As String
        'If (tempindex + 2 < tempname.Length) Then
        '    tempinit = tempname.Substring(tempindex + 2, 2).ToString + "."
        'End If
    End Function
    Public Function namestrahy2(ByVal namestr1 As String) As String
        tempname1 = namestr1.Trim()
        tempnamecomm1 = tempname1.IndexOf(",")
        If (tempnamecomm1 <> -1) Then
            tempindex1 = tempname1.IndexOf(",") - 1
            If (tempindex1 + 2 < tempname1.Length) Then
                tempinit1 = tempname1.Substring(tempindex1 + 2, 2).ToString + "."
            End If
        Else
            'tempinit = tempname
            tempindex1 = tempname1.Length - 1

        End If
        Return tempinit1
    End Function
    Public Function namestrahy3(ByVal namestr2 As String) As String
        tempname2 = namestr2.Trim()
        tempnamecomm2 = tempname2.IndexOf(",")
        If (tempnamecomm2 <> -1) Then
            tempindex2 = tempname2.IndexOf(",") - 1
            If (tempindex2 + 2 < tempname2.Length) Then
                tempinit2 = tempname2.Substring(tempindex2 + 2, 2).ToString + "."
            End If
        Else
            'tempinit = tempname
            tempindex2 = tempname2.Length - 1

        End If
        Return tempinit2
    End Function

    Private Sub frmPBPReport_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress

    End Sub
    Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            Dim StrTime As String = "00:00"
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "45+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                        'IIf(DiffSec.ToString.Length = 1, "0", CStr(DiffSec))
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)

                        'DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        'StrTime = "90+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "90+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "90+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 3 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        'StrTime = "105+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "105+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "105+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 4 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        'StrTime = "120+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "120+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "120+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 5 Then
                    StrTime = "120"
                End If
                'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
            Else
                If CurrPeriod = 1 Or CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "45+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                        'StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

                    End If
                Else
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "15+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "15+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                End If
            End If
            Return StrTime
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub frmPBPReport_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode.ToString = "F5") Then
            If (btnRefresh.Enabled.ToString() = "True") Then
                btnRefresh_Click(sender, e)
            End If
        End If
    End Sub

    Private Sub wbReport_PreviewKeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles wbReport.PreviewKeyDown
        If (e.KeyCode.ToString = "F5") Then
            If (btnRefresh.Enabled.ToString() = "True") Then
                btnRefresh_Click(sender, e)
            End If
        End If
    End Sub

    Private Sub BuildPlayByPlayPZ()
        Try
            m_strBuilder = New StringBuilder

            Dim StrTime As String
            Dim temper As String
            Dim dd As New DataSet
            Dim strDate As String = String.Empty
            Dim dsGameStats As New DataSet
            dsGameStats = m_objTeamStats.GetTeamStats(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            Dim dtPbpEvents As DataTable = _objGeneral.GetPZActionreport(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CInt(IIf(m_objGameDetails.IsDefaultGameClock, 1, 0))).Tables(0)
            Dim Dt As DateTime
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                Dt = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
            Else
                Dt = CDate(m_objGameDetails.GameDate)
            End If
            If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                strDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
            Else
                strDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
            End If
            Dim strDates() As String = strDate.Split(CChar(" "))
            Dim teamabr As String
            m_strBuilder.Append("<html><body>")
            m_strBuilder.Append("<table width=800px style=""font-family:@Arial Unicode MS"" align=center >")
            Dim strfieldname As String = m_objGameDetails.FieldName
            If (strfieldname.Trim() <> "") Then
                strfieldname = "(" + strfieldname + ")"
            End If
            m_strBuilder.Append("<tr align=" & "center" & "><td style=font-size:12;><b>" & m_objGameDetails.HomeTeam & " vs " & m_objGameDetails.AwayTeam & "</b>  " & strfieldname & " </td></tr></table>")
            m_strBuilder.Append("<table><tr><td colspan=2 width=800px style=font-size:14><b>" & strPlaybyplay & "</b></td> <td colspan=2 width=200px style=font-size:13 align=" & "right" & "> " & strDates(0).ToString & "</td></tr></table><br>")
            Dim startSeqNo As Decimal = 0
            Dim EndSeqNo As Decimal = 0
            Dim startSeqNo1 As Decimal = 0
            Dim EndSeqNo1 As Decimal = 0
            Dim drStart() As DataRow
            Dim drEnd() As DataRow


            ' GET SEQUNCE NUMBERS, TO DISPLAY THE BOOKINGS AND SUBSTITUTIONS WHICH ARE IN BETWEEN END OF THE FIRST HALF AND START OF THE SECOND HALF.
            drStart = dtPbpEvents.Select("PERIOD = 2 AND EVENT_CODE_ID = 21")
            drEnd = dtPbpEvents.Select("PERIOD = 1 AND EVENT_CODE_ID = 13")
            If drStart.Length > 0 Then
                startSeqNo = CDec(drStart(0).Item("SEQUENCE_NUMBER"))
            Else
                If dtPbpEvents.Rows.Count > 0 Then
                    startSeqNo = CDec(dtPbpEvents.Rows(dtPbpEvents.Rows.Count - 1).Item("SEQUENCE_NUMBER"))
                    startSeqNo = startSeqNo + 1
                End If
            End If
            If drEnd.Length > 0 Then
                EndSeqNo = CDec(drEnd(0).Item("SEQUENCE_NUMBER"))
            End If
            ' GET SEQUNCE NUMBERS, TO DISPLAY THE BOOKINGS AND SUBSTITUTIONS WHICH ARE IN BETWEEN END OF THE SECOND HALF AND START HALF.
            Dim drStart1() As DataRow = dtPbpEvents.Select("PERIOD = 3 AND EVENT_CODE_ID = 21")
            Dim drEnd1() As DataRow = dtPbpEvents.Select("PERIOD = 2 AND EVENT_CODE_ID = 13")
            If drStart1.Length > 0 Then
                startSeqNo1 = CDec(drStart1(0).Item("SEQUENCE_NUMBER"))
            Else
                If dtPbpEvents.Rows.Count > 0 Then
                    startSeqNo1 = CDec(dtPbpEvents.Rows(dtPbpEvents.Rows.Count - 1).Item("SEQUENCE_NUMBER"))
                    startSeqNo1 = startSeqNo1 + 1
                End If
            End If
            If drEnd1.Length > 0 Then
                EndSeqNo1 = CDec(drEnd1(0).Item("SEQUENCE_NUMBER"))
            End If


            For i As Integer = 0 To dtPbpEvents.Rows.Count - 1
                teamabr = dtPbpEvents.Rows(i).Item("Team").ToString
                If (teamabr.Length = 1) Then
                    teamabr = teamabr + "  "
                ElseIf (teamabr.ToString.Length = 2) Then
                    teamabr = teamabr + " "
                End If
                StrTime = CStr(dtPbpEvents.Rows(i).Item("Time"))
                CurrPeriod = CInt(dtPbpEvents.Rows(i).Item("Period"))

                If (temper <> dtPbpEvents.Rows(i).Item("Period").ToString) And dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "21" Then
                    Select Case CInt(dtPbpEvents.Rows(i).Item("Period"))
                        Case 1
                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & strFirstHalf & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        Case 2
                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & strSecondHalf & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        Case 3
                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & str1stExtraTime & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                        Case 4
                            m_strBuilder.Append("<b><center><tr><td>&nbsp</td></tr><tr><td style=font-size:12> </td> <td style=font-size:12;> " & str2ndExtraTime & " </td></tr><tr><td>&nbsp;&nbsp</td></tr></table></center></b><br>")
                    End Select

                    If DirectCast(DirectCast(dtPbpEvents.Rows(i).Item("SEQUENCE_NUMBER"), System.Object), System.Decimal) < startSeqNo And DirectCast(DirectCast(dtPbpEvents.Rows(i).Item("SEQUENCE_NUMBER"), System.Object), System.Decimal) > EndSeqNo Then
                        If (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "22") Then
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp;" & dtPbpEvents.Rows(i).Item("Event").ToString & "  " & dtPbpEvents.Rows(i).Item("OFFENSIVE_PLAYER_ID").ToString & "  " & "  for " & dtPbpEvents.Rows(i).Item("PLAYER_OUT_ID").ToString & "   reason " & dtPbpEvents.Rows(i).Item("SUB_REASON_ID").ToString & " " & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            Continue For
                            'booking And Expulsion
                        ElseIf (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "2") Then
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr & "</td><td  style=font-size:11>  &nbsp;&nbsp;&nbsp; Booking " & dtPbpEvents.Rows(i).Item("Event").ToString & " " & dtPbpEvents.Rows(i).Item("BOOKING_TYPE_ID").ToString & " for " & dtPbpEvents.Rows(i).Item("OFFENSIVE_PLAYER_ID").ToString & "   reason " & dtPbpEvents.Rows(i).Item("BOOKING_REASON_ID").ToString & " " & " </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            Continue For
                            'Expulsion
                        ElseIf (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "7") Then
                            m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; Booking " & dtPbpEvents.Rows(i).Item("Event").ToString & " " & dtPbpEvents.Rows(i).Item("BOOKING_TYPE_ID").ToString & " for " & dtPbpEvents.Rows(i).Item("PLAYER_OUT_ID").ToString & "   reason " & dtPbpEvents.Rows(i).Item("BOOKING_REASON_ID").ToString & " " & "  </td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                            Continue For
                        End If
                    End If
                End If
                temper = dtPbpEvents.Rows(i).Item("Period").ToString
                If (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "13") Then
                    Select Case CInt(dtPbpEvents.Rows(i).Item("Period"))
                        Case 1
                            BuildEndHalf(1, strFirstHalf, dtPbpEvents)
                        Case 2
                            BuildEndHalf(2, strSecondHalf, dtPbpEvents)
                        Case 3
                            BuildEndHalf(3, str1stExtraTime, dtPbpEvents)
                        Case 4
                            BuildEndHalf(4, str2ndExtraTime, dtPbpEvents)
                    End Select
                End If
                If (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "10" Or dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "13" Or dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "21") Then
					m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; <b>" & dtPbpEvents.Rows(i).Item("Event").ToString & "</b> ")
                Else
                    m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td width=60 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & StrTime & "</td><td width=5px></td> <td width=15 style=font-size:11;color:black; style=font-family:@Arial Unicode MS align=left>" & teamabr.ToString & "</td><td  style=font-size:11> &nbsp;&nbsp;&nbsp; " & dtPbpEvents.Rows(i).Item("Event").ToString & " ")
                End If

                Dim eventData = _objClsGetEventData.GetEventData(m_objGameDetails.RulesData.Tables(0), m_objGameDetails.RulesData.Tables(1), m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel, CInt(dtPbpEvents.Rows(i).Item("EVENT_CODE_ID")))

                For Each rulesListBox In (From e In eventData Select e.PbpColumnName).ToList.Distinct()
                    m_strBuilder.Append(IIf(IsDBNull(dtPbpEvents.Rows(i).Item(rulesListBox)), "", dtPbpEvents.Rows(i).Item(rulesListBox).ToString))
                Next
                m_strBuilder.Append(IIf(IsDBNull(dtPbpEvents.Rows(i).Item("Area")), "", dtPbpEvents.Rows(i).Item("Area")).ToString & "</td></tr></td><td align=left style=font-size:11 width=40px></td></tr></table>")
                If ((dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "17") Or (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "28") Or (dtPbpEvents.Rows(i).Item("EVENT_CODE_ID").ToString = "11")) Then
                    Dim foundRows() As DataRow
                    Dim z As Integer = 0
                    Dim z1 As Integer = 0
                    foundRows = dtPbpEvents.Select(" TEAM_ID='" & m_objGameDetails.HomeTeamID & "' and SEQUENCE_NUMBER > 1.0000  and SEQUENCE_NUMBER <='" & dtPbpEvents.Rows(i).Item("SEQUENCE_NUMBER").ToString & "'  And (EVENT_CODE_ID = '17' Or EVENT_CODE_ID = '28' or EVENT_CODE_ID = '11')  ")
                    z = foundRows.Length
                    foundRows = dtPbpEvents.Select(" TEAM_ID=" & m_objGameDetails.AwayTeamID & "and SEQUENCE_NUMBER > 1.0000  and SEQUENCE_NUMBER <='" & dtPbpEvents.Rows(i).Item("SEQUENCE_NUMBER").ToString & "'  And (EVENT_CODE_ID = '17' Or EVENT_CODE_ID = '28' or EVENT_CODE_ID = '11')  ")
                    z1 = foundRows.Length
                    m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <b>" & m_objGameDetails.HomeTeam & " " & z & "   &nbsp;&nbsp;   " & m_objGameDetails.AwayTeam & " " & z1 & "  </b></td></tr></table><br>")
                End If
            Next
            m_strBuilder.Append("</body></html>")
            wbReport.Refresh()
            wbReport.DocumentText = m_strBuilder.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function BuildEndHalf(ByVal Period As Int16, ByVal half As String, ByVal dtPbpEvents As DataTable) As Boolean
        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11> </td><td width=5px></td><td  style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & strend & "  " & half & "</b> </td></tr><tr><td>&nbsp;</td></tr></table>")
        Dim foundRows() As DataRow
        foundRows = dtPbpEvents.Select(" team_id='" & m_objGameDetails.HomeTeamID & "' and  period <='" & Period & "'  And (event_code_id = '17' Or event_code_id = '28' or event_code_id = '11')  ")
        hgola = foundRows.Length
        foundRows = dtPbpEvents.Select(" team_id=" & m_objGameDetails.AwayTeamID & " and  period <='" & Period & "'   And (event_code_id = '17' Or event_code_id = '28' or event_code_id = '11')  ")
        agola = foundRows.Length
        m_strBuilder.Append("<table cellpadding=0 cellspacing=0 border=0 width=795px style=font-family:@Arial Unicode MS><tr style=font-family:@Arial Unicode MS ><td style=font-size:11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" & m_objGameDetails.HomeTeam.ToString & "  " & hgola & " </b> </td> <tr><td style=font-size:11><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & m_objGameDetails.AwayTeam.ToString & "  " & agola & " </b> </td></tr></table><br><br>")
    End Function

End Class