﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmValidateScore
' Author        : Shravani
' Created Date  : 29-07-09
' Description   : 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmValidateScore

#Region "Constents and Variables"
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objValidateScore As clsValidateScore = clsValidateScore.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private lsupport As New languagesupport

    Dim strmessage As String = "Invalid score!"
    Dim strmessage1 As String = "Fill the score for both teams!"
    'Dim strmessage2 As String = "Please choose league"
    'Dim strmessage3 As String = "Please choose league and duration to generate upcoming assignments"
    Dim strmessage4 As String = "Score Mismatch for"
    Dim strmessage5 As String = "Incorrect score for BOTH teams - please correct the score first!"

#End Region

    'Private Sub frmValidateScore_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    Try
    '        btnOK_Click(sender, e)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


    'Private Sub frmValidateScore_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    Try
    '        'e.Cancel = True
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    Private Sub frmValidateScore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmValidateScore", Nothing, 1, 0)
            Me.Icon = frmMain.Icon
            Me.CenterToParent()

            If m_objclsGameDetails.HomeTeam.Length > 12 Then
                lblHomeTeamName.Text = m_objclsGameDetails.HomeTeam.Substring(0, 12) & ":"
            Else
                lblHomeTeamName.Text = m_objclsGameDetails.HomeTeam & ":"
            End If

            If m_objclsGameDetails.AwayTeam.Length > 12 Then
                lblAwayTeamName.Text = m_objclsGameDetails.AwayTeam.Substring(0, 12) & ":"
            Else
                lblAwayTeamName.Text = m_objclsGameDetails.AwayTeam & ":"
            End If


            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnOK.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnOK.Text)
                lblHomeTeamName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblHomeTeamName.Text)
                lblAwayTeamName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblAwayTeamName.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            'get the latest score
            'validate with current game score
            'If not correct return NO
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOK.Text, Nothing, 1, 0)


            Dim dsValidateScrData As DataSet
            Dim HomeScore As Integer
            Dim AwayScore As Integer

            If txtHomeScore.Text = "" Or txtAwayScore.Text = "" Then
                MessageDialog.Show(strmessage1, "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If clsValidation.ValidateNumeric(txtHomeScore.Text) = False Or clsValidation.ValidateNumeric(txtAwayScore.Text) = False Then
                MessageDialog.Show(strmessage, "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtAwayScore.Text = ""
                txtHomeScore.Text = ""
                Exit Sub
            End If
            HomeScore = CInt(txtHomeScore.Text)
            AwayScore = CInt(txtAwayScore.Text)



            dsValidateScrData = m_objValidateScore.GetScores(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)

            If m_objclsGameDetails.HomeTeamID = CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                'Last entered score from Home team
                If HomeScore <> CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE")) And AwayScore <> CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE")) Then
                    Me.DialogResult = Windows.Forms.DialogResult.No
                    MessageDialog.Show(strmessage5, "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                ElseIf HomeScore = CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE")) Then
                    If AwayScore = CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE")) Then
                        Me.DialogResult = Windows.Forms.DialogResult.Yes
                    Else
                        Me.DialogResult = Windows.Forms.DialogResult.No
                        MessageDialog.Show(strmessage4 + " '" & m_objclsGameDetails.AwayTeam & "' - please correct the score first!", "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    End If
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.No
                    MessageDialog.Show(strmessage4 + " '" & m_objclsGameDetails.HomeTeam & "' - please correct the score first!", "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If
            Else
                'Last entered score from Away team
                If AwayScore <> CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE")) And HomeScore <> CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE")) Then
                    Me.DialogResult = Windows.Forms.DialogResult.No
                    MessageDialog.Show(strmessage5, "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                ElseIf AwayScore = CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE")) Then
                    If HomeScore = CInt(dsValidateScrData.Tables(0).Rows(dsValidateScrData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE")) Then
                        Me.DialogResult = Windows.Forms.DialogResult.Yes
                    Else
                        Me.DialogResult = Windows.Forms.DialogResult.No
                        MessageDialog.Show(strmessage4 + "'" & m_objclsGameDetails.HomeTeam & "' - please correct the score first!", "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    End If
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.No
                    MessageDialog.Show(strmessage4 + "'" & m_objclsGameDetails.AwayTeam & "' - please correct the score first!", "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            Me.DialogResult = Windows.Forms.DialogResult.No
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtHomeScore_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHomeScore.Leave
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtHomeScore.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtAwayScore_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAwayScore.Leave
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtAwayScore.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try

    End Sub
End Class