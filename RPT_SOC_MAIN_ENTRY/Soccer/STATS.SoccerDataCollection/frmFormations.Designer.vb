﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFormations
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picTeamLogo = New System.Windows.Forms.PictureBox()
        Me.radTeam = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblTeamName = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.sstAddGame = New System.Windows.Forms.StatusStrip()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblFormation = New System.Windows.Forms.Label()
        Me.cmbFormation = New System.Windows.Forms.ComboBox()
        Me.cmbFormationSpecification = New System.Windows.Forms.ComboBox()
        Me.UdcSoccerFormation1 = New STATS.SoccerDataCollection.udcSoccerFormation()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'picTeamLogo
        '
        Me.picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picTeamLogo.Location = New System.Drawing.Point(5, 46)
        Me.picTeamLogo.Name = "picTeamLogo"
        Me.picTeamLogo.Size = New System.Drawing.Size(45, 45)
        Me.picTeamLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picTeamLogo.TabIndex = 349
        Me.picTeamLogo.TabStop = False
        '
        'radTeam
        '
        Me.radTeam.AutoSize = True
        Me.radTeam.ForeColor = System.Drawing.Color.White
        Me.radTeam.Location = New System.Drawing.Point(582, 12)
        Me.radTeam.Name = "radTeam"
        Me.radTeam.Size = New System.Drawing.Size(81, 17)
        Me.radTeam.TabIndex = 340
        Me.radTeam.TabStop = True
        Me.radTeam.Text = "Team name"
        Me.radTeam.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Panel2.Controls.Add(Me.lblTeamName)
        Me.Panel2.Controls.Add(Me.radTeam)
        Me.Panel2.Location = New System.Drawing.Point(56, 38)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(677, 59)
        Me.Panel2.TabIndex = 348
        '
        'lblTeamName
        '
        Me.lblTeamName.AutoSize = True
        Me.lblTeamName.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeamName.ForeColor = System.Drawing.Color.White
        Me.lblTeamName.Location = New System.Drawing.Point(17, 12)
        Me.lblTeamName.Name = "lblTeamName"
        Me.lblTeamName.Size = New System.Drawing.Size(115, 25)
        Me.lblTeamName.TabIndex = 14
        Me.lblTeamName.Text = "Team name"
        '
        'PictureBox6
        '
        Me.PictureBox6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox6.Location = New System.Drawing.Point(-2, 20)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(740, 18)
        Me.PictureBox6.TabIndex = 347
        Me.PictureBox6.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.PictureBox7.Location = New System.Drawing.Point(-2, 0)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(740, 20)
        Me.PictureBox7.TabIndex = 346
        Me.PictureBox7.TabStop = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnSave)
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(-2, 453)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(735, 60)
        Me.picButtonBar.TabIndex = 368
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(559, 16)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 365
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(640, 16)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 361
        Me.btnClose.Text = "Cancel"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'sstAddGame
        '
        Me.sstAddGame.AutoSize = False
        Me.sstAddGame.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstAddGame.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstAddGame.Location = New System.Drawing.Point(0, 512)
        Me.sstAddGame.Name = "sstAddGame"
        Me.sstAddGame.Size = New System.Drawing.Size(733, 22)
        Me.sstAddGame.TabIndex = 369
        Me.sstAddGame.Text = "StatusStrip1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(72, 143)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 372
        Me.Label3.Text = "Specification:"
        '
        'lblFormation
        '
        Me.lblFormation.AutoSize = True
        Me.lblFormation.Location = New System.Drawing.Point(72, 114)
        Me.lblFormation.Name = "lblFormation"
        Me.lblFormation.Size = New System.Drawing.Size(56, 13)
        Me.lblFormation.TabIndex = 371
        Me.lblFormation.Text = "Formation:"
        '
        'cmbFormation
        '
        Me.cmbFormation.FormattingEnabled = True
        Me.cmbFormation.Location = New System.Drawing.Point(150, 111)
        Me.cmbFormation.Name = "cmbFormation"
        Me.cmbFormation.Size = New System.Drawing.Size(168, 21)
        Me.cmbFormation.TabIndex = 370
        '
        'cmbFormationSpecification
        '
        Me.cmbFormationSpecification.FormattingEnabled = True
        Me.cmbFormationSpecification.Location = New System.Drawing.Point(150, 139)
        Me.cmbFormationSpecification.Name = "cmbFormationSpecification"
        Me.cmbFormationSpecification.Size = New System.Drawing.Size(168, 21)
        Me.cmbFormationSpecification.TabIndex = 373
        '
        'UdcSoccerFormation1
        '
        Me.UdcSoccerFormation1.BackColor = System.Drawing.Color.Wheat
        Me.UdcSoccerFormation1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UdcSoccerFormation1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UdcSoccerFormation1.Location = New System.Drawing.Point(4, 166)
        Me.UdcSoccerFormation1.Name = "UdcSoccerFormation1"
        Me.UdcSoccerFormation1.Size = New System.Drawing.Size(723, 280)
        Me.UdcSoccerFormation1.TabIndex = 374
        Me.UdcSoccerFormation1.USFFormation = Nothing
        Me.UdcSoccerFormation1.USFFormationSpecification = Nothing
        Me.UdcSoccerFormation1.USFStartingLineup = Nothing
        '
        'frmFormations
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(733, 534)
        Me.Controls.Add(Me.UdcSoccerFormation1)
        Me.Controls.Add(Me.cmbFormationSpecification)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblFormation)
        Me.Controls.Add(Me.cmbFormation)
        Me.Controls.Add(Me.sstAddGame)
        Me.Controls.Add(Me.picButtonBar)
        Me.Controls.Add(Me.picTeamLogo)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "frmFormations"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmFormations"
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picTeamLogo As System.Windows.Forms.PictureBox
    Friend WithEvents radTeam As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblTeamName As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents sstAddGame As System.Windows.Forms.StatusStrip
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblFormation As System.Windows.Forms.Label
    Friend WithEvents cmbFormation As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFormationSpecification As System.Windows.Forms.ComboBox
    Friend WithEvents UdcSoccerFormation1 As STATS.SoccerDataCollection.udcSoccerFormation
End Class
