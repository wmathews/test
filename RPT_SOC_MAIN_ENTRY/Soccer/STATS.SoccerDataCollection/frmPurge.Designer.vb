﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPurge
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkAll = New System.Windows.Forms.CheckBox()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.radDays = New System.Windows.Forms.RadioButton()
        Me.grpAuditTrail = New System.Windows.Forms.GroupBox()
        Me.radBetween = New System.Windows.Forms.RadioButton()
        Me.lblbet = New System.Windows.Forms.Label()
        Me.lblto = New System.Windows.Forms.Label()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.dtpBeginDate = New System.Windows.Forms.DateTimePicker()
        Me.lbldays = New System.Windows.Forms.Label()
        Me.lsttxtdocs = New System.Windows.Forms.CheckedListBox()
        Me.txtDays = New System.Windows.Forms.TextBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnPurge = New System.Windows.Forms.Button()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grpAuditTrail.SuspendLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sstMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.Location = New System.Drawing.Point(21, 321)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(97, 19)
        Me.chkAll.TabIndex = 5
        Me.chkAll.Text = "Select all items"
        Me.chkAll.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.BackColor = System.Drawing.Color.FromArgb(CType(CType(77, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnShow.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnShow.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnShow.Location = New System.Drawing.Point(300, 103)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(60, 25)
        Me.btnShow.TabIndex = 23
        Me.btnShow.Text = "Show"
        Me.btnShow.UseVisualStyleBackColor = False
        '
        'radDays
        '
        Me.radDays.AutoSize = True
        Me.radDays.Checked = True
        Me.radDays.Location = New System.Drawing.Point(20, 30)
        Me.radDays.Name = "radDays"
        Me.radDays.Size = New System.Drawing.Size(91, 19)
        Me.radDays.TabIndex = 26
        Me.radDays.TabStop = True
        Me.radDays.Text = "Files created "
        Me.radDays.UseVisualStyleBackColor = True
        '
        'grpAuditTrail
        '
        Me.grpAuditTrail.Controls.Add(Me.chkAll)
        Me.grpAuditTrail.Controls.Add(Me.btnShow)
        Me.grpAuditTrail.Controls.Add(Me.radDays)
        Me.grpAuditTrail.Controls.Add(Me.radBetween)
        Me.grpAuditTrail.Controls.Add(Me.lblbet)
        Me.grpAuditTrail.Controls.Add(Me.lblto)
        Me.grpAuditTrail.Controls.Add(Me.dtpEndDate)
        Me.grpAuditTrail.Controls.Add(Me.dtpBeginDate)
        Me.grpAuditTrail.Controls.Add(Me.lbldays)
        Me.grpAuditTrail.Controls.Add(Me.lsttxtdocs)
        Me.grpAuditTrail.Controls.Add(Me.txtDays)
        Me.grpAuditTrail.Location = New System.Drawing.Point(12, 41)
        Me.grpAuditTrail.Name = "grpAuditTrail"
        Me.grpAuditTrail.Size = New System.Drawing.Size(377, 347)
        Me.grpAuditTrail.TabIndex = 10
        Me.grpAuditTrail.TabStop = False
        Me.grpAuditTrail.Text = "Audit Trail Files"
        '
        'radBetween
        '
        Me.radBetween.AutoSize = True
        Me.radBetween.Location = New System.Drawing.Point(20, 62)
        Me.radBetween.Name = "radBetween"
        Me.radBetween.Size = New System.Drawing.Size(112, 19)
        Me.radBetween.TabIndex = 8
        Me.radBetween.Text = "Select Date range"
        Me.radBetween.UseVisualStyleBackColor = True
        '
        'lblbet
        '
        Me.lblbet.AutoSize = True
        Me.lblbet.Location = New System.Drawing.Point(17, 108)
        Me.lblbet.Name = "lblbet"
        Me.lblbet.Size = New System.Drawing.Size(32, 15)
        Me.lblbet.TabIndex = 7
        Me.lblbet.Text = "From"
        '
        'lblto
        '
        Me.lblto.AutoSize = True
        Me.lblto.Location = New System.Drawing.Point(155, 108)
        Me.lblto.Name = "lblto"
        Me.lblto.Size = New System.Drawing.Size(20, 15)
        Me.lblto.TabIndex = 6
        Me.lblto.Text = "To"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CustomFormat = "MM/dd/yyyy"
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndDate.Location = New System.Drawing.Point(194, 104)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(99, 22)
        Me.dtpEndDate.TabIndex = 5
        '
        'dtpBeginDate
        '
        Me.dtpBeginDate.CustomFormat = "MM/dd/yyyy"
        Me.dtpBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpBeginDate.Location = New System.Drawing.Point(51, 104)
        Me.dtpBeginDate.Name = "dtpBeginDate"
        Me.dtpBeginDate.Size = New System.Drawing.Size(99, 22)
        Me.dtpBeginDate.TabIndex = 4
        '
        'lbldays
        '
        Me.lbldays.AutoSize = True
        Me.lbldays.Location = New System.Drawing.Point(213, 33)
        Me.lbldays.Name = "lbldays"
        Me.lbldays.Size = New System.Drawing.Size(67, 15)
        Me.lbldays.TabIndex = 3
        Me.lbldays.Text = "Days before"
        '
        'lsttxtdocs
        '
        Me.lsttxtdocs.FormattingEnabled = True
        Me.lsttxtdocs.Location = New System.Drawing.Point(19, 140)
        Me.lsttxtdocs.Name = "lsttxtdocs"
        Me.lsttxtdocs.Size = New System.Drawing.Size(341, 174)
        Me.lsttxtdocs.TabIndex = 1
        '
        'txtDays
        '
        Me.txtDays.Location = New System.Drawing.Point(127, 30)
        Me.txtDays.Name = "txtDays"
        Me.txtDays.Size = New System.Drawing.Size(70, 22)
        Me.txtDays.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(445, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 340
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnPurge
        '
        Me.btnPurge.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnPurge.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnPurge.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPurge.ForeColor = System.Drawing.Color.White
        Me.btnPurge.Location = New System.Drawing.Point(364, 8)
        Me.btnPurge.Name = "btnPurge"
        Me.btnPurge.Size = New System.Drawing.Size(75, 25)
        Me.btnPurge.TabIndex = 341
        Me.btnPurge.Text = "Purge"
        Me.btnPurge.UseVisualStyleBackColor = False
        '
        'picReporterBar
        '
        Me.picReporterBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-296, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(698, 21)
        Me.picReporterBar.TabIndex = 343
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-296, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(698, 15)
        Me.picTopBar.TabIndex = 342
        Me.picTopBar.TabStop = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.sstMain.Location = New System.Drawing.Point(0, 439)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(402, 16)
        Me.sstMain.TabIndex = 345
        Me.sstMain.Text = "StatusStrip1"
        Me.sstMain.UseWaitCursor = True
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Margin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 0)
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnPurge)
        Me.Panel1.Location = New System.Drawing.Point(-131, 399)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(698, 60)
        Me.Panel1.TabIndex = 347
        '
        'frmPurge
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(402, 455)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.grpAuditTrail)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmPurge"
        Me.Text = "Audit Trial Purge Utility"
        Me.grpAuditTrail.ResumeLayout(False)
        Me.grpAuditTrail.PerformLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sstMain.ResumeLayout(False)
        Me.sstMain.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents radDays As System.Windows.Forms.RadioButton
    Friend WithEvents grpAuditTrail As System.Windows.Forms.GroupBox
    Friend WithEvents radBetween As System.Windows.Forms.RadioButton
    Friend WithEvents lblbet As System.Windows.Forms.Label
    Friend WithEvents lblto As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpBeginDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbldays As System.Windows.Forms.Label
    Friend WithEvents lsttxtdocs As System.Windows.Forms.CheckedListBox
    Friend WithEvents txtDays As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnPurge As System.Windows.Forms.Button
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
