﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMessageDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtDummy = New System.Windows.Forms.TextBox
        Me.lblMessage = New System.Windows.Forms.Label
        Me.pnlErrorDetails = New System.Windows.Forms.Panel
        Me.radInfo = New System.Windows.Forms.RadioButton
        Me.txtInfo = New System.Windows.Forms.TextBox
        Me.txtTarget = New System.Windows.Forms.TextBox
        Me.txtStack = New System.Windows.Forms.TextBox
        Me.radStack = New System.Windows.Forms.RadioButton
        Me.radTarget = New System.Windows.Forms.RadioButton
        Me.radError = New System.Windows.Forms.RadioButton
        Me.txtError = New System.Windows.Forms.TextBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.picIcon = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.Panel
        Me.pnlErrorDetails.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PictureBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.Button3.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Gainsboro
        Me.Button3.Location = New System.Drawing.Point(276, 8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 25)
        Me.Button3.TabIndex = 51
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.Button2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Gainsboro
        Me.Button2.Location = New System.Drawing.Point(195, 8)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 25)
        Me.Button2.TabIndex = 50
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Button1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.Button1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Gainsboro
        Me.Button1.Location = New System.Drawing.Point(114, 8)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 25)
        Me.Button1.TabIndex = 49
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtDummy
        '
        Me.txtDummy.Location = New System.Drawing.Point(459, 45)
        Me.txtDummy.Multiline = True
        Me.txtDummy.Name = "txtDummy"
        Me.txtDummy.Size = New System.Drawing.Size(1, 1)
        Me.txtDummy.TabIndex = 53
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(60, 48)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(161, 15)
        Me.lblMessage.TabIndex = 54
        Me.lblMessage.Text = "Message text will appear here..."
        '
        'pnlErrorDetails
        '
        Me.pnlErrorDetails.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.pnlErrorDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlErrorDetails.Controls.Add(Me.radInfo)
        Me.pnlErrorDetails.Controls.Add(Me.txtInfo)
        Me.pnlErrorDetails.Controls.Add(Me.txtTarget)
        Me.pnlErrorDetails.Controls.Add(Me.txtStack)
        Me.pnlErrorDetails.Controls.Add(Me.radStack)
        Me.pnlErrorDetails.Controls.Add(Me.radTarget)
        Me.pnlErrorDetails.Controls.Add(Me.radError)
        Me.pnlErrorDetails.Controls.Add(Me.txtError)
        Me.pnlErrorDetails.Location = New System.Drawing.Point(12, 83)
        Me.pnlErrorDetails.Name = "pnlErrorDetails"
        Me.pnlErrorDetails.Size = New System.Drawing.Size(440, 128)
        Me.pnlErrorDetails.TabIndex = 55
        '
        'radInfo
        '
        Me.radInfo.AutoSize = True
        Me.radInfo.Location = New System.Drawing.Point(365, 90)
        Me.radInfo.Name = "radInfo"
        Me.radInfo.Size = New System.Drawing.Size(43, 19)
        Me.radInfo.TabIndex = 11
        Me.radInfo.TabStop = True
        Me.radInfo.Text = "Info"
        Me.radInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radInfo.UseVisualStyleBackColor = True
        '
        'txtInfo
        '
        Me.txtInfo.BackColor = System.Drawing.Color.MintCream
        Me.txtInfo.Location = New System.Drawing.Point(14, 89)
        Me.txtInfo.Name = "txtInfo"
        Me.txtInfo.Size = New System.Drawing.Size(338, 22)
        Me.txtInfo.TabIndex = 10
        '
        'txtTarget
        '
        Me.txtTarget.BackColor = System.Drawing.Color.MintCream
        Me.txtTarget.Location = New System.Drawing.Point(14, 64)
        Me.txtTarget.Name = "txtTarget"
        Me.txtTarget.Size = New System.Drawing.Size(338, 22)
        Me.txtTarget.TabIndex = 9
        '
        'txtStack
        '
        Me.txtStack.BackColor = System.Drawing.Color.MintCream
        Me.txtStack.Location = New System.Drawing.Point(14, 39)
        Me.txtStack.Name = "txtStack"
        Me.txtStack.Size = New System.Drawing.Size(338, 22)
        Me.txtStack.TabIndex = 8
        '
        'radStack
        '
        Me.radStack.AutoSize = True
        Me.radStack.Location = New System.Drawing.Point(365, 40)
        Me.radStack.Name = "radStack"
        Me.radStack.Size = New System.Drawing.Size(53, 19)
        Me.radStack.TabIndex = 6
        Me.radStack.TabStop = True
        Me.radStack.Text = "Stack"
        Me.radStack.UseVisualStyleBackColor = True
        '
        'radTarget
        '
        Me.radTarget.AutoSize = True
        Me.radTarget.Location = New System.Drawing.Point(365, 65)
        Me.radTarget.Name = "radTarget"
        Me.radTarget.Size = New System.Drawing.Size(57, 19)
        Me.radTarget.TabIndex = 7
        Me.radTarget.TabStop = True
        Me.radTarget.Text = "Target"
        Me.radTarget.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radTarget.UseVisualStyleBackColor = True
        '
        'radError
        '
        Me.radError.AutoSize = True
        Me.radError.Location = New System.Drawing.Point(365, 15)
        Me.radError.Name = "radError"
        Me.radError.Size = New System.Drawing.Size(50, 19)
        Me.radError.TabIndex = 4
        Me.radError.TabStop = True
        Me.radError.Text = "Error"
        Me.radError.UseVisualStyleBackColor = True
        '
        'txtError
        '
        Me.txtError.BackColor = System.Drawing.Color.MintCream
        Me.txtError.Location = New System.Drawing.Point(14, 14)
        Me.txtError.Name = "txtError"
        Me.txtError.Size = New System.Drawing.Size(338, 22)
        Me.txtError.TabIndex = 0
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PictureBox2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox2.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(465, 30)
        Me.PictureBox2.TabIndex = 44
        Me.PictureBox2.TabStop = False
        '
        'picIcon
        '
        Me.picIcon.Location = New System.Drawing.Point(12, 36)
        Me.picIcon.Name = "picIcon"
        Me.picIcon.Size = New System.Drawing.Size(37, 40)
        Me.picIcon.TabIndex = 42
        Me.picIcon.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.PictureBox1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox1.Controls.Add(Me.Button1)
        Me.PictureBox1.Controls.Add(Me.Button2)
        Me.PictureBox1.Controls.Add(Me.Button3)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 199)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(465, 45)
        Me.PictureBox1.TabIndex = 56
        '
        'frmMessageDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.MintCream
        Me.ClientSize = New System.Drawing.Size(464, 239)
        Me.Controls.Add(Me.pnlErrorDetails)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.txtDummy)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.picIcon)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMessageDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlErrorDetails.ResumeLayout(False)
        Me.pnlErrorDetails.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PictureBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picIcon As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtDummy As System.Windows.Forms.TextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents pnlErrorDetails As System.Windows.Forms.Panel
    Friend WithEvents txtTarget As System.Windows.Forms.TextBox
    Friend WithEvents txtStack As System.Windows.Forms.TextBox
    Friend WithEvents radStack As System.Windows.Forms.RadioButton
    Friend WithEvents radTarget As System.Windows.Forms.RadioButton
    Friend WithEvents radError As System.Windows.Forms.RadioButton
    Friend WithEvents txtError As System.Windows.Forms.TextBox
    Friend WithEvents txtInfo As System.Windows.Forms.TextBox
    Friend WithEvents radInfo As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.Panel

End Class
