﻿
#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.IO
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : 
' Author        : 
' Created Date  : 
' Description   : 
'                 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmPenaltyShootout

#Region " Constants & Variables "
    Private m_objUtility As New clsUtility

    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objModule1PBP As New frmModule1PBP
    Private m_objPenaltyShootout As New STATS.SoccerBL.clsPenaltyShootout
    Private DsShootoutInputs As DataSet
    Private m_SeqNum As Decimal
    Private m_EditEventID As Integer
    Private m_dsTempShootout As New DataSet
    Private m_dsScores As New DataSet
    Private m_CurrLocalSeqNo As Integer
    Private m_ShootoutRound As Integer
    Private m_ScoreEdit As Boolean = False
    Private m_TeamID As Integer
    Private m_TeamIDtoValidate As Integer
    Private m_blnAutoFired As Boolean = False       'to check if selectedindexchanged event of listboxes was autofired
    Private m_HomeScoreSO As Integer = 0
    Private m_AwayScoreSO As Integer = 0
    Private m_dsShootoutData As DataSet
    Private MessageDialog As New frmMessageDialog

    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const SECONDEXTRA As Integer = 7200
    Private lsupport As New languagesupport

    Private m_SequenceNo As Decimal
    Private m_IsEditMode As Boolean = False
    Private m_formClosed As Boolean = False

    Dim strmessage As String = "Select the Sub Type for a Missed Shot!"
    Dim strmessage1 As String = "Select a Player to proceed!"
    Dim strmessage2 As String = "Select Scored OR Missed to proceed!"
    Dim strmessage3 As String = "Wrong team selected for Shootout!"
    Dim strmessage4 As String = "Please select mandatory data points (Team, Player, Result)!"
    Dim strmessage5 As String = "Are you Sure to delete this record ?"
    Dim strmessage6 As String = "Do you want to END the game?"
    Dim strmessage7 As String = "Is the Shoot-out finished"
    Dim strmessage8 As String = "Cancel/Save the current work first!"
    Dim strmessage9 As String = "Complete or cancel the current operation first!"
    Dim strmessage10 As String = "Game is over, you are not allowed to enter a new event"
    Dim strmessage11 As String = "if you want to enter, please delete the End Game event and continue."
    Dim strmessage12 As String = "Shootout"
    Dim strmessagehead As String = "Close"


#End Region

#Region " Event handlers "
    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Public Sub New(ByVal IsEditMode As Boolean, ByVal SequenceNumber As Decimal)
        m_SequenceNo = SequenceNumber
        m_IsEditMode = IsEditMode
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub


    Private Sub frmPenaltyShootout_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)

            'If m_objGameDetails.IsEndGame = False Then
            '    If essageDialog.Show("Is the Shoot-out finished  [ " & m_objGameDetails.HomeTeamAbbrev & ": " & m_HomeScoreSO & ", " & m_objGameDetails.AwayTeamAbbrev & ": " & m_AwayScoreSO & " ] ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
            '        If essageDialog.Show("Do you want to END the game?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
            '            m_objGameDetails.IsEndGame = True
            '            e.Cancel = False
            '        Else
            '            m_objGameDetails.IsEndGame = False
            '            e.Cancel = False
            '        End If
            '    Else
            '        m_objGameDetails.IsEndGame = False
            '        e.Cancel = True
            '    End If
            'Else
            '    e.Cancel = False
            'End If


            If m_objGameDetails.IsEditMode Then
                MessageDialog.Show(strmessage8, strmessagehead, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If m_formClosed = False Then
                If MessageDialog.Show(strmessage7 + " [ " & m_objGameDetails.HomeTeamAbbrev & ": " & m_HomeScoreSO & ", " & m_objGameDetails.AwayTeamAbbrev & ": " & m_AwayScoreSO & " ] ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                    If MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                        m_objGameDetails.IsEndGame = True
                    Else
                        m_objGameDetails.IsEndGame = False
                    End If


                    Dim dsPBP As New DataSet
                    dsPBP = m_objPenaltyShootout.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                    Dim dr() As DataRow = dsPBP.Tables(0).Select(" EVENT_CODE_ID IN(30,31) ")
                    If dr.Length > 0 Then
                        If dr(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
                            m_objGameDetails.IsScoreWindow = True
                            m_objGameDetails.IsDetailedWindow = False
                        Else
                            m_objGameDetails.IsDetailedWindow = True
                            m_objGameDetails.IsScoreWindow = False
                        End If
                    Else
                        m_objGameDetails.IsDetailedWindow = False
                        m_objGameDetails.IsScoreWindow = False
                    End If

                    'Me.Close()
                    m_formClosed = True
                Else
                    m_formClosed = False
                End If

            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmPenaltyShootout_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPenaltyShootout", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Dim TEMLENINT As Integer
            Me.Refresh()
            ' BuildReports()
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                TEMLENINT = Label1.Text.Length
                Label1.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label1.Text)
                linebreak(Label1, TEMLENINT)
                Label2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label2.Text)
                Label3.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label3.Text)
                Label4.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label4.Text)
                Label5.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label5.Text)
                Label6.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label6.Text)
                Label7.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label7.Text)
                Label8.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label8.Text)
                Label9.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label9.Text)
                Label10.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label10.Text)
                Label12.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label12.Text)

                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Sequence Number")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Seq")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Score")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Result")
                Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Type")
                Dim g10 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Player")
                Dim g11 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Goal Zone")
                Dim g12 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Keeper Zone")


                lvPenaltyShootout.Columns(0).Text = g4
                lvPenaltyShootout.Columns(1).Text = g5
                lvPenaltyShootout.Columns(2).Text = g6
                lvPenaltyShootout.Columns(3).Text = g7
                lvPenaltyShootout.Columns(4).Text = g8
                lvPenaltyShootout.Columns(6).Text = g9
                lvPenaltyShootout.Columns(7).Text = g10
                lvPenaltyShootout.Columns(8).Text = g11
                'lvPenaltyShootout.Columns(9).Text = g12
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)
                strmessage8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage8)
                strmessage9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage9)

            End If

            Call SwitchSlides()
            Call FillTeamLogos()
            Call FillTeamScores()
            Call FillShootoutInputs()
            Call DisableInputs(True)
            Call DoCancel()
            Call DisplayInListview()
            Call SetSequence()
            'If m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2 Then
            If m_objGameDetails.ModuleID = 2 Then
                lstType.Enabled = False
                lstGoalZone.Enabled = False
                lstKeeperZone.Enabled = False
                lstSubType.Enabled = False
            End If
            m_objGameDetails.IsEndGame = False
            btnSave.Enabled = True

            'Dim dsPBP As New DataSet
            'dsPBP = m_objPenaltyShootout.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

            'Dim dr() As DataRow = dsPBP.Tables(0).Select(" EVENT_CODE_ID = 30")
            'If dr.Length > 0 Then
            '    If dr(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
            '        m_objGameDetails.IsScoreWindow = True
            '    Else
            '        m_objGameDetails.IsDetailedWindow = True
            '    End If
            'Else
            '    m_objGameDetails.IsDetailedWindow = False
            '    m_objGameDetails.IsScoreWindow = False
            'End If

            If m_IsEditMode = True Then
                Me.Refresh()
                Dim Lstview As ListViewItem = lvPenaltyShootout.FindItemWithText(CStr(m_SequenceNo))
                lvPenaltyShootout.Items(Lstview.Index).Selected = True
                lvPenaltyShootout.Select()
                lvPenaltyShootout.EnsureVisible(Lstview.Index)
                m_dsTempShootout = frmMain.AddPBPColumns()
                m_objGameDetails.IsEditMode = True
                SetStateEditReplace(m_SequenceNo)
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                btnSave.Enabled = True
                lstTeam.Enabled = False
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub
    Public Sub linebreak(ByVal lbtemp As Label, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub
    Private Sub lvPenaltyShootout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvPenaltyShootout.Click
        Try
            If m_objGameDetails.IsEditMode Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            Else
                If lvPenaltyShootout.SelectedItems.Count > 0 Then
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    'btnSave.Enabled = False
                    'btnInsert.Enabled = True
                Else
                    btnSave.Enabled = True
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lvPenaltyShootout_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvPenaltyShootout.DoubleClick
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "lvPenSht Dbl Click", Nothing, 1, 0)
            If lvPenaltyShootout.SelectedItems.Count > 0 Then
                Dim SequenceNo As Decimal

                m_objGameDetails.IsEditMode = True
                SequenceNo = CDec(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(0).Text)
                'EventID = CInt(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(10).Text)
                'TeamID = CInt(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(3).Text)
                m_dsTempShootout = frmMain.AddPBPColumns()

                SetStateEditReplace(SequenceNo)
                'Set the values
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                btnSave.Enabled = True
                lstTeam.Enabled = False
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            HighlightShooutoutData()
        End Try
    End Sub

    Private Sub UdcSoccerGoal1_USGClick(ByVal sender As Object, ByVal e As USGEventArgs) Handles UdcSoccerGoal1.USGClick
        Try
            m_blnAutoFired = True
            'select the listbox item corresponding to the selected zone in user control
            lstGoalZone.ClearSelected()
            If e.USGZone <> Nothing Then
                For Each dr As DataRowView In lstGoalZone.Items
                    If dr.Item("GOALZONE_ID").ToString.Replace("/", " ").Trim = e.USGZone.Trim Then
                        'If dr.Item("GOALZONE").ToString.Replace("/", " ").Trim = e.USGZone.Trim Then
                        lstGoalZone.SelectedValue = dr.Item("GOALZONE_ID")
                        Exit Try
                    End If
                Next
                'clear the listbox selection
                lstGoalZone.SelectedIndex = -1

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            m_blnAutoFired = False
        End Try
    End Sub

    Private Sub UdcSoccerGoal2_USGClick(ByVal sender As Object, ByVal e As USGEventArgs) Handles UdcSoccerGoal2.USGClick
        Try
            m_blnAutoFired = True
            'select the listbox item corresponding to the selected zone in user control
            lstKeeperZone.ClearSelected()
            ' Dim SS As String = e.USGZone.ToString
            If e.USGZone <> Nothing Then
                For Each dr As DataRowView In lstKeeperZone.Items
                    If dr.Item("KEEPERZONE_ID").ToString.Replace("/", " ").Trim = e.USGZone.Trim Then
                        lstKeeperZone.SelectedValue = dr.Item("KEEPERZONE_ID")
                        Exit Try
                    End If
                Next

                'clear the listbox selection
                lstKeeperZone.SelectedIndex = -1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            m_blnAutoFired = False
        End Try
    End Sub

    Private Sub UdcSoccerGoal1_USGMouseMove(ByVal sender As Object, ByVal e As USGEventArgs) Handles UdcSoccerGoal1.USGMouseMove
        Try
            'display zone on mouse move
            Dim drsZone() As DataRow
            drsZone = DsShootoutInputs.Tables("Goal_Zone").Select("GOALZONE_ID = " & e.USGZone & "")
            If drsZone.Length > 0 Then
                lblGoalZone.Text = drsZone(0).ItemArray(1).ToString
                'lblGoalZone.Text = e.USGZone
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerGoal2_USGMouseMove(ByVal sender As Object, ByVal e As USGEventArgs) Handles UdcSoccerGoal2.USGMouseMove
        Try
            Dim drsZone() As DataRow
            drsZone = DsShootoutInputs.Tables("Keeper_Zone").Select("KEEPERZONE_ID = " & e.USGZone & "")
            If drsZone.Length > 0 Then
                lblKeeperZone.Text = drsZone(0).ItemArray(1).ToString
                'lblKeeperZone.Text = e.USGZone
            End If

            'display zone text on mouse move

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerGoal1_USGMouseLeave() Handles UdcSoccerGoal1.USGMouseLeave
        Try
            'clear zone text when mouse leaves user control
            lblGoalZone.Text = ""
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerGoal2_USGMouseLeave() Handles UdcSoccerGoal2.USGMouseLeave
        Try
            'clear zone text when mouse leaves user control
            lblKeeperZone.Text = ""
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lstGoalZone_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstGoalZone.SelectedIndexChanged
        Try
           m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lstTeam.Text, Nothing, 1, 0)
        
            'set the zone rectangle in user control corresponding to selected listbox item
            If Not m_blnAutoFired Then
                'UdcSoccerGoal1.USGSetZone(lstGoalZone.Text.Replace("/", " "))
                'Dim ss As String = lstGoalZone.Text.Replace("/", " ")
                UdcSoccerGoal1.USGSetZone(CStr(lstGoalZone.SelectedValue), "GOAL Zone")
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            HighlightShooutoutData()
        End Try
    End Sub

    Private Sub lstKeeperZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstKeeperZone.SelectedIndexChanged
        Try
            'set the zone rectangle in user control corresponding to selected listbox item
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListBoxClicked, lstKeeperZone.Text, Nothing, 1, 0)
            If Not m_blnAutoFired Then
                'UdcSoccerGoal2.USGSetZone(lstKeeperZone.Text.Replace("/", " "))
                UdcSoccerGoal2.USGSetZone(CStr(lstKeeperZone.SelectedValue), "Kepper Zone")
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            HighlightShooutoutData()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            If m_objGameDetails.IsEditMode Then
                MessageDialog.Show(strmessage8, "Close", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If MessageDialog.Show(strmessage7 + " [ " & m_objGameDetails.HomeTeamAbbrev & ": " & m_HomeScoreSO & ", " & m_objGameDetails.AwayTeamAbbrev & ": " & m_AwayScoreSO & " ] ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then

                'Dim dsEndGameCnt As New DataSet
                'dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                'If dsEndGameCnt.Tables.Count > 0 Then
                '    If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                '        Dim intEndGame As Integer = CInt(dsEndGameCnt.Tables(0).Rows(0).Item("endGameCount"))
                '        If intEndGame <= 0 Then
                '            If MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                '                m_objGameDetails.IsEndGame = True
                '                If m_objGameDetails.ModuleID = 1 Then
                '                    If (essageDialog.Show(" Do you want to enter the player ratings ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                '                        m_objGameDetails.IsEndGame = True
                '                        Dim objfrmTeamSetup As New frmTeamSetup("Home")
                '                        If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                '                            If (essageDialog.Show(" Do you want to enter the match rating ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                '                                Dim objfrmgameSetup As New frmGameSetup
                '                                objfrmgameSetup.ShowDialog()
                '                                m_objGameDetails.IsEndGame = True
                '                            End If
                '                        End If
                '                    Else
                '                        m_objGameDetails.IsEndGame = True
                '                        If (essageDialog.Show(" Do you want to enter the match rating ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                '                            Dim objfrmgameSetup As New frmGameSetup
                '                            objfrmgameSetup.ShowDialog()
                '                        End If
                '                    End If

                '                End If
                '                frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                '            Else
                '                m_objGameDetails.IsEndGame = False
                '            End If
                '        End If
                '    End If
                'End If

                Dim dsEndGameCnt As New DataSet
                dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                If dsEndGameCnt.Tables.Count > 0 Then
                    If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                        Dim intEndGame As Integer = CInt(dsEndGameCnt.Tables(0).Rows(0).Item("endGameCount"))
                        If intEndGame <= 0 Then
                            If MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                                m_objGameDetails.IsEndGame = True
                            Else
                                m_objGameDetails.IsEndGame = False
                            End If
                        Else
                            m_objGameDetails.IsEndGame = True
                        End If
                    End If
                End If

                Dim dsPBP As New DataSet
                dsPBP = m_objPenaltyShootout.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                Dim dr() As DataRow = dsPBP.Tables(0).Select(" EVENT_CODE_ID IN(30,31) ")
                If dr.Length > 0 Then
                    If dr(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
                        m_objGameDetails.IsScoreWindow = True
                        m_objGameDetails.IsDetailedWindow = False
                    Else
                        m_objGameDetails.IsDetailedWindow = True
                        m_objGameDetails.IsScoreWindow = False
                    End If
                Else
                    m_objGameDetails.IsDetailedWindow = False
                    m_objGameDetails.IsScoreWindow = False
                End If
                m_formClosed = True
                Me.Close()
                'Else
                '    Me.Close()
            Else
                m_formClosed = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lstTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstTeam.SelectedIndexChanged
        Try
            Dim dsShootOutPBPData As New DataSet
            Dim strDisplayUniform As String = ""
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lstTeam.Text, Nothing, 1, 0)
            If m_blnAutoFired = False And lstTeam.SelectedIndex <> -1 Then
                If ShootoutAllowed() = False And m_objGameDetails.IsEditMode = False And m_objGameDetails.CoverageLevel = 1 Then
                    MessageDialog.Show(strmessage3, "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    lstTeam.SelectedIndex = -1
                    Exit Sub
                Else
                    Dim m_DsPlayers As New DataSet
                    m_DsPlayers.Tables.Add()
                    m_DsPlayers.Tables(0).Columns.Add("PLAYER")
                    m_DsPlayers.Tables(0).Columns.Add("PLAYER_ID")

                    If m_objGameDetails.CoverageLevel = 1 Or m_objGameDetails.CoverageLevel = 2 Then

                        '''''
                        Dim drs() As DataRow
                        If CInt(lstTeam.SelectedValue) = m_objGameDetails.HomeTeamID Then
                            drs = m_objGameDetails.TeamRosters.Tables(0).Select("TEAM_ID =" & CInt(lstTeam.SelectedValue))
                        Else
                            drs = m_objGameDetails.TeamRosters.Tables(1).Select("TEAM_ID =" & CInt(lstTeam.SelectedValue))
                        End If
                        If drs.Length > 0 Then
                            Dim TeamId As Integer = CInt(drs(0).Item("TEAM_ID").ToString)
                            For Each drPlayers As DataRow In drs
                                Dim drPlayerData As DataRow = m_DsPlayers.Tables(0).NewRow()
                                strDisplayUniform = CStr(drPlayers.Item("DISPLAY_UNIFORM_NUMBER").ToString)
                                If strDisplayUniform = "" Then
                                    drPlayerData.Item("PLAYER") = CStr(drPlayers.Item("PLAYER").ToString)
                                Else
                                    drPlayerData.Item("PLAYER") = strDisplayUniform & " - " & CStr(drPlayers.Item("PLAYER").ToString)
                                End If
                                'drPlayerData.Item("PLAYER") = drPlayers.Item("PLAYER")
                                drPlayerData.Item("PLAYER_ID") = drPlayers.Item("PLAYER_ID")
                                m_DsPlayers.Tables(0).Rows.Add(drPlayerData)

                            Next
                            clsUtility.LoadControl(lstPlayer, m_DsPlayers.Tables(0), "PLAYER", "PLAYER_ID")
                        End If
                        '''''
                    Else
                        Dim drs() As DataRow
                        If CInt(lstTeam.SelectedValue) = m_objGameDetails.HomeTeamID Then
                            drs = m_objTeamSetup.RosterInfo.Tables("HomeCurrent").Select("TEAM_ID =" & CInt(lstTeam.SelectedValue) & " AND STARTING_POSITION IS NOT NULL")
                        Else
                            drs = m_objTeamSetup.RosterInfo.Tables("AwayCurrent").Select("TEAM_ID =" & CInt(lstTeam.SelectedValue) & " AND STARTING_POSITION IS NOT NULL")
                        End If
                        If drs.Length > 0 Then
                            Dim TeamId As Integer = CInt(drs(0).Item("TEAM_ID").ToString)
                            For Each drPlayers As DataRow In drs
                                Dim drPlayerData As DataRow = m_DsPlayers.Tables(0).NewRow()
                                strDisplayUniform = CStr(drPlayers.Item("DISPLAY_UNIFORM_NUMBER").ToString)
                                If strDisplayUniform = "" Then
                                    drPlayerData.Item("PLAYER") = CStr(drPlayers.Item("PLAYER").ToString)
                                Else
                                    drPlayerData.Item("PLAYER") = strDisplayUniform & " - " & CStr(drPlayers.Item("PLAYER").ToString)
                                End If
                                'drPlayerData.Item("PLAYER") = drPlayers.Item("PLAYER")
                                drPlayerData.Item("PLAYER_ID") = drPlayers.Item("PLAYER_ID")
                                m_DsPlayers.Tables(0).Rows.Add(drPlayerData)

                            Next
                            clsUtility.LoadControl(lstPlayer, m_DsPlayers.Tables(0), "PLAYER", "PLAYER_ID")
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            HighlightShooutoutData()
        End Try
    End Sub


    Private Sub lstResult_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstResult.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lstResult.Text, Nothing, 1, 0)
            If m_blnAutoFired = False Then
                'If m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2 Then
                If m_objGameDetails.ModuleID = 2 Then

                Else
                    If CInt(lstResult.SelectedValue) = 1 Then
                        lstGoalZone.Enabled = True
                        lstKeeperZone.Enabled = True
                        UdcSoccerGoal1.Enabled = True
                        lstSubType.ClearSelected()
                        lstSubType.Enabled = False
                    ElseIf CInt(lstResult.SelectedValue) = 2 Then
                        lstGoalZone.ClearSelected()
                        lstGoalZone.Enabled = False
                        UdcSoccerGoal1.Enabled = False
                        lstKeeperZone.Enabled = True
                        lstSubType.Enabled = True
                    End If
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            HighlightShooutoutData()
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            DoCancel()
            DisableInputs(True)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub HighlightShooutoutData()
        Try
            If m_objGameDetails.IsEditMode = True Then
                If lvPenaltyShootout.SelectedItems.Count > 0 Then
                    lvPenaltyShootout.Items(lvPenaltyShootout.SelectedItems(0).Index).Selected = True
                    lvPenaltyShootout.Select()
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, Nothing, 1, 0)
            If lvPenaltyShootout.SelectedItems.Count > 0 Then
                Dim SequenceNo As Decimal
                Dim EventID As Integer
                Dim dsDeleteShootoutData As DataSet

                btnEdit.Enabled = False
                'btnRepl.Enabled = False
                'btnInsert.Enabled = False
                btnDelete.Enabled = False

                SequenceNo = CDec(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(0).Text)

                dsDeleteShootoutData = m_objPenaltyShootout.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "EQUAL")
                EventID = CInt(dsDeleteShootoutData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                'TeamID = CInt(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(3).Text)
                'dsDeleteShootoutData = m_objPenaltyShootout.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "GREATER")

                If MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                    If EventID = clsGameDetails.SHOOTOUT_GOAL Then
                        'm_objPenaltyShootout.DeleteScoreEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo)
                        'dsDeleteShootoutData = m_objPenaltyShootout.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        'DisplayInListview()
                        m_objPenaltyShootout.DeletePBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, SequenceNo, m_objGameDetails.languageid)

                        m_dsScores.Tables.Clear()
                        m_dsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        GetTeamScores(m_dsScores)

                        If CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                            m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                            'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                            'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                        Else
                            m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                            'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                            'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                        End If
                        m_HomeScoreSO = 0
                        m_AwayScoreSO = 0
                        GetShootoutScore(m_dsScores)
                        'm_objGameDetails.HomeScore = m_HomeScoreSO
                        'm_objGameDetails.AwayScore = m_AwayScoreSO
                        'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
                        lblScoreHome.Text = CStr(m_HomeScoreSO)
                        lblScoreAway.Text = CStr(m_AwayScoreSO)
                    ElseIf EventID = clsGameDetails.SHOOTOUT_MISSED Or EventID = clsGameDetails.SHOOTOUT_SAVE Then
                        m_objPenaltyShootout.DeletePBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, SequenceNo, m_objGameDetails.languageid)
                        'dsPBPData = m_objPenaltyShootout.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    End If
                    DisplayInListview()
                    SetSequence() 'setting the sequence after deleteing an event
                    m_objPenaltyShootout.UpdateTeamGamePenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                End If
                DoCancel()
            End If
        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, Nothing, 1, 0)
            If lvPenaltyShootout.SelectedItems.Count > 0 Then
                Dim SequenceNo As Decimal

                m_objGameDetails.IsEditMode = True
                SequenceNo = CDec(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(0).Text)
                'EventID = CInt(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(10).Text)
                'TeamID = CInt(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(3).Text)
                m_dsTempShootout = frmMain.AddPBPColumns()

                SetStateEditReplace(SequenceNo)
                'Set the values
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                btnSave.Enabled = True
                lstTeam.Enabled = False
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            HighlightShooutoutData()
        End Try
    End Sub

    Private Sub lstSubType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSubType.SelectedIndexChanged
        Try

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListBoxClicked, lstSubType.Text, Nothing, 1, 0)

            If Not m_blnAutoFired Then
                HighlightShooutoutData()
                If CInt(lstSubType.SelectedValue) = 1 Then
                    lstKeeperZone.SelectedValue = 1
                End If
                If CInt(lstSubType.SelectedValue) = 2 Then
                    lstKeeperZone.SelectedValue = 2
                End If
                If CInt(lstSubType.SelectedValue) = 3 Then
                    lstKeeperZone.SelectedValue = 3
                End If
                If CInt(lstSubType.SelectedValue) = 4 Then
                    lstKeeperZone.SelectedValue = 6
                End If
                If CInt(lstSubType.SelectedValue) = 5 Then
                    lstKeeperZone.SelectedValue = 5
                End If
                If CInt(lstSubType.SelectedValue) = 6 Then
                    lstKeeperZone.SelectedValue = 4
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, Nothing, 1, 0)
            'validations
            If lstTeam.SelectedIndex = -1 Or lstPlayer.SelectedIndex = -1 Or lstResult.SelectedIndex = -1 Then
                MessageDialog.Show(strmessage4, "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If ShootoutAllowed() = False And m_objGameDetails.IsEditMode = False And m_objGameDetails.CoverageLevel = 1 Then
                MessageDialog.Show(strmessage3, "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If lstResult.SelectedIndex = -1 Then
                MessageDialog.Show(strmessage2, "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If lstPlayer.SelectedIndex = -1 Then
                MessageDialog.Show(strmessage1, "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If m_objGameDetails.CoverageLevel > 3 Then
                If lstSubType.Enabled = True Then
                    If lstSubType.SelectedIndex = -1 Then
                        MessageDialog.Show(strmessage, "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Exit Sub
                    End If
                End If
            End If


            'WE SHOULD NOT ALLOW TO ENTER TO NEW EVENT ONCE THE GAME IS OVER.
            If m_objGameDetails.IsEditMode = False Then
                Dim dsEndGameCnt As New DataSet
                dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                If dsEndGameCnt.Tables.Count > 0 Then
                    If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                        Dim intEndGame As Integer = CInt(dsEndGameCnt.Tables(0).Rows(0).Item("endGameCount"))
                        If intEndGame > 0 Then
                            MessageDialog.Show(strmessage10 + "," & vbNewLine & strmessage11, strmessage12, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            DoCancel()
                            Exit Sub
                        End If
                    End If
                End If
            End If

            If m_objGameDetails.IsEditMode Then
                m_objGameDetails.OffensiveTeamID = CInt(lstTeam.SelectedValue)
            End If


            Call AddPlayByPlayData()
            Call InsertPlaybyPlayData()
            m_objPenaltyShootout.UpdateTeamGamePenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)

            Call SetScoresInControls()
            Call DoCancel()
            Call DisableInputs(True)

            Call DisplayInListview()
            Call SetSequence()
            m_objModule1PBP.DisplayPBP()
            'Select next team
            lstTeam.SelectedIndex = -1
            Dim m_dsShootoutData As DataSet
            m_dsShootoutData = m_objPenaltyShootout.GetShootoutPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsShootoutData.Tables(0).Rows.Count > 0 Then

                If m_objGameDetails.HomeTeamID = CInt(m_dsShootoutData.Tables(0).Rows(m_dsShootoutData.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                    lstTeam.SelectedValue = m_objGameDetails.AwayTeamID
                Else
                    lstTeam.SelectedValue = m_objGameDetails.HomeTeamID
                End If
            End If
            m_objGameDetails.IsDetailedWindow = True
            btnSave.Enabled = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " User Defined Functions"


    Private Sub AddPlayByPlayData()
        Try
            Dim drPBPData As DataRow
            Dim drPBPEditData As DataRow
            Dim dtTimeDiff As DateTime
            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()

            If m_objGameDetails.IsEditMode Then
                drPBPEditData = m_dsTempShootout.Tables(0).Rows(0)
                drPBPData("GAME_CODE") = m_objGameDetails.GameCode
                drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drPBPData("UNIQUE_ID") = GetUniqueID()                ''
                drPBPData("SEQUENCE_NUMBER") = CDec(drPBPEditData("SEQUENCE_NUMBER"))
                drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
                'drPBPData("SYSTEM_TIME") = Format(Date.Now, "yyyy-MM-dd hh:mm:ss tt")

                dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

                'drPBPData("ONFIELD_ID") = GetOnfieldvalue(CInt(drPBPData("PERIOD")))
                drPBPData("ONFIELD_ID") = DBNull.Value
                'drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(Dt, DateTimeHelper.OutputFormat.CurrentFormat)
                drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                drPBPData("OPTICAL_TIMESTAMP") = DBNull.Value
                drPBPData("RECORD_EDITED") = "N"
                drPBPData("EDIT_UID") = drPBPEditData("UNIQUE_ID")
                drPBPData("PBP_STRING") = DBNull.Value
                drPBPData("MULTILINGUAL_PBP_STRING") = DBNull.Value
                drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
                drPBPData("PROCESSED") = "N"
                drPBPData("CONTINUATION") = "F"
                Dim DsLastEventDetails As DataSet
                DsLastEventDetails = m_objModule1PBP.getLastEvent()
                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED"))

                'drPBPData("TIME_ELAPSED") = CStr(getTimeElapsed())
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    drPBPData("DEMO_DATA") = "Y"
                Else
                    drPBPData("DEMO_DATA") = "N"
                End If
                drPBPData("SHOOTOUT_ROUND") = m_ShootoutRound
                drPBPData("OFFENSIVE_PLAYER_ID") = IIf(CInt(lstPlayer.SelectedValue) = 0, DBNull.Value, CInt(lstPlayer.SelectedValue))
                drPBPData("SHOT_DESC") = IIf(CInt(lstType.SelectedValue) = 0, DBNull.Value, CInt(lstType.SelectedValue))



                If lstGoalZone.Enabled = True Then
                    drPBPData("GOALZONE_ID") = IIf(CInt(lstGoalZone.SelectedValue) = 0, DBNull.Value, CInt(lstGoalZone.SelectedValue))
                    drPBPData("KEEPERZONE_ID") = IIf(CInt(lstKeeperZone.SelectedValue) = 0, DBNull.Value, CInt(lstKeeperZone.SelectedValue))
                    ''
                    If m_objGameDetails.ModuleID = 2 Then
                        If CInt(lstResult.SelectedValue) = 1 Then
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                        Else
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                        End If
                    Else
                        drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                    End If
                    ''

                Else
                    drPBPData("KEEPERZONE_ID") = IIf(CInt(lstKeeperZone.SelectedValue) = 0, DBNull.Value, CInt(lstKeeperZone.SelectedValue))
                    drPBPData("SHOT_RESULT") = IIf(CInt(lstSubType.SelectedValue) = 0, DBNull.Value, CInt(lstSubType.SelectedValue))
                    If m_objGameDetails.CoverageLevel > 3 Then
                        If IsDBNull(drPBPData("SHOT_RESULT")) = False Then
                            If CInt(drPBPData("SHOT_RESULT")) > 0 And CInt(drPBPData("SHOT_RESULT")) < 7 Then
                                drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_SAVE
                            Else
                                drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                            End If
                        End If
                    ElseIf m_objGameDetails.ModuleID = 2 Then
                        If CInt(lstResult.SelectedValue) = 1 Then
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                        Else
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                        End If
                    Else
                        drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                    End If
                End If

                drPBPData("TEAM_ID") = lstTeam.SelectedValue
                ''SCORE HAS TO BE HANDLED SEPARATELY
                ''Different Scenarios
                ''  Off Team TO Off Team
                ''Goal to Goal
                ''Goal o Miss
                ''Miss to Goal
                ''Miss to Miss
                ''Off Team Opp Team
                ''Goal to Goal
                ''Goal to Miss
                ''Miss to Goal
                ''Miss to Miss
                Dim PrevEventID As Integer
                Dim NextEventID As Integer
                Dim PrevEventGoal As Boolean = False
                Dim NextEventGoal As Boolean = False

                If m_objGameDetails.IsEditMode Then
                    PrevEventID = m_EditEventID
                    NextEventID = CInt(drPBPData("EVENT_CODE_ID"))
                    If PrevEventID = clsGameDetails.SHOOTOUT_GOAL Then
                        PrevEventGoal = True
                    Else
                        PrevEventGoal = False
                    End If
                    ''
                    If NextEventID = clsGameDetails.SHOOTOUT_GOAL Then
                        NextEventGoal = True
                    Else
                        NextEventGoal = False
                    End If

                    If NextEventID = clsGameDetails.SHOOTOUT_GOAL Then
                        If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                            m_objGameDetails.HomeScore = m_objGameDetails.HomeScore
                        Else
                            Dim tempHomeScore As Integer
                            tempHomeScore = m_objGameDetails.HomeScore
                            m_objGameDetails.HomeScore = m_objGameDetails.AwayScore
                            m_objGameDetails.AwayScore = tempHomeScore
                        End If
                    Else
                        If m_objGameDetails.OffensiveTeamID <> m_objGameDetails.HomeTeamID Then
                            Dim tempHomeScore As Integer
                            tempHomeScore = m_objGameDetails.HomeScore
                            m_objGameDetails.HomeScore = m_objGameDetails.AwayScore
                            m_objGameDetails.AwayScore = tempHomeScore
                        End If
                    End If
                End If
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                m_SeqNum = CDec(drPBPEditData("SEQUENCE_NUMBER"))
                If PrevEventGoal = True And NextEventGoal = True And m_TeamID <> m_objGameDetails.OffensiveTeamID Then
                    m_ScoreEdit = True
                    '    m_decInsertSeqNum = decSeqNo
                    '    dsPBP = m_objModule1PBP.UpdatePBP(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, decSeqNo)


                    '    m_objModule1PBP.EditScoreEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, decSeqNo)

                    '    '' Set correct Score now
                    '    m_dsScores.Tables.Clear()
                    '    m_dsScores = m_objModule1PBP.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    '    GetTeamScores(m_dsScores)

                    '    If CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                    '        m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    '        m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    '        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    '        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    '    Else
                    '        m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    '        m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    '        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    '        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    '    End If
                    'Else
                    '    dsPBP = m_objModule1PBP.UpdatePBP(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, decSeqNo)
                End If
            Else
                drPBPData("GAME_CODE") = m_objGameDetails.GameCode
                drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drPBPData("UNIQUE_ID") = GetUniqueID()
                drPBPData("SEQUENCE_NUMBER") = Convert.ToDecimal(drPBPData("UNIQUE_ID"))
                drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
                'drPBPData("SYSTEM_TIME") = Format(Date.Now, "yyyy-MM-dd hh:mm:ss tt")

                dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
                'drPBPData("ONFIELD_ID") = GetOnfieldvalue(CInt(drPBPData("PERIOD")))
                drPBPData("ONFIELD_ID") = DBNull.Value
                'drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(Dt, DateTimeHelper.OutputFormat.CurrentFormat)
                drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                drPBPData("OPTICAL_TIMESTAMP") = DBNull.Value
                drPBPData("RECORD_EDITED") = "N"
                drPBPData("EDIT_UID") = 0
                drPBPData("PBP_STRING") = DBNull.Value
                drPBPData("MULTILINGUAL_PBP_STRING") = DBNull.Value
                drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole '& Convert.ToString(m_objGameDetails.SerialNo)
                drPBPData("PROCESSED") = "N"
                drPBPData("CONTINUATION") = "F"
                Dim DsLastEventDetails As DataSet
                DsLastEventDetails = m_objModule1PBP.getLastEvent()
                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED"))

                'drPBPData("TIME_ELAPSED") = CStr(getTimeElapsed())
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    drPBPData("DEMO_DATA") = "Y"
                Else
                    drPBPData("DEMO_DATA") = "N"
                End If
                drPBPData("OFFENSIVE_PLAYER_ID") = IIf(CInt(lstPlayer.SelectedValue) = 0, DBNull.Value, CInt(lstPlayer.SelectedValue))
                drPBPData("SHOT_DESC") = IIf(CInt(lstType.SelectedValue) = 0, DBNull.Value, CInt(lstType.SelectedValue))
                If lstGoalZone.Enabled = True Then
                    drPBPData("GOALZONE_ID") = IIf(CInt(lstGoalZone.SelectedValue) = 0, DBNull.Value, CInt(lstGoalZone.SelectedValue))
                    drPBPData("KEEPERZONE_ID") = IIf(CInt(lstKeeperZone.SelectedValue) = 0, DBNull.Value, CInt(lstKeeperZone.SelectedValue))
                    If m_objGameDetails.ModuleID = 2 Then
                        If CInt(lstResult.SelectedValue) = 1 Then
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                        Else
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                        End If
                    Else
                        drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                    End If
                    'drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                Else

                    drPBPData("KEEPERZONE_ID") = IIf(CInt(lstKeeperZone.SelectedValue) = 0, DBNull.Value, CInt(lstKeeperZone.SelectedValue))
                    drPBPData("SHOT_RESULT") = IIf(CInt(lstSubType.SelectedValue) = 0, DBNull.Value, CInt(lstSubType.SelectedValue))
                    If m_objGameDetails.CoverageLevel > 3 Then
                        If IsDBNull(drPBPData("SHOT_RESULT")) = False Then
                            If CInt(drPBPData("SHOT_RESULT")) > 0 And CInt(drPBPData("SHOT_RESULT")) < 7 Then
                                drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_SAVE
                            Else
                                drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                            End If
                        End If
                        'ElseIf m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2 Then
                    ElseIf m_objGameDetails.ModuleID = 2 Then
                        If CInt(lstResult.SelectedValue) = 1 Then
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                        Else
                            drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                        End If
                    Else
                        drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_MISSED
                    End If

                End If

                ''Add Shootout Round
                'If lclSeqNo = 1 Or lclSeqNo = 2 Then
                '    drPBPData("SHOOTOUT_ROUND") = 1
                'Else
                If (CInt(txtSequence.Text) + 1) Mod 2 = 0 Then
                    drPBPData("SHOOTOUT_ROUND") = CInt(CInt(txtSequence.Text) + 1) / 2
                Else
                    drPBPData("SHOOTOUT_ROUND") = CInt(txtSequence.Text) / 2
                End If
                'drPBPData("SHOOTOUT_ROUND") = CInt((CInt(txtSequence.Text)) / 2)
                'End If
                drPBPData("TEAM_ID") = lstTeam.SelectedValue
                drPBPData("OFFENSE_SCORE") = CalculateScores(CInt(drPBPData("TEAM_ID")), CInt(drPBPData("EVENT_CODE_ID")), "OFFENSE_SCORE")
                drPBPData("DEFENSE_SCORE") = CalculateScores(CInt(drPBPData("TEAM_ID")), CInt(drPBPData("EVENT_CODE_ID")), "DEFENSE_SCORE")
                m_SeqNum = CInt(drPBPData("SEQUENCE_NUMBER"))
            End If

            m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Function CalculateScores(ByVal TeamID As Integer, ByVal EventID As Integer, ByVal Type As String) As Integer
        Try
            Dim m_dsScores As DataSet
            m_dsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsScores.Tables(0).Rows.Count > 0 Then
                If TeamID = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                    Select Case Type
                        Case "OFFENSE_SCORE"
                            If EventID = 30 Then
                                Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            Else
                                Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            End If
                        Case "DEFENSE_SCORE"
                            Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    End Select
                Else
                    Select Case Type
                        Case "OFFENSE_SCORE"
                            If EventID = 30 Then
                                Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                            Else
                                Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                            End If
                        Case "DEFENSE_SCORE"
                            Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    End Select
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function InsertPlaybyPlayData() As Boolean
        Try
            'code to save the data into SQL
            Dim dsTempPBPData As New DataSet
            Dim dsPBP As New DataSet
            m_objGameDetails.PBP.DataSetName = "SoccerEventData"
            dsTempPBPData = m_objGameDetails.PBP
            Dim strPBPXMLData As String = dsTempPBPData.GetXml()
            If m_objGameDetails.IsEditMode Then
                If m_ScoreEdit Then 'Score given from one team to another
                    'dsPBP = m_objPenaltyShootout.UpdatePBP(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_SeqNum)
                    'm_objPenaltyShootout.EditScoreEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_SeqNum, "SCTOSC")
                    dsPBP = m_objPenaltyShootout.UpdatePBP(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_SeqNum, "EDIT", m_objGameDetails.languageid)
                    '    '' Set correct Score now
                    m_dsScores.Tables.Clear()
                    m_dsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    GetTeamScores(m_dsScores)

                    If CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                        m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                        'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                        'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    Else
                        m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                        'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                        'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    End If
                    m_HomeScoreSO = 0
                    m_AwayScoreSO = 0
                    GetShootoutScore(m_dsScores)
                    'm_objGameDetails.HomeScore = m_HomeScoreSO
                    'm_objGameDetails.AwayScore = m_AwayScoreSO
                    'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
                    lblScoreHome.Text = CStr(m_HomeScoreSO)
                    lblScoreAway.Text = CStr(m_AwayScoreSO)
                Else
                    'dsPBP = m_objPenaltyShootout.UpdatePBP(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_SeqNum)
                    dsPBP = m_objPenaltyShootout.UpdatePBP(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_SeqNum, "EDIT", m_objGameDetails.languageid)
                End If
            Else
                m_objPenaltyShootout.InsertPBPDataToSQL(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_objGameDetails.languageid)
            End If
            'If m_objGameDetails.IsEditMode = False Then
            SetScoresInControls()
            'End If

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        Finally
            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next
            m_objGameDetails.IsContinuationEvent = False
        End Try
    End Function

    Private Sub SetScoresInControls()
        Try
            
            Dim m_DsScores As DataSet
            'FETCHING THE LAST EVENT SCORES FOR THE SELECTED GAME
            m_DsScores = m_objGeneral.GetPenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            ''
            m_HomeScoreSO = 0
            m_AwayScoreSO = 0
            If m_DsScores.Tables(0).Rows.Count > 0 Then
                If CDbl(m_DsScores.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                    If m_DsScores.Tables(1).Rows.Count > 0 Then
                        Dim drHome() As DataRow = m_DsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID)
                        Dim drAway() As DataRow = m_DsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID)
                        m_objGameDetails.HomeScore = drHome.Length
                        m_HomeScoreSO = drHome.Length
                        m_objGameDetails.AwayScore = drAway.Length
                        m_AwayScoreSO = drAway.Length
                    End If
                End If
            End If

            lblScoreHome.Text = CStr(m_HomeScoreSO)
            lblScoreAway.Text = CStr(m_AwayScoreSO)

            ''


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' FN FETCHED UNIQUE ID FROM DATASET IF IT IS A CONTINUATION EVENT ELSE FROM DATABASE
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetUniqueID() As Integer
        Try
            Return m_objGeneral.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub SetSequence()
        Try
            Dim m_dsShootoutData As DataSet
            m_dsShootoutData = m_objPenaltyShootout.GetShootoutPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsShootoutData.Tables(0).Rows.Count > 0 Then
                txtSequence.Text = CStr(m_dsShootoutData.Tables(0).Rows.Count + 1)
            Else
                txtSequence.Text = "1"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayInListview()
        Try

            Dim homeScore As Integer = 0
            Dim awayScore As Integer = 0
            lvPenaltyShootout.Items.Clear()
            m_dsShootoutData = m_objPenaltyShootout.GetShootoutPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsShootoutData.Tables(0).Rows.Count > 0 Then
                For intRowcount As Integer = 0 To m_dsShootoutData.Tables(0).Rows.Count - 1
                    Dim lvwitem As New ListViewItem(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("SEQUENCE_NUMBER").ToString)
                    lvwitem.SubItems.Add(CStr(intRowcount + 1))

                    If intRowcount = 0 Then
                        m_TeamIDtoValidate = CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("TEAM_ID"))
                    End If
                    If CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("EVENT_CODE_ID")) = clsGameDetails.SHOOTOUT_GOAL Then
                        If m_objGameDetails.HomeTeamID = CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("TEAM_ID")) Then
                            homeScore = homeScore + 1
                        Else
                            awayScore = awayScore + 1
                        End If
                    End If

                    lvwitem.SubItems.Add(homeScore & "-" & awayScore)
                    'If m_objGameDetails.OffensiveTeamID = CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("TEAM_ID")) Then
                    '    lvwitem.SubItems.Add(CStr(homeScore).ToString & "-" & CStr(awayScore).ToString)
                    'Else
                    '    lvwitem.SubItems.Add(CStr(awayScore).ToString & "-" & CStr(homeScore).ToString)
                    'End If
                    
                    'lvwitem.SubItems.Add(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("OFFENSE_SCORE").ToString & "-" & m_dsShootoutData.Tables(0).Rows(intRowcount).Item("DEFENSE_SCORE").ToString)
                    lvwitem.SubItems.Add(GetTeamName(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("TEAM_ID"))))
                    Select Case CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("EVENT_CODE_ID"))
                        Case clsGameDetails.SHOOTOUT_GOAL ' Scored
                            lvwitem.SubItems.Add("Scored")
                            lvwitem.SubItems.Add("")
                            lvwitem.SubItems.Add(GetPlayerName(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("OFFENSIVE_PLAYER_ID"))))
                            If Not IsDBNull(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("GOALZONE_ID")) Then
                                lvwitem.SubItems.Add(GetZoneDesc(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("GOALZONE_ID")), "GOALZONE"))
                            Else
                                lvwitem.SubItems.Add("")
                                If m_objGameDetails.CoverageLevel >= 4 Then
                                    lvwitem.UseItemStyleForSubItems = False
                                    lvwitem.SubItems(7).BackColor = Color.Red
                                End If
                            End If
                            If Not IsDBNull(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("KEEPERZONE_ID")) Then
                                lvwitem.SubItems.Add(GetZoneDesc(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("KEEPERZONE_ID")), "KEEPERZONE"))
                            Else
                                lvwitem.SubItems.Add("")
                                If m_objGameDetails.CoverageLevel >= 4 Then
                                    lvwitem.UseItemStyleForSubItems = False
                                    lvwitem.SubItems(8).BackColor = Color.Red
                                End If
                            End If

                        Case clsGameDetails.SHOOTOUT_MISSED, clsGameDetails.SHOOTOUT_SAVE ' Missed

                            lvwitem.SubItems.Add("Missed")
                            If Not IsDBNull(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("SHOT_RESULT")) Then
                                lvwitem.SubItems.Add(GetZoneDesc(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("SHOT_RESULT")), "SHOT_RESULT"))
                            Else
                                lvwitem.SubItems.Add("")
                                If m_objGameDetails.CoverageLevel >= 4 Then
                                    lvwitem.UseItemStyleForSubItems = False
                                    lvwitem.SubItems(6).BackColor = Color.Red
                                End If
                            End If
                            lvwitem.SubItems.Add(GetPlayerName(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("OFFENSIVE_PLAYER_ID"))))
                            lvwitem.SubItems.Add("")
                            If Not IsDBNull(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("KEEPERZONE_ID")) Then
                                lvwitem.SubItems.Add(GetZoneDesc(CInt(m_dsShootoutData.Tables(0).Rows(intRowcount).Item("KEEPERZONE_ID")), "KEEPERZONE"))
                            Else
                                lvwitem.SubItems.Add("")
                                If m_objGameDetails.CoverageLevel >= 4 Then
                                    lvwitem.UseItemStyleForSubItems = False
                                    lvwitem.SubItems(8).BackColor = Color.Red
                                End If
                            End If
                    End Select
                    lvPenaltyShootout.Items.Add(lvwitem)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetZoneDesc(ByVal ZoneID As Integer, ByVal Type As String) As String
        Try
            Dim drsZone() As DataRow
            Select Case Type
                Case "GOALZONE"
                    drsZone = DsShootoutInputs.Tables("Goal_Zone").Select("GOALZONE_ID = " & ZoneID & "")
                    If drsZone.Length > 0 Then
                        Return drsZone(0).Item("GOALZONE").ToString
                    End If
                Case "KEEPERZONE"
                    drsZone = DsShootoutInputs.Tables("Keeper_Zone").Select("KEEPERZONE_ID = " & ZoneID & "")
                    If drsZone.Length > 0 Then
                        Return drsZone(0).Item("KEEPERZONE").ToString
                    End If
                Case "SHOT_RESULT"
                    drsZone = DsShootoutInputs.Tables("Shot_Res").Select("SHOT_RESULT_ID = " & ZoneID & "")
                    If drsZone.Length > 0 Then
                        Return drsZone(0).Item("SHOT_RESULT").ToString
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetPlayerName(ByVal PlayerID As Integer) As String
        Try
            Dim StrPlayer As String = ""
            'FOR COVERAGELEVEL 2 USE TEAMROSTER PROPERTY DATASET TO GET THE PALYERNAME
            If m_objGameDetails.CoverageLevel = 2 Then
                If m_objGameDetails.TeamRosters.Tables.Count > 0 Then
                    Dim DsPlayers As DataSet = m_objGameDetails.TeamRosters.Copy()
                    Dim drs() As DataRow = Nothing
                    drs = DsPlayers.Tables(0).Select("PLAYER_ID = " & PlayerID & "")
                    If drs.Length > 0 Then
                        StrPlayer = CStr(drs(0).Item("PLAYER"))
                    Else
                        drs = DsPlayers.Tables(1).Select("PLAYER_ID = " & PlayerID & "")
                        If drs.Length > 0 Then
                            StrPlayer = CStr(drs(0).Item("PLAYER"))
                        End If
                    End If
                    Return StrPlayer
                End If
            Else
                If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                    Dim DsPlayers As DataSet = m_objTeamSetup.RosterInfo.Copy()
                    Dim drs() As DataRow = Nothing
                    drs = DsPlayers.Tables("Home").Select("PLAYER_ID = " & PlayerID & "")
                    If drs.Length > 0 Then
                        StrPlayer = CStr(drs(0).Item("PLAYER"))
                    Else
                        drs = DsPlayers.Tables("Away").Select("PLAYER_ID = " & PlayerID & "")
                        If drs.Length > 0 Then
                            StrPlayer = CStr(drs(0).Item("PLAYER"))
                        End If
                    End If
                    Return StrPlayer
                End If
            End If
            
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' For filling game information in controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillTeamLogos()
        Try
            Dim v_memLogo As MemoryStream
            lklHometeam.Text = m_objGameDetails.HomeTeam.Trim()
            lklAwayteam.Text = m_objGameDetails.AwayTeam.Trim()
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID)
            If v_memLogo IsNot Nothing Then
                picAwayLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, m_objGameDetails.HomeTeamID)
            If v_memLogo IsNot Nothing Then
                picHomeLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub FillShootoutInputs()
        Try
            DsShootoutInputs = m_objPenaltyShootout.FetchShootoutInputs(m_objGameDetails.GameCode, m_objGameDetails.languageid)

            If CInt(m_objGameDetails.languageid) <> 1 Then
                For COUNT As Integer = 0 To DsShootoutInputs.Tables("Result").Rows.Count - 1
                    Dim MISSED As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DsShootoutInputs.Tables("Result").Rows(COUNT).Item("RESULT_DESC").ToString)
                    DsShootoutInputs.Tables("Result").Rows(COUNT).Item("RESULT_DESC") = MISSED
                    DsShootoutInputs.AcceptChanges()

                Next
            End If

            If DsShootoutInputs.Tables.Count > 0 Then
                m_blnAutoFired = True
                clsUtility.LoadControl(lstResult, DsShootoutInputs.Tables("Result"), "RESULT_DESC", "RESULT_ID")
                clsUtility.LoadControl(lstType, DsShootoutInputs.Tables("Shot_Desc"), "SHOT_DESC", "SHOT_DESC_ID")
                clsUtility.LoadControl(lstGoalZone, DsShootoutInputs.Tables("Goal_Zone"), "GOALZONE", "GOALZONE_ID")
                clsUtility.LoadControl(lstKeeperZone, DsShootoutInputs.Tables("Keeper_Zone"), "KEEPERZONE", "KEEPERZONE_ID")
                clsUtility.LoadControl(lstSubType, DsShootoutInputs.Tables("Shot_Res"), "SHOT_RESULT", "SHOT_RESULT_ID")
                clsUtility.LoadControl(lstTeam, DsShootoutInputs.Tables("Team"), "TEAM_NAME", "TEAM_ID")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            m_blnAutoFired = False
        End Try

    End Sub

    Private Sub FillTeamScores()
        Try
            Dim m_DsScores As DataSet
            'FETCHING THE LAST EVENT SCORES FOR THE SELECTED GAME
            m_DsScores = m_objGeneral.GetPenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            ''
            m_HomeScoreSO = 0
            m_AwayScoreSO = 0
            If m_DsScores.Tables(0).Rows.Count > 0 Then
                If CDbl(m_DsScores.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                    If m_DsScores.Tables(1).Rows.Count > 0 Then
                        Dim drHome() As DataRow = m_DsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID)
                        Dim drAway() As DataRow = m_DsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID)
                        m_objGameDetails.HomeScore = drHome.Length
                        m_HomeScoreSO = drHome.Length
                        m_objGameDetails.AwayScore = drAway.Length
                        m_AwayScoreSO = drAway.Length
                    End If
                End If
            End If


            ' ''


            'If m_DsScores.Tables(0).Rows.Count > 0 Then
            '    If CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
            '        m_objGameDetails.HomeScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
            '        m_objGameDetails.AwayScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
            '        'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
            '        'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
            '        'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
            '    Else
            '        m_objGameDetails.AwayScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
            '        m_objGameDetails.HomeScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
            '        'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
            '        'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
            '    End If

            'GetShootoutScore(m_DsScores)
            '    'm_objGameDetails.HomeScore = m_HomeScoreSO
            '    'm_objGameDetails.AwayScore = m_AwayScoreSO
            '    'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
            lblScoreHome.Text = CStr(m_HomeScoreSO)
            lblScoreAway.Text = CStr(m_AwayScoreSO)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Function GetTeamName(ByVal TeamID As Integer) As String
        Try
            If TeamID = CInt(m_objGameDetails.HomeTeamID) Then
                Return m_objGameDetails.HomeTeam.ToString
            Else
                Return m_objGameDetails.AwayTeam.ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub DoCancel()
        Try
            lstTeam.SelectedIndex = -1
            lstResult.SelectedIndex = -1
            lstGoalZone.SelectedIndex = -1
            lstKeeperZone.SelectedIndex = -1
            lstPlayer.SelectedIndex = -1
            lstType.SelectedIndex = -1
            lstSubType.SelectedIndex = -1
            m_SeqNum = 0
            lstKeeperZone.ClearSelected()
            lstGoalZone.ClearSelected()
            lblKeeperZone.Text = ""
            lblGoalZone.Text = ""
            txtSequence.Enabled = True
            lstResult.Enabled = True
            lstGoalZone.Enabled = True
            lstSubType.Enabled = True
            If m_objGameDetails.IsEditMode Then
                setCurrentState()
            End If
            m_TeamID = 0
            m_ScoreEdit = False
            m_objGameDetails.IsEditMode = False
            m_EditEventID = 0
            btnEdit.Enabled = False
            btnDelete.Enabled = False
            btnSave.Enabled = True
            m_ShootoutRound = 0
            lstTeam.Enabled = True
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SwitchSlides()
        Try
            If m_objGameDetails.IsHomeTeamOnLeftSwitch = False Then
                picAwayLogo.Location = New Point(15, 34)
                picHomeLogo.Location = New Point(825, 34)
                lklAwayteam.Location = New Point(26, 11)
                lklHometeam.Location = New Point(563, 11)
                lblScoreAway.Location = New Point(216, 11)
                lblScoreHome.Location = New Point(464, 11)
                m_objGameDetails.OffensiveTeamID = m_objGameDetails.AwayTeamID
                m_objGameDetails.DefensiveTeamID = m_objGameDetails.HomeTeamID
            Else
                m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID
                m_objGameDetails.DefensiveTeamID = m_objGameDetails.AwayTeamID
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub DisableInputs(ByVal EnableType As Boolean)
        Try
            lstGoalZone.Enabled = EnableType
            lstSubType.Enabled = EnableType
            lstKeeperZone.Enabled = EnableType
            lstPlayer.Enabled = True

            'If m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2 Then
            If m_objGameDetails.ModuleID = 2 Then
                lstType.Enabled = False
                lstGoalZone.Enabled = False
                lstKeeperZone.Enabled = False
                lstSubType.Enabled = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetStateEditReplace(ByVal SeqNo As Decimal)
        Try
            Dim dsShootoutData As DataSet
            Dim drEditPBP As DataRow

            dsShootoutData = m_objPenaltyShootout.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SeqNo, "LESS_EQUAL")
            If dsShootoutData.Tables(0).Rows.Count > 0 Then
                m_TeamID = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                m_EditEventID = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("EVENT_CODE_ID"))

                ''COPY CURRENT INFORMATION HERE
                drEditPBP = m_dsTempShootout.Tables(0).NewRow()
                drEditPBP("GAME_CODE") = m_objGameDetails.GameCode
                drEditPBP("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drEditPBP("TIME_ELAPSED") = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("TIME_ELAPSED")
                drEditPBP("PERIOD") = m_objGameDetails.CurrentPeriod
                drEditPBP("TEAM_ID") = m_objGameDetails.OffensiveTeamID

                drEditPBP("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drEditPBP("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                drEditPBP("REPORTER_ROLE") = m_objGameDetails.ReporterRole
                drEditPBP("UNIQUE_ID") = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("UNIQUE_ID")
                m_CurrLocalSeqNo = CInt(txtSequence.Text)

                ''STORE EDITED RECORD INFORMATION
                m_objGameDetails.GameCode = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("GAME_CODE"))
                m_objGameDetails.FeedNumber = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("FEED_NUMBER"))
                m_objGameDetails.CurrentPeriod = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("PERIOD"))
                'm_EventID = CInt(dsShootoutData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                m_objGameDetails.OffensiveTeamID = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                If CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("EVENT_CODE_ID")) = clsGameDetails.SHOOTOUT_GOAL Then
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        m_objGameDetails.HomeScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.AwayScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    Else
                        m_objGameDetails.AwayScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.HomeScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    End If
                Else
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        m_objGameDetails.HomeScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.AwayScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    Else
                        m_objGameDetails.AwayScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.HomeScore = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    End If
                End If
                m_ShootoutRound = CInt(dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("SHOOTOUT_ROUND"))
                'm_objGameDetails.ReporterRole = CStr(dsShootoutData.Tables(0).Rows(0).Item("REPORTER_ROLE"))
                drEditPBP("SEQUENCE_NUMBER") = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER")
                'lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                'lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                m_HomeScoreSO = 0
                m_AwayScoreSO = 0
                GetShootoutScore(dsShootoutData)
                ''m_objGameDetails.HomeScore = m_HomeScoreSO
                ''m_objGameDetails.AwayScore = m_AwayScoreSO
                ''ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
                lblScoreHome.Text = CStr(m_HomeScoreSO)
                lblScoreAway.Text = CStr(m_AwayScoreSO)

                m_dsTempShootout.Tables(0).Rows.Clear()
                m_dsTempShootout.Tables(0).Rows.Add(drEditPBP)

                lstTeam.SelectedValue = m_TeamID
                lstType.SelectedValue = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("SHOT_DESC")
                m_blnAutoFired = True
                lstSubType.SelectedValue = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("SHOT_RESULT")
                m_blnAutoFired = False
                m_blnAutoFired = True
                lstKeeperZone.SelectedValue = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("KEEPERZONE_ID")
                m_blnAutoFired = False
                m_blnAutoFired = True
                lstGoalZone.SelectedValue = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("GOALZONE_ID")
                m_blnAutoFired = False
                lstPlayer.SelectedValue = dsShootoutData.Tables(0).Rows(dsShootoutData.Tables(0).Rows.Count - 1).Item("OFFENSIVE_PLAYER_ID")
                txtSequence.Text = CStr(DirectCast(lvPenaltyShootout.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(1).Text)
                txtSequence.Enabled = False
                If m_EditEventID = clsGameDetails.SHOOTOUT_GOAL Then
                    lstResult.SelectedIndex = 0
                    lstResult.Enabled = False
                ElseIf m_EditEventID = clsGameDetails.SHOOTOUT_MISSED Then
                    lstResult.SelectedIndex = 1
                    lstResult.Enabled = False
                ElseIf m_EditEventID = clsGameDetails.SHOOTOUT_SAVE Then
                    lstResult.SelectedIndex = 1
                    lstResult.Enabled = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub setCurrentState()
        Try
            Dim drEditShootData As DataRow
            If m_dsTempShootout.Tables.Count > 0 Then
                If m_dsTempShootout.Tables(0).Rows.Count > 0 Then
                    drEditShootData = m_dsTempShootout.Tables(0).Rows(0)


                    m_objGameDetails.GameCode = CInt(drEditShootData("GAME_CODE"))
                    m_objGameDetails.FeedNumber = CInt(drEditShootData("FEED_NUMBER"))
                    'txtTime.Text = clsUtility.ConvertSecondToMinute(CDbl(drPBPEditData("TIME_ELAPSED")), False)
                    If m_ScoreEdit = False Then
                        m_objGameDetails.CurrentPeriod = CInt(drEditShootData("PERIOD"))
                        m_objGameDetails.OffensiveTeamID = CInt(drEditShootData("TEAM_ID"))
                        m_objGameDetails.HomeScore = CInt(drEditShootData("OFFENSE_SCORE"))
                        m_objGameDetails.AwayScore = CInt(drEditShootData("DEFENSE_SCORE"))

                        Dim m_DsScores As DataSet
                        'FETCHING THE LAST EVENT SCORES FOR THE SELECTED GAME
                        m_DsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        
                        m_HomeScoreSO = 0
                        m_AwayScoreSO = 0
                        GetShootoutScore(m_DsScores)
                        
                        'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
                        lblScoreHome.Text = CStr(m_HomeScoreSO)
                        lblScoreAway.Text = CStr(m_AwayScoreSO)
                        
                    End If
                    'm_objGameDetails.HomeScore = CInt(drPBPEditData("OFFENSE_SCORE"))
                    'm_objGameDetails.AwayScore = CInt(drPBPEditData("DEFENSE_SCORE"))
                    txtSequence.Text = CStr(m_CurrLocalSeqNo)

                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetTeamScores(ByVal Scores As DataSet)
        Try
            If Scores.Tables.Count > 0 Then
                If Scores.Tables(0).Rows.Count > 0 Then
                    Dim intTeamID = CInt(Scores.Tables(0).Rows(Scores.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                    Dim intAwayScore As Integer = CInt(Scores.Tables(0).Rows(Scores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    Dim intHomeScore As Integer = CInt(Scores.Tables(0).Rows(Scores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))

                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
       
    End Sub

    Private Sub GetShootoutScore(ByVal Scores As DataSet)
        Try
            If Scores.Tables.Count > 0 Then
                If Scores.Tables(0).Rows.Count > 0 Then

                    Dim drHome() As DataRow = Scores.Tables(0).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID & " AND EVENT_CODE_ID = 30")
                    Dim drAway() As DataRow = Scores.Tables(0).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID & " AND EVENT_CODE_ID = 30")
                    m_objGameDetails.HomeScore = drHome.Length
                    m_HomeScoreSO = drHome.Length
                    m_objGameDetails.AwayScore = drAway.Length
                    m_AwayScoreSO = drAway.Length

                    'Dim intRowCount As Integer
                    'For intRowCount = 0 To Scores.Tables(0).Rows.Count - 1
                    '    If CInt(Scores.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = clsGameDetails.SHOOTOUT_GOAL Then
                    '        If CInt(Scores.Tables(0).Rows(intRowCount).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                    '            m_HomeScoreSO = m_HomeScoreSO
                    '        Else
                    '            m_AwayScoreSO = m_AwayScoreSO
                    '        End If
                    '    End If
                    'Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Function ShootoutAllowed() As Boolean
        Try
            Dim SeqNumber As Integer
            Dim m_dsShootoutData As DataSet
            m_dsShootoutData = m_objPenaltyShootout.GetShootoutPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            SeqNumber = CInt(txtSequence.Text)
            If SeqNumber > 1 Then
                If CInt(m_dsShootoutData.Tables(0).Rows(m_dsShootoutData.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = CInt(lstTeam.SelectedValue) Then

                    'If ((SeqNumber + 1) Mod 2 = 0 And m_TeamIDtoValidate <> CInt(lstTeam.SelectedValue)) Then
                    '    Return False
                    'End If
                    'If (SeqNumber Mod 2 = 0 And m_TeamIDtoValidate = CInt(lstTeam.SelectedValue)) Then
                    Return False
                End If
            End If


            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function getTimeElapsed() As Integer
        Try
            If m_objGameDetails.CurrentPeriod = 2 Then
                Return FIRSTHALF
            Else
                Return EXTRATIME
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub ChangeTheme(ByVal EditMode As Boolean)
        Try
            If EditMode Then
                Me.BackColor = Color.LightGoldenrodYellow
                picReporterBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                picButtonBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                pnlSave.BackColor = Color.Beige
            Else
                Me.BackColor = Color.WhiteSmoke
                picReporterBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                picButtonBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                pnlSave.BackColor = Color.Lavender
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub lstType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstType.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lstTeam.Text, Nothing, 1, 0)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub lstPlayer_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lstPlayer.DrawItem
        Try
            e.DrawBackground()
            Dim myBrush As Brush
            Dim myBackColor As Color = Color.White
            Dim myForeColor As Color = Color.Black
            Dim myFont As Font = Me.Font
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                myBackColor = Color.CornflowerBlue
                myForeColor = Color.White
            End If
            If lstPlayer.Items.Count > 0 Then
                If isPlayerUsedAlready(Convert.ToInt32(DirectCast(DirectCast(DirectCast(lstPlayer.Items(e.Index), System.Object), System.Data.DataRowView).Row, System.Data.DataRow).ItemArray(1))) Then
                    myFont = New Font(Me.Font, FontStyle.Strikeout)
                    myForeColor = Color.Maroon
                End If

                myBrush = New SolidBrush(myBackColor)
                e.Graphics.FillRectangle(myBrush, e.Bounds)
                myBrush = New SolidBrush(myForeColor)
                e.Graphics.DrawString(Convert.ToString(DirectCast(DirectCast(DirectCast(lstPlayer.Items(e.Index), System.Object), System.Data.DataRowView).Row, System.Data.DataRow).ItemArray(0)), myFont, myBrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
                e.DrawFocusRectangle()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function isPlayerUsedAlready(ByVal intPlayerId As Integer) As Boolean
        Try

            If (m_dsShootoutData.Tables(0).Select("OFFENSIVE_PLAYER_ID = " & intPlayerId).Length > 0) Then
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub lstPlayer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstPlayer.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lstTeam.Text, Nothing, 1, 0)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtSequence_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSequence.Leave
        Try
            If (txtSequence.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtSequence.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function GetOnfieldvalue(ByVal period As Integer) As Integer
        Try
            Dim intOnField As Integer = m_objGeneral.GetOnfieldValue(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, period)
            Return intOnField
        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class