﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class udcSoccerGoalFrame
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picSoccerGoalFrame = New System.Windows.Forms.PictureBox()
        CType(Me.picSoccerGoalFrame, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picSoccerGoalFrame
        '
        Me.picSoccerGoalFrame.BackColor = System.Drawing.Color.LightBlue
        Me.picSoccerGoalFrame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picSoccerGoalFrame.Location = New System.Drawing.Point(0, 0)
        Me.picSoccerGoalFrame.Margin = New System.Windows.Forms.Padding(0)
        Me.picSoccerGoalFrame.Name = "picSoccerGoalFrame"
        Me.picSoccerGoalFrame.Size = New System.Drawing.Size(450, 75)
        Me.picSoccerGoalFrame.TabIndex = 0
        Me.picSoccerGoalFrame.TabStop = False
        '
        'udcSoccerGoalFrame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.picSoccerGoalFrame)
        Me.Name = "udcSoccerGoalFrame"
        Me.Size = New System.Drawing.Size(450, 75)
        CType(Me.picSoccerGoalFrame, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picSoccerGoalFrame As System.Windows.Forms.PictureBox

End Class
