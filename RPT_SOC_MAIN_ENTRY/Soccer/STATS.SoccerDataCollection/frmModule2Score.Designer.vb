﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModule2Score
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwPBP = New System.Windows.Forms.ListView
        Me.EventID = New System.Windows.Forms.ColumnHeader
        Me.Period = New System.Windows.Forms.ColumnHeader
        Me.Team = New System.Windows.Forms.ColumnHeader
        Me.Time = New System.Windows.Forms.ColumnHeader
        Me.Score = New System.Windows.Forms.ColumnHeader
        Me.EventCodeID = New System.Windows.Forms.ColumnHeader
        Me.UniqueID = New System.Windows.Forms.ColumnHeader
        Me.TeamID = New System.Windows.Forms.ColumnHeader
        Me.SequenceNumber = New System.Windows.Forms.ColumnHeader
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnSwitchGames = New System.Windows.Forms.Button
        Me.btnInsert = New System.Windows.Forms.Button
        Me.pnlScore = New System.Windows.Forms.Panel
        Me.lblGoal = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.txtTimeMod2 = New System.Windows.Forms.MaskedTextBox
        Me.cmbTeam = New System.Windows.Forms.ComboBox
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblTime = New System.Windows.Forms.Label
        Me.lblTeam = New System.Windows.Forms.Label
        Me.btnTime = New System.Windows.Forms.Button
        Me.pnlScore.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvwPBP
        '
        Me.lvwPBP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.EventID, Me.Period, Me.Team, Me.Time, Me.Score, Me.EventCodeID, Me.UniqueID, Me.TeamID, Me.SequenceNumber})
        Me.lvwPBP.FullRowSelect = True
        Me.lvwPBP.Location = New System.Drawing.Point(245, 16)
        Me.lvwPBP.Name = "lvwPBP"
        Me.lvwPBP.Size = New System.Drawing.Size(517, 125)
        Me.lvwPBP.TabIndex = 0
        Me.lvwPBP.UseCompatibleStateImageBehavior = False
        Me.lvwPBP.View = System.Windows.Forms.View.Details
        '
        'EventID
        '
        Me.EventID.Text = "Event"
        Me.EventID.Width = 100
        '
        'Period
        '
        Me.Period.Text = "Period"
        Me.Period.Width = 80
        '
        'Team
        '
        Me.Team.Text = "Team"
        Me.Team.Width = 100
        '
        'Time
        '
        Me.Time.Text = "Time"
        Me.Time.Width = 100
        '
        'Score
        '
        Me.Score.Text = "Score"
        Me.Score.Width = 80
        '
        'EventCodeID
        '
        Me.EventCodeID.Text = "EventCodeID"
        Me.EventCodeID.Width = 0
        '
        'UniqueID
        '
        Me.UniqueID.Text = "UniqueID"
        Me.UniqueID.Width = 0
        '
        'TeamID
        '
        Me.TeamID.Text = "TeamID"
        Me.TeamID.Width = 0
        '
        'SequenceNumber
        '
        Me.SequenceNumber.Text = "SequenceNumber"
        Me.SequenceNumber.Width = 0
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(802, 15)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(74, 25)
        Me.btnEdit.TabIndex = 7
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(802, 83)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(74, 25)
        Me.btnDelete.TabIndex = 8
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnSwitchGames
        '
        Me.btnSwitchGames.BackColor = System.Drawing.Color.DimGray
        Me.btnSwitchGames.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSwitchGames.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSwitchGames.ForeColor = System.Drawing.Color.White
        Me.btnSwitchGames.Location = New System.Drawing.Point(776, 116)
        Me.btnSwitchGames.Name = "btnSwitchGames"
        Me.btnSwitchGames.Size = New System.Drawing.Size(100, 25)
        Me.btnSwitchGames.TabIndex = 9
        Me.btnSwitchGames.Text = "Switch Games"
        Me.btnSwitchGames.UseVisualStyleBackColor = False
        '
        'btnInsert
        '
        Me.btnInsert.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnInsert.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnInsert.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnInsert.ForeColor = System.Drawing.Color.White
        Me.btnInsert.Location = New System.Drawing.Point(802, 49)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(74, 25)
        Me.btnInsert.TabIndex = 6
        Me.btnInsert.Text = "Insert"
        Me.btnInsert.UseVisualStyleBackColor = False
        '
        'pnlScore
        '
        Me.pnlScore.BackColor = System.Drawing.Color.Lavender
        Me.pnlScore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlScore.Controls.Add(Me.btnTime)
        Me.pnlScore.Controls.Add(Me.lblGoal)
        Me.pnlScore.Controls.Add(Me.btnCancel)
        Me.pnlScore.Controls.Add(Me.txtTimeMod2)
        Me.pnlScore.Controls.Add(Me.cmbTeam)
        Me.pnlScore.Controls.Add(Me.btnSave)
        Me.pnlScore.Controls.Add(Me.lblTime)
        Me.pnlScore.Controls.Add(Me.lblTeam)
        Me.pnlScore.Location = New System.Drawing.Point(18, 16)
        Me.pnlScore.Name = "pnlScore"
        Me.pnlScore.Size = New System.Drawing.Size(208, 125)
        Me.pnlScore.TabIndex = 0
        '
        'lblGoal
        '
        Me.lblGoal.AutoSize = True
        Me.lblGoal.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblGoal.Location = New System.Drawing.Point(0, 0)
        Me.lblGoal.Name = "lblGoal"
        Me.lblGoal.Size = New System.Drawing.Size(42, 15)
        Me.lblGoal.TabIndex = 5
        Me.lblGoal.Text = "Goal :"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(103, 92)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(74, 25)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'txtTimeMod2
        '
        Me.txtTimeMod2.Location = New System.Drawing.Point(62, 53)
        Me.txtTimeMod2.Mask = "000"
        Me.txtTimeMod2.Name = "txtTimeMod2"
        Me.txtTimeMod2.Size = New System.Drawing.Size(34, 22)
        Me.txtTimeMod2.TabIndex = 2
        '
        'cmbTeam
        '
        Me.cmbTeam.FormattingEnabled = True
        Me.cmbTeam.Location = New System.Drawing.Point(61, 18)
        Me.cmbTeam.Name = "cmbTeam"
        Me.cmbTeam.Size = New System.Drawing.Size(123, 23)
        Me.cmbTeam.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(22, 92)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.lblTime.Location = New System.Drawing.Point(7, 57)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(36, 15)
        Me.lblTime.TabIndex = 1
        Me.lblTime.Text = "Time :"
        '
        'lblTeam
        '
        Me.lblTeam.AutoSize = True
        Me.lblTeam.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.lblTeam.Location = New System.Drawing.Point(1, 22)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(40, 15)
        Me.lblTeam.TabIndex = 0
        Me.lblTeam.Text = "Team :"
        '
        'btnTime
        '
        Me.btnTime.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.btnTime.Font = New System.Drawing.Font("Arial Unicode MS", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTime.ForeColor = System.Drawing.Color.PeachPuff
        Me.btnTime.Location = New System.Drawing.Point(109, 50)
        Me.btnTime.Name = "btnTime"
        Me.btnTime.Size = New System.Drawing.Size(75, 28)
        Me.btnTime.TabIndex = 259
        Me.btnTime.Text = "Time"
        Me.btnTime.UseVisualStyleBackColor = False
        '
        'frmModule2Score
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(891, 158)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlScore)
        Me.Controls.Add(Me.btnSwitchGames)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.lvwPBP)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frmModule2Score"
        Me.pnlScore.ResumeLayout(False)
        Me.pnlScore.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvwPBP As System.Windows.Forms.ListView
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents EventID As System.Windows.Forms.ColumnHeader
    Friend WithEvents Period As System.Windows.Forms.ColumnHeader
    Friend WithEvents Team As System.Windows.Forms.ColumnHeader
    Friend WithEvents Time As System.Windows.Forms.ColumnHeader
    Friend WithEvents Score As System.Windows.Forms.ColumnHeader
    Friend WithEvents EventCodeID As System.Windows.Forms.ColumnHeader
    Friend WithEvents UniqueID As System.Windows.Forms.ColumnHeader
    Friend WithEvents TeamID As System.Windows.Forms.ColumnHeader
    Friend WithEvents SequenceNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSwitchGames As System.Windows.Forms.Button
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents pnlScore As System.Windows.Forms.Panel
    Friend WithEvents cmbTeam As System.Windows.Forms.ComboBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents txtTimeMod2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblGoal As System.Windows.Forms.Label
    Friend WithEvents btnTime As System.Windows.Forms.Button
End Class
