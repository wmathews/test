﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmAddOfficial

#Region " Constants & Variables "
    Private m_objUtility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objAddOfficial As clsAddOfficial = clsAddOfficial.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_blnIsComboLoaded As Boolean = False
    Dim strNupID As String
    Dim strSelection As String
    Private m_dsAddOfficial As New DataSet
    Private m_strFirstName As String = String.Empty
    Private m_strLastName As String = String.Empty
    Private m_strUniformNumber As String
    Private strUniformNumber As String
    Private MessageDialog As New frmMessageDialog
    Private lsupport As New languagesupport
    Dim strmessage As String = "Please Apply or Ignore the changes"
    Dim strmessage1 As String = "Officials updated Successfully"
    Dim strmessage2 As String = "Officials Added Successfully"
    Dim strmessage3 As String = "Selected official is deleted successfully"
    Dim strmessage4 As String = "This official can not be deleted.The official"
    Dim strmessage5 As String = "is already assigned to the game thru game set-up screen"
    Dim strmessage6 As String = "Official Last Name is Not Entered"
    Dim strmessage7 As String = "Official First Name is Not Entered"
    Dim strmessage8 As String = "League  is Not selected"
    Dim strmessage9 As String = "Select a official first!"
    Dim strmessage10 As String = "Please select valid league"
    Dim strmessage11 As String = "Delete"
    Dim strmessage12 As String = "Are You Sure want to delete this official?"
    Dim strmessage13 As String = "The Official cannot be added at this time. Please check your internet connection and try again after sometime."

#End Region

#Region " Event Handlers "

    Private Sub frmAddOfficial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmAddOfficial", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)

            Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
            strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
            strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
            strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
            strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
            strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
            strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
            strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
            strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)
            strmessage8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage8)
            strmessage9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage9)
            strmessage10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage10)

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                pnlLeagueId.Visible = False
                Me.Height = Me.Height - pnlLeagueId.Height
                pnlOfficial.Location = New Point(0, 40)
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, False)
                lvwOfficial.Enabled = True
                LoadNewlyAddedOfficials()
            Else
                If m_objGameDetails.IsDemoGameSelected = False Then
                    LoadLeagueCombo()
                    CmbLeagueID.SelectedIndex = 0
                    EnableDisableButton(True, False, False, False, False)
                    EnableDisableTextBox(False, False, False)
                    CmbLeagueID.Enabled = False
                    lvwOfficial.Enabled = True
                    m_blnIsComboLoaded = True
                    LoadNewlyAddedOfficials()
                Else
                    pnlLeagueId.Visible = False
                    Me.Height = Me.Height - pnlLeagueId.Height
                    pnlOfficial.Location = New Point(0, 40)
                    EnableDisableButton(True, False, False, False, False)
                    EnableDisableTextBox(False, False, False)
                    lvwOfficial.Enabled = True
                    LoadNewlyAddedOfficials()
                End If

            End If

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                lblFirstName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblFirstName.Text)
                lblLastName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLastName.Text)
                lblLeagueId.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLeagueId.Text)
                btnAdd.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAdd.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnApply.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnApply.Text)
                btnIgnore.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnIgnore.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)

                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Official")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "League")
                lvwOfficial.Columns(0).Text = g1
                lvwOfficial.Columns(1).Text = g2

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnApply.Text, 1, 0)
            Dim dsUniformCount As New DataSet()
            If (RequireFieldValidation()) Then
                InsertNewOfficial()
                LoadNewlyAddedOfficials()
                strSelection = "Add"
                ClearTextBoxes()
                CmbLeagueID.Enabled = False
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, True)
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwOfficial_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwOfficial.Click
        Try
            ListviewSelectedItemDisplay()
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lvwOfficial.SelectedItems(0).Text + " - " + lvwOfficial.SelectedItems(0).SubItems(0).Text, 1, 0)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwOfficial_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvwOfficial.SelectedIndexChanged
        Try
            ListviewSelectedItemDisplay()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnIgnore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            Try
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnIgnore.Text, 1, 0)
                strSelection = "Add"
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, True)
                ClearTextBoxes()
                btnAdd.Focus()
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    If m_objGameDetails.IsDemoGameSelected = False Then
                        CmbLeagueID.Enabled = False
                        CmbLeagueID.SelectedIndex = 0
                    End If
                End If

            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If (strSelection = "Save" Or strSelection = "Update") Then
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnApply.Focus()
            Else
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, 1, 0)
                Me.Close()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region " User Methods "

    Private Sub EnableDisableButton(ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean)
        Try
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
            btnDelete.Enabled = blnButtonDelete
            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableTextBox(ByVal blnTextBoxMoniker As Boolean, ByVal blnTextBoxLastName As Boolean, ByVal blnListViewOfficial As Boolean)
        Try
            txtFirstName.Enabled = blnTextBoxMoniker
            txtLastName.Enabled = blnTextBoxLastName
            lvwOfficial.Enabled = blnListViewOfficial
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadNewlyAddedOfficials()
        Try
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                m_dsAddOfficial = m_objAddOfficial.SelectAddOfficial(m_objGameDetails.LeagueID, m_objGameDetails.SortOrder)
            Else
                m_dsAddOfficial = m_objAddOfficial.SelectAddOfficialDemo(m_objGameDetails.SortOrder)
            End If

            lvwOfficial.Items.Clear()
            If Not m_dsAddOfficial.Tables(0) Is Nothing Then
                For Each drNewOfficial As DataRow In m_dsAddOfficial.Tables(0).Rows
                    Dim lvItem As New ListViewItem(drNewOfficial(0).ToString() + ", " + drNewOfficial(1).ToString())
                    If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                        lvwOfficial.Columns(1).Width = 100
                        lvItem.SubItems.Add(drNewOfficial(5).ToString()) 'LEAGUE ABBREV
                    Else
                        If lvwOfficial.Columns.Count > 1 Then
                            lvwOfficial.Columns(1).Width = 0
                            lvItem.SubItems.Add("") 'LEAGUE ABBREV
                        End If
                    End If
                    'Uniform Number
                    If drNewOfficial(4).ToString <> "" Then
                        lvItem.SubItems.Add(Format(Convert.ToInt32(drNewOfficial(4).ToString()), "00"))
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    lvItem.SubItems.Add(drNewOfficial(2).ToString)
                    lvwOfficial.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearTextBoxes()
        Try
            txtFirstName.Text = ""
            txtLastName.Text = ""
            CmbLeagueID.SelectedValue = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function RequireFieldValidation() As Boolean
        Try
            Dim clsvalid As New Utility.clsValidation

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                If m_objGameDetails.IsDemoGameSelected = False Then
                    If (Not clsvalid.ValidateEmpty(CmbLeagueID.Text)) Then
                        MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        CmbLeagueID.Focus()
                        Return False
                    End If
                End If
            End If

            If txtFirstName.Text = "" Then
                MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtFirstName.Focus()
                Return False
            ElseIf txtLastName.Text = "" Then
                MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtLastName.Focus()
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub DeleteAddOfficial()
        Try
            Dim dsOfficial As New DataSet
            If (MessageDialog.Show(strmessage12, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then

                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    dsOfficial = m_objAddOfficial.DeleteAddOfficial(CInt(strNupID), m_objGameDetails.LeagueID, CChar("N"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    If dsOfficial.Tables.Count > 0 Then
                        If CDbl(dsOfficial.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                            MessageDialog.Show(strmessage4 + " " + strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Else
                            MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            LoadNewlyAddedOfficials()
                        End If
                    End If
                Else

                    If m_objGameDetails.IsDemoGameSelected = False Then
                        dsOfficial = m_objAddOfficial.DeleteAddOfficial(CInt(strNupID), CInt(CmbLeagueID.SelectedValue), CChar("Y"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        If CDbl(dsOfficial.Tables(0).Rows(0).Item(0).ToString) = 1 Then
                            MessageDialog.Show(strmessage4 + " " + strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Else
                            MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            LoadNewlyAddedOfficials()
                        End If
                    Else
                        dsOfficial = m_objAddOfficial.DeleteAddOfficial(CInt(strNupID), m_objGameDetails.LeagueID, CChar("Y"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        If CDbl(dsOfficial.Tables(0).Rows(0).Item(0).ToString) = 1 Then
                            MessageDialog.Show(strmessage4 + " " + strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Else
                            MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            LoadNewlyAddedOfficials()
                        End If
                    End If

                End If
                ClearTextBoxes()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ListviewSelectedItemDisplay()
        Try
            EnableDisableButton(True, True, True, False, False)
            EnableDisableTextBox(False, False, True)
            SelectedListviewDisplay()
            CmbLeagueID.Enabled = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SelectedListviewDisplay()
        Try
            Dim listselectDatarow() As DataRow
            For Each lvwitm As ListViewItem In lvwOfficial.SelectedItems
                strNupID = lvwitm.SubItems(3).Text

            Next
            listselectDatarow = m_dsAddOfficial.Tables(0).Select("REF_ID='" & strNupID & "'")
            If listselectDatarow.Length > 0 Then
                txtFirstName.Text = listselectDatarow(0).Item(1).ToString()
                txtLastName.Text = listselectDatarow(0).Item(0).ToString()
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    CmbLeagueID.SelectedValue = listselectDatarow(0).Item(3).ToString()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadLeagueCombo()
        Try
            Dim dtLeague As New DataTable
            Dim dsLeague As New DataSet
            dsLeague = m_objAddOfficial.GetLeague()
            dtLeague = dsLeague.Tables(0).Copy
            LoadControl(CmbLeagueID, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable
            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub InsertNewOfficial()
        Try
            If strSelection = "Save" Then
                strUniformNumber = CStr(m_objAddOfficial.GetUniform())
                strNupID = "970000" & Format(Convert.ToInt32(strUniformNumber), "00")
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    Try
                        strNupID = CStr(m_objAddOfficial.FetchMaxRefereeNUPID())
                    Catch ex As Exception
                        MessageDialog.Show(strmessage13, "Add Official", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Exit Sub
                    End Try

                    m_objAddOfficial.InsertUpdateOfficial(CInt(strNupID), m_objGameDetails.LeagueID, strUniformNumber, m_objGameDetails.ReporterRole, txtLastName.Text.Trim(), txtFirstName.Text.Trim(), CChar("N"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                Else
                    If m_objGameDetails.IsDemoGameSelected = False Then
                        m_objAddOfficial.InsertUpdateOfficial(CInt(strNupID), CInt(CmbLeagueID.SelectedValue.ToString()), strUniformNumber, m_objGameDetails.ReporterRole, txtLastName.Text.Trim(), txtFirstName.Text.Trim(), CChar("Y"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        CmbLeagueID.SelectedIndex = 0
                        CmbLeagueID.Enabled = False
                    Else
                        m_objAddOfficial.InsertUpdateOfficial(CInt(strNupID), m_objGameDetails.LeagueID, strUniformNumber, m_objGameDetails.ReporterRole, txtLastName.Text.Trim(), txtFirstName.Text.Trim(), CChar("Y"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    End If
                    MessageDialog.Show(strmessage2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            ElseIf strSelection = "Update" Then
                strNupID = lvwOfficial.SelectedItems(0).SubItems(3).Text
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    m_objAddOfficial.InsertUpdateOfficial(CInt(strNupID), m_objGameDetails.LeagueID, strUniformNumber, m_objGameDetails.ReporterRole, txtLastName.Text.Trim(), txtFirstName.Text.Trim(), CChar("N"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                Else
                    If m_objGameDetails.IsDemoGameSelected = False Then
                        m_objAddOfficial.InsertUpdateOfficial(CInt(strNupID), CInt(CmbLeagueID.SelectedValue.ToString()), strUniformNumber, m_objGameDetails.ReporterRole, txtLastName.Text.Trim(), txtFirstName.Text.Trim(), CChar("Y"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    Else
                        m_objAddOfficial.InsertUpdateOfficial(CInt(strNupID), m_objGameDetails.LeagueID, strUniformNumber, m_objGameDetails.ReporterRole, txtLastName.Text.Trim(), txtFirstName.Text.Trim(), CChar("Y"), m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    End If
                End If
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'AUDIT TRIAL
            strSelection = "Save"
            EnableDisableButton(False, False, False, True, True)
            EnableDisableTextBox(True, True, False)
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                If m_objGameDetails.IsDemoGameSelected = False Then
                    CmbLeagueID.Enabled = True
                    CmbLeagueID.Focus()
                    CmbLeagueID.SelectedIndex = 0
                Else
                    txtFirstName.Focus()
                End If
            Else
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAdd.Text, 1, 0)
                txtFirstName.Focus()
            End If
            ClearTextBoxes()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, 1, 0)

            strSelection = "Update"
            EnableDisableButton(False, False, False, True, True)
            EnableDisableTextBox(True, True, False)
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                txtFirstName.Focus()
            Else
                If m_objGameDetails.IsDemoGameSelected = False Then
                    CmbLeagueID.Enabled = True
                    CmbLeagueID.Focus()
                Else
                    txtFirstName.Focus()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'AUDIT TRIAL
            If lvwOfficial.SelectedItems.Count = 0 Then
                MessageDialog.Show(strmessage9, strmessage11, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, 1, 0)
            DeleteAddOfficial()
            EnableDisableButton(True, False, False, False, False)
            EnableDisableTextBox(False, False, True)
            ClearTextBoxes()
            strSelection = "Add"
            lvwOfficial.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtFirstName_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFirstName.KeyPress
        Try
            If Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtFirstName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFirstName.Validated
        Try
            'AUDIT TRAIL
            If txtFirstName.Text <> m_strFirstName Then
                If txtFirstName.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtFirstName.Text, lblFirstName.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtFirstName.Text & "' '", lblFirstName.Text, 1, 0)
                End If
            End If
            m_strFirstName = txtFirstName.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtLastName_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLastName.KeyPress
        Try
            If Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtLastName_Validated1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLastName.Validated
        Try
            'AUDIT TRIAL
            If txtLastName.Text <> m_strLastName Then
                If txtLastName.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtLastName.Text, lblLastName.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtLastName.Text & "' '", lblLastName.Text, 1, 0)
                End If
            End If
            m_strLastName = txtLastName.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbLeagueID_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbLeagueID.Validated
        Try
            If CmbLeagueID.Text <> "" And CmbLeagueID.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbLeagueID.Text = ""
                CmbLeagueID.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class