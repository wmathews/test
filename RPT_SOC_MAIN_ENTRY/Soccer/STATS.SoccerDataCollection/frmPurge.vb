﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmPurge
' Author        : Shravani
' Created Date  : 23-07-09
' Description   : 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmPurge

#Region "Constents and Variables"

    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objUtilAudit As New clsAuditLog

    Private intNDays As Integer
    Private dtDays As Date
    Private dtdBegin As Date
    Private dtdEnd As Date
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private lsupport As New languagesupport
    Private MessageDialog As New frmMessageDialog

    Dim strmessage As String = "Please select a file to delete."
    Dim strmessage1 As String = "No files to Delete"

    Dim strmessage2 As String = "Directory"
    Dim strmessage3 As String = "No files between these dates"
    Dim strmessage4 As String = "Enter valid number of Days [1 - 999]"
    Dim strmessage5 As String = "Enter number of days."
    Dim strmessage6 As String = "Please enter Numbers only..."
    Dim strmessage7 As String = "No files of these days old"



#End Region

#Region "Event Handler"
    Private Sub frmPurge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPurge", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Me.CenterToParent()
            If lsttxtdocs.Items.Count <= 0 Then
                chkAll.Enabled = False
            End If

            If radDays.Checked = True Then
                radBetween.Checked = False
            Else
                If radBetween.Checked = True Then
                    radDays.Checked = False
                End If
            End If
            txtDays.Text = ""
            Application.DoEvents()
            Dim TEMLENINT As Integer
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                TEMLENINT = radDays.Text.Length
                grpAuditTrail.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), grpAuditTrail.Text)
                radDays.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radDays.Text)
                linebreakrdo(radDays, TEMLENINT)

                radBetween.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radBetween.Text)
                lbldays.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lbldays.Text)
                lblbet.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblbet.Text)
                lblto.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblto.Text)
                btnShow.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnShow.Text)
                chkAll.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), chkAll.Text)
                btnPurge.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPurge.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub linebreakrdo(ByVal lbtemp As RadioButton, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtDays_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDays.KeyPress
        Try
            Dim keyAscii As Integer = CInt(Asc(e.KeyChar))
            Select Case keyAscii
                Case 8
                    keyAscii = 1
                Case 48 To 57
                    keyAscii = 1
                Case Else
                    keyAscii = 0
            End Select
            If keyAscii = 0 Then
                e.Handled = True
                MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            Else
                e.Handled = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radBetween_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBetween.CheckedChanged
        Try

            chkAll.Enabled = False
            If radBetween.Checked = True Then
                txtDays.Text = Nothing
                txtDays.Enabled = False
                lsttxtdocs.Items.Clear()
                dtpBeginDate.Enabled = True
                dtpEndDate.Enabled = True
            Else
                txtDays.Enabled = True
                lsttxtdocs.Items.Clear()
                dtpBeginDate.Enabled = False
                dtpEndDate.Enabled = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radDays_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDays.CheckedChanged
        Try


            chkAll.Enabled = False
            If radDays.Checked = True Then
                txtDays.Enabled = True
                lsttxtdocs.Items.Clear()
                dtpBeginDate.Enabled = False
                dtpEndDate.Enabled = False
            Else
                radDays.Checked = False
                txtDays.Text = Nothing
                lsttxtdocs.Items.Clear()
                txtDays.Enabled = False
                dtpBeginDate.Enabled = True
                dtpEndDate.Enabled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        Try
            If chkAll.Checked Then
                For intIndex As Integer = 0 To lsttxtdocs.Items.Count - 1
                    lsttxtdocs.SetItemCheckState(intIndex, CheckState.Checked)
                Next
            End If
            If (lsttxtdocs.CheckedItems.Count = lsttxtdocs.Items.Count) Then
                If Not chkAll.Checked Then
                    For intLoop As Integer = 0 To lsttxtdocs.Items.Count - 1
                        lsttxtdocs.SetItemCheckState(intLoop, CheckState.Unchecked)
                    Next
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub




    Private Sub btnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShow.Click
        Try

            'objUtilAudit.AuditLog(objFootballPropertise.HomeTeamAbbrev, objFootballPropertise.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnShow.Text, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnShow.Text, Nothing, 1, 0)
            chkAll.Checked = False
            lsttxtdocs.Items.Clear()
            If radBetween.Checked = True Then
                dtdBegin = dtpBeginDate.Value.Date
                dtdEnd = dtpEndDate.Value.Date
                txtfileInfo(dtdBegin, dtdEnd)
                Exit Sub
            End If
            If radDays.Checked = True Then
                If txtDays.Text = "" Then
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                ElseIf (txtDays.Text.Length) > 3 Then
                    MessageDialog.Show(strmessage4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    txtDays.Text = ""
                    txtDays.Focus()
                    Exit Sub
                Else

                    intNDays = CInt(txtDays.Text)
                    If intNDays >= 0 Then
                        txtfileInfo(intNDays)
                        Exit Sub '
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lsttxtdocs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsttxtdocs.SelectedIndexChanged
        Try
            If (lsttxtdocs.CheckedItems.Count <> lsttxtdocs.Items.Count) Then
                chkAll.Checked = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radDays_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDays.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radDays.Text, Nothing, 1, 0)
            'objUtilAudit.AuditLog(objFootballPropertise.HomeTeamAbbrev, objFootballPropertise.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radDays.Text, 1, 0)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radBetween_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBetween.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radBetween.Text, Nothing, 1, 0)
            'objUtilAudit.AuditLog(objFootballPropertise.HomeTeamAbbrev, objFootballPropertise.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radBetween.Text, 1, 0)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region "User Defined Functions"

    Private Sub txtfileInfo(ByVal intDay As Integer)
        Try
            Dim path As String = ""
            path = m_objGameDetails.AppPath
            path = path + "\SoccerDataCollection\AuditTrail"

            Dim dtrInfo As DirectoryInfo = New DirectoryInfo(path)
            If (dtrInfo.Exists) Then
                dtDays = CType(Now.AddDays(-intDay).Date.ToString, Date)
                Dim flnFileInfo As FileInfo
                For Each flnFileInfo In dtrInfo.GetFiles("*.txt")
                    Dim name As [String] = flnFileInfo.Name
                    Dim MyStamp As Date = FileDateTime(flnFileInfo.FullName)
                    Dim strDate As String = FormatDateTime(MyStamp, DateFormat.ShortDate)
                    If CDate(strDate) <= dtDays Then
                        lsttxtdocs.Items.Add(name)
                    End If
                Next flnFileInfo
                If lsttxtdocs.Items.Count <= 0 Then
                    MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    chkAll.Enabled = False

                Else
                    chkAll.Enabled = True

                End If
            Else
                MessageDialog.Show(strmessage2 + " " & "'" & path & "'  " & " not found in your system", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtfileInfo(ByVal dtBegin As Date, ByVal dtEnd As Date)
        Try
            Dim path As String = ""
            lsttxtdocs.Items.Clear()
            dtdBegin = dtBegin
            dtdEnd = dtEnd
            path = m_objGameDetails.AppPath
            path = path + "\SoccerDataCollection\AuditTrail"

            Dim dtrInfo As DirectoryInfo = New DirectoryInfo(path)
            If (dtrInfo.Exists) Then
                Dim flnFileInfo As FileInfo
                For Each flnFileInfo In dtrInfo.GetFiles("*.txt")
                    Dim name As [String] = flnFileInfo.Name
                    Dim MyStamp As Date = FileDateTime(flnFileInfo.FullName)
                    Dim strDate As String = FormatDateTime(MyStamp, DateFormat.ShortDate)
                    If CDate(strDate) >= dtdBegin And CDate(strDate) <= dtdEnd Then
                        lsttxtdocs.Items.Add(name)
                    End If
                Next flnFileInfo
                If lsttxtdocs.Items.Count <= 0 Then
                    MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    chkAll.Enabled = False

                Else
                    chkAll.Enabled = True

                End If
            Else
                MessageDialog.Show(strmessage2 + " " & "'" & path & "'  " & " not found in your system", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DeleteFile(ByVal intDay As Integer)
        Try
            Application.DoEvents()
            If lsttxtdocs.Items.Count > 0 And Not (lsttxtdocs.CheckedItems.Count > 0) Then
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If (lsttxtdocs.CheckedItems.Count > 0) Then
                Dim Buttons As MessageDialogButtons = MessageDialogButtons.YesNo
                Dim strMessage, strSucessmessage As String
                If (lsttxtdocs.CheckedItems.Count = 1) Then
                    strMessage = "Are you sure you want to delete the selected file"
                    strSucessmessage = "Selected file is deleted successfully."
                ElseIf lsttxtdocs.CheckedItems.Count > 1 Then
                    strMessage = "Are you sure you want to delete the selected files"
                    strSucessmessage = "Selected files are deleted successfully."
                End If

                If (MessageDialog.Show(strMessage, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes) Then
                    Application.DoEvents()
                    DeleteSelectedFile()
                    MessageDialog.Show(strSucessmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    If chkAll.Checked = True Then
                        chkAll.Checked = False
                        lsttxtdocs.Items.Clear()
                        Exit Sub
                    Else
                        lsttxtdocs.Items.Clear()
                        txtfileInfo(intNDays)
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DeleteSelectedFile()
        Try
            Dim path As String = ""
            path = m_objGameDetails.AppPath
            path = path + "\SoccerDataCollection\AuditTrail"
            Dim objList As Object = Nothing
            For intIndex As Integer = 0 To lsttxtdocs.CheckedItems.Count - 1
                Dim strfullpath As String = path & "\"
                objList = lsttxtdocs.CheckedItems.Item(intIndex).ToString
                strfullpath &= objList.ToString
                File.Delete(strfullpath)
                objList = Nothing
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DeleteFile(ByVal dtBegin As Date, ByVal dtEnd As Date)
        Try
            If lsttxtdocs.Items.Count < 1 Then
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If (lsttxtdocs.CheckedItems.Count > 0) Then
                Dim strMessage, strSucessmessage As String
                If (lsttxtdocs.CheckedItems.Count = 1) Then
                    strMessage = "Are you sure you want to delete the selected file"
                    strSucessmessage = "Selected file is deleted successfully."
                ElseIf lsttxtdocs.CheckedItems.Count > 1 Then
                    strMessage = "Are you sure you want to delete the selected files"
                    strSucessmessage = "Selected files are deleted successfully."
                End If
                Dim Buttons As MessageDialogButtons = MessageDialogButtons.YesNo
                If (MessageDialog.Show(strMessage, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes) Then
                    Application.DoEvents()
                    DeleteSelectedFile()
                    MessageDialog.Show(strSucessmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    If chkAll.Checked = True Then
                        chkAll.Checked = False
                        lsttxtdocs.Items.Clear()
                        Exit Sub
                    Else
                        lsttxtdocs.Items.Clear()
                        txtfileInfo(dtdBegin, dtdEnd)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region





    Private Sub btnPurge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPurge.Click
        Try

            'objUtilAudit.AuditLog(objFootballPropertise.HomeTeamAbbrev, objFootballPropertise.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPurge.Text, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPurge.Text, Nothing, 1, 0)
            Application.DoEvents()
            If lsttxtdocs.Items.Count <= 0 Then
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If radDays.Checked = True Then
                If lsttxtdocs.Items.Count > 0 And lsttxtdocs.CheckedItems.Count < 1 Then
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                Else
                    DeleteFile(intNDays)
                    Exit Sub
                End If
            End If
            If radBetween.Checked = True Then
                If lsttxtdocs.Items.Count > 0 And lsttxtdocs.CheckedItems.Count < 1 Then
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                Else
                    DeleteFile(dtdBegin, dtdEnd)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtDays_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDays.Leave
        Try
            If (txtDays.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtDays.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class