﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmAddDemoPlayer

#Region "Constants Variable"
    Private m_dsNewAddPlayer As New DataSet
    Private m_strNupId As String
    Private m_strCheckButtonOption As String
    Private m_dsPosition As New DataSet
    Private m_strPosition As String
    Private m_blnLeague As Boolean = False
    Private intTabindex As Integer
    Private intTabind As Integer
    Private m_blnCboValidate As Boolean = False
    Private m_blnIsPositionComboLoaded As Boolean = False
    Private m_objAddPlayer As New clsAddDemoPlayer
    Private MessageDialog As New frmMessageDialog
#End Region

    Private Sub frmAddDemoPlayer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            LoadLeagueCombo()
            cmbLeagueId.SelectedIndex = 0
            ClearTextboxes()
            EnableDisableButton(False, False, False, True, False)
            EnableDisableTextBox(False)
            cmbLeagueId.Enabled = False
            lvwPlayer.Enabled = True
            LoadNewlyAddedPlayers()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub LoadLeagueCombo()
        Try
            Dim dtLeague As New DataTable
            Dim dsLeague As New DataSet
            dsLeague = m_objAddPlayer.GetLeague()
            dtLeague = dsLeague.Tables(0).Copy
            LoadControl(cmbLeagueId, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
            m_blnLeague = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable
            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearTextboxes()
        Try
            txtFirstname.Text = ""
            txtlastname.Text = ""
            txtUniformnumber.Text = ""
            cmbTeam.SelectedIndex = -1
            cmbPosition.SelectedIndex = -1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableTextBox(ByVal type As Boolean)
        Try
            txtFirstname.Enabled = type
            txtlastname.Enabled = type
            txtUniformnumber.Enabled = type
            cmbLeagueId.Enabled = type
            cmbTeam.Enabled = type
            cmbPosition.Enabled = type
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableButton(ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonNewPlayer As Boolean, ByVal blnButtonEdit As Boolean)
        Try
            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
            btnDelete.Enabled = blnButtonDelete
            btnAdd.Enabled = blnButtonNewPlayer
            btnEdit.Enabled = blnButtonEdit
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadNewlyAddedPlayers()
        Try
            lvwPlayer.Items.Clear()
            m_dsNewAddPlayer = m_objAddPlayer.SelectAddPlayer()
            If Not m_dsNewAddPlayer.Tables(0) Is Nothing Then
                For Each drNewPlayer As DataRow In m_dsNewAddPlayer.Tables(0).Rows
                    Dim lvItem As New ListViewItem(drNewPlayer(3).ToString() + ", " + drNewPlayer(4).ToString())
                    lvItem.SubItems.Add(drNewPlayer(1).ToString)
                    If (drNewPlayer(7).ToString = Nothing) Then
                        lvItem.SubItems.Add(drNewPlayer(6).ToString())
                    Else
                        lvItem.SubItems.Add(drNewPlayer(7).ToString())
                    End If
                    lvItem.SubItems.Add(drNewPlayer(8).ToString)
                    lvItem.SubItems.Add(drNewPlayer(9).ToString)
                    lvItem.SubItems.Add(drNewPlayer(2).ToString)
                    lvwPlayer.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbLeagueId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLeagueId.SelectedIndexChanged
        Try
            If m_blnLeague = True Then
                LoadNewlyAddedPlayers()
                LoadTeamAndPosition(CInt(cmbLeagueId.SelectedValue))
                m_blnIsPositionComboLoaded = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub LoadTeamAndPosition(ByVal LeagueID As Integer)
        Try
            Dim dsTeamPosition As New DataSet
            dsTeamPosition = m_objAddPlayer.GetTeamAndPosition(LeagueID)

            Dim dtTeam, dtPosition As New DataTable
            dtTeam = dsTeamPosition.Tables(0).Copy
            dtPosition = dsTeamPosition.Tables(1).Copy

            LoadControl(cmbTeam, dtTeam, "TEMNAME", "TEAM_ID")
            LoadControl(cmbPosition, dtPosition, "POSITION", "POSITION_ID")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtUniformnumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUniformnumber.KeyPress
        Try
            If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lvwPlayer.Enabled = False
            EnableDisableTextBox(True)
            cmbLeagueId.Enabled = False
            m_strCheckButtonOption = "Update"
            EnableDisableButton(True, True, False, False, False)
            cmbLeagueId.Enabled = True
            cmbTeam.Enabled = True
            cmbLeagueId.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtlastname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlastname.KeyPress
        Try
            If (Microsoft.VisualBasic.Asc(e.KeyChar) < 65) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 90) And (Microsoft.VisualBasic.Asc(e.KeyChar) < 97) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 122) Then
                'Allowed space
                If (Microsoft.VisualBasic.Asc(e.KeyChar) <> 32) Then
                    e.Handled = True
                End If
            End If
            ' Allowed backspace
            If (Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 39 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 45) Then
                e.Handled = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtlastname_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtlastname.Validated
        Try
            If (txtlastname.Text <> "") Then
                txtlastname.Text = txtlastname.Text.Substring(0, 1).ToUpper() & txtlastname.Text.Substring(1)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtFirstname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFirstname.KeyPress
        Try
            If (Microsoft.VisualBasic.Asc(e.KeyChar) < 65) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 90) And (Microsoft.VisualBasic.Asc(e.KeyChar) < 97) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 122) Then
                'Allowed space
                If (Microsoft.VisualBasic.Asc(e.KeyChar) <> 32) Then
                    e.Handled = True
                End If
            End If
            ' Allowed backspace
            If (Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 39 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 45) Then
                e.Handled = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtFirstname_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFirstname.Validated
        Try
            If (txtFirstname.Text <> "") Then
                txtFirstname.Text = txtFirstname.Text.Substring(0, 1).ToUpper() & txtFirstname.Text.Substring(1)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Function RequireFieldValidation() As Boolean
        Try
            Dim clsvalid As New Utility.clsValidation

            If (Not clsvalid.ValidateEmpty(cmbLeagueId.Text)) Then
                MessageDialog.Show("League  is Not selected", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLeagueId.Focus()
                Return False
                'ElseIf (Not clsvalid.ValidateEmpty(txtFirstname.Text)) Then
                '    MessageDialog.Show("Player First Name is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                '    txtFirstname.Focus()
                '    Return False
            ElseIf (Not clsvalid.ValidateEmpty(txtlastname.Text)) Then
                MessageDialog.Show("Player Last Name is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtlastname.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbTeam.Text)) Then
                MessageDialog.Show("Select a Team to Which the player Belongs", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbTeam.Focus()
                cmbTeam.SelectedIndex = -1
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(txtUniformnumber.Text)) Then
                MessageDialog.Show("Uniform Number is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtUniformnumber.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbPosition.Text)) Then
                MessageDialog.Show("Select Player Position", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbPosition.Focus()
                cmbPosition.SelectedIndex = -1
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub lvwPlayer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPlayer.Click
        Try
            EnableDisableButton(False, False, True, True, True)
            lvwPlayer.Select()
            SelectedListviewDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwPlayer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPlayer.SelectedIndexChanged
        Try
            EnableDisableButton(False, False, True, True, True)
            SelectedListviewDisplay()
            'For i As Integer = 0 To lvwPlayer.Items.Count - 1
            '    If CInt(m_strNupId) = CInt(lvwPlayer.Items(i).SubItems(5).Text) Then
            '        lvwPlayer.Items(i).Selected = True
            '        lvwPlayer.Select()
            '        Me.lvwPlayer.EnsureVisible(i)
            '        Exit For
            '    End If
            'Next
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub SelectedListviewDisplay()
        Try
            Dim listselectDatarow() As DataRow
            For Each lvwitm As ListViewItem In lvwPlayer.SelectedItems
                m_strNupId = lvwitm.SubItems(5).Text
            Next

            listselectDatarow = m_dsNewAddPlayer.Tables(0).Select("PLAYER_ID='" & m_strNupId & "'")
            If listselectDatarow.Length > 0 Then
                cmbTeam.SelectedValue = listselectDatarow(0).Item(0).ToString()
                txtFirstname.Text = listselectDatarow(0).Item(3).ToString()
                txtlastname.Text = listselectDatarow(0).Item(4).ToString()
                cmbPosition.SelectedValue = listselectDatarow(0).Item(5).ToString()
                If (listselectDatarow(0).Item(7).ToString() = Nothing) Then
                    txtUniformnumber.Text = listselectDatarow(0).Item(6).ToString()
                Else
                    txtUniformnumber.Text = listselectDatarow(0).Item(7).ToString()
                End If
                cmbLeagueId.SelectedValue = listselectDatarow(0).Item(9).ToString()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbTeam_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTeam.Validated
        Try
            If cmbTeam.Text <> "" And cmbTeam.SelectedValue Is Nothing Then
                MessageDialog.Show("Please Select Valid " & lblTeam.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbTeam.Text = ""
                cmbTeam.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub cmbPosition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPosition.SelectedIndexChanged
        Try
            If m_blnIsPositionComboLoaded = True Then
                m_blnIsPositionComboLoaded = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmbPosition_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPosition.Validated
        Try
            If cmbPosition.Text <> "" And cmbPosition.SelectedValue Is Nothing Then
                MessageDialog.Show("Please Select Valid " & lblPosition.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbPosition.Text = ""
                cmbPosition.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ClearTextboxes()
            m_strCheckButtonOption = "Save"
            lvwPlayer.Enabled = False
            EnableDisableButton(True, True, False, False, False)
            cmbLeagueId.Enabled = True
            cmbLeagueId.SelectedIndex = 0
            cmbLeagueId.Focus()
            EnableDisableTextBox(True)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show("Please Apply or Ignore the changes", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
                Exit Sub
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnIgnore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            ClearTextboxes()
            EnableDisableTextBox(False)
            EnableDisableButton(False, False, False, True, False)
            lvwPlayer.Enabled = True
            m_strCheckButtonOption = ""
            cmbLeagueId.SelectedIndex = 0
            cmbLeagueId.Enabled = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Dim dsAddnewplayer As New DataSet
        Dim dsUpdatePlayer As New DataSet
        Dim clsvalid As New Utility.clsValidation
        Try
            If (RequireFieldValidation()) Then
                lvwPlayer.Enabled = True
                If (m_strCheckButtonOption = "Save") Then
                    Dim listselectDatarow() As DataRow
                    listselectDatarow = m_dsNewAddPlayer.Tables(0).Select("UNIFORM='" & txtUniformnumber.Text & "' AND TEAM_ID= '" & cmbTeam.SelectedValue.ToString() & "'")
                    If (listselectDatarow.Length >= 1) Then
                        MessageDialog.Show("Uniform Number Already Exists", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        txtUniformnumber.Focus()
                        Exit Sub
                    End If

                    Dim strUniform As String
                    If txtUniformnumber.Text.Length = 2 Then
                        If txtUniformnumber.Text.Substring(0, 1) = "0" Then
                            If txtUniformnumber.Text.Substring(0, 2) = "00" Then
                                strUniform = "0"
                            Else
                                strUniform = txtUniformnumber.Text.Substring(1, 1)
                            End If
                        Else
                            strUniform = txtUniformnumber.Text
                        End If
                    Else
                        strUniform = txtUniformnumber.Text
                    End If

                    m_strNupId = "99" & Format(Convert.ToInt32(cmbTeam.SelectedValue.ToString.Trim), "0000") & Format(Convert.ToInt32(txtUniformnumber.Text.Trim), "00")
                    m_objAddPlayer.InsertAddPlayer(CInt(m_strNupId), txtlastname.Text.Trim(), txtFirstname.Text.Trim(), CInt(cmbLeagueId.SelectedValue.ToString()), Integer.Parse(cmbTeam.SelectedValue.ToString()), strUniform.Trim(), strUniform.Trim(), Integer.Parse(cmbPosition.SelectedValue.ToString()))
                    MessageDialog.Show("Player Details Added Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)  '7th oct
                    cmbLeagueId.Enabled = False
                    cmbLeagueId.SelectedIndex = 0
                    LoadNewlyAddedPlayers()
                    ClearTextboxes()
                    cmbLeagueId.SelectedIndex = 0
                    EnableDisableTextBox(False)
                    txtFirstname.Focus()
                    EnableDisableButton(False, False, False, True, False)
                    m_strCheckButtonOption = ""
                ElseIf (m_strCheckButtonOption = "Update") Then
                    'm_strNupId = lvwPlayer.SelectedItems(0).SubItems(5).Text
                    m_objAddPlayer.UpdateAddPlayer(CInt(cmbTeam.SelectedValue.ToString()), CInt(cmbLeagueId.SelectedValue.ToString()), txtFirstname.Text, txtlastname.Text.Trim(), CInt(m_strNupId), txtUniformnumber.Text.Trim(), Integer.Parse(cmbPosition.SelectedValue.ToString().Trim()))
                    MessageDialog.Show("Player Details Updated Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information) '7th oct
                    cmbLeagueId.Enabled = False
                    cmbLeagueId.SelectedIndex = 0
                    cmbPosition.SelectedIndex = 0
                    ClearTextboxes()

                    LoadNewlyAddedPlayers()
                    EnableDisableTextBox(False)
                    txtFirstname.Focus()
                    EnableDisableButton(False, False, False, True, False)
                    m_strCheckButtonOption = ""
                End If
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            lvwPlayer.Enabled = False
            If (MessageDialog.Show("Are you sure you want to delete?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                m_strNupId = lvwPlayer.SelectedItems(0).SubItems(5).Text

                If m_objAddPlayer.DeleteAddPlayer(CInt(m_strNupId), CInt(cmbTeam.SelectedValue), CInt(cmbLeagueId.SelectedValue)) > 0 Then
                    MessageDialog.Show("Player Deleted Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Else
                    MessageDialog.Show("This player can not be deleted. The player is already assigned to the game through PBP screen", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If
                EnableDisableTextBox(False)
                EnableDisableButton(True, False, False, False, False)
                LoadNewlyAddedPlayers()
                ClearTextboxes()
                cmbPosition.SelectedIndex = 0
                lvwPlayer.Enabled = True
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLeagueId_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLeagueId.Validated
        Try
            If cmbLeagueId.Text <> "" And cmbLeagueId.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid league", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLeagueId.Text = ""
                cmbLeagueId.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class