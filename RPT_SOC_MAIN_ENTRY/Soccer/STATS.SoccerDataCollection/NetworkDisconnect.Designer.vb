﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NetworkDisconnect
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NetworkDisconnect))
        Me.btnClose = New System.Windows.Forms.Button()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.picNetworkDisconnect = New System.Windows.Forms.PictureBox()
        Me.lblCheckConnection = New System.Windows.Forms.Label()
        Me.chkDoNotShow = New System.Windows.Forms.CheckBox()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        CType(Me.picNetworkDisconnect, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(222, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 221)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(547, 21)
        Me.sstMain.TabIndex = 15
        Me.sstMain.Text = "StatusStrip1"
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-167, -3)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(714, 20)
        Me.picTopBar.TabIndex = 294
        Me.picTopBar.TabStop = False
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-163, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(710, 28)
        Me.picReporterBar.TabIndex = 295
        Me.picReporterBar.TabStop = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 176)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(547, 46)
        Me.picButtonBar.TabIndex = 14
        '
        'picNetworkDisconnect
        '
        Me.picNetworkDisconnect.Image = CType(resources.GetObject("picNetworkDisconnect.Image"), System.Drawing.Image)
        Me.picNetworkDisconnect.Location = New System.Drawing.Point(12, 46)
        Me.picNetworkDisconnect.Name = "picNetworkDisconnect"
        Me.picNetworkDisconnect.Size = New System.Drawing.Size(72, 72)
        Me.picNetworkDisconnect.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picNetworkDisconnect.TabIndex = 301
        Me.picNetworkDisconnect.TabStop = False
        '
        'lblCheckConnection
        '
        Me.lblCheckConnection.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblCheckConnection.ForeColor = System.Drawing.Color.Red
        Me.lblCheckConnection.Location = New System.Drawing.Point(90, 46)
        Me.lblCheckConnection.Name = "lblCheckConnection"
        Me.lblCheckConnection.Size = New System.Drawing.Size(445, 79)
        Me.lblCheckConnection.TabIndex = 302
        Me.lblCheckConnection.Text = "There are few actions that are not processed to Server for last few minutes. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ch" & _
    "eck your Network connection. If it is up, then try from menu Help -> Restart Dat" & _
    "async component. "
        Me.lblCheckConnection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCheckConnection.UseCompatibleTextRendering = True
        '
        'chkDoNotShow
        '
        Me.chkDoNotShow.AutoSize = True
        Me.chkDoNotShow.Location = New System.Drawing.Point(111, 128)
        Me.chkDoNotShow.Name = "chkDoNotShow"
        Me.chkDoNotShow.Size = New System.Drawing.Size(186, 19)
        Me.chkDoNotShow.TabIndex = 303
        Me.chkDoNotShow.Text = "Do not show this message again."
        Me.chkDoNotShow.UseVisualStyleBackColor = True
        '
        'NetworkDisconnect
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(547, 242)
        Me.Controls.Add(Me.chkDoNotShow)
        Me.Controls.Add(Me.lblCheckConnection)
        Me.Controls.Add(Me.picNetworkDisconnect)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "NetworkDisconnect"
        Me.Text = "NetworkDisconnected"
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        CType(Me.picNetworkDisconnect, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents picNetworkDisconnect As System.Windows.Forms.PictureBox
    Friend WithEvents lblCheckConnection As System.Windows.Forms.Label
    Friend WithEvents chkDoNotShow As System.Windows.Forms.CheckBox
End Class
