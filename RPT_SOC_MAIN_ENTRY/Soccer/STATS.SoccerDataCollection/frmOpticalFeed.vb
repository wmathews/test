﻿Imports STATS.SoccerBL
Imports STATS.Utility

Public Class frmOpticalFeed

    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objSportVU As clsSportVU = clsSportVU.GetInstance()
    Private m_objModule1PBP As clsModule1PBP = clsModule1PBP.GetInstance
    Private m_objUtility As New clsUtility
    Private m_objTouch As clsTouches = clsTouches.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_IsFormCloseOccured As Boolean = False
    Private m_objfrmModule As New frmModule1PBP
    Private m_dsScores As New DataSet
    Private mstring As String = "Optical event has been saved successfully"
    Private lsupport As New languagesupport
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()



    Private Sub frmOpticalFeed_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        If m_IsFormCloseOccured = False Then
            Me.Close()
        End If
    End Sub

    Private Sub frmOpticalFeed_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If m_objGameDetails.ModuleID = 1 Then
            'If (m_objGameDetails.OpticalEvent > 0) Or (Not m_objGameDetails.OpticalPBPAccepted Is Nothing) Then
            frmMain.DisplayOpticalData()
            'frmModule1PBP.DisplayOpticalData()
            'End If
            'ElseIf m_objGameDetails.ModuleID = 3 Then
            '    frmMain.DisplayOpticalData()
        End If
        m_IsFormCloseOccured = True
    End Sub

    Private Sub frmOpticalFeed_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If CInt(m_objclsGameDetails.languageid) <> 1 Then

                mstring = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), mstring)
            End If

            If m_objGameDetails.ModuleID = 1 Then
                If m_objGameDetails.OpticalData.Tables.Count > 0 Then
                    If m_objGameDetails.OpticalData.Tables(0).Rows.Count > 0 Then
                        dgvPBP.Rows.Clear()
                        Dim i As Integer
                        For Each drPBP As DataRow In m_objGameDetails.OpticalData.Tables(0).Rows

                            dgvPBP.Rows.Add()
                            dgvPBP.Rows(i).Cells(0).Value = clsUtility.ConvertSecondToMinute(Format(drPBP.Item("TIME_ELAPSED"), "00.00"), False)
                            dgvPBP.Rows(i).Cells(0).Selected = False
                            dgvPBP.Rows(i).Cells(1).Value = drPBP.Item("EVENT")
                            dgvPBP.Rows(i).Cells(1).Selected = False
                            dgvPBP.Rows(i).Cells(2).Value = drPBP.Item("TEAM_ABBREV")
                            dgvPBP.Rows(i).Cells(2).Selected = False
                            dgvPBP.Rows(i).Cells(3).Value = drPBP.Item("PLAYER_NAME")
                            dgvPBP.Rows(i).Cells(3).Selected = False
                            dgvPBP.Rows(i).Cells(4).Value = "Fill"
                            dgvPBP.Rows(i).Cells(4).Tag = drPBP.Item("UNIQUE_ID")
                            dgvPBP.Rows(i).Cells(5).Value = "Auto Save"
                            dgvPBP.Rows(i).Cells(5).Tag = drPBP.Item("UNIQUE_ID")
                            dgvPBP.Rows(i).Cells(6).Value = "Ignore"
                            dgvPBP.Rows(i).Cells(6).Tag = drPBP.Item("UNIQUE_ID")
                            'If CChar(drPBP.Item("USER_ACTION").ToString) = "I" Then
                            '    dgvPBP.Rows(i).DefaultCellStyle.BackColor = Color.Pink
                            'ElseIf CChar(drPBP.Item("USER_ACTION").ToString) = "S" Then
                            '    dgvPBP.Rows(i).DefaultCellStyle.BackColor = Color.DarkSeaGreen 'LightBlue  'Honeydew 'MintCream '
                            'End If
                            i = i + 1

                        Next
                    Else
                        dgvPBP.Visible = False
                        Me.Close()
                    End If
                End If
            ElseIf m_objGameDetails.ModuleID = 3 Then
                If m_objGameDetails.OpticalTouchData.Tables.Count > 0 Then
                    If m_objGameDetails.OpticalTouchData.Tables(0).Rows.Count > 0 Then
                        dgvPBP.Rows.Clear()
                        Dim i As Integer
                        For Each drTouch As DataRow In m_objGameDetails.OpticalTouchData.Tables(0).Rows

                            dgvPBP.Rows.Add()
                            dgvPBP.Rows(i).Cells(0).Value = clsUtility.ConvertSecondToMinute(Format(drTouch.Item("TIME_ELAPSED"), "00.00"), False)
                            dgvPBP.Rows(i).Cells(0).Selected = False
                            dgvPBP.Rows(i).Cells(1).Value = drTouch.Item("EVENT")
                            dgvPBP.Rows(i).Cells(1).Selected = False
                            dgvPBP.Rows(i).Cells(2).Value = drTouch.Item("TEAM_ABBREV")
                            dgvPBP.Rows(i).Cells(2).Selected = False
                            dgvPBP.Rows(i).Cells(3).Value = drTouch.Item("PLAYER_NAME")
                            dgvPBP.Rows(i).Cells(3).Selected = False
                            dgvPBP.Rows(i).Cells(4).Value = "Fill"
                            dgvPBP.Rows(i).Cells(4).Tag = drTouch.Item("UNIQUE_ID")
                            dgvPBP.Rows(i).Cells(5).Value = "Auto Save"
                            dgvPBP.Rows(i).Cells(5).Tag = drTouch.Item("UNIQUE_ID")
                            dgvPBP.Rows(i).Cells(6).Value = "Ignore"
                            dgvPBP.Rows(i).Cells(6).Tag = drTouch.Item("UNIQUE_ID")
                            i = i + 1
                        Next
                    Else
                        dgvPBP.Visible = False
                        Me.Close()
                    End If
                End If

            End If


            dgvPBP.Focus()
            ' Me.Location = C

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgvPBP_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPBP.CellClick
        Try
            If e.ColumnIndex = 4 Then 'Fill
                If m_objGameDetails.ModuleID = 1 Then
                    m_objGameDetails.OpticalEvent = dgvPBP.Rows(e.RowIndex).Cells(4).Tag
                ElseIf m_objGameDetails.ModuleID = 3 Then
                    m_objGameDetails.OpticalTouchEvent = dgvPBP.Rows(e.RowIndex).Cells(4).Tag
                End If
                'Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            ElseIf e.ColumnIndex = 5 Then 'Save
                Dim UniqueID As Integer = dgvPBP.Rows(e.RowIndex).Cells(4).Tag
                If m_objGameDetails.ModuleID = 1 Then
                    InsertOpticalPBPData(UniqueID)
                    MessageDialog.Show(mstring, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    m_objSportVU.UpdateProcessedOpticalPBP(m_objGameDetails.GameCode, UniqueID, "S")
                    m_objfrmModule.BringToFront()
                    m_objGameDetails.OpticalData.Clear()
                    'LoadOpticalPBP()
                    m_objGameDetails.OpticalEvent = 0
                ElseIf m_objGameDetails.ModuleID = 3 Then
                    InsertOpticalTouchData(UniqueID)
                    MessageDialog.Show(mstring, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    frmMain.BringToFront()
                    m_objSportVU.UpdateProcessedOpticalTouch(m_objGameDetails.GameCode, UniqueID)
                    m_objGameDetails.OpticalTouchData.Clear()
                    m_objGameDetails.OpticalTouchEvent = 0
                End If
                Me.Close()
            ElseIf e.ColumnIndex = 6 Then 'Ignore
                Dim strUniqueIDs As String = dgvPBP.Rows(e.RowIndex).Cells(4).Tag
                If m_objGameDetails.ModuleID = 1 Then
                    m_objSportVU.UpdateProcessedOpticalPBP(m_objGameDetails.GameCode, strUniqueIDs, "I")
                    m_objGameDetails.OpticalData.Clear()
                    m_objGameDetails.OpticalEvent = 0
                    'm_objGameDetails.IsSportDataIgnore = True
                ElseIf m_objGameDetails.ModuleID = 3 Then
                    m_objSportVU.UpdateProcessedOpticalTouch(m_objGameDetails.GameCode, strUniqueIDs)
                    m_objGameDetails.OpticalTouchData.Clear()
                    m_objGameDetails.OpticalTouchEvent = 0
                End If
                Me.Close()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            'm_objGameDetails.IsOpticalFeedClosed = True
        End Try
    End Sub

    Private Sub LoadOpticalPBP()
        Try
            dgvPBP.Rows.Clear()
            'm_objGameDetails.OpticalData = m_objSportVU.GetOpticalPBPData(m_objGameDetails.GameCode)

            If m_objGameDetails.OpticalData.Tables.Count > 0 Then
                If m_objGameDetails.OpticalData.Tables(0).Rows.Count > 0 Then

                    Dim i As Integer
                    For Each drPBP As DataRow In m_objGameDetails.OpticalData.Tables(0).Rows
                        dgvPBP.Rows.Add()
                        dgvPBP.Rows(i).Cells(0).Value = Format(drPBP.Item("TIME_ELAPSED"), "00.00")
                        dgvPBP.Rows(i).Cells(1).Value = drPBP.Item("EVENT")
                        dgvPBP.Rows(i).Cells(2).Value = drPBP.Item("TEAM_ABBREV")
                        dgvPBP.Rows(i).Cells(3).Value = drPBP.Item("PLAYER_NAME")
                        dgvPBP.Rows(i).Cells(4).Value = "Fill"
                        dgvPBP.Rows(i).Cells(4).Tag = drPBP.Item("UNIQUE_ID")
                        dgvPBP.Rows(i).Cells(5).Value = "Auto Save"
                        dgvPBP.Rows(i).Cells(5).Tag = drPBP.Item("UNIQUE_ID")
                        dgvPBP.Rows(i).Cells(6).Value = "Ignore"
                        dgvPBP.Rows(i).Cells(6).Tag = drPBP.Item("UNIQUE_ID")
                        i = i + 1
                    Next
                Else
                    'dgvPBP.Visible = False
                    frmMain.picOpticalFeed.Enabled = False
                    Me.Close()
                    'frmMain.picOpticalFeed.ImageLocation = m_objUtility.ReturnImage("Camera.gif")
                    'lblStatus.Visible = True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub InsertOpticalPBPData(ByVal UniqueId As Integer)
        Try
            Dim drPBPData As DataRow
            Dim intUniqueID As Integer
            Dim dsOpticalPBP As New DataSet
            Dim intTeamID As Integer
            Dim intAwayScore As Integer
            Dim intHomeScore As Integer
            Dim intOldAwayScore As Integer
            Dim EventID As Integer
            Dim dtTimeDiff As DateTime

            m_dsScores = m_objModule1PBP.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsScores.Tables(0).Rows.Count > 0 Then
                intTeamID = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                intAwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                intHomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
            End If
            dsOpticalPBP = m_objSportVU.GetOpticalEvent(m_objGameDetails.GameCode, UniqueId)

            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
            intUniqueID = m_objModule1PBP.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            drPBPData("GAME_CODE") = m_objGameDetails.GameCode
            drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drPBPData("UNIQUE_ID") = intUniqueID
            drPBPData("SEQUENCE_NUMBER") = Convert.ToDecimal(intUniqueID)
            drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
            For Each drOptical As DataRow In dsOpticalPBP.Tables(0).Rows
                drPBPData("EVENT_CODE_ID") = drOptical("EVENT_CODE_ID")

                dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

                'drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(Dt, DateTimeHelper.OutputFormat.CurrentFormat)
                drPBPData("TEAM_ID") = drOptical("TEAM_ID")
                'drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                'drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                EventID = drPBPData("EVENT_CODE_ID")
                If intTeamID = m_objGameDetails.HomeTeamID Then
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                            intHomeScore = intHomeScore + 1
                            m_objGameDetails.HomeScore = intHomeScore
                            'm_objGameDetails.AwayScore = intAwayScore
                        End If
                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    Else
                        If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                            intAwayScore = intAwayScore + 1
                            m_objGameDetails.AwayScore = intAwayScore
                        End If
                        intOldAwayScore = intAwayScore
                        intAwayScore = intHomeScore
                        intHomeScore = intOldAwayScore
                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    End If
                Else
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                            intAwayScore = intAwayScore + 1
                            m_objGameDetails.HomeScore = intAwayScore
                            'Else

                        End If
                        intOldAwayScore = intAwayScore
                        intAwayScore = intHomeScore
                        intHomeScore = intOldAwayScore
                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    Else
                        If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                            intHomeScore = intHomeScore + 1
                            m_objGameDetails.AwayScore = intHomeScore
                        End If
                        'intOldAwayScore = m_objGameDetails.AwayScore
                        'm_objGameDetails.AwayScore = m_objGameDetails.HomeScore
                        'm_objGameDetails.HomeScore = intOldAwayScore

                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    End If
                End If
                drPBPData("OFFENSE_SCORE") = intHomeScore
                drPBPData("DEFENSE_SCORE") = intAwayScore
                drPBPData("OFFENSIVE_PLAYER_ID") = drOptical("OFFENSIVE_PLAYER_ID")
                drPBPData("ASSISTING_PLAYER_ID") = drOptical("ASSISTING_PLAYER_ID")
                drPBPData("DEFENSIVE_PLAYER_ID") = drOptical("DEFENSIVE_PLAYER_ID")
                drPBPData("PLAYER_OUT_ID") = drOptical("PLAYER_OUT_ID")

                drPBPData("PENALTY_KICK_AWARDED") = drOptical("PENALTY_KICK_AWARDED")
                drPBPData("SHOT_TYPE_ID") = drOptical("SHOT_TYPE_ID")
                drPBPData("SAVE_TYPE_ID") = drOptical("SAVE_TYPE_ID")
                drPBPData("SITUATION_ID") = drOptical("SITUATION_ID")
                drPBPData("GOALZONE_ID") = drOptical("GOALZONE_ID")
                drPBPData("REBOUND") = drOptical("REBOUND")

                drPBPData("SHOOTOUT_ROUND") = drOptical("SHOOTOUT_ROUND")
                drPBPData("CELEBRATION_ID") = drOptical("CELEBRATION_ID")
                drPBPData("SECOND_ASSISTER_ID") = drOptical("SECOND_ASSISTER_ID")

                drPBPData("CORNER_CROSS_TYPE") = drOptical("CORNER_CROSS_TYPE")
                drPBPData("CORNER_CROSS_RES") = drOptical("CORNER_CROSS_RES")
                drPBPData("CORNER_CROSS_DESC") = drOptical("CORNER_CROSS_DESC")
                drPBPData("CORNER_CROSS_ZONE") = drOptical("CORNER_CROSS_ZONE")

                drPBPData("SHOT_DESC") = drOptical("SHOT_DESC")
                drPBPData("SHOT_RESULT") = drOptical("SHOT_RESULT")
                drPBPData("FOUL_TYPE_ID") = drOptical("FOUL_TYPE_ID")
                drPBPData("FOUL_RESULT_ID") = drOptical("FOUL_RESULT_ID")
                drPBPData("DEFENDED_TYPE_ID") = drOptical("DEFENDED_TYPE_ID")
                drPBPData("DEF_ACTION_TYPE_ID") = drOptical("DEF_ACTION_TYPE_ID")

                drPBPData("DEF_ACTION_DESC_ID") = drOptical("DEF_ACTION_DESC_ID")
                drPBPData("DEF_ACTION_RESULT_ID") = drOptical("DEF_ACTION_RESULT_ID")
                drPBPData("GOAL_THREAT_DESC_ID") = drOptical("GOAL_THREAT_DESC_ID")
                drPBPData("GOAL_THREAT_DEF_ID") = drOptical("GOAL_THREAT_DEF_ID")
                drPBPData("SUB_REASON_ID") = drOptical("SUB_REASON_ID")

                drPBPData("X_FIELD_ZONE_OPTICAL") = drOptical("X_FIELD_ZONE_OPTICAL")
                drPBPData("Y_FIELD_ZONE_OPTICAL") = drOptical("Y_FIELD_ZONE_OPTICAL")
                drPBPData("Z_FIELD_ZONE_OPTICAL") = drOptical("Z_FIELD_ZONE_OPTICAL")
                drPBPData("X_FIELD_ZONE_DEF_OPTICAL") = drOptical("X_FIELD_ZONE_DEF_OPTICAL")
                drPBPData("Y_FIELD_ZONE_DEF_OPTICAL") = drOptical("Y_FIELD_ZONE_DEF_OPTICAL")
                drPBPData("Z_FIELD_ZONE_DEF_OPTICAL") = drOptical("Z_FIELD_ZONE_DEF_OPTICAL")

                drPBPData("OPTICAL_TIMESTAMP") = drOptical("OPTICAL_TIMESTAMP")

                drPBPData("TIME_ELAPSED") = drOptical("TIME_ELAPSED")
            Next

            drPBPData("CONTINUATION") = "F"
            drPBPData("RECORD_EDITED") = "N"
            drPBPData("EDIT_UID") = 0
            drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
            drPBPData("PROCESSED") = "N"
            drPBPData("DEMO_DATA") = "N"

            m_objGameDetails.PBP.Tables(0).Rows.Clear()
            m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)

            Dim dsTempPBPData As New DataSet
            m_objGameDetails.PBP.DataSetName = "SoccerEventData"
            dsTempPBPData = m_objGameDetails.PBP
            Dim strPBPXMLData As String = dsTempPBPData.GetXml()
            Dim dsPBP As New DataSet
            dsPBP = m_objModule1PBP.InsertPBPDataToSQL(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, False, False, 0, m_objGameDetails.IsFoulAssociated, m_objGameDetails.languageid)
            m_objGameDetails.OpticalPBPAccepted = dsPBP
            'frmModule1PBP.DisplayInListView(dsPBP)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Private Sub dgvPBP_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvPBP.LostFocus
    '    Me.Close()
    'End Sub


    Private Sub InsertOpticalTouchData(ByVal UniqueId As Integer)
        Try
            'Dim drTouchData As DataRow
            Dim dsOpticalTouch As New DataSet


            dsOpticalTouch = m_objSportVU.GetOpticalTouchEvent(m_objGameDetails.GameCode, UniqueId)

            For Each drOptical As DataRow In dsOpticalTouch.Tables(0).Rows

                Dim intTimeElapsed As Integer = drOptical("TIME_ELAPSED")
                Dim intTouchTypeID As Integer
                m_objTouch.AddTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intTimeElapsed, m_objGameDetails.CurrentPeriod, drOptical("PLAYER_ID"), intTouchTypeID, m_objGameDetails.ReporterRole, CInt(drOptical("X_FIELD_ZONE_OPTICAL")), CInt(drOptical("Y_FIELD_ZONE_OPTICAL")))
            Next
            m_objGameDetails.OpticalTouchAccepted = m_objTouch.GetTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid, m_objclsGameDetails.SerialNo, m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.AwayTeamID, 0)
            'frmTouches.ClearTouchLabels()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class