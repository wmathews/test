﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommentary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommentary))
        Me.btnHomeTeam = New System.Windows.Forms.Button
        Me.btnVisitTeam = New System.Windows.Forms.Button
        Me.btnVisitPlayer11 = New System.Windows.Forms.Button
        Me.btnVisitPlayer6 = New System.Windows.Forms.Button
        Me.btnVisitPlayer7 = New System.Windows.Forms.Button
        Me.btnVisitPlayer8 = New System.Windows.Forms.Button
        Me.btnVisitPlayer9 = New System.Windows.Forms.Button
        Me.btnVisitPlayer10 = New System.Windows.Forms.Button
        Me.btnVisitPlayer1 = New System.Windows.Forms.Button
        Me.btnVisitPlayer2 = New System.Windows.Forms.Button
        Me.btnVisitPlayer3 = New System.Windows.Forms.Button
        Me.btnVisitPlayer4 = New System.Windows.Forms.Button
        Me.btnVisitPlayer5 = New System.Windows.Forms.Button
        Me.btnHomePlayer11 = New System.Windows.Forms.Button
        Me.btnHomePlayer6 = New System.Windows.Forms.Button
        Me.btnHomePlayer7 = New System.Windows.Forms.Button
        Me.btnHomePlayer8 = New System.Windows.Forms.Button
        Me.btnHomePlayer9 = New System.Windows.Forms.Button
        Me.btnHomePlayer10 = New System.Windows.Forms.Button
        Me.btnHomePlayer1 = New System.Windows.Forms.Button
        Me.btnHomePlayer2 = New System.Windows.Forms.Button
        Me.btnHomePlayer3 = New System.Windows.Forms.Button
        Me.btnHomePlayer4 = New System.Windows.Forms.Button
        Me.btnHomePlayer5 = New System.Windows.Forms.Button
        Me.lvwCommentary = New System.Windows.Forms.ListView
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.cmnCommentary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlPBP = New System.Windows.Forms.Panel
        Me.lvwPBP = New System.Windows.Forms.ListView
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader8 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader9 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader10 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader11 = New System.Windows.Forms.ColumnHeader
        Me.picRefresh = New System.Windows.Forms.PictureBox
        Me.lblPlayByPlay = New System.Windows.Forms.Label
        Me.slbReporterEntry = New System.Windows.Forms.ToolStripStatusLabel
        Me.pnlVisitBench = New System.Windows.Forms.Panel
        Me.btnVisitCoach = New System.Windows.Forms.Button
        Me.btnVisitSub28 = New System.Windows.Forms.Button
        Me.btnVisitSub29 = New System.Windows.Forms.Button
        Me.btnVisitSub30 = New System.Windows.Forms.Button
        Me.btnVisitSub25 = New System.Windows.Forms.Button
        Me.btnVisitSub26 = New System.Windows.Forms.Button
        Me.btnVisitSub27 = New System.Windows.Forms.Button
        Me.btnVisitSub22 = New System.Windows.Forms.Button
        Me.btnVisitSub23 = New System.Windows.Forms.Button
        Me.btnVisitSub24 = New System.Windows.Forms.Button
        Me.btnVisitSub19 = New System.Windows.Forms.Button
        Me.btnVisitSub20 = New System.Windows.Forms.Button
        Me.btnVisitSub21 = New System.Windows.Forms.Button
        Me.btnVisitSub10 = New System.Windows.Forms.Button
        Me.btnVisitSub11 = New System.Windows.Forms.Button
        Me.btnVisitSub12 = New System.Windows.Forms.Button
        Me.btnVisitSub16 = New System.Windows.Forms.Button
        Me.btnVisitSub17 = New System.Windows.Forms.Button
        Me.btnVisitSub18 = New System.Windows.Forms.Button
        Me.btnVisitSub13 = New System.Windows.Forms.Button
        Me.btnVisitSub14 = New System.Windows.Forms.Button
        Me.btnVisitSub15 = New System.Windows.Forms.Button
        Me.btnVisitSub7 = New System.Windows.Forms.Button
        Me.btnVisitSub8 = New System.Windows.Forms.Button
        Me.btnVisitSub9 = New System.Windows.Forms.Button
        Me.btnVisitSub4 = New System.Windows.Forms.Button
        Me.btnVisitSub5 = New System.Windows.Forms.Button
        Me.btnVisitSub6 = New System.Windows.Forms.Button
        Me.btnVisitSub1 = New System.Windows.Forms.Button
        Me.btnVisitSub2 = New System.Windows.Forms.Button
        Me.btnVisitSub3 = New System.Windows.Forms.Button
        Me.pnlHomeBench = New System.Windows.Forms.Panel
        Me.btnHomeCoach = New System.Windows.Forms.Button
        Me.btnHomeSub28 = New System.Windows.Forms.Button
        Me.btnHomeSub29 = New System.Windows.Forms.Button
        Me.btnHomeSub30 = New System.Windows.Forms.Button
        Me.btnHomeSub25 = New System.Windows.Forms.Button
        Me.btnHomeSub26 = New System.Windows.Forms.Button
        Me.btnHomeSub27 = New System.Windows.Forms.Button
        Me.btnHomeSub22 = New System.Windows.Forms.Button
        Me.btnHomeSub23 = New System.Windows.Forms.Button
        Me.btnHomeSub24 = New System.Windows.Forms.Button
        Me.btnHomeSub19 = New System.Windows.Forms.Button
        Me.btnHomeSub20 = New System.Windows.Forms.Button
        Me.btnHomeSub21 = New System.Windows.Forms.Button
        Me.btnHomeSub16 = New System.Windows.Forms.Button
        Me.btnHomeSub17 = New System.Windows.Forms.Button
        Me.btnHomeSub18 = New System.Windows.Forms.Button
        Me.btnHomeSub13 = New System.Windows.Forms.Button
        Me.btnHomeSub14 = New System.Windows.Forms.Button
        Me.btnHomeSub15 = New System.Windows.Forms.Button
        Me.btnHomeSub10 = New System.Windows.Forms.Button
        Me.btnHomeSub11 = New System.Windows.Forms.Button
        Me.btnHomeSub12 = New System.Windows.Forms.Button
        Me.btnHomeSub7 = New System.Windows.Forms.Button
        Me.btnHomeSub8 = New System.Windows.Forms.Button
        Me.btnHomeSub9 = New System.Windows.Forms.Button
        Me.btnHomeSub4 = New System.Windows.Forms.Button
        Me.btnHomeSub5 = New System.Windows.Forms.Button
        Me.btnHomeSub6 = New System.Windows.Forms.Button
        Me.btnHomeSub1 = New System.Windows.Forms.Button
        Me.btnHomeSub2 = New System.Windows.Forms.Button
        Me.btnHomeSub3 = New System.Windows.Forms.Button
        Me.tltpCommentary = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlCommentaryEntry = New System.Windows.Forms.Panel
        Me.lblCommentType = New System.Windows.Forms.Label
        Me.cmbCommentType = New System.Windows.Forms.ComboBox
        Me.txtHeadline = New System.Windows.Forms.TextBox
        Me.txtTime = New System.Windows.Forms.TextBox
        Me.txtComment = New System.Windows.Forms.TextBox
        Me.btnRefreshPlayers = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblHeadline = New System.Windows.Forms.Label
        Me.lblTime = New System.Windows.Forms.Label
        Me.tmrPBP = New System.Windows.Forms.Timer(Me.components)
        Me.pnlHome = New System.Windows.Forms.Panel
        Me.btnHomePlayer35 = New System.Windows.Forms.Button
        Me.btnHomePlayer34 = New System.Windows.Forms.Button
        Me.btnHomePlayer41 = New System.Windows.Forms.Button
        Me.btnHomePlayer40 = New System.Windows.Forms.Button
        Me.btnHomePlayer36 = New System.Windows.Forms.Button
        Me.btnHomePlayer39 = New System.Windows.Forms.Button
        Me.btnHomePlayer37 = New System.Windows.Forms.Button
        Me.btnHomePlayer38 = New System.Windows.Forms.Button
        Me.btnHomePlayer27 = New System.Windows.Forms.Button
        Me.btnHomePlayer26 = New System.Windows.Forms.Button
        Me.btnHomePlayer25 = New System.Windows.Forms.Button
        Me.btnHomePlayer33 = New System.Windows.Forms.Button
        Me.btnHomePlayer24 = New System.Windows.Forms.Button
        Me.btnHomePlayer23 = New System.Windows.Forms.Button
        Me.btnHomePlayer32 = New System.Windows.Forms.Button
        Me.btnHomePlayer28 = New System.Windows.Forms.Button
        Me.btnHomePlayer31 = New System.Windows.Forms.Button
        Me.btnHomePlayer29 = New System.Windows.Forms.Button
        Me.btnHomePlayer30 = New System.Windows.Forms.Button
        Me.btnHomePlayer16 = New System.Windows.Forms.Button
        Me.btnHomePlayer15 = New System.Windows.Forms.Button
        Me.btnHomePlayer14 = New System.Windows.Forms.Button
        Me.btnHomePlayer22 = New System.Windows.Forms.Button
        Me.btnHomePlayer13 = New System.Windows.Forms.Button
        Me.btnHomePlayer12 = New System.Windows.Forms.Button
        Me.btnHomePlayer21 = New System.Windows.Forms.Button
        Me.btnHomePlayer17 = New System.Windows.Forms.Button
        Me.btnHomePlayer20 = New System.Windows.Forms.Button
        Me.btnHomePlayer18 = New System.Windows.Forms.Button
        Me.btnHomePlayer19 = New System.Windows.Forms.Button
        Me.pnlVisit = New System.Windows.Forms.Panel
        Me.btnVisitPlayer35 = New System.Windows.Forms.Button
        Me.btnVisitPlayer34 = New System.Windows.Forms.Button
        Me.btnVisitPlayer41 = New System.Windows.Forms.Button
        Me.btnVisitPlayer40 = New System.Windows.Forms.Button
        Me.btnVisitPlayer36 = New System.Windows.Forms.Button
        Me.btnVisitPlayer39 = New System.Windows.Forms.Button
        Me.btnVisitPlayer37 = New System.Windows.Forms.Button
        Me.btnVisitPlayer38 = New System.Windows.Forms.Button
        Me.btnVisitPlayer27 = New System.Windows.Forms.Button
        Me.btnVisitPlayer26 = New System.Windows.Forms.Button
        Me.btnVisitPlayer25 = New System.Windows.Forms.Button
        Me.btnVisitPlayer33 = New System.Windows.Forms.Button
        Me.btnVisitPlayer24 = New System.Windows.Forms.Button
        Me.btnVisitPlayer23 = New System.Windows.Forms.Button
        Me.btnVisitPlayer32 = New System.Windows.Forms.Button
        Me.btnVisitPlayer28 = New System.Windows.Forms.Button
        Me.btnVisitPlayer31 = New System.Windows.Forms.Button
        Me.btnVisitPlayer29 = New System.Windows.Forms.Button
        Me.btnVisitPlayer30 = New System.Windows.Forms.Button
        Me.btnVisitPlayer16 = New System.Windows.Forms.Button
        Me.btnVisitPlayer15 = New System.Windows.Forms.Button
        Me.btnVisitPlayer14 = New System.Windows.Forms.Button
        Me.btnVisitPlayer22 = New System.Windows.Forms.Button
        Me.btnVisitPlayer13 = New System.Windows.Forms.Button
        Me.btnVisitPlayer12 = New System.Windows.Forms.Button
        Me.btnVisitPlayer21 = New System.Windows.Forms.Button
        Me.btnVisitPlayer17 = New System.Windows.Forms.Button
        Me.btnVisitPlayer20 = New System.Windows.Forms.Button
        Me.btnVisitPlayer18 = New System.Windows.Forms.Button
        Me.btnVisitPlayer19 = New System.Windows.Forms.Button
        Me.cmnCommentary.SuspendLayout()
        Me.pnlPBP.SuspendLayout()
        CType(Me.picRefresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlVisitBench.SuspendLayout()
        Me.pnlHomeBench.SuspendLayout()
        Me.pnlCommentaryEntry.SuspendLayout()
        Me.pnlHome.SuspendLayout()
        Me.pnlVisit.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnHomeTeam
        '
        Me.btnHomeTeam.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeTeam.FlatAppearance.BorderSize = 0
        Me.btnHomeTeam.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomeTeam.Location = New System.Drawing.Point(0, 1)
        Me.btnHomeTeam.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeTeam.Name = "btnHomeTeam"
        Me.btnHomeTeam.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeTeam.Size = New System.Drawing.Size(212, 29)
        Me.btnHomeTeam.TabIndex = 0
        Me.btnHomeTeam.UseVisualStyleBackColor = False
        '
        'btnVisitTeam
        '
        Me.btnVisitTeam.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitTeam.FlatAppearance.BorderSize = 0
        Me.btnVisitTeam.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitTeam.Location = New System.Drawing.Point(0, 1)
        Me.btnVisitTeam.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitTeam.Name = "btnVisitTeam"
        Me.btnVisitTeam.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitTeam.Size = New System.Drawing.Size(212, 29)
        Me.btnVisitTeam.TabIndex = 0
        Me.btnVisitTeam.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer11
        '
        Me.btnVisitPlayer11.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer11.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer11.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer11.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer11.Location = New System.Drawing.Point(0, 280)
        Me.btnVisitPlayer11.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer11.Name = "btnVisitPlayer11"
        Me.btnVisitPlayer11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer11.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer11.TabIndex = 11
        Me.btnVisitPlayer11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer11.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer6
        '
        Me.btnVisitPlayer6.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer6.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer6.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer6.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer6.Location = New System.Drawing.Point(0, 155)
        Me.btnVisitPlayer6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer6.Name = "btnVisitPlayer6"
        Me.btnVisitPlayer6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer6.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer6.TabIndex = 6
        Me.btnVisitPlayer6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer6.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer7
        '
        Me.btnVisitPlayer7.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer7.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer7.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer7.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer7.Location = New System.Drawing.Point(0, 180)
        Me.btnVisitPlayer7.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer7.Name = "btnVisitPlayer7"
        Me.btnVisitPlayer7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer7.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer7.TabIndex = 7
        Me.btnVisitPlayer7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer7.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer8
        '
        Me.btnVisitPlayer8.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer8.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer8.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer8.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer8.Location = New System.Drawing.Point(0, 205)
        Me.btnVisitPlayer8.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer8.Name = "btnVisitPlayer8"
        Me.btnVisitPlayer8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer8.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer8.TabIndex = 8
        Me.btnVisitPlayer8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer8.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer9
        '
        Me.btnVisitPlayer9.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer9.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer9.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer9.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer9.Location = New System.Drawing.Point(0, 230)
        Me.btnVisitPlayer9.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer9.Name = "btnVisitPlayer9"
        Me.btnVisitPlayer9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer9.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer9.TabIndex = 9
        Me.btnVisitPlayer9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer9.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer10
        '
        Me.btnVisitPlayer10.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer10.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer10.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer10.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer10.Location = New System.Drawing.Point(0, 255)
        Me.btnVisitPlayer10.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer10.Name = "btnVisitPlayer10"
        Me.btnVisitPlayer10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer10.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer10.TabIndex = 10
        Me.btnVisitPlayer10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer10.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer1
        '
        Me.btnVisitPlayer1.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer1.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer1.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer1.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer1.Location = New System.Drawing.Point(0, 30)
        Me.btnVisitPlayer1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer1.Name = "btnVisitPlayer1"
        Me.btnVisitPlayer1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer1.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer1.TabIndex = 1
        Me.btnVisitPlayer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer1.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer2
        '
        Me.btnVisitPlayer2.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer2.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer2.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer2.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer2.Location = New System.Drawing.Point(0, 55)
        Me.btnVisitPlayer2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer2.Name = "btnVisitPlayer2"
        Me.btnVisitPlayer2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer2.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer2.TabIndex = 2
        Me.btnVisitPlayer2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer2.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer3
        '
        Me.btnVisitPlayer3.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer3.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer3.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer3.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer3.Location = New System.Drawing.Point(0, 80)
        Me.btnVisitPlayer3.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer3.Name = "btnVisitPlayer3"
        Me.btnVisitPlayer3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer3.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer3.TabIndex = 3
        Me.btnVisitPlayer3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer3.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer4
        '
        Me.btnVisitPlayer4.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer4.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer4.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer4.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer4.Location = New System.Drawing.Point(0, 105)
        Me.btnVisitPlayer4.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer4.Name = "btnVisitPlayer4"
        Me.btnVisitPlayer4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer4.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer4.TabIndex = 4
        Me.btnVisitPlayer4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer4.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer5
        '
        Me.btnVisitPlayer5.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer5.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer5.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer5.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer5.Location = New System.Drawing.Point(0, 130)
        Me.btnVisitPlayer5.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer5.Name = "btnVisitPlayer5"
        Me.btnVisitPlayer5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer5.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer5.TabIndex = 5
        Me.btnVisitPlayer5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer5.UseVisualStyleBackColor = False
        '
        'btnHomePlayer11
        '
        Me.btnHomePlayer11.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer11.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer11.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer11.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer11.Location = New System.Drawing.Point(0, 280)
        Me.btnHomePlayer11.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer11.Name = "btnHomePlayer11"
        Me.btnHomePlayer11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer11.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer11.TabIndex = 11
        Me.btnHomePlayer11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer11.UseVisualStyleBackColor = False
        '
        'btnHomePlayer6
        '
        Me.btnHomePlayer6.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer6.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer6.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer6.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer6.Location = New System.Drawing.Point(0, 155)
        Me.btnHomePlayer6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer6.Name = "btnHomePlayer6"
        Me.btnHomePlayer6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer6.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer6.TabIndex = 6
        Me.btnHomePlayer6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer6.UseVisualStyleBackColor = False
        '
        'btnHomePlayer7
        '
        Me.btnHomePlayer7.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer7.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer7.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer7.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer7.Location = New System.Drawing.Point(0, 180)
        Me.btnHomePlayer7.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer7.Name = "btnHomePlayer7"
        Me.btnHomePlayer7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer7.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer7.TabIndex = 7
        Me.btnHomePlayer7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer7.UseVisualStyleBackColor = False
        '
        'btnHomePlayer8
        '
        Me.btnHomePlayer8.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer8.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer8.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer8.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer8.Location = New System.Drawing.Point(0, 205)
        Me.btnHomePlayer8.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer8.Name = "btnHomePlayer8"
        Me.btnHomePlayer8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer8.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer8.TabIndex = 8
        Me.btnHomePlayer8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer8.UseVisualStyleBackColor = False
        '
        'btnHomePlayer9
        '
        Me.btnHomePlayer9.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer9.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer9.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer9.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer9.Location = New System.Drawing.Point(0, 230)
        Me.btnHomePlayer9.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer9.Name = "btnHomePlayer9"
        Me.btnHomePlayer9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer9.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer9.TabIndex = 9
        Me.btnHomePlayer9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer9.UseVisualStyleBackColor = False
        '
        'btnHomePlayer10
        '
        Me.btnHomePlayer10.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer10.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer10.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer10.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer10.Location = New System.Drawing.Point(0, 255)
        Me.btnHomePlayer10.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer10.Name = "btnHomePlayer10"
        Me.btnHomePlayer10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer10.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer10.TabIndex = 10
        Me.btnHomePlayer10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer10.UseVisualStyleBackColor = False
        '
        'btnHomePlayer1
        '
        Me.btnHomePlayer1.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer1.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer1.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer1.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer1.Location = New System.Drawing.Point(0, 30)
        Me.btnHomePlayer1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer1.Name = "btnHomePlayer1"
        Me.btnHomePlayer1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer1.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer1.TabIndex = 1
        Me.btnHomePlayer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer1.UseVisualStyleBackColor = False
        '
        'btnHomePlayer2
        '
        Me.btnHomePlayer2.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer2.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer2.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer2.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer2.Location = New System.Drawing.Point(0, 55)
        Me.btnHomePlayer2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer2.Name = "btnHomePlayer2"
        Me.btnHomePlayer2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer2.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer2.TabIndex = 2
        Me.btnHomePlayer2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer2.UseVisualStyleBackColor = False
        '
        'btnHomePlayer3
        '
        Me.btnHomePlayer3.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer3.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer3.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer3.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer3.Location = New System.Drawing.Point(0, 80)
        Me.btnHomePlayer3.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer3.Name = "btnHomePlayer3"
        Me.btnHomePlayer3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer3.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer3.TabIndex = 3
        Me.btnHomePlayer3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer3.UseVisualStyleBackColor = False
        '
        'btnHomePlayer4
        '
        Me.btnHomePlayer4.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer4.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer4.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer4.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer4.Location = New System.Drawing.Point(0, 105)
        Me.btnHomePlayer4.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer4.Name = "btnHomePlayer4"
        Me.btnHomePlayer4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer4.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer4.TabIndex = 4
        Me.btnHomePlayer4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer4.UseVisualStyleBackColor = False
        '
        'btnHomePlayer5
        '
        Me.btnHomePlayer5.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer5.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer5.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer5.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer5.Location = New System.Drawing.Point(0, 130)
        Me.btnHomePlayer5.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer5.Name = "btnHomePlayer5"
        Me.btnHomePlayer5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer5.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer5.TabIndex = 5
        Me.btnHomePlayer5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer5.UseVisualStyleBackColor = False
        '
        'lvwCommentary
        '
        Me.lvwCommentary.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader4, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lvwCommentary.ContextMenuStrip = Me.cmnCommentary
        Me.lvwCommentary.FullRowSelect = True
        Me.lvwCommentary.Location = New System.Drawing.Point(262, 160)
        Me.lvwCommentary.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lvwCommentary.Name = "lvwCommentary"
        Me.lvwCommentary.Size = New System.Drawing.Size(485, 149)
        Me.lvwCommentary.TabIndex = 5
        Me.lvwCommentary.UseCompatibleStateImageBehavior = False
        Me.lvwCommentary.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Game time"
        Me.ColumnHeader5.Width = 71
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Period"
        Me.ColumnHeader4.Width = 47
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Headline"
        Me.ColumnHeader2.Width = 85
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Comment"
        Me.ColumnHeader3.Width = 219
        '
        'cmnCommentary
        '
        Me.cmnCommentary.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem, Me.DeleteToolStripMenuItem})
        Me.cmnCommentary.Name = "cmnCommentary"
        Me.cmnCommentary.Size = New System.Drawing.Size(106, 48)
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'pnlPBP
        '
        Me.pnlPBP.BackColor = System.Drawing.Color.FromArgb(CType(CType(217, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.pnlPBP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPBP.Controls.Add(Me.lvwPBP)
        Me.pnlPBP.Controls.Add(Me.picRefresh)
        Me.pnlPBP.Controls.Add(Me.lblPlayByPlay)
        Me.pnlPBP.Location = New System.Drawing.Point(306, 318)
        Me.pnlPBP.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlPBP.Name = "pnlPBP"
        Me.pnlPBP.Size = New System.Drawing.Size(398, 136)
        Me.pnlPBP.TabIndex = 6
        '
        'lvwPBP
        '
        Me.lvwPBP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11})
        Me.lvwPBP.FullRowSelect = True
        Me.lvwPBP.Location = New System.Drawing.Point(6, 18)
        Me.lvwPBP.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lvwPBP.Name = "lvwPBP"
        Me.lvwPBP.Size = New System.Drawing.Size(385, 112)
        Me.lvwPBP.TabIndex = 1
        Me.lvwPBP.UseCompatibleStateImageBehavior = False
        Me.lvwPBP.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Time"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Event"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Type"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Player"
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "SubType"
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "SubSubType"
        '
        'picRefresh
        '
        Me.picRefresh.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.picRefresh.Image = CType(resources.GetObject("picRefresh.Image"), System.Drawing.Image)
        Me.picRefresh.InitialImage = Nothing
        Me.picRefresh.Location = New System.Drawing.Point(379, 1)
        Me.picRefresh.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.picRefresh.Name = "picRefresh"
        Me.picRefresh.Size = New System.Drawing.Size(19, 20)
        Me.picRefresh.TabIndex = 178
        Me.picRefresh.TabStop = False
        Me.tltpCommentary.SetToolTip(Me.picRefresh, "Refresh")
        '
        'lblPlayByPlay
        '
        Me.lblPlayByPlay.AutoSize = True
        Me.lblPlayByPlay.Location = New System.Drawing.Point(4, 0)
        Me.lblPlayByPlay.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPlayByPlay.Name = "lblPlayByPlay"
        Me.lblPlayByPlay.Size = New System.Drawing.Size(69, 15)
        Me.lblPlayByPlay.TabIndex = 1
        Me.lblPlayByPlay.Text = "Play by play:"
        '
        'slbReporterEntry
        '
        Me.slbReporterEntry.BackColor = System.Drawing.Color.Transparent
        Me.slbReporterEntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slbReporterEntry.ForeColor = System.Drawing.Color.White
        Me.slbReporterEntry.Margin = New System.Windows.Forms.Padding(0)
        Me.slbReporterEntry.Name = "slbReporterEntry"
        Me.slbReporterEntry.Size = New System.Drawing.Size(0, 0)
        Me.slbReporterEntry.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.slbReporterEntry.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay
        '
        'pnlVisitBench
        '
        Me.pnlVisitBench.AutoScroll = True
        Me.pnlVisitBench.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlVisitBench.Controls.Add(Me.btnVisitCoach)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub28)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub29)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub30)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub25)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub26)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub27)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub22)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub23)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub24)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub19)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub20)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub21)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub10)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub11)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub12)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub16)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub17)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub18)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub13)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub14)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub15)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub7)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub8)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub9)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub4)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub5)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub6)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub1)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub2)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub3)
        Me.pnlVisitBench.Location = New System.Drawing.Point(709, 316)
        Me.pnlVisitBench.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlVisitBench.Name = "pnlVisitBench"
        Me.pnlVisitBench.Size = New System.Drawing.Size(278, 138)
        Me.pnlVisitBench.TabIndex = 3
        '
        'btnVisitCoach
        '
        Me.btnVisitCoach.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitCoach.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitCoach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitCoach.FlatAppearance.BorderSize = 0
        Me.btnVisitCoach.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitCoach.ForeColor = System.Drawing.Color.Black
        Me.btnVisitCoach.Location = New System.Drawing.Point(1, 0)
        Me.btnVisitCoach.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitCoach.Name = "btnVisitCoach"
        Me.btnVisitCoach.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitCoach.Size = New System.Drawing.Size(260, 35)
        Me.btnVisitCoach.TabIndex = 0
        Me.btnVisitCoach.UseVisualStyleBackColor = False
        '
        'btnVisitSub28
        '
        Me.btnVisitSub28.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub28.FlatAppearance.BorderSize = 0
        Me.btnVisitSub28.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub28.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub28.Location = New System.Drawing.Point(1, 345)
        Me.btnVisitSub28.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub28.Name = "btnVisitSub28"
        Me.btnVisitSub28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub28.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub28.TabIndex = 28
        Me.btnVisitSub28.UseVisualStyleBackColor = False
        Me.btnVisitSub28.Visible = False
        '
        'btnVisitSub29
        '
        Me.btnVisitSub29.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub29.FlatAppearance.BorderSize = 0
        Me.btnVisitSub29.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub29.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub29.Location = New System.Drawing.Point(89, 345)
        Me.btnVisitSub29.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub29.Name = "btnVisitSub29"
        Me.btnVisitSub29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub29.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub29.TabIndex = 29
        Me.btnVisitSub29.UseVisualStyleBackColor = False
        Me.btnVisitSub29.Visible = False
        '
        'btnVisitSub30
        '
        Me.btnVisitSub30.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub30.FlatAppearance.BorderSize = 0
        Me.btnVisitSub30.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub30.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub30.Location = New System.Drawing.Point(176, 345)
        Me.btnVisitSub30.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub30.Name = "btnVisitSub30"
        Me.btnVisitSub30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub30.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub30.TabIndex = 30
        Me.btnVisitSub30.UseVisualStyleBackColor = False
        Me.btnVisitSub30.Visible = False
        '
        'btnVisitSub25
        '
        Me.btnVisitSub25.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub25.FlatAppearance.BorderSize = 0
        Me.btnVisitSub25.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub25.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub25.Location = New System.Drawing.Point(1, 311)
        Me.btnVisitSub25.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub25.Name = "btnVisitSub25"
        Me.btnVisitSub25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub25.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub25.TabIndex = 25
        Me.btnVisitSub25.UseVisualStyleBackColor = False
        Me.btnVisitSub25.Visible = False
        '
        'btnVisitSub26
        '
        Me.btnVisitSub26.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub26.FlatAppearance.BorderSize = 0
        Me.btnVisitSub26.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub26.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub26.Location = New System.Drawing.Point(89, 311)
        Me.btnVisitSub26.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub26.Name = "btnVisitSub26"
        Me.btnVisitSub26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub26.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub26.TabIndex = 26
        Me.btnVisitSub26.UseVisualStyleBackColor = False
        Me.btnVisitSub26.Visible = False
        '
        'btnVisitSub27
        '
        Me.btnVisitSub27.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub27.FlatAppearance.BorderSize = 0
        Me.btnVisitSub27.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub27.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub27.Location = New System.Drawing.Point(176, 311)
        Me.btnVisitSub27.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub27.Name = "btnVisitSub27"
        Me.btnVisitSub27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub27.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub27.TabIndex = 27
        Me.btnVisitSub27.UseVisualStyleBackColor = False
        Me.btnVisitSub27.Visible = False
        '
        'btnVisitSub22
        '
        Me.btnVisitSub22.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub22.FlatAppearance.BorderSize = 0
        Me.btnVisitSub22.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub22.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub22.Location = New System.Drawing.Point(1, 277)
        Me.btnVisitSub22.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub22.Name = "btnVisitSub22"
        Me.btnVisitSub22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub22.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub22.TabIndex = 22
        Me.btnVisitSub22.UseVisualStyleBackColor = False
        Me.btnVisitSub22.Visible = False
        '
        'btnVisitSub23
        '
        Me.btnVisitSub23.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub23.FlatAppearance.BorderSize = 0
        Me.btnVisitSub23.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub23.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub23.Location = New System.Drawing.Point(89, 277)
        Me.btnVisitSub23.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub23.Name = "btnVisitSub23"
        Me.btnVisitSub23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub23.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub23.TabIndex = 23
        Me.btnVisitSub23.UseVisualStyleBackColor = False
        Me.btnVisitSub23.Visible = False
        '
        'btnVisitSub24
        '
        Me.btnVisitSub24.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub24.FlatAppearance.BorderSize = 0
        Me.btnVisitSub24.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub24.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub24.Location = New System.Drawing.Point(176, 277)
        Me.btnVisitSub24.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub24.Name = "btnVisitSub24"
        Me.btnVisitSub24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub24.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub24.TabIndex = 24
        Me.btnVisitSub24.UseVisualStyleBackColor = False
        Me.btnVisitSub24.Visible = False
        '
        'btnVisitSub19
        '
        Me.btnVisitSub19.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub19.FlatAppearance.BorderSize = 0
        Me.btnVisitSub19.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub19.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub19.Location = New System.Drawing.Point(1, 243)
        Me.btnVisitSub19.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub19.Name = "btnVisitSub19"
        Me.btnVisitSub19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub19.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub19.TabIndex = 19
        Me.btnVisitSub19.UseVisualStyleBackColor = False
        Me.btnVisitSub19.Visible = False
        '
        'btnVisitSub20
        '
        Me.btnVisitSub20.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub20.FlatAppearance.BorderSize = 0
        Me.btnVisitSub20.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub20.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub20.Location = New System.Drawing.Point(89, 243)
        Me.btnVisitSub20.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub20.Name = "btnVisitSub20"
        Me.btnVisitSub20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub20.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub20.TabIndex = 20
        Me.btnVisitSub20.UseVisualStyleBackColor = False
        Me.btnVisitSub20.Visible = False
        '
        'btnVisitSub21
        '
        Me.btnVisitSub21.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub21.FlatAppearance.BorderSize = 0
        Me.btnVisitSub21.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub21.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub21.Location = New System.Drawing.Point(176, 243)
        Me.btnVisitSub21.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub21.Name = "btnVisitSub21"
        Me.btnVisitSub21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub21.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub21.TabIndex = 21
        Me.btnVisitSub21.UseVisualStyleBackColor = False
        Me.btnVisitSub21.Visible = False
        '
        'btnVisitSub10
        '
        Me.btnVisitSub10.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub10.FlatAppearance.BorderSize = 0
        Me.btnVisitSub10.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub10.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub10.Location = New System.Drawing.Point(1, 140)
        Me.btnVisitSub10.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub10.Name = "btnVisitSub10"
        Me.btnVisitSub10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub10.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub10.TabIndex = 10
        Me.btnVisitSub10.UseVisualStyleBackColor = False
        Me.btnVisitSub10.Visible = False
        '
        'btnVisitSub11
        '
        Me.btnVisitSub11.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub11.FlatAppearance.BorderSize = 0
        Me.btnVisitSub11.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub11.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub11.Location = New System.Drawing.Point(89, 140)
        Me.btnVisitSub11.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub11.Name = "btnVisitSub11"
        Me.btnVisitSub11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub11.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub11.TabIndex = 11
        Me.btnVisitSub11.UseVisualStyleBackColor = False
        Me.btnVisitSub11.Visible = False
        '
        'btnVisitSub12
        '
        Me.btnVisitSub12.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub12.FlatAppearance.BorderSize = 0
        Me.btnVisitSub12.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub12.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub12.Location = New System.Drawing.Point(176, 140)
        Me.btnVisitSub12.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub12.Name = "btnVisitSub12"
        Me.btnVisitSub12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub12.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub12.TabIndex = 12
        Me.btnVisitSub12.UseVisualStyleBackColor = False
        Me.btnVisitSub12.Visible = False
        '
        'btnVisitSub16
        '
        Me.btnVisitSub16.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub16.FlatAppearance.BorderSize = 0
        Me.btnVisitSub16.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub16.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub16.Location = New System.Drawing.Point(1, 209)
        Me.btnVisitSub16.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub16.Name = "btnVisitSub16"
        Me.btnVisitSub16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub16.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub16.TabIndex = 16
        Me.btnVisitSub16.UseVisualStyleBackColor = False
        Me.btnVisitSub16.Visible = False
        '
        'btnVisitSub17
        '
        Me.btnVisitSub17.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub17.FlatAppearance.BorderSize = 0
        Me.btnVisitSub17.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub17.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub17.Location = New System.Drawing.Point(89, 209)
        Me.btnVisitSub17.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub17.Name = "btnVisitSub17"
        Me.btnVisitSub17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub17.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub17.TabIndex = 17
        Me.btnVisitSub17.UseVisualStyleBackColor = False
        Me.btnVisitSub17.Visible = False
        '
        'btnVisitSub18
        '
        Me.btnVisitSub18.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub18.FlatAppearance.BorderSize = 0
        Me.btnVisitSub18.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub18.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub18.Location = New System.Drawing.Point(176, 209)
        Me.btnVisitSub18.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub18.Name = "btnVisitSub18"
        Me.btnVisitSub18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub18.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub18.TabIndex = 18
        Me.btnVisitSub18.UseVisualStyleBackColor = False
        Me.btnVisitSub18.Visible = False
        '
        'btnVisitSub13
        '
        Me.btnVisitSub13.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub13.FlatAppearance.BorderSize = 0
        Me.btnVisitSub13.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub13.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub13.Location = New System.Drawing.Point(1, 175)
        Me.btnVisitSub13.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub13.Name = "btnVisitSub13"
        Me.btnVisitSub13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub13.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub13.TabIndex = 13
        Me.btnVisitSub13.UseVisualStyleBackColor = False
        Me.btnVisitSub13.Visible = False
        '
        'btnVisitSub14
        '
        Me.btnVisitSub14.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub14.FlatAppearance.BorderSize = 0
        Me.btnVisitSub14.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub14.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub14.Location = New System.Drawing.Point(89, 175)
        Me.btnVisitSub14.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub14.Name = "btnVisitSub14"
        Me.btnVisitSub14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub14.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub14.TabIndex = 14
        Me.btnVisitSub14.UseVisualStyleBackColor = False
        Me.btnVisitSub14.Visible = False
        '
        'btnVisitSub15
        '
        Me.btnVisitSub15.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub15.FlatAppearance.BorderSize = 0
        Me.btnVisitSub15.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub15.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub15.Location = New System.Drawing.Point(176, 175)
        Me.btnVisitSub15.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub15.Name = "btnVisitSub15"
        Me.btnVisitSub15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub15.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub15.TabIndex = 15
        Me.btnVisitSub15.UseVisualStyleBackColor = False
        Me.btnVisitSub15.Visible = False
        '
        'btnVisitSub7
        '
        Me.btnVisitSub7.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub7.FlatAppearance.BorderSize = 0
        Me.btnVisitSub7.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub7.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub7.Location = New System.Drawing.Point(1, 104)
        Me.btnVisitSub7.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub7.Name = "btnVisitSub7"
        Me.btnVisitSub7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub7.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub7.TabIndex = 7
        Me.btnVisitSub7.UseVisualStyleBackColor = False
        '
        'btnVisitSub8
        '
        Me.btnVisitSub8.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub8.FlatAppearance.BorderSize = 0
        Me.btnVisitSub8.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub8.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub8.Location = New System.Drawing.Point(89, 104)
        Me.btnVisitSub8.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub8.Name = "btnVisitSub8"
        Me.btnVisitSub8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub8.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub8.TabIndex = 8
        Me.btnVisitSub8.UseVisualStyleBackColor = False
        '
        'btnVisitSub9
        '
        Me.btnVisitSub9.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub9.FlatAppearance.BorderSize = 0
        Me.btnVisitSub9.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub9.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub9.Location = New System.Drawing.Point(176, 104)
        Me.btnVisitSub9.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub9.Name = "btnVisitSub9"
        Me.btnVisitSub9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub9.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub9.TabIndex = 9
        Me.btnVisitSub9.UseVisualStyleBackColor = False
        '
        'btnVisitSub4
        '
        Me.btnVisitSub4.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub4.FlatAppearance.BorderSize = 0
        Me.btnVisitSub4.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub4.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub4.Location = New System.Drawing.Point(1, 70)
        Me.btnVisitSub4.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub4.Name = "btnVisitSub4"
        Me.btnVisitSub4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub4.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub4.TabIndex = 4
        Me.btnVisitSub4.UseVisualStyleBackColor = False
        '
        'btnVisitSub5
        '
        Me.btnVisitSub5.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub5.FlatAppearance.BorderSize = 0
        Me.btnVisitSub5.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub5.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub5.Location = New System.Drawing.Point(89, 70)
        Me.btnVisitSub5.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub5.Name = "btnVisitSub5"
        Me.btnVisitSub5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub5.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub5.TabIndex = 5
        Me.btnVisitSub5.UseVisualStyleBackColor = False
        '
        'btnVisitSub6
        '
        Me.btnVisitSub6.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub6.FlatAppearance.BorderSize = 0
        Me.btnVisitSub6.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub6.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub6.Location = New System.Drawing.Point(176, 70)
        Me.btnVisitSub6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub6.Name = "btnVisitSub6"
        Me.btnVisitSub6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub6.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub6.TabIndex = 6
        Me.btnVisitSub6.UseVisualStyleBackColor = False
        '
        'btnVisitSub1
        '
        Me.btnVisitSub1.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub1.FlatAppearance.BorderSize = 0
        Me.btnVisitSub1.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub1.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub1.Location = New System.Drawing.Point(1, 35)
        Me.btnVisitSub1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub1.Name = "btnVisitSub1"
        Me.btnVisitSub1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub1.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub1.TabIndex = 1
        Me.btnVisitSub1.UseVisualStyleBackColor = False
        '
        'btnVisitSub2
        '
        Me.btnVisitSub2.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub2.FlatAppearance.BorderSize = 0
        Me.btnVisitSub2.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub2.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub2.Location = New System.Drawing.Point(89, 35)
        Me.btnVisitSub2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub2.Name = "btnVisitSub2"
        Me.btnVisitSub2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub2.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub2.TabIndex = 2
        Me.btnVisitSub2.UseVisualStyleBackColor = False
        '
        'btnVisitSub3
        '
        Me.btnVisitSub3.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub3.FlatAppearance.BorderSize = 0
        Me.btnVisitSub3.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub3.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub3.Location = New System.Drawing.Point(176, 35)
        Me.btnVisitSub3.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitSub3.Name = "btnVisitSub3"
        Me.btnVisitSub3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub3.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub3.TabIndex = 3
        Me.btnVisitSub3.UseVisualStyleBackColor = False
        '
        'pnlHomeBench
        '
        Me.pnlHomeBench.AutoScroll = True
        Me.pnlHomeBench.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlHomeBench.Controls.Add(Me.btnHomeCoach)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub28)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub29)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub30)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub25)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub26)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub27)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub22)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub23)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub24)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub19)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub20)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub21)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub16)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub17)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub18)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub13)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub14)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub15)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub10)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub11)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub12)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub7)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub8)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub9)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub4)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub5)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub6)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub1)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub2)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub3)
        Me.pnlHomeBench.Location = New System.Drawing.Point(22, 316)
        Me.pnlHomeBench.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlHomeBench.Name = "pnlHomeBench"
        Me.pnlHomeBench.Size = New System.Drawing.Size(278, 138)
        Me.pnlHomeBench.TabIndex = 1
        '
        'btnHomeCoach
        '
        Me.btnHomeCoach.BackColor = System.Drawing.Color.White
        Me.btnHomeCoach.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeCoach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeCoach.FlatAppearance.BorderSize = 0
        Me.btnHomeCoach.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeCoach.ForeColor = System.Drawing.Color.Black
        Me.btnHomeCoach.Location = New System.Drawing.Point(0, -1)
        Me.btnHomeCoach.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeCoach.Name = "btnHomeCoach"
        Me.btnHomeCoach.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeCoach.Size = New System.Drawing.Size(260, 35)
        Me.btnHomeCoach.TabIndex = 0
        Me.btnHomeCoach.UseVisualStyleBackColor = False
        '
        'btnHomeSub28
        '
        Me.btnHomeSub28.BackColor = System.Drawing.Color.White
        Me.btnHomeSub28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub28.FlatAppearance.BorderSize = 0
        Me.btnHomeSub28.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub28.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub28.Location = New System.Drawing.Point(1, 341)
        Me.btnHomeSub28.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub28.Name = "btnHomeSub28"
        Me.btnHomeSub28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub28.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub28.TabIndex = 28
        Me.btnHomeSub28.UseVisualStyleBackColor = False
        Me.btnHomeSub28.Visible = False
        '
        'btnHomeSub29
        '
        Me.btnHomeSub29.BackColor = System.Drawing.Color.White
        Me.btnHomeSub29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub29.FlatAppearance.BorderSize = 0
        Me.btnHomeSub29.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub29.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub29.Location = New System.Drawing.Point(89, 341)
        Me.btnHomeSub29.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub29.Name = "btnHomeSub29"
        Me.btnHomeSub29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub29.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub29.TabIndex = 29
        Me.btnHomeSub29.UseVisualStyleBackColor = False
        Me.btnHomeSub29.Visible = False
        '
        'btnHomeSub30
        '
        Me.btnHomeSub30.BackColor = System.Drawing.Color.White
        Me.btnHomeSub30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub30.FlatAppearance.BorderSize = 0
        Me.btnHomeSub30.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub30.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub30.Location = New System.Drawing.Point(176, 341)
        Me.btnHomeSub30.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub30.Name = "btnHomeSub30"
        Me.btnHomeSub30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub30.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub30.TabIndex = 30
        Me.btnHomeSub30.UseVisualStyleBackColor = False
        Me.btnHomeSub30.Visible = False
        '
        'btnHomeSub25
        '
        Me.btnHomeSub25.BackColor = System.Drawing.Color.White
        Me.btnHomeSub25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub25.FlatAppearance.BorderSize = 0
        Me.btnHomeSub25.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub25.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub25.Location = New System.Drawing.Point(1, 307)
        Me.btnHomeSub25.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub25.Name = "btnHomeSub25"
        Me.btnHomeSub25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub25.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub25.TabIndex = 25
        Me.btnHomeSub25.UseVisualStyleBackColor = False
        Me.btnHomeSub25.Visible = False
        '
        'btnHomeSub26
        '
        Me.btnHomeSub26.BackColor = System.Drawing.Color.White
        Me.btnHomeSub26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub26.FlatAppearance.BorderSize = 0
        Me.btnHomeSub26.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub26.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub26.Location = New System.Drawing.Point(89, 307)
        Me.btnHomeSub26.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub26.Name = "btnHomeSub26"
        Me.btnHomeSub26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub26.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub26.TabIndex = 26
        Me.btnHomeSub26.UseVisualStyleBackColor = False
        Me.btnHomeSub26.Visible = False
        '
        'btnHomeSub27
        '
        Me.btnHomeSub27.BackColor = System.Drawing.Color.White
        Me.btnHomeSub27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub27.FlatAppearance.BorderSize = 0
        Me.btnHomeSub27.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub27.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub27.Location = New System.Drawing.Point(176, 307)
        Me.btnHomeSub27.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub27.Name = "btnHomeSub27"
        Me.btnHomeSub27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub27.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub27.TabIndex = 27
        Me.btnHomeSub27.UseVisualStyleBackColor = False
        Me.btnHomeSub27.Visible = False
        '
        'btnHomeSub22
        '
        Me.btnHomeSub22.BackColor = System.Drawing.Color.White
        Me.btnHomeSub22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub22.FlatAppearance.BorderSize = 0
        Me.btnHomeSub22.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub22.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub22.Location = New System.Drawing.Point(1, 273)
        Me.btnHomeSub22.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub22.Name = "btnHomeSub22"
        Me.btnHomeSub22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub22.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub22.TabIndex = 22
        Me.btnHomeSub22.UseVisualStyleBackColor = False
        Me.btnHomeSub22.Visible = False
        '
        'btnHomeSub23
        '
        Me.btnHomeSub23.BackColor = System.Drawing.Color.White
        Me.btnHomeSub23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub23.FlatAppearance.BorderSize = 0
        Me.btnHomeSub23.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub23.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub23.Location = New System.Drawing.Point(89, 273)
        Me.btnHomeSub23.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub23.Name = "btnHomeSub23"
        Me.btnHomeSub23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub23.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub23.TabIndex = 23
        Me.btnHomeSub23.UseVisualStyleBackColor = False
        Me.btnHomeSub23.Visible = False
        '
        'btnHomeSub24
        '
        Me.btnHomeSub24.BackColor = System.Drawing.Color.White
        Me.btnHomeSub24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub24.FlatAppearance.BorderSize = 0
        Me.btnHomeSub24.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub24.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub24.Location = New System.Drawing.Point(176, 273)
        Me.btnHomeSub24.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub24.Name = "btnHomeSub24"
        Me.btnHomeSub24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub24.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub24.TabIndex = 24
        Me.btnHomeSub24.UseVisualStyleBackColor = False
        Me.btnHomeSub24.Visible = False
        '
        'btnHomeSub19
        '
        Me.btnHomeSub19.BackColor = System.Drawing.Color.White
        Me.btnHomeSub19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub19.FlatAppearance.BorderSize = 0
        Me.btnHomeSub19.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub19.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub19.Location = New System.Drawing.Point(1, 239)
        Me.btnHomeSub19.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub19.Name = "btnHomeSub19"
        Me.btnHomeSub19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub19.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub19.TabIndex = 19
        Me.btnHomeSub19.UseVisualStyleBackColor = False
        Me.btnHomeSub19.Visible = False
        '
        'btnHomeSub20
        '
        Me.btnHomeSub20.BackColor = System.Drawing.Color.White
        Me.btnHomeSub20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub20.FlatAppearance.BorderSize = 0
        Me.btnHomeSub20.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub20.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub20.Location = New System.Drawing.Point(89, 239)
        Me.btnHomeSub20.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub20.Name = "btnHomeSub20"
        Me.btnHomeSub20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub20.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub20.TabIndex = 20
        Me.btnHomeSub20.UseVisualStyleBackColor = False
        Me.btnHomeSub20.Visible = False
        '
        'btnHomeSub21
        '
        Me.btnHomeSub21.BackColor = System.Drawing.Color.White
        Me.btnHomeSub21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub21.FlatAppearance.BorderSize = 0
        Me.btnHomeSub21.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub21.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub21.Location = New System.Drawing.Point(176, 239)
        Me.btnHomeSub21.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub21.Name = "btnHomeSub21"
        Me.btnHomeSub21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub21.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub21.TabIndex = 21
        Me.btnHomeSub21.UseVisualStyleBackColor = False
        Me.btnHomeSub21.Visible = False
        '
        'btnHomeSub16
        '
        Me.btnHomeSub16.BackColor = System.Drawing.Color.White
        Me.btnHomeSub16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub16.FlatAppearance.BorderSize = 0
        Me.btnHomeSub16.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub16.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub16.Location = New System.Drawing.Point(1, 205)
        Me.btnHomeSub16.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub16.Name = "btnHomeSub16"
        Me.btnHomeSub16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub16.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub16.TabIndex = 16
        Me.btnHomeSub16.UseVisualStyleBackColor = False
        Me.btnHomeSub16.Visible = False
        '
        'btnHomeSub17
        '
        Me.btnHomeSub17.BackColor = System.Drawing.Color.White
        Me.btnHomeSub17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub17.FlatAppearance.BorderSize = 0
        Me.btnHomeSub17.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub17.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub17.Location = New System.Drawing.Point(90, 205)
        Me.btnHomeSub17.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub17.Name = "btnHomeSub17"
        Me.btnHomeSub17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub17.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub17.TabIndex = 17
        Me.btnHomeSub17.UseVisualStyleBackColor = False
        Me.btnHomeSub17.Visible = False
        '
        'btnHomeSub18
        '
        Me.btnHomeSub18.BackColor = System.Drawing.Color.White
        Me.btnHomeSub18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub18.FlatAppearance.BorderSize = 0
        Me.btnHomeSub18.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub18.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub18.Location = New System.Drawing.Point(176, 205)
        Me.btnHomeSub18.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub18.Name = "btnHomeSub18"
        Me.btnHomeSub18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub18.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub18.TabIndex = 18
        Me.btnHomeSub18.UseVisualStyleBackColor = False
        Me.btnHomeSub18.Visible = False
        '
        'btnHomeSub13
        '
        Me.btnHomeSub13.BackColor = System.Drawing.Color.White
        Me.btnHomeSub13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub13.FlatAppearance.BorderSize = 0
        Me.btnHomeSub13.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub13.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub13.Location = New System.Drawing.Point(1, 170)
        Me.btnHomeSub13.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub13.Name = "btnHomeSub13"
        Me.btnHomeSub13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub13.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub13.TabIndex = 13
        Me.btnHomeSub13.UseVisualStyleBackColor = False
        Me.btnHomeSub13.Visible = False
        '
        'btnHomeSub14
        '
        Me.btnHomeSub14.BackColor = System.Drawing.Color.White
        Me.btnHomeSub14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub14.FlatAppearance.BorderSize = 0
        Me.btnHomeSub14.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub14.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub14.Location = New System.Drawing.Point(90, 170)
        Me.btnHomeSub14.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub14.Name = "btnHomeSub14"
        Me.btnHomeSub14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub14.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub14.TabIndex = 14
        Me.btnHomeSub14.UseVisualStyleBackColor = False
        Me.btnHomeSub14.Visible = False
        '
        'btnHomeSub15
        '
        Me.btnHomeSub15.BackColor = System.Drawing.Color.White
        Me.btnHomeSub15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub15.FlatAppearance.BorderSize = 0
        Me.btnHomeSub15.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub15.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub15.Location = New System.Drawing.Point(176, 170)
        Me.btnHomeSub15.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub15.Name = "btnHomeSub15"
        Me.btnHomeSub15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub15.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub15.TabIndex = 15
        Me.btnHomeSub15.UseVisualStyleBackColor = False
        Me.btnHomeSub15.Visible = False
        '
        'btnHomeSub10
        '
        Me.btnHomeSub10.BackColor = System.Drawing.Color.White
        Me.btnHomeSub10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub10.FlatAppearance.BorderSize = 0
        Me.btnHomeSub10.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub10.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub10.Location = New System.Drawing.Point(1, 136)
        Me.btnHomeSub10.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub10.Name = "btnHomeSub10"
        Me.btnHomeSub10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub10.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub10.TabIndex = 10
        Me.btnHomeSub10.UseVisualStyleBackColor = False
        Me.btnHomeSub10.Visible = False
        '
        'btnHomeSub11
        '
        Me.btnHomeSub11.BackColor = System.Drawing.Color.White
        Me.btnHomeSub11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub11.FlatAppearance.BorderSize = 0
        Me.btnHomeSub11.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub11.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub11.Location = New System.Drawing.Point(90, 136)
        Me.btnHomeSub11.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub11.Name = "btnHomeSub11"
        Me.btnHomeSub11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub11.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub11.TabIndex = 11
        Me.btnHomeSub11.UseVisualStyleBackColor = False
        Me.btnHomeSub11.Visible = False
        '
        'btnHomeSub12
        '
        Me.btnHomeSub12.BackColor = System.Drawing.Color.White
        Me.btnHomeSub12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub12.FlatAppearance.BorderSize = 0
        Me.btnHomeSub12.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub12.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub12.Location = New System.Drawing.Point(176, 136)
        Me.btnHomeSub12.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub12.Name = "btnHomeSub12"
        Me.btnHomeSub12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub12.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub12.TabIndex = 12
        Me.btnHomeSub12.UseVisualStyleBackColor = False
        Me.btnHomeSub12.Visible = False
        '
        'btnHomeSub7
        '
        Me.btnHomeSub7.BackColor = System.Drawing.Color.White
        Me.btnHomeSub7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub7.FlatAppearance.BorderSize = 0
        Me.btnHomeSub7.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub7.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub7.Location = New System.Drawing.Point(1, 103)
        Me.btnHomeSub7.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub7.Name = "btnHomeSub7"
        Me.btnHomeSub7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub7.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub7.TabIndex = 7
        Me.btnHomeSub7.UseVisualStyleBackColor = False
        '
        'btnHomeSub8
        '
        Me.btnHomeSub8.BackColor = System.Drawing.Color.White
        Me.btnHomeSub8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub8.FlatAppearance.BorderSize = 0
        Me.btnHomeSub8.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub8.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub8.Location = New System.Drawing.Point(89, 104)
        Me.btnHomeSub8.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub8.Name = "btnHomeSub8"
        Me.btnHomeSub8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub8.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub8.TabIndex = 8
        Me.btnHomeSub8.UseVisualStyleBackColor = False
        '
        'btnHomeSub9
        '
        Me.btnHomeSub9.BackColor = System.Drawing.Color.White
        Me.btnHomeSub9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub9.FlatAppearance.BorderSize = 0
        Me.btnHomeSub9.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub9.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub9.Location = New System.Drawing.Point(176, 104)
        Me.btnHomeSub9.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub9.Name = "btnHomeSub9"
        Me.btnHomeSub9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub9.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub9.TabIndex = 9
        Me.btnHomeSub9.UseVisualStyleBackColor = False
        '
        'btnHomeSub4
        '
        Me.btnHomeSub4.BackColor = System.Drawing.Color.White
        Me.btnHomeSub4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub4.FlatAppearance.BorderSize = 0
        Me.btnHomeSub4.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub4.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub4.Location = New System.Drawing.Point(1, 69)
        Me.btnHomeSub4.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub4.Name = "btnHomeSub4"
        Me.btnHomeSub4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub4.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub4.TabIndex = 4
        Me.btnHomeSub4.UseVisualStyleBackColor = False
        '
        'btnHomeSub5
        '
        Me.btnHomeSub5.BackColor = System.Drawing.Color.White
        Me.btnHomeSub5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub5.FlatAppearance.BorderSize = 0
        Me.btnHomeSub5.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub5.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub5.Location = New System.Drawing.Point(89, 70)
        Me.btnHomeSub5.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub5.Name = "btnHomeSub5"
        Me.btnHomeSub5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub5.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub5.TabIndex = 5
        Me.btnHomeSub5.UseVisualStyleBackColor = False
        '
        'btnHomeSub6
        '
        Me.btnHomeSub6.BackColor = System.Drawing.Color.White
        Me.btnHomeSub6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub6.FlatAppearance.BorderSize = 0
        Me.btnHomeSub6.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub6.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub6.Location = New System.Drawing.Point(176, 70)
        Me.btnHomeSub6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub6.Name = "btnHomeSub6"
        Me.btnHomeSub6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub6.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub6.TabIndex = 6
        Me.btnHomeSub6.UseVisualStyleBackColor = False
        '
        'btnHomeSub1
        '
        Me.btnHomeSub1.BackColor = System.Drawing.Color.White
        Me.btnHomeSub1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub1.FlatAppearance.BorderSize = 0
        Me.btnHomeSub1.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub1.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub1.Location = New System.Drawing.Point(1, 35)
        Me.btnHomeSub1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub1.Name = "btnHomeSub1"
        Me.btnHomeSub1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub1.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub1.TabIndex = 1
        Me.btnHomeSub1.UseVisualStyleBackColor = False
        '
        'btnHomeSub2
        '
        Me.btnHomeSub2.BackColor = System.Drawing.Color.White
        Me.btnHomeSub2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub2.FlatAppearance.BorderSize = 0
        Me.btnHomeSub2.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub2.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub2.Location = New System.Drawing.Point(89, 35)
        Me.btnHomeSub2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub2.Name = "btnHomeSub2"
        Me.btnHomeSub2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub2.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub2.TabIndex = 2
        Me.btnHomeSub2.UseVisualStyleBackColor = False
        '
        'btnHomeSub3
        '
        Me.btnHomeSub3.BackColor = System.Drawing.Color.White
        Me.btnHomeSub3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub3.FlatAppearance.BorderSize = 0
        Me.btnHomeSub3.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub3.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub3.Location = New System.Drawing.Point(176, 35)
        Me.btnHomeSub3.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomeSub3.Name = "btnHomeSub3"
        Me.btnHomeSub3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub3.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub3.TabIndex = 3
        Me.btnHomeSub3.UseVisualStyleBackColor = False
        '
        'tltpCommentary
        '
        Me.tltpCommentary.Active = False
        '
        'pnlCommentaryEntry
        '
        Me.pnlCommentaryEntry.BackColor = System.Drawing.Color.Lavender
        Me.pnlCommentaryEntry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCommentaryEntry.Controls.Add(Me.lblCommentType)
        Me.pnlCommentaryEntry.Controls.Add(Me.cmbCommentType)
        Me.pnlCommentaryEntry.Controls.Add(Me.txtHeadline)
        Me.pnlCommentaryEntry.Controls.Add(Me.txtTime)
        Me.pnlCommentaryEntry.Controls.Add(Me.txtComment)
        Me.pnlCommentaryEntry.Controls.Add(Me.btnRefreshPlayers)
        Me.pnlCommentaryEntry.Controls.Add(Me.btnClear)
        Me.pnlCommentaryEntry.Controls.Add(Me.btnSave)
        Me.pnlCommentaryEntry.Controls.Add(Me.Label4)
        Me.pnlCommentaryEntry.Controls.Add(Me.lblHeadline)
        Me.pnlCommentaryEntry.Controls.Add(Me.lblTime)
        Me.pnlCommentaryEntry.Location = New System.Drawing.Point(260, 5)
        Me.pnlCommentaryEntry.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlCommentaryEntry.Name = "pnlCommentaryEntry"
        Me.pnlCommentaryEntry.Size = New System.Drawing.Size(487, 147)
        Me.pnlCommentaryEntry.TabIndex = 4
        Me.pnlCommentaryEntry.TabStop = True
        '
        'lblCommentType
        '
        Me.lblCommentType.AutoSize = True
        Me.lblCommentType.Location = New System.Drawing.Point(373, 42)
        Me.lblCommentType.Name = "lblCommentType"
        Me.lblCommentType.Size = New System.Drawing.Size(79, 15)
        Me.lblCommentType.TabIndex = 10
        Me.lblCommentType.Text = "Comment type:"
        '
        'cmbCommentType
        '
        Me.cmbCommentType.BackColor = System.Drawing.Color.White
        Me.cmbCommentType.FormattingEnabled = True
        Me.cmbCommentType.Location = New System.Drawing.Point(374, 57)
        Me.cmbCommentType.Name = "cmbCommentType"
        Me.cmbCommentType.Size = New System.Drawing.Size(106, 23)
        Me.cmbCommentType.TabIndex = 9
        '
        'txtHeadline
        '
        Me.txtHeadline.Location = New System.Drawing.Point(82, 18)
        Me.txtHeadline.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtHeadline.MaxLength = 100
        Me.txtHeadline.Name = "txtHeadline"
        Me.txtHeadline.Size = New System.Drawing.Size(401, 22)
        Me.txtHeadline.TabIndex = 3
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(3, 18)
        Me.txtTime.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtTime.MaxLength = 3
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(70, 22)
        Me.txtTime.TabIndex = 2
        '
        'txtComment
        '
        Me.txtComment.Location = New System.Drawing.Point(4, 57)
        Me.txtComment.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtComment.Multiline = True
        Me.txtComment.Name = "txtComment"
        Me.txtComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtComment.Size = New System.Drawing.Size(369, 82)
        Me.txtComment.TabIndex = 5
        '
        'btnRefreshPlayers
        '
        Me.btnRefreshPlayers.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnRefreshPlayers.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnRefreshPlayers.ForeColor = System.Drawing.Color.White
        Me.btnRefreshPlayers.Location = New System.Drawing.Point(374, 115)
        Me.btnRefreshPlayers.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRefreshPlayers.Name = "btnRefreshPlayers"
        Me.btnRefreshPlayers.Size = New System.Drawing.Size(108, 25)
        Me.btnRefreshPlayers.TabIndex = 8
        Me.btnRefreshPlayers.Text = "Refresh Players"
        Me.btnRefreshPlayers.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnClear.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClear.ForeColor = System.Drawing.Color.White
        Me.btnClear.Location = New System.Drawing.Point(434, 86)
        Me.btnClear.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(48, 25)
        Me.btnClear.TabIndex = 7
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(374, 86)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(61, 25)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(1, 42)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Comment:"
        '
        'lblHeadline
        '
        Me.lblHeadline.AutoSize = True
        Me.lblHeadline.BackColor = System.Drawing.Color.Transparent
        Me.lblHeadline.Location = New System.Drawing.Point(80, 2)
        Me.lblHeadline.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblHeadline.Name = "lblHeadline"
        Me.lblHeadline.Size = New System.Drawing.Size(52, 15)
        Me.lblHeadline.TabIndex = 1
        Me.lblHeadline.Text = "Headline:"
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.BackColor = System.Drawing.Color.Transparent
        Me.lblTime.Location = New System.Drawing.Point(5, 2)
        Me.lblTime.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(33, 15)
        Me.lblTime.TabIndex = 0
        Me.lblTime.Text = "Time:"
        '
        'tmrPBP
        '
        Me.tmrPBP.Interval = 5000
        '
        'pnlHome
        '
        Me.pnlHome.AutoScroll = True
        Me.pnlHome.Controls.Add(Me.btnHomePlayer35)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer34)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer41)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer40)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer36)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer39)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer37)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer38)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer27)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer26)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer25)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer33)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer24)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer23)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer32)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer28)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer31)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer29)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer30)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer16)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer15)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer14)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer22)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer13)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer12)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer21)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer17)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer20)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer18)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer19)
        Me.pnlHome.Controls.Add(Me.btnHomeTeam)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer5)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer4)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer3)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer11)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer2)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer1)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer10)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer6)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer9)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer7)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer8)
        Me.pnlHome.Location = New System.Drawing.Point(22, 5)
        Me.pnlHome.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlHome.Name = "pnlHome"
        Me.pnlHome.Size = New System.Drawing.Size(229, 305)
        Me.pnlHome.TabIndex = 0
        '
        'btnHomePlayer35
        '
        Me.btnHomePlayer35.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer35.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer35.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer35.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer35.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer35.Location = New System.Drawing.Point(0, 880)
        Me.btnHomePlayer35.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer35.Name = "btnHomePlayer35"
        Me.btnHomePlayer35.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer35.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer35.TabIndex = 35
        Me.btnHomePlayer35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer35.UseVisualStyleBackColor = False
        Me.btnHomePlayer35.Visible = False
        '
        'btnHomePlayer34
        '
        Me.btnHomePlayer34.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer34.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer34.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer34.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer34.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer34.Location = New System.Drawing.Point(0, 855)
        Me.btnHomePlayer34.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer34.Name = "btnHomePlayer34"
        Me.btnHomePlayer34.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer34.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer34.TabIndex = 34
        Me.btnHomePlayer34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer34.UseVisualStyleBackColor = False
        Me.btnHomePlayer34.Visible = False
        '
        'btnHomePlayer41
        '
        Me.btnHomePlayer41.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer41.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer41.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer41.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer41.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer41.Location = New System.Drawing.Point(0, 1030)
        Me.btnHomePlayer41.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer41.Name = "btnHomePlayer41"
        Me.btnHomePlayer41.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer41.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer41.TabIndex = 41
        Me.btnHomePlayer41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer41.UseVisualStyleBackColor = False
        Me.btnHomePlayer41.Visible = False
        '
        'btnHomePlayer40
        '
        Me.btnHomePlayer40.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer40.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer40.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer40.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer40.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer40.Location = New System.Drawing.Point(0, 1005)
        Me.btnHomePlayer40.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer40.Name = "btnHomePlayer40"
        Me.btnHomePlayer40.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer40.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer40.TabIndex = 40
        Me.btnHomePlayer40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer40.UseVisualStyleBackColor = False
        Me.btnHomePlayer40.Visible = False
        '
        'btnHomePlayer36
        '
        Me.btnHomePlayer36.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer36.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer36.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer36.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer36.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer36.Location = New System.Drawing.Point(0, 905)
        Me.btnHomePlayer36.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer36.Name = "btnHomePlayer36"
        Me.btnHomePlayer36.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer36.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer36.TabIndex = 36
        Me.btnHomePlayer36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer36.UseVisualStyleBackColor = False
        Me.btnHomePlayer36.Visible = False
        '
        'btnHomePlayer39
        '
        Me.btnHomePlayer39.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer39.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer39.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer39.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer39.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer39.Location = New System.Drawing.Point(0, 980)
        Me.btnHomePlayer39.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer39.Name = "btnHomePlayer39"
        Me.btnHomePlayer39.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer39.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer39.TabIndex = 39
        Me.btnHomePlayer39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer39.UseVisualStyleBackColor = False
        Me.btnHomePlayer39.Visible = False
        '
        'btnHomePlayer37
        '
        Me.btnHomePlayer37.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer37.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer37.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer37.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer37.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer37.Location = New System.Drawing.Point(0, 930)
        Me.btnHomePlayer37.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer37.Name = "btnHomePlayer37"
        Me.btnHomePlayer37.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer37.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer37.TabIndex = 37
        Me.btnHomePlayer37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer37.UseVisualStyleBackColor = False
        Me.btnHomePlayer37.Visible = False
        '
        'btnHomePlayer38
        '
        Me.btnHomePlayer38.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer38.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer38.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer38.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer38.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer38.Location = New System.Drawing.Point(0, 955)
        Me.btnHomePlayer38.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer38.Name = "btnHomePlayer38"
        Me.btnHomePlayer38.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer38.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer38.TabIndex = 38
        Me.btnHomePlayer38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer38.UseVisualStyleBackColor = False
        Me.btnHomePlayer38.Visible = False
        '
        'btnHomePlayer27
        '
        Me.btnHomePlayer27.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer27.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer27.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer27.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer27.Location = New System.Drawing.Point(0, 680)
        Me.btnHomePlayer27.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer27.Name = "btnHomePlayer27"
        Me.btnHomePlayer27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer27.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer27.TabIndex = 27
        Me.btnHomePlayer27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer27.UseVisualStyleBackColor = False
        Me.btnHomePlayer27.Visible = False
        '
        'btnHomePlayer26
        '
        Me.btnHomePlayer26.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer26.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer26.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer26.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer26.Location = New System.Drawing.Point(0, 655)
        Me.btnHomePlayer26.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer26.Name = "btnHomePlayer26"
        Me.btnHomePlayer26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer26.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer26.TabIndex = 26
        Me.btnHomePlayer26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer26.UseVisualStyleBackColor = False
        Me.btnHomePlayer26.Visible = False
        '
        'btnHomePlayer25
        '
        Me.btnHomePlayer25.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer25.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer25.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer25.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer25.Location = New System.Drawing.Point(0, 630)
        Me.btnHomePlayer25.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer25.Name = "btnHomePlayer25"
        Me.btnHomePlayer25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer25.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer25.TabIndex = 25
        Me.btnHomePlayer25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer25.UseVisualStyleBackColor = False
        Me.btnHomePlayer25.Visible = False
        '
        'btnHomePlayer33
        '
        Me.btnHomePlayer33.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer33.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer33.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer33.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer33.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer33.Location = New System.Drawing.Point(0, 830)
        Me.btnHomePlayer33.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer33.Name = "btnHomePlayer33"
        Me.btnHomePlayer33.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer33.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer33.TabIndex = 33
        Me.btnHomePlayer33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer33.UseVisualStyleBackColor = False
        Me.btnHomePlayer33.Visible = False
        '
        'btnHomePlayer24
        '
        Me.btnHomePlayer24.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer24.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer24.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer24.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer24.Location = New System.Drawing.Point(0, 605)
        Me.btnHomePlayer24.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer24.Name = "btnHomePlayer24"
        Me.btnHomePlayer24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer24.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer24.TabIndex = 24
        Me.btnHomePlayer24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer24.UseVisualStyleBackColor = False
        Me.btnHomePlayer24.Visible = False
        '
        'btnHomePlayer23
        '
        Me.btnHomePlayer23.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer23.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer23.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer23.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer23.Location = New System.Drawing.Point(0, 580)
        Me.btnHomePlayer23.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer23.Name = "btnHomePlayer23"
        Me.btnHomePlayer23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer23.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer23.TabIndex = 23
        Me.btnHomePlayer23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer23.UseVisualStyleBackColor = False
        Me.btnHomePlayer23.Visible = False
        '
        'btnHomePlayer32
        '
        Me.btnHomePlayer32.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer32.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer32.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer32.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer32.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer32.Location = New System.Drawing.Point(0, 805)
        Me.btnHomePlayer32.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer32.Name = "btnHomePlayer32"
        Me.btnHomePlayer32.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer32.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer32.TabIndex = 32
        Me.btnHomePlayer32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer32.UseVisualStyleBackColor = False
        Me.btnHomePlayer32.Visible = False
        '
        'btnHomePlayer28
        '
        Me.btnHomePlayer28.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer28.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer28.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer28.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer28.Location = New System.Drawing.Point(0, 705)
        Me.btnHomePlayer28.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer28.Name = "btnHomePlayer28"
        Me.btnHomePlayer28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer28.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer28.TabIndex = 28
        Me.btnHomePlayer28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer28.UseVisualStyleBackColor = False
        Me.btnHomePlayer28.Visible = False
        '
        'btnHomePlayer31
        '
        Me.btnHomePlayer31.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer31.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer31.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer31.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer31.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer31.Location = New System.Drawing.Point(0, 780)
        Me.btnHomePlayer31.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer31.Name = "btnHomePlayer31"
        Me.btnHomePlayer31.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer31.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer31.TabIndex = 31
        Me.btnHomePlayer31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer31.UseVisualStyleBackColor = False
        Me.btnHomePlayer31.Visible = False
        '
        'btnHomePlayer29
        '
        Me.btnHomePlayer29.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer29.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer29.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer29.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer29.Location = New System.Drawing.Point(0, 730)
        Me.btnHomePlayer29.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer29.Name = "btnHomePlayer29"
        Me.btnHomePlayer29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer29.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer29.TabIndex = 29
        Me.btnHomePlayer29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer29.UseVisualStyleBackColor = False
        Me.btnHomePlayer29.Visible = False
        '
        'btnHomePlayer30
        '
        Me.btnHomePlayer30.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer30.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer30.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer30.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer30.Location = New System.Drawing.Point(0, 755)
        Me.btnHomePlayer30.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer30.Name = "btnHomePlayer30"
        Me.btnHomePlayer30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer30.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer30.TabIndex = 30
        Me.btnHomePlayer30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer30.UseVisualStyleBackColor = False
        Me.btnHomePlayer30.Visible = False
        '
        'btnHomePlayer16
        '
        Me.btnHomePlayer16.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer16.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer16.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer16.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer16.Location = New System.Drawing.Point(0, 405)
        Me.btnHomePlayer16.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer16.Name = "btnHomePlayer16"
        Me.btnHomePlayer16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer16.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer16.TabIndex = 16
        Me.btnHomePlayer16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer16.UseVisualStyleBackColor = False
        Me.btnHomePlayer16.Visible = False
        '
        'btnHomePlayer15
        '
        Me.btnHomePlayer15.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer15.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer15.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer15.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer15.Location = New System.Drawing.Point(0, 380)
        Me.btnHomePlayer15.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer15.Name = "btnHomePlayer15"
        Me.btnHomePlayer15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer15.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer15.TabIndex = 15
        Me.btnHomePlayer15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer15.UseVisualStyleBackColor = False
        Me.btnHomePlayer15.Visible = False
        '
        'btnHomePlayer14
        '
        Me.btnHomePlayer14.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer14.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer14.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer14.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer14.Location = New System.Drawing.Point(0, 355)
        Me.btnHomePlayer14.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer14.Name = "btnHomePlayer14"
        Me.btnHomePlayer14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer14.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer14.TabIndex = 14
        Me.btnHomePlayer14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer14.UseVisualStyleBackColor = False
        Me.btnHomePlayer14.Visible = False
        '
        'btnHomePlayer22
        '
        Me.btnHomePlayer22.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer22.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer22.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer22.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer22.Location = New System.Drawing.Point(0, 555)
        Me.btnHomePlayer22.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer22.Name = "btnHomePlayer22"
        Me.btnHomePlayer22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer22.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer22.TabIndex = 22
        Me.btnHomePlayer22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer22.UseVisualStyleBackColor = False
        Me.btnHomePlayer22.Visible = False
        '
        'btnHomePlayer13
        '
        Me.btnHomePlayer13.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer13.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer13.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer13.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer13.Location = New System.Drawing.Point(0, 330)
        Me.btnHomePlayer13.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer13.Name = "btnHomePlayer13"
        Me.btnHomePlayer13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer13.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer13.TabIndex = 13
        Me.btnHomePlayer13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer13.UseVisualStyleBackColor = False
        Me.btnHomePlayer13.Visible = False
        '
        'btnHomePlayer12
        '
        Me.btnHomePlayer12.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer12.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer12.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer12.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer12.Location = New System.Drawing.Point(0, 305)
        Me.btnHomePlayer12.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer12.Name = "btnHomePlayer12"
        Me.btnHomePlayer12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer12.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer12.TabIndex = 12
        Me.btnHomePlayer12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer12.UseVisualStyleBackColor = False
        Me.btnHomePlayer12.Visible = False
        '
        'btnHomePlayer21
        '
        Me.btnHomePlayer21.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer21.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer21.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer21.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer21.Location = New System.Drawing.Point(0, 530)
        Me.btnHomePlayer21.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer21.Name = "btnHomePlayer21"
        Me.btnHomePlayer21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer21.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer21.TabIndex = 21
        Me.btnHomePlayer21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer21.UseVisualStyleBackColor = False
        Me.btnHomePlayer21.Visible = False
        '
        'btnHomePlayer17
        '
        Me.btnHomePlayer17.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer17.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer17.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer17.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer17.Location = New System.Drawing.Point(0, 430)
        Me.btnHomePlayer17.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer17.Name = "btnHomePlayer17"
        Me.btnHomePlayer17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer17.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer17.TabIndex = 17
        Me.btnHomePlayer17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer17.UseVisualStyleBackColor = False
        Me.btnHomePlayer17.Visible = False
        '
        'btnHomePlayer20
        '
        Me.btnHomePlayer20.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer20.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer20.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer20.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer20.Location = New System.Drawing.Point(0, 505)
        Me.btnHomePlayer20.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer20.Name = "btnHomePlayer20"
        Me.btnHomePlayer20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer20.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer20.TabIndex = 20
        Me.btnHomePlayer20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer20.UseVisualStyleBackColor = False
        Me.btnHomePlayer20.Visible = False
        '
        'btnHomePlayer18
        '
        Me.btnHomePlayer18.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer18.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer18.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer18.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer18.Location = New System.Drawing.Point(0, 455)
        Me.btnHomePlayer18.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer18.Name = "btnHomePlayer18"
        Me.btnHomePlayer18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer18.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer18.TabIndex = 18
        Me.btnHomePlayer18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer18.UseVisualStyleBackColor = False
        Me.btnHomePlayer18.Visible = False
        '
        'btnHomePlayer19
        '
        Me.btnHomePlayer19.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer19.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer19.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer19.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer19.Location = New System.Drawing.Point(0, 480)
        Me.btnHomePlayer19.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnHomePlayer19.Name = "btnHomePlayer19"
        Me.btnHomePlayer19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer19.Size = New System.Drawing.Size(212, 25)
        Me.btnHomePlayer19.TabIndex = 19
        Me.btnHomePlayer19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer19.UseVisualStyleBackColor = False
        Me.btnHomePlayer19.Visible = False
        '
        'pnlVisit
        '
        Me.pnlVisit.AutoScroll = True
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer35)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer34)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer41)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer40)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer36)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer39)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer37)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer38)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer27)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer26)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer25)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer33)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer24)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer23)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer32)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer28)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer31)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer29)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer30)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer16)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer15)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer14)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer22)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer13)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer12)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer21)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer17)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer20)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer18)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer19)
        Me.pnlVisit.Controls.Add(Me.btnVisitTeam)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer5)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer4)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer3)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer11)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer2)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer1)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer10)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer6)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer9)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer7)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer8)
        Me.pnlVisit.Location = New System.Drawing.Point(757, 5)
        Me.pnlVisit.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlVisit.Name = "pnlVisit"
        Me.pnlVisit.Size = New System.Drawing.Size(229, 305)
        Me.pnlVisit.TabIndex = 2
        '
        'btnVisitPlayer35
        '
        Me.btnVisitPlayer35.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer35.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer35.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer35.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer35.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer35.Location = New System.Drawing.Point(0, 880)
        Me.btnVisitPlayer35.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer35.Name = "btnVisitPlayer35"
        Me.btnVisitPlayer35.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer35.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer35.TabIndex = 35
        Me.btnVisitPlayer35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer35.UseVisualStyleBackColor = False
        Me.btnVisitPlayer35.Visible = False
        '
        'btnVisitPlayer34
        '
        Me.btnVisitPlayer34.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer34.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer34.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer34.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer34.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer34.Location = New System.Drawing.Point(0, 855)
        Me.btnVisitPlayer34.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer34.Name = "btnVisitPlayer34"
        Me.btnVisitPlayer34.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer34.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer34.TabIndex = 34
        Me.btnVisitPlayer34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer34.UseVisualStyleBackColor = False
        Me.btnVisitPlayer34.Visible = False
        '
        'btnVisitPlayer41
        '
        Me.btnVisitPlayer41.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer41.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer41.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer41.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer41.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer41.Location = New System.Drawing.Point(0, 1030)
        Me.btnVisitPlayer41.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer41.Name = "btnVisitPlayer41"
        Me.btnVisitPlayer41.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer41.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer41.TabIndex = 41
        Me.btnVisitPlayer41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer41.UseVisualStyleBackColor = False
        Me.btnVisitPlayer41.Visible = False
        '
        'btnVisitPlayer40
        '
        Me.btnVisitPlayer40.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer40.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer40.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer40.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer40.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer40.Location = New System.Drawing.Point(0, 1005)
        Me.btnVisitPlayer40.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer40.Name = "btnVisitPlayer40"
        Me.btnVisitPlayer40.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer40.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer40.TabIndex = 40
        Me.btnVisitPlayer40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer40.UseVisualStyleBackColor = False
        Me.btnVisitPlayer40.Visible = False
        '
        'btnVisitPlayer36
        '
        Me.btnVisitPlayer36.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer36.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer36.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer36.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer36.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer36.Location = New System.Drawing.Point(0, 905)
        Me.btnVisitPlayer36.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer36.Name = "btnVisitPlayer36"
        Me.btnVisitPlayer36.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer36.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer36.TabIndex = 36
        Me.btnVisitPlayer36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer36.UseVisualStyleBackColor = False
        Me.btnVisitPlayer36.Visible = False
        '
        'btnVisitPlayer39
        '
        Me.btnVisitPlayer39.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer39.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer39.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer39.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer39.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer39.Location = New System.Drawing.Point(0, 980)
        Me.btnVisitPlayer39.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer39.Name = "btnVisitPlayer39"
        Me.btnVisitPlayer39.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer39.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer39.TabIndex = 39
        Me.btnVisitPlayer39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer39.UseVisualStyleBackColor = False
        Me.btnVisitPlayer39.Visible = False
        '
        'btnVisitPlayer37
        '
        Me.btnVisitPlayer37.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer37.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer37.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer37.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer37.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer37.Location = New System.Drawing.Point(0, 930)
        Me.btnVisitPlayer37.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer37.Name = "btnVisitPlayer37"
        Me.btnVisitPlayer37.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer37.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer37.TabIndex = 37
        Me.btnVisitPlayer37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer37.UseVisualStyleBackColor = False
        Me.btnVisitPlayer37.Visible = False
        '
        'btnVisitPlayer38
        '
        Me.btnVisitPlayer38.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer38.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer38.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer38.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer38.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer38.Location = New System.Drawing.Point(0, 955)
        Me.btnVisitPlayer38.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer38.Name = "btnVisitPlayer38"
        Me.btnVisitPlayer38.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer38.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer38.TabIndex = 38
        Me.btnVisitPlayer38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer38.UseVisualStyleBackColor = False
        Me.btnVisitPlayer38.Visible = False
        '
        'btnVisitPlayer27
        '
        Me.btnVisitPlayer27.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer27.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer27.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer27.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer27.Location = New System.Drawing.Point(0, 680)
        Me.btnVisitPlayer27.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer27.Name = "btnVisitPlayer27"
        Me.btnVisitPlayer27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer27.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer27.TabIndex = 27
        Me.btnVisitPlayer27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer27.UseVisualStyleBackColor = False
        Me.btnVisitPlayer27.Visible = False
        '
        'btnVisitPlayer26
        '
        Me.btnVisitPlayer26.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer26.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer26.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer26.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer26.Location = New System.Drawing.Point(0, 655)
        Me.btnVisitPlayer26.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer26.Name = "btnVisitPlayer26"
        Me.btnVisitPlayer26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer26.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer26.TabIndex = 26
        Me.btnVisitPlayer26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer26.UseVisualStyleBackColor = False
        Me.btnVisitPlayer26.Visible = False
        '
        'btnVisitPlayer25
        '
        Me.btnVisitPlayer25.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer25.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer25.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer25.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer25.Location = New System.Drawing.Point(0, 630)
        Me.btnVisitPlayer25.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer25.Name = "btnVisitPlayer25"
        Me.btnVisitPlayer25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer25.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer25.TabIndex = 25
        Me.btnVisitPlayer25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer25.UseVisualStyleBackColor = False
        Me.btnVisitPlayer25.Visible = False
        '
        'btnVisitPlayer33
        '
        Me.btnVisitPlayer33.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer33.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer33.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer33.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer33.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer33.Location = New System.Drawing.Point(0, 830)
        Me.btnVisitPlayer33.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer33.Name = "btnVisitPlayer33"
        Me.btnVisitPlayer33.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer33.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer33.TabIndex = 33
        Me.btnVisitPlayer33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer33.UseVisualStyleBackColor = False
        Me.btnVisitPlayer33.Visible = False
        '
        'btnVisitPlayer24
        '
        Me.btnVisitPlayer24.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer24.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer24.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer24.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer24.Location = New System.Drawing.Point(0, 605)
        Me.btnVisitPlayer24.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer24.Name = "btnVisitPlayer24"
        Me.btnVisitPlayer24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer24.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer24.TabIndex = 24
        Me.btnVisitPlayer24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer24.UseVisualStyleBackColor = False
        Me.btnVisitPlayer24.Visible = False
        '
        'btnVisitPlayer23
        '
        Me.btnVisitPlayer23.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer23.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer23.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer23.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer23.Location = New System.Drawing.Point(0, 580)
        Me.btnVisitPlayer23.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer23.Name = "btnVisitPlayer23"
        Me.btnVisitPlayer23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer23.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer23.TabIndex = 23
        Me.btnVisitPlayer23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer23.UseVisualStyleBackColor = False
        Me.btnVisitPlayer23.Visible = False
        '
        'btnVisitPlayer32
        '
        Me.btnVisitPlayer32.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer32.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer32.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer32.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer32.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer32.Location = New System.Drawing.Point(0, 805)
        Me.btnVisitPlayer32.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer32.Name = "btnVisitPlayer32"
        Me.btnVisitPlayer32.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer32.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer32.TabIndex = 32
        Me.btnVisitPlayer32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer32.UseVisualStyleBackColor = False
        Me.btnVisitPlayer32.Visible = False
        '
        'btnVisitPlayer28
        '
        Me.btnVisitPlayer28.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer28.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer28.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer28.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer28.Location = New System.Drawing.Point(0, 705)
        Me.btnVisitPlayer28.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer28.Name = "btnVisitPlayer28"
        Me.btnVisitPlayer28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer28.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer28.TabIndex = 28
        Me.btnVisitPlayer28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer28.UseVisualStyleBackColor = False
        Me.btnVisitPlayer28.Visible = False
        '
        'btnVisitPlayer31
        '
        Me.btnVisitPlayer31.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer31.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer31.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer31.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer31.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer31.Location = New System.Drawing.Point(0, 780)
        Me.btnVisitPlayer31.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer31.Name = "btnVisitPlayer31"
        Me.btnVisitPlayer31.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer31.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer31.TabIndex = 31
        Me.btnVisitPlayer31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer31.UseVisualStyleBackColor = False
        Me.btnVisitPlayer31.Visible = False
        '
        'btnVisitPlayer29
        '
        Me.btnVisitPlayer29.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer29.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer29.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer29.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer29.Location = New System.Drawing.Point(0, 730)
        Me.btnVisitPlayer29.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer29.Name = "btnVisitPlayer29"
        Me.btnVisitPlayer29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer29.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer29.TabIndex = 29
        Me.btnVisitPlayer29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer29.UseVisualStyleBackColor = False
        Me.btnVisitPlayer29.Visible = False
        '
        'btnVisitPlayer30
        '
        Me.btnVisitPlayer30.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer30.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer30.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer30.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer30.Location = New System.Drawing.Point(0, 755)
        Me.btnVisitPlayer30.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer30.Name = "btnVisitPlayer30"
        Me.btnVisitPlayer30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer30.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer30.TabIndex = 30
        Me.btnVisitPlayer30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer30.UseVisualStyleBackColor = False
        Me.btnVisitPlayer30.Visible = False
        '
        'btnVisitPlayer16
        '
        Me.btnVisitPlayer16.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer16.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer16.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer16.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer16.Location = New System.Drawing.Point(0, 405)
        Me.btnVisitPlayer16.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer16.Name = "btnVisitPlayer16"
        Me.btnVisitPlayer16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer16.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer16.TabIndex = 16
        Me.btnVisitPlayer16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer16.UseVisualStyleBackColor = False
        Me.btnVisitPlayer16.Visible = False
        '
        'btnVisitPlayer15
        '
        Me.btnVisitPlayer15.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer15.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer15.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer15.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer15.Location = New System.Drawing.Point(0, 380)
        Me.btnVisitPlayer15.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer15.Name = "btnVisitPlayer15"
        Me.btnVisitPlayer15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer15.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer15.TabIndex = 15
        Me.btnVisitPlayer15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer15.UseVisualStyleBackColor = False
        Me.btnVisitPlayer15.Visible = False
        '
        'btnVisitPlayer14
        '
        Me.btnVisitPlayer14.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer14.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer14.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer14.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer14.Location = New System.Drawing.Point(0, 355)
        Me.btnVisitPlayer14.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer14.Name = "btnVisitPlayer14"
        Me.btnVisitPlayer14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer14.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer14.TabIndex = 14
        Me.btnVisitPlayer14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer14.UseVisualStyleBackColor = False
        Me.btnVisitPlayer14.Visible = False
        '
        'btnVisitPlayer22
        '
        Me.btnVisitPlayer22.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer22.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer22.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer22.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer22.Location = New System.Drawing.Point(0, 555)
        Me.btnVisitPlayer22.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer22.Name = "btnVisitPlayer22"
        Me.btnVisitPlayer22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer22.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer22.TabIndex = 22
        Me.btnVisitPlayer22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer22.UseVisualStyleBackColor = False
        Me.btnVisitPlayer22.Visible = False
        '
        'btnVisitPlayer13
        '
        Me.btnVisitPlayer13.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer13.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer13.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer13.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer13.Location = New System.Drawing.Point(0, 330)
        Me.btnVisitPlayer13.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer13.Name = "btnVisitPlayer13"
        Me.btnVisitPlayer13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer13.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer13.TabIndex = 13
        Me.btnVisitPlayer13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer13.UseVisualStyleBackColor = False
        Me.btnVisitPlayer13.Visible = False
        '
        'btnVisitPlayer12
        '
        Me.btnVisitPlayer12.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer12.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer12.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer12.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer12.Location = New System.Drawing.Point(0, 305)
        Me.btnVisitPlayer12.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer12.Name = "btnVisitPlayer12"
        Me.btnVisitPlayer12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer12.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer12.TabIndex = 12
        Me.btnVisitPlayer12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer12.UseVisualStyleBackColor = False
        Me.btnVisitPlayer12.Visible = False
        '
        'btnVisitPlayer21
        '
        Me.btnVisitPlayer21.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer21.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer21.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer21.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer21.Location = New System.Drawing.Point(0, 530)
        Me.btnVisitPlayer21.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer21.Name = "btnVisitPlayer21"
        Me.btnVisitPlayer21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer21.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer21.TabIndex = 21
        Me.btnVisitPlayer21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer21.UseVisualStyleBackColor = False
        Me.btnVisitPlayer21.Visible = False
        '
        'btnVisitPlayer17
        '
        Me.btnVisitPlayer17.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer17.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer17.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer17.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer17.Location = New System.Drawing.Point(0, 430)
        Me.btnVisitPlayer17.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer17.Name = "btnVisitPlayer17"
        Me.btnVisitPlayer17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer17.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer17.TabIndex = 17
        Me.btnVisitPlayer17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer17.UseVisualStyleBackColor = False
        Me.btnVisitPlayer17.Visible = False
        '
        'btnVisitPlayer20
        '
        Me.btnVisitPlayer20.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer20.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer20.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer20.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer20.Location = New System.Drawing.Point(0, 505)
        Me.btnVisitPlayer20.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer20.Name = "btnVisitPlayer20"
        Me.btnVisitPlayer20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer20.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer20.TabIndex = 20
        Me.btnVisitPlayer20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer20.UseVisualStyleBackColor = False
        Me.btnVisitPlayer20.Visible = False
        '
        'btnVisitPlayer18
        '
        Me.btnVisitPlayer18.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer18.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer18.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer18.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer18.Location = New System.Drawing.Point(0, 455)
        Me.btnVisitPlayer18.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer18.Name = "btnVisitPlayer18"
        Me.btnVisitPlayer18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer18.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer18.TabIndex = 18
        Me.btnVisitPlayer18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer18.UseVisualStyleBackColor = False
        Me.btnVisitPlayer18.Visible = False
        '
        'btnVisitPlayer19
        '
        Me.btnVisitPlayer19.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer19.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer19.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer19.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer19.Location = New System.Drawing.Point(0, 480)
        Me.btnVisitPlayer19.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnVisitPlayer19.Name = "btnVisitPlayer19"
        Me.btnVisitPlayer19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer19.Size = New System.Drawing.Size(212, 25)
        Me.btnVisitPlayer19.TabIndex = 19
        Me.btnVisitPlayer19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer19.UseVisualStyleBackColor = False
        Me.btnVisitPlayer19.Visible = False
        '
        'frmCommentary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(996, 458)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlHome)
        Me.Controls.Add(Me.pnlVisit)
        Me.Controls.Add(Me.pnlVisitBench)
        Me.Controls.Add(Me.pnlHomeBench)
        Me.Controls.Add(Me.pnlPBP)
        Me.Controls.Add(Me.lvwCommentary)
        Me.Controls.Add(Me.pnlCommentaryEntry)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Name = "frmCommentary"
        Me.cmnCommentary.ResumeLayout(False)
        Me.pnlPBP.ResumeLayout(False)
        Me.pnlPBP.PerformLayout()
        CType(Me.picRefresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlVisitBench.ResumeLayout(False)
        Me.pnlHomeBench.ResumeLayout(False)
        Me.pnlCommentaryEntry.ResumeLayout(False)
        Me.pnlCommentaryEntry.PerformLayout()
        Me.pnlHome.ResumeLayout(False)
        Me.pnlVisit.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents slbReporterEntry As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnHomeTeam As System.Windows.Forms.Button
    Friend WithEvents btnVisitTeam As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer11 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer6 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer7 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer8 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer9 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer10 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer1 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer2 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer3 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer4 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer5 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer11 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer6 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer7 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer8 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer9 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer10 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer1 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer2 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer3 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer4 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer5 As System.Windows.Forms.Button
    Friend WithEvents lvwCommentary As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlPBP As System.Windows.Forms.Panel
    Friend WithEvents lblPlayByPlay As System.Windows.Forms.Label
    Friend WithEvents pnlVisitBench As System.Windows.Forms.Panel
    Friend WithEvents pnlHomeBench As System.Windows.Forms.Panel
    Friend WithEvents btnHomeSub28 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub29 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub30 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub25 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub26 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub27 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub22 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub23 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub24 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub19 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub20 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub21 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub16 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub17 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub18 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub13 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub14 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub15 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub10 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub11 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub12 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub7 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub8 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub9 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub4 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub5 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub6 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub1 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub2 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub3 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub10 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub11 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub16 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub17 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub18 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub13 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub14 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub15 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub7 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub8 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub9 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub4 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub5 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub6 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub1 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub2 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub3 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub12 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub28 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub29 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub30 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub25 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub26 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub27 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub22 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub23 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub24 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub19 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub20 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub21 As System.Windows.Forms.Button
    Friend WithEvents cmnCommentary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tltpCommentary As System.Windows.Forms.ToolTip
    Friend WithEvents pnlCommentaryEntry As System.Windows.Forms.Panel
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblHeadline As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lvwPBP As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents tmrPBP As System.Windows.Forms.Timer
    Friend WithEvents picRefresh As System.Windows.Forms.PictureBox
    Friend WithEvents btnRefreshPlayers As System.Windows.Forms.Button
    Friend WithEvents pnlHome As System.Windows.Forms.Panel
    Friend WithEvents pnlVisit As System.Windows.Forms.Panel
    Friend WithEvents btnHomeCoach As System.Windows.Forms.Button
    Friend WithEvents btnVisitCoach As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer16 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer15 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer14 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer22 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer13 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer12 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer21 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer17 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer20 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer18 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer19 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer35 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer34 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer41 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer40 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer36 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer39 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer37 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer38 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer27 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer26 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer25 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer33 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer24 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer23 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer32 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer28 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer31 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer29 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer30 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer35 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer34 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer41 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer40 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer36 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer39 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer37 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer38 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer27 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer26 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer25 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer33 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer24 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer23 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer32 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer28 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer31 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer29 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer30 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer16 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer15 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer14 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer22 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer13 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer12 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer21 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer17 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer20 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer18 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer19 As System.Windows.Forms.Button
    Friend WithEvents txtHeadline As System.Windows.Forms.TextBox
    Friend WithEvents txtTime As System.Windows.Forms.TextBox
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents cmbCommentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCommentType As System.Windows.Forms.Label
End Class
