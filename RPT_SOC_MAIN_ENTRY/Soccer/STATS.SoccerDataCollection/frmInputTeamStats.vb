﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region
#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL

Imports Microsoft.Win32
Imports System.IO
Imports System.Globalization
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient

#End Region
#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmTeamSetup
' Author        : Shirley Ranjini S
' Created Date  : 11-05-09
' Description   : This form is used for the selection of starting Lineups players for Home and Away Team
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               | Wilson              |12-11-2009         |To Store The Entered Data
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class frmInputTeamStats
#Region " Constants & Variables "
    Private MessageDialog As New frmMessageDialog
    Private teamstats As New DataSet
    Private m_objTeamStats As clsTeamStats = clsTeamStats.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private clog As clsLogin
    Private m_dsGameSetup As New DataSet
    Private m_objGameSetup As New clsGameSetup
    Dim gstag As String
    Dim hometeam As String
    Dim awayteam As String
    Dim periodcount As Int32
    Dim temp As String
    Private m_objUtilAudit As New clsAuditLog
    Private lsupport As New languagesupport
    Dim strmessage As String = "Records Saved"
    Dim strmessage1 As String = "Enter Numeric Values"

#End Region
#Region "Form Load Code to Pass The Values to Grid Cells "
    Private Sub frmInputTeamStats_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmInputTeamStats", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            hometeam = m_objGameDetails.HomeTeamID.ToString
            awayteam = m_objGameDetails.AwayTeamID.ToString

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                lblTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTeam.Text)
                lblteamname.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblteamname.Text)

                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "1st half")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "2nd half")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Extra time 1")
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Extra time 2")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Total")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "1st half")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "2nd half")
                Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Extra time 1")
                Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Extra time 2")
                dgteamstats.Columns(0).HeaderText = g1
                dgteamstats.Columns(1).HeaderText = g2
                dgteamstats.Columns(2).HeaderText = g3
                dgteamstats.Columns(3).HeaderText = g4
                dgteamstats.Columns(4).HeaderText = g5
                dgteamstats.Columns(6).HeaderText = g6
                dgteamstats.Columns(7).HeaderText = g7
                dgteamstats.Columns(8).HeaderText = g8
                dgteamstats.Columns(9).HeaderText = g9
                dgteamstats.Columns(10).HeaderText = g5

            End If

            'Fill The dataset from dbo.LIVE_SOC_TEAM_STATS
            m_dsGameSetup = m_objGameSetup.SelectGameSetup(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID, m_objGameDetails.HomeTeamID, m_objGameDetails.languageid, m_objGameDetails.CoverageLevel, m_objGameDetails.ModuleID)
            m_objGameDetails.GameSetup = m_dsGameSetup
            'function call for Logo
            Dim dsGameStats As New DataSet
            DisplayTeamLogoandTeamAbbrev(dsGameStats)
            dsGameStats = m_objTeamStats.GetTeamStastoselect(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            'count of Period from dbo.LIVE_SOC_PBP
            periodcount = m_objTeamStats.Getperiodcount(m_objGameDetails.GameCode)
            lblTeam.Text = m_objGameDetails.HomeTeam
            lblteamname.Text = m_objGameDetails.AwayTeam
            Me.dgteamstats.Rows.Clear()
            '______Adding 5 rows In grid_______
            If (dgteamstats.Rows.Count <> 5) Then
                dgteamstats.Rows.Add(6)
            End If
            '________________________________________

            'Adding The Values Direct To Each And Every Cells
            '-------------------------------------------------------------------------------------------------------------------------------
            dgteamstats.Rows(0).Cells(5).Value = "Shots off target"
            dgteamstats.Rows(1).Cells(5).Value = "Shots on target"
            dgteamstats.Rows(2).Cells(5).Value = "Woodwork hits"
            dgteamstats.Rows(3).Cells(5).Value = "Off-sides"
            dgteamstats.Rows(4).Cells(5).Value = "Corners"
            dgteamstats.Rows(5).Cells(5).Value = "Fouls committed"
            '___________________________Enabling True And False Of the Cell According To Period __________________________________________________
            For j As Integer = 0 To 5
                If periodcount = 0 Then
                    dgteamstats.Rows(j).Cells(0).ReadOnly = True
                    dgteamstats.Rows(j).Cells(1).ReadOnly = True
                    dgteamstats.Rows(j).Cells(2).ReadOnly = True
                    dgteamstats.Rows(j).Cells(3).ReadOnly = True
                    dgteamstats.Rows(j).Cells(6).ReadOnly = True
                    dgteamstats.Rows(j).Cells(7).ReadOnly = True
                    dgteamstats.Rows(j).Cells(8).ReadOnly = True
                    dgteamstats.Rows(j).Cells(9).ReadOnly = True
                ElseIf periodcount = 1 Then
                    dgteamstats.Rows(j).Cells(0).ReadOnly = False
                    dgteamstats.Rows(j).Cells(1).ReadOnly = True
                    dgteamstats.Rows(j).Cells(2).ReadOnly = True
                    dgteamstats.Rows(j).Cells(3).ReadOnly = True
                    dgteamstats.Rows(j).Cells(6).ReadOnly = False
                    dgteamstats.Rows(j).Cells(7).ReadOnly = True
                    dgteamstats.Rows(j).Cells(8).ReadOnly = True
                    dgteamstats.Rows(j).Cells(9).ReadOnly = True
                ElseIf periodcount = 2 Then
                    dgteamstats.Rows(j).Cells(0).ReadOnly = False
                    dgteamstats.Rows(j).Cells(1).ReadOnly = False
                    dgteamstats.Rows(j).Cells(2).ReadOnly = True
                    dgteamstats.Rows(j).Cells(3).ReadOnly = True
                    dgteamstats.Rows(j).Cells(6).ReadOnly = False
                    dgteamstats.Rows(j).Cells(7).ReadOnly = False
                    dgteamstats.Rows(j).Cells(8).ReadOnly = True
                    dgteamstats.Rows(j).Cells(9).ReadOnly = True
                ElseIf periodcount = 3 Then
                    dgteamstats.Rows(j).Cells(0).ReadOnly = False
                    dgteamstats.Rows(j).Cells(1).ReadOnly = False
                    dgteamstats.Rows(j).Cells(2).ReadOnly = False
                    dgteamstats.Rows(j).Cells(3).ReadOnly = True
                    dgteamstats.Rows(j).Cells(6).ReadOnly = False
                    dgteamstats.Rows(j).Cells(7).ReadOnly = False
                    dgteamstats.Rows(j).Cells(8).ReadOnly = False
                    dgteamstats.Rows(j).Cells(9).ReadOnly = True
                ElseIf periodcount = 4 Then
                    dgteamstats.Rows(j).Cells(0).ReadOnly = False
                    dgteamstats.Rows(j).Cells(1).ReadOnly = False
                    dgteamstats.Rows(j).Cells(2).ReadOnly = False
                    dgteamstats.Rows(j).Cells(3).ReadOnly = False
                    dgteamstats.Rows(j).Cells(6).ReadOnly = False
                    dgteamstats.Rows(j).Cells(7).ReadOnly = False
                    dgteamstats.Rows(j).Cells(8).ReadOnly = False
                    dgteamstats.Rows(j).Cells(9).ReadOnly = False
                End If
            Next
            '_________________________________________________________________________________________________________________________
            Dim st As String = dgteamstats.Rows.Count.ToString
            If dsGameStats.Tables(0).Rows.Count > 0 Then
                For z As Integer = 0 To dsGameStats.Tables(0).Rows.Count - 1
                    'Home Team fill
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-1" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = hometeam) Then
                        dgteamstats.Rows(0).Cells(0).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + "" 'shots off
                        dgteamstats.Rows(1).Cells(0).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + "" 'shots
                        dgteamstats.Rows(2).Cells(0).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(0).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(0).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(0).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-2" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = hometeam) Then
                        dgteamstats.Rows(0).Cells(1).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(1).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(1).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(1).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(1).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(1).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-3" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = hometeam) Then
                        dgteamstats.Rows(0).Cells(2).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(2).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(2).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(2).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(2).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(2).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-4" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = hometeam) Then
                        dgteamstats.Rows(0).Cells(3).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(3).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(3).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(3).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(3).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(3).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    'Away Team fill
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-1" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = awayteam) Then
                        dgteamstats.Rows(0).Cells(6).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(6).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(6).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(6).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(6).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(6).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-2" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = awayteam) Then
                        dgteamstats.Rows(0).Cells(7).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(7).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(7).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(7).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(7).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(7).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-3" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = awayteam) Then
                        dgteamstats.Rows(0).Cells(8).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(8).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(8).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(8).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(8).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(8).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                    If (dsGameStats.Tables(0).Rows(z).ItemArray(3).ToString = "-4" And dsGameStats.Tables(0).Rows(z).ItemArray(2).ToString = awayteam) Then
                        dgteamstats.Rows(0).Cells(9).Value = "" + (CInt(dsGameStats.Tables(0).Rows(z).ItemArray(4)) - CInt(dsGameStats.Tables(0).Rows(z).ItemArray(5))).ToString + ""
                        dgteamstats.Rows(1).Cells(9).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(5).ToString + ""
                        dgteamstats.Rows(2).Cells(9).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(6).ToString + ""
                        dgteamstats.Rows(3).Cells(9).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(7).ToString + ""
                        dgteamstats.Rows(4).Cells(9).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(8).ToString + ""
                        dgteamstats.Rows(5).Cells(9).Value = "" + dsGameStats.Tables(0).Rows(z).ItemArray(9).ToString + ""
                    End If
                Next
            End If
            '--------------------------------------------------------------------------------------------------------------------------------------
            'Call For function Total
            '----------------------
            celltotal()
            '----------------------
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region
#Region "Function for getting  current Team logo and Name"
    Private Sub DisplayTeamLogoandTeamAbbrev(ByVal GameSetup As DataSet)
        Try
            'For Team Logo and Team name
            '--------------------------------------------------------------------------------------------------
            Dim mstrHomeLogo As MemoryStream = Nothing
            Dim mstrAwayLogo As MemoryStream = Nothing
            lblTeam.Text = m_objGameDetails.HomeTeam.ToString
            lblteamname.Text = m_objGameDetails.AwayTeam.ToString
            If m_dsGameSetup.Tables(2).Rows.Count > 0 Then
                mstrHomeLogo = New System.IO.MemoryStream(CType(m_dsGameSetup.Tables(2).Rows(0)(0), Byte()))
                If Not mstrHomeLogo Is Nothing Then
                    picTeamLogo.Image = New Bitmap(mstrHomeLogo)
                    picTeamLogo.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
                Else  'TOSOCRS-338
                    picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
                End If
            End If
            If m_dsGameSetup.Tables(1).Rows.Count > 0 Then
                mstrAwayLogo = New System.IO.MemoryStream(CType(m_dsGameSetup.Tables(1).Rows(0)(0), Byte()))
                If Not mstrAwayLogo Is Nothing Then
                    picawaylogo.Image = New Bitmap(mstrAwayLogo)
                    picawaylogo.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
                Else  'TOSOCRS-338
                    picawaylogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
                End If
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "close the form "
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
        Me.Close()
    End Sub
#End Region
#Region "Function Call for Calculating The Total Values "
    Private Sub dgteamstats_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgteamstats.CellEndEdit
        Try
            celltotal()
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Function for callculating Total"
    Public Sub celltotal() '
        Try
            'Passing the Values for Valicdation To  Allow only Numeric Values
            For k As Integer = 0 To dgteamstats.Rows.Count - 1
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(0).Value), k, 0)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(1).Value), k, 1)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(2).Value), k, 2)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(3).Value), k, 3)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(4).Value), k, 4)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(6).Value), k, 6)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(7).Value), k, 7)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(8).Value), k, 8)
                validatenumaric(CStr(dgteamstats.Rows(k).Cells(9).Value), k, 9)
                'Calculating Total If all The Values Are Numeric
                If (gstag = "yes") Then
                    dgteamstats.Rows(k).Cells(4).Value = CInt(dgteamstats.Rows(k).Cells(0).Value) + CInt(dgteamstats.Rows(k).Cells(1).Value) + CInt(dgteamstats.Rows(k).Cells(2).Value) + CInt(dgteamstats.Rows(k).Cells(3).Value)
                    dgteamstats.Rows(k).Cells(10).Value = CInt(dgteamstats.Rows(k).Cells(6).Value) + CInt(dgteamstats.Rows(k).Cells(7).Value) + CInt(dgteamstats.Rows(k).Cells(8).Value) + CInt(dgteamstats.Rows(k).Cells(9).Value)
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "Function for Validation done for checking Numeric using Ascii  and Passing Previous Value if Non-Numeric"
    Private Sub validatenumaric(ByVal cellvalue As String, ByVal rwcount As Int32, ByVal cellcnt As Int32)
        Try

            If (cellvalue <> Nothing) Then
                Dim Celval As String = cellvalue
                Dim ss As Encoding = Encoding.ASCII
                Dim aa As [Byte]() = ss.GetBytes(Celval)
                For l As Integer = 0 To aa.Length - 1
                    Dim y As Integer = Convert.ToInt32(aa(l).ToString())
                    If (y < 48 OrElse y > 57) Then
                        dgteamstats.Rows(rwcount).Cells(cellcnt).Value = temp
                        MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        gstag = "no"
                        Exit For
                    Else
                        gstag = "yes"
                    End If
                Next
            Else
                dgteamstats.Rows(rwcount).Cells(cellcnt).Value = "0"
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Passing the Value to Insert and Update In table dbo.LIVE_SOC_TEAM_STATS using SP(dbo.USP_addteamstats)"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim totalShotHome As Integer
            Dim totalShotAway As Integer
            totalShotHome = CInt(dgteamstats.Rows(0).Cells(0).Value) + CInt(dgteamstats.Rows(1).Cells(0).Value)
            totalShotAway = CInt(dgteamstats.Rows(0).Cells(6).Value) + CInt(dgteamstats.Rows(1).Cells(6).Value)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, Nothing, 1, 0)
            m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-1"), CInt(dgteamstats.Rows(0).Cells(0).Value) + CInt(dgteamstats.Rows(1).Cells(0).Value), CInt(dgteamstats.Rows(1).Cells(0).Value), CInt(dgteamstats.Rows(2).Cells(0).Value), CInt(dgteamstats.Rows(3).Cells(0).Value), CInt(dgteamstats.Rows(4).Cells(0).Value), CInt(dgteamstats.Rows(5).Cells(0).Value))
            m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-1"), CInt(dgteamstats.Rows(0).Cells(6).Value) + CInt(dgteamstats.Rows(1).Cells(6).Value), CInt(dgteamstats.Rows(1).Cells(6).Value), CInt(dgteamstats.Rows(2).Cells(6).Value), CInt(dgteamstats.Rows(3).Cells(6).Value), CInt(dgteamstats.Rows(4).Cells(6).Value), CInt(dgteamstats.Rows(5).Cells(6).Value))
            If (periodcount >= 2) Then
                m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-2"), CInt(dgteamstats.Rows(0).Cells(1).Value) + CInt(dgteamstats.Rows(1).Cells(1).Value), CInt(dgteamstats.Rows(1).Cells(1).Value), CInt(dgteamstats.Rows(2).Cells(1).Value), CInt(dgteamstats.Rows(3).Cells(1).Value), CInt(dgteamstats.Rows(4).Cells(1).Value), CInt(dgteamstats.Rows(5).Cells(1).Value))
                m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-2"), CInt(dgteamstats.Rows(0).Cells(7).Value) + CInt(dgteamstats.Rows(1).Cells(7).Value), CInt(dgteamstats.Rows(1).Cells(7).Value), CInt(dgteamstats.Rows(2).Cells(7).Value), CInt(dgteamstats.Rows(3).Cells(7).Value), CInt(dgteamstats.Rows(4).Cells(7).Value), CInt(dgteamstats.Rows(5).Cells(7).Value))
            End If
            If (periodcount >= 3) Then
                m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-3"), CInt(dgteamstats.Rows(0).Cells(2).Value) + CInt(dgteamstats.Rows(1).Cells(2).Value), CInt(dgteamstats.Rows(1).Cells(2).Value), CInt(dgteamstats.Rows(2).Cells(2).Value), CInt(dgteamstats.Rows(3).Cells(2).Value), CInt(dgteamstats.Rows(4).Cells(2).Value), CInt(dgteamstats.Rows(5).Cells(2).Value))
                m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-3"), CInt(dgteamstats.Rows(0).Cells(8).Value) + CInt(dgteamstats.Rows(1).Cells(8).Value), CInt(dgteamstats.Rows(1).Cells(8).Value), CInt(dgteamstats.Rows(2).Cells(8).Value), CInt(dgteamstats.Rows(3).Cells(8).Value), CInt(dgteamstats.Rows(4).Cells(8).Value), CInt(dgteamstats.Rows(5).Cells(8).Value))
            End If
            If (periodcount = 4) Then
                m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-4"), CInt(dgteamstats.Rows(0).Cells(3).Value) + CInt(dgteamstats.Rows(1).Cells(3).Value), CInt(dgteamstats.Rows(1).Cells(3).Value), CInt(dgteamstats.Rows(2).Cells(3).Value), CInt(dgteamstats.Rows(3).Cells(3).Value), CInt(dgteamstats.Rows(4).Cells(3).Value), CInt(dgteamstats.Rows(5).Cells(3).Value))
                m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-4"), CInt(dgteamstats.Rows(0).Cells(9).Value) + CInt(dgteamstats.Rows(1).Cells(9).Value), CInt(dgteamstats.Rows(1).Cells(9).Value), CInt(dgteamstats.Rows(2).Cells(9).Value), CInt(dgteamstats.Rows(3).Cells(9).Value), CInt(dgteamstats.Rows(4).Cells(9).Value), CInt(dgteamstats.Rows(5).Cells(9).Value))
            End If
            MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "Getting The Current Cell Value and storing it in a Variable"
    Private Sub dgteamstats_CellEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgteamstats.CellEnter
        Try
            temp = dgteamstats.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString
        Catch ex As Exception
        End Try
    End Sub
#End Region
End Class
#Region "code commented  not needed"
'If (CInt(dgteamstats.Rows(1).Cells(1).Value.ToString) > 0 Or CInt(dgteamstats.Rows(1).Cells(1).Value) > 0 Or CInt(dgteamstats.Rows(2).Cells(1).Value) > 0 Or CInt(dgteamstats.Rows(3).Cells(1).Value) > 0 Or CInt(dgteamstats.Rows(4).Cells(1).Value) > 0 Or CInt(dgteamstats.Rows(5).Cells(1).Value) > 0) Then
'End If
'If (CInt(dgteamstats.Rows(2).Cells(2).Value) > 0 Or CInt(dgteamstats.Rows(1).Cells(2).Value) > 0 Or CInt(dgteamstats.Rows(2).Cells(1).Value) > 0 Or CInt(dgteamstats.Rows(3).Cells(2).Value) > 0 Or CInt(dgteamstats.Rows(4).Cells(2).Value) > 0 Or CInt(dgteamstats.Rows(5).Cells(2).Value) > 0) Then
'End If
'If (CInt(dgteamstats.Rows(0).Cells(3).Value) > 0 Or CInt(dgteamstats.Rows(1).Cells(3).Value) > 0 Or CInt(dgteamstats.Rows(2).Cells(3).Value) > 0 Or CInt(dgteamstats.Rows(3).Cells(3).Value) > 0 Or CInt(dgteamstats.Rows(4).Cells(3).Value) > 0 Or CInt(dgteamstats.Rows(5).Cells(3).Value) > 0) Then
'End If
'If (CInt(dgteamstats.Rows(0).Cells(4).Value) <> "0" And CStr(dgteamstats.Rows(1).Cells(4).Value) <> "0" And CStr(dgteamstats.Rows(2).Cells(4).Value) <> "0" And CStr(dgteamstats.Rows(3).Cells(4).Value) <> "0" And CStr(dgteamstats.Rows(4).Cells(4).Value) <> "0" And CStr(dgteamstats.Rows(5).Cells(4).Value) <> Nothing) Then
'    m_objTeamStats.InsertTeamStas(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("3"), CInt(dgteamstats.Rows(0).Cells(4).Value), CInt(dgteamstats.Rows(1).Cells(4).Value), CInt(dgteamstats.Rows(2).Cells(4).Value), CInt(dgteamstats.Rows(3).Cells(4).Value), CInt(dgteamstats.Rows(4).Cells(4).Value), CInt(dgteamstats.Rows(5).Cells(4).Value))
'End If
#End Region