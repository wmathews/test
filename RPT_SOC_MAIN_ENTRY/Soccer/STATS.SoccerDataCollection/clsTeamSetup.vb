﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsTeamSetup
' Author        : Shirley Ranjini
' Created Date  : 12 May 2009
' Description   : This class contains information on Starting Lineup And Bench Players.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsTeamSetup

#Region " Constants & Variables "
    Shared m_myInstance As clsTeamSetup
    Private m_dsLineups As New DataSet
    Private m_clrHomeShirt As Color
    Private m_clrAwayShirt As Color
    Private m_clrHomeShort As Color
    Private m_clrAwayShort As Color
    Private m_clrHomeShirtNo As Color
    Private m_clrAwayShirtNo As Color

#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsTeamSetup
        If m_myInstance Is Nothing Then
            m_myInstance = New clsTeamSetup
        End If
        Return m_myInstance
    End Function
#End Region

#Region " Public properties "
    Public Property RosterInfo() As DataSet
        Get
            Return m_dsLineups
        End Get
        Set(ByVal value As DataSet)
            m_dsLineups = value
        End Set
    End Property

    Public Property HomeShirtColor() As Color
        Get
            Return m_clrHomeShirt
        End Get
        Set(ByVal value As Color)
            m_clrHomeShirt = value
        End Set
    End Property

    Public Property AwayShirtColor() As Color
        Get
            Return m_clrAwayShirt
        End Get
        Set(ByVal value As Color)
            m_clrAwayShirt = value
        End Set
    End Property
    Public Property HomeShirtColorNo() As Color
        Get
            Return m_clrHomeShirtNo
        End Get
        Set(ByVal value As Color)
            m_clrHomeShirtNo = value
        End Set
    End Property

    Public Property AwayShirtColorNo() As Color
        Get
            Return m_clrAwayShirtNo
        End Get
        Set(ByVal value As Color)
            m_clrAwayShirtNo = value
        End Set
    End Property

    Public Property HomeShortColor() As Color
        Get
            Return m_clrHomeShort
        End Get
        Set(ByVal value As Color)
            m_clrHomeShort = value
        End Set
    End Property

    Public Property AwayShortColor() As Color
        Get
            Return m_clrAwayShort
        End Get
        Set(ByVal value As Color)
            m_clrAwayShort = value
        End Set
    End Property

    'Public Property AwayKit() As Integer
    '    Get
    '        Return m_intAwatKit
    '    End Get
    '    Set(ByVal value As intege)
    '        m_intAwatKit = value
    '    End Set
    'End Property

    'Public Property HomeKit() As Integer
    '    Get
    '        Return m_intAwatKit
    '    End Get
    '    Set(ByVal value As intege)
    '        m_intAwatKit = value
    '    End Set
    'End Property

#End Region

End Class