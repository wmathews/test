﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddPlayer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picTeamLogo = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.radAwayTeam = New System.Windows.Forms.RadioButton()
        Me.lblTeamName = New System.Windows.Forms.Label()
        Me.radHomeTeam = New System.Windows.Forms.RadioButton()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.cmbPosition = New System.Windows.Forms.ComboBox()
        Me.txtlastname = New System.Windows.Forms.TextBox()
        Me.txtUniformnumber = New System.Windows.Forms.TextBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.txtFirstname = New System.Windows.Forms.TextBox()
        Me.lblPosition = New System.Windows.Forms.Label()
        Me.lblUniformNumber = New System.Windows.Forms.Label()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.btnIgnore = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.lvwPlayer = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.sstAddGame = New System.Windows.Forms.StatusStrip()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'picTeamLogo
        '
        Me.picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picTeamLogo.Location = New System.Drawing.Point(14, 47)
        Me.picTeamLogo.Name = "picTeamLogo"
        Me.picTeamLogo.Size = New System.Drawing.Size(46, 45)
        Me.picTeamLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picTeamLogo.TabIndex = 315
        Me.picTeamLogo.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Panel2.Controls.Add(Me.radAwayTeam)
        Me.Panel2.Controls.Add(Me.lblTeamName)
        Me.Panel2.Controls.Add(Me.radHomeTeam)
        Me.Panel2.Location = New System.Drawing.Point(73, 39)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(496, 53)
        Me.Panel2.TabIndex = 314
        '
        'radAwayTeam
        '
        Me.radAwayTeam.ForeColor = System.Drawing.Color.White
        Me.radAwayTeam.Location = New System.Drawing.Point(314, 28)
        Me.radAwayTeam.Name = "radAwayTeam"
        Me.radAwayTeam.Size = New System.Drawing.Size(107, 19)
        Me.radAwayTeam.TabIndex = 341
        Me.radAwayTeam.TabStop = True
        Me.radAwayTeam.Text = "Away team name"
        Me.radAwayTeam.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.radAwayTeam.UseVisualStyleBackColor = True
        '
        'lblTeamName
        '
        Me.lblTeamName.AutoSize = True
        Me.lblTeamName.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeamName.ForeColor = System.Drawing.Color.White
        Me.lblTeamName.Location = New System.Drawing.Point(17, 10)
        Me.lblTeamName.Name = "lblTeamName"
        Me.lblTeamName.Size = New System.Drawing.Size(115, 25)
        Me.lblTeamName.TabIndex = 14
        Me.lblTeamName.Text = "Team name"
        '
        'radHomeTeam
        '
        Me.radHomeTeam.ForeColor = System.Drawing.Color.White
        Me.radHomeTeam.Location = New System.Drawing.Point(314, 8)
        Me.radHomeTeam.Name = "radHomeTeam"
        Me.radHomeTeam.Size = New System.Drawing.Size(108, 19)
        Me.radHomeTeam.TabIndex = 340
        Me.radHomeTeam.TabStop = True
        Me.radHomeTeam.Text = "Home team name"
        Me.radHomeTeam.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.radHomeTeam.UseVisualStyleBackColor = True
        '
        'PictureBox6
        '
        Me.PictureBox6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox6.Location = New System.Drawing.Point(0, 15)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(569, 24)
        Me.PictureBox6.TabIndex = 313
        Me.PictureBox6.TabStop = False
        '
        'cmbPosition
        '
        Me.cmbPosition.BackColor = System.Drawing.Color.White
        Me.cmbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPosition.FormattingEnabled = True
        Me.cmbPosition.Location = New System.Drawing.Point(372, 132)
        Me.cmbPosition.Name = "cmbPosition"
        Me.cmbPosition.Size = New System.Drawing.Size(134, 23)
        Me.cmbPosition.TabIndex = 330
        '
        'txtlastname
        '
        Me.txtlastname.Location = New System.Drawing.Point(78, 133)
        Me.txtlastname.Name = "txtlastname"
        Me.txtlastname.Size = New System.Drawing.Size(163, 22)
        Me.txtlastname.TabIndex = 325
        '
        'txtUniformnumber
        '
        Me.txtUniformnumber.Location = New System.Drawing.Point(372, 106)
        Me.txtUniformnumber.MaxLength = 3
        Me.txtUniformnumber.Name = "txtUniformnumber"
        Me.txtUniformnumber.Size = New System.Drawing.Size(134, 22)
        Me.txtUniformnumber.TabIndex = 329
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnAdd.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(106, 169)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(57, 25)
        Me.btnAdd.TabIndex = 320
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(169, 169)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(57, 25)
        Me.btnEdit.TabIndex = 322
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(232, 169)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(57, 25)
        Me.btnDelete.TabIndex = 323
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'txtFirstname
        '
        Me.txtFirstname.Location = New System.Drawing.Point(78, 106)
        Me.txtFirstname.Name = "txtFirstname"
        Me.txtFirstname.Size = New System.Drawing.Size(163, 22)
        Me.txtFirstname.TabIndex = 321
        '
        'lblPosition
        '
        Me.lblPosition.AutoSize = True
        Me.lblPosition.BackColor = System.Drawing.Color.Transparent
        Me.lblPosition.Location = New System.Drawing.Point(264, 133)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(48, 15)
        Me.lblPosition.TabIndex = 336
        Me.lblPosition.Text = "Position:"
        '
        'lblUniformNumber
        '
        Me.lblUniformNumber.AutoSize = True
        Me.lblUniformNumber.BackColor = System.Drawing.Color.Transparent
        Me.lblUniformNumber.Location = New System.Drawing.Point(264, 107)
        Me.lblUniformNumber.Name = "lblUniformNumber"
        Me.lblUniformNumber.Size = New System.Drawing.Size(86, 15)
        Me.lblUniformNumber.TabIndex = 335
        Me.lblUniformNumber.Text = "Uniform number:"
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.BackColor = System.Drawing.Color.Transparent
        Me.lblLastName.Location = New System.Drawing.Point(13, 136)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(60, 15)
        Me.lblLastName.TabIndex = 333
        Me.lblLastName.Text = "Last name:"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.BackColor = System.Drawing.Color.Transparent
        Me.lblFirstName.Location = New System.Drawing.Point(14, 111)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(61, 15)
        Me.lblFirstName.TabIndex = 332
        Me.lblFirstName.Text = "First name:"
        '
        'btnApply
        '
        Me.btnApply.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnApply.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnApply.ForeColor = System.Drawing.Color.White
        Me.btnApply.Location = New System.Drawing.Point(295, 169)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(57, 25)
        Me.btnApply.TabIndex = 337
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = False
        '
        'btnIgnore
        '
        Me.btnIgnore.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnIgnore.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnIgnore.ForeColor = System.Drawing.Color.White
        Me.btnIgnore.Location = New System.Drawing.Point(358, 169)
        Me.btnIgnore.Name = "btnIgnore"
        Me.btnIgnore.Size = New System.Drawing.Size(57, 25)
        Me.btnIgnore.TabIndex = 338
        Me.btnIgnore.Text = "Ignore"
        Me.btnIgnore.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(433, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 339
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.PictureBox7.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(569, 17)
        Me.PictureBox7.TabIndex = 312
        Me.PictureBox7.TabStop = False
        '
        'lvwPlayer
        '
        Me.lvwPlayer.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvwPlayer.AutoArrange = False
        Me.lvwPlayer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwPlayer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvwPlayer.FullRowSelect = True
        Me.lvwPlayer.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwPlayer.HideSelection = False
        Me.lvwPlayer.Location = New System.Drawing.Point(12, 206)
        Me.lvwPlayer.MultiSelect = False
        Me.lvwPlayer.Name = "lvwPlayer"
        Me.lvwPlayer.ShowItemToolTips = True
        Me.lvwPlayer.Size = New System.Drawing.Size(496, 206)
        Me.lvwPlayer.TabIndex = 340
        Me.lvwPlayer.UseCompatibleStateImageBehavior = False
        Me.lvwPlayer.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Player"
        Me.ColumnHeader1.Width = 172
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Team"
        Me.ColumnHeader2.Width = 129
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Uniform"
        Me.ColumnHeader3.Width = 114
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Position"
        Me.ColumnHeader4.Width = 147
        '
        'sstAddGame
        '
        Me.sstAddGame.AutoSize = False
        Me.sstAddGame.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstAddGame.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstAddGame.Location = New System.Drawing.Point(0, 470)
        Me.sstAddGame.Name = "sstAddGame"
        Me.sstAddGame.Size = New System.Drawing.Size(520, 16)
        Me.sstAddGame.TabIndex = 341
        Me.sstAddGame.Text = "StatusStrip1"
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 422)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(525, 52)
        Me.picButtonBar.TabIndex = 342
        '
        'frmAddPlayer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(520, 486)
        Me.Controls.Add(Me.sstAddGame)
        Me.Controls.Add(Me.lvwPlayer)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.btnIgnore)
        Me.Controls.Add(Me.lblPosition)
        Me.Controls.Add(Me.lblUniformNumber)
        Me.Controls.Add(Me.lblLastName)
        Me.Controls.Add(Me.lblFirstName)
        Me.Controls.Add(Me.txtFirstname)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.txtUniformnumber)
        Me.Controls.Add(Me.txtlastname)
        Me.Controls.Add(Me.cmbPosition)
        Me.Controls.Add(Me.picTeamLogo)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "frmAddPlayer"
        Me.Text = "Add New Player"
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picTeamLogo As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblTeamName As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbPosition As System.Windows.Forms.ComboBox
    Friend WithEvents txtlastname As System.Windows.Forms.TextBox
    Friend WithEvents txtUniformnumber As System.Windows.Forms.TextBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents txtFirstname As System.Windows.Forms.TextBox
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents lblUniformNumber As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnIgnore As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents radAwayTeam As System.Windows.Forms.RadioButton
    Friend WithEvents radHomeTeam As System.Windows.Forms.RadioButton
    Friend WithEvents lvwPlayer As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents sstAddGame As System.Windows.Forms.StatusStrip
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
