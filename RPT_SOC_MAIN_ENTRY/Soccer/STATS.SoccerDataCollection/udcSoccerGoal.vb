﻿
#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : udcSoccerGoal
' Author        : Fiaz Ahmed
' Created Date  : 30th June 2009
' Description   : This user control represents a soccer goal.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class udcSoccerGoal

#Region " Constants & Variables "
    Private Const GOAL_WIDTH As Integer = 189
    Private Const GOAL_HEIGHT As Integer = 64
    Private Const POST_THICKNESS As Integer = 3
    Private Const GOAL_SCALE As Integer = 4

    Public Event USGClick(ByVal sender As Object, ByVal e As USGEventArgs)
    Public Event USGMarkRemoved(ByVal sender As Object, ByVal e As USGEventArgs)
    Public Event USGMouseMove(ByVal sender As Object, ByVal e As USGEventArgs)
    Public Event USGMouseLeave()

    Private m_intNextMark As Integer = 1
    Private m_intMarkCount As Integer = 0
    Private m_intMaximumMarksAllowed As Integer = 1

    Private m_dtGoalGrid As DataTable

    Private m_rctSelectedZone As Rectangle
    Private zonecheck As String
    Private m_objUtility As New STATS.Utility.clsUtility
#End Region

#Region " Event handlers "

    Private Sub picNet_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picNet.MouseMove
        Try
            Dim evargMouse As MouseEventArgs
            Dim strZone As String

            evargMouse = CType(e, MouseEventArgs)

            strZone = ""
            strZone = GetZone(evargMouse.X, evargMouse.Y)       'get the zone corresponding to mouse x and y

            DrawRectangle(strZone)          'draw zone hover rectangle

            Dim MyEventArgs As USGEventArgs

            MyEventArgs = New USGEventArgs(strZone)

            'raise event with zone details
            RaiseEvent USGMouseMove(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picNet_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles picNet.MouseLeave
        Try
            picNet.Refresh()        'clear the zone hover rectangle when the mouse moves out of the picture box
            RaiseEvent USGMouseLeave()      'raise event when mouse moves out of the picture box
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picSoccerGoal_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles picSoccerGoal.Paint
        Try
            DrawGoal(e)         'draw the goal
            DefineZone()        'define the 9 zones
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub udcSoccerGoal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'clear the zone selection rectangle
            m_rctSelectedZone.X = 0
            m_rctSelectedZone.Y = 0
            m_rctSelectedZone.Height = 0
            m_rctSelectedZone.Width = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picNet_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles picNet.Paint
        Try
            'draw the zone selection rectangle
            e.Graphics.DrawRectangle(Pens.OrangeRed, m_rctSelectedZone)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picNet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picNet.Click
        Try
            Dim evargMouse As MouseEventArgs
            Dim strZone As String
            Dim intX As Integer
            Dim intY As Integer
            Dim intWidth As Integer
            Dim intHeight As Integer

            evargMouse = CType(e, MouseEventArgs)

            'proceed only if left mouse button is clicked
            If evargMouse.Button <> Windows.Forms.MouseButtons.Left Then
                Exit Try
            End If

            strZone = ""
            strZone = GetZone(evargMouse.X, evargMouse.Y)       'get the zone corresponding to mouse x and y

            'get the rectangle co-ordinates corresponding to zone
            For Each dr As DataRow In m_dtGoalGrid.Rows
                Select Case strZone
                    Case CStr(dr("LeftValue"))
                        intX = CInt(dr("LeftMinX"))
                        intY = CInt(dr("LeftMinY"))
                        intWidth = CInt(dr("LeftMaxX")) - CInt(dr("LeftMinX"))
                        intHeight = CInt(dr("LeftMaxY")) - CInt(dr("LeftMinY"))
                    Case CStr(dr("CentreValue"))
                        intX = CInt(dr("CentreMinX"))
                        intY = CInt(dr("CentreMinY"))
                        intWidth = CInt(dr("CentreMaxX")) - CInt(dr("CentreMinX"))
                        intHeight = CInt(dr("CentreMaxY")) - CInt(dr("CentreMinY"))
                    Case CStr(dr("RightValue"))
                        intX = CInt(dr("RightMinX"))
                        intY = CInt(dr("RightMinY"))
                        intWidth = CInt(dr("RightMaxX")) - CInt(dr("RightMinX"))
                        intHeight = CInt(dr("RightMaxY")) - CInt(dr("RightMinY"))
                End Select
            Next

            Dim MyEventArgs As USGEventArgs

            'clear the zone selection rectangle if an existing selection is clicked
            If intX = m_rctSelectedZone.X And intY = m_rctSelectedZone.Y And intWidth = m_rctSelectedZone.Width And intHeight = m_rctSelectedZone.Height Then
                m_rctSelectedZone.X = 0
                m_rctSelectedZone.Y = 0
                m_rctSelectedZone.Width = 0
                m_rctSelectedZone.Height = 0

                MyEventArgs = New USGEventArgs("")
            Else        'draw the zone selection rectangle
                m_rctSelectedZone.X = intX
                m_rctSelectedZone.Y = intY
                m_rctSelectedZone.Width = intWidth
                m_rctSelectedZone.Height = intHeight

                MyEventArgs = New USGEventArgs(strZone)
            End If

            picNet.Refresh()        'refresh the picturebox to clear previous selection and display current selection
            RaiseEvent USGClick(sender, MyEventArgs)    'raise event with zone details
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Private methods "
    Private Sub DefineZone()
        Try
            Dim drGoalGrid As DataRow
            m_dtGoalGrid = New DataTable

            'Create a datatable representing the 3 X 3 Grid
            m_dtGoalGrid.Columns.Add("LeftMinX")
            m_dtGoalGrid.Columns.Add("LeftMinY")
            m_dtGoalGrid.Columns.Add("LeftMaxX")
            m_dtGoalGrid.Columns.Add("LeftMaxY")
            m_dtGoalGrid.Columns.Add("LeftValue")
            m_dtGoalGrid.Columns.Add("CentreMinX")
            m_dtGoalGrid.Columns.Add("CentreMinY")
            m_dtGoalGrid.Columns.Add("CentreMaxX")
            m_dtGoalGrid.Columns.Add("CentreMaxY")
            m_dtGoalGrid.Columns.Add("CentreValue")
            m_dtGoalGrid.Columns.Add("RightMinX")
            m_dtGoalGrid.Columns.Add("RightMinY")
            m_dtGoalGrid.Columns.Add("RightMaxX")
            m_dtGoalGrid.Columns.Add("RightMaxY")
            m_dtGoalGrid.Columns.Add("RightValue")

            If zonecheck = "Kepper Zone" Then
                drGoalGrid = m_dtGoalGrid.NewRow
                drGoalGrid("LeftMinX") = 0
                drGoalGrid("LeftMinY") = 0
                drGoalGrid("LeftMaxX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("LeftMaxY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                'drGoalGrid("LeftValue") = "Top Left"
                drGoalGrid("LeftValue") = "1"

                drGoalGrid("CentreMinX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("CentreMinY") = 0
                drGoalGrid("CentreMaxX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("CentreMaxY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                'drGoalGrid("CentreValue") = "Top Center"
                drGoalGrid("CentreValue") = "2"
                drGoalGrid("RightMinX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("RightMinY") = 0
                drGoalGrid("RightMaxX") = GOAL_WIDTH - (POST_THICKNESS * 2)
                drGoalGrid("RightMaxY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                'drGoalGrid("RightValue") = "Top Right"
                drGoalGrid("RightValue") = "3"
                m_dtGoalGrid.Rows.Add(drGoalGrid)

                drGoalGrid = m_dtGoalGrid.NewRow
                drGoalGrid("LeftMinX") = 0
                drGoalGrid("LeftMinY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                drGoalGrid("LeftMaxX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("LeftMaxY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                'drGoalGrid("LeftValue") = "Middle Left"
                drGoalGrid("LeftValue") = "8"
                drGoalGrid("CentreMinX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("CentreMinY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                drGoalGrid("CentreMaxX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("CentreMaxY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                'drGoalGrid("CentreValue") = "Middle Center"
                drGoalGrid("CentreValue") = "9"
                drGoalGrid("RightMinX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("RightMinY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                drGoalGrid("RightMaxX") = GOAL_WIDTH - (POST_THICKNESS * 2)
                drGoalGrid("RightMaxY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                'drGoalGrid("RightValue") = "Middle Right"
                drGoalGrid("RightValue") = "10"

                m_dtGoalGrid.Rows.Add(drGoalGrid)
                drGoalGrid = m_dtGoalGrid.NewRow
                drGoalGrid("LeftMinX") = 0
                drGoalGrid("LeftMinY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("LeftMaxX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("LeftMaxY") = GOAL_HEIGHT - POST_THICKNESS - 2
                ' drGoalGrid("LeftValue") = "Bottom Left"
                drGoalGrid("LeftValue") = "6"
                drGoalGrid("CentreMinX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("CentreMinY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("CentreMaxX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("CentreMaxY") = GOAL_HEIGHT - POST_THICKNESS - 2
                'drGoalGrid("CentreValue") = "Bottom Center"
                drGoalGrid("CentreValue") = "5"
                drGoalGrid("RightMinX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("RightMinY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("RightMaxX") = GOAL_WIDTH - (POST_THICKNESS * 2)
                drGoalGrid("RightMaxY") = GOAL_HEIGHT - POST_THICKNESS - 2
                'drGoalGrid("RightValue") = "Bottom Right"
                drGoalGrid("RightValue") = "4"
                m_dtGoalGrid.Rows.Add(drGoalGrid)
            Else
                drGoalGrid = m_dtGoalGrid.NewRow
                drGoalGrid("LeftMinX") = 0
                drGoalGrid("LeftMinY") = 0
                drGoalGrid("LeftMaxX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("LeftMaxY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                'drGoalGrid("LeftValue") = "Top Left"
                drGoalGrid("LeftValue") = "1"

                drGoalGrid("CentreMinX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("CentreMinY") = 0
                drGoalGrid("CentreMaxX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("CentreMaxY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                'drGoalGrid("CentreValue") = "Top Center"
                drGoalGrid("CentreValue") = "2"
                drGoalGrid("RightMinX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("RightMinY") = 0
                drGoalGrid("RightMaxX") = GOAL_WIDTH - (POST_THICKNESS * 2)
                drGoalGrid("RightMaxY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                'drGoalGrid("RightValue") = "Top Right"
                drGoalGrid("RightValue") = "3"
                m_dtGoalGrid.Rows.Add(drGoalGrid)

                drGoalGrid = m_dtGoalGrid.NewRow
                drGoalGrid("LeftMinX") = 0
                drGoalGrid("LeftMinY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                drGoalGrid("LeftMaxX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("LeftMaxY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                'drGoalGrid("LeftValue") = "Middle Left"
                drGoalGrid("LeftValue") = "7"
                drGoalGrid("CentreMinX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("CentreMinY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                drGoalGrid("CentreMaxX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("CentreMaxY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("CentreValue") = "8"
                ' drGoalGrid("CentreValue") = "Middle Center"
                drGoalGrid("RightMinX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("RightMinY") = (GOAL_HEIGHT - POST_THICKNESS) / 3
                drGoalGrid("RightMaxX") = GOAL_WIDTH - (POST_THICKNESS * 2)
                drGoalGrid("RightMaxY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                'drGoalGrid("RightValue") = "Middle Right"
                drGoalGrid("RightValue") = "9"

                m_dtGoalGrid.Rows.Add(drGoalGrid)
                drGoalGrid = m_dtGoalGrid.NewRow
                drGoalGrid("LeftMinX") = 0
                drGoalGrid("LeftMinY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("LeftMaxX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("LeftMaxY") = GOAL_HEIGHT - POST_THICKNESS - 2
                'drGoalGrid("LeftValue") = "Bottom Left"
                drGoalGrid("LeftValue") = "6"
                drGoalGrid("CentreMinX") = (GOAL_WIDTH - (POST_THICKNESS * 2)) / 3
                drGoalGrid("CentreMinY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("CentreMaxX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("CentreMaxY") = GOAL_HEIGHT - POST_THICKNESS - 2
                'drGoalGrid("CentreValue") = "Bottom Center"
                drGoalGrid("CentreValue") = "5"
                drGoalGrid("RightMinX") = ((GOAL_WIDTH - (POST_THICKNESS * 2)) / 3) * 2
                drGoalGrid("RightMinY") = ((GOAL_HEIGHT - POST_THICKNESS) / 3) * 2
                drGoalGrid("RightMaxX") = GOAL_WIDTH - (POST_THICKNESS * 2)
                drGoalGrid("RightMaxY") = GOAL_HEIGHT - POST_THICKNESS - 2
                'drGoalGrid("RightValue") = "Bottom Right"
                drGoalGrid("RightValue") = "4"
                m_dtGoalGrid.Rows.Add(drGoalGrid)

            End If

            

            
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DrawGoal(ByVal e As System.Windows.Forms.PaintEventArgs)
        Try
            Dim rctGoal As Rectangle
            Dim rctGoalMouth As Rectangle
            Dim pnDrawing As Pen
            Dim clrPenColor As Color
            Dim intPenWidth As Integer = 1

            clrPenColor = Color.Black
            pnDrawing = New Pen(Color:=clrPenColor, Width:=intPenWidth)

            'draw goal
            rctGoal = New Rectangle(x:=0, y:=0, Width:=GOAL_WIDTH, Height:=GOAL_HEIGHT)
            rctGoalMouth = New Rectangle(x:=POST_THICKNESS, y:=POST_THICKNESS, Width:=GOAL_WIDTH - (POST_THICKNESS * 2), Height:=GOAL_HEIGHT)

            e.Graphics.FillRectangle(Brushes.White, rctGoal)
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctGoal)

            e.Graphics.FillRectangle(Brushes.Lavender, rctGoalMouth)
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=rctGoalMouth)

            picNet.Top = rctGoalMouth.Y + 1
            picNet.Left = rctGoalMouth.X + 1
            picNet.Width = rctGoalMouth.Width - 1
            picNet.Height = rctGoalMouth.Height - 1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetZone(ByVal X As Integer, ByVal Y As Integer) As String
        Try
            For i As Integer = 0 To 2
                If X <= CType(m_dtGoalGrid.Rows(i).Item("LeftMaxX"), Double) And Y <= CType(m_dtGoalGrid.Rows(i).Item("LeftMaxY"), Double) Then
                    Return m_dtGoalGrid.Rows(i).Item("LeftValue").ToString
                End If

                If X <= CType(m_dtGoalGrid.Rows(i).Item("CentreMaxX"), Double) And Y <= CType(m_dtGoalGrid.Rows(i).Item("CentreMaxY"), Double) Then
                    Return m_dtGoalGrid.Rows(i).Item("CentreValue").ToString
                End If

                If X <= CType(m_dtGoalGrid.Rows(i).Item("RightMaxX"), Double) And Y <= CType(m_dtGoalGrid.Rows(i).Item("RightMaxY"), Double) Then
                    Return m_dtGoalGrid.Rows(i).Item("RightValue").ToString
                End If
            Next

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub DrawRectangle(ByVal Zone As String)
        Try
            Dim rctZone As Rectangle
            Dim g As Graphics = picNet.CreateGraphics

            For Each dr As DataRow In m_dtGoalGrid.Rows
                Select Case Zone
                    Case CStr(dr("LeftValue"))
                        rctZone.X = CInt(dr("LeftMinX"))
                        rctZone.Y = CInt(dr("LeftMinY"))
                        rctZone.Width = CInt(dr("LeftMaxX")) - CInt(dr("LeftMinX"))
                        rctZone.Height = CInt(dr("LeftMaxY")) - CInt(dr("LeftMinY"))

                        picNet.Refresh()
                        g.DrawRectangle(Pens.Blue, rctZone)
                    Case CStr(dr("CentreValue"))
                        rctZone.X = CInt(dr("CentreMinX"))
                        rctZone.Y = CInt(dr("CentreMinY"))
                        rctZone.Width = CInt(dr("CentreMaxX")) - CInt(dr("CentreMinX"))
                        rctZone.Height = CInt(dr("CentreMaxY")) - CInt(dr("CentreMinY"))

                        picNet.Refresh()
                        g.DrawRectangle(Pens.Blue, rctZone)
                    Case CStr(dr("RightValue"))
                        rctZone.X = CInt(dr("RightMinX"))
                        rctZone.Y = CInt(dr("RightMinY"))
                        rctZone.Width = CInt(dr("RightMaxX")) - CInt(dr("RightMinX"))
                        rctZone.Height = CInt(dr("RightMaxY")) - CInt(dr("RightMinY"))

                        picNet.Refresh()
                        g.DrawRectangle(Pens.Blue, rctZone)
                End Select
            Next


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Public Methods "
    ''' <summary>
    ''' Draws the zone selection rectangle
    ''' </summary>
    ''' <param name="Zone">Any of the nine goal zones as string</param>
    ''' <remarks>Use this method to set zone at run time in edit mode</remarks>
    Public Sub USGSetZone(ByVal Zone As String, ByVal checkzone As String)
        Try
            Dim intX As Integer
            Dim intY As Integer
            Dim intWidth As Integer
            Dim intHeight As Integer
            zonecheck = checkzone
            For Each dr As DataRow In m_dtGoalGrid.Rows
                Select Case Zone
                    Case CStr(dr("LeftValue"))
                        intX = CInt(dr("LeftMinX"))
                        intY = CInt(dr("LeftMinY"))
                        intWidth = CInt(dr("LeftMaxX")) - CInt(dr("LeftMinX"))
                        intHeight = CInt(dr("LeftMaxY")) - CInt(dr("LeftMinY"))
                    Case CStr(dr("CentreValue"))
                        intX = CInt(dr("CentreMinX"))
                        intY = CInt(dr("CentreMinY"))
                        intWidth = CInt(dr("CentreMaxX")) - CInt(dr("CentreMinX"))
                        intHeight = CInt(dr("CentreMaxY")) - CInt(dr("CentreMinY"))
                    Case CStr(dr("RightValue"))
                        intX = CInt(dr("RightMinX"))
                        intY = CInt(dr("RightMinY"))
                        intWidth = CInt(dr("RightMaxX")) - CInt(dr("RightMinX"))
                        intHeight = CInt(dr("RightMaxY")) - CInt(dr("RightMinY"))
                End Select
            Next

            Dim MyEventArgs As USGEventArgs
            If intX = m_rctSelectedZone.X And intY = m_rctSelectedZone.Y And intWidth = m_rctSelectedZone.Width And intHeight = m_rctSelectedZone.Height Then
                m_rctSelectedZone.X = 0
                m_rctSelectedZone.Y = 0
                m_rctSelectedZone.Width = 0
                m_rctSelectedZone.Height = 0

                MyEventArgs = New USGEventArgs("")
            Else
                m_rctSelectedZone.X = intX
                m_rctSelectedZone.Y = intY
                m_rctSelectedZone.Width = intWidth
                m_rctSelectedZone.Height = intHeight

                MyEventArgs = New USGEventArgs(Zone)
            End If

            picNet.Refresh()
            RaiseEvent USGClick(New Object, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Cleares the zone selection rectangle
    ''' </summary>
    ''' <remarks>Use this method to clear zone selection after save or cancel operation</remarks>
    Public Sub USGClearZone()
        Try
            m_rctSelectedZone.X = 0
            m_rctSelectedZone.Y = 0
            m_rctSelectedZone.Width = 0
            m_rctSelectedZone.Height = 0

            picNet.Refresh()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class

Public Class USGEventArgs
    Inherits System.EventArgs

#Region " Constants & Variables "
    Private m_strZone As String
#End Region

#Region " Public properties "
    Public ReadOnly Property USGZone() As String
        Get
            Return m_strZone
        End Get
    End Property
#End Region

#Region " Public Methods "
    Public Sub New(ByVal Zone As String)
        Try
            m_strZone = Zone
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


#End Region

End Class