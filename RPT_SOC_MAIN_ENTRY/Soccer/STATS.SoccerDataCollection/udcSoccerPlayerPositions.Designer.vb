﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class udcSoccerPlayerPositions
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picField = New System.Windows.Forms.PictureBox()
        Me.btnGoalie = New System.Windows.Forms.Button()
        Me.btnPlayer1 = New System.Windows.Forms.Button()
        Me.btnPlayer2 = New System.Windows.Forms.Button()
        Me.btnPlayer3 = New System.Windows.Forms.Button()
        Me.btnPlayer4 = New System.Windows.Forms.Button()
        Me.btnPlayer5 = New System.Windows.Forms.Button()
        Me.btnPlayer6 = New System.Windows.Forms.Button()
        Me.btnPlayer7 = New System.Windows.Forms.Button()
        Me.btnPlayer8 = New System.Windows.Forms.Button()
        Me.btnPlayer9 = New System.Windows.Forms.Button()
        Me.btnPlayer10 = New System.Windows.Forms.Button()
        CType(Me.picField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picField
        '
        Me.picField.BackColor = System.Drawing.Color.WhiteSmoke
        Me.picField.Location = New System.Drawing.Point(0, 0)
        Me.picField.Margin = New System.Windows.Forms.Padding(0)
        Me.picField.Name = "picField"
        Me.picField.Size = New System.Drawing.Size(301, 192)
        Me.picField.TabIndex = 0
        Me.picField.TabStop = False
        '
        'btnGoalie
        '
        Me.btnGoalie.Location = New System.Drawing.Point(3, 3)
        Me.btnGoalie.Name = "btnGoalie"
        Me.btnGoalie.Size = New System.Drawing.Size(75, 23)
        Me.btnGoalie.TabIndex = 1
        Me.btnGoalie.UseVisualStyleBackColor = True
        '
        'btnPlayer1
        '
        Me.btnPlayer1.Location = New System.Drawing.Point(84, 3)
        Me.btnPlayer1.Name = "btnPlayer1"
        Me.btnPlayer1.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer1.TabIndex = 2
        Me.btnPlayer1.UseVisualStyleBackColor = True
        '
        'btnPlayer2
        '
        Me.btnPlayer2.Location = New System.Drawing.Point(84, 32)
        Me.btnPlayer2.Name = "btnPlayer2"
        Me.btnPlayer2.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer2.TabIndex = 3
        Me.btnPlayer2.UseVisualStyleBackColor = True
        '
        'btnPlayer3
        '
        Me.btnPlayer3.Location = New System.Drawing.Point(84, 61)
        Me.btnPlayer3.Name = "btnPlayer3"
        Me.btnPlayer3.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer3.TabIndex = 4
        Me.btnPlayer3.UseVisualStyleBackColor = True
        '
        'btnPlayer4
        '
        Me.btnPlayer4.Location = New System.Drawing.Point(84, 90)
        Me.btnPlayer4.Name = "btnPlayer4"
        Me.btnPlayer4.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer4.TabIndex = 5
        Me.btnPlayer4.UseVisualStyleBackColor = True
        '
        'btnPlayer5
        '
        Me.btnPlayer5.Location = New System.Drawing.Point(84, 119)
        Me.btnPlayer5.Name = "btnPlayer5"
        Me.btnPlayer5.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer5.TabIndex = 6
        Me.btnPlayer5.UseVisualStyleBackColor = True
        '
        'btnPlayer6
        '
        Me.btnPlayer6.Location = New System.Drawing.Point(165, 3)
        Me.btnPlayer6.Name = "btnPlayer6"
        Me.btnPlayer6.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer6.TabIndex = 7
        Me.btnPlayer6.UseVisualStyleBackColor = True
        '
        'btnPlayer7
        '
        Me.btnPlayer7.Location = New System.Drawing.Point(165, 32)
        Me.btnPlayer7.Name = "btnPlayer7"
        Me.btnPlayer7.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer7.TabIndex = 8
        Me.btnPlayer7.UseVisualStyleBackColor = True
        '
        'btnPlayer8
        '
        Me.btnPlayer8.Location = New System.Drawing.Point(165, 61)
        Me.btnPlayer8.Name = "btnPlayer8"
        Me.btnPlayer8.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer8.TabIndex = 9
        Me.btnPlayer8.UseVisualStyleBackColor = True
        '
        'btnPlayer9
        '
        Me.btnPlayer9.Location = New System.Drawing.Point(165, 90)
        Me.btnPlayer9.Name = "btnPlayer9"
        Me.btnPlayer9.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer9.TabIndex = 10
        Me.btnPlayer9.UseVisualStyleBackColor = True
        '
        'btnPlayer10
        '
        Me.btnPlayer10.Location = New System.Drawing.Point(165, 119)
        Me.btnPlayer10.Name = "btnPlayer10"
        Me.btnPlayer10.Size = New System.Drawing.Size(75, 23)
        Me.btnPlayer10.TabIndex = 11
        Me.btnPlayer10.UseVisualStyleBackColor = True
        '
        'udcSoccerPlayerPositions
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnPlayer10)
        Me.Controls.Add(Me.btnPlayer9)
        Me.Controls.Add(Me.btnPlayer8)
        Me.Controls.Add(Me.btnPlayer7)
        Me.Controls.Add(Me.btnPlayer6)
        Me.Controls.Add(Me.btnPlayer5)
        Me.Controls.Add(Me.btnPlayer4)
        Me.Controls.Add(Me.btnPlayer3)
        Me.Controls.Add(Me.btnPlayer2)
        Me.Controls.Add(Me.btnPlayer1)
        Me.Controls.Add(Me.btnGoalie)
        Me.Controls.Add(Me.picField)
        Me.Name = "udcSoccerPlayerPositions"
        Me.Size = New System.Drawing.Size(301, 192)
        CType(Me.picField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picField As System.Windows.Forms.PictureBox
    Friend WithEvents btnGoalie As System.Windows.Forms.Button
    Friend WithEvents btnPlayer1 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer2 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer3 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer4 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer5 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer6 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer7 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer8 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer9 As System.Windows.Forms.Button
    Friend WithEvents btnPlayer10 As System.Windows.Forms.Button

End Class
