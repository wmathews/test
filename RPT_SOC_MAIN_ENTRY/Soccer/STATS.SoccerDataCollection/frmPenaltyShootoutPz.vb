﻿Imports System.IO
Imports STATS.SoccerBL
Imports STATS.Utility

Public Class frmPenaltyShootoutPz
#Region " Constants & Variables "
    Private _objUtility As New clsUtility
    Private ReadOnly _objUtilAudit As New clsAuditLog
    Private _objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private _objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private _objPenaltyShootout As New STATS.SoccerBL.clsPenaltyShootout
    Private ReadOnly _lsupport As New languagesupport
    Private _objActionDetails As ClsActionDetails = ClsActionDetails.GetInstance()
    Private _autoFired As Boolean = False       'to check if selectedindexchanged event of listboxes was autofired
    Private ReadOnly _messageDialog As New frmMessageDialog
    Private _playerTable As New List(Of ClsPlayerData.PlayerData)
    Private _objClsPlayerData As ClsPlayerData = ClsPlayerData.GetInstance()
    Private _dsShootoutInputs As DataSet
    Private _insertMode As Boolean = True
    Private _objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private _objActions As ClsActions = ClsActions.GetInstance()

#End Region

    Private Sub frmPenaltyShootoutPz_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPenaltyShootoutPz", Nothing, 1, 0)
            lklHometeam.Text = _objGameDetails.HomeTeam.Trim()
            lklAwayteam.Text = _objGameDetails.AwayTeam.Trim()

            lklHometeam.Text = _objGameDetails.HomeTeam.Trim()
            lklAwayteam.Text = _objGameDetails.AwayTeam.Trim()

            Dim logoImage As MemoryStream
             logoImage = _objGeneral.GetTeamLogo(_objGameDetails.LeagueID, _objGameDetails.AwayTeamID)
            If logoImage IsNot Nothing Then
                picAwayLogo.Image = New Bitmap(logoImage)
            Else 'TOSOCRS-338
                picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
            logoImage = _objGeneral.GetTeamLogo(_objGameDetails.LeagueID, _objGameDetails.HomeTeamID)
            If logoImage IsNot Nothing Then
                picHomeLogo.Image = New Bitmap(logoImage)
            Else  'TOSOCRS-338
                picHomeLogo.Image =  Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If

            rdoHome.Text = _objGameDetails.HomeTeam
            rdoAway.Text = _objGameDetails.AwayTeam
            rdoHome.Tag = _objGameDetails.HomeTeamID
            rdoAway.Tag = _objGameDetails.AwayTeamID

            If _objGameDetails.IsHomeTeamOnLeftSwitch = False Then
                picAwayLogo.Location = New Point(15, 34)
                picHomeLogo.Location = New Point(825, 34)
                lklAwayteam.Location = New Point(26, 11)
                lklHometeam.Location = New Point(563, 11)
                lblScoreAway.Location = New Point(216, 11)
                lblScoreHome.Location = New Point(464, 11)
                _objGameDetails.OffensiveTeamID = _objGameDetails.AwayTeamID
                _objGameDetails.DefensiveTeamID = _objGameDetails.HomeTeamID
            Else
                _objGameDetails.OffensiveTeamID = _objGameDetails.HomeTeamID
                _objGameDetails.DefensiveTeamID = _objGameDetails.AwayTeamID
            End If
            FillTeamScores()
            FillShootoutInputs()
            FillTimeLine()
            _playerTable = _objClsPlayerData.FillPlayer(_objGeneral.GetAllRosters(_objGameDetails.GameCode, _objGameDetails.languageid).Tables(0))
            SequenceCount()
            UdcSoccerGoalFrame1.USGFConfineToGoal = True
            LblAlert.Visible = False
            Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            rdoLeft.Checked = True
            rdoRight.Checked = False
            'Multilingual for The form controls
            If _objGameDetails.languageid <> 1 Then
                Dim lsupport As New languagesupport
                lsupport.FormLanguageTranslate(Me)
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub rdoAway_CheckedChanged(sender As Object, e As EventArgs) Handles rdoAway.CheckedChanged
        Try
            If (rdoAway.Checked) Then
                If ValidateTeam(rdoAway.Tag) Then
                    _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.CheckBoxSelected, rdoAway.Text, Nothing, 1, 0)
                    FillPlayers(rdoAway.Tag, rdoHome.Tag)
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub rdoHome_CheckedChanged(sender As Object, e As EventArgs) Handles rdoHome.CheckedChanged
        Try
            If (rdoHome.Checked) Then
                If ValidateTeam(rdoHome.Tag) Then
                    _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.CheckBoxSelected, rdoAway.Text, Nothing, 1, 0)
                    FillPlayers(rdoHome.Tag, rdoAway.Tag)
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Public Sub FillPlayers(ByVal hometeam As Integer, ByVal defenceteam As Integer)
        Try
            Dim playerData = (From players In _playerTable Where players.TeamId = hometeam AndAlso (players.LineUp Or players.Bench) Order By players.StartingPosition Ascending Select players.Player, players.PlayerId).ToList()
            clsUtility.LoadControl(lstPlayer, playerData.ConvertAll(Function(x) DirectCast(x, Object)), "Player", "PlayerId", "OFFENSIVE_PLAYER_ID", -1)
            Dim defencePlayerData = (From players In _playerTable Where players.TeamId = defenceteam AndAlso (players.LineUp Or players.Bench) Order By players.StartingPosition Select players.Player, players.PlayerId).ToList()
            clsUtility.LoadControl(lstDefence, defencePlayerData.ConvertAll(Function(x) DirectCast(x, Object)), "Player", "PlayerId", "DEFENSIVE_PLAYER_ID", 0) ' 0 GK is default Defending Player
            SequenceCount()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Validate The Team
    Function ValidateTeam(ByVal teamId As Integer) As Boolean
        Try
            Dim teamValidate As Boolean = True
            If Not _autoFired Then
                Dim dgrCount As Integer = dgvPBP.Rows.Count
                If dgrCount > 0 Then
                    If teamId = CInt(dgvPBP.Rows(dgrCount - 1).Cells("TEAM_ID").Value) And _objGameDetails.CoverageLevel = 1 Then
                        _messageDialog.Show(_objGameDetails.languageid, "Wrong team selected for Shootout!", "Shootout", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        teamValidate = False
                        ResetControls()
                        UncheckTeam()
                    End If
                End If
            End If
            Return teamValidate
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub lstResult_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstResult.SelectedIndexChanged
        Try
            UdcSoccerGoalFrame1.USGFClearMarks()
            UdcSoccerGoalFrame1.USGFConfineToGoal = False
            lstShotType.Enabled = True
            If Not _autoFired Then
                FillShotType(lstResult.SelectedValue)
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, Nothing, 1, 0)

            Dim validationCheck As Tuple(Of Boolean, String) = Validation()
            If validationCheck.Item1 Then
                If Validation.Item2 IsNot Nothing Then
                    _messageDialog.Show(_objGameDetails.languageid, Validation.Item2, Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                Exit Sub
            End If

            Dim drPbpData As DataRow = _objActionDetails.PzPBP.Tables(0).NewRow()
            'Default Values
            drPbpData("GAME_CODE") = _objGameDetails.GameCode
            drPbpData("FEED_NUMBER") = _objGameDetails.FeedNumber
            drPbpData("TEAM_ID") = IIf(rdoHome.Checked, rdoHome.Tag, rdoAway.Tag)
            drPbpData("REPORTER_ROLE") = _objGameDetails.ReporterRole
            drPbpData("CONTINUATION") = "F"
            drPbpData("RECORD_EDITED") = "N"
            drPbpData("PROCESSED") = "N"
            drPbpData("UNIQUE_ID") = 1
            drPbpData("EDIT_UID") = 0
            drPbpData("DEMO_DATA") = "N"
            drPbpData("PERIOD") = _objGameDetails.CurrentPeriod
            drPbpData("TIME_ELAPSED") = _objGeneral.GetActionsMaxTimeElapsed(_objGameDetails.GameCode, _objGameDetails.FeedNumber)
            If Not _insertMode Then
                Dim dtTimeDiff As DateTime
                dtTimeDiff = CDate(dgvPBP.SelectedRows(0).Cells("SYSTEM_TIME").Value.ToString())
                drPbpData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
                drPbpData("EDIT_UID") = dgvPBP.SelectedRows(0).Cells("UNIQUE_ID").Value
                drPbpData("OFFENSE_SCORE") = dgvPBP.SelectedRows(0).Cells("OFFENSE_SCORE").Value
                drPbpData("DEFENSE_SCORE") = dgvPBP.SelectedRows(0).Cells("DEFENSE_SCORE").Value
                drPbpData("SEQUENCE_NUMBER") = dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value
                drPbpData("SHOOTOUT_ROUND") = dgvPBP.SelectedRows(0).Cells("SHOOTOUT_ROUND").Value
            Else
                drPbpData("SYSTEM_TIME") = DateTimeHelper.GetDateString((Date.Now - _objLoginDetails.USIndiaTimeDiff), DateTimeHelper.OutputFormat.ISOFormat)
                drPbpData("EDIT_UID") = 0
                drPbpData("OFFENSE_SCORE") = 0
                drPbpData("DEFENSE_SCORE") = 0
                drPbpData("SEQUENCE_NUMBER") = 0

                If (CInt(txtSequence.Text) + 1) Mod 2 = 0 Then
                    drPbpData("SHOOTOUT_ROUND") = CInt(CInt(txtSequence.Text) + 1) / 2
                Else
                    drPbpData("SHOOTOUT_ROUND") = CInt(txtSequence.Text) / 2
                End If

            End If
            'Goal Zone green
            drPbpData("BALL_Y") = If(_objActionDetails.BallY, DBNull.Value)
            drPbpData("BALL_Z") = If(_objActionDetails.BallZ, DBNull.Value)
            'Goal Zone orange
            drPbpData("HAND_Y") = If(_objActionDetails.HandY, DBNull.Value)
            drPbpData("HAND_Z") = If(_objActionDetails.HandZ, DBNull.Value)
            drPbpData("KEEPERZONE_ID") = If(_objActionDetails.KepperZoneId, DBNull.Value)
            drPbpData("GOALZONE_ID") = If(_objActionDetails.GoalZoneId, DBNull.Value)
            drPbpData("SHOOTOUT_SIDE") = If(rdoLeft.Checked, "L", "R")
            For Each listControl As Control In pnlSave.Controls.OfType(Of ListBox)()
                Dim lstEventDetails As ListBox
                lstEventDetails = CType(listControl, ListBox)
                If lstEventDetails.Visible Then
                    drPbpData(lstEventDetails.Tag) = If(lstEventDetails.SelectedValue, DBNull.Value)
                End If
            Next

            _objActionDetails.PzPBP.Tables(0).Rows.Add(drPbpData)
            _objActionDetails.PzPBP.DataSetName = "SoccerEventData"
            _objActionDetails.PzPBP.Tables(0).TableName = "PBP"

            'pbp dataInsert/ Edit
            _objActions.InsertEditActionsPBPData(_objActionDetails.PzPBP.GetXml(), drPbpData("SEQUENCE_NUMBER"))
            FillTimeLine()
            SequenceCount()
            ResetControls()
            FillTeamScores()
            'Shooting Team Changes after each Shootout Kick
            If _insertMode Then
                _autoFired = True
                If rdoHome.Checked Then
                    rdoAway.Checked = True
                Else
                    rdoHome.Checked = True
                End If
            Else
                UncheckTeam()
                lstPlayer.DataSource = Nothing
                lstDefence.DataSource = Nothing
            End If
            LblAlert.Visible = False
        Catch ex As Exception
            Throw
        Finally
            _objActionDetails.PzPBP.Tables(0).Rows.Clear()
            _insertMode = True
            _autoFired = False
        End Try
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            _insertMode = True
            ResetControls()
            SequenceCount()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, Nothing, 1, 0)
            If (dgvPBP.SelectedRows.Count = 0) Then
                Exit Sub
            End If
            If _messageDialog.Show(_objGameDetails.languageid, "Are you Sure to delete this record ?", Text, MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                _objGeneral.DeletePlaybyPlayData(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, _objGameDetails.ReporterRoleSerial)
                FillTimeLine()
                SequenceCount()
                ResetControls()
                FillTeamScores()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        Finally
            _insertMode = True
        End Try
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            Close()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub frmPenaltyShootoutPz_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try

            If (FormClosing()) Then
                e.Cancel = False
            Else
                e.Cancel = True
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'if shooting player takes a penalty shot he is crossed out the next time his team is up for a penalty shootout
    Private Sub lstPlayer_DrawItem(sender As Object, e As DrawItemEventArgs) Handles lstPlayer.DrawItem
        Try
            e.DrawBackground()
            lstPlayer.HorizontalExtent = 500

            Dim myBrush As Brush
            Dim myBackColor As Color = Color.White
            Dim myForeColor As Color = Color.Black
            Dim myFont As Font = Me.Font
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                myBackColor = Color.CornflowerBlue
                myForeColor = Color.White
            End If
            If lstPlayer.Items.Count > 0 Then
                If IsPlayerUsedAlready(DirectCast(lstPlayer.Items(e.Index), Object).PlayerId) Then
                    myFont = New Font(Me.Font, FontStyle.Strikeout)
                    myForeColor = Color.Maroon
                End If

                myBrush = New SolidBrush(myBackColor)
                e.Graphics.FillRectangle(myBrush, e.Bounds)
                myBrush = New SolidBrush(myForeColor)
                e.Graphics.DrawString(Convert.ToString(DirectCast(lstPlayer.Items(e.Index), Object).player), myFont, myBrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
                e.DrawFocusRectangle()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub dgvPBP_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPBP.CellDoubleClick
        Try
            If (e.RowIndex = -1) Then 'To avoid Column header click
                Exit Sub
            End If
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "Double Click Datagrid Cell", 1, 0)

            If (dgvPBP.SelectedRows.Count = 0) Then
                Exit Sub
            End If
            _autoFired = True
            _insertMode = False
            LblAlert.Visible = True
            If rdoHome.Tag = dgvPBP.SelectedRows(0).Cells("TEAM_ID").Value Then
                rdoHome.Checked = True
            Else
                rdoAway.Checked = True
            End If
            rdoHome.Enabled = False
            rdoAway.Enabled = False
            txtSequence.Text = dgvPBP.SelectedRows(0).Cells("Sequence").Value
            FillShotType(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value)
            For Each listControl As Control In pnlSave.Controls.OfType(Of ListBox)()
                Dim lstEventDetails As ListBox
                lstEventDetails = CType(listControl, ListBox)
                If (dgvPBP.SelectedRows(0).Cells(lstEventDetails.Tag).Value.ToString() <> Nothing) Then
                    lstEventDetails.SelectedValue = If(dgvPBP.SelectedRows(0).Cells(lstEventDetails.Tag).Value, -1)
                End If
            Next
            If dgvPBP.SelectedRows(0).Cells("SHOOTOUT_SIDE").Value IsNot DBNull.Value Then
                If (dgvPBP.SelectedRows(0).Cells("SHOOTOUT_SIDE").Value = "R") Then
                    rdoRight.Checked = True
                Else
                    rdoLeft.Checked = True
                End If
            End If
            UdcSoccerGoalFrame1.USGFClearMarks()
            _objActionDetails.HandY = If(dgvPBP.SelectedRows(0).Cells("HAND_Y").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("HAND_Y").Value)
            _objActionDetails.HandZ = If(dgvPBP.SelectedRows(0).Cells("HAND_Z").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("HAND_Z").Value)
            _objActionDetails.BallY = If(dgvPBP.SelectedRows(0).Cells("BALL_Y").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("BALL_Y").Value)
            _objActionDetails.BallZ = If(dgvPBP.SelectedRows(0).Cells("BALL_Z").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("BALL_Z").Value)

            If _objActionDetails.BallY.HasValue Then
                SetGoalFramePoints(_objActionDetails.BallY, _objActionDetails.BallZ)
            End If

            If _objActionDetails.HandY.HasValue Then
                SetGoalFramePoints(_objActionDetails.HandY, _objActionDetails.HandZ)
            End If

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        Finally
            _autoFired = False
        End Try
    End Sub
    Private Sub dgvPBP_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPBP.CellClick
        If (e.RowIndex = -1) Then 'To avoid Column header click
            Exit Sub
        End If
        ResetControls()
        UncheckTeam()
        txtSequence.Text = Nothing
        _insertMode = True
    End Sub

    Private Sub dgvPBP_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvPBP.KeyDown
        Try
            If e.KeyCode = 46 And dgvPBP.SelectedRows.Count > 0 Then
                e.SuppressKeyPress = True
                btnDelete.PerformClick()
            ElseIf e.KeyCode = 33 Or e.KeyCode = 34 Then 'page up/down
                e.SuppressKeyPress = True
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub dgvPBP_KeyUp(sender As Object, e As KeyEventArgs) Handles dgvPBP.KeyUp
        If (e.KeyCode = 38 Or e.KeyCode = 40 Or e.KeyCode = 13) And dgvPBP.SelectedRows.Count > 0 Then  'Up/Down arrow ,enter key
            e.SuppressKeyPress = True
            ResetControls()
            UncheckTeam()
            txtSequence.Text = Nothing
            _insertMode = True
        End If
    End Sub
    Private Sub frmPenaltyShootoutPz_MouseClick(sender As Object, e As MouseEventArgs) Handles MyBase.MouseClick
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MouseClicked, "PenaltyShootout Save Through Mouse Right Click", 1, 0)
                btnSave.PerformClick()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub pnlSave_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlSave.MouseClick
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MouseClicked, "PenaltyShootout Save Through Mouse Right Click", 1, 0)
                btnSave.PerformClick()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub SubSide_of_field_CheckedChanged(sender As Object, e As EventArgs) Handles rdoLeft.CheckedChanged, rdoRight.CheckedChanged
        Try
            UdcSoccerGoalFrame1.USGFClearMarks()
            If _objActionDetails.BallY.HasValue Then
                SetGoalFramePoints(_objActionDetails.BallY, _objActionDetails.BallZ)
            End If
            If _objActionDetails.HandY.HasValue Then
                SetGoalFramePoints(_objActionDetails.HandY, _objActionDetails.HandZ)
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub


#Region "User Defined Functions"
    Private Sub SequenceCount()
        Try
            txtSequence.Text = dgvPBP.Rows.Count + 1
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UncheckTeam()
        rdoHome.Checked = False
        rdoAway.Checked = False
    End Sub

    Private Sub FillTeamScores()
        Try
            Dim dsScores As DataSet
            dsScores = _objGeneral.GetPenaltyShootoutScoresPz(_objGameDetails.GameCode, _objGameDetails.FeedNumber)
            lblScoreHome.Text = dsScores.Tables(0).Rows(0).Item("HOME").ToString()
            lblScoreAway.Text = dsScores.Tables(0).Rows(0).Item("AWAY").ToString()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub FillShootoutInputs()
        Try
            _dsShootoutInputs = _objPenaltyShootout.FetchShootoutInputsPz(_objGameDetails.GameCode, _objGameDetails.languageid)
            If _dsShootoutInputs.Tables.Count > 0 Then
                _autoFired = True
                clsUtility.LoadControl(lstType, _dsShootoutInputs.Tables(1), "SHOT_DESC", "SHOT_DESC_ID", "SHOT_DESC")
                clsUtility.LoadControl(lstResult, _dsShootoutInputs.Tables(2), "EVENT_CODE_DESC", "EVENT_CODE_ID", "EVENT_CODE_ID")
                clsUtility.LoadControl(lstBodyPart, _dsShootoutInputs.Tables(3), "BODY_PART", "BODY_PART_ID", "BODY_PART_ID")
            End If
        Catch ex As Exception
            Throw
        Finally
            _autoFired = False
        End Try
    End Sub
    Private Function IsPlayerUsedAlready(ByVal intPlayerId As Integer) As Boolean
        Try
            Dim isPlayerUsed As Boolean
            Dim playerCount = (From theRow As DataGridViewRow In dgvPBP.Rows _
                               Where Not IsDBNull(theRow.Cells("OFFENSIVE_PLAYER_ID").Value) AndAlso (theRow.Cells("OFFENSIVE_PLAYER_ID").Value) = intPlayerId _
                              Select theRow.Index).Count()

            If (playerCount > 0) Then
                isPlayerUsed = True
            End If
            Return isPlayerUsed
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub ResetControls()
        Try
            lstPlayer.SelectedIndex = -1
            lstResult.SelectedIndex = -1
            lstBodyPart.SelectedIndex = -1
            lstType.SelectedIndex = -1
            lstShotType.SelectedIndex = -1
            rdoHome.Enabled = True
            rdoAway.Enabled = True
            UdcSoccerGoalFrame1.USGFClearMarks()
            LblAlert.Visible = False
            _objActionDetails.HandY = Nothing
            _objActionDetails.HandZ = Nothing
            _objActionDetails.BallY = Nothing
            _objActionDetails.BallZ = Nothing
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub FillTimeLine()
        Try
            dgvPBP.DataSource = Nothing
            Dim dsShootoutPzpbpData As New DataSet
            dsShootoutPzpbpData = _objPenaltyShootout.GetShootoutPzpbpData(_objGameDetails.GameCode, _objGameDetails.FeedNumber, _objGameDetails.languageid)
            dgvPBP.DataSource = dsShootoutPzpbpData.Tables(0)
            'Hide grid column
            For columnIndex As Integer = 8 To dgvPBP.Columns.Count - 1
                dgvPBP.Columns(columnIndex).Visible = False
            Next
            ' Disable dgvPBP.Columns Sortable
            For Each Column In dgvPBP.Columns
                Column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next
            If (dgvPBP.RowCount > 0) Then
                dgvPBP.ClearSelection()
                dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub SetGoalFramePoints(ByVal fieldX? As Decimal, ByVal fieldY? As Decimal)
        Try
            If fieldX IsNot Nothing And fieldY IsNot Nothing Then
                Dim goalFramePoints As PointF
                goalFramePoints = GetUnmappedCoordinateValues(fieldX, fieldY)
                UdcSoccerGoalFrame1.USGFSetMark(goalFramePoints.X, goalFramePoints.Y)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub FillShotType(ByVal eventId As Integer)
        lblMissType.Text = "Miss Type"
        lstShotType.Visible = True
        lblMissType.Visible = True
        UdcSoccerGoalFrame1.USGFDisallowInGoal = False
        UdcSoccerGoalFrame1.USGFConfineToGoal = False
        UdcSoccerGoalFrame1.USGFClearMarks()
        If eventId = 31 Then 'Save
            clsUtility.LoadControl(lstShotType, _dsShootoutInputs.Tables(4), "SAVE_TYPE", "SAVE_TYPE_ID", "SAVE_TYPE_ID")
            lblMissType.Text = "Save Type"
            UdcSoccerGoalFrame1.USGFConfineToGoal = True
            UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Mark1Only
        ElseIf eventId = 41 Then 'Miss
            clsUtility.LoadControl(lstShotType, _dsShootoutInputs.Tables(0), "SHOT_RESULT", "SHOT_RESULT_ID", "SHOT_RESULT")
            UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Both
            UdcSoccerGoalFrame1.USGFDisallowInGoal = True
        Else
            lstShotType.Visible = False
            lblMissType.Visible = False
            lstShotType.DataSource = Nothing
            UdcSoccerGoalFrame1.USGFConfineToGoal = True
            lstShotType.Tag = "SHOT_RESULT"
            UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Both
        End If
    End Sub
    Function Validation() As Tuple(Of Boolean, String)
        Try

            If (Not rdoHome.Checked AndAlso Not rdoAway.Checked) Or lstPlayer.SelectedIndex = -1 Or lstResult.SelectedIndex = -1 Then
                Return New Tuple(Of Boolean, String)(True, "Please select mandatory data points (Team, Player, Result)!")
            End If
            If rdoLeft.Checked = False And rdoRight.Checked = False Then
                Return New Tuple(Of Boolean, String)(True, "Please select Goal Frame Direction Left or Right!")
            End If
            'Else no failure
            Return New Tuple(Of Boolean, String)(False, "No issues")
        Catch ex As Exception
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "InsertValidation Failed dgvRowsCount : " & dgvPBP.SelectedRows.Count, Nothing, 1, 0)
            Throw
        End Try
    End Function

    Private Function FormClosing() As Boolean
        Try
            Dim formClose As Boolean = True
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            If Not _messageDialog.Show(_objGameDetails.languageid, "You are ending the shootout with a score of " + lklHometeam.Text + " (" + lblScoreHome.Text + " )" + " and " + lklAwayteam.Text + " (" + lblScoreAway.Text + " ) . Are you sure?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                formClose = False
            End If
            Return formClose
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Function

#End Region

#Region "Goal Frame"
    Private Sub UdcSoccerGoalFrame1_USGFClick_1(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFClick

        Dim validationCheck As Tuple(Of Boolean, String) = Validation()
        If validationCheck.Item1 Then
            If Validation.Item2 IsNot Nothing Then
                _messageDialog.Show(_objGameDetails.languageid, Validation.Item2, Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            Exit Sub
        End If

        Dim pointYz As PointF
        If e.USGFmark1.IsEmpty Then
            lblClickLocationGreen.Text = ""
            picGFGreenBlink.Visible = True
            picGFGreen.Visible = False
            _objActionDetails.BallY = Nothing
            _objActionDetails.BallZ = Nothing
            _objActionDetails.GoalZoneId = Nothing
        Else
            'green
            pointYz = GetMappedCoordinateValues(e.USGFmark1.X, e.USGFmark1.Y)
            lblClickLocationGreen.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            picGFGreenBlink.Visible = False
            picGFGreen.Visible = True
            _objActionDetails.BallY = pointYz.X
            _objActionDetails.BallZ = pointYz.Y
            _objActionDetails.GoalZoneId = e.USGFmark1zone
        End If

        If e.USGFmark2.IsEmpty Then
            lblClickLocationOrange.Text = ""
            picGFOrangeBlink.Visible = True
            picGFOrange.Visible = False
            _objActionDetails.HandY = Nothing
            _objActionDetails.HandZ = Nothing
            _objActionDetails.KepperZoneId = Nothing
        Else
            'Orange
            pointYz = GetMappedCoordinateValues(e.USGFmark2.X, e.USGFmark2.Y)
            lblClickLocationOrange.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            picGFOrangeBlink.Visible = False
            picGFOrange.Visible = True
            _objActionDetails.HandY = pointYz.X
            _objActionDetails.HandZ = pointYz.Y
            _objActionDetails.KepperZoneId = e.USGFmark2zone
        End If
    End Sub
    Private Sub UdcSoccerGoalFrame1_USGFMarkRemoved_1(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFMarkRemoved
        Try
            Dim pointYz As PointF
            If e.USGFmark1.IsEmpty Then
                lblClickLocationGreen.Text = ""
                picGFGreenBlink.Visible = True
                picGFGreen.Visible = False
            Else
                picGFGreenBlink.Visible = False
                picGFGreen.Visible = True
                pointYz = GetMappedCoordinateValues(e.USGFmark1.X, e.USGFmark1.Y)
                _objActionDetails.BallY = Nothing
                _objActionDetails.BallZ = Nothing
                _objActionDetails.GoalZoneId = e.USGFmark1zone
                lblClickLocationGreen.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            End If

            If e.USGFmark2.IsEmpty Then
                lblClickLocationOrange.Text = ""
                picGFOrangeBlink.Visible = True
                picGFOrange.Visible = False
            Else
                picGFOrangeBlink.Visible = False
                picGFOrange.Visible = True
                pointYz = GetMappedCoordinateValues(e.USGFmark2.X, e.USGFmark2.Y)
                _objActionDetails.HandY = Nothing
                _objActionDetails.HandZ = Nothing
                _objActionDetails.KepperZoneId = Nothing
                lblClickLocationOrange.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub UdcSoccerGoalFrame1_USGFMouseMove_1(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFMouseMove
        Try
            Dim pointYz As PointF
            pointYz = GetMappedCoordinateValues(e.USGFx, e.USGFy)
            lblMoveY.Text = "Y: " & pointYz.X
            lblMoveZ.Text = "Z: " & pointYz.Y
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub btnZoom_Click(sender As Object, e As EventArgs) Handles btnZoom.Click
        Try
            If btnZoom.Text = "Wide View" Then
                UdcSoccerGoalFrame1.Width = pnlGoalframe.Width
                UdcSoccerGoalFrame1.Left = 0
                UdcSoccerGoalFrame1.Top = pnlGoalframe.Height - pnlInfo.Height - UdcSoccerGoalFrame1.Height
                btnZoom.Text = "Closer View"
            Else
                UdcSoccerGoalFrame1.Height = 184
                UdcSoccerGoalFrame1.Width = 1242
                UdcSoccerGoalFrame1.Top = 0
                UdcSoccerGoalFrame1.Left = -423
                btnZoom.Text = "Wide View"
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Function GetMappedCoordinateValues(dblX As Double, dblY As Double, Optional blnLeftGoal As Boolean = True) As PointF
        Try
            Dim pntReturn As PointF
            Const fieldHeight As Integer = 70

            If rdoLeft.Checked Then
                dblX = (fieldHeight / 2) - dblX
            Else
                dblX = (fieldHeight / 2) + dblX
            End If

            pntReturn.X = Math.Round(dblX, 2)
            pntReturn.Y = Math.Round(dblY, 2)
            Return pntReturn
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function GetUnmappedCoordinateValues(dblX As Double, dblY As Double) As PointF
        Try
            Dim pntReturn As PointF
            Const fieldHeight As Integer = 70

            If rdoLeft.Checked Then
                dblX = (fieldHeight / 2) - dblX
            Else
                dblX = dblX - (fieldHeight / 2)
            End If

            pntReturn.X = Math.Round(dblX, 2)
            pntReturn.Y = Math.Round(dblY, 2)
            Return pntReturn
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region


End Class