﻿#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.IO
Imports Microsoft.Win32
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmActiveGoalkeeper
' Author        :
' Created Date  :
' Description   : This form acts as a container for the entry forms of all soccer modules
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmActiveGoalkeeper

    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private m_intTeamID As Integer
    Private m_objUtility As New clsUtility
    Private m_dsPlayersChecked As DataSet
    Private m_decSeq As Decimal
    Private lsupport As New languagesupport
    Dim strmessage1 As String = "Enter the correct time!"
    Dim strmessage2 As String = "There are more than one active goal keeper selected. Please chek it"
    Dim strmessage3 As String = "One active goalkeeper is mandatory - please select one from the list!"
    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900

    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

    Private Sub frmActiveGoalkeeper_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                lblTime.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTime.Text)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)

            End If
            'ASSIGNING HOME AND AWAY TEAM NAMES TO THE RADIO BUTTONS
            'radHomeTeam.Text = m_objGameDetails.HomeTeam
            'radAwayTeam.Text = m_objGameDetails.AwayTeam
            If (m_objGameDetails.CoverageLevel = 6) Then
                Dim objClsPlayerData As ClsPlayerData = ClsPlayerData.GetInstance()
                m_dsPlayersChecked = m_objGeneral.GetAllRosters(m_objGameDetails.GameCode, m_objGameDetails.languageid)
            Else
                m_dsPlayersChecked = m_objclsTeamSetup.RosterInfo.Copy()
            End If

            radHomeTeam.Checked = False
            radAwayTeam.Checked = False

            If m_objGameDetails.HomeTeamAbbrev.Length > 12 Then
                radHomeTeam.Text = m_objGameDetails.HomeTeamAbbrev.Substring(0, 12)
            Else
                radHomeTeam.Text = m_objGameDetails.HomeTeamAbbrev
            End If

            If m_objGameDetails.AwayTeamAbbrev.Length > 12 Then
                radAwayTeam.Text = m_objGameDetails.AwayTeamAbbrev.Substring(0, 12)
            Else
                radAwayTeam.Text = m_objGameDetails.AwayTeamAbbrev
            End If

            If m_objGameDetails.IsEditMode Then
                If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                    radHomeTeam.Checked = True
                    radAwayTeam.Checked = False
                    lblTeamName.Text = radHomeTeam.Text
                Else
                    radHomeTeam.Checked = False
                    radAwayTeam.Checked = True
                    lblTeamName.Text = radAwayTeam.Text
                End If
                Dim Strtime As String = CalculateTimeElapseAfterMod2(m_objGameDetails.CommentsTime, m_objGameDetails.CurrentPeriod)
                Dim arr As Array = Strtime.Split(CChar(":"))
                txtTimeMod2.Text = arr.GetValue(0).ToString
                txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))

                txtTime.Text = CalculateTimeElapseAfterMod1(m_objGameDetails.CommentsTime, m_objGameDetails.CurrentPeriod)
                If (m_objGameDetails.CoverageLevel = 6) Then
                    txtTime.Text = CalculateTimeElapseAfter(m_objGameDetails.CommentsTime, m_objGameDetails.CurrentPeriod)
                End If
                txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
            Else
                radHomeTeam.Checked = True
                radAwayTeam.Checked = False
                lblTeamName.Text = radHomeTeam.Text
                If (m_objGameDetails.CoverageLevel = 6) Then
                    txtTime.Text = frmMain.UdcRunningClock1.URCCurrentTime.Replace(":", "").PadLeft(5, CChar(" "))
                End If
            End If
            lblTime.Visible = True
            If m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 3 Then
                txtTimeMod2.Visible = True
                txtTime.Visible = False
            Else
                txtTime.Visible = True
                txtTimeMod2.Visible = False
            End If
            If (m_objGameDetails.CoverageLevel = 6) Then
                txtTime.Enabled = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radHomeTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radHomeTeam.CheckedChanged
        Try
            If radHomeTeam.Checked = True Then
                lblTeamName.Text = radHomeTeam.Text.Trim

                FetchTeamID(radHomeTeam.Text)
                FetchTeamLogo(m_objGameDetails.HomeTeamID)

                LoadSubPlayers()
                RetriveCheckedPlayers()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radAwayTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAwayTeam.CheckedChanged
        Try
            If radAwayTeam.Checked = True Then

                lblTeamName.Text = radAwayTeam.Text.Trim

                FetchTeamID(radAwayTeam.Text)
                FetchTeamLogo(m_intTeamID)

                LoadSubPlayers()

                RetriveCheckedPlayers()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub RetriveCheckedPlayers()
        Try
            If (m_objGameDetails.CoverageLevel = 6) Then
                If m_objGameDetails.IsEditMode = False Then
                    'get the active goal keeper ID
                    Dim drs() As DataRow
                    Dim dsEvent As DataSet = m_objGeneral.GetGoalieChangeEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_intTeamID, m_objGameDetails.CoverageLevel)
                    If dsEvent.Tables(0).Rows.Count > 0 Then
                        drs = m_dsPlayersChecked.Tables(0).Select("TEAM_ID =" & m_intTeamID & " and Player_id = " & CInt(dsEvent.Tables(0).Rows(0).Item("OFFENSIVE_PLAYER_ID")) & " and Starting_position <> 0 and starting_position < 12")
                    Else
                        drs = m_dsPlayersChecked.Tables(0).Select("TEAM_ID =" & m_intTeamID & " and Position_id_1 = 1 and Starting_position <> 0 and starting_position = 1 ")
                    End If

                    If drs.Length > 0 Then
                        For i As Integer = 0 To ChkLstSubs.Items.Count - 1
                            For Each dr As DataRow In drs
                                If DirectCast(ChkLstSubs.Items(i), System.Data.DataRowView).Item(0).ToString = dr.Item("Player_id").ToString Then
                                    ChkLstSubs.SetItemChecked(i, True)
                                End If
                            Next
                        Next
                    End If
                Else
                    Dim drs() As DataRow = m_dsPlayersChecked.Tables(0).Select("TEAM_ID =" & m_objGameDetails.OffensiveTeamID & " and  PLAYER_ID = " & m_objGameDetails.ActiveGK & "")
                    If drs.Length > 0 Then
                        For i As Integer = 0 To ChkLstSubs.Items.Count - 1
                            For Each dr As DataRow In drs
                                If DirectCast(ChkLstSubs.Items(i), System.Data.DataRowView).Item(0).ToString = dr.Item("Player_id").ToString Then
                                    ChkLstSubs.SetItemChecked(i, True)
                                End If
                            Next
                        Next
                    End If
                End If
            Else
                If m_objGameDetails.IsEditMode = False Then
                    'get the active goal keeper ID
                    Dim drs() As DataRow
                    Dim dsEvent As DataSet = m_objGeneral.GetGoalieChangeEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_intTeamID, m_objGameDetails.CoverageLevel)
                    If dsEvent.Tables(0).Rows.Count > 0 Then
                        drs = m_dsPlayersChecked.Tables(GetTableName).Select("TEAM_ID =" & m_intTeamID & " and Player_id = " & CInt(dsEvent.Tables(0).Rows(0).Item("OFFENSIVE_PLAYER_ID")) & " and Starting_position <> 0 and starting_position < 12")
                    Else
                        drs = m_dsPlayersChecked.Tables(GetTableName).Select("TEAM_ID =" & m_intTeamID & " and Position_id_1 = 1 and Starting_position <> 0 and starting_position = 1 ")
                    End If

                    If drs.Length > 0 Then
                        For i As Integer = 0 To ChkLstSubs.Items.Count - 1
                            For Each dr As DataRow In drs
                                If DirectCast(ChkLstSubs.Items(i), System.Data.DataRowView).Item(0).ToString = dr.Item("Player_id").ToString Then
                                    ChkLstSubs.SetItemChecked(i, True)
                                End If
                            Next
                        Next
                    End If
                Else
                    Dim drs() As DataRow = m_dsPlayersChecked.Tables(GetTableName).Select("TEAM_ID =" & m_objGameDetails.OffensiveTeamID & " and  PLAYER_ID = " & m_objGameDetails.ActiveGK & "")
                    If drs.Length > 0 Then
                        For i As Integer = 0 To ChkLstSubs.Items.Count - 1
                            For Each dr As DataRow In drs
                                If DirectCast(ChkLstSubs.Items(i), System.Data.DataRowView).Item(0).ToString = dr.Item("Player_id").ToString Then
                                    ChkLstSubs.SetItemChecked(i, True)
                                End If
                            Next
                        Next
                    End If
                End If
            End If
           

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetTableName() As String
        Try
            If (m_objGameDetails.CoverageLevel = 6) Then
                Return 0
            Else
                Dim drs() As DataRow
                drs = m_dsPlayersChecked.Tables(2).Select("TEAM_ID = " & m_intTeamID & "")
                If drs.Length > 0 Then
                    Return "HomeCurrent"
                Else
                    Return "AwayCurrent"
                End If
            End If
            
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub LoadSubPlayers()
        Try
            Dim drs() As DataRow
            Dim DsSubPlayers As DataSet
            Dim dsTemp As DataSet
            ChkLstSubs.DataSource = Nothing
            ChkLstSubs.Items.Clear()

           

            If (m_objGameDetails.CoverageLevel = 6) Then
                If m_dsPlayersChecked.Tables.Count > 0 Then
                    DsSubPlayers = m_dsPlayersChecked.Copy()
                    dsTemp = DsSubPlayers.Clone()

                    'get only substituted players
                    drs = DsSubPlayers.Tables(0).Select("TEAM_ID = " & m_intTeamID & " AND STARTING_POSITION <> 0 and starting_position < 12")
                    'drs = DsSubPlayers.Tables(intTableCount).Select("TEAM_ID = " & m_objGameDetails.OffensiveTeamID & " AND STARTING_POSITION <> 0 and starting_position < 12")
                    If drs.Length > 0 Then
                        For Each DR As DataRow In drs
                            dsTemp.Tables(0).ImportRow(DR)
                        Next
                        'binding the substituted players
                        ChkLstSubs.DataSource = dsTemp.Tables(0)
                        ChkLstSubs.DisplayMember = dsTemp.Tables(0).Columns("PLAYER_ID").ToString
                        ChkLstSubs.DisplayMember = dsTemp.Tables(0).Columns("PLAYER").ToString
                    End If
                End If
            Else
                If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                    DsSubPlayers = m_objclsTeamSetup.RosterInfo.Copy()
                    dsTemp = DsSubPlayers.Clone()
                    For intTableCount As Integer = 2 To DsSubPlayers.Tables.Count - 1
                        'get only substituted players
                        drs = DsSubPlayers.Tables(intTableCount).Select("TEAM_ID = " & m_intTeamID & " AND STARTING_POSITION <> 0 and starting_position < 12")
                        'drs = DsSubPlayers.Tables(intTableCount).Select("TEAM_ID = " & m_objGameDetails.OffensiveTeamID & " AND STARTING_POSITION <> 0 and starting_position < 12")
                        If drs.Length > 0 Then
                            For Each DR As DataRow In drs
                                dsTemp.Tables(intTableCount).ImportRow(DR)
                            Next
                            'binding the substituted players
                            ChkLstSubs.DataSource = dsTemp.Tables(intTableCount)
                            ChkLstSubs.DisplayMember = dsTemp.Tables(intTableCount).Columns("PLAYER_ID").ToString
                            ChkLstSubs.DisplayMember = dsTemp.Tables(intTableCount).Columns("PLAYER").ToString
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' FETCHING LOGO FOR SELECTED TEAM
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamLogo(ByVal TeamID As Integer)
        Try
            Dim v_memLogo As MemoryStream
            picTeamLogo.Image = Nothing
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, TeamID)
            If v_memLogo IsNot Nothing Then
                picTeamLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else 'TOSOCRS-338
                picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHING TEAM ID BY PASSING TEAM NAME AS PARAMETER
    ''' </summary>
    ''' <param name="Teamname"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamID(ByVal Teamname As String)
        Try
            If Teamname = m_objGameDetails.HomeTeamAbbrev Then
                m_intTeamID = m_objGameDetails.HomeTeamID
            Else
                m_intTeamID = m_objGameDetails.AwayTeamID
            End If

            'If Teamname = m_objGameDetails.HomeTeam Then
            '    m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID
            'Else
            '    m_objGameDetails.OffensiveTeamID = m_objGameDetails.AwayTeamID
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtTime_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTime.Validated
        Try
            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim EditedTime As Integer
            If ChkLstSubs.CheckedItems.Count > 1 Then
                MessageDialog.Show(strmessage2, "Active Gk", MessageDialogButtons.OK, MessageDialogIcon.Question)
                Exit Sub
            ElseIf ChkLstSubs.CheckedItems.Count = 0 Then
                MessageDialog.Show(strmessage3, "Active Gk", MessageDialogButtons.OK, MessageDialogIcon.Question)
                Exit Sub
            End If

            If txtTimeMod2.Visible = True Then
                If txtTimeMod2.Text.Trim = "" Then
                    MessageDialog.Show(strmessage1, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
                EditedTime = CInt(txtTimeMod2.Text) * 60
                m_objGameDetails.CommentsTime = EditedTime
                m_objGameDetails.OffensiveTeamID = m_intTeamID
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTimeMod2.Text, Nothing, 1, 0)
            Else
                If txtTime.Text.Replace(" :", "").Trim = "" Then
                    MessageDialog.Show(strmessage1, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
                m_objGameDetails.CommentsTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text)))
                m_objGameDetails.OffensiveTeamID = m_intTeamID
            End If

            For i As Integer = 0 To ChkLstSubs.CheckedItems.Count - 1
                m_objGameDetails.ActiveGK = DirectCast(ChkLstSubs.CheckedItems(i), System.Data.DataRowView).Item(0)
                Exit For
            Next

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
            'InsertPlayByPlayData()
            'Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next
        End Try
    End Sub
    Private Sub txtTimeMod2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTimeMod2.Validated
        Try
            txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer) As Integer
        Try

            If (m_objGameDetails.IsDefaultGameClock) Then
                If m_objGameDetails.CurrentPeriod = 1 Then
                    Return CurrentElapsedTime
                ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                    Return CurrentElapsedTime - FIRSTHALF
                ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                    Return CurrentElapsedTime - SECONDHALF
                ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                    Return CurrentElapsedTime - FIRSTEXTRA
                End If
            Else
                Return CurrentElapsedTime
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapseAfterMod1(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If m_objGameDetails.ModuleID = 1 Then
                    If CurrPeriod = 1 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                    ElseIf CurrPeriod = 2 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                    ElseIf CurrPeriod = 3 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                    ElseIf CurrPeriod = 4 Then
                        Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                    End If
                Else
                    If CurrPeriod = 1 Then
                        Return CStr(CInt(ElapsedTime / 60))
                    ElseIf CurrPeriod = 2 Then
                        Return CStr(CInt((FIRSTHALF + ElapsedTime) / 60))
                    ElseIf CurrPeriod = 3 Then
                        Return CStr(CInt((SECONDHALF + ElapsedTime) / 60))
                        'clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                    ElseIf CurrPeriod = 4 Then
                        Return CStr(CInt((FIRSTEXTRA + ElapsedTime) / 60))
                        'clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                    End If

                End If
            End If

            If m_objGameDetails.IsDefaultGameClock = False And m_objGameDetails.ModuleID = 2 Then
                Return CStr(CInt(ElapsedTime / 60))
            End If

            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapseAfterMod2(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 0 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 1 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 2 Then
                    Return CStr(CInt((FIRSTHALF + ElapsedTime) / 60))
                ElseIf CurrPeriod = 3 Then
                    Return CStr(CInt((SECONDHALF + ElapsedTime) / 60))
                    'clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return CStr(CInt((FIRSTEXTRA + ElapsedTime) / 60))
                    'clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If

            If m_objGameDetails.IsDefaultGameClock = False And m_objGameDetails.ModuleID = 2 Then
                Return CStr(CInt(ElapsedTime / 60))
            End If

            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class