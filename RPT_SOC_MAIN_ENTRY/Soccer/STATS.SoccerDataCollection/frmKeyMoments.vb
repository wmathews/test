﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : KeyMoments.vb
' Author        : Ravi Krishna G
' Created Date  : 15th June, 09
' Description   : Class is used to handle the KeyMoments Event, which is a Special Event where a popup will be appearing (Me.form) which
'                 handles the Key Moments from a list of PBP Events.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmKeyMoments

#Region "Constants & Variables"
    Private m_dsPBP As New DataSet
    Private m_dsKeyMoments As New DataSet
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objModule1PBP As clsModule1PBP = clsModule1PBP.GetInstance()
    Private m_objKeyMoments As clsKeyMoments = clsKeyMoments.GetInstance()
    Private m_strFrom As String = String.Empty
    Private m_strTo As String = String.Empty
    Private m_intFrom As Integer = -1
    Private m_intTo As Integer = -1
    Private m_FromSeqNo As Decimal
    Private m_ToSeqNo As Decimal
    Private m_intKeyMomentID As Integer
    Private m_lvKeyMoments As ListViewItem

    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private lsupport As New languagesupport
    Private m_dsKeyMomentData As New DataSet
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Dim strmessage As String = "KeyMoment deleted successfully"
    Dim strmessage1 As String = "Are you sure to delete the selected keymoment?"
    Dim strmessage2 As String = "KeyMoments selected in wrong way."
    Dim strmessage3 As String = "KeyMoments start time should be less than end time."
    Dim strmessage4 As String = "KeyMoment inserted successfully"
    Dim strmessage5 As String = "Select events from PBP and click on Create button"
    Dim strmessage6 As String = "KeyMoment updated successfully"
    Dim strmessage7 As String = "Please select the start and end point to create the keyMoment."

    Dim strmessage8 As String = "You are not allowed to Insert/Update Events - Primary Reporter is Down!"
    Dim strmessage9 As String = "Last Entry in Process, Please try again!!..."
#End Region

#Region "Event Handlers"

    ''' <summary>
    ''' Handles the Load Event of the Form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmKeyMoments_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmKeyMoments", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            m_dsPBP = m_objModule1PBP.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            m_dsKeyMomentData = m_objKeyMoments.GetKeymomentsData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            DisplayInListView(m_dsPBP)
            'SHRAVANI - DISPLAYING OF KEYMOMENTS DATA 4TH AUG 2010
            DisplayKeyMoments(m_dsKeyMomentData)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                Label1.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label1.Text)
                Label2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label2.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnClear.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClear.Text)
                btnStart.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnStart.Text)
                btnEnd.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEnd.Text)
                btnCreate.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCreate.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "ID")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "PBPFrom")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "PBPTo")
                lvKeyMoments.Columns(0).Text = g1
                lvKeyMoments.Columns(1).Text = g2
                lvKeyMoments.Columns(2).Text = g3
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Event")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Period")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Time")
                Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Type")
                Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Player")
                Dim g10 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "SubType")
                Dim g11 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "SubSubType")
                Dim g12 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Area")
                Dim g13 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "SequenceNumber")
                Dim g14 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "EventCode_ID")
                Dim g15 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "UniqueID")
                Dim g16 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team_ID")
                lvPBP.Columns(0).Text = g4
                lvPBP.Columns(1).Text = g5
                lvPBP.Columns(2).Text = g6
                lvPBP.Columns(3).Text = g7
                lvPBP.Columns(4).Text = g8
                lvPBP.Columns(6).Text = g9
                lvPBP.Columns(7).Text = g10
                lvPBP.Columns(8).Text = g11
                lvPBP.Columns(9).Text = g12
                lvPBP.Columns(10).Text = g13
                lvPBP.Columns(11).Text = g14
                lvPBP.Columns(12).Text = g15
                'lvPBP.Columns(13).Text = g16
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClear.Text, Nothing, 1, 0)
            If lvKeyMoments.SelectedItems.Count > 0 Then
                lvKeyMoments.Items(lvKeyMoments.SelectedItems(0).Index).Selected = False

                If m_intFrom > -1 Then
                    lvPBP.Items(m_intFrom).Selected = False
                    lvPBP.Items(m_intFrom).BackColor = Color.White
                End If

                If m_intTo > -1 Then
                    lvPBP.Items(m_intTo).BackColor = Color.White
                    lvPBP.Items(m_intTo).Selected = False
                End If

                btnCreate.Text = "Create"
                frmMain.ChangeTheme(False)
                Me.ChangeTheme(False)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click Event of the Close Button on the Screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            m_intFrom = -1
            m_intTo = -1
            m_FromSeqNo = -1
            m_ToSeqNo = -1
            If (lvKeyMoments.SelectedItems.Count > 0) Then
                lvKeyMoments.SelectedItems(0).Selected = False
            End If
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    ''' <summary>
    ''' Handles the Click Event of the Start Button on the Screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>This indicates the Start Event of the KeyMoments</remarks>
    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnStart.Text, Nothing, 1, 0)
            If (lvPBP.SelectedItems.Count > 0) Then
                'Highlighting the Item selected for KeyMoment
                If (lvKeyMoments.SelectedItems.Count > 0) Then
                    m_lvKeyMoments = lvKeyMoments.SelectedItems(0)
                    lvKeyMoments.SelectedItems(0).Selected = False
                End If
                lvPBP.SelectedItems.Item(0).BackColor = Color.LightBlue
                If ((m_intFrom <> -1) And (m_intFrom <> lvPBP.SelectedItems.Item(0).Index)) Then
                    lvPBP.Items(m_intFrom).BackColor = Color.White
                    lvPBP.Items(m_intFrom).Selected = False
                End If
                m_intFrom = lvPBP.SelectedItems.Item(0).Index
                m_FromSeqNo = CDec(lvPBP.SelectedItems(0).SubItems(9).Text)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh.Text, Nothing, 1, 0)
            If m_intFrom > -1 Then
                lvPBP.Items(m_intFrom).Selected = False
                lvPBP.Items(m_intFrom).BackColor = Color.White
            End If

            If m_intTo > -1 Then
                lvPBP.Items(m_intTo).BackColor = Color.White
                lvPBP.Items(m_intTo).Selected = False
            End If

            btnCreate.Text = "Create"

            m_FromSeqNo = -1
            m_ToSeqNo = -1
            m_intKeyMomentID = -1
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)

            m_dsPBP = m_objModule1PBP.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            m_dsKeyMomentData = m_objKeyMoments.GetKeymomentsData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            DisplayInListView(m_dsPBP)
            'SHRAVANI - DISPLAYING OF KEYMOMENTS DATA 4TH AUG 2010
            DisplayKeyMoments(m_dsKeyMomentData)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' Handles the Click Event of the End Button on the Screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>This indicates the End Event of the KeyMoments</remarks>
    Private Sub btnEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnd.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEnd.Text, Nothing, 1, 0)
            If (lvPBP.SelectedItems.Count > 0) Then
                If (lvKeyMoments.SelectedItems.Count > 0) Then
                    m_lvKeyMoments = lvKeyMoments.SelectedItems(0)
                    lvKeyMoments.SelectedItems(0).Selected = False
                End If
                'Highlighting the Item selected for KeyMoment
                lvPBP.SelectedItems.Item(0).BackColor = Color.LightSalmon
                If ((m_intTo <> -1) And (m_intTo <> lvPBP.SelectedItems.Item(0).Index)) Then
                    lvPBP.Items(m_intTo).BackColor = Color.White
                    lvPBP.Items(m_intTo).Selected = False
                End If
                m_intTo = lvPBP.SelectedItems.Item(0).Index
                m_ToSeqNo = CDec(CDec(lvPBP.SelectedItems(0).SubItems(9).Text))

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Function IsPBPEntryAllowed() As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                Return True
            Else
                If m_objGameDetails.IsPrimaryReporterDown Then
                    MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                ElseIf m_objGameDetails.AssistersLastEntryStatus = False Then
                    MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                Else
                    Return True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Handles the Click Event of the Create Button on the Screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>This will create an entry in the KeyMoments List View and also stores the data in the PBP Table</remarks>
    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCreate.Text, Nothing, 1, 0)

            If (m_intTo >= 0 And m_intFrom >= 0) Then
                Dim startSNO As Decimal
                Dim endSNO As Decimal

                startSNO = CDec(lvPBP.Items(m_intFrom).SubItems(9).Text)
                endSNO = CDec(lvPBP.Items(m_intTo).SubItems(9).Text)

                If startSNO > endSNO Then
                    MessageDialog.Show(strmessage2 + " " + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

                    m_FromSeqNo = -1
                    m_ToSeqNo = -1
                    m_intKeyMomentID = -1
                    frmMain.ChangeTheme(False)
                    Me.ChangeTheme(False)
                    Exit Sub
                End If

            End If
            If (btnCreate.Text = "Update") Then
                btnCreate.Text = "Create"
                btnDelete.Enabled = True
                m_dsKeyMoments = UpdateKMToSQL(m_FromSeqNo, m_ToSeqNo, m_intKeyMomentID)

                'update last entry status
                If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                    m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, True)
                    m_objGameDetails.AssistersLastEntryStatus = False
                End If

                If (m_dsKeyMoments.Tables(0).Rows.Count > 0) Then
                    DisplayKeyMoments(m_dsKeyMoments)
                    MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    lvPBP.Items(m_intFrom).BackColor = Color.White
                    lvPBP.Items(m_intTo).BackColor = Color.White
                    lvPBP.Items(m_intFrom).Selected = False
                    lvPBP.Items(m_intTo).Selected = False
                End If
            Else
                'To retrieve an unique Keymoment ID from the Local PBP Table

                If (lvPBP.SelectedItems.Count <= 0) Then
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Else
                    'shirley without start and end point to create a key moments
                    '----------
                    If m_FromSeqNo <= 0 Or m_ToSeqNo <= 0 Then
                        MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_FromSeqNo = -1
                        m_ToSeqNo = -1
                        m_intKeyMomentID = -1
                        If m_intFrom >= 0 Then
                            lvPBP.Items(m_intFrom).BackColor = Color.White
                            lvPBP.Items(m_intFrom).Selected = False
                        End If
                        If m_intTo >= 0 Then
                            lvPBP.Items(m_intTo).BackColor = Color.White
                            lvPBP.Items(m_intTo).Selected = False
                        End If
                        Exit Sub
                    End If
                    '------------
                    m_intKeyMomentID = m_objKeyMoments.GetKeyMomentID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    Dim lvwpbp As New ListViewItem(m_intKeyMomentID.ToString)
                    'To Insert the above retrieved key moment Id to the set of PBP events selected
                    m_dsKeyMoments = UpdateToSQL(m_FromSeqNo, m_ToSeqNo, m_intKeyMomentID)
                    'lvPBP.Items(m_intFrom).SubItems(3).Text
                    'update last entry status
                    If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                        m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, True)
                        m_objGameDetails.AssistersLastEntryStatus = False
                    End If

                    If Not (m_dsKeyMoments Is Nothing) Then
                        If (m_dsKeyMoments.Tables.Count > 0) Then
                            If (m_dsKeyMoments.Tables(0).Rows.Count > 0) Then

                                DisplayKeyMoments(m_dsKeyMoments)
                                lvPBP.Items(m_intFrom).BackColor = Color.White
                                lvPBP.Items(m_intTo).BackColor = Color.White
                                lvPBP.Items(m_intFrom).Selected = False
                                lvPBP.Items(m_intTo).Selected = False
                                MessageDialog.Show(strmessage4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Else
                                MessageDialog.Show(strmessage2 + " " + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                If m_intFrom >= 0 Then
                                    lvPBP.Items(m_intFrom).BackColor = Color.White
                                    'lvPBP.Items(m_intTo).BackColor = Color.White
                                    lvPBP.Items(m_intFrom).Selected = False
                                End If
                                If m_intTo >= 0 Then
                                    'lvPBP.Items(m_intFrom).BackColor = Color.White
                                    lvPBP.Items(m_intTo).BackColor = Color.White
                                    lvPBP.Items(m_intTo).Selected = False
                                End If

                            End If

                        End If
                    End If

                End If
            End If

            m_FromSeqNo = -1
            m_ToSeqNo = -1
            m_intKeyMomentID = -1
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvKeyMoments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvKeyMoments.Click
        Try
            If btnCreate.Text = "Update" Then
                btnEdit.Enabled = True
            Else
                btnDelete.Enabled = True
                btnEdit.Enabled = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Selection Index Changed Event of the KeyMoments List Box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lvKeyMoments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvKeyMoments.SelectedIndexChanged
        Try
            'Dim i As Integer = 0
            'If (lvKeyMoments.SelectedItems.Count > 0) Then
            '    If (m_intFrom <> -1) Then
            '        lvPBP.Items(m_intFrom).BackColor = Color.White
            '    End If
            '    If (m_intTo <> -1) Then
            '        lvPBP.Items(m_intTo).BackColor = Color.White
            '    End If
            '    m_FromSeqNo = CDec(lvKeyMoments.SelectedItems(0).SubItems(3).Text)
            '    m_ToSeqNo = CDec(lvKeyMoments.SelectedItems(0).SubItems(4).Text)
            '    m_intKeyMomentID = CInt(lvKeyMoments.SelectedItems(0).Text)
            '    'Clears the Highlighted rows based on the From/To Unique ID
            '    For i = 0 To lvPBP.Items.Count - 1
            '        If (lvPBP.Items(i).SubItems.Count >= 11) Then
            '            If CDec(lvPBP.Items(i).SubItems(9).Text) = m_FromSeqNo Then
            '                m_intFrom = i
            '                lvPBP.Items(i).BackColor = Color.LightBlue
            '            End If
            '            If CDec(lvPBP.Items(i).SubItems(9).Text) = m_ToSeqNo Then
            '                m_intTo = i
            '                lvPBP.Items(i).BackColor = Color.LightSalmon
            '            End If
            '        End If
            '    Next
            '    If ((lvKeyMoments.SelectedItems.Count > 0) And (m_intFrom <> -1)) Then
            '        lvPBP.EnsureVisible(m_intFrom)
            '    End If
            'End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click Event of the Delete Button on the Screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>To Delete the KeyMomentID of the associated PBP Events in the PBP Table</remarks>
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, Nothing, 1, 0)
            If (lvKeyMoments.SelectedItems.Count > 0) Then

                If IsPBPEntryAllowed() = False Then
                    Exit Try
                End If

                markEditedComments()
                If MessageDialog.Show(strmessage1, "Key Moments", MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                    Dim intCount As Integer
                    intCount = m_objKeyMoments.DeleteKeyMoments(m_FromSeqNo, m_ToSeqNo, m_intKeyMomentID, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)

                    'Delete and create key moment causing duplicate Goal.
                    'update last entry status
                    If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                        m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, True)
                        m_objGameDetails.AssistersLastEntryStatus = False
                    End If

                    If (intCount >= 0) Then
                        lvKeyMoments.SelectedItems(0).Remove()
                        DisplayKeyMoments(m_objKeyMoments.GetKeymomentsData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber))
                        'Clears the Highlighted rows based on the From/To Unique ID
                        For i = 0 To lvPBP.Items.Count - 1
                            If (lvPBP.Items(i).SubItems.Count >= 11) Then
                                If CDec(lvPBP.Items(i).SubItems(9).Text) = m_FromSeqNo Then
                                    m_intFrom = i
                                    lvPBP.Items(i).BackColor = Color.White
                                    lvPBP.Items(i).Selected = False
                                End If
                                If CDec(lvPBP.Items(i).SubItems(9).Text) = m_ToSeqNo Then
                                    m_intTo = i
                                    lvPBP.Items(i).BackColor = Color.White
                                    lvPBP.Items(i).Selected = False
                                End If
                            End If
                        Next
                        MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        'm_intTo = -1
                        'm_intFrom = -1
                    End If
                End If
                frmMain.ChangeTheme(False)
                Me.ChangeTheme(False)
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click Event of the Edit Button on the Screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>To Edit the KeyMomentID of the associated PBP Events in the PBP Table</remarks>
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, Nothing, 1, 0)
            If (lvKeyMoments.SelectedItems.Count > 0) Then
                If IsPBPEntryAllowed() = False Then
                    Exit Try
                End If

                markEditedComments()
                lvKeyMoments.Items(lvKeyMoments.SelectedItems(0).Index).Selected = True
                btnCreate.Text = "Update"
                btnDelete.Enabled = False
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)
                lvKeyMoments.Items(lvKeyMoments.SelectedItems(0).Index).Selected = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region "User Functions"

    ''' <summary>
    ''' Displays the List of PBP Events from the Local SQL Database to the List View
    ''' </summary>
    ''' <param name="PBP"></param>
    ''' <remarks></remarks>
    '''
    Public Sub DisplayList()
        Try
            Dim dsPBP As DataSet

            dsPBP = m_objModule1PBP.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            DisplayInListView(dsPBP)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub DisplayInListView(ByVal PBP As DataSet)

        Try
            lvPBP.Items.Clear()
            Dim intRowCount As Integer
            Dim EventID As Integer
            Dim CurrPeriod As Integer
            Dim ElapsedTime As Integer
            Dim StrTime As String = "00:00"

            If Not PBP Is Nothing Then
                If (PBP.Tables(0).Rows.Count > 0) Then
                    For intRowCount = 0 To PBP.Tables(0).Rows.Count - 1
                        If CInt(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) <> clsGameDetails.TIME Then
                            EventID = CInt(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                            CurrPeriod = CInt(PBP.Tables(0).Rows(intRowCount).Item("PERIOD"))

                            If EventID = clsGameDetails.LEFT_FLANK_ATTACK Or EventID = clsGameDetails.RIGHT_FLANK_ATTACK Or EventID = clsGameDetails.CENTRAL_ATTACK Or EventID = clsGameDetails.LONG_BALL_ATTACK Then
                                Dim lvwpbp As New ListViewItem("Goal Threat")
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    'CONVERTING TO MINUTES
                                    ''
                                    If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                        'CONVERTING TO MINUTES
                                        ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                        'StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                        StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                    End If
                                    ''
                                    'StrTime = clsUtility.ConvertSecondToMinute(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED").ToString), False)
                                End If
                                lvwpbp.SubItems.Add(StrTime)

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_DESC").ToString) '42
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString) '4
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("GOAL_THREAT_DESC").ToString) '5
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("DEFENSIVE_PLAYER").ToString)
                                lvwpbp.SubItems.Add("")

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvPBP.Items.Add(lvwpbp)

                            ElseIf EventID = clsGameDetails.ONTARGET Then
                                Dim lvwpbp As New ListViewItem("On Target")
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    'CONVERTING TO MINUTES
                                    ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                    'StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                    StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                End If
                                lvwpbp.SubItems.Add(StrTime)

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_TYPE").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_RESULT").ToString)
                                lvwpbp.SubItems.Add("")

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvPBP.Items.Add(lvwpbp)
                            ElseIf EventID = clsGameDetails.PASS Or EventID = clsGameDetails.THROW_IN Or EventID = clsGameDetails.DEFLECTION Or EventID = clsGameDetails.TACKLE Or EventID = clsGameDetails.RUN_WITH_BALL Then
                                Dim lvwpbp As New ListViewItem("Key Moment")
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    'CONVERTING TO MINUTES
                                    ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                    'StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                    StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                End If
                                lvwpbp.SubItems.Add(StrTime)

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_DESC").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                lvwpbp.SubItems.Add("")
                                lvwpbp.SubItems.Add("")
                                lvwpbp.SubItems.Add("")

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvPBP.Items.Add(lvwpbp)
                            ElseIf EventID = clsGameDetails.OFFTARGET Then
                                Dim lvwpbp As New ListViewItem("Off Target")
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    'CONVERTING TO MINUTES
                                    ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                    'StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                    StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                End If
                                lvwpbp.SubItems.Add(StrTime)

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_TYPE").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_RESULT").ToString)
                                lvwpbp.SubItems.Add("")

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvPBP.Items.Add(lvwpbp)
                            ElseIf EventID = clsGameDetails.YELLOWCARD Or EventID = clsGameDetails.REDCARD Then

                                Dim lvwpbp As New ListViewItem("Booking")
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    'CONVERTING TO MINUTES
                                    ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                    'StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                    StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                End If
                                lvwpbp.SubItems.Add(StrTime)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("BOOKING_TYPE").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("BOOKING_REASON").ToString)
                                lvwpbp.SubItems.Add("")
                                lvwpbp.SubItems.Add("")

                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvPBP.Items.Add(lvwpbp)

                            Else
                                Dim lvwpbp As New ListViewItem(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_DESC").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    'CONVERTING TO MINUTES
                                    ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                    'StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                    StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                End If
                                lvwpbp.SubItems.Add(StrTime)

                                Select Case EventID

                                    Case clsGameDetails.GOALIE_CHANGE

                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.MISSED_PENALTY

                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_TYPE").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("DEFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_RESULT").ToString)

                                    Case clsGameDetails.MANAGEREXPULSION
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("BOOKING_REASON").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                    Case clsGameDetails.GAMESTART, clsGameDetails.STARTPERIOD, clsGameDetails.ENDPERIOD, clsGameDetails.ENDGAME, clsGameDetails.HomeStartingLineups, clsGameDetails.AwatStartingLineups

                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                    Case clsGameDetails.GOAL_KICK
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                    Case clsGameDetails.YD10
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        'BACKPASS
                                    Case clsGameDetails.Backpass
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)

                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        If Not IsDBNull(PBP.Tables(0).Rows(intRowCount).Item("AREA")) Then
                                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("AREA").ToString)
                                        Else
                                            lvwpbp.SubItems.Add("")
                                            'lvwpbp.UseItemStyleForSubItems = False
                                            'lvwpbp.SubItems(8).BackColor = Color.Red
                                        End If
                                    Case clsGameDetails.COMMENTS
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("COMMENTS").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                    Case clsGameDetails.DEFEN_ACTION
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("DEF_ACTION_TYPE").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("DEF_ACTION_DESC").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("DEF_ACTION_RESULT").ToString)
                                        lvwpbp.SubItems.Add("")
                                    Case clsGameDetails.FREEKICK
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("FK_RESULT").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                    Case clsGameDetails.CORNER, clsGameDetails.CROSS

                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("CORNER_CROSS_TYPE").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("CORNER_CROSS_DESC").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("CORNER_CROSS_RES").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("CORNER_CROSS_ZONE").ToString)

                                    Case clsGameDetails.GOAL, clsGameDetails.PENALTY

                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_TYPE").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("GOALZONE").ToString)
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.OWNGOAL

                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_TYPE").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("GOALZONE").ToString)
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.FOUL

                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("FOUL_TYPE").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("FOUL_RESULT").ToString)
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.OFFSIDE

                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFSIDE_DESC").ToString)
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFSIDE_RESULT").ToString)
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.SUBSTITUTE

                                        lvwpbp.SubItems.Add("") '3
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString) '4
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PLAYER_OUT").ToString) '5
                                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SUB_REASON").ToString)
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.ABANDONED, clsGameDetails.DELAYED
                                        If PBP.Tables(0).Rows(intRowCount).Item("COMMENTS") IsNot DBNull.Value Then
                                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("COMMENTS").ToString)
                                        Else
                                            lvwpbp.SubItems.Add("")
                                        End If
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")
                                        lvwpbp.SubItems.Add("")

                                    Case clsGameDetails.SHOOTOUT_MISSED, clsGameDetails.SHOOTOUT_SAVE, clsGameDetails.SHOOTOUT_GOAL
                                        If CInt(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = clsGameDetails.SHOOTOUT_MISSED Then
                                            lvwpbp.SubItems.Add("Missed")
                                        ElseIf CInt(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = clsGameDetails.SHOOTOUT_GOAL Then
                                            lvwpbp.SubItems.Add("Scored")
                                        Else
                                            lvwpbp.SubItems.Add("Saved")
                                        End If
                                        If PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER") IsNot DBNull.Value Then
                                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                                        Else
                                            lvwpbp.SubItems.Add("")
                                        End If
                                        If PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC") IsNot DBNull.Value Then
                                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_DESC").ToString)
                                        Else
                                            lvwpbp.SubItems.Add("")
                                        End If
                                        If PBP.Tables(0).Rows(intRowCount).Item("SHOT_RESULT") IsNot DBNull.Value Then
                                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SHOT_RESULT").ToString)
                                        Else
                                            lvwpbp.SubItems.Add("")
                                        End If
                                End Select
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvPBP.Items.Add(lvwpbp)
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To update the list of PBP Events with the KeyMomentID to identify the Sequence of KeyMoments
    ''' </summary>
    ''' <param name="FromUniqueID">An Integer Value representing the UniqueID of the PBP Event from which the KeyMoment has started</param>
    ''' <param name="ToUniqueID">An Integer Value representing the UniqueID of the PBP Event to which the KeyMoment has ended</param>
    ''' <param name="KeyMomentID">An Unique Integer Value representing the KeyMomentID</param>
    ''' <remarks></remarks>
    Private Function UpdateToSQL(ByVal FromUniqueID As Decimal, ByVal ToUniqueID As Decimal, ByVal KeyMomentID As Integer) As DataSet
        Try
            'Inserting the KeyMomentID to the associated PBP Events in the PBP Table
            Return m_objKeyMoments.InsertKeyMomentsToPBP(FromUniqueID, ToUniqueID, KeyMomentID, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To update the KeyMoment
    ''' </summary>
    ''' <param name="FromUniqueID">An Integer Value representing the UniqueID of the PBP Event from which the KeyMoment has started</param>
    ''' <param name="ToUniqueID">An Integer Value representing the UniqueID of the PBP Event to which the KeyMoment has ended</param>
    ''' <param name="KeyMomentID">An Unique Integer Value representing the KeyMomentID</param>
    ''' <remarks></remarks>
    Private Function UpdateKMToSQL(ByVal FromUniqueID As Decimal, ByVal ToUniqueID As Decimal, ByVal KeyMomentID As Integer) As DataSet
        Try
            'Updating the KeyMomentID to the associated PBP Events in the PBP Table
            Return m_objKeyMoments.UpdateKeyMomentsToPBP(FromUniqueID, ToUniqueID, KeyMomentID, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            Dim StrTime As String = "00:00"
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        StrTime = "45+" + CStr(DiffTime)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        StrTime = "90+" + CStr(DiffTime)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 3 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        StrTime = "105+" + CStr(DiffTime)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 4 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        StrTime = "120+" + CStr(DiffTime)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 5 Then
                    StrTime = "120"
                End If
                'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
            Else
                If CurrPeriod = 1 Or CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        StrTime = "45+" + CStr(DiffTime)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

                    End If
                Else
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        StrTime = "15+" + CStr(DiffTime)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                End If
            End If
            Return StrTime
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ChangeTheme(ByVal EditMode As Boolean)
        Try
            If EditMode Then
                Me.BackColor = Color.LightGoldenrodYellow
                picReporterBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                picButtonBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
            Else
                Me.BackColor = Color.WhiteSmoke
                picReporterBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                picButtonBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub markEditedComments()
        Try
            Dim i As Integer = 0
            If (lvKeyMoments.SelectedItems.Count > 0) Then
                If (m_intFrom <> -1) Then
                    lvPBP.Items(m_intFrom).BackColor = Color.White
                    'lvPBP.Items(m_intTo).BackColor = Color.White
                    lvPBP.Items(m_intFrom).Selected = False
                End If
                If (m_intTo <> -1) Then
                    'lvPBP.Items(m_intFrom).BackColor = Color.White
                    lvPBP.Items(m_intTo).BackColor = Color.White
                    lvPBP.Items(m_intTo).Selected = False
                End If
                m_FromSeqNo = CDec(lvKeyMoments.SelectedItems(0).SubItems(3).Text)
                m_ToSeqNo = CDec(lvKeyMoments.SelectedItems(0).SubItems(4).Text)
                m_intKeyMomentID = CInt(lvKeyMoments.SelectedItems(0).Text)
                'Clears the Highlighted rows based on the From/To Unique ID
                For i = 0 To lvPBP.Items.Count - 1
                    If (lvPBP.Items(i).SubItems.Count >= 11) Then
                        If CDec(lvPBP.Items(i).SubItems(9).Text) = m_FromSeqNo Then
                            m_intFrom = i
                            lvPBP.Items(i).BackColor = Color.LightBlue
                        End If
                        If CDec(lvPBP.Items(i).SubItems(9).Text) = m_ToSeqNo Then
                            m_intTo = i
                            lvPBP.Items(i).BackColor = Color.LightSalmon
                        End If
                    End If
                Next
                If ((lvKeyMoments.SelectedItems.Count > 0) And (m_intFrom <> -1)) Then
                    lvPBP.EnsureVisible(m_intFrom)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    'SHRAVANI - DISPLAYING OF KEYMOMENTS DATA 4TH AUG 2010
    ''' <summary>
    ''' DISPLAYS THE KEYMOMENTS DATA (CALLED AFTER INSERTION,DELETION AND UPDATION OF KEYMOMENTS)
    ''' </summary>
    ''' <param name="KeyMomentsData">Dataset whcich contains keymoment data</param>
    ''' <remarks></remarks>
    Public Sub DisplayKeyMoments(ByVal KeyMomentsData As DataSet)
        Try
            lvKeyMoments.Items.Clear()
            Dim strFrom, strTo As String
            Dim FromSeqNo As Decimal
            Dim ToSeqNo As Decimal
            If Not KeyMomentsData Is Nothing Then
                If KeyMomentsData.Tables(0).Rows.Count > 0 Then
                    For intRowCount = 0 To KeyMomentsData.Tables(0).Rows.Count - 1
                        Dim lvKM As New ListViewItem(KeyMomentsData.Tables(0).Rows(intRowCount).Item("KEY_MOMENT_ID").ToString)
                        strFrom = clsUtility.ConvertSecondToMinute(Convert.ToDouble(KeyMomentsData.Tables(0).Rows(intRowCount).Item("MIN_TIME_ELAPSED")), False) & " - " & KeyMomentsData.Tables(0).Rows(intRowCount).Item("MIN_EVENT_DESC").ToString
                        lvKM.SubItems.Add(strFrom)
                        strTo = clsUtility.ConvertSecondToMinute(Convert.ToDouble(KeyMomentsData.Tables(0).Rows(intRowCount).Item("MAX_TIME_ELAPSED")), False) & " - " & KeyMomentsData.Tables(0).Rows(intRowCount).Item("MAX_EVENT_DESC").ToString
                        lvKM.SubItems.Add(strTo)
                        FromSeqNo = CDec(KeyMomentsData.Tables(0).Rows(intRowCount).Item("MIN_UNIQUE_ID"))
                        lvKM.SubItems.Add(FromSeqNo.ToString)
                        ToSeqNo = CDec(KeyMomentsData.Tables(0).Rows(intRowCount).Item("MAX_UNIQUE_ID"))
                        lvKM.SubItems.Add(ToSeqNo.ToString())
                        lvKeyMoments.Items.Add(lvKM)
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

End Class