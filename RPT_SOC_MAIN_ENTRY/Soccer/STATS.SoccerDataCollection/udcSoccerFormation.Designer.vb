﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class udcSoccerFormation
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstPlayers = New System.Windows.Forms.ListBox
        Me.btnApply = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.pnlField = New System.Windows.Forms.Panel
        Me.SuspendLayout()
        '
        'lstPlayers
        '
        Me.lstPlayers.FormattingEnabled = True
        Me.lstPlayers.ItemHeight = 15
        Me.lstPlayers.Location = New System.Drawing.Point(559, 10)
        Me.lstPlayers.Name = "lstPlayers"
        Me.lstPlayers.Size = New System.Drawing.Size(155, 229)
        Me.lstPlayers.TabIndex = 1
        '
        'btnApply
        '
        Me.btnApply.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.btnApply.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(566, 248)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(69, 24)
        Me.btnApply.TabIndex = 3
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.btnReset.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(641, 248)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(69, 24)
        Me.btnReset.TabIndex = 4
        Me.btnReset.Text = "Reset"
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'pnlField
        '
        Me.pnlField.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnlField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlField.Location = New System.Drawing.Point(-1, -1)
        Me.pnlField.Name = "pnlField"
        Me.pnlField.Size = New System.Drawing.Size(551, 282)
        Me.pnlField.TabIndex = 5
        '
        'udcSoccerFormation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Wheat
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.pnlField)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.lstPlayers)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "udcSoccerFormation"
        Me.Size = New System.Drawing.Size(723, 280)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstPlayers As System.Windows.Forms.ListBox
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents pnlField As System.Windows.Forms.Panel

End Class
