﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTouches
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnVisitPlayer11 = New System.Windows.Forms.Button
        Me.btnVisitPlayer6 = New System.Windows.Forms.Button
        Me.btnVisitPlayer7 = New System.Windows.Forms.Button
        Me.btnVisitPlayer8 = New System.Windows.Forms.Button
        Me.btnVisitPlayer9 = New System.Windows.Forms.Button
        Me.btnVisitPlayer10 = New System.Windows.Forms.Button
        Me.btnVisitPlayer1 = New System.Windows.Forms.Button
        Me.btnVisitPlayer2 = New System.Windows.Forms.Button
        Me.btnVisitPlayer3 = New System.Windows.Forms.Button
        Me.btnVisitPlayer4 = New System.Windows.Forms.Button
        Me.btnVisitPlayer5 = New System.Windows.Forms.Button
        Me.btnHomePlayer11 = New System.Windows.Forms.Button
        Me.btnHomePlayer6 = New System.Windows.Forms.Button
        Me.btnHomePlayer7 = New System.Windows.Forms.Button
        Me.btnHomePlayer8 = New System.Windows.Forms.Button
        Me.btnHomePlayer9 = New System.Windows.Forms.Button
        Me.btnHomePlayer10 = New System.Windows.Forms.Button
        Me.btnHomePlayer1 = New System.Windows.Forms.Button
        Me.btnHomePlayer2 = New System.Windows.Forms.Button
        Me.btnHomePlayer3 = New System.Windows.Forms.Button
        Me.btnHomePlayer4 = New System.Windows.Forms.Button
        Me.btnHomePlayer5 = New System.Windows.Forms.Button
        Me.slbReporterEntry = New System.Windows.Forms.ToolStripStatusLabel
        Me.pnlVisitBench = New System.Windows.Forms.Panel
        Me.btnVisitSub28 = New System.Windows.Forms.Button
        Me.btnVisitSub29 = New System.Windows.Forms.Button
        Me.btnVisitSub30 = New System.Windows.Forms.Button
        Me.btnVisitSub25 = New System.Windows.Forms.Button
        Me.btnVisitSub26 = New System.Windows.Forms.Button
        Me.btnVisitSub27 = New System.Windows.Forms.Button
        Me.btnVisitSub22 = New System.Windows.Forms.Button
        Me.btnVisitSub23 = New System.Windows.Forms.Button
        Me.btnVisitSub24 = New System.Windows.Forms.Button
        Me.btnVisitSub19 = New System.Windows.Forms.Button
        Me.btnVisitSub20 = New System.Windows.Forms.Button
        Me.btnVisitSub21 = New System.Windows.Forms.Button
        Me.btnVisitSub10 = New System.Windows.Forms.Button
        Me.btnVisitSub11 = New System.Windows.Forms.Button
        Me.btnVisitSub12 = New System.Windows.Forms.Button
        Me.btnVisitSub16 = New System.Windows.Forms.Button
        Me.btnVisitSub17 = New System.Windows.Forms.Button
        Me.btnVisitSub18 = New System.Windows.Forms.Button
        Me.btnVisitSub13 = New System.Windows.Forms.Button
        Me.btnVisitSub14 = New System.Windows.Forms.Button
        Me.btnVisitSub15 = New System.Windows.Forms.Button
        Me.btnVisitSub7 = New System.Windows.Forms.Button
        Me.btnVisitSub8 = New System.Windows.Forms.Button
        Me.btnVisitSub9 = New System.Windows.Forms.Button
        Me.btnVisitSub4 = New System.Windows.Forms.Button
        Me.btnVisitSub5 = New System.Windows.Forms.Button
        Me.btnVisitSub6 = New System.Windows.Forms.Button
        Me.btnVisitSub1 = New System.Windows.Forms.Button
        Me.btnVisitSub2 = New System.Windows.Forms.Button
        Me.btnVisitSub3 = New System.Windows.Forms.Button
        Me.pnlHomeBench = New System.Windows.Forms.Panel
        Me.btnHomeSub28 = New System.Windows.Forms.Button
        Me.btnHomeSub29 = New System.Windows.Forms.Button
        Me.btnHomeSub30 = New System.Windows.Forms.Button
        Me.btnHomeSub25 = New System.Windows.Forms.Button
        Me.btnHomeSub26 = New System.Windows.Forms.Button
        Me.btnHomeSub27 = New System.Windows.Forms.Button
        Me.btnHomeSub22 = New System.Windows.Forms.Button
        Me.btnHomeSub23 = New System.Windows.Forms.Button
        Me.btnHomeSub24 = New System.Windows.Forms.Button
        Me.btnHomeSub19 = New System.Windows.Forms.Button
        Me.btnHomeSub20 = New System.Windows.Forms.Button
        Me.btnHomeSub21 = New System.Windows.Forms.Button
        Me.btnHomeSub16 = New System.Windows.Forms.Button
        Me.btnHomeSub17 = New System.Windows.Forms.Button
        Me.btnHomeSub18 = New System.Windows.Forms.Button
        Me.btnHomeSub13 = New System.Windows.Forms.Button
        Me.btnHomeSub14 = New System.Windows.Forms.Button
        Me.btnHomeSub15 = New System.Windows.Forms.Button
        Me.btnHomeSub10 = New System.Windows.Forms.Button
        Me.btnHomeSub11 = New System.Windows.Forms.Button
        Me.btnHomeSub12 = New System.Windows.Forms.Button
        Me.btnHomeSub7 = New System.Windows.Forms.Button
        Me.btnHomeSub8 = New System.Windows.Forms.Button
        Me.btnHomeSub9 = New System.Windows.Forms.Button
        Me.btnHomeSub4 = New System.Windows.Forms.Button
        Me.btnHomeSub5 = New System.Windows.Forms.Button
        Me.btnHomeSub6 = New System.Windows.Forms.Button
        Me.btnHomeSub1 = New System.Windows.Forms.Button
        Me.btnHomeSub2 = New System.Windows.Forms.Button
        Me.btnHomeSub3 = New System.Windows.Forms.Button
        Me.lblTouchType = New System.Windows.Forms.Label
        Me.pnlTouchTypes = New System.Windows.Forms.Panel
        Me.BtnKpPickUp = New System.Windows.Forms.Button
        Me.btnGKHand = New System.Windows.Forms.Button
        Me.btnGKThrow = New System.Windows.Forms.Button
        Me.btn50Win = New System.Windows.Forms.Button
        Me.btnAirWin = New System.Windows.Forms.Button
        Me.BtnIntBPass = New System.Windows.Forms.Button
        Me.BtnIntPass = New System.Windows.Forms.Button
        Me.btnTypeIntPass = New System.Windows.Forms.Button
        Me.btnTypeRunWithBall = New System.Windows.Forms.Button
        Me.btnTypeGoalKick = New System.Windows.Forms.Button
        Me.btnTypeTouch = New System.Windows.Forms.Button
        Me.btnTypeGoalAttempt = New System.Windows.Forms.Button
        Me.btnTypeClearance = New System.Windows.Forms.Button
        Me.btnTypeThrowIn = New System.Windows.Forms.Button
        Me.btnTypeOther = New System.Windows.Forms.Button
        Me.btnTypeTackle = New System.Windows.Forms.Button
        Me.btnTypeCatch = New System.Windows.Forms.Button
        Me.btnTypeSave = New System.Windows.Forms.Button
        Me.btnTypeBlock = New System.Windows.Forms.Button
        Me.btnTypeInt = New System.Windows.Forms.Button
        Me.btnTypePass = New System.Windows.Forms.Button
        Me.pnlHomePrimary = New System.Windows.Forms.Panel
        Me.btnRunWithBallHome = New System.Windows.Forms.Button
        Me.btnHomePrimary = New System.Windows.Forms.Button
        Me.btn50Home = New System.Windows.Forms.Button
        Me.btnAerialHome = New System.Windows.Forms.Button
        Me.pnlTouchDisplay = New System.Windows.Forms.Panel
        Me.lblDispLocation2 = New System.Windows.Forms.Label
        Me.lblLocationVal2 = New System.Windows.Forms.Label
        Me.lblValLocation = New System.Windows.Forms.Label
        Me.lblValUniform = New System.Windows.Forms.Label
        Me.lblValType = New System.Windows.Forms.Label
        Me.lblValTeam = New System.Windows.Forms.Label
        Me.lblDispLocation = New System.Windows.Forms.Label
        Me.lblDispType = New System.Windows.Forms.Label
        Me.lblUniform = New System.Windows.Forms.Label
        Me.lblTeam = New System.Windows.Forms.Label
        Me.chkEnableVoice = New System.Windows.Forms.CheckBox
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.cmnTouches = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.lnkRefreshPlayers = New System.Windows.Forms.LinkLabel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblSelection2 = New System.Windows.Forms.Label
        Me.picMark2 = New System.Windows.Forms.PictureBox
        Me.picMark1 = New System.Windows.Forms.PictureBox
        Me.lblSelection1 = New System.Windows.Forms.Label
        Me.lblFieldY = New System.Windows.Forms.Label
        Me.lblFieldX = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.tltpTouches = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblTime = New System.Windows.Forms.Label
        Me.pnlHome = New System.Windows.Forms.Panel
        Me.btnHomePlayer38 = New System.Windows.Forms.Button
        Me.btnHomePlayer37 = New System.Windows.Forms.Button
        Me.btnHomePlayer36 = New System.Windows.Forms.Button
        Me.btnHomePlayer35 = New System.Windows.Forms.Button
        Me.btnHomePlayer34 = New System.Windows.Forms.Button
        Me.btnHomePlayer41 = New System.Windows.Forms.Button
        Me.btnHomePlayer40 = New System.Windows.Forms.Button
        Me.btnHomePlayer39 = New System.Windows.Forms.Button
        Me.btnHomePlayer27 = New System.Windows.Forms.Button
        Me.btnHomePlayer26 = New System.Windows.Forms.Button
        Me.btnHomePlayer25 = New System.Windows.Forms.Button
        Me.btnHomePlayer24 = New System.Windows.Forms.Button
        Me.btnHomePlayer23 = New System.Windows.Forms.Button
        Me.btnHomePlayer32 = New System.Windows.Forms.Button
        Me.btnHomePlayer31 = New System.Windows.Forms.Button
        Me.btnHomePlayer30 = New System.Windows.Forms.Button
        Me.btnHomePlayer29 = New System.Windows.Forms.Button
        Me.btnHomePlayer28 = New System.Windows.Forms.Button
        Me.btnHomePlayer33 = New System.Windows.Forms.Button
        Me.btnHomePlayer16 = New System.Windows.Forms.Button
        Me.btnHomePlayer15 = New System.Windows.Forms.Button
        Me.btnHomePlayer14 = New System.Windows.Forms.Button
        Me.btnHomePlayer13 = New System.Windows.Forms.Button
        Me.btnHomePlayer12 = New System.Windows.Forms.Button
        Me.btnHomePlayer21 = New System.Windows.Forms.Button
        Me.btnHomePlayer20 = New System.Windows.Forms.Button
        Me.btnHomePlayer19 = New System.Windows.Forms.Button
        Me.btnHomePlayer18 = New System.Windows.Forms.Button
        Me.btnHomePlayer17 = New System.Windows.Forms.Button
        Me.btnHomePlayer22 = New System.Windows.Forms.Button
        Me.pnlVisit = New System.Windows.Forms.Panel
        Me.btnVisitPlayer38 = New System.Windows.Forms.Button
        Me.btnVisitPlayer37 = New System.Windows.Forms.Button
        Me.btnVisitPlayer36 = New System.Windows.Forms.Button
        Me.btnVisitPlayer35 = New System.Windows.Forms.Button
        Me.btnVisitPlayer34 = New System.Windows.Forms.Button
        Me.btnVisitPlayer41 = New System.Windows.Forms.Button
        Me.btnVisitPlayer40 = New System.Windows.Forms.Button
        Me.btnVisitPlayer39 = New System.Windows.Forms.Button
        Me.btnVisitPlayer27 = New System.Windows.Forms.Button
        Me.btnVisitPlayer26 = New System.Windows.Forms.Button
        Me.btnVisitPlayer25 = New System.Windows.Forms.Button
        Me.btnVisitPlayer24 = New System.Windows.Forms.Button
        Me.btnVisitPlayer23 = New System.Windows.Forms.Button
        Me.btnVisitPlayer32 = New System.Windows.Forms.Button
        Me.btnVisitPlayer31 = New System.Windows.Forms.Button
        Me.btnVisitPlayer30 = New System.Windows.Forms.Button
        Me.btnVisitPlayer29 = New System.Windows.Forms.Button
        Me.btnVisitPlayer28 = New System.Windows.Forms.Button
        Me.btnVisitPlayer33 = New System.Windows.Forms.Button
        Me.btnVisitPlayer16 = New System.Windows.Forms.Button
        Me.btnVisitPlayer15 = New System.Windows.Forms.Button
        Me.btnVisitPlayer14 = New System.Windows.Forms.Button
        Me.btnVisitPlayer13 = New System.Windows.Forms.Button
        Me.btnVisitPlayer12 = New System.Windows.Forms.Button
        Me.btnVisitPlayer21 = New System.Windows.Forms.Button
        Me.btnVisitPlayer20 = New System.Windows.Forms.Button
        Me.btnVisitPlayer19 = New System.Windows.Forms.Button
        Me.btnVisitPlayer18 = New System.Windows.Forms.Button
        Me.btnVisitPlayer17 = New System.Windows.Forms.Button
        Me.btnVisitPlayer22 = New System.Windows.Forms.Button
        Me.TmrTouches = New System.Windows.Forms.Timer(Me.components)
        Me.txtTime = New System.Windows.Forms.MaskedTextBox
        Me.ChkAllData = New System.Windows.Forms.CheckBox
        Me.txtCommand = New System.Windows.Forms.TextBox
        Me.btnRefreshTime = New System.Windows.Forms.Button
        Me.pnlInstructions = New System.Windows.Forms.Panel
        Me.lblInstructions = New System.Windows.Forms.Label
        Me.pnlVisitPrimary = New System.Windows.Forms.Panel
        Me.btnRunWithBallVisit = New System.Windows.Forms.Button
        Me.btn50Visit = New System.Windows.Forms.Button
        Me.btnVisitPrimary = New System.Windows.Forms.Button
        Me.btnAerialVisit = New System.Windows.Forms.Button
        Me.pnlTeamHighlight = New System.Windows.Forms.Panel
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cmbPeriod = New System.Windows.Forms.ComboBox
        Me.chkOwnTouches = New System.Windows.Forms.CheckBox
        Me.chkRevisit = New System.Windows.Forms.CheckBox
        Me.dgvTouches = New System.Windows.Forms.DataGridView
        Me.Revisit = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Perid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Time = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Uniform = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Player = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Location = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sequence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Uid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TeamId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UdcSoccerField1 = New STATS.SoccerDataCollection.udcSoccerField
        Me.pnlVisitBench.SuspendLayout()
        Me.pnlHomeBench.SuspendLayout()
        Me.pnlTouchTypes.SuspendLayout()
        Me.pnlHomePrimary.SuspendLayout()
        Me.pnlTouchDisplay.SuspendLayout()
        Me.cmnTouches.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.picMark2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMark1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHome.SuspendLayout()
        Me.pnlVisit.SuspendLayout()
        Me.pnlInstructions.SuspendLayout()
        Me.pnlVisitPrimary.SuspendLayout()
        CType(Me.dgvTouches, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnVisitPlayer11
        '
        Me.btnVisitPlayer11.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer11.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer11.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer11.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer11.Location = New System.Drawing.Point(0, 270)
        Me.btnVisitPlayer11.Name = "btnVisitPlayer11"
        Me.btnVisitPlayer11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer11.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer11.TabIndex = 11
        Me.btnVisitPlayer11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer11.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer6
        '
        Me.btnVisitPlayer6.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer6.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer6.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer6.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer6.Location = New System.Drawing.Point(0, 135)
        Me.btnVisitPlayer6.Name = "btnVisitPlayer6"
        Me.btnVisitPlayer6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer6.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer6.TabIndex = 6
        Me.btnVisitPlayer6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer6.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer7
        '
        Me.btnVisitPlayer7.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer7.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer7.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer7.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer7.Location = New System.Drawing.Point(0, 162)
        Me.btnVisitPlayer7.Name = "btnVisitPlayer7"
        Me.btnVisitPlayer7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer7.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer7.TabIndex = 7
        Me.btnVisitPlayer7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer7.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer8
        '
        Me.btnVisitPlayer8.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer8.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer8.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer8.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer8.Location = New System.Drawing.Point(0, 189)
        Me.btnVisitPlayer8.Name = "btnVisitPlayer8"
        Me.btnVisitPlayer8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer8.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer8.TabIndex = 8
        Me.btnVisitPlayer8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer8.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer9
        '
        Me.btnVisitPlayer9.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer9.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer9.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer9.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer9.Location = New System.Drawing.Point(0, 216)
        Me.btnVisitPlayer9.Name = "btnVisitPlayer9"
        Me.btnVisitPlayer9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer9.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer9.TabIndex = 9
        Me.btnVisitPlayer9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer9.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer10
        '
        Me.btnVisitPlayer10.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer10.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer10.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer10.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer10.Location = New System.Drawing.Point(0, 243)
        Me.btnVisitPlayer10.Name = "btnVisitPlayer10"
        Me.btnVisitPlayer10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer10.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer10.TabIndex = 10
        Me.btnVisitPlayer10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer10.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer1
        '
        Me.btnVisitPlayer1.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer1.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer1.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer1.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer1.Location = New System.Drawing.Point(0, 0)
        Me.btnVisitPlayer1.Name = "btnVisitPlayer1"
        Me.btnVisitPlayer1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer1.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer1.TabIndex = 1
        Me.btnVisitPlayer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer1.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer2
        '
        Me.btnVisitPlayer2.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer2.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer2.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer2.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer2.Location = New System.Drawing.Point(0, 27)
        Me.btnVisitPlayer2.Name = "btnVisitPlayer2"
        Me.btnVisitPlayer2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer2.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer2.TabIndex = 2
        Me.btnVisitPlayer2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer2.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer3
        '
        Me.btnVisitPlayer3.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer3.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer3.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer3.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer3.Location = New System.Drawing.Point(0, 54)
        Me.btnVisitPlayer3.Name = "btnVisitPlayer3"
        Me.btnVisitPlayer3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer3.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer3.TabIndex = 3
        Me.btnVisitPlayer3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer3.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer4
        '
        Me.btnVisitPlayer4.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer4.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer4.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer4.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer4.Location = New System.Drawing.Point(0, 81)
        Me.btnVisitPlayer4.Name = "btnVisitPlayer4"
        Me.btnVisitPlayer4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer4.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer4.TabIndex = 4
        Me.btnVisitPlayer4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer4.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer5
        '
        Me.btnVisitPlayer5.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer5.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer5.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer5.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer5.Location = New System.Drawing.Point(0, 108)
        Me.btnVisitPlayer5.Name = "btnVisitPlayer5"
        Me.btnVisitPlayer5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer5.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer5.TabIndex = 5
        Me.btnVisitPlayer5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer5.UseVisualStyleBackColor = False
        '
        'btnHomePlayer11
        '
        Me.btnHomePlayer11.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer11.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer11.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer11.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer11.Location = New System.Drawing.Point(0, 270)
        Me.btnHomePlayer11.Name = "btnHomePlayer11"
        Me.btnHomePlayer11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer11.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer11.TabIndex = 53
        Me.btnHomePlayer11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer11.UseVisualStyleBackColor = False
        '
        'btnHomePlayer6
        '
        Me.btnHomePlayer6.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer6.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer6.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer6.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer6.Location = New System.Drawing.Point(0, 135)
        Me.btnHomePlayer6.Name = "btnHomePlayer6"
        Me.btnHomePlayer6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer6.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer6.TabIndex = 48
        Me.btnHomePlayer6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer6.UseVisualStyleBackColor = False
        '
        'btnHomePlayer7
        '
        Me.btnHomePlayer7.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer7.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer7.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer7.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer7.Location = New System.Drawing.Point(0, 162)
        Me.btnHomePlayer7.Name = "btnHomePlayer7"
        Me.btnHomePlayer7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer7.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer7.TabIndex = 49
        Me.btnHomePlayer7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer7.UseVisualStyleBackColor = False
        '
        'btnHomePlayer8
        '
        Me.btnHomePlayer8.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer8.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer8.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer8.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer8.Location = New System.Drawing.Point(0, 189)
        Me.btnHomePlayer8.Name = "btnHomePlayer8"
        Me.btnHomePlayer8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer8.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer8.TabIndex = 50
        Me.btnHomePlayer8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer8.UseVisualStyleBackColor = False
        '
        'btnHomePlayer9
        '
        Me.btnHomePlayer9.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer9.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer9.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer9.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer9.Location = New System.Drawing.Point(0, 216)
        Me.btnHomePlayer9.Name = "btnHomePlayer9"
        Me.btnHomePlayer9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer9.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer9.TabIndex = 51
        Me.btnHomePlayer9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer9.UseVisualStyleBackColor = False
        '
        'btnHomePlayer10
        '
        Me.btnHomePlayer10.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer10.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer10.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer10.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer10.Location = New System.Drawing.Point(0, 243)
        Me.btnHomePlayer10.Name = "btnHomePlayer10"
        Me.btnHomePlayer10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer10.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer10.TabIndex = 52
        Me.btnHomePlayer10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer10.UseVisualStyleBackColor = False
        '
        'btnHomePlayer1
        '
        Me.btnHomePlayer1.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer1.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer1.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer1.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer1.Location = New System.Drawing.Point(0, 0)
        Me.btnHomePlayer1.Name = "btnHomePlayer1"
        Me.btnHomePlayer1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer1.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer1.TabIndex = 43
        Me.btnHomePlayer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer1.UseVisualStyleBackColor = False
        '
        'btnHomePlayer2
        '
        Me.btnHomePlayer2.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer2.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer2.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer2.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer2.Location = New System.Drawing.Point(0, 27)
        Me.btnHomePlayer2.Name = "btnHomePlayer2"
        Me.btnHomePlayer2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer2.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer2.TabIndex = 44
        Me.btnHomePlayer2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer2.UseVisualStyleBackColor = False
        '
        'btnHomePlayer3
        '
        Me.btnHomePlayer3.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer3.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer3.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer3.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer3.Location = New System.Drawing.Point(0, 54)
        Me.btnHomePlayer3.Name = "btnHomePlayer3"
        Me.btnHomePlayer3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer3.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer3.TabIndex = 45
        Me.btnHomePlayer3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer3.UseVisualStyleBackColor = False
        '
        'btnHomePlayer4
        '
        Me.btnHomePlayer4.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer4.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer4.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer4.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer4.Location = New System.Drawing.Point(0, 81)
        Me.btnHomePlayer4.Name = "btnHomePlayer4"
        Me.btnHomePlayer4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer4.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer4.TabIndex = 46
        Me.btnHomePlayer4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer4.UseVisualStyleBackColor = False
        '
        'btnHomePlayer5
        '
        Me.btnHomePlayer5.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer5.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer5.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer5.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer5.Location = New System.Drawing.Point(0, 108)
        Me.btnHomePlayer5.Name = "btnHomePlayer5"
        Me.btnHomePlayer5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer5.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer5.TabIndex = 47
        Me.btnHomePlayer5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer5.UseVisualStyleBackColor = False
        '
        'slbReporterEntry
        '
        Me.slbReporterEntry.BackColor = System.Drawing.Color.Transparent
        Me.slbReporterEntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slbReporterEntry.ForeColor = System.Drawing.Color.White
        Me.slbReporterEntry.Margin = New System.Windows.Forms.Padding(0)
        Me.slbReporterEntry.Name = "slbReporterEntry"
        Me.slbReporterEntry.Size = New System.Drawing.Size(0, 0)
        Me.slbReporterEntry.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.slbReporterEntry.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay
        '
        'pnlVisitBench
        '
        Me.pnlVisitBench.AutoScroll = True
        Me.pnlVisitBench.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub28)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub29)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub30)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub25)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub26)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub27)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub22)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub23)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub24)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub19)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub20)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub21)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub10)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub11)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub12)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub16)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub17)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub18)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub13)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub14)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub15)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub7)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub8)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub9)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub4)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub5)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub6)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub1)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub2)
        Me.pnlVisitBench.Controls.Add(Me.btnVisitSub3)
        Me.pnlVisitBench.Location = New System.Drawing.Point(22, 316)
        Me.pnlVisitBench.Name = "pnlVisitBench"
        Me.pnlVisitBench.Size = New System.Drawing.Size(278, 103)
        Me.pnlVisitBench.TabIndex = 180
        '
        'btnVisitSub28
        '
        Me.btnVisitSub28.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub28.FlatAppearance.BorderSize = 0
        Me.btnVisitSub28.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub28.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub28.Location = New System.Drawing.Point(1, 307)
        Me.btnVisitSub28.Name = "btnVisitSub28"
        Me.btnVisitSub28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub28.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub28.TabIndex = 39
        Me.btnVisitSub28.UseVisualStyleBackColor = False
        Me.btnVisitSub28.Visible = False
        '
        'btnVisitSub29
        '
        Me.btnVisitSub29.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub29.FlatAppearance.BorderSize = 0
        Me.btnVisitSub29.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub29.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub29.Location = New System.Drawing.Point(89, 307)
        Me.btnVisitSub29.Name = "btnVisitSub29"
        Me.btnVisitSub29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub29.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub29.TabIndex = 40
        Me.btnVisitSub29.UseVisualStyleBackColor = False
        Me.btnVisitSub29.Visible = False
        '
        'btnVisitSub30
        '
        Me.btnVisitSub30.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub30.FlatAppearance.BorderSize = 0
        Me.btnVisitSub30.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub30.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub30.Location = New System.Drawing.Point(176, 307)
        Me.btnVisitSub30.Name = "btnVisitSub30"
        Me.btnVisitSub30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub30.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub30.TabIndex = 41
        Me.btnVisitSub30.UseVisualStyleBackColor = False
        Me.btnVisitSub30.Visible = False
        '
        'btnVisitSub25
        '
        Me.btnVisitSub25.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub25.FlatAppearance.BorderSize = 0
        Me.btnVisitSub25.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub25.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub25.Location = New System.Drawing.Point(1, 273)
        Me.btnVisitSub25.Name = "btnVisitSub25"
        Me.btnVisitSub25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub25.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub25.TabIndex = 36
        Me.btnVisitSub25.UseVisualStyleBackColor = False
        Me.btnVisitSub25.Visible = False
        '
        'btnVisitSub26
        '
        Me.btnVisitSub26.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub26.FlatAppearance.BorderSize = 0
        Me.btnVisitSub26.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub26.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub26.Location = New System.Drawing.Point(89, 273)
        Me.btnVisitSub26.Name = "btnVisitSub26"
        Me.btnVisitSub26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub26.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub26.TabIndex = 37
        Me.btnVisitSub26.UseVisualStyleBackColor = False
        Me.btnVisitSub26.Visible = False
        '
        'btnVisitSub27
        '
        Me.btnVisitSub27.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub27.FlatAppearance.BorderSize = 0
        Me.btnVisitSub27.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub27.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub27.Location = New System.Drawing.Point(176, 273)
        Me.btnVisitSub27.Name = "btnVisitSub27"
        Me.btnVisitSub27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub27.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub27.TabIndex = 38
        Me.btnVisitSub27.UseVisualStyleBackColor = False
        Me.btnVisitSub27.Visible = False
        '
        'btnVisitSub22
        '
        Me.btnVisitSub22.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub22.FlatAppearance.BorderSize = 0
        Me.btnVisitSub22.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub22.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub22.Location = New System.Drawing.Point(1, 239)
        Me.btnVisitSub22.Name = "btnVisitSub22"
        Me.btnVisitSub22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub22.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub22.TabIndex = 33
        Me.btnVisitSub22.UseVisualStyleBackColor = False
        Me.btnVisitSub22.Visible = False
        '
        'btnVisitSub23
        '
        Me.btnVisitSub23.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub23.FlatAppearance.BorderSize = 0
        Me.btnVisitSub23.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub23.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub23.Location = New System.Drawing.Point(89, 239)
        Me.btnVisitSub23.Name = "btnVisitSub23"
        Me.btnVisitSub23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub23.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub23.TabIndex = 34
        Me.btnVisitSub23.UseVisualStyleBackColor = False
        Me.btnVisitSub23.Visible = False
        '
        'btnVisitSub24
        '
        Me.btnVisitSub24.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub24.FlatAppearance.BorderSize = 0
        Me.btnVisitSub24.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub24.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub24.Location = New System.Drawing.Point(176, 239)
        Me.btnVisitSub24.Name = "btnVisitSub24"
        Me.btnVisitSub24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub24.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub24.TabIndex = 35
        Me.btnVisitSub24.UseVisualStyleBackColor = False
        Me.btnVisitSub24.Visible = False
        '
        'btnVisitSub19
        '
        Me.btnVisitSub19.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub19.FlatAppearance.BorderSize = 0
        Me.btnVisitSub19.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub19.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub19.Location = New System.Drawing.Point(1, 205)
        Me.btnVisitSub19.Name = "btnVisitSub19"
        Me.btnVisitSub19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub19.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub19.TabIndex = 30
        Me.btnVisitSub19.UseVisualStyleBackColor = False
        Me.btnVisitSub19.Visible = False
        '
        'btnVisitSub20
        '
        Me.btnVisitSub20.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub20.FlatAppearance.BorderSize = 0
        Me.btnVisitSub20.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub20.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub20.Location = New System.Drawing.Point(89, 205)
        Me.btnVisitSub20.Name = "btnVisitSub20"
        Me.btnVisitSub20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub20.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub20.TabIndex = 31
        Me.btnVisitSub20.UseVisualStyleBackColor = False
        Me.btnVisitSub20.Visible = False
        '
        'btnVisitSub21
        '
        Me.btnVisitSub21.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub21.FlatAppearance.BorderSize = 0
        Me.btnVisitSub21.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub21.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub21.Location = New System.Drawing.Point(176, 205)
        Me.btnVisitSub21.Name = "btnVisitSub21"
        Me.btnVisitSub21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub21.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub21.TabIndex = 32
        Me.btnVisitSub21.UseVisualStyleBackColor = False
        Me.btnVisitSub21.Visible = False
        '
        'btnVisitSub10
        '
        Me.btnVisitSub10.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub10.FlatAppearance.BorderSize = 0
        Me.btnVisitSub10.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub10.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub10.Location = New System.Drawing.Point(1, 103)
        Me.btnVisitSub10.Name = "btnVisitSub10"
        Me.btnVisitSub10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub10.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub10.TabIndex = 21
        Me.btnVisitSub10.UseVisualStyleBackColor = False
        Me.btnVisitSub10.Visible = False
        '
        'btnVisitSub11
        '
        Me.btnVisitSub11.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub11.FlatAppearance.BorderSize = 0
        Me.btnVisitSub11.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub11.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub11.Location = New System.Drawing.Point(89, 103)
        Me.btnVisitSub11.Name = "btnVisitSub11"
        Me.btnVisitSub11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub11.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub11.TabIndex = 22
        Me.btnVisitSub11.UseVisualStyleBackColor = False
        Me.btnVisitSub11.Visible = False
        '
        'btnVisitSub12
        '
        Me.btnVisitSub12.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub12.FlatAppearance.BorderSize = 0
        Me.btnVisitSub12.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub12.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub12.Location = New System.Drawing.Point(176, 103)
        Me.btnVisitSub12.Name = "btnVisitSub12"
        Me.btnVisitSub12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub12.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub12.TabIndex = 23
        Me.btnVisitSub12.UseVisualStyleBackColor = False
        Me.btnVisitSub12.Visible = False
        '
        'btnVisitSub16
        '
        Me.btnVisitSub16.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub16.FlatAppearance.BorderSize = 0
        Me.btnVisitSub16.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub16.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub16.Location = New System.Drawing.Point(1, 171)
        Me.btnVisitSub16.Name = "btnVisitSub16"
        Me.btnVisitSub16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub16.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub16.TabIndex = 27
        Me.btnVisitSub16.UseVisualStyleBackColor = False
        Me.btnVisitSub16.Visible = False
        '
        'btnVisitSub17
        '
        Me.btnVisitSub17.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub17.FlatAppearance.BorderSize = 0
        Me.btnVisitSub17.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub17.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub17.Location = New System.Drawing.Point(89, 171)
        Me.btnVisitSub17.Name = "btnVisitSub17"
        Me.btnVisitSub17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub17.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub17.TabIndex = 28
        Me.btnVisitSub17.UseVisualStyleBackColor = False
        Me.btnVisitSub17.Visible = False
        '
        'btnVisitSub18
        '
        Me.btnVisitSub18.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub18.FlatAppearance.BorderSize = 0
        Me.btnVisitSub18.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub18.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub18.Location = New System.Drawing.Point(176, 171)
        Me.btnVisitSub18.Name = "btnVisitSub18"
        Me.btnVisitSub18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub18.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub18.TabIndex = 29
        Me.btnVisitSub18.UseVisualStyleBackColor = False
        Me.btnVisitSub18.Visible = False
        '
        'btnVisitSub13
        '
        Me.btnVisitSub13.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub13.FlatAppearance.BorderSize = 0
        Me.btnVisitSub13.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub13.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub13.Location = New System.Drawing.Point(1, 137)
        Me.btnVisitSub13.Name = "btnVisitSub13"
        Me.btnVisitSub13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub13.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub13.TabIndex = 24
        Me.btnVisitSub13.UseVisualStyleBackColor = False
        Me.btnVisitSub13.Visible = False
        '
        'btnVisitSub14
        '
        Me.btnVisitSub14.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub14.FlatAppearance.BorderSize = 0
        Me.btnVisitSub14.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub14.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub14.Location = New System.Drawing.Point(89, 137)
        Me.btnVisitSub14.Name = "btnVisitSub14"
        Me.btnVisitSub14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub14.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub14.TabIndex = 25
        Me.btnVisitSub14.UseVisualStyleBackColor = False
        Me.btnVisitSub14.Visible = False
        '
        'btnVisitSub15
        '
        Me.btnVisitSub15.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub15.FlatAppearance.BorderSize = 0
        Me.btnVisitSub15.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub15.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub15.Location = New System.Drawing.Point(176, 137)
        Me.btnVisitSub15.Name = "btnVisitSub15"
        Me.btnVisitSub15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub15.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub15.TabIndex = 26
        Me.btnVisitSub15.UseVisualStyleBackColor = False
        Me.btnVisitSub15.Visible = False
        '
        'btnVisitSub7
        '
        Me.btnVisitSub7.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub7.FlatAppearance.BorderSize = 0
        Me.btnVisitSub7.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub7.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub7.Location = New System.Drawing.Point(1, 69)
        Me.btnVisitSub7.Name = "btnVisitSub7"
        Me.btnVisitSub7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub7.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub7.TabIndex = 18
        Me.btnVisitSub7.UseVisualStyleBackColor = False
        '
        'btnVisitSub8
        '
        Me.btnVisitSub8.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub8.FlatAppearance.BorderSize = 0
        Me.btnVisitSub8.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub8.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub8.Location = New System.Drawing.Point(89, 69)
        Me.btnVisitSub8.Name = "btnVisitSub8"
        Me.btnVisitSub8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub8.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub8.TabIndex = 19
        Me.btnVisitSub8.UseVisualStyleBackColor = False
        '
        'btnVisitSub9
        '
        Me.btnVisitSub9.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub9.FlatAppearance.BorderSize = 0
        Me.btnVisitSub9.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub9.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub9.Location = New System.Drawing.Point(176, 69)
        Me.btnVisitSub9.Name = "btnVisitSub9"
        Me.btnVisitSub9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub9.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub9.TabIndex = 20
        Me.btnVisitSub9.UseVisualStyleBackColor = False
        '
        'btnVisitSub4
        '
        Me.btnVisitSub4.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub4.FlatAppearance.BorderSize = 0
        Me.btnVisitSub4.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub4.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub4.Location = New System.Drawing.Point(1, 35)
        Me.btnVisitSub4.Name = "btnVisitSub4"
        Me.btnVisitSub4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub4.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub4.TabIndex = 15
        Me.btnVisitSub4.UseVisualStyleBackColor = False
        '
        'btnVisitSub5
        '
        Me.btnVisitSub5.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub5.FlatAppearance.BorderSize = 0
        Me.btnVisitSub5.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub5.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub5.Location = New System.Drawing.Point(89, 35)
        Me.btnVisitSub5.Name = "btnVisitSub5"
        Me.btnVisitSub5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub5.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub5.TabIndex = 16
        Me.btnVisitSub5.UseVisualStyleBackColor = False
        '
        'btnVisitSub6
        '
        Me.btnVisitSub6.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub6.FlatAppearance.BorderSize = 0
        Me.btnVisitSub6.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub6.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub6.Location = New System.Drawing.Point(176, 35)
        Me.btnVisitSub6.Name = "btnVisitSub6"
        Me.btnVisitSub6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub6.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub6.TabIndex = 17
        Me.btnVisitSub6.UseVisualStyleBackColor = False
        '
        'btnVisitSub1
        '
        Me.btnVisitSub1.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub1.FlatAppearance.BorderSize = 0
        Me.btnVisitSub1.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub1.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub1.Location = New System.Drawing.Point(1, 1)
        Me.btnVisitSub1.Name = "btnVisitSub1"
        Me.btnVisitSub1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub1.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub1.TabIndex = 12
        Me.btnVisitSub1.UseVisualStyleBackColor = False
        '
        'btnVisitSub2
        '
        Me.btnVisitSub2.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub2.FlatAppearance.BorderSize = 0
        Me.btnVisitSub2.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub2.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub2.Location = New System.Drawing.Point(89, 1)
        Me.btnVisitSub2.Name = "btnVisitSub2"
        Me.btnVisitSub2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub2.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub2.TabIndex = 13
        Me.btnVisitSub2.UseVisualStyleBackColor = False
        '
        'btnVisitSub3
        '
        Me.btnVisitSub3.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitSub3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitSub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVisitSub3.FlatAppearance.BorderSize = 0
        Me.btnVisitSub3.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitSub3.ForeColor = System.Drawing.Color.Black
        Me.btnVisitSub3.Location = New System.Drawing.Point(176, 1)
        Me.btnVisitSub3.Name = "btnVisitSub3"
        Me.btnVisitSub3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitSub3.Size = New System.Drawing.Size(85, 35)
        Me.btnVisitSub3.TabIndex = 14
        Me.btnVisitSub3.UseVisualStyleBackColor = False
        '
        'pnlHomeBench
        '
        Me.pnlHomeBench.AutoScroll = True
        Me.pnlHomeBench.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub28)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub29)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub30)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub25)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub26)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub27)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub22)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub23)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub24)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub19)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub20)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub21)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub16)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub17)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub18)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub13)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub14)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub15)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub10)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub11)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub12)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub7)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub8)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub9)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub4)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub5)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub6)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub1)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub2)
        Me.pnlHomeBench.Controls.Add(Me.btnHomeSub3)
        Me.pnlHomeBench.Location = New System.Drawing.Point(22, 316)
        Me.pnlHomeBench.Name = "pnlHomeBench"
        Me.pnlHomeBench.Size = New System.Drawing.Size(278, 103)
        Me.pnlHomeBench.TabIndex = 181
        '
        'btnHomeSub28
        '
        Me.btnHomeSub28.BackColor = System.Drawing.Color.White
        Me.btnHomeSub28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub28.FlatAppearance.BorderSize = 0
        Me.btnHomeSub28.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub28.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub28.Location = New System.Drawing.Point(1, 307)
        Me.btnHomeSub28.Name = "btnHomeSub28"
        Me.btnHomeSub28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub28.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub28.TabIndex = 81
        Me.btnHomeSub28.UseVisualStyleBackColor = False
        Me.btnHomeSub28.Visible = False
        '
        'btnHomeSub29
        '
        Me.btnHomeSub29.BackColor = System.Drawing.Color.White
        Me.btnHomeSub29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub29.FlatAppearance.BorderSize = 0
        Me.btnHomeSub29.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub29.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub29.Location = New System.Drawing.Point(89, 307)
        Me.btnHomeSub29.Name = "btnHomeSub29"
        Me.btnHomeSub29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub29.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub29.TabIndex = 82
        Me.btnHomeSub29.UseVisualStyleBackColor = False
        Me.btnHomeSub29.Visible = False
        '
        'btnHomeSub30
        '
        Me.btnHomeSub30.BackColor = System.Drawing.Color.White
        Me.btnHomeSub30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub30.FlatAppearance.BorderSize = 0
        Me.btnHomeSub30.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub30.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub30.Location = New System.Drawing.Point(176, 307)
        Me.btnHomeSub30.Name = "btnHomeSub30"
        Me.btnHomeSub30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub30.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub30.TabIndex = 83
        Me.btnHomeSub30.UseVisualStyleBackColor = False
        Me.btnHomeSub30.Visible = False
        '
        'btnHomeSub25
        '
        Me.btnHomeSub25.BackColor = System.Drawing.Color.White
        Me.btnHomeSub25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub25.FlatAppearance.BorderSize = 0
        Me.btnHomeSub25.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub25.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub25.Location = New System.Drawing.Point(1, 273)
        Me.btnHomeSub25.Name = "btnHomeSub25"
        Me.btnHomeSub25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub25.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub25.TabIndex = 78
        Me.btnHomeSub25.UseVisualStyleBackColor = False
        Me.btnHomeSub25.Visible = False
        '
        'btnHomeSub26
        '
        Me.btnHomeSub26.BackColor = System.Drawing.Color.White
        Me.btnHomeSub26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub26.FlatAppearance.BorderSize = 0
        Me.btnHomeSub26.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub26.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub26.Location = New System.Drawing.Point(89, 273)
        Me.btnHomeSub26.Name = "btnHomeSub26"
        Me.btnHomeSub26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub26.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub26.TabIndex = 79
        Me.btnHomeSub26.UseVisualStyleBackColor = False
        Me.btnHomeSub26.Visible = False
        '
        'btnHomeSub27
        '
        Me.btnHomeSub27.BackColor = System.Drawing.Color.White
        Me.btnHomeSub27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub27.FlatAppearance.BorderSize = 0
        Me.btnHomeSub27.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub27.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub27.Location = New System.Drawing.Point(176, 273)
        Me.btnHomeSub27.Name = "btnHomeSub27"
        Me.btnHomeSub27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub27.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub27.TabIndex = 80
        Me.btnHomeSub27.UseVisualStyleBackColor = False
        Me.btnHomeSub27.Visible = False
        '
        'btnHomeSub22
        '
        Me.btnHomeSub22.BackColor = System.Drawing.Color.White
        Me.btnHomeSub22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub22.FlatAppearance.BorderSize = 0
        Me.btnHomeSub22.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub22.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub22.Location = New System.Drawing.Point(1, 239)
        Me.btnHomeSub22.Name = "btnHomeSub22"
        Me.btnHomeSub22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub22.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub22.TabIndex = 75
        Me.btnHomeSub22.UseVisualStyleBackColor = False
        Me.btnHomeSub22.Visible = False
        '
        'btnHomeSub23
        '
        Me.btnHomeSub23.BackColor = System.Drawing.Color.White
        Me.btnHomeSub23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub23.FlatAppearance.BorderSize = 0
        Me.btnHomeSub23.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub23.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub23.Location = New System.Drawing.Point(89, 239)
        Me.btnHomeSub23.Name = "btnHomeSub23"
        Me.btnHomeSub23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub23.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub23.TabIndex = 76
        Me.btnHomeSub23.UseVisualStyleBackColor = False
        Me.btnHomeSub23.Visible = False
        '
        'btnHomeSub24
        '
        Me.btnHomeSub24.BackColor = System.Drawing.Color.White
        Me.btnHomeSub24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub24.FlatAppearance.BorderSize = 0
        Me.btnHomeSub24.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub24.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub24.Location = New System.Drawing.Point(176, 239)
        Me.btnHomeSub24.Name = "btnHomeSub24"
        Me.btnHomeSub24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub24.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub24.TabIndex = 77
        Me.btnHomeSub24.UseVisualStyleBackColor = False
        Me.btnHomeSub24.Visible = False
        '
        'btnHomeSub19
        '
        Me.btnHomeSub19.BackColor = System.Drawing.Color.White
        Me.btnHomeSub19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub19.FlatAppearance.BorderSize = 0
        Me.btnHomeSub19.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub19.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub19.Location = New System.Drawing.Point(1, 205)
        Me.btnHomeSub19.Name = "btnHomeSub19"
        Me.btnHomeSub19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub19.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub19.TabIndex = 72
        Me.btnHomeSub19.UseVisualStyleBackColor = False
        Me.btnHomeSub19.Visible = False
        '
        'btnHomeSub20
        '
        Me.btnHomeSub20.BackColor = System.Drawing.Color.White
        Me.btnHomeSub20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub20.FlatAppearance.BorderSize = 0
        Me.btnHomeSub20.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub20.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub20.Location = New System.Drawing.Point(89, 205)
        Me.btnHomeSub20.Name = "btnHomeSub20"
        Me.btnHomeSub20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub20.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub20.TabIndex = 73
        Me.btnHomeSub20.UseVisualStyleBackColor = False
        Me.btnHomeSub20.Visible = False
        '
        'btnHomeSub21
        '
        Me.btnHomeSub21.BackColor = System.Drawing.Color.White
        Me.btnHomeSub21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub21.FlatAppearance.BorderSize = 0
        Me.btnHomeSub21.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub21.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub21.Location = New System.Drawing.Point(176, 205)
        Me.btnHomeSub21.Name = "btnHomeSub21"
        Me.btnHomeSub21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub21.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub21.TabIndex = 74
        Me.btnHomeSub21.UseVisualStyleBackColor = False
        Me.btnHomeSub21.Visible = False
        '
        'btnHomeSub16
        '
        Me.btnHomeSub16.BackColor = System.Drawing.Color.White
        Me.btnHomeSub16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub16.FlatAppearance.BorderSize = 0
        Me.btnHomeSub16.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub16.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub16.Location = New System.Drawing.Point(1, 171)
        Me.btnHomeSub16.Name = "btnHomeSub16"
        Me.btnHomeSub16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub16.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub16.TabIndex = 69
        Me.btnHomeSub16.UseVisualStyleBackColor = False
        Me.btnHomeSub16.Visible = False
        '
        'btnHomeSub17
        '
        Me.btnHomeSub17.BackColor = System.Drawing.Color.White
        Me.btnHomeSub17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub17.FlatAppearance.BorderSize = 0
        Me.btnHomeSub17.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub17.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub17.Location = New System.Drawing.Point(90, 171)
        Me.btnHomeSub17.Name = "btnHomeSub17"
        Me.btnHomeSub17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub17.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub17.TabIndex = 70
        Me.btnHomeSub17.UseVisualStyleBackColor = False
        Me.btnHomeSub17.Visible = False
        '
        'btnHomeSub18
        '
        Me.btnHomeSub18.BackColor = System.Drawing.Color.White
        Me.btnHomeSub18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub18.FlatAppearance.BorderSize = 0
        Me.btnHomeSub18.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub18.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub18.Location = New System.Drawing.Point(176, 171)
        Me.btnHomeSub18.Name = "btnHomeSub18"
        Me.btnHomeSub18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub18.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub18.TabIndex = 71
        Me.btnHomeSub18.UseVisualStyleBackColor = False
        Me.btnHomeSub18.Visible = False
        '
        'btnHomeSub13
        '
        Me.btnHomeSub13.BackColor = System.Drawing.Color.White
        Me.btnHomeSub13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub13.FlatAppearance.BorderSize = 0
        Me.btnHomeSub13.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub13.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub13.Location = New System.Drawing.Point(1, 137)
        Me.btnHomeSub13.Name = "btnHomeSub13"
        Me.btnHomeSub13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub13.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub13.TabIndex = 66
        Me.btnHomeSub13.UseVisualStyleBackColor = False
        Me.btnHomeSub13.Visible = False
        '
        'btnHomeSub14
        '
        Me.btnHomeSub14.BackColor = System.Drawing.Color.White
        Me.btnHomeSub14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub14.FlatAppearance.BorderSize = 0
        Me.btnHomeSub14.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub14.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub14.Location = New System.Drawing.Point(90, 137)
        Me.btnHomeSub14.Name = "btnHomeSub14"
        Me.btnHomeSub14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub14.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub14.TabIndex = 67
        Me.btnHomeSub14.UseVisualStyleBackColor = False
        Me.btnHomeSub14.Visible = False
        '
        'btnHomeSub15
        '
        Me.btnHomeSub15.BackColor = System.Drawing.Color.White
        Me.btnHomeSub15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub15.FlatAppearance.BorderSize = 0
        Me.btnHomeSub15.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub15.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub15.Location = New System.Drawing.Point(176, 137)
        Me.btnHomeSub15.Name = "btnHomeSub15"
        Me.btnHomeSub15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub15.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub15.TabIndex = 68
        Me.btnHomeSub15.UseVisualStyleBackColor = False
        Me.btnHomeSub15.Visible = False
        '
        'btnHomeSub10
        '
        Me.btnHomeSub10.BackColor = System.Drawing.Color.White
        Me.btnHomeSub10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub10.FlatAppearance.BorderSize = 0
        Me.btnHomeSub10.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub10.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub10.Location = New System.Drawing.Point(1, 103)
        Me.btnHomeSub10.Name = "btnHomeSub10"
        Me.btnHomeSub10.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub10.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub10.TabIndex = 63
        Me.btnHomeSub10.UseVisualStyleBackColor = False
        Me.btnHomeSub10.Visible = False
        '
        'btnHomeSub11
        '
        Me.btnHomeSub11.BackColor = System.Drawing.Color.White
        Me.btnHomeSub11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub11.FlatAppearance.BorderSize = 0
        Me.btnHomeSub11.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub11.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub11.Location = New System.Drawing.Point(90, 103)
        Me.btnHomeSub11.Name = "btnHomeSub11"
        Me.btnHomeSub11.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub11.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub11.TabIndex = 64
        Me.btnHomeSub11.UseVisualStyleBackColor = False
        Me.btnHomeSub11.Visible = False
        '
        'btnHomeSub12
        '
        Me.btnHomeSub12.BackColor = System.Drawing.Color.White
        Me.btnHomeSub12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub12.FlatAppearance.BorderSize = 0
        Me.btnHomeSub12.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub12.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub12.Location = New System.Drawing.Point(176, 103)
        Me.btnHomeSub12.Name = "btnHomeSub12"
        Me.btnHomeSub12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub12.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub12.TabIndex = 65
        Me.btnHomeSub12.UseVisualStyleBackColor = False
        Me.btnHomeSub12.Visible = False
        '
        'btnHomeSub7
        '
        Me.btnHomeSub7.BackColor = System.Drawing.Color.White
        Me.btnHomeSub7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub7.FlatAppearance.BorderSize = 0
        Me.btnHomeSub7.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub7.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub7.Location = New System.Drawing.Point(1, 69)
        Me.btnHomeSub7.Name = "btnHomeSub7"
        Me.btnHomeSub7.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub7.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub7.TabIndex = 60
        Me.btnHomeSub7.UseVisualStyleBackColor = False
        '
        'btnHomeSub8
        '
        Me.btnHomeSub8.BackColor = System.Drawing.Color.White
        Me.btnHomeSub8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub8.FlatAppearance.BorderSize = 0
        Me.btnHomeSub8.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub8.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub8.Location = New System.Drawing.Point(89, 69)
        Me.btnHomeSub8.Name = "btnHomeSub8"
        Me.btnHomeSub8.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub8.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub8.TabIndex = 61
        Me.btnHomeSub8.UseVisualStyleBackColor = False
        '
        'btnHomeSub9
        '
        Me.btnHomeSub9.BackColor = System.Drawing.Color.White
        Me.btnHomeSub9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub9.FlatAppearance.BorderSize = 0
        Me.btnHomeSub9.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub9.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub9.Location = New System.Drawing.Point(176, 69)
        Me.btnHomeSub9.Name = "btnHomeSub9"
        Me.btnHomeSub9.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub9.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub9.TabIndex = 62
        Me.btnHomeSub9.UseVisualStyleBackColor = False
        '
        'btnHomeSub4
        '
        Me.btnHomeSub4.BackColor = System.Drawing.Color.White
        Me.btnHomeSub4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub4.FlatAppearance.BorderSize = 0
        Me.btnHomeSub4.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub4.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub4.Location = New System.Drawing.Point(1, 35)
        Me.btnHomeSub4.Name = "btnHomeSub4"
        Me.btnHomeSub4.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub4.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub4.TabIndex = 57
        Me.btnHomeSub4.UseVisualStyleBackColor = False
        '
        'btnHomeSub5
        '
        Me.btnHomeSub5.BackColor = System.Drawing.Color.White
        Me.btnHomeSub5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub5.FlatAppearance.BorderSize = 0
        Me.btnHomeSub5.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub5.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub5.Location = New System.Drawing.Point(89, 35)
        Me.btnHomeSub5.Name = "btnHomeSub5"
        Me.btnHomeSub5.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub5.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub5.TabIndex = 58
        Me.btnHomeSub5.UseVisualStyleBackColor = False
        '
        'btnHomeSub6
        '
        Me.btnHomeSub6.BackColor = System.Drawing.Color.White
        Me.btnHomeSub6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub6.FlatAppearance.BorderSize = 0
        Me.btnHomeSub6.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub6.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub6.Location = New System.Drawing.Point(176, 35)
        Me.btnHomeSub6.Name = "btnHomeSub6"
        Me.btnHomeSub6.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub6.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub6.TabIndex = 59
        Me.btnHomeSub6.UseVisualStyleBackColor = False
        '
        'btnHomeSub1
        '
        Me.btnHomeSub1.BackColor = System.Drawing.Color.White
        Me.btnHomeSub1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub1.FlatAppearance.BorderSize = 0
        Me.btnHomeSub1.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub1.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub1.Location = New System.Drawing.Point(1, 1)
        Me.btnHomeSub1.Name = "btnHomeSub1"
        Me.btnHomeSub1.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub1.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub1.TabIndex = 54
        Me.btnHomeSub1.UseVisualStyleBackColor = False
        '
        'btnHomeSub2
        '
        Me.btnHomeSub2.BackColor = System.Drawing.Color.White
        Me.btnHomeSub2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub2.FlatAppearance.BorderSize = 0
        Me.btnHomeSub2.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub2.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub2.Location = New System.Drawing.Point(89, 1)
        Me.btnHomeSub2.Name = "btnHomeSub2"
        Me.btnHomeSub2.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub2.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub2.TabIndex = 55
        Me.btnHomeSub2.UseVisualStyleBackColor = False
        '
        'btnHomeSub3
        '
        Me.btnHomeSub3.BackColor = System.Drawing.Color.White
        Me.btnHomeSub3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomeSub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub3.FlatAppearance.BorderSize = 0
        Me.btnHomeSub3.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub3.ForeColor = System.Drawing.Color.Black
        Me.btnHomeSub3.Location = New System.Drawing.Point(176, 1)
        Me.btnHomeSub3.Name = "btnHomeSub3"
        Me.btnHomeSub3.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomeSub3.Size = New System.Drawing.Size(85, 35)
        Me.btnHomeSub3.TabIndex = 56
        Me.btnHomeSub3.UseVisualStyleBackColor = False
        '
        'lblTouchType
        '
        Me.lblTouchType.AutoSize = True
        Me.lblTouchType.Location = New System.Drawing.Point(581, 5)
        Me.lblTouchType.Name = "lblTouchType"
        Me.lblTouchType.Size = New System.Drawing.Size(65, 15)
        Me.lblTouchType.TabIndex = 255
        Me.lblTouchType.Text = "Touch type:"
        '
        'pnlTouchTypes
        '
        Me.pnlTouchTypes.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnlTouchTypes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTouchTypes.Controls.Add(Me.BtnKpPickUp)
        Me.pnlTouchTypes.Controls.Add(Me.btnGKHand)
        Me.pnlTouchTypes.Controls.Add(Me.btnGKThrow)
        Me.pnlTouchTypes.Controls.Add(Me.btn50Win)
        Me.pnlTouchTypes.Controls.Add(Me.btnAirWin)
        Me.pnlTouchTypes.Controls.Add(Me.BtnIntBPass)
        Me.pnlTouchTypes.Controls.Add(Me.BtnIntPass)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeIntPass)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeRunWithBall)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeGoalKick)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeTouch)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeGoalAttempt)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeClearance)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeThrowIn)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeOther)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeTackle)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeCatch)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeSave)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeBlock)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypeInt)
        Me.pnlTouchTypes.Controls.Add(Me.btnTypePass)
        Me.pnlTouchTypes.Location = New System.Drawing.Point(581, 24)
        Me.pnlTouchTypes.Name = "pnlTouchTypes"
        Me.pnlTouchTypes.Size = New System.Drawing.Size(397, 100)
        Me.pnlTouchTypes.TabIndex = 256
        '
        'BtnKpPickUp
        '
        Me.BtnKpPickUp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BtnKpPickUp.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnKpPickUp.Location = New System.Drawing.Point(167, 73)
        Me.BtnKpPickUp.Name = "BtnKpPickUp"
        Me.BtnKpPickUp.Size = New System.Drawing.Size(61, 22)
        Me.BtnKpPickUp.TabIndex = 304
        Me.BtnKpPickUp.Tag = "24"
        Me.BtnKpPickUp.Text = "K Pickup"
        Me.BtnKpPickUp.UseVisualStyleBackColor = False
        '
        'btnGKHand
        '
        Me.btnGKHand.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGKHand.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGKHand.Location = New System.Drawing.Point(247, 27)
        Me.btnGKHand.Name = "btnGKHand"
        Me.btnGKHand.Size = New System.Drawing.Size(61, 22)
        Me.btnGKHand.TabIndex = 303
        Me.btnGKHand.Tag = "22"
        Me.btnGKHand.Text = "GK Hand"
        Me.btnGKHand.UseVisualStyleBackColor = False
        '
        'btnGKThrow
        '
        Me.btnGKThrow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGKThrow.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGKThrow.Location = New System.Drawing.Point(247, 49)
        Me.btnGKThrow.Name = "btnGKThrow"
        Me.btnGKThrow.Size = New System.Drawing.Size(61, 22)
        Me.btnGKThrow.TabIndex = 302
        Me.btnGKThrow.Tag = "23"
        Me.btnGKThrow.Text = "GK Thrw"
        Me.btnGKThrow.UseVisualStyleBackColor = False
        '
        'btn50Win
        '
        Me.btn50Win.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btn50Win.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn50Win.Location = New System.Drawing.Point(325, 48)
        Me.btn50Win.Name = "btn50Win"
        Me.btn50Win.Size = New System.Drawing.Size(61, 22)
        Me.btn50Win.TabIndex = 301
        Me.btn50Win.Tag = "20"
        Me.btn50Win.Text = "50-Win"
        Me.btn50Win.UseVisualStyleBackColor = False
        '
        'btnAirWin
        '
        Me.btnAirWin.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAirWin.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAirWin.Location = New System.Drawing.Point(325, 70)
        Me.btnAirWin.Name = "btnAirWin"
        Me.btnAirWin.Size = New System.Drawing.Size(61, 22)
        Me.btnAirWin.TabIndex = 300
        Me.btnAirWin.Tag = "18"
        Me.btnAirWin.Text = "Air Win"
        Me.btnAirWin.UseVisualStyleBackColor = False
        '
        'BtnIntBPass
        '
        Me.BtnIntBPass.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BtnIntBPass.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnIntBPass.Location = New System.Drawing.Point(10, 73)
        Me.BtnIntBPass.Name = "BtnIntBPass"
        Me.BtnIntBPass.Size = New System.Drawing.Size(61, 22)
        Me.BtnIntBPass.TabIndex = 299
        Me.BtnIntBPass.Tag = "-98"
        Me.BtnIntBPass.Text = "Int/BPass"
        Me.BtnIntBPass.UseVisualStyleBackColor = False
        '
        'BtnIntPass
        '
        Me.BtnIntPass.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BtnIntPass.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnIntPass.Location = New System.Drawing.Point(10, 27)
        Me.BtnIntPass.Name = "BtnIntPass"
        Me.BtnIntPass.Size = New System.Drawing.Size(61, 22)
        Me.BtnIntPass.TabIndex = 298
        Me.BtnIntPass.Tag = "-99"
        Me.BtnIntPass.Text = "Int/Pass"
        Me.BtnIntPass.UseVisualStyleBackColor = False
        '
        'btnTypeIntPass
        '
        Me.btnTypeIntPass.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeIntPass.Enabled = False
        Me.btnTypeIntPass.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeIntPass.Location = New System.Drawing.Point(9, 26)
        Me.btnTypeIntPass.Name = "btnTypeIntPass"
        Me.btnTypeIntPass.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeIntPass.TabIndex = 216
        Me.btnTypeIntPass.Tag = "-99"
        Me.btnTypeIntPass.Text = "Int->Pass"
        Me.btnTypeIntPass.UseVisualStyleBackColor = False
        '
        'btnTypeRunWithBall
        '
        Me.btnTypeRunWithBall.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeRunWithBall.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeRunWithBall.Location = New System.Drawing.Point(325, 27)
        Me.btnTypeRunWithBall.Name = "btnTypeRunWithBall"
        Me.btnTypeRunWithBall.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeRunWithBall.TabIndex = 215
        Me.btnTypeRunWithBall.Tag = "17"
        Me.btnTypeRunWithBall.Text = "R W B"
        Me.btnTypeRunWithBall.UseVisualStyleBackColor = False
        '
        'btnTypeGoalKick
        '
        Me.btnTypeGoalKick.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeGoalKick.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeGoalKick.Location = New System.Drawing.Point(167, 27)
        Me.btnTypeGoalKick.Name = "btnTypeGoalKick"
        Me.btnTypeGoalKick.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeGoalKick.TabIndex = 214
        Me.btnTypeGoalKick.Tag = "12"
        Me.btnTypeGoalKick.Text = "GKLB"
        Me.btnTypeGoalKick.UseVisualStyleBackColor = False
        '
        'btnTypeTouch
        '
        Me.btnTypeTouch.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeTouch.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeTouch.Location = New System.Drawing.Point(10, 50)
        Me.btnTypeTouch.Name = "btnTypeTouch"
        Me.btnTypeTouch.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeTouch.TabIndex = 213
        Me.btnTypeTouch.Tag = "11"
        Me.btnTypeTouch.Text = "Bad Pass"
        Me.btnTypeTouch.UseVisualStyleBackColor = False
        '
        'btnTypeGoalAttempt
        '
        Me.btnTypeGoalAttempt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeGoalAttempt.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeGoalAttempt.Location = New System.Drawing.Point(87, 3)
        Me.btnTypeGoalAttempt.Name = "btnTypeGoalAttempt"
        Me.btnTypeGoalAttempt.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeGoalAttempt.TabIndex = 212
        Me.btnTypeGoalAttempt.Tag = "10"
        Me.btnTypeGoalAttempt.Text = "G A"
        Me.btnTypeGoalAttempt.UseVisualStyleBackColor = False
        '
        'btnTypeClearance
        '
        Me.btnTypeClearance.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeClearance.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeClearance.Location = New System.Drawing.Point(167, 3)
        Me.btnTypeClearance.Name = "btnTypeClearance"
        Me.btnTypeClearance.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeClearance.TabIndex = 211
        Me.btnTypeClearance.Tag = "9"
        Me.btnTypeClearance.Text = "Clear"
        Me.btnTypeClearance.UseVisualStyleBackColor = False
        '
        'btnTypeThrowIn
        '
        Me.btnTypeThrowIn.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeThrowIn.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeThrowIn.Location = New System.Drawing.Point(247, 73)
        Me.btnTypeThrowIn.Name = "btnTypeThrowIn"
        Me.btnTypeThrowIn.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeThrowIn.TabIndex = 210
        Me.btnTypeThrowIn.Tag = "8"
        Me.btnTypeThrowIn.Text = "Throw-In"
        Me.btnTypeThrowIn.UseVisualStyleBackColor = False
        '
        'btnTypeOther
        '
        Me.btnTypeOther.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeOther.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeOther.Location = New System.Drawing.Point(325, 4)
        Me.btnTypeOther.Name = "btnTypeOther"
        Me.btnTypeOther.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeOther.TabIndex = 209
        Me.btnTypeOther.Tag = "0"
        Me.btnTypeOther.Text = "Touch"
        Me.btnTypeOther.UseVisualStyleBackColor = False
        '
        'btnTypeTackle
        '
        Me.btnTypeTackle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeTackle.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeTackle.Location = New System.Drawing.Point(87, 73)
        Me.btnTypeTackle.Name = "btnTypeTackle"
        Me.btnTypeTackle.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeTackle.TabIndex = 208
        Me.btnTypeTackle.Tag = "4"
        Me.btnTypeTackle.Text = "Tackle"
        Me.btnTypeTackle.UseVisualStyleBackColor = False
        '
        'btnTypeCatch
        '
        Me.btnTypeCatch.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeCatch.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeCatch.Location = New System.Drawing.Point(247, 4)
        Me.btnTypeCatch.Name = "btnTypeCatch"
        Me.btnTypeCatch.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeCatch.TabIndex = 207
        Me.btnTypeCatch.Tag = "6"
        Me.btnTypeCatch.Text = "Catch"
        Me.btnTypeCatch.UseVisualStyleBackColor = False
        '
        'btnTypeSave
        '
        Me.btnTypeSave.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeSave.Location = New System.Drawing.Point(167, 49)
        Me.btnTypeSave.Name = "btnTypeSave"
        Me.btnTypeSave.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeSave.TabIndex = 206
        Me.btnTypeSave.Tag = "5"
        Me.btnTypeSave.Text = "Save"
        Me.btnTypeSave.UseVisualStyleBackColor = False
        '
        'btnTypeBlock
        '
        Me.btnTypeBlock.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeBlock.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeBlock.Location = New System.Drawing.Point(87, 49)
        Me.btnTypeBlock.Name = "btnTypeBlock"
        Me.btnTypeBlock.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeBlock.TabIndex = 205
        Me.btnTypeBlock.Tag = "3"
        Me.btnTypeBlock.Text = "Block"
        Me.btnTypeBlock.UseVisualStyleBackColor = False
        '
        'btnTypeInt
        '
        Me.btnTypeInt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypeInt.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypeInt.Location = New System.Drawing.Point(87, 26)
        Me.btnTypeInt.Name = "btnTypeInt"
        Me.btnTypeInt.Size = New System.Drawing.Size(61, 22)
        Me.btnTypeInt.TabIndex = 204
        Me.btnTypeInt.Tag = "2"
        Me.btnTypeInt.Text = "Int"
        Me.btnTypeInt.UseVisualStyleBackColor = False
        '
        'btnTypePass
        '
        Me.btnTypePass.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnTypePass.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTypePass.Location = New System.Drawing.Point(9, 4)
        Me.btnTypePass.Name = "btnTypePass"
        Me.btnTypePass.Size = New System.Drawing.Size(61, 22)
        Me.btnTypePass.TabIndex = 203
        Me.btnTypePass.Tag = "1"
        Me.btnTypePass.Text = "Pass"
        Me.btnTypePass.UseVisualStyleBackColor = False
        '
        'pnlHomePrimary
        '
        Me.pnlHomePrimary.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnlHomePrimary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHomePrimary.Controls.Add(Me.btnRunWithBallHome)
        Me.pnlHomePrimary.Controls.Add(Me.btnHomePrimary)
        Me.pnlHomePrimary.Controls.Add(Me.btn50Home)
        Me.pnlHomePrimary.Controls.Add(Me.btnAerialHome)
        Me.pnlHomePrimary.Location = New System.Drawing.Point(986, 43)
        Me.pnlHomePrimary.Name = "pnlHomePrimary"
        Me.pnlHomePrimary.Size = New System.Drawing.Size(213, 386)
        Me.pnlHomePrimary.TabIndex = 299
        Me.pnlHomePrimary.Visible = False
        '
        'btnRunWithBallHome
        '
        Me.btnRunWithBallHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnRunWithBallHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunWithBallHome.Location = New System.Drawing.Point(16, 287)
        Me.btnRunWithBallHome.Name = "btnRunWithBallHome"
        Me.btnRunWithBallHome.Size = New System.Drawing.Size(179, 72)
        Me.btnRunWithBallHome.TabIndex = 303
        Me.btnRunWithBallHome.Tag = "17"
        Me.btnRunWithBallHome.Text = "RWB"
        Me.btnRunWithBallHome.UseVisualStyleBackColor = False
        '
        'btnHomePrimary
        '
        Me.btnHomePrimary.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomePrimary.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePrimary.Location = New System.Drawing.Point(15, 16)
        Me.btnHomePrimary.Name = "btnHomePrimary"
        Me.btnHomePrimary.Size = New System.Drawing.Size(179, 72)
        Me.btnHomePrimary.TabIndex = 302
        Me.btnHomePrimary.Text = "Home Team"
        Me.btnHomePrimary.UseVisualStyleBackColor = False
        '
        'btn50Home
        '
        Me.btn50Home.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btn50Home.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn50Home.Location = New System.Drawing.Point(15, 194)
        Me.btn50Home.Name = "btn50Home"
        Me.btn50Home.Size = New System.Drawing.Size(179, 72)
        Me.btn50Home.TabIndex = 301
        Me.btn50Home.Tag = "20"
        Me.btn50Home.Text = "50-50 win"
        Me.btn50Home.UseVisualStyleBackColor = False
        '
        'btnAerialHome
        '
        Me.btnAerialHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAerialHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAerialHome.Location = New System.Drawing.Point(15, 105)
        Me.btnAerialHome.Name = "btnAerialHome"
        Me.btnAerialHome.Size = New System.Drawing.Size(179, 72)
        Me.btnAerialHome.TabIndex = 298
        Me.btnAerialHome.Tag = "18"
        Me.btnAerialHome.Text = "Aerial Win"
        Me.btnAerialHome.UseVisualStyleBackColor = False
        '
        'pnlTouchDisplay
        '
        Me.pnlTouchDisplay.BackColor = System.Drawing.Color.Lavender
        Me.pnlTouchDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTouchDisplay.Controls.Add(Me.lblDispLocation2)
        Me.pnlTouchDisplay.Controls.Add(Me.lblLocationVal2)
        Me.pnlTouchDisplay.Controls.Add(Me.lblValLocation)
        Me.pnlTouchDisplay.Controls.Add(Me.lblValUniform)
        Me.pnlTouchDisplay.Controls.Add(Me.lblValType)
        Me.pnlTouchDisplay.Controls.Add(Me.lblValTeam)
        Me.pnlTouchDisplay.Controls.Add(Me.lblDispLocation)
        Me.pnlTouchDisplay.Controls.Add(Me.lblDispType)
        Me.pnlTouchDisplay.Controls.Add(Me.lblUniform)
        Me.pnlTouchDisplay.Controls.Add(Me.lblTeam)
        Me.pnlTouchDisplay.Location = New System.Drawing.Point(234, 261)
        Me.pnlTouchDisplay.Name = "pnlTouchDisplay"
        Me.pnlTouchDisplay.Size = New System.Drawing.Size(342, 50)
        Me.pnlTouchDisplay.TabIndex = 257
        '
        'lblDispLocation2
        '
        Me.lblDispLocation2.AutoSize = True
        Me.lblDispLocation2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDispLocation2.ForeColor = System.Drawing.Color.Black
        Me.lblDispLocation2.Location = New System.Drawing.Point(178, 28)
        Me.lblDispLocation2.Name = "lblDispLocation2"
        Me.lblDispLocation2.Size = New System.Drawing.Size(34, 15)
        Me.lblDispLocation2.TabIndex = 264
        Me.lblDispLocation2.Text = "Loc2:"
        '
        'lblLocationVal2
        '
        Me.lblLocationVal2.AutoSize = True
        Me.lblLocationVal2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocationVal2.ForeColor = System.Drawing.Color.ForestGreen
        Me.lblLocationVal2.Location = New System.Drawing.Point(241, 29)
        Me.lblLocationVal2.Margin = New System.Windows.Forms.Padding(0)
        Me.lblLocationVal2.Name = "lblLocationVal2"
        Me.lblLocationVal2.Size = New System.Drawing.Size(0, 15)
        Me.lblLocationVal2.TabIndex = 263
        '
        'lblValLocation
        '
        Me.lblValLocation.AutoSize = True
        Me.lblValLocation.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValLocation.ForeColor = System.Drawing.Color.ForestGreen
        Me.lblValLocation.Location = New System.Drawing.Point(146, 28)
        Me.lblValLocation.Margin = New System.Windows.Forms.Padding(0)
        Me.lblValLocation.Name = "lblValLocation"
        Me.lblValLocation.Size = New System.Drawing.Size(0, 15)
        Me.lblValLocation.TabIndex = 262
        '
        'lblValUniform
        '
        Me.lblValUniform.AutoSize = True
        Me.lblValUniform.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValUniform.ForeColor = System.Drawing.Color.ForestGreen
        Me.lblValUniform.Location = New System.Drawing.Point(72, 28)
        Me.lblValUniform.Margin = New System.Windows.Forms.Padding(0)
        Me.lblValUniform.Name = "lblValUniform"
        Me.lblValUniform.Size = New System.Drawing.Size(0, 15)
        Me.lblValUniform.TabIndex = 261
        '
        'lblValType
        '
        Me.lblValType.AutoSize = True
        Me.lblValType.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValType.ForeColor = System.Drawing.Color.ForestGreen
        Me.lblValType.Location = New System.Drawing.Point(173, 7)
        Me.lblValType.Margin = New System.Windows.Forms.Padding(0)
        Me.lblValType.Name = "lblValType"
        Me.lblValType.Size = New System.Drawing.Size(0, 15)
        Me.lblValType.TabIndex = 260
        '
        'lblValTeam
        '
        Me.lblValTeam.AutoSize = True
        Me.lblValTeam.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValTeam.ForeColor = System.Drawing.Color.ForestGreen
        Me.lblValTeam.Location = New System.Drawing.Point(35, 7)
        Me.lblValTeam.Margin = New System.Windows.Forms.Padding(0)
        Me.lblValTeam.Name = "lblValTeam"
        Me.lblValTeam.Size = New System.Drawing.Size(0, 15)
        Me.lblValTeam.TabIndex = 259
        '
        'lblDispLocation
        '
        Me.lblDispLocation.AutoSize = True
        Me.lblDispLocation.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDispLocation.ForeColor = System.Drawing.Color.Black
        Me.lblDispLocation.Location = New System.Drawing.Point(91, 28)
        Me.lblDispLocation.Name = "lblDispLocation"
        Me.lblDispLocation.Size = New System.Drawing.Size(34, 15)
        Me.lblDispLocation.TabIndex = 258
        Me.lblDispLocation.Text = "Loc1:"
        '
        'lblDispType
        '
        Me.lblDispType.AutoSize = True
        Me.lblDispType.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDispType.ForeColor = System.Drawing.Color.Black
        Me.lblDispType.Location = New System.Drawing.Point(142, 7)
        Me.lblDispType.Name = "lblDispType"
        Me.lblDispType.Size = New System.Drawing.Size(35, 15)
        Me.lblDispType.TabIndex = 257
        Me.lblDispType.Text = "Type:"
        '
        'lblUniform
        '
        Me.lblUniform.AutoSize = True
        Me.lblUniform.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUniform.ForeColor = System.Drawing.Color.Black
        Me.lblUniform.Location = New System.Drawing.Point(3, 28)
        Me.lblUniform.Name = "lblUniform"
        Me.lblUniform.Size = New System.Drawing.Size(47, 15)
        Me.lblUniform.TabIndex = 256
        Me.lblUniform.Text = "Uniform:"
        '
        'lblTeam
        '
        Me.lblTeam.AutoSize = True
        Me.lblTeam.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.ForeColor = System.Drawing.Color.Black
        Me.lblTeam.Location = New System.Drawing.Point(2, 7)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(37, 15)
        Me.lblTeam.TabIndex = 255
        Me.lblTeam.Text = "Team:"
        '
        'chkEnableVoice
        '
        Me.chkEnableVoice.AutoSize = True
        Me.chkEnableVoice.Location = New System.Drawing.Point(807, 169)
        Me.chkEnableVoice.Name = "chkEnableVoice"
        Me.chkEnableVoice.Size = New System.Drawing.Size(88, 19)
        Me.chkEnableVoice.TabIndex = 258
        Me.chkEnableVoice.Text = "Enable voice"
        Me.chkEnableVoice.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(710, 134)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 25)
        Me.btnSave.TabIndex = 281
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClear.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClear.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.Color.White
        Me.btnClear.Location = New System.Drawing.Point(776, 134)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(60, 25)
        Me.btnClear.TabIndex = 282
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'cmnTouches
        '
        Me.cmnTouches.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem, Me.DeleteToolStripMenuItem})
        Me.cmnTouches.Name = "cmnTouches"
        Me.cmnTouches.Size = New System.Drawing.Size(108, 48)
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'lnkRefreshPlayers
        '
        Me.lnkRefreshPlayers.AutoSize = True
        Me.lnkRefreshPlayers.Location = New System.Drawing.Point(581, 169)
        Me.lnkRefreshPlayers.Name = "lnkRefreshPlayers"
        Me.lnkRefreshPlayers.Size = New System.Drawing.Size(86, 15)
        Me.lnkRefreshPlayers.TabIndex = 285
        Me.lnkRefreshPlayers.TabStop = True
        Me.lnkRefreshPlayers.Text = "Refresh Players"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblSelection2)
        Me.Panel1.Controls.Add(Me.picMark2)
        Me.Panel1.Controls.Add(Me.picMark1)
        Me.Panel1.Controls.Add(Me.lblSelection1)
        Me.Panel1.Controls.Add(Me.lblFieldY)
        Me.Panel1.Controls.Add(Me.lblFieldX)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(234, 233)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(342, 24)
        Me.Panel1.TabIndex = 287
        '
        'lblSelection2
        '
        Me.lblSelection2.AutoSize = True
        Me.lblSelection2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelection2.Location = New System.Drawing.Point(284, 3)
        Me.lblSelection2.Name = "lblSelection2"
        Me.lblSelection2.Size = New System.Drawing.Size(11, 15)
        Me.lblSelection2.TabIndex = 13
        Me.lblSelection2.Text = "-"
        '
        'picMark2
        '
        Me.picMark2.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePin
        Me.picMark2.Location = New System.Drawing.Point(269, 5)
        Me.picMark2.Name = "picMark2"
        Me.picMark2.Size = New System.Drawing.Size(14, 13)
        Me.picMark2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMark2.TabIndex = 12
        Me.picMark2.TabStop = False
        '
        'picMark1
        '
        Me.picMark1.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPin
        Me.picMark1.Location = New System.Drawing.Point(211, 5)
        Me.picMark1.Name = "picMark1"
        Me.picMark1.Size = New System.Drawing.Size(14, 13)
        Me.picMark1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMark1.TabIndex = 11
        Me.picMark1.TabStop = False
        '
        'lblSelection1
        '
        Me.lblSelection1.AutoSize = True
        Me.lblSelection1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelection1.Location = New System.Drawing.Point(225, 3)
        Me.lblSelection1.Name = "lblSelection1"
        Me.lblSelection1.Size = New System.Drawing.Size(11, 15)
        Me.lblSelection1.TabIndex = 9
        Me.lblSelection1.Text = "-"
        '
        'lblFieldY
        '
        Me.lblFieldY.AutoSize = True
        Me.lblFieldY.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFieldY.Location = New System.Drawing.Point(160, 4)
        Me.lblFieldY.Name = "lblFieldY"
        Me.lblFieldY.Size = New System.Drawing.Size(17, 15)
        Me.lblFieldY.TabIndex = 6
        Me.lblFieldY.Text = "Y:"
        '
        'lblFieldX
        '
        Me.lblFieldX.AutoSize = True
        Me.lblFieldX.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFieldX.Location = New System.Drawing.Point(123, 4)
        Me.lblFieldX.Name = "lblFieldX"
        Me.lblFieldX.Size = New System.Drawing.Size(17, 15)
        Me.lblFieldX.TabIndex = 5
        Me.lblFieldX.Text = "X:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "FIELD: 105m X 70m"
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(589, 138)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(33, 15)
        Me.lblTime.TabIndex = 290
        Me.lblTime.Text = "Time:"
        '
        'pnlHome
        '
        Me.pnlHome.Controls.Add(Me.btnHomePlayer38)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer37)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer36)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer35)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer34)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer41)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer40)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer39)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer27)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer26)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer25)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer24)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer23)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer32)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer31)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer30)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer29)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer28)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer33)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer16)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer15)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer14)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer13)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer12)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer21)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer20)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer19)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer18)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer17)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer22)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer5)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer4)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer3)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer2)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer1)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer10)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer9)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer8)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer7)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer6)
        Me.pnlHome.Controls.Add(Me.btnHomePlayer11)
        Me.pnlHome.Location = New System.Drawing.Point(22, 6)
        Me.pnlHome.Name = "pnlHome"
        Me.pnlHome.Size = New System.Drawing.Size(207, 297)
        Me.pnlHome.TabIndex = 292
        '
        'btnHomePlayer38
        '
        Me.btnHomePlayer38.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer38.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer38.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer38.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer38.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer38.Location = New System.Drawing.Point(0, 999)
        Me.btnHomePlayer38.Name = "btnHomePlayer38"
        Me.btnHomePlayer38.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer38.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer38.TabIndex = 80
        Me.btnHomePlayer38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer38.UseVisualStyleBackColor = False
        Me.btnHomePlayer38.Visible = False
        '
        'btnHomePlayer37
        '
        Me.btnHomePlayer37.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer37.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer37.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer37.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer37.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer37.Location = New System.Drawing.Point(0, 972)
        Me.btnHomePlayer37.Name = "btnHomePlayer37"
        Me.btnHomePlayer37.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer37.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer37.TabIndex = 79
        Me.btnHomePlayer37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer37.UseVisualStyleBackColor = False
        Me.btnHomePlayer37.Visible = False
        '
        'btnHomePlayer36
        '
        Me.btnHomePlayer36.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer36.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer36.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer36.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer36.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer36.Location = New System.Drawing.Point(0, 945)
        Me.btnHomePlayer36.Name = "btnHomePlayer36"
        Me.btnHomePlayer36.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer36.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer36.TabIndex = 78
        Me.btnHomePlayer36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer36.UseVisualStyleBackColor = False
        Me.btnHomePlayer36.Visible = False
        '
        'btnHomePlayer35
        '
        Me.btnHomePlayer35.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer35.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer35.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer35.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer35.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer35.Location = New System.Drawing.Point(0, 918)
        Me.btnHomePlayer35.Name = "btnHomePlayer35"
        Me.btnHomePlayer35.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer35.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer35.TabIndex = 77
        Me.btnHomePlayer35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer35.UseVisualStyleBackColor = False
        Me.btnHomePlayer35.Visible = False
        '
        'btnHomePlayer34
        '
        Me.btnHomePlayer34.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer34.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer34.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer34.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer34.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer34.Location = New System.Drawing.Point(0, 891)
        Me.btnHomePlayer34.Name = "btnHomePlayer34"
        Me.btnHomePlayer34.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer34.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer34.TabIndex = 76
        Me.btnHomePlayer34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer34.UseVisualStyleBackColor = False
        Me.btnHomePlayer34.Visible = False
        '
        'btnHomePlayer41
        '
        Me.btnHomePlayer41.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer41.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer41.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer41.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer41.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer41.Location = New System.Drawing.Point(0, 1080)
        Me.btnHomePlayer41.Name = "btnHomePlayer41"
        Me.btnHomePlayer41.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer41.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer41.TabIndex = 83
        Me.btnHomePlayer41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer41.UseVisualStyleBackColor = False
        Me.btnHomePlayer41.Visible = False
        '
        'btnHomePlayer40
        '
        Me.btnHomePlayer40.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer40.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer40.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer40.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer40.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer40.Location = New System.Drawing.Point(0, 1053)
        Me.btnHomePlayer40.Name = "btnHomePlayer40"
        Me.btnHomePlayer40.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer40.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer40.TabIndex = 82
        Me.btnHomePlayer40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer40.UseVisualStyleBackColor = False
        Me.btnHomePlayer40.Visible = False
        '
        'btnHomePlayer39
        '
        Me.btnHomePlayer39.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer39.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer39.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer39.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer39.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer39.Location = New System.Drawing.Point(0, 1026)
        Me.btnHomePlayer39.Name = "btnHomePlayer39"
        Me.btnHomePlayer39.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer39.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer39.TabIndex = 81
        Me.btnHomePlayer39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer39.UseVisualStyleBackColor = False
        Me.btnHomePlayer39.Visible = False
        '
        'btnHomePlayer27
        '
        Me.btnHomePlayer27.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer27.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer27.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer27.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer27.Location = New System.Drawing.Point(0, 702)
        Me.btnHomePlayer27.Name = "btnHomePlayer27"
        Me.btnHomePlayer27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer27.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer27.TabIndex = 69
        Me.btnHomePlayer27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer27.UseVisualStyleBackColor = False
        Me.btnHomePlayer27.Visible = False
        '
        'btnHomePlayer26
        '
        Me.btnHomePlayer26.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer26.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer26.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer26.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer26.Location = New System.Drawing.Point(0, 675)
        Me.btnHomePlayer26.Name = "btnHomePlayer26"
        Me.btnHomePlayer26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer26.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer26.TabIndex = 68
        Me.btnHomePlayer26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer26.UseVisualStyleBackColor = False
        Me.btnHomePlayer26.Visible = False
        '
        'btnHomePlayer25
        '
        Me.btnHomePlayer25.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer25.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer25.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer25.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer25.Location = New System.Drawing.Point(0, 648)
        Me.btnHomePlayer25.Name = "btnHomePlayer25"
        Me.btnHomePlayer25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer25.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer25.TabIndex = 67
        Me.btnHomePlayer25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer25.UseVisualStyleBackColor = False
        Me.btnHomePlayer25.Visible = False
        '
        'btnHomePlayer24
        '
        Me.btnHomePlayer24.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer24.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer24.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer24.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer24.Location = New System.Drawing.Point(0, 621)
        Me.btnHomePlayer24.Name = "btnHomePlayer24"
        Me.btnHomePlayer24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer24.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer24.TabIndex = 66
        Me.btnHomePlayer24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer24.UseVisualStyleBackColor = False
        Me.btnHomePlayer24.Visible = False
        '
        'btnHomePlayer23
        '
        Me.btnHomePlayer23.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer23.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer23.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer23.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer23.Location = New System.Drawing.Point(0, 594)
        Me.btnHomePlayer23.Name = "btnHomePlayer23"
        Me.btnHomePlayer23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer23.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer23.TabIndex = 65
        Me.btnHomePlayer23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer23.UseVisualStyleBackColor = False
        Me.btnHomePlayer23.Visible = False
        '
        'btnHomePlayer32
        '
        Me.btnHomePlayer32.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer32.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer32.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer32.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer32.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer32.Location = New System.Drawing.Point(0, 837)
        Me.btnHomePlayer32.Name = "btnHomePlayer32"
        Me.btnHomePlayer32.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer32.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer32.TabIndex = 74
        Me.btnHomePlayer32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer32.UseVisualStyleBackColor = False
        Me.btnHomePlayer32.Visible = False
        '
        'btnHomePlayer31
        '
        Me.btnHomePlayer31.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer31.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer31.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer31.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer31.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer31.Location = New System.Drawing.Point(0, 810)
        Me.btnHomePlayer31.Name = "btnHomePlayer31"
        Me.btnHomePlayer31.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer31.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer31.TabIndex = 73
        Me.btnHomePlayer31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer31.UseVisualStyleBackColor = False
        Me.btnHomePlayer31.Visible = False
        '
        'btnHomePlayer30
        '
        Me.btnHomePlayer30.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer30.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer30.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer30.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer30.Location = New System.Drawing.Point(0, 783)
        Me.btnHomePlayer30.Name = "btnHomePlayer30"
        Me.btnHomePlayer30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer30.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer30.TabIndex = 72
        Me.btnHomePlayer30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer30.UseVisualStyleBackColor = False
        Me.btnHomePlayer30.Visible = False
        '
        'btnHomePlayer29
        '
        Me.btnHomePlayer29.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer29.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer29.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer29.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer29.Location = New System.Drawing.Point(0, 756)
        Me.btnHomePlayer29.Name = "btnHomePlayer29"
        Me.btnHomePlayer29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer29.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer29.TabIndex = 71
        Me.btnHomePlayer29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer29.UseVisualStyleBackColor = False
        Me.btnHomePlayer29.Visible = False
        '
        'btnHomePlayer28
        '
        Me.btnHomePlayer28.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer28.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer28.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer28.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer28.Location = New System.Drawing.Point(0, 729)
        Me.btnHomePlayer28.Name = "btnHomePlayer28"
        Me.btnHomePlayer28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer28.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer28.TabIndex = 70
        Me.btnHomePlayer28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer28.UseVisualStyleBackColor = False
        Me.btnHomePlayer28.Visible = False
        '
        'btnHomePlayer33
        '
        Me.btnHomePlayer33.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer33.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer33.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer33.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer33.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer33.Location = New System.Drawing.Point(0, 864)
        Me.btnHomePlayer33.Name = "btnHomePlayer33"
        Me.btnHomePlayer33.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer33.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer33.TabIndex = 75
        Me.btnHomePlayer33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer33.UseVisualStyleBackColor = False
        Me.btnHomePlayer33.Visible = False
        '
        'btnHomePlayer16
        '
        Me.btnHomePlayer16.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer16.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer16.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer16.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer16.Location = New System.Drawing.Point(0, 405)
        Me.btnHomePlayer16.Name = "btnHomePlayer16"
        Me.btnHomePlayer16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer16.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer16.TabIndex = 58
        Me.btnHomePlayer16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer16.UseVisualStyleBackColor = False
        '
        'btnHomePlayer15
        '
        Me.btnHomePlayer15.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer15.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer15.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer15.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer15.Location = New System.Drawing.Point(0, 378)
        Me.btnHomePlayer15.Name = "btnHomePlayer15"
        Me.btnHomePlayer15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer15.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer15.TabIndex = 57
        Me.btnHomePlayer15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer15.UseVisualStyleBackColor = False
        '
        'btnHomePlayer14
        '
        Me.btnHomePlayer14.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer14.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer14.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer14.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer14.Location = New System.Drawing.Point(0, 351)
        Me.btnHomePlayer14.Name = "btnHomePlayer14"
        Me.btnHomePlayer14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer14.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer14.TabIndex = 56
        Me.btnHomePlayer14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer14.UseVisualStyleBackColor = False
        '
        'btnHomePlayer13
        '
        Me.btnHomePlayer13.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer13.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer13.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer13.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer13.Location = New System.Drawing.Point(0, 324)
        Me.btnHomePlayer13.Name = "btnHomePlayer13"
        Me.btnHomePlayer13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer13.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer13.TabIndex = 55
        Me.btnHomePlayer13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer13.UseVisualStyleBackColor = False
        '
        'btnHomePlayer12
        '
        Me.btnHomePlayer12.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer12.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer12.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer12.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer12.Location = New System.Drawing.Point(0, 297)
        Me.btnHomePlayer12.Name = "btnHomePlayer12"
        Me.btnHomePlayer12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer12.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer12.TabIndex = 54
        Me.btnHomePlayer12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer12.UseVisualStyleBackColor = False
        '
        'btnHomePlayer21
        '
        Me.btnHomePlayer21.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer21.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer21.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer21.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer21.Location = New System.Drawing.Point(0, 540)
        Me.btnHomePlayer21.Name = "btnHomePlayer21"
        Me.btnHomePlayer21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer21.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer21.TabIndex = 63
        Me.btnHomePlayer21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer21.UseVisualStyleBackColor = False
        Me.btnHomePlayer21.Visible = False
        '
        'btnHomePlayer20
        '
        Me.btnHomePlayer20.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer20.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer20.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer20.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer20.Location = New System.Drawing.Point(0, 513)
        Me.btnHomePlayer20.Name = "btnHomePlayer20"
        Me.btnHomePlayer20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer20.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer20.TabIndex = 62
        Me.btnHomePlayer20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer20.UseVisualStyleBackColor = False
        '
        'btnHomePlayer19
        '
        Me.btnHomePlayer19.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer19.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer19.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer19.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer19.Location = New System.Drawing.Point(0, 486)
        Me.btnHomePlayer19.Name = "btnHomePlayer19"
        Me.btnHomePlayer19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer19.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer19.TabIndex = 61
        Me.btnHomePlayer19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer19.UseVisualStyleBackColor = False
        '
        'btnHomePlayer18
        '
        Me.btnHomePlayer18.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer18.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer18.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer18.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer18.Location = New System.Drawing.Point(0, 459)
        Me.btnHomePlayer18.Name = "btnHomePlayer18"
        Me.btnHomePlayer18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer18.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer18.TabIndex = 60
        Me.btnHomePlayer18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer18.UseVisualStyleBackColor = False
        '
        'btnHomePlayer17
        '
        Me.btnHomePlayer17.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer17.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer17.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer17.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer17.Location = New System.Drawing.Point(0, 432)
        Me.btnHomePlayer17.Name = "btnHomePlayer17"
        Me.btnHomePlayer17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer17.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer17.TabIndex = 59
        Me.btnHomePlayer17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer17.UseVisualStyleBackColor = False
        '
        'btnHomePlayer22
        '
        Me.btnHomePlayer22.BackColor = System.Drawing.Color.White
        Me.btnHomePlayer22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnHomePlayer22.FlatAppearance.BorderSize = 0
        Me.btnHomePlayer22.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePlayer22.ForeColor = System.Drawing.Color.Black
        Me.btnHomePlayer22.Location = New System.Drawing.Point(0, 567)
        Me.btnHomePlayer22.Name = "btnHomePlayer22"
        Me.btnHomePlayer22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnHomePlayer22.Size = New System.Drawing.Size(190, 27)
        Me.btnHomePlayer22.TabIndex = 64
        Me.btnHomePlayer22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHomePlayer22.UseVisualStyleBackColor = False
        Me.btnHomePlayer22.Visible = False
        '
        'pnlVisit
        '
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer38)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer37)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer36)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer35)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer34)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer41)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer40)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer39)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer27)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer26)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer25)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer24)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer23)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer32)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer31)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer30)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer29)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer28)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer33)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer16)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer15)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer14)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer13)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer12)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer21)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer20)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer19)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer18)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer17)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer22)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer5)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer4)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer3)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer2)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer1)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer10)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer9)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer8)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer7)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer6)
        Me.pnlVisit.Controls.Add(Me.btnVisitPlayer11)
        Me.pnlVisit.Location = New System.Drawing.Point(22, 6)
        Me.pnlVisit.Name = "pnlVisit"
        Me.pnlVisit.Size = New System.Drawing.Size(207, 297)
        Me.pnlVisit.TabIndex = 293
        '
        'btnVisitPlayer38
        '
        Me.btnVisitPlayer38.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer38.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer38.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer38.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer38.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer38.Location = New System.Drawing.Point(0, 999)
        Me.btnVisitPlayer38.Name = "btnVisitPlayer38"
        Me.btnVisitPlayer38.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer38.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer38.TabIndex = 38
        Me.btnVisitPlayer38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer38.UseVisualStyleBackColor = False
        Me.btnVisitPlayer38.Visible = False
        '
        'btnVisitPlayer37
        '
        Me.btnVisitPlayer37.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer37.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer37.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer37.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer37.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer37.Location = New System.Drawing.Point(0, 972)
        Me.btnVisitPlayer37.Name = "btnVisitPlayer37"
        Me.btnVisitPlayer37.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer37.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer37.TabIndex = 37
        Me.btnVisitPlayer37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer37.UseVisualStyleBackColor = False
        Me.btnVisitPlayer37.Visible = False
        '
        'btnVisitPlayer36
        '
        Me.btnVisitPlayer36.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer36.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer36.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer36.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer36.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer36.Location = New System.Drawing.Point(0, 945)
        Me.btnVisitPlayer36.Name = "btnVisitPlayer36"
        Me.btnVisitPlayer36.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer36.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer36.TabIndex = 36
        Me.btnVisitPlayer36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer36.UseVisualStyleBackColor = False
        Me.btnVisitPlayer36.Visible = False
        '
        'btnVisitPlayer35
        '
        Me.btnVisitPlayer35.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer35.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer35.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer35.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer35.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer35.Location = New System.Drawing.Point(0, 918)
        Me.btnVisitPlayer35.Name = "btnVisitPlayer35"
        Me.btnVisitPlayer35.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer35.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer35.TabIndex = 35
        Me.btnVisitPlayer35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer35.UseVisualStyleBackColor = False
        Me.btnVisitPlayer35.Visible = False
        '
        'btnVisitPlayer34
        '
        Me.btnVisitPlayer34.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer34.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer34.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer34.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer34.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer34.Location = New System.Drawing.Point(0, 891)
        Me.btnVisitPlayer34.Name = "btnVisitPlayer34"
        Me.btnVisitPlayer34.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer34.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer34.TabIndex = 34
        Me.btnVisitPlayer34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer34.UseVisualStyleBackColor = False
        Me.btnVisitPlayer34.Visible = False
        '
        'btnVisitPlayer41
        '
        Me.btnVisitPlayer41.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer41.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer41.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer41.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer41.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer41.Location = New System.Drawing.Point(0, 1080)
        Me.btnVisitPlayer41.Name = "btnVisitPlayer41"
        Me.btnVisitPlayer41.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer41.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer41.TabIndex = 41
        Me.btnVisitPlayer41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer41.UseVisualStyleBackColor = False
        Me.btnVisitPlayer41.Visible = False
        '
        'btnVisitPlayer40
        '
        Me.btnVisitPlayer40.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer40.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer40.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer40.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer40.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer40.Location = New System.Drawing.Point(0, 1053)
        Me.btnVisitPlayer40.Name = "btnVisitPlayer40"
        Me.btnVisitPlayer40.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer40.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer40.TabIndex = 40
        Me.btnVisitPlayer40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer40.UseVisualStyleBackColor = False
        Me.btnVisitPlayer40.Visible = False
        '
        'btnVisitPlayer39
        '
        Me.btnVisitPlayer39.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer39.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer39.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer39.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer39.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer39.Location = New System.Drawing.Point(0, 1026)
        Me.btnVisitPlayer39.Name = "btnVisitPlayer39"
        Me.btnVisitPlayer39.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer39.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer39.TabIndex = 39
        Me.btnVisitPlayer39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer39.UseVisualStyleBackColor = False
        Me.btnVisitPlayer39.Visible = False
        '
        'btnVisitPlayer27
        '
        Me.btnVisitPlayer27.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer27.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer27.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer27.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer27.Location = New System.Drawing.Point(0, 702)
        Me.btnVisitPlayer27.Name = "btnVisitPlayer27"
        Me.btnVisitPlayer27.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer27.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer27.TabIndex = 27
        Me.btnVisitPlayer27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer27.UseVisualStyleBackColor = False
        Me.btnVisitPlayer27.Visible = False
        '
        'btnVisitPlayer26
        '
        Me.btnVisitPlayer26.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer26.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer26.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer26.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer26.Location = New System.Drawing.Point(0, 675)
        Me.btnVisitPlayer26.Name = "btnVisitPlayer26"
        Me.btnVisitPlayer26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer26.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer26.TabIndex = 26
        Me.btnVisitPlayer26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer26.UseVisualStyleBackColor = False
        Me.btnVisitPlayer26.Visible = False
        '
        'btnVisitPlayer25
        '
        Me.btnVisitPlayer25.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer25.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer25.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer25.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer25.Location = New System.Drawing.Point(0, 648)
        Me.btnVisitPlayer25.Name = "btnVisitPlayer25"
        Me.btnVisitPlayer25.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer25.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer25.TabIndex = 25
        Me.btnVisitPlayer25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer25.UseVisualStyleBackColor = False
        Me.btnVisitPlayer25.Visible = False
        '
        'btnVisitPlayer24
        '
        Me.btnVisitPlayer24.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer24.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer24.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer24.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer24.Location = New System.Drawing.Point(0, 621)
        Me.btnVisitPlayer24.Name = "btnVisitPlayer24"
        Me.btnVisitPlayer24.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer24.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer24.TabIndex = 24
        Me.btnVisitPlayer24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer24.UseVisualStyleBackColor = False
        Me.btnVisitPlayer24.Visible = False
        '
        'btnVisitPlayer23
        '
        Me.btnVisitPlayer23.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer23.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer23.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer23.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer23.Location = New System.Drawing.Point(0, 594)
        Me.btnVisitPlayer23.Name = "btnVisitPlayer23"
        Me.btnVisitPlayer23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer23.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer23.TabIndex = 23
        Me.btnVisitPlayer23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer23.UseVisualStyleBackColor = False
        Me.btnVisitPlayer23.Visible = False
        '
        'btnVisitPlayer32
        '
        Me.btnVisitPlayer32.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer32.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer32.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer32.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer32.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer32.Location = New System.Drawing.Point(0, 837)
        Me.btnVisitPlayer32.Name = "btnVisitPlayer32"
        Me.btnVisitPlayer32.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer32.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer32.TabIndex = 32
        Me.btnVisitPlayer32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer32.UseVisualStyleBackColor = False
        Me.btnVisitPlayer32.Visible = False
        '
        'btnVisitPlayer31
        '
        Me.btnVisitPlayer31.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer31.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer31.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer31.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer31.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer31.Location = New System.Drawing.Point(0, 810)
        Me.btnVisitPlayer31.Name = "btnVisitPlayer31"
        Me.btnVisitPlayer31.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer31.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer31.TabIndex = 31
        Me.btnVisitPlayer31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer31.UseVisualStyleBackColor = False
        Me.btnVisitPlayer31.Visible = False
        '
        'btnVisitPlayer30
        '
        Me.btnVisitPlayer30.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer30.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer30.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer30.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer30.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer30.Location = New System.Drawing.Point(0, 783)
        Me.btnVisitPlayer30.Name = "btnVisitPlayer30"
        Me.btnVisitPlayer30.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer30.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer30.TabIndex = 30
        Me.btnVisitPlayer30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer30.UseVisualStyleBackColor = False
        Me.btnVisitPlayer30.Visible = False
        '
        'btnVisitPlayer29
        '
        Me.btnVisitPlayer29.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer29.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer29.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer29.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer29.Location = New System.Drawing.Point(0, 756)
        Me.btnVisitPlayer29.Name = "btnVisitPlayer29"
        Me.btnVisitPlayer29.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer29.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer29.TabIndex = 29
        Me.btnVisitPlayer29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer29.UseVisualStyleBackColor = False
        Me.btnVisitPlayer29.Visible = False
        '
        'btnVisitPlayer28
        '
        Me.btnVisitPlayer28.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer28.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer28.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer28.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer28.Location = New System.Drawing.Point(0, 729)
        Me.btnVisitPlayer28.Name = "btnVisitPlayer28"
        Me.btnVisitPlayer28.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer28.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer28.TabIndex = 28
        Me.btnVisitPlayer28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer28.UseVisualStyleBackColor = False
        Me.btnVisitPlayer28.Visible = False
        '
        'btnVisitPlayer33
        '
        Me.btnVisitPlayer33.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer33.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer33.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer33.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer33.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer33.Location = New System.Drawing.Point(0, 864)
        Me.btnVisitPlayer33.Name = "btnVisitPlayer33"
        Me.btnVisitPlayer33.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer33.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer33.TabIndex = 33
        Me.btnVisitPlayer33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer33.UseVisualStyleBackColor = False
        Me.btnVisitPlayer33.Visible = False
        '
        'btnVisitPlayer16
        '
        Me.btnVisitPlayer16.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer16.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer16.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer16.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer16.Location = New System.Drawing.Point(0, 405)
        Me.btnVisitPlayer16.Name = "btnVisitPlayer16"
        Me.btnVisitPlayer16.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer16.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer16.TabIndex = 16
        Me.btnVisitPlayer16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer16.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer15
        '
        Me.btnVisitPlayer15.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer15.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer15.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer15.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer15.Location = New System.Drawing.Point(0, 378)
        Me.btnVisitPlayer15.Name = "btnVisitPlayer15"
        Me.btnVisitPlayer15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer15.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer15.TabIndex = 15
        Me.btnVisitPlayer15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer15.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer14
        '
        Me.btnVisitPlayer14.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer14.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer14.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer14.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer14.Location = New System.Drawing.Point(0, 351)
        Me.btnVisitPlayer14.Name = "btnVisitPlayer14"
        Me.btnVisitPlayer14.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer14.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer14.TabIndex = 14
        Me.btnVisitPlayer14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer14.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer13
        '
        Me.btnVisitPlayer13.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer13.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer13.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer13.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer13.Location = New System.Drawing.Point(0, 324)
        Me.btnVisitPlayer13.Name = "btnVisitPlayer13"
        Me.btnVisitPlayer13.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer13.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer13.TabIndex = 13
        Me.btnVisitPlayer13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer13.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer12
        '
        Me.btnVisitPlayer12.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer12.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer12.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer12.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer12.Location = New System.Drawing.Point(0, 297)
        Me.btnVisitPlayer12.Name = "btnVisitPlayer12"
        Me.btnVisitPlayer12.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer12.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer12.TabIndex = 12
        Me.btnVisitPlayer12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer12.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer21
        '
        Me.btnVisitPlayer21.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer21.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer21.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer21.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer21.Location = New System.Drawing.Point(0, 540)
        Me.btnVisitPlayer21.Name = "btnVisitPlayer21"
        Me.btnVisitPlayer21.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer21.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer21.TabIndex = 21
        Me.btnVisitPlayer21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer21.UseVisualStyleBackColor = False
        Me.btnVisitPlayer21.Visible = False
        '
        'btnVisitPlayer20
        '
        Me.btnVisitPlayer20.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer20.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer20.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer20.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer20.Location = New System.Drawing.Point(0, 513)
        Me.btnVisitPlayer20.Name = "btnVisitPlayer20"
        Me.btnVisitPlayer20.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer20.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer20.TabIndex = 20
        Me.btnVisitPlayer20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer20.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer19
        '
        Me.btnVisitPlayer19.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer19.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer19.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer19.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer19.Location = New System.Drawing.Point(0, 486)
        Me.btnVisitPlayer19.Name = "btnVisitPlayer19"
        Me.btnVisitPlayer19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer19.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer19.TabIndex = 19
        Me.btnVisitPlayer19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer19.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer18
        '
        Me.btnVisitPlayer18.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer18.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer18.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer18.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer18.Location = New System.Drawing.Point(0, 459)
        Me.btnVisitPlayer18.Name = "btnVisitPlayer18"
        Me.btnVisitPlayer18.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer18.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer18.TabIndex = 18
        Me.btnVisitPlayer18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer18.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer17
        '
        Me.btnVisitPlayer17.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer17.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer17.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer17.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer17.Location = New System.Drawing.Point(0, 432)
        Me.btnVisitPlayer17.Name = "btnVisitPlayer17"
        Me.btnVisitPlayer17.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer17.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer17.TabIndex = 17
        Me.btnVisitPlayer17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer17.UseVisualStyleBackColor = False
        '
        'btnVisitPlayer22
        '
        Me.btnVisitPlayer22.BackColor = System.Drawing.Color.DarkGray
        Me.btnVisitPlayer22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnVisitPlayer22.FlatAppearance.BorderSize = 0
        Me.btnVisitPlayer22.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPlayer22.ForeColor = System.Drawing.Color.White
        Me.btnVisitPlayer22.Location = New System.Drawing.Point(0, 567)
        Me.btnVisitPlayer22.Name = "btnVisitPlayer22"
        Me.btnVisitPlayer22.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnVisitPlayer22.Size = New System.Drawing.Size(190, 27)
        Me.btnVisitPlayer22.TabIndex = 22
        Me.btnVisitPlayer22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVisitPlayer22.UseVisualStyleBackColor = False
        Me.btnVisitPlayer22.Visible = False
        '
        'TmrTouches
        '
        Me.TmrTouches.Enabled = True
        Me.TmrTouches.Interval = 500
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(624, 135)
        Me.txtTime.Mask = "000:00"
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(45, 22)
        Me.txtTime.TabIndex = 294
        '
        'ChkAllData
        '
        Me.ChkAllData.AutoSize = True
        Me.ChkAllData.Location = New System.Drawing.Point(897, 167)
        Me.ChkAllData.Name = "ChkAllData"
        Me.ChkAllData.Size = New System.Drawing.Size(81, 19)
        Me.ChkAllData.TabIndex = 295
        Me.ChkAllData.Text = "Get all data"
        Me.ChkAllData.UseVisualStyleBackColor = True
        '
        'txtCommand
        '
        Me.txtCommand.BackColor = System.Drawing.Color.LightGray
        Me.txtCommand.Enabled = False
        Me.txtCommand.Location = New System.Drawing.Point(683, 163)
        Me.txtCommand.Name = "txtCommand"
        Me.txtCommand.Size = New System.Drawing.Size(62, 22)
        Me.txtCommand.TabIndex = 296
        Me.txtCommand.TabStop = False
        Me.txtCommand.Text = "3"
        '
        'btnRefreshTime
        '
        Me.btnRefreshTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Refresh_btn
        Me.btnRefreshTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefreshTime.Location = New System.Drawing.Point(672, 134)
        Me.btnRefreshTime.Name = "btnRefreshTime"
        Me.btnRefreshTime.Size = New System.Drawing.Size(24, 24)
        Me.btnRefreshTime.TabIndex = 297
        Me.btnRefreshTime.UseVisualStyleBackColor = True
        '
        'pnlInstructions
        '
        Me.pnlInstructions.Controls.Add(Me.lblInstructions)
        Me.pnlInstructions.Location = New System.Drawing.Point(581, 6)
        Me.pnlInstructions.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlInstructions.Name = "pnlInstructions"
        Me.pnlInstructions.Size = New System.Drawing.Size(193, 188)
        Me.pnlInstructions.TabIndex = 298
        Me.pnlInstructions.Visible = False
        '
        'lblInstructions
        '
        Me.lblInstructions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblInstructions.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstructions.Location = New System.Drawing.Point(0, 0)
        Me.lblInstructions.Margin = New System.Windows.Forms.Padding(6)
        Me.lblInstructions.Name = "lblInstructions"
        Me.lblInstructions.Size = New System.Drawing.Size(193, 188)
        Me.lblInstructions.TabIndex = 0
        Me.lblInstructions.Text = "Text here..."
        '
        'pnlVisitPrimary
        '
        Me.pnlVisitPrimary.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnlVisitPrimary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlVisitPrimary.Controls.Add(Me.btnRunWithBallVisit)
        Me.pnlVisitPrimary.Controls.Add(Me.btn50Visit)
        Me.pnlVisitPrimary.Controls.Add(Me.btnVisitPrimary)
        Me.pnlVisitPrimary.Controls.Add(Me.btnAerialVisit)
        Me.pnlVisitPrimary.Location = New System.Drawing.Point(987, 44)
        Me.pnlVisitPrimary.Name = "pnlVisitPrimary"
        Me.pnlVisitPrimary.Size = New System.Drawing.Size(213, 387)
        Me.pnlVisitPrimary.TabIndex = 300
        Me.pnlVisitPrimary.Visible = False
        '
        'btnRunWithBallVisit
        '
        Me.btnRunWithBallVisit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnRunWithBallVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunWithBallVisit.Location = New System.Drawing.Point(16, 285)
        Me.btnRunWithBallVisit.Name = "btnRunWithBallVisit"
        Me.btnRunWithBallVisit.Size = New System.Drawing.Size(179, 72)
        Me.btnRunWithBallVisit.TabIndex = 303
        Me.btnRunWithBallVisit.Tag = "17"
        Me.btnRunWithBallVisit.Text = "RWB"
        Me.btnRunWithBallVisit.UseVisualStyleBackColor = False
        '
        'btn50Visit
        '
        Me.btn50Visit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btn50Visit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn50Visit.Location = New System.Drawing.Point(15, 194)
        Me.btn50Visit.Name = "btn50Visit"
        Me.btn50Visit.Size = New System.Drawing.Size(179, 72)
        Me.btn50Visit.TabIndex = 301
        Me.btn50Visit.Tag = "20"
        Me.btn50Visit.Text = "50-50 win"
        Me.btn50Visit.UseVisualStyleBackColor = False
        '
        'btnVisitPrimary
        '
        Me.btnVisitPrimary.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnVisitPrimary.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitPrimary.Location = New System.Drawing.Point(15, 16)
        Me.btnVisitPrimary.Name = "btnVisitPrimary"
        Me.btnVisitPrimary.Size = New System.Drawing.Size(179, 72)
        Me.btnVisitPrimary.TabIndex = 302
        Me.btnVisitPrimary.Text = "Visit Team"
        Me.btnVisitPrimary.UseVisualStyleBackColor = False
        '
        'btnAerialVisit
        '
        Me.btnAerialVisit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAerialVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAerialVisit.Location = New System.Drawing.Point(15, 105)
        Me.btnAerialVisit.Name = "btnAerialVisit"
        Me.btnAerialVisit.Size = New System.Drawing.Size(179, 72)
        Me.btnAerialVisit.TabIndex = 298
        Me.btnAerialVisit.Tag = "18"
        Me.btnAerialVisit.Text = "Aerial Win"
        Me.btnAerialVisit.UseVisualStyleBackColor = False
        '
        'pnlTeamHighlight
        '
        Me.pnlTeamHighlight.BackColor = System.Drawing.Color.Gold
        Me.pnlTeamHighlight.Location = New System.Drawing.Point(988, 73)
        Me.pnlTeamHighlight.Name = "pnlTeamHighlight"
        Me.pnlTeamHighlight.Size = New System.Drawing.Size(15, 18)
        Me.pnlTeamHighlight.TabIndex = 301
        Me.pnlTeamHighlight.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(887, 139)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(41, 15)
        Me.lblPeriod.TabIndex = 302
        Me.lblPeriod.Text = "Period:"
        Me.lblPeriod.Visible = False
        '
        'cmbPeriod
        '
        Me.cmbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPeriod.FormattingEnabled = True
        Me.cmbPeriod.Location = New System.Drawing.Point(937, 135)
        Me.cmbPeriod.Name = "cmbPeriod"
        Me.cmbPeriod.Size = New System.Drawing.Size(41, 23)
        Me.cmbPeriod.TabIndex = 303
        Me.cmbPeriod.Visible = False
        '
        'chkOwnTouches
        '
        Me.chkOwnTouches.AutoSize = True
        Me.chkOwnTouches.Location = New System.Drawing.Point(765, 166)
        Me.chkOwnTouches.Name = "chkOwnTouches"
        Me.chkOwnTouches.Size = New System.Drawing.Size(131, 19)
        Me.chkOwnTouches.TabIndex = 304
        Me.chkOwnTouches.Text = "Display own Touches"
        Me.chkOwnTouches.UseVisualStyleBackColor = True
        Me.chkOwnTouches.Visible = False
        '
        'chkRevisit
        '
        Me.chkRevisit.AutoSize = True
        Me.chkRevisit.Location = New System.Drawing.Point(706, 165)
        Me.chkRevisit.Name = "chkRevisit"
        Me.chkRevisit.Size = New System.Drawing.Size(59, 19)
        Me.chkRevisit.TabIndex = 305
        Me.chkRevisit.Text = "Revisit"
        Me.chkRevisit.UseVisualStyleBackColor = True
        '
        'dgvTouches
        '
        Me.dgvTouches.AllowUserToAddRows = False
        Me.dgvTouches.AllowUserToDeleteRows = False
        Me.dgvTouches.AllowUserToResizeColumns = False
        Me.dgvTouches.AllowUserToResizeRows = False
        Me.dgvTouches.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTouches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTouches.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Revisit, Me.Perid, Me.Time, Me.Team, Me.Uniform, Me.Player, Me.Type, Me.Location, Me.Sequence, Me.Uid, Me.TeamId})
        Me.dgvTouches.ContextMenuStrip = Me.cmnTouches
        Me.dgvTouches.EnableHeadersVisualStyles = False
        Me.dgvTouches.Location = New System.Drawing.Point(582, 183)
        Me.dgvTouches.MultiSelect = False
        Me.dgvTouches.Name = "dgvTouches"
        Me.dgvTouches.ReadOnly = True
        Me.dgvTouches.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvTouches.RowHeadersVisible = False
        Me.dgvTouches.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvTouches.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTouches.Size = New System.Drawing.Size(401, 237)
        Me.dgvTouches.TabIndex = 306
        '
        'Revisit
        '
        Me.Revisit.FalseValue = "0"
        Me.Revisit.HeaderText = "Revisit"
        Me.Revisit.Name = "Revisit"
        Me.Revisit.ReadOnly = True
        Me.Revisit.ThreeState = True
        Me.Revisit.TrueValue = "1"
        Me.Revisit.Width = 43
        '
        'Perid
        '
        Me.Perid.HeaderText = "Pid"
        Me.Perid.Name = "Perid"
        Me.Perid.ReadOnly = True
        Me.Perid.Width = 23
        '
        'Time
        '
        Me.Time.HeaderText = "Time"
        Me.Time.Name = "Time"
        Me.Time.ReadOnly = True
        Me.Time.Width = 50
        '
        'Team
        '
        Me.Team.HeaderText = "Team"
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.Width = 44
        '
        'Uniform
        '
        Me.Uniform.HeaderText = "#"
        Me.Uniform.Name = "Uniform"
        Me.Uniform.ReadOnly = True
        Me.Uniform.Width = 21
        '
        'Player
        '
        Me.Player.HeaderText = "Player"
        Me.Player.Name = "Player"
        Me.Player.ReadOnly = True
        Me.Player.Width = 72
        '
        'Type
        '
        Me.Type.HeaderText = "Type"
        Me.Type.Name = "Type"
        Me.Type.ReadOnly = True
        Me.Type.Width = 84
        '
        'Location
        '
        Me.Location.HeaderText = "Location"
        Me.Location.Name = "Location"
        Me.Location.ReadOnly = True
        Me.Location.Width = 55
        '
        'Sequence
        '
        Me.Sequence.HeaderText = "Sno"
        Me.Sequence.Name = "Sequence"
        Me.Sequence.ReadOnly = True
        Me.Sequence.Visible = False
        '
        'Uid
        '
        Me.Uid.HeaderText = "Uid"
        Me.Uid.Name = "Uid"
        Me.Uid.ReadOnly = True
        Me.Uid.Visible = False
        '
        'TeamId
        '
        Me.TeamId.HeaderText = "TeamId"
        Me.TeamId.Name = "TeamId"
        Me.TeamId.ReadOnly = True
        Me.TeamId.Visible = False
        '
        'UdcSoccerField1
        '
        Me.UdcSoccerField1.Location = New System.Drawing.Point(234, 3)
        Me.UdcSoccerField1.Name = "UdcSoccerField1"
        Me.UdcSoccerField1.Size = New System.Drawing.Size(342, 228)
        Me.UdcSoccerField1.TabIndex = 286
        Me.UdcSoccerField1.USFAwayTeamName = ""
        Me.UdcSoccerField1.USFDisplayArrow = True
        Me.UdcSoccerField1.USFDisplayTeamName = True
        Me.UdcSoccerField1.USFDisplayTouchInstructions = False
        Me.UdcSoccerField1.USFEventText = ""
        Me.UdcSoccerField1.USFHomeTeamName = ""
        Me.UdcSoccerField1.USFHomeTeamOnLeft = True
        Me.UdcSoccerField1.USFMaximumMarksAllowed = 2
        Me.UdcSoccerField1.USFNextMark = 1
        '
        'frmTouches
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(995, 426)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgvTouches)
        Me.Controls.Add(Me.chkRevisit)
        Me.Controls.Add(Me.pnlVisitPrimary)
        Me.Controls.Add(Me.pnlHomePrimary)
        Me.Controls.Add(Me.chkOwnTouches)
        Me.Controls.Add(Me.cmbPeriod)
        Me.Controls.Add(Me.lblPeriod)
        Me.Controls.Add(Me.pnlTouchDisplay)
        Me.Controls.Add(Me.pnlTeamHighlight)
        Me.Controls.Add(Me.btnRefreshTime)
        Me.Controls.Add(Me.txtCommand)
        Me.Controls.Add(Me.ChkAllData)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlTouchTypes)
        Me.Controls.Add(Me.UdcSoccerField1)
        Me.Controls.Add(Me.pnlVisit)
        Me.Controls.Add(Me.txtTime)
        Me.Controls.Add(Me.pnlHome)
        Me.Controls.Add(Me.lnkRefreshPlayers)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.pnlVisitBench)
        Me.Controls.Add(Me.pnlHomeBench)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.chkEnableVoice)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblTouchType)
        Me.Controls.Add(Me.pnlInstructions)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frmTouches"
        Me.pnlVisitBench.ResumeLayout(False)
        Me.pnlHomeBench.ResumeLayout(False)
        Me.pnlTouchTypes.ResumeLayout(False)
        Me.pnlHomePrimary.ResumeLayout(False)
        Me.pnlTouchDisplay.ResumeLayout(False)
        Me.pnlTouchDisplay.PerformLayout()
        Me.cmnTouches.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picMark2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMark1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHome.ResumeLayout(False)
        Me.pnlVisit.ResumeLayout(False)
        Me.pnlInstructions.ResumeLayout(False)
        Me.pnlVisitPrimary.ResumeLayout(False)
        CType(Me.dgvTouches, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents slbReporterEntry As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnVisitPlayer11 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer6 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer7 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer8 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer9 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer10 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer1 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer2 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer3 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer4 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer5 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer11 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer6 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer7 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer8 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer9 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer10 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer1 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer2 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer3 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer4 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer5 As System.Windows.Forms.Button
    Friend WithEvents pnlVisitBench As System.Windows.Forms.Panel
    Friend WithEvents pnlHomeBench As System.Windows.Forms.Panel
    Friend WithEvents btnHomeSub28 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub29 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub30 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub25 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub26 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub27 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub22 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub23 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub24 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub19 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub20 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub21 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub16 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub17 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub18 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub13 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub14 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub15 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub10 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub11 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub12 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub7 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub8 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub9 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub4 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub5 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub6 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub1 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub2 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub3 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub10 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub11 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub16 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub17 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub18 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub13 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub14 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub15 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub7 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub8 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub9 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub4 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub5 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub6 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub1 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub2 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub3 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub12 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub28 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub29 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub30 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub25 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub26 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub27 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub22 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub23 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub24 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub19 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub20 As System.Windows.Forms.Button
    Friend WithEvents btnVisitSub21 As System.Windows.Forms.Button
    Friend WithEvents lblTouchType As System.Windows.Forms.Label
    Friend WithEvents pnlTouchTypes As System.Windows.Forms.Panel
    Friend WithEvents btnTypePass As System.Windows.Forms.Button
    Friend WithEvents btnTypeOther As System.Windows.Forms.Button
    Friend WithEvents btnTypeTackle As System.Windows.Forms.Button
    Friend WithEvents btnTypeCatch As System.Windows.Forms.Button
    Friend WithEvents btnTypeSave As System.Windows.Forms.Button
    Friend WithEvents btnTypeBlock As System.Windows.Forms.Button
    Friend WithEvents btnTypeInt As System.Windows.Forms.Button
    Friend WithEvents pnlTouchDisplay As System.Windows.Forms.Panel
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents chkEnableVoice As System.Windows.Forms.CheckBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents lblDispLocation As System.Windows.Forms.Label
    Friend WithEvents lblDispType As System.Windows.Forms.Label
    Friend WithEvents lblUniform As System.Windows.Forms.Label
    Friend WithEvents lblValLocation As System.Windows.Forms.Label
    Friend WithEvents lblValUniform As System.Windows.Forms.Label
    Friend WithEvents lblValType As System.Windows.Forms.Label
    Friend WithEvents lblValTeam As System.Windows.Forms.Label
    Friend WithEvents lnkRefreshPlayers As System.Windows.Forms.LinkLabel
    Friend WithEvents UdcSoccerField1 As STATS.SoccerDataCollection.udcSoccerField
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents picMark1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblSelection1 As System.Windows.Forms.Label
    Friend WithEvents lblFieldY As System.Windows.Forms.Label
    Friend WithEvents lblFieldX As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnTypeGoalKick As System.Windows.Forms.Button
    Friend WithEvents btnTypeTouch As System.Windows.Forms.Button
    Friend WithEvents btnTypeGoalAttempt As System.Windows.Forms.Button
    Friend WithEvents btnTypeClearance As System.Windows.Forms.Button
    Friend WithEvents btnTypeThrowIn As System.Windows.Forms.Button
    Friend WithEvents btnTypeRunWithBall As System.Windows.Forms.Button
    Friend WithEvents tltpTouches As System.Windows.Forms.ToolTip
    Friend WithEvents cmnTouches As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents pnlHome As System.Windows.Forms.Panel
    Friend WithEvents pnlVisit As System.Windows.Forms.Panel
    Friend WithEvents TmrTouches As System.Windows.Forms.Timer
    Friend WithEvents txtTime As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ChkAllData As System.Windows.Forms.CheckBox
    Friend WithEvents txtCommand As System.Windows.Forms.TextBox
    Friend WithEvents btnHomePlayer16 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer15 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer14 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer13 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer12 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer21 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer20 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer19 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer18 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer17 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer22 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer38 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer37 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer36 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer35 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer34 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer41 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer40 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer39 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer27 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer26 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer25 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer24 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer23 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer32 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer31 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer30 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer29 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer28 As System.Windows.Forms.Button
    Friend WithEvents btnHomePlayer33 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer16 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer15 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer14 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer13 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer12 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer21 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer20 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer19 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer18 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer17 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer22 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer38 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer37 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer36 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer35 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer34 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer41 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer40 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer39 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer27 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer26 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer25 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer24 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer23 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer32 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer31 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer30 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer29 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer28 As System.Windows.Forms.Button
    Friend WithEvents btnVisitPlayer33 As System.Windows.Forms.Button
    Friend WithEvents btnTypeIntPass As System.Windows.Forms.Button
    Friend WithEvents btnRefreshTime As System.Windows.Forms.Button
    Friend WithEvents BtnIntPass As System.Windows.Forms.Button
    Friend WithEvents btnGKHand As System.Windows.Forms.Button
    Friend WithEvents btnGKThrow As System.Windows.Forms.Button
    Friend WithEvents btn50Win As System.Windows.Forms.Button
    Friend WithEvents btnAirWin As System.Windows.Forms.Button
    Friend WithEvents BtnIntBPass As System.Windows.Forms.Button
    Friend WithEvents pnlInstructions As System.Windows.Forms.Panel
    Friend WithEvents lblInstructions As System.Windows.Forms.Label
    Friend WithEvents picMark2 As System.Windows.Forms.PictureBox
    Friend WithEvents lblSelection2 As System.Windows.Forms.Label
    Friend WithEvents lblDispLocation2 As System.Windows.Forms.Label
    Friend WithEvents lblLocationVal2 As System.Windows.Forms.Label
    Friend WithEvents pnlHomePrimary As System.Windows.Forms.Panel
    Friend WithEvents btn50Home As System.Windows.Forms.Button
    Friend WithEvents btnAerialHome As System.Windows.Forms.Button
    Friend WithEvents btnHomePrimary As System.Windows.Forms.Button
    Friend WithEvents pnlVisitPrimary As System.Windows.Forms.Panel
    Friend WithEvents btnVisitPrimary As System.Windows.Forms.Button
    Friend WithEvents btn50Visit As System.Windows.Forms.Button
    Friend WithEvents btnAerialVisit As System.Windows.Forms.Button
    Friend WithEvents pnlTeamHighlight As System.Windows.Forms.Panel
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cmbPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkOwnTouches As System.Windows.Forms.CheckBox
    Friend WithEvents btnRunWithBallVisit As System.Windows.Forms.Button
    Friend WithEvents btnRunWithBallHome As System.Windows.Forms.Button
    Friend WithEvents chkRevisit As System.Windows.Forms.CheckBox
    Friend WithEvents dgvTouches As System.Windows.Forms.DataGridView
    Friend WithEvents BtnKpPickUp As System.Windows.Forms.Button
    Friend WithEvents Revisit As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Perid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Time As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Team As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Uniform As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Player As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Location As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sequence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Uid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TeamId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
