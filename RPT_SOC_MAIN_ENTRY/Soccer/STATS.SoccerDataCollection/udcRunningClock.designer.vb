﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class udcRunningClock
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblClockDisplay = New System.Windows.Forms.Label()
        Me.tmrRunningClock = New System.Windows.Forms.Timer(Me.components)
        Me.lblHiddenClockDisplay = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblClockDisplay
        '
        Me.lblClockDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClockDisplay.Location = New System.Drawing.Point(2, 2)
        Me.lblClockDisplay.Name = "lblClockDisplay"
        Me.lblClockDisplay.Size = New System.Drawing.Size(40, 13)
        Me.lblClockDisplay.TabIndex = 0
        Me.lblClockDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmrRunningClock
        '
        '
        'lblHiddenClockDisplay
        '
        Me.lblHiddenClockDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHiddenClockDisplay.Location = New System.Drawing.Point(0, 2)
        Me.lblHiddenClockDisplay.Name = "lblHiddenClockDisplay"
        Me.lblHiddenClockDisplay.Size = New System.Drawing.Size(40, 13)
        Me.lblHiddenClockDisplay.TabIndex = 1
        Me.lblHiddenClockDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblHiddenClockDisplay.Visible = False
        '
        'udcRunningClock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblHiddenClockDisplay)
        Me.Controls.Add(Me.lblClockDisplay)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "udcRunningClock"
        Me.Size = New System.Drawing.Size(40, 17)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblClockDisplay As System.Windows.Forms.Label
    Friend WithEvents tmrRunningClock As System.Windows.Forms.Timer
    Friend WithEvents lblHiddenClockDisplay As System.Windows.Forms.Label

End Class
