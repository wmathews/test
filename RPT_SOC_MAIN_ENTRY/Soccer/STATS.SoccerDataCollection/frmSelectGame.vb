﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
Imports System.Text

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmSelectGame
' Author        : Shirley Ranjini
' Created Date  : 17th Nov,2009
' Description   : This form is used to select the games ..Mainly used for Module2 and OPS login                  
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region


Public Class frmSelectGame

#Region " Constants & Variables "
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objclsSelectGame As clsSelectGame = clsSelectGame.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private dsGames As DataSet
    Private m_dsDisplayGames As New DataSet
    Private m_dsDemoGame As New DataSet
    Dim m_dsGames As New DataSet
    Private lagid As Integer
    Private lsupport As New languagesupport
    Private m_dsQAGames As New DataSet

    Dim strmessage As String = "Error inserting data from Oracle"

    Dim strmessage2 As String = "to Sql tables.., Please try again"
    Dim strmessage3 As String = "The selected game data will be reloaded."
    Dim strmessage4 As String = "Currently there is no game available"
    Dim strmessage1 As String = "Currently there is no game(s) available"
    Dim strmessage5 As String = "Please select a game !!"

    Private m_arrRBLoc(4) As Integer
    Private m_arrRBVisibility(4) As Boolean
    Private m_RadioButtonCnt As Integer = 0

    Dim arrFeedButtonPos(3) As Integer

#End Region
    

#Region " Event Handlers "

    Private Sub frmSelectGame_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmSelectGame", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            lblTimeZone.Visible = True
            lblTimeZone.Text = "All times are displayed in " & m_objGameDetails.TimeZoneName & " time zone."
            ' Loadgames()

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                radFeedA.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radFeedA.Text)
                radFeedB.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radFeedB.Text)
                lblTimeZone.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTimeZone.Text)
                btnOk.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnOk.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)


                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "GameCode")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Kickoff")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Competition")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Home Team")
                Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Away Team")
                Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Tier")
                Dim g10 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Task")
                Dim g11 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "ModuleID")
                Dim g12 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "SerialNo")


                lvwGames.Columns(0).Text = g4
                lvwGames.Columns(1).Text = g5
                lvwGames.Columns(2).Text = g6
                lvwGames.Columns(3).Text = g7
                lvwGames.Columns(4).Text = g8
                lvwGames.Columns(6).Text = g9
                lvwGames.Columns(7).Text = g10
                lvwGames.Columns(8).Text = g11
                'lvwGames.Columns(9).Text = g12

                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
            End If

            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                BindOPSGames()
                radFeedA.Visible = True
                radFeedB.Visible = True
                radFeedC.Visible = False
                radFeedD.Visible = False
                radFeedA.Checked = True
                radFeedB.Checked = False

            Else
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then

                    'Code Added by chandrasekhar to Load the Sameple Game Details into Listview
                   
                    LoadDemoGames()
                    radFeedA.Visible = False
                    radFeedB.Visible = False
                    radFeedC.Visible = False
                    radFeedD.Visible = False
    


                ElseIf m_objGameDetails.ModuleID = 2 Then
                    Loadgames()
                    radFeedA.Visible = False
                    radFeedB.Visible = False
                    radFeedC.Visible = False
                    radFeedD.Visible = False
                Else
                    'Load all games - QA
                    GetFeedButtonPos()
                    LoadallGames()
            End If
             
            End If

            'GetFeedButtonPos()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Function GetAllLeagues() As DataSet
        Try
            Return m_objclsSelectGame.GetAllLeagues()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub GetFeedButtonPos()
        Try
            arrFeedButtonPos(0) = radFeedA.Left
            arrFeedButtonPos(1) = radFeedB.Left
            arrFeedButtonPos(2) = radFeedC.Left
            arrFeedButtonPos(3) = radFeedD.Left
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If m_objLoginDetails.GameMode <> clsLoginDetails.m_enmGameMode.DEMO And m_objGameDetails.ModuleID <> 2 Then
                If lvwGames.Items.Count > 0 Then
                    If lvwGames.SelectedItems.Count > 0 Then
                        Dim drs() As DataRow
                        Dim intFeedNumber As Integer
                        Dim StrGameCodes As String = String.Empty

                        For Each lvwitm As ListViewItem In lvwGames.SelectedItems


                            'Check for same game selected - if so exit from here
                            Dim intGameCode As String = lvwitm.SubItems(0).Text
                            Dim intModuleID As String = lvwitm.SubItems(10).Text

                            If radFeedA.Checked = True Then
                                intFeedNumber = 1
                                drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_A = " & intFeedNumber)
                            ElseIf radFeedB.Checked = True Then
                                intFeedNumber = 2
                                drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_B = " & intFeedNumber)
                            ElseIf radFeedC.Checked = True Then
                                intFeedNumber = 3
                                drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_C = " & intFeedNumber)
                            ElseIf radFeedD.Checked = True Then
                                intFeedNumber = 4
                                drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_D = " & intFeedNumber)
                            End If
                        Next

                        If drs IsNot Nothing Then 'sep 5 2012
                            '''''''''''''
                            ''''Check Backend for game/feed - if same
                            Dim dsLclGameDetails As DataSet
                            dsLclGameDetails = m_objclsSelectGame.FetchLclGameTable(CInt(drs(0).Item("GAME_CODE").ToString), intFeedNumber)

                            If dsLclGameDetails.Tables(0).Rows.Count > 0 Then
                                Me.DialogResult = Windows.Forms.DialogResult.Cancel
                                Exit Sub
                            End If
                            ''''''
                        End If
                    End If
                End If
            End If
            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                frmMain.tmrRefresh.Stop()

                If lvwGames.Items.Count > 0 Then
                    If lvwGames.SelectedItems.Count > 0 Then
                        'AUDIT TRIAL
                        FillGameDetails()
                        Me.Cursor = Cursors.Default
                        Me.DialogResult = Windows.Forms.DialogResult.OK
                    Else
                        Me.Cursor = Cursors.Default
                        MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                End If
            Else

                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    If lvwGames.Items.Count > 0 Then
                        If lvwGames.SelectedItems.Count = 0 Then
                            MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Try
                        End If
                        For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                            m_objGameDetails.GameCode = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("GAME_CODE").ToString)
                            m_objGameDetails.CoverageLevel = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("TIER").ToString)
                            m_objGameDetails.LeagueID = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                            'm_objGameDetails.ModuleID = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("MODULE_ID").ToString)
                            m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                        Next
                    End If
                Else

                    If m_objGameDetails.ModuleID = 2 Then
                        If lvwGames.Items.Count > 0 Then
                            If lvwGames.SelectedItems.Count = 0 Then
                                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Try
                            End If
                            For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                                m_objGameDetails.GameLastPlayed = m_objGameDetails.GameCode
                                m_objGameDetails.GameCode = CInt(dsGames.Tables(0).Rows(lvwitm.Index).Item("GAME_CODE").ToString)
                                m_objGameDetails.LeagueID = CInt(dsGames.Tables(0).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                                m_objclsTeamSetup.HomeShirtColor = Nothing
                                m_objclsTeamSetup.HomeShortColor = Nothing
                                m_objclsTeamSetup.AwayShirtColor = Nothing
                                m_objclsTeamSetup.AwayShortColor = Nothing
                                m_objGameDetails.HomeTeamColor = Color.WhiteSmoke
                                m_objGameDetails.VisitorTeamColor = Color.WhiteSmoke
                            Next
                        End If
                    Else
                        FillReporterGameDetails()
                        m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                    End If

                End If
            End If
            m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwGames_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwGames.DoubleClick
        Try
            btnOk_Click(btnOk, New EventArgs)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwGames_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwGames.Click
        Try
            lagid = 0
            Dim cnt As Integer = 0
            If m_objGameDetails.ModuleID = 2 Then
                m_objGameDetails.IsReporterRemoved = False
            End If
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                    If lvwGames.Items.Count > 0 Then
                        For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                           
                            Dim intGameCode As String = lvwitm.SubItems(0).Text
                            Dim intModuleID As String = lvwitm.SubItems(9).Text
                            Dim intSerialNo As String = lvwitm.SubItems(11).Text

                            lvwGames.Items(lvwitm.Index).Selected = True
                            lvwGames.Select()

                            Dim drs() As DataRow = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                            If drs.Length > 0 Then
                                m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                                m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                                m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)
                                m_objGameDetails.SerialNo = CInt(drs(0).Item("SERIAL_NO").ToString)
                                m_objclsTeamSetup.HomeShirtColor = Nothing
                                m_objclsTeamSetup.HomeShortColor = Nothing
                                m_objclsTeamSetup.AwayShirtColor = Nothing
                                m_objclsTeamSetup.AwayShortColor = Nothing
                                lagid = 1
                            End If

                            Dim dr() As DataRow = m_dsGames.Tables(2).Select("Game_code = " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND REPORTER_ID_B IS NOT NULL ")
                            If dr.Length > 0 Then
                                radFeedB.Enabled = True
                            Else
                                radFeedB.Enabled = False
                            End If

                        Next
                    End If
                ElseIf m_objGameDetails.ModuleID <> 2 Then
                    radFeedA.Visible = False
                    radFeedB.Visible = False
                    radFeedC.Visible = False
                    radFeedD.Visible = False

                    radFeedA.Checked = False
                    radFeedB.Checked = False
                    radFeedC.Checked = False
                    radFeedD.Checked = False

                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                        Dim intGameCode As String = lvwitm.SubItems(0).Text
                        Dim intModuleID As String = lvwitm.SubItems(10).Text
                        Dim intSerialNo As String = lvwitm.SubItems(12).Text

                        Dim drFeedA() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND REPORTER_ROLE_A = 'A' AND SERIAL_NO IN(2,3,4)")
                        Dim drFeedB() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND REPORTER_ROLE_B = 'B' AND SERIAL_NO IN(2,3,4)")
                        Dim drFeedC() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND REPORTER_ROLE_C = 'C' AND SERIAL_NO IN(2,3,4)")
                        Dim drFeedD() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND REPORTER_ROLE_D = 'D' AND SERIAL_NO IN(2,3,4)")


                        If drFeedA.Length > 0 Then
                            radFeedA.Visible = True
                            radFeedA.Enabled = True
                            cnt = cnt + 1
                        End If

                        If drFeedB.Length > 0 Then
                            radFeedB.Visible = True
                            radFeedB.Enabled = True
                            cnt = cnt + 1
                        End If

                        If drFeedC.Length > 0 Then
                            radFeedC.Visible = True
                            radFeedC.Enabled = True
                            cnt = cnt + 1
                        End If
                        If drFeedD.Length > 0 Then
                            radFeedD.Visible = True
                            radFeedD.Enabled = True
                            cnt = cnt + 1
                        End If
                    Next

                    Dim intPosIndex As Integer = 0
                    If (radFeedA.Visible = True) Then
                        radFeedA.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex += 1
                    End If
                    If (radFeedB.Visible = True) Then
                        radFeedB.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex += 1
                    End If
                    If (radFeedC.Visible = True) Then
                        radFeedC.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex += 1
                    End If
                    If (radFeedD.Visible = True) Then
                        radFeedD.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex = 0
                    End If

                    If cnt = 1 Then
                        If radFeedA.Visible = True Then
                            radFeedA.Checked = True
                        ElseIf radFeedB.Visible = True Then
                            radFeedB.Checked = True
                        ElseIf radFeedC.Visible = True Then
                            radFeedC.Checked = True
                        ElseIf radFeedD.Visible = True Then
                            radFeedD.Checked = True
                        End If
                    End If
                End If
            Else
                If lvwGames.Items.Count > 0 Then
                    If lvwGames.SelectedItems.Count = 0 Then
                        MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                        m_objGameDetails.GameCode = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("GAME_CODE").ToString)
                        m_objGameDetails.CoverageLevel = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("TIER").ToString)
                        m_objGameDetails.LeagueID = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                        m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                    Next
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CheckFeed(ByVal sender As Object, ByVal e As System.EventArgs) Handles radFeedA.CheckedChanged, radFeedB.CheckedChanged, radFeedC.CheckedChanged, radFeedD.CheckedChanged
        Try
            For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                lvwGames.Items(lvwitm.Index).Selected = True
                lvwGames.Select()
            Next
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region " User Methods "

    Private Sub Loadgames()
        Try
            Dim StrGameDate As String = ""
            dsGames = m_objGeneral.GetGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserId)
            If Not dsGames Is Nothing Then
                If (dsGames.Tables(0).Rows.Count > 0) Then
                    For intRowCount = 0 To dsGames.Tables(0).Rows.Count - 1
                        Dim lvwGameSchedule As New ListViewItem(dsGames.Tables(0).Rows(intRowCount).Item("GAME_CODE").ToString)
                        'displaying the gamedate based on his local time Zone
                        'Dim Dt As DateTime = DateTimeHelper.GetDate(dsGames.Tables(0).Rows(intRowCount).Item("KICKOFF").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                        ''Arindam 2-2-10
                        Dim Dt As DateTime '= DateTimeHelper.GetDate(CStr(dsGames.Tables(0).Rows(intRowCount).Item("KICKOFF")), DateTimeHelper.InputFormat.CurrentFormat)
                        Dt = CDate(Format(dsGames.Tables(0).Rows(intRowCount).Item("KICKOFF"), "yyyy-MM-dd HH:mm:ss tt"))
                        If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                            StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                            lvwGameSchedule.SubItems.Add(StrGameDate)
                        Else
                            StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                            lvwGameSchedule.SubItems.Add(StrGameDate)
                        End If
                        lvwGameSchedule.SubItems.Add(dsGames.Tables(0).Rows(intRowCount).Item("Competition").ToString)
                        lvwGameSchedule.SubItems.Add(dsGames.Tables(0).Rows(intRowCount).Item("HOMETEAM").ToString)
                        lvwGameSchedule.SubItems.Add(dsGames.Tables(0).Rows(intRowCount).Item("AWAYTEAM").ToString)
                        lvwGameSchedule.SubItems.Add(dsGames.Tables(0).Rows(intRowCount).Item("Tier").ToString)
                        lvwGameSchedule.SubItems.Add(dsGames.Tables(0).Rows(intRowCount).Item("Task").ToString)
                        lvwGameSchedule.SubItems.Add(dsGames.Tables(0).Rows(intRowCount).Item("MODULE_ID").ToString)
                        lvwGames.Items.Add(lvwGameSchedule)
                    Next

                    'highlighting the game , in which the reporter is currently scoring
                    For i As Integer = 0 To lvwGames.Items.Count - 1
                        If CDbl(lvwGames.Items(i).SubItems(0).Text) = m_objGameDetails.GameCode Then
                            lvwGames.Items(i).Selected = True
                            lvwGames.Select()
                            lvwGames.Items(i).Focused = True
                        End If
                    Next
                End If
            Else
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadDemoGames()
        Try

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                lvwGames.Items.Clear()
                m_dsDemoGame = m_objGeneral.GetDemoGameDetails()
                If Not m_dsDemoGame Is Nothing Then
                    If (m_dsDemoGame.Tables(1).Rows.Count > 0) Then
                        For intRowCount = 0 To m_dsDemoGame.Tables(1).Rows.Count - 1
                            Dim lvwGameSchedule As New ListViewItem(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("GAME_CODE").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("GAME_DATE").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Competition").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("HOMETEAM").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("AWAYTEAM").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Tier").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Task").ToString)
                            lvwGameSchedule.SubItems.Add("")
                            lvwGameSchedule.SubItems.Add("")
                            lvwGameSchedule.SubItems.Add("")
                            lvwGames.Items.Add(lvwGameSchedule)
                        Next
                    Else
                        Dim lvwGameSchedule As New ListViewItem(m_dsDemoGame.Tables(0).Rows(0).Item("GAME_CODE").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("GAME_DATE").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Competition").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("HOMETEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("AWAYTEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Tier").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Task").ToString)

                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGames.Items.Add(lvwGameSchedule)
                    End If
                    'highlighting the game , in which the reporter is currently scoring
                    For i As Integer = 0 To lvwGames.Items.Count - 1
                        If CDbl(lvwGames.Items(i).SubItems(0).Text) = m_objGameDetails.GameCode Then
                            lvwGames.Items(i).Selected = True
                            lvwGames.Select()
                            lvwGames.Items(i).Focused = True
                        End If
                    Next
                Else
                    MessageDialog.Show(strmessage4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub BindOPSGames()
        Try
            m_dsGames = m_objclsSelectGame.ValidateUserDetails("ops", "ops", m_objGameDetails.languageid)
            Dim drs() As DataRow
            m_dsDisplayGames = m_dsGames.Clone()
            For intTableCount As Integer = 0 To m_dsGames.Tables.Count - 1
                If intTableCount = 2 Then
                    drs = m_dsGames.Tables(intTableCount).Select("SERIAL_NO=1")
                    For Each DR As DataRow In drs
                        m_dsDisplayGames.Tables(intTableCount).ImportRow(DR)
                    Next
                Else
                    drs = m_dsGames.Tables(intTableCount).Select()
                    For Each DR As DataRow In drs
                        m_dsDisplayGames.Tables(intTableCount).ImportRow(DR)
                    Next
                End If
            Next

            lvwGames.Items.Clear()
            Dim intRowCount As Integer
            Dim StrGameDate As String = ""
            If (m_dsDisplayGames.Tables(2).Rows.Count > 0) Then
                For intRowCount = 0 To m_dsDisplayGames.Tables(2).Rows.Count - 1
                    Dim lvwGameSchedule As New ListViewItem(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("GAME_CODE").ToString)

                    'displaying the gamedate based on his local time Zone
                    Dim Dt As DateTime = DateTimeHelper.GetDate(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                    If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                        StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                        lvwGameSchedule.SubItems.Add(StrGameDate)
                    Else
                        StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                        lvwGameSchedule.SubItems.Add(StrGameDate)
                    End If
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("Competition").ToString)
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("HOMETEAM").ToString)
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("AWAYTEAM").ToString)
                    Dim Tier As String = ""
                    If m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_A") IsNot DBNull.Value Then
                        Tier = m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_A").ToString
                    ElseIf m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_B") IsNot DBNull.Value Then
                        Tier = m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_B").ToString
                    End If

                    If Tier = "0" Or Tier = "-1" Then
                        Continue For
                    End If
                    lvwGameSchedule.SubItems.Add(Tier)
                    'lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("Task").ToString)
                    lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("MODULE_ID").ToString)
                    lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("SERIAL_NO").ToString)
                    lvwGames.Items.Add(lvwGameSchedule)
                Next
                'highlighting the game , in which the reporter is currently scoring
                For i As Integer = 0 To lvwGames.Items.Count - 1
                    If CDbl(lvwGames.Items(i).SubItems(0).Text) = m_objGameDetails.GameCode And CDbl(lvwGames.Items(i).SubItems(9).Text) = m_objGameDetails.ModuleID Then
                        lvwGames.Items(i).Selected = True
                        lvwGames.Select()
                        lvwGames.Items(i).Focused = True
                    End If
                Next
                radFeedA.Checked = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FillGameDetails()
        Try
            Dim StrGameCodes As String = String.Empty

            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                If lvwGames.Items.Count > 0 Then
                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                        Dim intGameCode As String = lvwitm.SubItems(0).Text
                        Dim intModuleID As String = lvwitm.SubItems(9).Text
                        Dim intSerialNo As String = lvwitm.SubItems(11).Text

                        lvwGames.Items(lvwitm.Index).Selected = True
                        lvwGames.Select()

                        Dim drs() As DataRow = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                        If drs.Length > 0 Then
                            m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                            m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                            m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)

                            m_objGameDetails.ModuleName = drs(0).Item("Task").ToString
                            m_objGameDetails.AwayTeamID = CInt(drs(0).Item("AWAYTEAMID").ToString)
                            m_objGameDetails.AwayTeamAbbrev = drs(0).Item("AWAYABBREV").ToString
                            m_objGameDetails.AwayTeam = drs(0).Item("AWAYTEAM").ToString
                            m_objGameDetails.HomeTeamID = CInt(drs(0).Item("HOMETEAMID").ToString)
                            m_objGameDetails.HomeTeamAbbrev = drs(0).Item("HOMEABBREV").ToString
                            m_objGameDetails.HomeTeam = drs(0).Item("HOMETEAM").ToString
                            m_objGameDetails.LeagueID = CInt(drs(0).Item("LEAGUE_ID").ToString)
                            m_objGameDetails.LeagueName = drs(0).Item("Competition").ToString
                            m_objGameDetails.FieldID = CInt(drs(0).Item("FIELD_ID").ToString)
                            If radFeedA.Checked = True Then
                                m_objGameDetails.FeedNumber = 1
                            ElseIf radFeedB.Checked = True Then
                                m_objGameDetails.FeedNumber = 2
                            End If
                            'GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                            m_objGameDetails.ReporterRoleDisplay = "Operation Desk" ' CStr(IIf(CInt(m_dsGames.Tables(2).Rows(lvwitm.Index).Item("SERIAL_NO").ToString) = 1, "MAIN", "ASSISTER"))
                            m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE").ToString
                            m_objGameDetails.SerialNo = Convert.ToInt32(drs(0).Item("SERIAL_NO"))

                            lagid = 1
                        End If
                    Next

                    StrGameCodes = CStr(m_objGameDetails.GameCode)

                    'T1 WORKFLOW STARTS HERE 
                    Dim StrReporterRole As String = ""
                    Dim m_dsDataCountForRestart As DataSet
                    Dim m_dsRestartData As DataSet

                    If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                        'FOR PRIMARY REPORTER CHECK IF THE DATA EXISTS IN LOCAL SQL
                        m_dsDataCountForRestart = m_objclsSelectGame.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                            'IF SO CONTINUE FROM THE PLACE WHERE HE LEFT
                            MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            sstMain.Items(0).Text = "The game is reloading..."
                            m_objGameDetails.IsRestartGame = True
                            m_dsRestartData = m_objclsSelectGame.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                            m_objGameDetails.RestartData = m_dsRestartData
                        Else
                            'WRITE CODE IF SOME OTHER GAME EXITS IN HIS LOCAL SQL..
                            m_objGameDetails.IsRestartGame = False
                            'DOWNLOAD FROM ORACLE AND FILL THE LOCAL SQL TABLES
                            sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                            Application.DoEvents()
                            FillLocalSQLTables(StrGameCodes)
                            'DOWNLOAD ORACLE SP FILLS THE PBP TABLE 
                            'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                            m_dsDataCountForRestart = m_objclsSelectGame.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                            If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                                'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                                sstMain.Items(0).Text = "The game is reloading..."
                                MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                m_objGameDetails.IsRestartGame = True
                                m_dsRestartData = m_objclsSelectGame.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                                m_objGameDetails.RestartData = m_dsRestartData
                            End If
                            InsertCurrentGame()
                            If m_objLoginDetails.UserId = -1 Then
                                GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                            End If
                        End If

                    Else 'FOR SECONDARY REPORTER (ASSISTERS)
                        m_objGameDetails.IsRestartGame = False
                        'DOWNLOAD AND FILL THE LOCAL SQL TABLES
                        sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                        Application.DoEvents()
                        FillLocalSQLTables(StrGameCodes)
                        'DOWNLOAD ORACLE SP FILLS THE PBP TABLE 
                        'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                        m_dsDataCountForRestart = m_objclsSelectGame.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                            'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                            sstMain.Items(0).Text = "The Game is reloading..."
                            MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            m_objGameDetails.IsRestartGame = True
                            m_dsRestartData = m_objclsSelectGame.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                            m_objGameDetails.RestartData = m_dsRestartData
                        End If
                        InsertCurrentGame()
                        If m_objLoginDetails.UserId = -1 Then
                            GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                        End If
                    End If
                End If
                m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub FillLocalSQLTables(ByVal StrGameCode As String)
        Try
            ' If m_objLogin.DownloadSoccerData(m_objLoginDetails.UserId, m_objLoginDetails.LanguageID, CInt(StrGameCode), m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID) = False Then
            If m_objclsSelectGame.DownloadSoccerData(m_objLoginDetails.UserId, m_objLoginDetails.LanguageID, StrGameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID, m_objGameDetails.ReporterRole, m_objGameDetails.CoverageLevel, m_objGameDetails.LeagueID) = False Then
                MessageDialog.Show(strmessage + strmessage2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Error)
                Application.Exit()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub InsertCurrentGame()
        Try
            'INSERTION INTO CURRENT GAME TABLE
            If m_objGameDetails.ModuleID = 2 Then
                Dim dsMultipleGames As DataSet
                'FOR MODULE 2 INSERTING ALL THE GAMES WITH +/- 24 HRS TO CURRENT GAME TABLE
                'FETCHING MULTIPLE GAMES ASSIGNED TO THE SAME REPORTER WITH DURATION +/- 24 HRS.
                ' dsMultipleGames = m_objLogin.GetMultipleGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserType, CDate(m_objGameDetails.GameDate))
                dsMultipleGames = m_objGeneral.GetGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserId)

                If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                    m_objclsSelectGame.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, 0, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "INSERT")
                Else
                    'INSERTION INTO CURRENT GAME INTO CURRENT GAME SQL TABLE 
                    m_objclsSelectGame.InsertMultipleGames(dsMultipleGames, m_objGameDetails.GameCode, m_objLoginDetails.UserId)
                End If
            Else
                'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                m_objclsSelectGame.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, 0, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "INSERT")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetFeedCoverageInfo(ByVal GameCode As Integer, ByVal ReporterID As Integer, ByVal intFeedNumber As Integer)
        Try
            Dim DsFeedCoverageInfo As New DataSet
            DsFeedCoverageInfo = m_objclsSelectGame.GetFeedCoverageInfo(GameCode, ReporterID, intFeedNumber)
            If DsFeedCoverageInfo.Tables.Count > 0 Then
                'm_objGameDetails.FeedNumber = CInt(DsFeedCoverageInfo.Tables(0).Rows(0).Item("FEED_NUMBER"))
                m_objGameDetails.CoverageLevel = CInt(DsFeedCoverageInfo.Tables(0).Rows(0).Item("COVERAGELEVEL"))
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub LoadallGames()
        Try
            radFeedA.Visible = False
            radFeedB.Visible = False
            radFeedC.Visible = False
            radFeedD.Visible = False

            radFeedA.Checked = False
            radFeedB.Checked = False
            radFeedC.Checked = False
            radFeedD.Checked = False
            Dim StrGameDate As String = ""
            m_dsQAGames = m_objGameDetails.ReporterGames
           

            If Not m_dsQAGames Is Nothing Then
                If (m_dsQAGames.Tables(2).Rows.Count > 0) Then
                    For intRowCount = 0 To m_dsQAGames.Tables("Games").Rows.Count - 1
                        If CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("MODULE_ID")) = 2 Or CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("MODULE_ID")) = 4 Then
                            Continue For
                        End If

                        Dim lvwGameSchedule As New ListViewItem(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("GAME_CODE").ToString)
                        'displaying the gamedate based on his local time Zone
                        Dim Dt As DateTime = DateTimeHelper.GetDate(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                        If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                            StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                            lvwGameSchedule.SubItems.Add(StrGameDate)
                        Else
                            StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                            lvwGameSchedule.SubItems.Add(StrGameDate)
                        End If
                        lvwGameSchedule.SubItems.Add(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("Competition").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("HOMETEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("AWAYTEAM").ToString)
                        Dim Tier As String = ""

                        If m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_A") IsNot DBNull.Value Then
                            Tier = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_A").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_B") IsNot DBNull.Value Then
                            Tier = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_B").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_C") IsNot DBNull.Value Then
                            Tier = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_C").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_D") IsNot DBNull.Value Then
                            Tier = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("TIER_D").ToString
                        End If
                        lvwGameSchedule.SubItems.Add(Tier)

                        lvwGameSchedule.SubItems.Add(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("Task").ToString)

                        If CDbl(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("MODULE_ID").ToString) = 4 Then
                            If m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_A") IsNot DBNull.Value Or m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_B") IsNot DBNull.Value Then
                                lvwGameSchedule.SubItems.Add("Primary")
                            End If
                            If m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_C") IsNot DBNull.Value Or m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_D") IsNot DBNull.Value Then
                                lvwGameSchedule.SubItems.Add("Training")
                            End If
                        Else
                            If CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 1 And (m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_A") IsNot DBNull.Value Or m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_B") IsNot DBNull.Value) Then
                                lvwGameSchedule.SubItems.Add("Primary")
                            ElseIf (CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 2 Or CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 3 Or CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 4) And (m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_A") IsNot DBNull.Value Or m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_B") IsNot DBNull.Value) Then
                                lvwGameSchedule.SubItems.Add("Assister")
                            ElseIf CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 1 And (m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_C") IsNot DBNull.Value Or m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_D") IsNot DBNull.Value) Then
                                lvwGameSchedule.SubItems.Add("Primary (Training)")
                            ElseIf (CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 2 Or CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 3 Or CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO")) = 4) And (m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_C") IsNot DBNull.Value Or m_dsQAGames.Tables("Games").Rows(intRowCount).Item("REPORTER_ROLE_D") IsNot DBNull.Value) Then
                                lvwGameSchedule.SubItems.Add("Assister (Training)")
                            End If
                        End If

                        Dim Channel As String = ""
                        If m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_A") IsNot DBNull.Value Then
                            Channel = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_A").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_B") IsNot DBNull.Value Then
                            Channel = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_B").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_C") IsNot DBNull.Value Then
                            Channel = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_C").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_D") IsNot DBNull.Value Then
                            Channel = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("CHANNEL_D").ToString
                        End If
                        lvwGameSchedule.SubItems.Add(Channel)
                        Dim Desk As String = ""
                        If m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_A") IsNot DBNull.Value Then
                            Desk = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_A").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_B") IsNot DBNull.Value Then
                            Desk = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_B").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_C") IsNot DBNull.Value Then
                            Desk = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_C").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_D") IsNot DBNull.Value Then
                            Desk = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("DESK_D").ToString
                        End If
                        lvwGameSchedule.SubItems.Add(Desk)

                        lvwGameSchedule.SubItems.Add(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("MODULE_ID").ToString)

                        Dim Language As String = ""
                        If m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_A") IsNot DBNull.Value Then
                            Language = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_A").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_B") IsNot DBNull.Value Then
                            Language = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_B").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_C") IsNot DBNull.Value Then
                            Language = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_C").ToString
                        ElseIf m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_D") IsNot DBNull.Value Then
                            Language = m_dsQAGames.Tables("Games").Rows(intRowCount).Item("LANGUAGE_ID_D").ToString
                        End If

                        lvwGameSchedule.SubItems.Add(Language)
                        lvwGameSchedule.SubItems.Add(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("SERIAL_NO").ToString)

                        Dim GameCode As Integer
                        Dim ModuleID As Integer
                        Dim CurrGameCode As Integer
                        Dim CurrModuleID As Integer

                        If lvwGames.Items.Count = 0 Then
                            lvwGames.Items.Add(lvwGameSchedule)
                        Else
                            Dim lvitem As ListViewItem = lvwGames.Items(lvwGames.Items.Count - 1)
                            GameCode = CInt(lvitem.SubItems(0).Text)
                            ModuleID = CInt(lvitem.SubItems(10).Text)
                            CurrGameCode = CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("GAME_CODE"))
                            CurrModuleID = CInt(m_dsQAGames.Tables("Games").Rows(intRowCount).Item("MODULE_ID"))
                            If CurrGameCode = GameCode And CurrModuleID = ModuleID Then

                            Else
                                lvwGames.Items.Add(lvwGameSchedule)
                            End If

                        End If

                        'lvwGames.Items.Add(lvwGameSchedule)
                        lvwGames.Columns(5).Width = 60
                        lvwGames.Columns(6).Width = 75
                        lvwGames.Columns(7).Width = 174
                        lvwGames.Columns(8).Width = 140
                    Next

                    'HIGHLIGHTING THE CURRENT GAME AND FEED
                    For i As Integer = 0 To lvwGames.Items.Count - 1
                        If CDbl(lvwGames.Items(i).SubItems(0).Text) = m_objGameDetails.GameCode And CDbl(lvwGames.Items(i).SubItems(10).Text) = m_objGameDetails.ModuleID Then
                            lvwGames.Items(i).Selected = True
                            lvwGames.Select()
                            lvwGames.Items(i).Focused = True
                            lvwGames.EnsureVisible(i)
                        End If
                    Next
                    GetFeedButtonPos()
                    Dim drFeedA() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & m_objGameDetails.GameCode & " AND MODULE_ID = " & m_objGameDetails.ModuleID & " AND REPORTER_ROLE_A = 'A' AND SERIAL_NO IN(2,3,4)")
                    Dim drFeedB() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & m_objGameDetails.GameCode & " AND MODULE_ID = " & m_objGameDetails.ModuleID & " AND REPORTER_ROLE_B = 'B' AND SERIAL_NO IN(2,3,4)")
                    Dim drFeedC() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & m_objGameDetails.GameCode & " AND MODULE_ID = " & m_objGameDetails.ModuleID & " AND REPORTER_ROLE_C = 'C' AND SERIAL_NO IN(2,3,4)")
                    Dim drFeedD() As DataRow = m_dsQAGames.Tables("Games").Select("Game_code = " & m_objGameDetails.GameCode & " AND MODULE_ID = " & m_objGameDetails.ModuleID & " AND REPORTER_ROLE_D = 'D' AND SERIAL_NO IN(2,3,4)")

                    If drFeedA.Length > 0 Then
                        radFeedA.Visible = True
                        radFeedA.Enabled = True
                    End If

                    If drFeedB.Length > 0 Then
                        radFeedB.Visible = True
                        radFeedB.Enabled = True
                    End If

                    If drFeedC.Length > 0 Then
                        radFeedC.Visible = True
                        radFeedC.Enabled = True
                    End If
                    If drFeedD.Length > 0 Then
                        radFeedD.Visible = True
                        radFeedD.Enabled = True
                    End If

                    Dim intPosIndex As Integer = 0
                    If (radFeedA.Visible = True) Then
                        radFeedA.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex += 1

                    End If
                    If (radFeedB.Visible = True) Then
                        radFeedB.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex += 1
                    End If
                    If (radFeedC.Visible = True) Then
                        radFeedC.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex += 1
                    End If
                    If (radFeedD.Visible = True) Then
                        radFeedD.Left = arrFeedButtonPos(intPosIndex)
                        intPosIndex = 0
                    End If

                    If m_objGameDetails.FeedNumber = 1 Then
                        radFeedA.Checked = True
                    ElseIf m_objGameDetails.FeedNumber = 2 Then
                        radFeedB.Checked = True
                    ElseIf m_objGameDetails.FeedNumber = 3 Then
                        radFeedC.Checked = True
                    ElseIf m_objGameDetails.FeedNumber = 4 Then
                        radFeedD.Checked = True
                    End If

                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FillReporterGameDetails()
        Try
            Dim drs() As DataRow
            Dim intSerialNo As String
            Dim StrGameCodes As String = String.Empty
            If lvwGames.Items.Count > 0 Then
                For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                    Dim intGameCode As String = lvwitm.SubItems(0).Text
                    Dim intModuleID As String = lvwitm.SubItems(10).Text


                    lvwGames.Items(lvwitm.Index).Selected = True
                    lvwGames.Select()
                    'm_objGameDetails.CoverageLevel = CInt(lvwitm.SubItems(5).Text)
                    Dim intFeedNumber As Integer

                    If radFeedA.Checked = False And radFeedB.Checked = False And radFeedC.Checked = False And radFeedD.Checked = False Then
                        lvwGames_Click(lvwGames, New EventArgs)
                    End If
                    If radFeedA.Checked = True Then
                        intFeedNumber = 1
                        drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_A = " & intFeedNumber)
                    ElseIf radFeedB.Checked = True Then
                        intFeedNumber = 2
                        drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_B = " & intFeedNumber)
                    ElseIf radFeedC.Checked = True Then
                        intFeedNumber = 3
                        drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_C = " & intFeedNumber)
                    ElseIf radFeedD.Checked = True Then
                        intFeedNumber = 4
                        drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND FEED_D = " & intFeedNumber)
                    Else
                        intSerialNo = lvwitm.SubItems(12).Text
                        drs = m_dsQAGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)
                    End If

                    If drs IsNot Nothing Then
                        If drs.Length > 0 Then
                            m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                            m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                            m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)

                            m_objGameDetails.ModuleName = drs(0).Item("Task").ToString
                            m_objGameDetails.AwayTeamID = CInt(drs(0).Item("AWAYTEAMID").ToString)
                            m_objGameDetails.AwayTeamAbbrev = drs(0).Item("AWAYABBREV").ToString
                            m_objGameDetails.AwayTeam = drs(0).Item("AWAYTEAM").ToString
                            m_objGameDetails.HomeTeamID = CInt(drs(0).Item("HOMETEAMID").ToString)
                            m_objGameDetails.HomeTeamAbbrev = drs(0).Item("HOMEABBREV").ToString
                            m_objGameDetails.HomeTeam = drs(0).Item("HOMETEAM").ToString
                            m_objGameDetails.LeagueID = CInt(drs(0).Item("LEAGUE_ID").ToString)
                            m_objGameDetails.LeagueName = drs(0).Item("Competition").ToString
                            m_objGameDetails.FieldID = CInt(drs(0).Item("FIELD_ID").ToString)
                            If radFeedA.Checked = True Then
                                m_objGameDetails.FeedNumber = 1
                                m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_A").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_A"))
                            ElseIf radFeedB.Checked = True Then
                                m_objGameDetails.FeedNumber = 2
                                m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_B").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_B"))
                            ElseIf radFeedC.Checked = True Then
                                m_objGameDetails.FeedNumber = 3
                                m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_C").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_C"))
                            ElseIf radFeedD.Checked = True Then
                                m_objGameDetails.FeedNumber = 4
                                m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_D").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_D"))
                            End If
                            If radFeedA.Checked = False And radFeedB.Checked = False And radFeedC.Checked = False And radFeedD.Checked = False Then
                                If Not IsDBNull(drs(0).Item("FEED_A")) Then
                                    m_objGameDetails.FeedNumber = 1
                                    m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_A").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                    m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_A"))
                                ElseIf Not IsDBNull(drs(0).Item("FEED_B")) Then
                                    m_objGameDetails.FeedNumber = 2
                                    m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_B").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                    m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_B"))
                                ElseIf Not IsDBNull(drs(0).Item("FEED_C")) Then
                                    m_objGameDetails.FeedNumber = 3
                                    m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_C").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                    m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_C"))
                                ElseIf Not IsDBNull(drs(0).Item("FEED_D")) Then
                                    m_objGameDetails.FeedNumber = 4
                                    m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_D").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                    m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_D"))
                                End If
                            End If

                            'GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                            m_objGameDetails.ReporterRoleDisplay = CStr(IIf(CInt(drs(0).Item("SERIAL_NO").ToString) = 1, "MAIN", "ASSISTER"))
                            m_objGameDetails.SerialNo = Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                            m_objclsTeamSetup.HomeShirtColor = Nothing
                            m_objclsTeamSetup.HomeShortColor = Nothing
                            m_objclsTeamSetup.AwayShirtColor = Nothing
                            m_objclsTeamSetup.AwayShortColor = Nothing
                            lagid = 1
                        End If
                    End If
                Next

                StrGameCodes = CStr(m_objGameDetails.GameCode)

                'T1 WORKFLOW STARTS HERE 
                Dim StrReporterRole As String = ""
                Dim m_dsDataCountForRestart As DataSet
                Dim m_dsRestartData As DataSet

                If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                    'FOR PRIMARY REPORTER CHECK IF THE DATA EXISTS IN LOCAL SQL
                    m_dsDataCountForRestart = m_objclsSelectGame.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                    If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                        'IF SO CONTINUE FROM THE PLACE WHERE HE LEFT
                        MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        sstMain.Items(0).Text = "The game is reloading..."
                        m_objGameDetails.IsRestartGame = True
                        m_dsRestartData = m_objclsSelectGame.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        m_objGameDetails.RestartData = m_dsRestartData
                    Else
                        'WRITE CODE IF SOME OTHER GAME EXITS IN HIS LOCAL SQL..
                        m_objGameDetails.IsRestartGame = False
                        'DOWNLOAD FROM ORACLE AND FILL THE LOCAL SQL TABLES
                        sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                        Application.DoEvents()
                        FillLocalSQLTables(StrGameCodes)
                        'DOWNLOAD ORACLE SP FILLS THE PBP TABLE 
                        'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                        m_dsDataCountForRestart = m_objclsSelectGame.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                            'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                            sstMain.Items(0).Text = "The game is reloading..."
                            MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            m_objGameDetails.IsRestartGame = True
                            m_dsRestartData = m_objclsSelectGame.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                            m_objGameDetails.RestartData = m_dsRestartData
                        End If
                        InsertCurrentGame()
                        If m_objLoginDetails.UserId = -1 Then
                            GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                        End If
                    End If

                Else 'FOR SECONDARY REPORTER (ASSISTERS)
                    m_objGameDetails.IsRestartGame = False
                    'DOWNLOAD AND FILL THE LOCAL SQL TABLES
                    sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                    Application.DoEvents()
                    FillLocalSQLTables(StrGameCodes)
                    'DOWNLOAD ORACLE SP FILLS THE PBP TABLE 
                    'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                    m_dsDataCountForRestart = m_objclsSelectGame.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                    If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                        'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                        sstMain.Items(0).Text = "The Game is reloading..."
                        MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_objGameDetails.IsRestartGame = True
                        m_dsRestartData = m_objclsSelectGame.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        m_objGameDetails.RestartData = m_dsRestartData
                    End If
                    InsertCurrentGame()
                    m_objGameDetails.AssistersLastEntryStatus = True
                    If m_objLoginDetails.UserId = -1 Then
                        GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region
    
  

  End Class