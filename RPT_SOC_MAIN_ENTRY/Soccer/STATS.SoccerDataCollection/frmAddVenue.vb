﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmAddVenue

#Region " Constants & Variable "
    Private m_dsAddSatdium As New DataSet
    Private m_strCheckButtonOption As String
    Private m_intVenueid As Integer
    Private m_intsurfacetypeid As Integer
    Private m_intLeague As Integer
    Private m_blnLeague As Boolean = False
    Private m_objAddVenue As New clsAddVenue
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private lsupport As New languagesupport
#End Region

#Region "Event Handling"

    Private Sub frmAddStadium_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Try
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show("Please Apply or Ignore the changes", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
                e.Cancel = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnApply.Text, Nothing, 1, 0)
            Dim strGrass As String = Nothing
            Dim strDome As String = Nothing
            Dim strNickname As String
            If (chkArtificial.Checked = True) Then
                strGrass = "N"
            Else
                strGrass = "Y"
            End If
            If chkDome.Checked = True Then
                strDome = "Y"
            Else
                strDome = "N"
            End If
            If (RequireFieldValidation()) Then
                If (m_strCheckButtonOption = "Save") Then
                    If (txtNickname.Text = "") Then
                        strNickname = Nothing
                    Else
                        strNickname = txtNickname.Text.Trim()
                    End If
                    m_objAddVenue.InsertAddVenue(CInt(cmbLeagueId.SelectedValue.ToString()), txtName.Text.Trim(), strNickname, txtCity.Text.Trim(), CInt((txtCapacity.Text.Trim())), CChar(strGrass), CChar(strDome))
                ElseIf (m_strCheckButtonOption = "Update") Then
                    m_objAddVenue.UpdateAddVenue(CInt(cmbLeagueId.SelectedValue.ToString()), m_intVenueid, txtName.Text.Trim(), txtNickname.Text.Trim(), txtCity.Text.Trim(), CInt((txtCapacity.Text.Trim())), CChar(strGrass), CChar(strDome))
                    cmbLeagueId.Enabled = True
                    cmbLeagueId.Focus()
                End If
                LoadNewlyAddedSatdium()
                ClearTextboxes()
                EnableDisableTextBox(False, False, False, False, False, False, False, True)
                EnableDisableButton(True, False, False, False, False)
                m_strCheckButtonOption = "Add"
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAdd.Text, Nothing, 1, 0)
            m_strCheckButtonOption = "Save"
            ClearTextboxes()
            EnableDisableButton(False, False, False, True, True)
            chkDome.Enabled = True
            chkArtificial.Enabled = True
            EnableDisableTextBox(True, True, True, True, True, True, True, False)
            cmbLeagueId.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, Nothing, 1, 0)
            lvwStadium.Enabled = False
            m_strCheckButtonOption = "Update"
            EnableDisableTextBox(True, True, True, True, True, True, True, False)
            EnableDisableButton(False, False, False, True, True)
            chkDome.Enabled = True
            chkArtificial.Enabled = True
            cmbLeagueId.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnIgnore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnIgnore.Text, Nothing, 1, 0)
            m_strCheckButtonOption = "Add"
            ClearTextboxes()
            EnableDisableTextBox(False, False, False, False, False, False, False, True)
            chkDome.Enabled = False
            chkArtificial.Enabled = False
            EnableDisableButton(True, False, False, False, False)
            lvwStadium.Enabled = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, Nothing, 1, 0)
            Dim stadiumid As Integer
            lvwStadium.Enabled = False
            If (MessageDialog.Show("Are you sure you want to delete " & txtName.Text & "?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                stadiumid = m_intVenueid
                m_objAddVenue.DeleteAddVenue(stadiumid, CInt(cmbLeagueId.SelectedValue.ToString()))
                EnableDisableTextBox(False, False, False, False, False, False, False, True)
                EnableDisableButton(True, False, False, False, False)
                MessageDialog.Show("Venue Deleted Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                LoadNewlyAddedSatdium()
                ClearTextboxes()
                m_strCheckButtonOption = "Add"
                cmbLeagueId.SelectedIndex = -1
                cmbLeagueId.Focus()
                lvwStadium.Enabled = True
            Else
                lvwStadium.Enabled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwStadium_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvwStadium.Click
        Try
            For Each lvwitm As ListViewItem In lvwStadium.SelectedItems
                m_intVenueid = CInt(m_dsAddSatdium.Tables(0).Rows(lvwitm.Index).Item("FIELD_ID").ToString)
                m_intLeague = CInt(m_dsAddSatdium.Tables(0).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                ' m_intsurfacetypeid = CInt(m_dsAddSatdium.Tables(0).Rows(lvwitm.Index).Item("SURFACE_TYPE_ID").ToString)

            Next
            EnableDisableTextBox(False, False, False, False, False, False, False, True)
            chkDome.Enabled = False
            chkArtificial.Enabled = False
            EnableDisableButton(True, True, True, False, False)
            SelectedListviewDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtName_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        Try
            If Char.IsLetter(e.KeyChar) <> True And Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtNickname_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNickname.KeyPress
        Try
            If Char.IsLetter(e.KeyChar) <> True And Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtCapacity_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCapacity.KeyPress
        Try
            If Char.IsLetter(e.KeyChar) <> False And Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> True Then
                e.Handled = True
                MessageDialog.Show("Enter Digits only", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region "User Defind Function"

    'Checking The mandatory Field are Filled or not
    Public Function RequireFieldValidation() As Boolean
        Try
            Dim clsvalid As New Utility.clsValidation
            If (Not clsvalid.ValidateEmpty(cmbLeagueId.Text)) Then
                MessageDialog.Show("Select a League", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLeagueId.Focus()
                cmbLeagueId.SelectedIndex = 0
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(txtName.Text)) Then
                MessageDialog.Show("Satdium Name is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtName.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(txtCapacity.Text)) Then
                MessageDialog.Show("Stadium Capcity is not entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtCapacity.Focus()
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ClearTextboxes()
        Try
            txtName.Text = ""
            txtNickname.Text = ""
            txtCapacity.Text = ""
            txtCity.Text = ""
            cmbLeagueId.SelectedIndex = 0
            chkArtificial.Checked = False
            chkDome.Checked = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Selecting Newly Added Official
    Private Sub LoadNewlyAddedSatdium()
        Try
            m_dsAddSatdium = m_objAddVenue.SelectAddVenue()
            lvwStadium.Items.Clear()
            If Not m_dsAddSatdium.Tables(0) Is Nothing Then
                For Each drNewStadium As DataRow In m_dsAddSatdium.Tables(0).Rows
                    Dim lvItem As New ListViewItem(drNewStadium.ItemArray(2).ToString())  'Stadium Name
                    lvItem.SubItems.Add(drNewStadium.ItemArray(3).ToString) 'Nick Name
                    lvItem.SubItems.Add(drNewStadium.ItemArray(7).ToString) ' League Abbrev
                    lvwStadium.Items.Add(lvItem)
                Next
                lvwStadium.Items(0).Selected = True
                lvwStadium.EnsureVisible(0)
                Application.DoEvents()
            Else
                MessageDialog.Show("Currently there is no Stadium available", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Me.Close()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Enabling and disabling Button
    Private Sub EnableDisableButton(ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean)
        Try
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
            btnDelete.Enabled = blnButtonDelete
            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Enabling and Disabling TextBox,Combo,ListView
    Private Sub EnableDisableTextBox(ByVal blnCboLeagueId As Boolean, ByVal blnTextBoxName As Boolean, ByVal blnTextBoNickName As Boolean, ByVal blntxtCity As Boolean, ByVal blnTextBoxCapacity As Boolean, ByVal blnchkGrass As Boolean, ByVal blnchkDome As Boolean, ByVal blnListViewStadium As Boolean)
        Try
            cmbLeagueId.Enabled = blnCboLeagueId
            txtName.Enabled = blnTextBoxName
            txtNickname.Enabled = blnTextBoNickName
            txtCity.Enabled = blntxtCity
            txtCapacity.Enabled = blnTextBoxCapacity
            chkArtificial.Enabled = blnchkGrass
            chkDome.Enabled = blnchkDome
            lvwStadium.Enabled = blnListViewStadium
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Displaying Listview selected item in textbox and combo
    Private Sub SelectedListviewDisplay()
        Try
            Dim listselectDatarow() As DataRow
            Dim Stadiumid As Integer
            For Each lvwitm As ListViewItem In lvwStadium.SelectedItems
                Stadiumid = m_intVenueid
            Next
            listselectDatarow = m_dsAddSatdium.Tables(0).Select("FIELD_ID=" & Stadiumid & "")
            If listselectDatarow.Length > 0 Then
                cmbLeagueId.SelectedValue = listselectDatarow(0).Item(1).ToString()
                txtName.Text = listselectDatarow(0).Item(2).ToString()
                txtNickname.Text = listselectDatarow(0).Item(3).ToString()
                txtCity.Text = listselectDatarow(0).Item(8).ToString()
                txtCapacity.Text = listselectDatarow(0).Item(4).ToString()
                If (listselectDatarow(0).Item(5).ToString() = "N") Then
                    chkArtificial.Checked = True
                Else
                    chkArtificial.Checked = False
                End If
                If (listselectDatarow(0).Item(6).ToString() = "Y") Then
                    chkDome.Checked = True
                Else
                    chkDome.Checked = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadLeagueCombo()
        Try
            Dim dtLeague As New DataTable
            Dim dsLeague As New DataSet
            dsLeague = m_objAddVenue.GetLeague()
            dtLeague = dsLeague.Tables(0).Copy
            LoadControl(cmbLeagueId, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
            m_blnLeague = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub frmAddVenue_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmAddVenue", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            LoadLeagueCombo()
            cmbLeagueId.SelectedIndex = 0
            LoadNewlyAddedSatdium()
            EnableDisableButton(True, False, False, False, False)
            EnableDisableTextBox(False, False, False, False, False, False, False, True)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                lblLeague.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLeague.Text)
                lblName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblName.Text)
                lblNickname.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblNickname.Text)
                lblCity.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblCity.Text)
                lblCapacity.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblCapacity.Text)
                chkArtificial.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), chkArtificial.Text)
                chkDome.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), chkDome.Text)

                btnAdd.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAdd.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnApply.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnApply.Text)
                btnIgnore.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnIgnore.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)

                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Name")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Nickname")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "League")
                lvwStadium.Columns(0).Text = g1
                lvwStadium.Columns(1).Text = g2
                lvwStadium.Columns(2).Text = g3

            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show("Please Save or Cancel the changes", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLeagueId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLeagueId.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbLeagueId.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtName_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.Leave
        Try
            If (txtName.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtName.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtNickname_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNickname.Leave
        Try
            If (txtName.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtNickname.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtCity_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCity.Leave
        Try
            If (txtName.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtCity.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub chkDome_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDome.CheckStateChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.CheckBoxSelected, txtCity.Text, Nothing, 1, 0)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLeagueId_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLeagueId.Validated
        Try
            If cmbLeagueId.Text <> "" And cmbLeagueId.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid league", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLeagueId.Text = ""
                cmbLeagueId.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class