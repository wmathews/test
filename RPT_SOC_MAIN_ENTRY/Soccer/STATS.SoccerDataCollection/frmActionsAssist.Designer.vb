﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmActionsAssist
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pnlFieldInfo = New System.Windows.Forms.Panel()
        Me.btnViewGoal = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.picFieldBlinkOrange = New System.Windows.Forms.PictureBox()
        Me.picFieldBlinkGreen = New System.Windows.Forms.PictureBox()
        Me.btnRepeatLast = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.picFieldOrange = New System.Windows.Forms.PictureBox()
        Me.picFieldGreen = New System.Windows.Forms.PictureBox()
        Me.lblSelection2 = New System.Windows.Forms.Label()
        Me.lblSelection1 = New System.Windows.Forms.Label()
        Me.lblFieldY = New System.Windows.Forms.Label()
        Me.lblFieldX = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPass = New System.Windows.Forms.Button()
        Me.btn50BW = New System.Windows.Forms.Button()
        Me.btnSaves = New System.Windows.Forms.Button()
        Me.btnBlocks = New System.Windows.Forms.Button()
        Me.btnGKAction = New System.Windows.Forms.Button()
        Me.btnAirWon = New System.Windows.Forms.Button()
        Me.btnTackle = New System.Windows.Forms.Button()
        Me.btnChance = New System.Windows.Forms.Button()
        Me.btnObstacle = New System.Windows.Forms.Button()
        Me.btnControl = New System.Windows.Forms.Button()
        Me.btnClearance = New System.Windows.Forms.Button()
        Me.btnOOB = New System.Windows.Forms.Button()
        Me.btnUncontrolled = New System.Windows.Forms.Button()
        Me.btnGoalKick = New System.Windows.Forms.Button()
        Me.btnCorner = New System.Windows.Forms.Button()
        Me.btnShot = New System.Windows.Forms.Button()
        Me.btnOffSide = New System.Windows.Forms.Button()
        Me.btnFoul = New System.Windows.Forms.Button()
        Me.btnThrowIn = New System.Windows.Forms.Button()
        Me.btnFreeKick = New System.Windows.Forms.Button()
        Me.pnlGoalsLeft = New System.Windows.Forms.Panel()
        Me.btnOwnGoal = New System.Windows.Forms.Button()
        Me.btnNormalGoal = New System.Windows.Forms.Button()
        Me.btnPenalty = New System.Windows.Forms.Button()
        Me.pnlGoalLblLeft = New System.Windows.Forms.Panel()
        Me.lblGoalAway = New System.Windows.Forms.Label()
        Me.btnBooking = New System.Windows.Forms.Button()
        Me.btnSubstitute = New System.Windows.Forms.Button()
        Me.btnRefreshTime = New System.Windows.Forms.Button()
        Me.txtTime = New System.Windows.Forms.MaskedTextBox()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pnlEventProperties = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lstEventProp15 = New System.Windows.Forms.ListBox()
        Me.lblEventProp14 = New System.Windows.Forms.Label()
        Me.lstEventProp14 = New System.Windows.Forms.ListBox()
        Me.lblEventProp13 = New System.Windows.Forms.Label()
        Me.lstEventProp13 = New System.Windows.Forms.ListBox()
        Me.lblEventProp12 = New System.Windows.Forms.Label()
        Me.lstEventProp12 = New System.Windows.Forms.ListBox()
        Me.lblEventProp09 = New System.Windows.Forms.Label()
        Me.lblEventProp11 = New System.Windows.Forms.Label()
        Me.lblEventProp10 = New System.Windows.Forms.Label()
        Me.lblEventProp08 = New System.Windows.Forms.Label()
        Me.lblEventProp07 = New System.Windows.Forms.Label()
        Me.lblEventProp06 = New System.Windows.Forms.Label()
        Me.lblEventProp05 = New System.Windows.Forms.Label()
        Me.lblEventProp04 = New System.Windows.Forms.Label()
        Me.lblEventProp03 = New System.Windows.Forms.Label()
        Me.lblEventProp02 = New System.Windows.Forms.Label()
        Me.lblEventProp01 = New System.Windows.Forms.Label()
        Me.lstEventProp11 = New System.Windows.Forms.ListBox()
        Me.lstEventProp10 = New System.Windows.Forms.ListBox()
        Me.lstEventProp09 = New System.Windows.Forms.ListBox()
        Me.lstEventProp08 = New System.Windows.Forms.ListBox()
        Me.lstEventProp07 = New System.Windows.Forms.ListBox()
        Me.lstEventProp06 = New System.Windows.Forms.ListBox()
        Me.lstEventProp05 = New System.Windows.Forms.ListBox()
        Me.lstEventProp04 = New System.Windows.Forms.ListBox()
        Me.lstEventProp03 = New System.Windows.Forms.ListBox()
        Me.lstEventProp02 = New System.Windows.Forms.ListBox()
        Me.lstEventProp01 = New System.Windows.Forms.ListBox()
        Me.pnlSave = New System.Windows.Forms.Panel()
        Me.txtInjTime = New System.Windows.Forms.MaskedTextBox()
        Me.lblInjuryTime = New System.Windows.Forms.Label()
        Me.btnDecTime = New System.Windows.Forms.Button()
        Me.btnIncTime = New System.Windows.Forms.Button()
        Me.lblEvent = New System.Windows.Forms.Label()
        Me.dgvPBP = New System.Windows.Forms.DataGridView()
        Me.cmnuTimeline = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.InsertToolStripItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteActionToolStripItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.pnlsubstitute = New System.Windows.Forms.Panel()
        Me.btnSub33 = New System.Windows.Forms.Button()
        Me.btnSub32 = New System.Windows.Forms.Button()
        Me.btnSub31 = New System.Windows.Forms.Button()
        Me.btnSub29 = New System.Windows.Forms.Button()
        Me.btnSub28 = New System.Windows.Forms.Button()
        Me.btnSub27 = New System.Windows.Forms.Button()
        Me.btnSub26 = New System.Windows.Forms.Button()
        Me.btnSub25 = New System.Windows.Forms.Button()
        Me.btnSub24 = New System.Windows.Forms.Button()
        Me.btnSub23 = New System.Windows.Forms.Button()
        Me.btnSub22 = New System.Windows.Forms.Button()
        Me.btnSub21 = New System.Windows.Forms.Button()
        Me.btnSub20 = New System.Windows.Forms.Button()
        Me.btnSub19 = New System.Windows.Forms.Button()
        Me.btnSub18 = New System.Windows.Forms.Button()
        Me.btnSub17 = New System.Windows.Forms.Button()
        Me.btnSub16 = New System.Windows.Forms.Button()
        Me.btnSub15 = New System.Windows.Forms.Button()
        Me.btnSub14 = New System.Windows.Forms.Button()
        Me.btnSub13 = New System.Windows.Forms.Button()
        Me.btnSub12 = New System.Windows.Forms.Button()
        Me.btnSub11 = New System.Windows.Forms.Button()
        Me.btnSub10 = New System.Windows.Forms.Button()
        Me.btnSub9 = New System.Windows.Forms.Button()
        Me.btnSub8 = New System.Windows.Forms.Button()
        Me.btnSub7 = New System.Windows.Forms.Button()
        Me.btnSub6 = New System.Windows.Forms.Button()
        Me.btnSub5 = New System.Windows.Forms.Button()
        Me.btnSub4 = New System.Windows.Forms.Button()
        Me.btnSub3 = New System.Windows.Forms.Button()
        Me.btnSub2 = New System.Windows.Forms.Button()
        Me.btnSub1 = New System.Windows.Forms.Button()
        Me.pnlField = New System.Windows.Forms.Panel()
        Me.UdcSoccerField1 = New STATS.SoccerDataCollection.udcSoccerField()
        Me.pnlGoalframe = New System.Windows.Forms.Panel()
        Me.UdcSoccerGoalFrame1 = New STATS.SoccerDataCollection.udcSoccerGoalFrame()
        Me.pnlInfo = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblControlCaption = New System.Windows.Forms.Label()
        Me.btnViewField = New System.Windows.Forms.Button()
        Me.btnZoom = New System.Windows.Forms.Button()
        Me.picGFOrangeBlink = New System.Windows.Forms.PictureBox()
        Me.picGFGreenBlink = New System.Windows.Forms.PictureBox()
        Me.lblWidth = New System.Windows.Forms.Label()
        Me.picGFOrange = New System.Windows.Forms.PictureBox()
        Me.picGFGreen = New System.Windows.Forms.PictureBox()
        Me.lblClickLocationOrange = New System.Windows.Forms.Label()
        Me.lblClickLocationGreen = New System.Windows.Forms.Label()
        Me.lblMoveZ = New System.Windows.Forms.Label()
        Me.lblMoveY = New System.Windows.Forms.Label()
        Me.btnReplace = New System.Windows.Forms.Button()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.btnFilters = New System.Windows.Forms.Button()
        Me.chkQC = New System.Windows.Forms.CheckBox()
        Me.chkAway = New System.Windows.Forms.CheckBox()
        Me.chkHome = New System.Windows.Forms.CheckBox()
        Me.chkInc = New System.Windows.Forms.CheckBox()
        Me.chkOwnAction = New System.Windows.Forms.CheckBox()
        Me.UdcSoccerPlayerPositions1 = New STATS.SoccerDataCollection.udcSoccerPlayerPositions()
        Me.chk1sthalf = New System.Windows.Forms.CheckBox()
        Me.chk2ndhalf = New System.Windows.Forms.CheckBox()
        Me.pnlFieldInfo.SuspendLayout
        CType(Me.picFieldBlinkOrange,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picFieldBlinkGreen,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picFieldOrange,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picFieldGreen,System.ComponentModel.ISupportInitialize).BeginInit
        Me.pnlGoalsLeft.SuspendLayout
        Me.pnlGoalLblLeft.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.pnlEventProperties.SuspendLayout
        Me.pnlSave.SuspendLayout
        CType(Me.dgvPBP,System.ComponentModel.ISupportInitialize).BeginInit
        Me.cmnuTimeline.SuspendLayout
        Me.Panel3.SuspendLayout
        Me.Panel4.SuspendLayout
        Me.pnlsubstitute.SuspendLayout
        Me.pnlField.SuspendLayout
        Me.pnlGoalframe.SuspendLayout
        Me.pnlInfo.SuspendLayout
        CType(Me.picGFOrangeBlink,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picGFGreenBlink,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picGFOrange,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picGFGreen,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'pnlFieldInfo
        '
        Me.pnlFieldInfo.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.pnlFieldInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlFieldInfo.Controls.Add(Me.btnViewGoal)
        Me.pnlFieldInfo.Controls.Add(Me.Label3)
        Me.pnlFieldInfo.Controls.Add(Me.picFieldBlinkOrange)
        Me.pnlFieldInfo.Controls.Add(Me.picFieldBlinkGreen)
        Me.pnlFieldInfo.Controls.Add(Me.btnRepeatLast)
        Me.pnlFieldInfo.Controls.Add(Me.Label4)
        Me.pnlFieldInfo.Controls.Add(Me.picFieldOrange)
        Me.pnlFieldInfo.Controls.Add(Me.picFieldGreen)
        Me.pnlFieldInfo.Controls.Add(Me.lblSelection2)
        Me.pnlFieldInfo.Controls.Add(Me.lblSelection1)
        Me.pnlFieldInfo.Controls.Add(Me.lblFieldY)
        Me.pnlFieldInfo.Controls.Add(Me.lblFieldX)
        Me.pnlFieldInfo.Controls.Add(Me.Label1)
        Me.pnlFieldInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlFieldInfo.Name = "pnlFieldInfo"
        Me.pnlFieldInfo.Size = New System.Drawing.Size(52, 230)
        Me.pnlFieldInfo.TabIndex = 84
        '
        'btnViewGoal
        '
        Me.btnViewGoal.BackColor = System.Drawing.Color.Khaki
        Me.btnViewGoal.Enabled = false
        Me.btnViewGoal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewGoal.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnViewGoal.Location = New System.Drawing.Point(-6, 190)
        Me.btnViewGoal.Margin = New System.Windows.Forms.Padding(0)
        Me.btnViewGoal.Name = "btnViewGoal"
        Me.btnViewGoal.Size = New System.Drawing.Size(62, 44)
        Me.btnViewGoal.TabIndex = 15
        Me.btnViewGoal.Text = "View"&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"Goal"
        Me.btnViewGoal.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnViewGoal.UseVisualStyleBackColor = false
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(0, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "105 X 70"
        '
        'picFieldBlinkOrange
        '
        Me.picFieldBlinkOrange.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePinBlink
        Me.picFieldBlinkOrange.Location = New System.Drawing.Point(17, 131)
        Me.picFieldBlinkOrange.Name = "picFieldBlinkOrange"
        Me.picFieldBlinkOrange.Size = New System.Drawing.Size(14, 15)
        Me.picFieldBlinkOrange.TabIndex = 14
        Me.picFieldBlinkOrange.TabStop = false
        '
        'picFieldBlinkGreen
        '
        Me.picFieldBlinkGreen.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPinBlink
        Me.picFieldBlinkGreen.Location = New System.Drawing.Point(17, 96)
        Me.picFieldBlinkGreen.Name = "picFieldBlinkGreen"
        Me.picFieldBlinkGreen.Size = New System.Drawing.Size(14, 13)
        Me.picFieldBlinkGreen.TabIndex = 13
        Me.picFieldBlinkGreen.TabStop = false
        '
        'btnRepeatLast
        '
        Me.btnRepeatLast.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.RepeatLast
        Me.btnRepeatLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRepeatLast.Location = New System.Drawing.Point(12, 162)
        Me.btnRepeatLast.Name = "btnRepeatLast"
        Me.btnRepeatLast.Size = New System.Drawing.Size(24, 24)
        Me.btnRepeatLast.TabIndex = 8
        Me.btnRepeatLast.UseVisualStyleBackColor = true
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(5, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "meters"
        '
        'picFieldOrange
        '
        Me.picFieldOrange.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePin
        Me.picFieldOrange.Location = New System.Drawing.Point(17, 132)
        Me.picFieldOrange.Name = "picFieldOrange"
        Me.picFieldOrange.Size = New System.Drawing.Size(14, 13)
        Me.picFieldOrange.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picFieldOrange.TabIndex = 12
        Me.picFieldOrange.TabStop = false
        '
        'picFieldGreen
        '
        Me.picFieldGreen.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPin
        Me.picFieldGreen.Location = New System.Drawing.Point(17, 96)
        Me.picFieldGreen.Name = "picFieldGreen"
        Me.picFieldGreen.Size = New System.Drawing.Size(14, 13)
        Me.picFieldGreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picFieldGreen.TabIndex = 11
        Me.picFieldGreen.TabStop = false
        '
        'lblSelection2
        '
        Me.lblSelection2.Location = New System.Drawing.Point(2, 148)
        Me.lblSelection2.Name = "lblSelection2"
        Me.lblSelection2.Size = New System.Drawing.Size(45, 15)
        Me.lblSelection2.TabIndex = 7
        Me.lblSelection2.Text = "-"
        Me.lblSelection2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblSelection1
        '
        Me.lblSelection1.Location = New System.Drawing.Point(-1, 110)
        Me.lblSelection1.Name = "lblSelection1"
        Me.lblSelection1.Size = New System.Drawing.Size(52, 15)
        Me.lblSelection1.TabIndex = 6
        Me.lblSelection1.Text = "-"
        Me.lblSelection1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFieldY
        '
        Me.lblFieldY.AutoSize = true
        Me.lblFieldY.Location = New System.Drawing.Point(6, 71)
        Me.lblFieldY.Name = "lblFieldY"
        Me.lblFieldY.Size = New System.Drawing.Size(17, 15)
        Me.lblFieldY.TabIndex = 5
        Me.lblFieldY.Text = "Y:"
        '
        'lblFieldX
        '
        Me.lblFieldX.AutoSize = true
        Me.lblFieldX.Location = New System.Drawing.Point(6, 56)
        Me.lblFieldX.Name = "lblFieldX"
        Me.lblFieldX.Size = New System.Drawing.Size(17, 15)
        Me.lblFieldX.TabIndex = 4
        Me.lblFieldX.Text = "X:"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Field"
        '
        'btnPass
        '
        Me.btnPass.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnPass.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnPass.Location = New System.Drawing.Point(6, 4)
        Me.btnPass.Name = "btnPass"
        Me.btnPass.Size = New System.Drawing.Size(91, 26)
        Me.btnPass.TabIndex = 86
        Me.btnPass.Tag = "50"
        Me.btnPass.Text = "Pass"
        Me.btnPass.UseVisualStyleBackColor = true
        '
        'btn50BW
        '
        Me.btn50BW.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btn50BW.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btn50BW.Location = New System.Drawing.Point(288, 4)
        Me.btn50BW.Name = "btn50BW"
        Me.btn50BW.Size = New System.Drawing.Size(91, 26)
        Me.btn50BW.TabIndex = 87
        Me.btn50BW.Tag = "71"
        Me.btn50BW.Text = "50/50"
        Me.btn50BW.UseVisualStyleBackColor = true
        '
        'btnSaves
        '
        Me.btnSaves.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSaves.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnSaves.Location = New System.Drawing.Point(194, 4)
        Me.btnSaves.Name = "btnSaves"
        Me.btnSaves.Size = New System.Drawing.Size(91, 26)
        Me.btnSaves.TabIndex = 88
        Me.btnSaves.Tag = "75"
        Me.btnSaves.Text = "Save"
        Me.btnSaves.UseVisualStyleBackColor = true
        '
        'btnBlocks
        '
        Me.btnBlocks.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnBlocks.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnBlocks.Location = New System.Drawing.Point(100, 4)
        Me.btnBlocks.Name = "btnBlocks"
        Me.btnBlocks.Size = New System.Drawing.Size(91, 26)
        Me.btnBlocks.TabIndex = 93
        Me.btnBlocks.Tag = "78"
        Me.btnBlocks.Text = "Block"
        Me.btnBlocks.UseVisualStyleBackColor = true
        '
        'btnGKAction
        '
        Me.btnGKAction.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGKAction.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnGKAction.Location = New System.Drawing.Point(194, 36)
        Me.btnGKAction.Name = "btnGKAction"
        Me.btnGKAction.Size = New System.Drawing.Size(91, 26)
        Me.btnGKAction.TabIndex = 92
        Me.btnGKAction.Tag = "79"
        Me.btnGKAction.Text = "GK Action"
        Me.btnGKAction.UseVisualStyleBackColor = true
        '
        'btnAirWon
        '
        Me.btnAirWon.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAirWon.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnAirWon.Location = New System.Drawing.Point(288, 36)
        Me.btnAirWon.Name = "btnAirWon"
        Me.btnAirWon.Size = New System.Drawing.Size(91, 26)
        Me.btnAirWon.TabIndex = 91
        Me.btnAirWon.Tag = "73"
        Me.btnAirWon.Text = "Air 50/50"
        Me.btnAirWon.UseVisualStyleBackColor = true
        '
        'btnTackle
        '
        Me.btnTackle.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnTackle.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnTackle.Location = New System.Drawing.Point(100, 36)
        Me.btnTackle.Name = "btnTackle"
        Me.btnTackle.Size = New System.Drawing.Size(91, 26)
        Me.btnTackle.TabIndex = 97
        Me.btnTackle.Tag = "52"
        Me.btnTackle.Text = "Tackle"
        Me.btnTackle.UseVisualStyleBackColor = true
        '
        'btnChance
        '
        Me.btnChance.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnChance.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnChance.Location = New System.Drawing.Point(194, 67)
        Me.btnChance.Name = "btnChance"
        Me.btnChance.Size = New System.Drawing.Size(91, 26)
        Me.btnChance.TabIndex = 96
        Me.btnChance.Tag = "70"
        Me.btnChance.Text = "Chance"
        Me.btnChance.UseVisualStyleBackColor = true
        '
        'btnObstacle
        '
        Me.btnObstacle.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnObstacle.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnObstacle.Location = New System.Drawing.Point(288, 67)
        Me.btnObstacle.Name = "btnObstacle"
        Me.btnObstacle.Size = New System.Drawing.Size(91, 26)
        Me.btnObstacle.TabIndex = 95
        Me.btnObstacle.Tag = "68"
        Me.btnObstacle.Text = "Obstacle"
        Me.btnObstacle.UseVisualStyleBackColor = true
        '
        'btnControl
        '
        Me.btnControl.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnControl.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnControl.Location = New System.Drawing.Point(6, 36)
        Me.btnControl.Name = "btnControl"
        Me.btnControl.Size = New System.Drawing.Size(91, 26)
        Me.btnControl.TabIndex = 94
        Me.btnControl.Tag = "63"
        Me.btnControl.Text = "Control"
        Me.btnControl.UseVisualStyleBackColor = true
        '
        'btnClearance
        '
        Me.btnClearance.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClearance.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnClearance.Location = New System.Drawing.Point(100, 67)
        Me.btnClearance.Name = "btnClearance"
        Me.btnClearance.Size = New System.Drawing.Size(91, 26)
        Me.btnClearance.TabIndex = 101
        Me.btnClearance.Tag = "3"
        Me.btnClearance.Text = "Clear"
        Me.btnClearance.UseVisualStyleBackColor = true
        '
        'btnOOB
        '
        Me.btnOOB.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOOB.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnOOB.Location = New System.Drawing.Point(147, 96)
        Me.btnOOB.Name = "btnOOB"
        Me.btnOOB.Size = New System.Drawing.Size(91, 26)
        Me.btnOOB.TabIndex = 99
        Me.btnOOB.Tag = "67"
        Me.btnOOB.Text = "Out of Bounds"
        Me.btnOOB.UseVisualStyleBackColor = true
        '
        'btnUncontrolled
        '
        Me.btnUncontrolled.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnUncontrolled.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnUncontrolled.Location = New System.Drawing.Point(6, 67)
        Me.btnUncontrolled.Name = "btnUncontrolled"
        Me.btnUncontrolled.Size = New System.Drawing.Size(91, 26)
        Me.btnUncontrolled.TabIndex = 98
        Me.btnUncontrolled.Tag = "76"
        Me.btnUncontrolled.Text = "Involuntary"
        Me.btnUncontrolled.UseVisualStyleBackColor = true
        '
        'btnGoalKick
        '
        Me.btnGoalKick.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalKick.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnGoalKick.Location = New System.Drawing.Point(101, 76)
        Me.btnGoalKick.Name = "btnGoalKick"
        Me.btnGoalKick.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalKick.TabIndex = 110
        Me.btnGoalKick.Tag = "46"
        Me.btnGoalKick.Text = "Goal Kick"
        Me.btnGoalKick.UseVisualStyleBackColor = true
        '
        'btnCorner
        '
        Me.btnCorner.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCorner.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnCorner.Location = New System.Drawing.Point(51, 107)
        Me.btnCorner.Name = "btnCorner"
        Me.btnCorner.Size = New System.Drawing.Size(91, 26)
        Me.btnCorner.TabIndex = 108
        Me.btnCorner.Tag = "5"
        Me.btnCorner.Text = "Corner"
        Me.btnCorner.UseVisualStyleBackColor = true
        '
        'btnShot
        '
        Me.btnShot.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnShot.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnShot.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnShot.Location = New System.Drawing.Point(101, 9)
        Me.btnShot.Name = "btnShot"
        Me.btnShot.Size = New System.Drawing.Size(91, 26)
        Me.btnShot.TabIndex = 107
        Me.btnShot.Tag = "19"
        Me.btnShot.Text = "Shot"
        Me.btnShot.UseVisualStyleBackColor = false
        '
        'btnOffSide
        '
        Me.btnOffSide.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOffSide.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnOffSide.Location = New System.Drawing.Point(101, 44)
        Me.btnOffSide.Name = "btnOffSide"
        Me.btnOffSide.Size = New System.Drawing.Size(91, 26)
        Me.btnOffSide.TabIndex = 106
        Me.btnOffSide.Tag = "16"
        Me.btnOffSide.Text = "Offside"
        Me.btnOffSide.UseVisualStyleBackColor = true
        '
        'btnFoul
        '
        Me.btnFoul.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFoul.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnFoul.Location = New System.Drawing.Point(4, 9)
        Me.btnFoul.Name = "btnFoul"
        Me.btnFoul.Size = New System.Drawing.Size(91, 26)
        Me.btnFoul.TabIndex = 105
        Me.btnFoul.Tag = "8"
        Me.btnFoul.Text = "Foul"
        Me.btnFoul.UseVisualStyleBackColor = true
        '
        'btnThrowIn
        '
        Me.btnThrowIn.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnThrowIn.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnThrowIn.Location = New System.Drawing.Point(4, 44)
        Me.btnThrowIn.Name = "btnThrowIn"
        Me.btnThrowIn.Size = New System.Drawing.Size(91, 26)
        Me.btnThrowIn.TabIndex = 103
        Me.btnThrowIn.Tag = "47"
        Me.btnThrowIn.Text = "Throw In"
        Me.btnThrowIn.UseVisualStyleBackColor = true
        '
        'btnFreeKick
        '
        Me.btnFreeKick.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnFreeKick.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFreeKick.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnFreeKick.Location = New System.Drawing.Point(4, 76)
        Me.btnFreeKick.Name = "btnFreeKick"
        Me.btnFreeKick.Size = New System.Drawing.Size(91, 26)
        Me.btnFreeKick.TabIndex = 102
        Me.btnFreeKick.Tag = "9"
        Me.btnFreeKick.Text = "Free Kick"
        Me.btnFreeKick.UseVisualStyleBackColor = false
        '
        'pnlGoalsLeft
        '
        Me.pnlGoalsLeft.BackColor = System.Drawing.Color.FromArgb(CType(CType(21,Byte),Integer), CType(CType(185,Byte),Integer), CType(CType(94,Byte),Integer))
        Me.pnlGoalsLeft.Controls.Add(Me.btnOwnGoal)
        Me.pnlGoalsLeft.Controls.Add(Me.btnNormalGoal)
        Me.pnlGoalsLeft.Controls.Add(Me.btnPenalty)
        Me.pnlGoalsLeft.Location = New System.Drawing.Point(714, 463)
        Me.pnlGoalsLeft.Name = "pnlGoalsLeft"
        Me.pnlGoalsLeft.Size = New System.Drawing.Size(290, 30)
        Me.pnlGoalsLeft.TabIndex = 111
        '
        'btnOwnGoal
        '
        Me.btnOwnGoal.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOwnGoal.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnOwnGoal.Location = New System.Drawing.Point(193, 2)
        Me.btnOwnGoal.Name = "btnOwnGoal"
        Me.btnOwnGoal.Size = New System.Drawing.Size(91, 26)
        Me.btnOwnGoal.TabIndex = 3
        Me.btnOwnGoal.Tag = "28"
        Me.btnOwnGoal.Text = "Own"
        Me.btnOwnGoal.UseVisualStyleBackColor = true
        '
        'btnNormalGoal
        '
        Me.btnNormalGoal.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnNormalGoal.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnNormalGoal.Location = New System.Drawing.Point(99, 2)
        Me.btnNormalGoal.Name = "btnNormalGoal"
        Me.btnNormalGoal.Size = New System.Drawing.Size(91, 26)
        Me.btnNormalGoal.TabIndex = 2
        Me.btnNormalGoal.Tag = "11"
        Me.btnNormalGoal.Text = "Normal"
        Me.btnNormalGoal.UseVisualStyleBackColor = true
        '
        'btnPenalty
        '
        Me.btnPenalty.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnPenalty.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnPenalty.Location = New System.Drawing.Point(6, 2)
        Me.btnPenalty.Name = "btnPenalty"
        Me.btnPenalty.Size = New System.Drawing.Size(91, 26)
        Me.btnPenalty.TabIndex = 1
        Me.btnPenalty.Tag = "17"
        Me.btnPenalty.Text = "Penalty"
        Me.btnPenalty.UseVisualStyleBackColor = true
        '
        'pnlGoalLblLeft
        '
        Me.pnlGoalLblLeft.BackColor = System.Drawing.Color.Transparent
        Me.pnlGoalLblLeft.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lblGoalBg
        Me.pnlGoalLblLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlGoalLblLeft.Controls.Add(Me.lblGoalAway)
        Me.pnlGoalLblLeft.Location = New System.Drawing.Point(813, 491)
        Me.pnlGoalLblLeft.Name = "pnlGoalLblLeft"
        Me.pnlGoalLblLeft.Size = New System.Drawing.Size(92, 29)
        Me.pnlGoalLblLeft.TabIndex = 112
        '
        'lblGoalAway
        '
        Me.lblGoalAway.AutoSize = true
        Me.lblGoalAway.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblGoalAway.ForeColor = System.Drawing.Color.White
        Me.lblGoalAway.Location = New System.Drawing.Point(25, 5)
        Me.lblGoalAway.Name = "lblGoalAway"
        Me.lblGoalAway.Size = New System.Drawing.Size(42, 15)
        Me.lblGoalAway.TabIndex = 1
        Me.lblGoalAway.Text = "GOAL"
        '
        'btnBooking
        '
        Me.btnBooking.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnBooking.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnBooking.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnBooking.Location = New System.Drawing.Point(190, 3)
        Me.btnBooking.Name = "btnBooking"
        Me.btnBooking.Size = New System.Drawing.Size(91, 26)
        Me.btnBooking.TabIndex = 113
        Me.btnBooking.Tag = "2"
        Me.btnBooking.Text = "Booking"
        Me.btnBooking.UseVisualStyleBackColor = false
        '
        'btnSubstitute
        '
        Me.btnSubstitute.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSubstitute.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnSubstitute.Location = New System.Drawing.Point(3, 3)
        Me.btnSubstitute.Name = "btnSubstitute"
        Me.btnSubstitute.Size = New System.Drawing.Size(91, 26)
        Me.btnSubstitute.TabIndex = 114
        Me.btnSubstitute.Tag = "22"
        Me.btnSubstitute.Text = "Substitute"
        Me.btnSubstitute.UseVisualStyleBackColor = true
        '
        'btnRefreshTime
        '
        Me.btnRefreshTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Refresh_btn
        Me.btnRefreshTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefreshTime.Location = New System.Drawing.Point(0, 44)
        Me.btnRefreshTime.Name = "btnRefreshTime"
        Me.btnRefreshTime.Size = New System.Drawing.Size(24, 24)
        Me.btnRefreshTime.TabIndex = 302
        Me.btnRefreshTime.UseVisualStyleBackColor = True
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(56, 46)
        Me.txtTime.Mask = "000:00:000"
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(65, 22)
        Me.txtTime.TabIndex = 301
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(23, 49)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(33, 15)
        Me.lblTime.TabIndex = 300
        Me.lblTime.Text = "Time:"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(87, 77)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(60, 25)
        Me.btnCancel.TabIndex = 299
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(21, 76)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 25)
        Me.btnSave.TabIndex = 298
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Gray
        Me.Panel2.Controls.Add(Me.btn50BW)
        Me.Panel2.Controls.Add(Me.btnAirWon)
        Me.Panel2.Controls.Add(Me.btnObstacle)
        Me.Panel2.Controls.Add(Me.btnOOB)
        Me.Panel2.Controls.Add(Me.btnClearance)
        Me.Panel2.Controls.Add(Me.btnUncontrolled)
        Me.Panel2.Controls.Add(Me.btnTackle)
        Me.Panel2.Controls.Add(Me.btnChance)
        Me.Panel2.Controls.Add(Me.btnControl)
        Me.Panel2.Controls.Add(Me.btnBlocks)
        Me.Panel2.Controls.Add(Me.btnGKAction)
        Me.Panel2.Controls.Add(Me.btnSaves)
        Me.Panel2.Controls.Add(Me.btnPass)
        Me.Panel2.Location = New System.Drawing.Point(413, 272)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(384, 123)
        Me.Panel2.TabIndex = 306
        '
        'pnlEventProperties
        '
        Me.pnlEventProperties.AutoScroll = True
        Me.pnlEventProperties.BackColor = System.Drawing.Color.Lavender
        Me.pnlEventProperties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlEventProperties.Controls.Add(Me.Label7)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp15)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp14)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp14)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp13)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp13)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp12)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp12)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp09)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp11)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp10)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp08)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp07)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp06)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp05)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp04)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp03)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp02)
        Me.pnlEventProperties.Controls.Add(Me.lblEventProp01)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp11)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp10)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp09)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp08)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp07)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp06)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp05)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp04)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp03)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp02)
        Me.pnlEventProperties.Controls.Add(Me.lstEventProp01)
        Me.pnlEventProperties.Location = New System.Drawing.Point(411, 1)
        Me.pnlEventProperties.Name = "pnlEventProperties"
        Me.pnlEventProperties.Size = New System.Drawing.Size(621, 531)
        Me.pnlEventProperties.TabIndex = 310
        Me.pnlEventProperties.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(438, 567)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 15)
        Me.Label7.TabIndex = 46
        Me.Label7.Text = "lblEventProp15"
        Me.Label7.Visible = False
        '
        'lstEventProp15
        '
        Me.lstEventProp15.FormattingEnabled = True
        Me.lstEventProp15.ItemHeight = 15
        Me.lstEventProp15.Location = New System.Drawing.Point(438, 582)
        Me.lstEventProp15.Name = "lstEventProp15"
        Me.lstEventProp15.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp15.TabIndex = 45
        Me.lstEventProp15.Visible = False
        '
        'lblEventProp14
        '
        Me.lblEventProp14.AutoSize = True
        Me.lblEventProp14.Location = New System.Drawing.Point(297, 567)
        Me.lblEventProp14.Name = "lblEventProp14"
        Me.lblEventProp14.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp14.TabIndex = 44
        Me.lblEventProp14.Text = "lblEventProp14"
        Me.lblEventProp14.Visible = False
        '
        'lstEventProp14
        '
        Me.lstEventProp14.FormattingEnabled = True
        Me.lstEventProp14.ItemHeight = 15
        Me.lstEventProp14.Location = New System.Drawing.Point(297, 582)
        Me.lstEventProp14.Name = "lstEventProp14"
        Me.lstEventProp14.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp14.TabIndex = 43
        Me.lstEventProp14.Visible = False
        '
        'lblEventProp13
        '
        Me.lblEventProp13.AutoSize = True
        Me.lblEventProp13.Location = New System.Drawing.Point(156, 566)
        Me.lblEventProp13.Name = "lblEventProp13"
        Me.lblEventProp13.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp13.TabIndex = 42
        Me.lblEventProp13.Text = "lblEventProp13"
        Me.lblEventProp13.Visible = False
        '
        'lstEventProp13
        '
        Me.lstEventProp13.FormattingEnabled = True
        Me.lstEventProp13.ItemHeight = 15
        Me.lstEventProp13.Location = New System.Drawing.Point(156, 581)
        Me.lstEventProp13.Name = "lstEventProp13"
        Me.lstEventProp13.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp13.TabIndex = 41
        Me.lstEventProp13.Visible = False
        '
        'lblEventProp12
        '
        Me.lblEventProp12.AutoSize = True
        Me.lblEventProp12.Location = New System.Drawing.Point(11, 566)
        Me.lblEventProp12.Name = "lblEventProp12"
        Me.lblEventProp12.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp12.TabIndex = 40
        Me.lblEventProp12.Text = "lblEventProp12"
        Me.lblEventProp12.Visible = False
        '
        'lstEventProp12
        '
        Me.lstEventProp12.FormattingEnabled = True
        Me.lstEventProp12.ItemHeight = 15
        Me.lstEventProp12.Location = New System.Drawing.Point(11, 581)
        Me.lstEventProp12.Name = "lstEventProp12"
        Me.lstEventProp12.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp12.TabIndex = 39
        Me.lstEventProp12.Visible = False
        '
        'lblEventProp09
        '
        Me.lblEventProp09.AutoSize = True
        Me.lblEventProp09.Location = New System.Drawing.Point(8, 374)
        Me.lblEventProp09.Name = "lblEventProp09"
        Me.lblEventProp09.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp09.TabIndex = 36
        Me.lblEventProp09.Text = "lblEventProp09"
        Me.lblEventProp09.Visible = False
        '
        'lblEventProp11
        '
        Me.lblEventProp11.AutoSize = True
        Me.lblEventProp11.Location = New System.Drawing.Point(308, 374)
        Me.lblEventProp11.Name = "lblEventProp11"
        Me.lblEventProp11.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp11.TabIndex = 38
        Me.lblEventProp11.Text = "lblEventProp11"
        Me.lblEventProp11.Visible = False
        '
        'lblEventProp10
        '
        Me.lblEventProp10.AutoSize = True
        Me.lblEventProp10.Location = New System.Drawing.Point(158, 374)
        Me.lblEventProp10.Name = "lblEventProp10"
        Me.lblEventProp10.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp10.TabIndex = 37
        Me.lblEventProp10.Text = "lblEventProp10"
        Me.lblEventProp10.Visible = False
        '
        'lblEventProp08
        '
        Me.lblEventProp08.AutoSize = True
        Me.lblEventProp08.Location = New System.Drawing.Point(458, 189)
        Me.lblEventProp08.Name = "lblEventProp08"
        Me.lblEventProp08.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp08.TabIndex = 35
        Me.lblEventProp08.Text = "lblEventProp08"
        Me.lblEventProp08.Visible = False
        '
        'lblEventProp07
        '
        Me.lblEventProp07.AutoSize = True
        Me.lblEventProp07.Location = New System.Drawing.Point(308, 189)
        Me.lblEventProp07.Name = "lblEventProp07"
        Me.lblEventProp07.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp07.TabIndex = 34
        Me.lblEventProp07.Text = "lblEventProp07"
        Me.lblEventProp07.Visible = False
        '
        'lblEventProp06
        '
        Me.lblEventProp06.AutoSize = True
        Me.lblEventProp06.Location = New System.Drawing.Point(158, 189)
        Me.lblEventProp06.Name = "lblEventProp06"
        Me.lblEventProp06.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp06.TabIndex = 33
        Me.lblEventProp06.Text = "lblEventProp06"
        Me.lblEventProp06.Visible = False
        '
        'lblEventProp05
        '
        Me.lblEventProp05.AutoSize = True
        Me.lblEventProp05.Location = New System.Drawing.Point(8, 189)
        Me.lblEventProp05.Name = "lblEventProp05"
        Me.lblEventProp05.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp05.TabIndex = 32
        Me.lblEventProp05.Text = "lblEventProp05"
        Me.lblEventProp05.Visible = False
        '
        'lblEventProp04
        '
        Me.lblEventProp04.AutoSize = True
        Me.lblEventProp04.Location = New System.Drawing.Point(458, 4)
        Me.lblEventProp04.Name = "lblEventProp04"
        Me.lblEventProp04.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp04.TabIndex = 31
        Me.lblEventProp04.Text = "lblEventProp04"
        Me.lblEventProp04.Visible = False
        '
        'lblEventProp03
        '
        Me.lblEventProp03.AutoSize = True
        Me.lblEventProp03.Location = New System.Drawing.Point(308, 4)
        Me.lblEventProp03.Name = "lblEventProp03"
        Me.lblEventProp03.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp03.TabIndex = 30
        Me.lblEventProp03.Text = "lblEventProp03"
        Me.lblEventProp03.Visible = False
        '
        'lblEventProp02
        '
        Me.lblEventProp02.AutoSize = True
        Me.lblEventProp02.Location = New System.Drawing.Point(158, 4)
        Me.lblEventProp02.Name = "lblEventProp02"
        Me.lblEventProp02.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp02.TabIndex = 29
        Me.lblEventProp02.Text = "lblEventProp02"
        Me.lblEventProp02.Visible = False
        '
        'lblEventProp01
        '
        Me.lblEventProp01.AutoSize = True
        Me.lblEventProp01.Location = New System.Drawing.Point(8, 4)
        Me.lblEventProp01.Name = "lblEventProp01"
        Me.lblEventProp01.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp01.TabIndex = 28
        Me.lblEventProp01.Text = "lblEventProp01"
        Me.lblEventProp01.Visible = False
        '
        'lstEventProp11
        '
        Me.lstEventProp11.FormattingEnabled = True
        Me.lstEventProp11.ItemHeight = 15
        Me.lstEventProp11.Location = New System.Drawing.Point(308, 389)
        Me.lstEventProp11.Name = "lstEventProp11"
        Me.lstEventProp11.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp11.TabIndex = 26
        Me.lstEventProp11.Visible = False
        '
        'lstEventProp10
        '
        Me.lstEventProp10.FormattingEnabled = True
        Me.lstEventProp10.ItemHeight = 15
        Me.lstEventProp10.Location = New System.Drawing.Point(158, 389)
        Me.lstEventProp10.Name = "lstEventProp10"
        Me.lstEventProp10.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp10.TabIndex = 25
        Me.lstEventProp10.Visible = False
        '
        'lstEventProp09
        '
        Me.lstEventProp09.FormattingEnabled = True
        Me.lstEventProp09.ItemHeight = 15
        Me.lstEventProp09.Location = New System.Drawing.Point(11, 389)
        Me.lstEventProp09.Name = "lstEventProp09"
        Me.lstEventProp09.Size = New System.Drawing.Size(144, 169)
        Me.lstEventProp09.TabIndex = 24
        Me.lstEventProp09.Visible = False
        '
        'lstEventProp08
        '
        Me.lstEventProp08.FormattingEnabled = True
        Me.lstEventProp08.ItemHeight = 15
        Me.lstEventProp08.Location = New System.Drawing.Point(458, 204)
        Me.lstEventProp08.Name = "lstEventProp08"
        Me.lstEventProp08.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp08.TabIndex = 23
        Me.lstEventProp08.Visible = False
        '
        'lstEventProp07
        '
        Me.lstEventProp07.FormattingEnabled = True
        Me.lstEventProp07.ItemHeight = 15
        Me.lstEventProp07.Location = New System.Drawing.Point(308, 204)
        Me.lstEventProp07.Name = "lstEventProp07"
        Me.lstEventProp07.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp07.TabIndex = 22
        Me.lstEventProp07.Visible = False
        '
        'lstEventProp06
        '
        Me.lstEventProp06.FormattingEnabled = True
        Me.lstEventProp06.ItemHeight = 15
        Me.lstEventProp06.Location = New System.Drawing.Point(158, 204)
        Me.lstEventProp06.Name = "lstEventProp06"
        Me.lstEventProp06.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp06.TabIndex = 21
        Me.lstEventProp06.Visible = False
        '
        'lstEventProp05
        '
        Me.lstEventProp05.FormattingEnabled = True
        Me.lstEventProp05.ItemHeight = 15
        Me.lstEventProp05.Location = New System.Drawing.Point(8, 204)
        Me.lstEventProp05.Name = "lstEventProp05"
        Me.lstEventProp05.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp05.TabIndex = 20
        Me.lstEventProp05.Visible = False
        '
        'lstEventProp04
        '
        Me.lstEventProp04.FormattingEnabled = True
        Me.lstEventProp04.ItemHeight = 15
        Me.lstEventProp04.Location = New System.Drawing.Point(452, 20)
        Me.lstEventProp04.Name = "lstEventProp04"
        Me.lstEventProp04.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp04.TabIndex = 19
        Me.lstEventProp04.Visible = False
        '
        'lstEventProp03
        '
        Me.lstEventProp03.FormattingEnabled = True
        Me.lstEventProp03.ItemHeight = 15
        Me.lstEventProp03.Location = New System.Drawing.Point(304, 20)
        Me.lstEventProp03.Name = "lstEventProp03"
        Me.lstEventProp03.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp03.TabIndex = 18
        Me.lstEventProp03.Visible = False
        '
        'lstEventProp02
        '
        Me.lstEventProp02.FormattingEnabled = True
        Me.lstEventProp02.ItemHeight = 15
        Me.lstEventProp02.Location = New System.Drawing.Point(156, 20)
        Me.lstEventProp02.Name = "lstEventProp02"
        Me.lstEventProp02.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp02.TabIndex = 17
        Me.lstEventProp02.Visible = False
        '
        'lstEventProp01
        '
        Me.lstEventProp01.FormattingEnabled = True
        Me.lstEventProp01.ItemHeight = 15
        Me.lstEventProp01.Location = New System.Drawing.Point(8, 20)
        Me.lstEventProp01.Name = "lstEventProp01"
        Me.lstEventProp01.Size = New System.Drawing.Size(135, 169)
        Me.lstEventProp01.TabIndex = 16
        Me.lstEventProp01.Visible = False
        '
        'pnlSave
        '
        Me.pnlSave.Controls.Add(Me.txtInjTime)
        Me.pnlSave.Controls.Add(Me.lblInjuryTime)
        Me.pnlSave.Controls.Add(Me.btnDecTime)
        Me.pnlSave.Controls.Add(Me.btnIncTime)
        Me.pnlSave.Controls.Add(Me.lblEvent)
        Me.pnlSave.Controls.Add(Me.btnCancel)
        Me.pnlSave.Controls.Add(Me.btnSave)
        Me.pnlSave.Controls.Add(Me.lblTime)
        Me.pnlSave.Controls.Add(Me.txtTime)
        Me.pnlSave.Controls.Add(Me.btnRefreshTime)
        Me.pnlSave.Location = New System.Drawing.Point(415, 396)
        Me.pnlSave.Name = "pnlSave"
        Me.pnlSave.Size = New System.Drawing.Size(168, 139)
        Me.pnlSave.TabIndex = 303
        '
        'txtInjTime
        '
        Me.txtInjTime.Location = New System.Drawing.Point(101, 9)
        Me.txtInjTime.Mask = "000"
        Me.txtInjTime.Name = "txtInjTime"
        Me.txtInjTime.Size = New System.Drawing.Size(30, 22)
        Me.txtInjTime.TabIndex = 306
        Me.txtInjTime.Visible = False
        '
        'lblInjuryTime
        '
        Me.lblInjuryTime.AutoSize = True
        Me.lblInjuryTime.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.lblInjuryTime.Location = New System.Drawing.Point(12, 12)
        Me.lblInjuryTime.Name = "lblInjuryTime"
        Me.lblInjuryTime.Size = New System.Drawing.Size(66, 15)
        Me.lblInjuryTime.TabIndex = 307
        Me.lblInjuryTime.Text = "Injury Time :"
        Me.lblInjuryTime.Visible = False
        '
        'btnDecTime
        '
        Me.btnDecTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.time_down
        Me.btnDecTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDecTime.Location = New System.Drawing.Point(142, 45)
        Me.btnDecTime.Name = "btnDecTime"
        Me.btnDecTime.Size = New System.Drawing.Size(24, 24)
        Me.btnDecTime.TabIndex = 305
        Me.btnDecTime.UseVisualStyleBackColor = True
        '
        'btnIncTime
        '
        Me.btnIncTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.time_up
        Me.btnIncTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnIncTime.Location = New System.Drawing.Point(119, 45)
        Me.btnIncTime.Name = "btnIncTime"
        Me.btnIncTime.Size = New System.Drawing.Size(24, 24)
        Me.btnIncTime.TabIndex = 304
        Me.btnIncTime.UseVisualStyleBackColor = True
        '
        'lblEvent
        '
        Me.lblEvent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEvent.AutoSize = True
        Me.lblEvent.Font = New System.Drawing.Font("Arial Unicode MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEvent.Location = New System.Drawing.Point(32, 117)
        Me.lblEvent.Name = "lblEvent"
        Me.lblEvent.Size = New System.Drawing.Size(46, 16)
        Me.lblEvent.TabIndex = 303
        Me.lblEvent.Text = "Label6"
        Me.lblEvent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEvent.Visible = False
        '
        'dgvPBP
        '
        Me.dgvPBP.AllowUserToAddRows = False
        Me.dgvPBP.AllowUserToDeleteRows = False
        Me.dgvPBP.AllowUserToResizeColumns = False
        Me.dgvPBP.AllowUserToResizeRows = False
        Me.dgvPBP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPBP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPBP.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPBP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPBP.ContextMenuStrip = Me.cmnuTimeline
        Me.dgvPBP.Location = New System.Drawing.Point(410, 4)
        Me.dgvPBP.MultiSelect = False
        Me.dgvPBP.Name = "dgvPBP"
        Me.dgvPBP.ReadOnly = True
        Me.dgvPBP.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvPBP.RowHeadersVisible = False
        Me.dgvPBP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPBP.Size = New System.Drawing.Size(614, 235)
        Me.dgvPBP.TabIndex = 311
        '
        'cmnuTimeline
        '
        Me.cmnuTimeline.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InsertToolStripItem, Me.DeleteActionToolStripItem})
        Me.cmnuTimeline.Name = "cmnuTimeline"
        Me.cmnuTimeline.Size = New System.Drawing.Size(146, 48)
        '
        'InsertToolStripItem
        '
        Me.InsertToolStripItem.Name = "InsertToolStripItem"
        Me.InsertToolStripItem.Size = New System.Drawing.Size(145, 22)
        Me.InsertToolStripItem.Text = "Insert Action"
        '
        'DeleteActionToolStripItem
        '
        Me.DeleteActionToolStripItem.Name = "DeleteActionToolStripItem"
        Me.DeleteActionToolStripItem.Size = New System.Drawing.Size(145, 22)
        Me.DeleteActionToolStripItem.Text = "Delete Action"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnFoul)
        Me.Panel3.Controls.Add(Me.btnFreeKick)
        Me.Panel3.Controls.Add(Me.btnThrowIn)
        Me.Panel3.Controls.Add(Me.btnOffSide)
        Me.Panel3.Controls.Add(Me.btnShot)
        Me.Panel3.Controls.Add(Me.btnCorner)
        Me.Panel3.Controls.Add(Me.btnGoalKick)
        Me.Panel3.Location = New System.Drawing.Point(813, 273)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(195, 143)
        Me.Panel3.TabIndex = 318
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btnSubstitute)
        Me.Panel4.Controls.Add(Me.btnBooking)
        Me.Panel4.Location = New System.Drawing.Point(720, 426)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(285, 32)
        Me.Panel4.TabIndex = 318
        '
        'pnlsubstitute
        '
        Me.pnlsubstitute.AutoScroll = True
        Me.pnlsubstitute.Controls.Add(Me.btnSub33)
        Me.pnlsubstitute.Controls.Add(Me.btnSub32)
        Me.pnlsubstitute.Controls.Add(Me.btnSub31)
        Me.pnlsubstitute.Controls.Add(Me.btnSub29)
        Me.pnlsubstitute.Controls.Add(Me.btnSub28)
        Me.pnlsubstitute.Controls.Add(Me.btnSub27)
        Me.pnlsubstitute.Controls.Add(Me.btnSub26)
        Me.pnlsubstitute.Controls.Add(Me.btnSub25)
        Me.pnlsubstitute.Controls.Add(Me.btnSub24)
        Me.pnlsubstitute.Controls.Add(Me.btnSub23)
        Me.pnlsubstitute.Controls.Add(Me.btnSub22)
        Me.pnlsubstitute.Controls.Add(Me.btnSub21)
        Me.pnlsubstitute.Controls.Add(Me.btnSub20)
        Me.pnlsubstitute.Controls.Add(Me.btnSub19)
        Me.pnlsubstitute.Controls.Add(Me.btnSub18)
        Me.pnlsubstitute.Controls.Add(Me.btnSub17)
        Me.pnlsubstitute.Controls.Add(Me.btnSub16)
        Me.pnlsubstitute.Controls.Add(Me.btnSub15)
        Me.pnlsubstitute.Controls.Add(Me.btnSub14)
        Me.pnlsubstitute.Controls.Add(Me.btnSub13)
        Me.pnlsubstitute.Controls.Add(Me.btnSub12)
        Me.pnlsubstitute.Controls.Add(Me.btnSub11)
        Me.pnlsubstitute.Controls.Add(Me.btnSub10)
        Me.pnlsubstitute.Controls.Add(Me.btnSub9)
        Me.pnlsubstitute.Controls.Add(Me.btnSub8)
        Me.pnlsubstitute.Controls.Add(Me.btnSub7)
        Me.pnlsubstitute.Controls.Add(Me.btnSub6)
        Me.pnlsubstitute.Controls.Add(Me.btnSub5)
        Me.pnlsubstitute.Controls.Add(Me.btnSub4)
        Me.pnlsubstitute.Controls.Add(Me.btnSub3)
        Me.pnlsubstitute.Controls.Add(Me.btnSub2)
        Me.pnlsubstitute.Controls.Add(Me.btnSub1)
        Me.pnlsubstitute.Enabled = False
        Me.pnlsubstitute.Location = New System.Drawing.Point(4, 445)
        Me.pnlsubstitute.Name = "pnlsubstitute"
        Me.pnlsubstitute.Size = New System.Drawing.Size(401, 87)
        Me.pnlsubstitute.TabIndex = 319
        '
        'btnSub33
        '
        Me.btnSub33.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub33.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub33.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub33.Location = New System.Drawing.Point(287, 301)
        Me.btnSub33.Name = "btnSub33"
        Me.btnSub33.Size = New System.Drawing.Size(90, 41)
        Me.btnSub33.TabIndex = 150
        Me.btnSub33.UseVisualStyleBackColor = False
        '
        'btnSub32
        '
        Me.btnSub32.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub32.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub32.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub32.Location = New System.Drawing.Point(193, 301)
        Me.btnSub32.Name = "btnSub32"
        Me.btnSub32.Size = New System.Drawing.Size(90, 41)
        Me.btnSub32.TabIndex = 149
        Me.btnSub32.UseVisualStyleBackColor = False
        '
        'btnSub31
        '
        Me.btnSub31.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub31.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub31.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub31.Location = New System.Drawing.Point(101, 301)
        Me.btnSub31.Name = "btnSub31"
        Me.btnSub31.Size = New System.Drawing.Size(90, 41)
        Me.btnSub31.TabIndex = 148
        Me.btnSub31.UseVisualStyleBackColor = False
        '
        'btnSub29
        '
        Me.btnSub29.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub29.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub29.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub29.Location = New System.Drawing.Point(8, 301)
        Me.btnSub29.Name = "btnSub29"
        Me.btnSub29.Size = New System.Drawing.Size(90, 41)
        Me.btnSub29.TabIndex = 147
        Me.btnSub29.UseVisualStyleBackColor = False
        '
        'btnSub28
        '
        Me.btnSub28.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub28.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub28.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub28.Location = New System.Drawing.Point(287, 257)
        Me.btnSub28.Name = "btnSub28"
        Me.btnSub28.Size = New System.Drawing.Size(90, 41)
        Me.btnSub28.TabIndex = 146
        Me.btnSub28.UseVisualStyleBackColor = False
        '
        'btnSub27
        '
        Me.btnSub27.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub27.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub27.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub27.Location = New System.Drawing.Point(193, 257)
        Me.btnSub27.Name = "btnSub27"
        Me.btnSub27.Size = New System.Drawing.Size(90, 41)
        Me.btnSub27.TabIndex = 145
        Me.btnSub27.UseVisualStyleBackColor = False
        '
        'btnSub26
        '
        Me.btnSub26.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub26.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub26.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub26.Location = New System.Drawing.Point(101, 257)
        Me.btnSub26.Name = "btnSub26"
        Me.btnSub26.Size = New System.Drawing.Size(90, 41)
        Me.btnSub26.TabIndex = 144
        Me.btnSub26.UseVisualStyleBackColor = False
        '
        'btnSub25
        '
        Me.btnSub25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub25.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub25.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub25.Location = New System.Drawing.Point(8, 257)
        Me.btnSub25.Name = "btnSub25"
        Me.btnSub25.Size = New System.Drawing.Size(90, 41)
        Me.btnSub25.TabIndex = 143
        Me.btnSub25.UseVisualStyleBackColor = False
        '
        'btnSub24
        '
        Me.btnSub24.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub24.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub24.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub24.Location = New System.Drawing.Point(287, 215)
        Me.btnSub24.Name = "btnSub24"
        Me.btnSub24.Size = New System.Drawing.Size(90, 41)
        Me.btnSub24.TabIndex = 142
        Me.btnSub24.UseVisualStyleBackColor = False
        '
        'btnSub23
        '
        Me.btnSub23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub23.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub23.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub23.Location = New System.Drawing.Point(193, 215)
        Me.btnSub23.Name = "btnSub23"
        Me.btnSub23.Size = New System.Drawing.Size(90, 41)
        Me.btnSub23.TabIndex = 141
        Me.btnSub23.UseVisualStyleBackColor = False
        '
        'btnSub22
        '
        Me.btnSub22.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub22.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub22.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub22.Location = New System.Drawing.Point(101, 215)
        Me.btnSub22.Name = "btnSub22"
        Me.btnSub22.Size = New System.Drawing.Size(90, 41)
        Me.btnSub22.TabIndex = 140
        Me.btnSub22.UseVisualStyleBackColor = False
        '
        'btnSub21
        '
        Me.btnSub21.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub21.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub21.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub21.Location = New System.Drawing.Point(8, 215)
        Me.btnSub21.Name = "btnSub21"
        Me.btnSub21.Size = New System.Drawing.Size(90, 41)
        Me.btnSub21.TabIndex = 139
        Me.btnSub21.UseVisualStyleBackColor = False
        '
        'btnSub20
        '
        Me.btnSub20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub20.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub20.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub20.Location = New System.Drawing.Point(287, 172)
        Me.btnSub20.Name = "btnSub20"
        Me.btnSub20.Size = New System.Drawing.Size(90, 41)
        Me.btnSub20.TabIndex = 138
        Me.btnSub20.UseVisualStyleBackColor = False
        '
        'btnSub19
        '
        Me.btnSub19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub19.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub19.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub19.Location = New System.Drawing.Point(193, 172)
        Me.btnSub19.Name = "btnSub19"
        Me.btnSub19.Size = New System.Drawing.Size(90, 41)
        Me.btnSub19.TabIndex = 137
        Me.btnSub19.UseVisualStyleBackColor = False
        '
        'btnSub18
        '
        Me.btnSub18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub18.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub18.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub18.Location = New System.Drawing.Point(101, 172)
        Me.btnSub18.Name = "btnSub18"
        Me.btnSub18.Size = New System.Drawing.Size(90, 41)
        Me.btnSub18.TabIndex = 136
        Me.btnSub18.UseVisualStyleBackColor = False
        '
        'btnSub17
        '
        Me.btnSub17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub17.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub17.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub17.Location = New System.Drawing.Point(8, 172)
        Me.btnSub17.Name = "btnSub17"
        Me.btnSub17.Size = New System.Drawing.Size(90, 41)
        Me.btnSub17.TabIndex = 135
        Me.btnSub17.UseVisualStyleBackColor = False
        '
        'btnSub16
        '
        Me.btnSub16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub16.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub16.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub16.Location = New System.Drawing.Point(287, 129)
        Me.btnSub16.Name = "btnSub16"
        Me.btnSub16.Size = New System.Drawing.Size(90, 41)
        Me.btnSub16.TabIndex = 134
        Me.btnSub16.UseVisualStyleBackColor = False
        '
        'btnSub15
        '
        Me.btnSub15.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub15.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub15.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub15.Location = New System.Drawing.Point(193, 129)
        Me.btnSub15.Name = "btnSub15"
        Me.btnSub15.Size = New System.Drawing.Size(90, 41)
        Me.btnSub15.TabIndex = 133
        Me.btnSub15.UseVisualStyleBackColor = False
        '
        'btnSub14
        '
        Me.btnSub14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub14.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub14.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub14.Location = New System.Drawing.Point(101, 129)
        Me.btnSub14.Name = "btnSub14"
        Me.btnSub14.Size = New System.Drawing.Size(90, 41)
        Me.btnSub14.TabIndex = 132
        Me.btnSub14.UseVisualStyleBackColor = False
        '
        'btnSub13
        '
        Me.btnSub13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub13.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub13.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub13.Location = New System.Drawing.Point(8, 129)
        Me.btnSub13.Name = "btnSub13"
        Me.btnSub13.Size = New System.Drawing.Size(90, 41)
        Me.btnSub13.TabIndex = 131
        Me.btnSub13.UseVisualStyleBackColor = False
        '
        'btnSub12
        '
        Me.btnSub12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub12.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub12.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub12.Location = New System.Drawing.Point(287, 87)
        Me.btnSub12.Name = "btnSub12"
        Me.btnSub12.Size = New System.Drawing.Size(90, 41)
        Me.btnSub12.TabIndex = 130
        Me.btnSub12.UseVisualStyleBackColor = False
        '
        'btnSub11
        '
        Me.btnSub11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub11.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub11.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub11.Location = New System.Drawing.Point(193, 87)
        Me.btnSub11.Name = "btnSub11"
        Me.btnSub11.Size = New System.Drawing.Size(90, 41)
        Me.btnSub11.TabIndex = 129
        Me.btnSub11.UseVisualStyleBackColor = False
        '
        'btnSub10
        '
        Me.btnSub10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub10.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub10.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub10.Location = New System.Drawing.Point(101, 87)
        Me.btnSub10.Name = "btnSub10"
        Me.btnSub10.Size = New System.Drawing.Size(90, 41)
        Me.btnSub10.TabIndex = 128
        Me.btnSub10.UseVisualStyleBackColor = False
        '
        'btnSub9
        '
        Me.btnSub9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub9.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub9.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.btnSub9.Location = New System.Drawing.Point(8, 87)
        Me.btnSub9.Name = "btnSub9"
        Me.btnSub9.Size = New System.Drawing.Size(90, 41)
        Me.btnSub9.TabIndex = 127
        Me.btnSub9.UseVisualStyleBackColor = False
        '
        'btnSub8
        '
        Me.btnSub8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub8.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub8.Location = New System.Drawing.Point(287, 45)
        Me.btnSub8.Name = "btnSub8"
        Me.btnSub8.Size = New System.Drawing.Size(90, 41)
        Me.btnSub8.TabIndex = 126
        Me.btnSub8.UseVisualStyleBackColor = False
        '
        'btnSub7
        '
        Me.btnSub7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub7.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub7.Location = New System.Drawing.Point(193, 44)
        Me.btnSub7.Name = "btnSub7"
        Me.btnSub7.Size = New System.Drawing.Size(90, 41)
        Me.btnSub7.TabIndex = 125
        Me.btnSub7.UseVisualStyleBackColor = False
        '
        'btnSub6
        '
        Me.btnSub6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub6.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub6.Location = New System.Drawing.Point(101, 45)
        Me.btnSub6.Name = "btnSub6"
        Me.btnSub6.Size = New System.Drawing.Size(90, 41)
        Me.btnSub6.TabIndex = 124
        Me.btnSub6.UseVisualStyleBackColor = False
        '
        'btnSub5
        '
        Me.btnSub5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub5.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub5.Location = New System.Drawing.Point(8, 45)
        Me.btnSub5.Name = "btnSub5"
        Me.btnSub5.Size = New System.Drawing.Size(90, 41)
        Me.btnSub5.TabIndex = 123
        Me.btnSub5.UseVisualStyleBackColor = False
        '
        'btnSub4
        '
        Me.btnSub4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub4.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub4.Location = New System.Drawing.Point(287, 2)
        Me.btnSub4.Name = "btnSub4"
        Me.btnSub4.Size = New System.Drawing.Size(90, 41)
        Me.btnSub4.TabIndex = 122
        Me.btnSub4.UseVisualStyleBackColor = False
        '
        'btnSub3
        '
        Me.btnSub3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub3.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub3.Location = New System.Drawing.Point(193, 2)
        Me.btnSub3.Name = "btnSub3"
        Me.btnSub3.Size = New System.Drawing.Size(90, 41)
        Me.btnSub3.TabIndex = 121
        Me.btnSub3.UseVisualStyleBackColor = False
        '
        'btnSub2
        '
        Me.btnSub2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub2.Location = New System.Drawing.Point(101, 2)
        Me.btnSub2.Name = "btnSub2"
        Me.btnSub2.Size = New System.Drawing.Size(90, 41)
        Me.btnSub2.TabIndex = 120
        Me.btnSub2.UseVisualStyleBackColor = False
        '
        'btnSub1
        '
        Me.btnSub1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSub1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSub1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSub1.Location = New System.Drawing.Point(8, 2)
        Me.btnSub1.Name = "btnSub1"
        Me.btnSub1.Size = New System.Drawing.Size(90, 41)
        Me.btnSub1.TabIndex = 119
        Me.btnSub1.UseVisualStyleBackColor = False
        '
        'pnlField
        '
        Me.pnlField.Controls.Add(Me.pnlFieldInfo)
        Me.pnlField.Controls.Add(Me.UdcSoccerField1)
        Me.pnlField.Location = New System.Drawing.Point(7, 4)
        Me.pnlField.Name = "pnlField"
        Me.pnlField.Size = New System.Drawing.Size(398, 231)
        Me.pnlField.TabIndex = 320
        '
        'UdcSoccerField1
        '
        Me.UdcSoccerField1.Enabled = False
        Me.UdcSoccerField1.Location = New System.Drawing.Point(52, 0)
        Me.UdcSoccerField1.Name = "UdcSoccerField1"
        Me.UdcSoccerField1.Size = New System.Drawing.Size(345, 230)
        Me.UdcSoccerField1.TabIndex = 308
        Me.UdcSoccerField1.USFAwayTeamName = ""
        Me.UdcSoccerField1.USFDisplayArrow = True
        Me.UdcSoccerField1.USFDisplayTeamName = True
        Me.UdcSoccerField1.USFDisplayTouchInstructions = False
        Me.UdcSoccerField1.USFEventText = ""
        Me.UdcSoccerField1.USFHomeTeamName = ""
        Me.UdcSoccerField1.USFHomeTeamOnLeft = True
        Me.UdcSoccerField1.USFMaximumMarksAllowed = 2
        Me.UdcSoccerField1.USFNextMark = 1
        '
        'pnlGoalframe
        '
        Me.pnlGoalframe.BackColor = System.Drawing.Color.LightBlue
        Me.pnlGoalframe.Controls.Add(Me.UdcSoccerGoalFrame1)
        Me.pnlGoalframe.Controls.Add(Me.pnlInfo)
        Me.pnlGoalframe.Location = New System.Drawing.Point(7, 4)
        Me.pnlGoalframe.Name = "pnlGoalframe"
        Me.pnlGoalframe.Size = New System.Drawing.Size(397, 231)
        Me.pnlGoalframe.TabIndex = 321
        '
        'UdcSoccerGoalFrame1
        '
        Me.UdcSoccerGoalFrame1.Enabled = False
        Me.UdcSoccerGoalFrame1.Location = New System.Drawing.Point(-423, 0)
        Me.UdcSoccerGoalFrame1.Name = "UdcSoccerGoalFrame1"
        Me.UdcSoccerGoalFrame1.Size = New System.Drawing.Size(1242, 184)
        Me.UdcSoccerGoalFrame1.TabIndex = 312
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.Label2)
        Me.pnlInfo.Controls.Add(Me.lblControlCaption)
        Me.pnlInfo.Controls.Add(Me.btnViewField)
        Me.pnlInfo.Controls.Add(Me.btnZoom)
        Me.pnlInfo.Controls.Add(Me.picGFOrangeBlink)
        Me.pnlInfo.Controls.Add(Me.picGFGreenBlink)
        Me.pnlInfo.Controls.Add(Me.lblWidth)
        Me.pnlInfo.Controls.Add(Me.picGFOrange)
        Me.pnlInfo.Controls.Add(Me.picGFGreen)
        Me.pnlInfo.Controls.Add(Me.lblClickLocationOrange)
        Me.pnlInfo.Controls.Add(Me.lblClickLocationGreen)
        Me.pnlInfo.Controls.Add(Me.lblMoveZ)
        Me.pnlInfo.Controls.Add(Me.lblMoveY)
        Me.pnlInfo.Location = New System.Drawing.Point(0, 184)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(397, 47)
        Me.pnlInfo.TabIndex = 313
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 15)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "meters"
        '
        'lblControlCaption
        '
        Me.lblControlCaption.AutoSize = True
        Me.lblControlCaption.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblControlCaption.Location = New System.Drawing.Point(52, 3)
        Me.lblControlCaption.Name = "lblControlCaption"
        Me.lblControlCaption.Size = New System.Drawing.Size(70, 15)
        Me.lblControlCaption.TabIndex = 0
        Me.lblControlCaption.Text = "Goal frame"
        '
        'btnViewField
        '
        Me.btnViewField.BackColor = System.Drawing.Color.Khaki
        Me.btnViewField.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewField.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewField.Location = New System.Drawing.Point(-10, -2)
        Me.btnViewField.Margin = New System.Windows.Forms.Padding(0)
        Me.btnViewField.Name = "btnViewField"
        Me.btnViewField.Size = New System.Drawing.Size(62, 50)
        Me.btnViewField.TabIndex = 16
        Me.btnViewField.Text = "View" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Field"
        Me.btnViewField.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnViewField.UseVisualStyleBackColor = False
        '
        'btnZoom
        '
        Me.btnZoom.Location = New System.Drawing.Point(307, 11)
        Me.btnZoom.Name = "btnZoom"
        Me.btnZoom.Size = New System.Drawing.Size(75, 23)
        Me.btnZoom.TabIndex = 15
        Me.btnZoom.Text = "Wide View"
        Me.btnZoom.UseVisualStyleBackColor = True
        '
        'picGFOrangeBlink
        '
        Me.picGFOrangeBlink.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePinBlink
        Me.picGFOrangeBlink.Location = New System.Drawing.Point(191, 27)
        Me.picGFOrangeBlink.Name = "picGFOrangeBlink"
        Me.picGFOrangeBlink.Size = New System.Drawing.Size(14, 15)
        Me.picGFOrangeBlink.TabIndex = 14
        Me.picGFOrangeBlink.TabStop = False
        '
        'picGFGreenBlink
        '
        Me.picGFGreenBlink.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPinBlink
        Me.picGFGreenBlink.Location = New System.Drawing.Point(191, 9)
        Me.picGFGreenBlink.Name = "picGFGreenBlink"
        Me.picGFGreenBlink.Size = New System.Drawing.Size(14, 13)
        Me.picGFGreenBlink.TabIndex = 13
        Me.picGFGreenBlink.TabStop = False
        '
        'lblWidth
        '
        Me.lblWidth.AutoSize = True
        Me.lblWidth.Location = New System.Drawing.Point(58, 18)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(47, 15)
        Me.lblWidth.TabIndex = 1
        Me.lblWidth.Text = "54 X 7.6"
        '
        'picGFOrange
        '
        Me.picGFOrange.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePin
        Me.picGFOrange.Location = New System.Drawing.Point(191, 28)
        Me.picGFOrange.Name = "picGFOrange"
        Me.picGFOrange.Size = New System.Drawing.Size(14, 13)
        Me.picGFOrange.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picGFOrange.TabIndex = 12
        Me.picGFOrange.TabStop = False
        '
        'picGFGreen
        '
        Me.picGFGreen.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPin
        Me.picGFGreen.Location = New System.Drawing.Point(191, 9)
        Me.picGFGreen.Name = "picGFGreen"
        Me.picGFGreen.Size = New System.Drawing.Size(14, 13)
        Me.picGFGreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picGFGreen.TabIndex = 11
        Me.picGFGreen.TabStop = False
        '
        'lblClickLocationOrange
        '
        Me.lblClickLocationOrange.Location = New System.Drawing.Point(210, 25)
        Me.lblClickLocationOrange.Name = "lblClickLocationOrange"
        Me.lblClickLocationOrange.Size = New System.Drawing.Size(101, 15)
        Me.lblClickLocationOrange.TabIndex = 7
        Me.lblClickLocationOrange.Text = "-"
        '
        'lblClickLocationGreen
        '
        Me.lblClickLocationGreen.Location = New System.Drawing.Point(210, 7)
        Me.lblClickLocationGreen.Name = "lblClickLocationGreen"
        Me.lblClickLocationGreen.Size = New System.Drawing.Size(101, 15)
        Me.lblClickLocationGreen.TabIndex = 6
        Me.lblClickLocationGreen.Text = "-"
        '
        'lblMoveZ
        '
        Me.lblMoveZ.AutoSize = True
        Me.lblMoveZ.Location = New System.Drawing.Point(140, 25)
        Me.lblMoveZ.Name = "lblMoveZ"
        Me.lblMoveZ.Size = New System.Drawing.Size(17, 15)
        Me.lblMoveZ.TabIndex = 5
        Me.lblMoveZ.Text = "Z:"
        '
        'lblMoveY
        '
        Me.lblMoveY.AutoSize = True
        Me.lblMoveY.Location = New System.Drawing.Point(140, 7)
        Me.lblMoveY.Name = "lblMoveY"
        Me.lblMoveY.Size = New System.Drawing.Size(17, 15)
        Me.lblMoveY.TabIndex = 4
        Me.lblMoveY.Text = "Y:"
        '
        'btnReplace
        '
        Me.btnReplace.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnReplace.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnReplace.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReplace.ForeColor = System.Drawing.Color.Black
        Me.btnReplace.Location = New System.Drawing.Point(608, 455)
        Me.btnReplace.Name = "btnReplace"
        Me.btnReplace.Size = New System.Drawing.Size(96, 25)
        Me.btnReplace.TabIndex = 310
        Me.btnReplace.Text = "Replace Team"
        Me.btnReplace.UseVisualStyleBackColor = False
        '
        'btnInsert
        '
        Me.btnInsert.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnInsert.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnInsert.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInsert.ForeColor = System.Drawing.Color.Black
        Me.btnInsert.Location = New System.Drawing.Point(608, 496)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(96, 25)
        Me.btnInsert.TabIndex = 327
        Me.btnInsert.Text = "Insert"
        Me.btnInsert.UseVisualStyleBackColor = False
        '
        'btnFilters
        '
        Me.btnFilters.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFilters.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilters.Location = New System.Drawing.Point(792, 241)
        Me.btnFilters.Name = "btnFilters"
        Me.btnFilters.Size = New System.Drawing.Size(58, 26)
        Me.btnFilters.TabIndex = 331
        Me.btnFilters.Tag = "71"
        Me.btnFilters.Text = "Filter"
        Me.btnFilters.UseVisualStyleBackColor = True
        '
        'chkQC
        '
        Me.chkQC.AutoSize = True
        Me.chkQC.Location = New System.Drawing.Point(589, 244)
        Me.chkQC.Name = "chkQC"
        Me.chkQC.Size = New System.Drawing.Size(43, 19)
        Me.chkQC.TabIndex = 335
        Me.chkQC.Text = "QC"
        Me.chkQC.UseVisualStyleBackColor = True
        '
        'chkAway
        '
        Me.chkAway.AutoSize = True
        Me.chkAway.Location = New System.Drawing.Point(530, 244)
        Me.chkAway.Name = "chkAway"
        Me.chkAway.Size = New System.Drawing.Size(53, 19)
        Me.chkAway.TabIndex = 334
        Me.chkAway.Text = "Away"
        Me.chkAway.UseVisualStyleBackColor = True
        '
        'chkHome
        '
        Me.chkHome.AutoSize = True
        Me.chkHome.Location = New System.Drawing.Point(459, 244)
        Me.chkHome.Name = "chkHome"
        Me.chkHome.Size = New System.Drawing.Size(54, 19)
        Me.chkHome.TabIndex = 333
        Me.chkHome.Text = "Home"
        Me.chkHome.UseVisualStyleBackColor = True
        '
        'chkInc
        '
        Me.chkInc.AutoSize = True
        Me.chkInc.Location = New System.Drawing.Point(414, 244)
        Me.chkInc.Name = "chkInc"
        Me.chkInc.Size = New System.Drawing.Size(41, 19)
        Me.chkInc.TabIndex = 332
        Me.chkInc.Text = "Inc"
        Me.chkInc.UseVisualStyleBackColor = True
        '
        'chkOwnAction
        '
        Me.chkOwnAction.AutoSize = True
        Me.chkOwnAction.Location = New System.Drawing.Point(458, 244)
        Me.chkOwnAction.Name = "chkOwnAction"
        Me.chkOwnAction.Size = New System.Drawing.Size(118, 19)
        Me.chkOwnAction.TabIndex = 336
        Me.chkOwnAction.Text = "Display own Action"
        Me.chkOwnAction.UseVisualStyleBackColor = True
        '
        'UdcSoccerPlayerPositions1
        '
        Me.UdcSoccerPlayerPositions1.AllowDrop = True
        Me.UdcSoccerPlayerPositions1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.UdcSoccerPlayerPositions1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.UdcSoccerPlayerPositions1.Location = New System.Drawing.Point(4, 238)
        Me.UdcSoccerPlayerPositions1.Name = "UdcSoccerPlayerPositions1"
        Me.UdcSoccerPlayerPositions1.Size = New System.Drawing.Size(397, 201)
        Me.UdcSoccerPlayerPositions1.TabIndex = 309
        Me.UdcSoccerPlayerPositions1.USPLeftToRight = True
        '
        'chk1sthalf
        '
        Me.chk1sthalf.AutoSize = True
        Me.chk1sthalf.Location = New System.Drawing.Point(638, 244)
        Me.chk1sthalf.Name = "chk1sthalf"
        Me.chk1sthalf.Size = New System.Drawing.Size(63, 19)
        Me.chk1sthalf.TabIndex = 337
        Me.chk1sthalf.Text = "1st Half"
        Me.chk1sthalf.UseVisualStyleBackColor = True
        '
        'chk2ndhalf
        '
        Me.chk2ndhalf.AutoSize = True
        Me.chk2ndhalf.Location = New System.Drawing.Point(714, 244)
        Me.chk2ndhalf.Name = "chk2ndhalf"
        Me.chk2ndhalf.Size = New System.Drawing.Size(66, 19)
        Me.chk2ndhalf.TabIndex = 338
        Me.chk2ndhalf.Text = "2nd Half"
        Me.chk2ndhalf.UseVisualStyleBackColor = True
        '
        'FrmActionsAssist
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1031, 539)
        Me.ControlBox = False
        Me.Controls.Add(Me.chk2ndhalf)
        Me.Controls.Add(Me.chk1sthalf)
        Me.Controls.Add(Me.pnlSave)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.btnReplace)
        Me.Controls.Add(Me.chkOwnAction)
        Me.Controls.Add(Me.btnFilters)
        Me.Controls.Add(Me.chkQC)
        Me.Controls.Add(Me.chkAway)
        Me.Controls.Add(Me.chkHome)
        Me.Controls.Add(Me.chkInc)
        Me.Controls.Add(Me.pnlField)
        Me.Controls.Add(Me.pnlGoalframe)
        Me.Controls.Add(Me.pnlsubstitute)
        Me.Controls.Add(Me.dgvPBP)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.UdcSoccerPlayerPositions1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.pnlGoalLblLeft)
        Me.Controls.Add(Me.pnlGoalsLeft)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.pnlEventProperties)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = true
        Me.Name = "FrmActionsAssist"
        Me.pnlFieldInfo.ResumeLayout(false)
        Me.pnlFieldInfo.PerformLayout
        CType(Me.picFieldBlinkOrange,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picFieldBlinkGreen,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picFieldOrange,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picFieldGreen,System.ComponentModel.ISupportInitialize).EndInit
        Me.pnlGoalsLeft.ResumeLayout(false)
        Me.pnlGoalLblLeft.ResumeLayout(false)
        Me.pnlGoalLblLeft.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.pnlEventProperties.ResumeLayout(false)
        Me.pnlEventProperties.PerformLayout
        Me.pnlSave.ResumeLayout(false)
        Me.pnlSave.PerformLayout
        CType(Me.dgvPBP,System.ComponentModel.ISupportInitialize).EndInit
        Me.cmnuTimeline.ResumeLayout(false)
        Me.Panel3.ResumeLayout(false)
        Me.Panel4.ResumeLayout(false)
        Me.pnlsubstitute.ResumeLayout(false)
        Me.pnlField.ResumeLayout(false)
        Me.pnlGoalframe.ResumeLayout(false)
        Me.pnlInfo.ResumeLayout(false)
        Me.pnlInfo.PerformLayout
        CType(Me.picGFOrangeBlink,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picGFGreenBlink,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picGFOrange,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picGFGreen,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents pnlFieldInfo As System.Windows.Forms.Panel
    Friend WithEvents picFieldBlinkOrange As System.Windows.Forms.PictureBox
    Friend WithEvents picFieldBlinkGreen As System.Windows.Forms.PictureBox
    Friend WithEvents btnRepeatLast As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents picFieldOrange As System.Windows.Forms.PictureBox
    Friend WithEvents picFieldGreen As System.Windows.Forms.PictureBox
    Friend WithEvents lblSelection2 As System.Windows.Forms.Label
    Friend WithEvents lblSelection1 As System.Windows.Forms.Label
    Friend WithEvents lblFieldY As System.Windows.Forms.Label
    Friend WithEvents lblFieldX As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPass As System.Windows.Forms.Button
    Friend WithEvents btn50BW As System.Windows.Forms.Button
    Friend WithEvents btnSaves As System.Windows.Forms.Button
    Friend WithEvents btnBlocks As System.Windows.Forms.Button
    Friend WithEvents btnGKAction As System.Windows.Forms.Button
    Friend WithEvents btnAirWon As System.Windows.Forms.Button
    Friend WithEvents btnTackle As System.Windows.Forms.Button
    Friend WithEvents btnChance As System.Windows.Forms.Button
    Friend WithEvents btnObstacle As System.Windows.Forms.Button
    Friend WithEvents btnControl As System.Windows.Forms.Button
    Friend WithEvents btnClearance As System.Windows.Forms.Button
    Friend WithEvents btnOOB As System.Windows.Forms.Button
    Friend WithEvents btnGoalKick As System.Windows.Forms.Button
    Friend WithEvents btnCorner As System.Windows.Forms.Button
    Friend WithEvents btnShot As System.Windows.Forms.Button
    Friend WithEvents btnOffSide As System.Windows.Forms.Button
    Friend WithEvents btnFoul As System.Windows.Forms.Button
    Friend WithEvents btnThrowIn As System.Windows.Forms.Button
    Friend WithEvents btnFreeKick As System.Windows.Forms.Button
    Friend WithEvents pnlGoalsLeft As System.Windows.Forms.Panel
    Friend WithEvents btnOwnGoal As System.Windows.Forms.Button
    Friend WithEvents btnNormalGoal As System.Windows.Forms.Button
    Friend WithEvents btnPenalty As System.Windows.Forms.Button
    Friend WithEvents pnlGoalLblLeft As System.Windows.Forms.Panel
    Friend WithEvents lblGoalAway As System.Windows.Forms.Label
    Friend WithEvents btnBooking As System.Windows.Forms.Button
    Friend WithEvents btnSubstitute As System.Windows.Forms.Button
    Friend WithEvents btnRefreshTime As System.Windows.Forms.Button
    Friend WithEvents txtTime As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents UdcSoccerField1 As STATS.SoccerDataCollection.udcSoccerField
    Friend WithEvents UdcSoccerPlayerPositions1 As STATS.SoccerDataCollection.udcSoccerPlayerPositions
    Friend WithEvents pnlEventProperties As System.Windows.Forms.Panel
    Friend WithEvents lstEventProp04 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp03 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp02 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp01 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp11 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp10 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp09 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp08 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp07 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp06 As System.Windows.Forms.ListBox
    Friend WithEvents lstEventProp05 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventProp01 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp02 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp11 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp10 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp09 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp08 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp07 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp06 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp05 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp04 As System.Windows.Forms.Label
    Friend WithEvents lblEventProp03 As System.Windows.Forms.Label
    Friend WithEvents dgvPBP As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents pnlsubstitute As System.Windows.Forms.Panel
    Friend WithEvents btnSub12 As System.Windows.Forms.Button
    Friend WithEvents btnSub11 As System.Windows.Forms.Button
    Friend WithEvents btnSub10 As System.Windows.Forms.Button
    Friend WithEvents btnSub9 As System.Windows.Forms.Button
    Friend WithEvents btnSub8 As System.Windows.Forms.Button
    Friend WithEvents btnSub7 As System.Windows.Forms.Button
    Friend WithEvents btnSub6 As System.Windows.Forms.Button
    Friend WithEvents btnSub5 As System.Windows.Forms.Button
    Friend WithEvents btnSub4 As System.Windows.Forms.Button
    Friend WithEvents btnSub3 As System.Windows.Forms.Button
    Friend WithEvents btnSub2 As System.Windows.Forms.Button
    Friend WithEvents btnSub1 As System.Windows.Forms.Button
    Friend WithEvents pnlField As System.Windows.Forms.Panel
    Friend WithEvents pnlGoalframe As System.Windows.Forms.Panel
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents btnZoom As System.Windows.Forms.Button
    Friend WithEvents picGFOrangeBlink As System.Windows.Forms.PictureBox
    Friend WithEvents picGFGreenBlink As System.Windows.Forms.PictureBox
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents picGFOrange As System.Windows.Forms.PictureBox
    Friend WithEvents picGFGreen As System.Windows.Forms.PictureBox
    Friend WithEvents lblClickLocationOrange As System.Windows.Forms.Label
    Friend WithEvents lblClickLocationGreen As System.Windows.Forms.Label
    Friend WithEvents lblMoveZ As System.Windows.Forms.Label
    Friend WithEvents lblMoveY As System.Windows.Forms.Label
    Friend WithEvents lblControlCaption As System.Windows.Forms.Label
    Friend WithEvents btnViewGoal As System.Windows.Forms.Button
    Friend WithEvents btnViewField As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnUncontrolled As System.Windows.Forms.Button
    Friend WithEvents UdcSoccerGoalFrame1 As STATS.SoccerDataCollection.udcSoccerGoalFrame
    Friend WithEvents pnlSave As System.Windows.Forms.Panel
    Friend WithEvents lblEvent As System.Windows.Forms.Label
    Friend WithEvents btnDecTime As System.Windows.Forms.Button
    Friend WithEvents btnIncTime As System.Windows.Forms.Button
    Friend WithEvents btnReplace As System.Windows.Forms.Button
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents cmnuTimeline As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents InsertToolStripItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnFilters As System.Windows.Forms.Button
    Friend WithEvents chkQC As System.Windows.Forms.CheckBox
    Friend WithEvents chkAway As System.Windows.Forms.CheckBox
    Friend WithEvents chkHome As System.Windows.Forms.CheckBox
    Friend WithEvents chkInc As System.Windows.Forms.CheckBox
    Friend WithEvents chkOwnAction As System.Windows.Forms.CheckBox
    Friend WithEvents DeleteActionToolStripItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtInjTime As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblInjuryTime As System.Windows.Forms.Label
    Friend WithEvents btnSub33 As System.Windows.Forms.Button
    Friend WithEvents btnSub32 As System.Windows.Forms.Button
    Friend WithEvents btnSub31 As System.Windows.Forms.Button
    Friend WithEvents btnSub29 As System.Windows.Forms.Button
    Friend WithEvents btnSub28 As System.Windows.Forms.Button
    Friend WithEvents btnSub27 As System.Windows.Forms.Button
    Friend WithEvents btnSub26 As System.Windows.Forms.Button
    Friend WithEvents btnSub25 As System.Windows.Forms.Button
    Friend WithEvents btnSub24 As System.Windows.Forms.Button
    Friend WithEvents btnSub23 As System.Windows.Forms.Button
    Friend WithEvents btnSub22 As System.Windows.Forms.Button
    Friend WithEvents btnSub21 As System.Windows.Forms.Button
    Friend WithEvents btnSub20 As System.Windows.Forms.Button
    Friend WithEvents btnSub19 As System.Windows.Forms.Button
    Friend WithEvents btnSub18 As System.Windows.Forms.Button
    Friend WithEvents btnSub17 As System.Windows.Forms.Button
    Friend WithEvents btnSub16 As System.Windows.Forms.Button
    Friend WithEvents btnSub15 As System.Windows.Forms.Button
    Friend WithEvents btnSub14 As System.Windows.Forms.Button
    Friend WithEvents btnSub13 As System.Windows.Forms.Button
    Friend WithEvents lblEventProp12 As System.Windows.Forms.Label
    Friend WithEvents lstEventProp12 As System.Windows.Forms.ListBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lstEventProp15 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventProp14 As System.Windows.Forms.Label
    Friend WithEvents lstEventProp14 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventProp13 As System.Windows.Forms.Label
    Friend WithEvents lstEventProp13 As System.Windows.Forms.ListBox
    Friend WithEvents chk1sthalf As System.Windows.Forms.CheckBox
    Friend WithEvents chk2ndhalf As System.Windows.Forms.CheckBox
End Class
