﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddManager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLastName = New System.Windows.Forms.Label
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.picReporterBar = New System.Windows.Forms.PictureBox
        Me.picTopBar = New System.Windows.Forms.PictureBox
        Me.radAwayTeam = New System.Windows.Forms.RadioButton
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.radHomeTeam = New System.Windows.Forms.RadioButton
        Me.lblTeam = New System.Windows.Forms.Label
        Me.picTeamLogo = New System.Windows.Forms.PictureBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.sstMain = New System.Windows.Forms.StatusStrip
        Me.btnApply = New System.Windows.Forms.Button
        Me.btnIgnore = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.lvwManager = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.picButtonBar = New System.Windows.Forms.Panel
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.BackColor = System.Drawing.Color.Transparent
        Me.lblLastName.Location = New System.Drawing.Point(33, 134)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(60, 15)
        Me.lblLastName.TabIndex = 2
        Me.lblLastName.Text = "Last name:"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.BackColor = System.Drawing.Color.Transparent
        Me.lblFirstName.Location = New System.Drawing.Point(33, 106)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(61, 15)
        Me.lblFirstName.TabIndex = 0
        Me.lblFirstName.Text = "First name:"
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(116, 104)
        Me.txtFirstName.MaxLength = 20
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(204, 22)
        Me.txtFirstName.TabIndex = 1
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(116, 131)
        Me.txtLastName.MaxLength = 30
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(204, 22)
        Me.txtLastName.TabIndex = 3
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-80, 15)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(434, 24)
        Me.picReporterBar.TabIndex = 280
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-80, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(434, 17)
        Me.picTopBar.TabIndex = 279
        Me.picTopBar.TabStop = False
        '
        'radAwayTeam
        '
        Me.radAwayTeam.ForeColor = System.Drawing.Color.White
        Me.radAwayTeam.Location = New System.Drawing.Point(169, 25)
        Me.radAwayTeam.Name = "radAwayTeam"
        Me.radAwayTeam.Size = New System.Drawing.Size(107, 19)
        Me.radAwayTeam.TabIndex = 1
        Me.radAwayTeam.TabStop = True
        Me.radAwayTeam.Text = "Away team name"
        Me.radAwayTeam.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.radAwayTeam.UseVisualStyleBackColor = True
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.pnlHeader.Controls.Add(Me.radAwayTeam)
        Me.pnlHeader.Controls.Add(Me.radHomeTeam)
        Me.pnlHeader.Controls.Add(Me.lblTeam)
        Me.pnlHeader.Location = New System.Drawing.Point(68, 38)
        Me.pnlHeader.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(625, 53)
        Me.pnlHeader.TabIndex = 12
        '
        'radHomeTeam
        '
        Me.radHomeTeam.ForeColor = System.Drawing.Color.White
        Me.radHomeTeam.Location = New System.Drawing.Point(169, 6)
        Me.radHomeTeam.Name = "radHomeTeam"
        Me.radHomeTeam.Size = New System.Drawing.Size(108, 19)
        Me.radHomeTeam.TabIndex = 0
        Me.radHomeTeam.TabStop = True
        Me.radHomeTeam.Text = "Home team name"
        Me.radHomeTeam.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.radHomeTeam.UseVisualStyleBackColor = True
        '
        'lblTeam
        '
        Me.lblTeam.AutoSize = True
        Me.lblTeam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.ForeColor = System.Drawing.Color.White
        Me.lblTeam.Location = New System.Drawing.Point(9, 10)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(115, 25)
        Me.lblTeam.TabIndex = 0
        Me.lblTeam.Text = "Team name"
        '
        'picTeamLogo
        '
        Me.picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picTeamLogo.Location = New System.Drawing.Point(12, 46)
        Me.picTeamLogo.Name = "picTeamLogo"
        Me.picTeamLogo.Size = New System.Drawing.Size(45, 45)
        Me.picTeamLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picTeamLogo.TabIndex = 286
        Me.picTeamLogo.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(268, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 419)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(351, 18)
        Me.sstMain.TabIndex = 11
        Me.sstMain.Text = "StatusStrip1"
        '
        'btnApply
        '
        Me.btnApply.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnApply.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnApply.ForeColor = System.Drawing.Color.White
        Me.btnApply.Location = New System.Drawing.Point(211, 172)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(53, 25)
        Me.btnApply.TabIndex = 7
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = False
        '
        'btnIgnore
        '
        Me.btnIgnore.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnIgnore.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnIgnore.Enabled = False
        Me.btnIgnore.ForeColor = System.Drawing.Color.White
        Me.btnIgnore.Location = New System.Drawing.Point(270, 172)
        Me.btnIgnore.Name = "btnIgnore"
        Me.btnIgnore.Size = New System.Drawing.Size(53, 25)
        Me.btnIgnore.TabIndex = 8
        Me.btnIgnore.Text = "Ignore"
        Me.btnIgnore.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.Enabled = False
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(93, 172)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(53, 25)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Enabled = False
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(152, 172)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(53, 25)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnAdd.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(34, 172)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(53, 25)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'lvwManager
        '
        Me.lvwManager.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvwManager.AutoArrange = False
        Me.lvwManager.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwManager.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvwManager.FullRowSelect = True
        Me.lvwManager.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwManager.HideSelection = False
        Me.lvwManager.Location = New System.Drawing.Point(8, 208)
        Me.lvwManager.MultiSelect = False
        Me.lvwManager.Name = "lvwManager"
        Me.lvwManager.ShowItemToolTips = True
        Me.lvwManager.Size = New System.Drawing.Size(334, 155)
        Me.lvwManager.TabIndex = 9
        Me.lvwManager.UseCompatibleStateImageBehavior = False
        Me.lvwManager.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Manager"
        Me.ColumnHeader1.Width = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Team"
        Me.ColumnHeader2.Width = 100
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 372)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(388, 52)
        Me.picButtonBar.TabIndex = 10
        '
        'frmAddManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(351, 437)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.btnIgnore)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.lvwManager)
        Me.Controls.Add(Me.pnlHeader)
        Me.Controls.Add(Me.picTeamLogo)
        Me.Controls.Add(Me.lblLastName)
        Me.Controls.Add(Me.lblFirstName)
        Me.Controls.Add(Me.txtFirstName)
        Me.Controls.Add(Me.txtLastName)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmAddManager"
        Me.Text = "Add New Manager"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlHeader.PerformLayout()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents radAwayTeam As System.Windows.Forms.RadioButton
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents radHomeTeam As System.Windows.Forms.RadioButton
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents picTeamLogo As System.Windows.Forms.PictureBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnIgnore As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lvwManager As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
