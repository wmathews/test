﻿
#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports SpeechLib
Imports System.Xml
Imports System.Text.RegularExpressions

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmTouches
' Author        : Dijo Davis
' Created Date  : 27-05-09
' Description   : This is the entry form for touches module. This form will be contained within frmMain at runtime.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmTouches

#Region "Constants & Variables"
    Private m_objUtility As New clsUtility
    Dim m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Dim m_objclsTouches As STATS.SoccerBL.clsTouches = clsTouches.GetInstance()
    Dim m_objclsLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Dim m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Dim m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()

    Private m_objSportVU As New clsSportVU
    Private m_objUtilAudit As New clsAuditLog
    Private m_blnlvwValidate As Boolean = False
    Private m_strTxtTime As String = String.Empty
    Private m_decSequenceNo As String = "-1"
    Private m_intTouchTeamID As Integer = -1
    Private m_intTouchTypeID As Integer = -1
    Private m_hthome As New Hashtable
    Private m_htAway As New Hashtable

    Private m_hthomeUniform As New Hashtable
    Private m_htAwayUniform As New Hashtable

    Private m_blnChkTyping As Boolean = False
    Private m_X_FieldZone As Integer = -1
    Private m_Y_FieldZone As Integer = -1
    Private m_X_FieldZone_Def As Integer = -1
    Private m_Y_FieldZone_Def As Integer = -1
    Private m_dsTouchData As New DataSet()
    Private m_dtHome As DataTable
    Private m_dtAway As DataTable
    Private m_blnHomeLineUp As Boolean = False
    Private m_blnVisitLineUp As Boolean = False
    Private m_CurrentPeriod As Integer
    Private m_strPlayerID As String
    'RM 7904 - Recipient_Code
    Private m_strRecipientID As String
    Private m_boolPlayer1Filled As Boolean = False
    Private m_strUniqueId As String = String.Empty
    Private m_intRecTouchTeamID As Integer = -1

    'RM 7904 Recipient_Code
    Private m_touchType As Integer
    Private m_EdTouchType As Integer
    Private m_EdrecID As Integer
    Private m_EdrecUnID As Integer
    Private m_EdteamID As Integer
    Private m_boolLastEventEdit As Boolean = False

    'Touch details - will be used to reduce db hit
    Private m_TDplayerID As Integer = -1
    Private m_TDsecondPlayer As Integer = -1
    Private m_TDtouchTypeID As Integer = -1
    Private m_TDxFieldZone As Integer = -1
    Private m_TDyFieldZone As Integer = -1
    Private m_TDxFieldZoneDef As Integer = -1
    Private m_TDyFieldZoneDef As Integer = -1


    Private m_teamID As Integer
    Private m_recID As Integer
    Private m_recUnID As Integer


    Private m_blnMouseMoveOn As Boolean = False
    Private m_intRefresh As Boolean = False
    Private m_blnSavestatus As Boolean = True
    Private m_strTooltipText As String = ""
    'Private m_strUniqueId As String = String.Empty
    Private m_intRightPlayerPanelLeft As Integer = 0
    Private m_intLeftPlayerPanelLeft As Integer = 0
    Private m_intPlayerPanelHeightTall As Integer = 0
    Private m_intPlayerPanelHeightShort As Integer = 0
    Private m_dsFlag As New DataSet()
    Private m_homeStartDone As Boolean = True
    Private m_StartingLineups As New DataSet

    Private Const PLAYER_PANEL_TOP As Integer = 6
    Private Const LEFT_PLAYER_PANEL_LEFT As Integer = 22
    Private Const RIGHT_PLAYER_PANEL_LEFT As Integer = 779
    Private Const BENCH_PANEL_TOP As Integer = 316
    Private Const RIGHT_BENCH_PANEL_LEFT As Integer = 708
    Private Const PLAYER_PANEL_HEIGHT_TALL As Integer = 405
    Private Const PLAYER_PANEL_HEIGHT_SHORT As Integer = 297

    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

    Private MessageDialog As New frmMessageDialog
    'Dim WithEvents RecoContext As SpSharedRecoContext
    'Dim Grammar As ISpeechRecoGrammar
    Dim CharCount As Integer

    Dim bTyping As Boolean = False

    Dim xGrammarXML As New XmlDocument
    Dim sTouchTeam As String
    Dim iTouchTeam As Integer
    Dim sTouchType As String
    Dim sTouchTypeLong As String
    Dim sTouchUniform As String
    Dim iTouchUniform As Integer

    Dim intPrevUniqueID As Integer = 0

    Dim bDisplayPlayers As Boolean = True

    Dim m_dsHomeTeam As DataSet
    Dim m_dsAwayTeam As DataSet
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private dsLineUp As DataSet
    Private lsupport As New languagesupport
    Dim strmessage As String = "Please enter a numeric value"
    Dim strmessage1 As String = "Last Entry in Process, Please try again !!..."
    Dim strmessage2 As String = "You are not allowed to Insert/Update Events"
    Dim strmessage3 As String = "- Primary Reporter is Down!"
    Dim strmessage4 As String = "This record is not yet processed."
    Dim strmessage5 As String = "Please enter a numeric value between 0-90"
    Dim strmessage6 As String = "The record youare trying to edit is already"
    Dim strmessage7 As String = "edited/deleted by other reporters."
    Dim strmessage9 As String = "Please enter a valid time"
    Dim strmessage10 As String = "Invalid Uniform #!"
    Dim strmessage11 As String = "Fill in both team and uniform!"
    Dim strmessage12 As String = "Fill In At Least 1 Item!"
    Dim strmessage13 As String = "Time entered should be greater than 105 Minutes"
    Dim strmessage14 As String = "Time entered should be greater than 90 Minutes"
    Dim strmessage15 As String = "Time entered should be greater than 45 Minutes"
    Dim strmessage8 As String = "Current Clock Time should be greater than elapsed time!"
    Dim strmessage16 As String = "Current Clock Time should be greater than entered time!"
    Dim strmessage17 As String = "The record you are trying to delete is"
    Dim strmessage18 As String = "already edited/deleted by other reporters"
    Dim strmessage19 As String = "Please Save/Clear to continue"
    Dim strmessage20 As String = "Edit/Delete not allowed!"
    '7904 Issue #10 June 09 Edit/deleted not allowed not meaningful
    Dim strmessage30 As String = "The software is already in Edit Mode.Please click on CLEAR button if you wish to modify another event."
    Dim strmessage21 As String = "Please select at least one data point!"
    Dim strmessage22 As String = "Bench player selected - are you sure to proceed?"
    Dim strmessage23 As String = "You can't edit a start period or end period, however you can delete the start period or end period."
    Dim strmessage24 As String = "Assiter not allowed to DELTE Start/End Period!"
    Dim strmessage25 As String = "Please select the player!"
    Dim strmessage26 As String = "The Time entered should be less than the END PERIOD time."
    Dim strmessage27 As String = "Assisters not allowed to add new events! Please edit existing events only."
    Dim strmessage28 As String = "This is NOT the correct team data!"
    Dim strmessage29 As String = "Last edit of this record has not been processed - please wait before editing again!"
    Dim strmessage31 As String = "Please select the Touch Type!"
    Dim strmessage32 As String = "Please select the location(s)!"
    'RM 7904 - Recipient_Code
    Dim strmessage33 As String = "Please select recipient player!"
    Dim strmessage38 As String = "Enter Home Start for TOUCHES module!"
    Dim strmessage53 As String = "Enter Home Start (Extra Time) for TOUCHES module!"
    Dim strmessage54 As String = "You network conection has some problem please check it!"
    Dim strmessage55 As String = "Please start the period first!"

    Dim inhibitAutoCheck As Boolean


#End Region

#Region "Member Variables"
    Private m_btnBackColor As Color = Color.Gold
    Private m_btnDefaultBackColor As Color = Color.WhiteSmoke
    Private m_clrHighlightedButtonColor As Color = Color.Gold
    'RM 7904 - Recipient_Code
    Private m_clrRecipientButtonColor As Color = Color.Orange
    Private m_btntypeBackColor As Color = Color.White

    Private Enum enTeam
        Home
        Away
        None
    End Enum
#End Region



#Region "Event Handling"

    Private Sub frmTouches_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        m_blnChkTyping = False
    End Sub

    ''' <summary>
    ''' To populate and initialize controls
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmTouches_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'ADDED BY SANDEEP TO CREATE STORAGE FOR TOUCHES DATA
            CreateDataPoint()
            m_objGameDetails.PBP = AddPBPColumns()

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAdandon", Nothing, 1, 0)
            m_blnlvwValidate = False

            pnlInstructions.Top = pnlVisit.Top
            pnlInstructions.Width = pnlTouchTypes.Width
            pnlInstructions.BringToFront()
            '7904 Issue #2 June 09 Deactivate the button
            frmMain.EnableMenuItem("SwitchSidesToolStripMenuItem", False)

            If m_objclsGameDetails.SerialNo = 1 Then
                '7904 Issue #8 June 09 show only own touches
                chkOwnTouches.Visible = False
                chkOwnTouches.Checked = False

                frmMain.EnableMenuItem("SwitchSidesToolStripMenuItem", True) 'TOSOCRS-38 Touch Reporter 1-Switch Sides of Teams
                UdcSoccerField1.Visible = False
                Panel1.Visible = False
                pnlTouchDisplay.Visible = False
                pnlTouchTypes.Visible = False
                lblTouchType.Visible = False
                pnlInstructions.Visible = False
                pnlHome.Visible = False
                pnlVisit.Visible = False
                pnlHomeBench.Visible = False
                pnlVisitBench.Visible = False
                bDisplayPlayers = False
                btnHomePrimary.Text = m_objclsGameDetails.HomeTeam
                btnVisitPrimary.Text = m_objclsGameDetails.AwayTeam
                pnlHomePrimary.Left = CInt(((Me.Width / 2) / 2) - ((pnlHomePrimary.Width + pnlVisitPrimary.Width + 20) / 2))
                pnlVisitPrimary.Left = pnlHomePrimary.Left + pnlHomePrimary.Width + 20
                pnlHomePrimary.Top = CInt((Me.Height / 2) - (pnlHomePrimary.Height / 2))
                pnlVisitPrimary.Top = pnlHomePrimary.Top
                Application.DoEvents()
                pnlHomePrimary.Visible = True
                pnlVisitPrimary.Visible = True
                lblTime.Location = New Point(CInt(((Me.Width / 2) + 83)), Me.Top + 30)
                txtTime.Location = New Point(lblTime.Left + lblTime.Width + 2, CInt(((lblTime.Top + (lblTime.Height / 2)) - (txtTime.Height / 2))))
                btnRefreshTime.Location = New Point(txtTime.Left + txtTime.Width + 3, CInt(((lblTime.Top + (lblTime.Height / 2)) - (btnRefreshTime.Height / 2))))
                btnSave.Location = New Point(btnRefreshTime.Left + btnRefreshTime.Width + 3, CInt(((lblTime.Top + (lblTime.Height / 2)) - (btnSave.Height / 2))))
                btnClear.Location = New Point(btnSave.Left + btnSave.Width + 2, CInt(((lblTime.Top + (lblTime.Height / 2)) - (btnClear.Height / 2))))
                lblPeriod.Location = New Point(btnClear.Left + btnClear.Width + 10, lblTime.Top)
                cmbPeriod.Location = New Point(lblPeriod.Left + lblPeriod.Width + 2, CInt(((lblTime.Top + (lblTime.Height / 2)) - (cmbPeriod.Height / 2))))
                lblPeriod.Visible = True
                cmbPeriod.Visible = True
                dgvTouches.Location = New Point(lblTime.Left, Me.Top + 80)
                dgvTouches.Size = New Size(397, 324)
                lnkRefreshPlayers.Location = New Point(dgvTouches.Left, dgvTouches.Top - lnkRefreshPlayers.Height - 3)
                txtCommand.Location = New Point(683, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (txtCommand.Height / 2))))
                chkEnableVoice.Location = New Point(790, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (chkEnableVoice.Height / 2))))
                ChkAllData.Location = New Point(897, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (ChkAllData.Height / 2))))
                chkRevisit.Visible = False
                frmMain.EnableMenuItem("TouchesFinalized", False)
                'lvwTouches.CheckBoxes = False
            Else
                'RM 7902 Issue #7 June 09 Take away int/pass as an option for Rep 2 and 3.
                btnTypeIntPass.Enabled = False
                '7904 Issue #8 June 09 show only own touches
                chkOwnTouches.Visible = True
                chkRevisit.Visible = True

                lblPeriod.Visible = False
                cmbPeriod.Visible = False
                lblTime.Location = New Point(CInt((Me.Width / 2) + 91), 138)
                txtTime.Location = New Point(lblTime.Left + lblTime.Width + 2, CInt(((lblTime.Top + (lblTime.Height / 2)) - (txtTime.Height / 2))))
                btnRefreshTime.Location = New Point(txtTime.Left + txtTime.Width + 3, CInt(((lblTime.Top + (lblTime.Height / 2)) - (btnRefreshTime.Height / 2))))
                btnSave.Location = New Point(btnRefreshTime.Left + btnRefreshTime.Width + 14, CInt(((lblTime.Top + (lblTime.Height / 2)) - (btnSave.Height / 2))))
                btnClear.Location = New Point(btnSave.Left + btnSave.Width + 6, CInt(((lblTime.Top + (lblTime.Height / 2)) - (btnClear.Height / 2))))
                dgvTouches.Location = New Point(CInt((Me.Width / 2) + 83), 185)
                dgvTouches.Size = New Size(397, 234)
                lnkRefreshPlayers.Location = New Point(dgvTouches.Left, dgvTouches.Top - lnkRefreshPlayers.Height - 3)
                txtCommand.Location = New Point(683, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (txtCommand.Height / 2))))
                chkEnableVoice.Location = New Point(790, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (chkEnableVoice.Height / 2))))
                ChkAllData.Location = New Point(897, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (ChkAllData.Height / 2))))
                '7904 Issue #8 June 09 show only own touches
                chkOwnTouches.Location = New Point(765, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (ChkAllData.Height / 2))))
                chkRevisit.Location = New Point(706, CInt(((lnkRefreshPlayers.Top + (lnkRefreshPlayers.Height / 2)) - (chkOwnTouches.Height / 2))))

                m_intRightPlayerPanelLeft = RIGHT_PLAYER_PANEL_LEFT
                m_intLeftPlayerPanelLeft = LEFT_PLAYER_PANEL_LEFT
                UdcSoccerField1.Visible = True
                Panel1.Visible = True
                pnlTouchDisplay.Visible = True
                pnlTouchTypes.Visible = True
                pnlInstructions.Visible = False
                bDisplayPlayers = False
                m_intPlayerPanelHeightTall = PLAYER_PANEL_HEIGHT_TALL
                m_intPlayerPanelHeightShort = PLAYER_PANEL_HEIGHT_SHORT
                frmMain.EnableMenuItem("TouchesFinalized", True)
            End If
            pnlHome.Left = m_intLeftPlayerPanelLeft
            pnlVisit.Left = m_intRightPlayerPanelLeft
            pnlHome.Height = m_intPlayerPanelHeightShort
            pnlVisit.Height = m_intPlayerPanelHeightShort

            'btnHomeTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
            'btnVisitTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
            'btnHomeTeam.Tag = m_objclsGameDetails.HomeTeamID
            'btnVisitTeam.Tag = m_objclsGameDetails.AwayTeamID
            If m_objclsGameDetails.IsRestartGame = False Then
                BindControls()
            End If
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                lblTouchType.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTouchType.Text)
                btnTypePass.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypePass.Text)
                btnTypeSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeSave.Text)
                btnTypeThrowIn.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeThrowIn.Text)
                btnTypeTouch.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeTouch.Text)
                btnTypeInt.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeInt.Text)
                btnTypeCatch.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeCatch.Text)
                btnTypeClearance.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeClearance.Text)
                btnTypeGoalKick.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeGoalKick.Text)
                btnTypeBlock.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeBlock.Text)
                btnTypeTackle.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeTackle.Text)
                btnTypeGoalAttempt.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeGoalAttempt.Text)
                btnTypeOther.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeOther.Text)
                btnTypeIntPass.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeIntPass.Text)
                btnTypeRunWithBall.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnTypeRunWithBall.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnClear.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClear.Text)
                lblTime.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTime.Text)
                lblTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTeam.Text)
                lblDispType.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblDispType.Text)
                lblUniform.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblUniform.Text)
                lblDispLocation.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblDispLocation.Text)
                lnkRefreshPlayers.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lnkRefreshPlayers.Text)
                chkEnableVoice.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), chkEnableVoice.Text)
                ChkAllData.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), ChkAllData.Text)

                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Period")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Time")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Left")
                Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Player")
                Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Type")
                Dim g10 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Location")
                'need to check
                'dgvTouches.Columns(0).Text = g4
                'dgvTouches.Columns(1).Text = g5
                'dgvTouches.Columns(2).Text = g6
                'dgvTouches.Columns(3).Text = g7
                'dgvTouches.Columns(4).Text = g8
                'dgvTouches.Columns(5).Text = g9
                'lvwTouches.Columns(6).Text = g10
                strmessage26 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage26)
                strmessage27 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage27)
                strmessage28 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage28)
                strmessage29 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage29)
                strmessage31 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage31)
                strmessage32 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage32)
                strmessage33 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage33)

            End If


            'GetTouches()
            If m_objclsGameDetails.CurrentPeriod = 0 Then
                UdcSoccerField1.USFHomeTeamOnLeft = True    'set home team on left for field user control
            End If
            'UdcSoccerField1.USFMaximumMarksAllowed = 1

            'UdcSoccerField1.USFEnableOneClickRelocation = True
            UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft   'set home team on left for field user control
            UdcSoccerField1.USFHomeTeamName = m_objGameDetails.HomeTeam
            UdcSoccerField1.USFAwayTeamName = m_objGameDetails.AwayTeam
            UdcSoccerField1.USFDisplayTouchInstructions = True
            frmMain.tmrRefresh.Enabled = True
            chkEnableVoice.Visible = False
            txtCommand.Visible = False

            frmMain.lklHometeam.Links(0).Enabled = False
            frmMain.lklAwayteam.Links(0).Enabled = False
            frmMain.lklHometeam.LinkBehavior = LinkBehavior.NeverUnderline
            frmMain.lklAwayteam.LinkBehavior = LinkBehavior.NeverUnderline
            UdcSoccerField1.USFDisplayArrow = False

            If m_objclsGameDetails.SerialNo = 1 Then
                UdcSoccerField1.Enabled = False
            ElseIf m_objclsGameDetails.SerialNo = 2 Then
                UdcSoccerField1.Enabled = True
                UdcSoccerField1.USFMaximumMarksAllowed = 2
                UdcSoccerField1.USFNextMark = 1
                pnlVisit.Visible = False
                pnlVisitBench.Visible = False
            Else
                UdcSoccerField1.USFMaximumMarksAllowed = 2
                UdcSoccerField1.USFNextMark = 1
                SwitchSides()

                pnlHome.Visible = False
                pnlHomeBench.Visible = False
            End If
            If m_objGameDetails.SerialNo = 2 Or m_objGameDetails.SerialNo = 3 Then
                txtTime.Enabled = False
                btnRefreshTime.Enabled = False
                frmMain.UdcRunningClock1.Visible = False
                frmMain.btnClock.Visible = False
                frmMain.btnSecondDown.Visible = False
                frmMain.btnSecondUp.Visible = False
                frmMain.btnMinuteDown.Visible = False
                frmMain.btnMinuteUp.Visible = False
            End If

            'TO HIDE TEXT
            'pnlInstructions.Visible = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To hold the touches data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CreateDataPoint()
        Try
            'CREATE A DUMMY DATASET TO CREATE TABLES FOR TOUCHES.
            Dim ds As New DataSet

            ds.Tables.Add("LIVE_SOC_PLAYER_TOUCH")
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("GAME_CODE", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("FEED_NUMBER", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("UNIQUE_ID", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("SEQUENCE_NUMBER", GetType(Decimal))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("TIME_ELAPSED", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("PERIOD", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("PLAYER_ID", GetType(Long))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("X_FIELD_ZONE", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("Y_FIELD_ZONE", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("TOUCH_TYPE_ID", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("RECORD_EDITED", GetType(String))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("EDIT_UID", GetType(String))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("X_FIELD_ZONE_OPTICAL", GetType(Decimal))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("Y_FIELD_ZONE_OPTICAL", GetType(Decimal))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("Z_FIELD_ZONE_OPTICAL", GetType(Decimal))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("OPTICAL_TIMESTAMP", GetType(Long))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("REPORTER_ROLE", GetType(String))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("PROCESSED", GetType(Char))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("DEMO_DATA", GetType(String))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("REFRESHED", GetType(String))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("ASSISTER_UID", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("SYSTEM_TIME", GetType(String))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("X_FIELD_ZONE_DEF", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("Y_FIELD_ZONE_DEF", GetType(Integer))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("TEAM_ID", GetType(Integer))
            'RM 7904 - Recipient_Code
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("RECIPIENT_ID", GetType(Long))
            ds.Tables("LIVE_SOC_PLAYER_TOUCH").Columns.Add("MODIFY_TIME", GetType(String))

            m_objGameDetails.TouchesData = ds

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To populate Home Team Name in touch display Label
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeTeam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, m_objclsGameDetails.HomeTeam.Trim(), 1, 0)
            SetTime()
            lblValTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
            m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
            CheckForSave()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try


    End Sub

    ''' <summary>
    ''' To populate Visit Team Name in touch display Label
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitTeam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, m_objclsGameDetails.AwayTeam.Trim(), 1, 0)
            SetTime()
            lblValTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
            m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
            CheckForSave()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub HomePlayers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
     Handles btnHomePlayer1.Click, btnHomePlayer2.Click, btnHomePlayer3.Click, btnHomePlayer4.Click, btnHomePlayer5.Click, _
             btnHomePlayer6.Click, btnHomePlayer7.Click, btnHomePlayer8.Click, btnHomePlayer9.Click, btnHomePlayer10.Click, _
             btnHomePlayer11.Click, btnHomePlayer12.Click, btnHomePlayer13.Click, btnHomePlayer14.Click, btnHomePlayer15.Click, _
             btnHomePlayer16.Click, btnHomePlayer17.Click, btnHomePlayer18.Click, btnHomePlayer19.Click, btnHomePlayer20.Click, _
             btnHomePlayer21.Click, btnHomePlayer22.Click, btnHomePlayer23.Click, btnHomePlayer24.Click, btnHomePlayer25.Click, _
             btnHomePlayer26.Click, btnHomePlayer27.Click, btnHomePlayer28.Click, btnHomePlayer29.Click, btnHomePlayer30.Click, _
             btnHomePlayer31.Click, btnHomePlayer32.Click, btnHomePlayer33.Click, btnHomePlayer34.Click, btnHomePlayer35.Click, _
             btnHomePlayer36.Click, btnHomePlayer37.Click, btnHomePlayer38.Click, btnHomePlayer39.Click, btnHomePlayer40.Click
        Try
            'AUDIT TRIAL
            'm_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text, 1, 0)
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If
            'RM 7904 Recipient_Code

            If DirectCast(sender, System.Windows.Forms.Button).Tag IsNot Nothing Then
                If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then
                    If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 23 Or m_intTouchTypeID = -99) And m_boolPlayer1Filled Then 'TOSOCRS-41
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor
                        SetTime()
                        m_intRecTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
                        m_strRecipientID = String.Empty
                        m_strRecipientID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                    Else
                        m_boolPlayer1Filled = True
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                        SetTime()
                        lblValTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
                        m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
                        If DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim().Length <= 3 Then
                            lblValUniform.Text = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        Else
                            lblValUniform.Text = String.Empty
                        End If
                        m_strPlayerID = String.Empty
                        m_strPlayerID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        m_strRecipientID = String.Empty
                    End If
                    CheckForSave()
                   
                Else
                    If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 23 Or m_intTouchTypeID = -99) Then 'TOSOCRS-41
                        If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                            DirectCast(sender, System.Windows.Forms.Button).BackColor = m_objGameDetails.HomeTeamColor
                            m_strPlayerID = String.Empty
                            lblValUniform.Text = String.Empty
                            lblValTeam.Text = String.Empty
                            m_boolPlayer1Filled = False
                            m_intTouchTeamID = -1
                        End If
                    End If
                   
                End If
            End If

            GetOpticalData()

            If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                DisableOtherButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            ElseIf DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor Then
                DisableRecipientButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            End If
            'Mark the selection in Listview item
            'HighlightLVSelectedRow()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'RM 7904 - Recipient_Code

    Private Sub VisitPlayers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles btnVisitPlayer1.Click, btnVisitPlayer2.Click, btnVisitPlayer3.Click, btnVisitPlayer4.Click, btnVisitPlayer5.Click, _
            btnVisitPlayer6.Click, btnVisitPlayer7.Click, btnVisitPlayer8.Click, btnVisitPlayer9.Click, btnVisitPlayer10.Click, _
            btnVisitPlayer11.Click, btnVisitPlayer12.Click, btnVisitPlayer13.Click, btnVisitPlayer14.Click, btnVisitPlayer15.Click, _
            btnVisitPlayer16.Click, btnVisitPlayer17.Click, btnVisitPlayer18.Click, btnVisitPlayer19.Click, btnVisitPlayer20.Click, _
            btnVisitPlayer21.Click, btnVisitPlayer22.Click, btnVisitPlayer23.Click, btnVisitPlayer24.Click, btnVisitPlayer25.Click, _
            btnVisitPlayer26.Click, btnVisitPlayer27.Click, btnVisitPlayer28.Click, btnVisitPlayer29.Click, btnVisitPlayer30.Click, _
            btnVisitPlayer31.Click, btnVisitPlayer32.Click, btnVisitPlayer33.Click, btnVisitPlayer34.Click, btnVisitPlayer35.Click, _
            btnVisitPlayer36.Click, btnVisitPlayer37.Click, btnVisitPlayer38.Click, btnVisitPlayer39.Click, btnVisitPlayer40.Click, _
            btnVisitPlayer41.Click
        Try
            'AUDIT TRIAL
            'm_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text, 1, 0)
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If
            If DirectCast(sender, System.Windows.Forms.Button).Tag IsNot Nothing Then
                If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then
                    If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 23 Or m_intTouchTypeID = -99) And m_boolPlayer1Filled Then 'TOSOCRS-41
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor
                        SetTime()
                        m_intRecTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
                        m_strRecipientID = String.Empty
                        m_strRecipientID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                    Else
                        m_boolPlayer1Filled = True
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                        SetTime()
                        lblValTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
                        m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
                        If DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim().Length <= 3 Then
                            lblValUniform.Text = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        Else
                            lblValUniform.Text = String.Empty
                        End If
                        m_strPlayerID = String.Empty
                        m_strPlayerID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        m_strRecipientID = String.Empty
                    End If
                    CheckForSave()
                Else
                    If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 23 Or m_intTouchTypeID = -99) Then 'TOSOCRS-41
                        If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                            DirectCast(sender, System.Windows.Forms.Button).BackColor = m_objGameDetails.VisitorTeamColor
                            m_strPlayerID = String.Empty
                            lblValUniform.Text = String.Empty
                            lblValTeam.Text = String.Empty
                            m_boolPlayer1Filled = False
                            m_intTouchTeamID = -1
                        End If
                    End If

                End If
            End If
            GetOpticalData()

            If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                DisableOtherButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            ElseIf DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor Then
                DisableRecipientButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            End If
            'Mark the selection in Listview item
            ' HighlightLVSelectedRow()         

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub HomeSubPlayers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
   Handles btnHomeSub1.Click, btnHomeSub2.Click, btnHomeSub3.Click, btnHomeSub4.Click, btnHomeSub5.Click, _
           btnHomeSub6.Click, btnHomeSub7.Click, btnHomeSub8.Click, btnHomeSub9.Click, btnHomeSub10.Click, _
           btnHomeSub11.Click, btnHomeSub12.Click, btnHomeSub13.Click, btnHomeSub14.Click, btnHomeSub15.Click, _
           btnHomeSub16.Click, btnHomeSub17.Click, btnHomeSub18.Click, btnHomeSub19.Click, btnHomeSub20.Click, _
           btnHomeSub21.Click, btnHomeSub22.Click, btnHomeSub23.Click, btnHomeSub24.Click, btnHomeSub25.Click, _
           btnHomeSub26.Click, btnHomeSub27.Click, btnHomeSub28.Click, btnHomeSub29.Click, btnHomeSub30.Click
        Try
            'AUDIT TRIAL
            If DirectCast(sender, System.Windows.Forms.Button).Text.Trim() <> "" Then
                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If
            If (DirectCast(sender, System.Windows.Forms.Button).Tag IsNot Nothing) Then
                If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then

                    'RM 7904 - #44 Substitues for bench players showing "are you sure"
                    If (DirectCast(sender, System.Windows.Forms.Button).Tag.ToString <> "") Then
                        If CheckIfBenchPlayerIsSubbed(Convert.ToInt32(DirectCast(sender, System.Windows.Forms.Button).Tag.ToString)) = False Then
                            If (MessageDialog.Show(strmessage22, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No) Then
                                'DisableOtherButtonColor(btnClear.Name)
                                If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                                    DisableOtherButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
                                ElseIf DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor Then
                                    DisableRecipientButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
                                End If
                                Exit Sub
                            End If
                        End If
                    End If

                    If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 23 Or m_intTouchTypeID = -99) And m_boolPlayer1Filled Then 'TOSOCRS-41
                        'highlight the recipeinet
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor
                        SetTime()
                        m_intRecTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
                        m_strRecipientID = String.Empty
                        m_strRecipientID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                    Else
                        m_boolPlayer1Filled = True
                        'Exact code before recipient
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                        SetTime()
                        lblValTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
                        m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
                        If DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim().Length <= 3 Then
                            lblValUniform.Text = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        Else
                            lblValUniform.Text = String.Empty
                        End If
                        m_strPlayerID = String.Empty
                        m_strPlayerID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        m_strRecipientID = String.Empty
                    End If
                    CheckForSave()
                End If
            End If

            If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                DisableOtherButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            ElseIf DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor Then
                DisableRecipientButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'Private Sub HighlightLVSelectedRow()
    '    'Mark the selection in Listview item
    '    Try
    '        Dim lvindex As Integer = 0
    '        Dim Nevent As Decimal = 0
    '        'If lvwTouches.Items.Count > 0 Then
    '        '    Select Case m_objclsGameDetails.SerialNo
    '        '        Case 2, 3
    '        '            If lvwTouches.SelectedItems.Count > 0 Then
    '        '                lvindex = lvwTouches.SelectedItems(0).Index
    '        '                Nevent = CDec(lvwTouches.SelectedItems(0).SubItems(7).Text.Trim())
    '        '                Dim lvwTouchesData As New ListViewItem
    '        '                lvwTouchesData = lvwTouches.FindItemWithText(CStr(Nevent))
    '        '                If lvwTouchesData IsNot Nothing Then
    '        '                    lvwTouches.Items(lvwTouchesData.Index).Selected = True
    '        '                    lvwTouches.Select()
    '        '                    lvwTouches.EnsureVisible(lvwTouchesData.Index)
    '        '                End If
    '        '            End If
    '        '    End Select
    '        'End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub VisitSubPlayers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
   Handles btnVisitSub1.Click, btnVisitSub2.Click, btnVisitSub3.Click, btnVisitSub4.Click, btnVisitSub5.Click, _
           btnVisitSub6.Click, btnVisitSub7.Click, btnVisitSub8.Click, btnVisitSub9.Click, btnVisitSub10.Click, _
           btnVisitSub11.Click, btnVisitSub12.Click, btnVisitSub13.Click, btnVisitSub14.Click, btnVisitSub15.Click, _
           btnVisitSub16.Click, btnVisitSub17.Click, btnVisitSub18.Click, btnVisitSub19.Click, btnVisitSub20.Click, _
           btnVisitSub21.Click, btnVisitSub22.Click, btnVisitSub23.Click, btnVisitSub24.Click, btnVisitSub25.Click, _
           btnVisitSub26.Click, btnVisitSub27.Click, btnVisitSub28.Click, btnVisitSub29.Click, btnVisitSub30.Click
        Try
            'AUDIT TRIAL
            If DirectCast(sender, System.Windows.Forms.Button).Text.Trim() <> "" Then
                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If
            If (DirectCast(sender, System.Windows.Forms.Button).Tag IsNot Nothing) Then
                If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then
                    '  'RM 7904 - #44 Substitues for bench players showing "are you sure"
                    If (DirectCast(sender, System.Windows.Forms.Button).Tag.ToString <> "") Then
                        If CheckIfBenchPlayerIsSubbed(Convert.ToInt32(DirectCast(sender, System.Windows.Forms.Button).Tag.ToString)) = False Then
                            If (MessageDialog.Show(strmessage22, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No) Then
                                ' DisableOtherButtonColor(btnClear.Name)
                                If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                                    DisableOtherButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
                                ElseIf DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor Then
                                    DisableRecipientButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
                                End If
                                Exit Sub
                            End If
                        End If
                    End If

                    If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 23 Or m_intTouchTypeID = -99) And m_boolPlayer1Filled Then 'TOSOCRS-41
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor
                        SetTime()
                        m_intRecTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
                        m_strRecipientID = String.Empty
                        m_strRecipientID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                    Else
                        m_boolPlayer1Filled = True
                        DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                        SetTime()
                        lblValTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
                        m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
                        If DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim().Length <= 3 Then
                            lblValUniform.Text = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        Else
                            lblValUniform.Text = String.Empty
                        End If
                        m_strPlayerID = String.Empty
                        m_strPlayerID = DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        m_strRecipientID = String.Empty
                    End If
                    CheckForSave()
                End If

            End If
            If DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor Then
                DisableOtherButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            ElseIf DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrRecipientButtonColor Then
                DisableRecipientButtonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnTouchTypes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles btnAirWin.Click, btn50Win.Click, BtnIntBPass.Click, BtnIntPass.Click, btnGKHand.Click, btnGKThrow.Click, _
    btnTypePass.Click, btnTypeInt.Click, btnTypeIntPass.Click, btnTypeBlock.Click, btnTypeSave.Click, btnTypeCatch.Click, _
    btnTypeTackle.Click, btnTypeOther.Click, btnTypeRunWithBall.Click, btnTypeThrowIn.Click, btnTypeClearance.Click, _
    btnTypeGoalAttempt.Click, btnTypeTouch.Click, btnTypeGoalKick.Click, BtnKpPickUp.Click

        Try
            'm_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btn50Loss.Text, 1, 0)
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            Dim prvTouchType As Integer = m_intTouchTypeID
            'TOSOCRS-310 START
            'TOSOCRS-35 Touch Module - Add warning Message if Reporter 2 or 3 are changing duels
            If (m_intTouchTypeID <> -1) Then
                Select Case CInt(m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1))
                    Case 2, 3
                        Dim SelTouchTypeId As Integer = CInt(DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim())
                        If (m_intTouchTypeID <> SelTouchTypeId) Then
                            If (m_intTouchTypeID = 18 Or m_intTouchTypeID = 19 Or m_intTouchTypeID = 20 Or m_intTouchTypeID = 21) Or _
                                (SelTouchTypeId = 18 Or SelTouchTypeId = 19 Or SelTouchTypeId = 20 Or SelTouchTypeId = 21) Then
                                If MessageDialog.Show("You are about to change " + lblValType.Text + " touch type to " + DirectCast(sender, System.Windows.Forms.Button).Text + ". Are you sure ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.No Then
                                    Exit Sub
                                End If
                            End If
                        End If
                End Select
            End If

            'TOSOCRS-310 END
            If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then
                DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                SetTime()

                Select Case DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                    Case "21"
                        lblValType.Text = "50-Loss"
                        m_intTouchTypeID = 21
                    Case "20"
                        lblValType.Text = "50-Win"
                        m_intTouchTypeID = 20
                    Case "19"
                        lblValType.Text = "Air Lost"
                        m_intTouchTypeID = 19
                    Case "18"
                        lblValType.Text = "Air Win"
                        m_intTouchTypeID = 18
                    Case "22"
                        lblValType.Text = "GK Hand"
                        m_intTouchTypeID = 22
                    Case "23"
                        lblValType.Text = "GK Thrw"
                        m_intTouchTypeID = 23
                    Case "24"
                        lblValType.Text = "K Pickup"
                        m_intTouchTypeID = 24
                    Case "1"
                        lblValType.Text = "Pass"
                        m_intTouchTypeID = 1
                    Case "2"
                        lblValType.Text = "Interception"
                        m_intTouchTypeID = 2
                    Case "-98"
                        lblValType.Text = "Int/BPass"
                        m_intTouchTypeID = -98
                    Case "-99"
                        lblValType.Text = "Int/Pass"
                        m_intTouchTypeID = -99
                    Case "3"
                        lblValType.Text = "Block"
                        m_intTouchTypeID = 3
                    Case "5"
                        lblValType.Text = "Save"
                        m_intTouchTypeID = 5
                    Case "6"
                        lblValType.Text = "Catch"
                        m_intTouchTypeID = 6
                    Case "4"
                        lblValType.Text = "Tackle"
                        m_intTouchTypeID = 4
                    Case "0"
                        lblValType.Text = "Touch"
                        m_intTouchTypeID = 0
                    Case "17"
                        lblValType.Text = "Run With Ball"
                        m_intTouchTypeID = 17
                    Case "8"
                        lblValType.Text = "Throw-In"
                        m_intTouchTypeID = 8
                    Case "9"
                        lblValType.Text = "Clearance"
                        m_intTouchTypeID = 9
                    Case "10"
                        lblValType.Text = "Goal Attempt"
                        m_intTouchTypeID = 10
                    Case "11"
                        lblValType.Text = "Touch"
                        m_intTouchTypeID = 11
                    Case "12"
                        lblValType.Text = "GoalKeeper Long Ball"
                        m_intTouchTypeID = 12
                End Select
                CheckForSave()
            End If
            DisableotherEventbuttonColor(DirectCast(sender, System.Windows.Forms.Button).Name)
            'Mark the selection in Listview item
            'HighlightLVSelectedRow()
            'TOSOCRS-64 [Touch Software-Autofill GoalKeeper for GK Touches]
            Select Case m_intTouchTypeID
                Case 5, 6, 12, 22, 23, 24
                    ClearPlayerSelection()
                    If (m_objGameDetails.SerialNo = 2) Then
                        For Each ctrl As Control In Me.pnlHome.Controls
                            If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                                If CInt(ctrl.Tag) = GetGoalKeeperID(m_objGameDetails.HomeTeamID) And (Integer.Parse(Regex.Replace(ctrl.Name, "[^\d]", "")) < 12) Then
                                    HomePlayers_Click(ctrl, e)
                                    Exit For
                                End If
                            End If

                        Next
                    ElseIf (m_objGameDetails.SerialNo = 3) Then
                        For Each ctrl As Control In Me.pnlVisit.Controls
                            If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                                If (CInt(ctrl.Tag) = GetGoalKeeperID(m_objGameDetails.AwayTeamID)) And (Integer.Parse(Regex.Replace(ctrl.Name, "[^\d]", "")) < 12) Then
                                    VisitPlayers_Click(ctrl, e)
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case Else
                    Select Case prvTouchType
                        Case 5, 6, 12, 22, 23, 24
                            ClearPlayerSelection()
                            SelectedPreviousRecPlayer()
                    End Select
            End Select


        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub



    ''' <summary>
    ''' To clear display labels
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClear.Text, 1, 0)
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            ClearTouchLabels()
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                DisableControls(False)
            End If
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And frmMain.isMenuItemEnabled("EndGameToolStripMenuItem") = False Then
                DisableControls(False)
            End If
            UdcSoccerField1.USFClearMarks()
            lblSelection1.Text = "-"
            lblSelection2.Text = "-"
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
            m_objclsGameDetails.IsEditMode = False
            m_CurrentPeriod = 0
            'RM 7904 - Recipient_Code
            m_boolPlayer1Filled = False
            m_strRecipientID = String.Empty
            m_boolLastEventEdit = False

            DisableOtherButtonColor(btnClear.Name)
            DisableotherEventbuttonColor((btnClear.Name))
            m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
            'SPORT VU 
            If m_objclsGameDetails.IsSportVUAvailable = True Then
                m_objSportVU.UpdateProcessedOpticalTouch(m_objclsGameDetails.GameCode, m_strUniqueId)
            End If
            clearTouchData()
            setHomeStartforTouches()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To refresh players Home/Away
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkRefreshPlayers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRefreshPlayers.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lnkRefreshPlayers.Text, 1, 0)

            'BindTeamRostersByLineUp()
            'SortTeamPlayers()
            GetCurrentPlayersAfterRestart()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function IsdeleteAllowed(ByVal touchID As Integer) As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                Return True
            Else
                If touchID = 14 Or touchID = 15 Then
                    MessageDialog.Show(strmessage24, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To save Touch Information
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Try
            End If

            If m_objclsGameDetails.IsEditMode = False And (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                MessageDialog.Show(strmessage27, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            ' Primary Reporter Down Alert TOSOCRS-45
            If IsMyNetworkUP() = False Then
                Exit Try
            End If

            If IsPBPEntryAllowed(1) = False Then
                Exit Try
            End If

            If CheckForData() = False Then
                Exit Sub
            End If


            If String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            'RM 7904 Time Edit is possible only for Main reporter..
            If m_objclsGameDetails.SerialNo = 1 Then
                Dim chrTime(1) As Char
                chrTime(1) = Convert.ToChar(":")
                Dim strArrTime As String() = txtTime.Text.Split(chrTime)
                If String.IsNullOrEmpty(strArrTime(0).Trim()) And String.IsNullOrEmpty(strArrTime(1).Trim()) Then
                    MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If

                Dim chkRes As Boolean = True
                chkRes = clsValidation.ValidateTime(txtTime.Text.Trim())
                If chkRes Then
                    'Shirley - Throwing error if the input staring is --:44 in this format
                    If strArrTime.Length = 2 Then
                        If strArrTime(0).Trim = "" Or strArrTime(1).Trim = "" Then
                            If strArrTime(0).Trim = "" Then
                                txtTime.Text = "000:" & strArrTime(1).Trim
                            ElseIf strArrTime(1).Trim = "" Then
                                txtTime.Text = strArrTime(0).Trim & ":00"
                            End If
                        End If
                    End If

                    Dim chrSep(1) As Char
                    chrSep(1) = Convert.ToChar(":")
                    Dim strArrCurrentTime As String() = txtTime.Text.Split(chrSep)
                    chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, strArrCurrentTime(0).Trim(), 0, 150)
                Else
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                If Not chkRes Then
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                'If m_objclsGameDetails.IsEditMode = False Then
                If m_objclsGameDetails.SerialNo = 1 Then
                    chkRes = ValidateCurrentTime()
                End If

                'End If
                If Not chkRes Then
                    Exit Sub
                End If
            End If

            'RM 7904 Recipient_Code
            'If (m_intTouchTypeID = 1 Or m_intTouchTypeID = 8) And String.IsNullOrEmpty(m_strRecipientID) = True Then
            ''TOSOCRS-41/TOSOCRS-100 Recipients for new data points 
            If (m_intTouchTypeID = 1 Or m_intTouchTypeID = -99) And String.IsNullOrEmpty(m_strRecipientID) = True Then
                MessageDialog.Show(strmessage33, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                If compareTouchesData(m_intTouchTypeID, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def) Then
                    ClearTouchLabels()
                    'm_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                    m_decSequenceNo = "-1"
                    DisableOtherButtonColor(btnClear.Name)
                    DisableotherEventbuttonColor(btnClear.Name)
                    initializeTouchData()

                    If dgvTouches.SelectedRows.Count > 0 Then
                        checkForNextTouchData(0)
                        dgvTouches_DoubleClick(sender, e)
                        If ChkAllData.Checked = True Then
                            DisplayAllTouchesData()
                        End If
                        'm_blnSavestatus = True
                    End If
                Else
                    SaveTouches()
                    initializeTouchData()

                    If dgvTouches.SelectedRows.Count > 0 Then
                        checkForNextTouchData(1)
                        dgvTouches_DoubleClick(sender, e)
                        If ChkAllData.Checked = True Then
                            DisplayAllTouchesData()
                        End If
                    End If
                    'm_blnSavestatus = True
                End If
            Else
                SaveTouches()
                initializeTouchData()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub checkForNextTouchData(ByVal checkVal As Integer)
        Try
            Dim Nevent As Decimal = 0
            Nevent = CDec(dgvTouches.SelectedRows(0).Cells("Sequence").Value)
            '03/03  SNo 2 Home team and 3 Away Team
            If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                Dim dr() As DataRow
                Select Case m_objclsGameDetails.SerialNo
                    Case 2 'CStr(dgvTouches.SelectedRows(0).Cells("Sequence").Value)
                        If checkVal = 0 Then
                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER > '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                        Else
                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                        End If

                    Case 3
                        If checkVal = 0 Then
                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER > '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                        Else
                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                        End If

                End Select
                If dr.Length > 0 Then
                    Try
                        For Each drrow As DataRow In dr
                            Select Case drrow("TOUCH_TYPE_ID").ToString
                                Case "14", "15"
                                Case Else
                                    For Each row As DataGridViewRow In dgvTouches.Rows
                                        If CDec(row.Cells.Item("Sequence").Value) >= CDec(drrow.Item("Sequence_Number")) Then
                                            dgvTouches.Rows(row.Index).Selected = True
                                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                                            Exit Try
                                        End If
                                    Next
                            End Select
                        Next
                    Catch ex As Exception

                    End Try
                Else
                    btnClear_Click(Nothing, Nothing)
                    For Each row As DataGridViewRow In dgvTouches.Rows
                        dgvTouches.Rows(row.Index).Selected = False
                        dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                    Next
                    Exit Try
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub initializeTouchData()
        Try
            clearTouchData()
            'SANDEEP: ADD THE COMMENT FOR SAVE THE TOUCHES DATA

            If ChkAllData.Checked = True Then
                DisplayAllTouchesData()
            ElseIf chkOwnTouches.Checked = True Then '7904 Issue #8 June 09 Display own touches
                displayOwnTouchesData()
            ElseIf chkRevisit.Checked = True Then '7904 Issue #8 June 09 Display own touches
                RevisitTouchesData()
            Else
                DisplayTouchesData(0)
            End If
            'End If

            'GetTouches(1)
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                DisableControls(False)
            End If

            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And frmMain.isMenuItemEnabled("EndGameToolStripMenuItem") = False Then
                DisableControls(False)
            End If
            DisableOtherButtonColor(btnClear.Name)
            DisableotherEventbuttonColor(btnClear.Name)
            UdcSoccerField1.USFClearMarks()
            lblSelection1.Text = "-"
            m_objclsGameDetails.IsEditMode = False
            m_CurrentPeriod = 0
            'RM 7904 - Recipient_Code
            m_boolPlayer1Filled = False
            m_strRecipientID = String.Empty

            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
            'Arindam 13-Feb-2014 
            btnHomePrimary.BackColor = m_objGameDetails.HomeTeamColor
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To handle field user control click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub UdcSoccerField1_USFClick(ByVal sender As Object, ByVal e As USFEventArgs) Handles UdcSoccerField1.USFClick
        Try
            Dim strFieldarea As String
            Dim strFieldarea2 As String
            strFieldarea = e.USFMarks.Rows(0).Item("FieldArea").ToString
            strFieldarea2 = e.USFMarks.Rows(1).Item("FieldArea").ToString
            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            If strFieldarea2 <> "" Then
                lblSelection2.Text = e.USFMarks.Rows(1).Item("X").ToString & ", " & e.USFMarks.Rows(1).Item("Y").ToString
            End If
            'Dim strFieldarea As String
            strFieldarea = e.USFMarks.Rows(0).Item("FieldArea").ToString
            SetTime()
            If strFieldarea = "" Then
                lblSelection1.Text = "-"
                m_X_FieldZone = -1
                m_Y_FieldZone = -1
                lblValLocation.Text = String.Empty
            Else
                lblSelection1.Text = e.USFMarks.Rows(0).Item("X").ToString & ", " & e.USFMarks.Rows(0).Item("Y").ToString
                m_X_FieldZone = CInt(e.USFMarks.Rows(0).Item("X"))
                m_Y_FieldZone = CInt(e.USFMarks.Rows(0).Item("Y"))
                lblValLocation.Text = "X=" & e.USFMarks.Rows(0).Item("X").ToString & "," & "Y=" & e.USFMarks.Rows(0).Item("Y").ToString
                'm_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.FieldClicked, lblValLocation.Text, 1, 0)
                CheckForSave()
            End If

            If m_objclsGameDetails.SerialNo = 3 Then
                'Second location selection
                If strFieldarea2 = "" Then
                    lblSelection2.Text = "-"
                    m_X_FieldZone_Def = -1
                    m_Y_FieldZone_Def = -1
                    lblLocationVal2.Text = String.Empty
                Else
                    lblSelection2.Text = e.USFMarks.Rows(1).Item("X").ToString & ", " & e.USFMarks.Rows(1).Item("Y").ToString
                    m_X_FieldZone_Def = CInt(e.USFMarks.Rows(1).Item("X"))
                    m_Y_FieldZone_Def = CInt(e.USFMarks.Rows(1).Item("Y"))
                    lblLocationVal2.Text = "X1=" & e.USFMarks.Rows(1).Item("X").ToString & "," & "Y1=" & e.USFMarks.Rows(1).Item("Y").ToString
                    'm_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.FieldClicked, lblLocationVal2.Text, 1, 0)
                    CheckForSave()
                End If
            ElseIf m_objclsGameDetails.SerialNo = 2 Then
                If strFieldarea2 = "" Then
                    lblSelection2.Text = "-"
                    m_X_FieldZone_Def = -1
                    m_Y_FieldZone_Def = -1
                    lblLocationVal2.Text = String.Empty
                Else
                    lblSelection2.Text = e.USFMarks.Rows(1).Item("X").ToString & ", " & e.USFMarks.Rows(1).Item("Y").ToString
                    m_X_FieldZone_Def = CInt(e.USFMarks.Rows(1).Item("X"))
                    m_Y_FieldZone_Def = CInt(e.USFMarks.Rows(1).Item("Y"))
                    lblLocationVal2.Text = "X1=" & e.USFMarks.Rows(1).Item("X").ToString & "," & "Y1=" & e.USFMarks.Rows(1).Item("Y").ToString
                    'm_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.FieldClicked, lblLocationVal2.Text, 1, 0)
                    CheckForSave()
                End If
            Else 'when rep 3 enters - make selection 2 available only for good pass and run with ball
                lblSelection2.Text = "-"
                m_X_FieldZone_Def = -1
                m_Y_FieldZone_Def = -1
                lblLocationVal2.Text = String.Empty
            End If


            strFieldarea = e.USFMarks.Rows(1).Item("FieldArea").ToString
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To handle field user control marks removed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub UdcSoccerField1_USFMarkRemoved(ByVal sender As Object, ByVal e As USFEventArgs) Handles UdcSoccerField1.USFMarkRemoved
        Try
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            Dim strFieldarea As String
            strFieldarea = e.USFMarks.Rows(0).Item("FieldArea").ToString

            If strFieldarea = "" Then
                lblSelection1.Text = "-"
                m_X_FieldZone = -1
                m_Y_FieldZone = -1
                lblValLocation.Text = String.Empty
            Else
                lblSelection1.Text = e.USFMarks.Rows(0).Item("X").ToString & ", " & e.USFMarks.Rows(0).Item("Y").ToString
                m_X_FieldZone = CInt(e.USFMarks.Rows(0).Item("X"))
                m_Y_FieldZone = CInt(e.USFMarks.Rows(0).Item("Y"))
                lblValLocation.Text = "X=" & e.USFMarks.Rows(0).Item("X").ToString & ", " & "Y=" & e.USFMarks.Rows(0).Item("Y").ToString
            End If

            strFieldarea = e.USFMarks.Rows(1).Item("FieldArea").ToString
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.FieldMarksRemoved, lblValLocation.Text, 1, 0)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To handle field user control mouse move
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub UdcSoccerField1_USFMouseMove(ByVal sender As Object, ByVal e As USFEventArgs) Handles UdcSoccerField1.USFMouseMove
        Try
            lblFieldX.Text = "X: " & e.USFx
            lblFieldY.Text = "Y: " & e.USFy
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub setHomeStartforTouches()
        Try
            Dim DtHomeStart As DataSet
            DtHomeStart = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)

            If DtHomeStart.Tables.Count > 0 Then
                If DtHomeStart.Tables(0).Rows.Count > 0 Then
                    If m_objGameDetails.CurrentPeriod > 0 Then
                        If m_objGameDetails.CurrentPeriod < 3 Then
                            If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = False Then
                                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim <> "" Then
                                    If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "L" Then
                                        If m_objGameDetails.CurrentPeriod = 1 Then
                                            m_objGameDetails.IsHomeTeamOnLeft = True
                                        Else
                                            m_objGameDetails.IsHomeTeamOnLeft = False
                                        End If
                                    ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "R" Then
                                        If m_objGameDetails.CurrentPeriod = 1 Then
                                            m_objGameDetails.IsHomeTeamOnLeft = False
                                        Else
                                            m_objGameDetails.IsHomeTeamOnLeft = True
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = False Then
                                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim <> "" Then
                                    If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = "L" Then
                                        If m_objGameDetails.CurrentPeriod = 3 Then
                                            m_objGameDetails.IsHomeTeamOnLeft = True
                                        Else
                                            m_objGameDetails.IsHomeTeamOnLeft = False
                                        End If
                                    ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = "R" Then
                                        If m_objGameDetails.CurrentPeriod = 3 Then
                                            m_objGameDetails.IsHomeTeamOnLeft = False
                                        Else
                                            m_objGameDetails.IsHomeTeamOnLeft = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        m_objGameDetails.isHomeDirectionLeft = m_objGameDetails.IsHomeTeamOnLeft
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    '''' <summary>
    '''' To populate controls with the selected touch data
    '''' </summary>
    '''' <param name="sender"></param>
    '''' <param name="e"></param>
    '''' <remarks></remarks>
    'Private Sub lvwTouches_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvwTouches.SelectedItems.Count > 0 Then
    '            '7904 - Revisit double click dont automatically check the box.
    '            'If lvwTouches.SelectedItems.Item(0).Checked = True Then
    '            lvwTouches.SelectedItems.Item(0).Checked = False
    '            'Else
    '            '    lvwTouches.SelectedItems.Item(0).Checked = True
    '            'End If'

    '            If IsPBPEntryAllowed() = False Then
    '                Exit Try
    '            End If

    '            If Not String.IsNullOrEmpty(lblValTeam.Text) Or Not String.IsNullOrEmpty(m_strPlayerID) Or Not String.IsNullOrEmpty(lblValLocation.Text) Or Not String.IsNullOrEmpty(lblValType.Text) Then
    '                MessageDialog.Show(strmessage19, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '                Exit Sub
    '            End If
    '            If m_blnlvwValidate Then
    '                ClearTouchLabels()
    '                UdcSoccerField1.USFClearMarks()
    '                lblSelection1.Text = "-"
    '                SelectedListViewDisplay_Edit()
    '                DisableControls(True)
    '            End If



    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    Public Function AddPBPColumns() As DataSet
        Try
            Dim dsTempPBPData As New DataSet
            dsTempPBPData.Tables.Add("PBP")

            dsTempPBPData.Tables(0).Columns.Add("UNIQUE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SEQUENCE_NUMBER", GetType(Decimal))
            dsTempPBPData.Tables(0).Columns.Add("TIME_ELAPSED", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("PERIOD", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("TEAM_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("FLAG", GetType(String))

            Return dsTempPBPData

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Showing tooltip of Touches
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    '''' <remarks></remarks>
    'Private Sub lvwTouches_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
    '    Try
    '        Dim lvTouchInfo As ListViewHitTestInfo = lvwTouches.HitTest(e.Location)
    '        'If (lvTouchInfo.SubItem IsNot Nothing) Then
    '        '    'tltpCommentary.Show(lvCommentInfo.SubItem.Text.ToString(), lvwCommentary, New Point(e.Location.X + 20, e.Location.Y + 20), 2000)
    '        '    tltpTouches.SetToolTip(lvwTouches, lvTouchInfo.SubItem.Text.ToString())
    '        'End If

    '        If (lvTouchInfo.SubItem IsNot Nothing) Then
    '            If m_strTooltipText <> lvTouchInfo.SubItem.Text Then
    '                m_blnMouseMoveOn = False
    '                tltpTouches.RemoveAll()
    '            End If
    '            'tltpCommentary.Show(lvCommentInfo.SubItem.Text.ToString(), lvwCommentary, New Point(e.Location.X + 20, e.Location.Y + 20), 2000)
    '            If m_blnMouseMoveOn = False Then
    '                tltpTouches.Active = True
    '                tltpTouches.SetToolTip(lvwTouches, lvTouchInfo.SubItem.Text.ToString())
    '                tltpTouches.AutoPopDelay = 30000
    '                m_strTooltipText = lvTouchInfo.SubItem.Text
    '                m_blnMouseMoveOn = True
    '            End If
    '        End If


    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    ''' <summary>
    ''' To populate controls with the selected touches data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub EditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditToolStripMenuItem.Click
        Try
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If
            If dgvTouches.SelectedRows.Count > 0 Then
                If Not String.IsNullOrEmpty(lblValTeam.Text) Or Not String.IsNullOrEmpty(m_strPlayerID) Or Not String.IsNullOrEmpty(lblValLocation.Text) Or Not String.IsNullOrEmpty(lblValType.Text) Then
                    MessageDialog.Show(strmessage19, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ContextMenuItemSelected, EditToolStripMenuItem.Text, 1, 0)

                SelectedListViewDisplay_Edit()
                DisableControls(True)
                
                'm_objclsGameDetails.IsEditMode = True
                'frmMain.ChangeTheme(True)
                'Me.ChangeTheme(True)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To display current time in minutes and seconds
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtTime.Text = frmMain.UdcRunningClock1.URCCurrentTime
            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
            CheckForSave()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To validate time
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Try
            If Asc(e.KeyChar) <> 8 Then
                Dim chkRes = True
                chkRes = clsValidation.ValidateNumeric(Convert.ToString(e.KeyChar))
                If chkRes = False Then
                    txtTime.Text = String.Empty
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    e.Handled = True
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To validate time
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chrTime(1) As Char
            chrTime(1) = Convert.ToChar(":")
            Dim strArrTime As String() = txtTime.Text.Split(chrTime)
            If Not String.IsNullOrEmpty(strArrTime(0).Trim()) Or Not String.IsNullOrEmpty(strArrTime(0).Trim()) Then
                Dim chkRes As Boolean = True
                Dim chrSep(1) As Char
                chrSep(1) = Convert.ToChar(":")
                Dim strArrCurrentTime As String() = txtTime.Text.Split(chrSep)
                chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, strArrCurrentTime(0).Trim(), 0, 150)
                If chkRes = False Then
                    txtTime.Text = String.Empty
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Else
                    'If m_objclsGameDetails.IsEditMode = False Then
                    ValidateCurrentTime()
                    'End If
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Try
            If IsPBPEntryAllowed(1) = False Then
                Exit Try
            End If

            If Not String.IsNullOrEmpty(lblValTeam.Text) Or Not String.IsNullOrEmpty(m_strPlayerID) Or Not String.IsNullOrEmpty(lblValLocation.Text) Or Not String.IsNullOrEmpty(lblValType.Text) Then
                MessageDialog.Show(strmessage19, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ContextMenuItemSelected, DeleteToolStripMenuItem.Text, 1, 0)

            Dim m_intUID As Integer

            If dgvTouches.SelectedRows.Count > 0 Then
                'For Each lvwitm As ListViewItem In lvwTouches.SelectedItems
                '    m_decSequenceNo = lvwitm.SubItems(7).Text.Trim()
                '    m_intUID = CInt(lvwitm.SubItems(8).Text.Trim())
                'Next
                m_decSequenceNo = CStr(dgvTouches.SelectedRows(0).Cells("Sequence").Value)
                m_intUID = CInt(dgvTouches.SelectedRows(0).Cells("Uid").Value)
            End If

            If m_intUID < 0 Then
                MessageDialog.Show(strmessage4 + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Try
            End If

            If m_decSequenceNo <> "-1" Then
                Dim dsDeleteDataPrev As DataSet
                Dim dsDeleteDataNext As DataSet
                Dim dsDeleteDataCurrent As DataSet
                'Dim PeriodPrev As Integer
                'Dim PeriodCurrent As Integer
                Dim TimeElapsed As Integer


                ''Get Prev, Current and Next event
                '' If Next is NULL - YOU DELETED THE LAST EVENT
                ''If Period of Current and Prev are different, You just deleted START PERIOD
                ''Enable START PERIOD, REDUCE COUNT OF CURRENT PERIOD AND SET MAIN CLOCK WITH PREV TIME
                ''BASED ON NEW PERIOD, SET MAIN CLOCK PERIOD AND CLOCK SHOULD BE STOPPED
                ''If Both PERIOD differ, then 

                dsDeleteDataNext = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "NEXT")
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Delete - UId and Seq No] " & CInt(dgvTouches.SelectedRows(0).Cells("Uid").Value) & "," & m_decSequenceNo, 1, 0)

                Dim currTouchType As Integer
                dsDeleteDataCurrent = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "EQUAL")

                If dsDeleteDataCurrent.Tables(0).Rows.Count > 0 Then

                    If m_objclsGameDetails.SerialNo = 2 Then
                        If dsDeleteDataCurrent.Tables(0).Rows(0).Item("TEAM_ID") IsNot DBNull.Value Then
                            If CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.AwayTeamID Then
                                MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                btnClear_Click(Nothing, Nothing)
                                Exit Sub
                            End If
                        End If
                    ElseIf m_objclsGameDetails.SerialNo = 3 Then
                        If dsDeleteDataCurrent.Tables(0).Rows(0).Item("TEAM_ID") IsNot DBNull.Value Then
                            If CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                                MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                btnClear_Click(Nothing, Nothing)
                                Exit Sub
                            End If
                        End If
                    End If



                End If

                currTouchType = CInt(IIf(IsDBNull(dsDeleteDataCurrent.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = True, "99", dsDeleteDataCurrent.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")))
                If currTouchType = 14 Or currTouchType = 15 Then
                    If IsdeleteAllowed(currTouchType) = False Then
                        Exit Sub
                    End If

                    Dim dsNextData As DataSet
                    dsNextData = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "NEXTNOTIME")
                    If dsNextData.Tables(0).Rows.Count > 0 Then
                        MessageDialog.Show(strmessage20, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If
                End If
                If dsDeleteDataNext.Tables(0).Rows.Count = 0 Then
                    dsDeleteDataPrev = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "PREV")

                    If dsDeleteDataPrev.Tables(0).Rows.Count = 0 Then
                        'Special Case - last record deleted
                        Dim Period As Integer
                        Period = CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD"))
                        If Period = 0 Then
                            frmMain.lblPeriod.Text = "Pre-Game"
                        ElseIf Period = 1 Then
                            frmMain.lblPeriod.Text = "1st Half"
                        ElseIf Period = 2 Then
                            frmMain.lblPeriod.Text = "2nd Half"
                        ElseIf Period = 3 Then
                            frmMain.lblPeriod.Text = "1st ET"
                        ElseIf Period = 4 Then
                            frmMain.lblPeriod.Text = "2nd ET"
                        End If
                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                        Dim StrTime As String = "00:00"
                        Dim strNewTime() As String

                        'Set Main Clock with Prev Seq Time ELapsed
                        StrTime = CalculateTimeElapseAfter(0, Period)
                        strNewTime = StrTime.Split(CChar(":"))
                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                        If m_objclsTouches.DeleteTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole) = 0 Then
                            MessageDialog.Show(strmessage17 + strmessage18, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            m_decSequenceNo = "-1"
                            ClearTouchLabels()
                            UdcSoccerField1.USFClearMarks()
                            lblSelection1.Text = "-"
                            Exit Try
                        End If
                        'If frmMain.UdcRunningClock1.URCIsRunning = False Then
                        '    frmMain.UdcRunningClock1.URCToggle()
                        '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                        'End If
                        DisableControls(True)
                    Else
                        If CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) <> CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")) Then
                            If m_objclsTouches.DeleteTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole) = 0 Then
                                MessageDialog.Show(strmessage17 + strmessage18, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                m_decSequenceNo = "-1"
                                ClearTouchLabels()
                                UdcSoccerField1.USFClearMarks()
                                lblSelection1.Text = "-"
                                Exit Try
                            End If
                            TimeElapsed = CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                            m_objclsGameDetails.CurrentPeriod = CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) - 1
                            ''
                            Dim StrTime As String = "00:00"
                            Dim strNewTime() As String

                            'Set Main Clock with Prev Seq Time ELapsed
                            StrTime = CalculateTimeElapseAfter(TimeElapsed, CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")))
                            strNewTime = StrTime.Split(CChar(":"))
                            frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))

                            If m_objclsGameDetails.CurrentPeriod = 1 Then
                                frmMain.lblPeriod.Text = "1st Half"
                            ElseIf m_objclsGameDetails.CurrentPeriod = 2 Then
                                frmMain.lblPeriod.Text = "2nd Half"
                            ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                                frmMain.lblPeriod.Text = "1st ET"
                            ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                                frmMain.lblPeriod.Text = "2nd ET"
                            End If
                            If m_objclsGameDetails.CurrentPeriod <> 0 And m_objclsGameDetails.IsHalfTimeSwap Then
                                SwitchSides()
                                frmMain.SetHeader2()
                            End If
                            frmMain.btnClock.Enabled = True
                            frmMain.btnMinuteDown.Enabled = True
                            frmMain.btnMinuteUp.Enabled = True
                            frmMain.btnSecondDown.Enabled = True
                            frmMain.btnSecondUp.Enabled = True
                            If frmMain.UdcRunningClock1.URCIsRunning = False Then
                                frmMain.UdcRunningClock1.URCToggle()
                                frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                            End If
                            ''
                        Else
                            If m_objclsTouches.DeleteTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole) = 0 Then
                                MessageDialog.Show(strmessage17 + strmessage18, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                m_decSequenceNo = "-1"
                                ClearTouchLabels()
                                UdcSoccerField1.USFClearMarks()
                                lblSelection1.Text = "-"
                                Exit Try
                            End If
                            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                                DisableControls(True)
                                frmMain.btnClock.Enabled = True
                                frmMain.btnMinuteDown.Enabled = True
                                frmMain.btnMinuteUp.Enabled = True
                                frmMain.btnSecondDown.Enabled = True
                                frmMain.btnSecondUp.Enabled = True
                                If frmMain.UdcRunningClock1.URCIsRunning = False Then
                                    frmMain.UdcRunningClock1.URCToggle()
                                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                End If
                                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                                If m_objclsGameDetails.CurrentPeriod = 1 Then
                                    frmMain.lblPeriod.Text = "1st Half"
                                ElseIf m_objclsGameDetails.CurrentPeriod = 2 Then
                                    frmMain.lblPeriod.Text = "2nd Half"
                                ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                                    frmMain.lblPeriod.Text = "1st ET"
                                ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                                    frmMain.lblPeriod.Text = "2nd ET"
                                End If
                            End If
                        End If
                    End If

                Else
                    ' TOSOCRS-35 Touch Module - Add warning Message if Reporter 2 or 3 are changing duels
                    Select Case CInt(m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1))
                        Case 2, 3
                            Select Case currTouchType
                                Case 18, 19, 20, 21
                                    If MessageDialog.Show("You are about to delete " + CStr(dgvTouches.SelectedRows(0).Cells("Type").Value) + " touch type. Are you sure?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.No Then
                                        Exit Sub
                                    End If
                            End Select
                    End Select
                    If m_objclsTouches.DeleteTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), m_objclsGameDetails.ReporterRole) = 0 Then
                        MessageDialog.Show(strmessage17 + strmessage18, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_decSequenceNo = "-1"
                        ClearTouchLabels()
                        UdcSoccerField1.USFClearMarks()
                        lblSelection1.Text = "-"
                        Exit Try
                    End If
                End If
                If currTouchType = 14 Then
                    deleteStartPeriod()
                End If

                'IF CURRENT REPORTER IS AN ASSISTER THEN LOCK THE SYSTEM TILL THE CONFIRMATION FROM PRIMARY REPORTER
                If m_objclsGameDetails.ReporterRole.Substring(m_objclsGameDetails.ReporterRole.Length - 1) <> "1" Then
                    m_objGeneral.UpdateLastEntryStatus(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, True)
                    m_objclsGameDetails.AssistersLastEntryStatus = False
                End If
                'dsDeleteData = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_decSequenceNo, "PREV")
                'TimeElapsed = CInt(dsDeleteData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                'Period = CInt(dsDeleteData.Tables(0).Rows(0).Item("PERIOD"))
                ''Set Time in Main Clock
                'If Period <> m_objclsGameDetails.CurrentPeriod Then
                '    'Deleted START PERIOD
                'End If
                GetTouches(4)
                If currTouchType = 14 Then
                    frmMain.SetClockControl(True)
                    DisableControls(True)
                End If

                ''7904 Issue #8 June 09 Display own touches
                If chkOwnTouches.Checked Then
                    displayOwnTouchesData()
                ElseIf chkRevisit.Checked Then
                    RevisitTouchesData()
                Else
                    DisplayTouchesData(0)
                End If

                If ChkAllData.Checked Then
                    ChkAllData.Checked = False
                End If


                m_decSequenceNo = "-1"
                ClearTouchLabels()
                UdcSoccerField1.USFClearMarks()
                lblSelection1.Text = "-"
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'AUDIT TRIAL
            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
            'If txtTime.Text <> m_strTxtTime Then
            '    If txtTime.Text <> Nothing Then
            '        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTime.Text, lblTime.Text, 1, 0)
            '    Else
            '        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTime.Text & "' '", lblTime.Text, 1, 0)
            '    End If
            'End If
            'm_strTxtTime = txtTime.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkEnableVoice_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEnableVoice.CheckedChanged
        'Try
        '    'AUDIT TRIAL
        '    If chkEnableVoice.Checked = True Then
        '        chkEnableVoice.Text = "Disable Voice"
        '        Me.txtCommand.Focus()

        '        'First check to see if reco has been loaded before. If not lets load it.
        '        If (RecoContext Is Nothing) Then
        '            RecoContext = New SpSharedRecoContextClass          'Create a new Reco Context Class
        '            Grammar = RecoContext.CreateGrammar(1)              'Setup the Grammar
        '            Grammar.DictationLoad()                             'Load the Grammar
        '            Create_XML()
        '            Try
        '                Grammar.CmdLoadFromFile(My.Application.Deployment.DataDirectory & "xmlGrammar.xml", SpeechLoadOption.SLOStatic)
        '            Catch
        '                Grammar.CmdLoadFromFile("c:\xmlGrammar.xml", SpeechLoadOption.SLOStatic)
        '            End Try
        '        End If

        '        ''Grammar.DictationSetState(SpeechRuleState.SGDSActive)
        '        Grammar.CmdSetRuleIdState(1, SpeechRuleState.SGDSActive)


        '    Else
        '        chkEnableVoice.Text = "Enable Voice"
        '        Me.pnlTouchDisplay.Focus()

        '        'disable speech rec
        '        Grammar.DictationSetState(SpeechRuleState.SGDSInactive)

        '    End If
        'Catch ex As Exception
        '    MessageDialog.Show(ex, Me.Text)
        'End Try
    End Sub

    ''' <summary>
    ''' To validate time
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateCurrentTime() As Boolean
        Try
            Dim chkRes As Boolean = True
            Dim chrTime(1) As Char
            Dim CurrentClockTime As Integer
            Dim EventTime As Integer

            CurrentClockTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
            EventTime = CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text))

            If m_objclsGameDetails.IsEditMode Then
                If CurrentClockTime < EventTime And m_CurrentPeriod = m_objclsGameDetails.CurrentPeriod Then
                    MessageDialog.Show(strmessage16, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtTime.Text = String.Empty
                    Return False
                End If
                If IsEditTimeAllowed(EventTime) = False Then
                    txtTime.Text = String.Empty
                    Return False
                End If
            Else
                If CurrentClockTime < EventTime Then
                    MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtTime.Text = String.Empty
                    Return False
                Else
                    If m_objclsGameDetails.IsDefaultGameClock Then
                        If m_objclsGameDetails.CurrentPeriod = 2 Then
                            If EventTime < FIRSTHALF Then
                                MessageDialog.Show(strmessage15, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Return False
                            End If
                        ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                            If EventTime < SECONDHALF Then
                                MessageDialog.Show(strmessage14, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Return False
                            End If
                        ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                            If EventTime < FIRSTEXTRA Then
                                MessageDialog.Show(strmessage13, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            End If
                        End If
                    End If
                End If
                'Dim dsTouchData As DataSet
                'Dim Period As Integer
                'dsTouchData = m_objclsTouches.GetTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid)
                'If dsTouchData.Tables(0).Rows.Count > 0 Then
                '    Period = CInt(dsTouchData.Tables("Touches").Rows(dsTouchData.Tables.Count - 1).Item("PERIOD"))
                '    'If EventTime < CInt(dsTouchData.Tables("Touches").Rows(dsTouchData.Tables.Count - 1).Item("TIME_ELAPSED")) And Period = m_objclsGameDetails.CurrentPeriod Then
                '    '    essageDialog.Show("Elapsed time should be greater than last entered event time!")
                '    '    txtTime.Text = String.Empty
                '    '    Return False
                '    'End If
                'End If

            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Sub txtTime_LostFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTime.LostFocus
    '    Try
    '        txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

#End Region

#Region "User Defined Functions"

    Public Sub GetTouches(Optional ByVal Source As Integer = 0)
        '1 = INDIVIDUAL EVENTS ENTERED BY USER.
        '2 = MULTI REPORTER CASE.
        '3 = RESTART EXISTING GAME, T1 WORKFLOW.
        '4 = DELETE LAST EVENT ENTRY.
        Try
            Dim ElapsedTime As Integer
            Dim CurrPeriod As Integer
            Dim StrTime As String = "00:00"
            'LvwTouches.Items.Clear()
            m_dsTouchData = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                        If m_objclsGameDetails.IsRestartGame Then
                            Dim isClockStart As Boolean = False
                            ' m_objclsGameDetails.CurrentPeriod = Convert.ToInt32(m_dsTouchData.Tables("Touches").Rows(0).Item("PERIOD"))
                            'Setting the period property implemented by shirley 29/07/2009
                            Dim drTouchData() As DataRow = m_dsTouchData.Tables(0).Select("PERIOD > 0", "SEQUENCE_NUMBER DESC")
                            If drTouchData.Length > 0 Then
                                If m_objGameDetails.CurrentPeriod <> Convert.ToInt32(drTouchData(0).Item("PERIOD")) Then
                                    ''1. other side has higher Period than this local one
                                    ''2. Other side has lower than this local one
                                    If m_intRefresh Then
                                        '' START Period entered by other side
                                        ''STart the Clock as per new Period
                                        isClockStart = True

                                    End If
                                End If

                                m_objclsGameDetails.CurrentPeriod = Convert.ToInt32(drTouchData(0).Item("PERIOD"))
                            Else
                                m_objclsGameDetails.CurrentPeriod = 0
                                frmMain.UdcRunningClock1.URCSetTime(0, 0, True)
                                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                Exit Sub
                            End If

                        End If
                        'SANDEEP ADDED BELOW CONDITION TO DISPLAY PROPER TIME AND CLOCK DEFINITION IN MULTI REPORTER
                        Dim strTimeElapsed As String = "00:00"
                        Dim strNewTimeElapsed() As String
                        Dim intTouchTypeID As Integer
                        Dim strReporterRole As String
                        Dim intCurrentTimeElapsedInSecond As Integer
                        Dim intLastTimeElapsedInSecond As Integer
                        Dim intUID As Integer
                        Dim strRefreshed As String
                        Dim dr() As DataRow = m_dsTouchData.Tables(0).Select("", "SEQUENCE_NUMBER DESC")
                        strTimeElapsed = CalculateTimeElapseAfter(CInt(dr(0).Item("TIME_ELAPSED")), CInt(dr(0).Item("PERIOD")))
                        strNewTimeElapsed = strTimeElapsed.Split(CChar(":"))
                        intTouchTypeID = CInt(IIf(IsDBNull(dr(0).Item("TOUCH_TYPE_ID")) = True, 0, dr(0).Item("TOUCH_TYPE_ID")))

                        intCurrentTimeElapsedInSecond = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                        intLastTimeElapsedInSecond = CInt(dr(0).Item("TIME_ELAPSED"))
                        intUID = CInt(dr(0).Item("UNIQUE_ID"))
                        strRefreshed = dr(0).Item("REFRESHED").ToString


                        Select Case Source
                            Case 1  'INDIVIDUAL ENTRY
                                'CLOCK SHOULD KEEP ON RUNNING.
                            Case 2  'MULTI REPORTER
                                Dim dsTouchData As New DataSet()

                                dsTouchData = m_objclsTouches.GetTouchesRefresh(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
                                If dsTouchData IsNot Nothing Then
                                    If dsTouchData.Tables(0).Rows.Count > 0 Then
                                        strReporterRole = dsTouchData.Tables(0).Rows(0).Item("REPORTER_ROLE").ToString()
                                    Else
                                        strReporterRole = m_dsTouchData.Tables(0).Rows(0).Item("REPORTER_ROLE").ToString()
                                    End If
                                Else
                                    strReporterRole = m_dsTouchData.Tables(0).Rows(0).Item("REPORTER_ROLE").ToString()
                                End If
                                Select Case intTouchTypeID
                                    Case 14 'IN CASE OF START PERIOD CLOCK SHOULD START AND RUN.
                                        Select Case CInt(m_objclsGameDetails.ReporterRole.Substring(m_objclsGameDetails.ReporterRole.Length - 1))
                                            Case 1 'IN PRIMARY REPORTER SIDE ALWAYS SET THE HIGHER TIME IF COMING FROM ASSISTER
                                                'Dont change the clock time for T1
                                                'If intCurrentTimeElapsedInSecond < intLastTimeElapsedInSecond Then
                                                '    frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                                'End If
                                            Case Else   ' IN ASSISTER REPORTER SIDE ALWAYS SET THE TIME IRRESPECTIVE OF HIGHER OR LOWER CLOCK.
                                                frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                                
                                        End Select
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                            frmMain.UdcRunningClock1.URCToggle()
                                        End If

                                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                        frmMain.SetPeriod()
                                        DisableControls(True)
                                        
                                    Case 15 ''IN CASE OF END PERIOD CLOCK SHOULD STOP.
                                        Select Case CInt(m_objclsGameDetails.ReporterRole.Substring(m_objclsGameDetails.ReporterRole.Length - 1))
                                            Case 1 'IN PRIMARY REPORTER SIDE ALWAYS SET THE HIGHER TIME IF COMING FROM ASSISTER
                                                'Dont change the clock time for T1
                                                'If intCurrentTimeElapsedInSecond < intLastTimeElapsedInSecond Then
                                                '    frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                                'End If
                                            Case Else   ' IN ASSISTER REPORTER SIDE ALWAYS SET THE TIME IRRESPECTIVE OF HIGHER OR LOWER CLOCK.
                                                frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                                If frmMain.UdcRunningClock1.URCIsRunning Then
                                                    frmMain.UdcRunningClock1.URCToggle()
                                                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                                Else
                                                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                                End If
                                                If m_objGameDetails.CurrentPeriod = 4 Then
                                                    frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                                    frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                                    frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
                                                Else
                                                    frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                                    frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                                End If
                                                frmMain.SetPeriod()
                                                frmMain.SetClockControl(False)
                                                DisableControls(False)
                                        End Select
                                       
                                    Case Else   ''IN CASE OF OTHER EVENT CLOCK SHOULD START AND RUN.
                                        'Select Case m_objclsGameDetails.ReporterRole
                                        '    Case "A1", "B1", "C1", "D1" 'IN PRIMARY REPORTER SIDE ALWAYS SET THE HIGHER TIME IF COMING FROM ASSISTER
                                        '        If intCurrentTimeElapsedInSecond < intLastTimeElapsedInSecond Then
                                        '            frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        '        End If
                                        '    Case Else   ' IN ASSISTER REPORTER SIDE ALWAYS SET THE TIME IRRESPECTIVE OF HIGHER OR LOWER CLOCK.
                                        '        'If intUID > 0 And strReporterRole <> m_objclsGameDetails.ReporterRole Then
                                        '        '    frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        '        'End If
                                        'End Select
                                        'frmMain.SetPeriod()
                                        'If frmMain.UdcRunningClock1.URCIsRunning Then
                                        '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        'Else
                                        '    frmMain.UdcRunningClock1.URCToggle()
                                        '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        '    DisableControls(True)
                                        'End If
                                        'frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                        'frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)

                                End Select
                            Case 3  'RESTART OF EXISTING GAME
                                Select Case intTouchTypeID
                                    Case 14 'IN CASE OF START PERIOD CLOCK SHOULD START AND RUN.
                                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                            frmMain.UdcRunningClock1.URCToggle()
                                        End If
                                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                    Case 15 ''IN CASE OF END PERIOD CLOCK SHOULD STOP.
                                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.UdcRunningClock1.URCToggle()
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                        End If
                                        If m_objGameDetails.CurrentPeriod = 4 Then
                                            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                            frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
                                        Else
                                            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                        End If
                                        DisableControls(False)
                                    Case Else   ''IN CASE OF OTHER EVENT CLOCK SHOULD START AND RUN.
                                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                            frmMain.UdcRunningClock1.URCToggle()
                                        End If
                                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                        frmMain.SetPeriod()
                                End Select
                            Case 4  'DELETE
                                'frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                'If frmMain.UdcRunningClock1.URCIsRunning Then
                                '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                'Else
                                '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                'End If
                                Select Case intTouchTypeID
                                    Case 14 'IN CASE OF START PERIOD CLOCK SHOULD START AND RUN.
                                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                            frmMain.UdcRunningClock1.URCToggle()
                                        End If
                                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                        frmMain.SetPeriod()
                                    Case 15 ''IN CASE OF END PERIOD CLOCK SHOULD STOP.
                                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.UdcRunningClock1.URCToggle()
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                        End If
                                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                        frmMain.SetPeriod()
                                    Case Else   ''IN CASE OF OTHER EVENT CLOCK SHOULD START AND RUN.
                                        'frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                                        If frmMain.UdcRunningClock1.URCIsRunning Then
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                        Else
                                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                            frmMain.UdcRunningClock1.URCToggle()
                                        End If
                                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                        frmMain.SetPeriod()
                                End Select
                        End Select
                    Else
                        'lvwTouches.Items.Clear()
                        m_objclsGameDetails.CurrentPeriod = 0
                        frmMain.UdcRunningClock1.URCSetTime(0, 0, True)
                        frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                        DisableControls(False)
                    End If
                Else
                    'lvwTouches.Items.Clear()
                End If
            Else
                'lvwTouches.Items.Clear()
            End If
            m_intRefresh = False

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    'Private Function GetTouchDescriptions(ByVal strTouchDesc As String) As String
    '    Try
    '        Select Case strTouchDesc
    '            Case "Passes"
    '                Return "Pass"
    '            Case "Blocks"
    '                Return "Block"
    '            Case "Interceptions"
    '                Return "Interception"
    '            Case "Tackles"
    '                Return "Tackle"
    '            Case "Saves"
    '                Return "Save"
    '            Case "Catches"
    '                Return "Catch"
    '            Case "Others"
    '                Return "Other"
    '            Case Else
    '                Return "?"
    '        End Select
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Function CreateTouchDataObject(ByVal GameCode As Int32, ByVal FeedNumber As Int32, ByVal TimeElapsed As Int32, ByVal Period As Int32, ByVal PlayerID As Int32, ByVal TouchTypeID As Int32, ByVal ReporterRole As String, ByVal XFieldZone As Integer, ByVal YFieldZone As Integer, ByVal XFieldZoneDef As Integer, ByVal YFieldZoneDef As Integer, ByVal TouchTeamID As Integer) As DataSet
    Public Function CreateTouchDataObject(ByVal GameCode As Int32, ByVal FeedNumber As Int32, ByVal TimeElapsed As Int32, ByVal Period As Int32, ByVal PlayerID As Int32, ByVal RecipientID As Int32, ByVal TouchTypeID As Int32, ByVal ReporterRole As String, ByVal XFieldZone As Integer, ByVal YFieldZone As Integer, ByVal XFieldZoneDef As Integer, ByVal YFieldZoneDef As Integer, ByVal TouchTeamID As Integer) As DataSet
        Try
            Dim ds As New DataSet
            Dim drPBPDataPoint As DataRow
            Dim dtTimeDiff As DateTime

            drPBPDataPoint = m_objclsGameDetails.TouchesData.Tables("LIVE_SOC_PLAYER_TOUCH").NewRow()
            drPBPDataPoint(0) = GameCode
            drPBPDataPoint(1) = FeedNumber
            If m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 1 Then
                drPBPDataPoint(2) = 3
            Else
                drPBPDataPoint(2) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, 2, 1)
            End If
            If m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 1 Then
                drPBPDataPoint(3) = 3
            Else
                drPBPDataPoint(3) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, 2, 1)
            End If

            If m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0 Then
                Dim maxTimeElapsed As Integer
                maxTimeElapsed = m_objclsTouches.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

                If TimeElapsed < maxTimeElapsed Then
                    '    drPBPDataPoint(4) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, TimeElapsed - 1, TimeElapsed)
                    'Else
                    If TimeElapsed = 0 Then
                        drPBPDataPoint(4) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, TimeElapsed, TimeElapsed)
                    Else
                        drPBPDataPoint(4) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, TimeElapsed + 1, TimeElapsed)
                    End If
                Else
                    drPBPDataPoint(4) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, TimeElapsed + 1, TimeElapsed)
                End If
            Else
                drPBPDataPoint(4) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, TimeElapsed + 1, TimeElapsed)
            End If


            drPBPDataPoint(5) = Period
            drPBPDataPoint(6) = IIf(PlayerID = 0, DBNull.Value, PlayerID)
            drPBPDataPoint(7) = IIf(XFieldZone = -1, DBNull.Value, XFieldZone)
            drPBPDataPoint(8) = IIf(YFieldZone = -1, DBNull.Value, YFieldZone)
            drPBPDataPoint(9) = IIf(TouchTypeID = -1, DBNull.Value, TouchTypeID)
            drPBPDataPoint(10) = CChar("N")
            drPBPDataPoint(11) = 0
            drPBPDataPoint(12) = DBNull.Value
            drPBPDataPoint(13) = DBNull.Value
            drPBPDataPoint(14) = DBNull.Value
            drPBPDataPoint(15) = DBNull.Value
            drPBPDataPoint(16) = m_objclsGameDetails.ReporterRole
            drPBPDataPoint(17) = CChar("N")
            drPBPDataPoint(18) = m_objclsGameDetails.IsDemoGameSelected
            drPBPDataPoint(19) = CChar("N")
            drPBPDataPoint(20) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, 2, 1)
            dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
            drPBPDataPoint(21) = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
            drPBPDataPoint(22) = IIf(XFieldZoneDef = -1, DBNull.Value, XFieldZoneDef)
            drPBPDataPoint(23) = IIf(YFieldZoneDef = -1, DBNull.Value, YFieldZoneDef)
            drPBPDataPoint(24) = TouchTeamID
            'RM 7904 -  Recipient_Code
            drPBPDataPoint(25) = IIf(RecipientID = 0, DBNull.Value, RecipientID)
            drPBPDataPoint(26) = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
            m_objclsGameDetails.TouchesData.Tables("LIVE_SOC_PLAYER_TOUCH").Rows.Add(drPBPDataPoint)
            Return m_objclsGameDetails.TouchesData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function CreateTouchDataObject_Edit(ByVal GameCode As Int32, ByVal FeedNumber As Int32, ByVal TimeElapsed As Int32, ByVal PlayerID As Int32, ByVal TouchTypeID As Int32, ByVal XFieldZone As Integer, ByVal YFieldZone As Integer, ByVal SequenceNumber As Decimal, ByVal ReporterRole As String, ByVal Period As Integer, ByVal XFieldZoneDef As Integer, ByVal YFieldZoneDef As Integer, ByVal TouchTeamID As Integer) As DataSet
    Public Function CreateTouchDataObject_Edit(ByVal GameCode As Int32, ByVal FeedNumber As Int32, ByVal TimeElapsed As Int32, ByVal PlayerID As Int32, ByVal RecipientID As Int32, ByVal TouchTypeID As Int32, ByVal XFieldZone As Integer, ByVal YFieldZone As Integer, ByVal SequenceNumber As Decimal, ByVal ReporterRole As String, ByVal Period As Integer, ByVal XFieldZoneDef As Integer, ByVal YFieldZoneDef As Integer, ByVal TouchTeamID As Integer) As DataSet
        Try
            Dim ds As New DataSet
            Dim drPBPDataPoint As DataRow
            Dim dtTimeDiff As DateTime

            drPBPDataPoint = m_objclsGameDetails.TouchesData.Tables("LIVE_SOC_PLAYER_TOUCH").NewRow()
            drPBPDataPoint(0) = GameCode
            drPBPDataPoint(1) = FeedNumber
            drPBPDataPoint(2) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, 2, 1)
            drPBPDataPoint(3) = SequenceNumber
            'drPBPDataPoint(4) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, TimeElapsed + 1, TimeElapsed)
            If m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0 And (TouchTypeID <> 19 And TouchTypeID <> 21) Then 'TOSOCRS-310
                Dim drNxtTouch As DataSet

                drNxtTouch = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CDec(SequenceNumber), "NEXTNOTIME")
                If drNxtTouch.Tables(0).Rows.Count > 0 Then
                    If drNxtTouch.Tables(0).Rows(0).Item("TIME_ELAPSED") IsNot DBNull.Value Then
                        If TimeElapsed + 1 > CInt(drNxtTouch.Tables(0).Rows(0).Item("TIME_ELAPSED")) Then
                            drPBPDataPoint(4) = TimeElapsed
                        Else
                            drPBPDataPoint(4) = TimeElapsed + 1
                        End If
                    Else
                        drPBPDataPoint(4) = TimeElapsed + 1
                    End If
                Else
                    drPBPDataPoint(4) = TimeElapsed + 1
                End If
            Else
                drPBPDataPoint(4) = TimeElapsed
            End If

            drPBPDataPoint(5) = Period
            drPBPDataPoint(6) = IIf(PlayerID = 0, DBNull.Value, PlayerID)
            drPBPDataPoint(7) = IIf(XFieldZone = -1, DBNull.Value, XFieldZone) 'XFieldZone
            drPBPDataPoint(8) = IIf(YFieldZone = -1, DBNull.Value, YFieldZone) 'YFieldZone
            drPBPDataPoint(9) = IIf(TouchTypeID = -1, DBNull.Value, TouchTypeID)
            drPBPDataPoint(10) = CChar("N")
            drPBPDataPoint(11) = 0
            drPBPDataPoint(12) = DBNull.Value
            drPBPDataPoint(13) = DBNull.Value
            drPBPDataPoint(14) = DBNull.Value
            drPBPDataPoint(15) = DBNull.Value
            drPBPDataPoint(16) = ReporterRole
            drPBPDataPoint(17) = CChar("N")
            drPBPDataPoint(18) = m_objclsGameDetails.IsDemoGameSelected
            drPBPDataPoint(19) = CChar("N")
            drPBPDataPoint(20) = IIf(m_objclsGameDetails.TouchesData.Tables(0).Rows.Count > 0, 2, 1)
            dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
            drPBPDataPoint(21) = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
            m_objclsGameDetails.TouchesData.Tables("LIVE_SOC_PLAYER_TOUCH").Rows.Add(drPBPDataPoint)
            If (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                If m_intTouchTypeID = 1 Or m_intTouchTypeID = -99 Or m_intTouchTypeID = 8 Or m_intTouchTypeID = 12 Or m_intTouchTypeID = 17 Or m_intTouchTypeID = 23 Then 'TOSOCRS-41
                    drPBPDataPoint(22) = IIf(XFieldZoneDef = -1, DBNull.Value, XFieldZoneDef) 'XFieldZone
                    drPBPDataPoint(23) = IIf(YFieldZoneDef = -1, DBNull.Value, YFieldZoneDef) 'YFieldZone
                Else
                    drPBPDataPoint(22) = DBNull.Value 'XFieldZone
                    drPBPDataPoint(23) = DBNull.Value
                End If
                'Else
                '    drPBPDataPoint(22) = IIf(XFieldZoneDef = -1, DBNull.Value, XFieldZoneDef) 'XFieldZone
                '    drPBPDataPoint(23) = IIf(YFieldZoneDef = -1, DBNull.Value, YFieldZoneDef) 'YFieldZone
            End If
            drPBPDataPoint(24) = TouchTeamID
            'RM 7904 - Recipient_Code
            drPBPDataPoint(25) = IIf(RecipientID = 0, DBNull.Value, RecipientID)
            drPBPDataPoint(26) = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

            Return m_objclsGameDetails.TouchesData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveTouches()
        Try
            Dim intTimeElapsed As Integer
            If m_objclsGameDetails.IsDefaultGameClock Then
                If m_objclsGameDetails.IsEditMode Then
                    intTimeElapsed = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text)), m_CurrentPeriod)
                Else
                    intTimeElapsed = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text)), m_objclsGameDetails.CurrentPeriod)
                End If
            Else
                intTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text))
            End If

            Dim intPlayerID As Integer = 0
            If Not String.IsNullOrEmpty(m_strPlayerID) And Not m_intTouchTeamID = -1 Then
                intPlayerID = Get_Player_ID(Convert.ToInt32(m_strPlayerID), m_intTouchTeamID)
            End If

            'RM 7904 - Recipient_Code
            Dim intRecipientID As Integer = 0
            If Not String.IsNullOrEmpty(m_strRecipientID) And Not m_intRecTouchTeamID = -1 Then
                intRecipientID = Get_Recipient_ID(Convert.ToInt32(m_strRecipientID), m_intRecTouchTeamID)
            End If

            'Save prev events details 

            'RM 7904 - Recipient_Code
            m_touchType = m_intTouchTypeID
            m_teamID = m_intTouchTeamID
            m_recID = intRecipientID
            If m_strRecipientID <> "" Then
                m_recUnID = Convert.ToInt32(m_strRecipientID)
            Else
                m_recUnID = 0
            End If

            If m_decSequenceNo = "-1" Then
                If m_intTouchTypeID = -99 Or m_intTouchTypeID = -98 Then
                    If m_objclsGameDetails.ReporterRole.Substring(m_objclsGameDetails.ReporterRole.Length - 1) <> "2" Then
                        'Dim currTime As Integer = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)) + 1
                        Dim maxTimeElapsed As Integer
                        maxTimeElapsed = m_objclsTouches.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        Dim currTime As Integer
                        If m_objGameDetails.IsDefaultGameClock Then
                            currTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)), m_objclsGameDetails.CurrentPeriod) + 1
                        Else
                            currTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)) + 1
                        End If
                        If intTimeElapsed < maxTimeElapsed Then
                            CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 2, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                            'CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, 2, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone)
                            m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, CInt(IIf(m_intTouchTypeID = -99, 1, 11)), m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID))
                        Else
                            CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 2, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                            CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, CInt(IIf(m_intTouchTypeID = -99, 1, 11)), m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                            m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                        End If
                    Else
                        CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 2, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                        m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, CInt(IIf(m_intTouchTypeID = -99, 1, 11)), m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID))
                    End If

                    'CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, 2, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone)
                    'm_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, 1, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone))
                Else
                    If m_objclsGameDetails.ReporterRole.Substring(m_objclsGameDetails.ReporterRole.Length - 1) <> "2" Then
                        'Dim currTime As Integer = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)), m_objclsGameDetails.CurrentPeriod) + 1

                        Dim currTime As Integer
                        If m_objGameDetails.IsDefaultGameClock Then
                            currTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)), m_objclsGameDetails.CurrentPeriod) + 1
                        Else
                            currTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)) + 1
                        End If

                        'CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                        'm_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))

                        ''
                        If m_objGameDetails.SerialNo = 1 Then

                            If m_intTouchTypeID = 18 Then 'Aerial Win/Add Aerial loss for other team
                                If m_intTouchTeamID = m_objGameDetails.HomeTeamID Then
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                                    m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                                    'Air Loss for other team
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 19, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_objclsGameDetails.AwayTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_objclsGameDetails.AwayTeamID))

                                Else
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                                    m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                                    'Air loss for other team
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 19, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_objclsGameDetails.HomeTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_objclsGameDetails.HomeTeamID))


                                End If
                            ElseIf m_intTouchTypeID = 20 Then '50-50 Win/Add 50-50 loss for other team
                                If m_intTouchTeamID = m_objGameDetails.HomeTeamID Then
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                                    m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                                    '50-50 Loss for other team
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 21, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_objclsGameDetails.AwayTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_objclsGameDetails.AwayTeamID))

                                Else
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                                    m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                                    '50-50 loss for other team
                                    CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, 21, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_objclsGameDetails.HomeTeamID)
                                    m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_objclsGameDetails.HomeTeamID))


                                End If
                            Else
                                CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                                m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                            End If
                        Else
                            CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                            m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, currTime, m_objclsGameDetails.CurrentPeriod, 0, 0, 16, m_objclsGameDetails.ReporterRole, -1, -1, -1, -1, m_intTouchTeamID))
                        End If
                        ''
                    Else
                        m_objclsTouches.AddTouches(CreateTouchDataObject(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, m_objclsGameDetails.CurrentPeriod, intPlayerID, intRecipientID, m_intTouchTypeID, m_objclsGameDetails.ReporterRole, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID))
                    End If
                End If
            Else
                'shirley - july 2 2010 (Edited time should not be more than the End period Time)
                m_dsTouchData = m_objclsTouches.GetTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo, m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.AwayTeamID, 0)
                If m_dsTouchData IsNot Nothing Then
                    If m_dsTouchData.Tables.Count > 0 Then
                        If m_dsTouchData.Tables(0).Rows.Count > 0 Then
                            Dim drs() As DataRow = m_dsTouchData.Tables(0).Select("SEQUENCE_NUMBER = '" & CType(m_decSequenceNo, Decimal) & "'")
                            If drs.Length > 0 Then
                                Dim intperiod As Integer = CInt(drs(0).Item("PERIOD"))
                                drs = m_dsTouchData.Tables(0).Select("PERIOD = " & intperiod & " and TOUCH_TYPE_ID = 15")
                                If drs.Length > 0 Then
                                    Dim intEndPeriodTE As Integer = CInt(drs(0).Item("TIME_ELAPSED"))
                                    If intTimeElapsed > intEndPeriodTE Then
                                        MessageDialog.Show("The Time entered should be less than the END PERIOD time.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                        ClearTouchLabels()
                                        m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                                        m_decSequenceNo = "-1"
                                        DisableOtherButtonColor(btnClear.Name)
                                        DisableotherEventbuttonColor(btnClear.Name)
                                        'btnClear.Focus()
                                        Exit Try
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                'TOSOCRS-310 START
                Dim intReturnCnt As Integer = 0

                If m_intTouchTypeID = -99 Or m_intTouchTypeID = -98 Then
                    CreateTouchDataObject_Edit(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, intPlayerID, 0, 2, m_X_FieldZone, m_Y_FieldZone, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole, m_objclsGameDetails.CurrentPeriod, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                    intReturnCnt = m_objclsTouches.UpdateTouches(CreateTouchDataObject_Edit(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, intPlayerID, CInt(IIf(m_intTouchTypeID = -99, intRecipientID, 0)), CInt(IIf(m_intTouchTypeID = -99, 1, 11)), m_X_FieldZone, m_Y_FieldZone, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole, m_objclsGameDetails.CurrentPeriod, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID), CType(m_decSequenceNo, Decimal), m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)
                ElseIf ((m_intTouchTypeID = 18 And m_TDtouchTypeID <> 18) Or (m_intTouchTypeID = 20 And m_TDtouchTypeID <> 20)) Then
                    CreateTouchDataObject_Edit(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, intPlayerID, 0, m_intTouchTypeID, m_X_FieldZone, m_Y_FieldZone, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole, m_objclsGameDetails.CurrentPeriod, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID)
                    intReturnCnt = m_objclsTouches.UpdateTouches(CreateTouchDataObject_Edit(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, 0, 0, CInt(IIf(m_intTouchTypeID = 18, 19, 21)), m_X_FieldZone, m_Y_FieldZone, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole, m_objclsGameDetails.CurrentPeriod, m_X_FieldZone_Def, m_Y_FieldZone_Def, CInt(IIf(m_intTouchTeamID = m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.AwayTeamID, m_objGameDetails.HomeTeamID))), CType(m_decSequenceNo, Decimal), m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)
                Else
                    intReturnCnt = m_objclsTouches.UpdateTouches(CreateTouchDataObject_Edit(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTimeElapsed, intPlayerID, intRecipientID, m_intTouchTypeID, m_X_FieldZone, m_Y_FieldZone, CType(m_decSequenceNo, Decimal), m_objGameDetails.ReporterRole, m_objclsGameDetails.CurrentPeriod, m_X_FieldZone_Def, m_Y_FieldZone_Def, m_intTouchTeamID), CType(m_decSequenceNo, Decimal), m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)
                End If
                If intReturnCnt = 0 Then
                    MessageDialog.Show(strmessage29, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    ClearTouchLabels()
                    m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                    m_decSequenceNo = "-1"
                    DisableOtherButtonColor(btnClear.Name)
                    DisableotherEventbuttonColor(btnClear.Name)
                    Exit Try
                End If
                'TOSOCRS-310 END
            End If

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Ttype] " & m_intTouchTypeID, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[First Loc] " & m_X_FieldZone & "," & m_Y_FieldZone, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Second Loc] " & m_X_FieldZone_Def & "," & m_Y_FieldZone_Def, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Players] " & intPlayerID & "," & intRecipientID, 1, 0)

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Period] " & m_objGameDetails.CurrentPeriod, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Event entered at] " & txtTime.Text, 1, 0)
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 1)

            'TOSOCRS-37 Warning on unprocessed records
            Dim dsTDataForNDiscc As DataSet
            dsTDataForNDiscc = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)

            If dsTDataForNDiscc IsNot Nothing Then
                If dsTDataForNDiscc.Tables.Count > 0 Then
                    If m_objGameDetails.SerialNo = 2 Or m_objGameDetails.SerialNo = 3 Then
                        isRecProcTouchesAssister(dsTDataForNDiscc.Tables(0))
                    Else
                        isRecProcTouchesMain(dsTDataForNDiscc.Tables(0))
                    End If
                End If
            End If

            'IF CURRENT REPORTER IS AN ASSISTER THEN LOCK THE SYSTEM TILL THE CONFIRMATION FROM PRIMARY REPORTER
            'If m_objclsGameDetails.ReporterRole <> "A1" And m_objclsGameDetails.ReporterRole <> "B1" And m_objclsGameDetails.ReporterRole <> "C1" And m_objclsGameDetails.ReporterRole <> "D1" Then
            '    m_objGeneral.UpdateLastEntryStatus(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, True)
            '    m_objclsGameDetails.AssistersLastEntryStatus = False
            'End If
            ClearTouchLabels()
            m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
            m_decSequenceNo = "-1"
            DisableOtherButtonColor(btnClear.Name)
            DisableotherEventbuttonColor(btnClear.Name)
            'btnClear.Focus()
        Catch ex As Exception
            Throw ex
        Finally
            m_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
        End Try
    End Sub

    'TOSOCRS-37 Warning on unprocessed records
    Public Sub isRecProcTouchesMain(ByVal dtPBP As DataTable)
        Try
            If (m_objGameDetails.IsEditMode = False And m_objGameDetails.IsInsertMode = False) Then
                dtPBP.DefaultView.Sort = "UNIQUE_ID DESC"
                dtPBP = dtPBP.DefaultView.ToTable()
                Dim Dr() As DataRow = dtPBP.Select("PROCESSED='N' AND EDIT_UID=0")
                If (Dr.Length > 0) Then
                    If CStr(Dr(Dr.Length - 1).Item("REPORTER_ROLE")) = "A1" Or CStr(Dr(Dr.Length - 1).Item("REPORTER_ROLE")) = "B1" Or CStr(Dr(Dr.Length - 1).Item("REPORTER_ROLE")) = "C1" Or CStr(Dr(Dr.Length - 1).Item("REPORTER_ROLE")) = "D1" Then
                        Dim dtTimeDiff As DateTime
                        Dim span As TimeSpan
                        Dim endTime As DateTime = DateTime.Parse(Dr(Dr.Length - 1).Item("MODIFY_TIME").ToString)
                        dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                        Dim startTime As DateTime = DateTime.Parse(DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat))
                        span = startTime.Subtract(endTime)
                        If (span.Days > 0 Or span.Hours > 0 Or span.Minutes >= 1) Then
                            If (m_objGameDetails.donotshow = "Y") Then
                                NetworkDisconnect.ShowDialog()
                            End If
                        Else
                            m_objGameDetails.donotshow = "Y"
                        End If
                    End If
                End If
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'TOSOCRS-37 Warning on unprocessed records
    Public Sub isRecProcTouchesAssister(ByVal dtPBP As DataTable)
        Try
            ' If (m_objGameDetails.IsEditMode = False And m_objGameDetails.IsInsertMode = False) Then
            dtPBP.DefaultView.Sort = "UNIQUE_ID DESC"
            dtPBP = dtPBP.DefaultView.ToTable()
            Dim Dr() As DataRow = dtPBP.Select("UNIQUE_ID < 0")
            If (Dr.Length > 0) Then
                Dim dtTimeDiff As DateTime
                Dim span As TimeSpan
                Dim endTime As DateTime = DateTime.Parse(Dr(Dr.Length - 1).Item("MODIFY_TIME").ToString)
                dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                Dim startTime As DateTime = DateTime.Parse(DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat))
                span = startTime.Subtract(endTime)
                If (span.Days > 0 Or span.Hours > 0 Or span.Minutes >= 1) Then
                    If (m_objGameDetails.donotshow = "Y") Then
                        NetworkDisconnect.ShowDialog()
                    End If
                Else
                    m_objGameDetails.donotshow = "Y"
                End If
            End If
            'End If


        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    ''' <summary>
    ''' Check for valid playerID while saving
    ''' </summary>
    ''' <param name="intUniformNo"></param>
    ''' <param name="intTeamID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Get_Player_ID(ByVal intUniformNo As Integer, ByVal intTeamID As Integer) As Integer
        Try

            If intTeamID = m_objclsGameDetails.HomeTeamID Then
                If m_hthome.Contains(CStr(intUniformNo)) Then
                    Return CInt(m_hthome(CStr(intUniformNo)))
                Else
                    Return CInt(m_strPlayerID)
                End If
            Else
                If m_htAway.Contains(CStr(intUniformNo)) Then
                    Return CInt(m_htAway(CStr(intUniformNo)))
                Else
                    Return CInt(m_strPlayerID)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    'RM 7904 - Recipient_Code

    Private Function Get_Recipient_ID(ByVal intUniformNo As Integer, ByVal intTeamID As Integer) As Integer
        Try

            If intTeamID = m_objclsGameDetails.HomeTeamID Then
                If m_hthome.Contains(CStr(intUniformNo)) Then
                    Return CInt(m_hthome(CStr(intUniformNo)))
                Else
                    If m_hthome.Contains("0" & CStr(intUniformNo)) Then
                        Return CInt(m_hthome("0" & CStr(intUniformNo)))
                    Else
                        Return CInt(m_strRecipientID)
                    End If
                End If
            Else
                If m_htAway.Contains(CStr(intUniformNo)) Then
                    Return CInt(m_htAway(CStr(intUniformNo)))
                Else
                    If m_htAway.Contains("0" & CStr(intUniformNo)) Then
                        Return CInt(m_htAway("0" & CStr(intUniformNo)))
                    Else
                        Return CInt(m_strRecipientID)
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Private Function Get_Uniform_ID(ByVal intPlayerID As Integer, ByVal intTeamID As Integer) As String
        Try

            If intTeamID = m_objclsGameDetails.HomeTeamID Then
                If m_hthomeUniform.Contains(CInt(intPlayerID)) Then
                    Return CStr(m_hthomeUniform(CInt(intPlayerID)))
                Else
                    Return CStr("")
                End If
            Else
                If m_htAwayUniform.Contains(CInt(intPlayerID)) Then
                    Return CStr(m_htAwayUniform(CInt(intPlayerID)))
                Else
                    Return CStr("")
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' To populate controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindControls()
        Try
            BindTeamRosters()
            SortTeamPlayers()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To populate team rosters
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindTeamRosters()
        Try

            'Dim dsLineUp As DataSet = m_objclsTeamSetup.RosterInfo()
            dsLineUp = m_objclsTeamSetup.RosterInfo()
            Dim blnChkTeam As Boolean = False


            If dsLineUp IsNot Nothing Then
                If dsLineUp.Tables.Count > 0 Then
                    If dsLineUp.Tables("Home").Rows.Count > 0 And dsLineUp.Tables("Away").Rows.Count > 0 Then
                        blnChkTeam = True
                        'ButtonsHomeTeamTagged(dsLineUp.Tables("Home"))
                        'ButtonsVisitTeamTagged(dsLineUp.Tables("Away"))

                        m_blnHomeLineUp = True
                        m_blnVisitLineUp = True

                        ButtonsHomeTeamTagged(dsLineUp.Tables("Home"), True)
                        ButtonsVisitTeamTagged(dsLineUp.Tables("Away"), True)

                        pnlHome.AutoScroll = False
                        pnlVisit.AutoScroll = False
                        pnlHome.Height = m_intPlayerPanelHeightShort
                        pnlVisit.Height = m_intPlayerPanelHeightShort
                        If bDisplayPlayers = True Then
                            pnlHomeBench.Visible = True
                            pnlVisitBench.Visible = True
                        End If
                    End If
                Else
                    pnlHome.AutoScroll = True
                    pnlVisit.AutoScroll = True
                    pnlHome.Height = m_intPlayerPanelHeightTall
                    pnlVisit.Height = m_intPlayerPanelHeightTall
                    pnlHomeBench.Visible = False
                    pnlVisitBench.Visible = False
                End If
            Else
                pnlHome.AutoScroll = True
                pnlVisit.AutoScroll = True
                pnlHome.Height = m_intPlayerPanelHeightTall
                pnlVisit.Height = m_intPlayerPanelHeightTall
                pnlHomeBench.Visible = False
                pnlVisitBench.Visible = False
            End If


            If blnChkTeam = False Then
                'Home Team Roster
                'Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.HomeTeamID)
                m_dsHomeTeam = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
                If m_dsHomeTeam IsNot Nothing Then
                    If m_dsHomeTeam.Tables.Count > 0 Then
                        m_dsHomeTeam.Tables(0).TableName = "Home"
                        If m_dsHomeTeam.Tables("Home").Rows.Count > 0 Then
                            ButtonsHomeTeamTagged(m_dsHomeTeam.Tables("Home"), False)
                            m_blnHomeLineUp = False
                            m_hthome.Clear()
                            m_hthomeUniform.Clear()
                            For Each drHomeTeam As DataRow In m_dsHomeTeam.Tables("Home").Rows
                                If Not Convert.ToInt32(drHomeTeam("DISPLAY_UNIFORM_NUMBER")) = 0 And Not m_hthome.Contains(drHomeTeam("DISPLAY_UNIFORM_NUMBER")) Then
                                    m_hthome.Add(drHomeTeam("DISPLAY_UNIFORM_NUMBER"), drHomeTeam("PLAYER_ID"))
                                    m_hthomeUniform.Add(drHomeTeam("PLAYER_ID"), drHomeTeam("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next

                        End If
                    End If
                End If


                'Away TeamRoster
                'Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.AwayTeamID)
                m_dsAwayTeam = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
                If m_dsAwayTeam IsNot Nothing Then
                    If m_dsAwayTeam.Tables.Count > 0 Then
                        m_dsAwayTeam.Tables(0).TableName = "Away"
                        If m_dsAwayTeam.Tables("Away").Rows.Count > 0 Then
                            ButtonsVisitTeamTagged(m_dsAwayTeam.Tables("Away"), False)
                            m_blnVisitLineUp = False
                            m_htAway.Clear()
                            m_htAwayUniform.Clear()
                            For Each drAwayTeam As DataRow In m_dsAwayTeam.Tables("Away").Rows
                                If Not Convert.ToInt32(drAwayTeam("DISPLAY_UNIFORM_NUMBER")) = 0 And Not m_htAway.Contains(drAwayTeam("DISPLAY_UNIFORM_NUMBER")) Then
                                    m_htAway.Add(drAwayTeam("DISPLAY_UNIFORM_NUMBER"), drAwayTeam("PLAYER_ID"))
                                    m_htAwayUniform.Add(drAwayTeam("PLAYER_ID"), drAwayTeam("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To assign tags to Home Team Player Buttons
    ''' </summary>
    ''' <param name="dtHomeTeamRoster"></param>
    ''' <remarks></remarks>
    Private Sub ButtonsHomeTeamTagged(ByVal dtHomeTeamRoster As DataTable, ByVal blnChkTeam As Boolean)
        Try
            Dim intBtnCntr As Integer = 1
            Dim btn As Button
            Dim hthome As New Hashtable
            Dim hthomesub As New Hashtable
            Dim htaway As New Hashtable
            Dim htvisitingsub As New Hashtable
            Dim strPlayerLastName As String
            Dim dtHomeLineUpRosters As New DataTable
            Dim strMoniker As String
            Dim strPos As String


            'Clear buttons text of home team
            ClearHomeButtonText()

            If blnChkTeam = False Then

                hthome.Add(1, Me.btnHomePlayer1)
                hthome.Add(2, Me.btnHomePlayer2)
                hthome.Add(3, Me.btnHomePlayer3)
                hthome.Add(4, Me.btnHomePlayer4)
                hthome.Add(5, Me.btnHomePlayer5)
                hthome.Add(6, Me.btnHomePlayer6)
                hthome.Add(7, Me.btnHomePlayer7)
                hthome.Add(8, Me.btnHomePlayer8)
                hthome.Add(9, Me.btnHomePlayer9)
                hthome.Add(10, Me.btnHomePlayer10)
                hthome.Add(11, Me.btnHomePlayer11)
                hthome.Add(12, Me.btnHomePlayer12)
                hthome.Add(13, Me.btnHomePlayer13)
                hthome.Add(14, Me.btnHomePlayer14)
                hthome.Add(15, Me.btnHomePlayer15)
                hthome.Add(16, Me.btnHomePlayer16)
                hthome.Add(17, Me.btnHomePlayer17)
                hthome.Add(18, Me.btnHomePlayer18)
                hthome.Add(19, Me.btnHomePlayer19)
                hthome.Add(20, Me.btnHomePlayer20)
                hthome.Add(21, Me.btnHomePlayer21)
                hthome.Add(22, Me.btnHomePlayer22)
                hthome.Add(23, Me.btnHomePlayer23)
                hthome.Add(24, Me.btnHomePlayer24)
                hthome.Add(25, Me.btnHomePlayer25)
                hthome.Add(26, Me.btnHomePlayer26)
                hthome.Add(27, Me.btnHomePlayer27)
                hthome.Add(28, Me.btnHomePlayer28)
                hthome.Add(29, Me.btnHomePlayer29)
                hthome.Add(30, Me.btnHomePlayer30)
                hthome.Add(31, Me.btnHomePlayer31)
                hthome.Add(32, Me.btnHomePlayer32)
                hthome.Add(33, Me.btnHomePlayer33)
                hthome.Add(34, Me.btnHomePlayer34)
                hthome.Add(35, Me.btnHomePlayer35)
                hthome.Add(36, Me.btnHomePlayer36)
                hthome.Add(37, Me.btnHomePlayer37)
                hthome.Add(38, Me.btnHomePlayer38)
                hthome.Add(39, Me.btnHomePlayer39)
                hthome.Add(40, Me.btnHomePlayer40)
                hthome.Add(41, Me.btnHomePlayer41)

                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    For intBtnCntr = 1 To dtHomeTeamRoster.Rows.Count

                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            If dtHomeTeamRoster.Rows.Count >= intBtnCntr Then

                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    strMoniker = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString.Substring(0, 1)
                                Else
                                    strMoniker = ""
                                End If

                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    strPos = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                Else
                                    strPos = ""
                                End If

                                SetPlayerButton(btn, dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString, dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString, strMoniker, strPos)
                                btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            End If
                            btn.Visible = True
                        End If

                    Next

                    If dtHomeTeamRoster.Rows.Count > 20 Then
                        pnlHome.AutoScroll = True
                    Else
                        pnlHome.AutoScroll = False
                    End If
                    m_dtHome = dtHomeTeamRoster
                Else
                    Dim dtrowHomeLineUps As DataRow()
                    If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0 ", "STARTING_POSITION ASC")
                    Else
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
                    For Each drHomeLineUps As DataRow In dtrowHomeLineUps
                        dtHomeLineUpRosters.ImportRow(drHomeLineUps)
                    Next
                    For intBtnCntr = 1 To 11
                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then

                            If dtHomeLineUpRosters.Rows.Count >= intBtnCntr Then

                                Dim strFullName As String = String.Empty
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If

                                'POSITION ABBREV
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & "  " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    Else
                                        strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    End If
                                End If


                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If

                                'Arindam - tag_change
                                'If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                '    btn.Tag = String.Empty
                                'Else
                                '    btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                'End If
                                btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                            End If
                        End If
                    Next

                End If

            Else ' IF LINUEUPS AVILABLE 

                hthome.Add(1, Me.btnHomePlayer1)
                hthome.Add(2, Me.btnHomePlayer2)
                hthome.Add(3, Me.btnHomePlayer3)
                hthome.Add(4, Me.btnHomePlayer4)
                hthome.Add(5, Me.btnHomePlayer5)
                hthome.Add(6, Me.btnHomePlayer6)
                hthome.Add(7, Me.btnHomePlayer7)
                hthome.Add(8, Me.btnHomePlayer8)
                hthome.Add(9, Me.btnHomePlayer9)
                hthome.Add(10, Me.btnHomePlayer10)
                hthome.Add(11, Me.btnHomePlayer11)


                If dtHomeTeamRoster Is Nothing Then
                    Exit Sub
                End If

                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    For intBtnCntr = 1 To 11
                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then

                            If dtHomeTeamRoster.Rows.Count >= intBtnCntr Then

                                Dim strFullName As String = String.Empty
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If
                                'POSITION ABBREV
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & "  " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    Else
                                        strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If

                                'Arindam - tag_change
                                'If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                '    btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID")
                                'Else
                                '    btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                'End If
                                btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                            End If
                        End If
                    Next
                Else
                    Dim dtrowHomeLineUps As DataRow()
                    If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0 ", "STARTING_POSITION ASC")
                    Else
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
                    For Each drHomeLineUps As DataRow In dtrowHomeLineUps
                        dtHomeLineUpRosters.ImportRow(drHomeLineUps)
                    Next
                    For intBtnCntr = 1 To 11
                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then


                            If dtHomeLineUpRosters.Rows.Count >= intBtnCntr Then

                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    strMoniker = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString.Substring(0, 1)
                                Else
                                    strMoniker = ""
                                End If

                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    strPos = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                Else
                                    strPos = ""
                                End If

                                SetPlayerButton(btn, dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString, dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString, strMoniker, strPos)

                                btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                            End If
                        End If

                    Next
                End If
                hthomesub.Add(1, Me.btnHomeSub1)
                hthomesub.Add(2, Me.btnHomeSub2)
                hthomesub.Add(3, Me.btnHomeSub3)
                hthomesub.Add(4, Me.btnHomeSub4)
                hthomesub.Add(5, Me.btnHomeSub5)
                hthomesub.Add(6, Me.btnHomeSub6)
                hthomesub.Add(7, Me.btnHomeSub7)
                hthomesub.Add(8, Me.btnHomeSub8)
                hthomesub.Add(9, Me.btnHomeSub9)
                hthomesub.Add(10, Me.btnHomeSub10)
                hthomesub.Add(11, Me.btnHomeSub11)
                hthomesub.Add(12, Me.btnHomeSub12)
                hthomesub.Add(13, Me.btnHomeSub13)
                hthomesub.Add(14, Me.btnHomeSub14)
                hthomesub.Add(15, Me.btnHomeSub15)
                hthomesub.Add(16, Me.btnHomeSub16)
                hthomesub.Add(17, Me.btnHomeSub17)
                hthomesub.Add(18, Me.btnHomeSub18)
                hthomesub.Add(19, Me.btnHomeSub19)
                hthomesub.Add(20, Me.btnHomeSub20)
                hthomesub.Add(21, Me.btnHomeSub21)
                hthomesub.Add(22, Me.btnHomeSub22)
                hthomesub.Add(23, Me.btnHomeSub23)
                hthomesub.Add(24, Me.btnHomeSub24)
                hthomesub.Add(25, Me.btnHomeSub25)
                hthomesub.Add(26, Me.btnHomeSub26)
                hthomesub.Add(27, Me.btnHomeSub27)
                hthomesub.Add(28, Me.btnHomeSub28)
                hthomesub.Add(29, Me.btnHomeSub29)
                hthomesub.Add(30, Me.btnHomeSub30)

                If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    If dtHomeTeamRoster.Rows.Count > 11 Then
                        For intBtnCntr = 12 To dtHomeTeamRoster.Rows.Count
                            btn = CType(hthomesub.Item(intBtnCntr - 11), Button)
                            If btn IsNot Nothing Then

                                strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                If strPlayerLastName.Length > 6 Then
                                    strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                                End If
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                    'btn.Text = strPlayerLastName
                                    'Arindam - tag_change
                                    btn.Tag = String.Empty
                                Else
                                    strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                    'btn.Text = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                    'Arindam - tag_change
                                    'btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                    btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                End If
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    btn.Text = strPlayerLastName + "  " + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                End If
                                btn.Visible = True

                            End If
                        Next
                    End If
                Else
                    If m_objGameDetails.SerialNo = 2 Then
                        pnlHomeBench.Visible = True
                        pnlHomeBench.Enabled = True
                    ElseIf m_objGameDetails.SerialNo = 3 Then
                        pnlVisitBench.Visible = True
                        pnlVisitBench.Enabled = True
                    End If

                    'Dim dtrowHomeLineUps As DataRow() = dtHomeTeamRoster.Select("STARTING_POSITION > 11", "" & strSortOrder & " ASC")
                    Dim dtrowHomeLineUps As DataRow()
                    If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    If dtrowHomeLineUps.Length > 6 Then
                        pnlHomeBench.AutoScroll = True
                    Else
                        pnlHomeBench.AutoScroll = False
                    End If
                    dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
                    For Each drHomeLineUps As DataRow In dtrowHomeLineUps
                        dtHomeLineUpRosters.ImportRow(drHomeLineUps)
                    Next
                    For intBtnCntr = 1 To dtHomeLineUpRosters.Rows.Count
                        btn = CType(hthomesub.Item(intBtnCntr), Button)
                        strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                        If strPlayerLastName.Length > 6 Then
                            strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                        End If
                        If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                            'btn.Text = strPlayerLastName
                            'Arindam - tag_change
                            btn.Tag = String.Empty
                        Else
                            strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                            'btn.Text = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                            'Arindam - tag_change
                            'btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                            btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                        End If
                        If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                            btn.Text = strPlayerLastName + "  " + dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                        End If
                        btn.Visible = True
                    Next
                End If
                m_dtHome = dtHomeTeamRoster
                m_dtHome.AcceptChanges()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To assign tags to Away Team Player Buttons
    ''' </summary>
    ''' <param name="dtAwayTeamRoster"></param>
    ''' <remarks></remarks>
    Private Sub ButtonsVisitTeamTagged(ByVal dtAwayTeamRoster As DataTable, ByVal blnchkTeam As Boolean)
        Try
            Dim intBtnCntr As Integer = 1
            Dim btn As Button
            Dim htVisit As New Hashtable
            Dim htVisitsub As New Hashtable
            Dim htaway As New Hashtable
            Dim htvisitingsub As New Hashtable
            Dim strPlayerLastName As String
            Dim strPos As String
            Dim strMoniker As String

            'Clear buttons text of Visit team
            ClearVisitButtonText()
            Dim dtVisitLineUpRosters As New DataTable

            If blnchkTeam = False Then

                htVisit.Add(1, Me.btnVisitPlayer1)
                htVisit.Add(2, Me.btnVisitPlayer2)
                htVisit.Add(3, Me.btnVisitPlayer3)
                htVisit.Add(4, Me.btnVisitPlayer4)
                htVisit.Add(5, Me.btnVisitPlayer5)
                htVisit.Add(6, Me.btnVisitPlayer6)
                htVisit.Add(7, Me.btnVisitPlayer7)
                htVisit.Add(8, Me.btnVisitPlayer8)
                htVisit.Add(9, Me.btnVisitPlayer9)
                htVisit.Add(10, Me.btnVisitPlayer10)
                htVisit.Add(11, Me.btnVisitPlayer11)
                htVisit.Add(12, Me.btnVisitPlayer12)
                htVisit.Add(13, Me.btnVisitPlayer13)
                htVisit.Add(14, Me.btnVisitPlayer14)
                htVisit.Add(15, Me.btnVisitPlayer15)
                htVisit.Add(16, Me.btnVisitPlayer16)
                htVisit.Add(17, Me.btnVisitPlayer17)
                htVisit.Add(18, Me.btnVisitPlayer18)
                htVisit.Add(19, Me.btnVisitPlayer19)
                htVisit.Add(20, Me.btnVisitPlayer20)
                htVisit.Add(21, Me.btnVisitPlayer21)
                htVisit.Add(22, Me.btnVisitPlayer22)
                htVisit.Add(23, Me.btnVisitPlayer23)
                htVisit.Add(24, Me.btnVisitPlayer24)
                htVisit.Add(25, Me.btnVisitPlayer25)
                htVisit.Add(26, Me.btnVisitPlayer26)
                htVisit.Add(27, Me.btnVisitPlayer27)
                htVisit.Add(28, Me.btnVisitPlayer28)
                htVisit.Add(29, Me.btnVisitPlayer29)
                htVisit.Add(30, Me.btnVisitPlayer30)
                htVisit.Add(31, Me.btnVisitPlayer31)
                htVisit.Add(32, Me.btnVisitPlayer32)
                htVisit.Add(33, Me.btnVisitPlayer33)
                htVisit.Add(34, Me.btnVisitPlayer34)
                htVisit.Add(35, Me.btnVisitPlayer35)
                htVisit.Add(36, Me.btnVisitPlayer36)
                htVisit.Add(37, Me.btnVisitPlayer37)
                htVisit.Add(38, Me.btnVisitPlayer38)
                htVisit.Add(39, Me.btnVisitPlayer39)
                htVisit.Add(40, Me.btnVisitPlayer40)
                htVisit.Add(41, Me.btnVisitPlayer41)

                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    For intBtnCntr = 1 To dtAwayTeamRoster.Rows.Count
                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            If dtAwayTeamRoster.Rows.Count >= intBtnCntr Then

                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    strMoniker = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString.Substring(0, 1)
                                Else
                                    strMoniker = ""
                                End If

                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    strPos = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                Else
                                    strPos = ""
                                End If

                                SetPlayerButton(btn, dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString, dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString, strMoniker, strPos)

                                btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            End If
                            btn.Visible = True
                        End If
                    Next

                    If dtAwayTeamRoster.Rows.Count > 20 Then
                        pnlVisit.AutoScroll = True
                    Else
                        pnlVisit.AutoScroll = False
                    End If
                    m_dtAway = dtAwayTeamRoster
                Else
                    Dim dtrowVisitLineUps As DataRow()
                    If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    'Dim dtrowVisitLineUps As DataRow() = dtAwayTeamRoster.Select("STARTING_POSITION < 12", "" & strSortOrder & " ASC")
                    dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
                    For Each drVisitLineUps As DataRow In dtrowVisitLineUps
                        dtVisitLineUpRosters.ImportRow(drVisitLineUps)
                    Next
                    For intBtnCntr = 1 To 11
                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then

                            If dtVisitLineUpRosters.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & "  " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    Else
                                        strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                'Arindam - tag_change
                                'If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                '    btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'Else
                                '    btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                'End If

                                btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            End If
                        End If
                    Next
                End If
            Else
                htVisit.Add(1, Me.btnVisitPlayer1)
                htVisit.Add(2, Me.btnVisitPlayer2)
                htVisit.Add(3, Me.btnVisitPlayer3)
                htVisit.Add(4, Me.btnVisitPlayer4)
                htVisit.Add(5, Me.btnVisitPlayer5)
                htVisit.Add(6, Me.btnVisitPlayer6)
                htVisit.Add(7, Me.btnVisitPlayer7)
                htVisit.Add(8, Me.btnVisitPlayer8)
                htVisit.Add(9, Me.btnVisitPlayer9)
                htVisit.Add(10, Me.btnVisitPlayer10)
                htVisit.Add(11, Me.btnVisitPlayer11)

                If dtAwayTeamRoster Is Nothing Then
                    Exit Sub
                End If
                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    For intBtnCntr = 1 To 11
                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then


                            If dtAwayTeamRoster.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If
                                'POSITION ABBREV
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & "  " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    Else
                                        strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                'Arindam - tag_change
                                'If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                '    btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'Else
                                '    btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                'End If

                                btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            End If
                        End If
                    Next
                Else
                    Dim dtrowVisitLineUps As DataRow()
                    If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    'Dim dtrowVisitLineUps As DataRow() = dtAwayTeamRoster.Select("STARTING_POSITION < 12", "" & strSortOrder & " ASC")
                    dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
                    For Each drVisitLineUps As DataRow In dtrowVisitLineUps
                        dtVisitLineUpRosters.ImportRow(drVisitLineUps)
                    Next
                    For intBtnCntr = 1 To 11

                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            If dtVisitLineUpRosters.Rows.Count >= intBtnCntr Then

                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    strMoniker = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString.Substring(0, 1)
                                Else
                                    strMoniker = ""
                                End If

                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV") IsNot DBNull.Value Then
                                    strPos = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                Else
                                    strPos = ""
                                End If

                                SetPlayerButton(btn, dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString, dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString, strMoniker, strPos)


                                btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            End If
                        End If
                    Next
                End If
                htVisitsub.Add(1, Me.btnVisitSub1)
                htVisitsub.Add(2, Me.btnVisitSub2)
                htVisitsub.Add(3, Me.btnVisitSub3)
                htVisitsub.Add(4, Me.btnVisitSub4)
                htVisitsub.Add(5, Me.btnVisitSub5)
                htVisitsub.Add(6, Me.btnVisitSub6)
                htVisitsub.Add(7, Me.btnVisitSub7)
                htVisitsub.Add(8, Me.btnVisitSub8)
                htVisitsub.Add(9, Me.btnVisitSub9)
                htVisitsub.Add(10, Me.btnVisitSub10)
                htVisitsub.Add(11, Me.btnVisitSub11)
                htVisitsub.Add(12, Me.btnVisitSub12)
                htVisitsub.Add(13, Me.btnVisitSub13)
                htVisitsub.Add(14, Me.btnVisitSub14)
                htVisitsub.Add(15, Me.btnVisitSub15)
                htVisitsub.Add(16, Me.btnVisitSub16)
                htVisitsub.Add(17, Me.btnVisitSub17)
                htVisitsub.Add(18, Me.btnVisitSub18)
                htVisitsub.Add(19, Me.btnVisitSub19)
                htVisitsub.Add(20, Me.btnVisitSub20)
                htVisitsub.Add(21, Me.btnVisitSub21)
                htVisitsub.Add(22, Me.btnVisitSub22)
                htVisitsub.Add(23, Me.btnVisitSub23)
                htVisitsub.Add(24, Me.btnVisitSub24)
                htVisitsub.Add(25, Me.btnVisitSub25)
                htVisitsub.Add(26, Me.btnVisitSub26)
                htVisitsub.Add(27, Me.btnVisitSub27)
                htVisitsub.Add(28, Me.btnVisitSub28)
                htVisitsub.Add(29, Me.btnVisitSub29)
                htVisitsub.Add(30, Me.btnVisitSub30)

                If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    If dtAwayTeamRoster.Rows.Count > 11 Then
                        For intBtnCntr = 12 To dtAwayTeamRoster.Rows.Count
                            btn = CType(htVisitsub.Item(intBtnCntr - 11), Button)
                            If btn IsNot Nothing Then
                                strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                If strPlayerLastName.Length > 6 Then
                                    strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                    btn.Text = strPlayerLastName
                                    'Arindam - tag_change
                                    btn.Tag = String.Empty
                                Else
                                    strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                    'btn.Text = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                    'Arindam - tag_change
                                    'btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                    btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString() IsNot DBNull.Value Then
                                    btn.Text = strPlayerLastName + " " + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                                End If
                                btn.Visible = True
                            End If
                        Next
                    End If
                Else
                    If m_objGameDetails.SerialNo = 2 Then
                        pnlHomeBench.Visible = True
                        pnlHomeBench.Enabled = True
                    ElseIf m_objGameDetails.SerialNo = 3 Then
                        pnlVisitBench.Visible = True
                        pnlVisitBench.Enabled = True
                    End If
                    ' Dim dtrowVisitLineUps As DataRow() = dtAwayTeamRoster.Select("STARTING_POSITION > 11", "" & strSortOrder & " ASC")
                    Dim dtrowVisitLineUps As DataRow()
                    If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    If dtrowVisitLineUps.Length > 6 Then
                        pnlVisitBench.AutoScroll = True
                    Else
                        pnlVisitBench.AutoScroll = False
                    End If

                    dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
                    For Each drVisitLineUps As DataRow In dtrowVisitLineUps
                        dtVisitLineUpRosters.ImportRow(drVisitLineUps)
                    Next
                    For intBtnCntr = 1 To dtVisitLineUpRosters.Rows.Count
                        btn = CType(htVisitsub.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            If strPlayerLastName.Length > 6 Then
                                strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                            End If
                            If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                btn.Text = strPlayerLastName
                                'Arindam - tag_change
                                btn.Tag = String.Empty
                            Else
                                strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                'btn.Text = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                'Arindam - tag_change
                                'btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            End If
                            If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString() IsNot DBNull.Value Then
                                btn.Text = strPlayerLastName + "  " + dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("POSITION_ABBREV").ToString()
                            End If
                            btn.Visible = True
                        End If
                    Next
                End If
                m_dtAway = dtAwayTeamRoster
                m_dtAway.AcceptChanges()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To clear VisitTeam butons
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearVisitButtonText()


        For Each ctrl As Control In Me.Controls("pnlVisitBench").Controls
            If ctrl.Name <> "btnVisitTeam" Then
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    CType(ctrl, Button).Text = ""
                    CType(ctrl, Button).Visible = False
                End If
            End If
        Next

       
        btnVisitPlayer1.Text = ""
        btnVisitPlayer2.Text = ""
        btnVisitPlayer3.Text = ""
        btnVisitPlayer4.Text = ""
        btnVisitPlayer5.Text = ""
        btnVisitPlayer6.Text = ""
        btnVisitPlayer7.Text = ""
        btnVisitPlayer8.Text = ""
        btnVisitPlayer9.Text = ""
        btnVisitPlayer10.Text = ""
        btnVisitPlayer11.Text = ""

        btnVisitSub1.Visible = True
        btnVisitSub2.Visible = True
        btnVisitSub3.Visible = True
        btnVisitSub4.Visible = True
        btnVisitSub5.Visible = True
        btnVisitSub6.Visible = True
        btnVisitSub7.Visible = True
        btnVisitSub8.Visible = True
        btnVisitSub9.Visible = True
    End Sub

    ''' <summary>
    ''' To clear homeTeam buttons
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearHomeButtonText()
        For Each ctrl As Control In Me.Controls("pnlHomeBench").Controls
            If ctrl.Name <> "btnHomeTeam" Then
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    CType(ctrl, Button).Text = ""
                    CType(ctrl, Button).Visible = False
                End If
            End If
        Next
       
        btnHomePlayer1.Text = ""
        btnHomePlayer2.Text = ""
        btnHomePlayer3.Text = ""
        btnHomePlayer4.Text = ""
        btnHomePlayer5.Text = ""
        btnHomePlayer6.Text = ""
        btnHomePlayer7.Text = ""
        btnHomePlayer8.Text = ""
        btnHomePlayer9.Text = ""
        btnHomePlayer10.Text = ""
        btnHomePlayer11.Text = ""

        btnHomeSub1.Visible = True
        btnHomeSub2.Visible = True
        btnHomeSub3.Visible = True
        btnHomeSub4.Visible = True
        btnHomeSub5.Visible = True
        btnHomeSub6.Visible = True
        btnHomeSub7.Visible = True
        btnHomeSub8.Visible = True
        btnHomeSub9.Visible = True
    End Sub

    ''' <summary>
    ''' To clear the display labels
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearTouchLabels()
        Try
            lblValTeam.Text = String.Empty
            lblValUniform.Text = String.Empty
            m_strPlayerID = String.Empty
            lblValType.Text = String.Empty
            lblValLocation.Text = String.Empty
            txtTime.Text = String.Empty
            m_intTouchTeamID = -1
            m_intTouchTypeID = -1 'commented time being delaying the refresh so as to highlight the next event
            m_X_FieldZone = -1
            m_Y_FieldZone = -1
            m_decSequenceNo = "-1"
            lblLocationVal2.Text = String.Empty
            m_X_FieldZone_Def = -1
            m_Y_FieldZone_Def = -1
            HighlightTeam(enTeam.None)
            btnHomePrimary.BackColor = m_objGameDetails.HomeTeamColor
            btnVisitPrimary.BackColor = m_objGameDetails.VisitorTeamColor
            btn50Visit.BackColor = m_objGameDetails.VisitorTeamColor
            btn50Home.BackColor = m_objGameDetails.HomeTeamColor
            btnAerialHome.BackColor = m_objGameDetails.HomeTeamColor
            btnAerialVisit.BackColor = m_objGameDetails.VisitorTeamColor
            btnRunWithBallHome.BackColor = m_objGameDetails.HomeTeamColor
            btnRunWithBallVisit.BackColor = m_objGameDetails.VisitorTeamColor

            If m_objclsGameDetails.SerialNo = 2 Then
                pnlVisit.Visible = False
                pnlVisitBench.Visible = False
            Else
                pnlHome.Visible = False
                pnlHomeBench.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To populate Home Team and Visit Team Players by LineUps
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindTeamRostersByLineUp()
        Try

            'Home Team Roster
            Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
            If dsHomeTeam IsNot Nothing Then
                If dsHomeTeam.Tables.Count > 0 Then
                    dsHomeTeam.Tables(0).TableName = "Home"
                    If dsHomeTeam.Tables("Home").Rows.Count > 0 Then
                        ButtonsHomeTeamTagged(dsHomeTeam.Tables("Home"), True)
                        m_blnHomeLineUp = True
                        m_hthome.Clear()
                        m_hthomeUniform.Clear()
                        For Each drHomeTeam As DataRow In dsHomeTeam.Tables("Home").Rows
                            If Not Convert.ToInt32(drHomeTeam("DISPLAY_UNIFORM_NUMBER")) = 0 And Not m_hthome.Contains(drHomeTeam("DISPLAY_UNIFORM_NUMBER")) Then
                                m_hthome.Add(drHomeTeam("DISPLAY_UNIFORM_NUMBER"), drHomeTeam("PLAYER_ID"))
                                m_hthomeUniform.Add(drHomeTeam("PLAYER_ID"), drHomeTeam("DISPLAY_UNIFORM_NUMBER"))
                            End If
                        Next

                    End If
                End If
            End If

            'Away TeamRoster
            Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
            If dsAwayTeam IsNot Nothing Then
                If dsAwayTeam.Tables.Count > 0 Then
                    dsAwayTeam.Tables(0).TableName = "Away"
                    If dsAwayTeam.Tables("Away").Rows.Count > 0 Then
                        ButtonsVisitTeamTagged(dsAwayTeam.Tables("Away"), True)
                        m_blnVisitLineUp = True
                        m_htAway.Clear()
                        m_htAwayUniform.Clear()
                        For Each drAwayTeam As DataRow In dsAwayTeam.Tables("Away").Rows
                            If Not Convert.ToInt32(drAwayTeam("DISPLAY_UNIFORM_NUMBER")) = 0 And Not m_htAway.Contains(drAwayTeam("DISPLAY_UNIFORM_NUMBER")) Then
                                m_htAway.Add(drAwayTeam("DISPLAY_UNIFORM_NUMBER"), drAwayTeam("PLAYER_ID"))
                                m_htAwayUniform.Add(drAwayTeam("PLAYER_ID"), drAwayTeam("DISPLAY_UNIFORM_NUMBER"))
                            End If
                        Next
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To populate selected data in Touch display labels
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SelectedListViewDisplay()
        Try
            m_dsTouchData = m_objclsTouches.GetTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo, m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.AwayTeamID, 0)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    If m_dsTouchData.Tables(0).Rows.Count > 0 Then
                        If dgvTouches.SelectedRows.Count > 0 Then
                            Dim listselectDatarow() As DataRow
                            Dim m_intUID As Integer
                            Dim m_strTouchType As String = ""

                            'For Each lvwitm As ListViewItem In lvwTouches.SelectedItems
                            '    m_decSequenceNo = lvwitm.SubItems(7).Text.Trim()
                            '    m_intUID = CInt(lvwitm.SubItems(8).Text.Trim())
                            '    m_strTouchType = lvwitm.SubItems(5).Text.Trim()
                            'Next

                            m_decSequenceNo = CStr(dgvTouches.SelectedRows(0).Cells("Sequence").Value)
                            m_intUID = CInt(dgvTouches.SelectedRows(0).Cells("Uid").Value)
                            m_strTouchType = CStr(dgvTouches.SelectedRows(0).Cells("Type").Value)

                            If m_strTouchType = "Start Period" Or m_strTouchType = "End Period" Then
                                ' YOU CAN'T EDIT A START PERIOD OR END PERIOD
                                MessageDialog.Show(strmessage23, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Try
                            End If

                            If m_intUID < 0 Then
                                MessageDialog.Show(strmessage4 + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Try
                            End If

                            m_objclsGameDetails.IsEditMode = True
                            frmMain.ChangeTheme(True)
                            Me.ChangeTheme(True)


                            listselectDatarow = m_dsTouchData.Tables("Touches").Select("SEQUENCE_NUMBER='" & m_decSequenceNo & "' and UNIQUE_ID = '" & m_intUID & "'")
                            If listselectDatarow.Length > 0 Then

                                If listselectDatarow(0).Item("TEAM_ID") IsNot DBNull.Value Then
                                    If CInt(listselectDatarow(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                                        lblValTeam.Text = m_objclsGameDetails.HomeTeam
                                        m_intTouchTeamID = m_objclsGameDetails.HomeTeamID
                                    Else
                                        lblValTeam.Text = m_objclsGameDetails.AwayTeam
                                        m_intTouchTeamID = m_objclsGameDetails.AwayTeamID
                                    End If
                                Else
                                    lblValTeam.Text = String.Empty
                                End If
                                If listselectDatarow(0).Item("PERIOD") IsNot DBNull.Value Then
                                    m_CurrentPeriod = CInt(listselectDatarow(0).Item("PERIOD"))
                                End If

                                If listselectDatarow(0).Item("DISPLAY_UNIFORM_NUMBER") IsNot DBNull.Value Then
                                    lblValUniform.Text = listselectDatarow(0).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                    m_strPlayerID = CStr(listselectDatarow(0).Item("PLAYER_ID").ToString())
                                Else
                                    If listselectDatarow(0).Item("PLAYER_ID") IsNot DBNull.Value Then
                                        m_strPlayerID = CStr(listselectDatarow(0).Item("PLAYER_ID").ToString())
                                    End If
                                    lblValUniform.Text = String.Empty
                                End If
                                ChangePlayerBtnBackColor(m_strPlayerID, m_intTouchTeamID)
                                If listselectDatarow(0).Item("PLAYER_ID") IsNot DBNull.Value Then
                                    m_strPlayerID = CStr(listselectDatarow(0).Item("PLAYER_ID").ToString())
                                End If

                                If listselectDatarow(0).Item("TOUCH_TYPE_DESC") IsNot DBNull.Value Then
                                    lblValType.Text = listselectDatarow(0).Item("TOUCH_TYPE_DESC").ToString()
                                    m_intTouchTypeID = CInt(listselectDatarow(0).Item("TOUCH_TYPE_ID"))
                                    Select Case m_intTouchTypeID
                                        Case -99
                                            BtnIntPass.BackColor = m_clrHighlightedButtonColor
                                        Case -98
                                            BtnIntBPass.BackColor = m_clrHighlightedButtonColor
                                        Case 0
                                            btnTypeOther.BackColor = m_clrHighlightedButtonColor
                                        Case 1
                                            btnTypePass.BackColor = m_clrHighlightedButtonColor
                                        Case 2
                                            btnTypeInt.BackColor = m_clrHighlightedButtonColor
                                        Case 3
                                            btnTypeBlock.BackColor = m_clrHighlightedButtonColor
                                        Case 4
                                            btnTypeTackle.BackColor = m_clrHighlightedButtonColor
                                        Case 5
                                            btnTypeSave.BackColor = m_clrHighlightedButtonColor
                                        Case 6
                                            btnTypeCatch.BackColor = m_clrHighlightedButtonColor
                                        Case 7
                                            btnTypeRunWithBall.BackColor = m_clrHighlightedButtonColor
                                        Case 8
                                            btnTypeThrowIn.BackColor = m_clrHighlightedButtonColor
                                        Case 9
                                            btnTypeClearance.BackColor = m_clrHighlightedButtonColor
                                        Case 10
                                            btnTypeGoalAttempt.BackColor = m_clrHighlightedButtonColor
                                        Case 11
                                            btnTypeTouch.BackColor = m_clrHighlightedButtonColor
                                        Case 12
                                            btnTypeGoalKick.BackColor = m_clrHighlightedButtonColor
                                            ''Arindam added code for new touch types 12-Apr-12
                                        Case 17
                                            btnTypeRunWithBall.BackColor = m_clrHighlightedButtonColor
                                        Case 18 'TOSOCRS-310
                                            btnAirWin.BackColor = m_clrHighlightedButtonColor
                                        Case 20 'TOSOCRS-310
                                            btn50Win.BackColor = m_clrHighlightedButtonColor
                                        Case 22
                                            btnGKHand.BackColor = m_clrHighlightedButtonColor
                                        Case 23
                                            btnGKThrow.BackColor = m_clrHighlightedButtonColor
                                        Case 24
                                            BtnKpPickUp.BackColor = m_clrHighlightedButtonColor
                                    End Select

                                Else
                                    lblValType.Text = String.Empty
                                End If

                                If listselectDatarow(0).Item("X_FIELD_ZONE") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE") IsNot DBNull.Value Then
                                    lblValLocation.Text = "X=" & listselectDatarow(0).Item("X_FIELD_ZONE").ToString() & ", Y=" & listselectDatarow(0).Item("Y_FIELD_ZONE").ToString()
                                    m_X_FieldZone = CInt(listselectDatarow(0).Item("X_FIELD_ZONE"))
                                    m_Y_FieldZone = CInt(listselectDatarow(0).Item("Y_FIELD_ZONE"))
                                    If m_objclsGameDetails.SerialNo <> 1 Then
                                        UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                                        lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                                    End If

                                Else
                                    lblValLocation.Text = String.Empty
                                    m_X_FieldZone = -1
                                    m_Y_FieldZone = -1
                                End If



                                Dim ElapsedTime As Integer
                                Dim CurrPeriod As Integer
                                ElapsedTime = CInt(listselectDatarow(0).Item("TIME_ELAPSED"))
                                CurrPeriod = CInt(listselectDatarow(0).Item("PERIOD"))
                                txtTime.Text = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))

                                ''
                                'txtTime.Text = clsUtility.ConvertSecondToMinute(Convert.ToDouble(listselectDatarow(0).Item("TIME_ELAPSED")), False)

                                'AUDIT TRIAL
                                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemDoubleClicked, listselectDatarow(0).Item("PERIOD").ToString() & " - " & listselectDatarow(0).Item("DISPLAY_UNIFORM_NUMBER").ToString() & " - " & lblValLocation.Text, 1, 0)

                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Sub SelectedListViewDisplay_Edit()
        Try
            m_dsTouchData = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    If m_dsTouchData.Tables(0).Rows.Count > 0 Then
                        If dgvTouches.SelectedRows.Count > 0 Then
                            Dim listselectDatarow() As DataRow
                            Dim m_intUID As Integer
                            Dim m_strTouchType As String = ""
                            'For Each row As DataRow In dgvTouches.SelectedRows
                            m_decSequenceNo = CStr(dgvTouches.SelectedRows(0).Cells("Sequence").Value)
                            m_intUID = CInt(dgvTouches.SelectedRows(0).Cells("Uid").Value)
                            m_strTouchType = CStr(dgvTouches.SelectedRows(0).Cells("Type").Value)
                            'Next
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Edit Double Click -- UId and Seq No] " & m_intUID & "," & m_decSequenceNo, 1, 0)

                            If m_strTouchType = "Start Period" Or m_strTouchType = "End Period" Then
                                ' YOU CAN'T EDIT A START PERIOD OR END PERIOD
                                MessageDialog.Show(strmessage23, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Try
                            End If

                            m_objclsGameDetails.IsEditMode = True
                            frmMain.ChangeTheme(True)
                            Me.ChangeTheme(True)

                            ''Arindam 21-Aug-2014
                            'RM 7904 - Recipient_Code
                            Dim drPrvTouch As DataSet
                            Dim X_FieldZone_Prev As Integer = -1
                            Dim Y_FieldZone_Prev As Integer = -1
                            Dim recipient_ID As Integer = -1
                            Dim reci_UN As String = String.Empty
                            Dim PrevTeamID As Integer = -1

                            drPrvTouch = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CDec(m_decSequenceNo), "PREVCLK")
                            'drPrev = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER < " & m_decSequenceNo, "SEQUENCE_NUMBER")

                            If drPrvTouch.Tables(0).Rows.Count > 0 Then
                                If drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                                    X_FieldZone_Prev = CInt(drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF"))
                                    Y_FieldZone_Prev = CInt(drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF"))
                                    'If listselectDatarow(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                                    lblValLocation.Text = "X=" & drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF").ToString() & ", Y=" & drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF").ToString()
                                    m_X_FieldZone = CInt(drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF"))
                                    m_Y_FieldZone = CInt(drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF"))
                                    PrevTeamID = CInt(drPrvTouch.Tables(0).Rows(0).Item("TEAM_ID"))
                                    'If m_objclsGameDetails.SerialNo <> 1 Then
                                    '    UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                                    '    lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                                    'End If
                                    'End If
                                End If
                                If drPrvTouch.Tables(0).Rows(0).Item("RECIPIENT_ID") IsNot DBNull.Value And drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID") IsNot DBNull.Value Then
                                    If CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 1 Or CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 8 Or CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 12 Or CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 23 Then 'TOSOCRS-41
                                        If m_objclsGameDetails.SerialNo = 2 Then
                                            If CInt(drPrvTouch.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                                                recipient_ID = CInt(drPrvTouch.Tables(0).Rows(0).Item("RECIPIENT_ID"))
                                                reci_UN = Get_Uniform_ID(recipient_ID, m_objclsGameDetails.HomeTeamID)
                                            End If
                                        ElseIf m_objclsGameDetails.SerialNo = 3 Then
                                            If CInt(drPrvTouch.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.AwayTeamID Then
                                                recipient_ID = CInt(drPrvTouch.Tables(0).Rows(0).Item("RECIPIENT_ID"))
                                                reci_UN = Get_Uniform_ID(recipient_ID, m_objclsGameDetails.AwayTeamID)
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'Dim drPrvTouch As DataSet
                            'Dim X_FieldZone_Prev As Integer = -1
                            'Dim Y_FieldZone_Prev As Integer = -1
                            'drPrvTouch = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CDec(m_decSequenceNo), "PREVCLK")
                            ''drPrev = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER < " & m_decSequenceNo, "SEQUENCE_NUMBER")

                            'If drPrvTouch.Tables(0).Rows.Count > 0 Then
                            '    If drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                            '        X_FieldZone_Prev = CInt(drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF"))
                            '        Y_FieldZone_Prev = CInt(drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF"))
                            '        'If listselectDatarow(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                            '        lblValLocation.Text = "X=" & drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF").ToString() & ", Y=" & drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF").ToString()
                            '        m_X_FieldZone = CInt(drPrvTouch.Tables(0).Rows(0).Item("X_FIELD_ZONE_DEF"))
                            '        m_Y_FieldZone = CInt(drPrvTouch.Tables(0).Rows(0).Item("Y_FIELD_ZONE_DEF"))
                            '        If m_objclsGameDetails.SerialNo <> 1 Then
                            '            UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                            '            lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                            '        End If
                            '        'End If
                            '    End If
                            'End If
                            ''Arindam 21-Aug-2014

                            Dim plyID As Integer = -1
                            Dim xCoord As Integer = -1
                            Dim yCoord As Integer = -1
                            listselectDatarow = m_dsTouchData.Tables("Touches").Select("SEQUENCE_NUMBER='" & m_decSequenceNo & "' and UNIQUE_ID = '" & m_intUID & "'")
                            If listselectDatarow.Length > 0 Then
                                If m_objclsGameDetails.SerialNo = 2 Then
                                    If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                        If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.AwayTeamID Then
                                            MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            btnClear_Click(Nothing, Nothing)
                                            Exit Sub
                                        End If
                                    End If
                                ElseIf m_objclsGameDetails.SerialNo = 3 Then
                                    If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                        If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                            MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            btnClear_Click(Nothing, Nothing)
                                            Exit Sub
                                        End If
                                    End If
                                End If
                                If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                    If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                        lblValTeam.Text = m_objclsGameDetails.HomeTeam
                                        m_intTouchTeamID = m_objclsGameDetails.HomeTeamID
                                        HighlightTeam(enTeam.Home)
                                        pnlVisit.Visible = False
                                        pnlVisitBench.Visible = False
                                    Else
                                        lblValTeam.Text = m_objclsGameDetails.AwayTeam
                                        m_intTouchTeamID = m_objclsGameDetails.AwayTeamID
                                        HighlightTeam(enTeam.Away)
                                        pnlHome.Visible = False
                                        pnlHomeBench.Visible = False
                                    End If
                                Else
                                    lblValTeam.Text = String.Empty
                                End If
                                If listselectDatarow(0).Item("PERIOD") IsNot DBNull.Value Then
                                    m_CurrentPeriod = CInt(listselectDatarow(0).Item("PERIOD"))
                                End If

                                ''
                                If listselectDatarow(0).Item("PLAYER_ID") IsNot DBNull.Value Then
                                    m_strPlayerID = CStr(listselectDatarow(0).Item("PLAYER_ID").ToString())
                                    If listselectDatarow(0).Item("DISPLAY_UNIFORM_NUMBER") IsNot DBNull.Value Then
                                        lblValUniform.Text = listselectDatarow(0).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                    Else
                                        lblValUniform.Text = String.Empty
                                    End If
                                    ChangePlayerBtnBackColor(m_strPlayerID, m_intTouchTeamID)
                                    m_strRecipientID = String.Empty
                                    m_strPlayerID = CStr(listselectDatarow(0).Item("PLAYER_ID").ToString())
                                    If String.IsNullOrEmpty(m_strPlayerID) Then
                                        plyID = -1
                                    Else
                                        plyID = CInt(m_strPlayerID)
                                    End If
                                Else
                                    If recipient_ID > 0 Then

                                        m_strPlayerID = CStr(recipient_ID)
                                        ChangePlayerBtnBackColor(m_strPlayerID, m_intRecTouchTeamID)

                                        'RM 7904 - Recipient_Code

                                        If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                            m_intRecTouchTeamID = CInt(listselectDatarow(0).Item("TEAM_ID1"))
                                        End If
                                        If m_strRecipientID Is String.Empty Then
                                            m_strRecipientID = CStr(recipient_ID)
                                        End If
                                        setPlayerTeam(m_strRecipientID, m_intRecTouchTeamID)

                                        m_strRecipientID = String.Empty
                                        m_strPlayerID = CStr(recipient_ID)
                                    End If
                                End If

                                If listselectDatarow(0).Item("RECIPIENT_ID") IsNot DBNull.Value Then
                                    m_strRecipientID = CStr(listselectDatarow(0).Item("RECIPIENT_ID").ToString())
                                    reci_UN = String.Empty
                                    ChangeRecipientBtnBackColor(m_strRecipientID, m_intRecTouchTeamID)
                                    setPlayerTeamRecipient(m_strRecipientID, m_intRecTouchTeamID)
                                    'ElseIf recipient_ID > 0 Then
                                    '    m_strRecipientID = CStr(recipient_ID)
                                    '    ChangePlayerBtnBackColor(m_strRecipientID, m_intRecTouchTeamID)
                                    '    setPlayerTeam(m_strRecipientID, m_intRecTouchTeamID)
                                    '    m_strRecipientID = String.Empty
                                    '    m_strPlayerID = CStr(recipient_ID)
                                Else
                                    reci_UN = String.Empty
                                End If


                                'RM 7904 - Recipient_Code
                                If listselectDatarow(0).Item("REC_TEAM_ID") IsNot DBNull.Value Then
                                    If CInt(listselectDatarow(0).Item("REC_TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                                        'lblValTeam.Text = m_objclsGameDetails.HomeTeam
                                        m_intRecTouchTeamID = m_objclsGameDetails.HomeTeamID
                                    Else
                                        'lblValTeam.Text = m_objclsGameDetails.AwayTeam
                                        m_intRecTouchTeamID = m_objclsGameDetails.AwayTeamID
                                    End If
                                Else
                                    If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                        m_intRecTouchTeamID = CInt(listselectDatarow(0).Item("TEAM_ID1"))
                                    End If
                                End If

                                'RM 7904 - Recipient_Code



                                If listselectDatarow(0).Item("TOUCH_TYPE_DESC") IsNot DBNull.Value Then
                                    lblValType.Text = listselectDatarow(0).Item("TOUCH_TYPE_DESC").ToString()
                                    m_intTouchTypeID = CInt(listselectDatarow(0).Item("TOUCH_TYPE_ID"))
                                    Select Case m_intTouchTypeID
                                        Case -99
                                            BtnIntPass.BackColor = m_clrHighlightedButtonColor
                                        Case -98
                                            BtnIntBPass.BackColor = m_clrHighlightedButtonColor
                                        Case 0
                                            btnTypeOther.BackColor = m_clrHighlightedButtonColor
                                        Case 1
                                            btnTypePass.BackColor = m_clrHighlightedButtonColor
                                        Case 2
                                            btnTypeInt.BackColor = m_clrHighlightedButtonColor
                                        Case 3
                                            btnTypeBlock.BackColor = m_clrHighlightedButtonColor
                                        Case 4
                                            btnTypeTackle.BackColor = m_clrHighlightedButtonColor
                                        Case 5
                                            btnTypeSave.BackColor = m_clrHighlightedButtonColor
                                        Case 6
                                            btnTypeCatch.BackColor = m_clrHighlightedButtonColor
                                        Case 17 'RM 7904
                                            btnTypeRunWithBall.BackColor = m_clrHighlightedButtonColor
                                        Case 8
                                            btnTypeThrowIn.BackColor = m_clrHighlightedButtonColor
                                        Case 9
                                            btnTypeClearance.BackColor = m_clrHighlightedButtonColor
                                        Case 10
                                            btnTypeGoalAttempt.BackColor = m_clrHighlightedButtonColor
                                        Case 11
                                            btnTypeTouch.BackColor = m_clrHighlightedButtonColor
                                        Case 12
                                            btnTypeGoalKick.BackColor = m_clrHighlightedButtonColor
                                            ''Arindam added code for new touch types 12-Apr-12
                                        Case 17
                                            btnTypeRunWithBall.BackColor = m_clrHighlightedButtonColor
                                        Case 18 'TOSOCRS-310 
                                            btnAirWin.BackColor = m_clrHighlightedButtonColor
                                        Case 20 'TOSOCRS-310
                                            btn50Win.BackColor = m_clrHighlightedButtonColor
                                        Case 22
                                            btnGKHand.BackColor = m_clrHighlightedButtonColor
                                        Case 23
                                            btnGKThrow.BackColor = m_clrHighlightedButtonColor
                                        Case 24
                                            BtnKpPickUp.BackColor = m_clrHighlightedButtonColor
                                    End Select

                                Else
                                    lblValType.Text = String.Empty
                                End If

                                'If m_X_FieldZone <> -1 And m_Y_FieldZone <> -1 Then
                                '    If m_objclsGameDetails.SerialNo <> 1 Then
                                '        UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                                '        lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                                '    End If
                                '    If listselectDatarow(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                                '        lblLocationVal2.Text = "X=" & listselectDatarow(0).Item("X_FIELD_ZONE_DEF").ToString() & ", Y=" & listselectDatarow(0).Item("Y_FIELD_ZONE_DEF").ToString()
                                '        m_X_FieldZone_Def = CInt(listselectDatarow(0).Item("X_FIELD_ZONE_DEF"))
                                '        m_Y_FieldZone_Def = CInt(listselectDatarow(0).Item("Y_FIELD_ZONE_DEF"))
                                '        If m_objclsGameDetails.SerialNo <> 1 Then
                                '            UdcSoccerField1.USFSetMark(m_X_FieldZone_Def, m_Y_FieldZone_Def)
                                '            lblSelection2.Text = m_X_FieldZone_Def & ", " & m_Y_FieldZone_Def
                                '        End If
                                '    End If
                                'Else
                                If listselectDatarow(0).Item("X_FIELD_ZONE") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE") IsNot DBNull.Value Then
                                    lblValLocation.Text = "X=" & listselectDatarow(0).Item("X_FIELD_ZONE").ToString() & ", Y=" & listselectDatarow(0).Item("Y_FIELD_ZONE").ToString()
                                    m_X_FieldZone = CInt(listselectDatarow(0).Item("X_FIELD_ZONE"))
                                    m_Y_FieldZone = CInt(listselectDatarow(0).Item("Y_FIELD_ZONE"))
                                    If m_objclsGameDetails.SerialNo <> 1 Then
                                        UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                                        lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                                    End If
                                    xCoord = m_X_FieldZone
                                    yCoord = m_Y_FieldZone
                                    If listselectDatarow(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                                        lblLocationVal2.Text = "X=" & listselectDatarow(0).Item("X_FIELD_ZONE_DEF").ToString() & ", Y=" & listselectDatarow(0).Item("Y_FIELD_ZONE_DEF").ToString()
                                        m_X_FieldZone_Def = CInt(listselectDatarow(0).Item("X_FIELD_ZONE_DEF"))
                                        m_Y_FieldZone_Def = CInt(listselectDatarow(0).Item("Y_FIELD_ZONE_DEF"))
                                        If m_objclsGameDetails.SerialNo <> 1 Then
                                            UdcSoccerField1.USFSetMark(m_X_FieldZone_Def, m_Y_FieldZone_Def)
                                            lblSelection2.Text = m_X_FieldZone_Def & ", " & m_Y_FieldZone_Def
                                        End If
                                    End If
                                Else
                                    If X_FieldZone_Prev <> -1 And Y_FieldZone_Prev <> -1 And PrevTeamID = CInt(listselectDatarow(0).Item("TEAM_ID1")) Then
                                        If m_objclsGameDetails.SerialNo <> 1 Then
                                            UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                                            lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                                        End If
                                    Else

                                    End If

                                End If
                                'End If


                                If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then

                                    Dim recPlyID As Integer

                                    If String.IsNullOrEmpty(m_strRecipientID) Then
                                        recPlyID = -1
                                    Else
                                        recPlyID = CInt(m_strRecipientID)
                                    End If
                                    collectTouchesDataBE(m_intTouchTypeID, plyID, recPlyID, xCoord, yCoord, m_X_FieldZone_Def, m_Y_FieldZone_Def)
                                End If

                                'If listselectDatarow(0).Item("X_FIELD_ZONE_DEF") IsNot DBNull.Value And listselectDatarow(0).Item("Y_FIELD_ZONE_DEF") IsNot DBNull.Value Then
                                '    lblLocationVal2.Text = "X=" & listselectDatarow(0).Item("X_FIELD_ZONE_DEF").ToString() & ", Y=" & listselectDatarow(0).Item("Y_FIELD_ZONE_DEF").ToString()
                                '    m_X_FieldZone = CInt(listselectDatarow(0).Item("X_FIELD_ZONE_DEF"))
                                '    m_Y_FieldZone = CInt(listselectDatarow(0).Item("Y_FIELD_ZONE_DEF"))
                                '    If m_objclsGameDetails.SerialNo <> 1 Then
                                '        UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                                '        lblSelection2.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                                '    End If
                                'End If

                                Dim ElapsedTime As Integer
                                Dim CurrPeriod As Integer
                                ElapsedTime = CInt(listselectDatarow(0).Item("TIME_ELAPSED"))
                                CurrPeriod = CInt(listselectDatarow(0).Item("PERIOD"))
                                txtTime.Text = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                                txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))

                                ''
                                'txtTime.Text = clsUtility.ConvertSecondToMinute(Convert.ToDouble(listselectDatarow(0).Item("TIME_ELAPSED")), False)
                            Else
                                listselectDatarow = m_dsTouchData.Tables("Touches").Select("SEQUENCE_NUMBER='" & m_decSequenceNo & "'")
                                If listselectDatarow.Length > 0 Then
                                    If m_objclsGameDetails.SerialNo = 2 Then
                                        If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                            If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.AwayTeamID Then
                                                MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                btnClear_Click(Nothing, Nothing)
                                                Exit Sub
                                            End If
                                        End If
                                    ElseIf m_objclsGameDetails.SerialNo = 3 Then
                                        If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                            If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                                MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                btnClear_Click(Nothing, Nothing)
                                                Exit Sub
                                            End If
                                        End If
                                    End If
                                End If
                                'AUDIT TRIAL
                                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemDoubleClicked, listselectDatarow(0).Item("PERIOD").ToString() & " - " & listselectDatarow(0).Item("DISPLAY_UNIFORM_NUMBER").ToString() & " - " & lblValLocation.Text, 1, 0)
                                'Else
                                listselectDatarow = m_dsTouchData.Tables("Touches").Select("SEQUENCE_NUMBER='" & m_decSequenceNo & "'")
                                If listselectDatarow.Length > 0 Then
                                    If m_objclsGameDetails.SerialNo = 2 Then
                                        If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                            If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.AwayTeamID Then
                                                MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                btnClear_Click(Nothing, Nothing)
                                                Exit Sub
                                            End If
                                        End If
                                    ElseIf m_objclsGameDetails.SerialNo = 3 Then
                                        If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                            If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                                MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                btnClear_Click(Nothing, Nothing)
                                                Exit Sub
                                            End If
                                        End If
                                    End If
                                End If

                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'TOSOCRS-64 [Touch Software-Autofill GoalKeeper for GK Touches]
    Private Sub SelectedPreviousRecPlayer()
        Try
            m_dsTouchData = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    If m_dsTouchData.Tables(0).Rows.Count > 0 Then
                        If dgvTouches.SelectedRows.Count > 0 Then
                            m_decSequenceNo = CStr(dgvTouches.SelectedRows(0).Cells("Sequence").Value)
                            Dim drPrvTouch As DataSet
                            Dim recipient_ID As Integer = -1

                            drPrvTouch = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CDec(m_decSequenceNo), "PREVCLK")
                            If drPrvTouch.Tables(0).Rows.Count > 0 Then
                                If drPrvTouch.Tables(0).Rows(0).Item("RECIPIENT_ID") IsNot DBNull.Value And drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID") IsNot DBNull.Value Then
                                    If CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 1 Or CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 8 Or CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 12 Or CInt(drPrvTouch.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = 23 Then 'TOSOCRS-41
                                        If m_objclsGameDetails.SerialNo = 2 Then
                                            If CInt(drPrvTouch.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                                                recipient_ID = CInt(drPrvTouch.Tables(0).Rows(0).Item("RECIPIENT_ID"))

                                            End If
                                        ElseIf m_objclsGameDetails.SerialNo = 3 Then
                                            If CInt(drPrvTouch.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.AwayTeamID Then
                                                recipient_ID = CInt(drPrvTouch.Tables(0).Rows(0).Item("RECIPIENT_ID"))
                                            End If
                                        End If
                                    End If
                                End If
                            End If

                            If recipient_ID > 0 Then
                                m_strPlayerID = CStr(recipient_ID)
                                ChangePlayerBtnBackColor(m_strPlayerID, m_intRecTouchTeamID)
                                m_boolPlayer1Filled = True
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub collectTouchesDataBE(ByVal TouchTypeID As Integer, ByVal PlayerID As Integer, ByVal RecipientID As Integer, ByVal X_FieldZone As Integer, ByVal Y_FieldZone As Integer, ByVal X_FieldZone_Def As Integer, ByVal Y_FieldZone_Def As Integer)
        Try
            m_TDtouchTypeID = TouchTypeID
            m_TDplayerID = PlayerID
            m_TDsecondPlayer = RecipientID
            m_TDxFieldZone = X_FieldZone
            m_TDyFieldZone = Y_FieldZone
            m_TDxFieldZoneDef = X_FieldZone_Def
            m_TDyFieldZoneDef = Y_FieldZone_Def
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function compareTouchesData(ByVal TouchTypeID As Integer, ByVal X_FieldZone As Integer, ByVal Y_FieldZone As Integer, ByVal X_FieldZone_Def As Integer, ByVal Y_FieldZone_Def As Integer) As Boolean
        Try
            Dim PlayerID As Integer = -1
            If Not String.IsNullOrEmpty(m_strPlayerID) And Not m_intTouchTeamID = -1 Then
                PlayerID = Get_Player_ID(Convert.ToInt32(m_strPlayerID), m_intTouchTeamID)
            End If

            'RM 7904 - Recipient_Code
            Dim RecipientID As Integer = -1
            If Not String.IsNullOrEmpty(m_strRecipientID) And Not m_intRecTouchTeamID = -1 Then
                RecipientID = Get_Recipient_ID(Convert.ToInt32(m_strRecipientID), m_intRecTouchTeamID)
            End If

            If TouchTypeID = m_TDtouchTypeID And PlayerID = m_TDplayerID And RecipientID = m_TDsecondPlayer And X_FieldZone = m_TDxFieldZone And Y_FieldZone = m_TDyFieldZone And X_FieldZone_Def = m_TDxFieldZoneDef And Y_FieldZone_Def = m_TDyFieldZoneDef Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub clearTouchData()
        Try
            m_TDplayerID = -1
            m_TDsecondPlayer = -1
            m_TDtouchTypeID = -1
            m_TDxFieldZone = -1
            m_TDyFieldZone = -1
            m_TDxFieldZoneDef = -1
            m_TDyFieldZoneDef = -1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub SetTime()
        Try

            If txtTime.Text.Replace(" :", "").Trim = "" Then
                If m_objclsGameDetails.IsEditMode = False Then
                    'If String.IsNullOrEmpty(strArrCurrentTime(0).Trim()) And String.IsNullOrEmpty(strArrCurrentTime(1).Trim()) Then
                    txtTime.Text = frmMain.UdcRunningClock1.URCCurrentTime.Replace(":", "").PadLeft(5, CChar(" "))
                    'End If
                End If
            End If
            'Dim strArrCurrentTime As String() = txtTime.Text.Split(chrSep)
            If m_objclsGameDetails.SerialNo <> 1 Then
                UdcSoccerField1.Enabled = True
            Else
                UdcSoccerField1.Enabled = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CheckForSave()
        Try
            If m_decSequenceNo = "-1" Then
                If m_objclsGameDetails.SerialNo = 1 Then
                    If Not String.IsNullOrEmpty(lblValTeam.Text) And Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                        btnSave_Click(Nothing, Nothing)
                    End If
                Else
                    If Not String.IsNullOrEmpty(lblValTeam.Text) And Not String.IsNullOrEmpty(m_strPlayerID) And Not String.IsNullOrEmpty(lblValLocation.Text) And Not String.IsNullOrEmpty(lblValType.Text) And Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                        btnSave_Click(Nothing, Nothing)
                    End If
                End If
            Else
                If m_objclsGameDetails.SerialNo = 1 And m_objclsGameDetails.IsEditMode Then
                    If Not String.IsNullOrEmpty(lblValTeam.Text) And Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                        btnSave_Click(Nothing, Nothing)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CheckForData() As Boolean
        Try
            If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                '    If String.IsNullOrEmpty(lblValTeam.Text) Then
                '        MessageDialog.Show(strmessage21, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                '        Return False
                '    End If
                'Else
                If String.IsNullOrEmpty(lblValTeam.Text) And String.IsNullOrEmpty(m_strPlayerID) And String.IsNullOrEmpty(lblValLocation.Text) And String.IsNullOrEmpty(lblValType.Text) Then
                    MessageDialog.Show(strmessage21, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                ElseIf String.IsNullOrEmpty(m_strPlayerID) Then
                    MessageDialog.Show(strmessage25, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                ElseIf String.IsNullOrEmpty(lblValType.Text) Then
                    MessageDialog.Show(strmessage31, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                ElseIf String.IsNullOrEmpty(lblValLocation.Text) Then
                    MessageDialog.Show(strmessage32, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                    'ElseIf String.IsNullOrEmpty(lblValTeam.Text) And String.IsNullOrEmpty(m_strPlayerID) And String.IsNullOrEmpty(lblValLocation.Text) And String.IsNullOrEmpty(lblValType.Text) Then
                    '    MessageDialog.Show(strmessage21, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    '    Exit Sub
                ElseIf (Not String.IsNullOrEmpty(lblValTeam.Text) And String.IsNullOrEmpty(m_strPlayerID)) Or (String.IsNullOrEmpty(lblValTeam.Text) And Not String.IsNullOrEmpty(m_strPlayerID)) Then
                    MessageDialog.Show(strmessage11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                ElseIf Not String.IsNullOrEmpty(m_strPlayerID) And Not m_intTouchTeamID = -1 Then
                    If Get_Player_ID(Convert.ToInt32(m_strPlayerID), m_intTouchTeamID) = 0 Then
                        MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Return False
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub ChangeTheme(ByVal EditMode As Boolean)
        Try
            If EditMode Then
                Me.BackColor = Color.LightGoldenrodYellow
                pnlHome.BackColor = Color.LightGoldenrodYellow
                pnlVisit.BackColor = Color.LightGoldenrodYellow
                pnlHomeBench.BackColor = Color.LightGoldenrodYellow
                pnlVisitBench.BackColor = Color.LightGoldenrodYellow
                pnlTouchDisplay.BackColor = Color.Beige
                pnlTouchTypes.BackColor = Color.Khaki
            Else
                Me.BackColor = Color.WhiteSmoke
                pnlHome.BackColor = Color.WhiteSmoke
                pnlVisit.BackColor = Color.WhiteSmoke
                pnlHomeBench.BackColor = Color.WhiteSmoke
                pnlVisitBench.BackColor = Color.WhiteSmoke
                pnlTouchDisplay.BackColor = Color.Lavender
                pnlTouchTypes.BackColor = Color.LightSteelBlue
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            Dim StrTime As String = "00:00"
            If m_objclsGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "45+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "90+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "90+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 3 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "105+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "105+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 4 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "120+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "120+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 5 Then
                    StrTime = "120"
                End If
                'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
            Else
                If CurrPeriod = 1 Or CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "45+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

                    End If
                Else
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "15+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "15+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                End If
            End If
            Return StrTime
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objclsGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer, ByVal CurrPeriod As Integer) As Integer
        Try
            If CurrPeriod = 1 Then
                Return CurrentElapsedTime
            ElseIf CurrPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF
            ElseIf CurrPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF
            ElseIf CurrPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region


#Region " Public methods "

    Public Sub DisableControls(ByVal EnableType As Boolean)
        Try
            If EnableType = False Then
                '7904 #44 - Touch 1 goes to Halh, Touch 2 and 3 player lists shoould not be grayed out
                If m_objclsGameDetails.SerialNo = 1 Then
                    pnlHome.Enabled = EnableType
                    pnlHomeBench.Enabled = EnableType
                    pnlVisit.Enabled = EnableType
                    pnlVisitBench.Enabled = EnableType
                    pnlTouchTypes.Enabled = EnableType
                End If
            Else
                If m_objclsGameDetails.SerialNo = 2 Then
                    pnlHome.Visible = True
                    If btnHomePlayer12.Visible = True And pnlHomeBench.Visible = False Then
                        pnlHomeBench.Visible = False
                        pnlHomeBench.Enabled = False
                    Else
                        pnlHomeBench.Visible = True
                        pnlHomeBench.Enabled = True
                    End If

                    pnlHome.Enabled = True

                    pnlVisit.Visible = False
                    pnlVisitBench.Visible = False
                    pnlTouchTypes.Enabled = True
                ElseIf m_objclsGameDetails.SerialNo = 3 Then
                    pnlVisit.Visible = True
                    If btnVisitPlayer12.Visible = True And pnlVisitBench.Visible = False Then
                        pnlVisitBench.Visible = False
                        pnlVisitBench.Enabled = False
                    Else
                        pnlVisitBench.Visible = True
                        pnlVisitBench.Enabled = True
                    End If
                    pnlVisit.Enabled = True

                    pnlHome.Visible = False
                    pnlHomeBench.Visible = False
                    pnlTouchTypes.Enabled = True
                Else
                    pnlHome.Enabled = EnableType
                    pnlHomeBench.Enabled = EnableType
                    pnlVisit.Enabled = EnableType
                    pnlVisitBench.Enabled = EnableType
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SwitchSides()
        Try
            pnlHome.Top = PLAYER_PANEL_TOP
            pnlHomeBench.Top = BENCH_PANEL_TOP
            pnlVisitBench.Top = BENCH_PANEL_TOP

            If m_objclsGameDetails.IsHomeTeamOnLeftSwitch = True Then
                'pnlHome.Left = m_intRightPlayerPanelLeft
                pnlHome.Visible = False
                pnlVisit.Left = m_intLeftPlayerPanelLeft
                pnlVisit.Visible = True
                pnlHomeBench.Visible = False
                'pnlHomeBench.Left = RIGHT_BENCH_PANEL_LEFT
                pnlVisitBench.Left = LEFT_PLAYER_PANEL_LEFT
                If pnlVisit.Height = m_intPlayerPanelHeightShort Then
                    pnlVisitBench.Visible = True
                End If

                m_objclsGameDetails.IsHomeTeamOnLeftSwitch = False
            Else
                pnlHome.Left = m_intLeftPlayerPanelLeft
                pnlHome.Visible = True
                pnlVisit.Visible = False
                'pnlVisit.Left = m_intRightPlayerPanelLeft
                pnlHomeBench.Left = LEFT_PLAYER_PANEL_LEFT
                If pnlHome.Height = m_intPlayerPanelHeightShort Then
                    pnlHomeBench.Visible = True
                End If

                pnlVisitBench.Visible = False
                'pnlVisitBench.Left = RIGHT_BENCH_PANEL_LEFT

                m_objclsGameDetails.IsHomeTeamOnLeftSwitch = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SwitchSidesPrimary() 'TOSOCRS-38 Touch Reporter 1-Switch Sides of Teams
        Try
            Dim Possition As Integer
            Possition = pnlHomePrimary.Left
            pnlHomePrimary.Left = pnlVisitPrimary.Left
            pnlVisitPrimary.Left = Possition
            If m_objclsGameDetails.IsHomeTeamOnLeftSwitch = True Then
                m_objclsGameDetails.IsHomeTeamOnLeftSwitch = False
            Else
                m_objclsGameDetails.IsHomeTeamOnLeftSwitch = True
            End If
            Application.DoEvents()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetTeamColor(ByVal HomeTeam As Boolean, ByVal TeamColor As System.Drawing.Color)
        Try
            Dim clrTextColor As System.Drawing.Color
            clrTextColor = clsUtility.GetTextColor(TeamColor)

            If HomeTeam = True Then
                m_objGeneral.UpdateTeamColor(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, True, TeamColor.ToArgb)
                m_objclsGameDetails.HomeTeamColor = TeamColor
                frmMain.picHomeColorTop.BackColor = m_objGameDetails.HomeTeamColor
                frmMain.picHomeColorBottom.BackColor = m_objGameDetails.HomeTeamColor
                For Each obj As Control In pnlHome.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
                For Each obj As Control In pnlHomeBench.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
                For Each obj As Control In pnlHomePrimary.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
            Else
                m_objGeneral.UpdateTeamColor(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, False, TeamColor.ToArgb)
                m_objclsGameDetails.VisitorTeamColor = TeamColor
                frmMain.picAwayColorTop.BackColor = m_objGameDetails.VisitorTeamColor
                frmMain.picAwayColorBottom.BackColor = m_objGameDetails.VisitorTeamColor
                For Each obj As Control In pnlVisit.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
                For Each obj As Control In pnlVisitBench.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
                For Each obj As Control In pnlVisitPrimary.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'lineup correction through modules
    'when a player is corrected/chnaged in the lineup of module1, the player data for module3 should also be corrected,all 
    'the data allocated to the wrong player should be given to the correct player.
    Private Function IsLineupsModified() As Boolean
        Try
            Dim DsExistingPlayers As New DataSet
            Dim drsExisting() As DataRow
            Dim drsNew() As DataRow
            Dim blnlineupsChanged As Boolean = False
            Dim dsHomeTeam As DataSet
            Dim dsAwayTeam As DataSet

            If Not m_StartingLineups Is Nothing Then
                DsExistingPlayers = m_StartingLineups

                dsHomeTeam = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
                dsAwayTeam = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)

                If dsHomeTeam.Tables(0).Rows.Count > 0 And dsAwayTeam.Tables(0).Rows.Count > 0 Then
                    Dim DsRostersInfo As New DataSet
                    dsHomeTeam.Tables(0).TableName = "Home"
                    dsAwayTeam.Tables(0).TableName = "Away"

                    DsRostersInfo.Tables.Add(dsHomeTeam.Tables(0).Copy)
                    DsRostersInfo.Tables.Add(dsAwayTeam.Tables(0).Copy)

                    If DsExistingPlayers.Tables.Count > 0 Then
                        For intTableCnt As Integer = 0 To DsExistingPlayers.Tables.Count - 1
                            If DsExistingPlayers.Tables(intTableCnt).Columns.Contains("Starting_Position") Then
                                drsExisting = DsExistingPlayers.Tables(intTableCnt).Select("Starting_Position is not null", "Starting_Position")
                                If drsExisting.Length > 0 Then
                                    'fetching new players which are currently used
                                    drsNew = DsRostersInfo.Tables(intTableCnt).Select("Starting_Position is not null", "Starting_Position")
                                    If drsNew.Length > 0 Then
                                        For intRowCount As Integer = 0 To drsExisting.Length - 1
                                            If CInt(intRowCount) > CInt(drsNew.Length - 1) Then
                                                'issue occured when players are deselected and set to blanks
                                                Continue For
                                            End If

                                            If CInt(drsExisting(intRowCount).Item("Starting_Position")) = CInt(drsNew(intRowCount).Item("Starting_position")) Then
                                                'if lineups are matching, then check for player match
                                                If CLng(drsExisting(intRowCount).Item("Player_id")) <> CLng(drsNew(intRowCount).Item("Player_id")) Then
                                                    If CInt(drsExisting(intRowCount).Item("Starting_Position")) < 12 Then
                                                        '-- check in touches whether the player exists 
                                                        Dim intPlayerCount As Integer = m_objclsTouches.GetplayerCountInTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CInt(drsExisting(intRowCount).Item("Player_id")))
                                                        If intPlayerCount = 0 Then
                                                        Else
                                                            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                                                                'if player played any events, he should be updated with the new player
                                                                m_objclsTouches.UpdateExisPBPPlayerWithNewPlayer(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.ReporterRole, CInt(drsExisting(intRowCount).Item("Player_id")), CInt(drsNew(intRowCount).Item("Player_id")))
                                                                DisplayTouchesData(0)
                                                            End If
                                                        End If
                                                        blnlineupsChanged = True
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If


            If blnlineupsChanged Then
                m_StartingLineups.Tables.Clear()
                m_StartingLineups.Tables.Add(dsHomeTeam.Tables(0).Copy)
                m_StartingLineups.Tables.Add(dsAwayTeam.Tables(0).Copy)
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function IsRefreshNeeded() As Boolean
        Try
            Dim drs() As DataRow
            Dim dsHome As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
            Dim dsAway As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
            Dim blnChkTeam As Boolean = False


            If dsHome IsNot Nothing Or dsAway IsNot Nothing Then
                If dsHome.Tables.Count > 0 Or dsAway.Tables.Count > 0 Then
                    If dsHome.Tables(0).Rows.Count > 0 Or dsAway.Tables(0).Rows.Count > 0 Then
                        blnChkTeam = True
                        m_blnHomeLineUp = True
                        m_blnVisitLineUp = True
                        pnlHome.AutoScroll = False
                        pnlVisit.AutoScroll = False
                        pnlHome.Height = m_intPlayerPanelHeightShort
                        pnlVisit.Height = m_intPlayerPanelHeightShort
                        If bDisplayPlayers = True Then
                            pnlHomeBench.Visible = True
                            pnlVisitBench.Visible = True
                        End If
                    End If
                Else
                    pnlHome.AutoScroll = True
                    pnlVisit.AutoScroll = True
                    pnlHome.Height = m_intPlayerPanelHeightTall
                    pnlVisit.Height = m_intPlayerPanelHeightTall
                    pnlHomeBench.Visible = False
                    pnlVisitBench.Visible = False
                End If
            End If

            'roster comparison
            'Dim drs() As DataRow
            Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)


            If dsHomeTeam.Tables.Count > 0 Then
                If Not m_dtHome Is Nothing Then
                    If (dsHomeTeam.Tables(0).Rows.Count <> m_dtHome.Rows.Count) And (dsHomeTeam.Tables(0).Rows.Count > 0) Then
                        Return True
                    End If
                    If dsHomeTeam.Tables(0).Rows.Count > 0 Then
                        Dim drsHome() As DataRow = dsHomeTeam.Tables(0).Select("STARTING_POSITION > 11")
                        If drsHome.Length > 0 And btnHomeSub1.Text = "" Then
                            Return True
                        End If
                    End If
                End If

                If dsHomeTeam.Tables(0).Rows.Count > 0 Then
                    'TOSOCRS 214----------------------------------------
                    Dim DsSubEvents As DataSet = m_objGeneral.GetPBPData(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid)
                    If DsSubEvents.Tables(0).Rows.Count > 0 Then
                        For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                            Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                                Case 23
                                    Dim PrevEditUid As Integer = m_objclsGameDetails.StartingLineupsEditUid
                                    If (PrevEditUid = 0 And m_dtHome IsNot Nothing And m_dtAway IsNot Nothing And m_StartingLineups.Tables.Count = 0) Then
                                        m_StartingLineups.Merge(m_dtHome)
                                        m_StartingLineups.Merge(m_dtAway)
                                    End If
                                    m_objclsGameDetails.StartingLineupsEditUid = CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EDIT_UID"))
                                    If PrevEditUid <> m_objclsGameDetails.StartingLineupsEditUid Then
                                        If IsLineupsModified() Then
                                            Return True
                                        End If
                                    End If
                                    Exit For
                            End Select
                        Next
                    End If
                    '----------------------------------------
                    If Not m_dtHome Is Nothing Then
                        For i As Integer = 0 To dsHomeTeam.Tables(0).Rows.Count - 1
                            drs = m_dtHome.Select("PLAYER_ID = " & dsHomeTeam.Tables(0).Rows(i).Item("PLAYER_ID").ToString & "")
                            If drs.Length = 0 Then
                                Return True
                            Else
                                If drs(0).Item("DISPLAY_UNIFORM_NUMBER").ToString <> dsHomeTeam.Tables(0).Rows(i).Item("DISPLAY_UNIFORM_NUMBER").ToString Then
                                    Return True
                                End If
                            End If
                        Next
                    End If
                Else
                    Return False
                End If
            End If

            'Away TeamRoster
            Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
            If dsAwayTeam.Tables.Count > 0 Then
                If Not m_dtAway Is Nothing Then
                    If (dsAwayTeam.Tables(0).Rows.Count <> m_dtAway.Rows.Count) And (dsAwayTeam.Tables(0).Rows.Count > 0) Then
                        Return True
                    End If
                End If

                If dsAwayTeam.Tables(0).Rows.Count > 0 Then
                    Dim drsaway() As DataRow = dsAwayTeam.Tables(0).Select("STARTING_POSITION > 11")
                    If drsaway.Length > 0 And btnVisitSub1.Text = "" Then
                        Return True
                    End If
                End If

                If dsAwayTeam.Tables(0).Rows.Count > 0 Then
                    If Not m_dtAway Is Nothing Then
                        For i As Integer = 0 To dsAwayTeam.Tables(0).Rows.Count - 1
                            drs = m_dtAway.Select("PLAYER_ID = " & CInt(dsAwayTeam.Tables(0).Rows(i).Item("PLAYER_ID")) & "")
                            If drs.Length = 0 Then
                                Return True
                            Else
                                If drs(0).Item("DISPLAY_UNIFORM_NUMBER").ToString <> dsAwayTeam.Tables(0).Rows(i).Item("DISPLAY_UNIFORM_NUMBER").ToString Then
                                    Return True
                                End If
                            End If
                        Next
                    End If
                Else
                    Return False
                End If
            End If

            'Check for PBP events
            Dim m_dsEvents As DataSet = m_objGeneral.RefreshPlayers(intPrevUniqueID, m_objclsGameDetails.GameCode)
            If m_dsEvents.Tables(0).Rows.Count > 0 Then
                If m_dsEvents.Tables(1).Rows.Count > 0 Then
                    intPrevUniqueID = CInt(m_dsEvents.Tables(1).Rows(0).Item("UNIQUE_ID"))
                End If
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function CheckIfBenchPlayerIsSubbed(ByVal PlayerId As Integer) As Boolean
        Try
            ''7904 Issue #5 June 09 Ignore Subs for Reporters just show the starting lineups..
            ''Getting the current players from red card and sub events
            Dim DsSubEvents As DataSet = m_objGeneral.FetchSubsforCommAndTouches(m_objclsGameDetails.GameCode)
            If DsSubEvents.Tables(0).Rows.Count > 0 Then
                For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                    Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                        Case 7, 22
                            'SUBSTITUTION
                            If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) = PlayerId Then
                                Return True
                            End If
                    End Select
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GetCurrentPlayersAfterRestart()
        Try
            If IsRefreshNeeded() = True Then
                BindTeamRostersByLineUp()
                SortTeamPlayers()

                Dim DsCurrentPlayer As New DataSet
                DsCurrentPlayer.Merge(m_dtHome)
                DsCurrentPlayer.Merge(m_dtAway)

                ''7904 Issue #5 June 09 Ignore Subs for Reporters just show the starting lineups..
                ''Getting the current players from red card and sub events
                Dim DsSubEvents As DataSet = m_objGeneral.FetchSubsforCommAndTouches(m_objclsGameDetails.GameCode)
                If DsSubEvents.Tables(0).Rows.Count > 0 Then
                    For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                        Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                            Case 7, 22
                                For intTableCount As Integer = 0 To DsCurrentPlayer.Tables.Count - 1
                                    Dim DrPlayerOut() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) & "")
                                    If DrPlayerOut.Length > 0 Then
                                        If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = 7 Then
                                            'RED CARD
                                            DrPlayerOut(0).BeginEdit()
                                            DrPlayerOut(0).Item("STARTING_POSITION") = 99
                                            DrPlayerOut(0).EndEdit()
                                            DrPlayerOut(0).AcceptChanges()
                                        Else
                                            'SUBSTITUTION
                                            Dim playerOut_stPos As Integer
                                            Dim DrPlayerIn() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) & "")
                                            If DrPlayerIn.Length > 0 Then
                                                DrPlayerIn(0).BeginEdit()
                                                playerOut_stPos = CInt(DrPlayerIn(0).Item("STARTING_POSITION"))
                                                DrPlayerIn(0).Item("STARTING_POSITION") = DrPlayerOut(0).Item("STARTING_POSITION")
                                                DrPlayerIn(0).EndEdit()

                                                DrPlayerOut(0).BeginEdit()
                                                'DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                                DrPlayerOut(0).Item("STARTING_POSITION") = playerOut_stPos
                                                DrPlayerOut(0).EndEdit()

                                                DrPlayerIn(0).AcceptChanges()
                                            End If
                                        End If
                                    End If
                                Next

                        End Select
                    Next
                End If
                DsCurrentPlayer.AcceptChanges()
                ButtonsHomeTeamTagged(DsCurrentPlayer.Tables("Home"), True)
                ButtonsVisitTeamTagged(DsCurrentPlayer.Tables("Away"), True)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Function GetGoalKeeperID(ByVal TeamID As Integer) As Integer
        Try
            Dim intGoalKeeperID As Integer = -1

            Dim dsEvent As DataSet = m_objclsTouches.GetGoalieChangeEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, TeamID)
            If dsEvent.Tables(0).Rows.Count > 0 Then
                intGoalKeeperID = CInt(dsEvent.Tables(0).Rows(0).Item("OFFENSIVE_PLAYER_ID"))
            End If

            If intGoalKeeperID = -1 Then
                Dim dr() As DataRow
                If m_dtHome.Columns.Contains("STARTING_POSITION") Then
                    If TeamID = m_objGameDetails.HomeTeamID Then
                        dr = m_dtHome.Select("TEAM_ID = " & TeamID & "AND POSITION_ABBREV ='GK' AND  STARTING_POSITION <> 0 AND  STARTING_POSITION < 12")
                    Else
                        dr = m_dtAway.Select("TEAM_ID = " & TeamID & "AND POSITION_ABBREV ='GK' AND  STARTING_POSITION <> 0 AND  STARTING_POSITION < 12")
                    End If
                    If dr.Length > 0 Then
                        intGoalKeeperID = CInt(dr(0).Item("PLAYER_ID"))
                    End If
                End If
            End If
            Return intGoalKeeperID
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub SortTeamPlayers()
        Try
            If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", True)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                If m_dtHome IsNot Nothing Then 'SORTING OF LINUPS
                    If m_dtHome.Rows.Count > 0 Then
                        If Not m_dtHome.Columns.Contains("SORT_NUMBER") Then
                            Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                            dcHome.DataType = System.Type.GetType("System.Int32")
                            m_dtHome.Columns.Add(dcHome)
                            For Each drHome As DataRow In m_dtHome.Rows
                                If Not IsDBNull(drHome.Item("DISPLAY_UNIFORM_NUMBER")) Then
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Else
                                    drHome.Item("DISPLAY_UNIFORM_NUMBER") = 0
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                End If
                            Next
                        End If
                        Dim dvHome As New DataView(m_dtHome)
                        Dim dtSortHome As New DataTable
                        dtSortHome = m_dtHome.Clone
                        If m_blnHomeLineUp = True Then
                            Dim drArrHome As DataRow() = m_dtHome.Select("STARTING_POSITION<=11", "SORT_NUMBER ASC")
                            For Each drSortHome As DataRow In drArrHome
                                dtSortHome.ImportRow(drSortHome)
                            Next
                            Dim drArrHomeBench As DataRow() = m_dtHome.Select("STARTING_POSITION>11", "SORT_NUMBER ASC")
                            For Each drSortHomeBench As DataRow In drArrHomeBench
                                dtSortHome.ImportRow(drSortHomeBench)
                            Next
                            ButtonsHomeTeamTagged(dtSortHome, True)
                        Else
                            dvHome.Sort = "SORT_NUMBER ASC"
                            dtSortHome = dvHome.ToTable()
                            ButtonsHomeTeamTagged(dtSortHome, False)
                        End If

                        'If dsLineUp IsNot Nothing Then
                        '    ButtonsHomeTeamTagged(dtSortHome, True)
                        'Else
                        '    ButtonsHomeTeamTagged(dtSortHome, False)
                        'End If

                    End If

                Else ' SORTING OF ROSTERS - SHRAVANI
                    If Not m_dsHomeTeam Is Nothing Then
                        If m_dsHomeTeam.Tables("Home").Rows.Count > 0 Then
                            If Not m_dsHomeTeam.Tables("Home").Columns.Contains("SORT_NUMBER") Then
                                Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                                dcHome.DataType = System.Type.GetType("System.Int32")
                                m_dsHomeTeam.Tables("Home").Columns.Add(dcHome)
                                For Each drHome As DataRow In m_dsHomeTeam.Tables("Home").Rows
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Next
                            End If
                            Dim dvHome As New DataView(m_dsHomeTeam.Tables("Home"))
                            Dim dtSortHome As New DataTable
                            dtSortHome = m_dsHomeTeam.Tables("Home").Clone
                            If m_blnHomeLineUp = False Then
                                dvHome.Sort = "SORT_NUMBER ASC"
                                dtSortHome = dvHome.ToTable()
                            End If

                            ButtonsHomeTeamTagged(dtSortHome, False)
                        End If
                    End If
                End If

                'AWAY UNIFORM SORING
                If m_dtAway IsNot Nothing Then
                    If m_dtAway.Rows.Count > 0 Then
                        If Not m_dtAway.Columns.Contains("SORT_NUMBER") Then
                            Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                            dcAway.DataType = System.Type.GetType("System.Int32")
                            m_dtAway.Columns.Add(dcAway)
                            For Each drAway As DataRow In m_dtAway.Rows
                                If Not IsDBNull(drAway.Item("DISPLAY_UNIFORM_NUMBER")) Then
                                    If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drAway.Item("SORT_NUMBER") = 30000
                                    Else
                                        drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Else
                                    drAway.Item("DISPLAY_UNIFORM_NUMBER") = 0
                                    If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drAway.Item("SORT_NUMBER") = 30000
                                    Else
                                        drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                End If
                            Next
                        End If
                        Dim dvAway As New DataView(m_dtAway)
                        Dim dtSortAway As New DataTable
                        dtSortAway = m_dtAway.Clone
                        If m_blnVisitLineUp = True Then
                            Dim drArrAway As DataRow() = m_dtAway.Select("STARTING_POSITION<=11", "SORT_NUMBER ASC")
                            For Each drSortAway As DataRow In drArrAway
                                dtSortAway.ImportRow(drSortAway)
                            Next
                            Dim drArrAwayBench As DataRow() = m_dtAway.Select("STARTING_POSITION>11", "SORT_NUMBER ASC")
                            For Each drSortAwayBench As DataRow In drArrAwayBench
                                dtSortAway.ImportRow(drSortAwayBench)
                            Next
                            ButtonsVisitTeamTagged(dtSortAway, True)
                        Else
                            dvAway.Sort = "SORT_NUMBER ASC"
                            dtSortAway = dvAway.ToTable()
                            ButtonsVisitTeamTagged(dtSortAway, False)
                        End If

                        'If dsLineUp IsNot Nothing Then
                        '    ButtonsVisitTeamTagged(dtSortAway, True)
                        'Else
                        '    ButtonsVisitTeamTagged(dtSortAway, False)
                        'End If

                    End If
                Else 'SORTING ROSTERS  - SHRAVANI

                    If m_dsAwayTeam IsNot Nothing Then
                        If m_dsAwayTeam.Tables("Away").Rows.Count > 0 Then
                            If Not m_dsAwayTeam.Tables("Away").Columns.Contains("SORT_NUMBER") Then
                                Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                                dcAway.DataType = System.Type.GetType("System.Int32")
                                m_dsAwayTeam.Tables("Away").Columns.Add(dcAway)
                                For Each drAway As DataRow In m_dsAwayTeam.Tables("Away").Rows
                                    If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drAway.Item("SORT_NUMBER") = 30000
                                    Else
                                        drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Next
                            End If
                            Dim dvAway As New DataView(m_dsAwayTeam.Tables("Away"))
                            Dim dtSortAway As New DataTable
                            dtSortAway = m_dsAwayTeam.Tables("Away").Clone
                            If m_blnVisitLineUp = False Then
                                dvAway.Sort = "SORT_NUMBER ASC"
                                dtSortAway = dvAway.ToTable()
                            End If
                            'ButtonsVisitTeamTagged(dtSortAway)
                            ButtonsVisitTeamTagged(dtSortAway, False)
                        End If
                    End If
                End If
            End If


            If m_objclsLoginDetails.strSortOrder = "Last Name" Then
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", True)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                If m_dtHome IsNot Nothing Then
                    If m_dtHome.Rows.Count > 0 Then
                        If Not m_dtHome.Columns.Contains("SORT_NUMBER") Then
                            Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                            dcHome.DataType = System.Type.GetType("System.Int32")
                            m_dtHome.Columns.Add(dcHome)
                            For Each drHome As DataRow In m_dtHome.Rows
                                If Not IsDBNull(drHome.Item("DISPLAY_UNIFORM_NUMBER")) Then
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Else
                                    drHome.Item("DISPLAY_UNIFORM_NUMBER") = 0
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                End If
                            Next
                        End If
                        Dim dvHome As New DataView(m_dtHome)
                        Dim dtSortHome As New DataTable
                        dtSortHome = m_dtHome.Clone
                        If m_blnHomeLineUp = True Then
                            Dim drArrHome As DataRow() = m_dtHome.Select("STARTING_POSITION<=11", "LAST_NAME ASC")
                            For Each drSortHome As DataRow In drArrHome
                                dtSortHome.ImportRow(drSortHome)
                            Next
                            Dim drArrHomeBench As DataRow() = m_dtHome.Select("STARTING_POSITION>11", "LAST_NAME ASC")
                            For Each drSortHomeBench As DataRow In drArrHomeBench
                                dtSortHome.ImportRow(drSortHomeBench)
                            Next
                            ButtonsHomeTeamTagged(dtSortHome, True)
                        Else
                            dvHome.Sort = "LAST_NAME ASC"
                            dtSortHome = dvHome.ToTable()
                            ButtonsHomeTeamTagged(dtSortHome, False)
                        End If
                        'If dsLineUp IsNot Nothing Then
                        '    ButtonsHomeTeamTagged(dtSortHome, True)
                        'Else
                        '    ButtonsHomeTeamTagged(dtSortHome, False)
                        'End If
                    End If
                Else ' SORING OF ROSTERS 
                    If m_dsHomeTeam IsNot Nothing Then
                        If m_dsHomeTeam.Tables("Home").Rows.Count > 0 Then
                            If Not m_dsHomeTeam.Tables("Home").Columns.Contains("SORT_NUMBER") Then
                                Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                                dcHome.DataType = System.Type.GetType("System.Int32")
                                m_dsHomeTeam.Tables("Home").Columns.Add(dcHome)
                                For Each drHome As DataRow In m_dsHomeTeam.Tables("Home").Rows
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Next
                            End If
                            Dim dvHome As New DataView(m_dsHomeTeam.Tables("Home"))
                            Dim dtSortHome As New DataTable
                            dtSortHome = m_dsHomeTeam.Tables("Home").Clone
                            If m_blnHomeLineUp = False Then
                                dvHome.Sort = "LAST_NAME ASC"
                                dtSortHome = dvHome.ToTable()
                            End If
                            'ButtonsHomeTeamTagged(dtSortHome)
                            ButtonsHomeTeamTagged(dtSortHome, False)
                        End If
                    End If
                End If

                If m_dtAway IsNot Nothing Then
                    If m_dtAway.Rows.Count > 0 Then 'AWAY LINUPS SORTING
                        If Not m_dtAway.Columns.Contains("SORT_NUMBER") Then
                            Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                            dcAway.DataType = System.Type.GetType("System.Int32")
                            m_dtAway.Columns.Add(dcAway)
                            For Each drAway As DataRow In m_dtAway.Rows
                                If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drAway.Item("SORT_NUMBER") = 30000
                                Else
                                    drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvAway As New DataView(m_dtAway)
                        Dim dtSortAway As New DataTable
                        dtSortAway = m_dtAway.Clone
                        If m_blnVisitLineUp = True Then
                            Dim drArrAway As DataRow() = m_dtAway.Select("STARTING_POSITION<=11", "LAST_NAME ASC")
                            For Each drSortAway As DataRow In drArrAway
                                dtSortAway.ImportRow(drSortAway)
                            Next
                            Dim drArrAwayBench As DataRow() = m_dtAway.Select("STARTING_POSITION>11", "LAST_NAME ASC")
                            For Each drSortAwayBench As DataRow In drArrAwayBench
                                dtSortAway.ImportRow(drSortAwayBench)
                            Next
                            ButtonsVisitTeamTagged(dtSortAway, True)
                        Else
                            dvAway.Sort = "LAST_NAME ASC"
                            dtSortAway = dvAway.ToTable()
                            ButtonsVisitTeamTagged(dtSortAway, False)
                        End If
                        'If dsLineUp IsNot Nothing Then
                        '    ButtonsVisitTeamTagged(dtSortAway, True)
                        'Else
                        '    ButtonsVisitTeamTagged(dtSortAway, False)
                        'End If
                    End If
                Else 'AWAY ROSTERS SORTING
                    If m_dsAwayTeam IsNot Nothing Then
                        If m_dsAwayTeam.Tables("Away").Rows.Count > 0 Then
                            If Not m_dsAwayTeam.Tables("Away").Columns.Contains("SORT_NUMBER") Then
                                Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                                dcAway.DataType = System.Type.GetType("System.Int32")
                                m_dsAwayTeam.Tables("Away").Columns.Add(dcAway)
                                For Each drAway As DataRow In m_dsAwayTeam.Tables("Away").Rows
                                    If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drAway.Item("SORT_NUMBER") = 30000
                                    Else
                                        drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Next
                            End If
                            Dim dvAway As New DataView(m_dsAwayTeam.Tables("Away"))
                            Dim dtSortAway As New DataTable
                            dtSortAway = m_dsAwayTeam.Tables("Away").Clone
                            If m_blnVisitLineUp = False Then
                                dvAway.Sort = "LAST_NAME ASC"
                                dtSortAway = dvAway.ToTable()
                            End If
                            'ButtonsVisitTeamTagged(dtSortAway)
                            ButtonsVisitTeamTagged(dtSortAway, False)
                        End If
                    End If
                End If
            End If
            If m_objclsLoginDetails.strSortOrder = "Position" Then
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", True)
                If m_dtHome IsNot Nothing Then
                    If m_dtHome.Rows.Count > 0 Then
                        If Not m_dtHome.Columns.Contains("SORT_NUMBER") Then
                            Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                            dcHome.DataType = System.Type.GetType("System.Int32")
                            m_dtHome.Columns.Add(dcHome)
                            For Each drHome As DataRow In m_dtHome.Rows
                                If Not IsDBNull(drHome.Item("DISPLAY_UNIFORM_NUMBER")) Then
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Else
                                    drHome.Item("DISPLAY_UNIFORM_NUMBER") = 0
                                    If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drHome.Item("SORT_NUMBER") = 30000
                                    Else
                                        drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                End If
                            Next
                        End If
                        Dim dvHome As New DataView(m_dtHome)
                        Dim dtSortHome As New DataTable
                        dtSortHome = m_dtHome.Clone
                        If m_blnHomeLineUp = True Then
                            Dim drArrHome As DataRow() = m_dtHome.Select("STARTING_POSITION<=11", "STARTING_POSITION ASC")
                            For Each drSortHome As DataRow In drArrHome
                                dtSortHome.ImportRow(drSortHome)
                            Next
                            Dim drArrHomeBench As DataRow() = m_dtHome.Select("STARTING_POSITION>11", "STARTING_POSITION ASC")
                            For Each drSortHomeBench As DataRow In drArrHomeBench
                                dtSortHome.ImportRow(drSortHomeBench)
                            Next
                            ButtonsHomeTeamTagged(dtSortHome, True)
                        Else
                            dvHome.Sort = "DISPLAY_UNIFORM_NUMBER ASC"
                            dtSortHome = dvHome.ToTable()
                            ButtonsHomeTeamTagged(dtSortHome, False)
                        End If
                     
                    End If
                End If
                If m_dtAway IsNot Nothing Then
                    If m_dtAway.Rows.Count > 0 Then
                        If Not m_dtAway.Columns.Contains("SORT_NUMBER") Then
                            Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                            dcAway.DataType = System.Type.GetType("System.Int32")
                            m_dtAway.Columns.Add(dcAway)
                            For Each drAway As DataRow In m_dtAway.Rows
                                If Not IsDBNull(drAway.Item("DISPLAY_UNIFORM_NUMBER")) Then
                                    If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drAway.Item("SORT_NUMBER") = 30000
                                    Else
                                        drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                Else
                                    drAway.Item("DISPLAY_UNIFORM_NUMBER") = 0
                                    If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                        drAway.Item("SORT_NUMBER") = 30000
                                    Else
                                        drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                    End If
                                End If
                            Next
                        End If
                        Dim dvAway As New DataView(m_dtAway)
                        Dim dtSortAway As New DataTable
                        dtSortAway = m_dtAway.Clone
                        If m_blnVisitLineUp = True Then
                            Dim drArrAway As DataRow() = m_dtAway.Select("STARTING_POSITION<=11", "STARTING_POSITION ASC")
                            For Each drSortAway As DataRow In drArrAway
                                dtSortAway.ImportRow(drSortAway)
                            Next
                            Dim drArrAwayBench As DataRow() = m_dtAway.Select("STARTING_POSITION>11", "STARTING_POSITION ASC")
                            For Each drSortAwayBench As DataRow In drArrAwayBench
                                dtSortAway.ImportRow(drSortAwayBench)
                            Next
                            ButtonsVisitTeamTagged(dtSortAway, True)
                        Else
                            dvAway.Sort = "DISPLAY_UNIFORM_NUMBER ASC"
                            dtSortAway = dvAway.ToTable()
                            ButtonsVisitTeamTagged(dtSortAway, False)
                        End If
                       
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' T1 Workflow- Restart existing game
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Sub RestartExistingGame()
        Try
            If Not m_objclsTeamSetup.RosterInfo Is Nothing Then
                m_objclsTeamSetup.RosterInfo.Tables.Clear()
            End If

            BindTeamRostersByLineUp()
            If m_blnHomeLineUp = False And m_blnVisitLineUp = False Then
                BindTeamRosters()
            End If
            SortTeamPlayers()
            BindPBPData()
            GetCurrentPlayersAfterRestart()
            DisplayTouchesData(0)
            GetTouches(3)
            SetPeriod()
            frmMain.SetPeriod()
            frmMain.FillFormControls()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Refresh Game Data for Multi reporter 
    ''' </summary>;
    ''' <remarks></remarks>
    Public Sub RefreshGameData(ByVal refreshGameData As DataSet)
        Try
            Dim drs() As DataRow
            m_intRefresh = True
            'TOSOCRS-116
            Select Case m_objclsGameDetails.SerialNo
                Case 2
                    drs = refreshGameData.Tables(0).Select("UNIQUE_ID > 0 AND SEQUENCE_NUMBER = -1 AND ISNULL(TOUCH_TYPE_ID,99) IN (15)", "UNIQUE_ID")
                Case 3
                    drs = refreshGameData.Tables(0).Select("UNIQUE_ID > 0 AND SEQUENCE_NUMBER = -1 AND ISNULL(TOUCH_TYPE_ID,99) IN (15)", "UNIQUE_ID")
            End Select
            If Not drs Is Nothing Then
                If drs.Length > 0 Then
                    GetTouches(4)
                Else
                    GetTouches(2)
                End If
            Else
                GetTouches(2)
            End If
           
            'GetTouches(2)
            setTimeAfterRefresh(refreshGameData)
             
            'TOSOCRS-116
            'if records comes from that particular team, only then do a refresh...
            Select Case m_objclsGameDetails.SerialNo
                Case 2
                    drs = refreshGameData.Tables(0).Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND UNIQUE_ID > 0 AND  TEAM_ID = " & m_objclsGameDetails.HomeTeamID & " OR ISNULL(TOUCH_TYPE_ID,99) IN(14,15)", "SEQUENCE_NUMBER")
                Case 3
                    drs = refreshGameData.Tables(0).Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND UNIQUE_ID > 0 AND  TEAM_ID = " & m_objclsGameDetails.AwayTeamID & " OR ISNULL(TOUCH_TYPE_ID,99) IN(14,15)", "SEQUENCE_NUMBER")
            End Select

            'refresh only if new data comes for that TEAM
            If ChkAllData.Checked = True Then
                If drs.Length > 0 Then
                    DisplayAllTouchesData()
                End If
            ElseIf chkOwnTouches.Checked = True Then
                If drs.Length > 0 Then
                    displayOwnTouchesData()
                End If
            ElseIf chkRevisit.Checked = True Then
                If drs.Length > 0 Then
                    RevisitTouchesData()
                End If
            Else
                If (drs Is Nothing) And (m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1") Then
                    DisplayTouchesData(0)
                ElseIf (drs.Length > 0) Then
                    DisplayTouchesData(0)
                End If
            End If

            SetPeriod()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Sub DisplayTouches()
        Try
            DisplayTouchesData(0)
            'GetTouches()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function IsPBPEntryAllowed(Optional ByVal source As Integer = 0) As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objclsGameDetails.ReporterRole = "A1" Or m_objclsGameDetails.ReporterRole = "B1" Or m_objclsGameDetails.ReporterRole = "C1" Or m_objclsGameDetails.ReporterRole = "D1" Then
                Return True
            Else
                If source = 1 Then
                    If m_objclsGameDetails.IsPrimaryReporterDown Then
                        MessageDialog.Show(strmessage2 + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Return False
                        'ElseIf m_objclsGameDetails.AssistersLastEntryStatus = False Then
                        '    MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        '    Return False
                    Else
                        Return True
                    End If
                Else
                    Return True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

  ' Primary Reporter Down Alert TOSOCRS-45
    Private Function IsMyNetworkUP() As Boolean
        Try
            If My.Computer.Network.IsAvailable = False And (m_objGameDetails.ReporterRoleSerial <> 1) Then
                MessageDialog.Show(strmessage54, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                'If m_objclsGameDetails.SerialNo = 1 Then
                'btnClear_Click(Nothing, Nothing)
                'End If
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    ''' <summary>
    ''' To set period label for T1 workflow
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetPeriod()
        Try
            'TOSOCRS-117
            frmMain.SetClockControl(True)
            Dim DtHomeStart As DataSet
            DtHomeStart = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
            If DtHomeStart.Tables(0).Rows.Count > 0 Then
                If (m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 2) Then
                    If CStr(IIf(IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = True, "", DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH"))) <> "" Then

                        If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "L" Then
                            m_objGameDetails.IsHomeTeamOnLeft = True
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                m_objGameDetails.isHomeDirectionLeft = True
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                m_objGameDetails.isHomeDirectionLeft = False
                            End If
                        ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "R" Then
                            m_objGameDetails.IsHomeTeamOnLeft = False
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                m_objGameDetails.isHomeDirectionLeft = False
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                m_objGameDetails.isHomeDirectionLeft = True
                            End If
                        End If
                    Else
                        m_objGameDetails.IsHomeTeamOnLeft = True
                        If m_objGameDetails.CurrentPeriod = 1 Then
                            m_objGameDetails.isHomeDirectionLeft = True
                        ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                            m_objGameDetails.isHomeDirectionLeft = False
                        End If
                    End If
                End If

                If (m_objGameDetails.CurrentPeriod = 3 Or m_objGameDetails.CurrentPeriod = 4) Then
                    If (CStr(IIf(IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = True, " ", DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET"))) <> " ") Then
                        If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = "L" Then
                            m_objGameDetails.IsHomeTeamOnLeft = True
                            If m_objGameDetails.CurrentPeriod = 3 Then
                                m_objGameDetails.isHomeDirectionLeft = True
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                m_objGameDetails.isHomeDirectionLeft = False
                            End If
                        ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = "R" Then
                            m_objGameDetails.IsHomeTeamOnLeft = False
                            If m_objGameDetails.CurrentPeriod = 3 Then
                                m_objGameDetails.isHomeDirectionLeft = False
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                m_objGameDetails.isHomeDirectionLeft = True
                            End If
                        End If
                    Else
                        If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "L" Then
                            m_objGameDetails.IsHomeTeamOnLeft = True
                            m_objGameDetails.isHomeDirectionLeft = False
                        ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "R" Then
                            m_objGameDetails.IsHomeTeamOnLeft = False
                            m_objGameDetails.isHomeDirectionLeft = True
                        End If
                    End If
                End If
            End If
            UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub TmrTouches_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrTouches.Tick
        Try
            If m_objGameDetails.ModuleID = 3 Then
                TmrTouches.Enabled = False
                TmrTouches.Stop()
                BindPBPData()
                GetCurrentPlayersAfterRestart()
            End If

        Catch ex As Exception
            'Throw ex
            'we need to handle error here.
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Error, "ERROR OCCURED ON REFRESH(TmrTouches_Tick)" & ex.Message & vbNewLine, 1, 1)
        Finally
            TmrTouches.Enabled = True
            TmrTouches.Start()
        End Try
    End Sub


    Private Sub BindPBPData()
        Try
            Dim dsPBP As New DataSet
            Dim TeamID As Integer

            dsPBP = m_objclsTouches.GetPBPDataForTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.languageid)
            If dsPBP IsNot Nothing Then
                dsPBP.DataSetName = "PBP"
                If dsPBP.Tables(0).Rows.Count > 0 Then
                    dsPBP.Tables(0).TableName = "PBPData"
                    If dsPBP.Tables("PBPData").Rows.Count > 0 Then

                        If dsPBP.Tables("PBPData").Rows(0).Item("TEAM_ID") IsNot DBNull.Value Then
                            TeamID = CInt(dsPBP.Tables("PBPData").Rows(0).Item("TEAM_ID"))
                            If TeamID = m_objclsGameDetails.HomeTeamID Then
                                m_objclsGameDetails.HomeScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE"))
                                m_objclsGameDetails.AwayScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE"))

                                frmMain.lblScoreHome.Text = dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE").ToString()
                                frmMain.lblScoreAway.Text = dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE").ToString()
                            Else
                                m_objclsGameDetails.HomeScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE"))
                                m_objclsGameDetails.AwayScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE"))

                                frmMain.lblScoreAway.Text = dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE").ToString()
                                frmMain.lblScoreHome.Text = dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE").ToString()

                            End If

                        End If
                    Else
                        frmMain.lblScoreAway.Text = "0"
                        frmMain.lblScoreHome.Text = "0"
                    End If
                End If
            End If

            dsPBP.Dispose()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function IsEditTimeAllowed(ByVal CurrTime As Integer) As Boolean
        Try
            Dim dsEditPrevData As New DataSet
            Dim dsEditNextData As New DataSet
            Dim PrevElapsedTime As Integer
            Dim NextElapsedTime As Integer
            Dim EditPeriodPrev As Integer
            Dim EditPeriodNext As Integer = 0
            Dim SeqNumber As Decimal
            Dim drPBPEditData As DataRow
            Dim StrPrevTime As String = "00:00"
            Dim StrNextTime As String = "00:00"


            'drPBPEditData = m_dsTempPBP.Tables(0).Rows(0)
            'SeqNumber = CDec(drPBPEditData("SEQUENCE_NUMBER"))

            dsEditPrevData = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "PREV")
            If dsEditPrevData.Tables(0).Rows.Count <> 0 Then
                PrevElapsedTime = CInt(dsEditPrevData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                EditPeriodPrev = CInt(dsEditPrevData.Tables(0).Rows(0).Item("PERIOD"))
                StrPrevTime = clsUtility.ConvertSecondToMinute(CDbl(PrevElapsedTime.ToString), False)

            End If

            dsEditNextData = m_objclsTouches.GetTouchesRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "NEXT")
            If dsEditNextData.Tables(0).Rows.Count > 0 Then
                NextElapsedTime = CInt(dsEditNextData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                EditPeriodNext = CInt(dsEditNextData.Tables(0).Rows(0).Item("PERIOD"))
            Else
                NextElapsedTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                EditPeriodNext = m_objclsGameDetails.CurrentPeriod
            End If
            StrNextTime = clsUtility.ConvertSecondToMinute(CDbl(NextElapsedTime.ToString), False)

            If m_objclsGameDetails.IsDefaultGameClock Then
                If m_CurrentPeriod = 2 Then
                    If CurrTime < FIRSTHALF Then
                        MessageDialog.Show(strmessage15, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Return False
                    End If
                ElseIf m_CurrentPeriod = 3 Then
                    If CurrTime < SECONDHALF Then
                        MessageDialog.Show(strmessage14, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Return False
                    End If
                ElseIf m_CurrentPeriod = 4 Then
                    If CurrTime < FIRSTEXTRA Then
                        MessageDialog.Show(strmessage13, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    End If
                End If
                Dim StrTime As String
                StrTime = CalculateTimeElapseAfter(PrevElapsedTime, EditPeriodPrev)
                PrevElapsedTime = CInt(m_objUtility.ConvertMinuteToSecond(StrTime))

                StrTime = CalculateTimeElapseAfter(NextElapsedTime, EditPeriodNext)
                NextElapsedTime = CInt(m_objUtility.ConvertMinuteToSecond(StrTime))
            End If

            'If (CurrTime > NextElapsedTime Or PrevElapsedTime > CurrTime) And EditPeriodPrev = EditPeriodNext Then
            '    essageDialog.Show("Select time between " & StrNextTime & " and " & StrPrevTime & " !", "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)

            '    Return False
            'End If

            Return True

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub TouchHotkeys(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            Dim blnControl As Boolean = True
            If Me.ActiveControl IsNot Nothing Then
                If Me.ActiveControl.Name = "txtTime" Then
                    blnControl = False
                End If
            End If
            If blnControl Then
                'TOSOCRS-35 Touch Module - Add warning Message if Reporter 2 or 3 are changing duels
                Select Case CInt(m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1))
                    Case 2, 3
                        Select Case e.KeyCode
                            Case Keys.A, Keys.D, Keys.B, Keys.I, Keys.T, Keys.S, Keys.C, Keys.U, Keys.K, Keys.L, Keys.R, Keys.W, Keys.Z, Keys.E, Keys.F, Keys.G
                                Select Case m_intTouchTypeID
                                    Case 18, 19, 20, 21
                                        If MessageDialog.Show("You are about to change " + lblValType.Text + " touch type. Are you sure ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.No Then
                                            Exit Sub
                                        End If
                                End Select
                            Case Else
                                'TOSOCRS-310 START
                                If m_intTouchTypeID <> -1 And (m_intTouchTypeID <> 18 And m_intTouchTypeID <> 19 And m_intTouchTypeID <> 20 And m_intTouchTypeID <> 21) Then
                                    Select Case e.KeyCode
                                        Case Keys.M
                                            If MessageDialog.Show("You are about to change " + lblValType.Text + " touch type to 50-Win. Are you sure ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.No Then
                                                Exit Sub
                                            End If
                                        Case Keys.P
                                            If MessageDialog.Show("You are about to change " + lblValType.Text + " touch type Air Win. Are you sure ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.No Then
                                                Exit Sub
                                            End If
                                    End Select
                                End If
                             'TOSOCRS-310 END
                        End Select
                End Select
                m_blnChkTyping = True
                Dim prvTouchType As Integer = m_intTouchTypeID
                Dim sKey As String
                Select Case e.KeyCode
                    Case Keys.A
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeGoalAttempt.Text, 1, 0)

                        If btnTypeTouch.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeTouch.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Bad pass" 'Arindam Changed from Goal-Attemp to TOuch 'TOSOCRS-299 Touch changed to Bad pass
                            m_intTouchTypeID = 11
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeTouch.Name)
                    Case Keys.D 'Arindam Changed Pass frpm P to D
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypePass.Text, 1, 0)
                        If btnTypePass.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypePass.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Pass"
                            m_intTouchTypeID = 1
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypePass.Name)
                    Case Keys.I
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeInt.Text, 1, 0)
                        If btnTypeThrowIn.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeThrowIn.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Throw-In" 'Arindam Chnaged from Interception to Throw-In
                            m_intTouchTypeID = 8
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeThrowIn.Name)
                    Case Keys.B
                        If btnTypeBlock.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeBlock.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Block"
                            m_intTouchTypeID = 3
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeBlock.Name)
                    Case Keys.T
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeTouch.Text, 1, 0)
                        If btnTypeGoalAttempt.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeGoalAttempt.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Goal Attempt" 'Arindam Changed from Touch to Goal-Attempt
                            m_intTouchTypeID = 10
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeGoalAttempt.Name)
                    Case Keys.S
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeSave.Text, 1, 0)
                        If BtnIntPass.BackColor <> m_clrHighlightedButtonColor Then
                            BtnIntPass.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Int/Pass" 'Arindam changed from Save to Run-with-ball
                            m_intTouchTypeID = -99
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(BtnIntPass.Name)
                    Case Keys.C
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeCatch.Text, 1, 0)
                        If btnTypeCatch.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeCatch.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Catch"
                            m_intTouchTypeID = 6
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeCatch.Name)
                    Case Keys.U
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeOther.Text, 1, 0)
                        If btnTypeOther.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeOther.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Touch" 'TOSOCRS-299 UnKnown/Other changed to Touch
                            m_intTouchTypeID = 0
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeOther.Name)
                    Case Keys.K
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeTackle.Text, 1, 0)
                        If btnTypeTackle.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeTackle.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Tackle"
                            m_intTouchTypeID = 4
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeTackle.Name)
                    Case Keys.L
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeClearance.Text, 1, 0)
                        If btnTypeClearance.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeClearance.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Clearance"
                            m_intTouchTypeID = 9
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeClearance.Name)
                    Case Keys.R
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeRunWithBall.Text, 1, 0)
                        If btnTypeSave.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeSave.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Save" 'Arindam changed from Run-with-ball to Save
                            m_intTouchTypeID = 5
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeSave.Name)
                    Case Keys.W
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeThrowIn.Text, 1, 0)
                        If btnTypeInt.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeInt.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Interception" 'Arindam Chnaged from Throw-In to Interception 
                            m_intTouchTypeID = 2
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeInt.Name)
                    Case Keys.Z
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeClearance.Text, 1, 0)
                        If btnTypeRunWithBall.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeRunWithBall.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Run With Ball"
                            m_intTouchTypeID = 7
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeRunWithBall.Name)
                        ''Arindam Added new short cut keys 12-Apr-12
                    Case Keys.E
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnGKHand.Text, 1, 0)
                        If btnGKHand.BackColor <> m_clrHighlightedButtonColor Then
                            btnGKHand.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "GK Hand"
                            m_intTouchTypeID = 22
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnGKHand.Name)
                    Case Keys.F
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnGKThrow.Text, 1, 0)
                        If btnGKThrow.BackColor <> m_clrHighlightedButtonColor Then
                            btnGKThrow.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "GK Thrw"
                            m_intTouchTypeID = 23
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnGKThrow.Name)
                        ''
                    Case Keys.G
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnTypeGoalKick.Text, 1, 0)
                        If btnTypeGoalKick.BackColor <> m_clrHighlightedButtonColor Then
                            btnTypeGoalKick.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Goalkick / Keeper Throw"
                            m_intTouchTypeID = 12
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnTypeGoalKick.Name)
                    Case Keys.P 'TOSOCRS-310 
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btn50Win.Text, 1, 0)
                        If btn50Win.BackColor <> m_clrHighlightedButtonColor Then
                            btn50Win.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "50-Win"
                            m_intTouchTypeID = 20
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btn50Win.Name)
                    Case Keys.M 'TOSOCRS-310 
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAirWin.Text, 1, 0)
                        If btnAirWin.BackColor <> m_clrHighlightedButtonColor Then
                            btnAirWin.BackColor = m_clrHighlightedButtonColor
                            SetTime()
                            lblValType.Text = "Air Win"
                            m_intTouchTypeID = 18
                            CheckForSave()
                        End If
                        DisableotherEventbuttonColor(btnAirWin.Name)
                    Case Keys.Space
                        If m_objclsGameDetails.IsEditMode = False And (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                            MessageDialog.Show(strmessage27, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Exit Sub
                        End If

                        ' Primary Reporter Down Alert TOSOCRS-45
                        If IsMyNetworkUP() = False Then
                            Exit Sub
                        End If

                        If IsPBPEntryAllowed(1) = False Then
                            Exit Sub
                        End If

                        If CheckForData() = False Then
                            Exit Sub
                        End If


                        'If Not frmMain.UdcRunningClock1.URCIsRunning Then
                        '    essageDialog.Show("Please start the clock")
                        '    Exit Sub
                        'End If
                        If String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                            MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                        If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                            If String.IsNullOrEmpty(m_strPlayerID) Then
                                MessageDialog.Show(strmessage25, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Exit Sub
                            End If
                        End If


                        Dim chrTime(1) As Char
                        chrTime(1) = Convert.ToChar(":")
                        Dim strArrTime As String() = txtTime.Text.Split(chrTime)
                        If String.IsNullOrEmpty(strArrTime(0).Trim()) And String.IsNullOrEmpty(strArrTime(1).Trim()) Then
                            MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If


                        Dim chkRes As Boolean = True
                        chkRes = clsValidation.ValidateTime(txtTime.Text.Trim())
                        If chkRes Then
                            Dim chrSep(1) As Char
                            chrSep(1) = Convert.ToChar(":")
                            Dim strArrCurrentTime As String() = txtTime.Text.Split(chrSep)
                            chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, strArrCurrentTime(0).Trim(), 0, 150)
                        Else
                            MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                        If Not chkRes Then
                            MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                        'If m_objclsGameDetails.IsEditMode = False Then
                        If m_objclsGameDetails.SerialNo = 1 Then
                            chkRes = ValidateCurrentTime()
                        End If

                        'End If
                        If Not chkRes Then
                            Exit Sub
                        End If

                        'RM 7904 Recipient_Code
                        ''TOSOCRS-41/TOSOCRS-100 Recipients for new data points
                        If (m_intTouchTypeID = 1 Or m_intTouchTypeID = -99) And String.IsNullOrEmpty(m_strRecipientID) = True Then
                            MessageDialog.Show(strmessage33, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If

                        picMark1.Focus()
                        If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                            If compareTouchesData(m_intTouchTypeID, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def) Then
                                ClearTouchLabels()
                                'm_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                                m_decSequenceNo = "-1"
                                DisableOtherButtonColor(btnClear.Name)
                                DisableotherEventbuttonColor(btnClear.Name)
                                initializeTouchData()

                                If dgvTouches.SelectedRows.Count > 0 Then
                                    checkForNextTouchData(0)
                                    dgvTouches_DoubleClick(sender, e)
                                    If ChkAllData.Checked = True Then
                                        DisplayAllTouchesData()
                                    End If
                                    'm_blnSavestatus = True
                                End If
                            Else
                                SaveTouches()
                                initializeTouchData()

                                If dgvTouches.SelectedRows.Count > 0 Then
                                    checkForNextTouchData(1)
                                    dgvTouches_DoubleClick(sender, e)
                                    If ChkAllData.Checked = True Then
                                        DisplayAllTouchesData()
                                    End If
                                End If
                                'm_blnSavestatus = True
                            End If
                        Else
                            SaveTouches()
                            initializeTouchData()
                        End If


                    Case Keys.Back
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClear.Text, 1, 0)

                        ClearTouchLabels()
                        UdcSoccerField1.USFClearMarks()
                        lblSelection1.Text = "-"
                        frmMain.ChangeTheme(False)
                        Me.ChangeTheme(False)
                        m_objclsGameDetails.IsEditMode = False
                        m_CurrentPeriod = 0
                    Case Else
                        If (e.KeyValue >= Keys.D0 And e.KeyValue <= Keys.D9) Or _
                           (e.KeyValue >= Keys.NumPad0 And e.KeyValue <= Keys.NumPad9) Then

                            sKey = Replace(Replace(e.KeyCode.ToString, "D", ""), "NumPad", "")
                            If Len(lblValUniform.Text.Trim()) = 2 Then
                                lblValUniform.Text = sKey
                            Else
                                lblValUniform.Text = lblValUniform.Text.Trim() & sKey
                            End If

                        End If
                End Select
                'TOSOCRS-299 START- Main Touch hotkeys handled separately
                Select Case CInt(m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1))
                    Case 1
                        Select Case e.KeyCode
                            Case Keys.M
                                'AUDIT TRIAL
                                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, BtnIntPass.Text, 1, 0)
                                If BtnIntPass.BackColor <> m_clrHighlightedButtonColor Then
                                    BtnIntPass.BackColor = m_clrHighlightedButtonColor
                                    SetTime()
                                    lblValType.Text = "Air Win"
                                    m_intTouchTypeID = 18
                                    CheckForSave()
                                End If
                                DisableotherEventbuttonColor(BtnIntPass.Name)
                            Case Keys.P
                                'AUDIT TRIAL
                                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btn50Win.Text, 1, 0)
                                If btn50Win.BackColor <> m_clrHighlightedButtonColor Then
                                    btn50Win.BackColor = m_clrHighlightedButtonColor
                                    SetTime()
                                    lblValType.Text = "50-Win"
                                    m_intTouchTypeID = 20
                                    CheckForSave()
                                End If
                                DisableotherEventbuttonColor(btn50Win.Name)
                        End Select
                End Select
                'TOSOCRS-299 END

                'TOSOCRS-64 [Touch Software-Autofill GoalKeeper for GK Touches]
                If e.KeyCode <> Keys.Enter And e.KeyCode <> Keys.Back And e.KeyCode <> Keys.Space Then
                    Select Case m_intTouchTypeID
                        Case 5, 6, 12, 22, 23, 24
                            ClearPlayerSelection()
                            If (m_objGameDetails.SerialNo = 2) Then
                                For Each ctrl As Control In Me.pnlHome.Controls
                                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                                        If CInt(ctrl.Tag) = GetGoalKeeperID(m_objGameDetails.HomeTeamID) Then
                                            HomePlayers_Click(ctrl, e)
                                            Exit For
                                        End If
                                    End If
                                Next
                            ElseIf (m_objGameDetails.SerialNo = 3) Then
                                For Each ctrl As Control In Me.pnlVisit.Controls
                                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                                        If CInt(ctrl.Tag) = GetGoalKeeperID(m_objGameDetails.AwayTeamID) Then
                                            VisitPlayers_Click(ctrl, e)
                                            Exit For
                                        End If
                                    End If
                                Next
                            End If
                        Case Else
                            Select Case prvTouchType
                                Case 5, 6, 12, 22, 23, 24
                                    Select Case e.KeyCode
                                        Case Keys.A, Keys.D, Keys.B, Keys.I, Keys.T, Keys.S, Keys.U, Keys.K, Keys.L, Keys.W, Keys.Z
                                            ClearPlayerSelection()
                                            SelectedPreviousRecPlayer()
                                    End Select
                            End Select
                    End Select
                End If


            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region


    Private Sub txtTime_Validated1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTime.Validated
        Try
            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub Create_XML()

        Dim strXML As String = "<GRAMMAR>" & _
                                  "<RULE ID=""1"" Name=""number"" TOPLEVEL=""ACTIVE"">" & _
                                       "<L PROPNAME=""number"">" & _
                                           "<P VALSTR=""PASS"">pass</P>" & _
                                           "<P VALSTR=""BLOCK"">block</P>" & _
                                           "<P VALSTR=""INTERCEPTION"">interception</P>" & _
                                           "<P VALSTR=""OTHER"">other</P>" & _
                                           "<P VALSTR=""TACKLE"">tackle</P>" & _
                                           "<P VALSTR=""CATCH"">catch</P>" & _
                                           "<P VALSTR=""SAVE"">save</P>" & _
                                           "<P VALSTR=""HOME"">home</P>" & _
                                           "<P VALSTR=""AWAY"">away</P>" & _
                                           "<P VALSTR=""AWAY"">visitor</P>" & _
                                           "<P VALSTR=""ENTER"">enter</P>" & _
                                           "<P VAL=""0"">zero</P>" & _
                                           "<P VAL=""1"">one</P>" & _
                                           "<P VAL=""2"">two</P>" & _
                                           "<P VAL=""3"">three</P>" & _
                                           "<P VAL=""4"">four</P>" & _
                                           "<P VAL=""5"">five</P>" & _
                                           "<P VAL=""6"">six</P>" & _
                                           "<P VAL=""7"">seven</P>" & _
                                           "<P VAL=""8"">eight</P>" & _
                                           "<P VAL=""9"">nine</P>" & _
                                           "<P VAL=""10"">ten</P>" & _
                                           "<P VAL=""11"">eleven</P>" & _
                                           "<P VAL=""12"">twelve</P>" & _
                                           "<P VAL=""13"">thirteen</P>" & _
                                           "<P VAL=""14"">fourteen</P>" & _
                                           "<P VAL=""15"">fifteen</P>" & _
                                           "<P VAL=""16"">sixteen</P>" & _
                                           "<P VAL=""17"">seventeen</P>" & _
                                           "<P VAL=""18"">eighteen</P>" & _
                                           "<P VAL=""19"">nineteen</P>" & _
                                           "<P VAL=""20"">twenty</P>" & _
                                           "<P VAL=""21"">twenty one</P>" & _
                                           "<P VAL=""22"">twenty two</P>" & _
                                           "<P VAL=""23"">twenty three</P>" & _
                                           "<P VAL=""24"">twenty four</P>" & _
                                           "<P VAL=""25"">twenty five</P>" & _
                                           "<P VAL=""26"">twenty six</P>" & _
                                           "<P VAL=""27"">twenty seven</P>" & _
                                           "<P VAL=""28"">twenty eight</P>" & _
                                           "<P VAL=""29"">twenty nine</P>" & _
                                           "<P VAL=""30"">thirty</P>" & _
                                           "<P VAL=""31"">thirty one</P>" & _
                                           "<P VAL=""32"">thirty two</P>" & _
                                           "<P VAL=""33"">thirty three</P>" & _
                                           "<P VAL=""34"">thirty four</P>" & _
                                           "<P VAL=""35"">thirty five</P>" & _
                                           "<P VAL=""36"">thirty six</P>" & _
                                           "<P VAL=""37"">thirty seven</P>" & _
                                           "<P VAL=""38"">thirty eight</P>" & _
                                           "<P VAL=""39"">thirty nine</P>" & _
                                           "<P VAL=""40"">forty</P>" & _
                                           "<P VAL=""41"">forty one</P>" & _
                                           "<P VAL=""42"">forty two</P>" & _
                                           "<P VAL=""43"">forty three</P>" & _
                                           "<P VAL=""44"">forty four</P>" & _
                                           "<P VAL=""45"">forty five</P>" & _
                                           "<P VAL=""46"">forty six</P>" & _
                                           "<P VAL=""47"">forty seven</P>" & _
                                           "<P VAL=""48"">forty eight</P>" & _
                                           "<P VAL=""49"">forty nine</P>" & _
                                           "<P VAL=""50"">fifty</P>" & _
                                           "<P VAL=""51"">fifty one</P>" & _
                                           "<P VAL=""52"">fifty two</P>" & _
                                           "<P VAL=""53"">fifty three</P>" & _
                                           "<P VAL=""54"">fifty four</P>" & _
                                           "<P VAL=""55"">fifty five</P>" & _
                                           "<P VAL=""56"">fifty six</P>" & _
                                           "<P VAL=""57"">fifty seven</P>" & _
                                           "<P VAL=""58"">fifty eight</P>" & _
                                           "<P VAL=""59"">fifty nine</P>" & _
                                           "<P VAL=""60"">sixty</P>" & _
                                           "<P VAL=""61"">sixty one</P>" & _
                                           "<P VAL=""62"">sixty two</P>" & _
                                           "<P VAL=""63"">sixty three</P>" & _
                                           "<P VAL=""64"">sixty four</P>" & _
                                           "<P VAL=""65"">sixty five</P>" & _
                                           "<P VAL=""66"">sixty six</P>" & _
                                           "<P VAL=""67"">sixty seven</P>" & _
                                           "<P VAL=""68"">sixty eight</P>" & _
                                           "<P VAL=""69"">sixty nine</P>" & _
                                           "<P VAL=""70"">seventy</P>" & _
                                           "<P VAL=""71"">seventy one</P>" & _
                                           "<P VAL=""72"">seventy two</P>" & _
                                           "<P VAL=""73"">seventy three</P>" & _
                                           "<P VAL=""74"">seventy four</P>" & _
                                           "<P VAL=""75"">seventy five</P>" & _
                                           "<P VAL=""76"">seventy six</P>" & _
                                           "<P VAL=""77"">seventy seven</P>" & _
                                           "<P VAL=""78"">seventy eight</P>" & _
                                           "<P VAL=""79"">seventy nine</P>" & _
                                           "<P VAL=""80"">eighty</P>" & _
                                           "<P VAL=""81"">eighty one</P>" & _
                                           "<P VAL=""82"">eighty two</P>" & _
                                           "<P VAL=""83"">eighty three</P>" & _
                                           "<P VAL=""84"">eighty four</P>" & _
                                           "<P VAL=""85"">eighty five</P>" & _
                                           "<P VAL=""86"">eighty six</P>" & _
                                           "<P VAL=""87"">eighty seven</P>" & _
                                           "<P VAL=""88"">eighty eight</P>" & _
                                           "<P VAL=""89"">eighty nine</P>" & _
                                           "<P VAL=""90"">ninety</P>" & _
                                           "<P VAL=""91"">ninety one</P>" & _
                                           "<P VAL=""92"">ninety two</P>" & _
                                           "<P VAL=""93"">ninety three</P>" & _
                                           "<P VAL=""94"">ninety four</P>" & _
                                           "<P VAL=""95"">ninety five</P>" & _
                                           "<P VAL=""96"">ninety six</P>" & _
                                           "<P VAL=""97"">ninety seven</P>" & _
                                           "<P VAL=""98"">ninety eight</P>" & _
                                           "<P VAL=""99"">ninety nine</P>" & _
                                       "</L>" & _
                                   "</RULE>" & _
                               "</GRAMMAR>"

        xGrammarXML.LoadXml(strXML)

        Try
            xGrammarXML.Save(My.Application.Deployment.DataDirectory & "xmlGrammar.xml")
        Catch
            xGrammarXML.Save("C:\xmlGrammar.xml")
        End Try

    End Sub

    Private Sub txtCommand_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCommand.TextChanged

        Dim bSave As Boolean = False

        If Me.txtCommand.Text = "" Then
            Exit Sub
        End If

        If InStr(Me.txtCommand.Text, "HOME") > 0 Then
            sTouchTeam = m_objclsGameDetails.HomeTeam
            iTouchTeam = m_objclsGameDetails.HomeTeamID
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "HOME", "")
        End If

        If InStr(Me.txtCommand.Text, "AWAY") > 0 Then
            sTouchTeam = m_objclsGameDetails.AwayTeam
            iTouchTeam = m_objclsGameDetails.AwayTeamID
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "AWAY", "")
        End If

        If InStr(Me.txtCommand.Text, "VISITOR") > 0 Then
            sTouchTeam = m_objclsGameDetails.HomeTeam
            iTouchTeam = m_objclsGameDetails.HomeTeamID
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "VISITOR", "")
        End If

        If InStr(Me.txtCommand.Text, "PASS") > 0 Then
            sTouchType = "P"
            sTouchTypeLong = "Pass"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "PASS", "")
        End If

        If InStr(Me.txtCommand.Text, "INTERCEPTION") > 0 Then
            sTouchType = "I"
            sTouchTypeLong = "Interception"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "INTERCEPTION", "")
        End If

        If InStr(Me.txtCommand.Text, "BLOCK") > 0 Then
            sTouchType = "B"
            sTouchTypeLong = "Block"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "BLOCK", "")
        End If

        If InStr(Me.txtCommand.Text, "TACKLE") > 0 Then
            sTouchType = "T"
            sTouchTypeLong = "Tackle"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "TACKLE", "")
        End If

        If InStr(Me.txtCommand.Text, "SAVE") > 0 Then
            sTouchType = "S"
            sTouchTypeLong = "Save"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "SAVE", "")
        End If

        If InStr(Me.txtCommand.Text, "CATCH") > 0 Then
            sTouchType = "C"
            sTouchTypeLong = "Catch"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "CATCH", "")
        End If

        If InStr(Me.txtCommand.Text, "OTHER") > 0 Then
            sTouchType = "O"
            sTouchTypeLong = "Other"
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "OTHER", "")
        End If

        If InStr(Me.txtCommand.Text, "ENTER") > 0 Then
            bSave = True
            Me.txtCommand.Text = Replace(Me.txtCommand.Text, "ENTER", "")
            btnSave_Click(sender, e)
        End If

        If Me.txtCommand.Text <> "" Then
            If IsNumeric(Me.txtCommand.Text) Then
                If bTyping = True Then
                    If CInt(Me.txtCommand.Text) < 10 Then
                        If Len(sTouchUniform) = 2 Then
                            sTouchUniform = Me.txtCommand.Text
                            iTouchUniform = CInt(sTouchUniform)
                        Else
                            sTouchUniform = sTouchUniform & Me.txtCommand.Text
                            iTouchUniform = CInt(sTouchUniform)
                        End If
                    End If
                Else
                    sTouchUniform = Me.txtCommand.Text
                    iTouchUniform = CInt(sTouchUniform)
                End If
            End If
        End If

        Me.txtCommand.Text = ""

        If bSave = False Then
            lblValTeam.Text = sTouchTeam
            m_intTouchTeamID = iTouchTeam
            lblValUniform.Text = sTouchUniform
        End If

    End Sub


    Private Sub DisableOtherButtonColor(ByVal btnName As String)
        Dim clrHomeText As System.Drawing.Color = clsUtility.GetTextColor(m_objGameDetails.HomeTeamColor)
        Dim clrVisitText As System.Drawing.Color = clsUtility.GetTextColor(m_objGameDetails.VisitorTeamColor)
        Try
            For Each ctrl As Control In Me.pnlHome.Controls
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        CType(ctrl, Button).BackColor = m_objGameDetails.HomeTeamColor
                        CType(ctrl, Button).ForeColor = clrHomeText
                    End If
                End If
            Next
            For Each ctrl As Control In Me.pnlVisit.Controls
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        CType(ctrl, Button).BackColor = m_objGameDetails.VisitorTeamColor
                        CType(ctrl, Button).ForeColor = clrVisitText
                    End If
                End If
            Next
            For Each ctrl As Control In Me.pnlHomeBench.Controls
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        CType(ctrl, Button).BackColor = m_objGameDetails.HomeTeamColor
                        CType(ctrl, Button).ForeColor = clrHomeText
                    End If
                End If
            Next

            For Each ctrl As Control In Me.pnlVisitBench.Controls()
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        CType(ctrl, Button).BackColor = m_objGameDetails.VisitorTeamColor
                        CType(ctrl, Button).ForeColor = clrVisitText
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'RM 7904 - Recipient_Code

    Private Sub DisableRecipientButtonColor(ByVal btnName As String)
        Dim clrHomeText As System.Drawing.Color = clsUtility.GetTextColor(m_objGameDetails.HomeTeamColor)
        Dim clrVisitText As System.Drawing.Color = clsUtility.GetTextColor(m_objGameDetails.VisitorTeamColor)
        Try
            For Each ctrl As Control In Me.pnlHome.Controls
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        If CType(ctrl, Button).BackColor <> m_clrHighlightedButtonColor Then
                            CType(ctrl, Button).BackColor = m_objGameDetails.HomeTeamColor
                            CType(ctrl, Button).ForeColor = clrHomeText
                        End If

                    End If
                End If
            Next
            For Each ctrl As Control In Me.pnlVisit.Controls
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        If CType(ctrl, Button).BackColor <> m_clrHighlightedButtonColor Then
                            CType(ctrl, Button).BackColor = m_objGameDetails.VisitorTeamColor
                            CType(ctrl, Button).ForeColor = clrVisitText
                        End If

                    End If
                End If
            Next
            For Each ctrl As Control In Me.pnlHomeBench.Controls
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        If CType(ctrl, Button).BackColor <> m_clrHighlightedButtonColor Then
                            CType(ctrl, Button).BackColor = m_objGameDetails.HomeTeamColor
                            CType(ctrl, Button).ForeColor = clrHomeText
                        End If
                    End If
                End If
            Next

            For Each ctrl As Control In Me.pnlVisitBench.Controls()
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        If CType(ctrl, Button).BackColor <> m_clrHighlightedButtonColor Then
                            CType(ctrl, Button).BackColor = m_objGameDetails.VisitorTeamColor
                            CType(ctrl, Button).ForeColor = clrVisitText
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub ChangePlayerBtnBackColor(ByVal intUniformNo As String, ByVal TemaID As Integer)
        Try
            If TemaID = m_objGameDetails.HomeTeamID Then
                For Each ctrl As Control In Me.pnlHome.Controls
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If (ctrl.Tag.ToString() = intUniformNo And ctrl.Tag.ToString <> "") Then
                                CType(ctrl, Button).BackColor = m_clrHighlightedButtonColor
                                If m_objclsGameDetails.IsEditMode Then
                                    m_boolPlayer1Filled = True
                                End If
                            End If
                        End If
                    End If
                Next
                For Each ctrl As Control In Me.pnlHomeBench.Controls
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If (ctrl.Tag.ToString() = intUniformNo And ctrl.Tag.ToString <> "") Then
                                CType(ctrl, Button).BackColor = m_clrHighlightedButtonColor
                                If m_objclsGameDetails.IsEditMode Then
                                    m_boolPlayer1Filled = True
                                End If
                            End If
                        End If
                    End If
                Next
            Else
                For Each ctrl As Control In Me.pnlVisit.Controls
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If (ctrl.Tag.ToString() = intUniformNo And ctrl.Tag.ToString <> "") Then
                                CType(ctrl, Button).BackColor = m_clrHighlightedButtonColor
                                If m_objclsGameDetails.IsEditMode Then
                                    m_boolPlayer1Filled = True
                                End If
                            End If
                        End If
                    End If
                Next

                For Each ctrl As Control In Me.pnlVisitBench.Controls()
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If (ctrl.Tag.ToString() = intUniformNo And ctrl.Tag.ToString <> "") Then
                                CType(ctrl, Button).BackColor = m_clrHighlightedButtonColor
                                If m_objclsGameDetails.IsEditMode Then
                                    m_boolPlayer1Filled = True
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'RM 7904 - Recipient_Code
    Private Sub ChangeRecipientBtnBackColor(ByVal intUniformNo As String, ByVal TemaID As Integer)
        Try
            If TemaID = m_objGameDetails.HomeTeamID Then
                For Each ctrl As Control In Me.pnlHome.Controls
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If ctrl.Tag.ToString() = intUniformNo Then
                                CType(ctrl, Button).BackColor = m_clrRecipientButtonColor

                            End If
                        End If
                    End If
                Next
                For Each ctrl As Control In Me.pnlHomeBench.Controls
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If ctrl.Tag.ToString() = intUniformNo Then
                                CType(ctrl, Button).BackColor = m_clrRecipientButtonColor

                            End If
                        End If
                    End If
                Next
            Else
                For Each ctrl As Control In Me.pnlVisit.Controls
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If ctrl.Tag.ToString() = intUniformNo Then
                                CType(ctrl, Button).BackColor = m_clrRecipientButtonColor

                            End If
                        End If
                    End If
                Next

                For Each ctrl As Control In Me.pnlVisitBench.Controls()
                    If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                        If ctrl.Tag IsNot Nothing Then
                            If ctrl.Tag.ToString() = intUniformNo Then
                                CType(ctrl, Button).BackColor = m_clrRecipientButtonColor

                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'RM 7904 - Recipient_Code

    Private Sub setPlayerTeam(ByVal plID As String, ByVal plTeam As Integer)
        Try
            lblValTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
            m_intTouchTeamID = plTeam

            m_strPlayerID = String.Empty
            'm_strPlayerID = btnHomePlayer1.Tag.ToString().Trim()
            m_strPlayerID = plID

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub setPlayerTeamRecipient(ByVal plID As String, ByVal plTeam As Integer)
        Try
            m_intRecTouchTeamID = plTeam
            m_strRecipientID = String.Empty
            'm_strRecipientID = btnHomePlayer1.Tag.ToString().Trim()
            m_strRecipientID = plID
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub DisableotherEventbuttonColor(ByVal btnName As String)
        'Dim clrHomeText As System.Drawing.Color = clsUtility.GetTextColor(m_objGameDetails.HomeTeamColor)
        Try
            For Each ctrl As Control In Me.pnlTouchTypes.Controls()
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    If ctrl.Name <> btnName Then
                        CType(ctrl, Button).BackColor = Color.White
                        CType(ctrl, Button).ForeColor = Color.Black

                        'CType(ctrl, Button).BackColor = clrHomeText
                        'CType(ctrl, Button).ForeColor = Color.Black
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnRefreshTime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshTime.Click
        Try
            If m_objGameDetails.IsEditMode = False Then
                txtTime.Text = frmMain.UdcRunningClock1.URCCurrentTime
                txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub deleteStartPeriod()
        Try
            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
            If m_objGameDetails.CurrentPeriod = 1 Then
                frmMain.lblPeriod.Text = "Halftime"
            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                frmMain.lblPeriod.Text = "End Reg"
                frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                frmMain.lblPeriod.Text = "ET Break"
            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                frmMain.lblPeriod.Text = "End Reg"
                frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
            End If
            If m_objGameDetails.CurrentPeriod <> 0 And m_objGameDetails.IsHalfTimeSwap Then
                SwitchSides()
                frmMain.SetHeader2()
            End If
            'TOSOCRS-116
            If m_objGameDetails.CurrentPeriod = 0 Then
                frmMain.lblPeriod.ForeColor = Color.WhiteSmoke
                frmMain.lblPeriod.Text = "Pre-Game"
            Else
                frmMain.lblPeriod.ForeColor = Color.Red
            End If

            frmMain.btnClock.Enabled = False
            frmMain.btnMinuteDown.Enabled = False
            frmMain.btnMinuteUp.Enabled = False
            frmMain.btnSecondDown.Enabled = False
            frmMain.btnSecondUp.Enabled = False

            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
            DisableControls(False)

            frmMain.UdcRunningClock1.URCToggle()
            If UdcSoccerField1.USFHomeTeamOnLeft Then
                UdcSoccerField1.USFHomeTeamOnLeft = False
            Else
                UdcSoccerField1.USFHomeTeamOnLeft = True
            End If
            m_objGameDetails.isHomeDirectionLeft = UdcSoccerField1.USFHomeTeamOnLeft
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayTouchesInGridView(ByVal m_dsTouchData As DataSet)
        Try
            Dim drs() As DataRow


            Dim lvindex As Integer = 0
            Dim Nevent As Decimal = 0
            Dim NeventDisposed As Decimal = 0

            If dgvTouches.Rows.Count > 0 Then
                If dgvTouches.SelectedRows.Count > 0 Then
                    lvindex = dgvTouches.SelectedRows(0).Index
                    Nevent = CDec(dgvTouches.SelectedRows(0).Cells("Sequence").Value.ToString.Trim())
                End If
            End If
            'Dont clear all data
            dgvTouches.Rows.Clear()
            If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                If dgvTouches.Rows.Count > 1 Then
                    'clearing only some events from grid and refilling again
                    ''--------------------------------------------------------
                    'For Each row As DataGridViewRow In dgvTouches.Rows
                    '    If CDec(row.Cells.Item("Sequence").Value) >= Nevent - 5 Then 'prev 5 records are cleared
                    '        If NeventDisposed = 0 Then
                    '            NeventDisposed = CDec(row.Cells.Item("Sequence").Value)
                    '        End If
                    '        'row.Dispose()
                    '        dgvTouches.Rows.Remove(row)
                    '    End If
                    'Next
                    '--------------------------------------------------------
                    drs = m_dsTouchData.Tables(0).Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER > '" & CDec(dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Sequence").Value) & "'", "SEQUENCE_NUMBER ASC")
                    'drs = m_dsTouchData.Tables(0).Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= " & NeventDisposed & "", "SEQUENCE_NUMBER ASC")
                Else
                    'Main reporter always display the latest Sequence at the Top
                    If m_objclsGameDetails.SerialNo = 1 Then
                        drs = m_dsTouchData.Tables(0).Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER > 0", "SEQUENCE_NUMBER DESC")
                    Else
                        drs = m_dsTouchData.Tables(0).Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER > 0", "SEQUENCE_NUMBER ASC")
                    End If
                End If

                If drs.Length > 0 Then
                    For Each dr As DataRow In drs
                        'TOSOCRS-116 
                        'Select Case CInt(IIf(IsDBNull(dr.Item("TOUCH_TYPE_ID")) = True, 0, dr.Item("TOUCH_TYPE_ID")))
                        '    Case 14, 15
                        '    Case Else
                        dgvTouches.Rows.Add()
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Revisit").Value = False
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Perid").Value = dr.Item("PERIOD").ToString
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Time").Value = TimeAfterRegularInterval(CInt(dr.Item("TIME_ELAPSED")), CInt(dr.Item("PERIOD")))
                        If dr.Item("TEAM_ID1") IsNot DBNull.Value Then
                            If CInt(dr.Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = m_objclsGameDetails.HomeTeam
                            Else
                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = m_objclsGameDetails.AwayTeam
                            End If
                        Else
                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = "?"
                        End If
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Uniform").Value = IIf(IsDBNull(dr.Item("DISPLAY_UNIFORM_NUMBER")) = True, "?", dr.Item("DISPLAY_UNIFORM_NUMBER"))
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Player").Value = IIf(IsDBNull(dr.Item("PLAYER")) = True, "?", dr.Item("PLAYER"))
                        If (CInt(IIf(IsDBNull(dr.Item("TOUCH_TYPE_ID")) = True, 0, dr.Item("TOUCH_TYPE_ID"))) = 14) Then
                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Type").Value = "Start Period"
                        ElseIf (CInt(IIf(IsDBNull(dr.Item("TOUCH_TYPE_ID")) = True, 0, dr.Item("TOUCH_TYPE_ID"))) = 15) Then
                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Type").Value = "End Period"
                        Else
                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Type").Value = IIf(IsDBNull(dr.Item("TOUCH_TYPE_DESC")) = True, "?", dr.Item("TOUCH_TYPE_DESC"))
                        End If

                        If dr.Item("X_FIELD_ZONE") IsNot DBNull.Value And dr.Item("Y_FIELD_ZONE") IsNot DBNull.Value Then
                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Location").Value = "(" & dr.Item("X_FIELD_ZONE").ToString() & ", " & dr.Item("Y_FIELD_ZONE").ToString() & ")"
                        Else
                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Location").Value = "?"
                        End If
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Sequence").Value = dr.Item("SEQUENCE_NUMBER").ToString
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Uid").Value = dr.Item("UNIQUE_ID").ToString
                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("TeamId").Value = dr.Item("TEAM_ID1").ToString

                        If IsDBNull(dr.Item("TEAM_ID1")) = False Then
                            If CInt(dr.Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).DefaultCellStyle.BackColor = Color.White
                            Else
                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).DefaultCellStyle.BackColor = Color.LightGray
                            End If
                        End If

                        ' End Select
                    Next
                End If
            End If

            'Highlight the next touch type
            If dgvTouches.Rows.Count > 0 And m_objclsGameDetails.SerialNo <> 1 Then
                If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                    Dim dr() As DataRow
                    If ChkAllData.Checked Then
                        Select Case m_objclsGameDetails.SerialNo
                            Case 2
                                dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND UNIQUE_ID > 0 AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                            Case 3
                                dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND UNIQUE_ID > 0 AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                        End Select
                    Else
                        Select Case m_objclsGameDetails.SerialNo
                            Case 2
                                dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                            Case 3
                                dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                        End Select
                    End If

                    If (Nevent = 0) Then
                        btnClear_Click(Nothing, Nothing)
                        For Each row As DataGridViewRow In dgvTouches.Rows
                            dgvTouches.Rows(row.Index).Selected = False
                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                        Next
                        Exit Try
                    End If

                    If dr.Length > 0 Then
                        For Each drrow As DataRow In dr
                            Select Case drrow("TOUCH_TYPE_ID").ToString
                                Case "14", "15"
                                Case Else
                                    For Each row As DataGridViewRow In dgvTouches.Rows
                                        If CDec(row.Cells.Item("Sequence").Value) >= CDec(drrow.Item("Sequence_Number")) Then
                                            dgvTouches.Rows(row.Index).Selected = True
                                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                                            Exit Try
                                        End If
                                    Next
                            End Select
                        Next
                    Else
                        btnClear_Click(Nothing, Nothing)
                        For Each row As DataGridViewRow In dgvTouches.Rows
                            dgvTouches.Rows(row.Index).Selected = False
                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                        Next
                        Exit Try
                    End If
                End If

            ElseIf m_objclsGameDetails.SerialNo = 1 Then
                If dgvTouches.Rows.Count > 1 Then
                    dgvTouches.Rows(0).Selected = True
                    dgvTouches.FirstDisplayedScrollingRowIndex = dgvTouches.Rows(0).Index
                End If
            End If

        Catch ex As Exception
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Error, "Error occured on DisplayTouchesInGridview" & ex.Message & vbNewLine, 1, 1)
        End Try
    End Sub

    Public Sub DisplayTouchesData(ByVal periodFilter As Integer)
        Try
            m_dsTouchData = m_objclsTouches.GetTouches(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo, m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.AwayTeamID, periodFilter)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    DisplayTouchesInGridView(m_dsTouchData)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DisplayAllTouchesData()
        Try
            m_dsTouchData = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    DisplayTouchesInGridView(m_dsTouchData)
                End If
            End If

        Catch ex As Exception
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Error, "Error occured on DisplayAllTouchesData" & ex.Message & vbNewLine, 1, 1)
        End Try
    End Sub

    
    Private Sub setTimeAfterRefresh(ByVal refreshGameData As DataSet)
        Try
            Dim strTimeElapsed As String = "00:00"
            Dim strNewTimeElapsed() As String
            Dim intTouchTypeID As Integer
            Dim strReporterRole As String
            Dim intCurrentTimeElapsedInSecond As Integer
            Dim intLastTimeElapsedInSecond As Integer
            Dim intUID As Integer
            Dim strRefreshed As String
            Dim inteditUID As Integer
            Dim intMainTouchTypeID As Integer

            strTimeElapsed = CalculateTimeElapseAfter(CInt(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("TIME_ELAPSED")), CInt(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("PERIOD")))
            strNewTimeElapsed = strTimeElapsed.Split(CChar(":"))
            intTouchTypeID = CInt(IIf(IsDBNull(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("TOUCH_TYPE_ID")) = True, 0, refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("TOUCH_TYPE_ID")))
            If refreshGameData.Tables(0).Rows.Count > 1 Then
                intMainTouchTypeID = CInt(IIf(IsDBNull(refreshGameData.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")) = True, 0, refreshGameData.Tables(0).Rows(0).Item("TOUCH_TYPE_ID")))
            End If
            intCurrentTimeElapsedInSecond = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)), m_objGameDetails.CurrentPeriod)
            intLastTimeElapsedInSecond = CInt(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("TIME_ELAPSED"))
            intUID = CInt(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("UNIQUE_ID"))
            strRefreshed = refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("REFRESHED").ToString
            strReporterRole = refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("REPORTER_ROLE").ToString()
            inteditUID = CInt(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("EDIT_UID"))
            If intTouchTypeID <> 14 And intTouchTypeID <> 15 And intMainTouchTypeID <> 14 Then
                'sep 15 2010 commented by shirley (clock sync part totally commented)
                'Select Case m_objclsGameDetails.ReporterRole
                '    Case "A1", "B1", "C1", "D1" 'IN PRIMARY REPORTER SIDE ALWAYS SET THE HIGHER TIME IF COMING FROM ASSISTER
                '        If (intCurrentTimeElapsedInSecond < intLastTimeElapsedInSecond) And (m_objclsGameDetails.CurrentPeriod = CInt(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("PERIOD"))) Then
                '            frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                '        End If
                '    Case Else   ' IN ASSISTER REPORTER SIDE ALWAYS SET THE TIME IRRESPECTIVE OF HIGHER OR LOWER CLOCK.
                '        If intUID > 0 And strReporterRole <> m_objclsGameDetails.ReporterRole And inteditUID = 0 And refreshGameData.Tables(0).Rows(0).Item("RECORD_EDITED").ToString() = "N" Then
                '            'condition commented by shirley again on sep 14 2010 as A2 's clock is ahead in few sceanrios
                '            ''if A2 increses the clock time and enters an event,clock is setting in a1. if A1 sents an event after that, the clock is not
                '            'setting in A2 side,,
                '            If intCurrentTimeElapsedInSecond < intLastTimeElapsedInSecond Then 'added by shirley on may 14 2010
                '                frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                '            End If
                '        End If
                'End Select
                If intMainTouchTypeID <> 14 And inteditUID = 0 Then
                    If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") And frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") = False Then

                    Else
                        frmMain.SetPeriod()
                        If frmMain.UdcRunningClock1.URCIsRunning Then
                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                        Else
                            frmMain.UdcRunningClock1.URCToggle()
                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                            DisableControls(True)
                        End If
                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                    End If
                End If
            ElseIf intTouchTypeID = 15 And CDec(refreshGameData.Tables(0).Rows(refreshGameData.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER")) = -1 Then
                'Arindam - End Period DELETED

                If frmMain.UdcRunningClock1.URCIsRunning = False Then
                    frmMain.UdcRunningClock1.URCToggle()
                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    'Else
                    '    frmMain.UdcRunningClock1.URCToggle()
                    '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    '    DisableControls(True)
                End If
                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                frmMain.SetPeriod()
                frmMain.SetClockControl(True)
                DisableControls(True)
            ElseIf intMainTouchTypeID = 14 And CDec(refreshGameData.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")) = -1 Then 'TOSOCRS-116
                If frmMain.UdcRunningClock1.URCIsRunning Then
                    frmMain.UdcRunningClock1.URCToggle()
                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                    'Else
                    '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                End If
                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                frmMain.SetPeriod()
                frmMain.SetClockControl(False)
                DisableControls(False)
            End If
            If intMainTouchTypeID = 14 And CDec(refreshGameData.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")) <> -1 Then 'TOSOCRS-116
                Select Case CInt(m_objclsGameDetails.ReporterRole.Substring(m_objclsGameDetails.ReporterRole.Length - 1))
                    Case 1 'IN PRIMARY REPORTER SIDE ALWAYS SET THE HIGHER TIME IF COMING FROM ASSISTER
                        'If intCurrentTimeElapsedInSecond < intLastTimeElapsedInSecond Then
                        '    frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                        'End If
                    Case Else   ' IN ASSISTER REPORTER SIDE ALWAYS SET THE TIME IRRESPECTIVE OF HIGHER OR LOWER CLOCK.
                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTimeElapsed(0)), CShort(strNewTimeElapsed(1)))
                End Select
                If frmMain.UdcRunningClock1.URCIsRunning Then
                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                Else
                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    frmMain.UdcRunningClock1.URCToggle()
                End If
                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                frmMain.SetPeriod()
                DisableControls(True)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub SetPlayerButton(ByRef Btn As Button, ByVal Uniform As String, ByVal LastName As String, ByVal Moniker As String, ByVal Position As String)
        Try
            Dim strRightSpace As String = String.Empty
            Dim strUniform As String = String.Empty

            strUniform = Uniform
            If strUniform.Length = 2 Then
                strUniform = strUniform & "." & Space(1)
            ElseIf strUniform.Length = 1 Then
                strUniform = strUniform & "." & Space(3)
            ElseIf strUniform.Length = 0 Then
                strUniform = Space(6)
            End If

            strRightSpace = Space(20)

            If Position.Length = 1 Then
                Position = Position & Space(2)
            End If

            Btn.Text = strUniform & LastName & Space(1) & Moniker & strRightSpace & Position
            While Btn.PreferredSize.Width > Btn.Width
                If strRightSpace.Length > 3 Then
                    strRightSpace = strRightSpace.Substring(0, strRightSpace.Length - 1)
                Else
                    LastName = LastName.Substring(0, LastName.Length - 2) & "."
                End If
                Btn.Text = strUniform & LastName & Space(1) & Moniker & strRightSpace & Position
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region " SportVU  "

    Public Sub GetOpticalData()
        Try
            If m_objclsGameDetails.IsEditMode = False And m_objclsGameDetails.IsSportVUAvailable = True Then
                Dim dsTouch As New DataSet

                If Not m_objclsGameDetails.OpticalTouchData Is Nothing Then
                    m_objclsGameDetails.OpticalTouchData.Clear()
                End If

                If m_strPlayerID IsNot String.Empty And m_intTouchTeamID <> -1 Then
                    dsTouch = m_objSportVU.GetOpticalTouchData(m_objclsGameDetails.GameCode, Get_Player_ID(Convert.ToInt32(m_strPlayerID), m_intTouchTeamID), m_intTouchTeamID)
                    m_objclsGameDetails.OpticalTouchData = dsTouch
                End If

                If dsTouch.Tables.Count > 0 Then
                    If dsTouch.Tables(0).Rows.Count > 0 Then
                        Dim dr() As DataRow
                        dr = dsTouch.Tables(0).Select("TEAM_ID = " & m_intTouchTeamID & " AND " & "PLAYER_ID = " & Get_Player_ID(Convert.ToInt32(m_strPlayerID), m_intTouchTeamID))
                        If dr.Length > 0 Then
                            UdcSoccerField1.USFClearMarks()
                            lblSelection1.Text = "-"
                            If dr(0).Item("X_FIELD_ZONE_OPTICAL") IsNot DBNull.Value And dr(0).Item("Y_FIELD_ZONE_OPTICAL") IsNot DBNull.Value And dr(0).Item("Z_FIELD_ZONE_OPTICAL") IsNot DBNull.Value Then
                                lblValLocation.Text = "X=" & dr(0).Item("X_FIELD_ZONE_OPTICAL").ToString() & ", Y=" & dr(0).Item("Y_FIELD_ZONE_OPTICAL").ToString() & ",Z= " & dr(0).Item("z_FIELD_ZONE_OPTICAL").ToString()
                                m_X_FieldZone = CInt(dr(0).Item("X_FIELD_ZONE_OPTICAL"))
                                m_Y_FieldZone = CInt(dr(0).Item("Y_FIELD_ZONE_OPTICAL"))
                                m_strUniqueId = dr(0).Item("UNIQUE_ID").ToString()
                                UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone, True)
                            End If
                        End If
                    End If
                End If



            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Sub GetOpticalTouchEvent(ByVal UniqueId As Integer)
        Try
            Dim dsTouch As New DataSet

            dsTouch = m_objSportVU.GetOpticalTouchEvent(m_objclsGameDetails.GameCode, UniqueId)

            If dsTouch.Tables.Count > 0 Then
                If dsTouch.Tables(0).Rows.Count > 0 Then
                    'm_PlayerID = CInt(dsTouch.Tables(0).Rows(0)("Player_ID"))
                    'm_TouchTypeID = CInt(dsTouch.Tables(0).Rows(0)("TOUCH_TYPE_ID"))
                    UdcSoccerField1.USFClearMarks()
                    SetStateOpticalEvents(dsTouch)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Sub SetStateOpticalEvents(ByVal DsOpticalTouch As DataSet)
        Try
            Dim DStouchdata As New DataSet
            DStouchdata = DsOpticalTouch

            If DStouchdata IsNot Nothing Then
                If DStouchdata.Tables(0).Rows.Count > 0 Then
                    If DStouchdata.Tables(0).Rows(0).Item("TEAM_ID") IsNot DBNull.Value Then
                        If CInt(DStouchdata.Tables(0).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                            lblValTeam.Text = m_objclsGameDetails.HomeTeam
                            m_intTouchTeamID = m_objclsGameDetails.HomeTeamID
                        Else
                            lblValTeam.Text = m_objclsGameDetails.AwayTeam
                            m_intTouchTeamID = m_objclsGameDetails.AwayTeamID
                        End If
                    Else
                        lblValTeam.Text = String.Empty
                    End If
                    If DStouchdata.Tables(0).Rows(0).Item("PERIOD") IsNot DBNull.Value Then
                        m_CurrentPeriod = CInt(DStouchdata.Tables(0).Rows(0).Item("PERIOD"))
                    End If

                    If DStouchdata.Tables(0).Rows(0).Item("DISPLAY_UNIFORM_NUMBER") IsNot DBNull.Value Then
                        lblValUniform.Text = DStouchdata.Tables(0).Rows(0).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                    Else
                        If DStouchdata.Tables(0).Rows(0).Item("PLAYER_ID") IsNot DBNull.Value Then
                            m_strPlayerID = CStr(DStouchdata.Tables(0).Rows(0).Item("PLAYER_ID").ToString())
                        End If
                        lblValUniform.Text = String.Empty
                    End If
                    If DStouchdata.Tables(0).Rows(0).Item("PLAYER_ID") IsNot DBNull.Value Then
                        m_strPlayerID = CStr(DStouchdata.Tables(0).Rows(0).Item("PLAYER_ID").ToString())
                    End If

                    'TOUCH TYPE ID
                    'If dsTouchData.Tables(0).Rows(0).Item("TOUCH_TYPE_DESC") IsNot DBNull.Value Then
                    '    lblValType.Text = dsTouchData.Tables(0).Rows(0).Item("TOUCH_TYPE_DESC").ToString()
                    '    m_intTouchTypeID = CInt(dsTouchData.Tables(0).Rows(0).Item("TOUCH_TYPE_ID"))
                    'Else
                    '    lblValType.Text = String.Empty
                    'End If

                    If DStouchdata.Tables(0).Rows(0).Item("X_FIELD_ZONE_OPTICAL") IsNot DBNull.Value And DStouchdata.Tables(0).Rows(0).Item("Y_FIELD_ZONE_OPTICAL") IsNot DBNull.Value Then
                        lblValLocation.Text = "X=" & DStouchdata.Tables(0).Rows(0).Item("X_FIELD_ZONE_OPTICAL").ToString() & ", Y=" & DStouchdata.Tables(0).Rows(0).Item("Y_FIELD_ZONE_OPTICAL").ToString()
                        m_X_FieldZone = CInt(DStouchdata.Tables(0).Rows(0).Item("X_FIELD_ZONE_OPTICAL"))
                        m_Y_FieldZone = CInt(DStouchdata.Tables(0).Rows(0).Item("Y_FIELD_ZONE_OPTICAL"))
                        UdcSoccerField1.USFSetMark(m_X_FieldZone, m_Y_FieldZone)
                        lblSelection1.Text = m_X_FieldZone & ", " & m_Y_FieldZone
                    Else
                        lblValLocation.Text = String.Empty
                        m_X_FieldZone = -1
                        m_Y_FieldZone = -1
                    End If

                    ''
                    Dim ElapsedTime As Integer
                    Dim CurrPeriod As Integer
                    ElapsedTime = CInt(DStouchdata.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                    CurrPeriod = CInt(DStouchdata.Tables(0).Rows(0).Item("PERIOD"))
                    txtTime.Text = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))

                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, DStouchdata.Tables(0).Rows(0).Item("PERIOD").ToString() & " - " & DStouchdata.Tables(0).Rows(0).Item("DISPLAY_UNIFORM_NUMBER").ToString() & " - " & lblValLocation.Text, 1, 0)

                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Function TriggerSave() As Boolean
        Try

            If CheckForData() = False Then
                Return False
            End If

            If String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If
            If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                If String.IsNullOrEmpty(m_strPlayerID) Then
                    MessageDialog.Show(strmessage25, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                End If
            End If

            Dim chrTime(1) As Char
            chrTime(1) = Convert.ToChar(":")
            Dim strArrTime As String() = txtTime.Text.Split(chrTime)
            If String.IsNullOrEmpty(strArrTime(0).Trim()) And String.IsNullOrEmpty(strArrTime(1).Trim()) Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If

            Dim chkRes As Boolean = True
            chkRes = clsValidation.ValidateTime(txtTime.Text.Trim())
            If chkRes Then
                Dim chrSep(1) As Char
                chrSep(1) = Convert.ToChar(":")
                Dim strArrCurrentTime As String() = txtTime.Text.Split(chrSep)
                chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, strArrCurrentTime(0).Trim(), 0, 150)
            Else
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If
            If Not chkRes Then
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If
            'If m_objclsGameDetails.IsEditMode = False Then
            If m_objclsGameDetails.SerialNo = 1 Then
                chkRes = ValidateCurrentTime()
            End If

            'End If
            If Not chkRes Then
                Return False
            End If

            'RM 7904 Recipient_Code
            ''TOSOCRS-41/TOSOCRS-100 Recipients for new data points
            If (m_intTouchTypeID = 1 Or m_intTouchTypeID = -99) And String.IsNullOrEmpty(m_strRecipientID) = True Then
                MessageDialog.Show(strmessage33, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub frmTouches_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseClick
        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                If m_objclsGameDetails.IsEditMode = False And (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                    MessageDialog.Show(strmessage27, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If

                 ' Primary Reporter Down Alert TOSOCRS-45
                If IsMyNetworkUP() = False Then
                    Exit Sub
                End If

                If IsPBPEntryAllowed(1) = False Then
                    Exit Sub
                End If

                If TriggerSave() = False Then
                    Exit Sub
                End If

                ''
                'Save events here or skip
                If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                    If compareTouchesData(m_intTouchTypeID, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def) Then
                        ClearTouchLabels()
                        'm_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                        m_decSequenceNo = "-1"
                        DisableOtherButtonColor(btnClear.Name)
                        DisableotherEventbuttonColor(btnClear.Name)
                        initializeTouchData()

                        If dgvTouches.SelectedRows.Count > 0 Then
                            checkForNextTouchData(0)
                            dgvTouches_DoubleClick(sender, e)
                            If ChkAllData.Checked = True Then
                                DisplayAllTouchesData()
                            End If
                            'm_blnSavestatus = True
                        End If
                    Else
                        SaveTouches()
                        initializeTouchData()

                        If dgvTouches.SelectedRows.Count > 0 Then
                            checkForNextTouchData(1)
                            dgvTouches_DoubleClick(sender, e)
                            If ChkAllData.Checked = True Then
                                DisplayAllTouchesData()
                            End If
                        End If
                        'm_blnSavestatus = True
                    End If
                Else
                    SaveTouches()
                    initializeTouchData()
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerField1_USFRightClick(ByVal sender As Object, ByVal e As USFEventArgs) Handles UdcSoccerField1.USFRightClick
        Try
            If m_objclsGameDetails.IsEditMode = False And (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                MessageDialog.Show(strmessage27, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

           ' Primary Reporter Down Alert TOSOCRS-45
            If IsMyNetworkUP() = False Then
                Exit Sub
            End If

            If IsPBPEntryAllowed(1) = False Then
                Exit Sub
            End If

            If TriggerSave() = False Then
                Exit Sub
            End If
            ''
            If m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3 Then
                If compareTouchesData(m_intTouchTypeID, m_X_FieldZone, m_Y_FieldZone, m_X_FieldZone_Def, m_Y_FieldZone_Def) Then
                    ClearTouchLabels()
                    'm_objclsGameDetails.TouchesData.Tables(0).Rows.Clear()
                    m_decSequenceNo = "-1"
                    DisableOtherButtonColor(btnClear.Name)
                    DisableotherEventbuttonColor(btnClear.Name)
                    initializeTouchData()

                    If dgvTouches.SelectedRows.Count > 0 Then
                        checkForNextTouchData(0)
                        dgvTouches_DoubleClick(sender, e)
                        If ChkAllData.Checked = True Then
                            DisplayAllTouchesData()
                        End If
                        'm_blnSavestatus = True
                    End If
                Else
                    SaveTouches()
                    initializeTouchData()

                    If dgvTouches.SelectedRows.Count > 0 Then
                        checkForNextTouchData(1)
                        dgvTouches_DoubleClick(sender, e)
                        If ChkAllData.Checked = True Then
                            DisplayAllTouchesData()
                        End If
                    End If
                    'm_blnSavestatus = True
                End If
            Else
                SaveTouches()
                initializeTouchData()
            End If
        Catch ex As Exception
            Throw
        End Try

    End Sub


    Private Sub lblInstructions_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblInstructions.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If m_objclsGameDetails.IsEditMode = False And (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                MessageDialog.Show(strmessage27, "Touches", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If IsPBPEntryAllowed(1) = False Then
                Exit Sub
            End If

            If TriggerSave() = False Then
                Exit Sub
            End If
            If (m_objclsGameDetails.SerialNo = 2 Or m_objclsGameDetails.SerialNo = 3) Then
                If dgvTouches.SelectedRows.Count > 0 Then
                    Dim Nevent As Decimal = 0
                    Nevent = CDec(dgvTouches.SelectedRows(0).Cells("Sequence").Value)

                    Try
                        If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                            Dim dr() As DataRow
                            Select Case m_objclsGameDetails.SerialNo
                                Case 2
                                    dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                                Case 3
                                    dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER >= '" & Nevent & "' AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                            End Select
                            If dr.Length > 0 Then
                                For Each drrow As DataRow In dr
                                    Select Case drrow("TOUCH_TYPE_ID").ToString
                                        Case "14", "15"
                                        Case Else
                                            For Each row As DataGridViewRow In dgvTouches.Rows
                                                If CDec(row.Cells.Item("Sequence").Value) >= CDec(drrow.Item("Sequence_Number")) Then
                                                    dgvTouches.Rows(row.Index).Selected = True
                                                    dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                                                    Exit Try
                                                End If
                                            Next
                                    End Select
                                Next
                            End If
                        End If
                    Catch ex As Exception
                        Throw ex
                    End Try
                    'Need to check this later
                    dgvTouches_DoubleClick(sender, e)
                    If ChkAllData.Checked = True Then
                        DisplayAllTouchesData()
                    End If
                End If

            End If
        End If
    End Sub

    Private Sub btnVisitTouchTypes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles btnAerialVisit.Click, btnRunWithBallVisit.Click, btn50Visit.Click
        Try
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text, 1, 0)
            ''TOSOCRS-116 
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                MessageDialog.Show(strmessage55, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If

            If DirectCast(sender, System.Windows.Forms.Button).Text <> "" Then
                If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then
                    DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                    SetTime()
                    lblValTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
                    m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
                    m_objclsGameDetails.TouchesTeamID = m_intTouchTeamID
                    Select Case DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        Case "18"
                            lblValType.Text = "Air Win"
                            m_intTouchTypeID = 18
                        Case "17"
                            lblValType.Text = "Run With Ball"
                            m_intTouchTypeID = 17
                        Case "20"
                            lblValType.Text = "50-Win"
                            m_intTouchTypeID = 20
                    End Select
                    lblValUniform.Text = String.Empty
                    m_strPlayerID = String.Empty
                    CheckForSave()
                End If
            End If

            GetOpticalData()
            DisableOtherButtonColor(btnHomePlayer1.Name)
            'Mark the selection in Listview item
            'HighlightLVSelectedRow()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub btnHomeTocuhTypes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
     Handles btnAerialHome.Click, btnRunWithBallHome.Click, btn50Home.Click
        Try
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text, 1, 0)
            ''TOSOCRS-116 
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                MessageDialog.Show(strmessage55, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If

            If DirectCast(sender, System.Windows.Forms.Button).Text <> "" Then
                If DirectCast(sender, System.Windows.Forms.Button).BackColor <> m_clrHighlightedButtonColor Then
                    DirectCast(sender, System.Windows.Forms.Button).BackColor = m_clrHighlightedButtonColor
                    SetTime()
                    lblValTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
                    m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
                    m_objclsGameDetails.TouchesTeamID = m_intTouchTeamID
                    Select Case DirectCast(sender, System.Windows.Forms.Button).Tag.ToString().Trim()
                        Case "18"
                            lblValType.Text = "Air Win"
                            m_intTouchTypeID = 18
                        Case "17"
                            lblValType.Text = "Run With Ball"
                            m_intTouchTypeID = 17
                        Case "20"
                            lblValType.Text = "50-Win"
                            m_intTouchTypeID = 20
                    End Select
                    lblValUniform.Text = String.Empty
                    m_strPlayerID = String.Empty
                    CheckForSave()
                End If
            End If

            GetOpticalData()
            DisableOtherButtonColor(btnHomePlayer1.Name)
            'Mark the selection in Listview item
            'HighlightLVSelectedRow()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub



    Private Sub btnHomePrimary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHomePrimary.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePrimary.Text, 1, 0)
            ''TOSOCRS-116 
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                MessageDialog.Show(strmessage55, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If

            If btnHomePrimary.Text <> "" Then
                If btnHomePrimary.BackColor <> m_clrHighlightedButtonColor Then
                    btnHomePrimary.BackColor = m_clrHighlightedButtonColor
                    SetTime()
                    lblValTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
                    m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.HomeTeamID)
                    m_objclsGameDetails.TouchesTeamID = m_intTouchTeamID
                    'If btnHomePlayer1.Tag.ToString().Trim().Length <= 3 Then
                    '    lblValUniform.Text = btnHomePlayer1.Tag.ToString().Trim()
                    'Else
                    '    lblValUniform.Text = String.Empty
                    'End If
                    lblValUniform.Text = String.Empty
                    m_strPlayerID = String.Empty
                    'm_strPlayerID = btnHomePlayer1.Tag.ToString().Trim()
                    CheckForSave()
                End If
            End If
            GetOpticalData()
            DisableOtherButtonColor(btnHomePlayer1.Name)
            'Mark the selection in Listview item
            'HighlightLVSelectedRow()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub btnVisitPrimary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVisitPrimary.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPrimary.Text, 1, 0)
            ''TOSOCRS-116 
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                MessageDialog.Show(strmessage55, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If

            If btnVisitPrimary.Text <> "" Then
                If btnVisitPrimary.BackColor <> m_clrHighlightedButtonColor Then
                    btnVisitPrimary.BackColor = m_clrHighlightedButtonColor
                    SetTime()
                    lblValTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
                    m_intTouchTeamID = Convert.ToInt32(m_objclsGameDetails.AwayTeamID)
                    m_objclsGameDetails.TouchesTeamID = m_intTouchTeamID
                    'If btnHomePlayer1.Tag.ToString().Trim().Length <= 3 Then
                    '    lblValUniform.Text = btnHomePlayer1.Tag.ToString().Trim()
                    'Else
                    '    lblValUniform.Text = String.Empty
                    'End If
                    lblValUniform.Text = String.Empty
                    m_strPlayerID = String.Empty
                    'm_strPlayerID = btnHomePlayer1.Tag.ToString().Trim()
                    CheckForSave()
                End If
            End If
            GetOpticalData()
            DisableOtherButtonColor(btnHomePlayer1.Name)
            'Mark the selection in Listview item
            'HighlightLVSelectedRow()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub HighlightTeam(ByVal eTeam As enTeam)
        Try
            Select Case eTeam
                Case enTeam.None
                    pnlTeamHighlight.Visible = False
                Case enTeam.Home
                    pnlTeamHighlight.Height = pnlHome.Height + 10
                    pnlTeamHighlight.Width = pnlHome.Width + 10
                    pnlTeamHighlight.Left = pnlHome.Left - 5
                    pnlTeamHighlight.Top = pnlHome.Top - 5
                    pnlTeamHighlight.SendToBack()
                    pnlTeamHighlight.Visible = True
                Case enTeam.Away
                    pnlTeamHighlight.Height = pnlVisit.Height + 10
                    pnlTeamHighlight.Width = pnlVisit.Width + 10
                    pnlTeamHighlight.Left = pnlVisit.Left - 5
                    pnlTeamHighlight.Top = pnlVisit.Top - 5
                    pnlTeamHighlight.SendToBack()
                    pnlTeamHighlight.Visible = True
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbPeriod_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPeriod.Click
        Try
            Dim DsSelectedEvent As DataSet
            DsSelectedEvent = m_objclsTouches.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            cmbPeriod.Items.Clear()
            cmbPeriod.Items.Add("")
            If DsSelectedEvent.Tables(0).Rows.Count > 0 Then
                If CInt(DsSelectedEvent.Tables(0).Rows(0).Item("Period")) = 1 Then
                    cmbPeriod.Items.Add("1")
                ElseIf CInt(DsSelectedEvent.Tables(0).Rows(0).Item("Period")) = 2 Then
                    cmbPeriod.Items.Add("1")
                    cmbPeriod.Items.Add("2")
                ElseIf CInt(DsSelectedEvent.Tables(0).Rows(0).Item("Period")) = 3 Then
                    cmbPeriod.Items.Add("1")
                    cmbPeriod.Items.Add("2")
                    cmbPeriod.Items.Add("3")
                ElseIf CInt(DsSelectedEvent.Tables(0).Rows(0).Item("Period")) = 4 Then
                    cmbPeriod.Items.Add("1")
                    cmbPeriod.Items.Add("2")
                    cmbPeriod.Items.Add("3")
                    cmbPeriod.Items.Add("4")
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPeriod.SelectedIndexChanged
        Try
            Dim periodFilter As Integer
            If cmbPeriod.SelectedItem.ToString().Trim() = "" Then
                periodFilter = 0
            Else
                periodFilter = CInt(cmbPeriod.SelectedItem)
            End If
            If ChkAllData.Checked = True Then
                DisplayAllTouchesData()
            Else
                DisplayTouchesData(periodFilter)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Revisit Touches
    Private Sub chkRevisit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRevisit.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.CheckBoxSelected, ChkAllData.Text, 1, 0)
            btnClear_Click(Nothing, Nothing)

            If chkRevisit.Checked Then
                RevisitTouchesData()
                If chkOwnTouches.Checked = True Then
                    chkOwnTouches.Checked = False
                End If
                If ChkAllData.Checked = True Then
                    ChkAllData.Checked = False
                End If
            ElseIf ChkAllData.Checked Then
                DisplayAllTouchesData()
            ElseIf chkOwnTouches.Checked Then
                displayOwnTouchesData()
            Else
                DisplayTouchesData(0)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub ChkAllData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkAllData.CheckedChanged
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.CheckBoxSelected, ChkAllData.Text, 1, 0)
            btnClear_Click(Nothing, Nothing)

            If ChkAllData.Checked Then
                DisplayAllTouchesData()
                If chkOwnTouches.Checked = True Then
                    chkOwnTouches.Checked = False
                End If
                If chkRevisit.Checked = True Then
                    chkRevisit.Checked = False
                End If
            Else ''7904 Issue #8 June 09 Display own touches
                If (chkOwnTouches.Checked = False) And (chkRevisit.Checked = False) Then
                    DisplayTouchesData(0)
                ElseIf chkOwnTouches.Checked = True Then
                    displayOwnTouchesData()
                ElseIf chkRevisit.Checked = True Then
                    RevisitTouchesData()
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub chkOwnTouches_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOwnTouches.CheckedChanged
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.CheckBoxSelected, ChkAllData.Text, 1, 0)
            btnClear_Click(Nothing, Nothing)

            If chkOwnTouches.Checked Then
                displayOwnTouchesData()
                ChkAllData.Checked = False
                chkRevisit.Checked = False
            ElseIf (ChkAllData.Checked = False) And (chkRevisit.Checked = False) Then
                DisplayTouchesData(0)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Sub displayOwnTouchesData()
        Try
            Dim lvindex As Integer = 0
            Dim Nevent As Decimal = 0

            If dgvTouches.Rows.Count > 0 Then
                If dgvTouches.SelectedRows.Count > 0 Then
                    lvindex = dgvTouches.SelectedRows(0).Index
                    Nevent = CDec(dgvTouches.SelectedRows(0).Cells("Sequence").Value.ToString.Trim())
                End If
            End If

            dgvTouches.Rows.Clear()
            m_dsTouchData = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
            If m_dsTouchData IsNot Nothing Then
                If m_dsTouchData.Tables.Count > 0 Then
                    If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then

                        Dim dr() As DataRow
                        Select Case m_objclsGameDetails.SerialNo
                            Case 2
                                dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16  AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                            Case 3
                                dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16  AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                        End Select
                        If dr.Length > 0 Then
                            For Each drrow As DataRow In dr
                                '7904 - Selecting Revisiting should remove the touch from "Display own touches" list
                                Dim drPBPData As DataRow()
                                drPBPData = m_objGameDetails.PBP.Tables(0).Select("FLAG = 'Y' AND SEQUENCE_NUMBER = '" & CDec(drrow("SEQUENCE_NUMBER")) & "'")
                                If drPBPData.Length = 0 Then
                                    '---------------------------
                                    Select Case CInt(IIf(IsDBNull(drrow.Item("TOUCH_TYPE_ID")) = True, 0, drrow.Item("TOUCH_TYPE_ID")))
                                        Case 14, 15
                                        Case Else
                                            dgvTouches.Rows.Add()
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Revisit").Value = False
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Perid").Value = drrow.Item("PERIOD").ToString
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Time").Value = TimeAfterRegularInterval(CInt(drrow.Item("TIME_ELAPSED")), CInt(drrow.Item("PERIOD")))
                                            If drrow.Item("TEAM_ID1") IsNot DBNull.Value Then
                                                If CInt(drrow.Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = m_objclsGameDetails.HomeTeam
                                                Else
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = m_objclsGameDetails.AwayTeam
                                                End If
                                            Else
                                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = "?"
                                            End If
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Uniform").Value = IIf(IsDBNull(drrow.Item("DISPLAY_UNIFORM_NUMBER")) = True, "?", drrow.Item("DISPLAY_UNIFORM_NUMBER"))
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Player").Value = IIf(IsDBNull(drrow.Item("PLAYER")) = True, "?", drrow.Item("PLAYER"))
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Type").Value = IIf(IsDBNull(drrow.Item("TOUCH_TYPE_DESC")) = True, "?", drrow.Item("TOUCH_TYPE_DESC"))
                                            If drrow.Item("X_FIELD_ZONE") IsNot DBNull.Value And drrow.Item("Y_FIELD_ZONE") IsNot DBNull.Value Then
                                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Location").Value = "(" & drrow.Item("X_FIELD_ZONE").ToString() & ", " & drrow.Item("Y_FIELD_ZONE").ToString() & ")"
                                            Else
                                                dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Location").Value = "?"
                                            End If
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Sequence").Value = drrow.Item("SEQUENCE_NUMBER").ToString
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Uid").Value = drrow.Item("UNIQUE_ID").ToString
                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("TeamId").Value = drrow.Item("TEAM_ID1").ToString

                                            If IsDBNull(drrow.Item("TEAM_ID1")) = False Then
                                                If CInt(drrow.Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).DefaultCellStyle.BackColor = Color.White
                                                Else
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).DefaultCellStyle.BackColor = Color.LightGray
                                                End If
                                            End If

                                    End Select
                                End If
                            Next
                        End If
                    End If
                End If
            End If


            'Highlight the next touch type
            If dgvTouches.Rows.Count > 0 And m_objclsGameDetails.SerialNo <> 1 Then
                'dgvTouches.Rows.RemoveAt(dgvTouches.Rows(dgvTouches.Rows.Count - 1).Index)
                If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                    Dim dr() As DataRow 'SEQUENCE_NUMBER >= '" & Nevent & "' AND UNIQUE_ID > 0
                    Select Case m_objclsGameDetails.SerialNo
                        Case 2
                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND UNIQUE_ID > 0 AND SEQUENCE_NUMBER >= '" & Nevent & "'  AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                        Case 3
                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND UNIQUE_ID > 0 AND SEQUENCE_NUMBER >= '" & Nevent & "'  AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                    End Select

                    If Nevent = 0 Then
                        btnClear_Click(Nothing, Nothing)
                        For Each row As DataGridViewRow In dgvTouches.Rows
                            dgvTouches.Rows(row.Index).Selected = False
                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                        Next
                        Exit Try
                    End If

                    If dr.Length > 0 Then
                        For Each drrow As DataRow In dr
                            Select Case drrow("TOUCH_TYPE_ID").ToString
                                Case "14", "15"
                                Case Else
                                    For Each row As DataGridViewRow In dgvTouches.Rows
                                        If CDec(row.Cells.Item("Sequence").Value) >= CDec(drrow.Item("Sequence_Number")) Then
                                            dgvTouches.Rows(row.Index).Selected = True
                                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                                            Exit Sub
                                        End If
                                    Next
                            End Select
                        Next
                    Else
                        btnClear_Click(Nothing, Nothing)
                        For Each row As DataGridViewRow In dgvTouches.Rows
                            dgvTouches.Rows(row.Index).Selected = False
                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                        Next
                    End If
                End If
            End If

        Catch ex As Exception
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Error, "Error occured on DisplayOwnTouchesData" & ex.Message & vbNewLine, 1, 1)
        End Try
    End Sub

    Public Sub RevisitTouchesData()
        Try
            Dim StrTime As String = "00:00"
            Dim lvindex As Integer = 0
            Dim Nevent As Decimal = 0
            Dim dr() As DataRow

            If dgvTouches.Rows.Count > 0 Then
                If dgvTouches.SelectedRows.Count > 0 Then
                    lvindex = dgvTouches.SelectedRows(0).Index
                    If dgvTouches.SelectedRows(0).Cells("Sequence").Value IsNot Nothing Then
                        Nevent = CDec(dgvTouches.SelectedRows(0).Cells("Sequence").Value.ToString.Trim())
                    End If
                End If
            End If

            dgvTouches.Rows.Clear()
            m_dsTouchData = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
            If m_dsTouchData IsNot Nothing Then '----1
                If m_dsTouchData.Tables.Count > 0 Then '----2
                    If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then '----3

                        If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then '----4
                            Dim drflag() As DataRow
                            Select Case m_objclsGameDetails.SerialNo
                                Case 2
                                    drflag = m_objGameDetails.PBP.Tables(0).Select("FLAG = 'Y'", "SEQUENCE_NUMBER")
                                Case 3
                                    drflag = m_objGameDetails.PBP.Tables(0).Select("FLAG = 'Y'", "SEQUENCE_NUMBER")
                            End Select
                            If drflag.Length > 0 Then
                                For Each drf As DataRow In drflag
                                    Select Case m_objclsGameDetails.SerialNo
                                        Case 2
                                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER = '" & CDec(drf("SEQUENCE_NUMBER")) & "' AND TEAM_ID1 = " & m_objclsGameDetails.HomeTeamID, "SEQUENCE_NUMBER")
                                        Case 3
                                            dr = m_dsTouchData.Tables("Touches").Select("ISNULL(TOUCH_TYPE_ID,99) <>  16 AND SEQUENCE_NUMBER = '" & CDec(drf("SEQUENCE_NUMBER")) & "' AND TEAM_ID1 = " & m_objclsGameDetails.AwayTeamID, "SEQUENCE_NUMBER")
                                    End Select
                                    If dr.Length > 0 Then
                                        For Each drrow As DataRow In dr
                                            Select Case CInt(IIf(IsDBNull(drrow.Item("TOUCH_TYPE_ID")) = True, 0, drrow.Item("TOUCH_TYPE_ID")))
                                                Case 14, 15
                                                Case Else
                                                    dgvTouches.Rows.Add()
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Revisit").Value = True
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Perid").Value = drrow.Item("PERIOD").ToString
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Time").Value = TimeAfterRegularInterval(CInt(drrow.Item("TIME_ELAPSED")), CInt(drrow.Item("PERIOD")))
                                                    If drrow.Item("TEAM_ID1") IsNot DBNull.Value Then
                                                        If CInt(drrow.Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = m_objclsGameDetails.HomeTeam
                                                        Else
                                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = m_objclsGameDetails.AwayTeam
                                                        End If
                                                    Else
                                                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Team").Value = "?"
                                                    End If
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Uniform").Value = IIf(IsDBNull(drrow.Item("DISPLAY_UNIFORM_NUMBER")) = True, "?", drrow.Item("DISPLAY_UNIFORM_NUMBER"))
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Player").Value = IIf(IsDBNull(drrow.Item("PLAYER")) = True, "?", drrow.Item("PLAYER"))
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Type").Value = IIf(IsDBNull(drrow.Item("TOUCH_TYPE_DESC")) = True, "?", drrow.Item("TOUCH_TYPE_DESC"))
                                                    If drrow.Item("X_FIELD_ZONE") IsNot DBNull.Value And drrow.Item("Y_FIELD_ZONE") IsNot DBNull.Value Then
                                                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Location").Value = "(" & drrow.Item("X_FIELD_ZONE").ToString() & ", " & drrow.Item("Y_FIELD_ZONE").ToString() & ")"
                                                    Else
                                                        dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Location").Value = "?"
                                                    End If
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Sequence").Value = drrow.Item("SEQUENCE_NUMBER").ToString
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("Uid").Value = drrow.Item("UNIQUE_ID").ToString
                                                    dgvTouches.Rows(dgvTouches.Rows.Count - 1).Cells("TeamId").Value = drrow.Item("TEAM_ID1").ToString

                                                    If IsDBNull(drrow.Item("TEAM_ID1")) = False Then
                                                        If CInt(drrow.Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).DefaultCellStyle.BackColor = Color.White
                                                        Else
                                                            dgvTouches.Rows(dgvTouches.Rows.Count - 1).DefaultCellStyle.BackColor = Color.LightGray
                                                        End If
                                                    End If
                                            End Select
                                        Next
                                    End If
                                Next
                            End If
                        End If  '----4
                    End If  '----3
                End If '----2
            End If '----1 

            'Highlight the next touch type
            If dgvTouches.Rows.Count > 0 And m_objclsGameDetails.SerialNo <> 1 Then
                If m_dsTouchData.Tables("Touches").Rows.Count > 0 Then
                    Select Case m_objclsGameDetails.SerialNo
                        Case 2
                            dr = m_objGameDetails.PBP.Tables(0).Select("SEQUENCE_NUMBER >= '" & Nevent & "' AND UNIQUE_ID > 0 ", "SEQUENCE_NUMBER")
                        Case 3
                            dr = m_objGameDetails.PBP.Tables(0).Select("SEQUENCE_NUMBER >= '" & Nevent & "' AND UNIQUE_ID > 0 ", "SEQUENCE_NUMBER")
                    End Select
                    If dr.Length > 0 Then
                        For Each drrow As DataRow In dr
                            For Each row As DataGridViewRow In dgvTouches.Rows
                                If CDec(row.Cells.Item("Sequence").Value) >= CDec(drrow.Item("Sequence_Number")) Then
                                    dgvTouches.Rows(row.Index).Selected = True
                                    dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                                    Exit Sub
                                End If
                            Next
                        Next
                    Else
                        btnClear_Click(Nothing, Nothing)
                        For Each row As DataGridViewRow In dgvTouches.Rows
                            dgvTouches.Rows(row.Index).Selected = False
                            dgvTouches.FirstDisplayedScrollingRowIndex = row.Index
                        Next
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgvTouches_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTouches.DoubleClick
        Try
            If dgvTouches.SelectedRows.Count > 0 Then

                If Not String.IsNullOrEmpty(lblValTeam.Text) Or Not String.IsNullOrEmpty(m_strPlayerID) Or Not String.IsNullOrEmpty(lblValLocation.Text) Or Not String.IsNullOrEmpty(lblValType.Text) Then
                    MessageDialog.Show(strmessage19, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If

                If IsPBPEntryAllowed() = False Then
                    Exit Try
                End If

                If m_objclsGameDetails.SerialNo = 2 Then
                    'TOSOCRS-117
                    If dgvTouches.SelectedRows.Count > 0 Then
                        If (CInt(dgvTouches.SelectedRows(0).Cells("Perid").Value) = m_objGameDetails.CurrentPeriod) Then
                            addHomeSide()
                            If m_homeStartDone = False Then
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                ' If m_blnlvwValidate Then
                ClearTouchLabels()
                UdcSoccerField1.USFClearMarks()
                lblSelection1.Text = "-"
                SelectedListViewDisplay_Edit()
                DisableControls(True)
                ' End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub addHomeSide()
        Try
            ''
            Dim DtHomeStart As DataSet
            DtHomeStart = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
            'setStartClock()
            If DtHomeStart.Tables.Count > 0 Then
                If DtHomeStart.Tables(0).Rows.Count > 0 Then
                    If m_objGameDetails.CurrentPeriod < 3 Then
                        If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) Then
                            MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            'show game setup screen
                            Dim m_objHomestart As New frmHomeStart
                            m_objHomestart.ShowDialog()

                            Dim DtHomeStartInfo As DataSet
                            DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            If DtHomeStartInfo.Tables(0).Rows.Count > 0 Then
                                If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                                    'UdcRunningClock1.URCToggle()
                                    'strCurrTime = strTime.Split(CChar(":"))
                                    'UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                    'SetClockControl(False)
                                    'btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                    m_homeStartDone = False
                                    Exit Sub
                                End If

                            End If
                        Else
                            If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                                MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                                'show game setup screen
                                Dim m_objHomestart As New frmHomeStart
                                m_objHomestart.ShowDialog()

                                Dim DtHomeStartInfo As DataSet
                                DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                If DtHomeStartInfo.Tables(0).Rows.Count > 0 Then
                                    If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                                        'UdcRunningClock1.URCToggle()
                                        'strCurrTime = strTime.Split(CChar(":"))
                                        'UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                        'SetClockControl(False)
                                        'btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                        m_homeStartDone = False
                                        Exit Sub
                                    End If

                                End If


                            End If
                        End If

                    Else
                        If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) Then
                            MessageDialog.Show(strmessage53, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            'show game setup screen
                            Dim m_objHomestart As New frmHomeStart
                            m_objHomestart.ShowDialog()

                            Dim DtHomeStartInfo As DataSet
                            DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            If DtHomeStartInfo.Tables(0).Rows.Count > 0 Then
                                If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                                    'UdcRunningClock1.URCToggle()
                                    'strCurrTime = strTime.Split(CChar(":"))
                                    'UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                    'SetClockControl(False)
                                    'btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                    m_homeStartDone = False
                                    Exit Sub
                                End If

                            End If
                        Else
                            If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                                MessageDialog.Show(strmessage53, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                                'show game setup screen
                                Dim m_objHomestart As New frmHomeStart
                                m_objHomestart.ShowDialog()

                                Dim DtHomeStartInfo As DataSet
                                DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                If DtHomeStartInfo.Tables(0).Rows.Count > 0 Then
                                    If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                                        'UdcRunningClock1.URCToggle()
                                        'strCurrTime = strTime.Split(CChar(":"))
                                        'UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                        'SetClockControl(False)
                                        'btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                        m_homeStartDone = False
                                        Exit Sub
                                    End If

                                End If
                            End If
                        End If

                    End If
                Else
                    MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                    'show game setup screen
                    Dim m_objHomestart As New frmHomeStart
                    m_objHomestart.ShowDialog()

                    Dim DtHomeStartInfo As DataSet
                    DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                    If DtHomeStartInfo.Tables(0).Rows.Count > 0 Then
                        If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                            'UdcRunningClock1.URCToggle()
                            'strCurrTime = strTime.Split(CChar(":"))
                            'UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            'SetClockControl(False)
                            'btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            m_homeStartDone = False
                            Exit Sub
                        End If
                    Else
                        m_homeStartDone = False
                        Exit Sub
                    End If

                End If

            Else
                MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                'show game setup screen
                Dim m_objHomestart As New frmHomeStart
                m_objHomestart.ShowDialog()

                Dim DtHomeStartInfo As DataSet
                DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                If DtHomeStartInfo.Tables(0).Rows.Count > 0 Then
                    If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                        'UdcRunningClock1.URCToggle()
                        'strCurrTime = strTime.Split(CChar(":"))
                        'UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                        'SetClockControl(False)
                        'btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                        m_homeStartDone = False
                        Exit Sub
                    End If
                Else
                    m_homeStartDone = False
                    Exit Sub
                End If

            End If
            m_homeStartDone = True
            ''
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub dgvTouches_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTouches.CellContentClick
        Try
            If dgvTouches.Columns(e.ColumnIndex).HeaderText = "Revisit" Then
                Dim isChecked As Boolean = CBool(dgvTouches.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                If isChecked = False Then
                    dgvTouches.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True

                    Dim listselectDatarow As DataRow()
                    Dim m_alldata As DataSet
                    m_alldata = m_objclsTouches.GetTouchesAll(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsGameDetails.languageid, m_objclsGameDetails.SerialNo)
                    listselectDatarow = m_alldata.Tables("Touches").Select("SEQUENCE_NUMBER='" & CDec(dgvTouches.Rows(e.RowIndex).Cells("Sequence").Value) & "'")
                    If listselectDatarow.Length > 0 Then
                        If m_objclsGameDetails.SerialNo = 2 Then
                            If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.AwayTeamID Then
                                    MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    dgvTouches.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                                    Exit Sub
                                End If
                            End If
                        ElseIf m_objclsGameDetails.SerialNo = 3 Then
                            If listselectDatarow(0).Item("TEAM_ID1") IsNot DBNull.Value Then
                                If CInt(listselectDatarow(0).Item("TEAM_ID1")) = m_objclsGameDetails.HomeTeamID Then
                                    MessageDialog.Show(strmessage28, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    dgvTouches.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If


                    Dim drPBPData As DataRow
                    If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                        Dim drPBP As DataRow()
                        drPBP = m_objGameDetails.PBP.Tables(0).Select("SEQUENCE_NUMBER = '" & CDec(dgvTouches.Rows(e.RowIndex).Cells("Sequence").Value) & "'")
                        If drPBP.Length > 0 Then
                            For Each dr As DataRow In drPBP
                                dr("UNIQUE_ID") = CInt(dgvTouches.Rows(e.RowIndex).Cells("Uid").Value)
                                dr("SEQUENCE_NUMBER") = dgvTouches.Rows(e.RowIndex).Cells("Sequence").Value
                                dr("TIME_ELAPSED") = dgvTouches.Rows(e.RowIndex).Cells("Time").Value
                                dr("PERIOD") = dgvTouches.Rows(e.RowIndex).Cells("Perid").Value
                                dr("FLAG") = "Y"
                                dr.AcceptChanges()
                            Next
                        Else
                            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
                            drPBPData("UNIQUE_ID") = CInt(dgvTouches.Rows(e.RowIndex).Cells("Uid").Value)
                            drPBPData("SEQUENCE_NUMBER") = dgvTouches.Rows(e.RowIndex).Cells("Sequence").Value
                            drPBPData("TIME_ELAPSED") = dgvTouches.Rows(e.RowIndex).Cells("Time").Value
                            drPBPData("PERIOD") = dgvTouches.Rows(e.RowIndex).Cells("Perid").Value
                            drPBPData("FLAG") = "Y"
                            m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                        End If
                    Else
                        drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
                        drPBPData("UNIQUE_ID") = CInt(dgvTouches.Rows(e.RowIndex).Cells("Uid").Value)
                        drPBPData("SEQUENCE_NUMBER") = dgvTouches.Rows(e.RowIndex).Cells("Sequence").Value
                        drPBPData("TIME_ELAPSED") = dgvTouches.Rows(e.RowIndex).Cells("Time").Value
                        drPBPData("PERIOD") = dgvTouches.Rows(e.RowIndex).Cells("Perid").Value
                        drPBPData("FLAG") = "Y"
                        m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                    End If
                Else
                    dgvTouches.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                    Dim drPBPData As DataRow()
                    drPBPData = m_objGameDetails.PBP.Tables(0).Select("SEQUENCE_NUMBER = '" & CDec(dgvTouches.Rows(e.RowIndex).Cells("Sequence").Value) & "'")
                    If drPBPData.Length > 0 Then
                        For Each dr As DataRow In drPBPData
                            dr.Item("FLAG") = "N"
                            dr.AcceptChanges()
                        Next
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    'TOSOCRS-64 [Touch Software-Autofill GoalKeeper for GK Touches]
    Private Sub ClearPlayerSelection()
        Try
            DisableOtherButtonColor(btnClear.Name)
            m_strPlayerID = String.Empty
            m_boolPlayer1Filled = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
 
End Class



