﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : udcSoccerPlayerPositions
' Author        : Fiaz Ahmed
' Created Date  : 21st Dec 2015
' Description   : This user control is a player selection container where user can customize player positions
'                 by drag-and-drop via mouse+alt key.
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

#Region " Imports "
Imports System.ComponentModel
Imports STATS.SoccerBL
Imports STATS.Utility
#End Region

Public Class udcSoccerPlayerPositions

#Region " Constants & Variables "
    Private Const MIN_GOALIE_RELATIVE_WIDTH As Double = 61 / 397
    Private Const HIGHLIGHT_COLOR As String = "GOLD"
    Private m_blnLeftToRight As Boolean = True
    Private m_intMinGoalieWidth As Integer
    Private m_intGoalieWidth As Integer
    Private m_intPlayerWidth As Integer
    Private m_intGoalieHeight As Integer
    Private m_intPlayerHeight As Integer
    Private m_intMidrowPlayerHeight As Integer

    Private m_rctBorder As Rectangle
    Private m_ptNewLocation As Point

    Private m_enmDefaultFormation As Formation = Formation.f_442 'fff
    Private m_enmFormation As Formation = Formation.f_442 'fff

    Private dictCellCoordinates As Dictionary(Of String, Point)
    Private dictFormationCoordinates As Dictionary(Of Formation, String)
    Private dictPlayerPositions As Dictionary(Of Integer, String)

    Private blnDragging As Boolean
    Private intBeginX, intBeginY As Integer
    Private m_clrButtonBackColor As Color = Color.WhiteSmoke
    Private m_imgButtonBackgroundImage As Image
    Private m_imlButtonBackgroundImageLayout As ImageLayout
    Private m_dtPlayerDetails As DataTable
    Private _playerTable As New List(Of ClsPlayerData.PlayerData)
    Private m_intTeamId As Integer = 0

    Private m_StrPrevPositionKey As String
    Private m_intHighlightedButton As Integer = -1

    Public Event USPButtonClick(ByVal sender As Object, ByVal e As USPEventArgs)
    Public Event USPMouseClick(ByVal sender As Object, ByVal e As MouseEventArgs)
#End Region

#Region " Event handlers "

    Private Sub udcSoccerPlayerPositions_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            DockPicField()
            dictCellCoordinates = New Dictionary(Of String, Point)                    'all button positions in grid
            dictFormationCoordinates = New Dictionary(Of Formation, String)           'all formation positions
            dictPlayerPositions = New Dictionary(Of Integer, String)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub udcSoccerPlayerPositions_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Try
            DockPicField()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picField_MouseClick(sender As Object, e As MouseEventArgs) Handles picField.MouseClick
        Try
            RaiseEvent USPMouseClick(sender, e)  'exposing mouse click on button background for TOPZ-423
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picField_Paint(sender As Object, e As PaintEventArgs) Handles picField.Paint
        Try
            If Not blnDragging Then
                CalculateParameters()
            End If
            DrawGrid(e)
            If Not blnDragging Then
                PlaceButtons(m_enmFormation)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnPlayer_Click(sender As Object, e As EventArgs) Handles btnGoalie.Click, btnPlayer1.Click, btnPlayer2.Click, btnPlayer3.Click, btnPlayer4.Click, btnPlayer5.Click, btnPlayer6.Click, btnPlayer7.Click, btnPlayer8.Click, btnPlayer9.Click, btnPlayer10.Click
        Try
            Dim MyEventArgs As USPEventArgs
            Dim intPlayerId As Integer

            'to avoid spacebar player selection
            If e.Equals(EventArgs.Empty) Then
                Exit Sub
            End If

            If Control.ModifierKeys = Keys.Alt Then
                Exit Sub
            End If

            intPlayerId = CInt(CType(sender, Button).Tag)
            If intPlayerId > 0 Then
                UnhighlightButtons()
                CType(sender, Button).BackColor = Color.FromName(HIGHLIGHT_COLOR)
                CType(sender, Button).ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))

                If IsNumeric(CType(sender, Button).Name.Substring(CType(sender, Button).Name.Length - 1, 1)) Then
                    If IsNumeric(CType(sender, Button).Name.Substring(CType(sender, Button).Name.Length - 2, 2)) Then
                        m_intHighlightedButton = CInt(CType(sender, Button).Name.Substring(CType(sender, Button).Name.Length - 2, 2)) 'player 10
                    Else
                        m_intHighlightedButton = CInt(CType(sender, Button).Name.Substring(CType(sender, Button).Name.Length - 1, 1)) 'players 1 through 9
                    End If
                Else
                    m_intHighlightedButton = 0 'goalie
                End If
                MyEventArgs = New USPEventArgs(CInt(CType(sender, Button).Tag))
                RaiseEvent USPButtonClick(sender, MyEventArgs)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnPlayer_MouseDown(sender As Object, e As MouseEventArgs) Handles btnPlayer1.MouseDown, btnPlayer2.MouseDown, btnPlayer3.MouseDown, btnPlayer4.MouseDown, btnPlayer5.MouseDown, btnPlayer6.MouseDown, btnPlayer7.MouseDown, btnPlayer8.MouseDown, btnPlayer9.MouseDown, btnPlayer10.MouseDown
        Try
            Dim intPlayerNumber As Integer = -1

            m_StrPrevPositionKey = ""

            If Control.ModifierKeys = Keys.Alt Then
                blnDragging = True
                intBeginX = e.X
                intBeginY = e.Y
                CType(sender, Button).BringToFront()
                intPlayerNumber = CInt(CType(sender, Button).Name.Substring(CType(sender, Button).Name.Length - 1, 1))
                If intPlayerNumber = 0 Then
                    intPlayerNumber = 10    'player 10, substring of last one char returns 0
                End If
                m_StrPrevPositionKey = dictPlayerPositions(intPlayerNumber)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnPlayer_MouseMove(sender As Object, e As MouseEventArgs) Handles btnPlayer1.MouseMove, btnPlayer2.MouseMove, btnPlayer3.MouseMove, btnPlayer4.MouseMove, btnPlayer5.MouseMove, btnPlayer6.MouseMove, btnPlayer7.MouseMove, btnPlayer8.MouseMove, btnPlayer9.MouseMove, btnPlayer10.MouseMove
        Try
            If blnDragging Then
                m_ptNewLocation = New Point(CType(sender, Button).Location.X + e.X - intBeginX, CType(sender, Button).Location.Y + e.Y - intBeginY)
                CType(sender, Button).Location = m_ptNewLocation
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnPlayer_MouseUp(sender As Object, e As MouseEventArgs) Handles btnPlayer1.MouseUp, btnPlayer2.MouseUp, btnPlayer3.MouseUp, btnPlayer4.MouseUp, btnPlayer5.MouseUp, btnPlayer6.MouseUp, btnPlayer7.MouseUp, btnPlayer8.MouseUp, btnPlayer9.MouseUp, btnPlayer10.MouseUp
        Try
            Dim intPlayerNumber As Integer = -1
            Dim intMovLocation As Point
            Dim intExistingPlayer As Integer = -1
            Dim blnGotIt As Boolean = False

            RaiseEvent USPMouseClick(sender, e)  'exposing mouse click on buttons for TOPZ-423

            If blnDragging = True Then
                intPlayerNumber = CInt(CType(sender, Button).Name.Substring(CType(sender, Button).Name.Length - 1, 1))
                If intPlayerNumber = 0 Then
                    intPlayerNumber = 10    'player 10, substring of last one char returns 0
                End If

                intMovLocation = m_ptNewLocation

                For Each cckvp As KeyValuePair(Of String, Point) In dictCellCoordinates
                    If cckvp.Key <> "LeftGoalie" And cckvp.Key <> "RightGoalie" Then
                        If intMovLocation.X <= cckvp.Value.X + (m_intPlayerWidth / 2) And intMovLocation.Y <= cckvp.Value.Y + (m_intPlayerHeight / 2) And ((intMovLocation.X > m_intGoalieWidth And m_blnLeftToRight = True) Or (m_blnLeftToRight = False)) Then
                            intMovLocation = New Point(cckvp.Value.X, cckvp.Value.Y)

                            'check if player already exist in the dropped position
                            For Each ppkvp As KeyValuePair(Of Integer, String) In dictPlayerPositions
                                If ppkvp.Value = cckvp.Key Then
                                    intExistingPlayer = ppkvp.Key
                                    'set new position for existing player in dictPlayerPositions
                                    dictPlayerPositions(intExistingPlayer) = m_StrPrevPositionKey
                                    Exit For
                                End If
                            Next

                            'set new position for dragged player in dictPlayerPositions
                            dictPlayerPositions(intPlayerNumber) = cckvp.Key

                            'set height for dragged player
                            If cckvp.Key.Contains("R3") Then
                                CType(sender, Button).Height = m_intMidrowPlayerHeight + 1
                            Else
                                CType(sender, Button).Height = m_intPlayerHeight + 1
                            End If
                            blnGotIt = True
                            Exit For
                        End If
                    End If
                Next

                If blnGotIt Then
                    'move dragged player
                    CType(sender, Button).Location = intMovLocation

                    'move exisitng player if any (swap)
                    If intExistingPlayer <> -1 Then
                        Select Case intExistingPlayer
                            Case 1
                                btnPlayer1.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer1.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer1.Height = m_intPlayerHeight + 1
                                End If
                            Case 2
                                btnPlayer2.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer2.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer2.Height = m_intPlayerHeight + 1
                                End If
                            Case 3
                                btnPlayer3.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer3.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer3.Height = m_intPlayerHeight + 1
                                End If
                            Case 4
                                btnPlayer4.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer4.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer4.Height = m_intPlayerHeight + 1
                                End If
                            Case 5
                                btnPlayer5.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer5.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer5.Height = m_intPlayerHeight + 1
                                End If
                            Case 6
                                btnPlayer6.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer6.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer6.Height = m_intPlayerHeight + 1
                                End If
                            Case 7
                                btnPlayer7.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer7.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer7.Height = m_intPlayerHeight + 1
                                End If
                            Case 8
                                btnPlayer8.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer8.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer8.Height = m_intPlayerHeight + 1
                                End If
                            Case 9
                                btnPlayer9.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer9.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer9.Height = m_intPlayerHeight + 1
                                End If
                            Case 10
                                btnPlayer10.Location = dictCellCoordinates(m_StrPrevPositionKey)
                                If m_StrPrevPositionKey.Contains("R3") Then
                                    btnPlayer10.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer10.Height = m_intPlayerHeight + 1
                                End If
                        End Select
                    End If
                    m_enmFormation = Formation.f_Custom
                Else
                    'revert dragged player
                    CType(sender, Button).Location = dictCellCoordinates(dictPlayerPositions(intPlayerNumber))
                End If
                Me.Refresh()
                blnDragging = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Private methods "

    'TOPZ-1284
    Private Function FlipFormationCoordinate(coord As String) As String
        Try
            If coord.Contains("Left") Then
                Select Case coord
                    Case "LeftGoalie"
                        Return "RightGoalie"
                    Case "LeftR1C1"
                        Return "RightR1C5"
                    Case "LeftR2C1"
                        Return "RightR2C5"
                    Case "LeftR3C1"
                        Return "RightR3C5"
                    Case "LeftR4C1"
                        Return "RightR4C5"
                    Case "LeftR5C1"
                        Return "RightR5C5"
                    Case "LeftR1C2"
                        Return "RightR1C4"
                    Case "LeftR2C2"
                        Return "RightR2C4"
                    Case "LeftR3C2"
                        Return "RightR3C4"
                    Case "LeftR4C2"
                        Return "RightR4C4"
                    Case "LeftR5C2"
                        Return "RightR5C4"
                    Case "LeftR1C3"
                        Return "RightR1C3"
                    Case "LeftR2C3"
                        Return "RightR2C3"
                    Case "LeftR3C3"
                        Return "RightR3C3"
                    Case "LeftR4C3"
                        Return "RightR4C3"
                    Case "LeftR5C3"
                        Return "RightR5C3"
                    Case "LeftR1C4"
                        Return "RightR1C2"
                    Case "LeftR2C4"
                        Return "RightR2C2"
                    Case "LeftR3C4"
                        Return "RightR3C2"
                    Case "LeftR4C4"
                        Return "RightR4C2"
                    Case "LeftR5C4"
                        Return "RightR5C2"
                    Case "LeftR1C5"
                        Return "RightR1C1"
                    Case "LeftR2C5"
                        Return "RightR2C1"
                    Case "LeftR3C5"
                        Return "RightR3C1"
                    Case "LeftR4C5"
                        Return "RightR4C1"
                    Case "LeftR5C5"
                        Return "RightR5C1"
                    Case Else
                        Return coord
                End Select
            ElseIf coord.Contains("Right") Then
                Select Case coord
                    Case "RightGoalie"
                        Return "LeftGoalie"
                    Case "RightR1C5"
                        Return "LeftR1C1"
                    Case "RightR2C5"
                        Return "LeftR2C1"
                    Case "RightR3C5"
                        Return "LeftR3C1"
                    Case "RightR4C5"
                        Return "LeftR4C1"
                    Case "RightR5C5"
                        Return "LeftR5C1"
                    Case "RightR1C4"
                        Return "LeftR1C2"
                    Case "RightR2C4"
                        Return "LeftR2C2"
                    Case "RightR3C4"
                        Return "LeftR3C2"
                    Case "RightR4C4"
                        Return "LeftR4C2"
                    Case "RightR5C4"
                        Return "LeftR5C2"
                    Case "RightR1C3"
                        Return "LeftR1C3"
                    Case "RightR2C3"
                        Return "LeftR2C3"
                    Case "RightR3C3"
                        Return "LeftR3C3"
                    Case "RightR4C3"
                        Return "LeftR4C3"
                    Case "RightR5C3"
                        Return "LeftR5C3"
                    Case "RightR1C2"
                        Return "LeftR1C4"
                    Case "RightR2C2"
                        Return "LeftR2C4"
                    Case "RightR3C2"
                        Return "LeftR3C4"
                    Case "RightR4C2"
                        Return "LeftR4C4"
                    Case "RightR5C2"
                        Return "LeftR5C4"
                    Case "RightR1C1"
                        Return "LeftR1C5"
                    Case "RightR2C1"
                        Return "LeftR2C5"
                    Case "RightR3C1"
                        Return "LeftR3C5"
                    Case "RightR4C1"
                        Return "LeftR4C5"
                    Case "RightR5C1"
                        Return "LeftR5C5"
                    Case Else
                        Return coord
                End Select
            Else
                Return coord
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub DockPicField()
        Try
            picField.Size = Me.Size
            picField.Top = 0
            picField.Left = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CalculateParameters()
        Try
            Dim strCF As String = ""

            m_intMinGoalieWidth = CInt(Int(picField.Width * MIN_GOALIE_RELATIVE_WIDTH)) 'minimum width expected for goalie button
            m_intPlayerWidth = CInt(Int(picField.Width - m_intMinGoalieWidth) / 5)      'width of player button
            m_intGoalieWidth = picField.Width - (m_intPlayerWidth * 5)                  'width of goalie button
            m_intGoalieHeight = picField.Height                                         'height of goalie button
            m_intPlayerHeight = CInt(Int(picField.Height / 5))                          'height of all player buttons except middle row
            m_intMidrowPlayerHeight = picField.Height - (m_intPlayerHeight * 4)         'height of player buttons in middle row

            dictCellCoordinates.Clear()
            dictFormationCoordinates.Clear()
            If m_blnLeftToRight = True Then
                dictCellCoordinates.Add("LeftGoalie", New Point(-1, -1))                  'left goalie
                dictCellCoordinates.Add("LeftR1C1", New Point(m_intGoalieWidth - 1, -1))
                dictCellCoordinates.Add("LeftR1C2", New Point(m_intGoalieWidth + m_intPlayerWidth - 1, -1))
                dictCellCoordinates.Add("LeftR1C3", New Point(m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, -1))
                dictCellCoordinates.Add("LeftR1C4", New Point(m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, -1))
                dictCellCoordinates.Add("LeftR1C5", New Point(m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, -1))
                dictCellCoordinates.Add("LeftR2C1", New Point(m_intGoalieWidth - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR2C2", New Point(m_intGoalieWidth + m_intPlayerWidth - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR2C3", New Point(m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR2C4", New Point(m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR2C5", New Point(m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR3C1", New Point(m_intGoalieWidth - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("LeftR3C2", New Point(m_intGoalieWidth + m_intPlayerWidth - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("LeftR3C3", New Point(m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("LeftR3C4", New Point(m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("LeftR3C5", New Point(m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("LeftR4C1", New Point(m_intGoalieWidth - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR4C2", New Point(m_intGoalieWidth + m_intPlayerWidth - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR4C3", New Point(m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR4C4", New Point(m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR4C5", New Point(m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR5C1", New Point(m_intGoalieWidth - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR5C2", New Point(m_intGoalieWidth + m_intPlayerWidth - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR5C3", New Point(m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR5C4", New Point(m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("LeftR5C5", New Point(m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))

                dictFormationCoordinates.Add(Formation.f_442, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR5C2,LeftR4C2,LeftR2C2,LeftR1C2,LeftR4C3,LeftR2C3")
                dictFormationCoordinates.Add(Formation.f_532, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR3C1,LeftR2C1,LeftR1C1,LeftR4C2,LeftR3C2,LeftR2C2,LeftR4C3,LeftR2C3")
                dictFormationCoordinates.Add(Formation.f_352, "LeftGoalie,LeftR4C1,LeftR3C1,LeftR2C1,LeftR5C2,LeftR4C2,LeftR3C2,LeftR2C2,LeftR1C2,LeftR4C3,LeftR2C3")
                dictFormationCoordinates.Add(Formation.f_451, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR5C2,LeftR4C2,LeftR3C2,LeftR2C2,LeftR1C2,LeftR3C3")
                dictFormationCoordinates.Add(Formation.f_433, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR4C2,LeftR3C2,LeftR2C2,LeftR4C3,LeftR3C3,LeftR2C3")
                dictFormationCoordinates.Add(Formation.f_343, "LeftGoalie,LeftR4C1,LeftR3C1,LeftR2C1,LeftR5C2,LeftR4C2,LeftR2C2,LeftR1C2,LeftR4C3,LeftR3C3,LeftR2C3")
                dictFormationCoordinates.Add(Formation.f_4141, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR3C2,LeftR5C3,LeftR4C3,LeftR2C3,LeftR1C3,LeftR3C4")
                dictFormationCoordinates.Add(Formation.f_4321, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR4C2,LeftR3C2,LeftR2C2,LeftR4C3,LeftR2C3,LeftR3C4")
                dictFormationCoordinates.Add(Formation.f_4411, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR5C2,LeftR4C2,LeftR2C2,LeftR1C2,LeftR3C3,LeftR3C4")
                dictFormationCoordinates.Add(Formation.f_4123, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR3C2,LeftR4C3,LeftR2C3,LeftR4C4,LeftR3C4,LeftR2C4")
                dictFormationCoordinates.Add(Formation.f_541, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR3C1,LeftR2C1,LeftR1C1,LeftR5C2,LeftR4C2,LeftR2C2,LeftR1C2,LeftR3C3")
                dictFormationCoordinates.Add(Formation.f_4132, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR3C2,LeftR4C3,LeftR3C3,LeftR2C3,LeftR4C4,LeftR2C4")
                dictFormationCoordinates.Add(Formation.f_4231, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR4C2,LeftR2C2,LeftR4C3,LeftR3C3,LeftR2C3,LeftR3C4")
                dictFormationCoordinates.Add(Formation.f_4312, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR4C2,LeftR3C2,LeftR2C2,LeftR3C3,LeftR4C4,LeftR2C4")
                dictFormationCoordinates.Add(Formation.f_3511, "LeftGoalie,LeftR4C1,LeftR3C1,LeftR2C1,LeftR5C2,LeftR4C2,LeftR3C2,LeftR2C2,LeftR1C2,LeftR3C3,LeftR3C4")
                dictFormationCoordinates.Add(Formation.f_3421, "LeftGoalie,LeftR4C1,LeftR3C1,LeftR2C1,LeftR5C2,LeftR4C2,LeftR2C2,LeftR1C2,LeftR4C3,LeftR2C3,LeftR3C4")
                dictFormationCoordinates.Add(Formation.f_3412, "LeftGoalie,LeftR4C1,LeftR3C1,LeftR2C1,LeftR5C2,LeftR4C2,LeftR2C2,LeftR1C2,LeftR3C3,LeftR4C4,LeftR2C4")
                dictFormationCoordinates.Add(Formation.f_3142, "LeftGoalie,LeftR4C1,LeftR3C1,LeftR2C1,LeftR3C2,LeftR5C3,LeftR4C3,LeftR2C3,LeftR1C3,LeftR4C4,LeftR2C4")
                dictFormationCoordinates.Add(Formation.f_41212, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR2C1,LeftR1C1,LeftR3C2,LeftR4C3,LeftR2C3,LeftR3C4,LeftR4C5,LeftR2C5")
                dictFormationCoordinates.Add(Formation.f_3331, "LeftGoalie,LeftR5C1,LeftR3C1,LeftR1C1,LeftR5C2,LeftR3C2,LeftR1C2,LeftR5C3,LeftR3C3,LeftR1C3,LeftR3C4") 'TOPZ-515
                dictFormationCoordinates.Add(Formation.f_33211, "LeftGoalie,LeftR5C1,LeftR3C1,LeftR1C1,LeftR5C2,LeftR3C2,LeftR1C2,LeftR4C3,LeftR2C3,LeftR3C4,LeftR3C5") 'TOPZ-515
                dictFormationCoordinates.Add(Formation.f_5131, "LeftGoalie,LeftR5C1,LeftR4C1,LeftR3C1,LeftR2C1,LeftR1C1,LeftR3C2,LeftR5C3,LeftR3C3,LeftR1C3,LeftR3C4") 'TOPZ-515
                dictFormationCoordinates.Add(Formation.f_3241, "LeftGoalie,LeftR5C1,LeftR3C1,LeftR1C1,LeftR4C2,LeftR2C2,LeftR5C3,LeftR4C3,LeftR2C3,LeftR1C3,LeftR3C4") 'TOPZ-515
                strCF = "LeftGoalie"

                'TOPZ-1284
                For i As Integer = 1 To dictPlayerPositions.Count
                    If dictPlayerPositions(i).Contains("Right") Then
                        dictPlayerPositions(i) = FlipFormationCoordinate(dictPlayerPositions(i))
                        strCF = strCF & "," & dictPlayerPositions(i)
                    Else
                        strCF = strCF & "," & dictPlayerPositions(i)  'TOPZ-1301
                    End If
                Next
                'For Each ppkvp As KeyValuePair(Of Integer, String) In dictPlayerPositions
                '    If ppkvp.Value.Contains("Right") Then
                '        dictPlayerPositions(ppkvp.Key) = FlipFormationCoordinate(ppkvp.Value)
                '        strCF = strCF & "," & FlipFormationCoordinate(ppkvp.Value)
                '    Else
                '        strCF = strCF & "," & ppkvp.Value
                '    End If
                'Next
                dictFormationCoordinates.Add(Formation.f_Custom, strCF)
            Else
                dictCellCoordinates.Add("RightGoalie", New Point((picField.Width - m_intGoalieWidth) - 1, -1))  'right goalie
                dictCellCoordinates.Add("RightR1C1", New Point(-1, -1))
                dictCellCoordinates.Add("RightR1C2", New Point(m_intPlayerWidth - 1, -1))
                dictCellCoordinates.Add("RightR1C3", New Point((m_intPlayerWidth * 2) - 1, -1))
                dictCellCoordinates.Add("RightR1C4", New Point((m_intPlayerWidth * 3) - 1, -1))
                dictCellCoordinates.Add("RightR1C5", New Point((m_intPlayerWidth * 4) - 1, -1))
                dictCellCoordinates.Add("RightR2C1", New Point(-1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("RightR2C2", New Point(m_intPlayerWidth - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("RightR2C3", New Point((m_intPlayerWidth * 2) - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("RightR2C4", New Point((m_intPlayerWidth * 3) - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("RightR2C5", New Point((m_intPlayerWidth * 4) - 1, m_intPlayerHeight - 1))
                dictCellCoordinates.Add("RightR3C1", New Point(-1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("RightR3C2", New Point(m_intPlayerWidth - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("RightR3C3", New Point((m_intPlayerWidth * 2) - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("RightR3C4", New Point((m_intPlayerWidth * 3) - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("RightR3C5", New Point((m_intPlayerWidth * 4) - 1, (m_intPlayerHeight * 2) - 1))
                dictCellCoordinates.Add("RightR4C1", New Point(-1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR4C2", New Point(m_intPlayerWidth - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR4C3", New Point((m_intPlayerWidth * 2) - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR4C4", New Point((m_intPlayerWidth * 3) - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR4C5", New Point((m_intPlayerWidth * 4) - 1, (m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR5C1", New Point(-1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR5C2", New Point(m_intPlayerWidth - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR5C3", New Point((m_intPlayerWidth * 2) - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR5C4", New Point((m_intPlayerWidth * 3) - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))
                dictCellCoordinates.Add("RightR5C5", New Point((m_intPlayerWidth * 4) - 1, (m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1))

                dictFormationCoordinates.Add(Formation.f_442, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR1C4,RightR2C4,RightR4C4,RightR5C4,RightR2C3,RightR4C3")
                dictFormationCoordinates.Add(Formation.f_532, "RightGoalie,RightR1C5,RightR2C5,RightR3C5,RightR4C5,RightR5C5,RightR2C4,RightR3C4,RightR4C4,RightR2C3,RightR4C3")
                dictFormationCoordinates.Add(Formation.f_352, "RightGoalie,RightR2C5,RightR3C5,RightR4C5,RightR1C4,RightR2C4,RightR3C4,RightR4C4,RightR5C4,RightR2C3,RightR4C3")
                dictFormationCoordinates.Add(Formation.f_451, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR1C4,RightR2C4,RightR3C4,RightR4C4,RightR5C4,RightR3C3")
                dictFormationCoordinates.Add(Formation.f_433, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR2C4,RightR3C4,RightR4C4,RightR2C3,RightR3C3,RightR4C3")
                dictFormationCoordinates.Add(Formation.f_343, "RightGoalie,RightR2C5,RightR3C5,RightR4C5,RightR1C4,RightR2C4,RightR4C4,RightR5C4,RightR2C3,RightR3C3,RightR4C3")
                dictFormationCoordinates.Add(Formation.f_4141, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR3C4,RightR1C3,RightR2C3,RightR4C3,RightR5C3,RightR3C2")
                dictFormationCoordinates.Add(Formation.f_4321, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR2C4,RightR3C4,RightR4C4,RightR2C3,RightR4C3,RightR3C2")
                dictFormationCoordinates.Add(Formation.f_4411, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR1C4,RightR2C4,RightR4C4,RightR5C4,RightR3C3,RightR3C2")
                dictFormationCoordinates.Add(Formation.f_4123, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR3C4,RightR2C3,RightR4C3,RightR2C2,RightR3C2,RightR4C2")
                dictFormationCoordinates.Add(Formation.f_541, "RightGoalie,RightR1C5,RightR2C5,RightR3C5,RightR4C5,RightR5C5,RightR1C4,RightR2C4,RightR4C4,RightR5C4,RightR3C3")
                dictFormationCoordinates.Add(Formation.f_4132, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR3C4,RightR2C3,RightR3C3,RightR4C3,RightR2C2,RightR4C2")
                dictFormationCoordinates.Add(Formation.f_4231, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR2C4,RightR4C4,RightR2C3,RightR3C3,RightR4C3,RightR3C2")
                dictFormationCoordinates.Add(Formation.f_4312, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR2C4,RightR3C4,RightR4C4,RightR3C3,RightR2C2,RightR4C2")
                dictFormationCoordinates.Add(Formation.f_3511, "RightGoalie,RightR2C5,RightR3C5,RightR4C5,RightR1C4,RightR2C4,RightR3C4,RightR4C4,RightR5C4,RightR3C3,RightR3C2")
                dictFormationCoordinates.Add(Formation.f_3421, "RightGoalie,RightR2C5,RightR3C5,RightR4C5,RightR1C4,RightR2C4,RightR4C4,RightR5C4,RightR2C3,RightR4C3,RightR3C2")
                dictFormationCoordinates.Add(Formation.f_3412, "RightGoalie,RightR2C5,RightR3C5,RightR4C5,RightR1C4,RightR2C4,RightR4C4,RightR5C4,RightR3C3,RightR2C2,RightR4C2")
                dictFormationCoordinates.Add(Formation.f_3142, "RightGoalie,RightR2C5,RightR3C5,RightR4C5,RightR3C4,RightR1C3,RightR2C3,RightR4C3,RightR5C3,RightR2C2,RightR4C2")
                dictFormationCoordinates.Add(Formation.f_41212, "RightGoalie,RightR1C5,RightR2C5,RightR4C5,RightR5C5,RightR3C4,RightR2C3,RightR4C3,RightR3C2,RightR2C1,RightR4C1")
                dictFormationCoordinates.Add(Formation.f_3331, "RightGoalie,RightR1C5,RightR3C5,RightR5C5,RightR1C4,RightR3C4,RightR5C4,RightR1C3,RightR3C3,RightR5C3,RightR3C2") 'TOPZ-515
                dictFormationCoordinates.Add(Formation.f_33211, "RightGoalie,RightR1C5,RightR3C5,RightR5C5,RightR1C4,RightR3C4,RightR5C4,RightR2C3,RightR4C3,RightR3C2,RightR3C1") 'TOPZ-515
                dictFormationCoordinates.Add(Formation.f_5131, "RightGoalie,RightR1C5,RightR2C5,RightR3C5,RightR4C5,RightR5C5,RightR3C4,RightR1C3,RightR3C3,RightR5C3,RightR3C2") 'TOPZ-515
                dictFormationCoordinates.Add(Formation.f_3241, "RightGoalie,RightR1C5,RightR3C5,RightR5C5,RightR2C4,RightR4C4,RightR1C3,RightR2C3,RightR4C3,RightR5C3,RightR3C2") 'TOPZ-515
                strCF = "RightGoalie"

                'TOPZ-1284
                For i As Integer = 1 To dictPlayerPositions.Count
                    If dictPlayerPositions(i).Contains("Left") Then
                        dictPlayerPositions(i) = FlipFormationCoordinate(dictPlayerPositions(i))
                        strCF = strCF & "," & dictPlayerPositions(i)
                    Else
                        strCF = strCF & "," & dictPlayerPositions(i)  'TOPZ-1301
                    End If
                Next
                'For Each ppkvp As KeyValuePair(Of Integer, String) In dictPlayerPositions
                '    If ppkvp.Value.Contains("Left") Then
                '        dictPlayerPositions(ppkvp.Key) = FlipFormationCoordinate(ppkvp.Value)
                '        strCF = strCF & "," & FlipFormationCoordinate(ppkvp.Value)
                '    Else
                '        strCF = strCF & "," & ppkvp.Value
                '    End If
                'Next
                dictFormationCoordinates.Add(Formation.f_Custom, strCF)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DrawGrid(ByVal e As System.Windows.Forms.PaintEventArgs)
        Try
            Dim pnDrawing As Pen
            Dim clrPenColor As Color = Color.Gray
            Dim intPenWidth As Integer = 1

            pnDrawing = New Pen(Color:=clrPenColor, Width:=intPenWidth)
            pnDrawing.DashStyle = Drawing2D.DashStyle.Dot

            e.Graphics.Clear(Color.WhiteSmoke)

            'draw a border
            m_rctBorder = New Rectangle(x:=0, y:=0, Width:=picField.Width - 1, Height:=picField.Height - 1)
            e.Graphics.DrawRectangle(pen:=pnDrawing, rect:=m_rctBorder)

            If m_blnLeftToRight = True Then
                'Goalie on left of screen
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth - 1, y1:=-1, x2:=m_intGoalieWidth - 1, y2:=m_intGoalieHeight - 1) 'vertical line 1
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=(m_intGoalieWidth + m_intPlayerWidth) - 1, y1:=-1, x2:=(m_intGoalieWidth + m_intPlayerWidth) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 2
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, y1:=-1, x2:=m_intGoalieWidth + (m_intPlayerWidth * 2) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 3
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, y1:=-1, x2:=m_intGoalieWidth + (m_intPlayerWidth * 3) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 4
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, y1:=-1, x2:=m_intGoalieWidth + (m_intPlayerWidth * 4) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 5
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth - 1, y1:=m_intPlayerHeight - 1, x2:=picField.Width - 1, y2:=m_intPlayerHeight - 1) 'horizontal line 1
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth - 1, y1:=(m_intPlayerHeight * 2) - 1, x2:=picField.Width - 1, y2:=(m_intPlayerHeight * 2) - 1) 'horizontal line 2
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth - 1, y1:=(m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1, x2:=picField.Width - 1, y2:=(m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1) 'horizontal line 3
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intGoalieWidth - 1, y1:=(m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1, x2:=picField.Width - 1, y2:=(m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1) 'horizontal line 4
            Else
                'Goalie on right of screen
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=m_intPlayerWidth - 1, y1:=-1, x2:=m_intPlayerWidth - 1, y2:=m_intGoalieHeight - 1) 'vertical line 1
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=(m_intPlayerWidth * 2) - 1, y1:=-1, x2:=(m_intPlayerWidth * 2) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 2
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=(m_intPlayerWidth * 3) - 1, y1:=-1, x2:=(m_intPlayerWidth * 3) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 3
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=(m_intPlayerWidth * 4) - 1, y1:=-1, x2:=(m_intPlayerWidth * 4) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 4
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=(m_intPlayerWidth * 5) - 1, y1:=-1, x2:=(m_intPlayerWidth * 5) - 1, y2:=m_intGoalieHeight - 1) 'vertical line 5
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=0, y1:=m_intPlayerHeight - 1, x2:=(picField.Width - m_intGoalieWidth) - 1, y2:=m_intPlayerHeight - 1) 'horizontal line 1
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=0, y1:=(m_intPlayerHeight * 2) - 1, x2:=(picField.Width - m_intGoalieWidth) - 1, y2:=(m_intPlayerHeight * 2) - 1) 'horizontal line 2
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=0, y1:=(m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1, x2:=(picField.Width - m_intGoalieWidth) - 1, y2:=(m_intPlayerHeight * 2) + m_intMidrowPlayerHeight - 1) 'horizontal line 3
                e.Graphics.DrawLine(pen:=pnDrawing, x1:=0, y1:=(m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1, x2:=(picField.Width - m_intGoalieWidth) - 1, y2:=(m_intPlayerHeight * 3) + m_intMidrowPlayerHeight - 1) 'horizontal line 4
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PlaceButtons(frmtn As Formation)
        Try
            Dim strCoordVal As String = ""
            Dim strCoords() As String
            Dim ptCoord As Point

            If m_enmFormation <> Formation.f_Custom Then
                dictPlayerPositions.Clear()
            End If

            If dictFormationCoordinates.TryGetValue(frmtn, strCoordVal) Then
                strCoords = strCoordVal.Split(CChar(","))
                For i As Integer = 0 To strCoords.Length - 1
                    If dictCellCoordinates.TryGetValue(strCoords(i), ptCoord) Then
                        Select Case i
                            Case 0
                                btnGoalie.Location = ptCoord
                                btnGoalie.Height = m_intGoalieHeight + 1
                                btnGoalie.Width = m_intGoalieWidth + 1
                                If m_intHighlightedButton = 0 Then
                                    btnGoalie.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnGoalie.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnGoalie.BackColor = m_clrButtonBackColor
                                    btnGoalie.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnGoalie.BackgroundImage = m_imgButtonBackgroundImage
                                btnGoalie.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                            Case 1
                                btnPlayer1.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 1 Then
                                    btnPlayer1.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer1.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer1.BackColor = m_clrButtonBackColor
                                    btnPlayer1.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer1.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer1.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer1.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer1.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer1.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(1))
                                Else
                                    btnPlayer1.Location = ptCoord
                                    dictPlayerPositions.Add(1, strCoords(i))
                                End If
                            Case 2
                                btnPlayer2.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 2 Then
                                    btnPlayer2.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer2.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer2.BackColor = m_clrButtonBackColor
                                    btnPlayer2.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer2.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer2.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer2.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer2.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer2.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(2))
                                Else
                                    btnPlayer2.Location = ptCoord
                                    dictPlayerPositions.Add(2, strCoords(i))
                                End If
                            Case 3
                                btnPlayer3.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 3 Then
                                    btnPlayer3.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer3.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer3.BackColor = m_clrButtonBackColor
                                    btnPlayer3.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer3.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer3.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer3.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer3.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer3.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(3))
                                Else
                                    btnPlayer3.Location = ptCoord
                                    dictPlayerPositions.Add(3, strCoords(i))
                                End If
                            Case 4
                                btnPlayer4.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 4 Then
                                    btnPlayer4.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer4.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer4.BackColor = m_clrButtonBackColor
                                    btnPlayer4.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer4.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer4.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer4.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer4.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer4.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(4))
                                Else
                                    btnPlayer4.Location = ptCoord
                                    dictPlayerPositions.Add(4, strCoords(i))
                                End If
                            Case 5
                                btnPlayer5.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 5 Then
                                    btnPlayer5.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer5.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer5.BackColor = m_clrButtonBackColor
                                    btnPlayer5.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer5.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer5.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer5.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer5.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer5.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(5))
                                Else
                                    btnPlayer5.Location = ptCoord
                                    dictPlayerPositions.Add(5, strCoords(i))
                                End If
                            Case 6
                                btnPlayer6.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 6 Then
                                    btnPlayer6.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer6.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer6.BackColor = m_clrButtonBackColor
                                    btnPlayer6.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer6.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer6.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer6.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer6.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer6.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(6))
                                Else
                                    btnPlayer6.Location = ptCoord
                                    dictPlayerPositions.Add(6, strCoords(i))
                                End If
                            Case 7
                                btnPlayer7.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 7 Then
                                    btnPlayer7.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer7.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer7.BackColor = m_clrButtonBackColor
                                    btnPlayer7.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer7.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer7.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer7.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer7.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer7.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(7))
                                Else
                                    btnPlayer7.Location = ptCoord
                                    dictPlayerPositions.Add(7, strCoords(i))
                                End If
                            Case 8
                                btnPlayer8.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 8 Then
                                    btnPlayer8.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer8.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer8.BackColor = m_clrButtonBackColor
                                    btnPlayer8.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer8.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer8.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer8.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer8.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer8.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(8))
                                Else
                                    btnPlayer8.Location = ptCoord
                                    dictPlayerPositions.Add(8, strCoords(i))
                                End If
                            Case 9
                                btnPlayer9.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 9 Then
                                    btnPlayer9.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer9.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer9.BackColor = m_clrButtonBackColor
                                    btnPlayer9.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer9.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer9.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer9.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer9.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer9.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(9))
                                Else
                                    btnPlayer9.Location = ptCoord
                                    dictPlayerPositions.Add(9, strCoords(i))
                                End If
                            Case 10
                                btnPlayer10.Width = m_intPlayerWidth + 1
                                If m_intHighlightedButton = 10 Then
                                    btnPlayer10.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                                    btnPlayer10.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))
                                Else
                                    btnPlayer10.BackColor = m_clrButtonBackColor
                                    btnPlayer10.ForeColor = clsUtility.GetTextColor(btnGoalie.BackColor)
                                End If
                                btnPlayer10.BackgroundImage = m_imgButtonBackgroundImage
                                btnPlayer10.BackgroundImageLayout = m_imlButtonBackgroundImageLayout
                                If strCoords(i).Contains("R3") Then
                                    btnPlayer10.Height = m_intMidrowPlayerHeight + 1
                                Else
                                    btnPlayer10.Height = m_intPlayerHeight + 1
                                End If
                                If m_enmFormation = Formation.f_Custom Then
                                    btnPlayer10.Location = dictCellCoordinates.Item(dictPlayerPositions.Item(10))
                                Else
                                    btnPlayer10.Location = ptCoord
                                    dictPlayerPositions.Add(10, strCoords(i))
                                End If
                        End Select
                    End If
                Next
            End If

            btnGoalie.BringToFront()
            btnPlayer1.BringToFront()
            btnPlayer2.BringToFront()
            btnPlayer3.BringToFront()
            btnPlayer4.BringToFront()
            btnPlayer5.BringToFront()
            btnPlayer6.BringToFront()
            btnPlayer7.BringToFront()
            btnPlayer8.BringToFront()
            btnPlayer9.BringToFront()
            btnPlayer10.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetButtonText()
        Try
            Dim drarray() As DataRow

            If m_intTeamId <= 0 Then
                Exit Sub
            End If

            If m_dtPlayerDetails.Rows.Count > 0 Then
                drarray = m_dtPlayerDetails.Select("STARTING_POSITION > 0 and TEAM_ID = '" & m_intTeamId.ToString & "'", "TEAM_ID,STARTING_POSITION")

                For i = 0 To (drarray.Length - 1)
                    Select Case CInt(drarray(i)("STARTING_POSITION"))
                        Case 1
                            btnGoalie.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnGoalie.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 2
                            btnPlayer1.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer1.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 3
                            btnPlayer2.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer2.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 4
                            btnPlayer3.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer3.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 5
                            btnPlayer4.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer4.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 6
                            btnPlayer5.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer5.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 7
                            btnPlayer6.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer6.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 8
                            btnPlayer7.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer7.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 9
                            btnPlayer8.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer8.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 10
                            btnPlayer9.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer9.Tag = drarray(i)("PLAYER_ID").ToString
                        Case 11
                            btnPlayer10.Text = drarray(i)("DISPLAY_UNIFORM_NUMBER").ToString & Chr(13) & drarray(i)("LAST_NAME").ToString & " " & If(IsDBNull(drarray(i)("MONIKER")), "", drarray(i)("MONIKER").ToString.Substring(0, 1))
                            btnPlayer10.Tag = drarray(i)("PLAYER_ID").ToString
                    End Select
                Next
            Else

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub SetButtonTextCollection()
        Try
           Dim btnplayer As Button
            btnGoalie.Text=String.Empty
            btnGoalie.Tag=0
            For i As Integer = 1 To 10
                CType(Controls.Item("btnPlayer" & i), Button).Text = String.Empty
                CType(Controls.Item("btnPlayer" & i), Button).Tag = 0
            Next
            For Each player In _playerTable
                If player.StartingPosition = 1 Then
                    btnplayer = btnGoalie
                Else
                    btnplayer = CType(Controls.Item("btnPlayer" & player.StartingPosition - 1), Button)
                End If
                btnplayer.Text = player.Player
                btnplayer.Tag = player.PlayerId
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UnhighlightButtons()
        Try
            If m_intHighlightedButton = -1 Then
                Exit Sub
            End If
            Select Case m_intHighlightedButton
                Case 0
                    btnGoalie.BackColor = m_clrButtonBackColor
                    btnGoalie.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 1
                    btnPlayer1.BackColor = m_clrButtonBackColor
                    btnPlayer1.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 2
                    btnPlayer2.BackColor = m_clrButtonBackColor
                    btnPlayer2.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 3
                    btnPlayer3.BackColor = m_clrButtonBackColor
                    btnPlayer3.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 4
                    btnPlayer4.BackColor = m_clrButtonBackColor
                    btnPlayer4.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 5
                    btnPlayer5.BackColor = m_clrButtonBackColor
                    btnPlayer5.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 6
                    btnPlayer6.BackColor = m_clrButtonBackColor
                    btnPlayer6.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 7
                    btnPlayer7.BackColor = m_clrButtonBackColor
                    btnPlayer7.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 8
                    btnPlayer8.BackColor = m_clrButtonBackColor
                    btnPlayer8.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 9
                    btnPlayer9.BackColor = m_clrButtonBackColor
                    btnPlayer9.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
                Case 10
                    btnPlayer10.BackColor = m_clrButtonBackColor
                    btnPlayer10.ForeColor = clsUtility.GetTextColor(m_clrButtonBackColor)
            End Select
            m_intHighlightedButton = -1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub HighlightButton(PlayerId As Integer)
        Try
            'loop through all buttons and find the tag matching this playerid
            For Each cntrl As Control In Me.Controls
                If TypeOf cntrl Is Button Then
                    If CInt(cntrl.Tag) = PlayerId Then
                        UnhighlightButtons()

                        cntrl.BackColor = Color.FromName(HIGHLIGHT_COLOR)
                        cntrl.ForeColor = clsUtility.GetTextColor(Color.FromName(HIGHLIGHT_COLOR))

                        If IsNumeric(cntrl.Name.Substring(cntrl.Name.Length - 1, 1)) Then
                            If IsNumeric(cntrl.Name.Substring(cntrl.Name.Length - 2, 2)) Then
                                m_intHighlightedButton = CInt(cntrl.Name.Substring(cntrl.Name.Length - 2, 2)) 'player 10
                            Else
                                m_intHighlightedButton = CInt(cntrl.Name.Substring(cntrl.Name.Length - 1, 1)) 'players 1 through 9
                            End If
                        Else
                            m_intHighlightedButton = 0 'goalie
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Public properties "

    <Browsable(True),
     EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
    Public Property USPLeftToRight() As Boolean
        Get
            Return m_blnLeftToRight
        End Get
        Set(ByVal value As Boolean)
            m_blnLeftToRight = value
            picField.Refresh()
        End Set
    End Property

    <Browsable(False),
 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USPFormation() As Formation
        Get
            Return m_enmFormation
        End Get
        Set(ByVal value As Formation)
            m_enmFormation = value
            picField.Refresh()
        End Set
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public ReadOnly Property USPDefaultFormation() As Formation
        Get
            Return m_enmDefaultFormation
        End Get
    End Property

    <Browsable(False),
 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USPButtonBackColor() As Color
        Get
            Return m_clrButtonBackColor
        End Get
        Set(ByVal value As Color)
            m_clrButtonBackColor = value
            picField.Refresh()
        End Set
    End Property

    <Browsable(False),
 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USPButtonBackgroundImage() As Image
        Get
            Return m_imgButtonBackgroundImage
        End Get
        Set(ByVal value As Image)
            m_imgButtonBackgroundImage = value
            picField.Refresh()
        End Set
    End Property

    <Browsable(False),
 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USPButtonBackgroundImageLayout() As ImageLayout
        Get
            Return m_imlButtonBackgroundImageLayout
        End Get
        Set(ByVal value As ImageLayout)
            m_imlButtonBackgroundImageLayout = value
            picField.Refresh()
        End Set
    End Property

    <Browsable(False),
 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public WriteOnly Property USPPlayerDetails() As DataTable
        Set(value As DataTable)
            m_dtPlayerDetails = value
            SetButtonText()
        End Set
    End Property
    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public WriteOnly Property UspPlayerDetailsCollection() As List(Of ClsPlayerData.PlayerData)
        Set(value As List(Of ClsPlayerData.PlayerData))
            _playerTable = value
            SetButtonTextCollection()
        End Set
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USPTeamId As Integer
        Set(value As Integer)
            m_intTeamId = value
        End Set
        Get
            Return m_intTeamId
        End Get
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USPCustomFormationDetails() As Dictionary(Of Integer, String)
        Set(value As Dictionary(Of Integer, String))
            'dictPlayerPositions = value  TOPZ-1301
            dictPlayerPositions.Clear()
            For Each entry As KeyValuePair(Of Integer, String) In value
                dictPlayerPositions.Add(entry.Key, entry.Value)
            Next
            m_enmFormation = Formation.f_Custom
            picField.Refresh()
        End Set
        Get
            If m_enmFormation = Formation.f_Custom Then
                Return dictPlayerPositions
            Else
                Return Nothing
            End If
        End Get
    End Property

#End Region

#Region " Public enums "

    ''' <summary>
    ''' Enum for formations initilaized to match formation_id in soccer.soccer_formations
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum Formation As Integer
        f_442 = 1 Or Nothing
        f_532 = 2
        f_352 = 3
        f_451 = 4
        f_433 = 5
        f_343 = 6
        f_4141 = 7
        f_4321 = 8
        f_4411 = 9
        f_4123 = 10
        f_541 = 11
        f_4132 = 12
        f_4231 = 13
        f_4312 = 14
        f_3511 = 15
        f_3421 = 16
        f_3412 = 17
        f_3142 = 18
        f_41212 = 19
        f_3331 = 20   'TOPZ-515
        f_33211 = 21  'TOPZ-515
        f_5131 = 22   'TOPZ-515
        f_3241 = 23   'TOPZ-515
        f_Custom = 100
    End Enum

#End Region

#Region " Public methods "
    Public Sub USPUnhighlightButtons()
        Try
            UnhighlightButtons()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub USPHighlightButton(PlayerId As Integer)
        Try
            HighlightButton(PlayerId)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class

Public Class USPEventArgs
    Inherits System.EventArgs

#Region " Constants & Variables "
    Private m_PlayerID As Integer
#End Region

#Region " Public properties "
    Public ReadOnly Property USPplayerID() As Integer
        Get
            Return m_PlayerID
        End Get
    End Property
#End Region

#Region " Public Methods "
    Public Sub New(ByVal PlayerID As Integer)
        Try
            m_PlayerID = PlayerID
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class