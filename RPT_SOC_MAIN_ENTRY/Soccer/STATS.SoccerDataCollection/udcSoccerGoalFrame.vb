﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : udcSoccerGoalFrame
' Author        : Fiaz Ahmed
' Created Date  : 25th Jan 2016
' Description   : This user control represents a soccer goal with clickable area besides, above and witihin.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'TOPZ-296       | Fiaz                | 25/01/2016        | Initial creation
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Imports System.ComponentModel

Public Class udcSoccerGoalFrame

#Region " Constants & Variables "
    Private Const CONTROL_HEIGHT As Double = 7.6       '7.6 meters
    Private Const CONTROL_WIDTH As Double = 54         '54 meters
    Private Const CONTROL_VMARGIN As Double = 0.4      '0.4 meters->to make 7.6+0.4 = 8 to get a rounded height:width ratio of 4:27
    Private Const NET_HEIGHT As Double = 2.44          '2.44 meters without post
    Private Const NET_WIDTH As Double = 7.32           '7.32 meters without posts
    Private Const POST_THICKNESS As Double = 0.12      '12 centimeters
    Private Const LINE_THICKNESS As Double = 0.12      '12 centimeters
    Private Const GOAL_BOX_WIDTH As Double = 5.5       '5.5 meters from inside of frame
    Private Const PENALTY_BOX_WIDTH As Double = 16.5   '16.5 meters from inside of frame

    Private m_intPrevWidth As Integer = 0

    Private m_intNextMark As Integer = 1
    Private m_intMarkCount As Integer = 0
    Private m_intMaximumMarksAllowed As Integer = 2
    Private m_blnConfineToGoal As Boolean = False
    Private m_blnDisallowInGoal As Boolean = False
    Private m_intMarks As Marks = Marks.Both
    Private m_blnEnableOneClickRelocation As Boolean = True

    Private m_dblClickedX1 As Double?
    Private m_dblClickedY1 As Double?
    Private m_dblClickedX2 As Double?
    Private m_dblClickedY2 As Double?

    Public Event USGFClick(ByVal sender As Object, ByVal e As USGFEventArgs)
    Public Event USGFMouseMove(ByVal sender As Object, ByVal e As USGFEventArgs)
    Public Event USGFMarkRemoved(ByVal sender As Object, ByVal e As USGFEventArgs)
    Public Event USGFRightClick(ByVal sender As Object, ByVal e As USGFEventArgs)

    Private m_objUtility As New STATS.Utility.clsUtility

    Private Enum Zone
        Goal
        Keeper
    End Enum

    Public Enum Marks
        Mark1Only
        Mark2Only
        Both
    End Enum
#End Region

#Region " Event handlers "

    Private Sub udcSoccerGoalFrame_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Try
            Dim intHeight As Integer
            Dim intWidth As Integer
            Dim dblNewX As Double
            Dim dblNewY As Double

            If Me.DesignMode = False Then
                If Me.Height / 4 <> Me.Width / 27 Then
                    Me.Height = CInt(Me.Width / 27 * 4)
                End If
                For Each cnt As Control In picSoccerGoalFrame.Controls
                    If cnt.Name = "GreenMark" Or cnt.Name = "OrangeMark" Then
                        dblNewX = cnt.Left / m_intPrevWidth * Me.Width
                        dblNewY = cnt.Top / m_intPrevWidth * Me.Width
                        cnt.Left = CInt(dblNewX)
                        cnt.Top = CInt(dblNewY)
                    End If
                Next
                m_intPrevWidth = Me.Width
            Else
                'When developer drags the user control on form,
                'ensure that the resize always maintains 4:27 ratio for height:width
                If Me.Height / 4 > Me.Width / 27 Then
                    intHeight = CInt(4 * (Me.Width / 27))
                    While Not intHeight Mod 4 = 0
                        intHeight = intHeight + 1
                    End While
                    Me.Height = intHeight
                    Me.Width = CInt(intHeight / 4 * 27) ''''
                Else
                    intWidth = CInt(27 * (Me.Height / 4))
                    While Not intWidth Mod 27 = 0
                        intWidth = intWidth + 1
                    End While
                    Me.Width = intWidth
                    Me.Height = CInt(intWidth / 27 * 4) '''''
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picSoccerGoalFrame_Click(sender As Object, e As EventArgs) Handles picSoccerGoalFrame.Click
        Try
            Dim evargMouse As MouseEventArgs
            Dim dblX As Double
            Dim dblY As Double
            Dim MyEventArgs As USGFEventArgs
            Dim intWhichMark As Integer = 0
            Dim intZone As Integer?
            Dim intMark1Zone As Integer?
            Dim intMark2Zone As Integer?

            evargMouse = CType(e, MouseEventArgs)

            'fire the event only for left mouse click
            If Not evargMouse.Button = Windows.Forms.MouseButtons.Left Then
                RaiseEvent USGFRightClick(sender, MyEventArgs)
 '
                Exit Try
            End If

            dblX = evargMouse.X / Me.Width * CONTROL_WIDTH
            dblY = evargMouse.Y / Me.Width * CONTROL_WIDTH

            'exit if user clicks below net
            If dblY > CONTROL_HEIGHT Then
                Exit Sub
            End If

            If m_blnEnableOneClickRelocation = True And m_intMaximumMarksAllowed = 1 Then
                USGFClearMarks()
            End If

            'allow user to change the mark by clicking on another location only when one mark is allowed
            If m_intMarkCount >= m_intMaximumMarksAllowed Then
                Exit Try
            End If

            'convert x and y keeping center botom of goal post as 0,0
            dblX = dblX - (CONTROL_WIDTH / 2)
            dblY = CONTROL_HEIGHT - dblY

            'exit if user clicks ouside net if USGFConfineToGoal = true
            'provided it's the first mark TOPZ-811 and TOPZ-920
            If m_blnConfineToGoal = True And m_intNextMark = 1 Then
                If dblX < (NET_WIDTH / 2) * -1 Or dblX > (NET_WIDTH / 2) Then
                    Exit Try
                End If
                If dblY > NET_HEIGHT Then
                    Exit Try
                End If
            End If

            'exit if user clicks inside net if USGFDisallowInGoal = true
            'provided it's the first mark TOPZ-1118
            If m_blnDisallowInGoal = True And m_intNextMark = 1 Then
                If dblX > ((NET_WIDTH / 2) + POST_THICKNESS) * -1 And dblX < ((NET_WIDTH / 2) + POST_THICKNESS) And dblY < (NET_HEIGHT + POST_THICKNESS) Then
                    Exit Try
                End If
            End If

            'exit if user clicks beyond a height of 4  - TOPZ-811
            If m_blnConfineToGoal = True And dblY > 4 Then
                Exit Try
            End If

            'place the mark on user control
            intWhichMark = SetMark(evargMouse.X, evargMouse.Y)
            m_intPrevWidth = Me.Width   'store the width of control -> to be used for plotting green and orange marks during resize

            If intWhichMark = 1 Then
                m_dblClickedX1 = dblX
                m_dblClickedY1 = dblY
                intZone = GetZone(dblX, dblY, Zone.Goal)
            ElseIf intWhichMark = 2 Then
                m_dblClickedX2 = dblX
                m_dblClickedY2 = dblY
                intZone = GetZone(dblX, dblY, Zone.Keeper)
            End If

            'raise the user control's USGFClick event containing
            'the current mouse x, y
            If m_dblClickedX1.HasValue And m_dblClickedX2.HasValue Then
                intMark1Zone = GetZone(CDbl(m_dblClickedX1), CDbl(m_dblClickedY1), Zone.Goal)
                intMark2Zone = GetZone(CDbl(m_dblClickedX2), CDbl(m_dblClickedY2), Zone.Keeper)
                MyEventArgs = New USGFEventArgs(dblX, dblY, intZone, New PointF(CSng(m_dblClickedX1), CSng(m_dblClickedY1)), New PointF(CSng(m_dblClickedX2), CSng(m_dblClickedY2)), intMark1Zone, intMark2Zone)
            ElseIf m_dblClickedX1.HasValue And m_dblClickedX2.HasValue = False Then
                intMark1Zone = GetZone(CDbl(m_dblClickedX1), CDbl(m_dblClickedY1), Zone.Goal)
                MyEventArgs = New USGFEventArgs(dblX, dblY, intZone, New PointF(CSng(m_dblClickedX1), CSng(m_dblClickedY1)), Nothing, intMark1Zone, Nothing)
            ElseIf m_dblClickedX1.HasValue = False And m_dblClickedX2.HasValue Then
                intMark2Zone = GetZone(CDbl(m_dblClickedX2), CDbl(m_dblClickedY2), Zone.Keeper)
                MyEventArgs = New USGFEventArgs(dblX, dblY, intZone, Nothing, New PointF(CSng(m_dblClickedX2), CSng(m_dblClickedY2)), Nothing, intMark2Zone)
            Else
                MyEventArgs = New USGFEventArgs(dblX, dblY, intZone, Nothing, Nothing, Nothing, Nothing)
            End If

            RaiseEvent USGFClick(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picSoccerGoalFrame_MouseMove(sender As Object, e As MouseEventArgs) Handles picSoccerGoalFrame.MouseMove
        Try
            Dim dblX As Double
            Dim dblY As Double

            dblX = e.X / Me.Width * CONTROL_WIDTH
            dblY = e.Y / Me.Width * CONTROL_WIDTH

            If dblY > CONTROL_HEIGHT Then
                Exit Sub
            End If

            'convert x and y keeping center botom of goal post as 0,0
            dblX = dblX - (CONTROL_WIDTH / 2)
            dblY = CONTROL_HEIGHT - dblY

            Dim MyEventArgs As USGFEventArgs
            MyEventArgs = New USGFEventArgs(dblX, dblY, Nothing, Nothing, Nothing, Nothing, Nothing)

            RaiseEvent USGFMouseMove(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub picSoccerGoalFrame_Paint(sender As Object, e As PaintEventArgs) Handles picSoccerGoalFrame.Paint
        Try
            DrawGoal(e)        'draw the soccer goal frame
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Public Methods "
    ''' <summary>
    ''' Clears all the marks on soccer-goal-frame user control.
    ''' </summary>
    ''' <remarks>Call this method to clear all marks during Save or Cancel operation.</remarks>
    Public Sub USGFClearMarks()
        Try
            For i As Integer = 0 To picSoccerGoalFrame.Controls.Count - 1
                For Each cnt As Control In picSoccerGoalFrame.Controls
                    If cnt.Name = "GreenMark" Or cnt.Name = "OrangeMark" Then
                        cnt.Dispose()
                        m_dblClickedX1 = Nothing
                        m_dblClickedY1 = Nothing
                        m_dblClickedX2 = Nothing
                        m_dblClickedY2 = Nothing
                    End If
                Next
            Next
            m_intMarkCount = 0

            Select Case m_intMarks
                Case Marks.Mark1Only
                    m_intNextMark = 1
                Case Marks.Mark2Only
                    m_intNextMark = 2
                Case Marks.Both
                    m_intNextMark = 1
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Places the mark on soccer field.
    ''' </summary>
    ''' <param name="X">X-Coordinate of the mark.</param>
    ''' <param name="Y">Y-Coordinate of the mark.</param>
    ''' <remarks>Call this method to place marks on soccer field during Edit operations.</remarks>
    Public Sub USGFSetMark(ByVal X As Double, ByVal Y As Double, Optional ByVal Blink As Boolean = False)
        Try
            Dim intWhichMark As Integer = 0

            'place the mark
            intWhichMark = SetMark(CInt(MapXtoScreen(X)), CInt(MapYtoScreen(Y)), Blink)

            If intWhichMark = 1 Then
                m_dblClickedX1 = X
                m_dblClickedY1 = Y
            ElseIf intWhichMark = 2 Then
                m_dblClickedX2 = X
                m_dblClickedY2 = Y
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Private methods "

    Private Sub DrawGoal(ByVal e As System.Windows.Forms.PaintEventArgs)
        Try
            Dim rctGoal As Rectangle
            Dim rctGoalMouth As Rectangle
            Dim rctMargin As Rectangle
            Dim pnGoalDrawing As Pen
            Dim clrGoalPenColor As Color
            Dim intGoalPenWidth As Integer = 1
            Dim pnLineDrawing As Pen
            Dim clrLinePenColor As Color
            Dim intLinePenWidth As Integer = 1
            Dim pnNet As Pen
            Dim clrNetPenColor As Color
            Dim intNetPenWidth As Integer = 1
            Dim dblGoalWidth As Double
            Dim dblGoalHeight As Double
            Dim dblNetWidth As Double
            Dim dblNetHeight As Double
            Dim dblMargin As Double
            Dim dblPostThickness As Double
            Dim dblGoalBoxWidth As Double
            Dim dblPenaltyBoxWidth As Double

            dblGoalWidth = (NET_WIDTH + (POST_THICKNESS * 2)) / CONTROL_WIDTH * Me.Width
            dblGoalHeight = (NET_HEIGHT + POST_THICKNESS) / CONTROL_WIDTH * Me.Width
            dblNetWidth = NET_WIDTH / CONTROL_WIDTH * Me.Width
            dblNetHeight = NET_HEIGHT / CONTROL_WIDTH * Me.Width
            dblMargin = CONTROL_VMARGIN / CONTROL_WIDTH * Me.Width
            dblPostThickness = POST_THICKNESS / CONTROL_WIDTH * Me.Width
            dblGoalBoxWidth = GOAL_BOX_WIDTH / CONTROL_WIDTH * Me.Width
            dblPenaltyBoxWidth = PENALTY_BOX_WIDTH / CONTROL_WIDTH * Me.Width

            clrGoalPenColor = Color.Black
            pnGoalDrawing = New Pen(Color:=clrGoalPenColor, Width:=intGoalPenWidth)

            'draw goal
            rctGoal = New Rectangle(x:=CInt((Me.Width / 2) - (dblGoalWidth / 2)), y:=CInt(Me.Height - dblGoalHeight) - CInt(dblMargin), Width:=CInt(dblGoalWidth), Height:=CInt(dblGoalHeight))
            rctGoalMouth = New Rectangle(x:=rctGoal.X + CInt(dblPostThickness), y:=rctGoal.Y + CInt(dblPostThickness), Width:=rctGoal.Width - (CInt(dblPostThickness) * 2), Height:=rctGoal.Height - CInt(dblPostThickness))

            e.Graphics.FillRectangle(Brushes.White, rctGoal)
            e.Graphics.DrawRectangle(pen:=pnGoalDrawing, rect:=rctGoal)

            e.Graphics.FillRectangle(Brushes.LightBlue, rctGoalMouth)
            e.Graphics.DrawRectangle(pen:=pnGoalDrawing, rect:=rctGoalMouth)

            'draw net
            clrNetPenColor = Color.White
            pnNet = New Pen(Color:=clrNetPenColor, Width:=intNetPenWidth)
            For i As Integer = 2 To rctGoalMouth.Height - 1 Step 2
                e.Graphics.DrawLine(pnNet, rctGoalMouth.X + 1, rctGoalMouth.Y + i, rctGoalMouth.X + rctGoalMouth.Width - 1, rctGoalMouth.Y + i)
            Next

            For i As Integer = 2 To rctGoalMouth.Width - 1 Step 2
                e.Graphics.DrawLine(pnNet, rctGoalMouth.X + i, rctGoalMouth.Y + 1, rctGoalMouth.X + i, rctGoalMouth.Y + rctGoalMouth.Height - 1)
            Next

            'draw field
            rctMargin = New Rectangle(x:=0, y:=Me.Height - CInt(dblMargin), Width:=Me.Width, Height:=CInt(dblMargin))
            e.Graphics.FillRectangle(Brushes.DarkGreen, rctMargin)

            'shade the field to reprsent goal box and penalt box
            e.Graphics.FillRectangle(Brushes.SeaGreen, x:=CInt((Me.Width / 2) - (dblNetWidth / 2) - dblPenaltyBoxWidth), y:=Me.Height - CInt(dblMargin), width:=CInt(dblNetWidth + (dblPenaltyBoxWidth * 2)), height:=CInt(dblMargin))
            e.Graphics.FillRectangle(Brushes.MediumSeaGreen, x:=CInt((Me.Width / 2) - (dblNetWidth / 2) - dblGoalBoxWidth), y:=Me.Height - CInt(dblMargin), width:=CInt(dblNetWidth + (dblGoalBoxWidth * 2)), height:=CInt(dblMargin))

            'draw lines to represent goal box and penalt box
            clrLinePenColor = Color.SkyBlue
            pnLineDrawing = New Pen(Color:=clrLinePenColor, Width:=intLinePenWidth)

            e.Graphics.DrawLine(pnLineDrawing, x1:=CInt((Me.Width / 2) - (dblNetWidth / 2) - dblGoalBoxWidth), y1:=0, x2:=CInt((Me.Width / 2) - (dblNetWidth / 2) - dblGoalBoxWidth), y2:=Me.Height)
            e.Graphics.DrawLine(pnLineDrawing, x1:=CInt((Me.Width / 2) + (dblNetWidth / 2) + dblGoalBoxWidth), y1:=0, x2:=CInt((Me.Width / 2) + (dblNetWidth / 2) + dblGoalBoxWidth), y2:=Me.Height)
            e.Graphics.DrawLine(pnLineDrawing, x1:=CInt((Me.Width / 2) - (dblNetWidth / 2) - dblPenaltyBoxWidth), y1:=0, x2:=CInt((Me.Width / 2) - (dblNetWidth / 2) - dblPenaltyBoxWidth), y2:=Me.Height)
            e.Graphics.DrawLine(pnLineDrawing, x1:=CInt((Me.Width / 2) + (dblNetWidth / 2) + dblPenaltyBoxWidth), y1:=0, x2:=CInt((Me.Width / 2) + (dblNetWidth / 2) + dblPenaltyBoxWidth), y2:=Me.Height)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Place the mark on user control
    ''' </summary>
    ''' <param name="X">X-Coordinate</param>
    ''' <param name="Y">Y-Coordinate</param>
    ''' <param name="Blink"></param>
    ''' <returns>1 if 1st mark set, 2 if 2nd mark set, 0 if no mark set</returns>
    ''' <remarks></remarks>
    Private Function SetMark(ByVal X As Integer, ByVal Y As Integer, Optional ByVal Blink As Boolean = False) As Integer
        Try
            Dim pic As New PictureBox
            Dim ptXY As Point
            Dim intReturnVal As Integer = 0

            If m_intNextMark = 1 Then
                pic.Image = Image.FromFile(m_objUtility.ReturnImage("GreenPinSmall.gif"))
                pic.Name = "GreenMark"
                m_intMarkCount = m_intMarkCount + 1
                If m_intMaximumMarksAllowed > 1 Then
                    m_intNextMark = 2
                End If
                intReturnVal = 1
            ElseIf m_intNextMark = 2 Then
                pic.Image = Image.FromFile(m_objUtility.ReturnImage("OrangePinSmall.gif"))
                pic.Name = "OrangeMark"
                m_intMarkCount = m_intMarkCount + 1
                If m_intMaximumMarksAllowed > 1 Then
                    m_intNextMark = 1
                End If
                intReturnVal = 2
            Else
                Exit Try
            End If

            pic.SizeMode = PictureBoxSizeMode.AutoSize
            picSoccerGoalFrame.Controls.Add(pic)
            AddHandler pic.Click, AddressOf Me.ClickMark

            ptXY.X = X - 3
            ptXY.Y = Y - 3
            pic.Location = ptXY

            Return intReturnVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Convert db x to scaled user control x
    ''' </summary>
    ''' <param name="X"></param>
    ''' <remarks></remarks>
    Private Function MapXtoScreen(ByVal X As Double) As Double
        Try
            Return (X + (CONTROL_WIDTH / 2)) / CONTROL_WIDTH * Me.Width
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Convert db y to scaled user control y
    ''' </summary>
    ''' <param name="Y"></param>
    ''' <remarks></remarks>
    Private Function MapYtoScreen(ByVal Y As Double) As Double
        Try
            Return (CONTROL_HEIGHT - Y) / CONTROL_WIDTH * Me.Width
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Fires when any of the marks is clicked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ClickMark(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'delete the mark clicked by user
            Dim picdel As PictureBox
            picdel = CType(sender, PictureBox)
            If picdel.Name = "GreenMark" Then
                m_intMarkCount = m_intMarkCount - 1
                m_intNextMark = 1
                m_dblClickedX1 = Nothing
                m_dblClickedY1 = Nothing
            Else
                m_intMarkCount = m_intMarkCount - 1
                If m_intMaximumMarksAllowed > 1 Then
                    If Not m_dblClickedX1.HasValue Then
                        m_intNextMark = 1
                    Else
                        m_intNextMark = 2
                    End If
                Else
                    m_intNextMark = 2
                End If
                m_dblClickedX2 = Nothing
                m_dblClickedY2 = Nothing
            End If
            picdel.Dispose()

            Dim MyEventArgs As USGFEventArgs

            'raise the user control's USGFMarkRemoved event containing
            'existing mark details if any
            If m_dblClickedX1.HasValue And m_dblClickedX2.HasValue Then
                MyEventArgs = New USGFEventArgs(Nothing, Nothing, Nothing, New PointF(CSng(m_dblClickedX1), CSng(m_dblClickedY1)), New PointF(CSng(m_dblClickedX2), CSng(m_dblClickedY2)), Nothing, Nothing)
            ElseIf m_dblClickedX1.HasValue And m_dblClickedX2.HasValue = False Then
                MyEventArgs = New USGFEventArgs(Nothing, Nothing, Nothing, New PointF(CSng(m_dblClickedX1), CSng(m_dblClickedY1)), Nothing, Nothing, Nothing)
            ElseIf m_dblClickedX1.HasValue = False And m_dblClickedX2.HasValue Then
                MyEventArgs = New USGFEventArgs(Nothing, Nothing, Nothing, Nothing, New PointF(CSng(m_dblClickedX2), CSng(m_dblClickedY2)), Nothing, Nothing)
            Else
                MyEventArgs = New USGFEventArgs(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
            End If

            RaiseEvent USGFMarkRemoved(sender, MyEventArgs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetZone(ByVal ZoneX As Double, ZoneY As Double, ZoneType As Zone) As Integer?
        Try
            Dim strZoneDesc As String = ""

            If New RectangleF((NET_WIDTH / 2) * -1, 0, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "BottomLeft"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1, NET_HEIGHT / 3, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "MiddleLeft"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1, (NET_HEIGHT / 3) * 2, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "TopLeft"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1 + (NET_WIDTH / 3), 0, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "BottomCenter"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1 + (NET_WIDTH / 3), NET_HEIGHT / 3, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "MiddleCenter"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1 + (NET_WIDTH / 3), (NET_HEIGHT / 3) * 2, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "TopCenter"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1 + ((NET_WIDTH / 3) * 2), 0, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "BottomRight"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1 + ((NET_WIDTH / 3) * 2), NET_HEIGHT / 3, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "MiddleRight"
            ElseIf New RectangleF((NET_WIDTH / 2) * -1 + ((NET_WIDTH / 3) * 2), (NET_HEIGHT / 3) * 2, NET_WIDTH / 3, NET_HEIGHT / 3).Contains(CSng(ZoneX), CSng(ZoneY)) Then
                strZoneDesc = "TopRight"
            ElseIf (ZoneX < (NET_WIDTH / 2) * -1 And ZoneX > ((NET_WIDTH / 2) * -1) - PENALTY_BOX_WIDTH) Or (ZoneX > (NET_WIDTH / 2) And ZoneX < (NET_WIDTH / 2) + PENALTY_BOX_WIDTH) Then
                strZoneDesc = "GoalArea"
            End If

            If ZoneType = Zone.Goal Then
                Select Case strZoneDesc
                    Case "BottomLeft"
                        Return 6
                    Case "MiddleLeft"
                        Return 7
                    Case "TopLeft"
                        Return 1
                    Case "BottomCenter"
                        Return 5
                    Case "MiddleCenter"
                        Return 8
                    Case "TopCenter"
                        Return 2
                    Case "BottomRight"
                        Return 4
                    Case "MiddleRight"
                        Return 9
                    Case "TopRight"
                        Return 3
                End Select
            ElseIf ZoneType = Zone.Keeper Then
                Select Case strZoneDesc
                    Case "BottomLeft"
                        Return 6
                    Case "MiddleLeft"
                        Return 8
                    Case "TopLeft"
                        Return 1
                    Case "BottomCenter"
                        Return 5
                    Case "MiddleCenter"
                        Return 9
                    Case "TopCenter"
                        Return 2
                    Case "BottomRight"
                        Return 4
                    Case "MiddleRight"
                        Return 10
                    Case "TopRight"
                        Return 3
                    Case "GoalArea"
                        Return 7
                End Select
            End If

            Return Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region " Public properties "

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USGFMarksAllowed() As Marks
        Get
            Return m_intMarks
        End Get
        Set(ByVal value As Marks)
            'set this property only when there aren't any marks already set
            If m_intMarkCount = 0 Then
                m_intMarks = value
                Select Case m_intMarks
                    Case Marks.Mark1Only
                        m_intMaximumMarksAllowed = 1
                        m_intNextMark = 1
                    Case Marks.Mark2Only
                        m_intMaximumMarksAllowed = 1
                        m_intNextMark = 2
                    Case Marks.Both
                        m_intMaximumMarksAllowed = 2
                        m_intNextMark = 1
                End Select
            Else
                Throw New System.Exception("Please clear exisitng marks before setting this property.")
            End If
        End Set
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USGFConfineToGoal As Boolean
        Get
            Return m_blnConfineToGoal
        End Get
        Set(ByVal value As Boolean)
            m_blnConfineToGoal = value
        End Set
    End Property

    <Browsable(False),
DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property USGFDisallowInGoal As Boolean
        Get
            Return m_blnDisallowInGoal
        End Get
        Set(ByVal value As Boolean)
            m_blnDisallowInGoal = value
        End Set
    End Property

#End Region

End Class

Public Class USGFEventArgs
    Inherits System.EventArgs

#Region " Constants & Variables "
    Private m_dblX As Double
    Private m_dblY As Double
    Private m_intZone As Integer?
    Private m_ptMark1 As PointF
    Private m_ptMark2 As PointF
    Private m_intMark1Zone As Integer?
    Private m_intMark2Zone As Integer?
#End Region

#Region " Public properties "
    Public ReadOnly Property USGFx() As Double
        Get
            Return m_dblX
        End Get
    End Property

    Public ReadOnly Property USGFy() As Double
        Get
            Return m_dblY
        End Get
    End Property

    Public ReadOnly Property USGFmark1 As PointF
        Get
            Return m_ptMark1
        End Get
    End Property

    Public ReadOnly Property USGFmark2 As PointF
        Get
            Return m_ptMark2
        End Get
    End Property

    Public ReadOnly Property USGFzone As Integer?
        Get
            Return m_intZone
        End Get
    End Property

    Public ReadOnly Property USGFmark1zone As Integer?
        Get
            Return m_intMark1Zone
        End Get
    End Property

    Public ReadOnly Property USGFmark2zone As Integer?
        Get
            Return m_intMark2Zone
        End Get
    End Property
#End Region

#Region " Public Methods "
    Public Sub New(ByVal X As Double, ByVal Y As Double, ByVal Zone As Integer?, ByVal Mark1 As PointF, ByVal Mark2 As PointF, ByVal Mark1Zone As Integer?, ByVal Mark2Zone As Integer?)
        Try
            m_dblX = X
            m_dblY = Y
            m_ptMark1 = Mark1
            m_ptMark2 = Mark2
            m_intZone = Zone
            m_intMark1Zone = Mark1Zone
            m_intMark2Zone = Mark2Zone
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class
