﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmAddDemoGame

#Region " Constants and Variables "
    Private m_dsDemoGame As New DataSet
    Private m_objAddDemoGame As New STATS.SoccerBL.clsAddDemoGame
    Private m_blnIsLeagueComboLoaded As Boolean = False
    Private m_strNupId As String
    Private m_dsTeam As New DataSet
    Private m_strCheckButtonOption As String
    Private m_intgamecode As Integer
    Private m_intLeague As Integer
    Private m_intHometeamId As Integer
    Private m_intAwayteamId As Integer
    Private m_strLeague As String
    Private m_dtGameDate As DateTime
    Private m_strAwayTeam As String
    Private m_strHomeTeam As String
    Private m_strAwayTeamAbbrev As String
    Private m_strHomeTeamAbbrev As String
    Private m_strStadium As String
    Private m_intCoverageLevel As Integer
    Private m_intModuleID As Integer
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Dim strmessage As String = "Game Deleted Successfully"
    Dim strmessage1 As String = "Game Details Updated Successfully"
    Dim strmessage2 As String = "Away team and home teams are should not be same."
    Dim strmessage3 As String = "does not have player"
    Dim strmessage4 As String = "Select coverageLevel"
    Dim strmessage5 As String = "Select venue"
    Dim strmessage6 As String = "Select Home team"
    Dim strmessage7 As String = "Select Away Team"
    Dim strmessage8 As String = "Select the date of the game"
    Dim strmessage9 As String = "League is not selected"
    Dim strmessage10 As String = "Please Save or Cancel the changes"
    Dim strmessage11 As String = "Currently there is no game available"
    Dim strmessage12 As String = "Select the module"

#End Region

#Region " Event Handlers "

    Private Sub frmAddDemoGame_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            m_blnIsLeagueComboLoaded = False
            cmbVenue.DataSource = Nothing
            cmbVenue.Items.Clear()
            cmbAwayTeam.DataSource = Nothing
            cmbAwayTeam.Items.Clear()
            cmbHomeTeam.DataSource = Nothing
            cmbHomeTeam.Items.Clear()
            cmbCoverageLevel.DataSource = Nothing
            cmbCoverageLevel.Items.Clear()
            cmbModule.DataSource = Nothing
            cmbModule.Items.Clear()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmAddDemoGame_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmAddDemoGame", Nothing, 1, 0)

            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Me.CenterToParent()
            LoadLeagueCombo()
            EnableDisableButton(False, False, False, True, False)
            EnableDisableCombo(False, False, False, False, False, False, False)
            LoadGames()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnAddTeam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddTeam.Click
        Dim objAddTeam As New frmAddTeam
        objAddTeam.ShowDialog()
        Application.DoEvents()
        If m_blnIsLeagueComboLoaded = True Then
            LoadCombo()
        End If
        LoadGames()
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            m_strCheckButtonOption = "Save"
            lvwGames.Enabled = False
            EnableDisableButton(True, True, False, False, False)
            EnableDisableCombo(True, True, True, True, True, True, True)
            cmbLeague.Focus()
            cmbLeague.SelectedIndex = 0
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lvwGames_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwGames.Click
        Try
            dtpGameDate.MinDate = Convert.ToDateTime("1/1/1753")
            dtpGameDate.MaxDate = Convert.ToDateTime("12/31/9998")
            EnableDisableButton(False, False, True, True, True)
            For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                m_strLeague = m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("Competition").ToString
                m_intLeague = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                m_intgamecode = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("GAME_CODE").ToString)
                m_dtGameDate = Convert.ToDateTime(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("GAME_DATE").ToString)
                m_strHomeTeam = m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("HOMETEAM").ToString
                m_strAwayTeam = m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("AWAYTEAM").ToString
                m_strHomeTeamAbbrev = m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("HOMEABBREV").ToString
                m_strAwayTeamAbbrev = m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("AWAYABBREV").ToString
                m_intHometeamId = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("HOMETEAMID").ToString)
                m_intAwayteamId = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("AWAYTEAMID").ToString)
                m_strStadium = m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("FIELDNAME").ToString
                m_intCoverageLevel = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("TIER").ToString)
                m_intModuleID = CInt(m_dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("MODULE_ID").ToString)
            Next
            SelectedListviewDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                BtnIgnore.Focus()
                Exit Sub
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddVenue.Click
        Try
            Dim objAddVenue As New frmAddVenue
            objAddVenue.ShowDialog()
            Application.DoEvents()
            If m_blnIsLeagueComboLoaded = True Then
                LoadCombo()
            End If
            LoadGames()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnAddPlayer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPlayer.Click
        Try
            Dim objAddPlayer As New frmAddDemoPlayer
            objAddPlayer.ShowDialog()
            Application.DoEvents()
            If m_blnIsLeagueComboLoaded = True Then
                LoadCombo()
            End If
            LoadGames()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region

#Region " User Methods "

    ''' <summary>
    '''
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadGames()
        Try
            lvwGames.Items.Clear()
            m_dsDemoGame = m_objAddDemoGame.GetSamepleGameDisplay()
            If Not m_dsDemoGame Is Nothing Then
                If (m_dsDemoGame.Tables(1).Rows.Count > 0) Then
                    For intRowCount = 0 To m_dsDemoGame.Tables(1).Rows.Count - 1
                        Dim lvwGameSchedule As New ListViewItem(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("GAME_CODE").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("GAME_DATE").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("AWAYTEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("HOMETEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Competition").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Task").ToString)
                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGames.Items.Add(lvwGameSchedule)
                    Next
                End If
            Else
                MessageDialog.Show(strmessage11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadLeagueCombo()
        Try
            Dim dtLeague As New DataTable
            Dim dsLeague As New DataSet
            dsLeague = m_objAddDemoGame.GetLeague()
            dtLeague = dsLeague.Tables(0).Copy
            LoadControl(cmbLeague, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
            m_blnIsLeagueComboLoaded = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="blnButtonApply"></param>
    ''' <param name="blnButtonIgnore"></param>
    ''' <param name="blnButtonDelete"></param>
    ''' <param name="blnButtonadd"></param>
    ''' <param name="blnButtonEdit"></param>
    ''' <remarks></remarks>
    Private Sub EnableDisableButton(ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean)
        Try
            btnApply.Enabled = blnButtonApply
            BtnIgnore.Enabled = blnButtonIgnore
            BtnDelete.Enabled = blnButtonDelete
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="blnComboLeague"></param>
    ''' <param name="blnComboDate"></param>
    ''' <param name="blnComboAwayTeam"></param>
    ''' <param name="blnComboHomeTeam"></param>
    ''' <param name="blnComboStadium"></param>
    ''' <param name="blnComboCoverageLevel"></param>
    ''' <param name="blnComboModule"></param>
    ''' <remarks></remarks>
    Private Sub EnableDisableCombo(ByVal blnComboLeague As Boolean, ByVal blnComboDate As Boolean, ByVal blnComboAwayTeam As Boolean, ByVal blnComboHomeTeam As Boolean, ByVal blnComboStadium As Boolean, ByVal blnComboCoverageLevel As Boolean, ByVal blnComboModule As Boolean)
        Try
            cmbLeague.Enabled = blnComboLeague
            dtpGameDate.Enabled = blnComboDate
            cmbAwayTeam.Enabled = blnComboAwayTeam
            cmbHomeTeam.Enabled = blnComboHomeTeam
            cmbVenue.Enabled = blnComboStadium
            cmbCoverageLevel.Enabled = blnComboCoverageLevel
            cmbModule.Enabled = blnComboModule
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="Combo"></param>
    ''' <param name="GameSetup"></param>
    ''' <param name="DisplayColumnName"></param>
    ''' <param name="DataColumnName"></param>
    ''' <remarks></remarks>
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SelectedListviewDisplay()

        Try
            Dim listselectDatarow() As DataRow
            For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                m_intgamecode = CInt(lvwitm.SubItems(0).Text)
            Next
            listselectDatarow = m_dsDemoGame.Tables(1).Select("GAME_CODE='" & m_intgamecode & "'")
            EnableDisableCombo(False, False, False, False, False, False, False)
            If listselectDatarow.Length > 0 Then
                dtpGameDate.Value = Convert.ToDateTime(listselectDatarow(0).ItemArray(2).ToString())
                cmbLeague.SelectedValue = listselectDatarow(0).ItemArray(1).ToString()
                cmbAwayTeam.SelectedValue = listselectDatarow(0).ItemArray(10).ToString()
                cmbHomeTeam.SelectedValue = listselectDatarow(0).ItemArray(9).ToString()
                cmbCoverageLevel.Text = listselectDatarow(0).ItemArray(17).ToString()
                cmbVenue.SelectedValue = listselectDatarow(0).ItemArray(4).ToString()
                cmbModule.SelectedValue = listselectDatarow(0).ItemArray(14).ToString()
                EnableDisableButton(False, False, True, True, True)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

    Private Sub btnAddOfficial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOfficial.Click
        Try
            Dim objAddOfficial As New frmAddOfficial
            objAddOfficial.ShowDialog()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnAddManager_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddManager.Click
        Try
            Dim objAddManager As New frmAddDemoManager
            objAddManager.ShowDialog()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLeague_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLeague.SelectedIndexChanged
        Try
            If m_blnIsLeagueComboLoaded = True Then
                LoadCombo()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub LoadCombo()

        Try
            If CInt(cmbLeague.SelectedValue) > 0 Then
                Dim dsCombo As New DataSet
                dsCombo = m_objAddDemoGame.GetAllComboData(CInt(cmbLeague.SelectedValue))

                LoadControl(cmbHomeTeam, dsCombo.Tables(0), "TEMNAME", "TEAM_ID")
                LoadControl(cmbAwayTeam, dsCombo.Tables(0), "TEMNAME", "TEAM_ID")
                LoadControl(cmbVenue, dsCombo.Tables(2), "FIELD_NAME", "FIELD_ID")
                LoadControl(cmbModule, dsCombo.Tables(3), "MODULE_DESC", "MODULE_ID")

                Dim dtmin As DateTime
                Dim dtmax As DateTime

                dtpGameDate.MaxDate = Convert.ToDateTime(System.DateTime.Now.AddYears(1))
                dtpGameDate.MinDate = Convert.ToDateTime(System.DateTime.Now.AddYears(1).AddDays(-1))

                '6th oct
                If dsCombo.Tables(4).Rows.Count > 0 Then
                    dtmin = Convert.ToDateTime(dsCombo.Tables(4).Rows(0).Item("START_DATE").ToString())
                    dtmax = Convert.ToDateTime(dsCombo.Tables(4).Rows(0).Item("END_DATE").ToString())
                    dtpGameDate.MinDate = dtmin
                    dtpGameDate.MaxDate = dtmax

                    dtpGameDate.Value = Convert.ToDateTime(dsCombo.Tables(4).Rows(0).Item("START_DATE").ToString())
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function RequireFieldValidation() As Boolean
        Try
            Dim clsvalid As New Utility.clsValidation
            If (Not clsvalid.ValidateEmpty(cmbLeague.Text)) Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLeague.Focus()
                cmbLeague.SelectedIndex = -1
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(dtpGameDate.Text)) Then
                MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                dtpGameDate.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbAwayTeam.Text)) Then
                MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbAwayTeam.Focus()
                cmbAwayTeam.SelectedIndex = -1
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbHomeTeam.Text)) Then
                MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbHomeTeam.Focus()
                cmbHomeTeam.SelectedIndex = -1
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbVenue.Text)) Then
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbVenue.Focus()
                cmbVenue.SelectedIndex = -1
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbCoverageLevel.Text)) Then
                MessageDialog.Show(strmessage4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbCoverageLevel.Focus()
                cmbCoverageLevel.SelectedIndex = -1
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(cmbModule.Text)) Then
                MessageDialog.Show(strmessage12, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbModule.Focus()
                cmbModule.SelectedIndex = -1
                Return False
            ElseIf (CntPlayer(CInt(cmbLeague.SelectedValue.ToString), CInt(cmbAwayTeam.SelectedValue.ToString)) <= 0) Then
                If (cmbAwayTeam.Text <> "") Then
                    MessageDialog.Show(cmbAwayTeam.Text & "  " + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                cmbAwayTeam.Focus()
                Return False
                cmbAwayTeam.SelectedIndex = -1
            ElseIf (CntPlayer(CInt(cmbLeague.SelectedValue.ToString), CInt(cmbHomeTeam.SelectedValue.ToString)) <= 0) Then
                If (cmbHomeTeam.Text <> "") Then
                    MessageDialog.Show(cmbHomeTeam.Text & "  " + strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                cmbHomeTeam.Focus()
                cmbHomeTeam.SelectedIndex = -1
                Return False
            ElseIf (CInt(cmbAwayTeam.SelectedValue.ToString()) = CInt(cmbHomeTeam.SelectedValue.ToString())) Then
                MessageDialog.Show(strmessage2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbHomeTeam.Focus()
                cmbHomeTeam.SelectedIndex = -1
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CntPlayer(ByVal Leagueid As Integer, ByVal teamid As Integer) As Integer
        Dim dsCntPlayer As New DataSet
        Dim CountPlayer As Integer
        dsCntPlayer = m_objAddDemoGame.GetCntPlayer("select count(player_id) as CNT from cd_soccer_roster where league_id=" & Leagueid & " and team_id=" & teamid & " and demo_data='Y'")
        CountPlayer = CInt(dsCntPlayer.Tables(0).Rows(0).Item("CNT"))
        Return CountPlayer
    End Function

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        lvwGames.Enabled = True
        Dim gamecode As Integer
        Try
            If (RequireFieldValidation()) Then
                If (m_strCheckButtonOption = "Save") Then
                    m_objAddDemoGame.InsertAddGame(Convert.ToDateTime(dtpGameDate.Value), CInt(cmbLeague.SelectedValue.ToString()), CInt(cmbAwayTeam.SelectedValue.ToString()), CInt(cmbHomeTeam.SelectedValue.ToString()), CInt(cmbVenue.SelectedValue.ToString()), CInt(cmbCoverageLevel.Text), CInt(cmbModule.SelectedValue.ToString))
                ElseIf m_strCheckButtonOption = "Update" Then
                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                        gamecode = m_intgamecode
                    Next
                    m_objAddDemoGame.UpdateAddGame(gamecode, Convert.ToDateTime(dtpGameDate.Value), CInt(cmbLeague.SelectedValue.ToString()), CInt(cmbAwayTeam.SelectedValue.ToString()), CInt(cmbHomeTeam.SelectedValue.ToString()), CInt(cmbVenue.SelectedValue.ToString()), CInt(cmbCoverageLevel.Text.ToString()), CInt(cmbModule.SelectedValue.ToString()))
                    MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                LoadGames()
                EnableDisableCombo(False, False, False, False, False, False, False)
                EnableDisableButton(False, False, False, True, False)
                m_strCheckButtonOption = ""
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub BtnIgnore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnIgnore.Click
        Try
            EnableDisableCombo(False, False, False, False, False, False, False)
            EnableDisableButton(False, False, False, True, False)
            clearCombo()
            lvwGames.Enabled = True
            m_strCheckButtonOption = ""
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub clearCombo()
        Try
            cmbLeague.SelectedIndex = 0
            cmbAwayTeam.SelectedIndex = -1
            cmbHomeTeam.SelectedIndex = -1
            dtpGameDate.Text = ""
            cmbCoverageLevel.SelectedIndex = -1
            cmbVenue.SelectedIndex = -1
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            EnableDisableCombo(False, True, True, True, True, True, True)
            lvwGames.Enabled = False
            m_strCheckButtonOption = "Update"
            EnableDisableButton(True, True, False, False, False)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
        Try
            lvwGames.Enabled = False
            If (MessageDialog.Show("Are you sure you want to delete?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                m_objAddDemoGame.DeleteAddGame(m_intgamecode)
                clearCombo()
                EnableDisableCombo(False, False, False, False, False, False, False)
                EnableDisableButton(False, False, False, True, False)
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            Else
                EnableDisableButton(False, False, True, True, True)
                lvwGames.Enabled = True
            End If
            LoadGames()
            lvwGames.Enabled = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLeague_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLeague.Validated
        Try
            If cmbLeague.Text <> "" And cmbLeague.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid league", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLeague.Text = ""
                cmbLeague.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbAwayTeam_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAwayTeam.Validated
        Try
            If cmbAwayTeam.Text <> "" And cmbAwayTeam.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid team", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbAwayTeam.Text = ""
                cmbAwayTeam.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbHomeTeam_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbHomeTeam.Validated
        Try
            If cmbHomeTeam.Text <> "" And cmbHomeTeam.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid team", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbHomeTeam.Text = ""
                cmbHomeTeam.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbModule_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbModule.Validated
        Try
            If cmbModule.Text <> "" And cmbModule.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid module", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbModule.Text = ""
                cmbModule.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbVenue_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVenue.Validated
        Try
            If cmbVenue.Text <> "" And cmbVenue.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid venue", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbVenue.Text = ""
                cmbVenue.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbCoverageLevel_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCoverageLevel.Validated
        Try
            If cmbCoverageLevel.Text = "" Then
                MessageDialog.Show("Please select the coverage level", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbCoverageLevel.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class