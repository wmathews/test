﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmAddDemoManager

#Region "Constants Variable"
    Private m_dsNewAddManager As New DataSet
    Private m_strNupId As String
    Private m_strCheckButtonOption As String
    Private m_blnIsLeagueLoaded As Boolean = False
    Private m_blnIsTeamLoaded As Boolean = False
    Private m_objAddManager As clsAddDemoManager = New clsAddDemoManager
    Private m_objGameDetails As New clsGameDetails
    Private m_SeasonID As Integer
    Private MessageDialog As New frmMessageDialog
#End Region

    Private Sub frmAddDemoManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            LoadLeagueCombo()
            CmbLeagueID.SelectedIndex = 0
            EnableDisableButton(True, False, False, False, False)
            EnableDisableTextBox(False, False, False, False)
            CmbLeagueID.Enabled = False
            lvwManager.Enabled = True
            m_blnIsLeagueLoaded = True
            LoadNewlyAddedManager()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub LoadLeagueCombo()
        Try
            Dim dtLeague As New DataTable
            Dim dsLeague As New DataSet
            dsLeague = m_objAddManager.GetLeague()
            dtLeague = dsLeague.Tables(0).Copy
            LoadControl(CmbLeagueID, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable
            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableButton(ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean)
        Try

            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
            btnDelete.Enabled = blnButtonDelete
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableTextBox(ByVal blncmbTeam As Boolean, ByVal blnTextBoxMoniker As Boolean, ByVal blnTextBoxLastName As Boolean, ByVal blnListViewOfficial As Boolean)
        Try
            CmbTeam.Enabled = blncmbTeam
            txtFirstName.Enabled = blnTextBoxMoniker
            txtLastName.Enabled = blnTextBoxLastName
            lvwManager.Enabled = blnListViewOfficial
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadNewlyAddedManager()
        Try
            m_dsNewAddManager = m_objAddManager.SelectAddManager()
            lvwManager.Items.Clear()
            If Not m_dsNewAddManager.Tables(0) Is Nothing Then
                For Each drNewManager As DataRow In m_dsNewAddManager.Tables(0).Rows
                    Dim lvItem As New ListViewItem(drNewManager(0).ToString() + ", " + drNewManager(1).ToString())
                    lvItem.SubItems.Add(drNewManager(12).ToString()) 'TEAM ABBREV

                    'lvItem.SubItems.Add(Format(Convert.ToInt32(drNewManager(4).ToString()), "00"))
                    lvItem.SubItems.Add(drNewManager(2).ToString)
                    lvItem.SubItems.Add(drNewManager(13).ToString) 'LEAGUEID
                    lvwManager.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearTextBoxes()
        Try
            txtFirstName.Text = ""
            txtLastName.Text = ""
            CmbTeam.SelectedValue = 0
            CmbLeagueID.SelectedValue = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ClearTextBoxes()
            m_strCheckButtonOption = "Save"
            lvwManager.Enabled = False
            EnableDisableButton(False, False, False, True, True)
            EnableDisableComboandTextBox(True)
            CmbLeagueID.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub EnableDisableComboandTextBox(ByVal type As Boolean)
        Try
            CmbLeagueID.Enabled = type
            CmbTeam.Enabled = type
            txtFirstName.Enabled = type
            txtLastName.Enabled = type
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function RequireFieldValidation() As Boolean
        Try
            Dim clsvalid As New Utility.clsValidation
            If (Not clsvalid.ValidateEmpty(txtFirstName.Text)) Then
                MessageDialog.Show("Manager firstname is not entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtFirstName.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(txtLastName.Text)) Then
                MessageDialog.Show("Manager lastname is not entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtLastName.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(CmbTeam.Text)) Then
                MessageDialog.Show("Team Abbreviation is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbTeam.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(CmbLeagueID.Text)) Then
                MessageDialog.Show("Select a Leauge to Which the Team Belongs", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbLeagueID.Focus()
                CmbLeagueID.SelectedIndex = -1
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Function

    Private Sub CmbLeagueID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbLeagueID.SelectedIndexChanged
        Try
            If m_blnIsLeagueLoaded = True Then
                LoadNewlyAddedManager()
                LoadTeam(CInt(CmbLeagueID.SelectedValue))
                m_blnIsTeamLoaded = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub LoadTeam(ByVal LeagueID As Integer)
        Try

            Dim dsTeam As New DataSet
            dsTeam = m_objAddManager.GetTeam(LeagueID)

            Dim dtTeam, dtPosition As New DataTable
            dtTeam = dsTeam.Tables(0).Copy
            LoadControl(CmbTeam, dtTeam, "TEMNAME", "TEAM_ID")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        lvwManager.Enabled = True
        Dim intSeasonID, intSequence As Integer
        Try
            If (RequireFieldValidation()) Then
                If (m_strCheckButtonOption = "Save") Then

                    m_strNupId = "98" & Format(Convert.ToInt32(CmbTeam.SelectedValue), "00000") & "00"
                    intSeasonID = GetSeasonID(Convert.ToInt32(CmbLeagueID.SelectedValue.ToString))
                    intSequence = GetSequence()

                    m_objAddManager.InsertUpdateManager(CInt(m_strNupId), intSeasonID, CInt(CmbTeam.SelectedValue), intSequence, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), m_objGameDetails.ReporterRole)
                    LoadNewlyAddedManager()
                    MessageDialog.Show("Manager Details Added Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                ElseIf (m_strCheckButtonOption = "Update") Then
                    'm_strNupId = lvwManager.SelectedItems(0).SubItems(2).Text
                    intSeasonID = GetSeasonID(Convert.ToInt32(CmbLeagueID.SelectedValue.ToString))
                    m_objAddManager.InsertUpdateManager(CInt(m_strNupId), intSeasonID, CInt(CmbTeam.SelectedValue), intSequence, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), m_objGameDetails.ReporterRole)
                    LoadNewlyAddedManager()
                    MessageDialog.Show("Manager Details Updated Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                'LoadTeamDetails()
                EnableDisableComboandTextBox(False)
                CmbLeagueID.Focus()
                EnableDisableButton(True, False, False, False, False)
                m_strCheckButtonOption = "Add"
                ClearTextBoxes()
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function GetSeasonID(ByVal LeagueID As Integer) As Integer
        Try
            Return m_objAddManager.GetSeasonID(LeagueID)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetSequence() As Integer
        Try
            Return m_objAddManager.GetSequence()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lvwManager.Enabled = False
            EnableDisableComboandTextBox(True)
            m_strCheckButtonOption = "Update"
            CmbLeagueID.Focus()
            EnableDisableButton(False, False, False, True, True)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnIgnore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            m_strCheckButtonOption = "Add"
            ClearTextBoxes()
            EnableDisableComboandTextBox(False)
            EnableDisableButton(True, False, False, False, False)
            lvwManager.Enabled = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show("Please Save or Cancel the changes", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwManager_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwManager.Click
        Try
            For Each lvwitm As ListViewItem In lvwManager.SelectedItems
                m_strNupId = CStr(CInt(m_dsNewAddManager.Tables(0).Rows(lvwitm.Index).Item("COACH_ID").ToString))
                m_SeasonID = CInt(m_dsNewAddManager.Tables(0).Rows(lvwitm.Index).Item("SEASON_ID").ToString)
            Next
            EnableDisableTextBox(False, False, False, True)
            EnableDisableButton(True, True, True, False, False)
            SelectedListviewDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub SelectedListviewDisplay()
        Try
            Dim listselectDatarow() As DataRow
            Dim CoachID As Integer
            For Each lvwitm As ListViewItem In lvwManager.SelectedItems
                CoachID = CInt(m_strNupId)
            Next
            listselectDatarow = m_dsNewAddManager.Tables(0).Select("COACH_ID=" & CoachID & "")
            If listselectDatarow.Length > 0 Then
                CmbLeagueID.SelectedValue = listselectDatarow(0).Item(13).ToString()
                CmbTeam.SelectedValue = listselectDatarow(0).Item(6).ToString()
                txtFirstName.Text = listselectDatarow(0).Item(1).ToString()
                txtLastName.Text = listselectDatarow(0).Item(0).ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim CoachID As Integer
            lvwManager.Enabled = False
            If (MessageDialog.Show("Are you sure you want to delete " & txtFirstName.Text & "?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                CoachID = CInt(m_strNupId)
                m_objAddManager.DeletetManager(CoachID, m_SeasonID)
                EnableDisableTextBox(False, False, False, True)
                EnableDisableButton(True, False, False, False, False)
                MessageDialog.Show("Manager Deleted Successfully", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                LoadNewlyAddedManager()
                ClearTextBoxes()
                m_strCheckButtonOption = "Add"
                CmbLeagueID.SelectedIndex = -1
                CmbLeagueID.Focus()
            Else
                lvwManager.Enabled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbLeagueID_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbLeagueID.Validated
        Try
            If CmbLeagueID.Text <> "" And CmbLeagueID.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid league", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbLeagueID.Text = ""
                CmbLeagueID.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbTeam_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbTeam.Validated
        Try
            If CmbTeam.Text <> "" And CmbTeam.SelectedValue Is Nothing Then
                MessageDialog.Show("Please select valid team", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbTeam.Text = ""
                CmbTeam.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class