﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModule1Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Referee:", ""}, -1, System.Drawing.SystemColors.WindowText, System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer)), New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Linesman:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 7.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Linesman:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 7.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"4th official:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 7.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"5th official:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 7.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"6th official:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 7.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Kickoff:", ""}, -1, System.Drawing.SystemColors.InfoText, System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer)), New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Venue:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Attendance:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem10 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Delay:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Dim ListViewItem11 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Home start:", ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
        Me.lblPlayer1Home = New System.Windows.Forms.Label()
        Me.lblPlayer2Home = New System.Windows.Forms.Label()
        Me.lblPlayer3Home = New System.Windows.Forms.Label()
        Me.lblPlayer4Home = New System.Windows.Forms.Label()
        Me.lblPlayer5Home = New System.Windows.Forms.Label()
        Me.lblPlayer6Home = New System.Windows.Forms.Label()
        Me.lblPlayer7Home = New System.Windows.Forms.Label()
        Me.lblPlayer8Home = New System.Windows.Forms.Label()
        Me.lblPlayer9Home = New System.Windows.Forms.Label()
        Me.lblPlayer10Home = New System.Windows.Forms.Label()
        Me.lblPlayer11Home = New System.Windows.Forms.Label()
        Me.lblHomeBench = New System.Windows.Forms.Label()
        Me.lblPlayer11Visit = New System.Windows.Forms.Label()
        Me.lblPlayer10Visit = New System.Windows.Forms.Label()
        Me.lblPlayer9Visit = New System.Windows.Forms.Label()
        Me.lblPlayer8Visit = New System.Windows.Forms.Label()
        Me.lblPlayer7Visit = New System.Windows.Forms.Label()
        Me.lblPlayer6Visit = New System.Windows.Forms.Label()
        Me.lblPlayer5Visit = New System.Windows.Forms.Label()
        Me.lblPlayer4Visit = New System.Windows.Forms.Label()
        Me.lblPlayer3Visit = New System.Windows.Forms.Label()
        Me.lblPlayer2Visit = New System.Windows.Forms.Label()
        Me.lblPlayer1Visit = New System.Windows.Forms.Label()
        Me.lblVisitBench = New System.Windows.Forms.Label()
        Me.pnlStats = New System.Windows.Forms.Panel()
        Me.gpgoaltypes = New System.Windows.Forms.GroupBox()
        Me.lblteam = New System.Windows.Forms.Label()
        Me.btnCancelGoaltype = New System.Windows.Forms.Button()
        Me.btnokgoaltype = New System.Windows.Forms.Button()
        Me.radOwngoal = New System.Windows.Forms.RadioButton()
        Me.radNormalgoal = New System.Windows.Forms.RadioButton()
        Me.radPenalty = New System.Windows.Forms.RadioButton()
        Me.lblgoaltype = New System.Windows.Forms.Label()
        Me.lklEditVisitPenalty = New System.Windows.Forms.LinkLabel()
        Me.lvwVisitPenalties = New System.Windows.Forms.ListView()
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader27 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader45 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader47 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader48 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader49 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklEditVisitSubstitutions = New System.Windows.Forms.LinkLabel()
        Me.lvwVisitSubstitutions = New System.Windows.Forms.ListView()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader37 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader38 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader39 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader40 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader41 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklEditVisitBookings = New System.Windows.Forms.LinkLabel()
        Me.lvwVisitBookings = New System.Windows.Forms.ListView()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader18 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader23 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklEditVisitGoals = New System.Windows.Forms.LinkLabel()
        Me.lklMatchInfo = New System.Windows.Forms.LinkLabel()
        Me.lklOfficials = New System.Windows.Forms.LinkLabel()
        Me.lblMatchInfo = New System.Windows.Forms.Label()
        Me.lblOfficials = New System.Windows.Forms.Label()
        Me.lvwOfficials = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvwMatchInformation = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvwHomeBookings = New System.Windows.Forms.ListView()
        Me.ColumnHeader42 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader19 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader50 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader20 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklNewVisitSub = New System.Windows.Forms.LinkLabel()
        Me.lklEditHomeSubstitutions = New System.Windows.Forms.LinkLabel()
        Me.lklNewHomeSub = New System.Windows.Forms.LinkLabel()
        Me.lblHomeSubstitutions = New System.Windows.Forms.Label()
        Me.lvwHomeSubstitutions = New System.Windows.Forms.ListView()
        Me.ColumnHeader44 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader33 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader52 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader34 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader35 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader36 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklNewVisitPenalty = New System.Windows.Forms.LinkLabel()
        Me.lklEditHomePenalty = New System.Windows.Forms.LinkLabel()
        Me.lklNewHomePenalty = New System.Windows.Forms.LinkLabel()
        Me.lblMissedHomePenalties = New System.Windows.Forms.Label()
        Me.lvwHomePenalties = New System.Windows.Forms.ListView()
        Me.ColumnHeader46 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader26 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader54 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader28 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader29 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader30 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklNewVisitBooking = New System.Windows.Forms.LinkLabel()
        Me.lklEditHomeBookings = New System.Windows.Forms.LinkLabel()
        Me.lklNewHomeBooking = New System.Windows.Forms.LinkLabel()
        Me.lblHomeBookingExpulsions = New System.Windows.Forms.Label()
        Me.lklNewVisitGoal = New System.Windows.Forms.LinkLabel()
        Me.lklEditHomeGoals = New System.Windows.Forms.LinkLabel()
        Me.lklNewHomeGoal = New System.Windows.Forms.LinkLabel()
        Me.lvwHomeGoals = New System.Windows.Forms.ListView()
        Me.ColumnHeader43 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader51 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblHomeGoals = New System.Windows.Forms.Label()
        Me.lvwVisitGoals = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader53 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblVisitGoals = New System.Windows.Forms.Label()
        Me.lblVisitBookingExpulsions = New System.Windows.Forms.Label()
        Me.lblVisitSubstitutions = New System.Windows.Forms.Label()
        Me.lblMissedVisitPenalties = New System.Windows.Forms.Label()
        Me.pnlHome = New System.Windows.Forms.Panel()
        Me.lblPosition11Home = New System.Windows.Forms.Label()
        Me.lblPosition10Home = New System.Windows.Forms.Label()
        Me.lblPosition9Home = New System.Windows.Forms.Label()
        Me.lblPosition8Home = New System.Windows.Forms.Label()
        Me.lblPosition7Home = New System.Windows.Forms.Label()
        Me.lblPosition6Home = New System.Windows.Forms.Label()
        Me.lblPosition5Home = New System.Windows.Forms.Label()
        Me.lblPosition4Home = New System.Windows.Forms.Label()
        Me.lblPosition3Home = New System.Windows.Forms.Label()
        Me.lblPosition2Home = New System.Windows.Forms.Label()
        Me.lblPosition1Home = New System.Windows.Forms.Label()
        Me.pnlHomeBench = New System.Windows.Forms.Panel()
        Me.lblSubPosition26Home = New System.Windows.Forms.Label()
        Me.lblSubPosition27Home = New System.Windows.Forms.Label()
        Me.lblSubPosition28Home = New System.Windows.Forms.Label()
        Me.lblSubPosition29Home = New System.Windows.Forms.Label()
        Me.lblSubPosition30Home = New System.Windows.Forms.Label()
        Me.lblSubPosition20Home = New System.Windows.Forms.Label()
        Me.lblSubPosition21Home = New System.Windows.Forms.Label()
        Me.lblSubPosition22Home = New System.Windows.Forms.Label()
        Me.lblSubPosition23Home = New System.Windows.Forms.Label()
        Me.lblSubPosition24Home = New System.Windows.Forms.Label()
        Me.lblSubPosition25Home = New System.Windows.Forms.Label()
        Me.lblSubPosition14Home = New System.Windows.Forms.Label()
        Me.lblSubPosition15Home = New System.Windows.Forms.Label()
        Me.lblSubPosition16Home = New System.Windows.Forms.Label()
        Me.lblSubPosition17Home = New System.Windows.Forms.Label()
        Me.lblSubPosition18Home = New System.Windows.Forms.Label()
        Me.lblSubPosition19Home = New System.Windows.Forms.Label()
        Me.lblSubPosition8Home = New System.Windows.Forms.Label()
        Me.lblSubPosition9Home = New System.Windows.Forms.Label()
        Me.lblSubPosition10Home = New System.Windows.Forms.Label()
        Me.lblSubPosition11Home = New System.Windows.Forms.Label()
        Me.lblSubPosition12Home = New System.Windows.Forms.Label()
        Me.lblSubPosition13Home = New System.Windows.Forms.Label()
        Me.lblSubPosition1Home = New System.Windows.Forms.Label()
        Me.lblSubPosition2Home = New System.Windows.Forms.Label()
        Me.lblSubPosition3Home = New System.Windows.Forms.Label()
        Me.lblSubPosition4Home = New System.Windows.Forms.Label()
        Me.lblSubPosition5Home = New System.Windows.Forms.Label()
        Me.lblSubPosition6Home = New System.Windows.Forms.Label()
        Me.lblSubPosition7Home = New System.Windows.Forms.Label()
        Me.lblSub26Home = New System.Windows.Forms.Label()
        Me.lblSub27Home = New System.Windows.Forms.Label()
        Me.lblSub28Home = New System.Windows.Forms.Label()
        Me.lblSub29Home = New System.Windows.Forms.Label()
        Me.lblSub30Home = New System.Windows.Forms.Label()
        Me.lblSub20Home = New System.Windows.Forms.Label()
        Me.lblSub21Home = New System.Windows.Forms.Label()
        Me.lblSub22Home = New System.Windows.Forms.Label()
        Me.lblSub23Home = New System.Windows.Forms.Label()
        Me.lblSub24Home = New System.Windows.Forms.Label()
        Me.lblSub25Home = New System.Windows.Forms.Label()
        Me.lblSub14Home = New System.Windows.Forms.Label()
        Me.lblSub15Home = New System.Windows.Forms.Label()
        Me.lblSub16Home = New System.Windows.Forms.Label()
        Me.lblSub17Home = New System.Windows.Forms.Label()
        Me.lblSub18Home = New System.Windows.Forms.Label()
        Me.lblSub19Home = New System.Windows.Forms.Label()
        Me.lblSub8Home = New System.Windows.Forms.Label()
        Me.lblSub9Home = New System.Windows.Forms.Label()
        Me.lblSub10Home = New System.Windows.Forms.Label()
        Me.lblSub11Home = New System.Windows.Forms.Label()
        Me.lblSub12Home = New System.Windows.Forms.Label()
        Me.lblSub13Home = New System.Windows.Forms.Label()
        Me.lblSub1Home = New System.Windows.Forms.Label()
        Me.lblSub2Home = New System.Windows.Forms.Label()
        Me.lblSub3Home = New System.Windows.Forms.Label()
        Me.lblSub4Home = New System.Windows.Forms.Label()
        Me.lblSub5Home = New System.Windows.Forms.Label()
        Me.lblSub6Home = New System.Windows.Forms.Label()
        Me.lblSub7Home = New System.Windows.Forms.Label()
        Me.lblHomeManager = New System.Windows.Forms.Label()
        Me.lblManagerHome = New System.Windows.Forms.Label()
        Me.pnlVisit = New System.Windows.Forms.Panel()
        Me.lblPosition11Visit = New System.Windows.Forms.Label()
        Me.lblPosition10Visit = New System.Windows.Forms.Label()
        Me.lblPosition9Visit = New System.Windows.Forms.Label()
        Me.lblPosition8Visit = New System.Windows.Forms.Label()
        Me.lblPosition7Visit = New System.Windows.Forms.Label()
        Me.lblPosition6Visit = New System.Windows.Forms.Label()
        Me.lblPosition5Visit = New System.Windows.Forms.Label()
        Me.lblPosition4Visit = New System.Windows.Forms.Label()
        Me.lblPosition3Visit = New System.Windows.Forms.Label()
        Me.lblPosition2Visit = New System.Windows.Forms.Label()
        Me.lblPosition1Visit = New System.Windows.Forms.Label()
        Me.pnlVisitBench = New System.Windows.Forms.Panel()
        Me.lblSubPosition26Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition27Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition28Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition29Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition30Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition20Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition21Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition22Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition23Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition24Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition25Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition14Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition15Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition16Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition17Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition18Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition19Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition8Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition9Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition10Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition11Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition12Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition13Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition1Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition2Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition3Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition4Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition5Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition6Visit = New System.Windows.Forms.Label()
        Me.lblSubPosition7Visit = New System.Windows.Forms.Label()
        Me.lblSub26Visit = New System.Windows.Forms.Label()
        Me.lblSub27Visit = New System.Windows.Forms.Label()
        Me.lblSub28Visit = New System.Windows.Forms.Label()
        Me.lblSub29Visit = New System.Windows.Forms.Label()
        Me.lblSub30Visit = New System.Windows.Forms.Label()
        Me.lblSub20Visit = New System.Windows.Forms.Label()
        Me.lblSub21Visit = New System.Windows.Forms.Label()
        Me.lblSub22Visit = New System.Windows.Forms.Label()
        Me.lblSub23Visit = New System.Windows.Forms.Label()
        Me.lblSub24Visit = New System.Windows.Forms.Label()
        Me.lblSub25Visit = New System.Windows.Forms.Label()
        Me.lblSub14Visit = New System.Windows.Forms.Label()
        Me.lblSub15Visit = New System.Windows.Forms.Label()
        Me.lblSub16Visit = New System.Windows.Forms.Label()
        Me.lblSub17Visit = New System.Windows.Forms.Label()
        Me.lblSub18Visit = New System.Windows.Forms.Label()
        Me.lblSub19Visit = New System.Windows.Forms.Label()
        Me.lblSub8Visit = New System.Windows.Forms.Label()
        Me.lblSub9Visit = New System.Windows.Forms.Label()
        Me.lblSub10Visit = New System.Windows.Forms.Label()
        Me.lblSub11Visit = New System.Windows.Forms.Label()
        Me.lblSub12Visit = New System.Windows.Forms.Label()
        Me.lblSub13Visit = New System.Windows.Forms.Label()
        Me.lblSub1Visit = New System.Windows.Forms.Label()
        Me.lblSub2Visit = New System.Windows.Forms.Label()
        Me.lblSub3Visit = New System.Windows.Forms.Label()
        Me.lblSub4Visit = New System.Windows.Forms.Label()
        Me.lblSub5Visit = New System.Windows.Forms.Label()
        Me.lblSub6Visit = New System.Windows.Forms.Label()
        Me.lblSub7Visit = New System.Windows.Forms.Label()
        Me.lblVisitManager = New System.Windows.Forms.Label()
        Me.lblManagerAway = New System.Windows.Forms.Label()
        Me.btnSwitchGames = New System.Windows.Forms.Button()
        Me.pnlStats.SuspendLayout()
        Me.gpgoaltypes.SuspendLayout()
        Me.pnlHome.SuspendLayout()
        Me.pnlHomeBench.SuspendLayout()
        Me.pnlVisit.SuspendLayout()
        Me.pnlVisitBench.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblPlayer1Home
        '
        Me.lblPlayer1Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer1Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer1Home.Location = New System.Drawing.Point(0, 0)
        Me.lblPlayer1Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer1Home.Name = "lblPlayer1Home"
        Me.lblPlayer1Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer1Home.TabIndex = 0
        Me.lblPlayer1Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer2Home
        '
        Me.lblPlayer2Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer2Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer2Home.Location = New System.Drawing.Point(0, 23)
        Me.lblPlayer2Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer2Home.Name = "lblPlayer2Home"
        Me.lblPlayer2Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer2Home.TabIndex = 1
        Me.lblPlayer2Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer3Home
        '
        Me.lblPlayer3Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer3Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer3Home.Location = New System.Drawing.Point(0, 46)
        Me.lblPlayer3Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer3Home.Name = "lblPlayer3Home"
        Me.lblPlayer3Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer3Home.TabIndex = 2
        Me.lblPlayer3Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer4Home
        '
        Me.lblPlayer4Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer4Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer4Home.Location = New System.Drawing.Point(0, 69)
        Me.lblPlayer4Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer4Home.Name = "lblPlayer4Home"
        Me.lblPlayer4Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer4Home.TabIndex = 3
        Me.lblPlayer4Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer5Home
        '
        Me.lblPlayer5Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer5Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer5Home.Location = New System.Drawing.Point(0, 92)
        Me.lblPlayer5Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer5Home.Name = "lblPlayer5Home"
        Me.lblPlayer5Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer5Home.TabIndex = 4
        Me.lblPlayer5Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer6Home
        '
        Me.lblPlayer6Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer6Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer6Home.Location = New System.Drawing.Point(0, 115)
        Me.lblPlayer6Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer6Home.Name = "lblPlayer6Home"
        Me.lblPlayer6Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer6Home.TabIndex = 5
        Me.lblPlayer6Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer7Home
        '
        Me.lblPlayer7Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer7Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer7Home.Location = New System.Drawing.Point(0, 138)
        Me.lblPlayer7Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer7Home.Name = "lblPlayer7Home"
        Me.lblPlayer7Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer7Home.TabIndex = 6
        Me.lblPlayer7Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer8Home
        '
        Me.lblPlayer8Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer8Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer8Home.Location = New System.Drawing.Point(0, 161)
        Me.lblPlayer8Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer8Home.Name = "lblPlayer8Home"
        Me.lblPlayer8Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer8Home.TabIndex = 7
        Me.lblPlayer8Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer9Home
        '
        Me.lblPlayer9Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer9Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer9Home.Location = New System.Drawing.Point(0, 184)
        Me.lblPlayer9Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer9Home.Name = "lblPlayer9Home"
        Me.lblPlayer9Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer9Home.TabIndex = 8
        Me.lblPlayer9Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer10Home
        '
        Me.lblPlayer10Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer10Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer10Home.Location = New System.Drawing.Point(0, 207)
        Me.lblPlayer10Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer10Home.Name = "lblPlayer10Home"
        Me.lblPlayer10Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer10Home.TabIndex = 9
        Me.lblPlayer10Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer11Home
        '
        Me.lblPlayer11Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer11Home.ForeColor = System.Drawing.Color.White
        Me.lblPlayer11Home.Location = New System.Drawing.Point(0, 230)
        Me.lblPlayer11Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer11Home.Name = "lblPlayer11Home"
        Me.lblPlayer11Home.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer11Home.TabIndex = 10
        Me.lblPlayer11Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHomeBench
        '
        Me.lblHomeBench.AutoSize = True
        Me.lblHomeBench.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeBench.Location = New System.Drawing.Point(0, 264)
        Me.lblHomeBench.Name = "lblHomeBench"
        Me.lblHomeBench.Size = New System.Drawing.Size(47, 15)
        Me.lblHomeBench.TabIndex = 214
        Me.lblHomeBench.Text = "Bench:"
        Me.lblHomeBench.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer11Visit
        '
        Me.lblPlayer11Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer11Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer11Visit.Location = New System.Drawing.Point(0, 230)
        Me.lblPlayer11Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer11Visit.Name = "lblPlayer11Visit"
        Me.lblPlayer11Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer11Visit.TabIndex = 232
        Me.lblPlayer11Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer10Visit
        '
        Me.lblPlayer10Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer10Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer10Visit.Location = New System.Drawing.Point(0, 207)
        Me.lblPlayer10Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer10Visit.Name = "lblPlayer10Visit"
        Me.lblPlayer10Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer10Visit.TabIndex = 231
        Me.lblPlayer10Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer9Visit
        '
        Me.lblPlayer9Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer9Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer9Visit.Location = New System.Drawing.Point(0, 184)
        Me.lblPlayer9Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer9Visit.Name = "lblPlayer9Visit"
        Me.lblPlayer9Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer9Visit.TabIndex = 230
        Me.lblPlayer9Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer8Visit
        '
        Me.lblPlayer8Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer8Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer8Visit.Location = New System.Drawing.Point(0, 161)
        Me.lblPlayer8Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer8Visit.Name = "lblPlayer8Visit"
        Me.lblPlayer8Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer8Visit.TabIndex = 229
        Me.lblPlayer8Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer7Visit
        '
        Me.lblPlayer7Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer7Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer7Visit.Location = New System.Drawing.Point(0, 138)
        Me.lblPlayer7Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer7Visit.Name = "lblPlayer7Visit"
        Me.lblPlayer7Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer7Visit.TabIndex = 228
        Me.lblPlayer7Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer6Visit
        '
        Me.lblPlayer6Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer6Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer6Visit.Location = New System.Drawing.Point(0, 115)
        Me.lblPlayer6Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer6Visit.Name = "lblPlayer6Visit"
        Me.lblPlayer6Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer6Visit.TabIndex = 227
        Me.lblPlayer6Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer5Visit
        '
        Me.lblPlayer5Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer5Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer5Visit.Location = New System.Drawing.Point(0, 92)
        Me.lblPlayer5Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer5Visit.Name = "lblPlayer5Visit"
        Me.lblPlayer5Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer5Visit.TabIndex = 226
        Me.lblPlayer5Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer4Visit
        '
        Me.lblPlayer4Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer4Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer4Visit.Location = New System.Drawing.Point(0, 69)
        Me.lblPlayer4Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer4Visit.Name = "lblPlayer4Visit"
        Me.lblPlayer4Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer4Visit.TabIndex = 225
        Me.lblPlayer4Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer3Visit
        '
        Me.lblPlayer3Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer3Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer3Visit.Location = New System.Drawing.Point(0, 46)
        Me.lblPlayer3Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer3Visit.Name = "lblPlayer3Visit"
        Me.lblPlayer3Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer3Visit.TabIndex = 224
        Me.lblPlayer3Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer2Visit
        '
        Me.lblPlayer2Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPlayer2Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer2Visit.Location = New System.Drawing.Point(0, 23)
        Me.lblPlayer2Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer2Visit.Name = "lblPlayer2Visit"
        Me.lblPlayer2Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer2Visit.TabIndex = 223
        Me.lblPlayer2Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlayer1Visit
        '
        Me.lblPlayer1Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPlayer1Visit.ForeColor = System.Drawing.Color.White
        Me.lblPlayer1Visit.Location = New System.Drawing.Point(0, 0)
        Me.lblPlayer1Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPlayer1Visit.Name = "lblPlayer1Visit"
        Me.lblPlayer1Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblPlayer1Visit.TabIndex = 222
        Me.lblPlayer1Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVisitBench
        '
        Me.lblVisitBench.AutoSize = True
        Me.lblVisitBench.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVisitBench.Location = New System.Drawing.Point(0, 264)
        Me.lblVisitBench.Name = "lblVisitBench"
        Me.lblVisitBench.Size = New System.Drawing.Size(47, 15)
        Me.lblVisitBench.TabIndex = 241
        Me.lblVisitBench.Text = "Bench:"
        Me.lblVisitBench.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStats
        '
        Me.pnlStats.AutoScroll = True
        Me.pnlStats.BackColor = System.Drawing.Color.Honeydew
        Me.pnlStats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlStats.Controls.Add(Me.gpgoaltypes)
        Me.pnlStats.Controls.Add(Me.lklEditVisitPenalty)
        Me.pnlStats.Controls.Add(Me.lvwVisitPenalties)
        Me.pnlStats.Controls.Add(Me.lklEditVisitSubstitutions)
        Me.pnlStats.Controls.Add(Me.lvwVisitSubstitutions)
        Me.pnlStats.Controls.Add(Me.lklEditVisitBookings)
        Me.pnlStats.Controls.Add(Me.lvwVisitBookings)
        Me.pnlStats.Controls.Add(Me.lklEditVisitGoals)
        Me.pnlStats.Controls.Add(Me.lklMatchInfo)
        Me.pnlStats.Controls.Add(Me.lklOfficials)
        Me.pnlStats.Controls.Add(Me.lblMatchInfo)
        Me.pnlStats.Controls.Add(Me.lblOfficials)
        Me.pnlStats.Controls.Add(Me.lvwOfficials)
        Me.pnlStats.Controls.Add(Me.lvwMatchInformation)
        Me.pnlStats.Controls.Add(Me.lvwHomeBookings)
        Me.pnlStats.Controls.Add(Me.lklNewVisitSub)
        Me.pnlStats.Controls.Add(Me.lklEditHomeSubstitutions)
        Me.pnlStats.Controls.Add(Me.lklNewHomeSub)
        Me.pnlStats.Controls.Add(Me.lblHomeSubstitutions)
        Me.pnlStats.Controls.Add(Me.lvwHomeSubstitutions)
        Me.pnlStats.Controls.Add(Me.lklNewVisitPenalty)
        Me.pnlStats.Controls.Add(Me.lklEditHomePenalty)
        Me.pnlStats.Controls.Add(Me.lklNewHomePenalty)
        Me.pnlStats.Controls.Add(Me.lblMissedHomePenalties)
        Me.pnlStats.Controls.Add(Me.lvwHomePenalties)
        Me.pnlStats.Controls.Add(Me.lklNewVisitBooking)
        Me.pnlStats.Controls.Add(Me.lklEditHomeBookings)
        Me.pnlStats.Controls.Add(Me.lklNewHomeBooking)
        Me.pnlStats.Controls.Add(Me.lblHomeBookingExpulsions)
        Me.pnlStats.Controls.Add(Me.lklNewVisitGoal)
        Me.pnlStats.Controls.Add(Me.lklEditHomeGoals)
        Me.pnlStats.Controls.Add(Me.lklNewHomeGoal)
        Me.pnlStats.Controls.Add(Me.lvwHomeGoals)
        Me.pnlStats.Controls.Add(Me.lblHomeGoals)
        Me.pnlStats.Controls.Add(Me.lvwVisitGoals)
        Me.pnlStats.Controls.Add(Me.lblVisitGoals)
        Me.pnlStats.Controls.Add(Me.lblVisitBookingExpulsions)
        Me.pnlStats.Controls.Add(Me.lblVisitSubstitutions)
        Me.pnlStats.Controls.Add(Me.lblMissedVisitPenalties)
        Me.pnlStats.Location = New System.Drawing.Point(155, 6)
        Me.pnlStats.Name = "pnlStats"
        Me.pnlStats.Size = New System.Drawing.Size(705, 451)
        Me.pnlStats.TabIndex = 253
        '
        'gpgoaltypes
        '
        Me.gpgoaltypes.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer))
        Me.gpgoaltypes.Controls.Add(Me.lblteam)
        Me.gpgoaltypes.Controls.Add(Me.btnCancelGoaltype)
        Me.gpgoaltypes.Controls.Add(Me.btnokgoaltype)
        Me.gpgoaltypes.Controls.Add(Me.radOwngoal)
        Me.gpgoaltypes.Controls.Add(Me.radNormalgoal)
        Me.gpgoaltypes.Controls.Add(Me.radPenalty)
        Me.gpgoaltypes.Controls.Add(Me.lblgoaltype)
        Me.gpgoaltypes.Location = New System.Drawing.Point(354, 115)
        Me.gpgoaltypes.Name = "gpgoaltypes"
        Me.gpgoaltypes.Size = New System.Drawing.Size(188, 137)
        Me.gpgoaltypes.TabIndex = 295
        Me.gpgoaltypes.TabStop = False
        Me.gpgoaltypes.Text = "Goal Types"
        Me.gpgoaltypes.Visible = False
        '
        'lblteam
        '
        Me.lblteam.AutoSize = True
        Me.lblteam.Location = New System.Drawing.Point(87, 7)
        Me.lblteam.Name = "lblteam"
        Me.lblteam.Size = New System.Drawing.Size(0, 15)
        Me.lblteam.TabIndex = 6
        Me.lblteam.Visible = False
        '
        'btnCancelGoaltype
        '
        Me.btnCancelGoaltype.Location = New System.Drawing.Point(50, 107)
        Me.btnCancelGoaltype.Name = "btnCancelGoaltype"
        Me.btnCancelGoaltype.Size = New System.Drawing.Size(50, 23)
        Me.btnCancelGoaltype.TabIndex = 5
        Me.btnCancelGoaltype.Text = "Cancel"
        Me.btnCancelGoaltype.UseVisualStyleBackColor = True
        '
        'btnokgoaltype
        '
        Me.btnokgoaltype.Location = New System.Drawing.Point(6, 107)
        Me.btnokgoaltype.Name = "btnokgoaltype"
        Me.btnokgoaltype.Size = New System.Drawing.Size(40, 23)
        Me.btnokgoaltype.TabIndex = 4
        Me.btnokgoaltype.Text = "Ok"
        Me.btnokgoaltype.UseVisualStyleBackColor = True
        '
        'radOwngoal
        '
        Me.radOwngoal.AutoSize = True
        Me.radOwngoal.Location = New System.Drawing.Point(11, 75)
        Me.radOwngoal.Name = "radOwngoal"
        Me.radOwngoal.Size = New System.Drawing.Size(48, 19)
        Me.radOwngoal.TabIndex = 3
        Me.radOwngoal.TabStop = True
        Me.radOwngoal.Tag = "28"
        Me.radOwngoal.Text = "Own"
        Me.radOwngoal.UseVisualStyleBackColor = True
        '
        'radNormalgoal
        '
        Me.radNormalgoal.AutoSize = True
        Me.radNormalgoal.Location = New System.Drawing.Point(11, 55)
        Me.radNormalgoal.Name = "radNormalgoal"
        Me.radNormalgoal.Size = New System.Drawing.Size(59, 19)
        Me.radNormalgoal.TabIndex = 2
        Me.radNormalgoal.TabStop = True
        Me.radNormalgoal.Tag = "11"
        Me.radNormalgoal.Text = "Normal"
        Me.radNormalgoal.UseVisualStyleBackColor = True
        '
        'radPenalty
        '
        Me.radPenalty.AutoSize = True
        Me.radPenalty.Location = New System.Drawing.Point(11, 36)
        Me.radPenalty.Name = "radPenalty"
        Me.radPenalty.Size = New System.Drawing.Size(61, 19)
        Me.radPenalty.TabIndex = 1
        Me.radPenalty.TabStop = True
        Me.radPenalty.Tag = "17"
        Me.radPenalty.Text = "Penalty"
        Me.radPenalty.UseVisualStyleBackColor = True
        '
        'lblgoaltype
        '
        Me.lblgoaltype.AutoSize = True
        Me.lblgoaltype.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblgoaltype.Location = New System.Drawing.Point(3, 18)
        Me.lblgoaltype.Name = "lblgoaltype"
        Me.lblgoaltype.Size = New System.Drawing.Size(174, 15)
        Me.lblgoaltype.TabIndex = 0
        Me.lblgoaltype.Text = "Please choose the Goal type"
        '
        'lklEditVisitPenalty
        '
        Me.lklEditVisitPenalty.AutoSize = True
        Me.lklEditVisitPenalty.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditVisitPenalty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditVisitPenalty.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditVisitPenalty.LinkColor = System.Drawing.Color.White
        Me.lklEditVisitPenalty.Location = New System.Drawing.Point(645, 290)
        Me.lklEditVisitPenalty.Name = "lklEditVisitPenalty"
        Me.lklEditVisitPenalty.Size = New System.Drawing.Size(38, 17)
        Me.lklEditVisitPenalty.TabIndex = 294
        Me.lklEditVisitPenalty.TabStop = True
        Me.lklEditVisitPenalty.Text = "EDIT"
        '
        'lvwVisitPenalties
        '
        Me.lvwVisitPenalties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwVisitPenalties.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader8, Me.ColumnHeader27, Me.ColumnHeader45, Me.ColumnHeader47, Me.ColumnHeader48, Me.ColumnHeader49})
        Me.lvwVisitPenalties.FullRowSelect = True
        Me.lvwVisitPenalties.Location = New System.Drawing.Point(346, 306)
        Me.lvwVisitPenalties.Name = "lvwVisitPenalties"
        Me.lvwVisitPenalties.Size = New System.Drawing.Size(337, 40)
        Me.lvwVisitPenalties.TabIndex = 292
        Me.lvwVisitPenalties.UseCompatibleStateImageBehavior = False
        Me.lvwVisitPenalties.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "SlNo"
        Me.ColumnHeader8.Width = 0
        '
        'ColumnHeader27
        '
        Me.ColumnHeader27.Text = "Time"
        Me.ColumnHeader27.Width = 40
        '
        'ColumnHeader45
        '
        Me.ColumnHeader45.Text = "Period"
        Me.ColumnHeader45.Width = 50
        '
        'ColumnHeader47
        '
        Me.ColumnHeader47.Text = "Player missed"
        Me.ColumnHeader47.Width = 100
        '
        'ColumnHeader48
        '
        Me.ColumnHeader48.Text = "Saved"
        Me.ColumnHeader48.Width = 80
        '
        'ColumnHeader49
        '
        Me.ColumnHeader49.Text = "Caused by"
        '
        'lklEditVisitSubstitutions
        '
        Me.lklEditVisitSubstitutions.AutoSize = True
        Me.lklEditVisitSubstitutions.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditVisitSubstitutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditVisitSubstitutions.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditVisitSubstitutions.LinkColor = System.Drawing.Color.White
        Me.lklEditVisitSubstitutions.Location = New System.Drawing.Point(645, 229)
        Me.lklEditVisitSubstitutions.Name = "lklEditVisitSubstitutions"
        Me.lklEditVisitSubstitutions.Size = New System.Drawing.Size(38, 17)
        Me.lklEditVisitSubstitutions.TabIndex = 291
        Me.lklEditVisitSubstitutions.TabStop = True
        Me.lklEditVisitSubstitutions.Text = "EDIT"
        '
        'lvwVisitSubstitutions
        '
        Me.lvwVisitSubstitutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwVisitSubstitutions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader37, Me.ColumnHeader38, Me.ColumnHeader39, Me.ColumnHeader40, Me.ColumnHeader41})
        Me.lvwVisitSubstitutions.FullRowSelect = True
        Me.lvwVisitSubstitutions.Location = New System.Drawing.Point(346, 245)
        Me.lvwVisitSubstitutions.Name = "lvwVisitSubstitutions"
        Me.lvwVisitSubstitutions.Size = New System.Drawing.Size(337, 40)
        Me.lvwVisitSubstitutions.TabIndex = 289
        Me.lvwVisitSubstitutions.UseCompatibleStateImageBehavior = False
        Me.lvwVisitSubstitutions.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "SlNo"
        Me.ColumnHeader7.Width = 0
        '
        'ColumnHeader37
        '
        Me.ColumnHeader37.Text = "Time"
        Me.ColumnHeader37.Width = 40
        '
        'ColumnHeader38
        '
        Me.ColumnHeader38.Text = "Period"
        Me.ColumnHeader38.Width = 50
        '
        'ColumnHeader39
        '
        Me.ColumnHeader39.Text = "Player out"
        Me.ColumnHeader39.Width = 100
        '
        'ColumnHeader40
        '
        Me.ColumnHeader40.Text = "Player in"
        Me.ColumnHeader40.Width = 80
        '
        'ColumnHeader41
        '
        Me.ColumnHeader41.Text = "Reason"
        '
        'lklEditVisitBookings
        '
        Me.lklEditVisitBookings.AutoSize = True
        Me.lklEditVisitBookings.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditVisitBookings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditVisitBookings.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditVisitBookings.LinkColor = System.Drawing.Color.White
        Me.lklEditVisitBookings.Location = New System.Drawing.Point(645, 168)
        Me.lklEditVisitBookings.Name = "lklEditVisitBookings"
        Me.lklEditVisitBookings.Size = New System.Drawing.Size(38, 17)
        Me.lklEditVisitBookings.TabIndex = 288
        Me.lklEditVisitBookings.TabStop = True
        Me.lklEditVisitBookings.Text = "EDIT"
        '
        'lvwVisitBookings
        '
        Me.lvwVisitBookings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwVisitBookings.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader17, Me.ColumnHeader18, Me.ColumnHeader23, Me.ColumnHeader24, Me.ColumnHeader25})
        Me.lvwVisitBookings.FullRowSelect = True
        Me.lvwVisitBookings.Location = New System.Drawing.Point(346, 184)
        Me.lvwVisitBookings.Name = "lvwVisitBookings"
        Me.lvwVisitBookings.Size = New System.Drawing.Size(337, 40)
        Me.lvwVisitBookings.TabIndex = 286
        Me.lvwVisitBookings.UseCompatibleStateImageBehavior = False
        Me.lvwVisitBookings.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "SlNo"
        Me.ColumnHeader6.Width = 0
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Time"
        Me.ColumnHeader17.Width = 40
        '
        'ColumnHeader18
        '
        Me.ColumnHeader18.Text = "Period"
        Me.ColumnHeader18.Width = 50
        '
        'ColumnHeader23
        '
        Me.ColumnHeader23.Text = "Player"
        Me.ColumnHeader23.Width = 100
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "Reason"
        Me.ColumnHeader24.Width = 80
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "Card"
        '
        'lklEditVisitGoals
        '
        Me.lklEditVisitGoals.AutoSize = True
        Me.lklEditVisitGoals.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditVisitGoals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditVisitGoals.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditVisitGoals.LinkColor = System.Drawing.Color.White
        Me.lklEditVisitGoals.Location = New System.Drawing.Point(645, 107)
        Me.lklEditVisitGoals.Name = "lklEditVisitGoals"
        Me.lklEditVisitGoals.Size = New System.Drawing.Size(38, 17)
        Me.lklEditVisitGoals.TabIndex = 285
        Me.lklEditVisitGoals.TabStop = True
        Me.lklEditVisitGoals.Text = "EDIT"
        '
        'lklMatchInfo
        '
        Me.lklMatchInfo.AutoSize = True
        Me.lklMatchInfo.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklMatchInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklMatchInfo.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklMatchInfo.LinkColor = System.Drawing.Color.White
        Me.lklMatchInfo.Location = New System.Drawing.Point(645, 3)
        Me.lklMatchInfo.Name = "lklMatchInfo"
        Me.lklMatchInfo.Size = New System.Drawing.Size(38, 17)
        Me.lklMatchInfo.TabIndex = 282
        Me.lklMatchInfo.TabStop = True
        Me.lklMatchInfo.Text = "EDIT"
        '
        'lklOfficials
        '
        Me.lklOfficials.AutoSize = True
        Me.lklOfficials.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklOfficials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklOfficials.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklOfficials.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.lklOfficials.LinkColor = System.Drawing.Color.White
        Me.lklOfficials.Location = New System.Drawing.Point(303, 3)
        Me.lklOfficials.Name = "lklOfficials"
        Me.lklOfficials.Size = New System.Drawing.Size(38, 17)
        Me.lklOfficials.TabIndex = 281
        Me.lklOfficials.TabStop = True
        Me.lklOfficials.Text = "EDIT"
        '
        'lblMatchInfo
        '
        Me.lblMatchInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblMatchInfo.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMatchInfo.ForeColor = System.Drawing.Color.White
        Me.lblMatchInfo.Location = New System.Drawing.Point(346, 3)
        Me.lblMatchInfo.Name = "lblMatchInfo"
        Me.lblMatchInfo.Size = New System.Drawing.Size(337, 16)
        Me.lblMatchInfo.TabIndex = 279
        Me.lblMatchInfo.Text = "Match information:"
        '
        'lblOfficials
        '
        Me.lblOfficials.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblOfficials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOfficials.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOfficials.ForeColor = System.Drawing.Color.White
        Me.lblOfficials.Location = New System.Drawing.Point(4, 3)
        Me.lblOfficials.Name = "lblOfficials"
        Me.lblOfficials.Size = New System.Drawing.Size(337, 16)
        Me.lblOfficials.TabIndex = 277
        Me.lblOfficials.Text = "Officials:"
        '
        'lvwOfficials
        '
        Me.lvwOfficials.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer))
        Me.lvwOfficials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwOfficials.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvwOfficials.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem1.UseItemStyleForSubItems = False
        ListViewItem2.UseItemStyleForSubItems = False
        ListViewItem3.UseItemStyleForSubItems = False
        ListViewItem4.UseItemStyleForSubItems = False
        ListViewItem5.UseItemStyleForSubItems = False
        ListViewItem6.UseItemStyleForSubItems = False
        Me.lvwOfficials.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6})
        Me.lvwOfficials.Location = New System.Drawing.Point(4, 19)
        Me.lvwOfficials.MultiSelect = False
        Me.lvwOfficials.Name = "lvwOfficials"
        Me.lvwOfficials.Size = New System.Drawing.Size(337, 83)
        Me.lvwOfficials.TabIndex = 278
        Me.lvwOfficials.UseCompatibleStateImageBehavior = False
        Me.lvwOfficials.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Role"
        Me.ColumnHeader1.Width = 80
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Name"
        Me.ColumnHeader2.Width = 150
        '
        'lvwMatchInformation
        '
        Me.lvwMatchInformation.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(232, Byte), Integer))
        Me.lvwMatchInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwMatchInformation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvwMatchInformation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem7.UseItemStyleForSubItems = False
        ListViewItem8.UseItemStyleForSubItems = False
        ListViewItem9.UseItemStyleForSubItems = False
        ListViewItem10.UseItemStyleForSubItems = False
        ListViewItem11.UseItemStyleForSubItems = False
        Me.lvwMatchInformation.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem7, ListViewItem8, ListViewItem9, ListViewItem10, ListViewItem11})
        Me.lvwMatchInformation.Location = New System.Drawing.Point(346, 19)
        Me.lvwMatchInformation.MultiSelect = False
        Me.lvwMatchInformation.Name = "lvwMatchInformation"
        Me.lvwMatchInformation.Size = New System.Drawing.Size(337, 83)
        Me.lvwMatchInformation.TabIndex = 280
        Me.lvwMatchInformation.UseCompatibleStateImageBehavior = False
        Me.lvwMatchInformation.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Role"
        Me.ColumnHeader3.Width = 80
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Name"
        Me.ColumnHeader4.Width = 150
        '
        'lvwHomeBookings
        '
        Me.lvwHomeBookings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwHomeBookings.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader42, Me.ColumnHeader19, Me.ColumnHeader50, Me.ColumnHeader20, Me.ColumnHeader21, Me.ColumnHeader22})
        Me.lvwHomeBookings.FullRowSelect = True
        Me.lvwHomeBookings.Location = New System.Drawing.Point(4, 184)
        Me.lvwHomeBookings.Name = "lvwHomeBookings"
        Me.lvwHomeBookings.Size = New System.Drawing.Size(337, 40)
        Me.lvwHomeBookings.TabIndex = 262
        Me.lvwHomeBookings.UseCompatibleStateImageBehavior = False
        Me.lvwHomeBookings.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader42
        '
        Me.ColumnHeader42.Text = "SlNo"
        Me.ColumnHeader42.Width = 0
        '
        'ColumnHeader19
        '
        Me.ColumnHeader19.Text = "Time"
        Me.ColumnHeader19.Width = 40
        '
        'ColumnHeader50
        '
        Me.ColumnHeader50.Text = "Period"
        Me.ColumnHeader50.Width = 50
        '
        'ColumnHeader20
        '
        Me.ColumnHeader20.Text = "Player"
        Me.ColumnHeader20.Width = 100
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Reason"
        Me.ColumnHeader21.Width = 79
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Card"
        '
        'lklNewVisitSub
        '
        Me.lklNewVisitSub.AutoSize = True
        Me.lklNewVisitSub.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewVisitSub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewVisitSub.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewVisitSub.ForeColor = System.Drawing.Color.White
        Me.lklNewVisitSub.LinkColor = System.Drawing.Color.White
        Me.lklNewVisitSub.Location = New System.Drawing.Point(346, 229)
        Me.lklNewVisitSub.Name = "lklNewVisitSub"
        Me.lklNewVisitSub.Size = New System.Drawing.Size(37, 17)
        Me.lklNewVisitSub.TabIndex = 276
        Me.lklNewVisitSub.TabStop = True
        Me.lklNewVisitSub.Text = "NEW"
        '
        'lklEditHomeSubstitutions
        '
        Me.lklEditHomeSubstitutions.AutoSize = True
        Me.lklEditHomeSubstitutions.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditHomeSubstitutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditHomeSubstitutions.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditHomeSubstitutions.LinkColor = System.Drawing.Color.White
        Me.lklEditHomeSubstitutions.Location = New System.Drawing.Point(303, 229)
        Me.lklEditHomeSubstitutions.Name = "lklEditHomeSubstitutions"
        Me.lklEditHomeSubstitutions.Size = New System.Drawing.Size(38, 17)
        Me.lklEditHomeSubstitutions.TabIndex = 275
        Me.lklEditHomeSubstitutions.TabStop = True
        Me.lklEditHomeSubstitutions.Text = "EDIT"
        '
        'lklNewHomeSub
        '
        Me.lklNewHomeSub.AutoSize = True
        Me.lklNewHomeSub.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewHomeSub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewHomeSub.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewHomeSub.ForeColor = System.Drawing.Color.White
        Me.lklNewHomeSub.LinkColor = System.Drawing.Color.White
        Me.lklNewHomeSub.Location = New System.Drawing.Point(4, 229)
        Me.lklNewHomeSub.Name = "lklNewHomeSub"
        Me.lklNewHomeSub.Size = New System.Drawing.Size(37, 17)
        Me.lklNewHomeSub.TabIndex = 273
        Me.lklNewHomeSub.TabStop = True
        Me.lklNewHomeSub.Text = "NEW"
        '
        'lblHomeSubstitutions
        '
        Me.lblHomeSubstitutions.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblHomeSubstitutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHomeSubstitutions.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeSubstitutions.ForeColor = System.Drawing.Color.White
        Me.lblHomeSubstitutions.Location = New System.Drawing.Point(4, 229)
        Me.lblHomeSubstitutions.Name = "lblHomeSubstitutions"
        Me.lblHomeSubstitutions.Size = New System.Drawing.Size(337, 16)
        Me.lblHomeSubstitutions.TabIndex = 274
        Me.lblHomeSubstitutions.Text = "SUBSTITUTIONS"
        Me.lblHomeSubstitutions.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lvwHomeSubstitutions
        '
        Me.lvwHomeSubstitutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwHomeSubstitutions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader44, Me.ColumnHeader33, Me.ColumnHeader52, Me.ColumnHeader34, Me.ColumnHeader35, Me.ColumnHeader36})
        Me.lvwHomeSubstitutions.FullRowSelect = True
        Me.lvwHomeSubstitutions.Location = New System.Drawing.Point(4, 245)
        Me.lvwHomeSubstitutions.Name = "lvwHomeSubstitutions"
        Me.lvwHomeSubstitutions.Size = New System.Drawing.Size(337, 40)
        Me.lvwHomeSubstitutions.TabIndex = 272
        Me.lvwHomeSubstitutions.UseCompatibleStateImageBehavior = False
        Me.lvwHomeSubstitutions.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader44
        '
        Me.ColumnHeader44.Text = "SlNo"
        Me.ColumnHeader44.Width = 0
        '
        'ColumnHeader33
        '
        Me.ColumnHeader33.Text = "Time"
        Me.ColumnHeader33.Width = 40
        '
        'ColumnHeader52
        '
        Me.ColumnHeader52.Text = "Period"
        Me.ColumnHeader52.Width = 50
        '
        'ColumnHeader34
        '
        Me.ColumnHeader34.Text = "Player out"
        Me.ColumnHeader34.Width = 100
        '
        'ColumnHeader35
        '
        Me.ColumnHeader35.Text = "Player in"
        Me.ColumnHeader35.Width = 80
        '
        'ColumnHeader36
        '
        Me.ColumnHeader36.Text = "Reason"
        '
        'lklNewVisitPenalty
        '
        Me.lklNewVisitPenalty.AutoSize = True
        Me.lklNewVisitPenalty.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewVisitPenalty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewVisitPenalty.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewVisitPenalty.ForeColor = System.Drawing.Color.White
        Me.lklNewVisitPenalty.LinkColor = System.Drawing.Color.White
        Me.lklNewVisitPenalty.Location = New System.Drawing.Point(346, 290)
        Me.lklNewVisitPenalty.Name = "lklNewVisitPenalty"
        Me.lklNewVisitPenalty.Size = New System.Drawing.Size(37, 17)
        Me.lklNewVisitPenalty.TabIndex = 271
        Me.lklNewVisitPenalty.TabStop = True
        Me.lklNewVisitPenalty.Text = "NEW"
        '
        'lklEditHomePenalty
        '
        Me.lklEditHomePenalty.AutoSize = True
        Me.lklEditHomePenalty.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditHomePenalty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditHomePenalty.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditHomePenalty.LinkColor = System.Drawing.Color.White
        Me.lklEditHomePenalty.Location = New System.Drawing.Point(303, 290)
        Me.lklEditHomePenalty.Name = "lklEditHomePenalty"
        Me.lklEditHomePenalty.Size = New System.Drawing.Size(38, 17)
        Me.lklEditHomePenalty.TabIndex = 270
        Me.lklEditHomePenalty.TabStop = True
        Me.lklEditHomePenalty.Text = "EDIT"
        '
        'lklNewHomePenalty
        '
        Me.lklNewHomePenalty.AutoSize = True
        Me.lklNewHomePenalty.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewHomePenalty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewHomePenalty.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewHomePenalty.ForeColor = System.Drawing.Color.White
        Me.lklNewHomePenalty.LinkColor = System.Drawing.Color.White
        Me.lklNewHomePenalty.Location = New System.Drawing.Point(4, 290)
        Me.lklNewHomePenalty.Name = "lklNewHomePenalty"
        Me.lklNewHomePenalty.Size = New System.Drawing.Size(37, 17)
        Me.lklNewHomePenalty.TabIndex = 268
        Me.lklNewHomePenalty.TabStop = True
        Me.lklNewHomePenalty.Text = "NEW"
        '
        'lblMissedHomePenalties
        '
        Me.lblMissedHomePenalties.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblMissedHomePenalties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMissedHomePenalties.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMissedHomePenalties.ForeColor = System.Drawing.Color.White
        Me.lblMissedHomePenalties.Location = New System.Drawing.Point(4, 290)
        Me.lblMissedHomePenalties.Name = "lblMissedHomePenalties"
        Me.lblMissedHomePenalties.Size = New System.Drawing.Size(337, 16)
        Me.lblMissedHomePenalties.TabIndex = 269
        Me.lblMissedHomePenalties.Text = "MISSED PENALTIES"
        Me.lblMissedHomePenalties.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lvwHomePenalties
        '
        Me.lvwHomePenalties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwHomePenalties.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader46, Me.ColumnHeader26, Me.ColumnHeader54, Me.ColumnHeader28, Me.ColumnHeader29, Me.ColumnHeader30})
        Me.lvwHomePenalties.FullRowSelect = True
        Me.lvwHomePenalties.Location = New System.Drawing.Point(4, 306)
        Me.lvwHomePenalties.Name = "lvwHomePenalties"
        Me.lvwHomePenalties.Size = New System.Drawing.Size(337, 40)
        Me.lvwHomePenalties.TabIndex = 267
        Me.lvwHomePenalties.UseCompatibleStateImageBehavior = False
        Me.lvwHomePenalties.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader46
        '
        Me.ColumnHeader46.Text = "SlNo"
        Me.ColumnHeader46.Width = 0
        '
        'ColumnHeader26
        '
        Me.ColumnHeader26.Text = "Time"
        Me.ColumnHeader26.Width = 40
        '
        'ColumnHeader54
        '
        Me.ColumnHeader54.Text = "Period"
        Me.ColumnHeader54.Width = 50
        '
        'ColumnHeader28
        '
        Me.ColumnHeader28.Text = "Player missed"
        Me.ColumnHeader28.Width = 100
        '
        'ColumnHeader29
        '
        Me.ColumnHeader29.Text = "Saved"
        Me.ColumnHeader29.Width = 80
        '
        'ColumnHeader30
        '
        Me.ColumnHeader30.Text = "Caused by"
        '
        'lklNewVisitBooking
        '
        Me.lklNewVisitBooking.AutoSize = True
        Me.lklNewVisitBooking.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewVisitBooking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewVisitBooking.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewVisitBooking.ForeColor = System.Drawing.Color.White
        Me.lklNewVisitBooking.LinkColor = System.Drawing.Color.White
        Me.lklNewVisitBooking.Location = New System.Drawing.Point(346, 168)
        Me.lklNewVisitBooking.Name = "lklNewVisitBooking"
        Me.lklNewVisitBooking.Size = New System.Drawing.Size(37, 17)
        Me.lklNewVisitBooking.TabIndex = 266
        Me.lklNewVisitBooking.TabStop = True
        Me.lklNewVisitBooking.Text = "NEW"
        '
        'lklEditHomeBookings
        '
        Me.lklEditHomeBookings.AutoSize = True
        Me.lklEditHomeBookings.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditHomeBookings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditHomeBookings.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditHomeBookings.LinkColor = System.Drawing.Color.White
        Me.lklEditHomeBookings.Location = New System.Drawing.Point(303, 168)
        Me.lklEditHomeBookings.Name = "lklEditHomeBookings"
        Me.lklEditHomeBookings.Size = New System.Drawing.Size(38, 17)
        Me.lklEditHomeBookings.TabIndex = 265
        Me.lklEditHomeBookings.TabStop = True
        Me.lklEditHomeBookings.Text = "EDIT"
        '
        'lklNewHomeBooking
        '
        Me.lklNewHomeBooking.AutoSize = True
        Me.lklNewHomeBooking.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewHomeBooking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewHomeBooking.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewHomeBooking.ForeColor = System.Drawing.Color.White
        Me.lklNewHomeBooking.LinkColor = System.Drawing.Color.White
        Me.lklNewHomeBooking.Location = New System.Drawing.Point(4, 168)
        Me.lklNewHomeBooking.Name = "lklNewHomeBooking"
        Me.lklNewHomeBooking.Size = New System.Drawing.Size(37, 17)
        Me.lklNewHomeBooking.TabIndex = 263
        Me.lklNewHomeBooking.TabStop = True
        Me.lklNewHomeBooking.Text = "NEW"
        '
        'lblHomeBookingExpulsions
        '
        Me.lblHomeBookingExpulsions.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblHomeBookingExpulsions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHomeBookingExpulsions.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeBookingExpulsions.ForeColor = System.Drawing.Color.White
        Me.lblHomeBookingExpulsions.Location = New System.Drawing.Point(4, 168)
        Me.lblHomeBookingExpulsions.Name = "lblHomeBookingExpulsions"
        Me.lblHomeBookingExpulsions.Size = New System.Drawing.Size(337, 16)
        Me.lblHomeBookingExpulsions.TabIndex = 264
        Me.lblHomeBookingExpulsions.Text = "BOOKINGS && EXPULSIONS"
        Me.lblHomeBookingExpulsions.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lklNewVisitGoal
        '
        Me.lklNewVisitGoal.AutoSize = True
        Me.lklNewVisitGoal.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewVisitGoal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewVisitGoal.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewVisitGoal.ForeColor = System.Drawing.Color.White
        Me.lklNewVisitGoal.LinkColor = System.Drawing.Color.White
        Me.lklNewVisitGoal.Location = New System.Drawing.Point(346, 107)
        Me.lklNewVisitGoal.Name = "lklNewVisitGoal"
        Me.lklNewVisitGoal.Size = New System.Drawing.Size(37, 17)
        Me.lklNewVisitGoal.TabIndex = 261
        Me.lklNewVisitGoal.TabStop = True
        Me.lklNewVisitGoal.Text = "NEW"
        '
        'lklEditHomeGoals
        '
        Me.lklEditHomeGoals.AutoSize = True
        Me.lklEditHomeGoals.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklEditHomeGoals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklEditHomeGoals.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEditHomeGoals.LinkColor = System.Drawing.Color.White
        Me.lklEditHomeGoals.Location = New System.Drawing.Point(303, 107)
        Me.lklEditHomeGoals.Name = "lklEditHomeGoals"
        Me.lklEditHomeGoals.Size = New System.Drawing.Size(38, 17)
        Me.lklEditHomeGoals.TabIndex = 260
        Me.lklEditHomeGoals.TabStop = True
        Me.lklEditHomeGoals.Text = "EDIT"
        '
        'lklNewHomeGoal
        '
        Me.lklNewHomeGoal.AutoSize = True
        Me.lklNewHomeGoal.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lklNewHomeGoal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lklNewHomeGoal.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklNewHomeGoal.ForeColor = System.Drawing.Color.White
        Me.lklNewHomeGoal.LinkColor = System.Drawing.Color.White
        Me.lklNewHomeGoal.Location = New System.Drawing.Point(4, 107)
        Me.lklNewHomeGoal.Name = "lklNewHomeGoal"
        Me.lklNewHomeGoal.Size = New System.Drawing.Size(37, 17)
        Me.lklNewHomeGoal.TabIndex = 257
        Me.lklNewHomeGoal.TabStop = True
        Me.lklNewHomeGoal.Text = "NEW"
        '
        'lvwHomeGoals
        '
        Me.lvwHomeGoals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwHomeGoals.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader43, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader51, Me.ColumnHeader14, Me.ColumnHeader15})
        Me.lvwHomeGoals.FullRowSelect = True
        Me.lvwHomeGoals.Location = New System.Drawing.Point(4, 123)
        Me.lvwHomeGoals.Name = "lvwHomeGoals"
        Me.lvwHomeGoals.Size = New System.Drawing.Size(337, 40)
        Me.lvwHomeGoals.TabIndex = 253
        Me.lvwHomeGoals.UseCompatibleStateImageBehavior = False
        Me.lvwHomeGoals.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader43
        '
        Me.ColumnHeader43.Text = "SlNo"
        Me.ColumnHeader43.Width = 0
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Time"
        Me.ColumnHeader12.Width = 40
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Period"
        Me.ColumnHeader13.Width = 50
        '
        'ColumnHeader51
        '
        Me.ColumnHeader51.Text = "Type"
        Me.ColumnHeader51.Width = 100
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Scorer"
        Me.ColumnHeader14.Width = 80
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Score"
        '
        'lblHomeGoals
        '
        Me.lblHomeGoals.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblHomeGoals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHomeGoals.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeGoals.ForeColor = System.Drawing.Color.White
        Me.lblHomeGoals.Location = New System.Drawing.Point(4, 107)
        Me.lblHomeGoals.Name = "lblHomeGoals"
        Me.lblHomeGoals.Size = New System.Drawing.Size(337, 16)
        Me.lblHomeGoals.TabIndex = 259
        Me.lblHomeGoals.Text = "GOALS"
        Me.lblHomeGoals.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lvwVisitGoals
        '
        Me.lvwVisitGoals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwVisitGoals.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader53, Me.ColumnHeader11, Me.ColumnHeader16})
        Me.lvwVisitGoals.FullRowSelect = True
        Me.lvwVisitGoals.Location = New System.Drawing.Point(346, 123)
        Me.lvwVisitGoals.Name = "lvwVisitGoals"
        Me.lvwVisitGoals.Size = New System.Drawing.Size(337, 40)
        Me.lvwVisitGoals.TabIndex = 283
        Me.lvwVisitGoals.UseCompatibleStateImageBehavior = False
        Me.lvwVisitGoals.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "SlNo"
        Me.ColumnHeader5.Width = 0
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Time"
        Me.ColumnHeader9.Width = 40
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Period"
        Me.ColumnHeader10.Width = 50
        '
        'ColumnHeader53
        '
        Me.ColumnHeader53.Text = "Type"
        Me.ColumnHeader53.Width = 100
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Scorer"
        Me.ColumnHeader11.Width = 80
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Score"
        '
        'lblVisitGoals
        '
        Me.lblVisitGoals.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblVisitGoals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVisitGoals.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVisitGoals.ForeColor = System.Drawing.Color.White
        Me.lblVisitGoals.Location = New System.Drawing.Point(346, 107)
        Me.lblVisitGoals.Name = "lblVisitGoals"
        Me.lblVisitGoals.Size = New System.Drawing.Size(337, 16)
        Me.lblVisitGoals.TabIndex = 284
        Me.lblVisitGoals.Text = "GOALS"
        Me.lblVisitGoals.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVisitBookingExpulsions
        '
        Me.lblVisitBookingExpulsions.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblVisitBookingExpulsions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVisitBookingExpulsions.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVisitBookingExpulsions.ForeColor = System.Drawing.Color.White
        Me.lblVisitBookingExpulsions.Location = New System.Drawing.Point(346, 168)
        Me.lblVisitBookingExpulsions.Name = "lblVisitBookingExpulsions"
        Me.lblVisitBookingExpulsions.Size = New System.Drawing.Size(337, 16)
        Me.lblVisitBookingExpulsions.TabIndex = 287
        Me.lblVisitBookingExpulsions.Text = "BOOKINGS && EXPULSIONS"
        Me.lblVisitBookingExpulsions.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVisitSubstitutions
        '
        Me.lblVisitSubstitutions.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblVisitSubstitutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVisitSubstitutions.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVisitSubstitutions.ForeColor = System.Drawing.Color.White
        Me.lblVisitSubstitutions.Location = New System.Drawing.Point(346, 229)
        Me.lblVisitSubstitutions.Name = "lblVisitSubstitutions"
        Me.lblVisitSubstitutions.Size = New System.Drawing.Size(337, 16)
        Me.lblVisitSubstitutions.TabIndex = 290
        Me.lblVisitSubstitutions.Text = "SUBSTITUTIONS"
        Me.lblVisitSubstitutions.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblMissedVisitPenalties
        '
        Me.lblMissedVisitPenalties.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblMissedVisitPenalties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMissedVisitPenalties.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMissedVisitPenalties.ForeColor = System.Drawing.Color.White
        Me.lblMissedVisitPenalties.Location = New System.Drawing.Point(346, 290)
        Me.lblMissedVisitPenalties.Name = "lblMissedVisitPenalties"
        Me.lblMissedVisitPenalties.Size = New System.Drawing.Size(337, 16)
        Me.lblMissedVisitPenalties.TabIndex = 293
        Me.lblMissedVisitPenalties.Text = "MISSED PENALTIES"
        Me.lblMissedVisitPenalties.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlHome
        '
        Me.pnlHome.Controls.Add(Me.lblPosition11Home)
        Me.pnlHome.Controls.Add(Me.lblPosition10Home)
        Me.pnlHome.Controls.Add(Me.lblPosition9Home)
        Me.pnlHome.Controls.Add(Me.lblPosition8Home)
        Me.pnlHome.Controls.Add(Me.lblPosition7Home)
        Me.pnlHome.Controls.Add(Me.lblPosition6Home)
        Me.pnlHome.Controls.Add(Me.lblPosition5Home)
        Me.pnlHome.Controls.Add(Me.lblPosition4Home)
        Me.pnlHome.Controls.Add(Me.lblPosition3Home)
        Me.pnlHome.Controls.Add(Me.lblPosition2Home)
        Me.pnlHome.Controls.Add(Me.lblPosition1Home)
        Me.pnlHome.Controls.Add(Me.pnlHomeBench)
        Me.pnlHome.Controls.Add(Me.lblHomeManager)
        Me.pnlHome.Controls.Add(Me.lblManagerHome)
        Me.pnlHome.Controls.Add(Me.lblPlayer1Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer2Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer3Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer4Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer5Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer6Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer7Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer8Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer9Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer10Home)
        Me.pnlHome.Controls.Add(Me.lblPlayer11Home)
        Me.pnlHome.Controls.Add(Me.lblHomeBench)
        Me.pnlHome.Location = New System.Drawing.Point(5, 6)
        Me.pnlHome.Name = "pnlHome"
        Me.pnlHome.Size = New System.Drawing.Size(145, 499)
        Me.pnlHome.TabIndex = 261
        '
        'lblPosition11Home
        '
        Me.lblPosition11Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition11Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition11Home.Location = New System.Drawing.Point(102, 230)
        Me.lblPosition11Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition11Home.Name = "lblPosition11Home"
        Me.lblPosition11Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition11Home.TabIndex = 21
        Me.lblPosition11Home.Text = " "
        Me.lblPosition11Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition10Home
        '
        Me.lblPosition10Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition10Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition10Home.Location = New System.Drawing.Point(102, 207)
        Me.lblPosition10Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition10Home.Name = "lblPosition10Home"
        Me.lblPosition10Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition10Home.TabIndex = 20
        Me.lblPosition10Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition9Home
        '
        Me.lblPosition9Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition9Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition9Home.Location = New System.Drawing.Point(102, 184)
        Me.lblPosition9Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition9Home.Name = "lblPosition9Home"
        Me.lblPosition9Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition9Home.TabIndex = 19
        Me.lblPosition9Home.Text = " "
        Me.lblPosition9Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition8Home
        '
        Me.lblPosition8Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition8Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition8Home.Location = New System.Drawing.Point(102, 161)
        Me.lblPosition8Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition8Home.Name = "lblPosition8Home"
        Me.lblPosition8Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition8Home.TabIndex = 18
        Me.lblPosition8Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition7Home
        '
        Me.lblPosition7Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition7Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition7Home.Location = New System.Drawing.Point(102, 138)
        Me.lblPosition7Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition7Home.Name = "lblPosition7Home"
        Me.lblPosition7Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition7Home.TabIndex = 17
        Me.lblPosition7Home.Text = " "
        Me.lblPosition7Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition6Home
        '
        Me.lblPosition6Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition6Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition6Home.Location = New System.Drawing.Point(102, 115)
        Me.lblPosition6Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition6Home.Name = "lblPosition6Home"
        Me.lblPosition6Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition6Home.TabIndex = 16
        Me.lblPosition6Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition5Home
        '
        Me.lblPosition5Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition5Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition5Home.Location = New System.Drawing.Point(102, 92)
        Me.lblPosition5Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition5Home.Name = "lblPosition5Home"
        Me.lblPosition5Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition5Home.TabIndex = 15
        Me.lblPosition5Home.Text = " "
        Me.lblPosition5Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition4Home
        '
        Me.lblPosition4Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition4Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition4Home.Location = New System.Drawing.Point(102, 69)
        Me.lblPosition4Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition4Home.Name = "lblPosition4Home"
        Me.lblPosition4Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition4Home.TabIndex = 14
        Me.lblPosition4Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition3Home
        '
        Me.lblPosition3Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition3Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition3Home.Location = New System.Drawing.Point(102, 46)
        Me.lblPosition3Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition3Home.Name = "lblPosition3Home"
        Me.lblPosition3Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition3Home.TabIndex = 13
        Me.lblPosition3Home.Text = " "
        Me.lblPosition3Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition2Home
        '
        Me.lblPosition2Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition2Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition2Home.Location = New System.Drawing.Point(102, 23)
        Me.lblPosition2Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition2Home.Name = "lblPosition2Home"
        Me.lblPosition2Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition2Home.TabIndex = 12
        Me.lblPosition2Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition1Home
        '
        Me.lblPosition1Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition1Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition1Home.Location = New System.Drawing.Point(102, 0)
        Me.lblPosition1Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition1Home.Name = "lblPosition1Home"
        Me.lblPosition1Home.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition1Home.TabIndex = 11
        Me.lblPosition1Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlHomeBench
        '
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition26Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition27Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition28Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition29Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition30Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition20Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition21Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition22Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition23Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition24Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition25Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition14Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition15Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition16Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition17Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition18Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition19Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition8Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition9Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition10Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition11Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition12Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition13Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition1Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition2Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition3Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition4Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition5Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition6Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSubPosition7Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub26Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub27Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub28Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub29Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub30Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub20Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub21Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub22Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub23Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub24Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub25Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub14Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub15Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub16Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub17Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub18Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub19Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub8Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub9Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub10Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub11Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub12Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub13Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub1Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub2Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub3Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub4Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub5Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub6Home)
        Me.pnlHomeBench.Controls.Add(Me.lblSub7Home)
        Me.pnlHomeBench.Location = New System.Drawing.Point(0, 283)
        Me.pnlHomeBench.Name = "pnlHomeBench"
        Me.pnlHomeBench.Size = New System.Drawing.Size(145, 161)
        Me.pnlHomeBench.TabIndex = 285
        '
        'lblSubPosition26Home
        '
        Me.lblSubPosition26Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition26Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition26Home.Location = New System.Drawing.Point(102, 575)
        Me.lblSubPosition26Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition26Home.Name = "lblSubPosition26Home"
        Me.lblSubPosition26Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition26Home.TabIndex = 277
        Me.lblSubPosition26Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition26Home.Visible = False
        '
        'lblSubPosition27Home
        '
        Me.lblSubPosition27Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition27Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition27Home.Location = New System.Drawing.Point(102, 598)
        Me.lblSubPosition27Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition27Home.Name = "lblSubPosition27Home"
        Me.lblSubPosition27Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition27Home.TabIndex = 278
        Me.lblSubPosition27Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition27Home.Visible = False
        '
        'lblSubPosition28Home
        '
        Me.lblSubPosition28Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition28Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition28Home.Location = New System.Drawing.Point(102, 621)
        Me.lblSubPosition28Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition28Home.Name = "lblSubPosition28Home"
        Me.lblSubPosition28Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition28Home.TabIndex = 279
        Me.lblSubPosition28Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition28Home.Visible = False
        '
        'lblSubPosition29Home
        '
        Me.lblSubPosition29Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition29Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition29Home.Location = New System.Drawing.Point(102, 644)
        Me.lblSubPosition29Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition29Home.Name = "lblSubPosition29Home"
        Me.lblSubPosition29Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition29Home.TabIndex = 280
        Me.lblSubPosition29Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition29Home.Visible = False
        '
        'lblSubPosition30Home
        '
        Me.lblSubPosition30Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition30Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition30Home.Location = New System.Drawing.Point(102, 667)
        Me.lblSubPosition30Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition30Home.Name = "lblSubPosition30Home"
        Me.lblSubPosition30Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition30Home.TabIndex = 281
        Me.lblSubPosition30Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition30Home.Visible = False
        '
        'lblSubPosition20Home
        '
        Me.lblSubPosition20Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition20Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition20Home.Location = New System.Drawing.Point(102, 437)
        Me.lblSubPosition20Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition20Home.Name = "lblSubPosition20Home"
        Me.lblSubPosition20Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition20Home.TabIndex = 271
        Me.lblSubPosition20Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition20Home.Visible = False
        '
        'lblSubPosition21Home
        '
        Me.lblSubPosition21Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition21Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition21Home.Location = New System.Drawing.Point(102, 460)
        Me.lblSubPosition21Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition21Home.Name = "lblSubPosition21Home"
        Me.lblSubPosition21Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition21Home.TabIndex = 272
        Me.lblSubPosition21Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition21Home.Visible = False
        '
        'lblSubPosition22Home
        '
        Me.lblSubPosition22Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition22Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition22Home.Location = New System.Drawing.Point(102, 483)
        Me.lblSubPosition22Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition22Home.Name = "lblSubPosition22Home"
        Me.lblSubPosition22Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition22Home.TabIndex = 273
        Me.lblSubPosition22Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition22Home.Visible = False
        '
        'lblSubPosition23Home
        '
        Me.lblSubPosition23Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition23Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition23Home.Location = New System.Drawing.Point(102, 506)
        Me.lblSubPosition23Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition23Home.Name = "lblSubPosition23Home"
        Me.lblSubPosition23Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition23Home.TabIndex = 274
        Me.lblSubPosition23Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition23Home.Visible = False
        '
        'lblSubPosition24Home
        '
        Me.lblSubPosition24Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition24Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition24Home.Location = New System.Drawing.Point(102, 529)
        Me.lblSubPosition24Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition24Home.Name = "lblSubPosition24Home"
        Me.lblSubPosition24Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition24Home.TabIndex = 275
        Me.lblSubPosition24Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition24Home.Visible = False
        '
        'lblSubPosition25Home
        '
        Me.lblSubPosition25Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition25Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition25Home.Location = New System.Drawing.Point(102, 552)
        Me.lblSubPosition25Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition25Home.Name = "lblSubPosition25Home"
        Me.lblSubPosition25Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition25Home.TabIndex = 276
        Me.lblSubPosition25Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition25Home.Visible = False
        '
        'lblSubPosition14Home
        '
        Me.lblSubPosition14Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition14Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition14Home.Location = New System.Drawing.Point(102, 299)
        Me.lblSubPosition14Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition14Home.Name = "lblSubPosition14Home"
        Me.lblSubPosition14Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition14Home.TabIndex = 265
        Me.lblSubPosition14Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition14Home.Visible = False
        '
        'lblSubPosition15Home
        '
        Me.lblSubPosition15Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition15Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition15Home.Location = New System.Drawing.Point(102, 322)
        Me.lblSubPosition15Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition15Home.Name = "lblSubPosition15Home"
        Me.lblSubPosition15Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition15Home.TabIndex = 266
        Me.lblSubPosition15Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition15Home.Visible = False
        '
        'lblSubPosition16Home
        '
        Me.lblSubPosition16Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition16Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition16Home.Location = New System.Drawing.Point(102, 345)
        Me.lblSubPosition16Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition16Home.Name = "lblSubPosition16Home"
        Me.lblSubPosition16Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition16Home.TabIndex = 267
        Me.lblSubPosition16Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition16Home.Visible = False
        '
        'lblSubPosition17Home
        '
        Me.lblSubPosition17Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition17Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition17Home.Location = New System.Drawing.Point(102, 368)
        Me.lblSubPosition17Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition17Home.Name = "lblSubPosition17Home"
        Me.lblSubPosition17Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition17Home.TabIndex = 268
        Me.lblSubPosition17Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition17Home.Visible = False
        '
        'lblSubPosition18Home
        '
        Me.lblSubPosition18Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition18Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition18Home.Location = New System.Drawing.Point(102, 391)
        Me.lblSubPosition18Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition18Home.Name = "lblSubPosition18Home"
        Me.lblSubPosition18Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition18Home.TabIndex = 269
        Me.lblSubPosition18Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition18Home.Visible = False
        '
        'lblSubPosition19Home
        '
        Me.lblSubPosition19Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition19Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition19Home.Location = New System.Drawing.Point(102, 414)
        Me.lblSubPosition19Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition19Home.Name = "lblSubPosition19Home"
        Me.lblSubPosition19Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition19Home.TabIndex = 270
        Me.lblSubPosition19Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition19Home.Visible = False
        '
        'lblSubPosition8Home
        '
        Me.lblSubPosition8Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition8Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition8Home.Location = New System.Drawing.Point(102, 161)
        Me.lblSubPosition8Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition8Home.Name = "lblSubPosition8Home"
        Me.lblSubPosition8Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition8Home.TabIndex = 259
        Me.lblSubPosition8Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition8Home.Visible = False
        '
        'lblSubPosition9Home
        '
        Me.lblSubPosition9Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition9Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition9Home.Location = New System.Drawing.Point(102, 184)
        Me.lblSubPosition9Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition9Home.Name = "lblSubPosition9Home"
        Me.lblSubPosition9Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition9Home.TabIndex = 260
        Me.lblSubPosition9Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition9Home.Visible = False
        '
        'lblSubPosition10Home
        '
        Me.lblSubPosition10Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition10Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition10Home.Location = New System.Drawing.Point(102, 207)
        Me.lblSubPosition10Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition10Home.Name = "lblSubPosition10Home"
        Me.lblSubPosition10Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition10Home.TabIndex = 261
        Me.lblSubPosition10Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition10Home.Visible = False
        '
        'lblSubPosition11Home
        '
        Me.lblSubPosition11Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition11Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition11Home.Location = New System.Drawing.Point(102, 230)
        Me.lblSubPosition11Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition11Home.Name = "lblSubPosition11Home"
        Me.lblSubPosition11Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition11Home.TabIndex = 262
        Me.lblSubPosition11Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition11Home.Visible = False
        '
        'lblSubPosition12Home
        '
        Me.lblSubPosition12Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition12Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition12Home.Location = New System.Drawing.Point(102, 253)
        Me.lblSubPosition12Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition12Home.Name = "lblSubPosition12Home"
        Me.lblSubPosition12Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition12Home.TabIndex = 263
        Me.lblSubPosition12Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition12Home.Visible = False
        '
        'lblSubPosition13Home
        '
        Me.lblSubPosition13Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition13Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition13Home.Location = New System.Drawing.Point(102, 276)
        Me.lblSubPosition13Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition13Home.Name = "lblSubPosition13Home"
        Me.lblSubPosition13Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition13Home.TabIndex = 264
        Me.lblSubPosition13Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition13Home.Visible = False
        '
        'lblSubPosition1Home
        '
        Me.lblSubPosition1Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition1Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition1Home.Location = New System.Drawing.Point(102, 0)
        Me.lblSubPosition1Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition1Home.Name = "lblSubPosition1Home"
        Me.lblSubPosition1Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition1Home.TabIndex = 252
        Me.lblSubPosition1Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition2Home
        '
        Me.lblSubPosition2Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition2Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition2Home.Location = New System.Drawing.Point(102, 23)
        Me.lblSubPosition2Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition2Home.Name = "lblSubPosition2Home"
        Me.lblSubPosition2Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition2Home.TabIndex = 253
        Me.lblSubPosition2Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition3Home
        '
        Me.lblSubPosition3Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition3Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition3Home.Location = New System.Drawing.Point(102, 46)
        Me.lblSubPosition3Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition3Home.Name = "lblSubPosition3Home"
        Me.lblSubPosition3Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition3Home.TabIndex = 254
        Me.lblSubPosition3Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition4Home
        '
        Me.lblSubPosition4Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition4Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition4Home.Location = New System.Drawing.Point(102, 69)
        Me.lblSubPosition4Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition4Home.Name = "lblSubPosition4Home"
        Me.lblSubPosition4Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition4Home.TabIndex = 255
        Me.lblSubPosition4Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition5Home
        '
        Me.lblSubPosition5Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition5Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition5Home.Location = New System.Drawing.Point(102, 92)
        Me.lblSubPosition5Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition5Home.Name = "lblSubPosition5Home"
        Me.lblSubPosition5Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition5Home.TabIndex = 256
        Me.lblSubPosition5Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition6Home
        '
        Me.lblSubPosition6Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition6Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition6Home.Location = New System.Drawing.Point(102, 115)
        Me.lblSubPosition6Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition6Home.Name = "lblSubPosition6Home"
        Me.lblSubPosition6Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition6Home.TabIndex = 257
        Me.lblSubPosition6Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition7Home
        '
        Me.lblSubPosition7Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition7Home.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition7Home.Location = New System.Drawing.Point(102, 138)
        Me.lblSubPosition7Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition7Home.Name = "lblSubPosition7Home"
        Me.lblSubPosition7Home.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition7Home.TabIndex = 258
        Me.lblSubPosition7Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub26Home
        '
        Me.lblSub26Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub26Home.ForeColor = System.Drawing.Color.White
        Me.lblSub26Home.Location = New System.Drawing.Point(0, 575)
        Me.lblSub26Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub26Home.Name = "lblSub26Home"
        Me.lblSub26Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub26Home.TabIndex = 247
        Me.lblSub26Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub26Home.Visible = False
        '
        'lblSub27Home
        '
        Me.lblSub27Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub27Home.ForeColor = System.Drawing.Color.White
        Me.lblSub27Home.Location = New System.Drawing.Point(0, 598)
        Me.lblSub27Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub27Home.Name = "lblSub27Home"
        Me.lblSub27Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub27Home.TabIndex = 248
        Me.lblSub27Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub27Home.Visible = False
        '
        'lblSub28Home
        '
        Me.lblSub28Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub28Home.ForeColor = System.Drawing.Color.White
        Me.lblSub28Home.Location = New System.Drawing.Point(0, 621)
        Me.lblSub28Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub28Home.Name = "lblSub28Home"
        Me.lblSub28Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub28Home.TabIndex = 249
        Me.lblSub28Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub28Home.Visible = False
        '
        'lblSub29Home
        '
        Me.lblSub29Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub29Home.ForeColor = System.Drawing.Color.White
        Me.lblSub29Home.Location = New System.Drawing.Point(0, 644)
        Me.lblSub29Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub29Home.Name = "lblSub29Home"
        Me.lblSub29Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub29Home.TabIndex = 250
        Me.lblSub29Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub29Home.Visible = False
        '
        'lblSub30Home
        '
        Me.lblSub30Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub30Home.ForeColor = System.Drawing.Color.White
        Me.lblSub30Home.Location = New System.Drawing.Point(0, 667)
        Me.lblSub30Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub30Home.Name = "lblSub30Home"
        Me.lblSub30Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub30Home.TabIndex = 251
        Me.lblSub30Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub30Home.Visible = False
        '
        'lblSub20Home
        '
        Me.lblSub20Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub20Home.ForeColor = System.Drawing.Color.White
        Me.lblSub20Home.Location = New System.Drawing.Point(0, 437)
        Me.lblSub20Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub20Home.Name = "lblSub20Home"
        Me.lblSub20Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub20Home.TabIndex = 241
        Me.lblSub20Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub20Home.Visible = False
        '
        'lblSub21Home
        '
        Me.lblSub21Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub21Home.ForeColor = System.Drawing.Color.White
        Me.lblSub21Home.Location = New System.Drawing.Point(0, 460)
        Me.lblSub21Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub21Home.Name = "lblSub21Home"
        Me.lblSub21Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub21Home.TabIndex = 242
        Me.lblSub21Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub21Home.Visible = False
        '
        'lblSub22Home
        '
        Me.lblSub22Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub22Home.ForeColor = System.Drawing.Color.White
        Me.lblSub22Home.Location = New System.Drawing.Point(0, 483)
        Me.lblSub22Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub22Home.Name = "lblSub22Home"
        Me.lblSub22Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub22Home.TabIndex = 243
        Me.lblSub22Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub22Home.Visible = False
        '
        'lblSub23Home
        '
        Me.lblSub23Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub23Home.ForeColor = System.Drawing.Color.White
        Me.lblSub23Home.Location = New System.Drawing.Point(0, 506)
        Me.lblSub23Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub23Home.Name = "lblSub23Home"
        Me.lblSub23Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub23Home.TabIndex = 244
        Me.lblSub23Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub23Home.Visible = False
        '
        'lblSub24Home
        '
        Me.lblSub24Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub24Home.ForeColor = System.Drawing.Color.White
        Me.lblSub24Home.Location = New System.Drawing.Point(0, 529)
        Me.lblSub24Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub24Home.Name = "lblSub24Home"
        Me.lblSub24Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub24Home.TabIndex = 245
        Me.lblSub24Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub24Home.Visible = False
        '
        'lblSub25Home
        '
        Me.lblSub25Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub25Home.ForeColor = System.Drawing.Color.White
        Me.lblSub25Home.Location = New System.Drawing.Point(0, 552)
        Me.lblSub25Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub25Home.Name = "lblSub25Home"
        Me.lblSub25Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub25Home.TabIndex = 246
        Me.lblSub25Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub25Home.Visible = False
        '
        'lblSub14Home
        '
        Me.lblSub14Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub14Home.ForeColor = System.Drawing.Color.White
        Me.lblSub14Home.Location = New System.Drawing.Point(0, 299)
        Me.lblSub14Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub14Home.Name = "lblSub14Home"
        Me.lblSub14Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub14Home.TabIndex = 235
        Me.lblSub14Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub14Home.Visible = False
        '
        'lblSub15Home
        '
        Me.lblSub15Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub15Home.ForeColor = System.Drawing.Color.White
        Me.lblSub15Home.Location = New System.Drawing.Point(0, 322)
        Me.lblSub15Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub15Home.Name = "lblSub15Home"
        Me.lblSub15Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub15Home.TabIndex = 236
        Me.lblSub15Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub15Home.Visible = False
        '
        'lblSub16Home
        '
        Me.lblSub16Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub16Home.ForeColor = System.Drawing.Color.White
        Me.lblSub16Home.Location = New System.Drawing.Point(0, 345)
        Me.lblSub16Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub16Home.Name = "lblSub16Home"
        Me.lblSub16Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub16Home.TabIndex = 237
        Me.lblSub16Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub16Home.Visible = False
        '
        'lblSub17Home
        '
        Me.lblSub17Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub17Home.ForeColor = System.Drawing.Color.White
        Me.lblSub17Home.Location = New System.Drawing.Point(0, 368)
        Me.lblSub17Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub17Home.Name = "lblSub17Home"
        Me.lblSub17Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub17Home.TabIndex = 238
        Me.lblSub17Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub17Home.Visible = False
        '
        'lblSub18Home
        '
        Me.lblSub18Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub18Home.ForeColor = System.Drawing.Color.White
        Me.lblSub18Home.Location = New System.Drawing.Point(0, 391)
        Me.lblSub18Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub18Home.Name = "lblSub18Home"
        Me.lblSub18Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub18Home.TabIndex = 239
        Me.lblSub18Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub18Home.Visible = False
        '
        'lblSub19Home
        '
        Me.lblSub19Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub19Home.ForeColor = System.Drawing.Color.White
        Me.lblSub19Home.Location = New System.Drawing.Point(0, 414)
        Me.lblSub19Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub19Home.Name = "lblSub19Home"
        Me.lblSub19Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub19Home.TabIndex = 240
        Me.lblSub19Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub19Home.Visible = False
        '
        'lblSub8Home
        '
        Me.lblSub8Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub8Home.ForeColor = System.Drawing.Color.White
        Me.lblSub8Home.Location = New System.Drawing.Point(0, 161)
        Me.lblSub8Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub8Home.Name = "lblSub8Home"
        Me.lblSub8Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub8Home.TabIndex = 229
        Me.lblSub8Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub8Home.Visible = False
        '
        'lblSub9Home
        '
        Me.lblSub9Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub9Home.ForeColor = System.Drawing.Color.White
        Me.lblSub9Home.Location = New System.Drawing.Point(0, 184)
        Me.lblSub9Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub9Home.Name = "lblSub9Home"
        Me.lblSub9Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub9Home.TabIndex = 230
        Me.lblSub9Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub9Home.Visible = False
        '
        'lblSub10Home
        '
        Me.lblSub10Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub10Home.ForeColor = System.Drawing.Color.White
        Me.lblSub10Home.Location = New System.Drawing.Point(0, 207)
        Me.lblSub10Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub10Home.Name = "lblSub10Home"
        Me.lblSub10Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub10Home.TabIndex = 231
        Me.lblSub10Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub10Home.Visible = False
        '
        'lblSub11Home
        '
        Me.lblSub11Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub11Home.ForeColor = System.Drawing.Color.White
        Me.lblSub11Home.Location = New System.Drawing.Point(0, 230)
        Me.lblSub11Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub11Home.Name = "lblSub11Home"
        Me.lblSub11Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub11Home.TabIndex = 232
        Me.lblSub11Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub11Home.Visible = False
        '
        'lblSub12Home
        '
        Me.lblSub12Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub12Home.ForeColor = System.Drawing.Color.White
        Me.lblSub12Home.Location = New System.Drawing.Point(0, 253)
        Me.lblSub12Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub12Home.Name = "lblSub12Home"
        Me.lblSub12Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub12Home.TabIndex = 233
        Me.lblSub12Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub12Home.Visible = False
        '
        'lblSub13Home
        '
        Me.lblSub13Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub13Home.ForeColor = System.Drawing.Color.White
        Me.lblSub13Home.Location = New System.Drawing.Point(0, 276)
        Me.lblSub13Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub13Home.Name = "lblSub13Home"
        Me.lblSub13Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub13Home.TabIndex = 234
        Me.lblSub13Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub13Home.Visible = False
        '
        'lblSub1Home
        '
        Me.lblSub1Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub1Home.ForeColor = System.Drawing.Color.White
        Me.lblSub1Home.Location = New System.Drawing.Point(0, 0)
        Me.lblSub1Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub1Home.Name = "lblSub1Home"
        Me.lblSub1Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub1Home.TabIndex = 222
        Me.lblSub1Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub2Home
        '
        Me.lblSub2Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub2Home.ForeColor = System.Drawing.Color.White
        Me.lblSub2Home.Location = New System.Drawing.Point(0, 23)
        Me.lblSub2Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub2Home.Name = "lblSub2Home"
        Me.lblSub2Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub2Home.TabIndex = 223
        Me.lblSub2Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub3Home
        '
        Me.lblSub3Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub3Home.ForeColor = System.Drawing.Color.White
        Me.lblSub3Home.Location = New System.Drawing.Point(0, 46)
        Me.lblSub3Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub3Home.Name = "lblSub3Home"
        Me.lblSub3Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub3Home.TabIndex = 224
        Me.lblSub3Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub4Home
        '
        Me.lblSub4Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub4Home.ForeColor = System.Drawing.Color.White
        Me.lblSub4Home.Location = New System.Drawing.Point(0, 69)
        Me.lblSub4Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub4Home.Name = "lblSub4Home"
        Me.lblSub4Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub4Home.TabIndex = 225
        Me.lblSub4Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub5Home
        '
        Me.lblSub5Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub5Home.ForeColor = System.Drawing.Color.White
        Me.lblSub5Home.Location = New System.Drawing.Point(0, 92)
        Me.lblSub5Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub5Home.Name = "lblSub5Home"
        Me.lblSub5Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub5Home.TabIndex = 226
        Me.lblSub5Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub6Home
        '
        Me.lblSub6Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub6Home.ForeColor = System.Drawing.Color.White
        Me.lblSub6Home.Location = New System.Drawing.Point(0, 115)
        Me.lblSub6Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub6Home.Name = "lblSub6Home"
        Me.lblSub6Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub6Home.TabIndex = 227
        Me.lblSub6Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub7Home
        '
        Me.lblSub7Home.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub7Home.ForeColor = System.Drawing.Color.White
        Me.lblSub7Home.Location = New System.Drawing.Point(0, 138)
        Me.lblSub7Home.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub7Home.Name = "lblSub7Home"
        Me.lblSub7Home.Size = New System.Drawing.Size(104, 23)
        Me.lblSub7Home.TabIndex = 228
        Me.lblSub7Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHomeManager
        '
        Me.lblHomeManager.AutoSize = True
        Me.lblHomeManager.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeManager.Location = New System.Drawing.Point(0, 457)
        Me.lblHomeManager.Name = "lblHomeManager"
        Me.lblHomeManager.Size = New System.Drawing.Size(61, 15)
        Me.lblHomeManager.TabIndex = 281
        Me.lblHomeManager.Text = "Manager:"
        Me.lblHomeManager.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblManagerHome
        '
        Me.lblManagerHome.BackColor = System.Drawing.Color.LightSteelBlue
        Me.lblManagerHome.ForeColor = System.Drawing.Color.Black
        Me.lblManagerHome.Location = New System.Drawing.Point(0, 476)
        Me.lblManagerHome.Name = "lblManagerHome"
        Me.lblManagerHome.Size = New System.Drawing.Size(125, 23)
        Me.lblManagerHome.TabIndex = 282
        Me.lblManagerHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlVisit
        '
        Me.pnlVisit.Controls.Add(Me.lblPosition11Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition10Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition9Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition8Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition7Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition6Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition5Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition4Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition3Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition2Visit)
        Me.pnlVisit.Controls.Add(Me.lblPosition1Visit)
        Me.pnlVisit.Controls.Add(Me.pnlVisitBench)
        Me.pnlVisit.Controls.Add(Me.lblVisitManager)
        Me.pnlVisit.Controls.Add(Me.lblManagerAway)
        Me.pnlVisit.Controls.Add(Me.lblPlayer1Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer2Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer3Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer4Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer5Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer6Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer7Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer8Visit)
        Me.pnlVisit.Controls.Add(Me.lblVisitBench)
        Me.pnlVisit.Controls.Add(Me.lblPlayer9Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer10Visit)
        Me.pnlVisit.Controls.Add(Me.lblPlayer11Visit)
        Me.pnlVisit.Location = New System.Drawing.Point(865, 5)
        Me.pnlVisit.Name = "pnlVisit"
        Me.pnlVisit.Size = New System.Drawing.Size(145, 500)
        Me.pnlVisit.TabIndex = 262
        '
        'lblPosition11Visit
        '
        Me.lblPosition11Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition11Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition11Visit.Location = New System.Drawing.Point(102, 230)
        Me.lblPosition11Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition11Visit.Name = "lblPosition11Visit"
        Me.lblPosition11Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition11Visit.TabIndex = 298
        Me.lblPosition11Visit.Text = " "
        Me.lblPosition11Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition10Visit
        '
        Me.lblPosition10Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition10Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition10Visit.Location = New System.Drawing.Point(102, 207)
        Me.lblPosition10Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition10Visit.Name = "lblPosition10Visit"
        Me.lblPosition10Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition10Visit.TabIndex = 297
        Me.lblPosition10Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition9Visit
        '
        Me.lblPosition9Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition9Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition9Visit.Location = New System.Drawing.Point(102, 184)
        Me.lblPosition9Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition9Visit.Name = "lblPosition9Visit"
        Me.lblPosition9Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition9Visit.TabIndex = 296
        Me.lblPosition9Visit.Text = " "
        Me.lblPosition9Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition8Visit
        '
        Me.lblPosition8Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition8Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition8Visit.Location = New System.Drawing.Point(102, 161)
        Me.lblPosition8Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition8Visit.Name = "lblPosition8Visit"
        Me.lblPosition8Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition8Visit.TabIndex = 295
        Me.lblPosition8Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition7Visit
        '
        Me.lblPosition7Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition7Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition7Visit.Location = New System.Drawing.Point(102, 138)
        Me.lblPosition7Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition7Visit.Name = "lblPosition7Visit"
        Me.lblPosition7Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition7Visit.TabIndex = 294
        Me.lblPosition7Visit.Text = " "
        Me.lblPosition7Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition6Visit
        '
        Me.lblPosition6Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition6Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition6Visit.Location = New System.Drawing.Point(102, 115)
        Me.lblPosition6Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition6Visit.Name = "lblPosition6Visit"
        Me.lblPosition6Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition6Visit.TabIndex = 293
        Me.lblPosition6Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition5Visit
        '
        Me.lblPosition5Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition5Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition5Visit.Location = New System.Drawing.Point(102, 92)
        Me.lblPosition5Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition5Visit.Name = "lblPosition5Visit"
        Me.lblPosition5Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition5Visit.TabIndex = 292
        Me.lblPosition5Visit.Text = " "
        Me.lblPosition5Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition4Visit
        '
        Me.lblPosition4Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition4Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition4Visit.Location = New System.Drawing.Point(102, 69)
        Me.lblPosition4Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition4Visit.Name = "lblPosition4Visit"
        Me.lblPosition4Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition4Visit.TabIndex = 291
        Me.lblPosition4Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition3Visit
        '
        Me.lblPosition3Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition3Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition3Visit.Location = New System.Drawing.Point(102, 46)
        Me.lblPosition3Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition3Visit.Name = "lblPosition3Visit"
        Me.lblPosition3Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition3Visit.TabIndex = 290
        Me.lblPosition3Visit.Text = " "
        Me.lblPosition3Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition2Visit
        '
        Me.lblPosition2Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.lblPosition2Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition2Visit.Location = New System.Drawing.Point(102, 23)
        Me.lblPosition2Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition2Visit.Name = "lblPosition2Visit"
        Me.lblPosition2Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition2Visit.TabIndex = 289
        Me.lblPosition2Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition1Visit
        '
        Me.lblPosition1Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(82, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.lblPosition1Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblPosition1Visit.Location = New System.Drawing.Point(102, 0)
        Me.lblPosition1Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPosition1Visit.Name = "lblPosition1Visit"
        Me.lblPosition1Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblPosition1Visit.TabIndex = 288
        Me.lblPosition1Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlVisitBench
        '
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition26Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition27Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition28Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition29Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition30Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition20Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition21Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition22Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition23Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition24Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition25Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition14Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition15Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition16Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition17Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition18Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition19Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition8Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition9Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition10Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition11Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition12Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition13Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition1Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition2Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition3Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition4Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition5Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition6Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSubPosition7Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub26Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub27Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub28Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub29Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub30Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub20Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub21Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub22Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub23Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub24Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub25Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub14Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub15Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub16Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub17Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub18Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub19Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub8Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub9Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub10Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub11Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub12Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub13Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub1Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub2Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub3Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub4Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub5Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub6Visit)
        Me.pnlVisitBench.Controls.Add(Me.lblSub7Visit)
        Me.pnlVisitBench.Location = New System.Drawing.Point(0, 283)
        Me.pnlVisitBench.Name = "pnlVisitBench"
        Me.pnlVisitBench.Size = New System.Drawing.Size(145, 161)
        Me.pnlVisitBench.TabIndex = 287
        '
        'lblSubPosition26Visit
        '
        Me.lblSubPosition26Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition26Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition26Visit.Location = New System.Drawing.Point(102, 575)
        Me.lblSubPosition26Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition26Visit.Name = "lblSubPosition26Visit"
        Me.lblSubPosition26Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition26Visit.TabIndex = 307
        Me.lblSubPosition26Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition26Visit.Visible = False
        '
        'lblSubPosition27Visit
        '
        Me.lblSubPosition27Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition27Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition27Visit.Location = New System.Drawing.Point(102, 598)
        Me.lblSubPosition27Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition27Visit.Name = "lblSubPosition27Visit"
        Me.lblSubPosition27Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition27Visit.TabIndex = 308
        Me.lblSubPosition27Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition27Visit.Visible = False
        '
        'lblSubPosition28Visit
        '
        Me.lblSubPosition28Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition28Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition28Visit.Location = New System.Drawing.Point(102, 621)
        Me.lblSubPosition28Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition28Visit.Name = "lblSubPosition28Visit"
        Me.lblSubPosition28Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition28Visit.TabIndex = 309
        Me.lblSubPosition28Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition28Visit.Visible = False
        '
        'lblSubPosition29Visit
        '
        Me.lblSubPosition29Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition29Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition29Visit.Location = New System.Drawing.Point(102, 644)
        Me.lblSubPosition29Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition29Visit.Name = "lblSubPosition29Visit"
        Me.lblSubPosition29Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition29Visit.TabIndex = 310
        Me.lblSubPosition29Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition29Visit.Visible = False
        '
        'lblSubPosition30Visit
        '
        Me.lblSubPosition30Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition30Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition30Visit.Location = New System.Drawing.Point(102, 667)
        Me.lblSubPosition30Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition30Visit.Name = "lblSubPosition30Visit"
        Me.lblSubPosition30Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition30Visit.TabIndex = 311
        Me.lblSubPosition30Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition30Visit.Visible = False
        '
        'lblSubPosition20Visit
        '
        Me.lblSubPosition20Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition20Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition20Visit.Location = New System.Drawing.Point(102, 437)
        Me.lblSubPosition20Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition20Visit.Name = "lblSubPosition20Visit"
        Me.lblSubPosition20Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition20Visit.TabIndex = 301
        Me.lblSubPosition20Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition20Visit.Visible = False
        '
        'lblSubPosition21Visit
        '
        Me.lblSubPosition21Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition21Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition21Visit.Location = New System.Drawing.Point(102, 460)
        Me.lblSubPosition21Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition21Visit.Name = "lblSubPosition21Visit"
        Me.lblSubPosition21Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition21Visit.TabIndex = 302
        Me.lblSubPosition21Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition21Visit.Visible = False
        '
        'lblSubPosition22Visit
        '
        Me.lblSubPosition22Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition22Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition22Visit.Location = New System.Drawing.Point(102, 483)
        Me.lblSubPosition22Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition22Visit.Name = "lblSubPosition22Visit"
        Me.lblSubPosition22Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition22Visit.TabIndex = 303
        Me.lblSubPosition22Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition22Visit.Visible = False
        '
        'lblSubPosition23Visit
        '
        Me.lblSubPosition23Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition23Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition23Visit.Location = New System.Drawing.Point(102, 506)
        Me.lblSubPosition23Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition23Visit.Name = "lblSubPosition23Visit"
        Me.lblSubPosition23Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition23Visit.TabIndex = 304
        Me.lblSubPosition23Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition23Visit.Visible = False
        '
        'lblSubPosition24Visit
        '
        Me.lblSubPosition24Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition24Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition24Visit.Location = New System.Drawing.Point(102, 529)
        Me.lblSubPosition24Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition24Visit.Name = "lblSubPosition24Visit"
        Me.lblSubPosition24Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition24Visit.TabIndex = 305
        Me.lblSubPosition24Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition24Visit.Visible = False
        '
        'lblSubPosition25Visit
        '
        Me.lblSubPosition25Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition25Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition25Visit.Location = New System.Drawing.Point(102, 552)
        Me.lblSubPosition25Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition25Visit.Name = "lblSubPosition25Visit"
        Me.lblSubPosition25Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition25Visit.TabIndex = 306
        Me.lblSubPosition25Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition25Visit.Visible = False
        '
        'lblSubPosition14Visit
        '
        Me.lblSubPosition14Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition14Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition14Visit.Location = New System.Drawing.Point(102, 299)
        Me.lblSubPosition14Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition14Visit.Name = "lblSubPosition14Visit"
        Me.lblSubPosition14Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition14Visit.TabIndex = 295
        Me.lblSubPosition14Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition14Visit.Visible = False
        '
        'lblSubPosition15Visit
        '
        Me.lblSubPosition15Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition15Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition15Visit.Location = New System.Drawing.Point(102, 322)
        Me.lblSubPosition15Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition15Visit.Name = "lblSubPosition15Visit"
        Me.lblSubPosition15Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition15Visit.TabIndex = 296
        Me.lblSubPosition15Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition15Visit.Visible = False
        '
        'lblSubPosition16Visit
        '
        Me.lblSubPosition16Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition16Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition16Visit.Location = New System.Drawing.Point(102, 345)
        Me.lblSubPosition16Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition16Visit.Name = "lblSubPosition16Visit"
        Me.lblSubPosition16Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition16Visit.TabIndex = 297
        Me.lblSubPosition16Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition16Visit.Visible = False
        '
        'lblSubPosition17Visit
        '
        Me.lblSubPosition17Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition17Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition17Visit.Location = New System.Drawing.Point(102, 368)
        Me.lblSubPosition17Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition17Visit.Name = "lblSubPosition17Visit"
        Me.lblSubPosition17Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition17Visit.TabIndex = 298
        Me.lblSubPosition17Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition17Visit.Visible = False
        '
        'lblSubPosition18Visit
        '
        Me.lblSubPosition18Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition18Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition18Visit.Location = New System.Drawing.Point(102, 391)
        Me.lblSubPosition18Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition18Visit.Name = "lblSubPosition18Visit"
        Me.lblSubPosition18Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition18Visit.TabIndex = 299
        Me.lblSubPosition18Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition18Visit.Visible = False
        '
        'lblSubPosition19Visit
        '
        Me.lblSubPosition19Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition19Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition19Visit.Location = New System.Drawing.Point(102, 414)
        Me.lblSubPosition19Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition19Visit.Name = "lblSubPosition19Visit"
        Me.lblSubPosition19Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition19Visit.TabIndex = 300
        Me.lblSubPosition19Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition19Visit.Visible = False
        '
        'lblSubPosition8Visit
        '
        Me.lblSubPosition8Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition8Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition8Visit.Location = New System.Drawing.Point(102, 161)
        Me.lblSubPosition8Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition8Visit.Name = "lblSubPosition8Visit"
        Me.lblSubPosition8Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition8Visit.TabIndex = 289
        Me.lblSubPosition8Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition8Visit.Visible = False
        '
        'lblSubPosition9Visit
        '
        Me.lblSubPosition9Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition9Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition9Visit.Location = New System.Drawing.Point(102, 184)
        Me.lblSubPosition9Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition9Visit.Name = "lblSubPosition9Visit"
        Me.lblSubPosition9Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition9Visit.TabIndex = 290
        Me.lblSubPosition9Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition9Visit.Visible = False
        '
        'lblSubPosition10Visit
        '
        Me.lblSubPosition10Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition10Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition10Visit.Location = New System.Drawing.Point(102, 207)
        Me.lblSubPosition10Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition10Visit.Name = "lblSubPosition10Visit"
        Me.lblSubPosition10Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition10Visit.TabIndex = 291
        Me.lblSubPosition10Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition10Visit.Visible = False
        '
        'lblSubPosition11Visit
        '
        Me.lblSubPosition11Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition11Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition11Visit.Location = New System.Drawing.Point(102, 230)
        Me.lblSubPosition11Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition11Visit.Name = "lblSubPosition11Visit"
        Me.lblSubPosition11Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition11Visit.TabIndex = 292
        Me.lblSubPosition11Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition11Visit.Visible = False
        '
        'lblSubPosition12Visit
        '
        Me.lblSubPosition12Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition12Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition12Visit.Location = New System.Drawing.Point(102, 253)
        Me.lblSubPosition12Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition12Visit.Name = "lblSubPosition12Visit"
        Me.lblSubPosition12Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition12Visit.TabIndex = 293
        Me.lblSubPosition12Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition12Visit.Visible = False
        '
        'lblSubPosition13Visit
        '
        Me.lblSubPosition13Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition13Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition13Visit.Location = New System.Drawing.Point(102, 276)
        Me.lblSubPosition13Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition13Visit.Name = "lblSubPosition13Visit"
        Me.lblSubPosition13Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition13Visit.TabIndex = 294
        Me.lblSubPosition13Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSubPosition13Visit.Visible = False
        '
        'lblSubPosition1Visit
        '
        Me.lblSubPosition1Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition1Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition1Visit.Location = New System.Drawing.Point(102, 0)
        Me.lblSubPosition1Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition1Visit.Name = "lblSubPosition1Visit"
        Me.lblSubPosition1Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition1Visit.TabIndex = 282
        Me.lblSubPosition1Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition2Visit
        '
        Me.lblSubPosition2Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition2Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition2Visit.Location = New System.Drawing.Point(102, 23)
        Me.lblSubPosition2Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition2Visit.Name = "lblSubPosition2Visit"
        Me.lblSubPosition2Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition2Visit.TabIndex = 283
        Me.lblSubPosition2Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition3Visit
        '
        Me.lblSubPosition3Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition3Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition3Visit.Location = New System.Drawing.Point(102, 46)
        Me.lblSubPosition3Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition3Visit.Name = "lblSubPosition3Visit"
        Me.lblSubPosition3Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition3Visit.TabIndex = 284
        Me.lblSubPosition3Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition4Visit
        '
        Me.lblSubPosition4Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition4Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition4Visit.Location = New System.Drawing.Point(102, 69)
        Me.lblSubPosition4Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition4Visit.Name = "lblSubPosition4Visit"
        Me.lblSubPosition4Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition4Visit.TabIndex = 285
        Me.lblSubPosition4Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition5Visit
        '
        Me.lblSubPosition5Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition5Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition5Visit.Location = New System.Drawing.Point(102, 92)
        Me.lblSubPosition5Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition5Visit.Name = "lblSubPosition5Visit"
        Me.lblSubPosition5Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition5Visit.TabIndex = 286
        Me.lblSubPosition5Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition6Visit
        '
        Me.lblSubPosition6Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSubPosition6Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition6Visit.Location = New System.Drawing.Point(102, 115)
        Me.lblSubPosition6Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition6Visit.Name = "lblSubPosition6Visit"
        Me.lblSubPosition6Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition6Visit.TabIndex = 287
        Me.lblSubPosition6Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubPosition7Visit
        '
        Me.lblSubPosition7Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSubPosition7Visit.ForeColor = System.Drawing.Color.DarkGray
        Me.lblSubPosition7Visit.Location = New System.Drawing.Point(102, 138)
        Me.lblSubPosition7Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSubPosition7Visit.Name = "lblSubPosition7Visit"
        Me.lblSubPosition7Visit.Size = New System.Drawing.Size(23, 23)
        Me.lblSubPosition7Visit.TabIndex = 288
        Me.lblSubPosition7Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub26Visit
        '
        Me.lblSub26Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub26Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub26Visit.Location = New System.Drawing.Point(0, 575)
        Me.lblSub26Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub26Visit.Name = "lblSub26Visit"
        Me.lblSub26Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub26Visit.TabIndex = 247
        Me.lblSub26Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub26Visit.UseMnemonic = False
        Me.lblSub26Visit.Visible = False
        '
        'lblSub27Visit
        '
        Me.lblSub27Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub27Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub27Visit.Location = New System.Drawing.Point(0, 598)
        Me.lblSub27Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub27Visit.Name = "lblSub27Visit"
        Me.lblSub27Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub27Visit.TabIndex = 248
        Me.lblSub27Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub27Visit.Visible = False
        '
        'lblSub28Visit
        '
        Me.lblSub28Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub28Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub28Visit.Location = New System.Drawing.Point(0, 621)
        Me.lblSub28Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub28Visit.Name = "lblSub28Visit"
        Me.lblSub28Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub28Visit.TabIndex = 249
        Me.lblSub28Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub28Visit.Visible = False
        '
        'lblSub29Visit
        '
        Me.lblSub29Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub29Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub29Visit.Location = New System.Drawing.Point(0, 644)
        Me.lblSub29Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub29Visit.Name = "lblSub29Visit"
        Me.lblSub29Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub29Visit.TabIndex = 250
        Me.lblSub29Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub29Visit.Visible = False
        '
        'lblSub30Visit
        '
        Me.lblSub30Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub30Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub30Visit.Location = New System.Drawing.Point(0, 667)
        Me.lblSub30Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub30Visit.Name = "lblSub30Visit"
        Me.lblSub30Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub30Visit.TabIndex = 251
        Me.lblSub30Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub30Visit.Visible = False
        '
        'lblSub20Visit
        '
        Me.lblSub20Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub20Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub20Visit.Location = New System.Drawing.Point(0, 437)
        Me.lblSub20Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub20Visit.Name = "lblSub20Visit"
        Me.lblSub20Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub20Visit.TabIndex = 241
        Me.lblSub20Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub20Visit.Visible = False
        '
        'lblSub21Visit
        '
        Me.lblSub21Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub21Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub21Visit.Location = New System.Drawing.Point(0, 460)
        Me.lblSub21Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub21Visit.Name = "lblSub21Visit"
        Me.lblSub21Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub21Visit.TabIndex = 242
        Me.lblSub21Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub21Visit.Visible = False
        '
        'lblSub22Visit
        '
        Me.lblSub22Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub22Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub22Visit.Location = New System.Drawing.Point(0, 483)
        Me.lblSub22Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub22Visit.Name = "lblSub22Visit"
        Me.lblSub22Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub22Visit.TabIndex = 243
        Me.lblSub22Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub22Visit.Visible = False
        '
        'lblSub23Visit
        '
        Me.lblSub23Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub23Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub23Visit.Location = New System.Drawing.Point(0, 506)
        Me.lblSub23Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub23Visit.Name = "lblSub23Visit"
        Me.lblSub23Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub23Visit.TabIndex = 244
        Me.lblSub23Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub23Visit.Visible = False
        '
        'lblSub24Visit
        '
        Me.lblSub24Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub24Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub24Visit.Location = New System.Drawing.Point(0, 529)
        Me.lblSub24Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub24Visit.Name = "lblSub24Visit"
        Me.lblSub24Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub24Visit.TabIndex = 245
        Me.lblSub24Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub24Visit.Visible = False
        '
        'lblSub25Visit
        '
        Me.lblSub25Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub25Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub25Visit.Location = New System.Drawing.Point(0, 552)
        Me.lblSub25Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub25Visit.Name = "lblSub25Visit"
        Me.lblSub25Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub25Visit.TabIndex = 246
        Me.lblSub25Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub25Visit.Visible = False
        '
        'lblSub14Visit
        '
        Me.lblSub14Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub14Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub14Visit.Location = New System.Drawing.Point(0, 299)
        Me.lblSub14Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub14Visit.Name = "lblSub14Visit"
        Me.lblSub14Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub14Visit.TabIndex = 235
        Me.lblSub14Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub14Visit.Visible = False
        '
        'lblSub15Visit
        '
        Me.lblSub15Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub15Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub15Visit.Location = New System.Drawing.Point(0, 322)
        Me.lblSub15Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub15Visit.Name = "lblSub15Visit"
        Me.lblSub15Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub15Visit.TabIndex = 236
        Me.lblSub15Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub15Visit.Visible = False
        '
        'lblSub16Visit
        '
        Me.lblSub16Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub16Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub16Visit.Location = New System.Drawing.Point(0, 345)
        Me.lblSub16Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub16Visit.Name = "lblSub16Visit"
        Me.lblSub16Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub16Visit.TabIndex = 237
        Me.lblSub16Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub16Visit.Visible = False
        '
        'lblSub17Visit
        '
        Me.lblSub17Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub17Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub17Visit.Location = New System.Drawing.Point(0, 368)
        Me.lblSub17Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub17Visit.Name = "lblSub17Visit"
        Me.lblSub17Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub17Visit.TabIndex = 238
        Me.lblSub17Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub17Visit.Visible = False
        '
        'lblSub18Visit
        '
        Me.lblSub18Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub18Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub18Visit.Location = New System.Drawing.Point(0, 391)
        Me.lblSub18Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub18Visit.Name = "lblSub18Visit"
        Me.lblSub18Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub18Visit.TabIndex = 239
        Me.lblSub18Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub18Visit.Visible = False
        '
        'lblSub19Visit
        '
        Me.lblSub19Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub19Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub19Visit.Location = New System.Drawing.Point(0, 414)
        Me.lblSub19Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub19Visit.Name = "lblSub19Visit"
        Me.lblSub19Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub19Visit.TabIndex = 240
        Me.lblSub19Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub19Visit.Visible = False
        '
        'lblSub8Visit
        '
        Me.lblSub8Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub8Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub8Visit.Location = New System.Drawing.Point(0, 161)
        Me.lblSub8Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub8Visit.Name = "lblSub8Visit"
        Me.lblSub8Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub8Visit.TabIndex = 229
        Me.lblSub8Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub8Visit.Visible = False
        '
        'lblSub9Visit
        '
        Me.lblSub9Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub9Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub9Visit.Location = New System.Drawing.Point(0, 184)
        Me.lblSub9Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub9Visit.Name = "lblSub9Visit"
        Me.lblSub9Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub9Visit.TabIndex = 230
        Me.lblSub9Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub9Visit.Visible = False
        '
        'lblSub10Visit
        '
        Me.lblSub10Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub10Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub10Visit.Location = New System.Drawing.Point(0, 207)
        Me.lblSub10Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub10Visit.Name = "lblSub10Visit"
        Me.lblSub10Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub10Visit.TabIndex = 231
        Me.lblSub10Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub10Visit.Visible = False
        '
        'lblSub11Visit
        '
        Me.lblSub11Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub11Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub11Visit.Location = New System.Drawing.Point(0, 230)
        Me.lblSub11Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub11Visit.Name = "lblSub11Visit"
        Me.lblSub11Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub11Visit.TabIndex = 232
        Me.lblSub11Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub11Visit.Visible = False
        '
        'lblSub12Visit
        '
        Me.lblSub12Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub12Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub12Visit.Location = New System.Drawing.Point(0, 253)
        Me.lblSub12Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub12Visit.Name = "lblSub12Visit"
        Me.lblSub12Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub12Visit.TabIndex = 233
        Me.lblSub12Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub12Visit.Visible = False
        '
        'lblSub13Visit
        '
        Me.lblSub13Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub13Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub13Visit.Location = New System.Drawing.Point(0, 276)
        Me.lblSub13Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub13Visit.Name = "lblSub13Visit"
        Me.lblSub13Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub13Visit.TabIndex = 234
        Me.lblSub13Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSub13Visit.Visible = False
        '
        'lblSub1Visit
        '
        Me.lblSub1Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub1Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub1Visit.Location = New System.Drawing.Point(0, 0)
        Me.lblSub1Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub1Visit.Name = "lblSub1Visit"
        Me.lblSub1Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub1Visit.TabIndex = 222
        Me.lblSub1Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub2Visit
        '
        Me.lblSub2Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub2Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub2Visit.Location = New System.Drawing.Point(0, 23)
        Me.lblSub2Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub2Visit.Name = "lblSub2Visit"
        Me.lblSub2Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub2Visit.TabIndex = 223
        Me.lblSub2Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub3Visit
        '
        Me.lblSub3Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub3Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub3Visit.Location = New System.Drawing.Point(0, 46)
        Me.lblSub3Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub3Visit.Name = "lblSub3Visit"
        Me.lblSub3Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub3Visit.TabIndex = 224
        Me.lblSub3Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub4Visit
        '
        Me.lblSub4Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub4Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub4Visit.Location = New System.Drawing.Point(0, 69)
        Me.lblSub4Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub4Visit.Name = "lblSub4Visit"
        Me.lblSub4Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub4Visit.TabIndex = 225
        Me.lblSub4Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub5Visit
        '
        Me.lblSub5Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub5Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub5Visit.Location = New System.Drawing.Point(0, 92)
        Me.lblSub5Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub5Visit.Name = "lblSub5Visit"
        Me.lblSub5Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub5Visit.TabIndex = 226
        Me.lblSub5Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub6Visit
        '
        Me.lblSub6Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(137, Byte), Integer), CType(CType(99, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.lblSub6Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub6Visit.Location = New System.Drawing.Point(0, 115)
        Me.lblSub6Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub6Visit.Name = "lblSub6Visit"
        Me.lblSub6Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub6Visit.TabIndex = 227
        Me.lblSub6Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSub7Visit
        '
        Me.lblSub7Visit.BackColor = System.Drawing.Color.FromArgb(CType(CType(101, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.lblSub7Visit.ForeColor = System.Drawing.Color.White
        Me.lblSub7Visit.Location = New System.Drawing.Point(0, 138)
        Me.lblSub7Visit.Margin = New System.Windows.Forms.Padding(0)
        Me.lblSub7Visit.Name = "lblSub7Visit"
        Me.lblSub7Visit.Size = New System.Drawing.Size(104, 23)
        Me.lblSub7Visit.TabIndex = 228
        Me.lblSub7Visit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVisitManager
        '
        Me.lblVisitManager.AutoSize = True
        Me.lblVisitManager.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVisitManager.Location = New System.Drawing.Point(0, 457)
        Me.lblVisitManager.Name = "lblVisitManager"
        Me.lblVisitManager.Size = New System.Drawing.Size(61, 15)
        Me.lblVisitManager.TabIndex = 285
        Me.lblVisitManager.Text = "Manager:"
        Me.lblVisitManager.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblManagerAway
        '
        Me.lblManagerAway.BackColor = System.Drawing.Color.LightSteelBlue
        Me.lblManagerAway.ForeColor = System.Drawing.Color.Black
        Me.lblManagerAway.Location = New System.Drawing.Point(0, 476)
        Me.lblManagerAway.Name = "lblManagerAway"
        Me.lblManagerAway.Size = New System.Drawing.Size(125, 23)
        Me.lblManagerAway.TabIndex = 286
        Me.lblManagerAway.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSwitchGames
        '
        Me.btnSwitchGames.BackColor = System.Drawing.Color.DimGray
        Me.btnSwitchGames.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSwitchGames.ForeColor = System.Drawing.Color.White
        Me.btnSwitchGames.Location = New System.Drawing.Point(870, 509)
        Me.btnSwitchGames.Name = "btnSwitchGames"
        Me.btnSwitchGames.Size = New System.Drawing.Size(122, 25)
        Me.btnSwitchGames.TabIndex = 299
        Me.btnSwitchGames.Text = "Switch Games"
        Me.btnSwitchGames.UseVisualStyleBackColor = False
        Me.btnSwitchGames.Visible = False
        '
        'frmModule1Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 15!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 539)
        Me.ControlBox = false
        Me.Controls.Add(Me.btnSwitchGames)
        Me.Controls.Add(Me.pnlVisit)
        Me.Controls.Add(Me.pnlHome)
        Me.Controls.Add(Me.pnlStats)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmModule1Main"
        Me.pnlStats.ResumeLayout(false)
        Me.pnlStats.PerformLayout
        Me.gpgoaltypes.ResumeLayout(false)
        Me.gpgoaltypes.PerformLayout
        Me.pnlHome.ResumeLayout(false)
        Me.pnlHome.PerformLayout
        Me.pnlHomeBench.ResumeLayout(false)
        Me.pnlVisit.ResumeLayout(false)
        Me.pnlVisit.PerformLayout
        Me.pnlVisitBench.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents lblPlayer1Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer2Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer3Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer4Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer5Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer6Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer7Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer8Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer9Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer10Home As System.Windows.Forms.Label
    Friend WithEvents lblPlayer11Home As System.Windows.Forms.Label
    Friend WithEvents lblHomeBench As System.Windows.Forms.Label
    Friend WithEvents lblPlayer11Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer10Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer9Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer8Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer7Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer6Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer5Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer4Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer3Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer2Visit As System.Windows.Forms.Label
    Friend WithEvents lblPlayer1Visit As System.Windows.Forms.Label
    Friend WithEvents lblVisitBench As System.Windows.Forms.Label
    Friend WithEvents pnlStats As System.Windows.Forms.Panel
    Friend WithEvents lvwHomeGoals As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lklNewHomeGoal As System.Windows.Forms.LinkLabel
    Friend WithEvents lblHomeGoals As System.Windows.Forms.Label
    Friend WithEvents lklEditHomeGoals As System.Windows.Forms.LinkLabel
    Friend WithEvents lklNewVisitGoal As System.Windows.Forms.LinkLabel
    Friend WithEvents lklNewVisitBooking As System.Windows.Forms.LinkLabel
    Friend WithEvents lklEditHomeBookings As System.Windows.Forms.LinkLabel
    Friend WithEvents lklNewHomeBooking As System.Windows.Forms.LinkLabel
    Friend WithEvents lblHomeBookingExpulsions As System.Windows.Forms.Label
    Friend WithEvents lvwHomeBookings As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader19 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader20 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lklNewVisitPenalty As System.Windows.Forms.LinkLabel
    Friend WithEvents lklEditHomePenalty As System.Windows.Forms.LinkLabel
    Friend WithEvents lklNewHomePenalty As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMissedHomePenalties As System.Windows.Forms.Label
    Friend WithEvents lvwHomePenalties As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader26 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader28 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader29 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader30 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lklNewVisitSub As System.Windows.Forms.LinkLabel
    Friend WithEvents lklEditHomeSubstitutions As System.Windows.Forms.LinkLabel
    Friend WithEvents lklNewHomeSub As System.Windows.Forms.LinkLabel
    Friend WithEvents lblHomeSubstitutions As System.Windows.Forms.Label
    Friend WithEvents lvwHomeSubstitutions As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader33 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader34 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader35 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader36 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader42 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader43 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader44 As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlHome As System.Windows.Forms.Panel
    Friend WithEvents pnlVisit As System.Windows.Forms.Panel
    Friend WithEvents ColumnHeader46 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader50 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader52 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader54 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblHomeManager As System.Windows.Forms.Label
    Friend WithEvents lblManagerHome As System.Windows.Forms.Label
    Friend WithEvents lblVisitManager As System.Windows.Forms.Label
    Friend WithEvents lblManagerAway As System.Windows.Forms.Label
    Friend WithEvents pnlHomeBench As System.Windows.Forms.Panel
    Friend WithEvents lblSub26Home As System.Windows.Forms.Label
    Friend WithEvents lblSub27Home As System.Windows.Forms.Label
    Friend WithEvents lblSub28Home As System.Windows.Forms.Label
    Friend WithEvents lblSub29Home As System.Windows.Forms.Label
    Friend WithEvents lblSub30Home As System.Windows.Forms.Label
    Friend WithEvents lblSub20Home As System.Windows.Forms.Label
    Friend WithEvents lblSub21Home As System.Windows.Forms.Label
    Friend WithEvents lblSub22Home As System.Windows.Forms.Label
    Friend WithEvents lblSub23Home As System.Windows.Forms.Label
    Friend WithEvents lblSub24Home As System.Windows.Forms.Label
    Friend WithEvents lblSub25Home As System.Windows.Forms.Label
    Friend WithEvents lblSub14Home As System.Windows.Forms.Label
    Friend WithEvents lblSub15Home As System.Windows.Forms.Label
    Friend WithEvents lblSub16Home As System.Windows.Forms.Label
    Friend WithEvents lblSub17Home As System.Windows.Forms.Label
    Friend WithEvents lblSub18Home As System.Windows.Forms.Label
    Friend WithEvents lblSub19Home As System.Windows.Forms.Label
    Friend WithEvents lblSub8Home As System.Windows.Forms.Label
    Friend WithEvents lblSub9Home As System.Windows.Forms.Label
    Friend WithEvents lblSub10Home As System.Windows.Forms.Label
    Friend WithEvents lblSub11Home As System.Windows.Forms.Label
    Friend WithEvents lblSub12Home As System.Windows.Forms.Label
    Friend WithEvents lblSub13Home As System.Windows.Forms.Label
    Friend WithEvents lblSub1Home As System.Windows.Forms.Label
    Friend WithEvents lblSub2Home As System.Windows.Forms.Label
    Friend WithEvents lblSub3Home As System.Windows.Forms.Label
    Friend WithEvents lblSub4Home As System.Windows.Forms.Label
    Friend WithEvents lblSub5Home As System.Windows.Forms.Label
    Friend WithEvents lblSub6Home As System.Windows.Forms.Label
    Friend WithEvents lblSub7Home As System.Windows.Forms.Label
    Friend WithEvents pnlVisitBench As System.Windows.Forms.Panel
    Friend WithEvents lblSub26Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub27Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub28Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub29Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub30Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub20Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub21Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub22Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub23Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub24Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub25Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub14Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub15Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub16Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub17Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub18Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub19Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub8Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub9Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub10Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub11Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub12Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub13Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub1Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub2Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub3Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub4Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub5Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub6Visit As System.Windows.Forms.Label
    Friend WithEvents lblSub7Visit As System.Windows.Forms.Label
    Friend WithEvents lklMatchInfo As System.Windows.Forms.LinkLabel
    Friend WithEvents lklOfficials As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMatchInfo As System.Windows.Forms.Label
    Friend WithEvents lblOfficials As System.Windows.Forms.Label
    Friend WithEvents lvwOfficials As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvwMatchInformation As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvwVisitGoals As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblVisitGoals As System.Windows.Forms.Label
    Friend WithEvents lklEditVisitGoals As System.Windows.Forms.LinkLabel
    Friend WithEvents lvwVisitBookings As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader18 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader23 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader24 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader25 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblVisitBookingExpulsions As System.Windows.Forms.Label
    Friend WithEvents lklEditVisitBookings As System.Windows.Forms.LinkLabel
    Friend WithEvents lvwVisitSubstitutions As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader37 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader38 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader39 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader40 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader41 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblVisitSubstitutions As System.Windows.Forms.Label
    Friend WithEvents lklEditVisitSubstitutions As System.Windows.Forms.LinkLabel
    Friend WithEvents lvwVisitPenalties As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader27 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader45 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader47 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader48 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader49 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblMissedVisitPenalties As System.Windows.Forms.Label
    Friend WithEvents lklEditVisitPenalty As System.Windows.Forms.LinkLabel
    Friend WithEvents ColumnHeader51 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader53 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPosition8Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition7Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition6Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition5Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition4Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition3Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition2Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition1Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition11Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition10Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition9Home As System.Windows.Forms.Label
    Friend WithEvents lblPosition11Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition10Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition9Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition8Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition7Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition6Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition5Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition4Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition3Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition2Visit As System.Windows.Forms.Label
    Friend WithEvents lblPosition1Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition26Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition27Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition28Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition29Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition30Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition20Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition21Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition22Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition23Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition24Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition25Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition14Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition15Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition16Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition17Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition18Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition19Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition8Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition9Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition10Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition11Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition12Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition13Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition1Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition2Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition3Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition4Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition5Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition6Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition7Home As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition26Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition27Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition28Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition29Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition30Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition20Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition21Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition22Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition23Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition24Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition25Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition14Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition15Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition16Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition17Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition18Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition19Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition8Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition9Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition10Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition11Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition12Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition13Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition1Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition2Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition3Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition4Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition5Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition6Visit As System.Windows.Forms.Label
    Friend WithEvents lblSubPosition7Visit As System.Windows.Forms.Label
    Friend WithEvents btnSwitchGames As System.Windows.Forms.Button
    Friend WithEvents gpgoaltypes As System.Windows.Forms.GroupBox
    Friend WithEvents radOwngoal As System.Windows.Forms.RadioButton
    Friend WithEvents radNormalgoal As System.Windows.Forms.RadioButton
    Friend WithEvents radPenalty As System.Windows.Forms.RadioButton
    Friend WithEvents lblgoaltype As System.Windows.Forms.Label
    Friend WithEvents btnCancelGoaltype As System.Windows.Forms.Button
    Friend WithEvents btnokgoaltype As System.Windows.Forms.Button
    Friend WithEvents lblteam As System.Windows.Forms.Label
End Class
