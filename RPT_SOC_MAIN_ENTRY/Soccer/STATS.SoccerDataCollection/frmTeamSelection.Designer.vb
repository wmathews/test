﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTeamSelection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnHomeTeam = New System.Windows.Forms.Button()
        Me.btnAwayTeam = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(121, 125)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(110, 25)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnHomeTeam
        '
        Me.btnHomeTeam.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnHomeTeam.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeTeam.ForeColor = System.Drawing.Color.Black
        Me.btnHomeTeam.Location = New System.Drawing.Point(201, 30)
        Me.btnHomeTeam.Name = "btnHomeTeam"
        Me.btnHomeTeam.Size = New System.Drawing.Size(147, 51)
        Me.btnHomeTeam.TabIndex = 346
        Me.btnHomeTeam.Text = "Home Team"
        Me.btnHomeTeam.UseVisualStyleBackColor = False
        '
        'btnAwayTeam
        '
        Me.btnAwayTeam.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnAwayTeam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwayTeam.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwayTeam.ForeColor = System.Drawing.Color.Black
        Me.btnAwayTeam.Location = New System.Drawing.Point(12, 30)
        Me.btnAwayTeam.Name = "btnAwayTeam"
        Me.btnAwayTeam.Size = New System.Drawing.Size(160, 51)
        Me.btnAwayTeam.TabIndex = 347
        Me.btnAwayTeam.Text = "Away Team"
        Me.btnAwayTeam.UseVisualStyleBackColor = False
        '
        'frmTeamSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(364, 156)
        Me.Controls.Add(Me.btnAwayTeam)
        Me.Controls.Add(Me.btnHomeTeam)
        Me.Controls.Add(Me.btnClose)
        Me.Name = "frmTeamSelection"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Team Selection"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnHomeTeam As System.Windows.Forms.Button
    Friend WithEvents btnAwayTeam As System.Windows.Forms.Button
End Class
