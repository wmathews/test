﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpticalFeed
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvPBP = New System.Windows.Forms.DataGridView
        Me.btnClose = New System.Windows.Forms.Button
        Me.Time = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Event1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Player = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Fill = New System.Windows.Forms.DataGridViewLinkColumn
        Me.Save = New System.Windows.Forms.DataGridViewLinkColumn
        Me.Cancel = New System.Windows.Forms.DataGridViewLinkColumn
        CType(Me.dgvPBP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvPBP
        '
        Me.dgvPBP.AllowUserToAddRows = False
        Me.dgvPBP.AllowUserToDeleteRows = False
        Me.dgvPBP.AllowUserToResizeColumns = False
        Me.dgvPBP.AllowUserToResizeRows = False
        Me.dgvPBP.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvPBP.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPBP.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvPBP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPBP.ColumnHeadersVisible = False
        Me.dgvPBP.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Time, Me.Event1, Me.Team, Me.Player, Me.Fill, Me.Save, Me.Cancel})
        Me.dgvPBP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvPBP.EnableHeadersVisualStyles = False
        Me.dgvPBP.GridColor = System.Drawing.SystemColors.Control
        Me.dgvPBP.Location = New System.Drawing.Point(-1, -2)
        Me.dgvPBP.MultiSelect = False
        Me.dgvPBP.Name = "dgvPBP"
        Me.dgvPBP.RowHeadersVisible = False
        Me.dgvPBP.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvPBP.RowTemplate.ReadOnly = True
        Me.dgvPBP.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPBP.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPBP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvPBP.ShowEditingIcon = False
        Me.dgvPBP.Size = New System.Drawing.Size(350, 112)
        Me.dgvPBP.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(318, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(22, 18)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "X"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Time
        '
        Me.Time.Frozen = True
        Me.Time.HeaderText = "Time"
        Me.Time.Name = "Time"
        Me.Time.ReadOnly = True
        Me.Time.Width = 35
        '
        'Event1
        '
        Me.Event1.Frozen = True
        Me.Event1.HeaderText = "Event"
        Me.Event1.Name = "Event1"
        Me.Event1.ReadOnly = True
        Me.Event1.Width = 70
        '
        'Team
        '
        Me.Team.Frozen = True
        Me.Team.HeaderText = "Team"
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.Width = 30
        '
        'Player
        '
        Me.Player.Frozen = True
        Me.Player.HeaderText = "Player"
        Me.Player.Name = "Player"
        Me.Player.ReadOnly = True
        Me.Player.Width = 85
        '
        'Fill
        '
        Me.Fill.HeaderText = ""
        Me.Fill.Name = "Fill"
        Me.Fill.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Fill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Fill.Text = "Fill"
        Me.Fill.Width = 25
        '
        'Save
        '
        Me.Save.HeaderText = ""
        Me.Save.Name = "Save"
        Me.Save.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Save.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Save.Text = "Save"
        Me.Save.Width = 60
        '
        'Cancel
        '
        Me.Cancel.HeaderText = ""
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Text = "Cancel"
        Me.Cancel.Width = 45
        '
        'frmOpticalFeed
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(348, 110)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgvPBP)
        Me.Controls.Add(Me.btnClose)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOpticalFeed"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        CType(Me.dgvPBP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvPBP As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Time As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Event1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Team As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Player As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fill As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents Save As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents Cancel As System.Windows.Forms.DataGridViewLinkColumn
End Class
