﻿
#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region " Imports "
Imports System.Diagnostics
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : udcRunningClock
' Author        : Fiaz
' Created Date  : 29th April 2009
' Description   : A running clock user-control to be used in data collection software to represent forward running game clocks.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'URC0001        | Fiaz                | 13th July 2009    | Modified code to allow time to tick beyond 59 minutes
'               |                     |                   |
'URC0002        | Fiaz                | 6th July 2011     | Revamped the code to use dot net commands and not to use midnight logic
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class udcRunningClock

#Region " Constants & Variables "
    Private stopWatch As New Stopwatch
    Private Offset As New TimeSpan(0, 0, 0, 0, 0)
    Private OffsetOpeartor As String = ""
    Private RunForeColor As Color
    Private PauseForeColor As Color
#End Region

#Region " Public Properties "
    Public ReadOnly Property URCCurrentTimeInMilliSec() As String
        Get
            Return lblHiddenClockDisplay.Text
        End Get
    End Property
    Public ReadOnly Property URCCurrentTime() As String
        Get
            Return lblClockDisplay.Text
        End Get
    End Property

    Public Property URCBackColor() As Color
        Get
            Return lblClockDisplay.BackColor
        End Get
        Set(ByVal value As Color)
            lblClockDisplay.BackColor = value
            Me.BackColor = value
        End Set
    End Property

    Public Property URCStartForeColor() As Color
        Get
            Return lblClockDisplay.ForeColor
        End Get
        Set(ByVal value As Color)
            lblClockDisplay.ForeColor = value
        End Set
    End Property

    Public Property URCPauseForeColor() As Color
        Get
            Return PauseForeColor
        End Get
        Set(ByVal value As Color)
            PauseForeColor = value
        End Set
    End Property

    Public Property URCRunForeColor() As Color
        Get
            Return RunForeColor
        End Get
        Set(ByVal value As Color)
            RunForeColor = value
        End Set
    End Property
#End Region

#Region " Public Methods "

    Public Sub URCReset()
        Try
            stopWatch.Reset()
            OffsetOpeartor = ""
            Offset = TimeSpan.Zero
            SetClockDisplay()
            tmrRunningClock.Enabled = False
            lblClockDisplay.ForeColor = PauseForeColor
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub URCSetTime(ByVal DisplayMin As Int16, ByVal DisplaySec As Int16, Optional ByVal Dummy As Boolean = False)
        Dim tmpTimeSpan As TimeSpan
        Try
            If DisplayMin < 0 Or DisplaySec < 0 Then
                Exit Sub
            End If

            tmpTimeSpan = New TimeSpan(0, DisplayMin, DisplaySec)

            If Not IsWithinRange(tmpTimeSpan) Then
                Exit Sub
            End If

            If TimeSpan.Compare(stopWatch.Elapsed, tmpTimeSpan) > 0 Then
                Offset = stopWatch.Elapsed.Subtract(tmpTimeSpan)
                OffsetOpeartor = "S"
                SetClockDisplay()
            ElseIf TimeSpan.Compare(stopWatch.Elapsed, tmpTimeSpan) < 0 Then
                Offset = tmpTimeSpan.Subtract(stopWatch.Elapsed)
                OffsetOpeartor = "A"
                SetClockDisplay()
            ElseIf tmpTimeSpan = TimeSpan.Zero Then
                SetClockDisplay()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function URCIsRunning() As Boolean
        Try
            Return stopWatch.IsRunning
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub URCAdjustTime(ByVal Increment As Boolean, ByVal Unit As IncrementUnit)
        Try
            Dim adjTimeSpan As TimeSpan
            Dim stpTimeSpan As TimeSpan = stopWatch.Elapsed
            Dim tmpTimeSpan As TimeSpan
            Dim wasRunning As Boolean = URCIsRunning()

            If Unit = IncrementUnit.Minutes Then
                adjTimeSpan = New TimeSpan(0, 1, 0)
            ElseIf Unit = IncrementUnit.Seconds Then
                adjTimeSpan = New TimeSpan(0, 0, 1)
            End If

            If Increment Then
                If OffsetOpeartor = "S" Then
                    tmpTimeSpan = stpTimeSpan.Subtract(Offset.Subtract(adjTimeSpan))
                    If Not IsWithinRange(tmpTimeSpan) Then
                        Exit Sub
                    End If
                    Offset = Offset.Subtract(adjTimeSpan)
                Else
                    tmpTimeSpan = stpTimeSpan.Add(Offset.Add(adjTimeSpan))
                    If Not IsWithinRange(tmpTimeSpan) Then
                        Exit Sub
                    End If
                    Offset = Offset.Add(adjTimeSpan)
                    OffsetOpeartor = "A"
                End If
            Else
                If OffsetOpeartor = "S" Then
                    tmpTimeSpan = stpTimeSpan.Subtract(Offset.Add(adjTimeSpan))
                    If Not IsWithinRange(tmpTimeSpan) Then
                        Exit Sub
                    End If
                    Offset = Offset.Add(adjTimeSpan)
                Else
                    tmpTimeSpan = stpTimeSpan.Add(Offset.Subtract(adjTimeSpan))
                    If Not IsWithinRange(tmpTimeSpan) Then
                        Exit Sub
                    End If
                    Offset = Offset.Subtract(adjTimeSpan)
                    OffsetOpeartor = "A"
                End If
            End If

            If wasRunning Then
                URCPause()
            End If

            SetClockDisplay()

            If wasRunning Then
                URCRun()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub URCToggle()
        Try
            If URCIsRunning() Then
                URCPause()
            Else
                URCRun()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Event Handlers "
    Private Sub tmrRunningClock_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrRunningClock.Tick
        Try
            SetClockDisplay()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lblClockDisplay_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblClockDisplay.DoubleClick
        Try
            Call MyBase.OnDoubleClick(e)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Enums "
    Public Enum IncrementUnit
        Seconds
        Minutes
    End Enum
#End Region

#Region " Private Methods "
    Private Sub URCRun()
        Try
            lblClockDisplay.ForeColor = RunForeColor
            tmrRunningClock.Enabled = True
            stopWatch.Start()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub URCPause()
        Try
            stopWatch.Stop()
            tmrRunningClock.Enabled = False
            lblClockDisplay.ForeColor = PauseForeColor
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetClockDisplay()
        Try
            Dim stpTimeSpan As TimeSpan = stopWatch.Elapsed
            Dim tmpTimeSpan As TimeSpan
            Dim hours As Integer
            Dim minutes As Integer
            Dim seconds As Integer
            Dim millisec As Integer

            If OffsetOpeartor = "A" Then
                tmpTimeSpan = stpTimeSpan.Add(Offset)
                If Not IsWithinRange(tmpTimeSpan) Then
                    URCPause()
                    Exit Sub
                End If
                stpTimeSpan = stpTimeSpan.Add(Offset)
            ElseIf OffsetOpeartor = "S" Then
                tmpTimeSpan = stpTimeSpan.Subtract(Offset)
                If Not IsWithinRange(tmpTimeSpan) Then
                    URCPause()
                    Exit Sub
                End If
                stpTimeSpan = stpTimeSpan.Subtract(Offset)
            End If

            hours = stpTimeSpan.Hours
            minutes = stpTimeSpan.Minutes
            If hours > 0 Then
                minutes = minutes + (hours * 60)
            End If
            seconds = stpTimeSpan.Seconds
            millisec = stpTimeSpan.Milliseconds

            lblClockDisplay.Text = minutes.ToString.PadLeft(2, CChar("0")) & ":" & seconds.ToString.PadLeft(2, CChar("0"))
            lblHiddenClockDisplay.Text = minutes.ToString.PadLeft(2, CChar("0")) & ":" & seconds.ToString.PadLeft(2, CChar("0")) & ":" & millisec.ToString.PadLeft(3, CChar("0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function IsWithinRange(ByVal ts As TimeSpan) As Boolean
        Try
            If ts.Hours < 0 Or ts.Minutes < 0 Or ts.Seconds < 0 Then
                Return False
            End If

            If ts.Hours > 16 Then
                Return False
            End If

            If ts.Hours = 16 And ts.Minutes > 39 Then
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

End Class
