﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDuplicateUniformNos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.dgvPlayerUniformNumbers = New System.Windows.Forms.DataGridView()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.btnOK = New System.Windows.Forms.Button()
        CType(Me.dgvPlayerUniformNumbers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblMsg
        '
        Me.lblMsg.AutoSize = True
        Me.lblMsg.Location = New System.Drawing.Point(46, 54)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(39, 15)
        Me.lblMsg.TabIndex = 0
        Me.lblMsg.Text = "Label1"
        '
        'dgvPlayerUniformNumbers
        '
        Me.dgvPlayerUniformNumbers.AllowUserToAddRows = False
        Me.dgvPlayerUniformNumbers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPlayerUniformNumbers.Location = New System.Drawing.Point(49, 97)
        Me.dgvPlayerUniformNumbers.Name = "dgvPlayerUniformNumbers"
        Me.dgvPlayerUniformNumbers.Size = New System.Drawing.Size(399, 89)
        Me.dgvPlayerUniformNumbers.TabIndex = 1
        '
        'PictureBox6
        '
        Me.PictureBox6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox6.Location = New System.Drawing.Point(-3, 12)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(569, 24)
        Me.PictureBox6.TabIndex = 318
        Me.PictureBox6.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.PictureBox7.Location = New System.Drawing.Point(-3, -1)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(569, 17)
        Me.PictureBox7.TabIndex = 317
        Me.PictureBox7.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(433, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 339
        Me.btnClose.Text = "Cancel"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnOK)
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(-3, 210)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(525, 52)
        Me.picButtonBar.TabIndex = 343
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnOK.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOK.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.White
        Me.btnOK.Location = New System.Drawing.Point(352, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 25)
        Me.btnOK.TabIndex = 340
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'frmDuplicateUniformNos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(511, 260)
        Me.Controls.Add(Me.picButtonBar)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.dgvPlayerUniformNumbers)
        Me.Controls.Add(Me.lblMsg)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "frmDuplicateUniformNos"
        Me.Text = "Duplicate Uniform Numbers"
        CType(Me.dgvPlayerUniformNumbers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMsg As System.Windows.Forms.Label
    Friend WithEvents dgvPlayerUniformNumbers As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents btnOK As System.Windows.Forms.Button
End Class
