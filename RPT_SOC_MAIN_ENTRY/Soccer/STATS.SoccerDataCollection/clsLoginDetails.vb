﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsLoginDetails
' Author        : Ravi Krishna
' Created Date  : 24 April 2009
' Description   : This class contains information specific to the current login session.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsLoginDetails

#Region " Constants & Variables "

    Shared m_myInstance As clsLoginDetails
    Private m_intUserId As Integer
    Private m_strUserFirstName As String
    Private m_strUserLastName As String
    Private m_strUserLoginName As String
    Private m_strUserType As m_enmUserType
    Private m_dtmLoginDate As String
    Private m_intLanguageID As Integer
    Private m_strLanguage As String
    Private m_blnGameAvailability As Boolean
    Private m_strGameMode As m_enmGameMode
    Private m_strAuditDirectory As String
    Private m_strAuditFileName As String
    Private m_dtmUSIndTimeDiff As Date
    Private m_strSortOrder As String
    Private m_blnisDemoDatadownloaded As Boolean = False
    Private m_tsUSIndTimeDiff As TimeSpan

    Public Enum m_enmUserType
        Reporter
        OPS
    End Enum

    Public Enum m_enmGameMode
        LIVE
        DEMO
    End Enum

#End Region

#Region " Shared methods "
    Public Shared Function GetInstance() As clsLoginDetails
        If m_myInstance Is Nothing Then
            m_myInstance = New clsLoginDetails
        End If
        Return m_myInstance
    End Function
#End Region

#Region " Public properties "

    Public Property UserId() As Integer
        Get
            Return m_intUserId
        End Get
        Set(ByVal value As Integer)
            m_intUserId = value
        End Set
    End Property

    Public Property UserFirstName() As String
        Get
            Return m_strUserFirstName
        End Get
        Set(ByVal value As String)
            m_strUserFirstName = value
        End Set
    End Property

    Public Property UserLastName() As String
        Get
            Return m_strUserLastName
        End Get
        Set(ByVal value As String)
            m_strUserLastName = value
        End Set
    End Property

    Public Property UserLoginName() As String
        Get
            Return m_strUserLoginName
        End Get
        Set(ByVal value As String)
            m_strUserLoginName = value
        End Set
    End Property

    Public Property UserType() As m_enmUserType
        Get
            Return m_strUserType
        End Get
        Set(ByVal value As m_enmUserType)
            m_strUserType = value
        End Set
    End Property

    Public Property LoginDate() As String
        Get
            Return m_dtmLoginDate
        End Get
        Set(ByVal value As String)
            m_dtmLoginDate = value
        End Set
    End Property

    Public Property LanguageID() As Integer
        Get
            Return m_intLanguageID
        End Get
        Set(ByVal value As Integer)
            m_intLanguageID = value
        End Set
    End Property

    Public Property Language() As String
        Get
            Return m_strLanguage
        End Get
        Set(ByVal value As String)
            m_strLanguage = value
        End Set
    End Property

    Public Property GameAvailability() As Boolean
        Get
            Return m_blnGameAvailability
        End Get
        Set(ByVal value As Boolean)
            m_blnGameAvailability = value
        End Set
    End Property

    Public Property GameMode() As m_enmGameMode
        Get
            Return m_strGameMode
        End Get
        Set(ByVal value As m_enmGameMode)
            m_strGameMode = value
        End Set
    End Property

    Public Property AuditDirectory() As String
        Get
            Return m_strAuditDirectory
        End Get
        Set(ByVal value As String)
            m_strAuditDirectory = value
        End Set
    End Property

    Public Property AuditFileName() As String
        Get
            Return m_strAuditFileName
        End Get
        Set(ByVal value As String)
            m_strAuditFileName = value
        End Set
    End Property

    Public Property USIndTimeDiff() As Date
        Get
            Return m_dtmUSIndTimeDiff
        End Get
        Set(ByVal value As Date)
            m_dtmUSIndTimeDiff = value
        End Set
    End Property

    Public Property strSortOrder() As String
        Get
            Return m_strSortOrder
        End Get
        Set(ByVal value As String)
            m_strSortOrder = value
        End Set
    End Property

    Public Property IsDemoDatadownloaded() As Boolean
        Get
            Return m_blnisDemoDatadownloaded
        End Get
        Set(ByVal value As Boolean)
            m_blnisDemoDatadownloaded = value
        End Set
    End Property

    Public Property USIndiaTimeDiff() As TimeSpan
        Get
            Return m_tsUSIndTimeDiff
        End Get
        Set(ByVal value As TimeSpan)
            m_tsUSIndTimeDiff = value
        End Set
    End Property

#End Region

#Region " User methods "

#End Region

End Class