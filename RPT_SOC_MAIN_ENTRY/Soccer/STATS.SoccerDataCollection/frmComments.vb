﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
Imports System.Globalization
#End Region

Public Class frmComments

#Region " Constants & Variables "
    Private m_objUtility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objclsComments As clsComments = clsComments.GetInstance()
    Private m_blnIsComboLoaded As Boolean = False
    Private m_objModule1PBP As New frmModule1PBP
    Private m_objModule1Main As New frmModule1Main
    Private MessageDialog As New frmMessageDialog
    Private lsupport As New languagesupport
    Private m_blnIsLanguageLoaded As Boolean = False

    ''Arindam 6-May-2014

    Dim temparr(5) As String
    Dim stlangcheck As String
    Private Shared m_ClickCount As Integer = 0
    Dim str As String = String.Empty
    Dim strlanguagename As String = String.Empty

    Public cmbval As Int32
    Dim lagid As Integer

    ''Arindam

    Dim strmessage6 As String = "Please enter Comments"
    Dim strmessage1 As String = "Enter the correct time!"
    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

#End Region

    ''' <summary>
    ''' Handles the Load event of the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmComments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAdandon", Nothing, 1, 0)
            'If m_objModule1Main.
            Me.CenterToParent()

            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            'If Not m_objModule1Main Is Nothing Then
            'End If
            Dim DsLanguage As DataSet
            DsLanguage = m_objclsComments.GetLanguage()
            If (DsLanguage.Tables.Count > 0) Then
                If (DsLanguage.Tables(0).Rows.Count > 0) Then
                    clsUtility.LoadControl(cmbLanguage, clsUtility.SelectTypeDisplay.BLANK, DsLanguage.Tables(0), "LANGUAGE_DESC", "LANGUAGE_ID")
                    m_blnIsLanguageLoaded = True
                    cmbLanguage.SelectedValue = m_objGameDetails.languageid
                    If m_objGameDetails.IsDelayed = True Then
                        FillCommentsForDelay()
                    Else
                        FillComments()
                    End If
                End If
            End If
            If m_objGameDetails.ModuleID = 3 Then
                txtTimeMod2.Visible = False
                lblTime.Visible = False
                cmbTeam.Visible = False
                lblTeam.Visible = False
                mtxtDelay1.Visible = False
                lblDelay.Visible = False
            Else
                If m_objGameDetails.IsDelayed = True Then
                    txtTimeMod2.Visible = False

                    lblTime.Visible = False
                    cmbTeam.Visible = False
                    lblTeam.Visible = False
                    'Arindam 03-Oct-2013
                    mtxtDelay1.Visible = True
                    lblDelay.Visible = True
                    If m_objGameDetails.CurrentPeriod = 0 Then
                        mtxtDelay1.Enabled = True
                        lblDelay.Enabled = True
                        'txtTimeMod2.Enabled = False
                        Dim dsDelayData As DataSet
                        dsDelayData = m_objclsComments.getDelayData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        If (dsDelayData.Tables(0).Rows.Count > 0) Then
                            Dim dsGameSetupData As DataSet
                            dsGameSetupData = m_objclsComments.getGameSetupData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

                            If (dsGameSetupData.Tables(0).Rows.Count > 0) Then
                                m_objGameDetails.DelayTime = CDate(dsGameSetupData.Tables(0).Rows(0)("START_TIME_ACTUAL"))
                            End If
                            Dim timediff As TimeSpan
                            timediff = m_objGameDetails.DelayTime.Subtract(CDate(m_objGameDetails.GameDate))
                            If timediff.Hours < 0 Or timediff.Minutes < 0 Then
                                mtxtDelay1.Text = "00:00"
                            Else
                                mtxtDelay1.Text = Format(timediff.Hours, "00") & ":" & Format(timediff.Minutes, "00")
                            End If
                            'If IsDBNull(dsDelayData.Tables(0).Rows(0).Item("COMMENTS")) = False Then
                            '    txtComments.Text = CStr(dsDelayData.Tables(0).Rows(0).Item("COMMENTS"))
                            'End If
                            'If CInt(dsDelayData.Tables(0).Rows(0).Item("COMMENT_ID")) <> 0 Then
                            '    cmbComments.SelectedValue = CInt(dsDelayData.Tables(0).Rows(0).Item("COMMENT_ID"))
                            '    cmbLanguage.SelectedValue = CInt(dsDelayData.Tables(0).Rows(0).Item("COMMENT_LANGUAGE"))
                            'End If
                        End If

                    ElseIf m_objGameDetails.CurrentPeriod > 0 Then
                        mtxtDelay1.Visible = False
                        lblDelay.Visible = False
                        txtTimeMod2.Visible = False ' was true before
                        txtTimeMod2.Enabled = False
                        'RM 8695
                        If m_objGameDetails.CoverageLevel > 3 And m_objGameDetails.IsEditMode Then
                            mtxtDelay1.Visible = True
                            lblDelay.Text = "Clock Time :"
                            lblDelay.Visible = True
                            mtxtDelay1.Text = clsUtility.ConvertSecondToMinute(CDbl(m_objGameDetails.CommentsTime), False)
                        ElseIf m_objGameDetails.CoverageLevel < 4 Then
                            txtTimeMod2.Visible = True ' was true before
                            txtTimeMod2.Enabled = True
                            lblTime.Visible = True
                            lblTime.Text = "Clock Time"
                        End If
                    End If
                Else
                    txtTimeMod2.Visible = True
                    lblTime.Visible = True
                    cmbTeam.Visible = True
                    lblTeam.Visible = True
                    mtxtDelay1.Visible = False
                    lblDelay.Visible = False

                End If
            End If
            If m_objGameDetails.IsEditMode Then
                'COMMENTED BY SHRAVANI
                cmbLanguage.SelectedValue = m_objGameDetails.CommentLanguageID
                'cmbComments.SelectedValue = m_objGameDetails.PredefinedCommentID
            End If

            'LOAD TEAM COMBO
            LoadTeamCombo()
            If m_objGameDetails.ModuleID = 2 And (m_objGameDetails.CoverageLevel <= 3) And m_objGameDetails.IsDelayed = False Then
                lblTime.Visible = True
                txtTimeMod2.Visible = True
            End If

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Dim TEMLENINT As Integer
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnOK.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "OK")
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                TEMLENINT = lblTeam.Text.Length
                lblTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team name") + " :"
                linebreak(lblTeam, TEMLENINT)
                lblComments.Text = lblComments.Text.Replace(":", "")
                lblComments.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblComments.Text)
                lblComments.Text = lblComments.Text & ":"
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                lblComments.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblComments.Text)
                grpComments.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), grpComments.Text)
                lblLanguage.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLanguage.Text)
                lblPredefinedComments.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblPredefinedComments.Text)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub linebreak(ByVal lbtemp As Label, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub
    ''' <summary>
    ''' Loads team combo
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadTeamCombo()
        Try
            ''Dim dsTeam As New DataSet
            ''Dim dtTeam As DataTable
            ''dsTeam = m_objclsComments.LoadTeam(m_objGameDetails.GameCode, m_objGameDetails.LeagueID)
            ''dtTeam = dsTeam.Tables(0).Copy
            ''clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.BLANK, dtTeam, "TEAMNAME", "TEAM_ID")
            'cmbTeam.SelectedValue = m_objGameDetails.HomeTeamID
            'm_blnIsComboLoaded = True

            Dim dtTeam As New DataTable
            dtTeam.Columns.Add("TEAM_ID")
            dtTeam.Columns.Add("TEAM_NAME")

            Dim drTeam As DataRow
            drTeam = dtTeam.NewRow()
            drTeam("TEAM_ID") = -1
            drTeam("TEAM_NAME") = ""
            dtTeam.Rows.Add(drTeam)
            'HOME TEAM
            drTeam = dtTeam.NewRow()
            drTeam("TEAM_ID") = m_objGameDetails.HomeTeamID
            drTeam("TEAM_NAME") = m_objGameDetails.HomeTeam
            dtTeam.Rows.Add(drTeam)

            'AWAY TEAM
            drTeam = dtTeam.NewRow()
            drTeam("TEAM_ID") = m_objGameDetails.AwayTeamID
            drTeam("TEAM_NAME") = m_objGameDetails.AwayTeam
            dtTeam.Rows.Add(drTeam)
            txtComments.Text = ""
            clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.BLANK, dtTeam, "TEAM_NAME", "TEAM_ID")
            If m_objGameDetails.IsEditMode Then
                If m_objGameDetails.OffensiveTeamID = -1 Then
                    cmbTeam.SelectedIndex = -1
                Else
                    cmbTeam.SelectedValue = m_objGameDetails.OffensiveTeamID
                End If
                txtComments.SelectedText = m_objGameDetails.CommentData

                If m_objGameDetails.PredefinedCommentID = -1 Then
                    cmbComments.SelectedIndex = -1
                Else
                    cmbComments.SelectedValue = m_objGameDetails.PredefinedCommentID
                End If
                If m_objGameDetails.ModuleID = 2 Then
                    ' Dim Strtime As String = CalculateTimeElapseAfter(m_objGameDetails.CommentsTime, m_objGameDetails.CurrentPeriod)
                    ' Dim arr As Array = Strtime.Split(CChar(":"))
                    txtTimeMod2.Text = CStr(m_objGameDetails.CommentsTime / 60)
                    txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))

                Else
                    Dim Strtime As String = CalculateTimeElapseAfter(m_objGameDetails.CommentsTime, m_objGameDetails.CurrentPeriod)
                    Dim arr As Array = Strtime.Split(CChar(":"))
                    txtTimeMod2.Text = arr.GetValue(0).ToString
                    txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))

                End If

            End If
            'cmbTeam.SelectedValue = m_objGameDetails.HomeTeamID
            m_blnIsComboLoaded = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Handles the event of OK button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try

            Dim strTime As String
            Dim EditedTime As Integer
            Dim dsGameSetUpDelay As New DataSet
            '
            If txtComments.Text = "" And CInt(cmbComments.SelectedValue) = 0 Then
                MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            Else
                m_objGameDetails.CommentData = txtComments.Text
            End If
            'strTime = clsUtility.ConvertSecondToMinute(CInt(txtTimeMod2.Text) * 60, False)
            'strTime = txtTimeMod2.Text.Replace(":", "").PadLeft(5, CChar(" "))
            If txtTimeMod2.Visible = True Then
                If txtTimeMod2.Text.Trim = "" Then
                    MessageDialog.Show(strmessage1, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If

                'RM 8695
                EditedTime = CInt(txtTimeMod2.Text) * 60
                Dim TimeElapsed As Integer = m_objclsComments.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                If m_objGameDetails.IsDefaultGameClock Then
                    strTime = CalculateTimeElapseAfter(TimeElapsed, m_objGameDetails.CurrentPeriod)
                    If strTime = "00:00" Then
                        strTime = "0"
                    End If
                    TimeElapsed = CInt(strTime) * 60
                End If
                m_objGameDetails.CommentsTime = EditedTime
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOK.Text, Nothing, 1, 0)
            ElseIf mtxtDelay1.Visible = True Then
                EditedTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(mtxtDelay1.Text)))
                m_objGameDetails.CommentsTime = EditedTime
            End If

            If m_objGameDetails.IsDelayed = True Then
                If m_objGameDetails.CurrentPeriod = 0 Then
                    'If mtxtDelay1.Text <> "00:00" Then
                    '    Dim strDelayTime() As String
                    '    Dim DelayTime As Double

                    '    strDelayTime = mtxtDelay1.Text.Split(CChar(":"))
                    '    DelayTime = CDbl(strDelayTime(0)) * 60 + CDbl(strDelayTime(1))
                    '    m_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))  'DateTime.ParseExact(m_objGameDetails.GameDate, "MM/dd/yyyy HH:mm:ss am", System.Globalization.CultureInfo.InvariantCulture).Date

                    'End If
                    If mtxtDelay1.Text <> "00:00" Then
                        Dim strDelayTime() As String
                        Dim DelayTime As Double

                        strDelayTime = mtxtDelay1.Text.Split(CChar(":"))
                        DelayTime = CDbl(strDelayTime(0)) * 60 + CDbl(strDelayTime(1))
                        m_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))

                        Dim dtDate As DateTime = m_objGameDetails.DelayTime.AddMinutes(DelayTime)
                        Dim dsDelayData As DataSet
                        dsDelayData = m_objclsComments.getDelayData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        If (dsDelayData.Tables(0).Rows.Count > 0) Then
                            Dim dsUpdatedGameSetup As DataSet
                            dsUpdatedGameSetup = m_objclsComments.updateDelayTime(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, dtDate)
                            If dsUpdatedGameSetup.Tables(0).Rows.Count > 0 Then

                            End If
                        Else
                            'no delay data - first time entry
                            dsGameSetUpDelay = CreateAndAddDataColumn()
                            m_objclsComments.InsertUpdateGameSetupDelay(dsGameSetUpDelay)
                        End If

                        ''''drSelecteDataGamesetup("START_TIME_ACTUAL") = DateTimeHelper.GetDateString(dtDate, DateTimeHelper.OutputFormat.ISOFormat)
                        'm_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))  'DateTime.ParseExact(m_objGameDetails.GameDate, "MM/dd/yyyy HH:mm:ss am", System.Globalization.CultureInfo.InvariantCulture).Date
                        'drSelecteDataGamesetup("START_TIME_ACTUAL") = m_objGameDetails.DelayTime.AddMinutes(DelayTime).ToString("yyyy-MM-dd HH:mm:ss tt")
                    Else
                        'drSelecteDataGamesetup("START_TIME_ACTUAL") = Format(CDate(m_objGameDetails.GameDate), "yyyy-MM-dd HH:mm:ss tt")
                        '''''drSelecteDataGamesetup("START_TIME_ACTUAL") = DateTimeHelper.GetDateString(CDate(m_objGameDetails.GameDate), DateTimeHelper.OutputFormat.ISOFormat)
                    End If
                    ''''m_objGameDetails.DelayTime = CDate(drSelecteDataGamesetup("START_TIME_ACTUAL"))

                End If

            End If
            If CInt(cmbTeam.SelectedValue) > 0 Then
                m_objGameDetails.OffensiveTeamID = CInt(cmbTeam.SelectedValue)
            Else
                m_objGameDetails.OffensiveTeamID = -1
            End If

            'PREDEFINED COMMENTS
            If CInt(cmbComments.SelectedValue) > 0 Then
                m_objGameDetails.PredefinedCommentID = CInt(cmbComments.SelectedValue)
            Else
                m_objGameDetails.PredefinedCommentID = -1
            End If

            'COMMENTED BY SHRAVANI
            If CInt(cmbLanguage.SelectedValue) > 0 Then
                m_objGameDetails.CommentLanguageID = CInt(cmbLanguage.SelectedValue)
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
            txtComments.Text = ""
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function CreateAndAddDataColumn() As DataSet
        Try

            Dim dsSelectedData As New DataSet
            Dim strDelayTime() As String
            Dim DelayTime As Double
            dsSelectedData.Tables.Add()

            dsSelectedData.Tables(0).Columns.Add("GAME_CODE")
            dsSelectedData.Tables(0).Columns.Add("FEED_NUMBER")
            dsSelectedData.Tables(0).Columns.Add("LEAGUE_ID")
            dsSelectedData.Tables(0).Columns.Add("FEED_GAME_CODE")
            dsSelectedData.Tables(0).Columns.Add("VENUE_ID")
            dsSelectedData.Tables(0).Columns.Add("ATTENDANCE")
            dsSelectedData.Tables(0).Columns.Add("START_TIME_ACTUAL")
            dsSelectedData.Tables(0).Columns.Add("WEATHER")
            dsSelectedData.Tables(0).Columns.Add("WEATHER_ID")
            dsSelectedData.Tables(0).Columns.Add("TEMPERATURE_F")
            dsSelectedData.Tables(0).Columns.Add("TEMPERATURE_C")
            dsSelectedData.Tables(0).Columns.Add("REFEREE_ID")
            dsSelectedData.Tables(0).Columns.Add("REFEREE_AST_1_ID")
            dsSelectedData.Tables(0).Columns.Add("REFEREE_AST_2_ID")
            dsSelectedData.Tables(0).Columns.Add("FOURTH_OFFICIAL_ID")
            dsSelectedData.Tables(0).Columns.Add("FIFTH_OFFICIAL_ID")
            dsSelectedData.Tables(0).Columns.Add("SIXTH_OFFICIAL_ID")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE_ET")
            dsSelectedData.Tables(0).Columns.Add("MATCH_DONE")
            dsSelectedData.Tables(0).Columns.Add("SHOOTOUT")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE_TOUCH")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE_TOUCH_ET")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_1")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_2")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_3")
            'aRINDAM 1-May-12 5/6 th official ratins added
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_5")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_6")
            dsSelectedData.Tables(0).Columns.Add("MATCH_RATING")
            dsSelectedData.Tables(0).Columns.Add("PROCESSED")
            dsSelectedData.Tables(0).Columns.Add("DEMO_DATA")

            Dim drSelecteDataGamesetup As DataRow
            drSelecteDataGamesetup = dsSelectedData.Tables(0).NewRow()

            drSelecteDataGamesetup("GAME_CODE") = m_objGameDetails.GameCode
            drSelecteDataGamesetup("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drSelecteDataGamesetup("LEAGUE_ID") = m_objGameDetails.LeagueID
            drSelecteDataGamesetup("VENUE_ID") = m_objGameDetails.FieldID
            drSelecteDataGamesetup("ATTENDANCE") = DBNull.Value
            If mtxtDelay1.Text <> "00:00" Then
                strDelayTime = mtxtDelay1.Text.Split(CChar(":"))
                DelayTime = CDbl(strDelayTime(0)) * 60 + CDbl(strDelayTime(1))
                m_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
                Dim dtDate As DateTime = m_objGameDetails.DelayTime.AddMinutes(DelayTime)
                drSelecteDataGamesetup("START_TIME_ACTUAL") = DateTimeHelper.GetDateString(dtDate, DateTimeHelper.OutputFormat.ISOFormat)
                'm_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))  'DateTime.ParseExact(m_objGameDetails.GameDate, "MM/dd/yyyy HH:mm:ss am", System.Globalization.CultureInfo.InvariantCulture).Date
                'drSelecteDataGamesetup("START_TIME_ACTUAL") = m_objGameDetails.DelayTime.AddMinutes(DelayTime).ToString("yyyy-MM-dd HH:mm:ss tt")
            Else
                'drSelecteDataGamesetup("START_TIME_ACTUAL") = Format(CDate(m_objGameDetails.GameDate), "yyyy-MM-dd HH:mm:ss tt")
                drSelecteDataGamesetup("START_TIME_ACTUAL") = DateTimeHelper.GetDateString(CDate(m_objGameDetails.GameDate), DateTimeHelper.OutputFormat.ISOFormat)
            End If
            m_objGameDetails.DelayTime = CDate(drSelecteDataGamesetup("START_TIME_ACTUAL"))
            drSelecteDataGamesetup("REFEREE_ID") = DBNull.Value
            drSelecteDataGamesetup("REFEREE_AST_1_ID") = DBNull.Value
            drSelecteDataGamesetup("REFEREE_AST_2_ID") = DBNull.Value
            drSelecteDataGamesetup("FOURTH_OFFICIAL_ID") = DBNull.Value
            drSelecteDataGamesetup("FIFTH_OFFICIAL_ID") = DBNull.Value
            drSelecteDataGamesetup("SIXTH_OFFICIAL_ID") = DBNull.Value

            'Weather and temp
            drSelecteDataGamesetup("WEATHER") = DBNull.Value
            drSelecteDataGamesetup("WEATHER_ID") = DBNull.Value

            drSelecteDataGamesetup("TEMPERATURE_F") = DBNull.Value

            drSelecteDataGamesetup("TEMPERATURE_C") = DBNull.Value

            'Arindam - Module 2 NO NEED TO CHOOSE HOME TEM SIDE - DEFAULT TO LEFT ALWAYS (30-Jul-09)

            '14-Mar-2012 added code to update extra time home start informtion

            drSelecteDataGamesetup("HOME_START_SIDE") = ""

            If m_objGameDetails.CurrentPeriod <= 1 Then
                If m_objGameDetails.ModuleID = 1 Then
                    m_objGameDetails.isHomeDirectionLeft = m_objGameDetails.IsHomeTeamOnLeft
                End If
            End If

            'drSelecteDataGamesetup("REF_RATING_1") = IIf(txtRating1.Text = "", DBNull.Value, txtRating1.Text)
            'drSelecteDataGamesetup("REF_RATING_2") = IIf(txtRating2.Text = "", DBNull.Value, txtRating2.Text)
            'drSelecteDataGamesetup("REF_RATING_3") = IIf(txtRating3.Text = "", DBNull.Value, txtRating3.Text)

            drSelecteDataGamesetup("REF_RATING_1") = DBNull.Value
            drSelecteDataGamesetup("REF_RATING_2") = DBNull.Value
            drSelecteDataGamesetup("REF_RATING_3") = DBNull.Value
            'Arindam 1-May-12 5/6th Official
            drSelecteDataGamesetup("REF_RATING_5") = DBNull.Value
            drSelecteDataGamesetup("REF_RATING_6") = DBNull.Value
            drSelecteDataGamesetup("MATCH_RATING") = DBNull.Value

            drSelecteDataGamesetup("PROCESSED") = "N"

            If m_objGameDetails.IsDemoGameSelected = False Then
                drSelecteDataGamesetup("DEMO_DATA") = "N"
            Else
                drSelecteDataGamesetup("DEMO_DATA") = "Y"
            End If

            dsSelectedData.Tables(0).Rows.Add(drSelecteDataGamesetup)
            Return dsSelectedData

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            'm_objGameDetails.DelayTime = Nothing
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTeam.SelectedIndexChanged
        Try
            If (cmbTeam.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbTeam.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub txtComments_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComments.KeyPress
    '    Try
    '        Dim trestrlanguagename As String = Application.CurrentInputLanguage.Culture.Name.ToString
    '        Dim lgshort As String
    '        lgshort = "en-uS"
    '        Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
    '        Dim strlanguagename1 As String = Application.CurrentInputLanguage.Culture.EnglishName
    '        If (strlanguagename1 <> "English (United States)") Then
    '            MessageBox.Show("Currently English language is not Avilable in the computer install the language and restart the application")
    '            Me.Close()
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub txtComments_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComments.KeyPress
        Try
            Dim trestrlanguagename As String = Application.CurrentInputLanguage.Culture.Name.ToString
            Dim lgshort As String
            'lgshort = "en-uS"
            'Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            'Dim strlanguagename1 As String = Application.CurrentInputLanguage.Culture.EnglishName

            ''Arindam 6-May-2014
            lagid = CInt(cmbLanguage.SelectedValue)
            systemlanguagecheck(lagid)
            If (lagid <> 0) Then
                If (stlangcheck = "notset") Then
                    If lagid = 1 Then
                        MessageDialog.Show("Assigned Language Is not available in your computer!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Me.Close()
                    Else
                        If lagid = 9 Then
                            ''DUTCH
                            lgshort = "nl-NL"
                            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
                            If (strlanguagename <> lgshort) Then
                                lgshort = "nl-BE"
                                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
                                If (strlanguagename <> lgshort) Then
                                    lgshort = "en-US"
                                    Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                                    strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
                                    If (strlanguagename <> lgshort) Then
                                        MessageDialog.Show("Assigned Language Is not available in your computer!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                        Me.Close()
                                    Else
                                        MessageDialog.Show("Assigned Language Is not available in your computer - you can type only in English!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    End If

                                End If
                            End If
                            'If (strlanguagename <> lgshort) Then
                            '    lgshort = "nl-BE"
                            '    Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                            '    strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
                            'End If
                            ''
                        ElseIf lagid = 16 Then
                            ''RUSSIAN
                            lgshort = "ru-RU"
                            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
                            If (strlanguagename <> lgshort) Then
                                lgshort = "en-US"
                                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
                                If (strlanguagename <> lgshort) Then
                                    MessageDialog.Show("Assigned Language Is not available in your computer!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    Me.Close()
                                Else
                                    MessageDialog.Show("Assigned Language Is not available in your computer - you can type only in English!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                End If

                            End If
                            ''
                        End If
                    End If

                End If
            End If

            ''

            'If (strlanguagename1 <> "English (United States)") Then
            '    MessageBox.Show("Currently English language is not Avilable in the computer install the language and restart the application")
            '    Me.Close()
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtComments_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtComments.Leave
        Try
            If (txtComments.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtComments.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 0 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 1 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 2 Then
                    Return CStr(CInt((FIRSTHALF + ElapsedTime) / 60))
                ElseIf CurrPeriod = 3 Then
                    Return CStr(CInt((SECONDHALF + ElapsedTime) / 60))
                    'clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return CStr(CInt((FIRSTEXTRA + ElapsedTime) / 60))
                    'clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If

            If m_objGameDetails.IsDefaultGameClock = False And m_objGameDetails.ModuleID = 2 Then
                Return CStr(CInt(ElapsedTime / 60))
            End If

            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer) As Integer
        Try
            If m_objGameDetails.CurrentPeriod = 1 Then
                Return CurrentElapsedTime
            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF
            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA
            ElseIf m_objGameDetails.CurrentPeriod = 5 Then
                Return CurrentElapsedTime
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub txtTimeMod2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTimeMod2.Validated
        Try
            txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLanguage.SelectedIndexChanged
        Try
            If m_blnIsLanguageLoaded = True Then
                If m_objGameDetails.IsDelayed = True Then
                    FillCommentsForDelay()
                Else
                    FillComments()
                End If
                'FillComments()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub FillComments()
        Try
            cmbComments.DataSource = Nothing
            Dim dsComments As New DataSet
            dsComments = m_objclsComments.GetPredefinedComments(CInt(cmbLanguage.SelectedValue))
            If dsComments.Tables.Count > 0 Then
                If dsComments.Tables(0).Rows.Count > 0 Then
                    clsUtility.LoadControl(cmbComments, clsUtility.SelectTypeDisplay.NONE, dsComments.Tables(0), "COMMENT_DESC", "COMMENT_ID")
                    cmbComments.SelectedIndex = 0
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub FillCommentsForDelay()
        Try
            cmbComments.DataSource = Nothing
            Dim dsComments As New DataSet
            dsComments = m_objclsComments.GetPredefinedCommentsforDelay(CInt(cmbLanguage.SelectedValue))
            If dsComments.Tables.Count > 0 Then
                If dsComments.Tables(0).Rows.Count > 0 Then
                    clsUtility.LoadControl(cmbComments, clsUtility.SelectTypeDisplay.NONE, dsComments.Tables(0), "COMMENT_DESC", "COMMENT_ID")
                    cmbComments.SelectedIndex = 0
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ''Arindam 6-May-2014 START
#Region "Language Check"
    '#Region "Final Check based on avilable language in system , language seleted in login,language assigned for a game"
    '    Public Sub assignmentlanguagecheck()
    '        Try
    '            systemlanguagecheck(lagid)
    '            If (lagid <> 0) Then
    '                If (stlangcheck = "notset") Then
    '                    MessageDialog.Show("Assigned Language Is Not Available In Your System.It Automatically changes  language seleted in login", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '                    m_objGameDetails.languageid = CInt(cmbval)
    '                    m_objLoginDetails.LanguageID = CInt(cmbval)
    '                Else
    '                    If (m_objGameDetails.IsNewDemomode = False) Then
    '                        MessageDialog.Show("Language selected Differs .It Automatically changes To Assigned Language ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '                    End If
    '                    m_objGameDetails.languageid = CInt(lagid)
    '                    m_objLoginDetails.LanguageID = CInt(lagid)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            MessageDialog.Show(ex, Me.Text)
    '        End Try
    '    End Sub
    '#End Region
#Region "Check the language avilablke in system"
    Public Sub systemlanguagecheck(ByVal languageid As Integer)
        Try
            str = CStr(lsupport.langselect(CInt(languageid)))
            strlanguagename = CultureInfo.GetCultureInfo("" + str + "").DisplayName.ToString()
            strlanguagename = strlanguagename.Substring(0, strlanguagename.IndexOf("("))
            lanfunc(languageid)
            For i As Integer = 0 To 4
                Dim ss As String = Application.CurrentInputLanguage.Culture.Name.ToString
                If (temparr(i) = ss) Then
                    stlangcheck = "set"
                    Exit For
                Else
                    stlangcheck = "notset"
                End If
            Next
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region
#Region "Getting the language short form based on language id"
    Public Sub lanfunc(ByVal ch As Integer)
        Try
            For i As Integer = 0 To 3
                temparr(i) = ""
            Next
            If (ch = 2) Then
                temparr(0) = "es-ES"
                temparr(1) = "es-GT"
            ElseIf (ch = 3) Then
                temparr(0) = "zh-TW"
                temparr(1) = "zh-CN"
                temparr(2) = "zh-HK"
            ElseIf (ch = 4) Then
                temparr(0) = "ja-JP"
            ElseIf (ch = 5) Then
                temparr(0) = "de-DE"
            ElseIf (ch = 6) Then
                temparr(0) = "fr-FR"
            ElseIf (ch = 7) Then
                temparr(0) = "pt-BR"
            ElseIf (ch = 8) Then
                temparr(0) = "it-IT"
            ElseIf (ch = 9) Then
                temparr(0) = "nl-NL"
                temparr(1) = "nl-BE"
            ElseIf (ch = 10) Then
                temparr(0) = "ar-IQ"
            ElseIf (ch = 13) Then
                temparr(0) = "es-ES"
                temparr(1) = "es-GT"
                temparr(2) = "es-MX"
            ElseIf (ch = 16) Then
                temparr(0) = "ru-RU"
            ElseIf (ch = 1) Then
                temparr(0) = "en-US"
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region
#End Region

    ''Arindam 6-May-2014 END
End Class