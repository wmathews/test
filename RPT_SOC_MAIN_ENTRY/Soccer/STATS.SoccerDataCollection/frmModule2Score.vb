﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.Data.SqlClient
Imports System.IO
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmModule1
' Author        : Shravani
' Created Date  :
' Description   : This is the entry form for module 2 and CoverageLevel 1. This form will be contained within frmMain at runtime.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmModule2Score

#Region " Member Variables "
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objUtility As New clsUtility
    Private m_objGeneral As New clsGeneral
    Private m_objModule2Score As clsModule2Score = SoccerBL.clsModule2Score.GetInstance()
    Private m_dsSavePBPDataXML As New DataSet
    Private m_dsScores As New DataSet
    Private m_CurrPeriod As Integer = 0
    Private m_TimeEditMode As Boolean = False
    Private m_allowSave As Boolean = True
    Private m_intSequenceNumber As Decimal
    Private m_CurrentEventCode As Integer
    Private m_dsGames As New DataSet
    Private m_dsMultiplegames As New DataSet
    Dim m_dsPBPData As New DataSet
    Private m_EventID As Integer
    Private m_TeamID As Integer
    Private m_dsTempPBP As New DataSet
    Private m_NextElapsedTime As Integer
    Private m_NextEventCode As Integer
    Private m_PrevElapsedTime As Integer
    Private m_PrevSeq As Decimal
    Private m_CommentEditTime As Integer
    Private m_decOrigSeq As Decimal
    Private m_NextSeq As Decimal
#End Region

#Region " Constants "
    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200
    Private lsupport As New languagesupport

    Dim strmessage As String = "Cannot edit End Game!"
    Dim strmessage1 As String = "Cannot edit End Period!"
    Dim strmessage2 As String = "Cannot edit Start Period!"
    Dim strmessage3 As String = "Cannot edit TIME event!"
    Dim strmessage4 As String = "Cannot edit Game Start event!"
    Dim strmessage5 As String = "Enter a New Event now!"
    Dim strmessage6 As String = "Insert is not allowed!"
    Dim strmessage7 As String = "VISIT START LINEUP - not implemented yet!!!"
    Dim strmessage8 As String = "Home START LINEUP - not implemented yet!!!"
    Dim strmessage9 As String = "Game Start - not implemented yet!!!"
    Dim strmessage10 As String = "Cannot delete Start Period!"
    Dim strmessage11 As String = "Cannot delete End Game!"
    Dim strmessage12 As String = "Cannot delete End Period!"
    Dim strmessage13 As String = "Currently there is no game(s) available"
    Dim strmessage14 As String = "Cannot delete Game Start event!"
    Dim strmessage15 As String = "Time should be minimum 106 minutes (2nd Extra Time)!"
    Dim strmessage16 As String = "Time should be minimum 91 minutes (1st Extra Time)!"
    Dim strmessage17 As String = "Time should be minimum 46 minutes (2nd Half)!"
    Dim strmessage18 As String = "Please enter valid time !"
    Dim strmessage19 As String = "Please select the team !"
    Dim strmessage20 As String = "Incorrect time - current event time"
    Dim strmessage21 As String = "should be greater than the previous event!"
    Dim strMessage22 As String = "Complete or cancel the current operation first!"
    Dim strMessage24 As String = "Insert time should be between TWO EVENTS!"
    Dim strMessage25 As String = "Can't edit Abandone event!"

    'SPANISH TRANSLATION REQUIRED - SHRAVANI
    Dim strTimeEntrynotAllowed As String = "TIME event entry not allowed in EDIT/INSERT mode!"
    Dim strTimeEventValidation As String = "Time should be equal or higher than last entered event time!"
    Dim strEnterTime As String = "Enter the correct time!"

    Dim strTime2ndHalfChk_NonDefault As String = "Time should be minimum 1 minute (2nd Half)!"
    Dim strTime1stExtraTimeChk_NonDefault As String = "Time should be minimum 1 minutes (1st Extra Time)!"
    Dim strTime2ndExtraTimeChk_NonDefault As String = "Time should be minimum 1 minutes (2nd Extra Time)!"

    Dim strmessae23 As String = "This is not the correct place to insert event!"
    Dim strmessae25 As String = "Incorrect time - current event time should be greater than the previous event!"
    Dim strmessae26 As String = "Time entered is too high - please check and correct it!"

#End Region

#Region " Event Handlers "

    Private Sub frmModule2Score_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim dsPBP As New DataSet
            Dim dsEndPeriodCount As DataSet
            Dim EventID As Integer
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmModule1PBP", Nothing, 1, 0)
            'If m_objGameDetails.CurrentPeriod = 0 Then
            '    DisableAllControls(False)
            'End If
            LoadTeam(m_objGameDetails.GameCode)

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnInsert.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnInsert.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnSwitchGames.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSwitchGames.Text)
                lblTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTeam.Text)
                lblTime.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTime.Text)

                For i As Integer = 0 To lvwPBP.Columns.Count - 1
                    lvwPBP.Columns(0).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lvwPBP.Columns(0).Text).Replace(" ", "")

                Next

                'Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Event")
                'Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Period")
                'Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                'Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Time")
                'Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Score")
                'Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "EventCodeID")
                'Dim g10 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "UniqueID")
                'Dim g11 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team_ID")
                'Dim g12 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "SequenceNumber")
                'Dim g13 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "SequenceNumber")

                'lvwPBP.Columns(0).Text = g4
                'lvwPBP.Columns(1).Text = g5
                'lvwPBP.Columns(2).Text = g6
                'lvwPBP.Columns(3).Text = g7
                'lvwPBP.Columns(4).Text = g8
                'lvwPBP.Columns(6).Text = g9
                'lvwPBP.Columns(7).Text = g10
                'lvwPBP.Columns(8).Text = g11.Replace(" ", "")
                'Dim stemp As String = g12.Replace(" ", "")

                'lvwPBP.Columns(9).Text = stemp
                'lvwPBP.Columns(10).Text = g13.Replace(" ", "")
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)
                strmessage8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage8)
                strmessage9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage9)
                strmessage10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage10)
                strmessage11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage11)
                strmessage12 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage12)
                strmessage13 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage13)
                strmessage14 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage14)
                strmessage15 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage15)
                strmessage16 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage17)
                strmessage17 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage17)
                strmessage18 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage18)
                strmessage19 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage19)
                strmessage20 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage20)
                strmessage21 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage21)
                strmessae23 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessae23)
                strMessage24 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strMessage24)
                strmessae25 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessae25)
                strMessage25 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strMessage25)
                strmessae26 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessae26)
            End If

            dsPBP = m_objModule2Score.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            DisplayInListView(dsPBP)

            ''If LAST EVENT ENTERED IS ENDPERIOD - then DISABLE ALL EVENTS EXCEPT LISTVIEW
            If (dsPBP.Tables(0).Rows.Count > 0) Then
                For intRowCount = 0 To dsPBP.Tables(0).Rows.Count - 1
                    EventID = CInt(dsPBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                    Exit For
                Next
            End If

            dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)

            If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = m_objGameDetails.CurrentPeriod Then
                DisableAllControls(False)
                If m_objGameDetails.CurrentPeriod = 0 Then
                    If (dsPBP.Tables(0).Rows.Count > 0) Then
                        lvwPBP.Enabled = True
                    Else
                        lvwPBP.Enabled = False
                    End If

                Else
                    lvwPBP.Enabled = True
                End If
            Else
                DisableAllControls(True)
                lvwPBP.Enabled = True
            End If

            If EventID = clsGameDetails.ENDGAME Then
                DisableAllControls(False)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

    Public Sub DisableAllControls(ByVal Type As Boolean)
        Try
            Dim dsPBP As New DataSet
            dsPBP = m_objModule2Score.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

            If m_objGameDetails.CurrentPeriod = 0 Then
                If (dsPBP.Tables(0).Rows.Count > 0) Then
                    lvwPBP.Enabled = True
                Else
                    lvwPBP.Enabled = False
                End If

                'pnlScore.Enabled = False
            Else
                lvwPBP.Enabled = True
                'pnlScore.Enabled = True
            End If
            pnlScore.Enabled = Type
            btnEdit.Enabled = Type
            btnInsert.Enabled = Type
            btnDelete.Enabled = Type
            btnSwitchGames.Enabled = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub LoadTeam(ByVal GameCode As Integer)
        Try
            Dim dsTeam As New DataSet
            dsTeam = m_objModule2Score.GetTeams(GameCode)

            Dim dtTeam As New DataTable
            dtTeam = dsTeam.Tables(0).Copy
            LoadControl(cmbTeam, dtTeam, "TEM_NAME", "TEAM_ID")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' LOADS THE DATA INTO CONTROL
    ''' </summary>
    ''' <param name="Combo">Name of the Combo Box</param>
    ''' <param name="GameSetup">Name of the Data Table</param>
    ''' <param name="DisplayColumnName">A string represents the Column Name to be displayed</param>
    ''' <param name="DataColumnName">A string represents value of the column</param>
    ''' <remarks></remarks>
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ''' <summary>
    ''' FN FETCHED UNIQUE ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetUniqueID() As Integer
        Try
            Return m_objModule2Score.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub AddPlaybyPlaydata(ByVal EventID As Integer, ByVal Source As String)
        Try
            Dim drPBPData As DataRow
            Dim intTeamID As Integer
            Dim intAwayScore As Integer
            Dim intHomeScore As Integer
            Dim intOldAwayScore As Integer
            Dim dtTimeDiff As DateTime

            m_dsScores = m_objModule2Score.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsScores.Tables(0).Rows.Count > 0 Then
                intTeamID = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                intAwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                intHomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
            End If

            If intTeamID = m_objGameDetails.HomeTeamID Then
                If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        intHomeScore = intHomeScore + 1
                        m_objGameDetails.HomeScore = intHomeScore
                        'm_objGameDetails.AwayScore = intAwayScore
                    End If
                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                Else
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        intAwayScore = intAwayScore + 1
                        m_objGameDetails.AwayScore = intAwayScore
                    End If
                    intOldAwayScore = intAwayScore
                    intAwayScore = intHomeScore
                    intHomeScore = intOldAwayScore
                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                End If
            Else
                If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        intAwayScore = intAwayScore + 1
                        m_objGameDetails.HomeScore = intAwayScore
                        'Else

                    End If
                    intOldAwayScore = intAwayScore
                    intAwayScore = intHomeScore
                    intHomeScore = intOldAwayScore
                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                Else
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        intHomeScore = intHomeScore + 1
                        m_objGameDetails.AwayScore = intHomeScore
                    End If
                    'intOldAwayScore = m_objGameDetails.AwayScore
                    'm_objGameDetails.AwayScore = m_objGameDetails.HomeScore
                    'm_objGameDetails.HomeScore = intOldAwayScore

                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                End If
            End If

            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
            drPBPData("OFFENSE_SCORE") = intHomeScore
            drPBPData("DEFENSE_SCORE") = intAwayScore
            drPBPData("GAME_CODE") = m_objGameDetails.GameCode
            drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drPBPData("UNIQUE_ID") = GetUniqueID()
            drPBPData("SEQUENCE_NUMBER") = Convert.ToDecimal(drPBPData("UNIQUE_ID"))
            If m_objGameDetails.IsDefaultGameClock Then

                Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                If timElap >= 60 Then
                    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore((CInt(txtTimeMod2.Text) * 60) - 30)
                Else
                    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                End If
            Else
                Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                If timElap >= 60 Then
                    drPBPData("TIME_ELAPSED") = (CInt(txtTimeMod2.Text) * 60) - 30
                Else
                    drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                End If
            End If

            ''''''''''''''

            If m_objGameDetails.IsInsertMode Then
                If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                    m_CurrPeriod = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("PERIOD"))
                Else
                    If lvwPBP.SelectedItems.Count > 0 Then
                        m_EventID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(5).Text)
                        m_CurrPeriod = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(1).Text)
                        If m_EventID = clsGameDetails.STARTPERIOD Then
                            m_CurrPeriod = m_CurrPeriod - 1
                        End If
                    Else
                        m_CurrPeriod = m_objGameDetails.CurrentPeriod
                    End If
                    drPBPData("PERIOD") = m_CurrPeriod
                End If
            Else
                drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
            End If

            '''''''''''''

            'drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
            'drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod

            drPBPData("EVENT_CODE_ID") = EventID

            dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
            drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

            'drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(Dt, DateTimeHelper.OutputFormat.CurrentFormat)
            drPBPData("TEAM_ID") = cmbTeam.SelectedValue
            drPBPData("OPTICAL_TIMESTAMP") = DBNull.Value
            drPBPData("RECORD_EDITED") = "N"
            drPBPData("EDIT_UID") = 0
            drPBPData("PBP_STRING") = DBNull.Value
            drPBPData("MULTILINGUAL_PBP_STRING") = DBNull.Value
            drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
            drPBPData("PROCESSED") = "N"
            drPBPData("CONTINUATION") = "F"

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                drPBPData("DEMO_DATA") = "Y"
            Else
                drPBPData("DEMO_DATA") = "N"
            End If
            m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' sepcific function for start period, end period , game start etc
    ''' </summary>
    ''' <param name="EventID"></param>
    ''' <param name="Source"></param>
    ''' <remarks></remarks>
    Public Sub AddPBPGeneralEvents(ByVal EventID As Integer, ByVal Source As String)
        Try
            Dim drPBPData As DataRow
            Dim intTeamID As Integer
            Dim intAwayScore As Integer
            Dim intHomeScore As Integer
            Dim strTimeElapsed As Integer
            Dim dtTimeDiff As DateTime

            'To get the latest Score of the Teams
            m_dsScores = m_objModule2Score.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsScores.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                    intTeamID = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                End If

                intAwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                intHomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
            End If

            'Common inputs

            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
            drPBPData("GAME_CODE") = m_objGameDetails.GameCode
            drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drPBPData("UNIQUE_ID") = m_objModule2Score.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            drPBPData("SEQUENCE_NUMBER") = drPBPData("UNIQUE_ID")
            drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
            dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
            drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

            'drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(Dt, DateTimeHelper.OutputFormat.CurrentFormat)
            drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
            drPBPData("RECORD_EDITED") = "N"
            drPBPData("EDIT_UID") = 0
            drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
            drPBPData("PROCESSED") = "N"
            drPBPData("CONTINUATION") = "F"
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                drPBPData("DEMO_DATA") = "Y"
            Else
                drPBPData("DEMO_DATA") = "N"
            End If
            drPBPData("EVENT_CODE_ID") = EventID
            drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
            drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore

            'm_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)

            Select Case EventID

                Case clsGameDetails.ABANDONED
                    'If EventID = clsGameDetails.ABANDONED Then

                    If m_objGameDetails.IsDefaultGameClock Then
                        Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                        If timElap >= 60 Then
                            drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore((CInt(txtTimeMod2.Text) * 60) - 30)
                        Else
                            drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                        End If
                        'drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                    Else
                        Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                        If timElap >= 60 Then
                            drPBPData("TIME_ELAPSED") = (CInt(txtTimeMod2.Text) * 60) - 30
                        Else
                            drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                        End If
                        'drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                    End If
                    drPBPData("COMMENTS") = m_objGameDetails.Reason
                    drPBPData("EVENT_CODE_ID") = EventID
                    drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                    drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                    txtTimeMod2.Text = ""
                    m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                    'End If
                Case clsGameDetails.STARTPERIOD, clsGameDetails.ENDPERIOD, clsGameDetails.ENDGAME, clsGameDetails.GAMESTART
                    If EventID = clsGameDetails.ENDGAME Then
                        'Dim DsLastEventDetails As DataSet
                        'DsLastEventDetails = m_objModule2Score.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        'drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED"))

                        ''TOSOCRS-139 : Time_Elapsed for Half Over and Game Over Events for Tier 1, 2, and 3.
                        drPBPData("TIME_ELAPSED") = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

                        'If m_objGameDetails.CurrentPeriod = 0 Then
                        '    drPBPData("TIME_ELAPSED") = 0
                        'Else
                        '    If m_objGameDetails.IsDefaultGameClock Then
                        '        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                        '    Else
                        '        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                        '    End If
                        'End If

                    ElseIf EventID = clsGameDetails.GAMESTART Then
                        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                    ElseIf EventID = clsGameDetails.ENDPERIOD Then

                        Dim CurrentMainClockTime As Integer
                        If m_objGameDetails.IsDefaultGameClock Then
                            CurrentMainClockTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                        Else
                            CurrentMainClockTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                        End If

                        'If m_objGameDetails.ModuleID = 2 Then
                        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 2 Then

                            drPBPData("TIME_ELAPSED") = FIRSTHALF
                            'Dim DsLastEventDetails As DataSet
                            Dim currTime As Integer
                            '***********
                            'DsLastEventDetails = m_objModule2Score.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            'If m_objGameDetails.ModuleID = 1 Then
                            '    currTime = CurrentMainClockTime
                            'Else
                            '    currTime = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                            'End If
                            '**************
                            'currTime = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                            ''drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30

                            ''TOSOCRS-139 : Time_Elapsed for Half Over and Game Over Events for Tier 1, 2, and 3.
                            currTime = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If currTime < FIRSTHALF Then
                                'Set the Main Clock to FIRST HALF
                                If m_objGameDetails.IsDefaultGameClock Then
                                    If m_objGameDetails.CurrentPeriod = 1 Then
                                        frmMain.UdcRunningClock1.URCSetTime(45, 0, True)
                                    Else
                                        frmMain.UdcRunningClock1.URCSetTime(90, 0, True)
                                    End If
                                Else
                                    frmMain.UdcRunningClock1.URCSetTime(45, 0, True)
                                End If
                                drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                            Else
                                'drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 90
                                ''TOSOCRS-139 : Time_Elapsed for Half Over and Game Over Events for Tier 1, 2, and 3.
                                drPBPData("TIME_ELAPSED") = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            End If
                        ElseIf m_objGameDetails.CurrentPeriod = 3 Or m_objGameDetails.CurrentPeriod = 4 Then
                            drPBPData("TIME_ELAPSED") = FIRSTHALF
                            '***********
                            'Dim DsLastEventDetails As DataSet
                            'DsLastEventDetails = m_objModule2Score.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            'If m_objGameDetails.ModuleID = 1 Then
                            '    currTime = CurrentMainClockTime
                            'Else
                            '    currTime = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                            'End If
                            '***********
                            'currTime = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                            ''drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30

                            ''TOSOCRS-139 : Time_Elapsed for Half Over and Game Over Events for Tier 1, 2, and 3.
                            Dim currTime As Integer
                            currTime = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If currTime < EXTRATIME Then
                                'Set the Main Clock to FIRST HALF
                                If m_objGameDetails.IsDefaultGameClock Then
                                    If m_objGameDetails.CurrentPeriod = 3 Then
                                        frmMain.UdcRunningClock1.URCSetTime(105, 0, True)
                                    Else
                                        frmMain.UdcRunningClock1.URCSetTime(120, 0, True)
                                    End If
                                Else
                                    frmMain.UdcRunningClock1.URCSetTime(15, 0, True)
                                End If
                                drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                            Else
                                'drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 90
                                ''TOSOCRS-139 : Time_Elapsed for Half Over and Game Over Events for Tier 1, 2, and 3.
                                drPBPData("TIME_ELAPSED") = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            End If
                        End If
                    Else
                        strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))

                        If m_objGameDetails.CurrentPeriod = 1 Then
                            drPBPData("TIME_ELAPSED") = strTimeElapsed
                        Else
                            drPBPData("TIME_ELAPSED") = 0
                        End If
                    End If
                    drPBPData("EVENT_CODE_ID") = EventID
                    drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                    drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore

                    m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                Case clsGameDetails.TIME

                    If m_objGameDetails.IsDefaultGameClock Then
                        Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                        If timElap >= 60 Then
                            drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore((CInt(txtTimeMod2.Text) * 60) - 30)
                        Else
                            drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                        End If
                    Else
                        Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                        If timElap >= 60 Then
                            drPBPData("TIME_ELAPSED") = (CInt(txtTimeMod2.Text) * 60) - 30
                        Else
                            drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                        End If
                    End If

                    drPBPData("EVENT_CODE_ID") = EventID
                    drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                    drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore

                    m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)

                Case clsGameDetails.COMMENTS, clsGameDetails.DELAYED
                    If EventID = clsGameDetails.COMMENTS Then
                        If m_objGameDetails.OffensiveTeamID = -1 Then
                            drPBPData("TEAM_ID") = DBNull.Value
                        Else
                            drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                        End If
                    Else
                        If intTeamID <> 0 Then
                            drPBPData("TEAM_ID") = intTeamID
                        Else
                            drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                        End If
                    End If

                    'Dim TimeElapsed As Integer = m_objModule2Score.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    'Dim EditedTime As Integer

                    'If m_objGameDetails.IsDefaultGameClock Then
                    '    EditedTime = CalculateTimeElapsedBefore(m_objGameDetails.CommentsTime)
                    'Else
                    '    EditedTime = m_objGameDetails.CommentsTime
                    'End If

                    Dim TimeElapsed As Integer = m_objModule2Score.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    Dim EditedTime As Integer

                    If m_objGameDetails.IsDefaultGameClock Then
                        If m_objGameDetails.CommentsTime < 30 Then
                            EditedTime = CalculateTimeElapsedBefore(m_objGameDetails.CommentsTime)
                        Else
                            EditedTime = CalculateTimeElapsedBefore(m_objGameDetails.CommentsTime - 30)
                        End If

                    Else
                        EditedTime = m_objGameDetails.CommentsTime
                    End If

                    If EditedTime < TimeElapsed Then 'TIme EDIT
                        Dim DsPBPDataSet As DataSet
                        'Dim currentTime As Integer
                        'currentTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text)))

                        DsPBPDataSet = m_objModule2Score.GetPBPRecordBasedOnTime(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, EditedTime, m_objGameDetails.CurrentPeriod, "NEXT")
                        'highlight the record
                        Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(DsPBPDataSet.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")))
                        lvwPBP.Items(Lstview.Index).Selected = True
                        lvwPBP.Select()
                        lvwPBP.EnsureVisible(Lstview.Index)
                        m_TimeEditMode = True
                    End If
                    drPBPData("TIME_ELAPSED") = EditedTime
                    drPBPData("EVENT_CODE_ID") = EventID
                    Dim intOldAwayScore As Integer
                    If intTeamID = m_objGameDetails.HomeTeamID Then
                        If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                            drPBPData("DEFENSE_SCORE") = intAwayScore
                            drPBPData("OFFENSE_SCORE") = intHomeScore
                        Else
                            intOldAwayScore = intAwayScore
                            intAwayScore = intHomeScore
                            intHomeScore = intOldAwayScore
                            drPBPData("DEFENSE_SCORE") = intAwayScore
                            drPBPData("OFFENSE_SCORE") = intHomeScore
                        End If
                    Else
                        If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                            intOldAwayScore = intAwayScore
                            intAwayScore = intHomeScore
                            intHomeScore = intOldAwayScore
                            drPBPData("DEFENSE_SCORE") = intAwayScore
                            drPBPData("OFFENSE_SCORE") = intHomeScore
                        Else
                            drPBPData("DEFENSE_SCORE") = intAwayScore
                            drPBPData("OFFENSE_SCORE") = intHomeScore
                        End If
                    End If
                    If EventID = clsGameDetails.COMMENTS Or EventID = clsGameDetails.DELAYED Then
                        drPBPData("COMMENTS") = m_objGameDetails.CommentData
                    Else
                        drPBPData("COMMENTS") = m_objGameDetails.Reason
                    End If
                    m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                    '
            End Select

            Call InsertPlaybyPlayData()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer) As Integer
        Try

            If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 0 Then
                Return CurrentElapsedTime
            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF
            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA
            ElseIf m_objGameDetails.CurrentPeriod = 5 Then
                Return CurrentElapsedTime
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' FUNCTION TO CONVERT THE PBP DATA INTO XML AND SAVES TO LOCAL SQL
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertPlaybyPlayData() As Boolean
        Try
            'code to save the data into SQL
            Dim dsTempPBPData As New DataSet

            m_objGameDetails.PBP.DataSetName = "SoccerEventData"
            dsTempPBPData = m_objGameDetails.PBP
            Dim strPBPXMLData As String = dsTempPBPData.GetXml()
            Dim dsPBP As New DataSet

            'dsPBP = m_objModule2Score.InsertPBPDataToSQL(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_TimeEditMode, m_objGameDetails.IsInsertMode, m_intSequenceNumber, m_objGameDetails.languageid)

            dsPBP = m_objModule2Score.InsertPBPDataToSQL(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_TimeEditMode, m_objGameDetails.IsInsertMode, m_intSequenceNumber, m_objGameDetails.IsFoulAssociated, m_objGameDetails.languageid)

            If dsPBP.Tables(0).Rows.Count > 0 Then
                'start 26-11-13
                frmModule1PBP.IsRecordsProcessing(dsPBP.Tables(0))
                'end 26-11-13
            End If

            Dim DRS() As DataRow = m_objGameDetails.PBP.Tables(0).Select("EVENT_CODE_ID NOT IN (54,55,56,57)")
            If DRS.Length > 0 Then
                'Display in Listview taking from the live_soc_pbp table after the insertion of the event
                DisplayInListView(dsPBP)
            End If

            setScoreforTeam()

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 1)
            DoCancel()
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        Finally
            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next
        End Try
    End Function

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="PBP"></param>
    ''' <remarks></remarks>
    Public Sub DisplayInListView(ByVal PBP As DataSet, Optional ByVal IsSportVU As Boolean = False)
        Try

            If lvwPBP.Items.Count > 0 Then
                lvwPBP.Items.Clear()
            End If
            Dim intRowCount As Integer
            Dim EventID As Integer
            Dim CurrPeriod As Integer
            Dim ElapsedTime As Integer
            Dim StrTime As String = "00:00"
            Dim TeamID As Integer
            PBP = NumberingPairedEvent(PBP)
            If Not PBP Is Nothing Then
                If (PBP.Tables(0).Rows.Count > 0) Then

                    For intRowCount = 0 To PBP.Tables(0).Rows.Count - 1
                        'If CInt(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) <> clsGameDetails.TIME Then
                        If Not IsDBNull(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID")) Then
                            TeamID = CInt(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID"))
                        End If

                        EventID = CInt(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                        CurrPeriod = CInt(PBP.Tables(0).Rows(intRowCount).Item("PERIOD"))

                        If EventID = clsGameDetails.TIME Then
                            Dim lvpbp As New ListViewItem("TIME")
                            lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                            ''
                            lvpbp.SubItems.Add("")
                            If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                If ElapsedTime > 0 Then
                                    ElapsedTime = ElapsedTime + 30
                                End If
                                StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                            End If

                            Dim strCurrTime() As String
                            strCurrTime = StrTime.Split(CChar(":"))
                            lvpbp.SubItems.Add(strCurrTime(0))
                            lvpbp.SubItems.Add("")
                            lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                            lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                            lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                            lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                            lvpbp.SubItems.Add("")

                            lvwPBP.Items.Add(lvpbp)

                            '''''
                            'ElseIf EventID = clsGameDetails.SHOOTOUT_GOAL Then
                            '    Dim lvpbp As New ListViewItem("Shootout")
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                            '    If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                            '        'CONVERTING TO MINUTES
                            '        ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                            '        StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                            '    End If
                            '    Dim strCurrTime() As String
                            '    strCurrTime = StrTime.Split(CChar(":"))
                            '    lvpbp.SubItems.Add(strCurrTime(0))
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSE_SCORE").ToString & "-" & PBP.Tables(0).Rows(intRowCount).Item("DEFENSE_SCORE").ToString)
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                            '    lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                            '    lvpbp.SubItems.Add("")
                            '    lvwPBP.Items.Add(lvpbp)

                        Else
                            If EventID <> clsGameDetails.SHOOTOUT_GOAL Then
                                Dim lvpbp As New ListViewItem(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_DESC").ToString)

                                lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                                lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                                If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                                    ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                                    'If ElapsedTime > 0 Then
                                    '    ElapsedTime = ElapsedTime + 30
                                    'End If
                                    If (EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.OWNGOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.DELAYED) And m_objGameDetails.ModuleID = 2 And ElapsedTime > 0 Then
                                        ElapsedTime = ElapsedTime + 30
                                    End If
                                    If (EventID = clsGameDetails.ENDGAME Or EventID = clsGameDetails.ENDPERIOD) And m_objGameDetails.ModuleID = 2 Then
                                        ''TOSOCRS-139 : Time_Elapsed for Half Over and Game Over Events for Tier 1, 2, and 3.
                                        If ElapsedTime > 0 Then
                                            ElapsedTime = ElapsedTime + 30
                                        End If
                                        'If m_objGameDetails.IsDefaultGameClock Then
                                        '    If CurrPeriod = 1 Then
                                        '        StrTime = "45"
                                        '    ElseIf CurrPeriod = 2 Then
                                        '        StrTime = "90"
                                        '    ElseIf CurrPeriod = 3 Then
                                        '        StrTime = "105"
                                        '    ElseIf CurrPeriod = 4 Then
                                        '        StrTime = "120"
                                        '    Else
                                        '        StrTime = CStr(ElapsedTime / 60)
                                        '    End If
                                        'Else
                                        '    If CurrPeriod = 1 Or CurrPeriod = 2 Then
                                        '        StrTime = "45"
                                        '    ElseIf CurrPeriod = 3 Or CurrPeriod = 4 Then
                                        '        StrTime = "15"
                                        '    Else
                                        '        StrTime = CStr(ElapsedTime / 60)
                                        '    End If
                                        'End If
                                        If m_objGameDetails.IsDefaultGameClock Then

                                            StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                            'If CurrPeriod = 1 Then
                                            '    StrTime = "45"
                                            'ElseIf CurrPeriod = 2 Then
                                            '    StrTime = "90"
                                            'ElseIf CurrPeriod = 3 Then
                                            '    StrTime = "105"
                                            'ElseIf CurrPeriod = 4 Then
                                            '    StrTime = "120"
                                            'Else
                                            '    StrTime = CStr(ElapsedTime / 60)
                                            'End If
                                        Else
                                            'If CurrPeriod = 1 Or CurrPeriod = 2 Then
                                            '    StrTime = "45"
                                            'ElseIf CurrPeriod = 3 Or CurrPeriod = 4 Then
                                            '    StrTime = "15"
                                            'Else
                                            StrTime = CStr(ElapsedTime / 60)
                                            'End If
                                        End If
                                    Else
                                        StrTime = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                    End If
                                    Dim strCurrTime() As String
                                    strCurrTime = StrTime.Split(CChar(":"))
                                    lvpbp.SubItems.Add(strCurrTime(0))
                                End If
                                Select Case EventID
                                    Case clsGameDetails.GAMESTART, clsGameDetails.STARTPERIOD, clsGameDetails.ENDPERIOD, clsGameDetails.ENDGAME, clsGameDetails.DELAYED
                                        lvpbp.SubItems.Add("")
                                    Case clsGameDetails.GOAL
                                        lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSE_SCORE").ToString & "-" & PBP.Tables(0).Rows(intRowCount).Item("DEFENSE_SCORE").ToString)
                                    Case clsGameDetails.COMMENTS
                                        lvpbp.SubItems.Add("")
                                    Case clsGameDetails.ABANDONED
                                        lvpbp.SubItems.Add("")

                                End Select
                                lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID").ToString)
                                lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("UNIQUE_ID").ToString)
                                lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ID").ToString)
                                lvpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                                lvpbp.SubItems.Add("")

                                lvwPBP.Items.Add(lvpbp)
                                lvwPBP.EndUpdate()
                            End If
                        End If
                    Next

                    If CInt(PBP.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = clsGameDetails.ENDPERIOD Then
                        DisableAllControls(False)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If IsSportVU = True Then
                btnCancel_Click(Nothing, Nothing)
            End If
        End Try

    End Sub

    Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            Dim StrTime As String = "00:00"
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "45+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                        'IIf(DiffSec.ToString.Length = 1, "0", CStr(DiffSec))
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)

                        'DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        'StrTime = "90+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "90+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "90+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 3 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        'StrTime = "105+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "105+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "105+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 4 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        'StrTime = "120+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "120+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "120+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 5 Then
                    StrTime = "120"
                End If
                'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
            Else
                If CurrPeriod = 1 Or CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "45+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                        'StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

                    End If
                Else
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffSec.ToString.Length = 1 Then
                            StrTime = "15+" + CStr(DiffTime) + ":" + ("0" + CStr(DiffSec))
                        Else
                            StrTime = "15+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                        End If
                        'StrTime = "15+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                End If
            End If
            Return StrTime
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub setScoreforTeam()
        Try
            m_dsScores.Tables.Clear()
            m_dsScores = m_objModule2Score.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsScores.Tables(0).Rows.Count > 0 Then
                If CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                    m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                Else
                    m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                End If
                m_objGameDetails.CurrentPeriod = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("PERIOD"))

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then

                If CurrPeriod = 1 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 2 Then
                    'Dim Dval As Decimal
                    'Dval = CDec((FIRSTHALF + ElapsedTime) / 60)
                    'Return Dval.ToString("d2")
                    Return CStr(CInt((FIRSTHALF + ElapsedTime) / 60))
                ElseIf CurrPeriod = 3 Then
                    Return CStr(CInt((SECONDHALF + ElapsedTime) / 60))
                    'clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return CStr(CInt((FIRSTEXTRA + ElapsedTime) / 60))
                    'clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If

            If m_objGameDetails.IsDefaultGameClock = False And m_objGameDetails.ModuleID = 2 Then
                Return CStr(CInt(ElapsedTime / 60))
            End If

            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim lvItem As New ListViewItem
            Dim dsScores As New DataSet
            Dim EditedTime As Integer
            m_CurrentEventCode = clsGameDetails.GOAL
            m_objGameDetails.OffensiveTeamID = CInt(cmbTeam.SelectedValue)
            Dim strTime As String
            If Not ValidateSelection() Then
                Exit Sub
            End If

            'If m_allowSave = False Then
            '    checkForSave()
            '    Exit Try
            'End If

            'comm by shirley on july 29 2010
            'strTime = clsUtility.ConvertSecondToMinute(CInt(txtTimeMod2.Text) * 60, False)
            'strTime = txtTimeMod2.Text.Replace(":", "").PadLeft(5, CChar(" "))
            'EditedTime = CInt(txtTimeMod2.Text) * 60
            'added on on july 29 2010
            If m_objGameDetails.IsDefaultGameClock Then
                Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                'If timElap >= 60 Then
                '    EditedTime = CalculateTimeElapsedBefore((CInt(txtTimeMod2.Text) * 60) - 30)
                'Else
                '    EditedTime = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                'End If

                If timElap >= 60 Then
                    EditedTime = (CInt(txtTimeMod2.Text) * 60) - 30
                Else
                    EditedTime = CInt(txtTimeMod2.Text) * 60
                End If
                'If m_objGameDetails.IsDefaultGameClock Then
                '    EditedTime = CalculateTimeElapsedInsert(CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text)), m_CurrPeriod)
                'Else
                '    EditedTime = CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text))
                'End If

            Else
                Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                If timElap >= 60 Then
                    EditedTime = (CInt(txtTimeMod2.Text) * 60) - 30
                Else
                    EditedTime = CInt(txtTimeMod2.Text) * 60
                End If
            End If

            If m_objGameDetails.IsEditMode = True Then
                If IsTimeEdited(EditedTime) = True Then
                    m_TimeEditMode = True
                Else
                    m_TimeEditMode = False
                End If
            Else

                If m_objGameDetails.IsInsertMode And m_TimeEditMode = False Then
                    If (EditedTime > m_NextElapsedTime Or EditedTime < m_PrevElapsedTime) And m_NextEventCode <> clsGameDetails.STARTPERIOD Then
                        If m_objGameDetails.IsInsertMode And m_NextEventCode = clsGameDetails.ENDPERIOD Then
                            If EditedTime > m_NextElapsedTime Then
                                If EditedTime < m_PrevElapsedTime Then
                                    MessageDialog.Show(strmessae23, "Insert", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If
                            Else
                                If EditedTime < m_PrevElapsedTime Then
                                    MessageDialog.Show(strmessae23, "Insert", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If
                            End If
                        Else
                            MessageDialog.Show(strMessage24, "Insert", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Exit Sub
                        End If
                    ElseIf EditedTime < m_PrevElapsedTime And m_NextEventCode = clsGameDetails.STARTPERIOD Then
                        MessageDialog.Show(strmessae23, "Insert", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Exit Sub
                    End If
                Else

                    Dim TimeElapsed As Integer = m_objModule2Score.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    'If m_objGameDetails.IsDefaultGameClock Then
                    '    strTime = CalculateTimeElapseAfter(TimeElapsed, m_objGameDetails.CurrentPeriod)
                    '    TimeElapsed = CInt(strTime) * 60
                    'End If

                    'If m_objGameDetails.IsDefaultGameClock Then
                    '    strTime = CalculateTimeElapseAfter(TimeElapsed, m_objGameDetails.CurrentPeriod)
                    '    If m_objGameDetails.ModuleID = 1 Then
                    '        TimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(strTime))
                    '    Else
                    '        TimeElapsed = CInt(strTime) * 60
                    '    End If

                    'End If
                    If EditedTime < TimeElapsed Then
                        If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then

                            Dim edtTime As Integer = EditedTime
                            If m_objGameDetails.IsDefaultGameClock Then
                                edtTime = CalculateTimeElapsedBefore(EditedTime)
                            End If
                            If edtTime < CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("TIME_ELAPSED")) Then
                                MessageDialog.Show(strmessae25, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Exit Sub
                            End If
                        End If

                        If m_TimeEditMode = False Then
                            m_TimeEditMode = True
                        End If

                    Else
                        If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then

                            Dim edtTime As Integer = EditedTime
                            If m_objGameDetails.IsDefaultGameClock Then
                                edtTime = CalculateTimeElapsedBefore(EditedTime)
                            End If
                            If edtTime < CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("TIME_ELAPSED")) Then
                                MessageDialog.Show(strmessae25, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            End If

            '--------------------insertion of new records (Normal Mode)-------------------------------

            If m_CurrentEventCode <> 0 Then
                If m_objGameDetails.IsEditMode = True Then
                    AddPlayByPlayData_Edit(m_CurrentEventCode)
                    Call InsertPlayByPlayData_Edit(m_CurrentEventCode)
                Else
                    Call AddPlaybyPlaydata(m_CurrentEventCode, "SAVE")
                    Call InsertPlaybyPlayData()
                End If
            End If
            'insertes into local sql

            DoCancel()
            frmMain.GetPenaltyShootoutScore()
            m_TimeEditMode = False
            m_objGameDetails.IsInsertMode = False
            m_objGameDetails.IsEditMode = False
            m_intSequenceNumber = 0
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function ValidateSelection() As Boolean
        Try
            If Convert.ToInt32(cmbTeam.SelectedValue) = 0 Then
                MessageDialog.Show(strmessage19, "Save", MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            ElseIf txtTimeMod2.Text = "" Then
                MessageDialog.Show(strmessage18, "Save", MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If
            Dim currTime1 As Integer
            currTime1 = CInt(txtTimeMod2.Text) * 60
            If currTime1.ToString.Length() > 4 Then
                MessageDialog.Show(strmessae26, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If
            If m_objGameDetails.IsEditMode = False And m_objGameDetails.IsInsertMode = False Then
                If m_objGameDetails.IsDefaultGameClock Then
                    'validate clock period 2 should start from 45, period 3 should start from 90
                    'period 4 should start from 105
                    If txtTimeMod2.Text.Trim <> "" Then
                        If m_objGameDetails.CurrentPeriod = 2 And Convert.ToInt32(txtTimeMod2.Text) < 46 Then
                            MessageDialog.Show(strmessage17, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 91 And m_objGameDetails.CurrentPeriod = 3 Then
                            MessageDialog.Show(strmessage16, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 106 And m_objGameDetails.CurrentPeriod = 4 Then
                            MessageDialog.Show(strmessage15, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If

                    End If
                Else
                    If txtTimeMod2.Text.Trim <> "" Then
                        If m_objGameDetails.CurrentPeriod = 2 And Convert.ToInt32(txtTimeMod2.Text) < 1 Then
                            MessageDialog.Show(strTime2ndHalfChk_NonDefault, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 1 And m_objGameDetails.CurrentPeriod = 3 Then
                            MessageDialog.Show(strTime1stExtraTimeChk_NonDefault, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 1 And m_objGameDetails.CurrentPeriod = 4 Then
                            MessageDialog.Show(strTime2ndExtraTimeChk_NonDefault, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                    End If
                End If
            ElseIf m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Then
                If m_objGameDetails.IsDefaultGameClock Then
                    If txtTimeMod2.Text.Trim <> "" Then
                        If m_CurrPeriod = 2 And Convert.ToInt32(txtTimeMod2.Text) < 46 Then
                            MessageDialog.Show(strmessage17, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 91 And m_CurrPeriod = 3 Then
                            MessageDialog.Show(strmessage16, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 106 And m_CurrPeriod = 4 Then
                            MessageDialog.Show(strmessage15, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                    End If
                Else
                    If txtTimeMod2.Text.Trim <> "" Then
                        If m_objGameDetails.CurrentPeriod = 2 And Convert.ToInt32(txtTimeMod2.Text) < 1 Then
                            MessageDialog.Show(strTime2ndHalfChk_NonDefault, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 1 And m_objGameDetails.CurrentPeriod = 3 Then
                            MessageDialog.Show(strTime1stExtraTimeChk_NonDefault, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                        If Convert.ToInt32(txtTimeMod2.Text) < 1 And m_objGameDetails.CurrentPeriod = 4 Then
                            MessageDialog.Show(strTime2ndExtraTimeChk_NonDefault, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If

                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    'Private Sub checkForSave()
    '    Try
    '        If IsPBPEntryAllowed() = False Then
    '            m_allowSave = False
    '            Exit Try
    '        End If
    '        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsReplaceMode Then
    '            Call InsertPlayByPlayData_Edit(m_EventID)
    '            'clears the resouces
    '            m_TimeEditMode = False
    '            UdcSoccerField1.USFClearMarks()
    '            m_PlayerInID = 0
    '            m_PlayerOutID = 0
    '            DoCancel()
    '            ClearDataPointsAfterSave()
    '            m_objGameDetails.IsPairedEvent = False
    '            'essageDialog.Show("Record Updated Successfully...", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '            ExpandSavedEventsBox(True)
    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, Nothing, Nothing, Nothing, 0, 1)
    '        Else
    '            Call InsertPlaybyPlayData()
    '            m_addDirectEvent = False
    '            ExpandSavedEventsBox(True)
    '        End If
    '        DoCancel()
    '        ClearDataPointsAfterSave()

    '        btnSave.Enabled = False
    '        m_TimeEditMode = False
    '        m_objGameDetails.IsInsertMode = False
    '        m_intSequenceNumber = 0
    '        m_allowSave = True
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    Private Sub btnSwitchGames_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSwitchGames.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSwitchGames.Text, Nothing, 1, 0)
            Dim objSelectGame As New frmSelectGame
            If m_objGameDetails.IsInsertMode Or m_objGameDetails.IsEditMode Then
                MessageDialog.Show(strMessage22, "Switch", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If objSelectGame.ShowDialog() = Windows.Forms.DialogResult.OK Then
                FillSelectedGameDetails()
                m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
            End If
            'RM 8028 CPU Spike when switching between games..
            GC.Collect()

            If m_objGameDetails.CoverageLevel = 2 Or m_objGameDetails.CoverageLevel = 1 Then
                frmMain.EnableMenuItem("RevertSubsStripMenuItem", False)
            Else
                frmMain.EnableMenuItem("RevertSubsStripMenuItem", True)
                frmMain.EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", True)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' SETS THE PROPERTY FOR THE MODULE 2 GAME SELECTED IN THE MAIN SCREEN
    ''' </summary>
    ''' <remarks></remarks>
    '''
    Public Sub FillSelectedGameDetails()
        Try
            BindMod2MultipleGames()
            For i As Integer = 0 To m_dsMultiplegames.Tables(0).Rows.Count - 1
                If CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE")) = m_objGameDetails.GameCode Then
                    Dim intOldGameCode As Integer = m_objGameDetails.GameLastPlayed
                    m_objGameDetails.GameCode = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE").ToString)
                    m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(m_dsMultiplegames.Tables(0).Rows(i).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                    m_objGameDetails.ModuleID = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("MODULE_ID").ToString)
                    m_objGameDetails.CoverageLevel = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("TIER").ToString)
                    m_objGameDetails.AwayTeamID = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAMID").ToString)
                    m_objGameDetails.AwayTeamAbbrev = m_dsMultiplegames.Tables(0).Rows(i).Item("AWAYABBREV").ToString
                    m_objGameDetails.AwayTeam = m_dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAM").ToString
                    m_objGameDetails.HomeTeamID = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAMID").ToString)
                    m_objGameDetails.HomeTeamAbbrev = m_dsMultiplegames.Tables(0).Rows(i).Item("HOMEABBREV").ToString
                    m_objGameDetails.HomeTeam = m_dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAM").ToString
                    m_objGameDetails.LeagueID = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("LEAGUE_ID").ToString)
                    m_objGameDetails.LeagueName = m_dsMultiplegames.Tables(0).Rows(i).Item("Competition").ToString

                    If m_objGameDetails.CoverageLevel = 1 Then
                    Else
                        If m_dsMultiplegames.Tables(0).Rows(i).Item("FIELD_ID") IsNot DBNull.Value Then
                            m_objGameDetails.FieldID = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("FIELD_ID").ToString)
                        End If
                    End If
                    'm_objGameDetails.FieldName = dsMultiplegames.Tables(0).Rows(i).Item("FIELDNAME").ToString
                    m_objGameDetails.FeedNumber = CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("FEEDNUMBER").ToString)
                    m_objGameDetails.ReporterRoleDisplay = CStr(IIf(CInt(m_dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO").ToString) = 1, "MAIN", "ASSISTER"))
                    m_objGameDetails.ReporterRole = Convert.ToString(m_dsMultiplegames.Tables(0).Rows(i).Item("REPORTER_ROLE")) & Convert.ToInt32(m_dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))
                    m_objGameDetails.SerialNo = Convert.ToInt32(m_dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))

                    'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                    Dim intTimeElapsed As Integer = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                    m_objModule2Score.InsertCurrentGame(m_objGameDetails.GameCode, intOldGameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, intTimeElapsed, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")

                    If m_objLoginDetails.UserType <> clsLoginDetails.m_enmUserType.OPS And m_objGameDetails.CoverageLevel < 3 Then
                        m_objGeneral.InsertTeamGameForLowTiers(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                    End If

                    If m_objGameDetails.CoverageLevel = 1 Then
                        frmMain.SetMainFormSize()
                        frmMain.FillFormControls()
                        LoadTeam(m_objGameDetails.GameCode)
                        frmMain.ResetClockAndScores()
                        frmMain.SetMenuItemsforModule2Score()
                        'FillListView()
                        frmMain.SetTeamColor(True, Color.Empty)
                        frmMain.SetTeamColor(False, Color.Empty)
                        frmMain.SetChildForm()
                    Else
                        'frmMain.SetChildForm()
                        frmMain.SetMainFormSize()
                        'PERIOD,SCORES,TEAM NAMES DISPLAY IS DONE IN THIS FUNCTION
                        frmMain.FillFormControls()
                        frmMain.SetAllMenuItemsVisbleFalse()
                        frmMain.SetMainFormMenu()
                        frmMain.ResetClockAndScores()
                        frmMain.SetChildForm()

                        If Not frmMain.m_objModule1Main Is Nothing Then
                            frmMain.m_objModule1Main.ClearMainScreenControls()
                            'getting the current players for the selected game
                            frmMain.m_objModule1Main.GetCurrentPlayersAfterRestart()

                            frmMain.m_objModule1Main.SortPlayers()
                            'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
                            frmMain.m_objModule1Main.LabelHomeTeamTagged()
                            frmMain.m_objModule1Main.LabelAwayTeamTagged()
                            'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
                            frmMain.m_objModule1Main.FetchMainScreenPBPEvents()
                            'GAME SETUP INFO
                            frmMain.m_objModule1Main.InsertOfficialAndMatchInfo()
                            frmMain.m_objModule1Main.SetTeamButtonColor()
                            frmMain.m_objModule1Main.UpdateManager()

                        End If

                        If m_objGameDetails.CoverageLevel = 2 Or m_objGameDetails.CoverageLevel = 1 Then
                            frmMain.lklHometeam.Links(0).Enabled = False
                            frmMain.lklAwayteam.Links(0).Enabled = False
                        Else
                            frmMain.lklAwayteam.Links(0).Enabled = True
                            frmMain.lklHometeam.Links(0).Enabled = True
                        End If
                        m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)

                        If Not frmMain.m_objModule1PBP Is Nothing Then
                            frmMain.m_objModule1PBP.GetCurrentPlayersAfterRestart()
                            frmMain.m_objModule1PBP.SetTeamButtonColor()
                        End If
                        frmMain.GetPenaltyShootoutScore()
                    End If
                    Exit For
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub BindGames()
    '    Try
    '        m_dsGames = m_objModule2Score.GetGameDetails(m_objLoginDetails.LanguageID, 1)
    '        If Not m_dsGames Is Nothing Then
    '            If (m_dsGames.Tables(0).Rows.Count > 0) Then
    '            End If
    '        Else
    '            essageDialog.Show("Currently there is no game(s) available", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    ''' <summary>
    ''' LOADS MODULE2 GAMES ASSIGNED TO THE REPORTER (GAMES WITH +/- 24 HRS IS TAKEN INTO CONSIDERATION)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindMod2MultipleGames()
        Try
            'GETS MODULE 2 GAMES ASSIGNED TO THAT REPORTER (GAME DATE IS THE GAME DATE SELECTED FROM THE SELECT GAME SCREEN)
            'DATE DIFF IS CHECKED WITH THE GAME DATE PASSED AS PARAMETER AND DISPLAYS THE GAMES IN THE LISTBOX
            m_dsMultiplegames = m_objModule2Score.GetMultipleGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserId)
            'pnlDisplaygames.Visible = True
            If Not m_dsMultiplegames Is Nothing Then
                If (m_dsMultiplegames.Tables(0).Rows.Count > 0) Then
                End If
            Else
                MessageDialog.Show(strmessage13, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Sub FillListView()
        Try
            m_dsPBPData = m_objModule2Score.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            DisplayInListView(m_dsPBPData)
            DoCancel()
            frmMain.GetPenaltyShootoutScore()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub RestartExistingGame()
        Try
            frmMain.FillFormControls()
            LoadTeam(m_objGameDetails.GameCode)
            frmMain.ResetClockAndScores()
            FillListView()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Dim TeamID As Integer
            Dim SequenceNo As Decimal

            btnSwitchGames.Enabled = False

            If lvwPBP.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, 1, 0)

                SequenceNo = CDec(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(8).Text)
                m_EventID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(5).Text)
                If lvwPBP.SelectedItems.Item(0).SubItems(7).Text <> "" Then
                    TeamID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(7).Text)
                    m_TeamID = TeamID
                End If

                ''check for clock events
                'If m_EventID = clsGameDetails.CLOCK_INC Or m_EventID = clsGameDetails.CLOCK_DEC Or m_EventID = clsGameDetails.CLOCK_STOP Or m_EventID = clsGameDetails.CLOCK_START Then
                '    m_objGameDetails.IsEditMode = False
                '    Exit Sub
                'End If

                m_objGameDetails.IsEditMode = True
                m_dsTempPBP = frmMain.AddPBPColumns()
                m_intSequenceNumber = SequenceNo

                Dim DsSelectedEvent As DataSet
                DsSelectedEvent = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "EQUAL")
                m_CurrPeriod = CInt(DsSelectedEvent.Tables(0).Rows(0).Item("PERIOD"))

                'the selected event from main screen is highlighted
                Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(SequenceNo))
                lvwPBP.Items(Lstview.Index).Selected = True
                lvwPBP.Select()
                lvwPBP.EnsureVisible(Lstview.Index)

                m_objGameDetails.EditedEvents = m_objModule2Score.GetPBPMainEventData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "SET")

                If m_EventID = clsGameDetails.GAMESTART Then
                    MessageDialog.Show(strmessage4, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.TIME Then
                    MessageDialog.Show(strmessage3, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.STARTPERIOD Then
                    MessageDialog.Show(strmessage2, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.ENDPERIOD Then
                    MessageDialog.Show(strmessage1, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.ENDGAME Then
                    MessageDialog.Show(strmessage, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.ABANDONED Then
                    MessageDialog.Show(strMessage25, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                End If

                'Set Screen Color for EDIT
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)

                btnDelete.Enabled = False
                btnInsert.Enabled = False
                btnEdit.Enabled = False
                pnlScore.Enabled = True
                'btnSave.Enabled = True
                m_CurrentEventCode = m_EventID

                SetStateEdit(SequenceNo)
                'frmMain.GetPenaltyShootoutScore()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub ChangeTheme(ByVal EditMode As Boolean)
        Try
            If EditMode Then
                Me.BackColor = Color.LightGoldenrodYellow
                pnlScore.BackColor = Color.Beige
            Else
                Me.BackColor = Color.WhiteSmoke
                pnlScore.BackColor = Color.Lavender
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetStateEdit(ByVal SequenceNo As Decimal)
        Try
            Dim TeamID As Integer
            Dim drEditPBP As DataRow
            'Dim SequenceNo As Decimal
            Dim dsPBPData As New DataSet
            Dim ElapsedTime As Integer

            dsPBPData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "EQUAL")

            If dsPBPData.Tables(0).Rows.Count > 0 Then
                TeamID = m_TeamID

                If Not IsDBNull(CDbl(dsPBPData.Tables(0).Rows(0).Item("TIME_ELAPSED"))) Then
                    'CONVERTING TO MINUTES
                    Dim CurrPeriod As Integer
                    ElapsedTime = CInt(dsPBPData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                    CurrPeriod = CInt(dsPBPData.Tables(0).Rows(0).Item("PERIOD"))
                    Dim EventCodeID As Integer = CInt(dsPBPData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                    If ElapsedTime > 0 Then
                        ElapsedTime = ElapsedTime + 30
                    End If
                    Dim Strtime As String = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    Dim arr As Array = Strtime.Split(CChar(":"))
                    txtTimeMod2.Text = arr.GetValue(0).ToString
                    txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))
                    cmbTeam.SelectedValue = TeamID
                End If

                If m_objGameDetails.IsEditMode Then
                    setScoreforTeam()
                End If
                ''
                Select Case m_EventID
                    Case clsGameDetails.GOAL

                        drEditPBP = m_dsTempPBP.Tables(0).NewRow()

                        'Store current data to temp data set
                        drEditPBP("GAME_CODE") = m_objGameDetails.GameCode
                        drEditPBP("FEED_NUMBER") = m_objGameDetails.FeedNumber
                        drEditPBP("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                        drEditPBP("PERIOD") = m_objGameDetails.CurrentPeriod
                        drEditPBP("TEAM_ID") = TeamID
                        drEditPBP("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                        drEditPBP("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                        drEditPBP("REPORTER_ROLE") = m_objGameDetails.ReporterRole
                        drEditPBP("UNIQUE_ID") = dsPBPData.Tables(0).Rows(0).Item("UNIQUE_ID")

                        'use edited record data
                        m_objGameDetails.GameCode = CInt(dsPBPData.Tables(0).Rows(0).Item("GAME_CODE"))
                        m_objGameDetails.FeedNumber = CInt(dsPBPData.Tables(0).Rows(0).Item("FEED_NUMBER"))

                        m_objGameDetails.CurrentPeriod = CInt(dsPBPData.Tables(0).Rows(0).Item("PERIOD"))
                        m_EventID = CInt(dsPBPData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                        If m_objGameDetails.PBP.Tables(0).Rows.Count = 0 Then
                            m_objGameDetails.OffensiveTeamID = CInt(dsPBPData.Tables(0).Rows(0).Item("TEAM_ID"))
                        End If
                        '
                        If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                            If m_objGameDetails.HomeTeamID = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                                If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                Else
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                End If
                            Else
                                If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                Else
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)

                                End If
                            End If
                        Else
                            If m_objGameDetails.HomeTeamID = CInt(dsPBPData.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                    m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE")) - 1
                                    m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                Else
                                    m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE")) - 1
                                    m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                End If
                            Else
                                If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                    m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE")) - 1
                                    m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                Else
                                    m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE")) - 1
                                    m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                End If
                            End If
                        End If

                        drEditPBP("SEQUENCE_NUMBER") = dsPBPData.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")

                        m_dsTempPBP.Tables(0).Rows.Clear()
                        m_dsTempPBP.Tables(0).Rows.Add(drEditPBP)

                        'Comments
                    Case clsGameDetails.SHOOTOUT_SAVE, clsGameDetails.SHOOTOUT_MISSED, clsGameDetails.SHOOTOUT_GOAL
                        frmPenaltyShootoutScore.ShowDialog()
                        FillListView()
                        frmMain.GetPenaltyShootoutScore()
                    Case clsGameDetails.COMMENTS, clsGameDetails.DELAYED
                        Dim StrTime As String
                        drEditPBP = m_dsTempPBP.Tables(0).NewRow()

                        drEditPBP("GAME_CODE") = m_objGameDetails.GameCode
                        drEditPBP("FEED_NUMBER") = m_objGameDetails.FeedNumber
                        drEditPBP("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                        drEditPBP("PERIOD") = m_objGameDetails.CurrentPeriod
                        drEditPBP("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                        drEditPBP("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                        drEditPBP("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                        drEditPBP("REPORTER_ROLE") = m_objGameDetails.ReporterRole
                        drEditPBP("UNIQUE_ID") = dsPBPData.Tables(0).Rows(0).Item("UNIQUE_ID")
                        m_objGameDetails.GameCode = CInt(dsPBPData.Tables(0).Rows(0).Item("GAME_CODE"))
                        m_objGameDetails.FeedNumber = CInt(dsPBPData.Tables(0).Rows(0).Item("FEED_NUMBER"))
                        If IsDBNull(dsPBPData.Tables(0).Rows(0).Item("COMMENTS")) = True Then
                            m_objGameDetails.CommentData = ""
                        Else
                            m_objGameDetails.CommentData = CStr(dsPBPData.Tables(0).Rows(0).Item("COMMENTS"))
                        End If

                        m_objGameDetails.CurrentPeriod = CInt(dsPBPData.Tables(0).Rows(0).Item("PERIOD"))
                        'm_CommentEditTime = CInt(dsPBPData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                        m_EventID = CInt(dsPBPData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                        'm_objGameDetails.CommentsTime = m_CommentEditTime
                        m_objGameDetails.CommentsTime = CInt(drEditPBP("TIME_ELAPSED"))
                        If Not IsDBNull(dsPBPData.Tables(0).Rows(0).Item("TEAM_ID")) Then
                            m_objGameDetails.OffensiveTeamID = CInt(dsPBPData.Tables(0).Rows(0).Item("TEAM_ID"))
                        Else
                            m_objGameDetails.OffensiveTeamID = -1
                        End If
                        If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                            If m_objGameDetails.HomeTeamID = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                                If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                Else
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                End If
                            Else
                                If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                Else
                                    m_objGameDetails.HomeScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                                    m_objGameDetails.AwayScore = CInt(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))

                                    frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                    frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                End If
                            End If
                        Else
                            If IsDBNull(dsPBPData.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))
                                m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE"))

                                frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                            Else
                                If m_objGameDetails.HomeTeamID = CInt(dsPBPData.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                    If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                        m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE"))
                                        m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                    Else
                                        m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE"))
                                        m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                    End If
                                Else
                                    If m_objGameDetails.HomeTeamID = m_objGameDetails.OffensiveTeamID Then
                                        m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE"))
                                        m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                                    Else
                                        m_objGameDetails.HomeScore = CInt(dsPBPData.Tables(0).Rows(0).Item("OFFENSE_SCORE"))
                                        m_objGameDetails.AwayScore = CInt(dsPBPData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

                                        frmMain.lblScoreAway.Text = CStr(m_objGameDetails.HomeScore)
                                        frmMain.lblScoreHome.Text = CStr(m_objGameDetails.AwayScore)
                                    End If
                                End If
                            End If
                        End If

                        drEditPBP("SEQUENCE_NUMBER") = dsPBPData.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")

                        '
                        m_dsTempPBP.Tables(0).Rows.Clear()
                        m_dsTempPBP.Tables(0).Rows.Add(drEditPBP)

                End Select

                If (m_EventID = clsGameDetails.COMMENTS Or m_EventID = clsGameDetails.DELAYED) And m_objGameDetails.IsEditMode Then
                    If m_EventID = clsGameDetails.DELAYED Then
                        m_objGameDetails.IsDelayed = True
                    End If
                    If frmComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                        AddPlayByPlayData_Edit(m_EventID)
                        InsertPlayByPlayData_Edit(m_EventID)
                    End If
                    btnSave.Enabled = False
                    DoCancel()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
            txtTimeMod2.Text = ""
            cmbTeam.SelectedIndex = 0
            btnSave.Enabled = True
            frmMain.ResetClockAndScores()
            DoCancel()
            btnSwitchGames.Enabled = True
            m_objGameDetails.IsEditMode = False
            m_objGameDetails.IsInsertMode = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            If m_objGameDetails.PBP.Tables(0).Rows.Count > 1 Then
                Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER")))
                Dim TotCount As Integer
                If Not Lstview Is Nothing Then
                    lvwPBP.Items(Lstview.Index).Selected = True
                    lvwPBP.Select()
                    lvwPBP.EnsureVisible(Lstview.Index)
                End If
                TotCount = m_objGameDetails.PBP.Tables(0).Rows.Count
                If TotCount >= 1 Then
                    If m_dsTempPBP.Tables.Count > 0 Then
                        m_dsTempPBP.Tables(0).Rows.Clear()
                    End If

                    m_dsTempPBP = frmMain.AddPBPColumns()
                    Dim drTempData As DataRow
                    drTempData = m_objGameDetails.PBP.Tables(0).Rows(TotCount - 1)
                    m_dsTempPBP.Tables(0).ImportRow(drTempData)
                End If
                m_objGameDetails.PBP.Tables(0).Rows(TotCount - 1).Delete()
                If m_objGameDetails.IsInsertMode Then
                    m_PrevSeq = CDec(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER"))
                End If
            Else
                If m_objGameDetails.IsContinuationEvent = False Or m_objGameDetails.PBP.Tables(0).Rows.Count = 0 Then
                    setScoreforTeam()
                End If
                m_objGameDetails.IsContinuationEvent = False
                If m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                    If m_objGameDetails.IsInsertMode Then
                        Dim dsPBPData As DataSet
                        dsPBPData = m_objModule2Score.GetPrevSeq(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_intSequenceNumber, "PREV")
                        If dsPBPData.Tables(0).Rows.Count > 0 Then
                            If Not IsDBNull(dsPBPData.Tables(0).Rows(0).Item("Column1")) Then
                                m_PrevSeq = CDec(dsPBPData.Tables(0).Rows(0).Item("Column1"))
                            End If
                        End If
                    End If

                    Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER")))
                    m_intSequenceNumber = CDec(m_objGameDetails.PBP.Tables(0).Rows(m_objGameDetails.PBP.Tables(0).Rows.Count - 1).Item("SEQUENCE_NUMBER"))
                    For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                        m_objGameDetails.PBP.Tables(i).Rows.Clear()
                    Next
                    If Not Lstview Is Nothing Then
                        lvwPBP.Items(Lstview.Index).Selected = True
                        lvwPBP.Select()
                        lvwPBP.EnsureVisible(Lstview.Index)
                    End If
                Else
                    m_intSequenceNumber = 0
                    m_PrevSeq = 0
                    'm_NextSeq = 0
                    m_CurrPeriod = 0
                    m_TeamID = 0
                    m_allowSave = True
                    m_TimeEditMode = False
                    m_objGameDetails.IsInsertMode = False
                End If
            End If
            frmMain.GetPenaltyShootoutScore()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim SequenceNo As Decimal
            Dim EventID As Integer
            Dim seqGameStartEvent As Decimal
            Dim dsPBPData As New DataSet
            Dim dsPBPSeqData As New DataSet
            Dim dsDeletePBPData As New DataSet
            Dim dsGetPlayerInOut As New DataSet
            Dim StrMessage As String = ""

            btnSwitchGames.Enabled = False
            If lvwPBP.SelectedItems.Count > 0 Then
                btnEdit.Enabled = False
                btnInsert.Enabled = False
                btnDelete.Enabled = False

                EventID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(5).Text)
                SequenceNo = CDec(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(8).Text)
                m_intSequenceNumber = SequenceNo
                dsDeletePBPData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "GREATER")
                Dim drs() As DataRow = dsDeletePBPData.Tables(0).Select()
                If drs.Length > 0 Then
                    If EventID = clsGameDetails.GAMESTART Then
                        MessageDialog.Show(strmessage14, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        btnSwitchGames.Enabled = True
                        Exit Sub
                    ElseIf EventID = clsGameDetails.STARTPERIOD Then
                        MessageDialog.Show(strmessage10, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        btnSwitchGames.Enabled = True
                        Exit Sub
                    ElseIf EventID = clsGameDetails.ENDPERIOD Then
                        MessageDialog.Show(strmessage12, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        btnSwitchGames.Enabled = True
                        Exit Sub
                    ElseIf EventID = clsGameDetails.ENDGAME Then
                        MessageDialog.Show(strmessage11, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        btnSwitchGames.Enabled = True
                        Exit Sub
                    End If
                ElseIf EventID = clsGameDetails.STARTPERIOD And m_objGameDetails.CurrentPeriod = 1 Then
                    Dim gamestartData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod, clsGameDetails.GAMESTART, m_objGameDetails.ModuleID)
                    If gamestartData.Tables(0).Rows.Count > 0 Then
                        'Dim seqGameStartEvent As Decimal
                        seqGameStartEvent = CDec(gamestartData.Tables(0).Rows(0).Item("SEQUENCE_NUMBER"))
                    End If
                    'MessageDialog.Show(strmessage10, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    'btnSwitchGames.Enabled = True
                    'Exit Sub
                End If

                If StrMessage = "" Then
                    StrMessage = "Are you Sure to delete this record ?"
                End If

                If MessageDialog.Show(StrMessage, "Delete", MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, 1, 1)

                    Dim dsPrevPBPData As New DataSet
                    Dim StrTime As String = "00:00"
                    Dim strNewTime() As String
                    Dim ELapsedTime As Integer
                    Dim CurrentPeriod As Integer
                    Dim PrevSeq As Decimal

                    dsPBPSeqData = m_objModule2Score.GetPrevSeq(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "PREV")
                    If Not IsDBNull(dsPBPSeqData.Tables(0).Rows(0).Item("Column1")) Then
                        'Set Prev Seq Number
                        PrevSeq = CDec(dsPBPSeqData.Tables(0).Rows(0).Item("Column1"))
                        'Get Prev Seq Data
                        dsPrevPBPData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, PrevSeq, "EQUAL")

                        'Set Main Clock with Prev Seq Time ELapsed
                        ELapsedTime = CInt(dsPrevPBPData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                        CurrentPeriod = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(1).Text)

                    End If

                    If EventID = clsGameDetails.STARTPERIOD Or EventID = clsGameDetails.ENDPERIOD Then
                        'Set the current Period properly
                        If EventID = clsGameDetails.STARTPERIOD Then
                            m_objGameDetails.CurrentPeriod = CurrentPeriod - 1
                            If m_objGameDetails.CurrentPeriod = 0 Then
                                frmMain.lblPeriod.Text = "Pre-Game"
                            ElseIf m_objGameDetails.CurrentPeriod = 1 Then
                                frmMain.lblPeriod.Text = "Halftime"
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                frmMain.lblPeriod.Text = "End Reg"
                                frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
                            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                frmMain.lblPeriod.Text = "ET Break"
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                frmMain.lblPeriod.Text = "End Reg"
                                frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
                            End If
                            If m_objGameDetails.CurrentPeriod <> 0 And m_objGameDetails.IsHalfTimeSwap Then
                                frmMain.SetHeader2()
                            End If
                            frmMain.lblPeriod.ForeColor = Color.Red
                            frmMain.btnClock.Enabled = False
                            frmMain.btnMinuteDown.Enabled = False
                            frmMain.btnMinuteUp.Enabled = False
                            frmMain.btnSecondDown.Enabled = False
                            frmMain.btnSecondUp.Enabled = False

                            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                            DisableAllControls(False)
                            StrTime = CalculateTimeElapseAfter(ELapsedTime, m_objGameDetails.CurrentPeriod)
                            strNewTime = StrTime.Split(CChar(":"))

                            frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), 0)
                            frmMain.UdcRunningClock1.URCToggle()
                        Else
                            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                            frmMain.lblPeriod.ForeColor = Color.White
                            If m_objGameDetails.CurrentPeriod = 0 Then
                                frmMain.lblPeriod.Text = "Pre-Game"
                            ElseIf m_objGameDetails.CurrentPeriod = 1 Then
                                frmMain.lblPeriod.Text = "1st Half"
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                frmMain.lblPeriod.Text = "2nd Half"
                            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                frmMain.lblPeriod.Text = "1st ET"
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                frmMain.lblPeriod.Text = "2nd ET"
                            End If
                            frmMain.UdcRunningClock1.URCToggle()
                            DisableAllControls(True)
                            frmMain.btnClock.Enabled = True
                            frmMain.btnMinuteDown.Enabled = True
                            frmMain.btnMinuteUp.Enabled = True
                            frmMain.btnSecondDown.Enabled = True
                            frmMain.btnSecondUp.Enabled = True
                            frmMain.mtxtClockEdit.Enabled = True

                        End If

                        'Delete END PERIOD NOW
                        dsPBPData = m_objModule2Score.DeletePBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, SequenceNo, m_objGameDetails.languageid)
                        If EventID = clsGameDetails.ENDPERIOD Then
                            StrTime = CalculateTimeElapseAfter(CInt(dsPBPData.Tables(0).Rows(0).Item("TIME_ELAPSED")), m_objGameDetails.CurrentPeriod)
                            strNewTime = StrTime.Split(CChar(":"))
                            If m_objGameDetails.ModuleID = 1 Then
                                frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                            Else
                                frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), 0)
                            End If
                        ElseIf EventID = clsGameDetails.STARTPERIOD And m_objGameDetails.CurrentPeriod = 0 Then
                            dsPBPData = m_objModule2Score.DeletePBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, seqGameStartEvent, m_objGameDetails.languageid)

                        End If
                        If frmMain.UdcRunningClock1.URCIsRunning Then
                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                        Else
                            frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                        End If
                        'dsPBPData = m_objModule1PBP.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        DisplayInListView(dsPBPData)
                    Else
                        Select Case EventID
                            Case clsGameDetails.GAMESTART
                                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Case clsGameDetails.HomeStartingLineups
                                MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Case clsGameDetails.AwatStartingLineups
                                MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

                            Case Else
                                If EventID = clsGameDetails.ABANDONED Then
                                    frmMain.EnableMenuItem("AbandonToolStripMenuItem", True)
                                ElseIf EventID = clsGameDetails.DELAYED Then
                                    frmMain.EnableMenuItem("DelayedToolStripMenuItem", True)
                                End If
                                dsPBPData = m_objModule2Score.DeletePBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, SequenceNo, m_objGameDetails.languageid)
                                'dsPBPData = m_objModule1PBP.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                                DisplayInListView(dsPBPData)
                        End Select
                        If EventID = clsGameDetails.ENDGAME Then
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                frmMain.lblPeriod.Text = "1st Half"
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                frmMain.lblPeriod.Text = "2nd Half"
                                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
                            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                frmMain.lblPeriod.Text = "1st ET"
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                frmMain.lblPeriod.Text = "2nd ET"
                                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                frmMain.EnableMenuItem("EndGameToolStripMenuItem", True)
                            End If
                        End If
                    End If
                End If
                setScoreforTeam()
                frmMain.ResetClockAndScores()
            End If
            DoCancel()
            frmMain.GetPenaltyShootoutScore()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            btnSave.Enabled = True
        End Try
    End Sub

    Private Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnInsert.Text, 1, 0)

            btnSwitchGames.Enabled = False
            If lvwPBP.SelectedItems.Count > 0 Then

                m_objGameDetails.IsInsertMode = True
                Dim TeamID As Integer
                Dim SequenceNo As Decimal
                Dim dsPBPData As New DataSet
                Dim dsInsertData As New DataSet
                Dim StrTime As String
                Dim NextPeriod As Integer

                SequenceNo = CDec(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(8).Text)
                m_intSequenceNumber = SequenceNo
                m_EventID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(5).Text)
                If DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(7).Text <> "" Then
                    TeamID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(7).Text)
                End If

                'm_objGameDetails.IsInsertMode = True
                'm_dsTempPBP = frmMain.AddPBPColumns()

                Dim DsSelectedEvent As DataSet
                DsSelectedEvent = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "EQUAL")

                If IsPairedEvents(SequenceNo) Then
                    DoCancel()
                    Exit Sub
                End If

                'the selected event from main screen is highlighted
                Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(SequenceNo))
                lvwPBP.Items(Lstview.Index).Selected = True
                lvwPBP.Select()
                lvwPBP.EnsureVisible(Lstview.Index)

                'Get the time elapsed and prev seq number
                dsPBPData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "EQUAL")
                'Convert StrTime to Seconds
                m_NextElapsedTime = CInt(dsPBPData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                'Arindam 25Aug START
                If m_NextElapsedTime > 0 Then
                    m_NextElapsedTime = m_NextElapsedTime + 30
                End If
                'Arindam 25Aug END

                m_NextEventCode = CInt(dsPBPData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                NextPeriod = CInt(dsPBPData.Tables(0).Rows(0).Item("PERIOD"))

                'Arindam 25Aug START
                'COMM BY SHIRLEY ON JULY 28 2010
                StrTime = CalculateTimeElapseAfter(m_NextElapsedTime, NextPeriod)
                m_NextElapsedTime = CInt(StrTime) * 60
                'Arindam 25Aug END

                If m_EventID = clsGameDetails.ENDGAME Or m_EventID = clsGameDetails.GAMESTART Or m_EventID = clsGameDetails.STARTPERIOD Or m_EventID = clsGameDetails.TIME Then
                    MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    btnSwitchGames.Enabled = True
                    DoCancel()
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.STARTPERIOD And NextPeriod = 1 Then
                    MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    DoCancel()
                    Exit Sub
                End If

                dsInsertData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_PrevSeq, "EQUAL")
                If dsInsertData.Tables(0).Rows.Count > 0 Then
                    'Arindam 25Aug START
                    Dim StrTime1 As String
                    StrTime1 = CalculateTimeElapseAfter(CInt(dsInsertData.Tables(0).Rows(0).Item("TIME_ELAPSED")), CInt(dsInsertData.Tables(0).Rows(0).Item("PERIOD")))

                    m_PrevElapsedTime = CInt(dsInsertData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                    m_CurrPeriod = CInt(dsInsertData.Tables(0).Rows(0).Item("PERIOD"))
                    m_PrevElapsedTime = CInt(StrTime1) * 60
                    'Arindam 25Aug END
                    'm_PrevElapsedTime = CInt(StrTime1) * 60

                    If m_EventID = clsGameDetails.ENDPERIOD And CInt(dsInsertData.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = clsGameDetails.ABANDONED Then
                        'MessageDialog.Show(strmessage90, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        DoCancel()
                        Exit Sub
                    End If

                Else
                    m_PrevElapsedTime = m_NextElapsedTime
                    m_CurrPeriod = NextPeriod

                End If

                'StrTime = CalculateTimeElapseAfter(m_PrevElapsedTime, m_CurrPeriod)
                'm_PrevElapsedTime = CInt(m_objUtility.ConvertMinuteToSecond(StrTime))
                '' To prevent enabling all events in END PERIOD
                Dim dsEndPeriodCount As DataSet
                dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, NextPeriod)

                If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = m_objGameDetails.CurrentPeriod Then
                    If dsEndPeriodCount.Tables(1).Rows.Count > 0 Then
                        If CDec(dsEndPeriodCount.Tables(1).Rows(0).Item("Seqnum")) >= m_intSequenceNumber Then
                            DisableAllControls(True)
                        Else
                            DisableAllControls(False)
                        End If
                    Else
                        DisableAllControls(False)
                    End If
                    btnCancel.Enabled = True
                Else
                    If m_EventID = clsGameDetails.STARTPERIOD And NextPeriod > 1 Then
                        'DisableAllControlsAfterHalf(False, NextPeriod, m_EventID)
                    Else
                        If dsEndPeriodCount.Tables(1).Rows.Count > 0 Then
                            If CDec(dsEndPeriodCount.Tables(1).Rows(0).Item("Seqnum")) >= m_intSequenceNumber Then
                                DisableAllControls(True)
                            Else
                                ' DisableAllControlsAfterHalf(False, NextPeriod, m_EventID)
                            End If
                        End If
                    End If
                    btnCancel.Enabled = True
                End If

                SaveCurrentState()
                If dsInsertData.Tables(0).Rows.Count > 0 Then
                    SetOldState(dsInsertData)
                End If

                m_objGameDetails.IsInsertMode = True
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                btnInsert.Enabled = False
                m_TeamID = m_objGameDetails.OffensiveTeamID

                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)
                MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                'DisableAllControls(True)
                lvwPBP.Items(lvwPBP.SelectedItems(0).Index).Selected = True
                lvwPBP.Select()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub DoCancel()
        Try
            cmbTeam.SelectedIndex = 0
            txtTimeMod2.Text = ""
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)
            btnEdit.Enabled = False
            btnInsert.Enabled = False
            btnDelete.Enabled = False
            btnSwitchGames.Enabled = True
            Dim EventID As Integer

            m_objGameDetails.IsInsertMode = False
            m_objGameDetails.IsEditMode = False
            m_TimeEditMode = False
            Dim dsPBP As New DataSet
            dsPBP = m_objModule2Score.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            DisplayInListView(dsPBP)

            ''If LAST EVENT ENTERED IS ENDPERIOD - then DISABLE ALL EVENTS EXCEPT LISTVIEW
            If (dsPBP.Tables(0).Rows.Count > 0) Then
                For intRowCount = 0 To dsPBP.Tables(0).Rows.Count - 1
                    EventID = CInt(dsPBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                    Exit For
                Next
            End If
            If EventID = clsGameDetails.ENDPERIOD Or EventID = clsGameDetails.ENDGAME Then
                DisableAllControls(False)
            ElseIf EventID = clsGameDetails.SHOOTOUT_GOAL Or EventID = clsGameDetails.SHOOTOUT_MISSED Or EventID = clsGameDetails.SHOOTOUT_SAVE Then
                DisableAllControls(False)
            Else
                DisableAllControls(True)
            End If

            Dim dsEndPeriodCount As New DataSet
            dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)

            If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = m_objGameDetails.CurrentPeriod Then
                DisableAllControls(False)
                'lvwPBP.Enabled = False
            Else
                DisableAllControls(True)
                lvwPBP.Enabled = True
            End If

            If EventID = clsGameDetails.ENDGAME Then
                DisableAllControls(False)
            End If
            m_TimeEditMode = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lvwPBP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPBP.Click
        Try
            If m_objGameDetails.IsInsertMode Or m_objGameDetails.IsEditMode Then
                MessageDialog.Show(strMessage22, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            Else
                If lvwPBP.SelectedItems.Count > 0 Then
                    'AUDIT TRIAL
                    btnEdit.Enabled = True
                    btnInsert.Enabled = True
                    btnDelete.Enabled = True
                    btnSave.Enabled = True
                    btnCancel.Enabled = True
                Else
                    btnEdit.Enabled = False
                    btnInsert.Enabled = False
                    btnDelete.Enabled = False
                End If

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub SaveCurrentState()
        Try
            Dim drEditPBP As DataRow
            m_dsTempPBP = frmMain.AddPBPColumns()
            drEditPBP = m_dsTempPBP.Tables(0).NewRow()
            drEditPBP("GAME_CODE") = m_objGameDetails.GameCode
            drEditPBP("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drEditPBP("PERIOD") = m_objGameDetails.CurrentPeriod
            drEditPBP("TEAM_ID") = m_objGameDetails.OffensiveTeamID
            drEditPBP("OFFENSE_SCORE") = m_objGameDetails.HomeScore
            drEditPBP("DEFENSE_SCORE") = m_objGameDetails.AwayScore
            drEditPBP("REPORTER_ROLE") = m_objGameDetails.ReporterRole
            m_dsTempPBP.Tables(0).Rows.Clear()
            m_dsTempPBP.Tables(0).Rows.Add(drEditPBP)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetOldState(ByVal dsInsertData As DataSet)
        Try
            'Set Team ID , Scores and other information based on Prev Seq Number
            m_objGameDetails.GameCode = CInt(dsInsertData.Tables(0).Rows(0).Item("GAME_CODE"))
            m_objGameDetails.FeedNumber = CInt(dsInsertData.Tables(0).Rows(0).Item("FEED_NUMBER"))
            m_objGameDetails.CurrentPeriod = CInt(dsInsertData.Tables(0).Rows(0).Item("PERIOD"))
            m_EventID = CInt(dsInsertData.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
            'If Not IsDBNull(dsInsertData.Tables(0).Rows(0).Item("TEAM_ID")) And m_TimeEditMode = False Then
            '    m_objGameDetails.OffensiveTeamID = CInt(dsInsertData.Tables(0).Rows(0).Item("TEAM_ID"))
            '    cmbTeam.SelectedValue = m_objGameDetails.OffensiveTeamID
            'End If

            m_objGameDetails.HomeScore = CInt(dsInsertData.Tables(0).Rows(0).Item("OFFENSE_SCORE"))
            m_objGameDetails.AwayScore = CInt(dsInsertData.Tables(0).Rows(0).Item("DEFENSE_SCORE"))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub AddPlayByPlayData_Edit(ByVal EventID As Integer)
        'Dim strSoccerSavePBPpath As String = Application.StartupPath & "\Resources\Soc_PBP_Data.xml"
        Dim draEvents As DataRow()
        Dim strSelect As String
        Dim dr As DataRow
        Dim decSeqNo As Decimal
        Dim drPBPData As DataRow
        Dim drPBPEditData As DataRow
        Dim dtTimeDiff As DateTime

        Try
            If EventID = clsGameDetails.GOAL Then
                m_objGameDetails.OffensiveTeamID = Convert.ToInt32(cmbTeam.SelectedValue)
            End If
            'm_objGameDetails.OffensiveTeamID = Convert.ToInt32(cmbTeam.SelectedValue)
            drPBPEditData = m_dsTempPBP.Tables(0).Rows(0)
            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
            drPBPData("CONTINUATION") = "F"
            drPBPData("SEQUENCE_NUMBER") = m_intSequenceNumber
            decSeqNo = CDec(drPBPEditData("SEQUENCE_NUMBER"))
            drPBPData("UNIQUE_ID") = m_objModule2Score.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            drPBPData("SEQUENCE_NUMBER") = decSeqNo
            drPBPData("ORIG_SEQ") = DBNull.Value
            drPBPData("CONTINUATION") = "F"
            drPBPData("GAME_CODE") = m_objGameDetails.GameCode
            drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod

            dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
            drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

            'drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(Dt, DateTimeHelper.OutputFormat.CurrentFormat)
            'drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
            If EventID = clsGameDetails.COMMENTS And m_objGameDetails.OffensiveTeamID = -1 Then
                drPBPData("TEAM_ID") = DBNull.Value
            Else
                drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
            End If

            If EventID = clsGameDetails.COMMENTS Then
                drPBPData("TIME_ELAPSED") = m_objGameDetails.CommentsTime ' m_CommentEditTime

            Else

                If m_objGameDetails.IsDefaultGameClock Then

                    Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                    If timElap >= 60 Then
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore((CInt(txtTimeMod2.Text) * 60) - 30)
                    Else
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                    End If
                Else
                    Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                    If timElap >= 60 Then
                        drPBPData("TIME_ELAPSED") = (CInt(txtTimeMod2.Text) * 60) - 30
                    Else
                        drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                    End If
                End If
            End If

            If m_TeamID = m_objGameDetails.HomeTeamID Then
                If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        m_objGameDetails.HomeScore = m_objGameDetails.HomeScore + 1
                    End If
                Else
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        m_objGameDetails.AwayScore = m_objGameDetails.AwayScore + 1
                    End If
                End If
            Else
                If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        m_objGameDetails.AwayScore = m_objGameDetails.AwayScore + 1

                    End If
                Else
                    If EventID = clsGameDetails.GOAL Or EventID = clsGameDetails.PENALTY Or EventID = clsGameDetails.OWNGOAL Then
                        m_objGameDetails.HomeScore = m_objGameDetails.HomeScore + 1
                    End If
                End If
            End If
            drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
            drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
            drPBPData("OPTICAL_TIMESTAMP") = DBNull.Value
            drPBPData("RECORD_EDITED") = "N"
            drPBPData("EDIT_UID") = drPBPEditData("UNIQUE_ID")
            drPBPData("PBP_STRING") = DBNull.Value
            drPBPData("MULTILINGUAL_PBP_STRING") = DBNull.Value
            drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
            drPBPData("PROCESSED") = "N"
            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                drPBPData("DEMO_DATA") = "Y"
            Else
                drPBPData("DEMO_DATA") = "N"
            End If
            ''
            Select Case EventID
                Case clsGameDetails.GOAL, clsGameDetails.PENALTY, clsGameDetails.OWNGOAL 'Goal EVENT
                    drPBPData("EVENT_CODE_ID") = EventID
                Case clsGameDetails.COMMENTS, clsGameDetails.DELAYED

                    'If m_objGameDetails.IsEditMode Then
                    '    m_TimeEditMode = True
                    'End If

                    If m_objGameDetails.OffensiveTeamID = -1 Then
                        drPBPData("TEAM_ID") = DBNull.Value
                    Else
                        drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                    End If

                    Dim TimeElapsed As Integer = m_objModule2Score.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    Dim EditedTime As Integer

                    If m_objGameDetails.IsDefaultGameClock Then
                        If m_objGameDetails.CommentsTime = 0 Then
                            EditedTime = m_objGameDetails.CommentsTime
                        Else
                            EditedTime = CalculateTimeElapsedBefore(m_objGameDetails.CommentsTime - 30)
                        End If
                    Else
                        EditedTime = m_objGameDetails.CommentsTime
                    End If

                    drPBPData("TIME_ELAPSED") = EditedTime
                    If EditedTime <> TimeElapsed Then
                        m_TimeEditMode = True
                    End If
                    drPBPData("EVENT_CODE_ID") = EventID
                    drPBPData("COMMENTS") = m_objGameDetails.CommentData

            End Select
            m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    Private Sub InsertPlayByPlayData_Edit(ByVal EventID As Integer)
        Try
            Dim dsTempPBPData As New DataSet
            m_objGameDetails.PBP.DataSetName = "SoccerEventData"
            dsTempPBPData = m_objGameDetails.PBP
            Dim strPBPXMLData As String = dsTempPBPData.GetXml()
            Dim dsPBP As New DataSet

            If m_objGameDetails.IsEditMode = True Then
                'dsPBP = m_objModule2Score.UpdatePairedPBPEvents(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, CDec(m_objGameDetails.PBP.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")), "EDIT", m_objGameDetails.languageid)

                If m_TimeEditMode = False Then
                    dsPBP = m_objModule2Score.UpdatePairedPBPEvents(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, CDec(m_objGameDetails.PBP.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")), "EDIT", m_objGameDetails.languageid)
                Else
                    'Edit Time Functionality
                    dsPBP = m_objModule2Score.TimeEditOnPBPEvents(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, CDec(m_objGameDetails.PBP.Tables(0).Rows(0).Item("SEQUENCE_NUMBER")), m_objGameDetails.languageid)
                    'dsPBP.Tables.Remove(dsPBP.Tables(0))
                    dsPBP.AcceptChanges()
                End If
            End If

            ' Set correct Score now
            setScoreforTeam()
            'setCurrentState()
            DisplayInListView(dsPBP)
            frmMain.ResetClockAndScores()
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 1)
        Catch ex As Exception
            Throw ex
        Finally
            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next

        End Try
    End Sub

    Private Sub txtTimeMod2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTimeMod2.Validated
        Try
            txtTimeMod2.Text = txtTimeMod2.Text.PadLeft(3, CChar(" "))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lvwPBP_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPBP.DoubleClick
        Try
            Dim TeamID As Integer
            Dim SequenceNo As Decimal

            btnSwitchGames.Enabled = False

            If lvwPBP.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, 1, 0)

                SequenceNo = CDec(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(8).Text)
                m_EventID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(5).Text)
                If lvwPBP.SelectedItems.Item(0).SubItems(7).Text <> "" Then
                    TeamID = CInt(DirectCast(lvwPBP.SelectedItems, System.Windows.Forms.ListView.SelectedListViewItemCollection).Item(0).SubItems(7).Text)
                    m_TeamID = TeamID
                End If

                ''check for clock events
                'If m_EventID = clsGameDetails.CLOCK_INC Or m_EventID = clsGameDetails.CLOCK_DEC Or m_EventID = clsGameDetails.CLOCK_STOP Or m_EventID = clsGameDetails.CLOCK_START Then
                '    m_objGameDetails.IsEditMode = False
                '    Exit Sub
                'End If

                m_objGameDetails.IsEditMode = True
                m_dsTempPBP = frmMain.AddPBPColumns()
                m_intSequenceNumber = SequenceNo

                Dim DsSelectedEvent As DataSet
                DsSelectedEvent = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "EQUAL")

                m_CurrPeriod = CInt(DsSelectedEvent.Tables(0).Rows(0).Item("PERIOD"))

                If m_CurrPeriod = 0 And m_EventID = clsGameDetails.COMMENTS Then
                    m_CurrPeriod = m_objGameDetails.CurrentPeriod
                End If

                'the selected event from main screen is highlighted
                If m_EventID <> clsGameDetails.SHOOTOUT_GOAL Then
                    Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(SequenceNo))
                    lvwPBP.Items(Lstview.Index).Selected = True
                    lvwPBP.Select()
                    lvwPBP.EnsureVisible(Lstview.Index)
                End If

                m_objGameDetails.EditedEvents = m_objModule2Score.GetPBPMainEventData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SequenceNo, "SET")

                If m_EventID = clsGameDetails.GAMESTART Then
                    MessageDialog.Show(strmessage4, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.TIME Then
                    MessageDialog.Show(strmessage3, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.STARTPERIOD Then
                    MessageDialog.Show(strmessage2, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.ENDPERIOD Then
                    MessageDialog.Show(strmessage1, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.ENDGAME Then
                    MessageDialog.Show(strmessage, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                    Exit Sub
                ElseIf m_EventID = clsGameDetails.ABANDONED Then
                    MessageDialog.Show(strMessage25, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    m_objGameDetails.IsEditMode = False
                    btnSwitchGames.Enabled = True
                End If

                'Set Screen Color for EDIT
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)

                btnDelete.Enabled = False
                btnInsert.Enabled = False
                btnEdit.Enabled = False
                pnlScore.Enabled = True
                'cmbTeam.Enabled = True
                'txtTimeMod2.Enabled = True
                'btnSave.Enabled = True
                'btnCancel.Enabled = True
                m_CurrentEventCode = m_EventID

                SetStateEdit(SequenceNo)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function NumberingPairedEvent(ByVal DsPBP As DataSet) As DataSet
        Try

            Dim j As Integer = 1
            DsPBP.Tables(0).Columns.Add("NUM")
            Dim Drs() As DataRow = DsPBP.Tables(0).Select("", "SEQUENCE_NUMBER ASC")

            For i As Integer = 0 To Drs.Length - 1
                If i + 1 < Drs.Length Then
                    'if next record orig_seq is not null, then main event is paired event and assigining the value
                    If Not IsDBNull(Drs(i + 1).Item("ORIG_SEQ")) Then
                        If IsDBNull(Drs(i).Item("ORIG_SEQ")) Then
                            Drs(i).Item("Num") = j & "#"
                        End If
                    End If
                End If

                'if paired event orig_seq is not null, then assigning the value of the main event.
                If Not IsDBNull(Drs(i).Item("ORIG_SEQ")) Then
                    Drs(i).Item("Num") = j
                    If i + 1 < Drs.Length Then
                        If IsDBNull(Drs(i + 1).Item("ORIG_SEQ")) Then
                            j += 1
                        End If
                    End If
                End If

            Next

            Return DsPBP
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function IsTimeEdited(ByVal CurrTime As Integer) As Boolean
        Try
            Dim dscurrentData As DataSet
            Dim drPBPEditData As DataRow
            Dim editedTime As Integer
            Dim StrTime As String = "00:00"
            drPBPEditData = m_dsTempPBP.Tables(0).Rows(0)
            dscurrentData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CDec(drPBPEditData("SEQUENCE_NUMBER")), "EQUAL")

            'HALF TIME SUBSTITUTION HANDLING
            If dscurrentData.Tables(0).Rows.Count > 0 Then
                'StrTime = CalculateTimeElapseAfter(CInt(dscurrentData.Tables(0).Rows(0).Item("TIME_ELAPSED")), m_CurrPeriod)
                If CInt(dscurrentData.Tables(0).Rows(0).Item("TIME_ELAPSED")) > 0 Then
                    StrTime = CalculateTimeElapseAfter(CInt(dscurrentData.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30, m_CurrPeriod)
                Else
                    StrTime = CalculateTimeElapseAfter(CInt(dscurrentData.Tables(0).Rows(0).Item("TIME_ELAPSED")), m_CurrPeriod)
                End If
                editedTime = CInt(CDbl(StrTime) * 60)

                If CurrTime <> editedTime Then
                    Return True
                End If
            End If

            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsPairedEvents(ByVal SeqNo As Decimal) As Boolean

        Try

            Dim dsPBPData As New DataSet
            Dim dsPrevPBPData As DataSet
            Dim dsNextPBPData As DataSet

            If m_objGameDetails.IsInsertMode Then
                dsPBPData = m_objModule2Score.GetPrevSeq(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SeqNo, "PREV")
            Else
                dsPBPData = m_objModule2Score.GetPrevSeq(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SeqNo, "NEXT")
            End If

            'Store the seq Number
            dsPrevPBPData = m_objModule2Score.GetPBPRecordBasedOnSeqNumber(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, SeqNo, "EQUAL")
            'If CInt(dsPrevPBPData.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = clsGameDetails.REDCARD Then
            '    m_PlayerOutID = CInt(dsPrevPBPData.Tables(0).Rows(0).Item("PLAYER_OUT_ID"))
            'End If
            If Not IsDBNull(dsPrevPBPData.Tables(0).Rows(0).Item("ORIG_SEQ")) And m_objGameDetails.IsInsertMode = False Then
                MessageDialog.Show(strmessage13, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Return True
            End If
            If IsDBNull(dsPBPData.Tables(0).Rows(0).Item("Column1")) Then
                Return False
            End If

            m_PrevSeq = CDec(dsPBPData.Tables(0).Rows(0).Item("Column1"))

            m_NextSeq = CDec(dsPBPData.Tables(0).Rows(0).Item("Column2"))

            'CInt(cmbLinesman.SelectedValue) = 0, DBNull.Value, CInt(cmbLinesman.SelectedValue)
            If IsDBNull(dsPrevPBPData.Tables(0).Rows(0).Item("ORIG_SEQ")) Then
                m_decOrigSeq = 0
            End If

            'm_decOrigSeq = CDec(IIf(IsDBNull(dsPrevPBPData.Tables(0).Rows(0).Item("ORIG_SEQ")) = True, "0", CDec(dsPrevPBPData.Tables(0).Rows(0).Item("ORIG_SEQ"))))
            If (m_decOrigSeq <> 0 And m_objGameDetails.IsInsertMode = False) Then
                MessageDialog.Show(strmessage13, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Return True
            End If

            Return False

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Sub SetTeamButtonColor()
        Try
            'TEAMS COLOR
            Dim dsTeamColor As New DataSet
            dsTeamColor = m_objGeneral.GetTeamColors(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            Dim clrHome, clrAway As Color
            If dsTeamColor.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(dsTeamColor.Tables(0).Rows(0).Item("HOME_COLOR")) Then
                    clrHome = System.Drawing.Color.FromArgb(CInt(dsTeamColor.Tables(0).Rows(0).Item("HOME_COLOR")))
                    frmModule1PBP.SetTeamColor(True, clrHome)
                Else
                    m_objGameDetails.HomeTeamColor = Color.WhiteSmoke
                    frmModule1PBP.SetTeamColor(True, m_objGameDetails.HomeTeamColor)
                    'clrHome = Color.WhiteSmoke
                    'SetTeamColor(True, clrHome)
                End If

                If Not IsDBNull(dsTeamColor.Tables(0).Rows(0).Item("AWAY_COLOR")) Then
                    clrAway = System.Drawing.Color.FromArgb(CInt(dsTeamColor.Tables(0).Rows(0).Item("AWAY_COLOR")))
                    frmModule1PBP.SetTeamColor(False, clrAway)
                Else
                    m_objGameDetails.VisitorTeamColor = Color.WhiteSmoke
                    frmModule1PBP.SetTeamColor(False, m_objGameDetails.VisitorTeamColor)
                    'clrAway = Color.WhiteSmoke
                    'SetTeamColor(False, clrAway)
                End If
            Else
                m_objGameDetails.HomeTeamColor = Color.WhiteSmoke
                m_objGameDetails.VisitorTeamColor = Color.WhiteSmoke
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTeam.SelectedIndexChanged
        Try

            'the selected event from main screen is highlighted
            If lvwPBP.SelectedItems.Count > 0 Then
                Dim Lstview As ListViewItem = lvwPBP.FindItemWithText(CStr(m_intSequenceNumber))
                lvwPBP.Items(Lstview.Index).Selected = True
                lvwPBP.Select()
                lvwPBP.EnsureVisible(Lstview.Index)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnTime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTime.Click
        Try

            'If m_objGameDetails.IsInsertMode Or m_objGameDetails.IsEditMode Then
            '    MessageDialog.Show(strMessage22, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            '    Exit Sub
            'End If
            Dim currTime As Integer

            If txtTimeMod2.Text = "" Then
                MessageDialog.Show(strEnterTime, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If txtTimeMod2.Text.Trim <> "" Then
                If m_objGameDetails.CurrentPeriod = 2 And Convert.ToInt32(txtTimeMod2.Text) < 46 Then
                    MessageDialog.Show(strmessage17, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
                If Convert.ToInt32(txtTimeMod2.Text) < 91 And m_objGameDetails.CurrentPeriod = 3 Then
                    MessageDialog.Show(strmessage16, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
                If Convert.ToInt32(txtTimeMod2.Text) < 106 And m_objGameDetails.CurrentPeriod = 4 Then
                    MessageDialog.Show(strmessage15, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If

            End If

            Dim currTime1 As Integer
            currTime1 = CInt(txtTimeMod2.Text) * 60
            If currTime1.ToString.Length() > 4 Then
                MessageDialog.Show(strmessae26, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If

            If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Then
                If m_CurrentEventCode = clsGameDetails.TIME Then
                    MessageDialog.Show(strTimeEntrynotAllowed, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                Else
                    If m_objGameDetails.IsInsertMode = True Then
                        MessageDialog.Show(strTimeEntrynotAllowed, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If
                End If
            End If

            If m_objGameDetails.IsInsertMode Or m_objGameDetails.IsEditMode Then
                MessageDialog.Show(strMessage22, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If

            If txtTimeMod2.Text.Trim = "" Then
                MessageDialog.Show(strEnterTime, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If m_objGameDetails.IsDefaultGameClock Then
                currTime = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
            Else
                currTime = CInt(txtTimeMod2.Text) * 60
            End If

            'Check if the time entered is higher or equal to last event time
            Dim TimeElapsed As Integer = m_objModule2Score.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If currTime < TimeElapsed Then
                MessageDialog.Show(strTimeEventValidation, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If m_objGameDetails.IsEditMode = False And m_objGameDetails.IsInsertMode = False Then
                AddPBPGeneralEvents(clsGameDetails.TIME, "")
            Else
                MessageDialog.Show(strTimeEntrynotAllowed, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            ' DoCancel()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class