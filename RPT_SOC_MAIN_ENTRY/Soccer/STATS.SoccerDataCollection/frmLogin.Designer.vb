﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.lblMode = New System.Windows.Forms.Label()
        Me.radLive = New System.Windows.Forms.RadioButton()
        Me.radDemo = New System.Windows.Forms.RadioButton()
        Me.lblLanguage = New System.Windows.Forms.Label()
        Me.cmbLanguage = New System.Windows.Forms.ComboBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.lvwGames = New System.Windows.Forms.ListView()
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ReporterRole = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Languageid = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SerialNo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnAddGame = New System.Windows.Forms.Button()
        Me.lblTimeZone = New System.Windows.Forms.Label()
        Me.lnkupcomming = New System.Windows.Forms.LinkLabel()
        Me.radFeedB = New System.Windows.Forms.RadioButton()
        Me.radFeedA = New System.Windows.Forms.RadioButton()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbNoDays = New System.Windows.Forms.ComboBox()
        Me.txtDays = New System.Windows.Forms.Label()
        Me.grpDate = New System.Windows.Forms.GroupBox()
        Me.cmbLeague = New System.Windows.Forms.ComboBox()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblDateBetween = New System.Windows.Forms.Label()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.sstMain.SuspendLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.grpDate.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtUsername
        '
        Me.txtUsername.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtUsername.Location = New System.Drawing.Point(126, 161)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(199, 22)
        Me.txtUsername.TabIndex = 0
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.BackColor = System.Drawing.Color.Transparent
        Me.lblUsername.ForeColor = System.Drawing.Color.DimGray
        Me.lblUsername.Location = New System.Drawing.Point(60, 164)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(60, 15)
        Me.lblUsername.TabIndex = 5
        Me.lblUsername.Text = "Username:"
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.ForeColor = System.Drawing.Color.DimGray
        Me.lblPassword.Location = New System.Drawing.Point(60, 192)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(59, 15)
        Me.lblPassword.TabIndex = 7
        Me.lblPassword.Text = "Password:"
        '
        'txtPassword
        '
        Me.txtPassword.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtPassword.Location = New System.Drawing.Point(126, 189)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(199, 22)
        Me.txtPassword.TabIndex = 1
        '
        'lblMode
        '
        Me.lblMode.AutoSize = True
        Me.lblMode.BackColor = System.Drawing.Color.Transparent
        Me.lblMode.ForeColor = System.Drawing.Color.DimGray
        Me.lblMode.Location = New System.Drawing.Point(60, 108)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(37, 15)
        Me.lblMode.TabIndex = 0
        Me.lblMode.Text = "Mode:"
        '
        'radLive
        '
        Me.radLive.AutoSize = True
        Me.radLive.BackColor = System.Drawing.Color.Transparent
        Me.radLive.Checked = True
        Me.radLive.ForeColor = System.Drawing.Color.DimGray
        Me.radLive.Location = New System.Drawing.Point(126, 106)
        Me.radLive.Name = "radLive"
        Me.radLive.Size = New System.Drawing.Size(45, 19)
        Me.radLive.TabIndex = 3
        Me.radLive.TabStop = True
        Me.radLive.Text = "Live"
        Me.radLive.UseVisualStyleBackColor = False
        '
        'radDemo
        '
        Me.radDemo.AutoSize = True
        Me.radDemo.BackColor = System.Drawing.Color.Transparent
        Me.radDemo.ForeColor = System.Drawing.Color.DimGray
        Me.radDemo.Location = New System.Drawing.Point(186, 106)
        Me.radDemo.Name = "radDemo"
        Me.radDemo.Size = New System.Drawing.Size(53, 19)
        Me.radDemo.TabIndex = 4
        Me.radDemo.Text = "Demo"
        Me.radDemo.UseVisualStyleBackColor = False
        '
        'lblLanguage
        '
        Me.lblLanguage.AutoSize = True
        Me.lblLanguage.BackColor = System.Drawing.Color.Transparent
        Me.lblLanguage.ForeColor = System.Drawing.Color.DimGray
        Me.lblLanguage.Location = New System.Drawing.Point(60, 134)
        Me.lblLanguage.Name = "lblLanguage"
        Me.lblLanguage.Size = New System.Drawing.Size(58, 15)
        Me.lblLanguage.TabIndex = 3
        Me.lblLanguage.Text = "Language:"
        '
        'cmbLanguage
        '
        Me.cmbLanguage.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmbLanguage.FormattingEnabled = True
        Me.cmbLanguage.Location = New System.Drawing.Point(126, 131)
        Me.cmbLanguage.Name = "cmbLanguage"
        Me.cmbLanguage.Size = New System.Drawing.Size(199, 23)
        Me.cmbLanguage.TabIndex = 5
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(250, 221)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnLogin
        '
        Me.btnLogin.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.btnLogin.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnLogin.ForeColor = System.Drawing.Color.White
        Me.btnLogin.Location = New System.Drawing.Point(169, 221)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(75, 25)
        Me.btnLogin.TabIndex = 6
        Me.btnLogin.Text = "Login"
        Me.btnLogin.UseVisualStyleBackColor = False
        '
        'lvwGames
        '
        Me.lvwGames.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvwGames.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader9, Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ReporterRole, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader10, Me.Languageid, Me.SerialNo})
        Me.lvwGames.FullRowSelect = True
        Me.lvwGames.HideSelection = False
        Me.lvwGames.Location = New System.Drawing.Point(12, 47)
        Me.lvwGames.Name = "lvwGames"
        Me.lvwGames.Size = New System.Drawing.Size(598, 184)
        Me.lvwGames.TabIndex = 11
        Me.lvwGames.UseCompatibleStateImageBehavior = False
        Me.lvwGames.View = System.Windows.Forms.View.Details
        Me.lvwGames.Visible = False
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "GameCode"
        Me.ColumnHeader9.Width = 0
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Kickoff"
        Me.ColumnHeader1.Width = 93
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Competition"
        Me.ColumnHeader2.Width = 118
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Home Team"
        Me.ColumnHeader3.Width = 108
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Away Team"
        Me.ColumnHeader4.Width = 103
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Tier"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Task"
        Me.ColumnHeader6.Width = 75
        '
        'ReporterRole
        '
        Me.ReporterRole.Text = "Reporter Role"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Channel"
        Me.ColumnHeader7.Width = 174
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Desk"
        Me.ColumnHeader8.Width = 141
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "ModuleID"
        Me.ColumnHeader10.Width = 0
        '
        'Languageid
        '
        Me.Languageid.Text = "Languageid"
        Me.Languageid.Width = 0
        '
        'SerialNo
        '
        Me.SerialNo.Text = "SerialNo"
        Me.SerialNo.Width = 0
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.sstMain.Location = New System.Drawing.Point(0, 291)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(622, 16)
        Me.sstMain.TabIndex = 264
        Me.sstMain.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Margin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 0)
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'picReporterBar
        '
        Me.picReporterBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-75, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(698, 21)
        Me.picReporterBar.TabIndex = 242
        Me.picReporterBar.TabStop = False
        Me.picReporterBar.Visible = False
        '
        'picTopBar
        '
        Me.picTopBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-75, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(698, 15)
        Me.picTopBar.TabIndex = 241
        Me.picTopBar.TabStop = False
        Me.picTopBar.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(17, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 265
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        Me.btnDelete.Visible = False
        '
        'btnAddGame
        '
        Me.btnAddGame.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnAddGame.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAddGame.ForeColor = System.Drawing.Color.White
        Me.btnAddGame.Location = New System.Drawing.Point(15, 8)
        Me.btnAddGame.Name = "btnAddGame"
        Me.btnAddGame.Size = New System.Drawing.Size(75, 25)
        Me.btnAddGame.TabIndex = 266
        Me.btnAddGame.Text = "Add game..."
        Me.btnAddGame.UseVisualStyleBackColor = False
        Me.btnAddGame.Visible = False
        '
        'lblTimeZone
        '
        Me.lblTimeZone.AutoSize = True
        Me.lblTimeZone.BackColor = System.Drawing.Color.Black
        Me.lblTimeZone.ForeColor = System.Drawing.Color.White
        Me.lblTimeZone.Location = New System.Drawing.Point(104, 265)
        Me.lblTimeZone.Name = "lblTimeZone"
        Me.lblTimeZone.Size = New System.Drawing.Size(0, 15)
        Me.lblTimeZone.TabIndex = 267
        '
        'lnkupcomming
        '
        Me.lnkupcomming.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkupcomming.AutoSize = True
        Me.lnkupcomming.BackColor = System.Drawing.Color.Transparent
        Me.lnkupcomming.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkupcomming.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lnkupcomming.LinkColor = System.Drawing.Color.Blue
        Me.lnkupcomming.Location = New System.Drawing.Point(440, 267)
        Me.lnkupcomming.Name = "lnkupcomming"
        Me.lnkupcomming.Size = New System.Drawing.Size(133, 15)
        Me.lnkupcomming.TabIndex = 268
        Me.lnkupcomming.TabStop = True
        Me.lnkupcomming.Text = "View upcoming games"
        '
        'radFeedB
        '
        Me.radFeedB.AutoSize = True
        Me.radFeedB.BackColor = System.Drawing.Color.Black
        Me.radFeedB.ForeColor = System.Drawing.Color.LimeGreen
        Me.radFeedB.Location = New System.Drawing.Point(81, 262)
        Me.radFeedB.Name = "radFeedB"
        Me.radFeedB.Size = New System.Drawing.Size(60, 19)
        Me.radFeedB.TabIndex = 269
        Me.radFeedB.TabStop = True
        Me.radFeedB.Text = "Feed B"
        Me.radFeedB.UseVisualStyleBackColor = False
        Me.radFeedB.Visible = False
        '
        'radFeedA
        '
        Me.radFeedA.AutoSize = True
        Me.radFeedA.BackColor = System.Drawing.Color.Black
        Me.radFeedA.ForeColor = System.Drawing.Color.LimeGreen
        Me.radFeedA.Location = New System.Drawing.Point(9, 262)
        Me.radFeedA.Name = "radFeedA"
        Me.radFeedA.Size = New System.Drawing.Size(69, 19)
        Me.radFeedA.TabIndex = 268
        Me.radFeedA.TabStop = True
        Me.radFeedA.Text = "Feed A   "
        Me.radFeedA.UseVisualStyleBackColor = False
        Me.radFeedA.Visible = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.Label1)
        Me.picButtonBar.Controls.Add(Me.btnAddGame)
        Me.picButtonBar.Controls.Add(Me.btnDelete)
        Me.picButtonBar.Location = New System.Drawing.Point(-2, 250)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(626, 60)
        Me.picButtonBar.TabIndex = 270
        Me.picButtonBar.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(207, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 15)
        Me.Label1.TabIndex = 284
        Me.Label1.Text = "Label1"
        Me.Label1.Visible = False
        '
        'cmbNoDays
        '
        Me.cmbNoDays.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmbNoDays.DisplayMember = "1"
        Me.cmbNoDays.FormattingEnabled = True
        Me.cmbNoDays.Items.AddRange(New Object() {"1", "5", "10", "15", "20", "30", "60", "90"})
        Me.cmbNoDays.Location = New System.Drawing.Point(435, 160)
        Me.cmbNoDays.Name = "cmbNoDays"
        Me.cmbNoDays.Size = New System.Drawing.Size(54, 23)
        Me.cmbNoDays.TabIndex = 269
        Me.cmbNoDays.Visible = False
        '
        'txtDays
        '
        Me.txtDays.AutoSize = True
        Me.txtDays.BackColor = System.Drawing.Color.Transparent
        Me.txtDays.ForeColor = System.Drawing.Color.OrangeRed
        Me.txtDays.Location = New System.Drawing.Point(350, 165)
        Me.txtDays.Name = "txtDays"
        Me.txtDays.Size = New System.Drawing.Size(82, 15)
        Me.txtDays.TabIndex = 268
        Me.txtDays.Text = "# of days back:"
        Me.txtDays.Visible = False
        '
        'grpDate
        '
        Me.grpDate.BackColor = System.Drawing.Color.Transparent
        Me.grpDate.Controls.Add(Me.cmbLeague)
        Me.grpDate.Controls.Add(Me.btnGo)
        Me.grpDate.Controls.Add(Me.lblTo)
        Me.grpDate.Controls.Add(Me.lblDateBetween)
        Me.grpDate.Controls.Add(Me.dtpTo)
        Me.grpDate.Controls.Add(Me.dtpFrom)
        Me.grpDate.Location = New System.Drawing.Point(13, 43)
        Me.grpDate.Name = "grpDate"
        Me.grpDate.Size = New System.Drawing.Size(598, 42)
        Me.grpDate.TabIndex = 284
        Me.grpDate.TabStop = False
        Me.grpDate.Visible = False
        '
        'cmbLeague
        '
        Me.cmbLeague.Enabled = False
        Me.cmbLeague.FormattingEnabled = True
        Me.cmbLeague.Location = New System.Drawing.Point(372, 13)
        Me.cmbLeague.Name = "cmbLeague"
        Me.cmbLeague.Size = New System.Drawing.Size(128, 23)
        Me.cmbLeague.TabIndex = 344
        Me.cmbLeague.Text = " "
        '
        'btnGo
        '
        Me.btnGo.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnGo.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGo.ForeColor = System.Drawing.Color.White
        Me.btnGo.Location = New System.Drawing.Point(518, 13)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(44, 23)
        Me.btnGo.TabIndex = 4
        Me.btnGo.Text = "Go"
        Me.btnGo.UseVisualStyleBackColor = False
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.ForeColor = System.Drawing.SystemColors.Desktop
        Me.lblTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblTo.Location = New System.Drawing.Point(236, 17)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(18, 15)
        Me.lblTo.TabIndex = 3
        Me.lblTo.Text = "to"
        '
        'lblDateBetween
        '
        Me.lblDateBetween.AutoSize = True
        Me.lblDateBetween.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateBetween.ForeColor = System.Drawing.SystemColors.Desktop
        Me.lblDateBetween.Location = New System.Drawing.Point(12, 17)
        Me.lblDateBetween.Name = "lblDateBetween"
        Me.lblDateBetween.Size = New System.Drawing.Size(115, 15)
        Me.lblDateBetween.TabIndex = 2
        Me.lblDateBetween.Text = "Date between from"
        '
        'dtpTo
        '
        Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTo.Location = New System.Drawing.Point(264, 13)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(89, 22)
        Me.dtpTo.TabIndex = 1
        '
        'dtpFrom
        '
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFrom.Location = New System.Drawing.Point(135, 13)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(89, 22)
        Me.dtpFrom.TabIndex = 0
        '
        'frmLogin
        '
        Me.AcceptButton = Me.btnLogin
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.LoginBackground
        Me.ClientSize = New System.Drawing.Size(622, 307)
        Me.Controls.Add(Me.grpDate)
        Me.Controls.Add(Me.cmbNoDays)
        Me.Controls.Add(Me.lnkupcomming)
        Me.Controls.Add(Me.txtDays)
        Me.Controls.Add(Me.radFeedB)
        Me.Controls.Add(Me.radFeedA)
        Me.Controls.Add(Me.lblTimeZone)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.cmbLanguage)
        Me.Controls.Add(Me.lblLanguage)
        Me.Controls.Add(Me.radDemo)
        Me.Controls.Add(Me.radLive)
        Me.Controls.Add(Me.lblMode)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.lblUsername)
        Me.Controls.Add(Me.txtUsername)
        Me.Controls.Add(Me.lvwGames)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Soccer Data Collection"
        Me.sstMain.ResumeLayout(False)
        Me.sstMain.PerformLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.picButtonBar.PerformLayout()
        Me.grpDate.ResumeLayout(False)
        Me.grpDate.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents radLive As System.Windows.Forms.RadioButton
    Friend WithEvents radDemo As System.Windows.Forms.RadioButton
    Friend WithEvents lblLanguage As System.Windows.Forms.Label
    Friend WithEvents cmbLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents lvwGames As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAddGame As System.Windows.Forms.Button
    Friend WithEvents Languageid As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblTimeZone As System.Windows.Forms.Label
    Friend WithEvents lnkupcomming As System.Windows.Forms.LinkLabel
    Friend WithEvents radFeedB As System.Windows.Forms.RadioButton
    Friend WithEvents radFeedA As System.Windows.Forms.RadioButton
    Friend WithEvents SerialNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents ReporterRole As System.Windows.Forms.ColumnHeader
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents cmbNoDays As System.Windows.Forms.ComboBox
    Friend WithEvents txtDays As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpDate As System.Windows.Forms.GroupBox
    Friend WithEvents cmbLeague As System.Windows.Forms.ComboBox
    Friend WithEvents btnGo As System.Windows.Forms.Button
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblDateBetween As System.Windows.Forms.Label
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker

End Class
