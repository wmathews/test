﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValidateScore
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtHomeScore = New System.Windows.Forms.TextBox()
        Me.txtAwayScore = New System.Windows.Forms.TextBox()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.lblHomeTeamName = New System.Windows.Forms.Label()
        Me.lblAwayTeamName = New System.Windows.Forms.Label()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtHomeScore
        '
        Me.txtHomeScore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtHomeScore.Location = New System.Drawing.Point(119, 55)
        Me.txtHomeScore.MaxLength = 2
        Me.txtHomeScore.Name = "txtHomeScore"
        Me.txtHomeScore.Size = New System.Drawing.Size(108, 22)
        Me.txtHomeScore.TabIndex = 289
        '
        'txtAwayScore
        '
        Me.txtAwayScore.Location = New System.Drawing.Point(119, 83)
        Me.txtAwayScore.MaxLength = 2
        Me.txtAwayScore.Name = "txtAwayScore"
        Me.txtAwayScore.Size = New System.Drawing.Size(108, 22)
        Me.txtAwayScore.TabIndex = 290
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-86, 15)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(378, 24)
        Me.picReporterBar.TabIndex = 288
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-86, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(385, 17)
        Me.picTopBar.TabIndex = 287
        Me.picTopBar.TabStop = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 159)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(249, 18)
        Me.sstMain.TabIndex = 286
        Me.sstMain.Text = "StatusStrip1"
        '
        'lblHomeTeamName
        '
        Me.lblHomeTeamName.AllowDrop = True
        Me.lblHomeTeamName.Location = New System.Drawing.Point(19, 58)
        Me.lblHomeTeamName.Name = "lblHomeTeamName"
        Me.lblHomeTeamName.Size = New System.Drawing.Size(96, 15)
        Me.lblHomeTeamName.TabIndex = 291
        Me.lblHomeTeamName.Text = "HomeTeamName "
        '
        'lblAwayTeamName
        '
        Me.lblAwayTeamName.AllowDrop = True
        Me.lblAwayTeamName.Location = New System.Drawing.Point(18, 86)
        Me.lblAwayTeamName.Name = "lblAwayTeamName"
        Me.lblAwayTeamName.Size = New System.Drawing.Size(95, 15)
        Me.lblAwayTeamName.TabIndex = 292
        Me.lblAwayTeamName.Text = "AwayTeamName "
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnOK.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOK.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.White
        Me.btnOK.Location = New System.Drawing.Point(69, 7)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(77, 25)
        Me.btnOK.TabIndex = 343
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(152, 7)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 345
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnCancel)
        Me.picButtonBar.Controls.Add(Me.btnOK)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 120)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(249, 39)
        Me.picButtonBar.TabIndex = 346
        '
        'frmValidateScore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(249, 177)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblAwayTeamName)
        Me.Controls.Add(Me.lblHomeTeamName)
        Me.Controls.Add(Me.txtHomeScore)
        Me.Controls.Add(Me.txtAwayScore)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmValidateScore"
        Me.Text = "Validate Score"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtHomeScore As System.Windows.Forms.TextBox
    Friend WithEvents txtAwayScore As System.Windows.Forms.TextBox
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents lblHomeTeamName As System.Windows.Forms.Label
    Friend WithEvents lblAwayTeamName As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
