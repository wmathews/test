﻿#Region " Imports "
Imports System.Configuration
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.IO
Imports System.Linq
Imports System.Drawing.Graphics
Imports System.Drawing.Imaging
Imports System.Drawing.Design
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmMain
' Author        :
' Created Date  :
' Description   : This form acts as a container for the entry forms of all soccer modules
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmMain

#Region " Constants & Variables "
    Private ReadOnly m_objUtility As New clsUtility
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Dim m_objclsLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGameSetup As clsGameSetup = clsGameSetup.GetInstance()
    Private m_objActionDetails As ClsActionDetails = ClsActionDetails.GetInstance()

    Private m_objUtilAudit As New clsAuditLog
    Private m_objclsLogin As New clsLogin
    Dim m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objSportVU As clsSportVU = clsSportVU.GetInstance()
    Dim v_startInfo As System.Diagnostics.ProcessStartInfo
    Private _objClsPbpTree As ClsPbpTree = ClsPbpTree.GetInstance()

    Private m_objCommentary As frmCommentary
    Public m_objModule1Main As frmModule1Main
    Public m_objModule1PBP As frmModule1PBP
    Public m_objActions As FrmActions
    Public m_objActionsAssist As FrmActionsAssist

    Public m_objModule2Score As frmModule2Score
    Private m_objTouches As frmTouches
    Private m_objValidateScore As frmValidateScore
    Dim objPenaltyShoot As frmPenaltyShootout = Nothing
    Private m_formClosed As Boolean = False
    Private m_countToMinWin As Integer = 0

    Private m_objOptFeed As frmOpticalFeed
    Private m_dsGames As New DataSet
    Private m_objKeyMoments As frmKeyMoments

    Private Const MENU_HEIGHT As Integer = 24
    Private Const HEADER_HEIGHT As Integer = 82
    Private Const FOOTER_HEIGHT As Integer = 60

    Private Const PBP_MODULE As Integer = 1
    Private Const MULTIGAME_PBP_MODULE As Integer = 2
    Private Const TOUCHES_MODULE As Integer = 3
    Private Const COMMENTARY_MODULE As Integer = 4

    Private Const INTGAMESTART As Integer = 34
    Private Const INTSTARTPERIOD As Integer = 21
    Private Const INTENDPERIOD As Integer = 13
    Private Const INTENDGAME As Integer = 10
    Private Const INTCLOCKSTART As Integer = 54
    Private Const INTCLOCKSTOP As Integer = 55

    Private Const FIRSTHALF As Integer = 45
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 90
    Private Const FIRSTEXTRA As Integer = 105
    Private Const SECONDEXTRA As Integer = 120

    Private Const FIRSTHALF_SEC As Integer = 2700
    Private Const EXTRATIME_SEC As Integer = 900
    Private Const SECONDHALF_SEC As Integer = 5400
    Private Const FIRSTEXTRA_SEC As Integer = 6300
    Private Const SECONDEXTRA_SEC As Integer = 7200

    'Private Const PRE_GAME_DESC As String = "Pre-Game"
    'Private Const FIRST_HALF_DESC As String = "1st Half"
    'Private Const HALFTIME_DESC As String = "Halftime"
    'Private Const SECOND_HALF_DESC As String = "2nd Half"
    'Private Const END_REGULATION_DESC As String = "End Reg"
    'Private Const END_GAME_DESC As String = "Final"
    'Private Const FIRST_EXTRATIME_DESC As String = "1st ET"
    'Private Const EXTRATIME_BREAK_DESC As String = "ET Break"
    'Private Const SECOND_EXTRATIME_DESC As String = "2nd ET"

    Dim PRE_GAME_DESC As String = "Pre-Game"
    Dim FIRST_HALF_DESC As String = "1st Half"
    Dim HALFTIME_DESC As String = "Halftime"
    Dim SECOND_HALF_DESC As String = "2nd Half"
    Dim END_REGULATION_DESC As String = "End Reg"
    Dim END_GAME_DESC As String = "Final"
    Dim FIRST_EXTRATIME_DESC As String = "1st ET"
    Dim EXTRATIME_BREAK_DESC As String = "ET Break"
    Dim SECOND_EXTRATIME_DESC As String = "2nd ET"

    Friend WithEvents FileToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents AddPlayerToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents AddOfficialToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents AddManagerToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents GameSetupToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents TeamSetupToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents SwitchSidesToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents PeriodToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents StartPeriodToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents EndPeriodToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents EndGameToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents PurgeToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As New ToolStripMenuItem

    Friend WithEvents RestartDataSyncComponentMenuItem As New ToolStripMenuItem

    Friend WithEvents DeleteRecordsToolStripMenuItem As New ToolStripMenuItem
    Friend WithEvents BoxScoreToolStripMenuItem As New ToolStripMenuItem

    Private m_dtStartTime As Date
    Private m_blnIncrement As Boolean
    Private m_IncrementUnit As udcRunningClock.IncrementUnit
    Shared objTeamStats As frmTeamStats = Nothing
    Shared objPlayerTouches As frmPlayerTouches = Nothing
    Shared objBoxScore As frmBoxScore = Nothing
    Shared objpbpreport As frmPBPReport = Nothing
    Shared objPlayerStats As frmPlayerStats = Nothing
    Shared objformationreport As frmFormationReport = Nothing
    Shared objHotKeys As frmHotKeys = Nothing
    Private MessageDialog As New frmMessageDialog
    Private lsupport As New languagesupport

    Dim strmessage As String = "Last Entry in Process, Please Wait..."
    Dim strmessage1 As String = "You are not allowed to Insert/Update Events - Primary Reporter is Down!"
    Dim strmessage2 As String = "Replace with Manager Expulsion is not allowed!"
    Dim strmessage3 As String = "Time change not allowed - complete the current operation first!"
    Dim strmessage5 As String = "Comments inserted succesfully!"
    Dim strmessage6 As String = "Complete the current operation first!"
    Dim strmessage7 As String = "Replace with Comment is not allowed!"
    Dim strmessage8 As String = "Are you sure want to go for Penalty Shootout?"

    Dim strmessage10 As String = "You are removed from the assigned game. Please contact Operation desk."
    Dim strmessage11 As String = "Last Entry is Rejected"
    Dim strmessage12 As String = "Game is Abandoned, you are not allowed to enter any events!"
    Dim strmessage13 As String = "You have not ended the game - Press OK to stay in the"
    Dim strmessage14 As String = "application or CANCEL to close it without ending the game?"
    'Dim strmessage15 As String = "You are removed from the assigned game. Please contact Operation desk."
    Dim strmessage16 As String = "Save or Cancel this operation to proceed next screen!"
    Dim strmessage17 As String = "Do you want to Abandon the Match?"
    Dim strmessage18 As String = "End Period not allowed - complete the current operation first!"
    Dim strmessage19 As String = "Are you sure you want to end the half?"
    Dim strmessage20 As String = "Are you sure you want to start the period?"
    Dim strmessage21 As String = "Are you sure you want to start the game?"
    Dim strmessage22 As String = " Enter Home Start [Game Setup]!"
    Dim strmessage23 As String = "Cannot set the time below last event time!"
    Dim strmessage24 As String = " You are about to exit. Are you sure ?"
    Dim strmessage25 As String = "System has found few events that are yet to be processed. "
    Dim strmessage26 As String = "Please wait for a minute before closing the application. "
    Dim strmessage27 As String = "Do you want to close the application ?"
    Dim strmessage28 As String = "As a assister reporter you can't START a period."
    Dim strmessage29 As String = "As a assister reporter you can't END a Period or Game"
    Dim strmessage30 As String = "Click YES to go to detailed Shootout screen." & vbNewLine & "Click NO to enter Shootout scores."
    Dim strmessage31 As String = "This option not available for Tier 2 games!"

    Dim strmessage33 As String = "Are you sure to Pause the Clock? Press OK to continue..."
    Dim strmessage34 As String = "Play by Play"
    Dim strmessage35 As String = "Clock"
    Dim strmessage36 As String = "Clock edit is not allowed!"
    Dim strmessage37 As String = "You are not allowed to enter start half, as already penalty shootouts are entered!"
    Dim strmessage38 As String = "Enter Home Start for TOUCHES module!"
    Dim strmessage39 As String = "Score Tied - Do you really want to start Extra Time?"
    Dim strmessage42 As String = "Do you want to end the game ?"
    Dim strmessage43 As String = "Do you want to enter the player ratings ?"
    Dim strmessage44 As String = "Do you want to enter the match rating ?"
    Dim strmessage45 As String = "As a assister reporter you can't ABANDONED the game."
    Dim strmessage46 As String = "Please enter the time first!"
    Dim strmessage47 As String = "Enter Abandon from Play by Play Screen (PBP)!"
    Dim strmessage48 As String = "Game Over? Press YES to end the game, NO to continue in extra time/penalty shootout!"
    Dim strmessage49 As String = "You ended the game without selecting a Head Referee and Attendance, please enter if available!"
    Dim strmessage50 As String = "You ended the game without selecting a Head Referee, please enter if available!"
    Dim strmessage51 As String = "You ended the game without selecting Attendance, please enter if available!"
    Dim strmessage52 As String = " Enter Home Start for Extra Time [Game Setup]!"
    Dim strmessage53 As String = "Enter Home Start (Extra Time) for TOUCHES module!"
    Dim strmessage54 As String = "Cannot enter new delay - please edit the existing delay event!"

    'Rm 8695
    Dim strmessage55 As String = "Game is in delay, resume the game(Others-> Resume Delay) to continue."
    Dim strmessage56 As String = "You are about to enter a Injury Time – are you sure?"
    Dim strmessage57 As String = "You are about to resume the game from delay - are you sure?"
    Dim strmessage58 As String = "You are about to enter a delay – are you sure?"
    Dim strmessage59 As String = "Injury time already exists for this Period.Please check."
    Dim strmessage60 As String = "Are you sure you want to Finalize?"
    Dim strmessage61 As String = "Successfully Finalized."
    Dim strmessage62 As String = "You have not finalized your data. Are you sure you want to exit without finalizing?"
    Dim strmessage63 As String = "You are about to enter a Game Interruption – are you sure?"
    Dim strmessage64 As String = "As a assister reporter you can't enter a match DELAY."
    Dim strmessage65 As String = "As a assister reporter you can't enter GAME INTERRUPTION."
    Dim strmessage66 As String = "As a assister reporter you can't switch side."
    Private ReadOnly _goalEvents() As Integer = {11, 28, 17}
    Private ReadOnly _stoppageEvents() As Integer = {58, 59, 65}
#End Region

#Region " Event handlers "

    Private Sub frmMain_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If m_objGameDetails.ModuleID = 3 Then
                If e.Control And e.KeyCode = 83 And Not m_objGameDetails.IsEndGame And isMenuItemEnabled("StartPeriodToolStripMenuItem") = True Then
                    ProzoneMenu_Click("StartPeriodToolStripMenuItem")
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objGameDetails.AppPath =System.AppDomain.CurrentDomain.BaseDirectory.Substring(0, 3)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmMain", Nothing, 1, 0)
            Dim objLogin As New frmLogin
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Application.DoEvents()
            If objLogin.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                Me.Close()
                Exit Sub
            End If
            Application.DoEvents()
            m_objGameDetails.PBP = AddPBPColumns()
            SetMainFormMenu()
            SetChildForm()
            SetMainFormSize()
            SetPeriod()
            InitializeClock()
            FillFormControls()
            GetPenaltyShootoutScore()

            'SaveTeamlogos()   TOSOCRS-338
            If CInt(m_objGameDetails.languageid) <> 1 Then
                lblLanguage.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLanguage.Text)
                lklHometeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lklHometeam.Text)
                lblStatsConnection.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblStatsConnection.Text)
                lblStatsConnectionStatus.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblStatsConnectionStatus.Text)
                Label17.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label17.Text)
                lblSportVu.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblSportVu.Text)
                lklAwayteam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lklAwayteam.Text)
                lblPeriod.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblPeriod.Text)
                lblMode.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblMode.Text)
                lblReporter.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblReporter.Text)
                btnGotoPBP.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnGotoPBP.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)
                strmessage8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage8)

                strmessage10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage10)
                strmessage11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage11)
                strmessage12 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage12)
                strmessage13 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage13)
                strmessage14 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage14)
                strmessage16 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage16)
                strmessage17 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage17)
                strmessage18 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage18)
                strmessage19 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage19)

                strmessage20 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage20)
                strmessage21 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage21)
                strmessage22 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage22)
                strmessage23 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage23)
                strmessage24 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage24)
                strmessage25 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage25)
                strmessage26 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage26)
                strmessage27 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage27)
                strmessage30 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage30)
                strmessage31 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage31)
                'strmessage32 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage32)
                strmessage33 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage33)
                strmessage34 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage34)
                strmessage35 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage35)
                strmessage36 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage36)
                strmessage37 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage37)
                strmessage38 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage38)
                strmessage38 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage38)
                strmessage39 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage39)
                strmessage42 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage42)
                strmessage43 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage43)

                strmessage44 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage44)
                strmessage45 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage45)
                strmessage46 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage46)
                strmessage47 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage47)
                strmessage48 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage48)
                strmessage49 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage49)
                strmessage50 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage50)
                strmessage51 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage51)
                strmessage52 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage52)
                strmessage53 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage53)
                strmessage54 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage54)
                strmessage55 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage55)
                strmessage56 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage56)
                strmessage57 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage57)
                strmessage58 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage58)
                strmessage59 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage59)
                strmessage60 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage60)
                strmessage61 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage61)
                strmessage62 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage62)
                strmessage63 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage63)
                strmessage64 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage64)
                strmessage65 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage65)
                strmessage66 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage66)
            End If

            SetMenuItemsforDifferentTiers()
            SetMenuItemsforModule2Score()

            If m_objGameDetails.IsDemoGameSelected Then
                lblStatsConnection.Visible = False
                Label17.Visible = False
                lblStatsConnectionStatus.Visible = False
                lblSportVu.Visible = False
            End If

            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE And m_objGameDetails.IsNewDemomode = False Then
                LoadSoccerDataSync()
            ElseIf m_objGameDetails.IsNewDemomode = True Then
                lblReporter.Text = "Demo Reporter"
                lblMode.Text = "Demo Mode"

                m_objGameDetails.FeedNumber = 0
                EnableMenuItem("OpenToolStripMenuItem", False)
            End If

            'T1 WORKFLOW
            If m_objGameDetails.ModuleID <> COMMENTARY_MODULE Then
                If m_objGameDetails.CoverageLevel = 1 Then
                    If m_objGameDetails.IsRestartGame = True Then
                        m_objModule2Score.RestartExistingGame()
                        pnlEntryForm.Enabled = True
                        btnClock.Enabled = True
                        m_objGameDetails.IsRestartGame = False
                        m_objGameDetails.RestartData.Tables.Clear()
                    End If
                Else
                    If m_objGameDetails.IsRestartGame = True Then
                        m_objModule1Main.RestartExistingGame()
                        pnlEntryForm.Enabled = True
                        btnClock.Enabled = True
                        m_objGameDetails.IsRestartGame = False
                        m_objGameDetails.RestartData.Tables.Clear()
                    End If
                End If
            End If

            If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                'REFRESHING THE PBP EVENTS IN THE LISTBOXES
                If Not m_objModule1Main Is Nothing Then
                    m_objModule1Main.FetchMainScreenPBPEvents()
                    pnlEntryForm.Enabled = True
                End If
                Label17.Visible = False
                lblSportVu.Visible = False
            End If

            'T1 WORKFLOW FOR COMMENTARY
            If m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                If m_objGameDetails.IsRestartGame = True Then
                    m_objCommentary.RestartExistingGame()
                    pnlEntryForm.Enabled = True
                    btnClock.Enabled = True
                    ResetMenuItems()
                    m_objGameDetails.IsRestartGame = False
                    m_objGameDetails.RestartData.Tables.Clear()
                End If
            End If

            If (m_objGameDetails.CoverageLevel <> 1 And m_objGameDetails.ModuleID = 1) Then
                m_objModule1Main.ArrangeListViews()
            End If

            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                picRptr1.Visible = False
                picRptr2.Visible = False
                picRptr3.Visible = False
                picRptr4.Visible = False
                picRptr5.Visible = False
                picRptr6.Visible = False
            End If

            If m_objGameDetails.IsSportVUAvailable = True And m_objGameDetails.ModuleID = PBP_MODULE Then
                m_objModule1PBP.picOpticalFeed.Visible = True
                m_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-OFF.gif")
            Else
                If Not m_objModule1PBP Is Nothing Then
                    m_objModule1PBP.picOpticalFeed.Visible = False
                End If
            End If

            If m_objGameDetails.ModuleID = 3 Then
                m_objActionDetails.PzPBP = m_objGeneral.RefreshPZPbpData(0, 0, 0, 0, False, 0)
                If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                    m_objActions = New FrmActions
                Else
                    m_objActionsAssist = New FrmActionsAssist
                    m_objGameDetails.IsRestartGame = True
                    If m_objGameDetails.ReporterRoleSerial = 3 Or m_objGameDetails.ReporterRoleSerial = 4 Then
                        m_objActionsAssist.chkOwnAction.Checked = True
                        m_objActionsAssist.chkOwnAction.BringToFront()
                        m_objActionsAssist.chkHome.Visible = False
                        m_objActionsAssist.chkAway.Visible = False
                        m_objActionsAssist.btnReplace.Visible = False
                    Else
                        m_objActionsAssist.chkOwnAction.Visible = False
                        m_objActionsAssist.chkHome.Visible = True
                        m_objActionsAssist.chkAway.Visible = True
                        m_objActionsAssist.btnReplace.Visible = True
                        m_objActionsAssist.chkHome.BringToFront()
                        m_objActionsAssist.chkAway.BringToFront()
                    End If
                    SetClockControlVisibility()
                    EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                    EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                End If
                'TOPZ-812
                EnableMenuItem("CommentToolStripMenuItem", True)
                EnableMenuItem("SetTeamColorToolStripMenuItem", True)
                EnableMenuItem("HomeTeamColorToolStripMenuItem", True)
                EnableMenuItem("VisitorTeamColorToolStripMenuItem", True)
                tmrRefreshGameInfo.Enabled = True
                m_objModule1Main.EnableLinkLabels(False)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            m_objGameDetails.IsRestartGame = False
        End Try
    End Sub

    Public Sub SetMenuItemsforDifferentTiers()
        Try
            SetAllMenuItemsVisbleTrue()
            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Or m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                'DEMO MODE
                If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    'TEAM SETUP INFO
                    m_objclsLoginDetails.strSortOrder = "Last Name"
                    m_objModule1Main.GetCurrentPlayersAfterRestart()
                    If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                        If Not m_objModule1Main Is Nothing Then
                            SortPlayers()
                            m_objModule1Main.LabelsTagged()
                        End If
                    End If
                    'GAME SETUP INFO
                    If Not m_objModule1Main Is Nothing Then
                        m_objModule1Main.InsertOfficialsandMatchInfo()
                    End If
                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                    EnableMenuItem("EndGameToolStripMenuItem", False)
                Else
                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                    EnableMenuItem("EndGameToolStripMenuItem", False)
                End If
            ElseIf m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                '[TOSOCRS-62] Touch Reporter 2/3-End Game Warning
                If (m_objGameDetails.ModuleID = TOUCHES_MODULE And (m_objGameDetails.SerialNo = 2 Or m_objGameDetails.SerialNo = 3)) Then
                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                Else
                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                End If
                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                EnableMenuItem("EndGameToolStripMenuItem", False)
            End If

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                If Not m_objModule1Main Is Nothing Then
                    EnableMenuItem("SetTeamColorToolStripMenuItem", False)
                    EnableMenuItem("HomeTeamColorToolStripMenuItem", False)
                    EnableMenuItem("VisitorTeamColorToolStripMenuItem", False)

                Else
                    EnableMenuItem("SetTeamColorToolStripMenuItem", True)
                    EnableMenuItem("HomeTeamColorToolStripMenuItem", True)
                    EnableMenuItem("VisitorTeamColorToolStripMenuItem", True)
                End If
            End If
            'ADDED BY SHRAVANI
            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                    EnableMenuItem("OpenToolStripMenuItem", True)
                Else
                    If m_objGameDetails.IsPrimaryReporter Or m_objGameDetails.ModuleID = 2 Or m_objGameDetails.ModuleID = 4 Then
                        EnableMenuItem("OpenToolStripMenuItem", False)
                    Else
                        EnableMenuItem("OpenToolStripMenuItem", True)
                    End If
                End If
            Else
                EnableMenuItem("OpenToolStripMenuItem", False)
            End If

            'ALL THESE ARE DISABLED TILL FUNCTIONALITIES ARE IMPLEMENTED!!!!
            EnableMenuItem("ViewToolStripMenuItem", True)
            EnableMenuItem("EditToolStripMenuItem", False)
            EnableMenuItem("PurgeAuditTrailToolStripMenuItem", True)
            If m_objGameDetails.IsRestartGame = False Then
                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
            End If

            'EnableMenuItem("PlayerTouchesToolStripMenuItem", False)

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                EnableMenuItem("CommentToolStripMenuItem", True)
            Else
                EnableMenuItem("CommentToolStripMenuItem", False)
            End If

            If (m_objGameDetails.CoverageLevel <> 3) Then
                EnableMenuItem("InputtingteamstatsToolStripMenuItem ", False)
            Else
                EnableMenuItem("InputtingteamstatsToolStripMenuItem ", True)
            End If

            'Added by Shravani
            If m_objGameDetails.CoverageLevel = 2 Then
                EnableMenuItem("TeamSetupToolStripMenuItem", False)
                EnableMenuItem("SortPlayersToolStripMenuItem", False)
                EnableMenuItem("SortByUniformToolStripMenuItem", False) 'Switch Side
                EnableMenuItem("SortByLastNameToolStripMenuItem", False) 'Player Sort
                EnableMenuItem("SortByPositionToolStripMenuItem", False)
                EnableMenuItem("SwitchSidesToolStripMenuItem", False)

                EnableMenuItem("InputTeamStatsToolStripMenuItem", False) 'Switch Side
                EnableMenuItem("ReportFormationToolStripMenuItem", False) 'Player Sort
                EnableMenuItem("AddOfficialToolStripMenuItem", False)
                EnableMenuItem("AddManagerToolStripMenuItem", False)

                EnableMenuItem("PlayerStatsToolStripMenuItem", False)
            End If
            If m_objGameDetails.ModuleID = PBP_MODULE Then
                ShowMenuItem("RatingsToolStripMenuItem", True)
                EnableMenuItem("RatingsToolStripMenuItem", False)
            Else
                ShowMenuItem("RatingsToolStripMenuItem", False)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ResetMenuItems()
        Try
            If m_objGameDetails.CurrentPeriod = 0 Then
                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                EnableMenuItem("EndPeriodToolStripMenuItem", False)
            Else
                EnableMenuItem("StartPeriodToolStripMenuItem", False)
                EnableMenuItem("EndPeriodToolStripMenuItem", True)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To check whether unprocessed records are existing in local database
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If (m_objGameDetails.IsNewDemomode = True) Then
                Exit Sub
            End If
            'TOSOCRS-159
            If Not m_objModule1PBP Is Nothing And m_objGameDetails.IsReporterRemoved = False Then
                If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    MessageDialog.Show(strmessage6, "Close", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    e.Cancel = True
                    Exit Sub
                End If
            End If
            'FPLRS-139	
            Dim networkConnection = m_objGeneral.CheckNetConnection()

            If networkConnection = False Then
                KillDataSync()
                Exit Try
            End If

            Dim v_strFilepath As String
            Dim v_arrProcess() As System.Diagnostics.Process
            v_strFilepath = Application.StartupPath
            v_strFilepath += "\Resources\STATS.SoccerDataSync.exe"
            If File.Exists(v_strFilepath) Then
                v_strFilepath = Path.GetFileNameWithoutExtension(v_strFilepath)
                v_arrProcess = Process.GetProcessesByName(v_strFilepath)
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.FormClosing, "Form Closing", 1, 1)

                If v_arrProcess.Length > 0 Then
                    If Convert.ToInt32(m_objGeneral.GetUnprocessedRecords(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)) > 0 Then
                        If MessageDialog.Show(strmessage25 + "" + strmessage26 + strmessage27, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                            'audit file transfer code if it is pointing to Production
                            auditFTPTranfer()

                            KillDataSync()
                            m_objGeneral.DeleteReporterExistance(m_objclsLoginDetails.UserId)
                            If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                m_objclsLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                            End If
                            e.Cancel = False
                        Else
                            e.Cancel = True
                            m_formClosed = False
                        End If
                    Else
                        Dim endGameCount As DataSet
                        endGameCount = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        If CInt(endGameCount.Tables(0).Rows(0).Item("endGameCount")) = 0 And m_formClosed = False And m_objGameDetails.CurrentPeriod > 0 Then

                            If m_objGameDetails.ModuleID = 3 Or m_objGameDetails.ModuleID = 4 Then '----Module condition
                                '----------------------------------------------------------------
                                If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                                    If MessageDialog.Show(strmessage24, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                                        'audit file transfer code if it is pointing to Production
                                        auditFTPTranfer()

                                        KillDataSync()
                                        m_objGeneral.DeleteReporterExistance(m_objclsLoginDetails.UserId)
                                        If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                            m_objclsLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                                        End If
                                        e.Cancel = False
                                    Else
                                        e.Cancel = True
                                        m_formClosed = False
                                    End If
                                Else
                                    Select Case m_objGameDetails.ModuleID
                                        Case 3
                                            Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                                                'TOSOCRS-62 Touch Reporter 2/3-End Game Warning
                                                Case "1"
                                                    If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                                                    Else
                                                        If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                                            e.Cancel = True
                                                            m_formClosed = False
                                                            Exit Sub
                                                        End If
                                                    End If
                                                Case "3", "4"
                                                    'TOSOCRS-29 Exit Warning for Unfinalized Touches
                                                    If Not m_objGameDetails.IsTouchFinalized Then
                                                        If MessageDialog.Show(strmessage62, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                                            e.Cancel = True
                                                            m_formClosed = False
                                                            Exit Sub
                                                        End If
                                                    End If
                                            End Select
                                        Case 4
                                            If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                                            Else
                                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                                    e.Cancel = True
                                                    m_formClosed = False
                                                    Exit Sub
                                                End If
                                            End If
                                    End Select
                                    If MessageDialog.Show(strmessage24, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                                        'audit file transfer code if it is pointing to Production
                                        auditFTPTranfer()

                                        KillDataSync()
                                        m_objGeneral.DeleteReporterExistance(m_objclsLoginDetails.UserId)
                                        If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                            m_objclsLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                                        End If
                                        e.Cancel = False
                                    Else
                                        e.Cancel = True
                                        m_formClosed = False
                                    End If
                                End If
                                '--------------------------------------------------------------------------
                            Else
                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                    e.Cancel = True
                                Else
                                    If MessageDialog.Show(strmessage24, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                                        'audit file transfer code if it is pointing to Production
                                        auditFTPTranfer()

                                        KillDataSync()
                                        m_objGeneral.DeleteReporterExistance(m_objclsLoginDetails.UserId)
                                        If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                            m_objclsLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                                        End If
                                        e.Cancel = False
                                    Else
                                        e.Cancel = True
                                        m_formClosed = False
                                    End If
                                End If
                            End If

                        Else
                            If m_objGameDetails.IsReporterRemoved = True Then

                                'audit file transfer code if it is pointing to Production
                                auditFTPTranfer()

                                KillDataSync()
                                m_objGeneral.DeleteReporterExistance(m_objclsLoginDetails.UserId)
                                If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                    m_objclsLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                                End If
                                e.Cancel = False
                            Else
                                If MessageDialog.Show(strmessage24, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                                    'audit file transfer code if it is pointing to Production
                                    auditFTPTranfer()
                                    KillDataSync()
                                    m_objGeneral.DeleteReporterExistance(m_objclsLoginDetails.UserId)
                                    If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                        m_objclsLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                                    End If
                                    e.Cancel = False
                                Else
                                    e.Cancel = True
                                    m_formClosed = False
                                End If
                            End If
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
            KillDataSync()
        End Try
    End Sub

    Private Sub auditFTPTranfer()
        Try
            Dim strServerName As String = "Web Server"
            strServerName = ConfigurationSettings.AppSettings("Environment")
            If (strServerName = "UATRAC" Or strServerName = "PRODUCTION") And m_objGameDetails.GameCode > 0 Then
                'AUDIT TRIAL UPLOAD
                FTPHelper.Host = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("AuditHost"))
                FTPHelper.UserName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("AuditMoveUser"))
                FTPHelper.Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("AuditMovePwd"))

                m_objGameDetails.AuditDirectory = m_objUtilAudit.CreateAuditLogDirectory
                If m_objGameDetails.AuditDirectory <> "" Then
                    'm_objGameDetails.AuditFileName = m_objUtilAudit.CreateAuditLogFileName(m_objGameDetails.AuditDirectory)
                    Dim directoryPath = New DirectoryInfo(m_objGameDetails.AuditDirectory)
                    Dim from_date As DateTime = DateTime.Now.AddHours(-5)
                    Dim to_date As DateTime = DateTime.Now
                    Dim files = directoryPath.GetFiles().Where(Function(file) file.LastWriteTime >= from_date AndAlso file.LastWriteTime <= to_date)

                    For Each LogFile In files
                        m_objGameDetails.AuditFileName = LogFile.FullName
                        If m_objGameDetails.AuditFileName <> "" Then
                            If Directory.Exists(m_objGameDetails.AuditDirectory) Then
                                If File.Exists(m_objGameDetails.AuditFileName) Then
                                    FTPHelper.FileUpload(m_objGameDetails.AuditDirectory, LogFile.Name, FTPHelper.FileDel.No)
                                End If
                            End If
                        End If
                    Next
                End If
                'ERROR LOG FILE UPLOAD
                Dim path As String
                Dim fileName As String = ""
                path = m_objGameDetails.AppPath
                path = path + "\SoccerDataCollection\ErrorLog"

                If (m_objGameDetails.GameCode > 0) Then
                    If Directory.Exists(path) Then
                        Dim directoryPath = New DirectoryInfo(path)
                        Dim from_date As DateTime = DateTime.Now.AddHours(-5)
                        Dim to_date As DateTime = DateTime.Now
                        Dim files = directoryPath.GetFiles().Where(Function(file) file.LastWriteTime >= from_date AndAlso file.LastWriteTime <= to_date)
                        For Each LogFile In files
                            If File.Exists(LogFile.FullName) Then
                                FTPHelper.FileUpload(path, LogFile.Name, FTPHelper.FileDel.No)
                            End If
                        Next

                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, 1, 1)

            If Not m_objModule1PBP Is Nothing Then
                If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    MessageDialog.Show(strmessage6, "Close", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
            End If

            Select Case m_objGameDetails.ModuleID
                Case 3
                    Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                        'TOSOCRS-62 Touch Reporter 2/3-End Game Warning
                        Case "1"
                            If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                            Else
                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                    Exit Sub
                                End If
                            End If
                    End Select
                    'TOSOCRS-29 Exit Warning for Unfinalized Touches
                    If m_objGameDetails.ModuleID = 3 Then
                        Select Case m_objGameDetails.ReporterRoleSerial
                            Case 3, 4
                                If Not m_objGameDetails.IsTouchFinalized Then
                                    If MessageDialog.Show(strmessage62, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                        Exit Sub
                                    End If
                                End If
                        End Select
                    End If
                Case 4
                    If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                    Else
                        If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                            Exit Sub
                        End If
                    End If
                Case Else
                    Dim endGameCount As DataSet
                    endGameCount = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    If CInt(endGameCount.Tables(0).Rows(0).Item("endGameCount")) = 0 And m_objGameDetails.CurrentPeriod > 0 Then
                        If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                            Exit Sub
                        End If
                    End If
            End Select
            m_formClosed = True
            Me.Close()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnGotoPBP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGotoPBP.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnGotoPBP.Text, 1, 0)
            btnGotoPBP.Visible = False
            btnGotoMain.Visible = True
            If m_objGameDetails.ModuleID = 3 Then
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.ActionsForm
                ActionsChildForm()
            Else
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                SetChildForm()
            End If
            SetMainFormSize()
            SetHeader2()

            If m_objGameDetails.ModuleID <> 3 Then
                If m_objGameDetails.CoverageLevel = 2 Then
                    EnableMenuItem("TeamSetupToolStripMenuItem", False)
                    EnableMenuItem("SortPlayersToolStripMenuItem", False)
                    EnableMenuItem("SortByUniformToolStripMenuItem", False) 'Switch Side
                    EnableMenuItem("SortByLastNameToolStripMenuItem", False) 'Player Sort
                    EnableMenuItem("SortByPositionToolStripMenuItem", False)
                    EnableMenuItem("SwitchSidesToolStripMenuItem", False)

                    EnableMenuItem("InputTeamStatsToolStripMenuItem", False) 'Switch Side
                    EnableMenuItem("ReportFormationToolStripMenuItem", False) 'Player Sort
                    EnableMenuItem("AddOfficialToolStripMenuItem", False)
                    EnableMenuItem("AddManagerToolStripMenuItem", False)
                    EnableMenuItem("PlayerStatsToolStripMenuItem", False)
                End If
                If Not m_objclsTeamSetup.RosterInfo Is Nothing Then
                    If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                        SortPlayers()
                    End If
                End If
                If (m_objGameDetails.CurrentPeriod = 0 And (m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE)) Then
                    Dim dsPBP As DataSet
                    dsPBP = m_objGeneral.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
                    If dsPBP.Tables(0).Rows.Count > 0 Then
                        pnlEntryForm.Enabled = True
                    Else
                        'PZ changes
                        If m_objGameDetails.CoverageLevel = 6 Then
                            pnlEntryForm.Enabled = True
                        Else
                            pnlEntryForm.Enabled = False
                        End If
                    End If
                End If
                'RM 8695
                If (m_objGameDetails.ModuleID = PBP_MODULE And m_objGameDetails.CoverageLevel >= 4) Then
                    Dim dsPBP As DataSet
                    dsPBP = m_objGeneral.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    If dsPBP.Tables(0).Rows.Count > 0 Then
                        If CInt(dsPBP.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = 59 Then
                            EnableMenuItem("DelayedToolStripMenuItem", False)
                            EnableMenuItem("DelayOverToolStripMenuItem", True)

                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.DisableAllControls(False)
                                m_objModule1PBP.btnBookingAway.Enabled = False
                                m_objModule1PBP.btnBookingHome.Enabled = False
                                m_objModule1PBP.btnSubstituteAway.Enabled = False
                                m_objModule1PBP.btnSubstituteHome.Enabled = False
                            End If
                        End If
                    End If
                End If
                If m_objGameDetails.IsSportVUAvailable = True And m_objGameDetails.ModuleID = PBP_MODULE Then
                    'If Not m_objModule1PBP Is Nothing Then
                    m_objModule1PBP.picOpticalFeed.Visible = True
                    m_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-OFF.gif")
                    'End If
                Else
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.picOpticalFeed.Visible = False
                    End If
                End If
                EnableMenuItem("SetTeamColorToolStripMenuItem", True)
                EnableMenuItem("HomeTeamColorToolStripMenuItem", True)
                EnableMenuItem("VisitorTeamColorToolStripMenuItem", True)
                If m_objGameDetails.ModuleID = 2 Then
                    EnableMenuItem("AbandonToolStripMenuItem", True)
                    EnableMenuItem("DelayedToolStripMenuItem", True)
                End If
                GetPenaltyShootoutScore()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnGotoMain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGotoMain.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnGotoMain.Text, 1, 0)
            If Not m_objModule1PBP Is Nothing Then
                If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    MessageDialog.Show(strmessage16, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
            End If
            btnGotoPBP.Visible = True
            btnGotoMain.Visible = False

            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.MainForm
            If m_objGameDetails.ModuleID = 3 Then
                ActionsChildForm()
            Else
                SetChildForm()
            End If

            SetMainFormSize()
            pnlEntryForm.Enabled = True

            If Me.m_objGameDetails.CoverageLevel <> 6 Then
                'ONCE THE PLAYERS ARE FILLED IN STARTING LINEUPS, THE MAIN FORM PLAYERS ARE REFRESHED ..
                If Not m_objclsTeamSetup.RosterInfo Is Nothing Then
                    If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                        m_objModule1Main.GetCurrentPlayersAfterRestart()
                        SortPlayers()
                        m_objModule1Main.LabelsTagged()
                    End If
                End If
                'REFRESHING THE PBP EVENTS IN THE LISTBOXES
                m_objModule1Main.FetchMainScreenPBPEvents()
                m_objModule1Main.InsertOfficialsandMatchInfo()
                If isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False Then

                    m_objModule1Main.lklNewHomeBooking.Enabled = False
                    m_objModule1Main.lklNewHomeGoal.Enabled = False
                    m_objModule1Main.lklNewHomePenalty.Enabled = False
                    m_objModule1Main.lklNewHomeSub.Enabled = False

                    m_objModule1Main.lklNewVisitBooking.Enabled = False
                    m_objModule1Main.lklNewVisitGoal.Enabled = False
                    m_objModule1Main.lklNewVisitPenalty.Enabled = False
                    m_objModule1Main.lklNewVisitSub.Enabled = False
                    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                        m_objModule1Main.lklOfficials.Enabled = False
                        m_objModule1Main.lklMatchInfo.Enabled = False
                    End If
                End If
                EnableMenuItem("SetTeamColorToolStripMenuItem", False)
                EnableMenuItem("HomeTeamColorToolStripMenuItem", False)
                EnableMenuItem("VisitorTeamColorToolStripMenuItem", False)
                If m_objGameDetails.ModuleID = 2 Then
                    EnableMenuItem("AbandonToolStripMenuItem", True)
                    EnableMenuItem("DelayedToolStripMenuItem", True)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClock.Click
        Dim actionsScreen As Boolean
        Try
            If m_objGameDetails.CurrentPeriod = 0 Then
                Exit Sub
            End If
            If IsClockEntryAllowed() = False Then
                Exit Sub
            End If
            If Not m_objModule1PBP Is Nothing Then
                If m_objModule1PBP.dblstEventDetails01.Visible = True Or m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                    MessageDialog.Show(strmessage6, strmessage34, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
            End If

            If Not m_objTouches Is Nothing Then
                If m_objTouches.txtTime.Text.Replace(" :", "").Trim <> "" Then
                    MessageDialog.Show(strmessage6, strmessage34, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
            End If

            If (m_objActions IsNot Nothing) And (m_objActionDetails.IsEditMode = False) Then
                actionsScreen = True
            End If

            If UdcRunningClock1.URCIsRunning Then
                If MessageDialog.Show(strmessage33, strmessage35, MessageDialogButtons.OKCancel, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Cancel Then
                    Exit Sub
                End If
            End If

            UdcRunningClock1.URCToggle()
            If UdcRunningClock1.URCIsRunning Then
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                If Not m_objModule1Main Is Nothing Then
                    m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_START, "")
                ElseIf Not m_objModule1PBP Is Nothing Then
                    m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.CLOCK_START, "")
                End If
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, " Clock - To Stop ", 1, 0)
            Else
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                If Not m_objModule1Main Is Nothing Then
                    m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_STOP, "")
                ElseIf Not m_objModule1PBP Is Nothing Then
                    m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.CLOCK_STOP, "")
                End If
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, " Clock - To Start", 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            If actionsScreen Then
                m_objActions.Focus()
            End If
        End Try
    End Sub

    Private Sub ClockAdjust_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnSecondUp.MouseDown, btnSecondDown.MouseDown, btnMinuteUp.MouseDown, btnMinuteDown.MouseDown
        Dim actionsScreen As Boolean
        Try
            If e.Button = Windows.Forms.MouseButtons.Left Then
                Dim strNewTime() As String
                strNewTime = UdcRunningClock1.URCCurrentTime.ToString.Split(CChar(":"))
                If IsClockEntryAllowed() = False Then
                    Exit Sub
                End If

                If m_objGameDetails.CurrentPeriod = 0 Then
                    Exit Sub
                End If

                Select Case CType(sender, Button).Name
                    Case "btnSecondUp", "btnMinuteUp"
                        If Not m_objModule1PBP Is Nothing Then
                            If m_objModule1PBP.dblstEventDetails01.Visible = True Or m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                                MessageDialog.Show(strmessage6, strmessage34, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        End If
                        If (m_objActions IsNot Nothing) And (m_objActionDetails.IsEditMode = False) Then
                            actionsScreen = True
                            'Save Primary in memory record if entered
                            If Not SaveInMemoryActionInPrimary() Then
                                Exit Sub
                            End If
                        End If
                        m_blnIncrement = True
                        If CType(sender, Button).Name = "btnSecondUp" Then
                            UdcRunningClock1.URCAdjustTime(True, udcRunningClock.IncrementUnit.Seconds)
                            m_IncrementUnit = udcRunningClock.IncrementUnit.Seconds
                        Else
                            UdcRunningClock1.URCAdjustTime(True, udcRunningClock.IncrementUnit.Minutes)
                            m_IncrementUnit = udcRunningClock.IncrementUnit.Minutes
                        End If

                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_INC, "")
                        ElseIf Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.CLOCK_INC, "")
                        End If
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Increment Seconds", "Clock", 1, 0)

                    Case "btnSecondDown"
                        Dim MaxTimeElapsed As Integer
                        Dim CurrentClockTIme As Integer
                        CurrentClockTIme = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)) - 1
                        If Not m_objModule1PBP Is Nothing Then
                            If m_objModule1PBP.dblstEventDetails01.Visible = True Or m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                                MessageDialog.Show(strmessage6, strmessage34, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                            MaxTimeElapsed = m_objModule1PBP.GetMaxElapsedTime(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        End If

                        If m_objActions IsNot Nothing Then
                            actionsScreen = True
                            Dim TimeElapsed As Integer = 0
                            TimeElapsed = m_objGeneral.GetActionsMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If m_objGameDetails.IsDefaultGameClock Then
                                MaxTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(CalculateTimeElapseAfter(TimeElapsed, m_objGameDetails.CurrentPeriod)))
                            End If
                            'Take inmemory record into Consideration.
                            Dim dtPBP As DataTable = m_objActions.dgvPBP.DataSource
                            If IsDBNull(dtPBP.Rows(dtPBP.Rows.Count - 1).Item("SEQUENCE_NUMBER")) Then
                                MaxTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(dtPBP.Rows(dtPBP.Rows.Count - 1).Item("TIME")))
                            End If
                        End If

                        If CurrentClockTIme < MaxTimeElapsed Then
                            MessageDialog.Show(strmessage23, strmessage35, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                        If m_objGameDetails.CurrentPeriod = 1 Then
                            UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Seconds)
                            m_blnIncrement = False
                            m_IncrementUnit = udcRunningClock.IncrementUnit.Seconds
                        ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                            If CInt(strNewTime(1)) = 0 And CInt(strNewTime(0)) = FIRSTHALF Then
                            Else
                                UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Seconds)
                                m_blnIncrement = False
                                m_IncrementUnit = udcRunningClock.IncrementUnit.Seconds
                            End If
                        ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                            If CInt(strNewTime(0)) = SECONDHALF And CInt(strNewTime(1)) = 0 Then
                            Else
                                UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Seconds)
                                m_blnIncrement = False
                                m_IncrementUnit = udcRunningClock.IncrementUnit.Seconds
                            End If
                        ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                            If CInt(strNewTime(0)) = FIRSTEXTRA And CInt(strNewTime(1)) = 0 Then
                            Else
                                UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Seconds)
                                m_blnIncrement = False
                                m_IncrementUnit = udcRunningClock.IncrementUnit.Seconds
                            End If
                        End If
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_DEC, "")
                        ElseIf Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.CLOCK_DEC, "")
                        End If
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Decrement Seconds", "Clock", 1, 0)

                    Case "btnMinuteDown"
                        Dim MaxTimeElapsed As Integer
                        Dim MainClocktime As Integer
                        Dim StrTime As String = "00:00"
                        Dim strGetMin() As String

                        strNewTime = UdcRunningClock1.URCCurrentTime.ToString.Split(CChar(":"))
                        If CInt(strNewTime(0)) = 0 Then
                            Exit Sub
                        End If
                        MainClocktime = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                        If Not m_objModule1PBP Is Nothing Then
                            MaxTimeElapsed = m_objModule1PBP.GetMaxElapsedTime(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            'deduct one minute
                            If m_objModule1PBP.dblstEventDetails01.Visible = True Or m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                                MessageDialog.Show(strmessage6, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        End If
                        If m_objActions IsNot Nothing Then
                            actionsScreen = True
                            Dim TimeElapsed As Integer = 0
                            TimeElapsed = m_objGeneral.GetActionsMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If m_objGameDetails.IsDefaultGameClock Then
                                MaxTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(CalculateTimeElapseAfter(TimeElapsed, m_objGameDetails.CurrentPeriod)))
                            End If
                            'Take inmemory record into Consideration.
                            Dim dtPBP As DataTable = m_objActions.dgvPBP.DataSource
                            If IsDBNull(dtPBP.Rows(dtPBP.Rows.Count - 1).Item("SEQUENCE_NUMBER")) Then
                                MaxTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(dtPBP.Rows(dtPBP.Rows.Count - 1).Item("TIME")))
                            End If
                        End If

                        StrTime = clsUtility.ConvertSecondToMinute(CDbl(MaxTimeElapsed.ToString), False)
                        strGetMin = StrTime.Split(CChar(":"))
                        If MainClocktime > 60 Then
                            MainClocktime = MainClocktime - 60
                            If MainClocktime < MaxTimeElapsed Then
                                MessageDialog.Show(strmessage23, strmessage35, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        End If

                        If m_objGameDetails.IsDefaultGameClock Then
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Minutes)
                                m_blnIncrement = False
                                m_IncrementUnit = udcRunningClock.IncrementUnit.Minutes
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                If CInt(strNewTime(0)) > FIRSTHALF Then
                                    UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Minutes)
                                    m_blnIncrement = False
                                    m_IncrementUnit = udcRunningClock.IncrementUnit.Minutes
                                End If
                            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                If CInt(strNewTime(0)) > SECONDHALF Then
                                    UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Minutes)
                                    m_blnIncrement = False
                                    m_IncrementUnit = udcRunningClock.IncrementUnit.Minutes
                                End If
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                If CInt(strNewTime(0)) > FIRSTEXTRA Then
                                    UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Minutes)
                                    m_blnIncrement = False
                                    m_IncrementUnit = udcRunningClock.IncrementUnit.Minutes
                                End If
                            End If
                        Else
                            UdcRunningClock1.URCAdjustTime(False, udcRunningClock.IncrementUnit.Minutes)
                            m_blnIncrement = False
                            m_IncrementUnit = udcRunningClock.IncrementUnit.Minutes
                        End If
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_DEC, "")
                        ElseIf Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.CLOCK_DEC, "")
                        End If
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Decrement Minutes", "Clock", 1, 0)
                End Select
                tmrClockIncrement.Interval = 500
                tmrClockIncrement.Start()
                m_dtStartTime = Now
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally
            If actionsScreen Then
                m_objActions.Focus()
            End If
        End Try
    End Sub

    Private Sub ClockAdjust_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnSecondUp.MouseUp, btnSecondDown.MouseUp, btnMinuteUp.MouseUp, btnMinuteDown.MouseUp
        Try
            If e.Button = Windows.Forms.MouseButtons.Left Then
                tmrClockIncrement.Stop()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub tmrClockIncrement_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrClockIncrement.Tick
        Try
            UdcRunningClock1.URCAdjustTime(m_blnIncrement, m_IncrementUnit)
            Application.DoEvents()
            tmrClockIncrement.Interval = 100
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklAwayteam_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAwayteam.LinkClicked
        Try
            Dim objTeamSetup As New frmTeamSetup("Away")
            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Or m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                If IsPBPEntryAllowed() = False Then
                    Exit Try
                End If
                If Not m_objModule1PBP Is Nothing Then
                    If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                        MessageDialog.Show(strmessage6, "Team Setup", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If
                End If

                'objTeamSetup.radAwayTeam.Checked = True
                objTeamSetup.ShowDialog()

                'ONCE THE PLAYERS ARE FILLED IN STARTING LINEUPS, THE MAIN FORM PLAYERS ARE REFRESHED ..
                If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                    If Not m_objModule1Main Is Nothing Then
                        m_objModule1Main.LabelsTagged()
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.DisplayPBP()
                    End If
                    'display players in Actions screen.
                    If Not m_objActions Is Nothing Then
                        m_objActions.FillPlayersInUserControls()
                    End If
                End If
            End If
            GetPenaltyShootoutScore()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklHometeam_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklHometeam.LinkClicked
        Try
            Dim objTeamSetup As New frmTeamSetup("Home")

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Or m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                If IsPBPEntryAllowed() = False Then
                    Exit Try
                End If

                If Not m_objModule1PBP Is Nothing Then
                    If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                        MessageDialog.Show(strmessage6, "Team Setup", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If
                End If
                objTeamSetup.ShowDialog()
                'ONCE THE PLAYERS ARE FILLED IN STARTING LINEUPS, THE MAIN FORM PLAYERS ARE REFRESHED ..
                If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                    If Not m_objModule1Main Is Nothing Then
                        m_objModule1Main.LabelsTagged()
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.DisplayPBP()
                    End If

                    'display players in Actions screen.
                    If Not m_objActions Is Nothing Then
                        m_objActions.FillPlayersInUserControls()
                    End If

                End If
            End If
            GetPenaltyShootoutScore()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcRunningClock1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles UdcRunningClock1.DoubleClick
        Try
            If m_objGameDetails.ModuleID = 3 Then
                MessageDialog.Show(strmessage36, "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                MessageDialog.Show(strmessage36, "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            If Not m_objModule1PBP Is Nothing Then
                If m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    MessageDialog.Show(strmessage3, "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
            End If
            If UdcRunningClock1.URCIsRunning = True Then
                UdcRunningClock1.URCToggle()
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
            End If
            mtxtClockEdit.Mask = "000:00"
            mtxtClockEdit.Left = UdcRunningClock1.Left
            mtxtClockEdit.Top = UdcRunningClock1.Top
            mtxtClockEdit.Height = UdcRunningClock1.Height
            mtxtClockEdit.Width = UdcRunningClock1.Width
            mtxtClockEdit.Text = UdcRunningClock1.URCCurrentTime.Replace(":", "").PadLeft(5, CChar(" "))
            mtxtClockEdit.Visible = True
            mtxtClockEdit.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub mtxtClockEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles mtxtClockEdit.KeyDown
        Try
            Dim strNewTime() As String
            If mtxtClockEdit.Visible = True Then
                If e.KeyCode = Keys.Enter Then
                    mtxtClockEdit.Text = mtxtClockEdit.Text.Replace(":", "").PadLeft(5, CChar(" "))
                    strNewTime = mtxtClockEdit.Text.Split(CChar(":"))
                    If strNewTime(1).Trim = "" Then
                        strNewTime(1) = "0"
                    End If
                    If strNewTime(0).Trim = "" Then
                        strNewTime(0) = "0"
                    End If
                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                    mtxtClockEdit.Visible = False
                ElseIf e.KeyCode = Keys.Escape Then
                    mtxtClockEdit.Visible = False
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub mtxtClockEdit_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtxtClockEdit.LostFocus
        Try
            Dim strNewTime() As String
            If mtxtClockEdit.Visible = True Then
                strNewTime = mtxtClockEdit.Text.Split(CChar(":"))
                If strNewTime(1).Trim = "" Then
                    strNewTime(1) = "0"
                End If
                If strNewTime(0).Trim = "" Then
                    strNewTime(0) = "0"
                End If
                UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                mtxtClockEdit.Visible = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub mtxtClockEdit_MaskInputRejected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MaskInputRejectedEventArgs) Handles mtxtClockEdit.MaskInputRejected
        Try
            ttClockEdit.IsBalloon = True
            ttClockEdit.ToolTipTitle = "Invalid Input"
            ttClockEdit.Show("Enter only digits for time in the range of 00:00 to 59:59", mtxtClockEdit, 3000)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub StartPeriodToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles StartPeriodToolStripMenuItem.Click
        Try
            UdcRunningClock1.URCToggle()
            pnlEntryForm.Enabled = True
            If m_objGameDetails.CurrentPeriod = 0 Then
                frmModule1PBP.AddPlayByPlayData(INTGAMESTART, "")
            End If
            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
            'If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
            frmModule1PBP.AddPlayByPlayData(INTSTARTPERIOD, "")
            'End If
            StartPeriodToolStripMenuItem.Enabled = False
            EndPeriodToolStripMenuItem.Enabled = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub EndPeriodToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EndPeriodToolStripMenuItem.Click
        Try
            UdcRunningClock1.URCToggle()
            'If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
            If m_objGameDetails.CurrentPeriod = 2 Then
                EndGameToolStripMenuItem.Enabled = True
            End If
            frmModule1PBP.AddPlayByPlayData(INTENDPERIOD, "Save")
            StartPeriodToolStripMenuItem.Enabled = True
            'End If
            pnlEntryForm.Enabled = False
            EndPeriodToolStripMenuItem.Enabled = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub EndGameToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EndGameToolStripMenuItem.Click
        Try

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                'UdcRunningClock1.URCToggle()
                Dim objfrmModule1PBP As New frmModule1PBP

                objfrmModule1PBP.AddPlayByPlayData(INTENDGAME, "Save")
            End If
            StartPeriodToolStripMenuItem.Enabled = False
            pnlEntryForm.Enabled = False
            'End period DISABLED
            'objfrmModule1PBP.EnableMenuItems(False, True, False)
            EndPeriodToolStripMenuItem.Enabled = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub picOpticalFeed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picOpticalFeed.Click
        Try
            If m_objGameDetails.ModuleID = 1 Then
                If m_objGameDetails.OpticalData.Tables.Count > 0 Then
                    If m_objGameDetails.OpticalData.Tables(0).Rows.Count > 0 Then
                        m_objGameDetails.OpticalEvent = 0

                        m_objOptFeed = New frmOpticalFeed
                        m_objOptFeed.Top = Control.MousePosition.Y
                        m_objOptFeed.Left = Control.MousePosition.X
                        m_objOptFeed.Show()

                    End If
                End If
            ElseIf m_objGameDetails.ModuleID = 3 Then
                If m_objGameDetails.OpticalTouchData.Tables.Count > 0 Then
                    If m_objGameDetails.OpticalTouchData.Tables(0).Rows.Count > 0 Then
                        m_objGameDetails.OpticalTouchEvent = 0

                        m_objOptFeed = New frmOpticalFeed
                        m_objOptFeed.Top = Control.MousePosition.Y
                        m_objOptFeed.Left = Control.MousePosition.X
                        m_objOptFeed.Show()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lblReporter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lblReporter.LinkClicked
        Try
            Dim objUpcomingAssignments As New frmUpComingAssignments(m_objclsLoginDetails.UserId, m_objclsLoginDetails.UserFirstName & " " & m_objclsLoginDetails.UserLastName)
            objUpcomingAssignments.ShowDialog()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region

#Region " Private methods "

    Private Sub InitializeClock()
        Try
            UdcRunningClock1.URCBackColor = Color.FromArgb(0, 0, 51)
            UdcRunningClock1.URCStartForeColor = Color.White
            UdcRunningClock1.URCPauseForeColor = Color.Red
            UdcRunningClock1.URCRunForeColor = Color.LightGreen
            If m_objGameDetails.IsRestartGame = True And m_objGameDetails.ModuleID = TOUCHES_MODULE Then
            Else
                UdcRunningClock1.URCSetTime(0, 0)
            End If

            btnClock.Enabled = False
            btnMinuteDown.Enabled = False
            btnMinuteUp.Enabled = False
            btnSecondDown.Enabled = False
            btnSecondUp.Enabled = False
            If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                btnClock.Visible = False
                btnMinuteDown.Visible = False
                btnMinuteUp.Visible = False
                btnSecondDown.Visible = False
                btnSecondUp.Visible = False
                UdcRunningClock1.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To set the period
    ''' </summary>
    ''' <remarks></remarks>
    '''

    Public Sub SetPeriod()
        Try
            If CInt(m_objGameDetails.languageid) <> 1 Then

                PRE_GAME_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), PRE_GAME_DESC)
                FIRST_HALF_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), FIRST_HALF_DESC)
                HALFTIME_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), HALFTIME_DESC)
                SECOND_HALF_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), SECOND_HALF_DESC)
                END_REGULATION_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), END_REGULATION_DESC)
                END_GAME_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), END_GAME_DESC)
                FIRST_EXTRATIME_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), FIRST_EXTRATIME_DESC)
                EXTRATIME_BREAK_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), EXTRATIME_BREAK_DESC)
                SECOND_EXTRATIME_DESC = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), SECOND_EXTRATIME_DESC)
            End If

            Dim dsEndPeriodCount As DataSet
            If Me.m_objGameDetails.ModuleID = 3 Then
                dsEndPeriodCount = m_objGeneral.PzGetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.ENDPERIOD)
            Else
                dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.ENDPERIOD)
            End If

            If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 0 Then
                If m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                    If m_objGameDetails.CurrentPeriod = 1 And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("StartPeriodToolStripMenuItem") = True Then
                        lblPeriod.Text = HALFTIME_DESC
                        lblPeriod.ForeColor = Color.Red
                    Else
                        If m_objGameDetails.CurrentPeriod = 0 Then
                            lblPeriod.Text = PRE_GAME_DESC
                        Else
                            lblPeriod.Text = FIRST_HALF_DESC
                        End If

                        lblPeriod.ForeColor = Color.White
                    End If
                Else
                    If m_objGameDetails.CurrentPeriod = 1 And m_objGameDetails.CurrentPeriod = CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) Then
                        lblPeriod.Text = HALFTIME_DESC
                        lblPeriod.ForeColor = Color.Red
                    Else
                        If m_objGameDetails.CurrentPeriod = 0 Then
                            lblPeriod.Text = PRE_GAME_DESC
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.DisableAllControls(False)
                                m_objModule1PBP.btnBookingAway.Enabled = False
                                m_objModule1PBP.btnBookingHome.Enabled = False
                                m_objModule1PBP.btnSubstituteAway.Enabled = False
                                m_objModule1PBP.btnSubstituteHome.Enabled = False
                            End If
                        Else
                            lblPeriod.Text = FIRST_HALF_DESC
                        End If

                        lblPeriod.ForeColor = Color.White
                    End If
                End If

            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                If isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False Then
                    lblPeriod.Text = END_GAME_DESC
                ElseIf m_objGameDetails.CurrentPeriod = 2 And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("StartPeriodToolStripMenuItem") = True Then
                    lblPeriod.Text = END_REGULATION_DESC
                    lblPeriod.ForeColor = Color.Red
                Else
                    lblPeriod.Text = SECOND_HALF_DESC
                    lblPeriod.ForeColor = Color.White
                End If
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                If m_objGameDetails.CurrentPeriod = 3 And m_objGameDetails.CurrentPeriod = CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) Then
                    lblPeriod.Text = EXTRATIME_BREAK_DESC
                    lblPeriod.ForeColor = Color.Red
                Else
                    lblPeriod.Text = FIRST_EXTRATIME_DESC
                    lblPeriod.ForeColor = Color.White
                End If

            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                If isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False Then
                    lblPeriod.Text = END_GAME_DESC
                    lblPeriod.ForeColor = Color.Red
                ElseIf m_objGameDetails.CurrentPeriod = 4 And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False Then
                    lblPeriod.Text = END_REGULATION_DESC
                    lblPeriod.ForeColor = Color.Red
                Else
                    lblPeriod.Text = SECOND_EXTRATIME_DESC
                    lblPeriod.ForeColor = Color.White
                End If

            End If

            'Check for END Game event - if it is there, then set period text as "Final"

            Dim endGameData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, clsGameDetails.ENDGAME, m_objGameDetails.ModuleID)
            If Not endGameData Is Nothing Then
                If endGameData.Tables.Count > 0 Then
                    If endGameData.Tables(0).Rows.Count > 0 Then
                        lblPeriod.Text = END_GAME_DESC
                        lblPeriod.ForeColor = Color.Red
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.lklNewHomeBooking.Enabled = False
                            m_objModule1Main.lklNewHomeGoal.Enabled = False
                            m_objModule1Main.lklNewHomePenalty.Enabled = False
                            m_objModule1Main.lklNewHomeSub.Enabled = False
                            m_objModule1Main.lklNewVisitBooking.Enabled = False
                            m_objModule1Main.lklNewVisitGoal.Enabled = False
                            m_objModule1Main.lklNewVisitPenalty.Enabled = False
                            m_objModule1Main.lklNewVisitSub.Enabled = False
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            EnableMenuItem("RatingsToolStripMenuItem", True)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub KillSystemTray()
    '    Try
    '        Dim strFilepath, strFile As String
    '        Dim blnExist As Boolean
    '        Dim arrPrc() As System.Diagnostics.Process
    '        strFilepath = Application.StartupPath
    '        strFilepath += "\Resources\STATS.SoccerDataSync.exe"
    '        strFile = strFilepath
    '        blnExist = File.Exists(strFilepath)
    '        'see the application EXE is there in resource folder
    '        If blnExist Then
    '            strFile = Path.GetFileNameWithoutExtension(strFile)
    '            'get process object in a array
    '            arrPrc = Process.GetProcessesByName(strFile)
    '            'See the SystemTray application is running or not
    '            If arrPrc.Length > 0 Then
    '                arrPrc(0).Kill()
    '                'TOSOCRS-33-SOC - Manual restart of datasync component in reporter software
    '                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Help" & " -> " & "Restart DataSync Component  on Clicked Datasync Killed", 1, 0)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    ''' <summary>
    ''' To load data sync application
    ''' </summary>
    ''' <remarks></remarks>
    '''

    Private Sub LoadSoccerDataSync()
        Try
            KillDataSync()

            Dim SoccerDataSyncProcess As New ProcessStartInfo(Application.StartupPath + "\Resources\STATS.SoccerDataSync.exe", ConfigurationSettings.AppSettings("Environment"))
            SoccerDataSyncProcess.WindowStyle = ProcessWindowStyle.Hidden
            SoccerDataSyncProcess.CreateNoWindow = True
            Process.Start(SoccerDataSyncProcess)

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "DataSync Started", 1, 0)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub KillDataSync()
        Try
            For Each proc As System.Diagnostics.Process In Process.GetProcessesByName("STATS.SoccerDataSync")
                proc.Kill()
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "DataSync Killed", 1, 0)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Dynamically sets the main form menu based on current module.
    ''' </summary>
    ''' <remarks></remarks>
    '''

    Public Sub SetMainFormMenu()
        Try
            Dim ds As New DataSet
            If m_objGameDetails.languageid = 13 Then
                ds.ReadXml(Application.StartupPath & "\Resources\Soc_MenuMexico.xml")
            ElseIf m_objGameDetails.languageid = 16 Then
                ds.ReadXml(Application.StartupPath & "\Resources\Soc_MenuRussian.xml")
            Else
                ds.ReadXml(Application.StartupPath & "\Resources\Soc_Menu.xml")
            End If
            mnsMain.Items.Clear()
            Dim drarrMenuItem() As DataRow
            Dim drarrModuleMenu() As DataRow = ds.Tables("tblModuleMenu").Select("ModuleId='" & m_objGameDetails.ModuleID & "' and ParentMenuId=''")

            For Each drModuleMenu As DataRow In drarrModuleMenu
                drarrMenuItem = ds.Tables("tblMenuItems").Select("Id='" & drModuleMenu.Item("MenuId").ToString & "'")
                If drarrMenuItem.Length > 0 Then
                    'build main menu
                    Dim tsmi As New SoccerToolStripMenuItem
                    tsmi.Name = drarrMenuItem(0).Item("Name").ToString
                    tsmi.Text = drarrMenuItem(0).Item("Text").ToString
                    mnsMain.Items.Add(tsmi)

                    'build sub menus for this main menu item
                    BuildSubMenu(ds, m_objGameDetails.ModuleID, tsmi, CInt(drModuleMenu.Item("MenuId")))
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BuildSubMenu(ByVal ds As DataSet, ByVal ModuleId As Integer, ByVal ParentToolStripMenuItem As SoccerToolStripMenuItem, ByVal ParentMenuId As Integer)
        Try
            Dim drarrMenuItem() As DataRow
            Dim drarrModuleMenu() As DataRow = ds.Tables("tblModuleMenu").Select("ModuleId='" & ModuleId & "' and ParentMenuId='" & ParentMenuId & "'")
            For Each dr As DataRow In drarrModuleMenu
                drarrMenuItem = ds.Tables("tblMenuItems").Select("Id='" & dr.Item("MenuId").ToString & "'")

                Dim chtsmi As New SoccerToolStripMenuItem
                chtsmi.Name = drarrMenuItem(0).Item("Name").ToString
                chtsmi.Text = drarrMenuItem(0).Item("Text").ToString

                If chtsmi.Text = "-" Then
                    ParentToolStripMenuItem.DropDownItems.Add("-")
                Else
                    ParentToolStripMenuItem.DropDownItems.Add(chtsmi)
                    BuildSubMenu(ds, ModuleId, chtsmi, CInt(dr.Item("MenuId")))
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' ENABLE/DISABLE MENU ITEMS SPECIFIC FUNCTION FOR T1 WORKFLOW AND MODULE2 GAMES
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SetMenuItems()
        Try

            Dim m_DsLastPBPEvent As DataSet
            m_DsLastPBPEvent = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_DsLastPBPEvent.Tables(0).Rows.Count > 0 Then
                Dim CurrPeriod As Integer
                CurrPeriod = CInt(m_DsLastPBPEvent.Tables(0).Rows(m_DsLastPBPEvent.Tables(0).Rows.Count - 1).Item("PERIOD"))
                If CurrPeriod > 0 Then
                    EnableMenuItem("ManagerExpulsionToolStripMenuItem", True)
                End If
                Select Case CInt(m_DsLastPBPEvent.Tables(0).Rows(m_DsLastPBPEvent.Tables(0).Rows.Count - 1).Item("EVENT_CODE_ID"))
                    Case clsGameDetails.STARTPERIOD, 37 'START HALF , BEGIN OVERTIME
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("EndPeriodToolStripMenuItem", True)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        SetClockControl(True)
                        If Not m_objModule1PBP Is Nothing Then
                            If m_objModule1PBP.dblstEventDetails01.Visible = False Then
                                m_objModule1PBP.DisableAllControls(True)
                            End If
                        End If
                    Case clsGameDetails.ENDPERIOD, 38 'HALF OVER, END OVERTIME
                        If CInt(m_DsLastPBPEvent.Tables(0).Rows(m_DsLastPBPEvent.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE")) = CInt(m_DsLastPBPEvent.Tables(0).Rows(m_DsLastPBPEvent.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE")) Then
                            'SCORES TIED
                            If CurrPeriod = 2 Or CurrPeriod = 4 Then
                                If CurrPeriod = 4 Then
                                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                Else
                                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                End If

                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndGameToolStripMenuItem", True)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                            Else
                                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndGameToolStripMenuItem", False)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            End If
                        Else
                            If CurrPeriod = 2 Or CurrPeriod = 4 Then
                                If CurrPeriod = 2 Then
                                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndGameToolStripMenuItem", True)
                                    EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                                Else
                                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndGameToolStripMenuItem", True)
                                    EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                                End If

                            Else
                                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndGameToolStripMenuItem", False)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            End If
                        End If
                        SetClockControl(False)

                    Case 10 ' GAME OVER EVENT
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        EnableMenuItem("RatingsToolStripMenuItem", True)
                        SetClockControl(False)
                    Case Else
                        If m_DsLastPBPEvent.Tables(0).Rows.Count = 2 Then
                            EnableMenuItem("StartPeriodToolStripMenuItem", True)
                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        Else
                            Dim dsEndPeriodCount As DataSet
                            'Dim endPeriodCount As Integer
                            dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.ENDPERIOD)
                            If CurrPeriod = 0 Then
                                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndGameToolStripMenuItem", False)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            ElseIf CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = CurrPeriod Then
                                If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = 1 Or CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = 3 Then
                                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndGameToolStripMenuItem", False)
                                    EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                                Else
                                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndGameToolStripMenuItem", True)
                                    EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                                End If

                            Else
                                EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                EnableMenuItem("EndGameToolStripMenuItem", False)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            End If

                            If Not m_objModule1PBP Is Nothing Then
                                If m_objModule1PBP.dblstEventDetails01.Visible = False Then
                                    m_objModule1PBP.DisableAllControls(True)
                                End If

                            End If
                        End If
                        SetClockControl(True)
                End Select
                SetPeriod()
            Else
                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                EnableMenuItem("EndGameToolStripMenuItem", False)
                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                SetClockControl(False)
            End If
            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                    EnableMenuItem("OpenToolStripMenuItem", True)
                Else
                    If m_objGameDetails.IsPrimaryReporter Or m_objGameDetails.ModuleID = 2 Or m_objGameDetails.ModuleID = 4 Then
                        EnableMenuItem("OpenToolStripMenuItem", False)
                    Else
                        EnableMenuItem("OpenToolStripMenuItem", True)
                    End If
                End If
            Else
                EnableMenuItem("OpenToolStripMenuItem", False)
            End If

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                EnableMenuItem("CommentToolStripMenuItem", True)
            Else
                EnableMenuItem("CommentToolStripMenuItem", False)
            End If

            If (m_objGameDetails.CoverageLevel > 3) Then
                EnableMenuItem("InputtingteamstatsToolStripMenuItem ", False)
            Else
                EnableMenuItem("InputtingteamstatsToolStripMenuItem ", True)
            End If
            'Added by Shravani
            If m_objGameDetails.CoverageLevel = 2 Then
                EnableMenuItem("TeamSetupToolStripMenuItem", False)
                EnableMenuItem("SortPlayersToolStripMenuItem", False)
                EnableMenuItem("SortByUniformToolStripMenuItem", False) 'Switch Side
                EnableMenuItem("SortByLastNameToolStripMenuItem", False) 'Player Sort
                EnableMenuItem("SortByPositionToolStripMenuItem", False)
                EnableMenuItem("SwitchSidesToolStripMenuItem", False)
                If m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm Then
                    EnableMenuItem("SetTeamColorToolStripMenuItem", True)
                Else
                    EnableMenuItem("SetTeamColorToolStripMenuItem", False)
                End If
                EnableMenuItem("InputTeamStatsToolStripMenuItem", False) 'Switch Side
                EnableMenuItem("ReportFormationToolStripMenuItem", False) 'Player Sort
                EnableMenuItem("AddOfficialToolStripMenuItem", False)
                EnableMenuItem("AddManagerToolStripMenuItem", False)
                EnableMenuItem("PlayerStatsToolStripMenuItem", False)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' setting the clock time specific for T1 workflow and module2 games
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetGameClock()
        Try
            'MODULE 2 - GAME CLOCK TIME IS CALCULATED BASED ON THE TIME ELAPSED STORED IN CURRENT GAME TABLE.
            Dim StrTime As String = "00:00"
            Dim strNewTime() As String
            Dim CurrPeriod As Integer = 0
            Dim dsLastEvent As DataSet

            dsLastEvent = m_objGeneral.GetLastClockEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)

            If m_objGameDetails.ModuleID = 2 Then
            Else
                If dsLastEvent.Tables.Count > 1 Then
                    If dsLastEvent.Tables(1).Rows.Count > 0 Then
                        If Not IsDBNull(dsLastEvent.Tables(1).Rows(0).Item("TIME_ELAPSED")) Then
                            Dim ElapsedTime As Integer
                            ElapsedTime = CInt(dsLastEvent.Tables(1).Rows(0).Item("TIME_ELAPSED"))
                            'CurrPeriod = CInt(DsPBP.Tables(0).Rows(DsPBP.Tables(0).Rows.Count - 1).Item("PERIOD"))

                            StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                        End If
                    End If
                End If
                strNewTime = StrTime.Split(CChar(":"))
                'SETTING THE CLOCK TIME
                UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)), True)
            End If
            If m_objGameDetails.CurrentPeriod = 0 Then
                If UdcRunningClock1.URCIsRunning = True Then
                    UdcRunningClock1.URCToggle()
                End If
            Else
                If dsLastEvent.Tables(0).Rows.Count > 0 Then
                    If CInt(dsLastEvent.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = INTCLOCKSTART Then
                        If UdcRunningClock1.URCIsRunning = False Then
                            UdcRunningClock1.URCToggle()
                        End If
                    ElseIf CInt(dsLastEvent.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = INTCLOCKSTOP Then
                        If UdcRunningClock1.URCIsRunning = True Then
                            UdcRunningClock1.URCToggle()
                        End If
                    End If
                End If

            End If

            If UdcRunningClock1.URCIsRunning Then
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
            Else
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' DISPLAY  OF GAME SCORES
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetScores()
        Try
            'DISPLAY SCORES IN MAIN SCREEN
            Dim m_DsScores As DataSet
            'FETCHING THE LAST EVENT SCORES FOR THE SELECTED GAME
            m_DsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_DsScores.Tables(0).Rows.Count > 0 Then
                If CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                    m_objGameDetails.HomeScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    m_objGameDetails.AwayScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
                    lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                Else
                    m_objGameDetails.AwayScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    m_objGameDetails.HomeScore = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                    lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                End If
                'SETTING THE PROPERTIES
                m_objGameDetails.CurrentPeriod = CInt(m_DsScores.Tables(0).Rows(m_DsScores.Tables(0).Rows.Count - 1).Item("PERIOD"))
                If m_objGameDetails.IsHalfTimeSwap Then
                    If m_objGameDetails.IsHomeTeamOnLeft Then 'Home Team started left, so Switch Sides applicable in period 2 and 4
                        If (m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4) And m_objGameDetails.IsRestartGame Then
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.SwitchSides()
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.SwitchSides()
                            End If
                            SetHeader2()
                        End If
                    Else 'Home Team started left, so Switch Sides applicable in period 1 and 3
                        If (m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3) And m_objGameDetails.IsRestartGame Then
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.SwitchSides()
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.SwitchSides()
                            End If
                            SetHeader2()
                        End If
                    End If
                End If
                SetPeriod()
            Else
                'IF NO RECORD EXISTS FOR THE SELECTED GAME , SET THE LABLES AND PROPERTIES
                lblScoreHome.Text = "0"
                lblScoreAway.Text = "0"
                m_objGameDetails.HomeScore = 0
                m_objGameDetails.AwayScore = 0
                m_objGameDetails.CurrentPeriod = 0
                SetPeriod()
            End If
            'Set Module1Main Edit/New buttons
            If Not m_objModule1Main Is Nothing Then
                m_objModule1Main.setEventButton()
            End If
            If m_objGameDetails.IsHomeTeamOnLeft Then
                If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                    m_objGameDetails.isHomeDirectionLeft = True
                    frmModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                ElseIf m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                    m_objGameDetails.isHomeDirectionLeft = False
                    frmModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                End If
            ElseIf m_objGameDetails.IsHomeTeamOnLeft = False Then
                If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                    m_objGameDetails.isHomeDirectionLeft = False
                    frmModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                ElseIf m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                    m_objGameDetails.isHomeDirectionLeft = True
                    frmModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                End If
            End If

            GetPenaltyShootoutScore()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Public methods "

    ''' <summary>
    ''' Adds the PBP columns to the temp table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AddPBPColumns() As DataSet
        Try
            Dim dsTempPBPData As New DataSet
            dsTempPBPData.Tables.Add("PBP")

            dsTempPBPData.Tables(0).Columns.Add("GAME_CODE", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("FEED_NUMBER", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("UNIQUE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SEQUENCE_NUMBER", GetType(Decimal))
            dsTempPBPData.Tables(0).Columns.Add("ORIG_SEQ", GetType(Decimal))

            dsTempPBPData.Tables(0).Columns.Add("TIME_ELAPSED", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("PERIOD", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("EVENT_CODE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SYSTEM_TIME", GetType(String))

            dsTempPBPData.Tables(0).Columns.Add("ONFIELD_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("TEAM_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("OFFENSE_SCORE", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("DEFENSE_SCORE", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("CONTINUATION", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("OFFENSIVE_PLAYER_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("ASSISTING_PLAYER_ID", GetType(Int64))
            'dsTempPBPData.Tables(0).Columns.Add("CAUSED_BY_PLAYER_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("DEFENSIVE_PLAYER_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("PLAYER_OUT_ID", GetType(Int64))

            dsTempPBPData.Tables(0).Columns.Add("BOOKING_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("BOOKING_REASON_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SUB_REASON_ID", GetType(Integer))
            'dsTempPBPData.Tables(0).Columns.Add("KEEPERZONE_ID", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("PENALTY_KICK_AWARDED", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("SHOT_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SAVE_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("ASSIST_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("FOUL_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("FOUL_RESULT_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("FOOT_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SITUATION_ID", GetType(Int64))
            'dsTempPBPData.Tables(0).Columns.Add("FIELD_AREA", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("GOALZONE_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("REBOUND", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("COMMENTS", GetType(String))

            dsTempPBPData.Tables(0).Columns.Add("SHOOTOUT_ROUND", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("CELEBRATION_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SECOND_ASSISTER_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("MANAGER_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("CAUSED_BY_PLAYER_ID", GetType(Int64))
            dsTempPBPData.Tables(0).Columns.Add("CORNER_CROSS_TYPE", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("CORNER_CROSS_RES", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("CORNER_CROSS_DESC", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("CORNER_CROSS_ZONE", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("CORNER_REASON_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SHOT_DESC", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SHOT_RESULT", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("SAVE_RESULT", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("BACK_PASS_ID", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("DEFENDED_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("OFFSIDE_DESC_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("OFFSIDE_RESULT_ID", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("DEF_ACTION_TYPE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("DEF_ACTION_DESC_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("DEF_ACTION_RESULT_ID", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("GOAL_THREAT_DESC_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("GOAL_THREAT_DEF_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("KEEPERZONE_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("COMMENT_ID", GetType(Integer))

            'COMMENTED BY SHRAVANI
            dsTempPBPData.Tables(0).Columns.Add("COMMENT_LANGUAGE", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("X_FIELD_ZONE", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("Y_FIELD_ZONE", GetType(Integer))
            'dsTempPBPData.Tables(0).Columns.Add("Z_COOR_FIELD", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("X_FIELD_ZONE_DEF", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("Y_FIELD_ZONE_DEF", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("X_FIELD_ZONE_OPTICAL", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("Y_FIELD_ZONE_OPTICAL", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("Z_FIELD_ZONE_OPTICAL", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("X_FIELD_ZONE_DEF_OPTICAL", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("Y_FIELD_ZONE_DEF_OPTICAL", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("Z_FIELD_ZONE_DEF_OPTICAL", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("KEY_MOMENT_ID", GetType(Integer))

            dsTempPBPData.Tables(0).Columns.Add("OPTICAL_TIMESTAMP", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("FK_RESULT_ID", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("RECORD_EDITED", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("EDIT_UID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("PBP_STRING", GetType(String))

            dsTempPBPData.Tables(0).Columns.Add("MULTILINGUAL_PBP_STRING", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("REPORTER_ROLE", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("PROCESSED", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("DEMO_DATA", GetType(String))
            dsTempPBPData.Tables(0).Columns.Add("FOUL_LOC_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("INJURY_TIME", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("OUT_OF_BOUNDS_RES_ID", GetType(Integer))
            dsTempPBPData.Tables(0).Columns.Add("GAME_TIME_MS", GetType(Integer))
            dsTempPBPData.Tables.Add("ONFIELD")
            dsTempPBPData.Tables(1).Columns.Add("ONFIELD_ID", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("GAME_CODE", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("FEED_NUMBER", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("ONFIELD_SEQ", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("TEAM_ID", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("PLAYER_ID", GetType(Integer))

            dsTempPBPData.Tables(1).Columns.Add("POSITION", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("FORMATION_ID", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("FORMATION_SPEC_ID", GetType(Integer))
            dsTempPBPData.Tables(1).Columns.Add("REPORTER_ROLE", GetType(String))
            dsTempPBPData.Tables(1).Columns.Add("PROCESSED", GetType(String))

            Return dsTempPBPData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Dynamically sets the position and size of the main form (frmMain) to fit the module specific form.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SetMainFormSize()
        Try
            Dim intHeightDiff As Integer
            Dim intWidthDiff As Integer

            intHeightDiff = Me.Height - Me.ClientRectangle.Height
            intWidthDiff = Me.Width - Me.ClientRectangle.Width

            '----------------------------
            Select Case m_objGameDetails.ModuleID
                Case COMMENTARY_MODULE
                    pnlEntryForm.Size = frmCommentary.Size

                Case TOUCHES_MODULE, PBP_MODULE, MULTIGAME_PBP_MODULE
                    If m_objGameDetails.CoverageLevel = 1 Then
                        pnlEntryForm.Size = frmModule2Score.Size
                    ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.MainForm Then
                        pnlEntryForm.Size = frmModule1Main.Size
                    ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm Then
                        pnlEntryForm.Size = frmModule1PBP.Size
                    ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.ActionsForm Then
                        Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                            Case "1"
                                pnlEntryForm.Size = FrmActions.Size
                            Case Else
                                pnlEntryForm.Size = FrmActionsAssist.Size
                        End Select
                    End If
            End Select

            'resize main form
            Me.Size = New Size(pnlEntryForm.Width + intWidthDiff, pnlEntryForm.Height + intHeightDiff + MENU_HEIGHT + HEADER_HEIGHT + FOOTER_HEIGHT)

            'set header1
            picTopBar.Left = 0                                      'top bar
            picTopBar.Top = MENU_HEIGHT
            picTopBar.Width = pnlEntryForm.Width
            picReporterBar.Left = 0                                 'reporter bar
            picReporterBar.Width = pnlEntryForm.Width
            picCompanyLogo.Top = MENU_HEIGHT                        'company logo
            picCompanyLogo.Left = pnlEntryForm.Width - picCompanyLogo.Width

            If m_objGameDetails.ModuleID = 3 Then
                picRptr6.Left = picCompanyLogo.Left - picRptr6.Width     'reporter details
                picRptr5.Left = picRptr6.Left - picRptr5.Width
                picRptr4.Left = picRptr5.Left - picRptr4.Width
                picRptr3.Left = picRptr4.Left - picRptr3.Width
                picRptr2.Left = picRptr3.Left - picRptr2.Width
                picRptr1.Left = picRptr2.Left - picRptr1.Width
            Else
                picRptr6.Visible = False
                picRptr5.Visible = False
                picRptr4.Left = picCompanyLogo.Left - picRptr4.Width     'reporter details
                picRptr3.Left = picRptr4.Left - picRptr3.Width
                picRptr2.Left = picRptr3.Left - picRptr2.Width
                picRptr1.Left = picRptr2.Left - picRptr1.Width
            End If

            lblMode.Left = picCompanyLogo.Left - lblMode.Width      'reporter details
            lblReporterPipe.Left = lblMode.Left - lblReporterPipe.Width
            pnlReporter.Left = lblReporterPipe.Left - pnlReporter.Width

            Select Case m_objGameDetails.ModuleID
                Case TOUCHES_MODULE, PBP_MODULE
                    picRptr6.Width = 18
                    picRptr5.Width = 18
                    picRptr4.Width = 18
                    picRptr3.Width = 18
                    picRptr2.Width = 18
                    picRptr1.Width = 18
                Case Else
                    picRptr6.Width = 0
                    picRptr5.Width = 0
                    picRptr4.Width = 0
                    picRptr3.Width = 0
                    picRptr2.Width = 0
                    picRptr1.Width = 0
            End Select

            SetHeader2()

            'set footer
            picButtonBar.Left = 0
            picButtonBar.Top = pnlEntryForm.Top + pnlEntryForm.Height
            picButtonBar.Width = pnlEntryForm.Width
            pnlFooterInfo.Left = 4
            pnlFooterInfo.Top = picButtonBar.Top + 9

            btnClose.Top = 10 'picButtonBar.Top + 11
            btnClose.Left = picButtonBar.Width - btnClose.Width - 10           'pnlEntryForm.Width - btnClose.Width - 10

            Select Case m_objGameDetails.ModuleID
                Case PBP_MODULE, TOUCHES_MODULE
                    btnGotoPBP.Top = btnClose.Top
                    btnGotoPBP.Left = btnClose.Left - btnGotoPBP.Width - 8
                    btnGotoMain.Top = btnClose.Top
                    btnGotoMain.Left = btnClose.Left - btnGotoMain.Width - 8
                Case MULTIGAME_PBP_MODULE
                    If m_objGameDetails.CoverageLevel = 1 Then
                        btnGotoPBP.Visible = False
                    Else
                        btnGotoPBP.Top = btnClose.Top
                        btnGotoPBP.Left = btnClose.Left - btnGotoPBP.Width - 10
                        btnGotoMain.Top = btnClose.Top
                        btnGotoMain.Left = btnClose.Left - btnGotoMain.Width - 10
                    End If
                Case Else
                    btnGotoPBP.Visible = False
            End Select

            'position main form at the center of screen
            Me.Location = New Point(CInt((SystemInformation.WorkingArea.Width - Me.ClientRectangle.Width) / 2), CInt((SystemInformation.WorkingArea.Height - Me.ClientRectangle.Height) / 2))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ActionsChildForm()
        Try
            pnlEntryForm.Location = New Point(0, MENU_HEIGHT + HEADER_HEIGHT)
            Me.pnlEntryForm.Controls.Clear()
            Dim STRTITLE = "Soccer Data Collection"
            Dim STRTITLE1 = "Play by play"
            Dim STRTITLE2 = "Touches"
            Dim STRTITLE3 = "Main"

            Select Case m_objGameDetails.ModuleID
                Case TOUCHES_MODULE
                    If m_objGameDetails.FormState = clsGameDetails.m_enumFormState.MainForm Then
                        m_objModule1Main.TopLevel = False
                        btnGotoPBP.Visible = True
                        btnGotoMain.Visible = False
                        Me.pnlEntryForm.Controls.Add(m_objModule1Main)
                        Me.Text = STRTITLE + " - " + STRTITLE3
                        pnlEntryForm.Size = m_objModule1Main.Size

                    ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.ActionsForm Then
                        'Tier 6 - primary load Actions screen else frmActionAssist for assisters,home/away touches.
                        btnGotoMain.Visible = True
                        btnGotoPBP.Visible = False
                        Me.Text = STRTITLE + " - " + STRTITLE1
                        Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                            Case "1"
                                m_objActions.TopLevel = False
                                Me.pnlEntryForm.Controls.Add(m_objActions)
                                pnlEntryForm.Size = m_objActions.Size
                                m_objActions.Show()
                                'TOPZ-414
                                m_objActions.Focus()
                                m_objActions.SetCustomFormation()
                            Case "2", "3", "4", "5", "6"
                                'assisters,home/away touch screen screen has to be loaded
                                m_objActionsAssist.TopLevel = False
                                Me.pnlEntryForm.Controls.Add(m_objActionsAssist)
                                pnlEntryForm.Size = m_objActionsAssist.Size
                                m_objActionsAssist.Show()
                                'TOPZ-414
                                m_objActionsAssist.Focus()
                        End Select
                    End If

            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub SetChildForm()
        Try
            pnlEntryForm.Location = New Point(0, MENU_HEIGHT + HEADER_HEIGHT)

            Me.pnlEntryForm.Controls.Clear()
            Dim STRTITLE = "Soccer Data Collection"
            Dim STRTITLE1 = "Play by play"
            Dim STRTITLE2 = "Touches"
            Dim STRTITLE3 = "Main"
            Dim STRTITLE4 = "Commentary"
            If CInt(m_objGameDetails.languageid) <> 1 Then
                STRTITLE = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), STRTITLE)
                STRTITLE1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), STRTITLE1)
                STRTITLE2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), STRTITLE2)
                STRTITLE3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), STRTITLE3)
                STRTITLE4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), STRTITLE4)
            End If


            Select Case m_objGameDetails.ModuleID
                Case COMMENTARY_MODULE
                    m_objModule1PBP = Nothing
                    m_objModule1Main = Nothing
                    m_objTouches = Nothing
                    btnGotoPBP.Visible = False
                    btnGotoMain.Visible = False

                    m_objCommentary = New frmCommentary
                    m_objCommentary.TopLevel = False
                    Me.pnlEntryForm.Controls.Add(m_objCommentary)
                    'Me.Text = "Soccer Data Collection - Commentary"
                    Me.Text = STRTITLE + " - " + STRTITLE4
                    pnlEntryForm.Size = m_objCommentary.Size
                    m_objCommentary.Show()

                Case MULTIGAME_PBP_MODULE
                    If m_objGameDetails.CoverageLevel = 1 Then
                        'If m_objModule2Score Is Nothing Then
                        m_objModule2Score = New frmModule2Score
                        'End If
                        m_objModule2Score.TopLevel = False
                        m_objModule1PBP = Nothing
                        m_objModule1Main = Nothing
                        btnGotoPBP.Visible = False
                        btnGotoMain.Visible = False
                        Me.pnlEntryForm.Controls.Add(m_objModule2Score)
                        Me.Text = "Soccer Data Collection - (Multiple Games - Tier  " + CStr(m_objGameDetails.CoverageLevel) + " Goal Entry)"
                        pnlEntryForm.Size = m_objModule2Score.Size
                        m_objModule2Score.Show()
                    Else
                        If m_objGameDetails.FormState = clsGameDetails.m_enumFormState.MainForm Then
                            If m_objModule1Main Is Nothing Then
                                m_objModule1Main = New frmModule1Main
                            End If
                            m_objModule1Main.TopLevel = False
                            m_objModule1PBP = Nothing
                            m_objModule2Score = Nothing
                            btnGotoPBP.Visible = True
                            btnGotoMain.Visible = False
                            Me.pnlEntryForm.Controls.Add(m_objModule1Main)
                            'Me.Text = "Soccer Data Collection - Main"
                            Me.Text = STRTITLE + " - " + STRTITLE3
                            pnlEntryForm.Size = m_objModule1Main.Size
                            m_objModule1Main.Show()
                            m_objModule1Main.Refresh()
                            If m_countToMinWin > 8 Then
                                Me.WindowState = FormWindowState.Minimized
                                Me.WindowState = FormWindowState.Normal
                                m_countToMinWin = 0
                            Else
                                m_countToMinWin = m_countToMinWin + 1
                            End If

                        ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm Then
                            If m_objModule1PBP Is Nothing Then
                                m_objModule1PBP = New frmModule1PBP
                            End If
                            m_objModule1PBP.TopLevel = False
                            m_objModule1Main = Nothing
                            m_objModule2Score = Nothing
                            btnGotoMain.Visible = True
                            btnGotoPBP.Visible = False
                            Me.pnlEntryForm.Controls.Add(m_objModule1PBP)
                            Me.Text = "Soccer Data Collection - Play by play(Multiple Games - Tier " + CStr(m_objGameDetails.CoverageLevel) + ")"
                            pnlEntryForm.Size = m_objModule1PBP.Size
                            m_objModule1PBP.Show()
                        End If
                    End If
                Case PBP_MODULE, TOUCHES_MODULE
                    If m_objGameDetails.FormState = clsGameDetails.m_enumFormState.MainForm Then
                        If m_objModule1Main Is Nothing Then
                            m_objModule1Main = New frmModule1Main
                        End If
                        m_objModule1Main.TopLevel = False
                        m_objModule1PBP = Nothing
                        btnGotoPBP.Visible = True
                        btnGotoMain.Visible = False
                        Me.pnlEntryForm.Controls.Add(m_objModule1Main)
                        Me.Text = STRTITLE + " - " + STRTITLE3
                        'Me.Text = "Soccer Data Collection - Main"
                        pnlEntryForm.Size = m_objModule1Main.Size
                        m_objModule1Main.Show()
                        m_objModule1Main.Refresh()
                    ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm Then
                        If m_objModule1PBP Is Nothing Then
                            m_objModule1PBP = New frmModule1PBP
                        End If
                        m_objModule1PBP.TopLevel = False
                        m_objModule1Main = Nothing
                        btnGotoMain.Visible = True
                        btnGotoPBP.Visible = False
                        Me.pnlEntryForm.Controls.Add(m_objModule1PBP)
                        Me.Text = STRTITLE + " - " + STRTITLE1
                        ' Me.Text = "Soccer Data Collection - Play by play"
                        pnlEntryForm.Size = m_objModule1PBP.Size
                        m_objModule1PBP.Show()
                    End If

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' For filling game information in controls
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillFormControls()
        Try
            Dim v_memLogo As MemoryStream
            lblLanguage.Text = m_objclsLoginDetails.Language
            Dim strUserName As String = StrConv(m_objclsLoginDetails.UserFirstName & " " & m_objclsLoginDetails.UserLastName, VbStrConv.ProperCase)
            If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                lblReporter.Text = strUserName & " (" & StrConv(m_objGameDetails.ReporterRoleDisplay, VbStrConv.ProperCase) & ")"
            Else
                lblReporter.Text = StrConv(m_objGameDetails.ReporterRoleDisplay, VbStrConv.ProperCase)
            End If
            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                lblMode.Text = "Demo Mode"
            End If
            lklHometeam.Text = m_objGameDetails.HomeTeam.Trim()
            lklAwayteam.Text = m_objGameDetails.AwayTeam.Trim()
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID)
            If v_memLogo IsNot Nothing Then
                picAwayLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, m_objGameDetails.HomeTeamID)
            If v_memLogo IsNot Nothing Then
                picHomeLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
            SaveTeamlogos()   'TOSOCRS-338
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetHeader2()
        Try
            Dim hometeamonLeft As Boolean = True
            If m_objGameDetails.ReporterRoleSerial = 1 And m_objGameDetails.IsHalfTimeSwap And m_objGameDetails.CurrentPeriod > 1 Then
                hometeamonLeft = m_objGameDetails.IsHomeTeamOnLeft
            End If
            If m_objGameDetails.ModuleID = 4 Then
                'set header2
                If hometeamonLeft = True Then
                    picHomeLogo.Left = 24
                    picAwayLogo.Left = pnlEntryForm.Width - picAwayLogo.Width - 28
                Else
                    picAwayLogo.Left = 24
                    picHomeLogo.Left = pnlEntryForm.Width - picAwayLogo.Width - 28
                End If

                pnlClockBar.Left = 85
                pnlClockBar.Width = pnlEntryForm.Width - 170

                If hometeamonLeft = True Then
                    lklHometeam.Left = 17
                    lklAwayteam.Left = pnlEntryForm.Width - 345
                    lblScoreHome.Left = CInt((pnlEntryForm.Width - 233 - 170) / 2)
                    btnMinuteUp.Left = lblScoreHome.Left + 52
                    btnMinuteDown.Left = lblScoreHome.Left + 52
                    picHomeColorTop.Top = 5
                    picHomeColorTop.Left = 0
                    picHomeColorBottom.Top = 36
                    picHomeColorBottom.Left = 0
                    picHomeColorTop.Width = btnMinuteUp.Left
                    picHomeColorBottom.Width = btnMinuteUp.Left

                Else
                    lklAwayteam.Left = 17
                    lklHometeam.Left = pnlEntryForm.Width - 345
                    lblScoreAway.Left = CInt((pnlEntryForm.Width - 233 - 170) / 2)
                    btnMinuteUp.Left = lblScoreAway.Left + 52
                    btnMinuteDown.Left = lblScoreAway.Left + 52
                    picAwayColorTop.Top = 5
                    picAwayColorTop.Left = 0
                    picAwayColorBottom.Top = 36
                    picAwayColorBottom.Left = 0
                    picAwayColorTop.Width = btnMinuteUp.Left
                    picAwayColorBottom.Width = btnMinuteUp.Left

                End If

                If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                    lblPeriod.Top = 17
                Else
                    lblPeriod.Top = 2
                End If

                lblPeriod.Left = btnMinuteDown.Left + 25 '31
                UdcRunningClock1.Left = btnMinuteDown.Left + 35 'lblPeriod.Left + CInt(lblPeriod.Width / 2) - CInt(UdcRunningClock1.Width / 2) - 10
                btnSecondUp.Left = btnMinuteUp.Left + 98
                btnSecondDown.Left = btnMinuteDown.Left + 98
                btnClock.Left = btnSecondUp.Left + 28
                picOpticalFeed.Left = lblPeriod.Left + 100

                If hometeamonLeft = True Then
                    lblScoreAway.Left = btnClock.Left + 70
                    picAwayColorTop.Top = 5
                    picAwayColorTop.Left = btnClock.Left + btnClock.Width
                    picAwayColorBottom.Top = 36
                    picAwayColorBottom.Left = btnClock.Left + btnClock.Width
                    picAwayColorTop.Width = pnlClockBar.Width - picAwayColorTop.Left
                    picAwayColorBottom.Width = pnlClockBar.Width - picAwayColorTop.Left
                Else
                    lblScoreHome.Left = btnClock.Left + 70
                    picHomeColorTop.Top = 5
                    picHomeColorTop.Left = btnClock.Left + btnClock.Width
                    picHomeColorBottom.Top = 36
                    picHomeColorBottom.Left = btnClock.Left + btnClock.Width
                    picHomeColorTop.Width = pnlClockBar.Width - picHomeColorTop.Left
                    picHomeColorBottom.Width = pnlClockBar.Width - picHomeColorTop.Left
                End If

            Else
                'set header2
                If m_objGameDetails.IsHomeTeamOnLeftSwitch = True Then
                    picHomeLogo.Left = 24
                    picAwayLogo.Left = pnlEntryForm.Width - picAwayLogo.Width - 28
                Else
                    picAwayLogo.Left = 24
                    picHomeLogo.Left = pnlEntryForm.Width - picAwayLogo.Width - 28
                End If

                pnlClockBar.Left = 85
                pnlClockBar.Width = pnlEntryForm.Width - 170

                If m_objGameDetails.IsHomeTeamOnLeftSwitch = True Then
                    lklHometeam.Left = 17
                    lklAwayteam.Left = pnlEntryForm.Width - 345
                    lblScoreHome.Left = CInt((pnlEntryForm.Width - 233 - 170) / 2)
                    btnMinuteUp.Left = lblScoreHome.Left + 52
                    btnMinuteDown.Left = lblScoreHome.Left + 52
                    picHomeColorTop.Top = 5
                    picHomeColorTop.Left = 0
                    picHomeColorBottom.Top = 36
                    picHomeColorBottom.Left = 0
                    picHomeColorTop.Width = btnMinuteUp.Left
                    picHomeColorBottom.Width = btnMinuteUp.Left

                Else
                    lklAwayteam.Left = 17
                    lklHometeam.Left = pnlEntryForm.Width - 345
                    lblScoreAway.Left = CInt((pnlEntryForm.Width - 233 - 170) / 2)
                    btnMinuteUp.Left = lblScoreAway.Left + 52
                    btnMinuteDown.Left = lblScoreAway.Left + 52
                    picAwayColorTop.Top = 5
                    picAwayColorTop.Left = 0
                    picAwayColorBottom.Top = 36
                    picAwayColorBottom.Left = 0
                    picAwayColorTop.Width = btnMinuteUp.Left
                    picAwayColorBottom.Width = btnMinuteUp.Left

                End If

                If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                    lblPeriod.Top = 17
                Else
                    lblPeriod.Top = 2
                End If

                lblPeriod.Left = btnMinuteDown.Left + 25 '31
                UdcRunningClock1.Left = btnMinuteDown.Left + 35 'lblPeriod.Left + CInt(lblPeriod.Width / 2) - CInt(UdcRunningClock1.Width / 2) - 10
                btnSecondUp.Left = btnMinuteUp.Left + 98
                btnSecondDown.Left = btnMinuteDown.Left + 98
                btnClock.Left = btnSecondUp.Left + 28
                picOpticalFeed.Left = lblPeriod.Left + 100

                If m_objGameDetails.IsHomeTeamOnLeftSwitch = True Then
                    lblScoreAway.Left = btnClock.Left + 70
                    picAwayColorTop.Top = 5
                    picAwayColorTop.Left = btnClock.Left + btnClock.Width
                    picAwayColorBottom.Top = 36
                    picAwayColorBottom.Left = btnClock.Left + btnClock.Width
                    picAwayColorTop.Width = pnlClockBar.Width - picAwayColorTop.Left
                    picAwayColorBottom.Width = pnlClockBar.Width - picAwayColorTop.Left
                Else
                    lblScoreHome.Left = btnClock.Left + 70
                    picHomeColorTop.Top = 5
                    picHomeColorTop.Left = btnClock.Left + btnClock.Width
                    picHomeColorBottom.Top = 36
                    picHomeColorBottom.Left = btnClock.Left + btnClock.Width
                    picHomeColorTop.Width = pnlClockBar.Width - picHomeColorTop.Left
                    picHomeColorBottom.Width = pnlClockBar.Width - picHomeColorTop.Left
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' SPECIFIC FOR T1 WORKFLOW AND MODULE 2 GAMES
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ResetClockAndScores()
        Try
            SetMenuItems()
            SetScores()
            SetGameClock()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetClockControlVisibility()
        Try
            btnClock.Visible = False
            btnMinuteDown.Visible = False
            btnMinuteUp.Visible = False
            btnSecondDown.Visible = False
            btnSecondUp.Visible = False
            mtxtClockEdit.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetClockControl(ByVal EnableType As Boolean)
        Try
            btnClock.Enabled = EnableType
            btnMinuteDown.Enabled = EnableType
            btnMinuteUp.Enabled = EnableType
            btnSecondDown.Enabled = EnableType
            btnSecondUp.Enabled = EnableType
            mtxtClockEdit.Enabled = EnableType
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Menu_Click(ByVal MenuItemName As String)
        Try

            Select Case MenuItemName
                Case "TouchesFinalized"
                    Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                        Case "2", "3"
                            If MessageDialog.Show(strmessage60, "Touches Finalized", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                m_objGeneral.UpdateTouchesDone(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                                MessageDialog.Show(strmessage61, "Touches Finalized", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                EnableMenuItem(MenuItemName, False)  'TOSOCRS-29 Exit Warning for Unfinalized Touches
                                Exit Try
                            End If
                    End Select

                Case "StartPeriodToolStripMenuItem"
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If

                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3 Then
                        If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                            MessageDialog.Show(strmessage28, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Try
                        End If
                    End If

                    If (m_objGameDetails.ModuleID = 3 Or m_objGameDetails.ModuleID = 4) And m_objGameDetails.IsEditMode Then
                        MessageDialog.Show(strmessage6, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If

                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    ElseIf Not m_objModule2Score Is Nothing Then
                        If m_objGameDetails.IsEditMode Then
                            MessageDialog.Show(strmessage6, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    Dim strCurrTime() As String
                    Dim strTime As String = "00:00"

                    'Check Default Clock value and call Before or After Function
                    'If m_objGameDetails.IsDefaultGameClock Then
                    '    strTime = CalculateTimeElapseAfter(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)), m_objGameDetails.CurrentPeriod)
                    '    strCurrTime = strTime.Split(CChar(":"))
                    '    UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                    'Else
                    '    intCurrTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
                    '    strTime = clsUtility.ConvertSecondToMinute(intCurrTime, False)
                    '    strCurrTime = strTime.Split(CChar(":"))
                    '    UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                    'End If
                    ''

                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Period " & " -> " & "Start Period", 1, 0)
                    'Warning for START PERIOD
                    'Store Current Clock
                    strTime = UdcRunningClock1.URCCurrentTime.ToString

                    'Check for Max Period in backend - rare scenario timeout happened in STRT PERIOD before start period
                    ' saved in database - this caused start period count gets incremented (m_objGameDetails.CurrentPeriod)
                    ' without that entry in database. So get the max period from database and if there is mismatch reassign this
                    ' value to m_objGameDetails.CurrentPeriod
                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        If IsPeriodMismatch() = True Then
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                        End If
                    End If

                    If (m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4) And (m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 2) Then
                        Dim dsLastEventDetails As DataSet = m_objGeneral.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        Dim EventID As Integer = CInt(dsLastEventDetails.Tables(0).Rows(0).Item("EVENT_CODE_ID"))
                        If EventID = clsGameDetails.SHOOTOUT_GOAL Or EventID = clsGameDetails.SHOOTOUT_SAVE Or EventID = clsGameDetails.SHOOTOUT_MISSED Then
                            MessageDialog.Show(strmessage37, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    If m_objGameDetails.CurrentPeriod = 0 Then
                        If m_objGameDetails.ModuleID = PBP_MODULE Then
                            Dim dsOfficialandMatch As New DataSet
                            dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            If dsOfficialandMatch.Tables(1).Rows.Count = 0 Then
                                MessageDialog.Show(strmessage22, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                'show game setup screen
                                Dim objGameSetup As New frmGameSetup
                                objGameSetup.ShowDialog()
                                If m_objGameDetails.Reason <> "" Then
                                    If Not m_objModule1PBP Is Nothing Then
                                        m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                                    ElseIf Not m_objModule1Main Is Nothing Then
                                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                                    End If
                                End If
                                If Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.InsertOfficialsandMatchInfo()
                                    'commented by shirley as Tier 5 start period it switches slides and player names are shown wrongly.
                                    'm_objModule1Main.SwitchSides()
                                    'SetHeader2()
                                End If

                                Dim dsHomeSideInfo As New DataSet
                                dsHomeSideInfo = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                If dsHomeSideInfo.Tables(1).Rows.Count > 0 Then
                                    If CStr(dsHomeSideInfo.Tables(1).Rows(0).Item("HOME_START_SIDE")).Trim = "" Then
                                        UdcRunningClock1.URCToggle()
                                        strCurrTime = strTime.Split(CChar(":"))
                                        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                        SetClockControl(False)
                                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")

                                        Exit Sub
                                    End If
                                Else
                                    'UdcRunningClock1.URCToggle()'TOSOCRS-329
                                    strCurrTime = strTime.Split(CChar(":"))
                                    UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                    SetClockControl(False)
                                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                    Exit Sub
                                End If

                            Else
                                If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE")) Then
                                    MessageDialog.Show(strmessage22, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    'show game setup screen
                                    Dim objGameSetup As New frmGameSetup
                                    objGameSetup.ShowDialog()
                                    If m_objGameDetails.Reason <> "" Then
                                        If Not m_objModule1PBP Is Nothing Then
                                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                                        ElseIf Not m_objModule1Main Is Nothing Then
                                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                                        End If
                                    End If
                                    If Not m_objModule1Main Is Nothing Then
                                        m_objModule1Main.InsertOfficialsandMatchInfo()
                                        m_objModule1Main.SwitchSides()
                                        SetHeader2()
                                    End If

                                    Dim dsHomeSideInfo As New DataSet
                                    dsHomeSideInfo = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                    If dsHomeSideInfo.Tables(1).Rows.Count > 0 Then
                                        If CStr(dsHomeSideInfo.Tables(1).Rows(0).Item("HOME_START_SIDE")).Trim = "" Then
                                            UdcRunningClock1.URCToggle()
                                            strCurrTime = strTime.Split(CChar(":"))
                                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                            SetClockControl(False)
                                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")

                                            Exit Sub
                                        End If
                                    Else
                                        UdcRunningClock1.URCToggle()
                                        strCurrTime = strTime.Split(CChar(":"))
                                        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                        SetClockControl(False)
                                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")

                                        Exit Sub
                                    End If
                                Else
                                    If CStr(dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE")).Trim = "" Then
                                        MessageDialog.Show(strmessage22, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        'show game setup screen
                                        Dim objGameSetup As New frmGameSetup
                                        objGameSetup.ShowDialog()
                                        If m_objGameDetails.Reason <> "" Then
                                            If Not m_objModule1PBP Is Nothing Then
                                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                                            ElseIf Not m_objModule1Main Is Nothing Then
                                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                                            End If
                                        End If
                                        If Not m_objModule1Main Is Nothing Then
                                            m_objModule1Main.InsertOfficialsandMatchInfo()
                                            m_objModule1Main.SwitchSides()
                                            SetHeader2()
                                        End If

                                        Dim dsHomeSideInfo As DataSet
                                        dsHomeSideInfo = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                        If dsHomeSideInfo.Tables(1).Rows.Count > 0 Then
                                            If CStr(dsHomeSideInfo.Tables(1).Rows(0).Item("HOME_START_SIDE")).Trim = "" Then
                                                UdcRunningClock1.URCToggle()
                                                strCurrTime = strTime.Split(CChar(":"))
                                                UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                                SetClockControl(False)
                                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")

                                                Exit Sub
                                            End If
                                        Else
                                            UdcRunningClock1.URCToggle()
                                            strCurrTime = strTime.Split(CChar(":"))
                                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                            SetClockControl(False)
                                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")

                                            Exit Sub
                                        End If
                                    End If
                                End If

                            End If
                        ElseIf m_objGameDetails.ModuleID = TOUCHES_MODULE Then

                            'Dim DtHomeStart As DataSet
                            'DtHomeStart = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            'setStartClock()
                            'If DtHomeStart.Tables.Count > 0 Then
                            '    If DtHomeStart.Tables(0).Rows.Count > 0 Then
                            '        If m_objGameDetails.CurrentPeriod < 3 Then
                            '            If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) Then
                            '                MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            '                'show game setup screen
                            '                Dim m_objHomestart As New frmHomeStart
                            '                m_objHomestart.ShowDialog()

                            '                Dim DtHomeStartInfo As DataSet
                            '                DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            '                If DtHomeStartInfo.Tables.Count > 0 Then
                            '                    If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                            '                        UdcRunningClock1.URCToggle()
                            '                        strCurrTime = strTime.Split(CChar(":"))
                            '                        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            '                        SetClockControl(False)
                            '                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            '                        Exit Sub
                            '                    End If

                            '                End If
                            '            Else
                            '                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                            '                    MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            '                    'show game setup screen
                            '                    Dim m_objHomestart As New frmHomeStart
                            '                    m_objHomestart.ShowDialog()

                            '                    Dim DtHomeStartInfo As DataSet
                            '                    DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            '                    If DtHomeStartInfo.Tables.Count > 0 Then
                            '                        If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                            '                            UdcRunningClock1.URCToggle()
                            '                            strCurrTime = strTime.Split(CChar(":"))
                            '                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            '                            SetClockControl(False)
                            '                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            '                            Exit Sub
                            '                        End If

                            '                    End If

                            '                End If
                            '            End If

                            '        Else
                            '            If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) Then
                            '                MessageDialog.Show(strmessage53, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            '                'show game setup screen
                            '                Dim m_objHomestart As New frmHomeStart
                            '                m_objHomestart.ShowDialog()

                            '                Dim DtHomeStartInfo As DataSet
                            '                DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            '                If DtHomeStartInfo.Tables.Count > 0 Then
                            '                    If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                            '                        UdcRunningClock1.URCToggle()
                            '                        strCurrTime = strTime.Split(CChar(":"))
                            '                        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            '                        SetClockControl(False)
                            '                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            '                        Exit Sub
                            '                    End If

                            '                End If
                            '            Else
                            '                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                            '                    MessageDialog.Show(strmessage53, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            '                    'show game setup screen
                            '                    Dim m_objHomestart As New frmHomeStart
                            '                    m_objHomestart.ShowDialog()

                            '                    Dim DtHomeStartInfo As DataSet
                            '                    DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            '                    If DtHomeStartInfo.Tables.Count > 0 Then
                            '                        If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                            '                            UdcRunningClock1.URCToggle()
                            '                            strCurrTime = strTime.Split(CChar(":"))
                            '                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            '                            SetClockControl(False)
                            '                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            '                            Exit Sub
                            '                        End If

                            '                    End If
                            '                End If
                            '            End If

                            '        End If
                            '    Else
                            '        MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            '        'show game setup screen
                            '        Dim m_objHomestart As New frmHomeStart
                            '        m_objHomestart.ShowDialog()

                            '        Dim DtHomeStartInfo As DataSet
                            '        DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            '        If DtHomeStartInfo.Tables.Count > 0 Then
                            '            If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                            '                UdcRunningClock1.URCToggle()
                            '                strCurrTime = strTime.Split(CChar(":"))
                            '                UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            '                SetClockControl(False)
                            '                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            '                Exit Sub
                            '            End If
                            '
                            '        End If

                            '    End If

                            'Else
                            '    MessageDialog.Show(strmessage38, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                            '    'show game setup screen
                            '    Dim m_objHomestart As New frmHomeStart
                            '    m_objHomestart.ShowDialog()

                            '    Dim DtHomeStartInfo As DataSet
                            '    DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            '    If DtHomeStartInfo.Tables.Count > 0 Then
                            '        If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim = "" Then
                            '            UdcRunningClock1.URCToggle()
                            '            strCurrTime = strTime.Split(CChar(":"))
                            '            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            '            SetClockControl(False)
                            '            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            '            Exit Sub
                            '        End If

                            '    End If

                            'End If

                        End If

                        'TOSOCRS-116
                        m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                        If MessageDialog.Show(strmessage21, "Start Game", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                            'TOSOCRS-116
                            If m_objGameDetails.CurrentPeriod = 0 Then
                                lblPeriod.ForeColor = Color.WhiteSmoke
                                lblPeriod.Text = "Pre-Game"
                                strTime = "00:00"
                            End If
                            'UdcRunningClock1.URCToggle() 'TOSOCRS-329
                            strCurrTime = strTime.Split(CChar(":"))
                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            SetClockControl(False)
                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            Exit Sub
                        Else
                            setStartClock() 'TOSOCRS-329
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                            If m_objGameDetails.ModuleID = PBP_MODULE Then
                                Dim dsStartPeriodCount As DataSet
                                'This check is done to prevent duplicate START PERIOD in multi reporter
                                dsStartPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.STARTPERIOD)

                                If CInt(dsStartPeriodCount.Tables(2).Rows(0).Item("PeriodCount")) > m_objGameDetails.CurrentPeriod Then
                                    m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                                    If m_objGameDetails.CurrentPeriod = 2 Then
                                        lblPeriod.Text = SECOND_HALF_DESC
                                    ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                        lblPeriod.Text = FIRST_EXTRATIME_DESC
                                        EnableMenuItem("EndGameToolStripMenuItem", False)
                                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                                    ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                        lblPeriod.Text = SECOND_EXTRATIME_DESC
                                    End If
                                    lblPeriod.ForeColor = Color.White
                                    Exit Sub
                                End If
                            End If

                        End If
                    Else
                        'Real Madrid-Depor touches (if any refresh comes while poping up the message cur period is restting to wrong value)
                        Dim LatPeriod As Integer
                        m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                        LatPeriod = m_objGameDetails.CurrentPeriod

                        If MessageDialog.Show(strmessage20, "Start Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                            'm_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                            m_objGameDetails.CurrentPeriod = LatPeriod - 1
                            'UdcRunningClock1.URCToggle() 'TOSOCRS-329
                            strCurrTime = strTime.Split(CChar(":"))
                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            SetClockControl(False)
                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")

                            Exit Sub
                        Else
                            If (LatPeriod <> 3) Then 'TOSOCRS-329
                                setStartClock()
                            End If
                            'm_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                            m_objGameDetails.CurrentPeriod = LatPeriod - 1

                            If m_objGameDetails.ModuleID = PBP_MODULE Then
                                Dim dsStartPeriodCount As DataSet
                                'This check is done to prevent duplicate START PERIOD in multi reporter
                                dsStartPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.STARTPERIOD)

                                If CInt(dsStartPeriodCount.Tables(2).Rows(0).Item("PeriodCount")) > m_objGameDetails.CurrentPeriod Then
                                    m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                                    If m_objGameDetails.CurrentPeriod = 2 Then
                                        lblPeriod.Text = SECOND_HALF_DESC
                                    ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                        If m_objGameDetails.ModuleID = PBP_MODULE Then
                                            'Arindam 14-Mar-2012 open gamesetup screen to take home side left/right
                                            ''
                                            Dim dsOfficialandMatch As New DataSet
                                            dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                            setStartClock()
                                            If dsOfficialandMatch.Tables(2).Rows.Count = 0 Then
                                                MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                'show game setup screen
                                                Dim objGameSetup As New frmGameSetup
                                                objGameSetup.ShowDialog()
                                            Else
                                                If IsDBNull(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")) Then
                                                    MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                    'show game setup screen
                                                    Dim objGameSetup As New frmGameSetup
                                                    objGameSetup.ShowDialog()
                                                    ''
                                                Else
                                                    If CStr(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")).Trim = "" Then
                                                        MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                        'show game setup screen
                                                        Dim objGameSetup As New frmGameSetup
                                                        objGameSetup.ShowDialog()
                                                    End If
                                                    ''
                                                End If
                                            End If
                                            ''

                                        End If

                                        lblPeriod.Text = FIRST_EXTRATIME_DESC
                                        EnableMenuItem("EndGameToolStripMenuItem", False)
                                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                                    ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                        lblPeriod.Text = SECOND_EXTRATIME_DESC
                                    End If
                                    lblPeriod.ForeColor = Color.White
                                    Exit Sub
                                End If

                            End If

                        End If
                    End If

                    'shravani

                    If m_objGameDetails.CurrentPeriod = 2 And m_objGameDetails.HomeScore = m_objGameDetails.AwayScore Then
                        If (MessageDialog.Show(strmessage39, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No) Then
                            EnableMenuItem("EndGameToolStripMenuItem", True)
                            'UdcRunningClock1.URCToggle() 'TOSOCRS-329
                            strCurrTime = strTime.Split(CChar(":"))
                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                            SetClockControl(False)
                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            Exit Sub
                        Else
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                            setStartClock() 'TOSOCRS-329
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                        End If
                    End If
                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        EnableMenuItem("ManagerExpulsionToolStripMenuItem", True)
                    End If
                    SetClockControl(True)
                    'UdcRunningClock1.URCToggle()
                    pnlEntryForm.Enabled = True
                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.lklNewHomeBooking.Enabled = True
                            m_objModule1Main.lklNewHomeGoal.Enabled = True
                            m_objModule1Main.lklNewHomePenalty.Enabled = True
                            m_objModule1Main.lklNewHomeSub.Enabled = True
                            m_objModule1Main.lklNewVisitBooking.Enabled = True
                            m_objModule1Main.lklNewVisitGoal.Enabled = True
                            m_objModule1Main.lklNewVisitPenalty.Enabled = True
                            m_objModule1Main.lklNewVisitSub.Enabled = True

                            m_objModule1Main.lklEditHomeBookings.Enabled = True
                            m_objModule1Main.lklEditHomeGoals.Enabled = True
                            m_objModule1Main.lklEditHomePenalty.Enabled = True
                            m_objModule1Main.lklEditHomeSubstitutions.Enabled = True
                            m_objModule1Main.lklEditVisitBookings.Enabled = True
                            m_objModule1Main.lklEditVisitGoals.Enabled = True
                            m_objModule1Main.lklEditVisitPenalty.Enabled = True
                            m_objModule1Main.lklEditVisitSubstitutions.Enabled = True
                        End If
                        'If m_objGameDetails.CurrentPeriod = 0 Then
                        '    m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                        '    If Not m_objModule1Main Is Nothing Then
                        '        m_objModule1Main.InsertPlayByPlayData(INTGAMESTART, "")
                        '        m_objModule1Main.InsertPlayByPlayData(INTSTARTPERIOD, "")
                        '    ElseIf Not m_objModule1PBP Is Nothing Then
                        '        m_objModule1PBP.DisableAllControls(True)
                        '        m_objModule1PBP.AddPBPGeneralEvents(INTGAMESTART, "")
                        '        m_objModule1PBP.AddPBPGeneralEvents(INTSTARTPERIOD, "")
                        '    End If
                        'Else

                        '    m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                        '    'Added by Arindam
                        '    If Not m_objModule1Main Is Nothing Then
                        '        m_objModule1Main.InsertPlayByPlayData(INTSTARTPERIOD, "")
                        '    ElseIf Not m_objModule1PBP Is Nothing Then
                        '        m_objModule1PBP.DisableAllControls(True)
                        '        m_objModule1PBP.AddPBPGeneralEvents(INTSTARTPERIOD, "")
                        '    End If
                        'End If
                        ''
                        If m_objGameDetails.CurrentPeriod = 0 Then
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(INTGAMESTART, "")
                                m_objModule1Main.InsertPlayByPlayData(INTSTARTPERIOD, "")
                                If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                    m_objModule1Main.InsertPlayByPlayData(INTCLOCKSTART, "CLOCKSTART")
                                End If

                            ElseIf Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.DisableAllControls(True)
                                m_objModule1PBP.AddPBPGeneralEvents(INTGAMESTART, "")
                                m_objModule1PBP.AddPBPGeneralEvents(INTSTARTPERIOD, "")
                                If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                    m_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTART, "CLOCKSTART")
                                End If
                                'Shravani
                            ElseIf Not m_objModule2Score Is Nothing Then
                                m_objModule2Score.DisableAllControls(True)
                                m_objModule2Score.AddPBPGeneralEvents(INTGAMESTART, "")
                                m_objModule2Score.AddPBPGeneralEvents(INTSTARTPERIOD, "")
                            Else
                                If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                                    Dim intTimeElapsed As Integer
                                    Select Case m_objGameDetails.CurrentPeriod
                                        Case 1
                                            intTimeElapsed = 0
                                        Case 2
                                            If m_objGameDetails.IsDefaultGameClock = True Then
                                                intTimeElapsed = 0
                                            Else
                                                intTimeElapsed = 0
                                            End If
                                        Case 3
                                            If m_objGameDetails.IsDefaultGameClock = True Then
                                                intTimeElapsed = 0
                                            Else
                                                intTimeElapsed = 0
                                            End If
                                        Case 4
                                            If m_objGameDetails.IsDefaultGameClock = True Then
                                                intTimeElapsed = 0
                                            Else
                                                intTimeElapsed = 0
                                            End If
                                    End Select
                                    Dim m_objclsTouches As STATS.SoccerBL.clsTouches = clsTouches.GetInstance()
                                    m_objclsTouches.AddTouches(frmTouches.CreateTouchDataObject(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intTimeElapsed, m_objGameDetails.CurrentPeriod, 0, 0, 14, m_objGameDetails.ReporterRole, -1, -1, -1, -1, m_objGameDetails.TouchesTeamID))
                                    m_objGameDetails.TouchesData.Tables(0).Rows.Clear()
                                End If
                            End If
                        Else
                            If m_objGameDetails.CurrentPeriod <= 4 Then
                                m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                                'Added by Arindam
                                If m_objGameDetails.CurrentPeriod = 3 And m_objGameDetails.ModuleID = PBP_MODULE Then
                                    'Arindam 14-Mar-2012 open gamesetup screen to take home side left/right
                                    ''
                                    Dim dsOfficialandMatch As New DataSet
                                    dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                    setStartClock()
                                    If dsOfficialandMatch.Tables(2).Rows.Count = 0 Then
                                        MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        'show game setup screen
                                        Dim objGameSetup As New frmGameSetup
                                        objGameSetup.ShowDialog()
                                    Else
                                        If IsDBNull(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")) Then
                                            MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            'show game setup screen
                                            Dim objGameSetup As New frmGameSetup
                                            objGameSetup.ShowDialog()
                                            ''
                                        Else
                                            If CStr(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")).Trim = "" Then
                                                MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                'show game setup screen
                                                Dim objGameSetup As New frmGameSetup
                                                objGameSetup.ShowDialog()
                                            End If
                                            ''
                                        End If
                                    End If
                                End If
                                If Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.InsertPlayByPlayData(INTSTARTPERIOD, "")
                                    If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                        m_objModule1Main.InsertPlayByPlayData(INTCLOCKSTART, "CLOCKSTART")
                                    End If
                                ElseIf Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.DisableAllControls(True)
                                    m_objModule1PBP.AddPBPGeneralEvents(INTSTARTPERIOD, "")
                                    If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                        m_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTART, "CLOCKSTART")
                                    End If
                                    GetPenaltyShootoutScore()
                                    'Shravani
                                ElseIf Not m_objModule2Score Is Nothing Then
                                    m_objModule2Score.DisableAllControls(True)
                                    m_objModule2Score.AddPBPGeneralEvents(INTSTARTPERIOD, "")
                                End If
                            End If

                        End If

                        ''

                        'If m_objGameDetails.IsDefaultGameClock = False Then
                        '    UdcRunningClock1.URCSetTime(0, 0)

                        'End If
                    Else
                        If m_objGameDetails.CurrentPeriod <= 4 Then
                            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                            If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                                Dim intTimeElapsed As Integer
                                '--Arindam 27-Mar-2012--'
                                ' ''If m_objGameDetails.CurrentPeriod = 3 Then
                                ' ''    'Arindam 14-Mar-2012 open gamesetup screen to take home side left/right
                                ' ''    ''
                                ' ''    Dim DtHomeStart As DataSet
                                ' ''    DtHomeStart = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                ' ''    setStartClock()
                                ' ''    If DtHomeStart.Tables.Count > 0 Then
                                ' ''        If DtHomeStart.Tables(0).Rows.Count > 0 Then
                                ' ''            If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) Then
                                ' ''                MessageDialog.Show(strmessage53, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                                ' ''                'show game setup screen
                                ' ''                Dim m_objHomestart As New frmHomeStart
                                ' ''                m_objHomestart.ShowDialog()

                                ' ''                Dim DtHomeStartInfo As DataSet
                                ' ''                DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                ' ''                If DtHomeStartInfo.Tables.Count > 0 Then
                                ' ''                    If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                                ' ''                        UdcRunningClock1.URCToggle()
                                ' ''                        strCurrTime = strTime.Split(CChar(":"))
                                ' ''                        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                ' ''                        SetClockControl(False)
                                ' ''                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                ' ''                        Exit Sub
                                ' ''                    End If

                                ' ''                End If
                                ' ''            Else
                                ' ''                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                                ' ''                    MessageDialog.Show(strmessage53, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                                ' ''                    'show game setup screen
                                ' ''                    Dim m_objHomestart As New frmHomeStart
                                ' ''                    m_objHomestart.ShowDialog()

                                ' ''                    Dim DtHomeStartInfo As DataSet
                                ' ''                    DtHomeStartInfo = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                ' ''                    If DtHomeStartInfo.Tables.Count > 0 Then
                                ' ''                        If CStr(DtHomeStartInfo.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim = "" Then
                                ' ''                            UdcRunningClock1.URCToggle()
                                ' ''                            strCurrTime = strTime.Split(CChar(":"))
                                ' ''                            UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                                ' ''                            SetClockControl(False)
                                ' ''                            btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                ' ''                            Exit Sub
                                ' ''                        End If

                                ' ''                    End If
                                ' ''                End If
                                ' ''            End If

                                ' ''        End If
                                ' ''    End If

                                ' ''    'If dsOfficialandMatch.Tables(2).Rows.Count = 0 Then
                                ' ''    '    MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ' ''    '    'show game setup screen
                                ' ''    '    Dim objGameSetup As New frmGameSetup
                                ' ''    '    objGameSetup.ShowDialog()
                                ' ''    'Else
                                ' ''    '    If IsDBNull(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")) Then
                                ' ''    '        MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ' ''    '        'show game setup screen
                                ' ''    '        Dim objGameSetup As New frmGameSetup
                                ' ''    '        objGameSetup.ShowDialog()
                                ' ''    '        ''
                                ' ''    '    Else
                                ' ''    '        If CStr(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")).Trim = "" Then
                                ' ''    '            MessageDialog.Show(strmessage52, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ' ''    '            'show game setup screen
                                ' ''    '            Dim objGameSetup As New frmGameSetup
                                ' ''    '            objGameSetup.ShowDialog()
                                ' ''    '        End If
                                ' ''    '        ''
                                ' ''    '    End If
                                ' ''    'End If
                                ' ''End If
                                ''

                                If m_objGameDetails.IsDefaultGameClock Then
                                    intTimeElapsed = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)) + 2)
                                Else
                                    'setting the time to "0" if prefrences are changed to 0-45
                                    UdcRunningClock1.URCSetTime(0, 0) 'TOSOCRS-116
                                    intTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                                End If

                                'Select Case m_objGameDetails.CurrentPeriod

                                '    'If m_objGameDetails.IsDefaultGameClock Then

                                '    '    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)) + 2)

                                '    'Else
                                '    '    drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                                '    'End If

                                '    Case 1
                                '        intTimeElapsed = 0
                                '    Case 2
                                '        If m_objGameDetails.IsDefaultGameClock = True Then
                                '            intTimeElapsed = 0
                                '        Else
                                '            intTimeElapsed = 0
                                '        End If
                                '    Case 3
                                '        If m_objGameDetails.IsDefaultGameClock = True Then
                                '            intTimeElapsed = 0
                                '        Else
                                '            intTimeElapsed = 0
                                '        End If
                                '    Case 4
                                '        If m_objGameDetails.IsDefaultGameClock = True Then
                                '            intTimeElapsed = 0
                                '        Else
                                '            intTimeElapsed = 0
                                '        End If
                                'End Select
                                Dim m_objclsTouches As STATS.SoccerBL.clsTouches = clsTouches.GetInstance()
                                frmTouches.CreateTouchDataObject(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, m_objGameDetails.CurrentPeriod, 0, 0, 14, m_objGameDetails.ReporterRole, -1, -1, -1, -1, m_objGameDetails.TouchesTeamID)
                                m_objclsTouches.AddTouches(frmTouches.CreateTouchDataObject(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intTimeElapsed, m_objGameDetails.CurrentPeriod, 0, 0, 16, m_objGameDetails.ReporterRole, -1, -1, -1, -1, m_objGameDetails.TouchesTeamID))
                                m_objTouches.DisplayTouches()
                                m_objGameDetails.TouchesData.Tables(0).Rows.Clear()
                            End If
                        End If
                        If Not m_objTouches Is Nothing And m_objGameDetails.CurrentPeriod > 0 Then
                            m_objTouches.pnlTouchTypes.Enabled = True
                            m_objTouches.UdcSoccerField1.Enabled = True
                            m_objTouches.btnSave.Enabled = True
                            m_objTouches.btnClear.Enabled = True
                        End If
                    End If

                    'setStartClock()

                    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 3 Or m_objGameDetails.CurrentPeriod = 4 Then
                        If m_objGameDetails.IsHalfTimeSwap Then
                            'Added by Arindam 18-Jun-09
                            Select Case m_objGameDetails.ModuleID
                                Case COMMENTARY_MODULE
                                    m_objCommentary.SwitchSides()
                                Case TOUCHES_MODULE
                                    If m_objGameDetails.SerialNo = 1 Then  'TOSOCRS-38
                                        m_objTouches.SwitchSidesPrimary()
                                    Else
                                        m_objTouches.SwitchSides()
                                    End If
                                Case PBP_MODULE, MULTIGAME_PBP_MODULE
                                    If Not m_objModule1Main Is Nothing Then
                                        m_objModule1Main.SwitchSides()
                                    ElseIf Not m_objModule1PBP Is Nothing Then
                                        m_objModule1PBP.SwitchSides()
                                    End If
                            End Select
                            SetHeader2()
                        End If
                        ' Arindam Clock - 45:00 + 00:00 - Pass this value to Clock
                        'UdcRunningClock1.URCSetTime(45, 0)
                        If m_objGameDetails.CurrentPeriod = 2 Then
                            lblPeriod.Text = SECOND_HALF_DESC
                        ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                            lblPeriod.Text = FIRST_EXTRATIME_DESC
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                            lblPeriod.Text = SECOND_EXTRATIME_DESC
                            'ElseIf m_objGameDetails.CurrentPeriod = 5 Then
                            '    lblPeriod.Text = "Shootout"
                            '    'If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then

                            '    frmPenaltyShootout.Show()
                            '    If Not m_objModule1PBP Is Nothing Then
                            '        m_objModule1PBP.DisplayPBP()
                            '    End If
                            'End If
                        End If
                    End If
                    If UdcRunningClock1.URCIsRunning = False And m_objGameDetails.CurrentPeriod <> 5 Then
                        UdcRunningClock1.URCToggle()
                    End If
                    If UdcRunningClock1.URCIsRunning Then
                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    Else
                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                    End If

                    If m_objGameDetails.CurrentPeriod > 1 Then
                        ''
                        If m_objGameDetails.CurrentPeriod = 3 Then
                            If m_objGameDetails.IsHomeTeamOnLeft = True Then
                                If Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                                ElseIf Not m_objTouches Is Nothing Then
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True
                                End If
                                m_objGameDetails.isHomeDirectionLeft = True
                            Else
                                If Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                                ElseIf Not m_objTouches Is Nothing Then
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = False
                                End If
                                m_objGameDetails.isHomeDirectionLeft = False
                            End If
                        Else
                            If Not m_objModule1PBP Is Nothing Then
                                If m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True Then
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                                    m_objGameDetails.isHomeDirectionLeft = False
                                Else
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                                    m_objGameDetails.isHomeDirectionLeft = True
                                End If
                            ElseIf Not m_objTouches Is Nothing Then
                                If m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True Then
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = False
                                    'm_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.IsHomeTeamOnLeft

                                    m_objGameDetails.isHomeDirectionLeft = False
                                Else
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True
                                    m_objGameDetails.isHomeDirectionLeft = True
                                End If
                            End If
                        End If

                        ''

                    ElseIf m_objGameDetails.CurrentPeriod = 1 Then
                        lblPeriod.Text = FIRST_HALF_DESC
                        If m_objGameDetails.IsHomeTeamOnLeft = True Then
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                            ElseIf Not m_objTouches Is Nothing Then
                                m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True
                            End If
                            m_objGameDetails.isHomeDirectionLeft = True
                        Else
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                            ElseIf Not m_objTouches Is Nothing Then
                                m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = False
                            End If
                            m_objGameDetails.isHomeDirectionLeft = False
                        End If
                    End If

                    If Not m_objTouches Is Nothing Then
                        m_objTouches.DisableControls(True)
                        m_objTouches.txtTime.Text = ""
                    End If
                    If Not m_objCommentary Is Nothing Then
                        m_objCommentary.txtTime.Text = ""
                        m_objCommentary.txtTime.Enabled = True
                    End If
                    'If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                    lblPeriod.ForeColor = Color.White
                    'End If
                    'If Not m_objCommentary Is Nothing Then
                    '    m_objCommentary.DisableControls(True)
                    'End If
                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                    EnableMenuItem("EndPeriodToolStripMenuItem", True)
                    If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        If Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.btnTime.Enabled = True
                        End If
                    End If
                    If m_objGameDetails.IsSportVUAvailable = True Then
                        If Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.picOpticalFeed.Visible = True
                            m_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-OFF.gif")
                        End If
                    End If
                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        If m_objGameDetails.CurrentPeriod > 0 Then
                            EnableMenuItem("DelayedToolStripMenuItem", True)
                        End If
                    End If

                Case "EndPeriodToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Period" & " -> " & "End Period", 1, 0)
                    'If Not m_objModule1PBP Is Nothing Then
                    '    If Not m_objModule1PBP.correctRoster() Then
                    '        Exit Sub
                    '    End If
                    'End If
                    'If Not m_objModule1Main Is Nothing Then
                    '    If Not m_objModule1Main.correctRoster() Then
                    '        Exit Sub
                    '    End If
                    'End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3 Then
                        If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                            MessageDialog.Show(strmessage29, "End Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Try
                        End If
                    End If

                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "End Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    'If essageDialog.Show("Are you sure to end the period?", "End Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    Dim strPeriodString As String = ""

                    If m_objGameDetails.CurrentPeriod = 1 Then
                        strPeriodString = "Halftime"
                    ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                        strPeriodString = "Full Time"
                    ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                        strPeriodString = "ET Break"
                    ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                        strPeriodString = "2nd ET"
                    End If

                    If MessageDialog.Show(strmessage19 + "  [ " & strPeriodString & "  (" & m_objGameDetails.HomeTeamAbbrev & ":  " & m_objGameDetails.HomeScore & ", " & m_objGameDetails.AwayTeamAbbrev & ":  " & m_objGameDetails.AwayScore & ") ] ", "End Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If

                    'If MessageDialog.Show(strmessage19 + " [ " & strPeriodString & "  (" & m_objGameDetails.HomeTeamAbbrev & ":  " & m_objGameDetails.HomeScore & ", " & m_objGameDetails.AwayTeamAbbrev & ":  " & m_objGameDetails.AwayScore & ") ] ", "End Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    '    If m_objGameDetails.ModuleID = PBP_MODULE Then
                    '        Dim dsEndPeriodCount As DataSet
                    '        'Dim endPeriodCount As Integer
                    '        dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.ENDPERIOD)

                    '        If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) >= m_objGameDetails.CurrentPeriod Then
                    '            Exit Sub
                    '        End If
                    '    End If

                    '    If m_objGameDetails.CurrentPeriod = 2 Then
                    '        If m_objGameDetails.ModuleID = 1 Then
                    '            If (essageDialog.Show(" Do you want to enter the player ratings ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    '                Dim objfrmTeamSetup As New frmTeamSetup("Home")
                    '                objfrmTeamSetup.ShowDialog()
                    '                Me.Close()
                    '            Else
                    '                Me.Close()
                    '            End If
                    '        End If
                    '    End If
                    'Else
                    '    Exit Sub
                    'End If

                    If Not m_objTouches Is Nothing Then
                        m_objTouches.txtTime.Text = ""
                    End If
                    If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        'Ask reporter to enter the score
                        'frmValidateScore.ShowDialog()
                        Dim m_objValidateScore As New frmValidateScore
                        If m_objValidateScore.ShowDialog = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        Else
                            'essageDialog.Show("Bull's Eye!!!!!", "Validate Score", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            lblPeriod.ForeColor = Color.Red
                        End If
                    End If
                    lblPeriod.ForeColor = Color.Red
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objModule1PBP.dblstEventDetails01.Visible = True Or m_objGameDetails.IsReplaceMode Or m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Then
                            MessageDialog.Show(strmessage18, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If
                    If UdcRunningClock1.URCIsRunning Then
                        UdcRunningClock1.URCToggle()
                    End If
                    If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                        If Not m_objTouches Is Nothing Then
                            m_objTouches.DisableControls(False)
                        End If
                    End If

                    'If m_objGameDetails.CurrentPeriod = 1 Then
                    '    UdcRunningClock1.URCSetTime(45, 0)
                    'ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                    '    ''NEED TO IMPLEMENT THIS BASED ON DATE TIME SELECTION
                    '    If m_objGameDetails.IsDefaultGameClock Then
                    '        UdcRunningClock1.URCSetTime(90, 0)
                    '    Else
                    '        UdcRunningClock1.URCSetTime(45, 0)
                    '    End If

                    'ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                    '    If m_objGameDetails.IsDefaultGameClock Then
                    '        UdcRunningClock1.URCSetTime(105, 0)
                    '    Else
                    '        UdcRunningClock1.URCSetTime(15, 0)
                    '    End If
                    'ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                    '    If m_objGameDetails.IsDefaultGameClock Then
                    '        UdcRunningClock1.URCSetTime(120, 0)
                    '    Else
                    '        UdcRunningClock1.URCSetTime(15, 0)
                    '    End If
                    'End If

                    If UdcRunningClock1.URCIsRunning Then
                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    Else
                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                    End If
                    SetClockControl(False)
                    Dim blnEndGame As Boolean = False
                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        'Arindam_1-2-11: Adding game over same time
                        If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then

                            If MessageDialog.Show(strmessage48, "End Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                If Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.InsertPlayByPlayData(INTENDPERIOD, "Save")
                                    If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                        m_objModule1Main.InsertPlayByPlayData(INTCLOCKSTOP, "CLOCKSTOP")
                                    End If
                                    m_objModule1Main.lklNewHomeBooking.Enabled = False
                                    m_objModule1Main.lklNewHomeGoal.Enabled = False
                                    m_objModule1Main.lklNewHomePenalty.Enabled = False
                                    m_objModule1Main.lklNewHomeSub.Enabled = False
                                    m_objModule1Main.lklNewVisitBooking.Enabled = False
                                    m_objModule1Main.lklNewVisitGoal.Enabled = False
                                    m_objModule1Main.lklNewVisitPenalty.Enabled = False
                                    m_objModule1Main.lklNewVisitSub.Enabled = False
                                ElseIf Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                    If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                        m_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTOP, "CLOCKSTOP")
                                    End If
                                    m_objModule1PBP.DisableAllControls(False)
                                ElseIf Not m_objModule2Score Is Nothing Then
                                    m_objModule2Score.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                    m_objModule2Score.DisableAllControls(False)
                                End If
                            Else 'We are going to enter END GAME ALSO
                                'Dim dsOfficialandMatch As New DataSet
                                'dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                'If dsOfficialandMatch.Tables.Count = 0 Then
                                '    MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                'Else
                                '    If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                '        If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                '            MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                '        ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                '            MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                '        Else
                                '            MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                '        End If
                                '    Else
                                '        If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                '            If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                '                MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                '            End If
                                '        End If
                                '        If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                '            If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                '                MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                '            End If
                                '        End If
                                '    End If
                                'End If

                                m_objGameDetails.IsEndGame = True
                                blnEndGame = True
                                If Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.InsertPlayByPlayData(INTENDPERIOD, "Save")
                                    If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                        m_objModule1Main.InsertPlayByPlayData(INTCLOCKSTOP, "CLOCKSTOP")
                                    End If
                                    m_objModule1Main.lklNewHomeBooking.Enabled = False
                                    m_objModule1Main.lklNewHomeGoal.Enabled = False
                                    m_objModule1Main.lklNewHomePenalty.Enabled = False
                                    m_objModule1Main.lklNewHomeSub.Enabled = False
                                    m_objModule1Main.lklNewVisitBooking.Enabled = False
                                    m_objModule1Main.lklNewVisitGoal.Enabled = False
                                    m_objModule1Main.lklNewVisitPenalty.Enabled = False
                                    m_objModule1Main.lklNewVisitSub.Enabled = False
                                    If m_objGameDetails.IsEndGame = True Then
                                        m_objModule1Main.InsertPlayByPlayData(INTENDGAME, "Save")
                                    End If
                                ElseIf Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                    If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                        m_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTOP, "CLOCKSTOP")
                                    End If
                                    m_objModule1PBP.DisableAllControls(False)
                                    If m_objGameDetails.IsEndGame = True Then
                                        m_objModule1PBP.AddPBPGeneralEvents(INTENDGAME, "Save")
                                    End If
                                    m_objModule1PBP.btnTime.Enabled = False
                                ElseIf Not m_objModule2Score Is Nothing Then
                                    m_objModule2Score.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                    m_objModule2Score.DisableAllControls(False)
                                    If m_objGameDetails.IsEndGame = True Then
                                        m_objModule2Score.AddPBPGeneralEvents(INTENDGAME, "Save")
                                    End If
                                End If
                            End If
                        Else
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(INTENDPERIOD, "Save")
                                If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                    m_objModule1Main.InsertPlayByPlayData(INTCLOCKSTOP, "CLOCKSTOP")
                                End If
                                m_objModule1Main.lklNewHomeBooking.Enabled = False
                                m_objModule1Main.lklNewHomeGoal.Enabled = False
                                m_objModule1Main.lklNewHomePenalty.Enabled = False
                                m_objModule1Main.lklNewHomeSub.Enabled = False
                                m_objModule1Main.lklNewVisitBooking.Enabled = False
                                m_objModule1Main.lklNewVisitGoal.Enabled = False
                                m_objModule1Main.lklNewVisitPenalty.Enabled = False
                                m_objModule1Main.lklNewVisitSub.Enabled = False
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                If m_objGameDetails.ModuleID <> MULTIGAME_PBP_MODULE Then
                                    m_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTOP, "CLOCKSTOP")
                                End If
                                m_objModule1PBP.DisableAllControls(False)
                            ElseIf Not m_objModule2Score Is Nothing Then
                                m_objModule2Score.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                m_objModule2Score.DisableAllControls(False)
                            End If
                        End If
                    End If
                    Dim abanData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, clsGameDetails.ABANDONED, m_objGameDetails.ModuleID)

                    'SANDEEP: BELOW CODE IS ADDED TO ENTER END PERIOD EVENT.
                    If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                        Dim m_objclsTouches As STATS.SoccerBL.clsTouches = clsTouches.GetInstance()
                        Dim intTimeElapsed As Integer
                        If m_objGameDetails.IsDefaultGameClock Then
                            intTimeElapsed = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
                        Else
                            intTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                        End If
                        'Select Case m_objGameDetails.CurrentPeriod
                        '    Case 1
                        '        intTimeElapsed = 45 * 60
                        '    Case 2
                        '        If m_objGameDetails.IsDefaultGameClock = True Then
                        '            intTimeElapsed = 45 * 60
                        '        Else
                        '            intTimeElapsed = 45 * 60
                        '        End If
                        '    Case 3
                        '        If m_objGameDetails.IsDefaultGameClock = True Then
                        '            intTimeElapsed = 15 * 60
                        '        Else
                        '            intTimeElapsed = 15 * 60
                        '        End If
                        '    Case 4
                        '        If m_objGameDetails.IsDefaultGameClock = True Then
                        '            intTimeElapsed = 15 * 60
                        '        Else
                        '            intTimeElapsed = 15 * 60
                        '        End If
                        'End Select
                        m_objclsTouches.AddTouches(frmTouches.CreateTouchDataObject(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intTimeElapsed, m_objGameDetails.CurrentPeriod, 0, 0, 15, m_objGameDetails.ReporterRole, -1, -1, -1, -1, m_objGameDetails.TouchesTeamID))
                        m_objTouches.DisplayTouches()
                        m_objGameDetails.TouchesData.Tables(0).Rows.Clear()
                    ElseIf m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                        Dim CurrentMainClockTime As Integer
                        If m_objGameDetails.IsDefaultGameClock Then
                            CurrentMainClockTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
                        Else
                            CurrentMainClockTime = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                        End If
                        If m_objGameDetails.IsDefaultGameClock Then
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                If CurrentMainClockTime < FIRSTHALF_SEC Then
                                    UdcRunningClock1.URCSetTime(45, 0, True)
                                End If
                            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                                If CurrentMainClockTime < FIRSTHALF_SEC Then
                                    UdcRunningClock1.URCSetTime(90, 0, True)
                                End If
                            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                                If CurrentMainClockTime < EXTRATIME_SEC Then
                                    UdcRunningClock1.URCSetTime(105, 0, True)
                                End If

                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                If CurrentMainClockTime < EXTRATIME_SEC Then
                                    UdcRunningClock1.URCSetTime(120, 0, True)
                                End If
                            End If
                        Else
                            If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 2 Then
                                If CurrentMainClockTime < FIRSTHALF_SEC Then
                                    UdcRunningClock1.URCSetTime(45, 0, True)
                                End If
                            ElseIf m_objGameDetails.CurrentPeriod = 3 Or m_objGameDetails.CurrentPeriod = 4 Then
                                If CurrentMainClockTime < EXTRATIME_SEC Then
                                    UdcRunningClock1.URCSetTime(15, 0, True)
                                End If

                            End If
                        End If
                    End If

                    If abanData.Tables.Count > 0 Then
                        If abanData.Tables(0).Rows.Count > 0 Then
                            EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                        Else
                            If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                                'EndGameToolStripMenuItem.Enabled = True
                                lblPeriod.Text = END_REGULATION_DESC
                                If m_objGameDetails.ModuleID <> TOUCHES_MODULE Then
                                    EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                                End If

                                'If m_objGameDetails.HomeScore = m_objGameDetails.AwayScore Then
                                If m_objGameDetails.CurrentPeriod = 2 Then
                                    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                    EnableMenuItem("EndGameToolStripMenuItem", True)
                                ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                    'If m_objGameDetails.ModuleID <> TOUCHES_MODULE Then
                                    '    'If (essageDialog.Show("Score Tied - Do you want to go for Shootout?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                    '    '    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                    '    '    essageDialog.Show("Select Start Period to enable Shootout Screen...", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    '    '    '' Need to Change this with Shootout screen
                                    '    'Else
                                    '    EnableMenuItem("EndGameToolStripMenuItem", True)
                                    '    'End If
                                    'Else ' For Touches, no need for Penalty Shotout
                                    EnableMenuItem("EndGameToolStripMenuItem", True)
                                    'End If
                                End If
                            End If
                            'Else
                            '    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                            '    EnableMenuItem("EndGameToolStripMenuItem", True)
                            'End If
                            'End If
                            If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                If m_objGameDetails.CurrentPeriod = 1 Then
                                    lblPeriod.Text = HALFTIME_DESC
                                Else
                                    lblPeriod.Text = EXTRATIME_BREAK_DESC
                                End If

                            End If
                            If m_objGameDetails.CurrentPeriod = 5 Then
                                EnableMenuItem("EndGameToolStripMenuItem", True)
                            End If
                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            'EndPeriodToolStripMenuItem.Enabled = False
                        End If
                    Else
                        If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                            'EndGameToolStripMenuItem.Enabled = True
                            lblPeriod.Text = END_REGULATION_DESC
                            If m_objGameDetails.ModuleID <> TOUCHES_MODULE Then
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                            End If

                            'If m_objGameDetails.HomeScore = m_objGameDetails.AwayScore Then
                            If m_objGameDetails.CurrentPeriod = 2 Then
                                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                EnableMenuItem("EndGameToolStripMenuItem", True)
                            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                                'If m_objGameDetails.ModuleID <> TOUCHES_MODULE Then
                                '    'If (essageDialog.Show("Score Tied - Do you want to go for Shootout?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                '    '    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                '    '    essageDialog.Show("Select Start Period to enable Shootout Screen...", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                '    '    '' Need to Change this with Shootout screen
                                '    'Else
                                '    EnableMenuItem("EndGameToolStripMenuItem", True)
                                '    'End If
                                'Else ' For Touches, no need for Penalty Shotout
                                EnableMenuItem("EndGameToolStripMenuItem", True)
                                'End If
                            End If
                        End If
                        'Else
                        '    EnableMenuItem("StartPeriodToolStripMenuItem", True)
                        '    EnableMenuItem("EndGameToolStripMenuItem", True)
                        'End If
                        'End If
                        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                            EnableMenuItem("StartPeriodToolStripMenuItem", True)
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                lblPeriod.Text = HALFTIME_DESC
                            Else
                                lblPeriod.Text = EXTRATIME_BREAK_DESC
                            End If

                        End If
                        If m_objGameDetails.CurrentPeriod = 5 Then
                            EnableMenuItem("EndGameToolStripMenuItem", True)
                        End If
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        'EndPeriodToolStripMenuItem.Enabled = False
                    End If
                    If blnEndGame Then 'entered END GAME - do the necessary work here (ONLY Mod 1 and Mod 2)
                        lblPeriod.Text = END_GAME_DESC

                        If m_objGameDetails.ModuleID = 1 Then
                            '15-Mar-2012 Arindam code added to handle ref/attdn check
                            Dim dsOfficialandMatch As New DataSet
                            dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                            If dsOfficialandMatch.Tables.Count = 0 Then
                                MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Else
                                If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                    If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                        MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                        MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    ElseIf IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                        MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    End If
                                Else
                                    If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                        If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                            MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        End If
                                    End If
                                    If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                        If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                            MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        End If
                                    End If
                                End If
                            End If

                            ''
                            If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                m_objGameDetails.IsEndGame = True
                                Dim objfrmTeamSetup As New frmTeamSetup("Home")
                                If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                    If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                        Dim objfrmgameSetup As New frmGameSetup
                                        objfrmgameSetup.ShowDialog()
                                        m_objGameDetails.IsEndGame = True
                                    End If
                                End If
                            Else
                                m_objGameDetails.IsEndGame = True
                                If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                    Dim objfrmgameSetup As New frmGameSetup
                                    objfrmgameSetup.ShowDialog()
                                End If
                            End If
                        ElseIf m_objGameDetails.ModuleID = 2 Then
                            If m_objGameDetails.CoverageLevel <> 1 Then
                                '15-Mar-2012 Arindam code added to handle ref/attdn check
                                Dim dsOfficialandMatch As New DataSet
                                dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                If dsOfficialandMatch.Tables.Count = 0 Then
                                    MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Else
                                    If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                        If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                            MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                            MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        ElseIf IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                            MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        End If
                                    ElseIf dsOfficialandMatch.Tables(0).Rows.Count = 0 And dsOfficialandMatch.Tables(1).Rows.Count = 0 Then
                                        MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Else
                                        If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                            If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                                MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            End If
                                        End If
                                        If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                            If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                                MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            End If
                                        End If
                                    End If
                                End If

                                ''
                            End If
                        End If 'ggg
                        EnableMenuItem("AbandonToolStripMenuItem", False)
                        EnableMenuItem("DelayedToolStripMenuItem", False)
                        EnableMenuItem("RevertSubsStripMenuItem", False)
                        EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("RatingsToolStripMenuItem", True)
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("AbandonToolStripMenuItem", False)
                        EnableMenuItem("RevertSubsStripMenuItem", False)
                        EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                        EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                        If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.DisableAllControls(False)
                                m_objModule1PBP.btnBookingAway.Enabled = False
                                m_objModule1PBP.btnBookingHome.Enabled = False
                                m_objModule1PBP.btnSubstituteAway.Enabled = False
                                m_objModule1PBP.btnSubstituteHome.Enabled = False
                            End If

                        End If
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.lklNewHomeBooking.Enabled = False
                            m_objModule1Main.lklNewHomeGoal.Enabled = False
                            m_objModule1Main.lklNewHomePenalty.Enabled = False
                            m_objModule1Main.lklNewHomeSub.Enabled = False
                            m_objModule1Main.lklNewVisitBooking.Enabled = False
                            m_objModule1Main.lklNewVisitGoal.Enabled = False
                            m_objModule1Main.lklNewVisitPenalty.Enabled = False
                            m_objModule1Main.lklNewVisitSub.Enabled = False
                        End If

                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                    End If

                Case "InputTeamStatsToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "InputTeamStats", 1, 0)
                    frmInputTeamStats.ShowDialog()

                    ' Dim frmInputTeamStats As Form

                Case "EndGameToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Period" & " -> " & "End Game", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3 Then
                        If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                            MessageDialog.Show(strmessage29, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Try
                        End If
                    End If

                    If (m_objGameDetails.ModuleID = 3 Or m_objGameDetails.ModuleID = 4) And m_objGameDetails.IsEditMode Then
                        MessageDialog.Show(strmessage6, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If

                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    'Added by Shravani

                    'Dim strPeriodString As String = ""

                    'If m_objGameDetails.CurrentPeriod = 1 Then
                    '    strPeriodString = "Halftime"
                    'ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                    '    strPeriodString = "Full Time"
                    'ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                    '    strPeriodString = "ET Break"
                    'ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                    '    strPeriodString = "2nd ET"
                    'End If

                    'If essageDialog.Show("Do you want to end the game ?", "End Game", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    '    If m_objGameDetails.ModuleID = 1 Then
                    '        If (essageDialog.Show(" Do you want to enter the player ratings ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    '            m_objGameDetails.IsEndGame = True
                    '            Dim objfrmTeamSetup As New frmTeamSetup("Home")
                    '            If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                    '                If (essageDialog.Show(" Do you want to enter the match rating ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    '                    Dim objfrmgameSetup As New frmGameSetup
                    '                    objfrmgameSetup.ShowDialog()
                    '                    m_objGameDetails.IsEndGame = True
                    '                End If
                    '            End If
                    '        Else
                    '            m_objGameDetails.IsEndGame = True
                    '            If (essageDialog.Show(" Do you want to enter the match rating ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    '                Dim objfrmgameSetup As New frmGameSetup
                    '                objfrmgameSetup.ShowDialog()
                    '            End If
                    '        End If

                    '    End If
                    'Else
                    '    m_objGameDetails.IsEndGame = False
                    '    Exit Sub
                    'End If

                    '
                    'm_objGameDetails.IsEndGame = True
                    SetClockControl(False)

                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                        'UdcRunningClock1.URCToggle()
                        'Dim objfrmModule1PBP As New frmModule1PBP
                        If MessageDialog.Show(strmessage42, "End Game", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            m_objGameDetails.IsEndGame = True
                        Else
                            m_objGameDetails.IsEndGame = False
                        End If
                        If Not m_objModule1Main Is Nothing Then
                            If m_objGameDetails.IsEndGame = True Then
                                m_objModule1Main.InsertPlayByPlayData(INTENDGAME, "Save")
                            End If
                        ElseIf Not m_objModule1PBP Is Nothing Then
                            If m_objGameDetails.IsEndGame = True Then
                                m_objModule1PBP.AddPBPGeneralEvents(INTENDGAME, "Save")
                            End If
                            m_objModule1PBP.btnTime.Enabled = False
                        ElseIf Not m_objModule2Score Is Nothing Then
                            If m_objGameDetails.IsEndGame = True Then
                                m_objModule2Score.AddPBPGeneralEvents(INTENDGAME, "Save")
                            End If
                        End If
                        'Dim dsEndGameCnt As New DataSet
                        'dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                        'If dsEndGameCnt.Tables.Count > 0 Then
                        '    If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                        '        Dim intEndGame As Integer = CInt(dsEndGameCnt.Tables(0).Rows(0).Item("endGameCount"))
                        '        If intEndGame <= 0 Then
                        If m_objGameDetails.IsEndGame = True Then
                            lblPeriod.Text = END_GAME_DESC
                            If m_objGameDetails.ModuleID = 1 Then
                                ''
                                '15-Mar-2012 Arindam code added to handle ref/attdn check
                                Dim dsOfficialandMatch As New DataSet
                                dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                If dsOfficialandMatch.Tables.Count = 0 Then
                                    MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                Else
                                    If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                        If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                            MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                            MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        ElseIf IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                            MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        End If
                                    Else
                                        If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                            If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                                MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            End If
                                        End If
                                        If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                            If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                                MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            End If
                                        End If
                                    End If
                                End If

                                ''
                                If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                    m_objGameDetails.IsEndGame = True
                                    Dim objfrmTeamSetup As New frmTeamSetup("Home")
                                    If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                        If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                            Dim objfrmgameSetup As New frmGameSetup
                                            objfrmgameSetup.ShowDialog()
                                            m_objGameDetails.IsEndGame = True
                                        End If
                                    End If
                                Else
                                    m_objGameDetails.IsEndGame = True
                                    If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                        Dim objfrmgameSetup As New frmGameSetup
                                        objfrmgameSetup.ShowDialog()
                                    End If
                                End If
                            ElseIf m_objGameDetails.ModuleID = 2 Then
                                If m_objGameDetails.CoverageLevel <> 1 Then
                                    '15-Mar-2012 Arindam code added to handle ref/attdn check
                                    Dim dsOfficialandMatch As New DataSet
                                    dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                                    If dsOfficialandMatch.Tables.Count = 0 Then
                                        MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Else
                                        If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                            If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                                MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                                MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            ElseIf IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                                MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                            End If
                                        ElseIf dsOfficialandMatch.Tables(0).Rows.Count = 0 And dsOfficialandMatch.Tables(1).Rows.Count = 0 Then
                                            MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                        Else
                                            If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                                If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                                    MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                End If
                                            End If
                                            If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                                If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                                    MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                                End If
                                            End If
                                        End If
                                    End If

                                    ''
                                End If
                            End If 'ggg
                            EnableMenuItem("AbandonToolStripMenuItem", False)
                            EnableMenuItem("DelayedToolStripMenuItem", False)
                            EnableMenuItem("RevertSubsStripMenuItem", False)
                            EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                            EnableMenuItem("RatingsToolStripMenuItem", True)
                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            EnableMenuItem("AbandonToolStripMenuItem", False)
                            EnableMenuItem("RevertSubsStripMenuItem", False)
                            EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                            EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                            If m_objGameDetails.ModuleID <> COMMENTARY_MODULE Then
                                If Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.DisableAllControls(False)
                                    m_objModule1PBP.btnBookingAway.Enabled = False
                                    m_objModule1PBP.btnBookingHome.Enabled = False
                                    m_objModule1PBP.btnSubstituteAway.Enabled = False
                                    m_objModule1PBP.btnSubstituteHome.Enabled = False
                                End If
                                If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                                    If Not m_objTouches Is Nothing Then
                                        m_objTouches.DisableControls(False)
                                    End If
                                End If
                            End If
                            'EnableMenuItem("StartPeriodToolStripMenuItem", True)

                            'End period DISABLED
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.lklNewHomeBooking.Enabled = False
                                m_objModule1Main.lklNewHomeGoal.Enabled = False
                                m_objModule1Main.lklNewHomePenalty.Enabled = False
                                m_objModule1Main.lklNewHomeSub.Enabled = False
                                m_objModule1Main.lklNewVisitBooking.Enabled = False
                                m_objModule1Main.lklNewVisitGoal.Enabled = False
                                m_objModule1Main.lklNewVisitPenalty.Enabled = False
                                m_objModule1Main.lklNewVisitSub.Enabled = False
                            End If

                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        End If
                        GetPenaltyShootoutScore()
                    Else
                        lblPeriod.Text = END_GAME_DESC
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("RatingsToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                        'Disable
                        Select Case m_objGameDetails.ModuleID
                            Case COMMENTARY_MODULE
                                m_objCommentary.BindCommentaryData()
                        End Select
                    End If
                    GetPenaltyShootoutScore()

                    'If m_objGameDetails.ModuleID = 1 Then
                    '    If (essageDialog.Show("Game Over - Do you want to enter the player ratings", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    '        Dim objfrmTeamSetup As New frmTeamSetup("Home")
                    '        objfrmTeamSetup.ShowDialog()
                    '        Me.Close()
                    '    Else
                    '        Me.Close()
                    '    End If
                    'End If

                    'If m_objGameDetails.ModuleID <> 2 Then
                    '    If (essageDialog.Show("Game Over - Are you sure to exit the game?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                    '        'Application.Exit()
                    '        Me.Close()
                    '    End If
                    'Else
                    '    'If Not m_objModule1Main Is Nothing Then
                    '    '    If m_objModule1Main.radGame00.Checked Then
                    '    '        m_objModule1Main.radGame00.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame01.Checked Then
                    '    '        m_objModule1Main.radGame01.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame02.Checked Then
                    '    '        m_objModule1Main.radGame02.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame03.Checked Then
                    '    '        m_objModule1Main.radGame03.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame04.Checked Then
                    '    '        m_objModule1Main.radGame04.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame05.Checked Then
                    '    '        m_objModule1Main.radGame05.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame06.Checked Then
                    '    '        m_objModule1Main.radGame06.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame07.Checked Then
                    '    '        m_objModule1Main.radGame07.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame08.Checked Then
                    '    '        m_objModule1Main.radGame08.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame09.Checked Then
                    '    '        m_objModule1Main.radGame09.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame10.Checked Then
                    '    '        m_objModule1Main.radGame10.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame11.Checked Then
                    '    '        m_objModule1Main.radGame11.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame12.Checked Then
                    '    '        m_objModule1Main.radGame12.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame13.Checked Then
                    '    '        m_objModule1Main.radGame13.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame14.Checked Then
                    '    '        m_objModule1Main.radGame14.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame15.Checked Then
                    '    '        m_objModule1Main.radGame15.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame16.Checked Then
                    '    '        m_objModule1Main.radGame16.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame17.Checked Then
                    '    '        m_objModule1Main.radGame17.BackColor = Color.Yellow
                    '    '    End If

                    '    '    If m_objModule1Main.radGame18.Checked Then
                    '    '        m_objModule1Main.radGame18.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame19.Checked Then
                    '    '        m_objModule1Main.radGame19.BackColor = Color.Yellow
                    '    '    End If
                    '    '    If m_objModule1Main.radGame20.Checked Then
                    '    '        m_objModule1Main.radGame20.BackColor = Color.Yellow
                    '    '    End If
                    '    'End If
                    'End If

                Case "InjuryTimeToolStripMenuItem"
                    'RM 8695
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Injury Time", 1, 0)

                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    'Check if the Injury time already exists for that Period
                    Dim dsPBP As New DataSet
                    Dim drs() As DataRow
                    dsPBP = m_objGeneral.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
                    If dsPBP.Tables(0).Rows.Count > 0 Then
                        drs = dsPBP.Tables(0).Select("EVENT_CODE_ID = 66 AND PERIOD = " & m_objGameDetails.CurrentPeriod)
                        If drs.Length > 0 Then
                            MessageDialog.Show(strmessage59, "Injury Time", MessageDialogButtons.OK, MessageDialogIcon.Error)
                            Exit Try
                        End If
                    End If

                    'If MessageDialog.Show(strmessage56, "Injury Time", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim objInjtime As New frmInjuryTime
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Injury Time", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    If objInjtime.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.INJURY_TIME, "Save")
                            m_objModule1PBP.DoCancel()
                        ElseIf Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.INJURY_TIME, "")
                        End If
                    End If
                    m_objGameDetails.IsInsertMode = False
                Case "DelayOverToolStripMenuItem"
                    'RM 8695
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Resume Delay", 1, 0)

                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If MessageDialog.Show(strmessage57, "Delay Over", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                        If Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAY_OVER, "Save")
                            If Not m_objModule1PBP Is Nothing And m_objGameDetails.CurrentPeriod > 0 Then
                                m_objModule1PBP.DisableAllControls(True)
                            End If
                        ElseIf Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAY_OVER, "")
                        End If
                        EnableMenuItem("DelayedToolStripMenuItem", True)
                        EnableMenuItem("DelayOverToolStripMenuItem", False)
                    End If

                Case "DelayedToolStripMenuItem"
                    'Arindam 2-May-12 Added code for Match Delay event
                    m_objGameDetails.IsDelayed = True
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Match Delayed", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    If m_objGameDetails.IsReplaceMode Or m_objGameDetails.IsInsertMode Then
                        MessageDialog.Show(strmessage7, "Replace", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If

                    Dim objComments As New frmComments
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If
                    If m_objGameDetails.CurrentPeriod = 0 Then
                        Dim dsPBP As New DataSet
                        dsPBP = m_objGeneral.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                        Dim dr() As DataRow = dsPBP.Tables(0).Select("EVENT_CODE_ID = 59 ")
                        If dr.Length > 0 Then
                            MessageDialog.Show(strmessage54, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    If MessageDialog.Show(strmessage58, "Delay", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                            If Not m_objModule1PBP Is Nothing Then

                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")

                            ElseIf Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                            ElseIf Not m_objModule2Score Is Nothing Then
                                m_objModule2Score.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                                m_objModule2Score.lvwPBP.Enabled = True
                            End If

                            GetPenaltyShootoutScore()
                            If UdcRunningClock1.URCIsRunning Then
                                UdcRunningClock1.URCToggle()
                            End If
                            If UdcRunningClock1.URCIsRunning Then
                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                            Else
                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            End If

                            'RM 8695
                            If m_objGameDetails.CoverageLevel = 4 Or m_objGameDetails.CoverageLevel = 5 Or m_objGameDetails.CoverageLevel = 6 Then
                                MessageDialog.Show(strmessage55, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                EnableMenuItem("DelayedToolStripMenuItem", False)
                                EnableMenuItem("DelayOverToolStripMenuItem", True)

                                If Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.DisableAllControls(False)
                                    m_objModule1PBP.btnBookingAway.Enabled = False
                                    m_objModule1PBP.btnBookingHome.Enabled = False
                                    m_objModule1PBP.btnSubstituteAway.Enabled = False
                                    m_objModule1PBP.btnSubstituteHome.Enabled = False
                                End If
                            End If
                        End If
                    End If
                    m_objGameDetails.IsDelayed = False

                Case "AbandonToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Abandon", 1, 0)
                    If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3 Then
                        If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                            MessageDialog.Show(strmessage45, "Abandoned", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Try
                        End If
                    End If

                    'm_objGameDetails.GameCode
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.ModuleID = 2 And (Not m_objModule1Main Is Nothing) Then
                        MessageDialog.Show(strmessage47, "Abandon", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Exit Sub
                    End If

                    If MessageDialog.Show(strmessage17, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Dim objabandon As New frmAbandon
                        If Not m_objModule1PBP Is Nothing Then
                            If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                                MessageDialog.Show(strmessage6, "Abandon", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        End If
                        If m_objGameDetails.ModuleID = 2 Then
                            If m_objGameDetails.CoverageLevel > 1 Then
                                If m_objModule1PBP.txtTimeMod2.Text.Replace(" :", "").Trim = "" Then
                                    MessageDialog.Show(strmessage46, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If
                            Else
                                If m_objModule2Score.txtTimeMod2.Text.Replace(" :", "").Trim = "" Then
                                    MessageDialog.Show(strmessage46, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If
                            End If

                        End If
                        If objabandon.ShowDialog = Windows.Forms.DialogResult.OK Then
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.ABANDONED, "Save")
                            ElseIf Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.ABANDONED, "")
                            ElseIf Not m_objModule2Score Is Nothing Then
                                m_objModule2Score.AddPBPGeneralEvents(clsGameDetails.ABANDONED, "")
                            End If
                            If Not m_objModule1Main Is Nothing Then
                                'm_objModule1Main.InsertPlayByPlayData(INTENDPERIOD, "Save")
                                'm_objModule1Main.InsertPlayByPlayData(INTCLOCKSTOP, "CLOCKSTOP")
                                m_objModule1Main.lklNewHomeBooking.Enabled = False
                                m_objModule1Main.lklNewHomeGoal.Enabled = False
                                m_objModule1Main.lklNewHomePenalty.Enabled = False
                                m_objModule1Main.lklNewHomeSub.Enabled = False
                                m_objModule1Main.lklNewVisitBooking.Enabled = False
                                m_objModule1Main.lklNewVisitGoal.Enabled = False
                                m_objModule1Main.lklNewVisitPenalty.Enabled = False
                                m_objModule1Main.lklNewVisitSub.Enabled = False
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                'm_objModule1PBP.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                                'm_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTOP, "CLOCKSTOP")
                                m_objModule1PBP.DisableAllControls(False)
                                m_objModule1PBP.btnSubstituteAway.Enabled = False
                                m_objModule1PBP.btnSubstituteHome.Enabled = False
                                m_objModule1PBP.btnBookingAway.Enabled = False
                                m_objModule1PBP.btnBookingHome.Enabled = False
                            End If

                            If UdcRunningClock1.URCIsRunning Then
                                UdcRunningClock1.URCToggle()
                            End If
                            If UdcRunningClock1.URCIsRunning Then
                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                            Else
                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            End If
                            SetClockControl(False)

                            EnableMenuItem("AbandonToolStripMenuItem", False)
                            EnableMenuItem("DelayedToolStripMenuItem", False)
                            'EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            'EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            'EnableMenuItem("EndGameToolStripMenuItem", True)
                        End If
                    End If
                Case "RevertSubsStripMenuItem"
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    Dim objDisplaySub As New frmDisplaySubs
                    objDisplaySub.ShowDialog()

                Case "ActiveGoalKeeperToolStripMenuItem"
                    If m_objGameDetails.CoverageLevel = 2 Then
                        MessageDialog.Show(strmessage31, "Active Goalie", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If

                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Team Setup", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If
                    Dim objactGK As New frmActiveGoalkeeper
                    If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                        'objactGK.ShowDialog()

                        If objactGK.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            If Not m_objModule1PBP Is Nothing Then
                                If m_objGameDetails.IsInsertMode Then
                                    m_objModule1PBP.AddPlayByPlayData(clsGameDetails.GOALIE_CHANGE, "")
                                    m_objModule1PBP.InsertPlaybyPlayDatafromMain()
                                    m_objModule1PBP.CancelfromMain()
                                    m_objGameDetails.IsInsertMode = False
                                ElseIf m_objGameDetails.IsReplaceMode Then
                                    m_objGameDetails.IsReplaceMode = False
                                ElseIf m_objGameDetails.IsEditMode Then

                                Else
                                    m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.GOALIE_CHANGE, "Save")
                                End If
                            ElseIf Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.GOALIE_CHANGE, "")
                            End If
                            'MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        End If
                        GetPenaltyShootoutScore()
                        'If Not m_objModule1PBP Is Nothing Then
                        '    m_objModule1PBP.DisplayPBP()
                        'End If
                    End If
                Case "OpenToolStripMenuItem"

                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Open", 1, 0)

                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage16, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    'Shravani
                    tmrRefresh.Enabled = False
                    tmrRefresh.Stop()
                    frmTouches.TmrTouches.Stop()

                    Dim objSelectGame As New frmSelectGame
                    If objSelectGame.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                        Exit Sub
                    End If

                    If m_objGameDetails.ModuleID = 3 Then
                        m_objModule1Main = Nothing
                        m_objModule1PBP = Nothing
                        m_objTouches = New frmTouches
                    ElseIf m_objGameDetails.ModuleID = 1 Then
                        If m_objGameDetails.FormState = clsGameDetails.m_enumFormState.MainForm Then
                            m_objModule1Main = New frmModule1Main
                            m_objTouches = Nothing
                            m_objModule1PBP = Nothing
                        ElseIf m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm Then
                            m_objModule1PBP = New frmModule1PBP
                            m_objTouches = Nothing
                            m_objModule1Main = Nothing
                        End If
                    End If

                    If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                        If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                            If Not m_objModule1Main Is Nothing Then
                                m_objGameDetails.PBP = AddPBPColumns()
                                SetChildForm()
                                SetMainFormMenu()
                                SetMainFormSize()
                                SetPeriod()
                                InitializeClock()
                                FillFormControls()
                                m_objModule1Main.FillSelectedGameDetails()
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                m_objGameDetails.PBP = AddPBPColumns()
                                SetPeriod()
                                SetChildForm()
                                SetMainFormMenu()
                                SetMainFormSize()

                                InitializeClock()
                                FillFormControls()
                                FillGames()
                                'getting the current players for the selected game
                                ResetClockAndScores()
                                If m_objGameDetails.CurrentPeriod <> 0 Then
                                    pnlEntryForm.Enabled = True
                                    If m_objModule1PBP.checkForHalftime() Then
                                        m_objModule1PBP.DisableAllControls(False)
                                    Else
                                        m_objModule1PBP.DisableAllControls(True)
                                    End If

                                End If
                                m_objModule1PBP.GetCurrentPlayersAfterRestart()
                                m_objModule1PBP.DisplayPBP()
                                m_objModule1PBP.SetTeamButtonColor()
                                m_objModule1PBP.getDirectionFromHomeStart(m_objGameDetails.CurrentPeriod)
                                'If m_objGameDetails.IsRestartGame = True Then
                                '    pnlEntryForm.Enabled = True
                                '    btnClock.Enabled = True
                                '    m_objGameDetails.IsRestartGame = False
                                '    m_objGameDetails.RestartData.Tables.Clear()
                                '    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                                '        SetHeader2()
                                '    End If
                                'End If
                            ElseIf Not m_objModule2Score Is Nothing Then
                                FillGames()
                                FillFormControls()
                                m_objModule2Score.LoadTeam(m_objGameDetails.GameCode)
                                m_objModule2Score.FillListView()
                                ResetClockAndScores()
                            ElseIf Not m_objTouches Is Nothing Then
                                Dim Ds As New DataSet
                                m_objclsTeamSetup.RosterInfo = Ds
                                'SetChildForm()
                                SetMainFormMenu()
                                'SetMainFormSize()
                                InitializeClock()
                                FillFormControls()

                                If m_objGameDetails.IsRestartGame = True Then
                                    m_objTouches.RestartExistingGame()
                                    pnlEntryForm.Enabled = True
                                    btnClock.Enabled = True
                                    m_objGameDetails.IsRestartGame = False
                                    m_objGameDetails.RestartData.Tables.Clear()
                                    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                                        SetHeader2()
                                    End If
                                    If m_objGameDetails.CurrentPeriod = 0 Then
                                        m_objTouches.pnlTouchTypes.Enabled = False
                                        m_objTouches.UdcSoccerField1.Enabled = False
                                        m_objTouches.btnSave.Enabled = False
                                        m_objTouches.btnClear.Enabled = False
                                    Else
                                        m_objTouches.pnlTouchTypes.Enabled = True
                                        m_objTouches.UdcSoccerField1.Enabled = True
                                        m_objTouches.btnSave.Enabled = True
                                        m_objTouches.btnClear.Enabled = True
                                    End If
                                Else
                                    m_objTouches.GetTouches(3)
                                End If
                                SetPeriod()
                                SetChildForm()
                                SetMainFormSize()
                                m_objTouches.SetTeamColor(True, m_objGameDetails.HomeTeamColor)
                                m_objTouches.SetTeamColor(False, m_objGameDetails.VisitorTeamColor)

                            End If
                            m_objGameDetails.IsRestartGame = False
                        Else
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.FillSelectedDemoGameDetails(m_objGameDetails.GameCode)
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                FillGames()
                                FillFormControls()
                                ResetClockAndScores()
                                m_objModule1PBP.DisplayPBP()
                            End If
                        End If
                    Else

                        If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                            m_objCommentary = Nothing
                            m_objTouches = Nothing
                            DisplayScreensForOPS()
                            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                                If Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.FillSelectedGameDetails()
                                ElseIf Not m_objModule1PBP Is Nothing Then
                                    FillGames()
                                    FillFormControls()
                                    ResetClockAndScores()
                                    m_objModule1PBP.DisplayPBP()
                                End If

                            Else
                                If Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.FillSelectedDemoGameDetails(m_objGameDetails.GameCode)
                                ElseIf Not m_objModule1PBP Is Nothing Then
                                    FillGames()
                                    FillFormControls()
                                    ResetClockAndScores()
                                    m_objModule1PBP.DisplayPBP()
                                End If
                            End If

                        ElseIf m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                            m_objModule1Main = Nothing
                            m_objModule1PBP = Nothing
                            m_objCommentary = Nothing
                            btnGotoPBP.Visible = False
                            btnGotoMain.Visible = False
                            DisplayScreensForOPS()
                        ElseIf m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                            'SetMainFormMenu()
                            'SetChildForm()
                            'SetMainFormSize()
                            m_objModule1Main = Nothing
                            m_objModule1PBP = Nothing
                            m_objTouches = Nothing
                            btnGotoPBP.Visible = False
                            btnGotoMain.Visible = False
                            DisplayScreensForOPS()
                            EnableMenuItem("CommentToolStripMenuItem", True)

                            EnableMenuItem("SetTeamColorToolStripMenuItem", True)
                            EnableMenuItem("OpenToolStripMenuItem", True)
                        End If
                    End If
                Case "ExitToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Exit", 1, 0)
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Close", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            'e.Cancel = True
                            Exit Sub
                        End If
                    End If
                    Select Case m_objGameDetails.ModuleID
                        Case 3 'Touch Module.
                            Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                                'TOSOCRS-62 Touch Reporter 2/3-End Game Warning
                                Case "1"
                                    If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                                    Else
                                        If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                            Exit Sub
                                        End If
                                    End If
                            End Select
                            'TOSOCRS-29 Exit Warning for Unfinalized Touches
                            If m_objGameDetails.ModuleID = 3 Then
                                Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                                    Case "2", "3"
                                        If isMenuItemEnabled("TouchesFinalized") = True Then
                                            If MessageDialog.Show(strmessage62, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                                Exit Sub
                                            End If
                                        End If
                                End Select
                            End If
                        Case 4 'Çommentary Module
                            If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                            Else
                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                    Exit Sub
                                End If
                            End If
                        Case Else 'PBP Module
                            Dim endGameCount As DataSet
                            endGameCount = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If CInt(endGameCount.Tables(0).Rows(0).Item("endGameCount")) = 0 And m_objGameDetails.CurrentPeriod > 0 Then
                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                    Exit Sub
                                End If
                            End If
                    End Select
                    m_formClosed = True
                    Me.Close()
                Case "TeamSetupToolStripMenuItem"
                    ' AUDIT(TRIAL)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Team Setup", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                        If IsPBPEntryAllowed() = False Then
                            Exit Try
                        End If
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Team Setup", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If
                    ''CALLING TEAM SETUP SCREEN
                    Dim objTeamSetup As New frmTeamSetup("Home")
                    objTeamSetup.ShowDialog()
                    'ONCE THE PLAYERS ARE FILLED IN STARTING LINEUPS, THE MAIN FORM PLAYERS ARE REFRESHED ..
                    If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                        If Not m_objModule1Main Is Nothing Then
                            SortPlayers()
                            m_objModule1Main.LabelsTagged()
                        End If
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.DisplayPBP()
                    End If

                Case "GameSetupToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Game Setup", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    Dim objGameSetup As New frmGameSetup
                    If objGameSetup.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If m_objGameDetails.Reason <> "" Then
                            m_objGameDetails.CommentData = m_objGameDetails.Reason
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft   'set home team on left for field user control
                            ElseIf Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                            End If
                        End If
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertOfficialsandMatchInfo()
                        End If
                        If Not m_objModule1PBP Is Nothing Then
                            'Arindam 23-May-2012
                            If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft   'set home team on left for field user control
                                m_objModule1PBP.UdcSoccerField1.USFAwayTeamName = m_objGameDetails.AwayTeam
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamName = m_objGameDetails.HomeTeam
                            End If
                        End If
                    End If
                    'objGameSetup.ShowDialog()

                Case "AddOfficialToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Official", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    Dim objAddOfficial As New frmAddOfficial
                    objAddOfficial.Show()

                Case "AddPlayerToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Player", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Close", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            'e.Cancel = True
                            Exit Sub
                        End If
                    End If

                    Dim objAddPlayer As New frmAddPlayer("Home")
                    objAddPlayer.ShowDialog()
                    If Not m_objModule1Main Is Nothing Then
                        m_objModule1Main.GetCurrentPlayersAfterRestart()
                        SortPlayers()
                    End If
                    If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE And m_objGameDetails.CoverageLevel = 2 Then
                        m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
                    End If

                Case "AddManagerToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Manager", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    Dim objAddManager As New frmAddManager("Home")
                    objAddManager.ShowDialog()

                Case "DeleteRecordsToolStripMenuItem"

                    m_objclsLogin.DeleteCurrentGameData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                Case "SwitchSidesToolStripMenuItem"

                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Switch Slides", 1, 0)

                    Select Case m_objGameDetails.ModuleID
                        Case COMMENTARY_MODULE
                            m_objCommentary.SwitchSides()
                        Case TOUCHES_MODULE
                            If m_objGameDetails.SerialNo = 1 Then 'TOSOCRS-38
                                m_objTouches.SwitchSidesPrimary()
                            Else
                                m_objTouches.SwitchSides()
                            End If
                        Case PBP_MODULE, MULTIGAME_PBP_MODULE
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.SwitchSides()
                            ElseIf Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.SwitchSides()
                            End If

                    End Select
                    SetHeader2()
                Case "HomeTeamColorToolStripMenuItem", "VisitorTeamColorToolStripMenuItem"
                    If Not cdlgTeam.ShowDialog = Windows.Forms.DialogResult.OK Then
                        Exit Select
                    End If
                    If Not m_objModule1Main Is Nothing Then
                        EnableMenuItem("HomeTeamColorToolStripMenuItem", False)
                        EnableMenuItem("VisitorTeamColorToolStripMenuItem", False)
                    End If
                    SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                    Select Case m_objGameDetails.ModuleID
                        Case COMMENTARY_MODULE
                            m_objCommentary.SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                        Case TOUCHES_MODULE
                            m_objTouches.SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                        Case PBP_MODULE
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                            End If
                        Case MULTIGAME_PBP_MODULE
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                            End If
                    End Select
                Case "AboutToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Help" & " -> " & "About", 1, 0)

                    Dim objAbout As New frmAbout
                    objAbout.ShowDialog()

                    'TOSOCRS-33-SOC - Manual restart of datasync component in reporter software
                Case "RestartDataSyncComponentMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Help" & " -> " & "Restart DataSync Component Clicked", 1, 0)
                    LoadSoccerDataSync()
                Case "PenaltyShootoutToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Manager", 1, 0)
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    'If m_objGameDetails.IsEndGame Then
                    '    essageDialog.Show("Game over - you can not go for a penalty shootout!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '    Exit Try
                    'End If

                    If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then

                        If m_objGameDetails.CoverageLevel = 2 Or m_objGameDetails.CoverageLevel = 3 Then
                            Dim dsPBP As New DataSet
                            dsPBP = m_objGeneral.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                            Dim dr() As DataRow = dsPBP.Tables(0).Select("EVENT_CODE_ID = 30 ")
                            If dr.Length > 0 Then
                                If dr(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
                                    m_objGameDetails.IsScoreWindow = True
                                    m_objGameDetails.IsDetailedWindow = False
                                Else
                                    m_objGameDetails.IsDetailedWindow = True
                                    m_objGameDetails.IsScoreWindow = False
                                End If
                            End If
                            If (m_objGameDetails.IsScoreWindow = False And m_objGameDetails.IsDetailedWindow = False) Or (dr.Length <= 0) Then

                                Dim Answer As DialogResult
                                Answer = MessageDialog.Show(strmessage30, Me.Text, MessageDialogButtons.YesNoCancel, MessageDialogIcon.Question)
                                If Answer = Windows.Forms.DialogResult.Yes Then
                                    'Detailed window
                                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    'Dim objPenaltyShoot As frmPenaltyShootout = Nothing
                                    objPenaltyShoot = New frmPenaltyShootout()
                                    objPenaltyShoot.ShowDialog()
                                ElseIf Answer = Windows.Forms.DialogResult.No Then
                                    'Score window
                                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    Dim objPenaltyShootoutScore As frmPenaltyShootoutScore = Nothing
                                    objPenaltyShootoutScore = New frmPenaltyShootoutScore()
                                    objPenaltyShootoutScore.ShowDialog()
                                End If
                            ElseIf m_objGameDetails.IsDetailedWindow Then
                                EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                'Dim objPenaltyShoot As frmPenaltyShootout = Nothing
                                objPenaltyShoot = New frmPenaltyShootout()
                                objPenaltyShoot.ShowDialog()
                            ElseIf m_objGameDetails.IsScoreWindow Then
                                'Score window
                                EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                Dim objPenaltyShootoutScore As frmPenaltyShootoutScore = Nothing
                                objPenaltyShootoutScore = New frmPenaltyShootoutScore()
                                objPenaltyShootoutScore.ShowDialog()
                            End If
                        ElseIf m_objGameDetails.CoverageLevel = 1 Then
                            EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            Dim objPenaltyShootoutScore As frmPenaltyShootoutScore = Nothing
                            objPenaltyShootoutScore = New frmPenaltyShootoutScore()
                            objPenaltyShootoutScore.ShowDialog()
                        Else
                            If MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                'LoadPenalShootout()
                                EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                'Dim objPenaltyShoot As frmPenaltyShootout = Nothing
                                objPenaltyShoot = New frmPenaltyShootout()
                                objPenaltyShoot.ShowDialog()
                            End If
                        End If
                        If Not m_objModule1PBP Is Nothing Then
                            m_objModule1PBP.DisplayPBP()
                        End If
                        If Not m_objModule2Score Is Nothing Then
                            m_objModule2Score.FillListView()
                        End If
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.FetchMainScreenPBPEvents()
                        End If
                        If m_objGameDetails.IsEndGame Then
                            'EnableMenuItem("EndGameToolStripMenuItem", True)
                            'm_objGameDetails.IsEndGame = True
                            SetClockControl(False)
                            lblPeriod.Text = END_GAME_DESC
                            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                                'If Not m_objModule1Main Is Nothing Then
                                '    m_objModule1Main.InsertPlayByPlayData(INTENDGAME, "Save")
                                'ElseIf Not m_objModule1PBP Is Nothing Then
                                '    m_objModule1PBP.AddPBPGeneralEvents(INTENDGAME, "Save")
                                'End If

                                Dim dsEndGameCnt As New DataSet
                                dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                                If dsEndGameCnt.Tables.Count > 0 Then
                                    If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                                        Dim intEndGame As Integer = CInt(dsEndGameCnt.Tables(0).Rows(0).Item("endGameCount"))
                                        If intEndGame <= 0 Then
                                            If Not m_objModule1Main Is Nothing Then
                                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.ENDGAME, "Save")
                                            ElseIf Not m_objModule1PBP Is Nothing Then
                                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.ENDGAME, "Save")
                                                m_objModule1PBP.btnTime.Enabled = False
                                            End If
                                            If m_objGameDetails.ModuleID = 1 Then
                                                If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                                    m_objGameDetails.IsEndGame = True
                                                    Dim objfrmTeamSetup As New frmTeamSetup("Home")
                                                    If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                                        If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                                            Dim objfrmgameSetup As New frmGameSetup
                                                            objfrmgameSetup.ShowDialog()
                                                            m_objGameDetails.IsEndGame = True
                                                        End If
                                                    End If
                                                Else
                                                    m_objGameDetails.IsEndGame = True
                                                    If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                                        Dim objfrmgameSetup As New frmGameSetup
                                                        objfrmgameSetup.ShowDialog()
                                                    End If
                                                End If
                                            End If
                                            EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                                        End If
                                    End If
                                End If

                                EnableMenuItem("AbandonToolStripMenuItem", False)
                                EnableMenuItem("DelayedToolStripMenuItem", False)
                                EnableMenuItem("RevertSubsStripMenuItem", False)
                                EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            End If
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.DisableAllControls(False)
                                m_objModule1PBP.btnBookingAway.Enabled = False
                                m_objModule1PBP.btnBookingHome.Enabled = False
                                m_objModule1PBP.btnSubstituteAway.Enabled = False
                                m_objModule1PBP.btnSubstituteHome.Enabled = False
                            End If

                            'EnableMenuItem("StartPeriodToolStripMenuItem", True)

                            'End period DISABLED
                            If Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.lklNewHomeBooking.Enabled = False
                                m_objModule1Main.lklNewHomeGoal.Enabled = False
                                m_objModule1Main.lklNewHomePenalty.Enabled = False
                                m_objModule1Main.lklNewHomeSub.Enabled = False
                                m_objModule1Main.lklNewVisitBooking.Enabled = False
                                m_objModule1Main.lklNewVisitGoal.Enabled = False
                                m_objModule1Main.lklNewVisitPenalty.Enabled = False
                                m_objModule1Main.lklNewVisitSub.Enabled = False
                            End If

                            EnableMenuItem("EndPeriodToolStripMenuItem", False)
                            EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            EnableMenuItem("EndGameToolStripMenuItem", False)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                            EnableMenuItem("RatingsToolStripMenuItem", True)

                        Else
                            EnableMenuItem("EndGameToolStripMenuItem", True)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                            EnableMenuItem("RatingsToolStripMenuItem", False)
                        End If

                        GetPenaltyShootoutScore()
                    End If

                Case "BoxScoreToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "BoxScore", 1, 0)
                    LoadBoxScore()
                    ' Dim objBoxScore As New frmBoxScore
                    'objBoxScore.ShowDialog()

                Case "SortByUniformToolStripMenuItem"
                    m_objclsLoginDetails.strSortOrder = "Uniform"
                    Select Case m_objGameDetails.ModuleID
                        Case COMMENTARY_MODULE
                            m_objCommentary.SortTeamPlayers()
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Uniform", 1, 0)
                        Case TOUCHES_MODULE
                            m_objTouches.SortTeamPlayers()
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Uniform", 1, 0)
                        Case Else
                            SortPlayers()
                    End Select

                Case "SortByLastNameToolStripMenuItem"
                    m_objclsLoginDetails.strSortOrder = "Last Name"
                    Select Case m_objGameDetails.ModuleID
                        Case COMMENTARY_MODULE
                            m_objCommentary.SortTeamPlayers()
                            'AUDIT TRIAL
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Last Name", 1, 0)
                        Case TOUCHES_MODULE
                            m_objTouches.SortTeamPlayers()
                            'AUDIT TRIAL
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Last Name", 1, 0)
                        Case Else
                            SortPlayers()
                    End Select
                Case "SortByPositionToolStripMenuItem"
                    m_objclsLoginDetails.strSortOrder = "Position"
                    Select Case m_objGameDetails.ModuleID
                        Case COMMENTARY_MODULE
                            m_objCommentary.SortTeamPlayers()
                            'AUDIT TRIAL
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Position", 1, 0)

                        Case TOUCHES_MODULE
                            m_objTouches.SortTeamPlayers()
                            'AUDIT TRIAL
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Position", 1, 0)
                        Case Else
                            SortPlayers()
                    End Select
                Case "KeyMomentToolStripMenuItem"

                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    m_objKeyMoments = New frmKeyMoments()
                    m_objKeyMoments.ShowDialog()
                    'frmKeyMoments.ShowDialog()
                    'Case "KeyMomentToolStripMenuItem"
                    '    'Dim objKeyMoments As New frmKeyMoments
                    '    'objKeyMoments.ShowDialog()
                    '    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    '    If IsPBPEntryAllowed() = False Then
                    '        Exit Try
                    '    End If

                    '    frmKeyMoments.ShowDialog()
                    m_objKeyMoments = Nothing
                Case "TeamPossessionMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "TeamPossessionMenuItem", 1, 0)

                    Dim objTeamPossessionMenuItem As New frmTeamPossession
                    objTeamPossessionMenuItem.ShowDialog()

                Case "CommentToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Comments", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    If m_objGameDetails.IsReplaceMode Then
                        MessageDialog.Show(strmessage7, "Replace", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        'If Not m_objModule1PBP Is Nothing Then

                        'End If
                        Exit Sub
                    End If
                    Dim objComments As New frmComments
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Comment", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If Not m_objModule1PBP Is Nothing Then
                            If m_objGameDetails.IsInsertMode Then
                                m_objModule1PBP.AddPlayByPlayData(clsGameDetails.COMMENTS, "")
                                m_objModule1PBP.InsertPlaybyPlayDatafromMain()
                                m_objModule1PBP.CancelfromMain()
                                m_objGameDetails.IsInsertMode = False
                            ElseIf m_objGameDetails.IsReplaceMode Then
                                'm_objModule1PBP.a(m_EventID)
                                'InsertPlayByPlayData_Edit(m_EventID)

                                'ClearDataPointsAfterSave()
                                ''End If
                                'DoCancel()
                                'btnSave.Enabled = False
                                m_objGameDetails.IsReplaceMode = False
                            Else
                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.COMMENTS, "Save")
                            End If
                        ElseIf Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.COMMENTS, "")
                        ElseIf Not m_objModule2Score Is Nothing Then
                            m_objModule2Score.AddPBPGeneralEvents(clsGameDetails.COMMENTS, "Save")
                        End If
                        'MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                    GetPenaltyShootoutScore()

                Case "PreferencesToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Preferences", 1, 0)
                    Dim PrevDefaultClock As Boolean = False
                    Dim ClockRunning As Boolean = False
                    If m_objGameDetails.IsEditMode Or m_objGameDetails.IsReplaceMode Or m_objGameDetails.IsInsertMode Then
                        MessageDialog.Show(strmessage3, "Time Pref", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    Else
                        If Not m_objModule1PBP Is Nothing Then
                            If m_objModule1PBP.dblstEventDetails01.Visible = True Then
                                MessageDialog.Show(strmessage3, "Time Pref", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        End If
                        If Not m_objTouches Is Nothing Then
                            If m_objTouches.txtTime.Text.Replace(":", "").Trim <> "" Then
                                MessageDialog.Show(strmessage3, "Time Pref", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        End If
                        If m_objGameDetails.IsDefaultGameClock Then
                            PrevDefaultClock = True
                        End If
                        If UdcRunningClock1.URCIsRunning Then
                            ClockRunning = True
                        End If
                        Dim objPreferences As New frmPreferences
                        objPreferences.ShowDialog()
                        If m_objGameDetails.CurrentPeriod > 1 Then
                            If (m_objGameDetails.IsDefaultGameClock And PrevDefaultClock = False) Or (m_objGameDetails.IsDefaultGameClock = False And PrevDefaultClock = True) Then
                                ConvertAndSetClock()
                                'If ClockRunning Then
                                '    UdcRunningClock1.URCToggle()
                                'End If
                            End If
                            'Reset all the timings now

                        End If
                    End If

                Case "PurgeAuditTrailToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Purge Audit Trial", 1, 0)

                    Dim objPurge As New frmPurge
                    objPurge.ShowDialog()

                Case "ManagerExpulsionToolStripMenuItem"
                    Dim objMangaerExpl As New frmManagerExpulsion
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.IsReplaceMode Then
                        MessageDialog.Show(strmessage2, "Replace", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        'If Not m_objModule1PBP Is Nothing Then

                        'End If
                        Exit Sub
                    End If
                    If objMangaerExpl.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If Not m_objModule1PBP Is Nothing Then
                            'If m_objGameDetails.IsInsertMode Then
                            '    m_objModule1PBP.AddPlayByPlayData(clsGameDetails.COMMENTS, "")
                            '    m_objModule1PBP.InsertPlaybyPlayDatafromMain()
                            '    m_objModule1PBP.CancelfromMain()
                            'Else
                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.MANAGEREXPULSION, "Save")
                            'End If
                        ElseIf Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.MANAGEREXPULSION, "")
                        End If
                        'essageDialog.Show("Comments inserted succesfully!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.None)
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.DoCancel()
                    End If
                    m_objGameDetails.IsInsertMode = False
                    m_objGameDetails.IsEditMode = False
                Case "TeamStatsToolStripMenuItem"
                    LoadTeamStats()
                Case "PlayerTouchesToolStripMenuItem"
                    LoadPlayerTouches()

                Case "PlayerStatsToolStripMenuItem"
                    LoadPlayerStats()
                Case "ReportFormationToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "Formation Report", 1, 0)
                    Loadfrmformation()
                    'Dim objfrm As New FrmFormationreport
                    'objfrm.ShowDialog()
                    'Dim objfrmTeamSetup As New frmTeamSetup("Home")
                    'Dim objfrmFormation As New FrmFormationreport()
                    'objfrmFormation.Show()
                Case "ReportPBPToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "Formation Report", 1, 0)
                    Loadpbpreport()
                    'Dim objfrmplaybyplay As New frmPBPReport()
                    'objfrmplaybyplay.Show()
                Case "TouchHotkeysToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Help" & " -> " & "Touch Hotkeys", 1, 0)

                    Dim objTouchHotkeys As New frmTouchHotkeys
                    objTouchHotkeys.ShowDialog()

                Case "OthersToolStripMenuItem"
                    'If ((m_objGameDetails.CurrentPeriod > 0) And (m_objGameDetails.CoverageLevel = 2) Or (m_objGameDetails.CoverageLevel = 3)) Then
                    '    ShowMenuItem("InputTeamStatsToolStripMenuItem", True)
                    'Else
                    '    ShowMenuItem("InputTeamStatsToolStripMenuItem", False)
                    'End If
                    If m_objGameDetails.CurrentPeriod > 0 Then
                        EnableMenuItem("InputTeamStatsToolStripMenuItem", True)
                    Else
                        EnableMenuItem("InputTeamStatsToolStripMenuItem", False)
                    End If
                    If m_objGameDetails.CurrentPeriod > 0 Then
                        EnableMenuItem("TeamPossessionMenuItem", True)
                    Else
                        EnableMenuItem("TeamPossessionMenuItem", False)
                    End If
                Case "SportVUToolStripMenuItem"
                    Dim m_objfrmSportVU As New frmSportVU
                    m_objfrmSportVU.Show()

                Case "HomeStartToolStripMenuItem"
                    Dim m_objHomestart As New frmHomeStart
                    'm_objHomestart.ShowDialog()

                    If m_objHomestart.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If m_objGameDetails.IsHalfTimeSwap Then
                            m_objTouches.SwitchSides()
                            SetHeader2()
                        End If
                        'Arindam 23-May-2012
                        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                            m_objGameDetails.isHomeDirectionLeft = m_objGameDetails.IsHomeTeamOnLeft
                            m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft
                            m_objTouches.UdcSoccerField1.USFHomeTeamName = m_objGameDetails.HomeTeam
                            m_objTouches.UdcSoccerField1.USFAwayTeamName = m_objGameDetails.AwayTeam
                        End If

                    End If
                Case "RatingsToolStripMenuItem"
                    If m_objGameDetails.ModuleID = 1 Then
                        If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                            m_objGameDetails.IsEndGame = True
                            Dim objfrmTeamSetup As New frmTeamSetup("Home")
                            If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                    Dim objfrmgameSetup As New frmGameSetup
                                    objfrmgameSetup.ShowDialog()
                                    m_objGameDetails.IsEndGame = True
                                End If
                            End If
                        Else
                            m_objGameDetails.IsEndGame = True
                            If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                Dim objfrmgameSetup As New frmGameSetup
                                objfrmgameSetup.ShowDialog()
                            End If
                        End If
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        Finally
            If (MenuItemName = "OpenToolStripMenuItem" And tmrRefresh.Enabled = False) Then
                tmrRefresh.Enabled = True
                tmrRefresh.Start()
                frmTouches.TmrTouches.Start()
            End If
        End Try
    End Sub

    Private Function SaveInMemoryActionInPrimary() As Boolean
        Try
            If m_objGameDetails.ReporterRoleSerial = 1 Then
                If (m_objActionDetails.EventId = 67) Or (m_objActionDetails.EventId = 68) Or (m_objActionDetails.TeamId IsNot Nothing) Then
                    If m_objGameDetails.ReporterRoleSerial = 1 Then
                        If Not m_objActions.SavePreviousAction() Then
                            Return False
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub ProzoneMenu_Click(ByVal MenuItemName As String)
        Try
            Dim activateGame As Boolean = False

            Select Case MenuItemName
                Case "StartPeriodToolStripMenuItem"
                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage28, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objActionDetails.EventId = 63 Then
                        MessageDialog.Show("Please select Booking/Substitution or cancel the entry and continue.", "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Period " & " -> " & "Start Period", 1, 0)
                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    If IsPZPeriodMismatch() = True Then
                        m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
                    End If
                    Dim activeGameForm = New frmActivateGames
                    activeGameForm.ShowDialog()
                    Dim isActive = activeGameForm.returnString
                    If isActive Then
                        m_objModule1Main.InsertOfficialsandMatchInfo()
                        If btnGotoPBP.Visible = True Then
                            btnGotoPBP.PerformClick()
                        End If
                        m_objActions.EnableDisableControls(True)
                        m_objGameDetails.ActivateGame = True
                        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 3 Then
                            If m_objGameDetails.IsHalfTimeSwap Then
                                m_objActions.SwitchSide()
                            End If
                        End If
                    End If
                    Exit Sub
                Case "EndPeriodToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Period" & " -> " & "End Period", 1, 0)
                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "End Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage29, "End Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    Dim strPeriodString As String = ""
                    If m_objGameDetails.CurrentPeriod = 1 Then
                        strPeriodString = "Halftime"
                    ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                        strPeriodString = "Full Time"
                    ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                        strPeriodString = "ET Break"
                    ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                        strPeriodString = "2nd ET"
                    End If
                    If MessageDialog.Show(strmessage19 + "  [ " & strPeriodString & "  (" & m_objGameDetails.HomeTeamAbbrev & ":  " & lblScoreHome.Text & ", " & m_objGameDetails.AwayTeamAbbrev & ":  " & lblScoreAway.Text & ") ] ", "End Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If

                    lblPeriod.ForeColor = Color.Red
                    If UdcRunningClock1.URCIsRunning Then
                        UdcRunningClock1.URCToggle()
                    End If
                    If UdcRunningClock1.URCIsRunning Then
                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    Else
                        btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                    End If
                    SetClockControl(False)

                    Dim blnEndGame As Boolean = False
                    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                        If MessageDialog.Show(strmessage48, "End Period", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            m_objGameDetails.IsEndGame = True
                            blnEndGame = True
                        End If
                    End If

                    'insert end period/game over events.
                    m_objActionDetails.EventId = 13
                    m_objActionDetails.TeamId = Me.m_objGameDetails.HomeTeamID
                    m_objActions.InsertPlayByPlayData()
                    m_objActionDetails.EventId = 55
                    m_objActions.InsertPlayByPlayData()
                    If m_objGameDetails.IsEndGame = True Then
                        m_objActionDetails.EventId = 10
                        m_objActions.InsertPlayByPlayData()
                    End If
                    If m_objGameDetails.IsEndGame = True Then
                        m_objActions.EnableDisableControls(False, False)
                    Else
                        m_objActions.EnableDisableControls(True, False)
                    End If
                    m_objActions.UnHighlightActions()
                    m_objActions.ResetProperties()

                    'code for Abandoned - commented need to check when being worked.
                    'Dim abanData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, clsGameDetails.ABANDONED, m_objGameDetails.ModuleID)
                    'If abanData.Tables.Count > 0 Then
                    '    If abanData.Tables(0).Rows.Count > 0 Then
                    '        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                    '        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                    '        EnableMenuItem("EndGameToolStripMenuItem", False)
                    '    Else
                    '        If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                    '            lblPeriod.Text = END_REGULATION_DESC
                    '            If m_objGameDetails.ModuleID <> TOUCHES_MODULE Then
                    '                EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                    '            End If
                    '            If m_objGameDetails.CurrentPeriod = 2 Then
                    '                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                    '                EnableMenuItem("EndGameToolStripMenuItem", True)
                    '            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                    '                EnableMenuItem("EndGameToolStripMenuItem", True)
                    '            End If
                    '        End If
                    '        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                    '            EnableMenuItem("StartPeriodToolStripMenuItem", True)
                    '            If m_objGameDetails.CurrentPeriod = 1 Then
                    '                lblPeriod.Text = HALFTIME_DESC
                    '            Else
                    '                lblPeriod.Text = EXTRATIME_BREAK_DESC
                    '            End If

                    '        End If
                    '        If m_objGameDetails.CurrentPeriod = 5 Then
                    '            EnableMenuItem("EndGameToolStripMenuItem", True)
                    '        End If
                    '        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                    '    End If
                    'Else
                    '    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                    '        lblPeriod.Text = END_REGULATION_DESC
                    '        If m_objGameDetails.ModuleID <> TOUCHES_MODULE Then
                    '            EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                    '        End If
                    '        If m_objGameDetails.CurrentPeriod = 2 Then
                    '            EnableMenuItem("StartPeriodToolStripMenuItem", True)
                    '            EnableMenuItem("EndGameToolStripMenuItem", True)
                    '        End If
                    '    End If
                    '    If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                    '        EnableMenuItem("StartPeriodToolStripMenuItem", True)
                    '        If m_objGameDetails.CurrentPeriod = 1 Then
                    '            lblPeriod.Text = HALFTIME_DESC
                    '        Else
                    '            lblPeriod.Text = EXTRATIME_BREAK_DESC
                    '        End If

                    '    End If
                    '    If m_objGameDetails.CurrentPeriod = 5 Then
                    '        EnableMenuItem("EndGameToolStripMenuItem", True)
                    '    End If
                    '    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                    'End If

                    If blnEndGame Then 'entered END GAME - do the necessary work here (ONLY Mod 1 and Mod 2)
                        lblPeriod.Text = END_GAME_DESC
                        Dim dsOfficialandMatch As New DataSet
                        dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                        If dsOfficialandMatch.Tables.Count = 0 Then
                            MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Else
                            If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                    MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                    MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ElseIf IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                    MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                End If
                            Else
                                If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                    If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                        MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    End If
                                End If
                                If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                    If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                        MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    End If
                                End If
                            End If
                        End If
                        If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                            m_objGameDetails.IsEndGame = True
                            Dim objfrmTeamSetup As New frmTeamSetup("Home")
                            If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                    Dim objfrmgameSetup As New frmGameSetup
                                    objfrmgameSetup.ShowDialog()
                                    m_objGameDetails.IsEndGame = True
                                End If
                            End If
                        Else
                            m_objGameDetails.IsEndGame = True
                            If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                Dim objfrmgameSetup As New frmGameSetup
                                objfrmgameSetup.ShowDialog()
                            End If
                        End If
                        EnableMenuItem("AbandonToolStripMenuItem", False)
                        EnableMenuItem("DelayedToolStripMenuItem", False)
                        EnableMenuItem("RevertSubsStripMenuItem", False)
                        EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("RatingsToolStripMenuItem", True)
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("AbandonToolStripMenuItem", False)
                        EnableMenuItem("RevertSubsStripMenuItem", False)
                        EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                        EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                    End If
                Case "InputTeamStatsToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "InputTeamStats", 1, 0)
                    frmInputTeamStats.ShowDialog()

                Case "EndGameToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Period" & " -> " & "End Game", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Start Period", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage29, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    m_objGameDetails.IsEndGame = True
                    SetClockControl(False)
                    If MessageDialog.Show(strmessage42, "End Game", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        m_objGameDetails.IsEndGame = True
                    Else
                        m_objGameDetails.IsEndGame = False
                    End If
                    'insert end game
                    m_objActionDetails.TeamId = Me.m_objGameDetails.HomeTeamID
                    If m_objGameDetails.IsEndGame = True Then
                        m_objActionDetails.EventId = 10
                        m_objActions.InsertPlayByPlayData()
                    End If
                    If m_objGameDetails.IsEndGame = True Then
                        m_objActions.EnableDisableControls(False, False)
                    Else
                        m_objActions.EnableDisableControls(True, False)
                    End If
                    m_objActions.UnHighlightActions()
                    m_objActions.ResetProperties()

                    If m_objGameDetails.IsEndGame = True Then
                        lblPeriod.Text = END_GAME_DESC
                        Dim dsOfficialandMatch As New DataSet
                        dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
                        If dsOfficialandMatch.Tables.Count = 0 Then
                            MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Else
                            If dsOfficialandMatch.Tables(0).Rows.Count = 1 And dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) And IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                    MessageDialog.Show(strmessage49, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ElseIf IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                    MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                ElseIf IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                    MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                End If
                            Else
                                If dsOfficialandMatch.Tables(0).Rows.Count = 1 Then
                                    If IsDBNull(dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE")) Then
                                        MessageDialog.Show(strmessage50, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    End If
                                End If
                                If dsOfficialandMatch.Tables(1).Rows.Count = 1 Then
                                    If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                                        MessageDialog.Show(strmessage51, "End Game", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    End If
                                End If
                            End If
                        End If
                        If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                            m_objGameDetails.IsEndGame = True
                            Dim objfrmTeamSetup As New frmTeamSetup("Home")
                            If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                    Dim objfrmgameSetup As New frmGameSetup
                                    objfrmgameSetup.ShowDialog()
                                    m_objGameDetails.IsEndGame = True
                                End If
                            End If
                        Else
                            m_objGameDetails.IsEndGame = True
                            If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                Dim objfrmgameSetup As New frmGameSetup
                                objfrmgameSetup.ShowDialog()
                            End If
                        End If
                        EnableMenuItem("AbandonToolStripMenuItem", False)
                        EnableMenuItem("DelayedToolStripMenuItem", False)
                        EnableMenuItem("RevertSubsStripMenuItem", False)
                        EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("RatingsToolStripMenuItem", True)
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("AbandonToolStripMenuItem", False)
                        EnableMenuItem("RevertSubsStripMenuItem", False)
                        EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                        EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                    End If
                    '' GetPenaltyShootoutScore()

                Case "InjuryTimeToolStripMenuItem"
                    'RM 8695
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Injury Time", 1, 0)

                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "Injury Time", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Injury Time", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    'Check if the Injury time already exists for that Period
                    If m_objGeneral.GetInjuryTimePeriodCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod) > 0 Then
                        MessageDialog.Show(strmessage59, "Injury Time", MessageDialogButtons.OK, MessageDialogIcon.Error)
                        Exit Try
                    End If

                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    Dim objInjtime As New frmInjuryTime
                    If objInjtime.ShowDialog = Windows.Forms.DialogResult.OK Then
                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.INJURY_TIME, "")
                    End If
                    m_objGameDetails.IsInsertMode = False

                    'Case "DelayOverToolStripMenuItem"
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Resume Delay", 1, 0)

                    '    If IsPBPEntryAllowed() = False Then
                    '        Exit Try
                    '    End If
                    '    If MessageDialog.Show(strmessage57, "Delay Over", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    '        If Not m_objModule1PBP Is Nothing Then
                    '            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAY_OVER, "Save")
                    '            If Not m_objModule1PBP Is Nothing And m_objGameDetails.CurrentPeriod > 0 Then
                    '                m_objModule1PBP.DisableAllControls(True)
                    '            End If
                    '        ElseIf Not m_objModule1Main Is Nothing Then
                    '            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAY_OVER, "")
                    '        End If
                    '        EnableMenuItem("DelayedToolStripMenuItem", True)
                    '        EnableMenuItem("DelayOverToolStripMenuItem", False)
                    '    End If

                    'Case "DelayedToolStripMenuItem"
                    '    'Arindam 2-May-12 Added code for Match Delay event
                    '    m_objGameDetails.IsDelayed = True
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Match Delayed", 1, 0)
                    '    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    '    If IsPBPEntryAllowed() = False Then
                    '        Exit Try
                    '    End If

                    '    If m_objGameDetails.IsReplaceMode Or m_objGameDetails.IsInsertMode Then
                    '        MessageDialog.Show(strmessage7, "Replace", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '        Exit Sub
                    '    End If

                    '    Dim objComments As New frmComments
                    '    If Not m_objModule1PBP Is Nothing Then
                    '        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    '            MessageDialog.Show(strmessage6, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '            Exit Sub
                    '        End If
                    '    End If
                    '    If m_objGameDetails.CurrentPeriod = 0 Then
                    '        Dim dsPBP As New DataSet
                    '        dsPBP = m_objGeneral.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                    '        Dim dr() As DataRow = dsPBP.Tables(0).Select("EVENT_CODE_ID = 59 ")
                    '        If dr.Length > 0 Then
                    '            MessageDialog.Show(strmessage54, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '            Exit Sub
                    '        End If
                    '    End If

                    '    If MessageDialog.Show(strmessage58, "Delay", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    '        If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                    '            If Not m_objModule1PBP Is Nothing Then

                    '                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")

                    '            ElseIf Not m_objModule1Main Is Nothing Then
                    '                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                    '            ElseIf Not m_objModule2Score Is Nothing Then
                    '                m_objModule2Score.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                    '                m_objModule2Score.lvwPBP.Enabled = True
                    '            End If

                    '            GetPenaltyShootoutScore()
                    '            If UdcRunningClock1.URCIsRunning Then
                    '                UdcRunningClock1.URCToggle()
                    '            End If
                    '            If UdcRunningClock1.URCIsRunning Then
                    '                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    '            Else
                    '                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                    '            End If

                    '            'RM 8695
                    '            If m_objGameDetails.CoverageLevel = 4 Or m_objGameDetails.CoverageLevel = 5 Or m_objGameDetails.CoverageLevel = 6 Then
                    '                MessageDialog.Show(strmessage55, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '                EnableMenuItem("DelayedToolStripMenuItem", False)
                    '                EnableMenuItem("DelayOverToolStripMenuItem", True)

                    '                If Not m_objModule1PBP Is Nothing Then
                    '                    m_objModule1PBP.DisableAllControls(False)
                    '                    m_objModule1PBP.btnBookingAway.Enabled = False
                    '                    m_objModule1PBP.btnBookingHome.Enabled = False
                    '                    m_objModule1PBP.btnSubstituteAway.Enabled = False
                    '                    m_objModule1PBP.btnSubstituteHome.Enabled = False
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    '    m_objGameDetails.IsDelayed = False

                    'Case "AbandonToolStripMenuItem"
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Abandon", 1, 0)
                    '    If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3 Then
                    '        If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                    '            MessageDialog.Show(strmessage45, "Abandoned", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '            Exit Try
                    '        End If
                    '    End If

                    '    'm_objGameDetails.GameCode
                    '    If IsGameAbandoned() = True Then
                    '        Exit Try
                    '    End If
                    '    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    '    If IsPBPEntryAllowed() = False Then
                    '        Exit Try
                    '    End If
                    '    If m_objGameDetails.ModuleID = 2 And (Not m_objModule1Main Is Nothing) Then
                    '        MessageDialog.Show(strmessage47, "Abandon", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    '        Exit Sub
                    '    End If

                    '    If MessageDialog.Show(strmessage17, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    '        Dim objabandon As New frmAbandon
                    '        If Not m_objModule1PBP Is Nothing Then
                    '            If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    '                MessageDialog.Show(strmessage6, "Abandon", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '                Exit Sub
                    '            End If
                    '        End If
                    '        If m_objGameDetails.ModuleID = 2 Then
                    '            If m_objGameDetails.CoverageLevel > 1 Then
                    '                If m_objModule1PBP.txtTimeMod2.Text.Replace(" :", "").Trim = "" Then
                    '                    MessageDialog.Show(strmessage46, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    '                    Exit Sub
                    '                End If
                    '            Else
                    '                If m_objModule2Score.txtTimeMod2.Text.Replace(" :", "").Trim = "" Then
                    '                    MessageDialog.Show(strmessage46, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    '                    Exit Sub
                    '                End If
                    '            End If

                    '        End If
                    '        If objabandon.ShowDialog = Windows.Forms.DialogResult.OK Then
                    '            If Not m_objModule1PBP Is Nothing Then
                    '                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.ABANDONED, "Save")
                    '            ElseIf Not m_objModule1Main Is Nothing Then
                    '                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.ABANDONED, "")
                    '            ElseIf Not m_objModule2Score Is Nothing Then
                    '                m_objModule2Score.AddPBPGeneralEvents(clsGameDetails.ABANDONED, "")
                    '            End If
                    '            If Not m_objModule1Main Is Nothing Then
                    '                'm_objModule1Main.InsertPlayByPlayData(INTENDPERIOD, "Save")
                    '                'm_objModule1Main.InsertPlayByPlayData(INTCLOCKSTOP, "CLOCKSTOP")
                    '                m_objModule1Main.lklNewHomeBooking.Enabled = False
                    '                m_objModule1Main.lklNewHomeGoal.Enabled = False
                    '                m_objModule1Main.lklNewHomePenalty.Enabled = False
                    '                m_objModule1Main.lklNewHomeSub.Enabled = False
                    '                m_objModule1Main.lklNewVisitBooking.Enabled = False
                    '                m_objModule1Main.lklNewVisitGoal.Enabled = False
                    '                m_objModule1Main.lklNewVisitPenalty.Enabled = False
                    '                m_objModule1Main.lklNewVisitSub.Enabled = False
                    '            ElseIf Not m_objModule1PBP Is Nothing Then
                    '                'm_objModule1PBP.AddPBPGeneralEvents(INTENDPERIOD, "Save")
                    '                'm_objModule1PBP.AddPBPGeneralEvents(INTCLOCKSTOP, "CLOCKSTOP")
                    '                m_objModule1PBP.DisableAllControls(False)
                    '                m_objModule1PBP.btnSubstituteAway.Enabled = False
                    '                m_objModule1PBP.btnSubstituteHome.Enabled = False
                    '                m_objModule1PBP.btnBookingAway.Enabled = False
                    '                m_objModule1PBP.btnBookingHome.Enabled = False
                    '            End If

                    '            If UdcRunningClock1.URCIsRunning Then
                    '                UdcRunningClock1.URCToggle()
                    '            End If
                    '            If UdcRunningClock1.URCIsRunning Then
                    '                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                    '            Else
                    '                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                    '            End If
                    '            SetClockControl(False)

                    '            EnableMenuItem("AbandonToolStripMenuItem", False)
                    '            EnableMenuItem("DelayedToolStripMenuItem", False)
                    '        End If
                    '    End If

                    'Case "RevertSubsStripMenuItem"
                    '    If IsGameAbandoned() = True Then
                    '        Exit Try
                    '    End If
                    '    Dim objDisplaySub As New frmDisplaySubs
                    '    objDisplaySub.ShowDialog()

                Case "ActiveGoalKeeperToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Active Goalkeeper", 1, 0)
                    Dim objactGK As New frmActiveGoalkeeper

                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.IsReplaceMode Then
                        MessageDialog.Show(strmessage2, "Replace", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If
                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    If objactGK.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.GOALIE_CHANGE, "")
                    End If
                    GetPenaltyShootoutScore()
                    If (Not m_objActions Is Nothing) Then
                        m_objActions.FillPlayersInUserControls()
                    End If
                Case "AbandonToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Abandon", 1, 0)

                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage45, "Abandoned", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Abandoned", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    If MessageDialog.Show(strmessage17, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Dim objabandon As New frmAbandon
                        If objabandon.ShowDialog = Windows.Forms.DialogResult.OK Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.ABANDONED, "")
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_STOP, "")
                            m_objGameDetails.IsGameAbandoned = True
                            m_objActions.EnableDisableControls(False, False)
                            SetClockControl(False)
                            EnableMenuItem("AbandonToolStripMenuItem", False)
                            If UdcRunningClock1.URCIsRunning Then
                                UdcRunningClock1.URCToggle()
                            End If
                        End If
                    End If
                Case "DelayOverToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Resume Delay", 1, 0)

                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage64, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If m_objGameDetails.IsDelayed = False Then
                        MessageDialog.Show("You are trying to resume a game which is not in delay, please check!", "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    If MessageDialog.Show(strmessage57, "Delay Over", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAY_OVER, "")
                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_START, "")
                        m_objGameDetails.IsDelayed = False
                        If Not isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                            m_objActions.EnableDisableControls(True)
                            If Not UdcRunningClock1.URCIsRunning Then
                                UdcRunningClock1.URCToggle()
                            End If
                        End If
                        EnableMenuItem("DelayedToolStripMenuItem", True)
                    End If

                Case "DelayedToolStripMenuItem"

                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Match Delayed", 1, 0)

                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage64, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Delay", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    Dim objComments As New frmComments

                    If MessageDialog.Show(strmessage58, "Delay", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.CLOCK_STOP, "")
                            m_objActions.EnableDisableControls(False, False)
                            m_objGameDetails.IsDelayed = True
                            EnableMenuItem("DelayedToolStripMenuItem", False)
                            If UdcRunningClock1.URCIsRunning Then
                                UdcRunningClock1.URCToggle()
                            End If
                        End If
                    End If

                Case "GameInterruptionMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Game Interruption", 1, 0)

                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage65, "Game Interruption", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsGameAbandoned Then
                        MessageDialog.Show(strmessage12, "Game Interruption", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If m_objGameDetails.IsDelayed Then
                        MessageDialog.Show(strmessage55, "Game Interruption", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If
                    If MessageDialog.Show(strmessage63, "Game Interruption", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.GAMEINTERRUPTION, "")
                    End If
                Case "OpenToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Open", 1, 0)
                    'Case "OpenToolStripMenuItem"
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Open", 1, 0)

                    '    If Not m_objModule1PBP Is Nothing Then
                    '        If m_objGameDetails.IsEditMode Or m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    '            MessageDialog.Show(strmessage16, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '            Exit Sub
                    '        End If
                    '    End If

                    '    'Shravani
                    '    tmrRefresh.Enabled = False
                    '    tmrRefresh.Stop()
                    '    frmTouches.TmrTouches.Stop()

                    '    Dim objSelectGame As New frmSelectGame
                    '    If objSelectGame.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                    '        Exit Sub
                    '    End If

                    '    If m_objGameDetails.ModuleID = 3 Then
                    '        m_objModule1Main = Nothing
                    '        m_objModule1PBP = Nothing
                    '        m_objActions = New FrmActions
                    '    End If

                    '    If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                    '        If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    '            If Not m_objModule1Main Is Nothing Then
                    '                m_objGameDetails.PBP = AddPBPColumns()
                    '                SetChildForm()
                    '                SetMainFormMenu()
                    '                SetMainFormSize()
                    '                SetPeriod()
                    '                InitializeClock()
                    '                FillFormControls()
                    '                m_objModule1Main.FillSelectedGameDetails()
                    '            ElseIf Not m_objModule1PBP Is Nothing Then
                    '                m_objGameDetails.PBP = AddPBPColumns()
                    '                SetPeriod()
                    '                SetChildForm()
                    '                SetMainFormMenu()
                    '                SetMainFormSize()

                    '                InitializeClock()
                    '                FillFormControls()
                    '                FillGames()
                    '                'getting the current players for the selected game
                    '                ResetClockAndScores()
                    '                If m_objGameDetails.CurrentPeriod <> 0 Then
                    '                    pnlEntryForm.Enabled = True
                    '                    If m_objModule1PBP.checkForHalftime() Then
                    '                        m_objModule1PBP.DisableAllControls(False)
                    '                    Else
                    '                        m_objModule1PBP.DisableAllControls(True)
                    '                    End If

                    '                End If
                    '                m_objModule1PBP.GetCurrentPlayersAfterRestart()
                    '                m_objModule1PBP.DisplayPBP()
                    '                m_objModule1PBP.SetTeamButtonColor()
                    '                m_objModule1PBP.getDirectionFromHomeStart(m_objGameDetails.CurrentPeriod)

                    '            ElseIf Not m_objModule2Score Is Nothing Then
                    '                FillGames()
                    '                FillFormControls()
                    '                m_objModule2Score.LoadTeam(m_objGameDetails.GameCode)
                    '                m_objModule2Score.FillListView()
                    '                ResetClockAndScores()
                    '            ElseIf Not m_objTouches Is Nothing Then
                    '                Dim Ds As New DataSet
                    '                m_objclsTeamSetup.RosterInfo = Ds
                    '                'SetChildForm()
                    '                SetMainFormMenu()
                    '                'SetMainFormSize()
                    '                InitializeClock()
                    '                FillFormControls()

                    '                If m_objGameDetails.IsRestartGame = True Then
                    '                    m_objTouches.RestartExistingGame()
                    '                    pnlEntryForm.Enabled = True
                    '                    btnClock.Enabled = True
                    '                    m_objGameDetails.IsRestartGame = False
                    '                    m_objGameDetails.RestartData.Tables.Clear()
                    '                    If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                    '                        SetHeader2()
                    '                    End If
                    '                    If m_objGameDetails.CurrentPeriod = 0 Then
                    '                        m_objTouches.pnlTouchTypes.Enabled = False
                    '                        m_objTouches.UdcSoccerField1.Enabled = False
                    '                        m_objTouches.btnSave.Enabled = False
                    '                        m_objTouches.btnClear.Enabled = False
                    '                    Else
                    '                        m_objTouches.pnlTouchTypes.Enabled = True
                    '                        m_objTouches.UdcSoccerField1.Enabled = True
                    '                        m_objTouches.btnSave.Enabled = True
                    '                        m_objTouches.btnClear.Enabled = True
                    '                    End If
                    '                Else
                    '                    m_objTouches.GetTouches(3)
                    '                End If
                    '                SetPeriod()
                    '                SetChildForm()
                    '                SetMainFormSize()
                    '                m_objActions.SetTeamColor(True, m_objGameDetails.HomeTeamColor)
                    '                m_objActions.SetTeamColor(False, m_objGameDetails.VisitorTeamColor)

                    '            End If
                    '            m_objGameDetails.IsRestartGame = False
                    '        Else
                    '            If Not m_objModule1Main Is Nothing Then
                    '                m_objModule1Main.FillSelectedDemoGameDetails(m_objGameDetails.GameCode)
                    '            ElseIf Not m_objModule1PBP Is Nothing Then
                    '                FillGames()
                    '                FillFormControls()
                    '                ResetClockAndScores()
                    '                m_objModule1PBP.DisplayPBP()
                    '            End If
                    '        End If
                    '    Else
                    '        If m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                    '            m_objModule1Main = Nothing
                    '            m_objModule1PBP = Nothing
                    '            m_objActions = Nothing
                    '            btnGotoPBP.Visible = False
                    '            btnGotoMain.Visible = False
                    '            DisplayScreensForOPS()
                    '            EnableMenuItem("CommentToolStripMenuItem", True)

                    '            EnableMenuItem("SetTeamColorToolStripMenuItem", True)
                    '            EnableMenuItem("OpenToolStripMenuItem", True)
                    '        Else
                    '            'PBP_MODULE, MULTIGAME_PBP_MODULE, TOUCHES_MODULE
                    '            m_objCommentary = Nothing
                    '            DisplayScreensForOPS()
                    '            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    '                If Not m_objModule1Main Is Nothing Then
                    '                    m_objModule1Main.FillSelectedGameDetails()
                    '                ElseIf Not m_objModule1PBP Is Nothing Then
                    '                    FillGames()
                    '                    FillFormControls()
                    '                    ResetClockAndScores()
                    '                    m_objModule1PBP.DisplayPBP()
                    '                End If

                    '            Else
                    '                If Not m_objModule1Main Is Nothing Then
                    '                    m_objModule1Main.FillSelectedDemoGameDetails(m_objGameDetails.GameCode)
                    '                ElseIf Not m_objModule1PBP Is Nothing Then
                    '                    FillGames()
                    '                    FillFormControls()
                    '                    ResetClockAndScores()
                    '                    m_objModule1PBP.DisplayPBP()
                    '                End If
                    '            End If
                    '        End If
                    '    End If

                Case "ExitToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Exit", 1, 0)
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Close", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If
                    Select Case m_objGameDetails.ModuleID
                        Case 3 'Touch Module.
                            Select Case m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1)
                                'TOSOCRS-62 Touch Reporter 2/3-End Game Warning
                                Case "1"
                                    If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                                    Else
                                        If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                            Exit Sub
                                        End If
                                    End If
                            End Select
                            'TOSOCRS-29 Exit Warning for Unfinalized Touches
                            If m_objGameDetails.ModuleID = 3 Then
                                Select Case m_objGameDetails.ReporterRoleSerial
                                    Case 3, 4
                                        If Not m_objGameDetails.IsTouchFinalized Then
                                            If MessageDialog.Show(strmessage62, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                                Exit Sub
                                            End If
                                        End If
                                End Select
                            End If
                        Case 4 'Çommentary Module
                            If m_objGameDetails.CurrentPeriod > 0 And (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False) Then
                            Else
                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                    Exit Sub
                                End If
                            End If
                        Case Else 'PBP Module
                            Dim endGameCount As DataSet
                            endGameCount = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If CInt(endGameCount.Tables(0).Rows(0).Item("endGameCount")) = 0 And m_objGameDetails.CurrentPeriod > 0 Then
                                If MessageDialog.Show(strmessage13 + " " + strmessage14, "Close", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.OK Then
                                    Exit Sub
                                End If
                            End If
                    End Select
                    m_formClosed = True
                    Me.Close()

                Case "TeamSetupToolStripMenuItem"
                    ' AUDIT(TRIAL)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Team Setup", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                        If IsPBPEntryAllowed() = False Then
                            Exit Try
                        End If
                    End If
                    'display players in Actions screen.
                    If Not m_objActions Is Nothing Then
                        m_objActions.SetCustomFormation()
                    End If

                    Dim objTeamSetup As New frmTeamSetup("Home")
                    objTeamSetup.ShowDialog()
                    'ONCE THE PLAYERS ARE FILLED IN STARTING LINEUPS, THE MAIN FORM PLAYERS ARE REFRESHED ..
                    If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                        If Not m_objModule1Main Is Nothing Then
                            SortPlayers()
                            m_objModule1Main.LabelsTagged()
                        End If
                    End If
                    'display players in Actions screen.
                    If Not m_objActions Is Nothing Then
                        m_objActions.FillPlayersInUserControls()
                    End If

                Case "GameSetupToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Game Setup", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    Dim objGameSetup As New frmGameSetup
                    If objGameSetup.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If m_objGameDetails.Reason <> "" Then
                            m_objGameDetails.CommentData = m_objGameDetails.Reason
                            If Not m_objModule1PBP Is Nothing Then
                                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.DELAYED, "Save")
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft   'set home team on left for field user control
                            ElseIf Not m_objModule1Main Is Nothing Then
                                m_objModule1Main.InsertPlayByPlayData(clsGameDetails.DELAYED, "")
                            End If
                        End If
                        If Not m_objModule1Main Is Nothing Then
                            m_objModule1Main.InsertOfficialsandMatchInfo()
                        End If
                        If Not m_objModule1PBP Is Nothing Then
                            'Arindam 23-May-2012
                            If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft   'set home team on left for field user control
                                m_objModule1PBP.UdcSoccerField1.USFAwayTeamName = m_objGameDetails.AwayTeam
                                m_objModule1PBP.UdcSoccerField1.USFHomeTeamName = m_objGameDetails.HomeTeam
                            End If
                        End If
                    End If
                    'objGameSetup.ShowDialog()

                Case "AddOfficialToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Official", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    Dim objAddOfficial As New frmAddOfficial
                    objAddOfficial.Show()

                Case "AddPlayerToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Player", 1, 0)
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    If Not m_objModule1PBP Is Nothing Then
                        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                            MessageDialog.Show(strmessage6, "Close", MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If
                    End If

                    Dim objAddPlayer As New frmAddPlayer("Home")
                    objAddPlayer.ShowDialog()
                    If Not m_objModule1Main Is Nothing Then
                        m_objModule1Main.GetCurrentPlayersAfterRestart()
                        SortPlayers()
                    End If
                    If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE And m_objGameDetails.CoverageLevel = 2 Then
                        m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
                    End If
                    If Not m_objActions Is Nothing Then
                        m_objActions.FillPlayersInUserControls()
                    End If

                Case "AddManagerToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Manager", 1, 0)
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    Dim objAddManager As New frmAddManager("Home")
                    objAddManager.ShowDialog()

                Case "SwitchSidesToolStripMenuItem"

                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Switch Sides", 1, 0)

                    If m_objGameDetails.ReporterRoleSerial <> 1 Then
                        MessageDialog.Show(strmessage66, "Switch Side", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Try
                    End If

                    m_objActions.SwitchSide()

                Case "HomeTeamColorToolStripMenuItem", "VisitorTeamColorToolStripMenuItem"
                    If Not cdlgTeam.ShowDialog = Windows.Forms.DialogResult.OK Then
                        Exit Select
                    End If
                    SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                    SetTeamColorInMain(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)

                    If Not m_objActions Is Nothing Then
                        m_objActions.SetTeamColor(MenuItemName = "HomeTeamColorToolStripMenuItem", cdlgTeam.Color)
                    Else
                        If (m_objActionDetails.TeamId = m_objGameDetails.HomeTeamID) Then
                            m_objActionsAssist.SetTeamColor(m_objGameDetails.HomeTeamColor)
                        Else
                            m_objActionsAssist.SetTeamColor(m_objGameDetails.VisitorTeamColor)
                        End If
                    End If
                Case "AboutToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Help" & " -> " & "About", 1, 0)

                    Dim objAbout As New frmAbout
                    objAbout.ShowDialog()

                    'TOSOCRS-33-SOC - Manual restart of datasync component in reporter software
                Case "RestartDataSyncComponentMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Help" & " -> " & "Restart DataSync Component Clicked", 1, 0)
                    ' Killing Datasync and starting again on click of menu
                    LoadSoccerDataSync()
                Case "PenaltyShootoutToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Add Manager", 1, 0)
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If

                    EnableMenuItem("StartPeriodToolStripMenuItem", False)
                    EnableMenuItem("EndPeriodToolStripMenuItem", False)
                    Dim objPenaltyShootoutScore As frmPenaltyShootoutPz = Nothing
                    objPenaltyShootoutScore = New frmPenaltyShootoutPz()
                    objPenaltyShootoutScore.ShowDialog()

                    If m_objGameDetails.IsEndGame Then
                        SetClockControl(False)
                        lblPeriod.Text = END_GAME_DESC
                        If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
                            Dim dsEndGameCnt As New DataSet
                            dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                            If dsEndGameCnt.Tables.Count > 0 Then
                                If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                                    Dim intEndGame As Integer = CInt(dsEndGameCnt.Tables(0).Rows(0).Item("endGameCount"))
                                    If intEndGame <= 0 Then
                                        If Not m_objModule1Main Is Nothing Then
                                            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.ENDGAME, "Save")
                                        ElseIf Not m_objModule1PBP Is Nothing Then
                                            m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.ENDGAME, "Save")
                                            m_objModule1PBP.btnTime.Enabled = False
                                        End If
                                        If m_objGameDetails.ModuleID = 1 Then
                                            If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                                m_objGameDetails.IsEndGame = True
                                                Dim objfrmTeamSetup As New frmTeamSetup("Home")
                                                If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                                                    If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                                        Dim objfrmgameSetup As New frmGameSetup
                                                        objfrmgameSetup.ShowDialog()
                                                        m_objGameDetails.IsEndGame = True
                                                    End If
                                                End If
                                            Else
                                                m_objGameDetails.IsEndGame = True
                                                If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                                    Dim objfrmgameSetup As New frmGameSetup
                                                    objfrmgameSetup.ShowDialog()
                                                End If
                                            End If
                                        End If
                                        EnableMenuItem("ManagerExpulsionToolStripMenuItem", False)
                                    End If
                                End If
                            End If
                            EnableMenuItem("AbandonToolStripMenuItem", False)
                            EnableMenuItem("DelayedToolStripMenuItem", False)
                            EnableMenuItem("RevertSubsStripMenuItem", False)
                            EnableMenuItem("ActiveGoalKeeperToolStripMenuItem", False)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        End If

                        EnableMenuItem("EndPeriodToolStripMenuItem", False)
                        EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        EnableMenuItem("EndGameToolStripMenuItem", False)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                        EnableMenuItem("RatingsToolStripMenuItem", True)

                    Else
                        EnableMenuItem("EndGameToolStripMenuItem", True)
                        EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                        EnableMenuItem("RatingsToolStripMenuItem", False)
                    End If

                    ''GetPenaltyShootoutScore()

                Case "BoxScoreToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "BoxScore", 1, 0)
                    LoadBoxScore()
                    ' Dim objBoxScore As New frmBoxScore
                    'objBoxScore.ShowDialog()

                    'Case "SortByUniformToolStripMenuItem"
                    '    m_objclsLoginDetails.strSortOrder = "Uniform"
                    '    Select Case m_objGameDetails.ModuleID
                    '        Case COMMENTARY_MODULE
                    '            m_objCommentary.SortTeamPlayers()
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Uniform", 1, 0)
                    '        Case TOUCHES_MODULE
                    '            m_objTouches.SortTeamPlayers()
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Uniform", 1, 0)
                    '        Case Else
                    '            SortPlayers()
                    '    End Select

                    'Case "SortByLastNameToolStripMenuItem"
                    '    m_objclsLoginDetails.strSortOrder = "Last Name"
                    '    Select Case m_objGameDetails.ModuleID
                    '        Case COMMENTARY_MODULE
                    '            m_objCommentary.SortTeamPlayers()
                    '            'AUDIT TRIAL
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Last Name", 1, 0)
                    '        Case TOUCHES_MODULE
                    '            m_objTouches.SortTeamPlayers()
                    '            'AUDIT TRIAL
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Last Name", 1, 0)
                    '        Case Else
                    '            SortPlayers()
                    '    End Select
                    'Case "SortByPositionToolStripMenuItem"
                    '    m_objclsLoginDetails.strSortOrder = "Position"
                    '    Select Case m_objGameDetails.ModuleID
                    '        Case COMMENTARY_MODULE
                    '            m_objCommentary.SortTeamPlayers()
                    '            'AUDIT TRIAL
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Position", 1, 0)

                    '        Case TOUCHES_MODULE
                    '            m_objTouches.SortTeamPlayers()
                    '            'AUDIT TRIAL
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup -> Sort Players -> Position", 1, 0)
                    '        Case Else
                    '            SortPlayers()
                    '    End Select
                    'Case "KeyMomentToolStripMenuItem"

                    '    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    '    If IsPBPEntryAllowed() = False Then
                    '        Exit Try
                    '    End If
                    '    m_objKeyMoments = New frmKeyMoments()
                    '    m_objKeyMoments.ShowDialog()
                    '    'frmKeyMoments.ShowDialog()
                    '    'Case "KeyMomentToolStripMenuItem"
                    '    '    'Dim objKeyMoments As New frmKeyMoments
                    '    '    'objKeyMoments.ShowDialog()
                    '    '    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    '    '    If IsPBPEntryAllowed() = False Then
                    '    '        Exit Try
                    '    '    End If

                    '    '    frmKeyMoments.ShowDialog()
                    '    m_objKeyMoments = Nothing
                    'Case "TeamPossessionMenuItem"
                    '    'AUDIT TRIAL
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "TeamPossessionMenuItem", 1, 0)

                    '    Dim objTeamPossessionMenuItem As New frmTeamPossession
                    '    objTeamPossessionMenuItem.ShowDialog()

                    'Case "CommentToolStripMenuItem"
                    '    'AUDIT TRIAL
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    '    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Comments", 1, 0)
                    '    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    '    If IsPBPEntryAllowed() = False Then
                    '        Exit Try
                    '    End If

                    '    If m_objGameDetails.IsReplaceMode Then
                    '        MessageDialog.Show(strmessage7, "Replace", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '        'If Not m_objModule1PBP Is Nothing Then

                    '        'End If
                    '        Exit Sub
                    '    End If
                    '    Dim objComments As New frmComments
                    '    If Not m_objModule1PBP Is Nothing Then
                    '        If m_objGameDetails.IsEditMode Or m_objModule1PBP.dblstEventDetails01.Visible = True Then
                    '            MessageDialog.Show(strmessage6, "Comment", MessageDialogButtons.OK, MessageDialogIcon.Information)
                    '            Exit Sub
                    '        End If
                    '    End If

                    '    If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                    '        If Not m_objModule1PBP Is Nothing Then
                    '            If m_objGameDetails.IsInsertMode Then
                    '                m_objModule1PBP.AddPlayByPlayData(clsGameDetails.COMMENTS, "")
                    '                m_objModule1PBP.InsertPlaybyPlayDatafromMain()
                    '                m_objModule1PBP.CancelfromMain()
                    '                m_objGameDetails.IsInsertMode = False
                    '            ElseIf m_objGameDetails.IsReplaceMode Then
                    '                m_objGameDetails.IsReplaceMode = False
                    '            Else
                    '                m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.COMMENTS, "Save")
                    '            End If
                    '        ElseIf Not m_objModule1Main Is Nothing Then
                    '            m_objModule1Main.InsertPlayByPlayData(clsGameDetails.COMMENTS, "")
                    '        ElseIf Not m_objModule2Score Is Nothing Then
                    '            m_objModule2Score.AddPBPGeneralEvents(clsGameDetails.COMMENTS, "Save")
                    '        End If
                    '    End If
                    '    GetPenaltyShootoutScore()

                Case "PreferencesToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Setup" & " -> " & "Preferences", 1, 0)
                    Dim PrevDefaultClock As Boolean = False
                    Dim ClockRunning As Boolean = False
                    If m_objGameDetails.IsInsertMode Then
                        MessageDialog.Show(strmessage3, "Time Pref", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    Else
                        'If Not m_objModule1PBP Is Nothing Then
                        '    If m_objModule1PBP.dblstEventDetails01.Visible = True Then
                        '        MessageDialog.Show(strmessage3, "Time Pref", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        '        Exit Sub
                        '    End If
                        'End If
                        'If Not m_objTouches Is Nothing Then
                        '    If m_objTouches.txtTime.Text.Replace(":", "").Trim <> "" Then
                        '        MessageDialog.Show(strmessage3, "Time Pref", MessageDialogButtons.OK, MessageDialogIcon.Information)
                        '        Exit Sub
                        '    End If
                        'End If
                        If m_objGameDetails.IsDefaultGameClock Then
                            PrevDefaultClock = True
                        End If
                        If UdcRunningClock1.URCIsRunning Then
                            ClockRunning = True
                        End If
                        Dim objPreferences As New frmPreferences
                        objPreferences.ShowDialog()
                        If m_objGameDetails.CurrentPeriod > 1 Then
                            If (m_objGameDetails.IsDefaultGameClock And PrevDefaultClock = False) Or (m_objGameDetails.IsDefaultGameClock = False And PrevDefaultClock = True) Then
                                ConvertAndSetClock()
                            End If
                        End If
                    End If

                Case "PurgeAuditTrailToolStripMenuItem"
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "File" & " -> " & "Purge Audit Trial", 1, 0)

                    Dim objPurge As New frmPurge
                    objPurge.ShowDialog()

                Case "ManagerExpulsionToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "Others" & " -> " & "Manager Expulsion", 1, 0)


                    Dim objMangaerExpl As New frmManagerExpulsion
                    If IsGameAbandoned() = True Then
                        Exit Try
                    End If
                    'CHECK WHTHER REPORTER IS ALLOWED TO ENTER DATA OR NOT
                    If IsPBPEntryAllowed() = False Then
                        Exit Try
                    End If
                    'Save Primary in memory record if entered
                    If Not SaveInMemoryActionInPrimary() Then
                        Exit Sub
                    End If

                    If objMangaerExpl.ShowDialog = Windows.Forms.DialogResult.OK Then

                        m_objModule1Main.InsertPlayByPlayData(clsGameDetails.MANAGEREXPULSION, "")

                    End If

                    m_objGameDetails.IsInsertMode = False
                    m_objGameDetails.IsEditMode = False
                Case "TeamStatsToolStripMenuItem"
                    LoadTeamStats()
                Case "PlayerTouchesToolStripMenuItem"
                    LoadPlayerTouches()
                Case "PlayerStatsToolStripMenuItem"
                    LoadPlayerStats()
                Case "ReportFormationToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "Formation Report", 1, 0)
                    Loadfrmformation()
                Case "ReportPBPToolStripMenuItem"
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "Formation Report", 1, 0)
                    Loadpbpreport()
                Case "OthersToolStripMenuItem"
                    If m_objGameDetails.CurrentPeriod > 0 Then
                        EnableMenuItem("InputTeamStatsToolStripMenuItem", True)
                    Else
                        EnableMenuItem("InputTeamStatsToolStripMenuItem", False)
                    End If
                    If m_objGameDetails.CurrentPeriod > 0 Then
                        EnableMenuItem("TeamPossessionMenuItem", True)
                    Else
                        EnableMenuItem("TeamPossessionMenuItem", False)
                    End If
                    If m_objGameDetails.ReporterRoleSerial > 1 Then
                        EnableMenuItem("TouchesFinalized", True)
                    Else
                        EnableMenuItem("TouchesFinalized", False)
                    End If
                    'Case "SportVUToolStripMenuItem"
                    '    Dim m_objfrmSportVU As New frmSportVU
                    '    m_objfrmSportVU.Show()

                    'Case "HomeStartToolStripMenuItem"
                    '    Dim m_objHomestart As New frmHomeStart
                    '    If m_objHomestart.ShowDialog = Windows.Forms.DialogResult.OK Then
                    '        If m_objGameDetails.IsHalfTimeSwap Then
                    '            m_objTouches.SwitchSides()
                    '            SetHeader2()
                    '        End If
                    '        'Arindam 23-May-2012
                    '        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 3 Then
                    '            m_objGameDetails.isHomeDirectionLeft = m_objGameDetails.IsHomeTeamOnLeft
                    '            m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.isHomeDirectionLeft
                    '            m_objTouches.UdcSoccerField1.USFHomeTeamName = m_objGameDetails.HomeTeam
                    '            m_objTouches.UdcSoccerField1.USFAwayTeamName = m_objGameDetails.AwayTeam
                    '        End If
                    '    End If
                Case "RatingsToolStripMenuItem"
                    If (MessageDialog.Show(strmessage43, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                        m_objGameDetails.IsEndGame = True
                        Dim objfrmTeamSetup As New frmTeamSetup("Home")
                        If objfrmTeamSetup.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                            If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                                Dim objfrmgameSetup As New frmGameSetup
                                objfrmgameSetup.ShowDialog()
                                m_objGameDetails.IsEndGame = True
                            End If
                        End If
                    Else
                        m_objGameDetails.IsEndGame = True
                        If (MessageDialog.Show(strmessage44, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                            Dim objfrmgameSetup As New frmGameSetup
                            objfrmgameSetup.ShowDialog()
                        End If
                    End If
                Case "TouchesFinalized"
                    Select Case m_objGameDetails.ReporterRoleSerial
                        Case 2, 3, 4
                            'TOPZ-2249
                            For Each element As String In m_objActionsAssist.FinalValidation()
                                If element <> Nothing Then
                                    If MessageDialog.Show(element, "Finalized", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                        Exit Try
                                    End If
                                End If
                            Next

                            If MessageDialog.Show(strmessage60, "Finalized", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                m_objGeneral.UpdateTouchesDone(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                                MessageDialog.Show(strmessage61, "Finalized", MessageDialogButtons.OK, MessageDialogIcon.Information)
                                m_objGameDetails.IsTouchFinalized = True
                                Exit Try
                            End If
                    End Select
                Case "HotKeysMenuItem"  'TOSOCDATA-402
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MenuItemSelected, "View" & " -> " & "Hot Keys", 1, 0)
                    LoadfrmHotKeys()
            End Select
        Catch ex As Exception
            Throw ex
        Finally
            If (MenuItemName = "OpenToolStripMenuItem" And tmrRefresh.Enabled = False) Then
                tmrRefresh.Enabled = True
                tmrRefresh.Start()
                frmTouches.TmrTouches.Start()
            End If
        End Try

    End Sub
    Public Sub SetTeamColorInMain(ByVal homeTeam As Boolean, ByVal teamColor As Color)
        Try
            Dim textForeColor As Color = clsUtility.GetTextColor(teamColor)
            m_objGeneral.UpdateTeamColor(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, homeTeam, teamColor.ToArgb)
            Dim setPictureColorTop As Windows.Forms.PictureBox
            Dim setPictureColorBottom As Windows.Forms.PictureBox
            If homeTeam Then
                m_objGameDetails.HomeTeamColor = teamColor
                setPictureColorTop = picHomeColorTop
                setPictureColorBottom = picHomeColorBottom
                m_objGameDetails.HomeTeamColor = teamColor
            Else
                m_objGameDetails.VisitorTeamColor = teamColor
                setPictureColorTop = picAwayColorTop
                setPictureColorBottom = picAwayColorBottom
                m_objGameDetails.VisitorTeamColor = teamColor
            End If
            setPictureColorTop.BackColor = teamColor
            setPictureColorBottom.BackColor = teamColor

        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Private Sub ClearActionProperties()
    '    Try
    '        m_objActionDetails.TeamId = Nothing
    '        m_objActionDetails.EventId = Nothing
    '        m_objActionDetails.PlayerId = Nothing
    '        m_objActions.grpBooking.Visible = False
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub
    Private Sub FillGames()
        Dim dsMultiplegames As DataSet
        dsMultiplegames = m_objGeneral.GetGameDetails(m_objclsLoginDetails.LanguageID, m_objclsLoginDetails.UserId)

        If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
            For i As Integer = 0 To dsMultiplegames.Tables(0).Rows.Count - 1
                If CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE")) = m_objGameDetails.GameCode Then
                    m_objGameDetails.GameCode = CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE").ToString)
                    m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(dsMultiplegames.Tables(0).Rows(i).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                    m_objGameDetails.ModuleID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("MODULE_ID").ToString)
                    m_objGameDetails.CoverageLevel = CInt(dsMultiplegames.Tables(0).Rows(i).Item("TIER").ToString)
                    m_objGameDetails.AwayTeamID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAMID").ToString)
                    m_objGameDetails.AwayTeamAbbrev = dsMultiplegames.Tables(0).Rows(i).Item("AWAYABBREV").ToString
                    m_objGameDetails.AwayTeam = dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAM").ToString
                    m_objGameDetails.HomeTeamID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAMID").ToString)
                    m_objGameDetails.HomeTeamAbbrev = dsMultiplegames.Tables(0).Rows(i).Item("HOMEABBREV").ToString
                    m_objGameDetails.HomeTeam = dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAM").ToString
                    m_objGameDetails.LeagueID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("LEAGUE_ID").ToString)
                    m_objGameDetails.LeagueName = dsMultiplegames.Tables(0).Rows(i).Item("Competition").ToString
                    If m_objGameDetails.CoverageLevel = 1 Then
                    Else
                        m_objGameDetails.FieldID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("FIELD_ID").ToString)
                    End If
                    'm_objGameDetails.FieldName = dsMultiplegames.Tables(0).Rows(i).Item("FIELDNAME").ToString
                    m_objGameDetails.FeedNumber = CInt(dsMultiplegames.Tables(0).Rows(i).Item("FEEDNUMBER").ToString)
                    m_objGameDetails.ReporterRole = Convert.ToString(dsMultiplegames.Tables(0).Rows(i).Item("REPORTER_ROLE")) & Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))
                    m_objGameDetails.SerialNo = Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))
                    m_objGameDetails.ReporterRoleDisplay = frmLogin.GetReporterRoleDescription()
                    'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                    Dim intTimeElapsed As Integer = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                    m_objGeneral.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameLastPlayed, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, intTimeElapsed, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")
                End If
            Next
        ElseIf m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
            m_dsGames = m_objGeneral.ValidateUserDetails("ops", "ops", m_objGameDetails.languageid)
            Dim drs() As DataRow = m_dsGames.Tables(2).Select("GAME_CODE= " & m_objGameDetails.GameCode & " AND MODULE_ID = " & m_objGameDetails.ModuleID)

            If drs.Length > 0 Then
                m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)
                'm_objGameDetails.CoverageLevel = CInt(m_dsGames.Tables(2).Rows(i).Item("TIER").ToString)
                m_objGameDetails.AwayTeamID = CInt(drs(0).Item("AWAYTEAMID").ToString)
                m_objGameDetails.AwayTeamAbbrev = drs(0).Item("AWAYABBREV").ToString
                m_objGameDetails.AwayTeam = drs(0).Item("AWAYTEAM").ToString
                m_objGameDetails.HomeTeamID = CInt(drs(0).Item("HOMETEAMID").ToString)
                m_objGameDetails.HomeTeamAbbrev = drs(0).Item("HOMEABBREV").ToString
                m_objGameDetails.HomeTeam = drs(0).Item("HOMETEAM").ToString
                m_objGameDetails.LeagueID = CInt(drs(0).Item("LEAGUE_ID").ToString)
                m_objGameDetails.LeagueName = drs(0).Item("Competition").ToString
                m_objGameDetails.FieldID = CInt(drs(0).Item("FIELD_ID").ToString)
                m_objGameDetails.ReporterRoleDisplay = "Operation Desk"
                m_objGameDetails.ReporterRole = "OPS"
                m_objGameDetails.SerialNo = Convert.ToInt32(drs(0).Item("SERIAL_NO"))

                If m_objclsLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                    GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                End If
            End If
        Else
            For i As Integer = 0 To dsMultiplegames.Tables(0).Rows.Count - 1
                'If CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE")) = m_objGameDetails.GameCode Then
                '    Dim intOldGameCode As Integer = m_objGameDetails.GameCode
                '    m_objGameDetails.GameCode = CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE").ToString)
                '    m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(dsMultiplegames.Tables(0).Rows(i).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                '    m_objGameDetails.ModuleID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("MODULE_ID").ToString)
                '    m_objGameDetails.CoverageLevel = CInt(dsMultiplegames.Tables(0).Rows(i).Item("TIER").ToString)
                '    m_objGameDetails.AwayTeamID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAMID").ToString)
                '    m_objGameDetails.AwayTeamAbbrev = dsMultiplegames.Tables(0).Rows(i).Item("AWAYABBREV").ToString
                '    m_objGameDetails.AwayTeam = dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAM").ToString
                '    m_objGameDetails.HomeTeamID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAMID").ToString)
                '    m_objGameDetails.HomeTeamAbbrev = dsMultiplegames.Tables(0).Rows(i).Item("HOMEABBREV").ToString
                '    m_objGameDetails.HomeTeam = dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAM").ToString
                '    m_objGameDetails.LeagueID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("LEAGUE_ID").ToString)
                '    m_objGameDetails.LeagueName = dsMultiplegames.Tables(0).Rows(i).Item("Competition").ToString
                '    m_objGameDetails.FieldID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("FIELD_ID").ToString)
                '    If frmSelectGame.radFeedA.Checked = True Then
                '        m_objGameDetails.FeedNumber = 1
                '    ElseIf frmSelectGame.radFeedB.Checked = True Then
                '        m_objGameDetails.FeedNumber = 2
                '    End If
                '    'GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                '    m_objGameDetails.ReporterRoleDisplay = "OPS"
                '    m_objGameDetails.ReporterRole = Convert.ToString(dsMultiplegames.Tables(0).Rows(i).Item("REPORTER_ROLE")) & Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))
                '    m_objGameDetails.SerialNo = Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))

                '    'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                '    Dim intTimeElapsed As Integer = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                '    m_objGeneral.InsertCurrentGame(m_objGameDetails.GameCode, intOldGameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, intTimeElapsed, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")
                'End If

            Next
        End If
    End Sub

    Private Function SortPlayers() As DataSet
        Try
            Dim DsPlayers As New DataSet
            If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                DsPlayers = m_objTeamSetup.RosterInfo.Copy()
            End If
            m_objTeamSetup.RosterInfo.Tables.Clear()

            Select Case m_objclsLoginDetails.strSortOrder
                Case "Uniform"
                    CheckMenuItem("SortByUniformToolStripMenuItem", True)
                    CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                    CheckMenuItem("SortByPositionToolStripMenuItem", False)

                    If DsPlayers IsNot Nothing Then
                        If DsPlayers.Tables.Count > 0 Then
                            For i As Integer = 0 To DsPlayers.Tables.Count - 1
                                If DsPlayers.Tables(i).Rows.Count > 0 Then
                                    If Not DsPlayers.Tables(i).Columns.Contains("SORT_NUMBER") Then
                                        Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                                        dcHome.DataType = System.Type.GetType("System.Int32")
                                        DsPlayers.Tables(i).Columns.Add(dcHome)
                                        For Each drHome As DataRow In DsPlayers.Tables(i).Rows
                                            If IsDBNull(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = True Then
                                                drHome.Item("SORT_NUMBER") = 30000
                                            Else
                                                drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                            End If
                                        Next
                                    End If

                                    Dim dvHome As New DataView(DsPlayers.Tables(i))
                                    Dim dtSortHome As New DataTable
                                    dtSortHome = DsPlayers.Tables(i).Clone
                                    dtSortHome.TableName = DsPlayers.Tables(i).TableName
                                    Dim drArrHome As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION<=11", "SORT_NUMBER ASC")
                                    For Each drSortHome As DataRow In drArrHome
                                        dtSortHome.ImportRow(drSortHome)
                                    Next
                                    Dim drArrHomeBench As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION>11", "SORT_NUMBER ASC")
                                    For Each drSortHomeBench As DataRow In drArrHomeBench
                                        dtSortHome.ImportRow(drSortHomeBench)
                                    Next
                                    m_objTeamSetup.RosterInfo.Tables.Add(dtSortHome)
                                End If
                            Next
                        End If
                    End If

                Case "Position"
                    CheckMenuItem("SortByUniformToolStripMenuItem", False)
                    CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                    CheckMenuItem("SortByPositionToolStripMenuItem", True)

                    If DsPlayers IsNot Nothing Then
                        If DsPlayers.Tables.Count > 0 Then
                            For i As Integer = 0 To DsPlayers.Tables.Count - 1
                                If DsPlayers.Tables(i).Rows.Count > 0 Then
                                    Dim dvHome As New DataView(DsPlayers.Tables(i))
                                    Dim dtSortHome As New DataTable
                                    dtSortHome = DsPlayers.Tables(i).Clone
                                    dtSortHome.TableName = DsPlayers.Tables(i).TableName
                                    Dim drArrHome As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION<=11", "STARTING_POSITION ASC")
                                    For Each drSortHome As DataRow In drArrHome
                                        dtSortHome.ImportRow(drSortHome)
                                    Next
                                    Dim drArrHomeBench As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION>11", "STARTING_POSITION ASC")
                                    For Each drSortHomeBench As DataRow In drArrHomeBench
                                        dtSortHome.ImportRow(drSortHomeBench)
                                    Next
                                    m_objTeamSetup.RosterInfo.Tables.Add(dtSortHome)
                                End If
                            Next
                        End If
                    End If

                Case "Last Name"
                    CheckMenuItem("SortByUniformToolStripMenuItem", False)
                    CheckMenuItem("SortByLastNameToolStripMenuItem", True)
                    CheckMenuItem("SortByPositionToolStripMenuItem", False)
                    If DsPlayers IsNot Nothing Then
                        If DsPlayers.Tables.Count > 0 Then
                            For i As Integer = 0 To DsPlayers.Tables.Count - 1
                                If DsPlayers.Tables(i).Rows.Count > 0 Then
                                    Dim dvHome As New DataView(DsPlayers.Tables(i))
                                    Dim dtSortHome As New DataTable
                                    dtSortHome = DsPlayers.Tables(i).Clone
                                    dtSortHome.TableName = DsPlayers.Tables(i).TableName
                                    Dim drArrHome As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION<=11", "LAST_NAME ASC")
                                    For Each drSortHome As DataRow In drArrHome
                                        dtSortHome.ImportRow(drSortHome)
                                    Next
                                    Dim drArrHomeBench As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION>11", "LAST_NAME ASC")
                                    For Each drSortHomeBench As DataRow In drArrHomeBench
                                        dtSortHome.ImportRow(drSortHomeBench)
                                    Next
                                    m_objTeamSetup.RosterInfo.Tables.Add(dtSortHome)
                                End If
                            Next
                        End If
                    End If
            End Select

            If Not m_objModule1Main Is Nothing Then
                m_objModule1Main.LabelsTagged()
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function FormIsOpen(ByVal TargetForm As Form) As Boolean
        Try
            If TargetForm Is Nothing Then
                FormIsOpen = False
            Else
                FormIsOpen = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub LoadTeamStats()
        Try
            If FormIsOpen(objTeamStats) = False Then
                objTeamStats = New frmTeamStats()
                objTeamStats.Show()
            ElseIf objTeamStats Is Nothing OrElse objTeamStats.IsDisposed = True Then
                objTeamStats = New frmTeamStats()
                objTeamStats.Show()
            Else
                objTeamStats.Focus()
                objTeamStats.DisplayGameStats()
            End If
            objTeamStats.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub LoadPlayerStats()
        Try
            If FormIsOpen(objPlayerStats) = False Then
                objPlayerStats = New frmPlayerStats()
                objPlayerStats.Show()
            ElseIf objPlayerStats Is Nothing OrElse objPlayerStats.IsDisposed = True Then
                objPlayerStats = New frmPlayerStats()
                objPlayerStats.Show()
            Else
                objPlayerStats.Focus()
                objPlayerStats.DisplayPlayerStats()
            End If
            objPlayerStats.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'TOSOCDATA-402
    Public Sub LoadfrmHotKeys()
        Try
            If FormIsOpen(objHotKeys) = False Then
                objHotKeys = New frmHotKeys()
                objHotKeys.Show()
            ElseIf objHotKeys Is Nothing OrElse objHotKeys.IsDisposed = True Then
                objHotKeys = New frmHotKeys()
                objHotKeys.Show()
            Else
                objHotKeys.Focus()
                objHotKeys.loadreport()
            End If
            objHotKeys.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub Loadfrmformation()
        Try
            If FormIsOpen(objformationreport) = False Then
                objformationreport = New frmFormationReport()
                objformationreport.Show()
            ElseIf objformationreport Is Nothing OrElse objformationreport.IsDisposed = True Then
                objformationreport = New frmFormationReport()
                objformationreport.Show()
            Else
                objformationreport.Focus()
                objformationreport.loadreport()

            End If
            objformationreport.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub LoadPlayerTouches()
        If FormIsOpen(objPlayerTouches) = False Then
            objPlayerTouches = New frmPlayerTouches()
            objPlayerTouches.Show()
        ElseIf objPlayerTouches Is Nothing OrElse objPlayerTouches.IsDisposed = True Then
            objPlayerTouches = New frmPlayerTouches()
            objPlayerTouches.Show()
        Else
            objPlayerTouches.Focus()
            objPlayerTouches.DisplayGameStats()
        End If
        objPlayerTouches.BringToFront()
    End Sub

    Public Sub LoadBoxScore()
        Try
            If FormIsOpen(objBoxScore) = False Then
                objBoxScore = New frmBoxScore()
                objBoxScore.Show()
            ElseIf objBoxScore Is Nothing OrElse objBoxScore.IsDisposed = True Then
                objBoxScore = New frmBoxScore()
                objBoxScore.Show()
            Else
                objBoxScore.Focus()
                objBoxScore.CustomReports()
            End If
            objBoxScore.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Loadpbpreport()
        Try
            If FormIsOpen(objpbpreport) = False Then
                objpbpreport = New frmPBPReport()
                objpbpreport.Show()
            ElseIf objpbpreport Is Nothing OrElse objpbpreport.IsDisposed = True Then
                objpbpreport = New frmPBPReport()
                objpbpreport.Show()
            Else
                objpbpreport.Focus()
                'objpbpreport.CustomReports()
            End If
            objpbpreport.BringToFront()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EnableMenuItem(ByVal MenuItemName As String, ByVal Enable As Boolean)
        Try
            For Each tsmi As ToolStripMenuItem In mnsMain.Items
                If tsmi.Name = MenuItemName Then
                    tsmi.Enabled = Enable
                    Exit For
                End If
                For Each obj As Object In tsmi.DropDownItems
                    If Not obj.ToString = "System.Windows.Forms.ToolStripSeparator" Then
                        Dim chtsmi As ToolStripMenuItem = CType(obj, ToolStripMenuItem)
                        If chtsmi.Name = MenuItemName Then
                            chtsmi.Enabled = Enable
                            Exit For
                        End If
                    End If
                Next

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Added by shravani
    Public Sub ShowMenuItem(ByVal MenuItemName As String, ByVal Visible As Boolean)
        Try
            For Each tsmi As ToolStripMenuItem In mnsMain.Items
                If tsmi.Name = MenuItemName Then
                    tsmi.Visible = Visible
                    Exit For
                End If
                For Each obj As Object In tsmi.DropDownItems
                    If Not obj.ToString = "System.Windows.Forms.ToolStripSeparator" Then
                        Dim chtsmi As ToolStripMenuItem = CType(obj, ToolStripMenuItem)
                        If chtsmi.Name = MenuItemName Then
                            chtsmi.Visible = Visible
                            Exit For
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'SetAllMenuItemsVisbleFalse

    Public Sub SetAllMenuItemsVisbleFalse()
        Try
            For Each tsmi As ToolStripMenuItem In mnsMain.Items
                tsmi.Visible = False
                For Each obj As Object In tsmi.DropDownItems
                    If Not obj.ToString = "System.Windows.Forms.ToolStripSeparator" Then
                        Dim chtsmi As ToolStripMenuItem = CType(obj, ToolStripMenuItem)
                        chtsmi.Visible = False
                    Else
                        Dim tss As ToolStripSeparator = CType(obj, ToolStripSeparator)
                        tss.Visible = False
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'SetAllMenuItemsVisbletrue

    Public Sub SetAllMenuItemsVisbleTrue()
        Try
            For Each tsmi As ToolStripMenuItem In mnsMain.Items
                tsmi.Visible = True
                For Each obj As Object In tsmi.DropDownItems
                    If Not obj.ToString = "System.Windows.Forms.ToolStripSeparator" Then
                        Dim chtsmi As ToolStripMenuItem = CType(obj, ToolStripMenuItem)
                        chtsmi.Visible = True
                    Else
                        Dim tss As ToolStripSeparator = CType(obj, ToolStripSeparator)
                        tss.Visible = True
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function isMenuItemEnabled(ByVal MenuItemName As String) As Boolean
        Try
            For Each tsmi As ToolStripMenuItem In mnsMain.Items
                If tsmi.Name = MenuItemName Then
                    If tsmi.Enabled Then
                        Return True
                    Else
                        Return False
                    End If
                    Exit For
                End If
                For Each obj As Object In tsmi.DropDownItems
                    If Not obj.ToString = "System.Windows.Forms.ToolStripSeparator" Then
                        Dim chtsmi As ToolStripMenuItem = CType(obj, ToolStripMenuItem)
                        If chtsmi.Name = MenuItemName Then
                            If chtsmi.Enabled Then
                                Return True
                            Else
                                Return False
                            End If
                            Exit For
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FUNCTION CALLED FROM MODULE1MAIN SCREEN
    ''' TRIGGERES ONCE THE LINKS(NEW/EDITS) ARE SELECTED FROM THE MAIN SCREEN
    ''' IT FIRES THE EVENT CALLAED IN MODULE1PBP SCREEN
    ''' </summary>
    ''' <param name="StrEvent"></param>
    ''' <remarks></remarks>
    Public Sub FiringEventsFromMain(ByVal StrEvent As String, ByVal StrLinkSelected As String, Optional ByVal Sequence_Number As Decimal = 0)
        Try
            m_objModule1PBP.FiringEventsFromMain(StrEvent, StrLinkSelected, Sequence_Number)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'ONCE OPTICAL EVENT IS SELECTED/SAVED, SHOW THE EVENT TO USER
    Public Sub DisplayOpticalData()
        Try
            If m_objGameDetails.ModuleID = 1 Then
                If m_objGameDetails.OpticalEvent > 0 Then
                    m_objModule1PBP.GetOpticalPBPEvent(m_objGameDetails.OpticalEvent)
                    m_objSportVU.UpdateProcessedOpticalPBP(m_objGameDetails.GameCode, CStr(m_objGameDetails.OpticalEvent), CChar("S"))
                ElseIf Not m_objGameDetails.OpticalPBPAccepted Is Nothing Then 'SOME EVENTS ARE AUTO SAVED
                    If m_objGameDetails.OpticalPBPAccepted.Tables(0).Rows.Count > 0 Then
                        m_objModule1PBP.DisplayInListView(m_objGameDetails.OpticalPBPAccepted, True)
                    End If
                End If

                If Not m_objGameDetails.OpticalPBPAccepted Is Nothing Then
                    m_objGameDetails.OpticalPBPAccepted.Clear()
                End If

                If m_objGameDetails.OpticalEvent = 0 Then
                    m_objModule1PBP.DoCancel()
                End If

                'ElseIf m_objGameDetails.ModuleID = 3 Then
                '    Me.Focus()
                '    If m_objGameDetails.OpticalTouchEvent > 0 Then
                '        m_objTouches.GetOpticalTouchEvent(m_objGameDetails.OpticalTouchEvent)
                '        m_objSportVU.UpdateProcessedOpticalTouch(m_objGameDetails.GameCode, CStr(m_objGameDetails.OpticalTouchEvent))
                '        'FETCH OPTICAL DATA AGAIN
                '        m_objTouches.GetOpticalData()
                '    ElseIf Not m_objGameDetails.OpticalTouchAccepted Is Nothing Then 'SOME EVENTS ARE AUTO SAVED
                '        If m_objGameDetails.OpticalTouchAccepted.Tables(0).Rows.Count > 0 Then
                '            m_objTouches.DisplayTouches()
                '            m_objTouches.ClearTouchLabels()
                '            DisableOpticalFeed()
                '            'm_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-Off.gif")
                '        Else
                '            'FETCH OPTICAL DATA AGAIN
                '            m_objTouches.GetOpticalData()
                '        End If
                '    ElseIf m_objGameDetails.OpticalTouchEvent = 0 Then
                '        'FETCH OPTICAL DATA AGAIN
                '        m_objTouches.GetOpticalData()
                '    End If

                '    If Not m_objGameDetails.OpticalTouchAccepted Is Nothing Then
                '        m_objGameDetails.OpticalTouchAccepted.Clear()
                '    End If

            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.BringToFront()
            Me.Focus()
        End Try

    End Sub

    Public Sub ChangeTheme(ByVal EditMode As Boolean)
        Try
            If EditMode Then
                Me.BackColor = Color.LightGoldenrodYellow
                picReporterBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                pnlReporter.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                picButtonBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                pnlFooterInfo.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\LinesGold.gif")
                picEditMode.Visible = True
                picEditMode.Top = lblReporter.Top
                picEditMode.Left = lblReporter.Left - picEditMode.Width
                If m_objGameDetails.IsEditMode = True Then
                    picEditMode.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\Edit.gif")
                ElseIf m_objGameDetails.IsInsertMode = True Then
                    picEditMode.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\Insert.gif")
                ElseIf m_objGameDetails.IsReplaceMode = True Then
                    picEditMode.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\Replace.gif")
                End If

            Else
                Me.BackColor = Color.WhiteSmoke
                picReporterBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                pnlReporter.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                picButtonBar.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                pnlFooterInfo.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\lines.gif")
                picEditMode.Visible = False
                picEditMode.Top = lblReporter.Top
                picEditMode.Left = lblReporter.Left - picEditMode.Width
                picEditMode.Image = Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF_SEC + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF_SEC + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA_SEC + ElapsedTime).ToString), False)
                End If
            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer) As Integer
        Try
            If m_objGameDetails.CurrentPeriod = 1 Then
                Return CurrentElapsedTime
            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF_SEC
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF_SEC
            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA_SEC
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ConvertAndSetClock()
        Try
            'Get the current Main Clock value
            Dim strCurrTime() As String
            Dim strTime As String = "00:00"
            Dim intCurrTime As Integer

            'Check Default Clock value and call Before or After Function
            If (m_objGameDetails.ModuleID = PBP_MODULE) Or (m_objGameDetails.ModuleID = TOUCHES_MODULE) Then
                If m_objGameDetails.IsDefaultGameClock Then
                    If m_objGameDetails.SerialNo = 1 Then
                        strTime = CalculateTimeElapseAfter(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)), m_objGameDetails.CurrentPeriod)
                        strCurrTime = strTime.Split(CChar(":"))
                        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                    End If
                Else
                    intCurrTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
                    strTime = clsUtility.ConvertSecondToMinute(intCurrTime, False)
                    strCurrTime = strTime.Split(CChar(":"))
                    UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                End If
                m_objModule1Main.ClearListviews()
                TimerRefreshPzPbp(True)

            End If
            'If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
            '    If m_objGameDetails.IsDefaultGameClock Then
            '        strTime = CalculateTimeElapseAfter(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)), m_objGameDetails.CurrentPeriod)
            '        strCurrTime = strTime.Split(CChar(":"))
            '        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
            '    Else
            '        intCurrTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
            '        strTime = clsUtility.ConvertSecondToMinute(intCurrTime, False)
            '        strCurrTime = strTime.Split(CChar(":"))
            '        UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
            '    End If
            'End If
            If m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                If m_objGameDetails.IsDefaultGameClock Then
                    strTime = CalculateTimeElapseAfter(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)), m_objGameDetails.CurrentPeriod)
                    strCurrTime = strTime.Split(CChar(":"))
                    UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                Else
                    intCurrTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
                    strTime = clsUtility.ConvertSecondToMinute(intCurrTime, False)
                    strCurrTime = strTime.Split(CChar(":"))
                    UdcRunningClock1.URCSetTime(CShort(strCurrTime(0)), CShort(strCurrTime(1)), True)
                End If
            End If
            'If UdcRunningClock1.URCIsRunning = False And btnClock.Enabled = True Then
            '    UdcRunningClock1.URCToggle()
            'End If
            'Dim dsPBP As DataSet
            ''dsPBP = m_objModule1PBP.getGetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            'm_objModule1PBP.DisplayInListView(dsPBP)
            ''Set time to Main Clock
            If Not m_objModule1PBP Is Nothing Then
                m_objModule1PBP.DisplayPBP()
            ElseIf Not m_objModule1Main Is Nothing Then
                m_objModule1Main.FetchMainScreenPBPEvents()
            ElseIf Not m_objTouches Is Nothing Then
                'm_objTouches.DisplayTouches()
                m_objTouches.DisplayTouchesData(0)
            ElseIf Not m_objCommentary Is Nothing Then
                m_objCommentary.DisplayComments()
            ElseIf Not m_objModule2Score.btnCancel Is Nothing Then
                m_objModule2Score.FillListView()
            End If
            'Call Display Function for PBP or Main Appropriately
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EnableOpticalFeed()
        picOpticalFeed.Enabled = True
        picOpticalFeed.ImageLocation = m_objUtility.ReturnImage("CameraBlink.gif")
        m_objGameDetails.IsSportVUAvailable = True
    End Sub

    Public Sub DisableOpticalFeed()
        picOpticalFeed.Enabled = False
        picOpticalFeed.ImageLocation = m_objUtility.ReturnImage("Camera.gif")
    End Sub

    Private Function IsPBPEntryAllowed() As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                Return True
            Else
                If m_objGameDetails.IsPrimaryReporterDown Then
                    MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                End If
                If m_objGameDetails.AssistersLastEntryStatus = False And m_objGameDetails.ModuleID = 1 Then
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Return False
                Else
                    Return True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsClockEntryAllowed() As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objGameDetails.IsInsertMode Or m_objGameDetails.IsReplaceMode Then
                MessageDialog.Show(strmessage6, strmessage34, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            ElseIf m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                Return True
            Else
                If m_objGameDetails.ModuleID <> 3 Then
                    If m_objGameDetails.IsPrimaryReporterDown Then
                        MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Return True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsGameAbandoned() As Boolean
        Try
            Dim abanData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, clsGameDetails.ABANDONED, m_objGameDetails.ModuleID)

            If abanData IsNot Nothing Then
                If abanData.Tables.Count > 0 Then
                    If abanData.Tables(0).Rows.Count > 0 Then
                        MessageDialog.Show(strmessage12, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Return True
                    End If
                End If
            End If

            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsPeriodMismatch() As Boolean
        Try
            Dim dsEndPeriodCount As DataSet
            dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)

            If dsEndPeriodCount IsNot Nothing Then
                If dsEndPeriodCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dsEndPeriodCount.Tables(0).Rows(dsEndPeriodCount.Tables(0).Rows.Count - 1).Item("PeriodCount")) <> m_objGameDetails.CurrentPeriod Then
                        Return True
                    End If
                End If
            End If

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsPZPeriodMismatch() As Boolean
        Try
            Dim dsEndPeriodCount As DataSet
            dsEndPeriodCount = m_objGeneral.PzGetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)

            If dsEndPeriodCount IsNot Nothing Then
                If dsEndPeriodCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dsEndPeriodCount.Tables(0).Rows(dsEndPeriodCount.Tables(0).Rows.Count - 1).Item("PeriodCount")) <> m_objGameDetails.CurrentPeriod Then
                        Return True
                    End If
                End If
            End If

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub setStartClock()
        Try
            If m_objGameDetails.CurrentPeriod = 0 And UdcRunningClock1.URCIsRunning Then 'special case to handle clock started but home start not entered
                Exit Sub
            Else
                If m_objGameDetails.IsDefaultGameClock = False Then
                    UdcRunningClock1.URCReset()
                    UdcRunningClock1.URCSetTime(0, 0)
                Else
                    If m_objGameDetails.CurrentPeriod = 2 Then
                        UdcRunningClock1.URCSetTime(45, 0)
                    ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                        UdcRunningClock1.URCSetTime(90, 0)
                    ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                        UdcRunningClock1.URCSetTime(105, 0)
                        'ElseIf m_objGameDetails.CurrentPeriod = 5 Then
                        '    UdcRunningClock1.URCSetTime(120, 0)
                        '    SetClockControl(False)
                        '    If Not m_objModule1PBP Is Nothing Then
                        '        m_objModule1PBP.DisableAllControls(False)
                        '    End If
                    End If
                End If
                UdcRunningClock1.URCToggle()
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub tmrRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrRefresh.Tick
        Dim dsRefreshData As New DataSet
        Try
            tmrRefresh.Stop()

            If m_objGameDetails.ModuleID = 1 Then
                If m_objGameDetails.IsEditMode = False And m_objGameDetails.IsReplaceMode = False Then
                    dsRefreshData.Clear()
                    dsRefreshData = m_objGeneral.RefreshGameData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID, False, m_objGameDetails.languageid)
                    If dsRefreshData.Tables(13).Rows.Count > 0 Then
                        'If Not m_objTouches Is Nothing Then
                        If CInt(dsRefreshData.Tables(13).Rows(0).Item("LINEUPCNT")) = 2 Then
                            SetMarqueeText("")

                        Else
                            If m_objGameDetails.IsLineupChecked = False Then
                                m_objGameDetails.IsLineupChecked = True
                                SetMarqueeText("Lineup has NOT been sent to the client. Please do this immediately!")
                            End If
                            'SetMarqueeText("Lineup been sent to the client. Please do this immediately!")
                        End If
                        'End If
                    End If
                    '' IF ASSISTER ASSIGNED THEN ONLY REFRESH SHOULD HAPPEN
                    DisplayReportersStatus(dsRefreshData)
                    RefreshData(dsRefreshData)

                Else
                    dsRefreshData.Clear()
                    ' dsRefreshData = m_objGeneral.RefreshGameData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID, True, m_objGameDetails.languageid)
                    ' DisplayReportersStatus(dsRefreshData)
                End If
            ElseIf m_objGameDetails.ModuleID = 3 Then 'Prozone Soccer Coverage level 6
                TimerRefreshPzPbp()
            ElseIf m_objGameDetails.ModuleID = 2 Then
                dsRefreshData.Clear()
                If m_objGameDetails.IsReporterRemoved = False Then
                    Dim dsReporterAssignment As DataSet
                    'rm 8099 - June 10 returns 2 datasets 1-retuening if the reporter id exists and another checking the active game in LCL current game
                    dsReporterAssignment = m_objGeneral.getAssignmentForCommentary(m_objGameDetails.GameCode, m_objclsLoginDetails.UserId, m_objGameDetails.ModuleID, m_objGameDetails.SerialNo)

                    If (dsReporterAssignment.Tables(0).Rows.Count = 0) Then 'And (dsReporterAssignment.Tables(1).Rows.Count > 0) Then
                        MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_objGameDetails.IsReporterRemoved = True
                        SetAllMenuItemsVisbleFalse()
                        Dim dsGameData As DataSet
                        dsGameData = m_objGeneral.DeleteCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objclsLoginDetails.UserId)
                        If dsGameData.Tables(0).Rows.Count > 0 Then
                            If m_objModule1PBP Is Nothing Then
                                If m_objGameDetails.CoverageLevel <> 1 Then
                                    m_objModule1Main.ClearMainScreenControls()
                                End If
                                'm_objModule1Main.btnSwitchGames_Click(sender, e)
                            Else
                                m_objModule1PBP.DisableAllControls(False)
                                m_objModule1PBP.btnBookingAway.Enabled = False
                                m_objModule1PBP.btnBookingHome.Enabled = False
                                m_objModule1PBP.btnSubstituteAway.Enabled = False
                                m_objModule1PBP.btnSubstituteHome.Enabled = False
                                m_objModule1PBP.btnTime.Enabled = False
                                m_objModule1PBP.btnCancel.Enabled = False
                                'm_objModule1PBP.btnSwitchGames_Click(sender, e)
                            End If
                            Me.Close()
                        Else
                            Me.Close()
                        End If
                        ''Delete this game from lcl_current game table
                        ''open OPEN screen to choose other games
                        Me.Close()
                    End If

                    dsRefreshData = m_objGeneral.RefreshGameData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID, False, m_objGameDetails.languageid)
                    If dsRefreshData.Tables(11).Rows.Count > 0 And m_objGameDetails.CoverageLevel > 2 Then
                        'If Not m_objTouches Is Nothing Then
                        If CInt(dsRefreshData.Tables(11).Rows(0).Item("LINEUPCNT")) = 2 Then
                            SetMarqueeText("")

                        Else
                            If m_objGameDetails.IsLineupChecked = False Then
                                m_objGameDetails.IsLineupChecked = True
                                SetMarqueeText("Lineup has NOT been sent to the client. Please do this immediately!")
                            End If
                            'SetMarqueeText("Lineup been sent to the client. Please do this immediately!")
                        End If
                        'End If
                    End If
                End If
            ElseIf m_objGameDetails.ModuleID = 4 Then

                Dim dsReporterAssignment As DataSet
                dsReporterAssignment = m_objGeneral.getAssignmentForCommentary(m_objGameDetails.GameCode, m_objclsLoginDetails.UserId, m_objGameDetails.ModuleID, m_objGameDetails.SerialNo)

                If dsReporterAssignment.Tables(0).Rows.Count = 0 Then
                    MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    m_objGameDetails.IsReporterRemoved = True
                    Me.Close()
                End If

                'If gameCount = 0 Then
                '    MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                '    Me.Close()
                'End If
            End If

            'CHECKS CONNECTION TO STATS
            If My.Computer.Network.IsAvailable Then
                lblStatsConnectionStatus.Text = ": Available"
                lblStatsConnectionStatus.ForeColor = Color.LimeGreen
            Else
                lblStatsConnectionStatus.Text = ": Not Available"
                lblStatsConnectionStatus.ForeColor = Color.Crimson
            End If

            If CInt(m_objGameDetails.languageid) <> 1 Then
                lblStatsConnectionStatus.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblStatsConnectionStatus.Text)
            End If
            ''TOSOCRS-159
            If m_objGameDetails.IsReporterRemoved = False Then

            End If
            If (Process.GetProcessesByName("STATS.SoccerDataSync").Length <> 1) Then
                LoadSoccerDataSync()
            End If

        Catch ex As Exception
            'MessageDialog.Show(ex, Me.Text)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "*************ERROR OCCURED ON REFRESH(MAIN) " & ex.Message & vbNewLine, 1, 1)
        Finally
            tmrRefresh.Start()
            dsRefreshData = Nothing
        End Try
    End Sub

    'CODE ADDED TO SHOW PRIMARY/ASSISTER REPORTER ONLINE/OFFLINE STATUS AND ASSISTER'S LAST ENTRY STATUS
    Private Sub DisplayReportersStatus(ByVal RptrInfo As DataSet)
        Try
            'PRIMARY/ASSISTER REPORTER'S (ONLINE/OFFLINE) STATUS
            If RptrInfo.Tables(8).Rows.Count > 0 Then
                If m_objGameDetails.ModuleID = 3 Then
                    picRptr1.ImageLocation = GetReporterOnOffImage(1, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_1")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_1"))))
                    picRptr2.ImageLocation = GetReporterOnOffImage(2, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_2")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_2"))))
                    picRptr3.ImageLocation = GetReporterOnOffImage(3, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_3")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_3"))))
                    picRptr4.ImageLocation = GetReporterOnOffImage(4, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_4")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_4"))))
                ElseIf m_objGameDetails.ModuleID = 1 Then
                    picRptr1.ImageLocation = GetReporterOnOffImage(1, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_1")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_1"))))
                    picRptr2.ImageLocation = GetReporterOnOffImage(2, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_2")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_2"))))
                    picRptr3.ImageLocation = GetReporterOnOffImage(3, CChar(IIf(IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_3")), "N", RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_3"))))
                End If

                'IF CURRENT REPORTER IS ASSISTER THEN CHECK PRIMARY REPORTER IS OFFLINE OR NOT
                If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                    If IsDBNull(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_1")) = False Then
                        'SHOW A MESSAGE WHEN PRIMARY REPORTER IS DOWN
                        If CChar(RptrInfo.Tables(8).Rows(0)("REPORTER_STATUS_1")) = "N" Then
                            m_objGameDetails.IsPrimaryReporterDown = True
                        Else
                            m_objGameDetails.IsPrimaryReporterDown = False
                        End If
                    End If
                End If
            End If

            'IF CURRENT REPORTER IS AN ASSISTER THEN CHECK HIS LAST ENTRY STATUS(ACCEPTED/REJECTED BY PRIMARY REPORTER)
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                If RptrInfo.Tables(9).Rows.Count > 0 Then
                    If IsDBNull(RptrInfo.Tables(9).Rows(0)("LAST_ENTRY_STATUS")) = False Then
                        If CChar(RptrInfo.Tables(9).Rows(0)("LAST_ENTRY_STATUS")) = "N" Then
                            'N --> LAST ENTRY STILL IN PROCESS
                            m_objGameDetails.AssistersLastEntryStatus = False
                        ElseIf CChar(RptrInfo.Tables(9).Rows(0)("LAST_ENTRY_STATUS")) = "D" Then
                            'D --> LAST ENTRY IS DECLINED/REJECTED BY PRIMARY REPORTER
                            If m_objGameDetails.ModuleID = 3 Then
                                m_objTouches.GetTouches()
                                m_objTouches.DisplayTouchesData(0)
                            End If
                            MessageDialog.Show(strmessage11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            If m_objKeyMoments IsNot Nothing Then
                                Dim dsKeyMomentData As DataSet
                                dsKeyMomentData = m_objGeneral.GetKeymomentsData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                                m_objKeyMoments.DisplayList()
                                ''SHRAVANI - DISPLAYING OF KEYMOMENTS DATA 4TH AUG 2010
                                m_objKeyMoments.DisplayKeyMoments(dsKeyMomentData)
                            End If
                            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, strmessage11, 1, 1)
                            m_objGameDetails.AssistersLastEntryStatus = True
                            m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, False)
                            ''ONCE ASSISTER'S ENTRY IS REJECTED, RESTART SHOULD HAPPEN TO REFRESH(SCORE,POSSESSION AND SET STATE) THE SCREEN
                            'RestartExistingGame("RESTART")
                        ElseIf CChar(RptrInfo.Tables(9).Rows(0)("LAST_ENTRY_STATUS")) = "A" Then
                            'A --> LAST ENTRY IS ACCEPTED BY PRIMARY REPORTER
                            m_objGameDetails.AssistersLastEntryStatus = True
                            m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, False)
                        End If
                    End If
                End If
            End If

            'CHECK CURRENT REPORTER HAS REMOVED FROM THE SCORING GAME
            If m_objGameDetails.ReporterRole.ToUpper <> "OPS" Then
                If RptrInfo.Tables(10).Rows.Count > 0 Then
                    If CInt(RptrInfo.Tables(10).Rows(0)("GAMECOUNT")) = 0 Then 'REMOVED
                        MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_objGameDetails.IsReporterRemoved = True
                        'TOSOCRS-159
                        m_formClosed = True
                        Me.Close()
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub RefreshData(ByVal RefreshData As DataSet)

        Try
            Dim dsSportVUFeed As New DataSet
            'dsSportVUFeed= m_objGeneral.
            If RefreshData.Tables.Count > 0 Then
                'refresh the module1main screens with players,game setup data and managers
                If m_objModule1PBP Is Nothing Then
                    If RefreshData.Tables(1).Rows.Count > 0 Then
                        m_objModule1Main.FillMainMatchPlayers()
                    End If
                    'refresh game setup info
                    If RefreshData.Tables(2).Rows.Count > 0 Then
                        m_objModule1Main.FillMainMatchOfficials()
                    End If
                    'refresh managers in main match screen
                    If RefreshData.Tables(3).Rows.Count > 0 Then
                        m_objModule1Main.FillMainMatchManagers()
                    End If
                Else
                    m_objModule1PBP.getMatchInfo()
                End If

                If RefreshData.Tables(0).Rows.Count > 0 Then
                    'if any record needs to be refreshed (ie if Refreshed filed in PBP is "N")
                    'refresh PBP screen
                    Dim startEvent As Boolean = False
                    If m_objModule1Main Is Nothing Then
                        'list view refresh
                        m_objModule1PBP.RefreshGameData(RefreshData.Copy)
                        'refresh for start/end period
                        If RefreshData.Tables(5).Rows.Count > 0 Then
                            If CInt(RefreshData.Tables(5).Rows(0).Item("EVENT_CODE_ID")) = clsGameDetails.STARTPERIOD Then
                                pnlEntryForm.Enabled = True
                                m_objModule1PBP.DisableAllControls(True)
                                startEvent = True
                            ElseIf CInt(RefreshData.Tables(5).Rows(0).Item("EVENT_CODE_ID")) = clsGameDetails.ENDPERIOD Then
                                m_objModule1PBP.DisableAllControls(False)
                            End If
                        End If
                    End If
                    'refresh module1main screen
                    If m_objModule1PBP Is Nothing Then
                        m_objModule1Main.RefreshData()
                    End If
                    'set menu itemns and scores
                    SetScores() ' Changed the order by ARINDAM on 11-OCT-09
                    SetMenuItems()
                    SyncGameClock()
                    ''
                    If startEvent Then
                        If m_objGameDetails.CurrentPeriod > 1 Then
                            If Not m_objModule1PBP Is Nothing Then
                                If m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True Then
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                                    m_objGameDetails.isHomeDirectionLeft = False
                                Else
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                                    m_objGameDetails.isHomeDirectionLeft = True
                                End If
                            ElseIf Not m_objTouches Is Nothing Then
                                If m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True Then
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = False
                                    'm_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = m_objGameDetails.IsHomeTeamOnLeft

                                    m_objGameDetails.isHomeDirectionLeft = False
                                Else
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True
                                    m_objGameDetails.isHomeDirectionLeft = True
                                End If
                            End If
                        ElseIf m_objGameDetails.CurrentPeriod = 1 Then
                            lblPeriod.Text = FIRST_HALF_DESC
                            If m_objGameDetails.IsHomeTeamOnLeft = True Then
                                If Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = True
                                ElseIf Not m_objTouches Is Nothing Then
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = True
                                End If
                                m_objGameDetails.isHomeDirectionLeft = True
                            Else
                                If Not m_objModule1PBP Is Nothing Then
                                    m_objModule1PBP.UdcSoccerField1.USFHomeTeamOnLeft = False
                                ElseIf Not m_objTouches Is Nothing Then
                                    m_objTouches.UdcSoccerField1.USFHomeTeamOnLeft = False
                                End If
                                m_objGameDetails.isHomeDirectionLeft = False
                            End If
                        End If
                        startEvent = False
                    End If
                    ''
                    If m_objModule1Main Is Nothing Then
                        Dim dsEndPeriodCount As DataSet
                        dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.ENDPERIOD)

                        If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) = m_objGameDetails.CurrentPeriod And m_objGameDetails.CurrentPeriod <> 0 And m_objGameDetails.IsEditMode = False And m_objGameDetails.IsInsertMode = False And m_objGameDetails.IsReplaceMode = False Then ' may be we have entered booking/subs
                            If m_objModule1PBP.dblstEventDetails01.Visible = False Then
                                m_objModule1PBP.DisableAllControls(False)
                            End If

                        End If
                    End If
                End If

                'SETTING A2'S CLCOK
                If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                    Dim ElapsedTime As Integer
                    Dim CurrentTIme As Integer
                    Dim StrTime As String = "", strNewTime() As String
                    If RefreshData.Tables(4).Rows.Count > 0 Then
                        Dim DRS() As DataRow = RefreshData.Tables(4).Select("REPORTER_ROLE <> '" & m_objGameDetails.ReporterRole & "'")
                        If DRS.Length > 0 Then
                            For Each drPBP As DataRow In RefreshData.Tables(4).Rows
                                If CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTOP And CDec(drPBP.Item("SEQUENCE_NUMBER")) = -1 Then
                                    'DELETE OF END PERIOD...
                                    ElapsedTime = CInt(drPBP.Item("TIME_ELAPSED"))
                                    CurrentTIme = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                    strNewTime = StrTime.Split(CChar(":"))
                                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                    If UdcRunningClock1.URCIsRunning = False Then
                                        UdcRunningClock1.URCToggle()
                                    End If
                                ElseIf CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTART And CDec(drPBP.Item("SEQUENCE_NUMBER")) = -1 Then
                                    'DELETE OF START PERIOD
                                    Dim endperiodData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod, clsGameDetails.ENDPERIOD, m_objGameDetails.ModuleID)
                                    ElapsedTime = CInt(endperiodData.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                                    'ElapsedTime = m_objModule1PBP.GetMaxElapsedTime(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

                                    CurrentTIme = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                    strNewTime = StrTime.Split(CChar(":"))
                                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                    If UdcRunningClock1.URCIsRunning Then
                                        UdcRunningClock1.URCToggle()
                                    End If
                                Else
                                    ElapsedTime = CInt(drPBP.Item("TIME_ELAPSED"))
                                    CurrentTIme = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                    strNewTime = StrTime.Split(CChar(":"))
                                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                    'SETTING THE CLOCK EVENTS START,STOP,INC AND DEC

                                    If CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTART Then
                                        If UdcRunningClock1.URCIsRunning = False Then
                                            UdcRunningClock1.URCToggle()
                                        End If
                                    ElseIf CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTOP Then
                                        If UdcRunningClock1.URCIsRunning = True Then
                                            UdcRunningClock1.URCToggle()
                                        End If
                                    End If
                                End If
                            Next

                            If UdcRunningClock1.URCIsRunning Then
                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                            Else
                                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                            End If
                        End If
                        If RefreshData.Tables(0).Rows.Count > 0 Then
                            If CInt(RefreshData.Tables(0).Rows(0)("EVENT_CODE_ID")) = clsGameDetails.STARTPERIOD Or CInt(RefreshData.Tables(0).Rows(0)("EVENT_CODE_ID")) = clsGameDetails.ENDPERIOD Then
                                If CInt(RefreshData.Tables(0).Rows(0)("EVENT_CODE_ID")) = clsGameDetails.STARTPERIOD And Not m_objModule1Main Is Nothing Then
                                    m_objModule1Main.lklNewHomeBooking.Enabled = True
                                    m_objModule1Main.lklNewHomeGoal.Enabled = True
                                    m_objModule1Main.lklNewHomePenalty.Enabled = True
                                    m_objModule1Main.lklNewHomeSub.Enabled = True
                                    m_objModule1Main.lklNewVisitBooking.Enabled = True
                                    m_objModule1Main.lklNewVisitGoal.Enabled = True
                                    m_objModule1Main.lklNewVisitPenalty.Enabled = True
                                    m_objModule1Main.lklNewVisitSub.Enabled = True
                                End If
                            Else
                                If RefreshData.Tables(12).Rows.Count > 0 Then
                                    'SETTING LAST EVENT TIME
                                    If IsDBNull(RefreshData.Tables(12).Rows(0)("TIME_ELAPSED")) = False Then
                                        If UdcRunningClock1.URCIsRunning = True Then
                                            ElapsedTime = CInt(RefreshData.Tables(12).Rows(0)("TIME_ELAPSED"))
                                            StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                            strNewTime = StrTime.Split(CChar(":"))
                                            UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                        Else
                                            ElapsedTime = CInt(RefreshData.Tables(12).Rows(0)("TIME_ELAPSED"))
                                            StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                            strNewTime = StrTime.Split(CChar(":"))
                                            UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                        End If
                                    End If
                                End If
                            End If
                        End If

                    End If
                    'SETTING A1'S CLOCK
                ElseIf m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                    If RefreshData.Tables(4).Rows.Count > 0 And RefreshData.Tables(0).Rows.Count > 0 Then
                        If CInt(RefreshData.Tables(12).Rows(0)("TIME_ELAPSED")) > m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime) And CInt(RefreshData.Tables(12).Rows(0)("EDIT_UID")) = 0 Then
                            Dim ElapsedTime As Integer
                            Dim CurrentTIme As Integer
                            Dim StrTime As String = "", strNewTime() As String
                            Dim DRS() As DataRow = RefreshData.Tables(4).Select("REPORTER_ROLE <> '" & m_objGameDetails.ReporterRole & "' AND EVENT_CODE_ID NOT IN (56,57)")
                            If DRS.Length > 0 Then
                                For Each drPBP As DataRow In RefreshData.Tables(4).Rows
                                    ElapsedTime = CInt(drPBP.Item("TIME_ELAPSED"))
                                    CurrentTIme = CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime))
                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                    strNewTime = StrTime.Split(CChar(":"))

                                    'SETTING THE CLOCK EVENTS (START AND STOP)
                                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                    If CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTART Then
                                        If UdcRunningClock1.URCIsRunning = False Then
                                            UdcRunningClock1.URCToggle()
                                        End If
                                    ElseIf CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTOP Then
                                        If UdcRunningClock1.URCIsRunning = True Then
                                            UdcRunningClock1.URCToggle()
                                        End If
                                    End If
                                Next

                                'SETTING LAST EVENT TIME
                                If UdcRunningClock1.URCIsRunning = True Then
                                    ElapsedTime = CInt(RefreshData.Tables(12).Rows(0)("TIME_ELAPSED"))
                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                    strNewTime = StrTime.Split(CChar(":"))
                                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                Else
                                    ElapsedTime = CInt(RefreshData.Tables(12).Rows(0)("TIME_ELAPSED"))
                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                    strNewTime = StrTime.Split(CChar(":"))
                                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                                End If

                            End If
                        End If
                    End If
                End If
                If Not m_objModule1Main Is Nothing Then
                    m_objModule1Main.CheckforAbandoned()
                ElseIf Not m_objModule1PBP Is Nothing Then
                    m_objModule1PBP.CheckforAbandoned()
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SyncGameClock()
        Try
            Dim StrTime As String = "00:00"
            Dim strNewTime() As String
            Dim CurrPeriod As Integer = 0
            Dim dsLastEvent As DataSet
            Dim ElapsedTime As Integer

            dsLastEvent = m_objGeneral.GetLastClockEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)
            If dsLastEvent.Tables.Count > 1 Then
                If dsLastEvent.Tables(1).Rows.Count > 0 Then
                    If Not IsDBNull(dsLastEvent.Tables(1).Rows(0).Item("TIME_ELAPSED")) Then
                        ElapsedTime = CInt(dsLastEvent.Tables(1).Rows(0).Item("TIME_ELAPSED"))
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                        ElapsedTime = CInt(m_objUtility.ConvertMinuteToSecond(StrTime))
                    End If
                End If
            End If

            If m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime) < ElapsedTime Then
                strNewTime = StrTime.Split(CChar(":"))
                'SETTING THE CLOCK TIME
                UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)), True)
                If m_objGameDetails.CurrentPeriod = 0 Then
                    If UdcRunningClock1.URCIsRunning = True Then
                        UdcRunningClock1.URCToggle()
                    End If
                Else
                    If dsLastEvent.Tables(0).Rows.Count > 0 Then
                        If CInt(dsLastEvent.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = INTCLOCKSTART Then
                            If UdcRunningClock1.URCIsRunning = False Then
                                UdcRunningClock1.URCToggle()
                            End If
                        ElseIf CInt(dsLastEvent.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = INTCLOCKSTOP Then
                            If UdcRunningClock1.URCIsRunning = True Then
                                UdcRunningClock1.URCToggle()
                            End If
                        End If
                    End If
                End If
                If UdcRunningClock1.URCIsRunning Then
                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                Else
                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                End If
            ElseIf m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime) >= ElapsedTime Then
                If m_objGameDetails.CurrentPeriod = 0 Then
                    If UdcRunningClock1.URCIsRunning = True Then
                        UdcRunningClock1.URCToggle()
                    End If
                Else
                    If dsLastEvent.Tables(0).Rows.Count > 0 Then
                        If CInt(dsLastEvent.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = INTCLOCKSTART Then
                            If UdcRunningClock1.URCIsRunning = False Then
                                UdcRunningClock1.URCToggle()
                            End If
                        ElseIf CInt(dsLastEvent.Tables(0).Rows(0).Item("EVENT_CODE_ID")) = INTCLOCKSTOP Then
                            If UdcRunningClock1.URCIsRunning = True Then
                                UdcRunningClock1.URCToggle()
                            End If
                        End If
                    End If
                End If
                If UdcRunningClock1.URCIsRunning Then
                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                Else
                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                End If
            End If

            'if start and end period events are deleted...Set the last record time..
            If ElapsedTime = 2700 Then
                If (m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime) = ElapsedTime) Or (m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime) > ElapsedTime) Then
                    If dsLastEvent.Tables.Count > 1 Then
                        If dsLastEvent.Tables(2).Rows.Count > 0 Then
                            If Not IsDBNull(dsLastEvent.Tables(2).Rows(0).Item("TIME_ELAPSED")) Then
                                ElapsedTime = CInt(dsLastEvent.Tables(2).Rows(0).Item("TIME_ELAPSED"))
                                StrTime = CalculateTimeElapseAfter(ElapsedTime, m_objGameDetails.CurrentPeriod)
                                strNewTime = StrTime.Split(CChar(":"))
                                'SETTING THE CLOCK TIME
                                UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)), True)
                                If UdcRunningClock1.URCIsRunning Then
                                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                Else
                                    btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                End If
                                If CInt(dsLastEvent.Tables(2).Rows(0).Item("EVENT_CODE_ID")) = clsGameDetails.ENDPERIOD Or (isMenuItemEnabled("StartPeriodToolStripMenuItem") = True) Then
                                    If UdcRunningClock1.URCIsRunning = True Then
                                        UdcRunningClock1.URCToggle()
                                    End If
                                Else
                                    If UdcRunningClock1.URCIsRunning = False Then
                                        UdcRunningClock1.URCToggle()
                                    End If
                                End If

                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function GetReporterOnOffImage(ByVal RptrLevel As Integer, ByVal StatusFlag As Char) As String
        Try
            Dim strImagePath As String = Application.StartupPath
            strImagePath = strImagePath & "\Resources\"

            Select Case RptrLevel
                Case 1
                    If StatusFlag = "N" Then
                        Return strImagePath & "main-dim.gif"
                    Else
                        Return strImagePath & "main.gif"
                    End If
                Case 2, 3, 4
                    If StatusFlag = "N" Then
                        Return strImagePath & "assist-dim.gif"
                    Else
                        Return strImagePath & "assist.gif"
                    End If
                Case Else
                    Return strImagePath & "assist.gif"
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Sub StartSystemTray()
    '    Try
    '        If v_pStart.HasExited Then
    '            v_pStart.Start()
    '            'TOSOCRS-33-SOC - Manual restart of datasync component in reporter software
    '            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GameSelected, "DataSync Loaded", 1, 0)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub enterTimeEvent()
        Try
            'Arindam 06-12-2011 Added code to enter new TIME event every 10 seconds: applicable only for running clock T4 and higher...
            If m_objGameDetails.CoverageLevel >= 4 And UdcRunningClock1.URCIsRunning Then
                If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                    Dim getCurrentTIme As Integer
                    Dim dsLastClock As DataSet
                    dsLastClock = m_objGeneral.GetLastClockEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod)
                    getCurrentTIme = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)))
                    If dsLastClock.Tables(2).Rows.Count > 0 Then
                        If getCurrentTIme <> 0 And getCurrentTIme > CInt(dsLastClock.Tables(2).Rows(0).Item("TIME_ELAPSED")) Then
                            If CInt(getCurrentTIme Mod 10) = 0 Then
                                If Not m_objModule1PBP Is Nothing Then
                                    If m_objModule1PBP.dblstEventDetails01.Visible = True Or m_objGameDetails.PBP.Tables(0).Rows.Count > 0 Then
                                        'User entry going on, no need to send TIME event!
                                    Else
                                        m_objModule1PBP.AddPBPGeneralEvents(clsGameDetails.TIME, "")
                                    End If
                                Else
                                    m_objModule1Main.InsertPlayByPlayData(clsGameDetails.TIME, "")
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetFeedCoverageInfo(ByVal GameCode As Integer, ByVal ReporterID As Integer, ByVal intFeedNumber As Integer)
        Try
            Dim DsFeedCoverageInfo As New DataSet
            'DsFeedCoverageInfo = m_objLogin.GetFeedCoverageInfo(GameCode, ReporterID, intFeedNumber)
            If DsFeedCoverageInfo.Tables.Count > 0 Then
                'm_objGameDetails.FeedNumber = CInt(DsFeedCoverageInfo.Tables(0).Rows(0).Item("FEED_NUMBER"))
                m_objGameDetails.CoverageLevel = CInt(DsFeedCoverageInfo.Tables(0).Rows(0).Item("COVERAGELEVEL"))
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Private Sub DisplayScreensForOPS()
        Try
            m_objGameDetails.PBP = AddPBPColumns()
            SetMainFormMenu()
            SetMainFormSize()
            SetPeriod()
            SetChildForm()
            InitializeClock()
            FillFormControls()

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                EnableMenuItem("CommentToolStripMenuItem", True)
            Else
                EnableMenuItem("CommentToolStripMenuItem", False)
            End If

            If m_objGameDetails.IsDemoGameSelected Then
                'Label15.Visible = False
                lblStatsConnection.Visible = False
                Label17.Visible = False
                lblStatsConnectionStatus.Visible = False
                'lblNextGameTime.Visible = False
                lblSportVu.Visible = False
            End If
            'If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
            '    LoadSoccerDataSync()
            'End If

            'T1 WORKFLOW
            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                If m_objGameDetails.IsRestartGame = True Then
                    If Not m_objModule1Main Is Nothing Then
                        m_objModule1Main.RestartExistingGame()
                        pnlEntryForm.Enabled = True
                        btnClock.Enabled = True
                        m_objGameDetails.IsRestartGame = False
                        m_objGameDetails.RestartData.Tables.Clear()
                    End If
                End If
            End If

            If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                'REFRESHING THE PBP EVENTS IN THE LISTBOXES
                m_objModule1Main.FetchMainScreenPBPEvents()
                pnlEntryForm.Enabled = True
            End If

            'T1 WORKFLOW FOR COMMENTARY
            If m_objGameDetails.ModuleID = COMMENTARY_MODULE Then
                If m_objGameDetails.IsRestartGame = True Then
                    m_objCommentary.RestartExistingGame()
                    pnlEntryForm.Enabled = True
                    btnClock.Enabled = True
                    ResetMenuItems()
                    m_objGameDetails.IsRestartGame = False
                    m_objGameDetails.RestartData.Tables.Clear()
                End If
            End If

            ''T1 WORKFLOW FOR TOUCHES
            'If m_objGameDetails.ModuleID = TOUCHES_MODULE Then
            '    If m_objGameDetails.IsRestartGame = True Then
            '        m_objTouches.RestartExistingGame()
            '        pnlEntryForm.Enabled = True
            '        btnClock.Enabled = True
            '        m_objGameDetails.IsRestartGame = False
            '        m_objGameDetails.RestartData.Tables.Clear()
            '        If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
            '            SetHeader2()
            '        End If
            '        If m_objGameDetails.CurrentPeriod = 0 Then
            '            m_objTouches.pnlTouchTypes.Enabled = False
            '            m_objTouches.UdcSoccerField1.Enabled = False
            '            m_objTouches.btnSave.Enabled = False
            '            m_objTouches.btnClear.Enabled = False
            '        Else
            '            m_objTouches.pnlTouchTypes.Enabled = True
            '            m_objTouches.UdcSoccerField1.Enabled = True
            '            m_objTouches.btnSave.Enabled = True
            '            m_objTouches.btnClear.Enabled = True
            '        End If
            '    Else
            '        m_objTouches.GetTouches(3)
            '    End If
            '    SetPeriod()
            '    SetChildForm()
            '    SetMainFormSize()
            'End If

            If (m_objGameDetails.CoverageLevel <> 3) Then

                EnableMenuItem("InputtingteamstatsToolStripMenuItem ", False)
            Else
                EnableMenuItem("InputtingteamstatsToolStripMenuItem ", True)

            End If

            If m_objGameDetails.ModuleID = PBP_MODULE Or m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE Then
                If Not m_objModule1Main Is Nothing Then
                    m_objModule1Main.ArrangeListViews()
                End If
            End If

            If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                picRptr1.Visible = False
                picRptr2.Visible = False
                picRptr3.Visible = False
                picRptr4.Visible = False
                picRptr5.Visible = False
                picRptr6.Visible = False
            End If

            If m_objGameDetails.IsSportVUAvailable = True And m_objGameDetails.ModuleID = PBP_MODULE Then
                'If Not m_objModule1PBP Is Nothing Then
                m_objModule1PBP.picOpticalFeed.Visible = True
                m_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-OFF.gif")
                'End If
            Else
                If Not m_objModule1PBP Is Nothing Then
                    m_objModule1PBP.picOpticalFeed.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub SetTeamColor(ByVal HomeTeam As Boolean, ByVal TeamColor As System.Drawing.Color)
        Try
            If HomeTeam Then
                picHomeColorTop.BackColor = TeamColor
                picHomeColorBottom.BackColor = TeamColor
            Else
                picAwayColorTop.BackColor = TeamColor
                picAwayColorBottom.BackColor = TeamColor
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub DisplaySportVUStatus(ByVal RefreshData As DataSet)
        Try
            Dim intGamecnt As New Integer
            intGamecnt = m_objGeneral.GetSportVUFeed(m_objGameDetails.GameCode)
            Dim SportVUStatus As String = "N"
            If RefreshData.Tables.Count > 0 Then
                'CHECK SPORTVU FEED AVAILABLE
                If RefreshData.Tables(6).Rows.Count > 0 Then
                    If RefreshData.Tables(6).Rows(0)("SPORTVU_STATUS").ToString = "Y" Then
                        m_objGameDetails.IsSportVUAvailable = True
                        lblSportVu.Text = ": Connected"
                        lblSportVu.ForeColor = Color.LimeGreen
                    End If
                End If

                If RefreshData.Tables(6).Rows.Count > 0 Then
                    If RefreshData.Tables(6).Rows(0)("SPORTVU_STATUS").ToString = "Y" Then
                        SportVUStatus = "Y"
                    Else
                        SportVUStatus = "N"
                    End If
                End If

                If intGamecnt = 1 And SportVUStatus = "Y" Then
                    m_objGameDetails.IsSportVUAvailable = True
                    EnableMenuItem("SportVUToolStripMenuItem", True)
                    lblSportVu.Text = ": Connected"
                    lblSportVu.ForeColor = Color.LimeGreen
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.picOpticalFeed.Visible = True
                        'm_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-OFF.gif")
                    End If
                ElseIf intGamecnt = 1 Then
                    lblSportVu.Text = ": Not Conncted"
                    EnableMenuItem("SportVUToolStripMenuItem", False)
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.picOpticalFeed.Visible = True
                        m_objModule1PBP.picOpticalFeed.Image = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\SPORTVU-OFF.gif")
                    End If
                Else
                    m_objGameDetails.IsSportVUAvailable = False
                    lblSportVu.Text = ": Not Supported"
                    EnableMenuItem("SportVUToolStripMenuItem", False)
                    If Not m_objModule1PBP Is Nothing Then
                        m_objModule1PBP.picOpticalFeed.Visible = False
                    End If
                End If
            End If
            If CInt(m_objGameDetails.languageid) <> 1 Then
                lblSportVu.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblSportVu.Text)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub GetPenaltyShootoutScore()
        Try
            Dim dsScores As DataSet
            dsScores = m_objGeneral.GetPenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            Dim tempHomeScore As Integer = 0
            Dim tempAwayScore As Integer = 0

            If dsScores.Tables.Count > 0 Then

                If dsScores.Tables(2).Rows.Count > 0 Then
                    If CInt(dsScores.Tables(2).Rows(dsScores.Tables(2).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                        m_objGameDetails.HomeScore = CInt(dsScores.Tables(2).Rows(dsScores.Tables(2).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.AwayScore = CInt(dsScores.Tables(2).Rows(dsScores.Tables(2).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    Else
                        m_objGameDetails.AwayScore = CInt(dsScores.Tables(2).Rows(dsScores.Tables(2).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        m_objGameDetails.HomeScore = CInt(dsScores.Tables(2).Rows(dsScores.Tables(2).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    End If
                End If

                If dsScores.Tables(0).Rows.Count > 0 Then
                    If CDbl(dsScores.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                        If dsScores.Tables(1).Rows.Count > 0 Then
                            Dim drHome() As DataRow = dsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID)
                            Dim drAway() As DataRow = dsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID)

                            m_objGameDetails.PenaltyShootOutHomeScore = drHome.Length
                            m_objGameDetails.PenaltyShootOutAwayScore = drAway.Length

                            lblScoreHome.Text = CStr(m_objGameDetails.HomeScore) + " [" + CStr(m_objGameDetails.PenaltyShootOutHomeScore) + "]"
                            lblScoreAway.Text = CStr(m_objGameDetails.AwayScore) + " [" + CStr(m_objGameDetails.PenaltyShootOutAwayScore) + "]"

                            'If CInt(dsScores.Tables(1).Rows(dsScores.Tables(1).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                            '    tempHomeScore = CInt(dsScores.Tables(1).Rows(dsScores.Tables(1).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            '    tempAwayScore = CInt(dsScores.Tables(1).Rows(dsScores.Tables(1).Rows.Count - 1).Item("DEFENSE_SCORE"))
                            'Else
                            '    tempAwayScore = CInt(dsScores.Tables(1).Rows(dsScores.Tables(1).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            '    tempHomeScore = CInt(dsScores.Tables(1).Rows(dsScores.Tables(1).Rows.Count - 1).Item("DEFENSE_SCORE"))
                            'End If

                            'm_objGameDetails.PenaltyShootOutHomeScore = m_objGameDetails.HomeScore - tempHomeScore
                            'm_objGameDetails.PenaltyShootOutAwayScore = m_objGameDetails.AwayScore - tempAwayScore

                            'lblScoreHome.Text = CStr(tempHomeScore) + " [" + CStr(m_objGameDetails.PenaltyShootOutHomeScore) + "]"
                            'lblScoreAway.Text = CStr(tempAwayScore) + " [" + CStr(m_objGameDetails.PenaltyShootOutAwayScore) + "]"

                        End If
                    Else
                        lblScoreHome.Text = CStr(m_objGameDetails.HomeScore)
                        lblScoreAway.Text = CStr(m_objGameDetails.AwayScore)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetMenuItemsforModule2Score()
        Try
            If m_objGameDetails.ModuleID = MULTIGAME_PBP_MODULE And m_objGameDetails.CoverageLevel = 1 Then
                If m_objclsLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    SetAllMenuItemsVisbleFalse()
                    ShowMenuItem("FileToolStripMenuItem", True)
                    'ShowMenuItem("OpenToolStripMenuItem", True)
                    ShowMenuItem("PurgeAuditTrailToolStripMenuItem", True)
                    ShowMenuItem("ExitToolStripMenuItem", True)
                    ShowMenuItem("SetupToolStripMenuItem", True)
                    ShowMenuItem("PreferencesToolStripMenuItem", True)
                    ShowMenuItem("PeriodToolStripMenuItem", True)
                    ShowMenuItem("StartPeriodToolStripMenuItem", True)
                    ShowMenuItem("EndPeriodToolStripMenuItem", True)
                    ShowMenuItem("EndGameToolStripMenuItem", True)
                    ShowMenuItem("OthersToolStripMenuItem", True)
                    ShowMenuItem("PenaltyShootoutToolStripMenuItem", True)
                    ShowMenuItem("CommentToolStripMenuItem", True)
                    ShowMenuItem("AbandonToolStripMenuItem", True)
                    ShowMenuItem("DelayedToolStripMenuItem", True)
                    ShowMenuItem("HelpToolStripMenuItem", True)
                    ShowMenuItem("AboutToolStripMenuItem", True)
                    ShowMenuItem("RestartDataSyncComponentMenuItem", True)
                    lklHometeam.Links(0).Enabled = False
                    lklAwayteam.Links(0).Enabled = False
                    lklHometeam.LinkBehavior = LinkBehavior.NeverUnderline
                    lklAwayteam.LinkBehavior = LinkBehavior.NeverUnderline
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CheckMenuItem(ByVal MenuItemName As String, ByVal Enable As Boolean)
        Try
            For Each tsmi As ToolStripMenuItem In mnsMain.Items
                For Each obj As Object In tsmi.DropDownItems
                    If Not obj.ToString = "System.Windows.Forms.ToolStripSeparator" Then
                        Dim chtsmi As ToolStripMenuItem = CType(obj, ToolStripMenuItem)
                        For Each obj1 As Object In chtsmi.DropDownItems
                            Dim chchtsmi As ToolStripMenuItem = CType(obj1, ToolStripMenuItem)
                            If chchtsmi.Name = MenuItemName Then
                                chchtsmi.Checked = Enable
                                Exit For
                            End If
                        Next
                    End If
                Next

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetMaxElapsedTimeTouches(ByVal gamecode As Integer, ByVal feednumber As Integer) As Integer
        Try
            Dim TimeElapsed As Integer = 0
            TimeElapsed = m_objGeneral.GetMaxTimeElapsedTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_objGameDetails.IsDefaultGameClock Then
                Dim StrTime As String
                StrTime = CalculateTimeElapseAfter(TimeElapsed, m_objGameDetails.CurrentPeriod)
                TimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(StrTime))
            End If
            Return TimeElapsed
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub SetMarqueeText(ByVal MarqueeText As String)
        Try
            If MarqueeText.Trim = "" Then
                For Each ctrl As Control In Me.Controls
                    If ctrl.GetType.Equals(GetType(WebBrowser)) Then
                        If ctrl.Name = "wbcMarqueeText" Then
                            Me.Controls.Remove(ctrl)
                            ctrl = Nothing
                        End If
                    End If
                Next
            Else
                Dim strBuilder As New System.Text.StringBuilder()
                Dim wbc As New WebBrowser
                Me.Controls.Add(wbc)
                wbc.Name = "wbcMarqueeText"
                wbc.Location = New Point(0, 24)
                wbc.Size = New Size(Me.DisplayRectangle.Width - picCompanyLogo.Width, 15)
                wbc.AllowWebBrowserDrop = False
                wbc.AllowNavigation = False
                wbc.ScrollBarsEnabled = False

                Dim strBgImage As String = Application.StartupPath & "\\Resources\bottombar.gif"
                strBuilder.Append("<html><style type=""text/css"">body{background-color:#20284b;background-image:url('" & strBgImage & "');background-repeat:repeat; color:yellow;vertical-align:top;padding:0px; margin:0px; font-size:12px;font-family:@Arial Unicode MS;}</style></head><body>")
                strBuilder.Append("<marquee BEHAVIOR=SCROLL scrollamount=5 onmouseover=this.stop(); onmouseout=this.start();>" & MarqueeText & "</marquee> ")
                strBuilder.Append("</body></html>")
                wbc.DocumentText = strBuilder.ToString
                wbc.BringToFront()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveTeamlogos()
        Try
            Dim bmp As New Bitmap(Width, Height)
            Dim bmp1 As New Bitmap(picHomeLogo.Width, picHomeLogo.Height)
            Dim original1 As Image = bmp1

            Dim rec As New Rectangle(0, 0, picHomeLogo.Width, picHomeLogo.Height)
            picHomeLogo.DrawToBitmap(bmp1, rec)
            Dim gfx As Graphics = Graphics.FromImage(original1)
            gfx.CompositingQuality = Drawing2D.CompositingQuality.GammaCorrected
            gfx.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
            gfx.InterpolationMode = Drawing2D.InterpolationMode.High
            gfx.DrawImage(original1, rec)
            Dim rpath As String = Application.StartupPath() + "\" + "Resources\"
            Dim imgName1 As String = rpath + "\HomeLogo.bmp"
            original1.Save(imgName1, ImageFormat.Bmp)
            gfx.Dispose()
            bmp.Dispose()
            original1.Dispose()

            Dim bmp2 As New Bitmap(Width, Height)
            Dim bmp3 As New Bitmap(picAwayLogo.Width, picAwayLogo.Height)
            Dim original2 As Image = bmp3

            Dim rec1 As New Rectangle(0, 0, picAwayLogo.Width, picAwayLogo.Height)
            picAwayLogo.DrawToBitmap(bmp3, rec)
            Dim gfx1 As Graphics = Graphics.FromImage(original2)
            gfx1.CompositingQuality = Drawing2D.CompositingQuality.GammaCorrected
            gfx1.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
            gfx1.InterpolationMode = Drawing2D.InterpolationMode.High
            gfx1.DrawImage(original2, rec)
            Dim rpath1 As String = Application.StartupPath() + "\" + "Resources\"
            Dim imgName2 As String = rpath1 + "\AwayLogo.bmp"
            original2.Save(imgName2, ImageFormat.Bmp)
            gfx1.Dispose()
            bmp3.Dispose()
            original2.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetTeamFilter() As Integer
        Dim teamFilter As Integer = 0
        Try
            If m_objGameDetails.ReporterRoleSerial > 2 Then
                If m_objActionsAssist.chkOwnAction.Checked Then 'only when the Display own Action is checked
                    teamFilter = IIf(m_objGameDetails.ReporterRoleSerial = 3, m_objGameDetails.HomeTeamID, m_objGameDetails.AwayTeamID)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Text)
        End Try
        Return teamFilter
    End Function

    Private Sub TimerRefreshPzGameInfo()
        Try
            Dim dsRefreshData As New DataSet
            dsRefreshData = m_objGeneral.RefreshPZGameInfoData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            'Fill Player Data
            If dsRefreshData.Tables(0).Rows.Count > 0 Then
                m_objModule1Main.FillPlayersInMainScreen(dsRefreshData.Tables(0))
            End If
            'refresh game setup info
            If dsRefreshData.Tables(1).Rows.Count > 0 Then
                m_objModule1Main.FillGameInfoInMainScreen(dsRefreshData.Tables(1))
            End If
            'refresh managers in main match screen
            If dsRefreshData.Tables(2).Rows.Count > 0 Then
                m_objModule1Main.FillCoachesInMainScreen(dsRefreshData.Tables(2))
            End If
            If dsRefreshData.Tables(3).Rows.Count > 0 Then
                DisplayReporterStatusInMainScreen(dsRefreshData.Tables(3))
            End If


            ReporterRemovedFromGame(dsRefreshData.Tables(4))
        Catch ex As Exception
            MessageDialog.Show(ex, Text)
        End Try
    End Sub
    ''' <summary>
    ''' Timer refresh specific for Module 3 Prozone soccer software
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub TimerRefreshPzPbp(Optional pbpTreefullLoad As Boolean = False)
        Try

            Dim fullload As Boolean = False 'when dgvpbp count is 0 then load it fully
            Dim dgvPbpReference As New DataGridView

            fullload = pbpTreefullLoad
            If m_objGameDetails.ReporterRoleSerial = 1 Then
                dgvPbpReference = m_objActions.dgvPBP
            Else
                dgvPbpReference = m_objActionsAssist.dgvPBP
            End If

            If dgvPbpReference.Rows.Count <= 0 Then
                fullload = True
                m_objModule1Main.ClearListviews()
            End If

            Dim dsRefreshData As New DataSet
            'get the refreshed 'N' records
            dsRefreshData = m_objGeneral.RefreshPZPbpData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid, CInt(IIf(m_objGameDetails.IsDefaultGameClock, 1, 0)), fullload, CInt(m_objGameDetails.ReporterRoleSerial), 0)
            If dsRefreshData.Tables(0).Rows.Count > 0 Then
                'm_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "TimerRefreshPzPBP start" + DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString(), m_objGameDetails.AwayTeam, 1, 0)
                If m_objGameDetails.ReporterRoleSerial = 1 Then
                    m_objActions.PbpDataTreeLoad(dsRefreshData.Tables(0), fullload, False)
                    'substitution AddPlayerToolStripMenuItem change
                    Dim subs = (From pbp In dsRefreshData.Tables(0).AsEnumerable() Where pbp.Field(Of Int16?)("event_code_id") = 22 Or pbp.Field(Of Int16?)("event_code_id") = 23 Or pbp.Field(Of Int16?)("event_code_id") = 29 Or pbp.Field(Of Int16?)("event_code_id") = 7).ToList()
                    If (subs.Count > 0) Then
                        m_objActions.FillPlayersInUserControls()
                    End If
                Else
                    'TOSOCRS-439 if insertion of goal, we get huge no of records do fullload that time.
                    If fullload = False And dsRefreshData.Tables(0).Rows.Count > 500 Then
                        fullload = True
                    End If
                    m_objActionsAssist.PbpDataTreeLoad(dsRefreshData.Tables(0), fullload) 'PBP tree Loading
                End If

                'm_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "TimerRefreshPzPBP end" + DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString(), m_objGameDetails.AwayTeam, 1, 0)
                _objClsPbpTree.PbpDataUpdateRefreshed(dsRefreshData.Tables(0)) 'Update the PBP Data REFRESHED = 'Y'
                HighlightGoalInGrid(dgvPbpReference)
                'set start of keymoment row to backcolor "Yellow"
                HighlightKeyMomentStartData()
                'Set the latest scores to Lables in main screen
                SetScoresInMainScreen(dsRefreshData.Tables(1), dsRefreshData.Tables(4))
                'PbpEventsInMain
                DisplayPbpEventsInMainScreenPz(dsRefreshData.Tables(0))
                SetMenusInMainScreen(dsRefreshData.Tables(0))
                'Period label Desc
                SetPeriodDescInMainScreen()
            End If
            SetClockTimeInMainScreen(dsRefreshData)

            Dim networkDown As Boolean
            'Network Disconnect Check
            If Not My.Computer.Network.IsAvailable Then
                networkDown = True
            ElseIf dsRefreshData.Tables(6).Rows.Count > 0 Then
                Dim dtTimeDiff As DateTime
                Dim span As TimeSpan
                Dim endTime As DateTime = DateTime.Parse(dsRefreshData.Tables(6).Rows(0).Item("SYSTEM_TIME").ToString)
                dtTimeDiff = Date.Now - m_objclsLoginDetails.USIndiaTimeDiff
                Dim startTime As DateTime = DateTime.Parse(DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat))
                span = startTime.Subtract(endTime)
                If (span.Days > 0 Or span.Hours > 0 Or span.Minutes >= 2) Then
                    networkDown = True
                End If
            End If

            If networkDown Then
                ''TOSOCRS-452 --------------------------------begin
                If lblNetwork.Visible = False Then
                    'FPLRS-149
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "****************** NETWORK DISCONNECTED ************************", 1, 0)
                End If

                lblNetwork.Enabled = True
                lblNetwork.Visible = True
                lblNetwork.Image = System.Drawing.Image.FromFile(Application.StartupPath & "\\Resources\\NetworkDisconnect.gif ")
                lblNetworkTop.Enabled = True
                lblNetworkTop.Visible = True
                lblNetworkTop.Image = System.Drawing.Image.FromFile(Application.StartupPath & "\\Resources\\NetworkDisconnect.gif ")

                ''TOSOCRS-452 --------------------------------end
            Else
                If lblNetwork.Visible = True Then
                    'FPLRS-149
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "****************** NETWORK UP ************************", 1, 0)
                End If
                lblNetwork.Enabled = False
                lblNetwork.Visible = False
                lblNetworkTop.Enabled = False
                lblNetworkTop.Visible = False
                m_objActionDetails.IsNetworkDisconnected = False
            End If


        Catch ex As Exception
            MessageDialog.Show(ex, Text)
        End Try
    End Sub
    'HighLight Goal Events
    Public Function HighlightGoalInGrid(ByVal dgvPbpReference As DataGridView)
        Try
            'highlight keymoment event
            Dim goalEventRow = (From theRow As DataGridViewRow In dgvPbpReference.Rows _
                              Where _goalEvents.Contains(theRow.Cells("EVENT_CODE_ID").Value)
                              Select theRow.Index).ToList()
            For Each goalEventRowIndex In goalEventRow
                dgvPbpReference.Rows(goalEventRowIndex).DefaultCellStyle.BackColor = Color.Green
            Next

        Catch ex As Exception
            MessageDialog.Show(ex, Text)
        End Try
    End Function

    Public Sub DisplayPbpEventsInMainScreenPz(ByVal dtPBP As DataTable)
        Try
            m_objModule1Main.DisplayPbpEventsInMainPz(dtPBP)
        Catch ex As Exception
            MessageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub HighlightKeyMomentStartData()
        Try
            If m_objGameDetails.ReporterRoleSerial <> 1 Then
                m_objActionsAssist.HighlightKeyMomentDataInGrid()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub tmrRefreshGameInfo_Tick(sender As Object, e As EventArgs) Handles tmrRefreshGameInfo.Tick
        Try
            tmrRefreshGameInfo.Stop()
            If m_objGameDetails.ModuleID = 3 Then
                TimerRefreshPzGameInfo()
            End If
        Catch ex As Exception

        Finally
            tmrRefreshGameInfo.Start()
        End Try
    End Sub
    Private Sub SetMenusInMainScreen(ByVal dtRefreshData As DataTable)
        Try
            'refresh for start/end period
            Dim drsStartEndPeriod() As DataRow = dtRefreshData.Select("RECORD_EDITED ='N' AND EVENT_CODE_ID IN (13,21,37,38,10)", "SEQUENCE_NUMBER DESC")
            If drsStartEndPeriod.Length > 0 Then
                EnableMenuItem("RatingsToolStripMenuItem", False)
                EnableMenuItem("StartPeriodToolStripMenuItem", False)
                EnableMenuItem("EndPeriodToolStripMenuItem", False)
                EnableMenuItem("EndGameToolStripMenuItem", False)
                EnableMenuItem("PenaltyShootoutToolStripMenuItem", False)
                Dim setClockStatus As Boolean
                Select Case CInt(drsStartEndPeriod(0).Item("EVENT_CODE_ID"))
                    Case 21, 37 'START HALF , BEGIN OVERTIME
                        pnlEntryForm.Enabled = True
                        If drsStartEndPeriod(0).Item("SEQUENCE_NUMBER") = -1 Then
                            EnableMenuItem("StartPeriodToolStripMenuItem", True)
                            setClockStatus = False
                        Else
                            EnableMenuItem("EndPeriodToolStripMenuItem", True)
                            setClockStatus = True
                        End If

                    Case 13, 38 'HALF OVER, END OVERTIME

                        If drsStartEndPeriod(0).Item("SEQUENCE_NUMBER") = -1 Then
                            EnableMenuItem("EndPeriodToolStripMenuItem", True)
                            setClockStatus = True
                        Else
                            If m_objGameDetails.CurrentPeriod = 2 Or m_objGameDetails.CurrentPeriod = 4 Then
                                EnableMenuItem("EndGameToolStripMenuItem", True)
                                EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                            End If
                            If m_objGameDetails.CurrentPeriod <> 4 Then
                                EnableMenuItem("StartPeriodToolStripMenuItem", True)
                            End If
                            setClockStatus = False
                        End If
                    Case 10 ' GAME OVER EVENT
                        If drsStartEndPeriod(0).Item("SEQUENCE_NUMBER") = -1 Then
                            EnableMenuItem("EndGameToolStripMenuItem", True)
                            EnableMenuItem("PenaltyShootoutToolStripMenuItem", True)
                            setClockStatus = True
                        Else
                            EnableMenuItem("RatingsToolStripMenuItem", True)
                            setClockStatus = False
                        End If

                End Select
                If m_objGameDetails.ReporterRoleSerial = 1 Then
                    SetClockControl(setClockStatus)
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayReporterStatusInMainScreen(ByVal RptrInfo As DataTable)
        Try
            'PRIMARY/ASSISTER REPORTER'S (ONLINE/OFFLINE) STATUS
            Dim strImagePath As String = Application.StartupPath
            strImagePath = strImagePath & "\Resources\"
            picRptr1.ImageLocation = If(RptrInfo.Rows(0)("REPORTER_STATUS_1").ToString = "N", strImagePath & "main-dim.gif", strImagePath & "main.gif")
            picRptr2.ImageLocation = If(RptrInfo.Rows(0)("REPORTER_STATUS_2").ToString = "N", strImagePath & "assist-dim.gif", strImagePath & "assist.gif")
            picRptr3.ImageLocation = If(RptrInfo.Rows(0)("REPORTER_STATUS_3").ToString = "N", strImagePath & "assist-dim.gif", strImagePath & "assist.gif")
            picRptr4.ImageLocation = If(RptrInfo.Rows(0)("REPORTER_STATUS_4").ToString = "N", strImagePath & "assist-dim.gif", strImagePath & "assist.gif")
            picRptr5.ImageLocation = If(RptrInfo.Rows(0)("REPORTER_STATUS_5").ToString = "N", strImagePath & "assist-dim.gif", strImagePath & "assist.gif")
            picRptr6.ImageLocation = If(RptrInfo.Rows(0)("REPORTER_STATUS_6").ToString = "N", strImagePath & "assist-dim.gif", strImagePath & "assist.gif")

            'IF CURRENT REPORTER IS ASSISTER THEN CHECK PRIMARY REPORTER IS OFFLINE OR NOT
            If m_objGameDetails.ReporterRoleSerial <> 1 Then
                If CChar(RptrInfo.Rows(0)("REPORTER_STATUS_1")) = "N" Then
                    m_objGameDetails.IsPrimaryReporterDown = True
                Else
                    m_objGameDetails.IsPrimaryReporterDown = False
                End If
                'IF CURRENT REPORTER IS AN ASSISTER THEN CHECK HIS LAST ENTRY STATUS(ACCEPTED/REJECTED BY PRIMARY REPORTER)
                Select Case RptrInfo.Rows(0)("LAST_ENTRY_STATUS").ToString
                    Case "N" '--> LAST ENTRY STILL IN PROCESS
                        m_objGameDetails.AssistersLastEntryStatus = False
                    Case "D" '--> LAST ENTRY IS DECLINED/REJECTED BY PRIMARY REPORTER
                        MessageDialog.Show(strmessage11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, strmessage11, 1, 1)
                        m_objGameDetails.AssistersLastEntryStatus = True
                        m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, False)
                    Case "A" '--> LAST ENTRY IS ACCEPTED BY PRIMARY REPORTER
                        m_objGameDetails.AssistersLastEntryStatus = True
                        m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, False)
                End Select
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'CHECK CURRENT REPORTER HAS REMOVED FROM THE SCORING GAME
    Private Sub ReporterRemovedFromGame(ByVal RptrInfo As DataTable)
        Try

            If m_objGameDetails.ReporterRole.ToUpper <> "OPS" Then
                If RptrInfo.Rows.Count > 0 Then
                    If CInt(RptrInfo.Rows(0)("GAMECOUNT")) = 0 Then 'REMOVED
                        MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        m_objGameDetails.IsReporterRemoved = True
                        m_formClosed = True
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub SetClockTimeInMainScreen(ByVal RefreshData As DataSet)
        Try
            Dim StrTime As String = "", strNewTime() As String, boolSetClock As Boolean = False ', intTime As Integer = 0
            Dim eventCodeId As Integer = 0
            Dim drs() As DataRow
            If m_objGameDetails.ReporterRoleSerial <> 1 Then
                If RefreshData.Tables(2).Rows.Count > 0 Then
                    boolSetClock = True
                    For Each drPBP As DataRow In RefreshData.Tables(2).Rows
                        If UdcRunningClock1.URCCurrentTime <> "" And m_objGameDetails.CurrentPeriod > 0 Then
                            StrTime = CalculateTimeElapseAfter(CInt(drPBP.Item("TIME_ELAPSED")), m_objGameDetails.CurrentPeriod)
                            strNewTime = StrTime.Split(CChar(":"))
                            UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                            If CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTOP Then 'Or CInt(drPBP.Item("EVENT_CODE_ID")) = clsGameDetails.ABANDONED Or CInt(drPBP.Item("EVENT_CODE_ID")) = clsGameDetails.DELAYED Then
                                If UdcRunningClock1.URCIsRunning Then
                                    UdcRunningClock1.URCToggle()
                                End If
                            ElseIf CInt(drPBP.Item("EVENT_CODE_ID")) = INTCLOCKSTART Then 'Or CInt(drPBP.Item("EVENT_CODE_ID")) = clsGameDetails.DELAY_OVER Then
                                If Not UdcRunningClock1.URCIsRunning Then
                                    UdcRunningClock1.URCToggle()
                                End If
                            End If
                        End If
                        Exit For
                    Next
                End If
                'sync with primary reporter
                If RefreshData.Tables(3).Rows.Count > 0 Then
                    If Not IsDBNull(RefreshData.Tables(3).Rows(0)("TIME_ELAPSED")) Then
                        boolSetClock = True
                    End If
                End If
            End If

            'deletion of start/end period handling
            If (RefreshData.Tables(0).Rows.Count > 0 And UdcRunningClock1.URCCurrentTime <> "") Then
                drs = RefreshData.Tables(0).Select("EVENT_CODE_ID IN (13,21,58,65,59) AND SEQUENCE_NUMBER = -1")
                If drs.Length > 0 Then
                    StrTime = CalculateTimeElapseAfter(CInt(RefreshData.Tables(3).Rows(0)("TIME_ELAPSED")), m_objGameDetails.CurrentPeriod)
                    strNewTime = StrTime.Split(CChar(":"))
                    UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                    If (drs(0).Item("EVENT_CODE_ID") = 13 Or drs(0).Item("EVENT_CODE_ID") = 58 Or drs(0).Item("EVENT_CODE_ID") = 59) And m_objGameDetails.CurrentPeriod > 0 Then 'Or ((drs(0).Item("EVENT_CODE_ID") = 58 Or drs(0).Item("EVENT_CODE_ID") = 59) And Not isMenuItemEnabled("StartPeriodToolStripMenuItem")) Then
                        If Not UdcRunningClock1.URCIsRunning Then
                            UdcRunningClock1.URCToggle()
                        End If
                    Else
                        If UdcRunningClock1.URCIsRunning Then
                            UdcRunningClock1.URCToggle()
                        End If
                    End If
                End If
            End If

            If (RefreshData.Tables(3).Rows.Count > 0 And (boolSetClock Or UdcRunningClock1.URCCurrentTime = "")) Then
                If Not IsDBNull(RefreshData.Tables(3).Rows(0)("TIME_ELAPSED")) Then
                    StrTime = CalculateTimeElapseAfter(CInt(RefreshData.Tables(3).Rows(0)("TIME_ELAPSED")), m_objGameDetails.CurrentPeriod)
                    strNewTime = StrTime.Split(CChar(":"))
                    If IIf(m_objGameDetails.IsDefaultGameClock, CalculateTimeElapsedBefore(m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)), m_objUtility.ConvertMinuteToSecond(UdcRunningClock1.URCCurrentTime)) < CInt(RefreshData.Tables(3).Rows(0)("TIME_ELAPSED")) Then
                        UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                        If UdcRunningClock1.URCIsRunning = False And lblPeriod.ForeColor <> Color.Red And eventCodeId <> 58 And eventCodeId <> 59 Then
                            UdcRunningClock1.URCToggle()
                        End If
                    ElseIf (UdcRunningClock1.URCCurrentTime = "") And StrTime = "00:00" Then
                        UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), CShort(strNewTime(1)))
                        If lblPeriod.Text <> PRE_GAME_DESC Then
                            If UdcRunningClock1.URCIsRunning = False Then
                                UdcRunningClock1.URCToggle()
                            End If
                        End If

                    End If
                End If
            End If

            If UdcRunningClock1.URCIsRunning Then
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
            Else
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
            End If
        Catch ex As Exception
            Throw
        End Try

    End Sub
    Private Sub SetScoresInMainScreen(ByVal dtRefreshScores As DataTable, ByVal dtRefreshShootoutScore As DataTable)
        Try
            Dim drScores = dtRefreshScores.Rows(0)
            lblScoreHome.Text = If(CInt(drScores.Item("TEAM_ID")) = m_objGameDetails.HomeTeamID, drScores.Item("OFFENSE_SCORE"), drScores.Item("DEFENSE_SCORE")).ToString
            lblScoreAway.Text = If(CInt(drScores.Item("TEAM_ID")) = m_objGameDetails.AwayTeamID, drScores.Item("OFFENSE_SCORE"), drScores.Item("DEFENSE_SCORE")).ToString
            'set the current period
            m_objGameDetails.HomeScore = lblScoreHome.Text
            m_objGameDetails.AwayScore = lblScoreAway.Text
            m_objGameDetails.CurrentPeriod = CInt(drScores.Item("PERIOD"))

            If dtRefreshShootoutScore.Rows(0).Item("Home_ShootoutScore") > 0 Or dtRefreshShootoutScore.Rows(0).Item("Away_ShootoutScore") > 0 Then
                lblScoreHome.Text += " [" + dtRefreshShootoutScore.Rows(0).Item("Home_ShootoutScore").ToString() + "]"
                lblScoreAway.Text += " [" + dtRefreshShootoutScore.Rows(0).Item("Away_ShootoutScore").ToString() + "]"
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetPeriodDescInMainScreen()
        Try
            Select Case m_objGameDetails.CurrentPeriod
                Case 0
                    lblPeriod.Text = PRE_GAME_DESC
                    lblPeriod.ForeColor = Color.White
                Case 1
                    If isMenuItemEnabled("StartPeriodToolStripMenuItem") = True Then
                        lblPeriod.Text = HALFTIME_DESC
                        lblPeriod.ForeColor = Color.Red
                    Else
                        lblPeriod.Text = FIRST_HALF_DESC
                        lblPeriod.ForeColor = Color.White
                    End If
                Case 2
                    If (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False) Then
                        lblPeriod.Text = END_GAME_DESC
                        lblPeriod.ForeColor = Color.Red
                        m_objGameDetails.IsEndGame = True
                    ElseIf (isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And isMenuItemEnabled("StartPeriodToolStripMenuItem") = True) Then
                        lblPeriod.Text = END_REGULATION_DESC
                        lblPeriod.ForeColor = Color.Red
                    Else
                        lblPeriod.Text = SECOND_HALF_DESC
                        lblPeriod.ForeColor = Color.White
                    End If
                Case 3
                    If isMenuItemEnabled("StartPeriodToolStripMenuItem") = True Then
                        lblPeriod.Text = EXTRATIME_BREAK_DESC
                        lblPeriod.ForeColor = Color.Red
                    Else
                        lblPeriod.Text = FIRST_EXTRATIME_DESC
                        lblPeriod.ForeColor = Color.White
                    End If
                Case 4
                    If (isMenuItemEnabled("StartPeriodToolStripMenuItem") = False And isMenuItemEnabled("EndGameToolStripMenuItem") = False And isMenuItemEnabled("EndPeriodToolStripMenuItem") = False) Then
                        lblPeriod.Text = END_GAME_DESC
                        lblPeriod.ForeColor = Color.Red
                        m_objGameDetails.IsEndGame = True
                    ElseIf (isMenuItemEnabled("EndPeriodToolStripMenuItem") = False) Then
                        lblPeriod.Text = END_REGULATION_DESC
                        lblPeriod.ForeColor = Color.Red
                    Else
                        lblPeriod.Text = SECOND_EXTRATIME_DESC
                        lblPeriod.ForeColor = Color.White
                    End If
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function ActivateGames() As Boolean
        Try
            Dim returnStatus As Boolean = True

            If m_objGameDetails.CurrentPeriod = 0 Then
                UdcRunningClock1.URCToggle()
                Dim dsStartPeriodCount As DataSet
                'This check is done to prevent duplicate START PERIOD in multi reporter
                dsStartPeriodCount = m_objGeneral.PzGetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.STARTPERIOD)
                If CInt(dsStartPeriodCount.Tables(2).Rows(0).Item("PeriodCount")) > m_objGameDetails.CurrentPeriod Then
                    m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                End If
            Else
                Dim dsStartPeriodCount As DataSet
                'This check is done to prevent duplicate START PERIOD in multi reporter
                dsStartPeriodCount = m_objGeneral.PzGetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.STARTPERIOD)
                If CInt(dsStartPeriodCount.Tables(2).Rows(0).Item("PeriodCount")) > m_objGameDetails.CurrentPeriod Then
                    m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                    returnStatus = False
                End If
            End If

            m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
            setStartClock()
            If (m_objActionDetails.EventId = 67) Or (m_objActionDetails.EventId = 68) Or (m_objActionDetails.TeamId IsNot Nothing) Then
                'if any actions is selected before End period..
                m_objActions.SaveAction()
            End If

            If m_objGameDetails.CurrentPeriod = 1 Then
                m_objActionDetails.EventId = 34
                m_objActionDetails.TeamId = Me.m_objGameDetails.HomeTeamID
                m_objActions.InsertPlayByPlayData()
            End If
            m_objActionDetails.EventId = 21
            m_objActionDetails.TeamId = Me.m_objGameDetails.HomeTeamID
            m_objActions.InsertPlayByPlayData()
            m_objActionDetails.EventId = 54
            m_objActions.InsertPlayByPlayData()

            m_objActions.EnableDisableControls(True)
            m_objActions.UnHighlightActions()
            m_objActions.ResetProperties()

            If UdcRunningClock1.URCIsRunning = False And m_objGameDetails.CurrentPeriod <> 5 Then
                UdcRunningClock1.URCToggle()
            End If

            If UdcRunningClock1.URCIsRunning Then
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
            Else
                btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
            End If
            TimerRefreshPzPbp()
            Return returnStatus
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

Public Class SoccerToolStripMenuItem
    Inherits ToolStripMenuItem

    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance

    Private Sub SoccerToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Click
        Try
            Dim strMenuItemName As String = CType(sender, SoccerToolStripMenuItem).Name
            If m_objGameDetails.ModuleID = 3 Then
                frmMain.ProzoneMenu_Click(strMenuItemName)
            Else
                frmMain.Menu_Click(strMenuItemName)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

End Class