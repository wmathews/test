﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmGameSetup

#Region " Constants & Variables "
    Private m_objUtility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Dim m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objGameSetup As New clsGameSetup
    Private m_dsGameSetup As New DataSet
    Private blnSkipSEChanged As Boolean = False
    Private m_strAttendance As String = String.Empty
    Private MessageDialog As New frmMessageDialog
    Private Dsref As DataSet
    Private lsupport As New languagesupport
    Dim strmessage As String = "Please select the Foul Event to associate Booking ."
    Dim strmessage1 As String = "Attendence exceeds the stadium capacity. Do you want to continue ?"
    Dim strmessage2 As String = "Please Select Valid"
    Dim strmessage3 As String = "Selcet valid"
    Private m_intEndGameCnt As Integer = 0

    Dim strLeft As String = "Left"
    Dim strRight As String = "Right"
    Dim message1 As String = "Ratings entered should be between 1 and 10 ."
    Dim message2 As String = "Match rating should be entered between 1 and 10 ."
    Dim message3 As String = "Last Entry in Process, Please Wait..."
    Dim message4 As String = "You are not allowed to Insert/Update Events - Primary Reporter is Down!"
    Dim message5 As String = "The Referee is already selected in"
    Dim message6 As String = "Decimal value not allowed!"

#End Region

#Region " Event Handlers "

    ''' <summary>
    ''' Loads the data into Referee Combo boxes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    '''
    Public Sub linebreak(ByVal lbtemp As Label, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub

    Private Sub frmGameSetup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim TEMLENINT As Integer
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmGameSetup", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            m_dsGameSetup = m_objGameSetup.SelectGameSetup(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID, m_objGameDetails.HomeTeamID, m_objGameDetails.languageid, m_objGameDetails.CoverageLevel, m_objGameDetails.ModuleID)
            m_objGameDetails.GameSetup = m_dsGameSetup
            '14-Mar-2012 Arindam do not display home start ET not 3rd period
            If (m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3) Then
                If m_objGameDetails.CurrentPeriod < 3 Then
                    cmbhomestartET.Visible = False
                    lblhomestartET.Visible = False
                    lblHomeStart.Visible = True
                Else
                    lblHomeStart.Visible = False
                    lblhomestartET.Visible = True
                    cmbHomeStart.Visible = False
                    cmbhomestartET.Visible = True
                    cmbhomestartET.Enabled = True
                End If
            ElseIf m_objGameDetails.ModuleID = 2 Then
                lblHomeStart.Visible = False
                lblhomestartET.Visible = False
                cmbHomeStart.Visible = False
                cmbhomestartET.Visible = False
                cmbhomestartET.Enabled = False
                Label4.Visible = False
                Label5.Visible = False
                lblHTeamName.Visible = False
            End If

            LoadAllControl(m_dsGameSetup)
            DisplayTeamLogoandTeamAbbrev(m_dsGameSetup)

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                lblHomeTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblHomeTeam.Text)
                lblAwayTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblAwayTeam.Text)
                lblReferee.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblReferee.Text)
                TEMLENINT = lblLinesman.Text.Length
                lblLinesman.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLinesman.Text)
                linebreak(lblLinesman, TEMLENINT)
                TEMLENINT = lblLinesman1.Text.Length
                lblLinesman1.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLinesman1.Text)
                linebreak(lblLinesman1, TEMLENINT)
                lbl4thOfficial.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lbl4thOfficial.Text)
                lbl5thOfficial.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lbl5thOfficial.Text)
                TEMLENINT = lblVenue.Text.Length
                lblVenue.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblVenue.Text)
                linebreak(lblVenue, TEMLENINT)

                TEMLENINT = lblKickOff.Text.Length
                lblKickOff.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblKickOff.Text)
                linebreak(lblKickOff, TEMLENINT)

                lblOfficials.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblOfficials.Text)

                TEMLENINT = 10
                lblAttendance.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblAttendance.Text)
                linebreak(lblAttendance, TEMLENINT)
                lblDelay.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblDelay.Text)
                TEMLENINT = 8
                lblHomeStart.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblHomeStart.Text)
                '' linebreak(lblHomeStart, TEMLENINT)
                lblMatchInformation.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblMatchInformation.Text)
                btnAddOfficial.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Add official...")
                lblMatchRating.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblMatchRating.Text)

                message1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message1)
                message2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message2)
                message3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message3)
                message4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message4)
                message5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message5)
                message6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message6)
            End If
            If (m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 3) Then
                lblHomeStart.Visible = True
                Label5.Visible = True
                Label4.Visible = True
                lblHTeamName.Visible = True
                lblHTeamName.Text = m_objGameDetails.HomeTeam.ToString()
            Else
                lblHomeStart.Visible = False
                Label5.Visible = False
                Label4.Visible = False
                lblHTeamName.Visible = False
            End If
            If m_objGameDetails.CurrentPeriod = 0 Then
                mtxtDelay.Enabled = True
            Else
                mtxtDelay.Enabled = False
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally

        End Try
    End Sub

    Private Sub PopulateDropdowns(ByVal OriginalDataset As DataSet, Optional ByVal Combo As ComboBox = Nothing)
        Try
            blnSkipSEChanged = True

            Dim RunningDataset As DataSet = OriginalDataset.Copy
            Dim strPrevSelectedValue As String = ""

            If Not Combo Is Nothing Then
                If Not Combo.Name = "cmbReferee" Then
                    If Not cmbReferee.SelectedValue Is Nothing Then
                        strPrevSelectedValue = cmbReferee.SelectedValue.ToString
                    Else
                        strPrevSelectedValue = ""
                    End If

                    cmbReferee.DataSource = Nothing
                    Dim dr() As DataRow
                    If cmbLinesman.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(0).Select("REF_ID=" & cmbLinesman.SelectedValue.ToString)
                        RunningDataset.Tables(0).Rows.Remove(dr(0))
                    End If
                    If CmbLinesman1.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(0).Select("REF_ID=" & CmbLinesman1.SelectedValue.ToString)
                        RunningDataset.Tables(0).Rows.Remove(dr(0))
                    End If
                    If cmb4thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(0).Select("REF_ID=" & cmb4thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(0).Rows.Remove(dr(0))
                    End If
                    If cmb5thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(0).Select("REF_ID=" & cmb5thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(0).Rows.Remove(dr(0))
                    End If

                    If cmb6thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(0).Select("REF_ID=" & cmb6thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(0).Rows.Remove(dr(0))
                    End If

                    'cmbReferee.DataSource = RunningDataset.Tables(0)
                    'cmbReferee.DisplayMember = RunningDataset.Tables(0).Columns(1).ToString
                    'cmbReferee.ValueMember = RunningDataset.Tables(0).Columns(0).ToString
                    LoadControl(cmbReferee, RunningDataset.Tables(0), "REFEREE", "REF_ID")

                    If strPrevSelectedValue = "" Then
                        cmbReferee.SelectedIndex = -1
                    Else
                        cmbReferee.SelectedValue = strPrevSelectedValue
                    End If
                End If
                If Not Combo.Name = "cmbLinesman" Then
                    If Not cmbLinesman.SelectedValue Is Nothing Then
                        strPrevSelectedValue = cmbLinesman.SelectedValue.ToString
                    Else
                        strPrevSelectedValue = ""
                    End If

                    cmbLinesman.DataSource = Nothing
                    Dim dr() As DataRow
                    If cmbReferee.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(1).Select("REF_ID=" & cmbReferee.SelectedValue.ToString)
                        RunningDataset.Tables(1).Rows.Remove(dr(0))
                    End If
                    If CmbLinesman1.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(1).Select("REF_ID=" & CmbLinesman1.SelectedValue.ToString)
                        RunningDataset.Tables(1).Rows.Remove(dr(0))
                    End If
                    If cmb4thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(1).Select("REF_ID=" & cmb4thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(1).Rows.Remove(dr(0))
                    End If
                    If cmb5thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(1).Select("REF_ID=" & cmb5thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(1).Rows.Remove(dr(0))
                    End If
                    If cmb6thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(1).Select("REF_ID=" & cmb6thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(1).Rows.Remove(dr(0))
                    End If
                    'cmbLinesman.DataSource = RunningDataset.Tables(1)
                    'cmbLinesman.DisplayMember = RunningDataset.Tables(1).Columns(1).ToString
                    'cmbLinesman.ValueMember = RunningDataset.Tables(1).Columns(0).ToString

                    LoadControl(cmbLinesman, RunningDataset.Tables(1), "REFEREE", "REF_ID")

                    If strPrevSelectedValue = "" Then
                        cmbLinesman.SelectedIndex = -1
                    Else
                        cmbLinesman.SelectedValue = strPrevSelectedValue
                    End If
                End If
                If Not Combo.Name = "CmbLinesman1" Then
                    If Not CmbLinesman1.SelectedValue Is Nothing Then
                        strPrevSelectedValue = CmbLinesman1.SelectedValue.ToString
                    Else
                        strPrevSelectedValue = ""
                    End If

                    CmbLinesman1.DataSource = Nothing
                    Dim dr() As DataRow
                    If cmbReferee.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(2).Select("REF_ID=" & cmbReferee.SelectedValue.ToString)
                        RunningDataset.Tables(2).Rows.Remove(dr(0))
                    End If
                    If cmbLinesman.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(2).Select("REF_ID=" & cmbLinesman.SelectedValue.ToString)
                        RunningDataset.Tables(2).Rows.Remove(dr(0))
                    End If
                    If cmb4thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(2).Select("REF_ID=" & cmb4thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(2).Rows.Remove(dr(0))
                    End If
                    If cmb5thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(2).Select("REF_ID=" & cmb5thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(2).Rows.Remove(dr(0))
                    End If
                    If cmb6thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(2).Select("REF_ID=" & cmb6thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(2).Rows.Remove(dr(0))
                    End If
                    'CmbLinesman1.DataSource = RunningDataset.Tables(2)
                    'CmbLinesman1.DisplayMember = RunningDataset.Tables(2).Columns(1).ToString
                    'CmbLinesman1.ValueMember = RunningDataset.Tables(2).Columns(0).ToString
                    LoadControl(CmbLinesman1, RunningDataset.Tables(2), "REFEREE", "REF_ID")

                    If strPrevSelectedValue = "" Then
                        CmbLinesman1.SelectedIndex = -1
                    Else
                        CmbLinesman1.SelectedValue = strPrevSelectedValue
                    End If
                End If
                If Not Combo.Name = "cmb4thOfficial" Then
                    If Not cmb4thOfficial.SelectedValue Is Nothing Then
                        strPrevSelectedValue = cmb4thOfficial.SelectedValue.ToString
                    Else
                        strPrevSelectedValue = ""
                    End If

                    cmb4thOfficial.DataSource = Nothing
                    Dim dr() As DataRow
                    If cmbReferee.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(3).Select("REF_ID=" & cmbReferee.SelectedValue.ToString)
                        RunningDataset.Tables(3).Rows.Remove(dr(0))
                    End If
                    If cmbLinesman.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(3).Select("REF_ID=" & cmbLinesman.SelectedValue.ToString)
                        RunningDataset.Tables(3).Rows.Remove(dr(0))
                    End If
                    If CmbLinesman1.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(3).Select("REF_ID=" & CmbLinesman1.SelectedValue.ToString)
                        RunningDataset.Tables(3).Rows.Remove(dr(0))
                    End If
                    If cmb5thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(3).Select("REF_ID=" & cmb5thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(3).Rows.Remove(dr(0))
                    End If
                    If cmb6thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(3).Select("REF_ID=" & cmb6thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(3).Rows.Remove(dr(0))
                    End If
                    'cmb4thOfficial.DataSource = RunningDataset.Tables(3)
                    'cmb4thOfficial.DisplayMember = RunningDataset.Tables(3).Columns(1).ToString
                    'cmb4thOfficial.ValueMember = RunningDataset.Tables(3).Columns(0).ToString
                    LoadControl(cmb4thOfficial, RunningDataset.Tables(3), "REFEREE", "REF_ID")

                    If strPrevSelectedValue = "" Then
                        cmb4thOfficial.SelectedIndex = -1
                    Else
                        cmb4thOfficial.SelectedValue = strPrevSelectedValue
                    End If
                End If
                If Not Combo.Name = "cmb5thOfficial" Then
                    If Not cmb5thOfficial.SelectedValue Is Nothing Then
                        strPrevSelectedValue = cmb5thOfficial.SelectedValue.ToString
                    Else
                        strPrevSelectedValue = ""
                    End If

                    cmb5thOfficial.DataSource = Nothing
                    Dim dr() As DataRow
                    If cmbReferee.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(4).Select("REF_ID=" & cmbReferee.SelectedValue.ToString)
                        RunningDataset.Tables(4).Rows.Remove(dr(0))
                    End If
                    If cmbLinesman.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(4).Select("REF_ID=" & cmbLinesman.SelectedValue.ToString)
                        RunningDataset.Tables(4).Rows.Remove(dr(0))
                    End If
                    If CmbLinesman1.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(4).Select("REF_ID=" & CmbLinesman1.SelectedValue.ToString)
                        RunningDataset.Tables(4).Rows.Remove(dr(0))
                    End If
                    If cmb4thOfficial.SelectedIndex > 0 Then
                        dr = RunningDataset.Tables(4).Select("REF_ID=" & cmb4thOfficial.SelectedValue.ToString)
                        RunningDataset.Tables(4).Rows.Remove(dr(0))
                    End If

                    'cmb5thOfficial.DataSource = RunningDataset.Tables(4)
                    'cmb5thOfficial.DisplayMember = RunningDataset.Tables(4).Columns(1).ToString
                    'cmb5thOfficial.ValueMember = RunningDataset.Tables(4).Columns(0).ToString
                    LoadControl(cmb5thOfficial, RunningDataset.Tables(4), "REFEREE", "REF_ID")
                    LoadControl(cmb6thOfficial, RunningDataset.Tables(4), "REFEREE", "REF_ID")

                    If strPrevSelectedValue = "" Then
                        cmb5thOfficial.SelectedIndex = -1
                    Else
                        cmb5thOfficial.SelectedValue = strPrevSelectedValue
                    End If
                End If
            Else

                'cmbReferee.DataSource = RunningDataset.Tables(0)
                'cmbReferee.DisplayMember = RunningDataset.Tables(0).Columns(1).ToString
                'cmbReferee.ValueMember = RunningDataset.Tables(0).Columns(0).ToString
                LoadControl(cmbReferee, RunningDataset.Tables(0), "REFEREE", "REF_ID")
                cmbReferee.SelectedIndex = -1

                'cmbLinesman.DataSource = RunningDataset.Tables(1)
                'cmbLinesman.DisplayMember = RunningDataset.Tables(1).Columns(1).ToString
                'cmbLinesman.ValueMember = RunningDataset.Tables(1).Columns(0).ToString
                LoadControl(cmbLinesman, RunningDataset.Tables(1), "REFEREE", "REF_ID")
                cmbLinesman.SelectedIndex = -1

                'CmbLinesman1.DataSource = RunningDataset.Tables(2)
                'CmbLinesman1.DisplayMember = RunningDataset.Tables(2).Columns(1).ToString
                'CmbLinesman1.ValueMember = RunningDataset.Tables(2).Columns(0).ToString
                LoadControl(CmbLinesman1, RunningDataset.Tables(2), "REFEREE", "REF_ID")
                CmbLinesman1.SelectedIndex = -1

                'cmb4thOfficial.DataSource = RunningDataset.Tables(3)
                'cmb4thOfficial.DisplayMember = RunningDataset.Tables(3).Columns(1).ToString
                'cmb4thOfficial.ValueMember = RunningDataset.Tables(3).Columns(0).ToString
                LoadControl(cmb4thOfficial, RunningDataset.Tables(3), "REFEREE", "REF_ID")
                cmb4thOfficial.SelectedIndex = -1

                'cmb5thOfficial.DataSource = RunningDataset.Tables(4)
                'cmb5thOfficial.DisplayMember = RunningDataset.Tables(4).Columns(1).ToString
                'cmb5thOfficial.ValueMember = RunningDataset.Tables(4).Columns(0).ToString
                LoadControl(cmb5thOfficial, RunningDataset.Tables(4), "REFEREE", "REF_ID")
                cmb5thOfficial.SelectedIndex = -1

                LoadControl(cmb6thOfficial, RunningDataset.Tables(4), "REFEREE", "REF_ID")
                cmb6thOfficial.SelectedIndex = -1

                LoadControl(cmbWeather, RunningDataset.Tables(5), "DESCRIPTION", "WEATHER_ID")
                cmbWeather.SelectedIndex = -1
            End If

            blnSkipSEChanged = False
        Catch ex As Exception
            blnSkipSEChanged = False
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Saves the added or updated gamesetup info
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 0)

            If IsPBPEntryAllowed() = False Then
                Exit Try
            End If

            Dim dsGameSetUp As New DataSet
            If (ValidateGameSetup()) Then

                'Delay event showing the pop up for entering the reason for delay
                If m_objGameDetails.GameSetup.Tables.Count > 0 Then
                    If m_objGameDetails.GameSetup.Tables(4).Rows.Count > 0 Then
                        If mtxtDelay.Text <> "00:00" Then
                            Dim strDelayTime() As String
                            Dim DelayTime As Double

                            strDelayTime = mtxtDelay.Text.Split(CChar(":"))
                            DelayTime = CDbl(strDelayTime(0)) * 60 + CDbl(strDelayTime(1))
                            m_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))  'DateTime.ParseExact(m_objGameDetails.GameDate, "MM/dd/yyyy HH:mm:ss am", System.Globalization.CultureInfo.InvariantCulture).Date
                            If m_objGameDetails.GameSetup.Tables(4).Rows(0).Item("START_TIME_ACTUAL").ToString = "" Then
                                Dim objabandon As New frmAbandon
                                objabandon.ShowDialog()
                            ElseIf CDate(m_objGameDetails.GameSetup.Tables(4).Rows(0).Item("START_TIME_ACTUAL").ToString) <> CDate(m_objGameDetails.DelayTime.AddMinutes(DelayTime).ToString("yyyy-MM-dd HH:mm:ss tt")) Then
                                Dim objabandon As New frmAbandon
                                objabandon.ShowDialog()
                            Else
                                m_objGameDetails.Reason = ""
                            End If

                        Else
                            m_objGameDetails.Reason = ""
                        End If
                    Else
                        If mtxtDelay.Text <> "00:00" Then
                            Dim strDelayTime() As String
                            Dim DelayTime As Double

                            strDelayTime = mtxtDelay.Text.Split(CChar(":"))
                            DelayTime = CDbl(strDelayTime(0)) * 60 + CDbl(strDelayTime(1))
                            m_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))  'DateTime.ParseExact(m_objGameDetails.GameDate, "MM/dd/yyyy HH:mm:ss am", System.Globalization.CultureInfo.InvariantCulture).Date
                            Dim objabandon As New frmAbandon
                            objabandon.ShowDialog()
                        Else
                            m_objGameDetails.Reason = ""
                        End If
                    End If
                End If
                Me.DialogResult = Windows.Forms.DialogResult.OK
                dsGameSetUp = CreateAndAddDataColumn()
                m_objGameSetup.InsertUpdateGameSetup(dsGameSetUp)

                ''Added by shirley on Aug 31 2010 for Multi rep comm
                'If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1"  And m_objGameDetails.ReporterRole <> "OPS" Then
                '    m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, True)
                '    m_objGameDetails.AssistersLastEntryStatus = False
                'End If

                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Closes the Gamesetup form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, 1, 0)
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'Private Sub cmbReferee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbReferee.KeyPress
    '    Try
    '        cmbReferee.DroppedDown = False
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub cmbReferee_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmbReferee.MouseClick
    '    Try
    '        cmbReferee.DroppedDown = True
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    ''' <summary>
    ''' Selects Referee value and checks whether the selected value already assigned to any other referee
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbReferee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbReferee.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If cmbReferee.Text <> "" Then
                    If (CInt(cmbReferee.SelectedValue.ToString()) <> 0) Then
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbReferee.Text, 1, 0)
                        If m_objGameDetails.IsEndGame = True Or m_intEndGameCnt > 0 Then
                            txtRating1.Enabled = True
                        Else
                            txtRating1.Enabled = False
                            txtRating1.Text = ""
                        End If
                    ElseIf cmbReferee.SelectedValue Is Nothing Then
                        MessageDialog.Show(strmessage3 & lblReferee.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                Else
                    txtRating1.Enabled = False
                    txtRating1.Text = ""
                End If
                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLinesman_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbLinesman.KeyPress
        Try
            cmbLinesman.DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbLinesman_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmbLinesman.MouseClick
        Try
            cmbLinesman.DroppedDown = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Selects Linesman  value and checks whether the selected value already assigned to any other referee
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbLinesman_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLinesman.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If cmbLinesman.Text <> "" Then
                    If (CInt(cmbLinesman.SelectedValue.ToString()) <> 0) Then
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbLinesman.Text, 1, 0)
                        If m_objGameDetails.IsEndGame = True Or m_intEndGameCnt > 0 Then
                            txtRating2.Enabled = True
                        Else
                            txtRating2.Enabled = False
                            txtRating2.Text = ""
                        End If
                    ElseIf cmbLinesman.SelectedValue Is Nothing Then
                        MessageDialog.Show(strmessage3 & lblLinesman.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                Else
                    txtRating2.Enabled = False
                    txtRating2.Text = ""
                End If

                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbLinesman1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CmbLinesman1.KeyPress
        Try
            CmbLinesman1.DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbLinesman1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CmbLinesman1.MouseClick
        Try
            CmbLinesman1.DroppedDown = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Selects Linesman  value and checks whether the selected value already assigned to any other referee
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub CmbLinesman1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbLinesman1.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If CmbLinesman1.Text <> "" Then
                    If (CInt(CmbLinesman1.SelectedValue.ToString()) <> 0) Then
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, CmbLinesman1.Text, 1, 0)
                        If m_objGameDetails.IsEndGame = True Or m_intEndGameCnt > 0 Then
                            txtRating3.Enabled = True
                            txtRating5.Enabled = True
                            txtRating6.Enabled = True
                        Else
                            txtRating3.Enabled = False
                            txtRating3.Text = ""
                            txtRating5.Enabled = False
                            txtRating5.Text = ""
                            txtRating6.Enabled = False
                            txtRating6.Text = ""
                        End If
                    ElseIf CmbLinesman1.SelectedValue Is Nothing Then
                        MessageDialog.Show(strmessage3 & lblLinesman1.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                Else
                    txtRating3.Enabled = False
                    txtRating3.Text = ""
                    txtRating5.Enabled = False
                    txtRating5.Text = ""
                    txtRating6.Enabled = False
                    txtRating6.Text = ""
                End If

                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmb4thOfficial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmb4thOfficial.KeyPress
        Try
            cmb4thOfficial.DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub cmb4thOfficial_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmb4thOfficial.MouseClick
        Try
            cmb4thOfficial.DroppedDown = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Selects 4thOfficial  value and checks whether the selected value already assigned to any other referee
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmb4thOfficial_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb4thOfficial.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If (CInt(cmb4thOfficial.SelectedValue.ToString()) <> 0) Then
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmb4thOfficial.Text, 1, 0)
                ElseIf cmb4thOfficial.SelectedValue Is Nothing Then
                    MessageDialog.Show(strmessage3 & lbl4thOfficial.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmb5thOfficial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmb5thOfficial.KeyPress
        Try
            cmb5thOfficial.DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmb5thOfficial_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmb5thOfficial.MouseClick
        Try
            cmb5thOfficial.DroppedDown = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Selects 5thOfficial  value and checks whether the selected value already assigned to any other referee
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmb5thOfficial_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb5thOfficial.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If (CInt(cmb5thOfficial.SelectedValue.ToString()) <> 0) Then
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmb5thOfficial.Text, 1, 0)
                    If (strComboName <> "") Then
                        MessageDialog.Show(GetRefereeName(CInt(cmb5thOfficial.SelectedValue)) & " is already assigned as a " & strComboName & "!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Error)
                        cmb5thOfficial.SelectedIndex = 0
                        cmb5thOfficial.Focus()
                    ElseIf cmb5thOfficial.SelectedValue Is Nothing Then
                        MessageDialog.Show(strmessage3 & lbl5thOfficial.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                End If
                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Validates Referee Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbReferee_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbReferee.Validated
        Try
            If cmbReferee.Text <> "" And cmbReferee.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lblReferee.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbReferee.Text = ""
                cmbReferee.Focus()
                Exit Try
            End If

            Dim Str As String = ComborefereeSelectedChk(cmbReferee)

            If (Str <> Nothing) Then
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    Str = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Str + ":")
                End If
                MessageDialog.Show(message5 + " " & Str & " ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                cmbReferee.Text = ""
                cmbReferee.Focus()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Validates Linesman Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbLinesman_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinesman.Validated
        Try
            If cmbLinesman.Text <> "" And cmbLinesman.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lblLinesman.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbLinesman.Text = ""
                cmbLinesman.Focus()
                Exit Try
            End If

            Dim Str As String = ComborefereeSelectedChk(cmbLinesman)

            If (Str <> Nothing) Then
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    Str = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Str + ":")
                End If
                MessageDialog.Show(message5 + Str, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                cmbLinesman.Text = ""
                cmbLinesman.Focus()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Validates 2nd Linesman Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub CmbLinesman1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbLinesman1.Validated
        Try
            If CmbLinesman1.Text <> "" And CmbLinesman1.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lblLinesman1.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbLinesman1.Text = ""
                CmbLinesman1.Focus()
                Exit Try
            End If

            Dim Str As String = ComborefereeSelectedChk(CmbLinesman1)

            If (Str <> Nothing) Then
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    Str = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Str + ":")
                End If
                MessageDialog.Show(message5 + Str, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                CmbLinesman1.Text = ""
                CmbLinesman1.Focus()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Validates 4th Official Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmb4thOfficial_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb4thOfficial.Validated
        Try
            If cmb4thOfficial.Text <> "" And cmb4thOfficial.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lbl4thOfficial.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmb4thOfficial.Text = ""
                cmb4thOfficial.Focus()
                Exit Try
            End If

            Dim Str As String = ComborefereeSelectedChk(cmb4thOfficial)

            If (Str <> Nothing) Then
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    Str = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Str + ":")
                End If
                MessageDialog.Show(message5 + Str, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                cmb4thOfficial.Text = ""
                cmb4thOfficial.Focus()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Validates 5th Official Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmb5thOfficial_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb5thOfficial.Validated
        Try
            If cmb5thOfficial.Text <> "" And cmb5thOfficial.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lbl5thOfficial.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmb5thOfficial.Text = ""
                cmb5thOfficial.Focus()
                Exit Try
            End If

            Dim strvalidation As String = ComborefereeSelectedChk(cmb5thOfficial)
            If (strvalidation <> Nothing) Then
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    strvalidation = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strvalidation + ":")
                End If
                MessageDialog.Show(message5 + strvalidation, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                cmb5thOfficial.Text = ""
                cmb5thOfficial.Focus()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbVenue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbVenue.KeyPress
        Try
            cmbVenue.DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Validates Venu Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbVenue_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVenue.Validated
        Try
            If cmbVenue.Text <> "" And cmbVenue.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lblVenue.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbVenue.Text = ""
                cmbVenue.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub mtxtDelay_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtxtDelay.Validated
        Try
            Try
                Dim drTime() As String
                drTime = mtxtDelay.Text.Split(CChar(":"))
                Select Case mtxtDelay.Text.Trim.Length
                    Case 1
                        mtxtDelay.Text = "00" & ":" & "00"
                    Case 2
                        mtxtDelay.Text = "00" & ":" & "0" & drTime(0).ToString()
                    Case 3
                        If (drTime(0).Trim.Length > 1) Then
                            mtxtDelay.Text = "00" & ":" & drTime(0).ToString()
                        ElseIf ((drTime(0).Trim.Length = 1)) Then
                            mtxtDelay.Text = "00" & ":" & "0" & drTime(0).ToString()
                        ElseIf (drTime(0).Trim.Length = 0) Then
                            mtxtDelay.Text = "00" & ":" & "00"
                        End If
                    Case 4
                        mtxtDelay.Text = "0" & drTime(0).Substring(0, 1) & ":" & drTime(0).Substring(1, 1) & drTime(1).ToString()
                End Select
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnAddOfficial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOfficial.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAddOfficial.Text, 1, 0)

            m_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
            'storing the values
            Dim ds As New DataSet
            ds = CreateAndAddDataColumn()

            'Loads the combo with the new officials that is added thro Add official
            RefreshAddOfficialData()

            'existing's values entered are retrived and displayed.
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    cmbReferee.SelectedValue = CInt(IIf(IsDBNull(ds.Tables(0).Rows(0)("REFEREE_ID")), 0, ds.Tables(0).Rows(0)("REFEREE_ID")).ToString)
                    cmbLinesman.SelectedValue = CInt(IIf(IsDBNull(ds.Tables(0).Rows(0)("REFEREE_AST_1_ID")), 0, ds.Tables(0).Rows(0)("REFEREE_AST_1_ID")).ToString)
                    CmbLinesman1.SelectedValue = CInt(IIf(IsDBNull(ds.Tables(0).Rows(0)("REFEREE_AST_2_ID")), 0, ds.Tables(0).Rows(0)("REFEREE_AST_2_ID")).ToString)
                    cmb4thOfficial.SelectedValue = CInt(IIf(IsDBNull(ds.Tables(0).Rows(0)("FOURTH_OFFICIAL_ID")), 0, ds.Tables(0).Rows(0)("FOURTH_OFFICIAL_ID")).ToString)
                    cmb5thOfficial.SelectedValue = CInt(IIf(IsDBNull(ds.Tables(0).Rows(0)("FIFTH_OFFICIAL_ID")), 0, ds.Tables(0).Rows(0)("FIFTH_OFFICIAL_ID")).ToString)
                    If (ds.Tables(0).Rows(0)("START_TIME_ACTUAL").ToString() <> "") Then
                        'Time Zone displayed based on the local time format
                        m_objGameDetails.DelayTime = CDate(ds.Tables(0).Rows(0)("START_TIME_ACTUAL"))
                        Dim Dt As DateTime
                        If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                            Dt = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
                        Else
                            Dt = CDate(m_objGameDetails.GameDate)
                        End If
                        If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                            txtKickoff.Text = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                        Else
                            txtKickoff.Text = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                        End If
                    End If

                    If (ds.Tables(0).Rows(0)("ATTENDANCE").ToString() <> "") Then
                        txtAttendance.Text = CStr(ds.Tables(0).Rows(0)("ATTENDANCE"))
                    End If

                    Dim timediff As TimeSpan
                    timediff = m_objGameDetails.DelayTime.Subtract(CDate(m_objGameDetails.GameDate))
                    If timediff.Hours < 0 Or timediff.Minutes < 0 Then
                        mtxtDelay.Text = "00:00"
                    Else
                        mtxtDelay.Text = Format(timediff.Hours, "00") & ":" & Format(timediff.Minutes, "00")
                    End If

                    If (ds.Tables(0).Rows(0)("HOME_START_SIDE").ToString().Trim <> "") Then
                        If ds.Tables(0).Rows(0)("HOME_START_SIDE").ToString = CChar("L") Then
                            cmbHomeStart.Text = "Left"
                        Else
                            cmbHomeStart.Text = "Right"
                        End If
                    Else
                        cmbHomeStart.Text = ""
                    End If
                    txtRating1.Text = ds.Tables(0).Rows(0)("REF_RATING_1").ToString()
                    txtRating2.Text = ds.Tables(0).Rows(0)("REF_RATING_2").ToString()
                    txtRating3.Text = ds.Tables(0).Rows(0)("REF_RATING_3").ToString()
                    'Arindam 1-May-12 Added 5/6th Official
                    txtRating5.Text = ds.Tables(0).Rows(0)("REF_RATING_5").ToString()
                    txtRating6.Text = ds.Tables(0).Rows(0)("REF_RATING_6").ToString()
                    txtMatchRating.Text = ds.Tables(0).Rows(0)("MATCH_RATING").ToString()
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbHomeStart_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbHomeStart.SelectedIndexChanged
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbHomeStart.Text, 1, 0)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtAttendance_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAttendance.KeyPress
        Try
            If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtAttendance_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAttendance.Validated
        Try
            'AUDIT TRAIL
            If txtAttendance.Text <> m_strAttendance Then
                If txtAttendance.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtAttendance.Text, lblAttendance.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtAttendance.Text & "' '", lblAttendance.Text, 1, 0)
                End If
            End If
            m_strAttendance = txtAttendance.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region " User Methods "

    ''' <summary>
    ''' Loads the data in all Combos
    ''' </summary>
    ''' <param name="GameSetup"></param>
    ''' <remarks></remarks>
    Private Sub LoadAllControl(ByVal GameSetup As DataSet)
        Try
            Dim dtReferee, dtLinesman, dtLinesman1, dt4thOfficial, dt5thOfficial, dt6thOfficial, dtweather As DataTable
            blnSkipSEChanged = True
            'inserting a blank row for display purpose
            'Dim drTempRow As DataRow
            'drTempRow = GameSetup.Tables(0).NewRow()
            'drTempRow(0) = 0
            'drTempRow(1) = ""
            'GameSetup.Tables(0).Rows.InsertAt(drTempRow, 0)

            Dim ds As New DataSet
            dtReferee = GameSetup.Tables(0).Copy
            dtReferee.TableName = "Table1"
            dtLinesman = GameSetup.Tables(0).Copy
            dtLinesman.TableName = "Table2"
            dtLinesman1 = GameSetup.Tables(0).Copy
            dtLinesman1.TableName = "Table3"
            dt4thOfficial = GameSetup.Tables(0).Copy
            dt4thOfficial.TableName = "Table4"
            dt5thOfficial = GameSetup.Tables(0).Copy
            dt5thOfficial.TableName = "Table5"
            dtweather = GameSetup.Tables(9).Copy
            dtweather.TableName = "Table6"
            dt6thOfficial = GameSetup.Tables(0).Copy
            dt6thOfficial.TableName = "Table7"

            ds.Tables.Add(dtReferee)
            ds.Tables.Add(dtLinesman)
            ds.Tables.Add(dtLinesman1)
            ds.Tables.Add(dt4thOfficial)
            ds.Tables.Add(dt5thOfficial)
            ds.Tables.Add(dtweather)
            ds.Tables.Add(dt6thOfficial)
            Dsref = ds.Copy()

            PopulateDropdowns(Dsref)
            blnSkipSEChanged = False

            'LOAD HOMESTART COMBO
            cmbHomeStart.Items.Clear()
            cmbHomeStart.Items.Add("")
            cmbHomeStart.Items.Add("Left")
            cmbHomeStart.Items.Add("Right")
            cmbHomeStart.SelectedIndex = 0

            If m_objGameDetails.CurrentPeriod > 2 Then
                cmbhomestartET.Items.Clear()
                cmbhomestartET.Items.Add("")
                cmbhomestartET.Items.Add("Left")
                cmbhomestartET.Items.Add("Right")
                cmbhomestartET.SelectedIndex = 0
            End If

            'LOAD ALL REFEREE AND VENUE COMBOS
            If GameSetup.Tables.Count > 0 Then
                'TimeZone to be Displayed based on his Local Time
                'txtKickoff.Text = CStr(m_objGameDetails.GameDate)
                'displaying the gamedate based on his local time Zone (Imp by Shirley)
                'Dim Dt As DateTime = CDate(m_objGameDetails.GameDate)
                Dim Dt As DateTime
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    'Dt = DateTimeHelper.GetDate(CStr(m_objGameDetails.GameDate), DateTimeHelper.InputFormat.CurrentFormat)
                    Dt = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
                    'Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt")
                Else
                    Dt = CDate(m_objGameDetails.GameDate)
                End If

                If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                    txtKickoff.Text = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                Else
                    txtKickoff.Text = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                End If

                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "(Auto Selected)" & txtAttendance.Text, lblAttendance.Text, 1, 0)

                'cmbVenue.SelectedValue = m_objGameDetails.FieldID
                cmbVenue.SelectedValue = 0

                'If CInt(cmbReferee.SelectedValue) = 0 Then
                '    txtRating1.Enabled = False
                'Else
                '    txtRating1.Enabled = True
                'End If

                'If CInt(cmbLinesman.SelectedValue) = 0 Then
                '    txtRating2.Enabled = False
                'Else
                '    txtRating2.Enabled = True
                'End If

                'If CInt(CmbLinesman1.SelectedValue) = 0 Then
                '    txtRating3.Enabled = False
                'Else
                '    txtRating3.Enabled = True
                'End If

                'If m_objBLTeamSetup.GetPeriodEndCount(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objGameDetails.CurrentPeriod) >= 2 Then
                '     txtMatchRating.Enabled= = true
                'Else

                GetEndGameCount()

                If m_objGameDetails.IsEndGame = True Or m_intEndGameCnt > 0 Then
                    txtMatchRating.Enabled = True

                    If CInt(cmbReferee.SelectedValue) = 0 Then
                        txtRating1.Enabled = False
                    Else
                        txtRating1.Enabled = True
                    End If

                    If CInt(cmbLinesman.SelectedValue) = 0 Then
                        txtRating2.Enabled = False
                    Else
                        txtRating2.Enabled = True
                    End If

                    If CInt(CmbLinesman1.SelectedValue) = 0 Then
                        txtRating3.Enabled = False
                    Else
                        txtRating3.Enabled = True
                    End If
                    'Arindam 1-May-12 Added for 5/6th official
                    If CInt(cmb5thOfficial.SelectedValue) = 0 Then
                        txtRating5.Enabled = False
                    Else
                        txtRating5.Enabled = True
                    End If

                    If CInt(cmb6thOfficial.SelectedValue) = 0 Then
                        txtRating6.Enabled = False
                    Else
                        txtRating6.Enabled = True
                    End If
                Else
                    txtMatchRating.Enabled = False
                    txtRating1.Enabled = False
                    txtRating2.Enabled = False
                    txtRating3.Enabled = False
                    'Arindam 1-May-12 Added 5/6th Official
                    txtRating5.Enabled = False
                    txtRating6.Enabled = False
                End If

                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, "(Auto Selected)" & cmbVenue.Text, lblVenue.Text, 1, 0)
            End If

            'If (GameSetup.Tables(4).Rows.Count > 0) Then
            FillrefereInfo(GameSetup)
            'End If
            'Arindam 30-Jul-09: For Module 2 games, there will be NO ENTRY for HOME LEFT/RIGHT
            'Arindam - Fill weather and Temp info.

            If m_objGameDetails.ModuleID = 2 Then
                cmbHomeStart.Visible = False
                lblHomeStart.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Inserts the selected referee and game info
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateAndAddDataColumn() As DataSet
        Try

            Dim dsSelectedData As New DataSet
            Dim strDelayTime() As String
            Dim DelayTime As Double
            dsSelectedData.Tables.Add()

            dsSelectedData.Tables(0).Columns.Add("GAME_CODE")
            dsSelectedData.Tables(0).Columns.Add("FEED_NUMBER")
            dsSelectedData.Tables(0).Columns.Add("LEAGUE_ID")
            dsSelectedData.Tables(0).Columns.Add("FEED_GAME_CODE")
            dsSelectedData.Tables(0).Columns.Add("VENUE_ID")
            dsSelectedData.Tables(0).Columns.Add("ATTENDANCE")
            dsSelectedData.Tables(0).Columns.Add("START_TIME_ACTUAL")
            dsSelectedData.Tables(0).Columns.Add("WEATHER")
            dsSelectedData.Tables(0).Columns.Add("WEATHER_ID")
            dsSelectedData.Tables(0).Columns.Add("TEMPERATURE_F")
            dsSelectedData.Tables(0).Columns.Add("TEMPERATURE_C")
            dsSelectedData.Tables(0).Columns.Add("REFEREE_ID")
            dsSelectedData.Tables(0).Columns.Add("REFEREE_AST_1_ID")
            dsSelectedData.Tables(0).Columns.Add("REFEREE_AST_2_ID")
            dsSelectedData.Tables(0).Columns.Add("FOURTH_OFFICIAL_ID")
            dsSelectedData.Tables(0).Columns.Add("FIFTH_OFFICIAL_ID")
            dsSelectedData.Tables(0).Columns.Add("SIXTH_OFFICIAL_ID")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE_ET")
            dsSelectedData.Tables(0).Columns.Add("MATCH_DONE")
            dsSelectedData.Tables(0).Columns.Add("SHOOTOUT")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE_TOUCH")
            dsSelectedData.Tables(0).Columns.Add("HOME_START_SIDE_TOUCH_ET")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_1")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_2")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_3")
            'aRINDAM 1-May-12 5/6 th official ratins added
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_5")
            dsSelectedData.Tables(0).Columns.Add("REF_RATING_6")
            dsSelectedData.Tables(0).Columns.Add("MATCH_RATING")
            dsSelectedData.Tables(0).Columns.Add("PROCESSED")
            dsSelectedData.Tables(0).Columns.Add("DEMO_DATA")

            Dim drSelecteDataGamesetup As DataRow
            drSelecteDataGamesetup = dsSelectedData.Tables(0).NewRow()

            drSelecteDataGamesetup("GAME_CODE") = m_objGameDetails.GameCode
            drSelecteDataGamesetup("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drSelecteDataGamesetup("LEAGUE_ID") = m_objGameDetails.LeagueID
            drSelecteDataGamesetup("VENUE_ID") = IIf(CInt(cmbVenue.SelectedValue) = 0, DBNull.Value, CInt(cmbVenue.SelectedValue))
            drSelecteDataGamesetup("ATTENDANCE") = IIf(txtAttendance.Text = "", DBNull.Value, txtAttendance.Text)

            If mtxtDelay.Text <> "00:00" Then
                strDelayTime = mtxtDelay.Text.Split(CChar(":"))
                DelayTime = CDbl(strDelayTime(0)) * 60 + CDbl(strDelayTime(1))
                Dim dtDate As DateTime = m_objGameDetails.DelayTime.AddMinutes(DelayTime)
                drSelecteDataGamesetup("START_TIME_ACTUAL") = DateTimeHelper.GetDateString(dtDate, DateTimeHelper.OutputFormat.ISOFormat)
                'm_objGameDetails.DelayTime = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))  'DateTime.ParseExact(m_objGameDetails.GameDate, "MM/dd/yyyy HH:mm:ss am", System.Globalization.CultureInfo.InvariantCulture).Date
                'drSelecteDataGamesetup("START_TIME_ACTUAL") = m_objGameDetails.DelayTime.AddMinutes(DelayTime).ToString("yyyy-MM-dd HH:mm:ss tt")
            Else
                'drSelecteDataGamesetup("START_TIME_ACTUAL") = Format(CDate(m_objGameDetails.GameDate), "yyyy-MM-dd HH:mm:ss tt")
                drSelecteDataGamesetup("START_TIME_ACTUAL") = DateTimeHelper.GetDateString(CDate(m_objGameDetails.GameDate), DateTimeHelper.OutputFormat.ISOFormat)
            End If
            m_objGameDetails.DelayTime = CDate(drSelecteDataGamesetup("START_TIME_ACTUAL"))
            drSelecteDataGamesetup("REFEREE_ID") = IIf(CInt(cmbReferee.SelectedValue) = 0, DBNull.Value, CInt(cmbReferee.SelectedValue))
            drSelecteDataGamesetup("REFEREE_AST_1_ID") = IIf(CInt(cmbLinesman.SelectedValue) = 0, DBNull.Value, CInt(cmbLinesman.SelectedValue))
            drSelecteDataGamesetup("REFEREE_AST_2_ID") = IIf(CInt(CmbLinesman1.SelectedValue) = 0, DBNull.Value, CInt(CmbLinesman1.SelectedValue))
            drSelecteDataGamesetup("FOURTH_OFFICIAL_ID") = IIf(CInt(cmb4thOfficial.SelectedValue) = 0, DBNull.Value, CInt(cmb4thOfficial.SelectedValue))
            drSelecteDataGamesetup("FIFTH_OFFICIAL_ID") = IIf(CInt(cmb5thOfficial.SelectedValue) = 0, DBNull.Value, CInt(cmb5thOfficial.SelectedValue))
            drSelecteDataGamesetup("SIXTH_OFFICIAL_ID") = IIf(CInt(cmb6thOfficial.SelectedValue) = 0, DBNull.Value, CInt(cmb6thOfficial.SelectedValue))

            'Weather and temp
            drSelecteDataGamesetup("WEATHER") = cmbWeather.Text
            drSelecteDataGamesetup("WEATHER_ID") = IIf(CInt(cmbWeather.SelectedValue) = 0, DBNull.Value, CInt(cmbWeather.SelectedValue))
            If cmbCorF.Text = "F" Then
                drSelecteDataGamesetup("TEMPERATURE_F") = IIf(txtTemp.Text = "", DBNull.Value, txtTemp.Text)
            Else
                drSelecteDataGamesetup("TEMPERATURE_C") = IIf(txtTemp.Text = "", DBNull.Value, txtTemp.Text)
            End If

            'Arindam - Module 2 NO NEED TO CHOOSE HOME TEM SIDE - DEFAULT TO LEFT ALWAYS (30-Jul-09)
            If m_objGameDetails.ModuleID = 2 Then
                drSelecteDataGamesetup("HOME_START_SIDE") = "L"
                m_objGameDetails.IsHomeTeamOnLeft = True
            Else
                '14-Mar-2012 added code to update extra time home start informtion

                If cmbHomeStart.Text = "Right" Then
                    drSelecteDataGamesetup("HOME_START_SIDE") = "R"
                    m_objGameDetails.IsHomeTeamOnLeft = False
                ElseIf cmbHomeStart.Text = "Left" Then
                    drSelecteDataGamesetup("HOME_START_SIDE") = "L"
                    m_objGameDetails.IsHomeTeamOnLeft = True
                End If

                If m_objGameDetails.CurrentPeriod > 2 Then
                    '14-Mar-2012 added code to update extra time home start informtion
                    If cmbhomestartET.Text = "Right" Then
                        drSelecteDataGamesetup("HOME_START_SIDE_ET") = "R"
                        m_objGameDetails.IsHomeTeamOnLeft = False
                    ElseIf cmbhomestartET.Text = "Left" Then
                        drSelecteDataGamesetup("HOME_START_SIDE_ET") = "L"
                        m_objGameDetails.IsHomeTeamOnLeft = True
                        'ElseIf cmbhomestartET.Text = "" Then
                        '    drSelecteDataGamesetup("HOME_START_SIDE_ET") = DBNull.Value
                    End If
                    'Else
                    '    drSelecteDataGamesetup("HOME_START_SIDE_ET") = DBNull.Value
                End If
            End If

            If m_objGameDetails.CurrentPeriod <= 1 Then
                If m_objGameDetails.ModuleID = 1 Then
                    m_objGameDetails.isHomeDirectionLeft = m_objGameDetails.IsHomeTeamOnLeft
                End If
            End If

            'drSelecteDataGamesetup("REF_RATING_1") = IIf(txtRating1.Text = "", DBNull.Value, txtRating1.Text)
            'drSelecteDataGamesetup("REF_RATING_2") = IIf(txtRating2.Text = "", DBNull.Value, txtRating2.Text)
            'drSelecteDataGamesetup("REF_RATING_3") = IIf(txtRating3.Text = "", DBNull.Value, txtRating3.Text)

            drSelecteDataGamesetup("REF_RATING_1") = IIf(txtRating1.Text = "", DBNull.Value, txtRating1.Text)
            drSelecteDataGamesetup("REF_RATING_2") = IIf(txtRating2.Text = "", DBNull.Value, txtRating2.Text)
            drSelecteDataGamesetup("REF_RATING_3") = IIf(txtRating3.Text = "", DBNull.Value, txtRating3.Text)
            'Arindam 1-May-12 5/6th Official
            drSelecteDataGamesetup("REF_RATING_5") = IIf(txtRating5.Text = "", DBNull.Value, txtRating5.Text)
            drSelecteDataGamesetup("REF_RATING_6") = IIf(txtRating6.Text = "", DBNull.Value, txtRating6.Text)
            drSelecteDataGamesetup("MATCH_RATING") = IIf(txtMatchRating.Text = "", DBNull.Value, txtMatchRating.Text)

            drSelecteDataGamesetup("PROCESSED") = "N"

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                drSelecteDataGamesetup("DEMO_DATA") = "N"
            Else
                drSelecteDataGamesetup("DEMO_DATA") = "Y"
            End If

            dsSelectedData.Tables(0).Rows.Add(drSelecteDataGamesetup)
            Return dsSelectedData

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Inserts the Team Logos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DisplayTeamLogoandTeamAbbrev(ByVal GameSetup As DataSet)
        Try
            Dim v_memLogo As MemoryStream

            Dim mstrHomeLogo As MemoryStream = Nothing
            Dim mstrAwayLogo As MemoryStream = Nothing

            lblAwayTeam.Text = m_objGameDetails.AwayTeam.ToString()
            lblHomeTeam.Text = m_objGameDetails.HomeTeam.ToString()

            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID)
            If v_memLogo IsNot Nothing Then
                picAwayLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, m_objGameDetails.HomeTeamID)
            If v_memLogo IsNot Nothing Then
                picHomeLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If

            'If m_dsGameSetup.Tables(2).Rows.Count > 0 Then
            '    mstrHomeLogo = New System.IO.MemoryStream(CType(m_dsGameSetup.Tables(2).Rows(0)(0), Byte()))
            '    If Not mstrHomeLogo Is Nothing Then
            '        picHomeLogo.Image = New Bitmap(mstrHomeLogo)
            '        picHomeLogo.Image.RotateFlip(RotateFlipType.Rotate180FlipNone)
            '    End If
            'End If

            'If m_dsGameSetup.Tables(1).Rows.Count > 0 Then
            '    mstrAwayLogo = New System.IO.MemoryStream(CType(m_dsGameSetup.Tables(1).Rows(0)(0), Byte()))

            '    If Not mstrAwayLogo Is Nothing Then
            '        picAwayLogo.Image = New Bitmap(mstrAwayLogo)
            '        picAwayLogo.Image.RotateFlip(RotateFlipType.Rotate180FlipNone)
            '    End If
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''loading the referees every time, whenever there is any change in combo's
    'Private Sub LoadFilteredReferees()
    '    Try
    '        Dim Referee, Linesman, Linesman1, int4thOfficial, int5thOfficial As Integer
    '        If cmbReferee.Text <> "" Then
    '            Referee = CInt(cmbReferee.SelectedValue)
    '        End If
    '        If cmbLinesman.Text <> "" Then
    '            Linesman = CInt(cmbLinesman.SelectedValue)
    '        End If
    '        If CmbLinesman1.Text <> "" Then
    '            Linesman1 = CInt(CmbLinesman1.SelectedValue)
    '        End If
    '        If cmb4thOfficial.Text <> "" Then
    '            int4thOfficial = CInt(cmb4thOfficial.SelectedValue)
    '        End If
    '        If cmb5thOfficial.Text <> "" Then
    '            int5thOfficial = CInt(cmb5thOfficial.SelectedValue)
    '        End If

    '        FilterSelectedReferees(m_dsGameSetup, CInt(Referee), cmbReferee)
    '        blnSkipSEChanged = True
    '        cmbReferee.SelectedValue = Referee
    '        blnSkipSEChanged = False

    '        FilterSelectedReferees(m_dsGameSetup, CInt(Linesman), cmbLinesman)
    '        blnSkipSEChanged = True
    '        cmbLinesman.SelectedValue = Linesman
    '        blnSkipSEChanged = False

    '        FilterSelectedReferees(m_dsGameSetup, CInt(Linesman1), CmbLinesman1)
    '        blnSkipSEChanged = True
    '        CmbLinesman1.SelectedValue = Linesman1
    '        blnSkipSEChanged = False

    '        FilterSelectedReferees(m_dsGameSetup, CInt(int4thOfficial), cmb4thOfficial)
    '        blnSkipSEChanged = True
    '        cmb4thOfficial.SelectedValue = int4thOfficial
    '        blnSkipSEChanged = False

    '        FilterSelectedReferees(m_dsGameSetup, CInt(int5thOfficial), cmb5thOfficial)
    '        blnSkipSEChanged = True
    '        cmb5thOfficial.SelectedValue = int5thOfficial
    '        blnSkipSEChanged = False

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''' <summary>
    ''' Displays the selected GameSetup inforamtion
    ''' </summary>
    ''' <param name="GameSetUpdata"></param>
    ''' <remarks></remarks>
    Private Sub FillrefereInfo(ByVal GameSetUpdata As DataSet)
        Try
            If (GameSetUpdata.Tables(4).Rows.Count > 0) Then
                If (GameSetUpdata.Tables(4).Rows(0)("REFEREE_ID").ToString() <> "") Then
                    cmbReferee.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("REFEREE_ID").ToString()
                End If
                If (GameSetUpdata.Tables(4).Rows(0)("REFEREE_AST_1_ID").ToString() <> "") Then
                    cmbLinesman.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("REFEREE_AST_1_ID")
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("REFEREE_AST_2_ID").ToString() <> "") Then
                    CmbLinesman1.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("REFEREE_AST_2_ID")
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("FOURTH_OFFICIAL_ID").ToString() <> "") Then
                    cmb4thOfficial.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("FOURTH_OFFICIAL_ID")
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("FIFTH_OFFICIAL_ID").ToString() <> "") Then
                    cmb5thOfficial.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("FIFTH_OFFICIAL_ID")
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("SIXTH_OFFICIAL_ID").ToString() <> "") Then
                    cmb6thOfficial.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("SIXTH_OFFICIAL_ID")
                End If
                If (GameSetUpdata.Tables(4).Rows(0)("START_TIME_ACTUAL").ToString() <> "") Then
                    'txtKickoff.Text = CStr(m_objGameDetails.GameDate)
                    'CStr(GameSetUpdata.Tables(4).Rows(0)("START_TIME_ACTUAL"))
                    'Time Zone displayed based on the local time format
                    m_objGameDetails.DelayTime = CDate(GameSetUpdata.Tables(4).Rows(0)("START_TIME_ACTUAL"))
                    Dim Dt As DateTime
                    If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                        'Dt = DateTimeHelper.GetDate(CStr(m_objGameDetails.GameDate), DateTimeHelper.InputFormat.CurrentFormat)
                        Dt = CDate(Format(m_objGameDetails.GameDate, "yyyy-MM-dd HH:mm:ss tt"))
                    Else
                        Dt = CDate(m_objGameDetails.GameDate)
                    End If

                    'Dim Dt As DateTime = DateTimeHelper.GetDate(CStr(m_objGameDetails.GameDate), DateTimeHelper.InputFormat.CurrentFormat)
                    If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                        txtKickoff.Text = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                    Else
                        txtKickoff.Text = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                    End If
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("ATTENDANCE").ToString() <> "") Then
                    txtAttendance.Text = CStr(GameSetUpdata.Tables(4).Rows(0)("ATTENDANCE"))
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("WEATHER_ID").ToString() <> "") Then
                    cmbWeather.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("WEATHER_ID")
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("TEMPERATURE_F").ToString() <> "") Then
                    txtTemp.Text = CStr(GameSetUpdata.Tables(4).Rows(0)("TEMPERATURE_F"))
                    cmbCorF.Text = "F"
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("TEMPERATURE_C").ToString() <> "") Then
                    txtTemp.Text = CStr(GameSetUpdata.Tables(4).Rows(0)("TEMPERATURE_C"))
                    cmbCorF.Text = "C"
                End If
                'Dim timediff As TimeSpan
                'timediff = m_objGameDetails.DelayTime.Subtract(CDate(m_objGameDetails.GameDate))
                'If timediff.Hours < 0 Or timediff.Minutes < 0 Then
                '    mtxtDelay.Text = "00:00"
                'Else
                '    mtxtDelay.Text = Format(timediff.Hours, "00") & ":" & Format(timediff.Minutes, "00")
                'End If

                Dim timediff As TimeSpan
                timediff = m_objGameDetails.DelayTime.Subtract(CDate(m_objGameDetails.GameDate))
                If timediff.Hours < 0 Or timediff.Minutes < 0 Then
                    mtxtDelay.Text = "00:00"
                Else
                    mtxtDelay.Text = Format(timediff.Hours, "00") & ":" & Format(timediff.Minutes, "00")
                End If

                If (GameSetUpdata.Tables(4).Rows(0)("HOME_START_SIDE").ToString().Trim <> "") Then
                    If GameSetUpdata.Tables(4).Rows(0)("HOME_START_SIDE").ToString = CChar("L") Then
                        cmbHomeStart.Text = "Left"
                    Else
                        cmbHomeStart.Text = "Right"
                    End If
                Else
                    cmbHomeStart.Text = ""
                    'frmModule1Main.lvwMatchInformation.Items(4).SubItems(1).Text = cmbHomeStart.Text
                End If

                If m_objGameDetails.CurrentPeriod > 2 Then
                    If (GameSetUpdata.Tables(4).Rows(0)("HOME_START_SIDE_ET").ToString().Trim <> "") Then
                        If GameSetUpdata.Tables(4).Rows(0)("HOME_START_SIDE_ET").ToString = CChar("L") Then
                            cmbhomestartET.Text = "Left"
                        Else
                            cmbhomestartET.Text = "Right"
                        End If
                    Else
                        cmbhomestartET.Text = ""
                        'frmModule1Main.lvwMatchInformation.Items(4).SubItems(1).Text = cmbHomeStart.Text
                    End If
                End If

                'If CInt(cmbReferee.SelectedValue) = 0 Then
                '    txtRating1.Enabled = False
                'Else
                '    txtRating1.Enabled = True
                'End If

                'If CInt(cmbLinesman.SelectedValue) = 0 Then
                '    txtRating2.Enabled = False
                'Else
                '    txtRating2.Enabled = True
                'End If

                'If CInt(CmbLinesman1.SelectedValue) = 0 Then
                '    txtRating3.Enabled = False
                'Else
                '    txtRating3.Enabled = True
                'End If

                txtRating1.Text = GameSetUpdata.Tables(4).Rows(0)("REF_RATING_1").ToString()
                txtRating2.Text = GameSetUpdata.Tables(4).Rows(0)("REF_RATING_2").ToString()
                txtRating3.Text = GameSetUpdata.Tables(4).Rows(0)("REF_RATING_3").ToString()
                'Arindam 1-May-12 5/6th Official added
                txtRating5.Text = GameSetUpdata.Tables(4).Rows(0)("REF_RATING_5").ToString()
                txtRating6.Text = GameSetUpdata.Tables(4).Rows(0)("REF_RATING_6").ToString()
                txtMatchRating.Text = GameSetUpdata.Tables(4).Rows(0)("MATCH_RATING").ToString()
            End If

            'displaying venue
            If (GameSetUpdata.Tables(4).Rows.Count > 0) Then 'checking from live_soc_game table
                If (GameSetUpdata.Tables(4).Rows(0)("VENUE_ID").ToString() <> "") Then
                    'cmbVenue.Items.Clear()
                    LoadControl(cmbVenue, GameSetUpdata.Tables(7), "FIELD_NAME", "FIELD_ID")
                    cmbVenue.SelectedValue = GameSetUpdata.Tables(4).Rows(0)("VENUE_ID")
                ElseIf GameSetUpdata.Tables(5).Rows.Count > 0 Then
                    If GameSetUpdata.Tables(5).Rows(0)("FIELD_ID").ToString() <> "" Then
                        'cmbVenue.Items.Clear()
                        LoadControl(cmbVenue, GameSetUpdata.Tables(8), "FIELD_NAME", "FIELD_ID")
                        cmbVenue.SelectedValue = GameSetUpdata.Tables(5).Rows(0)("FIELD_ID")
                    End If
                Else
                    'cmbVenue.Items.Clear()
                    LoadControl(cmbVenue, GameSetUpdata.Tables(6), "FIELD_NAME", "FIELD_ID")
                    cmbVenue.SelectedValue = 0
                End If
            ElseIf GameSetUpdata.Tables(5).Rows.Count > 0 Then
                If GameSetUpdata.Tables(5).Rows(0)("FIELD_ID").ToString() <> "" Then
                    'cmbVenue.Items.Clear()
                    LoadControl(cmbVenue, GameSetUpdata.Tables(8), "FIELD_NAME", "FIELD_ID")
                    cmbVenue.SelectedValue = GameSetUpdata.Tables(5).Rows(0)("FIELD_ID")
                End If
            Else
                'cmbVenue.Items.Clear()
                LoadControl(cmbVenue, GameSetUpdata.Tables(6), "FIELD_NAME", "FIELD_ID")
                cmbVenue.SelectedValue = 0
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''assigning all the Id's which is selected in combos's
    'Private Sub GetReferees()
    '    Try
    '        'set Selected column to NULL , and then update the to "Y" based on the referee id's selected
    '        SetRefDatasetToNull()
    '        If cmbReferee.Text <> "" Then
    '            'update selected column to "y" for the referee selected.
    '            UpdateRefDataset(CInt(cmbReferee.SelectedValue))
    '        End If
    '        If cmbLinesman.Text <> "" Then
    '            UpdateRefDataset(CInt(cmbLinesman.SelectedValue))
    '        End If
    '        If CmbLinesman1.Text <> "" Then
    '            UpdateRefDataset(CInt(CmbLinesman1.SelectedValue))
    '        End If
    '        If cmb4thOfficial.Text <> "" Then
    '            UpdateRefDataset(CInt(cmb4thOfficial.SelectedValue))
    '        End If
    '        If cmb5thOfficial.Text <> "" Then
    '            UpdateRefDataset(CInt(cmb5thOfficial.SelectedValue))
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''set Selected column to NULL for all the refress which is set to Selected  ="y"
    'Private Sub SetRefDatasetToNull()
    '    Try
    '        Dim drs() As DataRow = m_dsGameSetup.Tables(0).Select("Selected is not null")
    '        If drs.Length > 0 Then
    '            For Each dr As DataRow In drs
    '                dr.Item("selected") = DBNull.Value
    '            Next
    '        End If
    '        m_dsGameSetup.Tables(0).AcceptChanges()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''update the dataset with the selected column to "y" based on the referre selected
    'Private Sub UpdateRefDataset(ByVal RefID As Integer)
    '    Try
    '        Dim DRS() As DataRow = m_dsGameSetup.Tables(0).Select("REF_ID = " & RefID & "")
    '        If DRS.Length > 0 Then
    '            DRS(0).BeginEdit()
    '            DRS(0).Item("SELECTED") = "Y"
    '            DRS(0).EndEdit()
    '        End If
    '        m_dsGameSetup.Tables(0).AcceptChanges()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    '' Filters the selected refree and includes the refree selected from combo..
    '' reloads the combo's with the new list
    'Private Sub FilterSelectedReferees(ByVal DsRefrees As DataSet, ByVal RefID As Integer, ByVal objcom As ComboBox)
    '    Try

    '        Dim drs() As DataRow
    '        Dim m_dtRefs As DataTable
    '        drs = DsRefrees.Tables(0).Select("SELECTED IS NULL")
    '        If drs.Length > 0 Then
    '            m_dtRefs = DsRefrees.Tables(0).Clone()
    '            Dim DR As DataRow
    '            'fetching only the players who are not used in Lineups
    '            For Each DR In drs
    '                m_dtRefs.ImportRow(DR)
    '            Next
    '            'inserting the selected player in the dataset to bind
    '            drs = DsRefrees.Tables(0).Select("REF_ID = " & RefID & "")
    '            For Each DR In drs
    '                m_dtRefs.ImportRow(DR)
    '            Next
    '            'sorted based on last name
    '            Dim dsRefs As DataTable
    '            dsRefs = m_dtRefs.Clone()
    '            drs = m_dtRefs.Select("", "REFEREENAME")
    '            For Each DR In drs
    '                dsRefs.ImportRow(DR)
    '            Next
    '            blnSkipSEChanged = True
    '            LoadControl(objcom, dsRefs, "REF_ID", "REFEREENAME")
    '            'inserting a blank row to the top
    '            blnSkipSEChanged = False

    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''' <summary>
    ''' Checking whether referee already selected or not
    ''' </summary>
    ''' <param name="Referee">Represents Combo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComborefereeSelectedChk(ByVal Referee As ComboBox) As String
        Try
            If (cmbReferee.Name <> Referee.Name) And cmbReferee.SelectedIndex > 0 Then
                If (Referee.SelectedValue.ToString() = cmbReferee.SelectedValue.ToString()) Then
                    Return "Referee"
                End If
            End If
            If (cmbLinesman.Name <> Referee.Name) And cmbLinesman.SelectedIndex > 0 Then
                If (Referee.SelectedValue.ToString() = cmbLinesman.SelectedValue.ToString()) Then
                    Return "Linesman"
                End If
            End If
            If (CmbLinesman1.Name <> Referee.Name) And CmbLinesman1.SelectedIndex > 0 Then
                If (Referee.SelectedValue.ToString() = CmbLinesman1.SelectedValue.ToString()) Then
                    Return "Linesman"
                End If
            End If
            If (cmb4thOfficial.Name <> Referee.Name) And cmb4thOfficial.SelectedIndex > 0 Then
                If (Referee.SelectedValue.ToString() = cmb4thOfficial.SelectedValue.ToString()) Then
                    Return "4th Official"
                End If
            End If
            If (cmb5thOfficial.Name <> Referee.Name) And cmb5thOfficial.SelectedIndex > 0 Then
                If (Referee.SelectedValue.ToString() = cmb5thOfficial.SelectedValue.ToString()) Then
                    Return "5th Official"
                End If
            End If

            Return Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Gets the Referee Name
    ''' </summary>
    ''' <param name="intRefeID">Represents an integer</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetRefereeName(ByVal intRefeID As Integer) As String
        Try
            Dim drRefereename() As DataRow = m_objGameDetails.GameSetup.Tables(0).Select("REF_ID=" & intRefeID)
            If (drRefereename.Length > 0) Then
                Return (drRefereename(0).Item("REFEREENAME").ToString())
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Validates Gamesetup info
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateGameSetup() As Boolean
        Try
            Dim intValidateOfficials As Integer = 0
            Dim strMessage As String = ""

            'TOPZ-1443 Extra Time issue
            If m_objGameDetails.ModuleID = 3 Then
                If m_objGameDetails.CurrentPeriod = 1 And cmbHomeStart.Text = "" Then
                    MessageDialog.Show("Home Start not entered.Please check!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                End If
                If m_objGameDetails.CurrentPeriod = 3 And cmbhomestartET.Text = "" Then
                    MessageDialog.Show("Home Start ET not entered.Please check!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                End If
            End If

            If CInt(cmbReferee.SelectedValue) = 0 Then
                If m_objGameDetails.ModuleID = 1 Then
                    If cmbHomeStart.Text = "" Then
                        strMessage = strMessage & "Home Start not entered" & vbNewLine
                    End If
                End If
                strMessage = strMessage & "Required Officials not entered" & vbNewLine
                intValidateOfficials = intValidateOfficials + 1
            End If
            If (txtAttendance.Text <> "") Then
                If (cmbVenue.Text <> "") Then
                    Dim drLoadData() As DataRow
                    drLoadData = m_objGameDetails.GameSetup.Tables(3).Select("FIELD_ID=" & cmbVenue.SelectedValue.ToString())
                    If (drLoadData.Length > 0) Then
                        If Not IsDBNull(drLoadData(0).Item("CAPACITY_STANDARD")) Then
                            If (CInt(drLoadData(0).Item("CAPACITY_STANDARD").ToString()) < CInt(txtAttendance.Text)) Then
                                If MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                                Else
                                    txtAttendance.Focus()
                                    Return False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            If intValidateOfficials > 0 Then
                If MessageDialog.Show(strMessage & vbNewLine & "Do you want to continue ?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Return True
                Else
                    If CInt(cmbReferee.SelectedValue) = 0 Then
                        cmbReferee.Focus()
                    ElseIf CInt(cmbLinesman.SelectedValue) = 0 Then
                        cmbLinesman.Focus()
                    ElseIf CInt(CmbLinesman1.SelectedValue) = 0 Then
                        CmbLinesman1.Focus()
                    ElseIf CInt(cmb4thOfficial.SelectedValue) = 0 Then
                        cmb4thOfficial.Focus()
                    End If
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            If Combo.Name <> cmbVenue.Name Then
                Combo.DropDownStyle = ComboBoxStyle.DropDown
                Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
                Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                Combo.AutoCompleteCustomSource = acscRosterData
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub RefreshAddOfficialData()
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAddOfficial.Text, 1, 0)

            Dim objAddOfficial As New frmAddOfficial
            objAddOfficial.ShowDialog()

            ''GET OFFICIAL DATA AFTER ADDING NEW OFFICIAL
            m_dsGameSetup.Tables.Clear()
            m_dsGameSetup = m_objGameSetup.SelectGameSetup(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID, m_objGameDetails.HomeTeamID, m_objGameDetails.languageid, m_objGameDetails.CoverageLevel, m_objGameDetails.ModuleID)
            m_objGameDetails.GameSetup = m_dsGameSetup
            LoadAllControl(m_dsGameSetup)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetEndGameCount()
        Try
            Dim dsEndGameCnt As New DataSet
            dsEndGameCnt = m_objGeneral.getEndGameCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            If Not dsEndGameCnt Is Nothing Then
                If dsEndGameCnt.Tables(0).Rows.Count > 0 Then
                    m_intEndGameCnt = CInt(dsEndGameCnt.Tables(0).Rows(0).Item(0).ToString())
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub txtAttendance_DragLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAttendance.DragLeave
        Try
            If (txtAttendance.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtAttendance.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub mtxtDelay_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mtxtDelay.Leave
        Try
            If (mtxtDelay.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, mtxtDelay.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbVenue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVenue.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbVenue.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtKickoff_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKickoff.Leave
        Try
            If (txtKickoff.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtKickoff.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function IsPBPEntryAllowed() As Boolean
        Try
            'CHECK CURRENT REPORTER ROLE
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                Return True
            Else
                If m_objGameDetails.ModuleID <> 3 Then
                    If m_objGameDetails.IsPrimaryReporterDown Then
                        MessageDialog.Show(message4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Return False
                    ElseIf m_objGameDetails.AssistersLastEntryStatus = False Then
                        MessageDialog.Show(message3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Return True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub txtRating1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRating1.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtRating2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRating2.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtRating3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRating3.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtRating5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRating5.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtRating6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRating6.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtMatchRating_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMatchRating.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub ValidateRatings(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRating1.Validated, txtRating2.Validated, txtRating3.Validated, txtRating5.Validated, txtRating6.Validated
        Try
            If Not txtRating1.Text = "" Then
                If CInt(txtRating1.Text) > 10 Or CInt(txtRating1.Text) <= 0 Then
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtRating1.Text = ""
                    txtRating1.Focus()
                End If
            ElseIf Not txtRating2.Text = "" Then
                If CInt(txtRating2.Text) > 10 Or CInt(txtRating2.Text) <= 0 Then
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtRating2.Text = ""
                    txtRating2.Focus()
                End If
            ElseIf Not txtRating3.Text = "" Then
                If CInt(txtRating3.Text) > 10 Or CInt(txtRating3.Text) <= 0 Then
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtRating3.Text = ""
                    txtRating3.Focus()
                End If
            ElseIf Not txtRating5.Text = "" Then 'Arindam 1-May-2 5/6 official ratings added
                If CInt(txtRating5.Text) > 10 Or CInt(txtRating5.Text) <= 0 Then
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtRating5.Text = ""
                    txtRating5.Focus()
                End If
            ElseIf Not txtRating6.Text = "" Then 'Arindam 1-May-2 5/6 official ratings added
                If CInt(txtRating6.Text) > 10 Or CInt(txtRating6.Text) <= 0 Then
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtRating6.Text = ""
                    txtRating6.Focus()
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtMatchRating_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMatchRating.Validated
        Try
            If Not txtMatchRating.Text = "" Then
                If CInt(txtMatchRating.Text) > 10 Or CInt(txtMatchRating.Text) <= 0 Then
                    MessageDialog.Show(message2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtMatchRating.Text = ""
                    txtMatchRating.Focus()
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtTemp_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTemp.KeyPress
        Try
            If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) And (Asc(e.KeyChar) < 47 Or Asc(e.KeyChar) > 57) Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub cmb6thOfficial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmb6thOfficial.KeyPress
        Try
            cmb6thOfficial.DroppedDown = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmb6thOfficial_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmb6thOfficial.MouseClick
        Try
            cmb6thOfficial.DroppedDown = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmb6thOfficial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb6thOfficial.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If (CInt(cmb6thOfficial.SelectedValue.ToString()) <> 0) Then
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmb6thOfficial.Text, 1, 0)
                    If (strComboName <> "") Then
                        MessageDialog.Show(GetRefereeName(CInt(cmb6thOfficial.SelectedValue)) & " is already assigned as a " & strComboName & "!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Error)
                        cmb6thOfficial.SelectedIndex = 0
                        cmb6thOfficial.Focus()
                    ElseIf cmb6thOfficial.SelectedValue Is Nothing Then
                        MessageDialog.Show(strmessage3 & lbl6thOfficial.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                End If
                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmb6thOfficial_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb6thOfficial.Validated
        Try
            If cmb6thOfficial.Text <> "" And cmb6thOfficial.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage2 & lbl6thOfficial.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmb6thOfficial.Text = ""
                cmb6thOfficial.Focus()
                Exit Try
            End If

            Dim Str As String = ComborefereeSelectedChk(cmb6thOfficial)

            If (Str <> Nothing) Then
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    Str = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Str + ":")
                End If
                MessageDialog.Show(message5 + Str, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                cmb6thOfficial.Text = ""
                cmb6thOfficial.Focus()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

End Class