﻿#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.IO
Imports System.Linq
Imports System.Drawing.Graphics
Imports System.Drawing.Imaging
Imports System.Drawing.Design
#End Region
Public Class frmActivateGames
    Public returnString As Boolean = False
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private m_objGameSetup As clsGameSetup = clsGameSetup.GetInstance()
    Private _objUtilAudit As New clsAuditLog
    Private MessageDialog As New frmMessageDialog
    Dim period As Boolean = False
    Private ReadOnly _objGameDetails As clsGameDetails = clsGameDetails.GetInstance()

    Private Sub btnActivateGame_Click(sender As Object, e As EventArgs) Handles btnActivateGame.Click
        Try
            Dim startSide As Boolean = False
            _objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            Dim objGameSetup As New frmGameSetup
            Dim dsOfficialandMatch As New DataSet
            dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
            If dsOfficialandMatch.Tables(1).Rows.Count = 0 Then
                'show game setup screen
                objGameSetup.ShowDialog()
            Else
                If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE")) Then
                    'show game setup screen
                    objGameSetup.ShowDialog()
                ElseIf m_objGameDetails.CurrentPeriod <= 4 Then
                    If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") And m_objGameDetails.CurrentPeriod = 2 Then
                        period = True
                        m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod + 1
                    End If
                    If m_objGameDetails.CurrentPeriod = 3 Then
                        If IsDBNull(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")) Then
                            objGameSetup.ShowDialog()
                        End If
                    End If
                End If
            End If

            dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
            If m_objGameDetails.CurrentPeriod = 0 Then
                If dsOfficialandMatch.Tables(1).Rows.Count > 0 Then
                    If Not IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE")) Then
                        Me.Close()
                        startSide = True
                    End If
                End If
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                If dsOfficialandMatch.Tables(2).Rows.Count > 0 Then
                    If Not IsDBNull(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")) Then
                        Me.Close()
                        startSide = True
                    End If
                End If
                If period Then m_objGameDetails.CurrentPeriod = m_objGameDetails.CurrentPeriod - 1
            Else
                Me.Close()
                startSide = True
            End If
            If dsOfficialandMatch.Tables(1).Rows.Count > 0 Then
                If IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE")) Then
                    MessageDialog.Show(_objGameDetails.languageid, "Home Start not entered.Please check!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If
            End If
            returnString = startSide
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub frmActivateGames_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
        returnString = False
        'Multilingual for The form controls
        If _objGameDetails.languageid <> 1 Then
            Dim lsupport As New languagesupport
            lsupport.FormLanguageTranslate(Me)
        End If
    End Sub
End Class