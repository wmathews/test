﻿#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text
Imports System.IO

#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmTeamStats
' Author        : Shravani
' Created Date  : 07-AUG-09
' Description   : DIPLAYS THE TEAM STATISTICS
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------

#End Region


Public Class frmTeamStats

#Region " Constants "
    Dim strBuilder As New StringBuilder
    Private m_objTeamStats As clsTeamStats = clsTeamStats.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_HomeFirstHalfGoals, m_HomeSecondHalfGoals, m_AwayFirstHalfGoals, m_AwaySecondHalfGoals, m_HomeExtraGoals, m_HomeExtraGoals1, m_AwayExtraGoals, m_AwayExtraGoals1 As Integer
    Private m_HomeFirstHalfOnTargets, m_HomeSecondHalfOnTargets, m_AwayFirstHalfOnTargets, m_AwaySecondHalfOnTargets, m_HomeExtraOnTargets, m_HomeExtraOnTargets1, m_AwayExtraOnTargets, m_AwayExtraOnTargets1 As Integer
    Private m_HomeFirstHalfOffTargets, m_HomeSecondHalfOffTargets, m_AwayFirstHalfOffTargets, m_AwaySecondHalfOffTargets, m_HomeExtraOffTargets, m_HomeExtraOffTargets1, m_AwayExtraOffTargets, m_AwayExtraOffTargets1 As Integer
    Private m_HomeFirstHalfFouls, m_HomeSecondHalfFouls, m_AwayFirstHalfFouls, m_AwaySecondHalfFouls, m_HomeExtraFouls, m_HomeExtraFouls1, m_AwayExtraFouls, m_AwayExtraFouls1 As Integer
    Private m_HomeFirstHalfOffSides, m_HomeSecondHalfOffSides, m_AwayFirstHalfOffSides, m_AwaySecondHalfOffSides, m_HomeExtraOffSides, m_HomeExtraOffSides1, m_AwayExtraOffSides, m_AwayExtraOffSides1 As Integer
    Private m_HomeFirstHalfCrosses, m_HomeSecondHalfCrosses, m_AwayFirstHalfCrosses, m_AwaySecondHalfCrosses, m_HomeExtraCrosses, m_HomeExtraCrosses1, m_AwayExtraCrosses, m_AwayExtraCrosses1 As Integer
    Private m_HomeFirstHalfCorners, m_HomeSecondHalfCorners, m_AwayFirstHalfCorners, m_AwaySecondHalfCorners, m_HomeExtraCorners, m_HomeExtraCorners1, m_AwayExtraCorners, m_AwayExtraCorners1 As Integer
    Private m_HomeFirstHalfYCards, m_HomeSecondHalfYCards, m_AwayFirstHalfYCards, m_AwaySecondHalfYCards, m_HomeExtraYCards, m_HomeExtraYCards1, m_AwayExtraYCards, m_AwayExtraYCards1 As Integer
    Private m_HomeFirstHalfRCards, m_HomeSecondHalfRCards, m_AwayFirstHalfRCardss, m_AwaySecondHalfRCards, m_HomeExtraRCards, m_HomeExtraRCards1, m_AwayExtraRCards, m_AwayExtraRCards1 As Integer
    Private m_HomeFirstHalfGoalAttTot, m_HomeSecondHalfGoalAttTot, m_AwayFirstHalfGoalAttTot, m_AwaySecondHalfGoalAttTot, m_HomeExtraGoalAtt, m_HomeExtraGoalAtt1, m_AwayExtraGoalAtt, m_AwayExtraGoalAtt1 As Integer
    Private m_HomeFirstHalfMissedpen, m_HomeSecondHalfMissedpen, m_AwayFirstHalfMissedpen, m_AwaySecondHalfMissedpen, m_HomeExtraMissedpen, m_HomeExtraMissedpen1, m_AwayExtraMissedpen, m_AwayExtraMissedpen1 As Integer
    Private m_HomeFirstHalfBS, m_HomeSecondHalfBS, m_AwayFirstHalfBS, m_AwaySecondHalfBS, m_HomeExtraBS, m_AwayExtraBS, m_HomeExtraBS1, m_AwayExtraBS1 As Integer
    Private MessageDialog As New frmMessageDialog
    Private m_dsGameSetup As New DataSet
    Private dsGameStats1 As New DataSet
    Private m_objUtilAudit As New clsAuditLog
    Dim m_strBuilder As StringBuilder
    Dim periodcount As Int32
    Private lsupport As New languagesupport
    Dim strheadertext1 As String = "TEAM STATISTICS"
    Dim strheadertext2 As String = "Goals"
    Dim strheadertext3 As String = "Yellow Cards"
    Dim strheadertext4 As String = "Red Cards"
    Dim strheadertext5 As String = "On Target Attempts"
    Dim strheadertext6 As String = "Off Target Attempts"
    Dim strheadertext7 As String = "Total Shots"
    Dim strheadertext8 As String = "Fouls Committed"
    Dim strheadertext9 As String = "Offsides"
    Dim strheadertext10 As String = "Crosses"
    Dim strheadertext11 As String = "Corners"
    Dim strheadertext12 As String = "Missed Penalties"
    Dim strheadertext13 As String = "Shots Blocked"

#End Region

    Private Sub frmTeamStats_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        Try
            DisplayGameStats()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub frmTeamStats_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmTeamStats", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnPrint.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPrint.Text)
                btnRefresh.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnRefresh.Text)
                strheadertext1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext1)
                strheadertext2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext2)
                strheadertext3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext3)
                strheadertext4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext4)
                strheadertext5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext5)
                strheadertext6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext6)
                strheadertext7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext7)
                strheadertext8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext8)
                strheadertext9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext9)
                strheadertext10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext10)
                strheadertext11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext11)
                strheadertext12 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext12)
                strheadertext13 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext13)
            End If
            DisplayGameStats()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub DisplayGameStats()
        Try

            m_strBuilder = New StringBuilder
            Dim dsGameStats As New DataSet
            'count of Period from dbo.LIVE_SOC_PBP
            'periodcount = m_objTeamStats.Getperiodcount(m_objGameDetails.GameCode)
            m_objTeamStats.InsertTeamStastoSummary(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID, m_objGameDetails.CoverageLevel)
            dsGameStats.Tables.Clear()
            dsGameStats = m_objTeamStats.GetTeamStats(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            dsGameStats1 = m_objTeamStats.GetTeamStastoselect(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            Dim cnt As Integer = 0
           
            For i As Integer = 0 To dsGameStats1.Tables(0).Rows.Count - 1


                Dim teperiodmy As String = dsGameStats1.Tables(0).Rows(i)("PERIOD").ToString
                Dim teammy As String = dsGameStats1.Tables(0).Rows(i)("TEAM_ID").ToString

                For j As Integer = 0 To dsGameStats.Tables(0).Rows.Count - 1

                    Dim team As String = dsGameStats.Tables(0).Rows(j)("TEAM_ID").ToString
                    Dim te22 As String = dsGameStats.Tables(0).Rows(j)("PERIOD").ToString
                    'Dim team11111 As String = dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL").ToString
                    If ((te22 = "1") And (teperiodmy = "-1") And (teammy = team)) Then
                        dsGameStats.Tables(0).Rows(j)("shots") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("shots")), 0, dsGameStats.Tables(0).Rows(j)("shots").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("shots")), 0, dsGameStats1.Tables(0).Rows(i)("shots").ToString)))
                        dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL")), 0, dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL")), 0, dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL").ToString)))
                        dsGameStats.Tables(0).Rows(j)("OFFSIDES") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("OFFSIDES")), 0, dsGameStats.Tables(0).Rows(j)("OFFSIDES").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("OFFSIDES")), 0, dsGameStats1.Tables(0).Rows(i)("OFFSIDES").ToString)))
                        dsGameStats.Tables(0).Rows(j)("CORNER_KICKS") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("CORNER_KICKS")), 0, dsGameStats.Tables(0).Rows(j)("CORNER_KICKS").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS")), 0, dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS").ToString)))
                        dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED")), 0, dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED")), 0, dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED").ToString)))
                        dsGameStats.Tables(0).Rows(j).AcceptChanges()
                    End If
                    If ((te22 = "2") And (teperiodmy = "-2") And (teammy = team)) Then
                        dsGameStats.Tables(0).Rows(j)("shots") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("shots")), 0, dsGameStats.Tables(0).Rows(j)("shots").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("shots")), 0, dsGameStats1.Tables(0).Rows(i)("shots").ToString)))
                        dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL")), 0, dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL")), 0, dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL").ToString)))
                        dsGameStats.Tables(0).Rows(j)("OFFSIDES") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("OFFSIDES")), 0, dsGameStats.Tables(0).Rows(j)("OFFSIDES").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("OFFSIDES")), 0, dsGameStats1.Tables(0).Rows(i)("OFFSIDES").ToString)))
                        dsGameStats.Tables(0).Rows(j)("CORNER_KICKS") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("CORNER_KICKS")), 0, dsGameStats.Tables(0).Rows(j)("CORNER_KICKS").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS")), 0, dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS").ToString)))
                        dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED")), 0, dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED")), 0, dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED").ToString)))
                        dsGameStats.Tables(0).Rows(j).AcceptChanges()
                    End If
                    If ((te22 = "3") And (teperiodmy = "-3") And (teammy = team)) Then
                        dsGameStats.Tables(0).Rows(j)("shots") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("shots")), 0, dsGameStats.Tables(0).Rows(j)("shots").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("shots")), 0, dsGameStats1.Tables(0).Rows(i)("shots").ToString)))
                        Dim yy As Int32 = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL")), 0, dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL").ToString))
                        Dim yy11 As Int32 = CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL")), 0, dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL").ToString)))
                        dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL")), 0, dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL")), 0, dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL").ToString)))
                        dsGameStats.Tables(0).Rows(j)("OFFSIDES") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("OFFSIDES")), 0, dsGameStats.Tables(0).Rows(j)("OFFSIDES").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("OFFSIDES")), 0, dsGameStats1.Tables(0).Rows(i)("OFFSIDES").ToString)))
                        dsGameStats.Tables(0).Rows(j)("CORNER_KICKS") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("CORNER_KICKS")), 0, dsGameStats.Tables(0).Rows(j)("CORNER_KICKS").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS")), 0, dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS").ToString)))
                        dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED")), 0, dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED")), 0, dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED").ToString)))
                        dsGameStats.Tables(0).Rows(j).AcceptChanges()
                    End If
                    If ((te22 = "4") And (teperiodmy = "-4") And (teammy = team)) Then
                        dsGameStats.Tables(0).Rows(j)("shots") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("shots")), 0, dsGameStats.Tables(0).Rows(j)("shots").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("shots")), 0, dsGameStats1.Tables(0).Rows(i)("shots").ToString)))
                        dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL")), 0, dsGameStats.Tables(0).Rows(j)("SHOTS_ON_GOAL").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL")), 0, dsGameStats1.Tables(0).Rows(i)("SHOTS_ON_GOAL").ToString)))
                        dsGameStats.Tables(0).Rows(j)("OFFSIDES") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("OFFSIDES")), 0, dsGameStats.Tables(0).Rows(j)("OFFSIDES").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("OFFSIDES")), 0, dsGameStats1.Tables(0).Rows(i)("OFFSIDES").ToString)))
                        dsGameStats.Tables(0).Rows(j)("CORNER_KICKS") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("CORNER_KICKS")), 0, dsGameStats.Tables(0).Rows(j)("CORNER_KICKS").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS")), 0, dsGameStats1.Tables(0).Rows(i)("CORNER_KICKS").ToString)))
                        dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED") = CInt(IIf(IsDBNull(dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED")), 0, dsGameStats.Tables(0).Rows(j)("FOULS_COMMITTED").ToString)) + CInt((IIf(IsDBNull(dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED")), 0, dsGameStats1.Tables(0).Rows(i)("FOULS_COMMITTED").ToString)))
                        dsGameStats.Tables(0).Rows(j).AcceptChanges()
                    End If


                Next
                

            Next

            m_strBuilder.Append("<html><body>")
            m_strBuilder.Append("<table width=500 color:black style=font-family:@Arial Unicode MS border=0 align=center>")

            'HEADINGS
            '---- Modified by Yash - Start ----------------------------'
            m_strBuilder.Append("<tr>")
            m_strBuilder.Append("<td width=100 style=font-size:13;color:gray; style=font-family:@Arial Unicode MS><b>" & strheadertext1 & "</b></td>")
            'm_strBuilder.Append("<td width=150  style=font-size:13;color:gray; style=font-family:@Arial Unicode MS align=left><b>" & m_objGameDetails.HomeTeam & "</b></td>")
            'm_strBuilder.Append("<td width=150 style=font-size:13;color:gray; style=font-family:@Arial Unicode MS align=left><b>" & m_objGameDetails.AwayTeam & "</b></td>")
            m_strBuilder.Append("</tr>")

            m_strBuilder.Append("<tr style=background-color:#e9e9e9; style=color:black;>")
            m_strBuilder.Append("<td width=30%  style=font-size:13; style=font-family:@Arial Unicode MS align=left></td>")
            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=center><b>" & m_objGameDetails.HomeTeam & "</b></td>")
            m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=center><b>" & m_objGameDetails.AwayTeam & "</b></td>")
            m_strBuilder.Append("</tr>")
            '---- Modified by Yash - End ----------------------------'


            'STATISTICS
            Dim drHomeFirstHalf(), drHomeSecondHalf(), drAwayFirstHalf(), drAwaySecondHalf(), drHomeExtra(), drHomeExtra1(), drAwayExtra(), drAwayExtra1() As DataRow

            drHomeFirstHalf = dsGameStats.Tables(0).Select("period =1 and team_id =" & m_objGameDetails.HomeTeamID)
            drHomeSecondHalf = dsGameStats.Tables(0).Select("period =2 and team_id =" & m_objGameDetails.HomeTeamID)
            drHomeExtra = dsGameStats.Tables(0).Select("period =3 and team_id =" & m_objGameDetails.HomeTeamID)
            drHomeExtra1 = dsGameStats.Tables(0).Select("period =4 and team_id =" & m_objGameDetails.HomeTeamID)
            drAwayFirstHalf = dsGameStats.Tables(0).Select("period =1 and team_id =" & m_objGameDetails.AwayTeamID)
            drAwaySecondHalf = dsGameStats.Tables(0).Select("period =2 and team_id =" & m_objGameDetails.AwayTeamID)
            drAwayExtra = dsGameStats.Tables(0).Select("period =3 and team_id =" & m_objGameDetails.AwayTeamID)
            drAwayExtra1 = dsGameStats.Tables(0).Select("period =4 and team_id =" & m_objGameDetails.AwayTeamID)
            '-------------------------------------------------------------------------------------------------------------------------
            
            '---- Modified by Yash - Start ----------------------------'
            m_strBuilder.Append("<tr style=background-color:#e9e9e9; style=color:black;>")
            m_strBuilder.Append("<td width=30%  style=font-size:13; style=font-family:@Arial Unicode MS align=left><b> </b></td>")


            m_strBuilder.Append("<td width=30% align=left style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=100% color:black style=font-family:@Arial Unicode MS border=0>")
            m_strBuilder.Append("<td width=10%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b> 1st</b></td>")
            m_strBuilder.Append("<td width=10% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b> 2nd </b></td>")
            m_strBuilder.Append("<td width=10% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b> OT </b></td>")
            m_strBuilder.Append("<td width=10% style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b> TOT </b></td>")
            m_strBuilder.Append("<td width=5% style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b> </b></td>")
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")

            m_strBuilder.Append("<td width=30%  align=right style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=100% color:black style=font-family:@Arial Unicode MS border=0>")
            m_strBuilder.Append("<td width=10%   style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b> 1st</b></td>")
            m_strBuilder.Append("<td width=10%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b><b> 2nd </b></b></td>")
            m_strBuilder.Append("<td width=10%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b><b> OT </b></b></td>")
            m_strBuilder.Append("<td width=10%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b> TOT </b> </td>")
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")

            m_strBuilder.Append("</tr>")
            '---- Modified by Yash - End ----------------------------'
 
            'GOALS ----------------------------------------------------------------------------------------------------------

            m_strBuilder.Append("<tr>")
            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext2 & "</b> </td>")
            m_strBuilder.Append("</td>")


            m_strBuilder.Append("<td width=20% valign=top style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

            If drHomeFirstHalf.Length > 0 Then
                m_HomeFirstHalfGoals = CInt(IIf(drHomeFirstHalf(0).Item("GOALS") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("GOALS")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfGoals & "</td>")
            Else
                m_HomeFirstHalfGoals = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfGoals & "</td>")
            End If

            If drHomeSecondHalf.Length > 0 Then
                m_HomeSecondHalfGoals = CInt(IIf(drHomeSecondHalf(0).Item("GOALS") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("GOALS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfGoals & "</td>")
            Else
                m_HomeSecondHalfGoals = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfGoals & "</td>")
            End If

            If drHomeExtra1.Length > 0 Then
                m_HomeExtraGoals1 = CInt(IIf(drHomeExtra1(0).Item("GOALS") Is DBNull.Value, 0, drHomeExtra1(0).Item("GOALS")))
            Else
                m_HomeExtraGoals1 = 0
            End If

            If drHomeExtra.Length > 0 Then
                m_HomeExtraGoals = CInt(IIf(drHomeExtra(0).Item("GOALS") Is DBNull.Value, 0, drHomeExtra(0).Item("GOALS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraGoals + m_HomeExtraGoals1 & "</td>")
            Else
                m_HomeExtraGoals = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraGoals + m_HomeExtraGoals1 & "</td>")
            End If
            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfGoals + m_HomeSecondHalfGoals + m_HomeExtraGoals + m_HomeExtraGoals1 & "</b></td>")
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")
            m_strBuilder.Append("<td width=30%  valign=top style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
            If drAwayFirstHalf.Length > 0 Then
                m_AwayFirstHalfGoals = CInt(IIf(drAwayFirstHalf(0).Item("GOALS") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("GOALS")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfGoals & "</td>")
            Else
                m_AwayFirstHalfGoals = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfGoals & "</td>")
            End If
            If drAwaySecondHalf.Length > 0 Then
                m_AwaySecondHalfGoals = CInt(IIf(drAwaySecondHalf(0).Item("GOALS") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("GOALS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfGoals & "</td>")
            Else
                m_AwaySecondHalfGoals = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfGoals & "</td>")
            End If

            If drAwayExtra1.Length > 0 Then
                m_AwayExtraGoals1 = CInt(IIf(drAwayExtra1(0).Item("GOALS") Is DBNull.Value, 0, drAwayExtra1(0).Item("GOALS")))
            Else
                m_AwayExtraGoals1 = 0
            End If

            If drAwayExtra.Length > 0 Then
                m_AwayExtraGoals = CInt(IIf(drAwayExtra(0).Item("GOALS") Is DBNull.Value, 0, drAwayExtra(0).Item("GOALS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraGoals + m_AwayExtraGoals1 & "</td>")
            Else
                m_AwayExtraGoals = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraGoals + m_AwayExtraGoals1 & "</td>")
            End If




            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfGoals + m_AwaySecondHalfGoals + m_AwayExtraGoals + m_AwayExtraGoals1 & "</b></td>")

            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")

            m_strBuilder.Append("</tr>")
            '-----------------------------------------------------------------------------------------------------------------------------------------



            'YEllow Cards ----------------------------------------------------------------------------------------------------------
            m_strBuilder.Append("<tr style=background-color:#f7f7f7>")
            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext3 & "</b> </td>")
            m_strBuilder.Append("</td>")


            m_strBuilder.Append("<td width=20%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

            If drHomeFirstHalf.Length > 0 Then
                m_HomeFirstHalfYCards = CInt(IIf(drHomeFirstHalf(0).Item("Yellow_Cards") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("Yellow_Cards")))

                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfYCards & "</td>")
            Else
                m_HomeFirstHalfYCards = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfYCards & "</td>")
            End If

            If drHomeSecondHalf.Length > 0 Then
                m_HomeSecondHalfYCards = CInt(IIf(drHomeSecondHalf(0).Item("Yellow_Cards") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("Yellow_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfYCards & "</td>")
            Else
                m_HomeSecondHalfYCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfYCards & "</td>")
            End If
            If drHomeExtra1.Length > 0 Then
                m_HomeExtraYCards1 = CInt(IIf(drHomeExtra1(0).Item("Yellow_Cards") Is DBNull.Value, 0, drHomeExtra1(0).Item("Yellow_Cards")))
                ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraYCards1 & "</b></td>")
            Else
                m_HomeExtraYCards1 = 0
                ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraYCards1 & "</td>")
            End If

            If drHomeExtra.Length > 0 Then
                m_HomeExtraYCards = CInt(IIf(drHomeExtra(0).Item("Yellow_Cards") Is DBNull.Value, 0, drHomeExtra(0).Item("Yellow_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraYCards + m_HomeExtraYCards1 & "</td>")
            Else
                m_HomeExtraYCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraYCards + m_HomeExtraYCards1 & "</td>")
            End If




            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfYCards + m_HomeSecondHalfYCards + m_HomeExtraYCards + m_HomeExtraYCards1 & "</b></td>")

            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")



            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
            If drAwayFirstHalf.Length > 0 Then
                m_AwayFirstHalfYCards = CInt(IIf(drAwayFirstHalf(0).Item("Yellow_Cards") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("Yellow_Cards")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfYCards & "</td>")
            Else
                m_AwayFirstHalfYCards = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfYCards & "</td>")
            End If
            If drAwaySecondHalf.Length > 0 Then
                m_AwaySecondHalfYCards = CInt(IIf(drAwaySecondHalf(0).Item("Yellow_Cards") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("Yellow_Cards")))

                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfYCards & "</td>")
            Else
                m_AwaySecondHalfYCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfYCards & "</td>")
            End If
            If drAwayExtra1.Length > 0 Then
                m_AwayExtraYCards1 = CInt(IIf(drAwayExtra1(0).Item("Yellow_Cards") Is DBNull.Value, 0, drAwayExtra1(0).Item("Yellow_Cards")))
                'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraYCards1 & "</b></td>")
            Else
                m_AwayExtraYCards1 = 0
                ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraYCards1 & "</td>")
            End If
            If drAwayExtra.Length > 0 Then
                m_AwayExtraYCards = CInt(IIf(drAwayExtra(0).Item("Yellow_Cards") Is DBNull.Value, 0, drAwayExtra(0).Item("Yellow_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraYCards + m_AwayExtraYCards1 & "</td>")
            Else
                m_AwayExtraYCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraYCards + m_AwayExtraYCards1 & "</td>")
            End If


            m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfYCards + m_AwaySecondHalfYCards + m_AwayExtraYCards + m_AwayExtraYCards1 & " </b></td>")

            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")

            m_strBuilder.Append("</tr>")
            '-----------------------------------------------------------------------------------------------------------------------------------------


            'Red Cards ----------------------------------------------------------------------------------------------------------
            m_strBuilder.Append("<tr>")
            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext4 & "</b> </td>")
            m_strBuilder.Append("</td>")


            m_strBuilder.Append("<td width=20%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

            If drHomeFirstHalf.Length > 0 Then
                m_HomeFirstHalfRCards = CInt(IIf(drHomeFirstHalf(0).Item("Red_Cards") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("Red_Cards")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfRCards & "</td>")
            Else
                m_HomeFirstHalfRCards = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfRCards & "</td>")
            End If

            If drHomeSecondHalf.Length > 0 Then
                m_HomeSecondHalfRCards = CInt(IIf(drHomeSecondHalf(0).Item("Red_Cards") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("Red_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfRCards & "</td>")
            Else
                m_HomeSecondHalfRCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfRCards & "</td>")
            End If

            If drHomeExtra1.Length > 0 Then
                m_HomeExtraRCards1 = CInt(IIf(drHomeExtra1(0).Item("Red_Cards") Is DBNull.Value, 0, drHomeExtra1(0).Item("Red_Cards")))
                ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraRCards1 & "</b></td>")
            Else
                m_HomeExtraRCards1 = 0
                'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraRCards1 & "</td>")
            End If

            If drHomeExtra.Length > 0 Then
                m_HomeExtraRCards = CInt(IIf(drHomeExtra(0).Item("Red_Cards") Is DBNull.Value, 0, drHomeExtra(0).Item("Red_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraRCards + m_HomeExtraRCards1 & "</td>")
            Else
                m_HomeExtraRCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraRCards + m_HomeExtraRCards1 & "</td>")
            End If

            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfRCards + m_HomeSecondHalfRCards + m_HomeExtraRCards + m_HomeExtraRCards1 & "</b></td>")
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")



            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
            If drAwayFirstHalf.Length > 0 Then
                m_AwayFirstHalfRCardss = CInt(IIf(drAwayFirstHalf(0).Item("Red_Cards") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("Red_Cards")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfRCardss & "</td>")
            Else
                m_AwayFirstHalfRCardss = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfRCardss & "</td>")
            End If
            If drAwaySecondHalf.Length > 0 Then
                m_AwaySecondHalfRCards = CInt(IIf(drAwaySecondHalf(0).Item("Red_Cards") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("Red_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfRCards & "</td>")
            Else
                m_AwaySecondHalfRCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfRCards & "</td>")
            End If
            If drAwayExtra1.Length > 0 Then
                m_AwayExtraRCards1 = CInt(IIf(drAwayExtra1(0).Item("Red_Cards") Is DBNull.Value, 0, drAwayExtra1(0).Item("Red_Cards")))
                'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraRCards1 & "</b></td>")
            Else
                m_AwayExtraRCards1 = 0
                'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraRCards1 & "</td>")
            End If

            If drAwayExtra.Length > 0 Then
                m_AwayExtraRCards = CInt(IIf(drAwayExtra(0).Item("Red_Cards") Is DBNull.Value, 0, drAwayExtra(0).Item("Red_Cards")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraRCards + m_AwayExtraRCards1 & "</td>")
            Else
                m_AwayExtraRCards = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraRCards + m_AwayExtraRCards1 & "</td>")
            End If



            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfRCardss + m_AwaySecondHalfRCards + m_AwayExtraRCards + m_AwayExtraRCards1 & "</b></td>")

            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")

            m_strBuilder.Append("</tr>")
            '-----------------------------------------------------------------------------------------------------------------------------------------


            'Goal Attempts Total = Ontarget Attempts + Goals----------------------------------------------------------------------------------------
            m_strBuilder.Append("<tr style=background-color:#f7f7f7>")
            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext7 & "</b> </td>")
            m_strBuilder.Append("</td>")


            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

            If drHomeFirstHalf.Length > 0 Then
                'm_HomeFirstHalfGoalAttTot = CInt(IIf(drHomeFirstHalf(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("GOALATTEMPTSTOTAL")))
                m_HomeFirstHalfGoalAttTot = CInt(IIf(drHomeFirstHalf(0).Item("SHOTS") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("SHOTS")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfGoalAttTot & "</td>")
            Else
                m_HomeFirstHalfGoalAttTot = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfGoalAttTot & "</td>")
            End If

            If drHomeSecondHalf.Length > 0 Then
                'm_HomeSecondHalfGoalAttTot = CInt(IIf(drHomeSecondHalf(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("GOALATTEMPTSTOTAL")))
                m_HomeSecondHalfGoalAttTot = CInt(IIf(drHomeSecondHalf(0).Item("SHOTS") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("SHOTS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfGoalAttTot & "</td>")
            Else
                m_HomeSecondHalfGoalAttTot = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfGoalAttTot & "</td>")
            End If

            If drHomeExtra1.Length > 0 Then
                'm_HomeExtraGoalAtt1 = CInt(IIf(drHomeExtra1(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drHomeExtra1(0).Item("GOALATTEMPTSTOTAL")))
                m_HomeExtraGoalAtt1 = CInt(IIf(drHomeExtra1(0).Item("SHOTS") Is DBNull.Value, 0, drHomeExtra1(0).Item("SHOTS")))
                'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraGoalAtt1 & "</b></td>")
            Else
                m_HomeExtraGoalAtt1 = 0
                ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraGoalAtt1 & "</td>")
            End If

            If drHomeExtra.Length > 0 Then
                'm_HomeExtraGoalAtt = CInt(IIf(drHomeExtra(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drHomeExtra(0).Item("GOALATTEMPTSTOTAL")))
                m_HomeExtraGoalAtt = CInt(IIf(drHomeExtra(0).Item("SHOTS") Is DBNull.Value, 0, drHomeExtra(0).Item("SHOTS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraGoalAtt + m_HomeExtraGoalAtt1 & "</td>")
            Else
                m_HomeExtraGoalAtt = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraGoalAtt + m_HomeExtraGoalAtt1 & "</td>")
            End If



            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfGoalAttTot + m_HomeSecondHalfGoalAttTot + m_HomeExtraGoalAtt + m_HomeExtraGoalAtt1 & "</b></td>")

            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")



            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
            If drAwayFirstHalf.Length > 0 Then
                'm_AwayFirstHalfGoalAttTot = CInt(IIf(drAwayFirstHalf(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("GOALATTEMPTSTOTAL")))
                m_AwayFirstHalfGoalAttTot = CInt(IIf(drAwayFirstHalf(0).Item("SHOTS") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("SHOTS")))
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfGoalAttTot & "</td>")
            Else
                m_AwayFirstHalfGoalAttTot = 0
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfGoalAttTot & "</td>")
            End If
            If drAwaySecondHalf.Length > 0 Then
                'm_AwaySecondHalfGoalAttTot = CInt(IIf(drAwaySecondHalf(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("GOALATTEMPTSTOTAL")))
                m_AwaySecondHalfGoalAttTot = CInt(IIf(drAwaySecondHalf(0).Item("SHOTS") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("SHOTS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfGoalAttTot & "</td>")
            Else
                m_AwaySecondHalfGoalAttTot = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfGoalAttTot & "</td>")
            End If
            If drAwayExtra1.Length > 0 Then
                'm_AwayExtraGoalAtt1 = CInt(IIf(drAwayExtra1(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drAwayExtra1(0).Item("GOALATTEMPTSTOTAL")))
                m_AwayExtraGoalAtt1 = CInt(IIf(drAwayExtra1(0).Item("SHOTS") Is DBNull.Value, 0, drAwayExtra1(0).Item("SHOTS")))
                ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraGoalAtt1 & "</b></td>")
            Else
                m_AwayExtraGoalAtt1 = 0
                'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraGoalAtt1 & "</td>")
            End If

            If drAwayExtra.Length > 0 Then
                'm_AwayExtraGoalAtt = CInt(IIf(drAwayExtra(0).Item("GOALATTEMPTSTOTAL") Is DBNull.Value, 0, drAwayExtra(0).Item("GOALATTEMPTSTOTAL")))
                m_AwayExtraGoalAtt = CInt(IIf(drAwayExtra(0).Item("SHOTS") Is DBNull.Value, 0, drAwayExtra(0).Item("SHOTS")))
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraGoalAtt + m_AwayExtraGoalAtt1 & "</td>")
            Else
                m_AwayExtraGoalAtt = 0
                m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraGoalAtt + m_AwayExtraGoalAtt1 & "</td>")
            End If




            m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfGoalAttTot + m_AwaySecondHalfGoalAttTot + m_AwayExtraGoalAtt + m_AwayExtraGoalAtt1 & "</b></td>")

            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</td>")

            m_strBuilder.Append("</tr>")
            '-----------------------------------------------------------------------------------------------------------------------------------------



            'OnTargetAttemps ----------------------------------------------------------------------------------------------------------
            If Not (m_objGameDetails.ModuleID = 2 And (m_objGameDetails.CoverageLevel = 2 Or m_objGameDetails.CoverageLevel = 3)) Then
                m_strBuilder.Append("<tr style=background-color:#f7f7f7>")
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext5 & "</b>  </td>")
                m_strBuilder.Append("</td>")


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                If drHomeFirstHalf.Length > 0 Then
                    m_HomeFirstHalfOnTargets = CInt(IIf(drHomeFirstHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("SHOTS_ON_GOAL")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOnTargets & "</td>")
                Else
                    m_HomeFirstHalfOnTargets = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOnTargets & "</td>")
                End If

                If drHomeSecondHalf.Length > 0 Then
                    m_HomeSecondHalfOnTargets = CInt(IIf(drHomeSecondHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("SHOTS_ON_GOAL")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOnTargets & "</td>")
                Else
                    m_HomeSecondHalfOnTargets = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOnTargets & "</td>")
                End If

                If drHomeExtra1.Length > 0 Then
                    m_HomeExtraOnTargets1 = CInt(IIf(drHomeExtra1(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeExtra1(0).Item("SHOTS_ON_GOAL")))
                    '  m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraOnTargets1 & "</b></td>")
                Else
                    m_HomeExtraOnTargets1 = 0
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOnTargets1 & "</td>")
                End If

                If drHomeExtra.Length > 0 Then
                    m_HomeExtraOnTargets = CInt(IIf(drHomeExtra(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeExtra(0).Item("SHOTS_ON_GOAL")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOnTargets + m_HomeExtraOnTargets1 & "</td>")
                Else
                    m_HomeExtraOnTargets = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOnTargets + m_HomeExtraOnTargets1 & "</td>")
                End If


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfOnTargets + m_HomeSecondHalfOnTargets + m_HomeExtraOnTargets + m_HomeExtraOnTargets1 & "</b></td>")
                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                If drAwayFirstHalf.Length > 0 Then
                    m_AwayFirstHalfOnTargets = CInt(IIf(drAwayFirstHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("SHOTS_ON_GOAL")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOnTargets & "</td>")
                Else
                    m_AwayFirstHalfOnTargets = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOnTargets & "</td>")
                End If
                If drAwaySecondHalf.Length > 0 Then
                    m_AwaySecondHalfOnTargets = CInt(IIf(drAwaySecondHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("SHOTS_ON_GOAL")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOnTargets & "</td>")
                Else
                    m_AwaySecondHalfOnTargets = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOnTargets & "</td>")
                End If

                If drAwayExtra1.Length > 0 Then
                    m_AwayExtraOnTargets1 = CInt(IIf(drAwayExtra1(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwayExtra1(0).Item("SHOTS_ON_GOAL")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraOnTargets1 & "</b></td>")
                Else
                    m_AwayExtraOnTargets1 = 0
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOnTargets1 & "</td>")
                End If

                If drAwayExtra.Length > 0 Then
                    m_AwayExtraOnTargets = CInt(IIf(drAwayExtra(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwayExtra(0).Item("SHOTS_ON_GOAL")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOnTargets + m_AwayExtraOnTargets1 & "</td>")
                Else
                    m_AwayExtraOnTargets = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOnTargets + m_AwayExtraOnTargets1 & "</td>")
                End If



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfOnTargets + m_AwaySecondHalfOnTargets + m_AwayExtraOnTargets + m_AwayExtraOnTargets1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")

                m_strBuilder.Append("</tr>")
                '-----------------------------------------------------------------------------------------------------------------------------------------


                'OFFTargetAttemps ----------------------------------------------------------------------------------------------------------
                If (m_objGameDetails.CoverageLevel = 6) Then
                    m_strBuilder.Append("<tr>")
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext6 & "</b> </td>")
                    m_strBuilder.Append("</td>")


                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                    m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                    If drHomeFirstHalf.Length > 0 Then
                        m_HomeFirstHalfOffTargets = CInt(IIf(drHomeFirstHalf(0).Item("OFF_TARGET") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("OFF_TARGET")))
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOffTargets & "</td>")
                    Else
                        m_HomeFirstHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOffTargets & "</td>")
                    End If

                    If drHomeSecondHalf.Length > 0 Then
                        m_HomeSecondHalfOffTargets = CInt(IIf(drHomeSecondHalf(0).Item("OFF_TARGET") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("OFF_TARGET")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOffTargets & "</td>")
                    Else
                        m_HomeSecondHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOffTargets & "</td>")
                    End If

                    If drHomeExtra1.Length > 0 Then
                        m_HomeExtraOffTargets1 = CInt(IIf(drHomeExtra1(0).Item("OFF_TARGET") Is DBNull.Value, 0, drHomeExtra1(0).Item("OFF_TARGET")))
                        'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraOffTargets1 & "</b></td>")
                    Else
                        m_HomeExtraOffTargets1 = 0
                        ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffTargets1 & "</td>")
                    End If


                    If drHomeExtra.Length > 0 Then
                        m_HomeExtraOffTargets = CInt(IIf(drHomeExtra(0).Item("OFF_TARGET") Is DBNull.Value, 0, drHomeExtra(0).Item("OFF_TARGET")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffTargets + m_HomeExtraOffTargets1 & "</td>")
                    Else
                        m_HomeExtraOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffTargets + m_HomeExtraOffTargets1 & "</td>")
                    End If



                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfOffTargets + m_HomeSecondHalfOffTargets + m_HomeExtraOffTargets + m_HomeExtraOffTargets1 & "</b></td>")

                    m_strBuilder.Append("</table>")
                    m_strBuilder.Append("</td>")


                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                    m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                    If drAwayFirstHalf.Length > 0 Then
                        m_AwayFirstHalfOffTargets = CInt(IIf(drAwayFirstHalf(0).Item("OFF_TARGET") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("OFF_TARGET")))
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOffTargets & "</td>")
                    Else
                        m_AwayFirstHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOffTargets & "</td>")
                    End If
                    If drAwaySecondHalf.Length > 0 Then
                        m_AwaySecondHalfOffTargets = CInt(IIf(drAwaySecondHalf(0).Item("OFF_TARGET") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("OFF_TARGET")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOffTargets & "</td>")
                    Else
                        m_AwaySecondHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOffTargets & "</td>")
                    End If

                    If drAwayExtra1.Length > 0 Then
                        m_AwayExtraOffTargets1 = CInt(IIf(drAwayExtra1(0).Item("OFF_TARGET") Is DBNull.Value, 0, drAwayExtra1(0).Item("OFF_TARGET")))
                        'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraOffTargets1 & "</b></td>")
                    Else
                        m_AwayExtraOffTargets1 = 0
                        ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffTargets1 & "</td>")
                    End If

                    If drAwayExtra.Length > 0 Then
                        m_AwayExtraOffTargets = CInt(IIf(drAwayExtra(0).Item("OFF_TARGET") Is DBNull.Value, 0, drAwayExtra(0).Item("OFF_TARGET")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffTargets + m_AwayExtraOffTargets1 & "</td>")
                    Else
                        m_AwayExtraOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffTargets + m_AwayExtraOffTargets1 & "</td>")
                    End If

                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfOffTargets + m_AwaySecondHalfOffTargets + m_AwayExtraOffTargets + m_AwayExtraOffTargets1 & "</b></td>")
                    m_strBuilder.Append("</table>")
                    m_strBuilder.Append("</td>")
                    m_strBuilder.Append("</tr>")
                Else
                    m_strBuilder.Append("<tr>")
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext6 & "</b> </td>")
                    m_strBuilder.Append("</td>")


                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                    m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                    If drHomeFirstHalf.Length > 0 Then
                        m_HomeFirstHalfOffTargets = CInt(IIf(drHomeFirstHalf(0).Item("SHOTS") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("SHOTS"))) - CInt(IIf(drHomeFirstHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("SHOTS_ON_GOAL")))
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOffTargets & "</td>")
                    Else
                        m_HomeFirstHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOffTargets & "</td>")
                    End If

                    If drHomeSecondHalf.Length > 0 Then
                        m_HomeSecondHalfOffTargets = CInt(IIf(drHomeSecondHalf(0).Item("SHOTS") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("SHOTS"))) - CInt(IIf(drHomeSecondHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("SHOTS_ON_GOAL")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOffTargets & "</td>")
                    Else
                        m_HomeSecondHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOffTargets & "</td>")
                    End If

                    If drHomeExtra1.Length > 0 Then
                        m_HomeExtraOffTargets1 = CInt(IIf(drHomeExtra1(0).Item("SHOTS") Is DBNull.Value, 0, drHomeExtra1(0).Item("SHOTS"))) - CInt(IIf(drHomeExtra1(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeExtra1(0).Item("SHOTS_ON_GOAL")))
                        'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraOffTargets1 & "</b></td>")
                    Else
                        m_HomeExtraOffTargets1 = 0
                        ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffTargets1 & "</td>")
                    End If


                    If drHomeExtra.Length > 0 Then
                        m_HomeExtraOffTargets = CInt(IIf(drHomeExtra(0).Item("SHOTS") Is DBNull.Value, 0, drHomeExtra(0).Item("SHOTS"))) - CInt(IIf(drHomeExtra(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drHomeExtra(0).Item("SHOTS_ON_GOAL")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffTargets + m_HomeExtraOffTargets1 & "</td>")
                    Else
                        m_HomeExtraOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffTargets + m_HomeExtraOffTargets1 & "</td>")
                    End If



                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfOffTargets + m_HomeSecondHalfOffTargets + m_HomeExtraOffTargets + m_HomeExtraOffTargets1 & "</b></td>")

                    m_strBuilder.Append("</table>")
                    m_strBuilder.Append("</td>")


                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                    m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                    If drAwayFirstHalf.Length > 0 Then
                        m_AwayFirstHalfOffTargets = CInt(IIf(drAwayFirstHalf(0).Item("SHOTS") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("SHOTS"))) - CInt(IIf(drAwayFirstHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("SHOTS_ON_GOAL")))
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOffTargets & "</td>")
                    Else
                        m_AwayFirstHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOffTargets & "</td>")
                    End If
                    If drAwaySecondHalf.Length > 0 Then
                        m_AwaySecondHalfOffTargets = CInt(IIf(drAwaySecondHalf(0).Item("SHOTS") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("SHOTS"))) - CInt(IIf(drAwaySecondHalf(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("SHOTS_ON_GOAL")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOffTargets & "</td>")
                    Else
                        m_AwaySecondHalfOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOffTargets & "</td>")
                    End If

                    If drAwayExtra1.Length > 0 Then
                        m_AwayExtraOffTargets1 = CInt(IIf(drAwayExtra1(0).Item("SHOTS") Is DBNull.Value, 0, drAwayExtra1(0).Item("SHOTS"))) - CInt(IIf(drAwayExtra1(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwayExtra1(0).Item("SHOTS_ON_GOAL")))
                        'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraOffTargets1 & "</b></td>")
                    Else
                        m_AwayExtraOffTargets1 = 0
                        ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffTargets1 & "</td>")
                    End If

                    If drAwayExtra.Length > 0 Then
                        m_AwayExtraOffTargets = CInt(IIf(drAwayExtra(0).Item("SHOTS") Is DBNull.Value, 0, drAwayExtra(0).Item("SHOTS"))) - CInt(IIf(drAwayExtra(0).Item("SHOTS_ON_GOAL") Is DBNull.Value, 0, drAwayExtra(0).Item("SHOTS_ON_GOAL")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffTargets + m_AwayExtraOffTargets1 & "</td>")
                    Else
                        m_AwayExtraOffTargets = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffTargets + m_AwayExtraOffTargets1 & "</td>")
                    End If

                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfOffTargets + m_AwaySecondHalfOffTargets + m_AwayExtraOffTargets + m_AwayExtraOffTargets1 & "</b></td>")
                    m_strBuilder.Append("</table>")
                    m_strBuilder.Append("</td>")
                    m_strBuilder.Append("</tr>")
                End If
               
                '-----------------------------------------------------------------------------------------------------------------------------------------


                If m_objGameDetails.CoverageLevel = 6 Then
                    'BLOCKED SHOTS
                    m_strBuilder.Append("<tr>")
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext13 & "</b> </td>")
                    m_strBuilder.Append("</td>")


                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                    m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                    If drHomeFirstHalf.Length > 0 Then
                        m_HomeFirstHalfBS = CInt(IIf(drHomeFirstHalf(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("BLOCKED_SHOTS")))
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfBS & "</td>")
                    Else
                        m_HomeFirstHalfBS = 0
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfBS & "</td>")
                    End If

                    If drHomeSecondHalf.Length > 0 Then
                        m_HomeSecondHalfBS = CInt(IIf(drHomeSecondHalf(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("BLOCKED_SHOTS")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfBS & "</td>")
                    Else
                        m_HomeSecondHalfBS = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfBS & "</td>")
                    End If

                    If drHomeExtra1.Length > 0 Then
                        m_HomeExtraBS1 = CInt(IIf(drHomeExtra1(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drHomeExtra1(0).Item("BLOCKED_SHOTS")))
                        
                    Else
                        m_HomeExtraBS1 = 0
                       
                    End If

                    If drHomeExtra.Length > 0 Then
                        m_HomeExtraBS = CInt(IIf(drHomeExtra(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drHomeExtra(0).Item("BLOCKED_SHOTS")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraBS + m_HomeExtraBS1 & "</td>")
                    Else
                        m_HomeExtraBS = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraBS + m_HomeExtraBS1 & "</td>")
                    End If
                   

                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfBS + m_HomeSecondHalfBS + m_HomeExtraBS + m_HomeExtraBS1 & "</b></td>")

                    m_strBuilder.Append("</table>")
                    m_strBuilder.Append("</td>")



                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                    m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                    If drAwayFirstHalf.Length > 0 Then
                        m_AwayFirstHalfBS = CInt(IIf(drAwayFirstHalf(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("BLOCKED_SHOTS")))
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfBS & "</td>")
                    Else
                        m_AwayFirstHalfBS = 0
                        m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfBS & "</td>")
                    End If
                    If drAwaySecondHalf.Length > 0 Then
                        m_AwaySecondHalfBS = CInt(IIf(drAwaySecondHalf(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("BLOCKED_SHOTS")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfBS & "</td>")
                    Else
                        m_AwaySecondHalfBS = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfBS & "</td>")
                    End If

                    If drAwayExtra1.Length > 0 Then
                        m_AwayExtraBS1 = CInt(IIf(drAwayExtra1(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drAwayExtra1(0).Item("BLOCKED_SHOTS")))
                       
                    Else
                        m_AwayExtraBS1 = 0
                        
                    End If

                    If drAwayExtra.Length > 0 Then
                        m_AwayExtraBS = CInt(IIf(drAwayExtra(0).Item("BLOCKED_SHOTS") Is DBNull.Value, 0, drAwayExtra(0).Item("BLOCKED_SHOTS")))
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraBS + m_AwayExtraBS1 & "</td>")
                    Else
                        m_AwayExtraBS = 0
                        m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraBS + m_AwayExtraBS1 & "</td>")
                    End If
                   
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfBS + m_AwaySecondHalfBS + m_AwayExtraBS + m_AwayExtraBS1 & "</b></td>")

                    m_strBuilder.Append("</table>")
                    m_strBuilder.Append("</td>")

                    m_strBuilder.Append("</tr>")
                End If
                '-----------------------------------------------------------------------------------------------------------------------------------------



                'Fouls ----------------------------------------------------------------------------------------------------------
                m_strBuilder.Append("<tr>")
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext8 & "</b> </td>")
                m_strBuilder.Append("</td>")


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                If drHomeFirstHalf.Length > 0 Then
                    m_HomeFirstHalfFouls = CInt(IIf(drHomeFirstHalf(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("FOULS_COMMITTED")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfFouls & "</td>")
                Else
                    m_HomeFirstHalfFouls = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfFouls & "</td>")
                End If

                If drHomeSecondHalf.Length > 0 Then
                    m_HomeSecondHalfFouls = CInt(IIf(drHomeSecondHalf(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("FOULS_COMMITTED")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfFouls & "</td>")
                Else
                    m_HomeSecondHalfFouls = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfFouls & "</td>")
                End If

                If drHomeExtra1.Length > 0 Then
                    m_HomeExtraFouls1 = CInt(IIf(drHomeExtra1(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drHomeExtra1(0).Item("FOULS_COMMITTED")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraFouls1 & "</b></td>")
                Else
                    m_HomeExtraFouls1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraFouls1 & "</td>")
                End If
                If drHomeExtra.Length > 0 Then
                    m_HomeExtraFouls = CInt(IIf(drHomeExtra(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drHomeExtra(0).Item("FOULS_COMMITTED")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraFouls + m_HomeExtraFouls1 & "</td>")
                Else
                    m_HomeExtraFouls = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraFouls + m_HomeExtraFouls1 & "</td>")
                End If


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfFouls + m_HomeSecondHalfFouls + m_HomeExtraFouls + m_HomeExtraFouls1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                If drAwayFirstHalf.Length > 0 Then
                    m_AwayFirstHalfFouls = CInt(IIf(drAwayFirstHalf(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("FOULS_COMMITTED")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfFouls & "</td>")
                Else
                    m_AwayFirstHalfFouls = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfFouls & "</td>")
                End If
                If drAwaySecondHalf.Length > 0 Then
                    m_AwaySecondHalfFouls = CInt(IIf(drAwaySecondHalf(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("FOULS_COMMITTED")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfFouls & "</td>")
                Else
                    m_AwaySecondHalfFouls = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfFouls & "</td>")
                End If
                If drAwayExtra1.Length > 0 Then
                    m_AwayExtraFouls1 = CInt(IIf(drAwayExtra1(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drAwayExtra1(0).Item("FOULS_COMMITTED")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraFouls1 & "</b></td>")
                Else
                    m_AwayExtraFouls1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraFouls1 & "</td>")
                End If
                If drAwayExtra.Length > 0 Then
                    m_AwayExtraFouls = CInt(IIf(drAwayExtra(0).Item("FOULS_COMMITTED") Is DBNull.Value, 0, drAwayExtra(0).Item("FOULS_COMMITTED")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraFouls + m_AwayExtraFouls1 & "</td>")
                Else
                    m_AwayExtraFouls = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraFouls + m_AwayExtraFouls1 & "</td>")
                End If


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfFouls + m_AwaySecondHalfFouls + m_AwayExtraFouls + m_AwayExtraFouls1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")

                m_strBuilder.Append("</tr>")
                '-----------------------------------------------------------------------------------------------------------------------------------------

                'OffSides ----------------------------------------------------------------------------------------------------------
                m_strBuilder.Append("<tr style=background-color:#f7f7f7>")
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext9 & "</b> </td>")
                m_strBuilder.Append("</td>")


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                If drHomeFirstHalf.Length > 0 Then
                    m_HomeFirstHalfOffSides = CInt(IIf(drHomeFirstHalf(0).Item("OffSides") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("OffSides")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOffSides & "</td>")
                Else
                    m_HomeFirstHalfOffSides = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfOffSides & "</td>")
                End If

                If drHomeSecondHalf.Length > 0 Then
                    m_HomeSecondHalfOffSides = CInt(IIf(drHomeSecondHalf(0).Item("OffSides") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("OffSides")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOffSides & "</td>")
                Else
                    m_HomeSecondHalfOffSides = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfOffSides & "</td>")
                End If
                If drHomeExtra1.Length > 0 Then
                    m_HomeExtraOffSides1 = CInt(IIf(drHomeExtra1(0).Item("OffSides") Is DBNull.Value, 0, drHomeExtra1(0).Item("OffSides")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraOffSides1 & "</b></td>")
                Else
                    m_HomeExtraOffSides1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffSides1 & "</td>")
                End If

                If drHomeExtra.Length > 0 Then
                    m_HomeExtraOffSides = CInt(IIf(drHomeExtra(0).Item("OffSides") Is DBNull.Value, 0, drHomeExtra(0).Item("OffSides")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffSides + m_HomeExtraOffSides1 & "</td>")
                Else
                    m_HomeExtraOffSides = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraOffSides + m_HomeExtraOffSides1 & "</td>")
                End If


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfOffSides + m_HomeSecondHalfOffSides + m_HomeExtraOffSides + m_HomeExtraOffSides1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                If drAwayFirstHalf.Length > 0 Then
                    m_AwayFirstHalfOffSides = CInt(IIf(drAwayFirstHalf(0).Item("OffSides") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("OffSides")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOffSides & "</td>")
                Else
                    m_AwayFirstHalfOffSides = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfOffSides & "</td>")
                End If
                If drAwaySecondHalf.Length > 0 Then
                    m_AwaySecondHalfOffSides = CInt(IIf(drAwaySecondHalf(0).Item("OffSides") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("OffSides")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOffSides & "</td>")
                Else
                    m_AwaySecondHalfOffSides = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfOffSides & "</td>")
                End If
                If drAwayExtra1.Length > 0 Then
                    m_AwayExtraOffSides1 = CInt(IIf(drAwayExtra1(0).Item("OffSides") Is DBNull.Value, 0, drAwayExtra1(0).Item("OffSides")))
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraOffSides1 & "</b></td>")
                Else
                    m_AwayExtraOffSides1 = 0
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffSides1 & "</td>")
                End If
                If drAwayExtra.Length > 0 Then
                    m_AwayExtraOffSides = CInt(IIf(drAwayExtra(0).Item("OffSides") Is DBNull.Value, 0, drAwayExtra(0).Item("OffSides")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffSides + m_AwayExtraOffSides1 & "</td>")
                Else
                    m_AwayExtraOffSides = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraOffSides + m_AwayExtraOffSides1 & "</td>")
                End If



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfOffSides + m_AwaySecondHalfOffSides + m_AwayExtraOffSides + m_AwayExtraOffSides1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")

                m_strBuilder.Append("</tr>")
                '-----------------------------------------------------------------------------------------------------------------------------------------


                'Crosses ----------------------------------------------------------------------------------------------------------
                m_strBuilder.Append("<tr>")
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext10 & "</b> </td>")
                m_strBuilder.Append("</td>")


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                If drHomeFirstHalf.Length > 0 Then
                    m_HomeFirstHalfCrosses = CInt(IIf(drHomeFirstHalf(0).Item("Crosses") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("Crosses")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfCrosses & "</td>")
                Else
                    m_HomeFirstHalfCrosses = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfCrosses & "</td>")
                End If

                If drHomeSecondHalf.Length > 0 Then
                    m_HomeSecondHalfCrosses = CInt(IIf(drHomeSecondHalf(0).Item("Crosses") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("Crosses")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfCrosses & "</td>")
                Else
                    m_HomeSecondHalfCrosses = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfCrosses & "</td>")
                End If
                If drHomeExtra1.Length > 0 Then
                    m_HomeExtraCrosses1 = CInt(IIf(drHomeExtra1(0).Item("Crosses") Is DBNull.Value, 0, drHomeExtra1(0).Item("Crosses")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraCrosses1 & "</b></td>")
                Else
                    m_HomeExtraCrosses1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraCrosses1 & "</td>")
                End If

                If drHomeExtra.Length > 0 Then
                    m_HomeExtraCrosses = CInt(IIf(drHomeExtra(0).Item("Crosses") Is DBNull.Value, 0, drHomeExtra(0).Item("Crosses")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraCrosses + m_HomeExtraCrosses1 & "</td>")
                Else
                    m_HomeExtraCrosses = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraCrosses + m_HomeExtraCrosses1 & "</td>")
                End If


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfCrosses + m_HomeSecondHalfCrosses + m_HomeExtraCrosses + m_HomeExtraCrosses1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                If drAwayFirstHalf.Length > 0 Then
                    m_AwayFirstHalfCrosses = CInt(IIf(drAwayFirstHalf(0).Item("Crosses") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("Crosses")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfCrosses & "</td>")
                Else
                    m_AwayFirstHalfCrosses = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfCrosses & "</td>")
                End If
                If drAwaySecondHalf.Length > 0 Then
                    m_AwaySecondHalfCrosses = CInt(IIf(drAwaySecondHalf(0).Item("Crosses") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("Crosses")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfCrosses & "</td>")
                Else
                    m_AwaySecondHalfCrosses = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfCrosses & "</td>")
                End If

                If drAwayExtra1.Length > 0 Then
                    m_AwayExtraCrosses1 = CInt(IIf(drAwayExtra1(0).Item("Crosses") Is DBNull.Value, 0, drAwayExtra1(0).Item("Crosses")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraCrosses1 & "</b></td>")
                Else
                    m_AwayExtraCrosses1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraCrosses1 & "</td>")
                End If

                If drAwayExtra.Length > 0 Then
                    m_AwayExtraCrosses = CInt(IIf(drAwayExtra(0).Item("Crosses") Is DBNull.Value, 0, drAwayExtra(0).Item("Crosses")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraCrosses + m_AwayExtraCrosses1 & "</td>")
                Else
                    m_AwayExtraCrosses = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraCrosses + m_AwayExtraCrosses1 & "</td>")
                End If


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfCrosses + m_AwaySecondHalfCrosses + m_AwayExtraCrosses + m_AwayExtraCrosses1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")

                m_strBuilder.Append("</tr>")
                '-----------------------------------------------------------------------------------------------------------------------------------------



                'Corners ----------------------------------------------------------------------------------------------------------
                m_strBuilder.Append("<tr style=background-color:#f7f7f7>")
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext11 & "</b> </td>")
                m_strBuilder.Append("</td>")


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                If drHomeFirstHalf.Length > 0 Then
                    m_HomeFirstHalfCorners = CInt(IIf(drHomeFirstHalf(0).Item("Corner_Kicks") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("Corner_Kicks")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfCorners & "</td>")
                Else
                    m_HomeFirstHalfCorners = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfCorners & "</td>")
                End If

                If drHomeSecondHalf.Length > 0 Then
                    m_HomeSecondHalfCorners = CInt(IIf(drHomeSecondHalf(0).Item("Corner_Kicks") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("Corner_Kicks")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfCorners & "</td>")
                Else
                    m_HomeSecondHalfCorners = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfCorners & "</td>")
                End If
                If drHomeExtra1.Length > 0 Then
                    m_HomeExtraCorners1 = CInt(IIf(drHomeExtra1(0).Item("Corner_Kicks") Is DBNull.Value, 0, drHomeExtra1(0).Item("Corner_Kicks")))
                    '  m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraCorners1 & "</b></td>")
                Else
                    m_HomeExtraCorners = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraCorners1 & "</td>")
                End If

                If drHomeExtra.Length > 0 Then
                    m_HomeExtraCorners = CInt(IIf(drHomeExtra(0).Item("Corner_Kicks") Is DBNull.Value, 0, drHomeExtra(0).Item("Corner_Kicks")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraCorners + m_HomeExtraCorners1 & "</td>")
                Else
                    m_HomeExtraCorners = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraCorners + m_HomeExtraCorners1 & "</td>")
                End If



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfCorners + m_HomeSecondHalfCorners + m_HomeExtraCorners + m_HomeExtraCorners1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                If drAwayFirstHalf.Length > 0 Then
                    m_AwayFirstHalfCorners = CInt(IIf(drAwayFirstHalf(0).Item("Corner_Kicks") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("Corner_Kicks")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfCorners & "</td>")
                Else
                    m_AwayFirstHalfCorners = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfCorners & "</td>")
                End If
                If drAwaySecondHalf.Length > 0 Then
                    m_AwaySecondHalfCorners = CInt(IIf(drAwaySecondHalf(0).Item("Corner_Kicks") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("Corner_Kicks")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfCorners & "</td>")
                Else
                    m_AwaySecondHalfCorners = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfCorners & "</td>")
                End If
                If drAwayExtra1.Length > 0 Then
                    m_AwayExtraCorners1 = CInt(IIf(drAwayExtra1(0).Item("Corner_Kicks") Is DBNull.Value, 0, drAwayExtra1(0).Item("Corner_Kicks")))
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraCorners1 & "</b></td>")
                Else
                    m_AwayExtraCorners1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraCorners1 & "</td>")
                End If

                If drAwayExtra.Length > 0 Then
                    m_AwayExtraCorners = CInt(IIf(drAwayExtra(0).Item("Corner_Kicks") Is DBNull.Value, 0, drAwayExtra(0).Item("Corner_Kicks")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraCorners + m_AwayExtraCorners1 & "</td>")
                Else
                    m_AwayExtraCorners = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraCorners + m_AwayExtraCorners1 & "</td>")
                End If

                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfCorners + m_AwaySecondHalfCorners + m_AwayExtraCorners + m_AwayExtraCorners1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")

                m_strBuilder.Append("</tr>")
                '---------------------------------------------------------------------------------------------------------------------------------------
            End If
            If m_objGameDetails.CoverageLevel >= 3 Then
                'Missed Penalties = Penalty Kick Attempts  - Penalty Kick goals-------------------------------------------------------------------------
                m_strBuilder.Append("<tr>")
                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & strheadertext12 & "</b> </td>")
                m_strBuilder.Append("</td>")


                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")

                If drHomeFirstHalf.Length > 0 Then
                    m_HomeFirstHalfMissedpen = CInt(IIf(drHomeFirstHalf(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drHomeFirstHalf(0).Item("MISSEDPENALTIES")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfMissedpen & "</td>")
                Else
                    m_HomeFirstHalfMissedpen = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeFirstHalfMissedpen & "</td>")
                End If

                If drHomeSecondHalf.Length > 0 Then
                    m_HomeSecondHalfMissedpen = CInt(IIf(drHomeSecondHalf(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drHomeSecondHalf(0).Item("MISSEDPENALTIES")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfMissedpen & "</td>")
                Else
                    m_HomeSecondHalfMissedpen = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeSecondHalfMissedpen & "</td>")
                End If
                If drHomeExtra1.Length > 0 Then
                    m_HomeExtraMissedpen1 = CInt(IIf(drHomeExtra1(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drHomeExtra1(0).Item("MISSEDPENALTIES")))
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_HomeExtraMissedpen1 & "</b></td>")
                Else
                    m_HomeExtraMissedpen1 = 0
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraMissedpen1 & "</td>")
                End If

                If drHomeExtra.Length > 0 Then
                    m_HomeExtraMissedpen = CInt(IIf(drHomeExtra(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drHomeExtra(0).Item("MISSEDPENALTIES")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraMissedpen + m_HomeExtraMissedpen1 & "</td>")
                Else
                    m_HomeExtraMissedpen = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_HomeExtraMissedpen + m_HomeExtraMissedpen1 & "</td>")
                End If



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_HomeFirstHalfMissedpen + m_HomeSecondHalfMissedpen + m_HomeExtraMissedpen + m_HomeExtraMissedpen1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")



                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS>")
                m_strBuilder.Append("<table width=85% color:black style=font-family:@Arial Unicode MS border=0>")
                If drAwayFirstHalf.Length > 0 Then
                    m_AwayFirstHalfMissedpen = CInt(IIf(drAwayFirstHalf(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drAwayFirstHalf(0).Item("MISSEDPENALTIES")))
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfMissedpen & "</td>")
                Else
                    m_AwayFirstHalfMissedpen = 0
                    m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayFirstHalfMissedpen & "</td>")
                End If
                If drAwaySecondHalf.Length > 0 Then
                    m_AwaySecondHalfMissedpen = CInt(IIf(drAwaySecondHalf(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drAwaySecondHalf(0).Item("MISSEDPENALTIES")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfMissedpen & "</td>")
                Else
                    m_AwaySecondHalfMissedpen = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwaySecondHalfMissedpen & "</td>")
                End If
                If drAwayExtra1.Length > 0 Then
                    m_AwayExtraMissedpen1 = CInt(IIf(drAwayExtra1(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drAwayExtra1(0).Item("MISSEDPENALTIES")))
                    'm_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left><b>" & m_AwayExtraMissedpen1 & "</b></td>")
                Else
                    m_AwayExtraMissedpen1 = 0
                    ' m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraMissedpen1 & "</td>")
                End If
                If drAwayExtra.Length > 0 Then
                    m_AwayExtraMissedpen = CInt(IIf(drAwayExtra(0).Item("MISSEDPENALTIES") Is DBNull.Value, 0, drAwayExtra(0).Item("MISSEDPENALTIES")))
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraMissedpen + m_AwayExtraMissedpen1 & "</td>")
                Else
                    m_AwayExtraMissedpen = 0
                    m_strBuilder.Append("<td width=30% style=font-size:13;color:black; style=font-family:@Arial Unicode MS align=left>" & m_AwayExtraMissedpen + m_AwayExtraMissedpen1 & "</td>")
                End If

                m_strBuilder.Append("<td width=30%  style=font-size:13;color:black; style=font-family:@Arial Unicode MS><b>" & m_AwayFirstHalfMissedpen + m_AwaySecondHalfMissedpen + m_AwayExtraMissedpen + m_AwayExtraMissedpen1 & "</b></td>")

                m_strBuilder.Append("</table>")
                m_strBuilder.Append("</td>")

                m_strBuilder.Append("</tr>")
            End If
     
            


            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</body></html>")

            wbReport.DocumentText = m_strBuilder.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim path As String
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 0)
            If wbReport.DocumentText = Nothing Then
                'MessageDialog.Show("No Document is Present", Me.Text)
                Exit Sub
            Else
                path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                SaveFileDialog1.InitialDirectory = path
                SaveFileDialog1.Filter = "HTML Files|*.htm"
                If SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fileName As String = SaveFileDialog1.FileName
                    Dim sw As StreamWriter
                    If File.Exists(fileName) = False Then
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    Else
                        File.Delete(fileName)
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            PrintDialog1.Document = PrintDocument1
            PrintDialog1.AllowSomePages = True
            PrintDocument1.DefaultPageSettings.Landscape = True
            If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim pritcopy As Integer = DirectCast(DirectCast(PrintDialog1, System.Windows.Forms.PrintDialog).PrinterSettings, System.Drawing.Printing.PrinterSettings).Copies
                For i As Integer = 0 To pritcopy - 1
                    wbReport.Print()
                Next
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh.Text, Nothing, 1, 0)
            DisplayGameStats()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub
End Class