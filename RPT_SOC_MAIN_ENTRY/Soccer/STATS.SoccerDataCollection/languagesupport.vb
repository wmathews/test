﻿Imports STATS.SoccerDataCollection
Imports STATS.Utility
Imports System.Data
Imports System.Text
Imports STATS.SoccerBL
Imports System.Globalization

Public Class languagesupport

    Private m_objWebSoc As STATS.SoccerBL.ClsWebService = STATS.SoccerBL.ClsWebService.GetInstance()
    Public dsMultilingualData As New DataSet
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private _objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private ReadOnly _controlNormal() As String = {"Button", "Label", "CheckBox", "ListBox", "RadioButton"}
    Private ReadOnly _controlGroup() As String = {"Panel", "GroupBox"}
    Private controlLength As Integer
    Private controlFont As Font = New Font(" Arial Unicode MS", 7, FontStyle.Bold)
    ''Private ReadOnly _objUtilAudit As New clsAuditLog

    'For Multilingual
    Public Function LanguageTranslate(ByVal intLangID As Integer, ByVal strLabelDesc As String) As String

        Dim drr() As DataRow = Nothing
        Dim strLabel As String = Nothing
        Dim dr1() As DataRow = Nothing
        Try
            If intLangID <> 1 Then
                strLabelDesc = strLabelDesc.Replace("'", "")
                If (dsMultilingualData.Tables.Count = 0) Then
                    dsMultilingualData = m_objGeneral.LanguageTranslate(intLangID)
                End If
                If Not (dsMultilingualData.Tables(0) Is Nothing) Then
                    If dsMultilingualData.Tables(0).Rows.Count > 0 Then
                        drr = dsMultilingualData.Tables(0).Select("language_id = 1 and label_description = '" & strLabelDesc & "'")
                        If drr.Length = 0 Then
                            strLabel = strLabelDesc
                        Else
                            dr1 = Nothing
                            dr1 = dsMultilingualData.Tables(0).Select("language_id = " & intLangID & " and label_id = " & drr(0).Item(0) & "")
                            If dr1.Length = 0 Then
                                strLabel = strLabelDesc
                            Else
                                strLabel = dr1(0).Item("label_description").ToString
                            End If
                        End If
                    End If
                End If
                Return strLabel
            End If
            Return strLabelDesc
        Catch ex As Exception
            Return strLabel
            MessageBox.Show(ex.ToString, "Multingual", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Function
    Public Function LanguageTranslateora(ByVal intLangID As Integer, ByVal strLabelDesc As String) As String
        Dim drr() As DataRow = Nothing
        Dim strLabel As String = Nothing
        Dim dr1() As DataRow = Nothing

        Try
            If intLangID <> 1 Then
                strLabelDesc = strLabelDesc.Replace("'", "")
                If (dsMultilingualData.Tables.Count = 0) Then
                    dsMultilingualData = m_objWebSoc._objWebSoc.GetMultilingualData(intLangID, System.Configuration.ConfigurationManager.AppSettings("SecurityToken"))
                End If
                If Not (dsMultilingualData.Tables(0) Is Nothing) Then
                    If dsMultilingualData.Tables(0).Rows.Count > 0 Then
                        drr = dsMultilingualData.Tables(0).Select("language_id = 1 and label_description = '" & strLabelDesc & "'")
                        If drr.Length = 0 Then
                            strLabel = strLabelDesc
                        Else
                            dr1 = Nothing
                            dr1 = dsMultilingualData.Tables(0).Select("language_id = " & intLangID & " and label_id = " & drr(0).Item(0) & "")
                            If dr1.Length = 0 Then
                                strLabel = strLabelDesc
                            Else
                                strLabel = dr1(0).Item("label_description").ToString
                            End If
                        End If
                    End If
                End If
                Return strLabel
            End If
            Return strLabelDesc
        Catch ex As Exception
            Return strLabel
            MessageBox.Show(ex.ToString, "Multingual", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Function
    'For Multilingual Line Break
    Public Sub linebreak(ByVal lbtemp As Label, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub
    Public Sub linebreakck(ByVal lbtemp As CheckBox, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub
    Public Sub linebreakrdo(ByVal lbtemp As RadioButton, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub
    Public Function langselect(ByVal temp As Int32)
        Dim lgshort As String
        Dim strlanguagename As String = Nothing
        If (temp = 1) Then
            lgshort = "en-uS"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        ElseIf (temp = 2) Then
            lgshort = "es-ES"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            If (strlanguagename <> lgshort) Then
                lgshort = "es-GT"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
        ElseIf (temp = 3) Then
            lgshort = "zh-TW"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            If (strlanguagename <> lgshort) Then
                lgshort = "zh-TW"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
            If (strlanguagename <> lgshort) Then
                lgshort = "zh-CN"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
            If (strlanguagename <> lgshort) Then
                lgshort = "zh-HK"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
        ElseIf (temp = 4) Then
            lgshort = "ja-JP"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        ElseIf (temp = 5) Then
            lgshort = "de-DE"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString

        ElseIf (temp = 6) Then
            lgshort = "fr-FR"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        ElseIf (temp = 7) Then
            lgshort = "pt-BR"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        ElseIf (temp = 8) Then
            lgshort = "it-IT"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        ElseIf (temp = 9) Then
            lgshort = "nl-NL"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            If (strlanguagename <> lgshort) Then
                lgshort = "nl-NL"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
            If (strlanguagename <> lgshort) Then
                lgshort = "nl-BE"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
        ElseIf (temp = 10) Then
            lgshort = "ar-IQ"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        ElseIf (temp = 13) Then
            lgshort = "es-ES"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            If (strlanguagename <> lgshort) Then
                lgshort = "es-GT"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
            If (strlanguagename <> lgshort) Then
                lgshort = "es-MX"
                Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
                strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
            End If
        ElseIf (temp = 16) Then
            lgshort = "ru-RU"
            Application.CurrentInputLanguage = InputLanguage.FromCulture(New CultureInfo("" + lgshort + ""))
            strlanguagename = Application.CurrentInputLanguage.Culture.Name.ToString
        End If
        Return lgshort
    End Function
    Public Function langget(ByVal lgstr As Int32)
        Dim strlanguagtext As String = Nothing
        If (lgstr = 1) Then
            strlanguagtext = "English"
        ElseIf (lgstr = 2) Then
            strlanguagtext = "Spanish"
        ElseIf (lgstr = 3) Then
            strlanguagtext = "Chinese"
        ElseIf (lgstr = 4) Then
            strlanguagtext = "Japanese"
        ElseIf (lgstr = 5) Then
            strlanguagtext = "German"
        ElseIf (lgstr = 6) Then
            strlanguagtext = "French"
        ElseIf (lgstr = 7) Then
            strlanguagtext = "Portuguese"
        ElseIf (lgstr = 8) Then
            strlanguagtext = "Italian"
        ElseIf (lgstr = 9) Then
            strlanguagtext = "Dutch"
        ElseIf (lgstr = 10) Then
            strlanguagtext = "Arabic"
        ElseIf (lgstr = 13) Then
            strlanguagtext = "Spanish"
        End If
        Return strlanguagtext
    End Function

    'Loop the Form Controls
    Public Sub FormLanguageTranslate(ByVal translateForm As Form)
        Try
            For Each control As Control In translateForm.Controls
                If _controlNormal.Contains(control.GetType().Name.ToString()) Then  'Button, Label , CheckBox
                    FormControlText(control)
                ElseIf _controlGroup.Contains(control.GetType().Name.ToString()) Then 'Panel, GroupBox
                    FormControlText(control) 'Group Box text
                    ControlGroup(control)
                ElseIf control.GetType().Name.ToString() = "DataGridView" Then
                    Dim controlDataGridView As DataGridView = CType(control, DataGridView)
                    For columnIndex As Integer = 0 To controlDataGridView.Columns.Count - 1
                        controlDataGridView.Columns(columnIndex).HeaderText = MultilingualDictionary(controlDataGridView.Columns(columnIndex).HeaderText).ToString()
                    Next
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Loop the Panel / Group Controls
    Public Sub ControlGroup(ByVal controlAssign As Control)
        Try
            For Each subControls As Control In controlAssign.Controls
                If _controlNormal.Contains(subControls.GetType().Name.ToString()) Then
                    FormControlText(subControls)
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Assigning the Control Text in Multilingual
    Public Sub FormControlText(ByVal controlAssign As Control)
        Try
            controlLength = controlAssign.Text.Length
            controlAssign.Text = MultilingualDictionary(controlAssign.Text)
            If controlLength < controlAssign.Text.Length Then 'Check  the length of  the control to adjust the Font Size
                controlAssign.Font = controlFont
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Function MultilingualDictionary(ByVal englishString As String) As String
        Try
            Dim returnString = englishString
            If _objGameDetails.MultilingualData.ContainsKey(englishString) Then
                returnString = _objGameDetails.MultilingualData.Item(returnString)
                'Else
                '_objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, returnString.Trim().Replace(vbNewLine, " "), 1, 0)
            End If
            Return returnString
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class