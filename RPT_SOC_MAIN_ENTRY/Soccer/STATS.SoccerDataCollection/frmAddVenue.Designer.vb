﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddVenue
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.txtCapacity = New System.Windows.Forms.TextBox()
        Me.lblCapacity = New System.Windows.Forms.Label()
        Me.txtNickname = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.cmbLeagueId = New System.Windows.Forms.ComboBox()
        Me.lblNickname = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblLeague = New System.Windows.Forms.Label()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.lvwStadium = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnIgnore = New System.Windows.Forms.Button()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.pnlTimeButtons = New System.Windows.Forms.Panel()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtCity = New System.Windows.Forms.TextBox()
        Me.chkArtificial = New System.Windows.Forms.CheckBox()
        Me.chkDome = New System.Windows.Forms.CheckBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.sstAddGame = New System.Windows.Forms.StatusStrip()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTimeButtons.SuspendLayout()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.BackColor = System.Drawing.Color.Transparent
        Me.lblCity.Location = New System.Drawing.Point(14, 135)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(29, 15)
        Me.lblCity.TabIndex = 6
        Me.lblCity.Text = "City:"
        '
        'txtCapacity
        '
        Me.txtCapacity.Location = New System.Drawing.Point(98, 158)
        Me.txtCapacity.MaxLength = 6
        Me.txtCapacity.Name = "txtCapacity"
        Me.txtCapacity.Size = New System.Drawing.Size(87, 22)
        Me.txtCapacity.TabIndex = 9
        '
        'lblCapacity
        '
        Me.lblCapacity.AutoSize = True
        Me.lblCapacity.BackColor = System.Drawing.Color.Transparent
        Me.lblCapacity.Location = New System.Drawing.Point(14, 161)
        Me.lblCapacity.Name = "lblCapacity"
        Me.lblCapacity.Size = New System.Drawing.Size(53, 15)
        Me.lblCapacity.TabIndex = 8
        Me.lblCapacity.Text = "Capacity:"
        '
        'txtNickname
        '
        Me.txtNickname.Location = New System.Drawing.Point(98, 106)
        Me.txtNickname.Name = "txtNickname"
        Me.txtNickname.Size = New System.Drawing.Size(238, 22)
        Me.txtNickname.TabIndex = 5
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(98, 80)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(238, 22)
        Me.txtName.TabIndex = 3
        '
        'cmbLeagueId
        '
        Me.cmbLeagueId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLeagueId.FormattingEnabled = True
        Me.cmbLeagueId.Location = New System.Drawing.Point(98, 53)
        Me.cmbLeagueId.Name = "cmbLeagueId"
        Me.cmbLeagueId.Size = New System.Drawing.Size(238, 23)
        Me.cmbLeagueId.TabIndex = 1
        '
        'lblNickname
        '
        Me.lblNickname.AutoSize = True
        Me.lblNickname.BackColor = System.Drawing.Color.Transparent
        Me.lblNickname.Location = New System.Drawing.Point(14, 109)
        Me.lblNickname.Name = "lblNickname"
        Me.lblNickname.Size = New System.Drawing.Size(58, 15)
        Me.lblNickname.TabIndex = 4
        Me.lblNickname.Text = "Nickname:"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.BackColor = System.Drawing.Color.Transparent
        Me.lblName.Location = New System.Drawing.Point(14, 83)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(38, 15)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name:"
        '
        'lblLeague
        '
        Me.lblLeague.AutoSize = True
        Me.lblLeague.BackColor = System.Drawing.Color.Transparent
        Me.lblLeague.Location = New System.Drawing.Point(14, 57)
        Me.lblLeague.Name = "lblLeague"
        Me.lblLeague.Size = New System.Drawing.Size(46, 15)
        Me.lblLeague.TabIndex = 0
        Me.lblLeague.Text = "League:"
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-4, 15)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(363, 24)
        Me.picReporterBar.TabIndex = 267
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-4, -1)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(393, 20)
        Me.picTopBar.TabIndex = 266
        Me.picTopBar.TabStop = False
        '
        'lvwStadium
        '
        Me.lvwStadium.AutoArrange = False
        Me.lvwStadium.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwStadium.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7})
        Me.lvwStadium.FullRowSelect = True
        Me.lvwStadium.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwStadium.LabelWrap = False
        Me.lvwStadium.Location = New System.Drawing.Point(9, 42)
        Me.lvwStadium.MultiSelect = False
        Me.lvwStadium.Name = "lvwStadium"
        Me.lvwStadium.ShowItemToolTips = True
        Me.lvwStadium.Size = New System.Drawing.Size(331, 163)
        Me.lvwStadium.TabIndex = 5
        Me.lvwStadium.UseCompatibleStateImageBehavior = False
        Me.lvwStadium.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Name"
        Me.ColumnHeader5.Width = 132
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Nickname"
        Me.ColumnHeader6.Width = 128
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "League"
        Me.ColumnHeader7.Width = 53
        '
        'btnIgnore
        '
        Me.btnIgnore.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnIgnore.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnIgnore.ForeColor = System.Drawing.Color.White
        Me.btnIgnore.Location = New System.Drawing.Point(268, 9)
        Me.btnIgnore.Name = "btnIgnore"
        Me.btnIgnore.Size = New System.Drawing.Size(53, 25)
        Me.btnIgnore.TabIndex = 4
        Me.btnIgnore.Text = "Ignore"
        Me.btnIgnore.UseVisualStyleBackColor = False
        '
        'btnApply
        '
        Me.btnApply.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnApply.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnApply.ForeColor = System.Drawing.Color.White
        Me.btnApply.Location = New System.Drawing.Point(209, 9)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(53, 25)
        Me.btnApply.TabIndex = 3
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = False
        '
        'pnlTimeButtons
        '
        Me.pnlTimeButtons.BackColor = System.Drawing.Color.Lavender
        Me.pnlTimeButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTimeButtons.Controls.Add(Me.lvwStadium)
        Me.pnlTimeButtons.Controls.Add(Me.btnApply)
        Me.pnlTimeButtons.Controls.Add(Me.btnIgnore)
        Me.pnlTimeButtons.Controls.Add(Me.btnDelete)
        Me.pnlTimeButtons.Controls.Add(Me.btnEdit)
        Me.pnlTimeButtons.Controls.Add(Me.btnAdd)
        Me.pnlTimeButtons.Location = New System.Drawing.Point(-3, 193)
        Me.pnlTimeButtons.Name = "pnlTimeButtons"
        Me.pnlTimeButtons.Size = New System.Drawing.Size(373, 216)
        Me.pnlTimeButtons.TabIndex = 12
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(150, 9)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(53, 25)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(91, 9)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(53, 25)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnAdd.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(32, 9)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(53, 25)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'txtCity
        '
        Me.txtCity.Location = New System.Drawing.Point(98, 132)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(238, 22)
        Me.txtCity.TabIndex = 7
        '
        'chkArtificial
        '
        Me.chkArtificial.AutoSize = True
        Me.chkArtificial.Location = New System.Drawing.Point(212, 159)
        Me.chkArtificial.Name = "chkArtificial"
        Me.chkArtificial.Size = New System.Drawing.Size(65, 19)
        Me.chkArtificial.TabIndex = 10
        Me.chkArtificial.Text = "Aritificial"
        Me.chkArtificial.UseVisualStyleBackColor = True
        '
        'chkDome
        '
        Me.chkDome.AutoSize = True
        Me.chkDome.Location = New System.Drawing.Point(280, 159)
        Me.chkDome.Name = "chkDome"
        Me.chkDome.Size = New System.Drawing.Size(54, 19)
        Me.chkDome.TabIndex = 11
        Me.chkDome.Text = "Dome"
        Me.chkDome.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(267, 15)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 13
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'sstAddGame
        '
        Me.sstAddGame.AutoSize = False
        Me.sstAddGame.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstAddGame.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstAddGame.Location = New System.Drawing.Point(0, 456)
        Me.sstAddGame.Name = "sstAddGame"
        Me.sstAddGame.Size = New System.Drawing.Size(346, 16)
        Me.sstAddGame.TabIndex = 14
        Me.sstAddGame.Text = "StatusStrip1"
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(-3, 405)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(393, 60)
        Me.picButtonBar.TabIndex = 279
        '
        'frmAddVenue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(346, 472)
        Me.Controls.Add(Me.sstAddGame)
        Me.Controls.Add(Me.chkDome)
        Me.Controls.Add(Me.chkArtificial)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.pnlTimeButtons)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.lblCity)
        Me.Controls.Add(Me.txtCapacity)
        Me.Controls.Add(Me.lblCapacity)
        Me.Controls.Add(Me.txtNickname)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.cmbLeagueId)
        Me.Controls.Add(Me.lblNickname)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblLeague)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "frmAddVenue"
        Me.Text = "Add Venue"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTimeButtons.ResumeLayout(False)
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtCapacity As System.Windows.Forms.TextBox
    Friend WithEvents lblCapacity As System.Windows.Forms.Label
    Friend WithEvents txtNickname As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmbLeagueId As System.Windows.Forms.ComboBox
    Friend WithEvents lblNickname As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblLeague As System.Windows.Forms.Label
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents lvwStadium As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnIgnore As System.Windows.Forms.Button
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents pnlTimeButtons As System.Windows.Forms.Panel
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtCity As System.Windows.Forms.TextBox
    Friend WithEvents chkArtificial As System.Windows.Forms.CheckBox
    Friend WithEvents chkDome As System.Windows.Forms.CheckBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents sstAddGame As System.Windows.Forms.StatusStrip
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
