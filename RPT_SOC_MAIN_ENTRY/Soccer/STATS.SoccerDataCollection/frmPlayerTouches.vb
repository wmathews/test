﻿#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text
Imports System.IO

#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmPlayerTouches
' Author        : Shravani
' Created Date  : 07-AUG-09
' Description   : DIPLAYS THE TEAM STATISTICS
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------

#End Region


Public Class frmPlayerTouches

#Region " Constants "
    Dim strBuilder As New StringBuilder
    Private m_objPlayerTouches As clsPlayerTouches = clsPlayerTouches.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_HomeFirstHalfGoals, m_HomeSecondHalfGoals, m_AwayFirstHalfGoals, m_AwaySecondHalfGoals As Integer
    Private m_HomeFirstHalfOnTargets, m_HomeSecondHalfOnTargets, m_AwayFirstHalfOnTargets, m_AwaySecondHalfOnTargets As Integer
    Private m_HomeFirstHalfOffTargets, m_HomeSecondHalfOffTargets, m_AwayFirstHalfOffTargets, m_AwaySecondHalfOffTargets As Integer
    Private m_HomeFirstHalfFouls, m_HomeSecondHalfFouls, m_AwayFirstHalfFouls, m_AwaySecondHalfFouls As Integer
    Private m_HomeFirstHalfOffSides, m_HomeSecondHalfOffSides, m_AwayFirstHalfOffSides, m_AwaySecondHalfOffSides As Integer
    Private m_HomeFirstHalfCrosses, m_HomeSecondHalfCrosses, m_AwayFirstHalfCrosses, m_AwaySecondHalfCrosses As Integer
    Private m_HomeFirstHalfCorners, m_HomeSecondHalfCorners, m_AwayFirstHalfCorners, m_AwaySecondHalfCorners As Integer
    Private m_HomeFirstHalfYCards, m_HomeSecondHalfYCards, m_AwayFirstHalfYCards, m_AwaySecondHalfYCards As Integer
    Private m_HomeFirstHalfRCards, m_HomeSecondHalfRCards, m_AwayFirstHalfRCardss, m_AwaySecondHalfRCards As Integer
    Private m_HomeFirstHalfGoalAttTot, m_HomeSecondHalfGoalAttTot, m_AwayFirstHalfGoalAttTot, m_AwaySecondHalfGoalAttTot As Integer
    Private m_HomeFirstHalfMissedpen, m_HomeSecondHalfMissedpen, m_AwayFirstHalfMissedpen, m_AwaySecondHalfMissedpen As Integer
    Private m_objUtilAudit As New clsAuditLog
    Dim m_strBuilder As StringBuilder
    Private lsupport As New languagesupport
    Dim strheadertext1 As String = "Player"
#End Region

    Private Sub frmPlayerTouches_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        Try
            DisplayGameStats()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub frmPlayerTouches_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPlayerTouches", Nothing, 1, 0)
            Me.Icon = frmMain.Icon
            DisplayGameStats()
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnSave2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave2.Text)
                btnPrint2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPrint2.Text)
                btnRefresh2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnRefresh2.Text)
                strheadertext1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext1)
                btnClose2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose2.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub DisplayGameStats()
        Try

            m_strBuilder = New StringBuilder
            Dim dsGameStats As New DataSet
            Dim dsTouchTypes As New DataSet
            Dim dsHomeRoster As New DataSet
            Dim dsAwayRoster As New DataSet
            Dim strBlank As String = "                 "

            Dim m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()


            m_objPlayerTouches.InsertPlayerTouchestoSummary(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)

            dsGameStats.Tables.Clear()
            dsGameStats = m_objPlayerTouches.GetPlayerTouches(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid, m_objGameDetails.SerialNo, m_objGameDetails.HomeTeamID, m_objGameDetails.AwayTeamID, 0)

            dsTouchTypes.Tables.Clear()
            dsTouchTypes = m_objPlayerTouches.GetTouchTypes()

            dsHomeRoster.Tables.Clear()
            dsHomeRoster = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objGameDetails.HomeTeamID, m_objGameDetails.languageid)
            dsAwayRoster.Tables.Clear()
            dsAwayRoster = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objGameDetails.AwayTeamID, m_objGameDetails.languageid)



            m_strBuilder.Append("<html><head><style type=""text/css"">table { border-collapse: collapse; font-family: Arial Unicode MS; border: 1px solid black; font-size: 12px;} td { border: 1px solid black; text-align: center;} th { border: 1px solid black }</style></head><body>")



            m_strBuilder.Append("<table>")
            'HEADINGS
            m_strBuilder.Append("<tr>")
            If CInt(m_objGameDetails.languageid) <> 1 Then
                strheadertext1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext1)
                m_strBuilder.Append("<th>" & strheadertext1 & "</th>")
            Else
                m_strBuilder.Append("<th>" & strheadertext1 & "</th>")
            End If



            Dim drTouchTypes(), drPlayerTouches(), drHomePlayers(), drAwayPlayers() As DataRow
            drTouchTypes = dsTouchTypes.Tables(0).Select()

            For i As Integer = 0 To (drTouchTypes.Length - 1)
                Dim strtochdesc As String = drTouchTypes(i)("TOUCH_TYPE_DESC").ToString
                If CInt(m_objGameDetails.languageid) <> 1 Then
                    strtochdesc = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strtochdesc)
                End If


                m_strBuilder.Append("<th>" & strtochdesc & "</th>")
            Next i
            m_strBuilder.Append("</tr>")






            drAwayPlayers = dsAwayRoster.Tables(0).Select()
            drHomePlayers = dsHomeRoster.Tables(0).Select()


            m_strBuilder.Append("<tr><th colspan=""14"">" & m_objGameDetails.AwayTeam & "</th></tr>")
            For i As Integer = 0 To (drAwayPlayers.Length - 1)

                'drPlayerTouches = dsGameStats.Tables(0).Select(" team_id =" & m_objGameDetails.AwayTeamID.ToString)
                m_strBuilder.Append("<tr><th>" & drAwayPlayers(i)("PLAYER_NAME").ToString & "</th>")

                For j As Integer = 0 To (drTouchTypes.Length - 1)
                    drPlayerTouches = dsGameStats.Tables(0).Select(" player_id =" & drAwayPlayers(i)("PLAYER_ID").ToString & " AND touch_type_id =" & drTouchTypes(j)("TOUCH_TYPE_ID").ToString)
                    
                    m_strBuilder.Append("<td>" & drPlayerTouches.Length.ToString & "</td>")




                Next


                m_strBuilder.Append("</tr>")

            Next
            m_strBuilder.Append("<td><b><i><font color='green'> TOTAL </font></i></b></td>")
            For j As Integer = 0 To (drTouchTypes.Length - 1)
                drPlayerTouches = dsGameStats.Tables(0).Select(" team_id =" & m_objGameDetails.AwayTeamID.ToString & " AND touch_type_id =" & drTouchTypes(j)("TOUCH_TYPE_ID").ToString)
                m_strBuilder.Append("<td><b><i><font color='green'>" & drPlayerTouches.Length.ToString & "</font></i></b></td>")
            Next

            ''m_strBuilder.Append("<tr>     </tr>")
            'For j As Integer = 0 To (drTouchTypes.Length - 1)
            '    m_strBuilder.Append("<tr> <td>                                            </td></tr>")
            'Next
            m_strBuilder.Append("<tr><th colspan=""28""><font color='white'>" & "." & "</font></th></tr>")
            m_strBuilder.Append("<tr><th colspan=""28"">" & m_objGameDetails.HomeTeam & "</th></tr>")
            For i As Integer = 0 To (drHomePlayers.Length - 1)

                'drPlayerTouches = dsGameStats.Tables(0).Select(" team_id =" & m_objGameDetails.AwayTeamID.ToString)
                m_strBuilder.Append("<tr><th>" & drHomePlayers(i)("PLAYER_NAME").ToString & "</th>")

                For j As Integer = 0 To (drTouchTypes.Length - 1)
                    drPlayerTouches = dsGameStats.Tables(0).Select(" player_id =" & drHomePlayers(i)("PLAYER_ID").ToString & " AND touch_type_id =" & drTouchTypes(j)("TOUCH_TYPE_ID").ToString)

                    m_strBuilder.Append("<td>" & drPlayerTouches.Length.ToString & "</td>")
                Next
                m_strBuilder.Append("</tr>")
            Next
            m_strBuilder.Append("<td><b><i><font color='green'> TOTAL </font></i></b></td>")
            For j As Integer = 0 To (drTouchTypes.Length - 1)
                drPlayerTouches = dsGameStats.Tables(0).Select(" team_id =" & m_objGameDetails.HomeTeamID.ToString & " AND touch_type_id =" & drTouchTypes(j)("TOUCH_TYPE_ID").ToString)
                m_strBuilder.Append("<td><b><i><font color='green'>" & drPlayerTouches.Length.ToString & "</font></i></b></td>")
            Next
            m_strBuilder.Append("</body></html>")

            wbReport2.DocumentText = m_strBuilder.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Sub btnSave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave2.Click
        Dim path As String
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave2.Text, Nothing, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            If wbReport.DocumentText = Nothing Then
                'MessageDialog.Show("No Document is Present", Me.Text)
                Exit Sub
            Else
                path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                SaveFileDialog2.InitialDirectory = path
                SaveFileDialog2.Filter = "HTML Files|*.htm"
                If SaveFileDialog2.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fileName As String = SaveFileDialog2.FileName
                    Dim sw As StreamWriter
                    If File.Exists(fileName) = False Then
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    Else
                        File.Delete(fileName)
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnPrint2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint2.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint2.Text, Nothing, 1, 0)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            PrintDialog2.Document = PrintDocument2
            PrintDialog2.AllowSomePages = True
            PrintDocument2.DefaultPageSettings.Landscape = True
            If PrintDialog2.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim pritcopy As Integer = DirectCast(DirectCast(PrintDialog1, System.Windows.Forms.PrintDialog).PrinterSettings, System.Drawing.Printing.PrinterSettings).Copies
                For i As Integer = 0 To pritcopy - 1
                    wbReport2.Print()
                Next
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub btnRefresh2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh2.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh2.Text, Nothing, 1, 0)
            DisplayGameStats()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub btnClose2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose2.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose2.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub picButtonBar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picButtonBar2.Click

    End Sub
End Class