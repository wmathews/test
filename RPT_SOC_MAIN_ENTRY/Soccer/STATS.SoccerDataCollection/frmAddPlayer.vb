﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmAddPlayer
' Author        : Shravani
' Created Date  : 16-05-09
' Description   : This form is used to add the player
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmAddPlayer

#Region " Constants & Variables "

    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objValidation As New clsValidation
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objAddPlayer As clsAddPlayer = clsAddPlayer.GetInstance()
    Private m_blnIsComboLoaded As Boolean = False
    Private m_dsPosition As New DataSet
    Private m_intTeamID As Integer

    Private m_strCheckButtonOption As String
    Private m_strNupId As String
    Private m_dsAddPlayer As New DataSet
    Private m_blnCboValidate As Boolean = False
    Private m_intTabind As Integer
    Private m_strFirstName As String = String.Empty
    Private m_strLastName As String = String.Empty
    Private m_strEditUniformNumber As String = String.Empty
    Private m_strUniformNumber As String
    Private StrTeamChecked As String
    Private MessageDialog As New frmMessageDialog
    Private lsupport As New languagesupport
    Dim strmessage As String = "Select Team.."
    Dim strmessage1 As String = "Select Player Position"
    Dim strmessage2 As String = "Uniform Number is Not Entered"
    Dim strmessage3 As String = "Player Last Name is Not Entered"
    Dim strmessage4 As String = "Please Select Valid"
    Dim strmessage5 As String = "Select a Team to Which the player Belongs"
    Dim strmessage6 As String = "The same uniform number exists for"
    Dim strmessage7 As String = "This player can not be deleted - already used in this game and it is preserved as a history!"
    Dim strmessage8 As String = "Player deleted successfully."
    Dim strmessage9 As String = "Please Apply or Ignore the changes"
    Dim strmessage10 As String = "Please select a player"
    Dim strmessage11 As String = "Select a player first!"
    Dim strmessage12 As String = "Are you sure you want to delete?"
    Dim strmessage13 As String = "The Player cannot be added at this time. Please check your internet connection and try again after sometime."
#End Region

#Region " EVENT HANDLERS "

    Private Sub frmAddPlayer_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave

    End Sub

    Private Sub frmAddPlayer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmAddPlayer.vb", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)

            'ASSIGNING HOME AND AWAY TEAM NAMES TO THE RADIO BUTTONS
            'radHomeTeam.Text = m_objclsGameDetails.HomeTeam
            'radAwayTeam.Text = m_objclsGameDetails.AwayTeam
            If m_objGameDetails.HomeTeamAbbrev.Length > 12 Then
                radHomeTeam.Text = m_objGameDetails.HomeTeamAbbrev.Substring(0, 12)
            Else
                radHomeTeam.Text = m_objGameDetails.HomeTeamAbbrev
            End If

            If m_objGameDetails.AwayTeamAbbrev.Length > 12 Then
                radAwayTeam.Text = m_objGameDetails.AwayTeamAbbrev.Substring(0, 12)
            Else
                radAwayTeam.Text = m_objGameDetails.AwayTeamAbbrev
            End If

            'BY DEFAULT SELECT THE HOME TEAM RADIO BUTTON
            'radHomeTeam.Checked = True
            'radAwayTeam.Checked = False
            If StrTeamChecked = "Home" Then
                radHomeTeam.Checked = True
                radAwayTeam.Checked = False
                'lblTeamName.Text = radHomeTeam.Text
                lblTeamName.Text = m_objGameDetails.HomeTeamAbbrev
            ElseIf StrTeamChecked = "Away" Then
                radHomeTeam.Checked = False
                radAwayTeam.Checked = True
                'lblTeamName.Text = radAwayTeam.Text
                lblTeamName.Text = m_objGameDetails.AwayTeamAbbrev
            End If

            LoadPositionCombo()

            m_blnCboValidate = False
            ClearTextBoxes()
            EnableDisableButton(True, False, False, False, False)
            EnableDisableTextBox(False)
            LoadNewlyAddedPlayers()
            m_blnCboValidate = True

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                lblFirstName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblFirstName.Text)
                lblLastName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLastName.Text)
                lblUniformNumber.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblUniformNumber.Text)
                lblPosition.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblPosition.Text)
                btnAdd.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAdd.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnApply.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnApply.Text)
                btnIgnore.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnIgnore.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                radHomeTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radHomeTeam.Text)
                radAwayTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radAwayTeam.Text)
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Player")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Uniform")
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Position")
                lvwPlayer.Columns(0).Text = g1
                lvwPlayer.Columns(1).Text = g2
                lvwPlayer.Columns(2).Text = g3
                lvwPlayer.Columns(3).Text = g4
                strmessage11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage11)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' CLOSES THE FORM
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
                Exit Sub
            Else
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, "Add New Player", 1, 0)
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radAwayTeam_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAwayTeam.CheckedChanged
        Try
            'LOGO , TEAM NAMES AND PLAYERS ARE LOADED BASED ON TEAM SELECTION
            If radAwayTeam.Checked = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radAwayTeam.Text, 1, 0)
                lblTeamName.Text = radAwayTeam.Text.Trim
                FetchTeamID(radAwayTeam.Text)
                'LOGO TO DISPLAY
                FetchTeamLogo(m_intTeamID)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radHomeTeam_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radHomeTeam.CheckedChanged
        Try

            If radHomeTeam.Checked = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radHomeTeam.Text, 1, 0)
                lblTeamName.Text = radHomeTeam.Text.Trim
                FetchTeamID(radHomeTeam.Text)
                'LOGO TO DISPLAY
                FetchTeamLogo(m_intTeamID)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmAddPlayer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show(strmessage9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
                e.Cancel = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
    '    Dim dsAddnewplayer As New DataSet
    '    Try
    '        If (RequireFieldValidation()) Then
    '            lvwPlayer.Enabled = True
    '            If m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update" Then
    '                Dim strUniform As String
    '                If txtUniformnumber.Text.Length = 3 Then
    '                    If txtUniformnumber.Text.Substring(0, 1) = "0" Then
    '                        If txtUniformnumber.Text.Substring(0, 2) = "00" Then
    '                            If txtUniformnumber.Text.Substring(0, 3) = "000" Then
    '                                strUniform = "0"
    '                            Else
    '                                strUniform = txtUniformnumber.Text.Substring(2, 1)
    '                            End If

    '                        Else
    '                            strUniform = txtUniformnumber.Text.Substring(1, 2)
    '                        End If
    '                    Else
    '                        strUniform = txtUniformnumber.Text
    '                    End If
    '                ElseIf txtUniformnumber.Text.Length = 2 Then
    '                    If txtUniformnumber.Text.Substring(0, 1) = "0" Then
    '                        If txtUniformnumber.Text.Substring(0, 2) = "00" Then
    '                            strUniform = "0"
    '                        Else
    '                            strUniform = txtUniformnumber.Text.Substring(1, 1)
    '                        End If
    '                    Else
    '                        strUniform = txtUniformnumber.Text
    '                    End If
    '                Else
    '                    strUniform = txtUniformnumber.Text
    '                End If

    '                m_strNupId = "99" & Format(m_intTeamID, "0000") & Format(Convert.ToInt32(txtUniformnumber.Text.Trim), "000")
    '                m_objAddPlayer.InsertUpdatePlayer(m_strNupId, m_objclsGameDetails.LeagueID, m_intTeamID, txtlastname.Text.Trim(), txtFirstname.Text.Trim(), strUniform.Trim(), strUniform.Trim(), Integer.Parse(cmbPosition.SelectedValue.ToString()))

    '                LoadNewlyAddedPlayers()
    '                ClearTextBoxes()
    '                'txtFirstname.Focus()
    '                'EnableDisableButton(False, False, False, True, False)
    '                'm_strCheckButtonOption = ""

    '                EnableDisableButton(True, False, False, False, False)
    '                EnableDisableTextBox(False, False, False, False, True)
    '                cmbPosition.SelectedIndex = 0
    '                btnAdd.Focus()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvwPlayer.SelectedItems.Count = 0 Then
                MessageDialog.Show(strmessage11, "Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            lvwPlayer.Enabled = False
            If (MessageDialog.Show(strmessage12, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, 1, 0)

                m_strNupId = lvwPlayer.SelectedItems(0).SubItems(4).Text
                If radHomeTeam.Checked = True Then
                    FetchTeamID(radHomeTeam.Text)
                Else
                    FetchTeamID(radAwayTeam.Text)
                End If
                'Before deleting a new player,check in touches/pbp table if this player is used..
                Dim PlryCntInPBP As Integer
                PlryCntInPBP = m_objAddPlayer.DeleteAddPlayer(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CLng(m_strNupId), m_objclsGameDetails.LeagueID, m_intTeamID, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")), m_objGameDetails.CoverageLevel)

                'TOSOCRS-204 Deleting players with PBP and Touch records
                If (PlryCntInPBP = 0) Then
                    MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Else
                    MessageDialog.Show(strmessage7, "Player Delete", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If

                EnableDisableTextBox(False)
                EnableDisableButton(True, False, False, False, False)
                LoadNewlyAddedPlayers()
                ClearTextBoxes()
                radAwayTeam.Enabled = False
                radHomeTeam.Enabled = False
                cmbPosition.SelectedIndex = 0
                lvwPlayer.Enabled = True
            Else
                EnableDisableTextBox(False)
                EnableDisableButton(True, False, False, False, False)
                LoadNewlyAddedPlayers()
                ClearTextBoxes()
                radAwayTeam.Enabled = False
                radHomeTeam.Enabled = False
                cmbPosition.SelectedIndex = 0
                lvwPlayer.Enabled = True
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAdd.Text, 1, 0)

            m_strCheckButtonOption = "Save"
            EnableDisableButton(False, False, False, True, True)
            EnableDisableTextBox(True, True, True, True, False)
            radHomeTeam.Enabled = True
            radAwayTeam.Enabled = True
            'radHomeTeam.Checked = True
            ClearTextBoxes()
            cmbPosition.SelectedIndex = 0
            txtFirstname.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' VALIDATES THE UNIFORMNUMBER TEXTBOX
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtUniformnumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUniformnumber.KeyPress
        Try
            Try
                If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 8 And Not IsNumeric(e.KeyChar) Then
                    e.Handled = True
                End If
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' VALIDATES THE FIRST NAME TEXTBOX
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtFirstname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFirstname.KeyPress
        Try
            'TOSOCRS-296
            If (Microsoft.VisualBasic.Asc(e.KeyChar) < 65) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 90) And (Microsoft.VisualBasic.Asc(e.KeyChar) < 97) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 122) Then
                'Allowed space
                If (Microsoft.VisualBasic.Asc(e.KeyChar) <> 32 And Microsoft.VisualBasic.Asc(e.KeyChar) <> 8 And Microsoft.VisualBasic.Asc(e.KeyChar) <> 39 And Microsoft.VisualBasic.Asc(e.KeyChar) <> 45) Then
                    e.Handled = True
                End If
            End If
            
            'If (Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 39 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 45) Then
            '    e.Handled = False
            'End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' VALIDATES THE LASTNAME TEXTNAME
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub txtlastname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlastname.KeyPress
        Try
            'TOSOCRS-296
            If (Microsoft.VisualBasic.Asc(e.KeyChar) < 65) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 90) And (Microsoft.VisualBasic.Asc(e.KeyChar) < 97) Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 122) Then
                'Allowed space
                If (Microsoft.VisualBasic.Asc(e.KeyChar) <> 32 And Microsoft.VisualBasic.Asc(e.KeyChar) <> 8 And Microsoft.VisualBasic.Asc(e.KeyChar) <> 39 And Microsoft.VisualBasic.Asc(e.KeyChar) <> 45) Then
                    e.Handled = True
                End If
            End If
            '' Allowed backspace        
            'If (Microsoft.VisualBasic.Asc(e.KeyChar) = 8 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 39 Or Microsoft.VisualBasic.Asc(e.KeyChar) = 45) Then
            '    e.Handled = False
            'End If
            If Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Dim dsAddnewplayer As New DataSet
        Try
            If (RequireFieldValidation()) Then
                lvwPlayer.Enabled = True
                If m_strCheckButtonOption = "Save" Then
                    Dim strDispUniform As String
                    If txtUniformnumber.Text.Length = 2 Then
                        If txtUniformnumber.Text.Substring(0, 1) = "0" Then
                            If txtUniformnumber.Text.Substring(0, 2) = "00" Then
                                strDispUniform = "0"
                            Else
                                strDispUniform = txtUniformnumber.Text.Substring(1, 1)
                            End If
                        Else
                            strDispUniform = txtUniformnumber.Text
                        End If
                    Else
                        strDispUniform = txtUniformnumber.Text
                    End If

                    'checking whether in uniform field the value exists, If so, then generate the uniform number which is not used by the team
                    Dim strUniform As String = GetUnusedUniformNumber()
                    ' display unifrom number - value entered by user
                    ' uniform no - unused no if exists ..
                    m_strNupId = "99" & Format(m_intTeamID, "00000") & Format(Convert.ToInt32(strUniform.Trim), "00")
                    Dim intPlayerId As Long
                    If (m_objGameDetails.IsNewDemomode = True) Then
                        intPlayerId = CLng(m_strNupId)
                    Else
                        Try
                            intPlayerId = CLng(m_objAddPlayer.FetchPlayerIDFromOracle())
                        Catch ex As Exception
                            MessageDialog.Show(strmessage13, "New Player", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Exit Sub
                        End Try
                    End If
                    
                    m_objAddPlayer.InsertUpdatePlayer(CLng(intPlayerId), m_objclsGameDetails.LeagueID, m_intTeamID, txtlastname.Text.Trim(), txtFirstname.Text.Trim(), strUniform.Trim(), strDispUniform.Trim(), Integer.Parse(cmbPosition.SelectedValue.ToString()), CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")))
                    MessageDialog.Show("Player successfully added for " & FetchTeamAbbrev(m_intTeamID) & " team.", "New Player", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                ElseIf m_strCheckButtonOption = "Update" Then
                    m_strNupId = lvwPlayer.SelectedItems(0).SubItems(4).Text
                    m_objAddPlayer.InsertUpdatePlayer(CLng(m_strNupId), m_objclsGameDetails.LeagueID, m_intTeamID, txtlastname.Text.Trim(), txtFirstname.Text.Trim(), txtUniformnumber.Text.Trim(), txtUniformnumber.Text.Trim(), Integer.Parse(cmbPosition.SelectedValue.ToString()), CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")))
                End If

                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnApply.Text, 1, 0)

                LoadNewlyAddedPlayers()
                ClearTextBoxes()
                'txtFirstname.Focus()
                'EnableDisableButton(False, False, False, True, False)
                m_strCheckButtonOption = ""
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, False, False, True)
                cmbPosition.SelectedIndex = 0
                radAwayTeam.Enabled = True
                radHomeTeam.Enabled = True
                radHomeTeam.Checked = True
                m_objGameDetails.IsEditMode = False
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function GetUnusedUniformNumber() As String
        Try
            Dim listselectDatarow() As DataRow
            'validate uniform number
            If (m_objclsGameDetails.HomeTeamID = m_intTeamID) Then
                If (txtUniformnumber.Text.Trim.Length = 1) Then
                    If (txtUniformnumber.Text = "0") Then
                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM=0 Or UNIFORM=00 ")
                    Else
                        Dim strUniform1 As String = "0" & txtUniformnumber.Text.Trim()
                        Dim strUniform2 As String = "00" & txtUniformnumber.Text.Trim
                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM= '" & txtUniformnumber.Text & "' " & "OR UNIFORM= '" & strUniform1 & "'" & "OR UNIFORM= '" & strUniform2 & "'")
                    End If
                ElseIf (txtUniformnumber.Text.Trim.Length = 2) Then
                    If (txtUniformnumber.Text = "00") Then
                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM=0 Or UNIFORM=00 ")
                    Else
                        If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                            Dim strUniform1 As String = txtUniformnumber.Text.Substring(1, 1)
                            Dim strUniform2 As String = "00" & txtUniformnumber.Text.Substring(1)
                            listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "Or UNIFORM='" & strUniform1 & "'" & "OR UNIFORM= '" & strUniform2 & "' ")
                        Else
                            Dim strUniform2 As String = "0" & txtUniformnumber.Text
                            listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "OR UNIFORM= '" & strUniform2 & "' ")
                        End If
                    End If
                ElseIf (txtUniformnumber.Text.Trim.Length = 3) Then
                    If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                        If (txtUniformnumber.Text.Substring(0, 2) = "00") Then
                            listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "Or UNIFORM='" & txtUniformnumber.Text.Substring(2) & "'" & "OR UNIFORM= '" & txtUniformnumber.Text.Substring(1) & "' ")
                        Else
                            listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "OR UNIFORM= '" & txtUniformnumber.Text.Substring(1) & "' ")
                        End If

                    Else
                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("UNIFORM= '" & txtUniformnumber.Text & "' ")
                    End If
                End If
            Else
                If (txtUniformnumber.Text.Trim.Length = 1) Then
                    If (txtUniformnumber.Text = "0") Then
                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM=0 Or UNIFORM=00 ")
                    Else
                        Dim strUniform1 As String = "0" & txtUniformnumber.Text.Trim
                        Dim strUniform2 As String = "00" & txtUniformnumber.Text.Trim
                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM= '" & txtUniformnumber.Text & "' " & "OR UNIFORM= '" & strUniform1 & "'" & "OR UNIFORM= '" & strUniform2 & "'")
                    End If
                ElseIf (txtUniformnumber.Text.Trim.Length = 2) Then
                    If (txtUniformnumber.Text = "00") Then
                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM=0 Or UNIFORM=00 OR UNIFORM=000")
                    Else
                        If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                            Dim strUniform1 As String = txtUniformnumber.Text.Substring(1, 1)
                            Dim strUniform2 As String = "00" & txtUniformnumber.Text.Substring(1)
                            listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "Or UNIFORM='" & strUniform1 & "'" & "OR UNIFORM= '" & strUniform2 & "' ")
                        Else
                            Dim strUniform2 As String = "0" & txtUniformnumber.Text
                            listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "OR UNIFORM= '" & strUniform2 & "' ")
                        End If
                    End If
                ElseIf (txtUniformnumber.Text.Trim.Length = 3) Then
                    If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                        If (txtUniformnumber.Text.Substring(0, 2) = "00") Then
                            listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "Or UNIFORM='" & txtUniformnumber.Text.Substring(2) & "'" & "OR UNIFORM= '" & txtUniformnumber.Text.Substring(1) & "' ")
                        Else
                            listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM= '" & txtUniformnumber.Text & "'" & "OR UNIFORM= '" & txtUniformnumber.Text.Substring(1) & "' ")
                        End If

                    Else
                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("UNIFORM= '" & txtUniformnumber.Text & "' ")
                    End If
                End If
            End If
            If (listselectDatarow.Length >= 1) Then
                'if the number already exists 
                Dim dsNewPlayer As New DataSet
                dsNewPlayer = m_objAddPlayer.GetUnusedUniformNumber(m_intTeamID)
                If dsNewPlayer.Tables.Count > 0 Then
                    Return dsNewPlayer.Tables(0).Rows(0).Item("UNIFORM").ToString
                Else
                    Return txtUniformnumber.Text
                End If
            Else
                Return txtUniformnumber.Text
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnIgnore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            Try
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnIgnore.Text, 1, 0)

                ClearTextBoxes()
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, False, False, True)
                lvwPlayer.Enabled = True
                m_strCheckButtonOption = ""
                cmbPosition.SelectedIndex = 0
                'Dec 9 2009 Shirley Modified
                'radAwayTeam.Enabled = True
                'radHomeTeam.Enabled = True
                'radHomeTeam.Checked = True
                'If StrTeamChecked = "Home" Then
                '    radHomeTeam.Checked = True
                '    radAwayTeam.Checked = False
                '    lblTeamName.Text = radHomeTeam.Text
                'ElseIf StrTeamChecked = "Away" Then
                '    radHomeTeam.Checked = False
                '    radAwayTeam.Checked = True
                '    lblTeamName.Text = radAwayTeam.Text
                'End If

                radHomeTeam.Enabled = True
                radAwayTeam.Enabled = True
                If StrTeamChecked = "Home" Then
                    radHomeTeam.Checked = True
                    radAwayTeam.Checked = False
                ElseIf StrTeamChecked = "Away" Then
                    radHomeTeam.Checked = False
                    radAwayTeam.Checked = True
                End If
                lblTeamName.Text = radAwayTeam.Text
                lblTeamName.Text = radHomeTeam.Text
                m_objGameDetails.IsEditMode = False
                btnAdd.Focus()
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwPlayer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPlayer.Click
        Try
            EnableDisableButton(True, True, True, False, False)
            SelectedListviewDisplay()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwPlayer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPlayer.SelectedIndexChanged
        Try
            If m_objGameDetails.IsEditMode = False Then
                EnableDisableButton(True, True, True, False, False)
            Else
                EnableDisableButton(True, True, True, True, True)
            End If

            SelectedListviewDisplay()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, 1, 0)
            If lvwPlayer.SelectedItems.Count = 0 Then
                MessageDialog.Show(strmessage11, "Edit", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            'radHomeTeam.Enabled = True
            'radAwayTeam.Enabled = True
            If radAwayTeam.Checked = True Then
                radHomeTeam.Enabled = False
            ElseIf radHomeTeam.Checked = True Then
                radAwayTeam.Enabled = False
            End If
            m_objGameDetails.IsEditMode = True
            'lvwPlayer.Enabled = False
            EnableDisableTextBox(True)
            m_strCheckButtonOption = "Update"
            m_strEditUniformNumber = txtUniformnumber.Text

            'radAwayTeam.Enabled = True
            'radHomeTeam.Enabled = True
            EnableDisableButton(False, False, False, True, True)
            lvwPlayer.Enabled = False
            txtFirstname.Focus()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtUniformnumber_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUniformnumber.Validated

        ' Instead of  Uniform number, Display uniform number is used to check if the uniform number already exists
        ' modified by shirley 16/11/2009
        ' works for both ADD/UPDATE
        m_dsAddPlayer = m_objAddPlayer.SelectAddPlayer(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.SortOrder, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")))

        Dim listselectDatarow() As DataRow
        Try
            'AUDIT TRIAL
            If txtUniformnumber.Text <> m_strUniformNumber Then
                If txtUniformnumber.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtUniformnumber.Text, lblUniformNumber.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtUniformnumber.Text & "' '", lblUniformNumber.Text, 1, 0)
                End If
            End If

            m_strUniformNumber = txtUniformnumber.Text
            If (m_strCheckButtonOption = "Save") Or (m_strCheckButtonOption = "Update") Then
                If radHomeTeam.Checked = True Or radAwayTeam.Checked = True Then
                    If txtUniformnumber.Text <> "" Then

                        If (m_objclsGameDetails.HomeTeamID = m_intTeamID) Then
                            If (txtUniformnumber.Text.Trim.Length = 1) Then
                                If (txtUniformnumber.Text = "0") Then
                                    listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    Dim strUniform1 As String = "0" & txtUniformnumber.Text.Trim()
                                    Dim strUniform2 As String = "00" & txtUniformnumber.Text.Trim
                                    listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "'")
                                End If
                            ElseIf (txtUniformnumber.Text.Trim.Length = 2) Then
                                If (txtUniformnumber.Text = "00") Then
                                    listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                                        Dim strUniform1 As String = txtUniformnumber.Text.Substring(1, 1)
                                        Dim strUniform2 As String = "00" & txtUniformnumber.Text.Substring(1)
                                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    Else
                                        Dim strUniform2 As String = "0" & txtUniformnumber.Text
                                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    End If
                                End If
                            ElseIf (txtUniformnumber.Text.Trim.Length = 3) Then
                                If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                                    If (txtUniformnumber.Text.Substring(0, 2) = "00") Then
                                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & txtUniformnumber.Text.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text.Substring(1) & "' ")
                                    Else
                                        listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text.Substring(1) & "' ")
                                    End If

                                Else
                                    listselectDatarow = m_dsAddPlayer.Tables(1).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "' ")
                                End If
                            End If
                        Else
                            If (txtUniformnumber.Text.Trim.Length = 1) Then
                                If (txtUniformnumber.Text = "0") Then
                                    listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    Dim strUniform1 As String = "0" & txtUniformnumber.Text.Trim
                                    Dim strUniform2 As String = "00" & txtUniformnumber.Text.Trim
                                    listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "'")
                                End If
                            ElseIf (txtUniformnumber.Text.Trim.Length = 2) Then
                                If (txtUniformnumber.Text = "00") Then
                                    listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 OR DISPLAY_UNIFORM_NUMBER=000")
                                Else
                                    If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                                        Dim strUniform1 As String = txtUniformnumber.Text.Substring(1, 1)
                                        Dim strUniform2 As String = "00" & txtUniformnumber.Text.Substring(1)
                                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    Else
                                        Dim strUniform2 As String = "0" & txtUniformnumber.Text
                                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    End If
                                End If
                            ElseIf (txtUniformnumber.Text.Trim.Length = 3) Then
                                If (txtUniformnumber.Text.Substring(0, 1) = "0") Then
                                    If (txtUniformnumber.Text.Substring(0, 2) = "00") Then
                                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & txtUniformnumber.Text.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text.Substring(1) & "' ")
                                    Else
                                        listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text.Substring(1) & "' ")
                                    End If

                                Else
                                    listselectDatarow = m_dsAddPlayer.Tables(2).Select("DISPLAY_UNIFORM_NUMBER= '" & txtUniformnumber.Text & "' ")
                                End If
                            End If
                        End If
                        If (listselectDatarow.Length >= 1) Then
                            'essageDialog.Show("Uniform Number already Exists...", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            'MessageDialog.Show(strmessage6 + " " & listselectDatarow(0).Item(3).ToString & ". Please resolve the duplicate uniform number: " & txtUniformnumber.Text & ". ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Dim m_objDuplicateUN As frmDuplicateUniformNos
                            If radHomeTeam.Checked = True Then
                                m_objDuplicateUN = New frmDuplicateUniformNos(m_objGameDetails.HomeTeamID, txtUniformnumber.Text)
                            Else
                                m_objDuplicateUN = New frmDuplicateUniformNos(m_objGameDetails.AwayTeamID, txtUniformnumber.Text)
                            End If
                            Dim i As Integer

                            If m_objGameDetails.IsEditMode And txtUniformnumber.Text.Trim <> m_strEditUniformNumber.Trim Then
                                i = lvwPlayer.SelectedItems(0).Index

                                m_objDuplicateUN.ShowDialog()
                                LoadNewlyAddedPlayers()
                                lvwPlayer.Items(i).Selected = True
                                lvwPlayer.Select()
                                txtUniformnumber.Text = ""
                                txtUniformnumber.Focus()
                            ElseIf m_objGameDetails.IsEditMode = False Then
                                m_objDuplicateUN.ShowDialog()
                                LoadNewlyAddedPlayers()
                                txtUniformnumber.Text = ""
                                txtUniformnumber.Focus()
                            End If
                          
                        End If


                    End If
                Else
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub cmbPosition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPosition.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbPosition.Text, Nothing, 1, 0)
            If m_blnIsComboLoaded = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbPosition.Text, 1, 0)
                m_blnIsComboLoaded = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' Validates Position Combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbPosition_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPosition.Validated
        Try
            If cmbPosition.Text <> "" And cmbPosition.SelectedValue Is Nothing Then
                MessageDialog.Show(strmessage4 + " " & lblPosition.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbPosition.Text = ""
                cmbPosition.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtFirstName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFirstname.Validated
        Try
            'AUDIT TRAIL
            If txtFirstname.Text <> m_strFirstName Then
                If txtFirstname.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtFirstname.Text, lblFirstName.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtFirstname.Text & "' '", lblFirstName.Text, 1, 0)
                End If
            End If
            m_strFirstName = txtFirstname.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub txtLastName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtlastname.Validated
        Try
            'AUDIT TRIAL
            If txtlastname.Text <> m_strLastName Then
                If txtlastname.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtlastname.Text, lblLastName.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtlastname.Text & "' '", lblLastName.Text, 1, 0)
                End If
            End If
            m_strLastName = txtlastname.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region " USER METHODS "

    ''' <summary>
    ''' LOADS POSITION COMBO
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadPositionCombo()
        Try
            m_dsPosition = m_objAddPlayer.LoadPosition()

            Dim dtPosition As New DataTable
            dtPosition = m_dsPosition.Tables(0).Copy

            If m_dsPosition.Tables.Count > 0 Then
                If m_dsPosition.Tables(0).Rows.Count > 0 Then
                    m_blnIsComboLoaded = True
                    LoadControl(cmbPosition, dtPosition, "POSITION_DESC", "POSITION_ID")
                    cmbPosition.SelectedIndex = 0
                    m_blnIsComboLoaded = False
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' LOADS THE DATA INTO COMBO
    ''' </summary>
    ''' <param name="Combo">Name of the Combo</param>
    ''' <param name="Position"></param>
    ''' <param name="DisplayColumnName"></param>
    ''' <param name="DataColumnName"></param>
    ''' <remarks></remarks>
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal Position As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To Position.Rows.Count - 1
                acscRosterData.Add(Position.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = Position.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' FETCHING LOGO FOR SELECTED TEAM
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamLogo(ByVal TeamID As Integer)
        Try
            Dim v_memLogo As MemoryStream
            picTeamLogo.Image = Nothing
            v_memLogo = m_objGeneral.GetTeamLogo(m_objclsGameDetails.LeagueID, TeamID)
            If v_memLogo IsNot Nothing Then
                picTeamLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHING TEAM ID BY PASSING TEAM NAME AS PARAMETER
    ''' </summary>
    ''' <param name="Teamname"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamID(ByVal Teamname As String)
        Try
            If Teamname = m_objclsGameDetails.HomeTeamAbbrev Then
                m_intTeamID = m_objclsGameDetails.HomeTeamID
            Else
                m_intTeamID = m_objclsGameDetails.AwayTeamID
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Function FetchTeamAbbrev(ByVal m_intTeamID As Integer) As String
        Try
            If m_intTeamID = m_objclsGameDetails.HomeTeamID Then
                Return m_objclsGameDetails.HomeTeam
            Else
                Return m_objclsGameDetails.AwayTeam
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' ENABLES OR DISABLES THE BUTTONS
    ''' </summary>
    ''' <param name="blnButtonadd"></param>
    ''' <param name="blnButtonEdit"></param>
    ''' <param name="blnButtonDelete"></param>
    ''' <param name="blnButtonApply"></param>
    ''' <param name="blnButtonIgnore"></param>
    ''' <remarks></remarks>
    Private Sub EnableDisableButton(ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean)
        Try
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
            btnDelete.Enabled = blnButtonDelete
            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    '''  ENABLES OR DISABLES THE TEXTBOXES
    ''' </summary>
    ''' <param name="blnTextBoxMoniker"></param>
    ''' <param name="blnTextBoxLastName"></param>
    ''' <param name="blnTextBoxUniformNumber"></param>
    ''' <param name="blnListViewPlayer"></param>
    ''' <remarks></remarks>

    Private Sub EnableDisableTextBox(ByVal blnTextBoxMoniker As Boolean, ByVal blnTextBoxLastName As Boolean, ByVal blnTextBoxUniformNumber As Boolean, ByVal blnCmbPositon As Boolean, ByVal blnListViewPlayer As Boolean)
        Try
            txtFirstname.Enabled = blnTextBoxMoniker
            txtlastname.Enabled = blnTextBoxLastName
            txtUniformnumber.Enabled = blnTextBoxUniformNumber
            cmbPosition.Enabled = blnCmbPositon
            lvwPlayer.Enabled = blnListViewPlayer
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ''' <summary>
    ''' CLEARS THE TEXTBOXES
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearTextBoxes()
        Try
            txtFirstname.Text = ""
            txtlastname.Text = ""
            txtUniformnumber.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadNewlyAddedPlayers()
        Try

            m_dsAddPlayer = m_objAddPlayer.SelectAddPlayer(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.SortOrder, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")))
            lvwPlayer.Items.Clear()
            If Not m_dsAddPlayer.Tables(0) Is Nothing Then
                For Each drNewPlayer As DataRow In m_dsAddPlayer.Tables(0).Rows
                    Dim lvItem As New ListViewItem
                    If drNewPlayer(4).ToString <> "" Then
                        lvItem = New ListViewItem(drNewPlayer(3).ToString() + ", " + drNewPlayer(4).ToString())
                    Else
                        lvItem = New ListViewItem(drNewPlayer(3).ToString())
                    End If
                    lvItem.SubItems.Add(drNewPlayer(1).ToString)
                    If (drNewPlayer(7).ToString = Nothing) Then
                        lvItem.SubItems.Add(drNewPlayer(6).ToString())
                    Else
                        lvItem.SubItems.Add(drNewPlayer(7).ToString())
                    End If
                    lvItem.SubItems.Add(drNewPlayer(8).ToString)
                    lvItem.SubItems.Add(drNewPlayer(2).ToString)
                    lvwPlayer.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'Enabling and disabling the text box
    Private Sub EnableDisableTextBox(ByVal type As Boolean)
        Try
            txtFirstname.Enabled = type
            txtlastname.Enabled = type
            txtUniformnumber.Enabled = type
            cmbPosition.Enabled = type
            'radHomeTeam.Enabled = type
            'radAwayTeam.Enabled = type
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Displaying Listview selected item in textbox and combo
    Private Sub SelectedListviewDisplay()
        If lvwPlayer.SelectedItems.Count > 0 Then
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, lvwPlayer.SelectedItems(0).Text + " - " + lvwPlayer.SelectedItems(0).SubItems(1).Text + " - " + lvwPlayer.SelectedItems(0).SubItems(2).Text + " - " + lvwPlayer.SelectedItems(0).SubItems(3).Text, 1, 0)


            Try
                Dim listselectDatarow() As DataRow
                For Each lvwitm As ListViewItem In lvwPlayer.SelectedItems
                    m_strNupId = lvwitm.SubItems(4).Text
                Next
                m_intTabind = 0

                radHomeTeam.Enabled = True
                radAwayTeam.Enabled = True

                listselectDatarow = m_dsAddPlayer.Tables(m_intTabind).Select("PLAYER_ID='" & m_strNupId & "'")
                If listselectDatarow.Length > 0 Then
                    'cboTeam.SelectedValue = listselectDatarow(0).Item(0).ToString()
                    If CDbl(listselectDatarow(0).Item(0).ToString()) = m_objclsGameDetails.HomeTeamID Then
                        radHomeTeam.Checked = True
                        radAwayTeam.Enabled = False
                    Else
                        radAwayTeam.Checked = True
                        radHomeTeam.Enabled = False
                    End If
                    txtFirstname.Text = listselectDatarow(0).Item(4).ToString()
                    txtlastname.Text = listselectDatarow(0).Item(3).ToString()
                    cmbPosition.SelectedValue = listselectDatarow(0).Item(5).ToString()
                    If (listselectDatarow(0).Item(7).ToString() = Nothing) Then
                        txtUniformnumber.Text = listselectDatarow(0).Item(6).ToString()
                    Else
                        txtUniformnumber.Text = listselectDatarow(0).Item(7).ToString()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

    'Checking The mandatory Field are Filled or not
    Public Function RequireFieldValidation() As Boolean
        Try

            'If (Not m_objValidation.ValidateEmpty(txtFirstname.Text)) Then
            '    essageDialog.Show("Player First Name is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            '    txtFirstname.Focus()
            '    Return False
            'Else
            If (Not m_objValidation.ValidateEmpty(txtlastname.Text)) Then
                MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtlastname.Focus()
                Return False
            ElseIf (Not m_objValidation.ValidateEmpty(txtUniformnumber.Text)) Then
                MessageDialog.Show(strmessage2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtUniformnumber.Focus()
                Return False
            ElseIf (Not m_objValidation.ValidateEmpty(cmbPosition.Text)) Then
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                cmbPosition.Focus()
                cmbPosition.SelectedIndex = 0
                Return False
            ElseIf radAwayTeam.Checked = False And radHomeTeam.Checked = False Then
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function


#End Region

    Private Sub txtFirstname_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFirstname.Leave
        Try
            If (txtFirstname.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtFirstname.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtUniformnumber_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUniformnumber.Leave
        Try
            If (txtUniformnumber.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtUniformnumber.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtlastname_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtlastname.Leave
        Try
            If (txtlastname.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtlastname.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

   
    Public Sub New(ByVal TeamChecked As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        StrTeamChecked = TeamChecked
        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class