﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmteampossesion
' Author        : wilson
' Created Date  : 29-01-10
' Description   : 
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class frmTeamPossession
#Region " Constants & Variables "
    Private m_objUtility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objTeamStats As clsTeamStats = clsTeamStats.GetInstance()
    Private m_objGameSetup As New clsGameSetup
    Dim periodcount As Int32
    Private m_dsGameSetup As New DataSet
    Private lsupport As New languagesupport
    Dim strmessage As String = "Records Saved"

#End Region
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, "Preferences", 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If (txthmteam.Text = "") Then
                txthmteam.Text = "0"
            End If
            If (txtawteam.Text = "") Then
                txtawteam.Text = "0"
            End If
            If (CInt(txthmteam.Text) > 100) Then
                MessageDialog.Show(lblhome.Text + " Percentage  Exceed 100", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            ElseIf (CInt(txtawteam.Text) > 100) Then
                MessageDialog.Show(lblaway.Text + " Percentage  Exceed 100", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            ElseIf ((CInt(txthmteam.Text) + CInt(txtawteam.Text)) > 100) Then
                MessageDialog.Show(lblhome.Text + " + " + lblaway.Text + " Percentage  Exceed 100", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

            ElseIf ((CInt(txthmteam.Text) + CInt(txtawteam.Text)) < 100) Then
                MessageDialog.Show(lblhome.Text + " + " + lblaway.Text + " Percentage  Less Than 100", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            Else
                periodcount = m_objTeamStats.Getperiodcount(m_objGameDetails.GameCode)
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, Nothing, 1, 0)
                If (periodcount = 1) Then
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-1"), CInt(txthmteam.Text))
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-1"), CInt(txtawteam.Text))
                ElseIf (periodcount = 2) Then
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-2"), CInt(txthmteam.Text))
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-2"), CInt(txtawteam.Text))
                ElseIf (periodcount = 3) Then
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-3"), CInt(txthmteam.Text))
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-3"), CInt(txtawteam.Text))
                ElseIf (periodcount = 4) Then
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID, CInt("-4"), CInt(txthmteam.Text))
                    m_objTeamStats.InsertTeampossion(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, CInt("-4"), CInt(txtawteam.Text))
                End If
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub frmTeamPossession_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPreferences", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            lblhome.Text = m_objGameDetails.HomeTeam
            lblaway.Text = m_objGameDetails.AwayTeam
            Dim dsGameStats As New DataSet

            dsGameStats = m_objTeamStats.GetTeamStastoselect(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            'm_dsGameSetup = m_objGameSetup.SelectGameSetup(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID, m_objGameDetails.AwayTeamID, m_objGameDetails.HomeTeamID)
            ' m_objGameDetails.GameSetup = m_dsGameSetup
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                lblhome.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblhome.Text)
                lblaway.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblaway.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
            End If

            If dsGameStats.Tables(0).Rows.Count > 0 Then
                For z As Integer = 0 To dsGameStats.Tables(0).Rows.Count - 1

                    If (dsGameStats.Tables(0).Rows(z).Item("TEAM_ID").ToString = m_objGameDetails.HomeTeamID.ToString) Then
                        txthmteam.Text = dsGameStats.Tables(0).Rows(z).Item("POSSESSION").ToString
                        txthmteam.Text = txthmteam.Text.PadLeft(3, CChar(" "))
                        Exit For
                    
                    End If
                Next

                For z As Integer = 0 To dsGameStats.Tables(0).Rows.Count - 1

                    If (dsGameStats.Tables(0).Rows(z).Item("TEAM_ID").ToString = m_objGameDetails.AwayTeamID.ToString) Then
                        txtawteam.Text = dsGameStats.Tables(0).Rows(z).Item("POSSESSION").ToString
                        txtawteam.Text = txtawteam.Text.PadLeft(3, CChar(" "))
                        Exit For
                    End If
                Next

                'Else

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub txthmteam_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txthmteam.TextChanged
        Try
            'If (txtawteam.Text <> "") Then
            '    If ((CInt(txthmteam.Text) + CInt(txtawteam.Text)) > 100) Then
            '        MessageBox.Show("Percentage  Exceed 100 ")
            '        txthmteam.Clear()
            '    End If
            'ElseIf (CInt(txthmteam.Text) > 100) Then
            '    MessageBox.Show("Percentage  Exceed 100 ")
            '    txthmteam.Clear()
            'End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtawteam_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtawteam.TextChanged
        Try
            'If (txthmteam.Text <> "") Then
            '    If ((CInt(txthmteam.Text) + CInt(txtawteam.Text)) > 100) Then
            '        MessageBox.Show("Percentage  Exceed 100 ")
            '        txtawteam.Clear()
            '    End If
            'ElseIf (CInt(txtawteam.Text) > 100) Then
            '    MessageBox.Show("Percentage  Exceed 100 ")
            '    txtawteam.Clear()
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txthmteam_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txthmteam.Leave
        ' txthmteam.Text = Format(PBP.Tables(0).Rows(0).Item("TIME_ELAPSED"), "000")
        txthmteam.Text = txthmteam.Text.PadLeft(3, CChar(" "))

    End Sub

    Private Sub txtawteam_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtawteam.Leave
        txtawteam.Text = txtawteam.Text.PadLeft(3, CChar(" "))

    End Sub
End Class