#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text
Imports System.IO

#End Region

#Region "Comments"
#End Region

Public Class frmBoxScore

    Private m_objBoxScore As clsBoxScore = clsBoxScore.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objModule1Main As clsModule1Main = clsModule1Main.GetInstance()
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objPlayerStats As clsPlayerStats = clsPlayerStats.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private m_dsVenueDetails As New DataSet
    Public m_alScoringSummary As New ArrayList()
    Private dsDisplayMasterData As New DataSet()
    Private MessageDialog As New frmMessageDialog
    Dim dsPlayerStats As New DataSet
    Private lsupport As New languagesupport
    Private m_dsScores As New DataSet
    'Private m_objBS As New STATS.SoccerBL.clsBoxScore = SoccerBL.clsBoxScore.clsBoxScore.clsBoxScore.clsBoxScore.
    Dim strheadertext1 As String = "Date:"
    Dim strheadertext2 As String = "Kickoff:"
    Dim strheadertext3 As String = "Venue:"
    Dim strheadertext4 As String = "Attendance:"
    Dim strheadertext5 As String = "Place:"
    Dim strheadertext6 As String = "Scoring Summary"
    Dim strheadertext7 As String = "Score"
    Dim strheadertext8 As String = "Time"
    Dim strheadertext9 As String = "Team"
    Dim strheadertext10 As String = "Goal Scorer"
    Dim strheadertext11 As String = "Goal Type"
    Dim strheadertext12 As String = "Assist"
    Dim strheadertext13 As String = "Bookings and Expulsions"
    Dim strheadertext14 As String = "Player"
    Dim strheadertext15 As String = "Card"
    Dim strheadertext16 As String = "Reason"
    Dim strheadertext17 As String = "Substitutions"
    Dim strheadertext18 As String = "In"
    Dim strheadertext19 As String = "Out"
    Dim strheadertext20 As String = "Lineup"
    Dim strheadertext21 As String = "Bench"
    Dim strheadertext22 As String = "Injuries/Suspensions"
    Dim strheadertext23 As String = "Manager:"
    Dim strheadertext24 As String = "Referees"
    Dim strheadertext25 As String = "Assistant"
    Dim strheadertext26 As String = "4th Official"
    Dim strheadertext27 As String = "Team Information"
    Dim strheadertext28 As String = "Head"
    Dim strheadertext29 As String = "5th Official"
    Dim strheadertext30 As String = "Shots Off Target"
    Dim strheadertext31 As String = "Blocked Shots"

#Region " Constants "
    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

#End Region

    Dim strBuilder As StringBuilder

#Region "Declarations"

#End Region

#Region "Event Handlers"

    Private Sub frmBoxScore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmBoxScore", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            ' BuildReports()
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnPrint.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPrint.Text)
                btnRefresh.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnRefresh.Text)
                strheadertext1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext1)
                strheadertext2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext2)
                strheadertext3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext3)
                strheadertext4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext4)
                strheadertext5 = strheadertext5.Replace(":", "")
                strheadertext5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext5)
                strheadertext5 = strheadertext5 & ":"
                strheadertext6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext6)
                strheadertext7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext7)
                strheadertext8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext8)
                strheadertext9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext9)
                strheadertext10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext10)
                strheadertext11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext11)
                strheadertext12 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext12)
                strheadertext13 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext13)
                strheadertext14 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext14)
                strheadertext15 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext15)
                strheadertext16 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext16)
                strheadertext17 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext17)
                strheadertext18 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext18)
                strheadertext19 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext19)
                strheadertext20 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext20)
                strheadertext21 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext21)
                strheadertext22 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext22)
                strheadertext23 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext23)
                strheadertext24 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext24)
                strheadertext25 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext25)
                strheadertext26 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext26)

            End If

            CustomReports()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)

        End Try

    End Sub

#End Region

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            ' m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.ToString, 1, 0)

            ' BuildReports()
            CustomReports()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function FetchPlayerNames(ByVal PlayerID As Integer) As String
        Try
            Dim StrPlayer As String = ""
            Dim StrPlayerUniform As String = ""
            'If m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2 Then
            If m_objGameDetails.TeamRosters.Tables.Count > 0 Then
                Dim DsPlayers As DataSet = m_objGameDetails.TeamRosters.Copy()
                Dim drs() As DataRow = Nothing

                drs = DsPlayers.Tables(0).Select("PLAYER_ID = " & PlayerID & "")
                If drs.Length > 0 Then

                    If IsDBNull(drs(0).Item("DISPLAY_UNIFORM_NUMBER")) Then
                        StrPlayerUniform = "--"
                    Else
                        StrPlayerUniform = CStr(drs(0).Item("DISPLAY_UNIFORM_NUMBER")).ToString()
                    End If

                    StrPlayer = StrPlayerUniform + "`" + CStr(drs(0).Item("PLAYER"))

                Else
                    drs = DsPlayers.Tables(1).Select("PLAYER_ID = " & PlayerID & "")
                    If drs.Length > 0 Then
                        If IsDBNull(drs(0).Item("DISPLAY_UNIFORM_NUMBER")) Then
                            StrPlayerUniform = "--"
                        Else
                            StrPlayerUniform = CStr(drs(0).Item("DISPLAY_UNIFORM_NUMBER")).ToString()
                            If StrPlayerUniform.Length = 1 Then
                                StrPlayerUniform = "" + StrPlayerUniform
                            End If
                        End If
                        StrPlayer = StrPlayerUniform + "`" + CStr(drs(0).Item("PLAYER"))
                    End If
                End If
                Return StrPlayer
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetCustomDate(ByVal strDate As String) As [String]
        Try
            If strDate = Nothing Or strDate = String.Empty Then
                Return Nothing
            End If
            If IsDate(strDate) Then
                Dim dt As DateTime = Convert.ToDateTime(strDate)
                Dim strMonth As String = dt.[Date].ToString("MMMM")

                Return (strMonth & " " & dt.Date.Day & ", ") & dt.Date.Year
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Sub CustomReports()
        Try
            Dim strHalfTimeScores As String = Nothing
            Dim DsMainEvents As DataSet
            Dim strGameDate As String = String.Empty
            Dim strDate As String = String.Empty
            Dim strKickOff As String = String.Empty
            Dim strVenue As String = String.Empty
            Dim strAttendence As String = String.Empty
            Dim strTown As String = String.Empty
            Dim strCountry As String = String.Empty
            Dim strMgrStrings As String() = Nothing
            Dim strHomeFormation As String = String.Empty
            Dim strAwayFormation As String = String.Empty
            Dim arrHomePlayerStrings As String() = Nothing
            Dim arrAwayPlayerStrings As String() = Nothing
            Dim drHomeInj(), drAwayInj() As DataRow
            Dim drHomeSus(), drAwaySus() As DataRow
            Dim strScore As String = Nothing
            Dim strGoalScorer As String = Nothing
            Dim arrGoalScorer As String() = Nothing
            Dim homeScorer As String = Nothing
            Dim awayScorer As String = Nothing
            Dim strAssist As String = Nothing
            Dim arrAssist As String() = Nothing
            Dim homeAssist As String = Nothing
            Dim awayAssist As String = Nothing
            Dim strPeriod As String = Nothing
            Dim strTime As String = Nothing
            Dim intMin As Integer
            Dim srtGoalType As String = Nothing
            Dim strPlayer As String = Nothing
            Dim arrPlayer As String() = Nothing
            Dim strCardType As String = Nothing
            Dim strReason As String = Nothing
            Dim strHomePlayer As String = Nothing
            Dim strAwayPlayer As String = Nothing
            Dim strReasn As String = String.Empty
            Dim strPlayerIn As String = Nothing
            Dim strPlayerOut As String = Nothing
            Dim arrPlayerIn As String() = Nothing
            Dim strHomePlayerIn As String = Nothing
            Dim strHomePlayerOut As String = Nothing
            Dim strAwayPlayerIn As String = Nothing
            Dim strAwayPlayerOut As String = Nothing
            Dim arrPlayerOut As String() = Nothing
            Dim strReferee As String = String.Empty
            Dim strAsstReferee As String = String.Empty
            Dim strAsstReferee2 As String = String.Empty
            Dim str4thOfficial As String = String.Empty
            Dim str5thOfficial As String = String.Empty
            Dim strGameStatus As String = String.Empty
            Dim strHomeMgr As String = String.Empty
            Dim strAwayMgr As String = String.Empty
            Dim strShotsOffTarget As String = Nothing
            Dim strBlockedShots As String = Nothing

            Dim st1 As String = Application.StartupPath() + "\" + "Resources\" + "goal.gif"
            Dim ast1 As String = "" & " "" " + st1 + """ "
            Dim asyellocard As String = Application.StartupPath() + "\" + "Resources\" + "yellow.gif"
            Dim asyellocard1 As String = "" & " "" " + asyellocard + """ "
            Dim asredcard As String = Application.StartupPath() + "\" + "Resources\" + "red.gif"
            Dim asredcard1 As String = "" & " "" " + asredcard + """ "
            Dim ashplayerout As String = Application.StartupPath() + "\" + "Resources\" + "outHome.gif"
            Dim ashplayerout1 As String = "" & " "" " + ashplayerout + """ "
            Dim ashplayerin As String = Application.StartupPath() + "\" + "Resources\" + "inHome.gif"
            Dim ashplayerin1 As String = "" & " "" " + ashplayerin + """ "
            Dim asaplayerout As String = Application.StartupPath() + "\" + "Resources\" + "outAway.gif"
            Dim asaplayerout1 As String = "" & " "" " + asaplayerout + """ "
            Dim asaplayerin As String = Application.StartupPath() + "\" + "Resources\" + "inAway.gif"
            Dim asaplayerin1 As String = "" & " "" " + asaplayerin + """ "

            Dim s As String = ""

            m_dsVenueDetails = m_objBoxScore.GetVenueDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID, m_objGameDetails.languageid)

            If m_dsVenueDetails.Tables(0).Rows.Count > 0 Then
                Dim Dt As DateTime
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                    If m_dsVenueDetails.Tables(0).Rows(0).Item("Kickoff").ToString <> "" Then
                        Dt = CDate(m_dsVenueDetails.Tables(0).Rows(0).Item("Kickoff").ToString)
                    Else
                        Dt = CDate(m_objGameDetails.GameDate)
                    End If
                Else
                    Dt = CDate(m_objGameDetails.GameDate)
                End If

                If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                    strDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                Else
                    strDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                End If

                Dim strDates() As String = strDate.Split(CChar(" "))
                strGameDate = strDates(0)
                strKickOff = strDates(1) & " " & strDates(2) & " " & strDates(3)

                If IsDBNull(m_dsVenueDetails.Tables(0).Rows(0)("FIELD_NAME")) Then
                    strVenue = String.Empty
                Else
                    strVenue = m_dsVenueDetails.Tables(0).Rows(0)("FIELD_NAME").ToString
                End If

                If IsDBNull(m_dsVenueDetails.Tables(0).Rows(0)("ATTENDANCE")) Then
                    strAttendence = String.Empty
                Else
                    strAttendence = CInt(m_dsVenueDetails.Tables(0).Rows(0)("ATTENDANCE")).ToString("N")
                    strAttendence = strAttendence.Substring(0, Len(strAttendence) - 3)
                End If

                If IsDBNull(m_dsVenueDetails.Tables(0).Rows(0)("CITY")) Then
                    strTown = String.Empty
                Else
                    strTown = (m_dsVenueDetails.Tables(0).Rows(0)("CITY").ToString)
                End If

                If IsDBNull(m_dsVenueDetails.Tables(0).Rows(0)("COUNTRY")) Then
                    strCountry = String.Empty
                Else
                    strCountry = " (" & m_dsVenueDetails.Tables(0).Rows(0)("COUNTRY").ToString & ")"
                End If
            End If
            DsMainEvents = m_objBoxScore.GetNewBoxScoreReport(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid, m_objGameDetails.CoverageLevel)
            strHalfTimeScores = GetHalfTimeScores(DsMainEvents)
            strHalfTimeScores = frmMain.lblScoreHome.Text & "-" & frmMain.lblScoreAway.Text

            If DsMainEvents.Tables(10).Rows.Count > 0 Then
                strGameStatus = DsMainEvents.Tables(10).Rows(0)("GAME_STATUS").ToString
            Else
                strGameStatus = "&nbsp;"
            End If

            If Not (m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2) Then
                Dim dsMgr As DataSet = DsMainEvents.Tables(7).DataSet.Copy()

                If dsMgr.Tables.Count > 0 Then
                    If dsMgr.Tables(7).Rows.Count > 0 Then
                        strMgrStrings = GetMgrName(m_objGameDetails.HomeTeamID, dsMgr).Split(CChar(";"))
                        strHomeMgr = strMgrStrings.GetValue(0).ToString.Replace("-", " ")
                        strAwayMgr = strMgrStrings.GetValue(1).ToString.Replace("-", " ")
                    End If
                End If
            End If

            If DsMainEvents.Tables(3).Rows.Count > 0 Then
                strHomeFormation = CStr(DsMainEvents.Tables(3).Rows(0).Item(1))
                If DsMainEvents.Tables(3).Rows.Count > 1 Then
                    strAwayFormation = CStr(DsMainEvents.Tables(3).Rows(1).Item(1))
                End If
            End If
            If (m_objGameDetails.CoverageLevel = 6) Then
                GetCurrentPlayersAfterRestart()
            End If

            Dim DsPlayersLineUp As DataSet = m_objTeamSetup.RosterInfo.Copy()
            Dim arrHomePlayerList As ArrayList = GetTeamLineUPByGoalKeeper(DsPlayersLineUp, "HOME")
            Dim arrAwayPlayerList As ArrayList = GetTeamLineUPByGoalKeeper(DsPlayersLineUp, "AWAY")

            drHomeInj = DsMainEvents.Tables(8).Select("INJURY_TEAM_ID =" & m_objGameDetails.HomeTeamID)
            drAwayInj = DsMainEvents.Tables(8).Select("INJURY_TEAM_ID =" & m_objGameDetails.AwayTeamID)

            drHomeSus = DsMainEvents.Tables(9).Select("TEAM_ID=" & m_objGameDetails.HomeTeamID)
            drAwaySus = DsMainEvents.Tables(9).Select("TEAM_ID=" & m_objGameDetails.AwayTeamID)

            Dim dtt As New DataTable
            dtt = DsMainEvents.Tables(8).Clone()

            Dim dtt2 As New DataTable
            dtt2 = DsMainEvents.Tables(9).Clone()
            For i As Integer = 0 To drHomeSus.Length - 1
                dtt.Rows.Add(drHomeSus(i).Item("Player"), drHomeSus(i).Item("UNIFORM"), drHomeSus(i).Item("DISPLAY_UNIFORM_NUMBER"), drHomeSus(i).Item("TEAM_ID"), drHomeSus(i).Item("Comments"))
            Next
            For i As Integer = 0 To drAwaySus.Length - 1
                dtt2.Rows.Add(drAwaySus(i).Item("Player"), drAwaySus(i).Item("UNIFORM"), drAwaySus(i).Item("DISPLAY_UNIFORM_NUMBER"), drAwaySus(i).Item("TEAM_ID"), drAwaySus(i).Item("Comments"))
            Next

            For i As Integer = 0 To drHomeInj.Length - 1
                dtt.Rows.Add(drHomeInj(i).Item("Player"), drHomeInj(i).Item("UNIFORM"), drHomeInj(i).Item("DISPLAY_UNIFORM_NUMBER"), drHomeInj(i).Item("INJURY_TEAM_ID"), drHomeInj(i).Item("Comments"))
            Next
            For i As Integer = 0 To drAwayInj.Length - 1
                dtt2.Rows.Add(drAwayInj(i).Item("Player"), drAwayInj(i).Item("UNIFORM"), drAwayInj(i).Item("DISPLAY_UNIFORM_NUMBER"), drAwayInj(i).Item("INJURY_TEAM_ID"), drAwayInj(i).Item("Comments"))
            Next

            strBuilder = New StringBuilder
            strBuilder.Append("<html>")
            strBuilder.Append("<head><title>Boxscore</title>")
            'style
            strBuilder.Append("<style type='text/css'>")
            strBuilder.Append("body{font-family: @Arial Unicode MS;}")
            strBuilder.Append("table.main{font-family: @Arial Unicode MS;}")
            strBuilder.Append("td.underline{border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #C0C0C0;}")
            strBuilder.Append("tr.tblhdrgrey{font-size: 13;color: black;background-color: #e9e9e9;padding: 0px;}")
            strBuilder.Append(".black12{font-size: 12;color: black;}")
            strBuilder.Append("table.mystyle{border-width: 0 0 1px 1px;border-spacing: 0;border-collapse: collapse;border-style: solid;border-color: #e9e9e9;}")
            strBuilder.Append(".mystyle td, .mystyle th{margin: 0;padding: 4px;border-width: 1px 1px 0 0;border-style: solid;border-color: #e9e9e9;}")
            strBuilder.Append("td.tblhdrwhite{font-size: 13;font-weight: bold;color: gray;border: 1px solid #C0C0C0;text-align: center;padding: 0px;}")
            strBuilder.Append("</style>")
            strBuilder.Append("</head>")
            'body
            strBuilder.Append("<body>")
            strBuilder.Append("<table id='Main' class='main' width='100%' cellpadding='0' cellspacing='0'>")
            strBuilder.Append("<tr style='font-size: 20'><td id='Team_Home' width='20%' align='left'>" & m_objGameDetails.HomeTeam & "</td><td id='Score' width='60%' align='center'>" & strHalfTimeScores & "</td><td id='Team_Away' width='20%' align='right'>" & m_objGameDetails.AwayTeam & "</td></tr>")

            If Not m_dsScores Is Nothing Then
                If m_dsScores.Tables(0).Rows.Count > 0 Then
                    If CDbl(m_dsScores.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                        If m_dsScores.Tables(1).Rows.Count > 0 Then
                            Dim drHome() As DataRow = m_dsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID)
                            Dim drAway() As DataRow = m_dsScores.Tables(1).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID)
                            m_objGameDetails.PenaltyShootOutHomeScore = drHome.Length
                            m_objGameDetails.PenaltyShootOutAwayScore = drAway.Length
                        End If
                        strBuilder.Append("<tr style='font-size: 15'><td width='20%'>&nbsp;</td><td id='Penalty Shootout Score' width='60%' align='center'>" & "Penalty shootout score:" & m_objGameDetails.PenaltyShootOutHomeScore & " - " & m_objGameDetails.PenaltyShootOutAwayScore & "</td><td>&nbsp;</td></tr>")
                    End If
                End If
            End If

            strBuilder.Append("<tr id='GameStatus' style='font-size: 15'><td align='left' class='underline'>&nbsp;</td><td align='center' class='underline'>" & strGameStatus & "</td><td align='right' class='underline'>&nbsp;</td></tr>")
            strBuilder.Append("<tr><td align='center'><img src='" & Application.StartupPath() + "\" + "Resources\HomeLogo.bmp'" & " alt='' /></td><td align='center'><table id='GameInfo' width='100%' style='font-size: 13'>")
            strBuilder.Append("<tr><td align='center' width='60%'><b>" & strheadertext1 & "&nbsp;" & strGameDate & "</b></td></tr>")
            strBuilder.Append("<tr><td align='center'><b>" & strheadertext2 & "&nbsp;" & strKickOff & "</b></td></tr>")
            strBuilder.Append("<tr><td align='center'><b>" & strheadertext3 & "&nbsp;" & strVenue & "</b></td></tr>")
            strBuilder.Append("<tr><td align='center'><b>" & strheadertext4 & "&nbsp;" & strAttendence & "</b></td></tr>")
            strBuilder.Append("<tr><td align='center'><b>" & strheadertext5 & "&nbsp;" & strTown & strCountry & "</b></td></tr>")
            strBuilder.Append("</table></td><td align='center'><img src='" & Application.StartupPath() + "\" + "Resources\AwayLogo.bmp'" & " alt='' /></td></tr>")
            'home section ----------------------------------------------------------------------------
            strBuilder.Append("<tr><td valign='top' id='Home'>")
            'home team info
            strBuilder.Append("<table width='100%' id='TeamInfo_Home'><tr><td class='tblhdrwhite'>" & strheadertext27 & "</td></tr><tr class='black12'><td style='height: 35px; vertical-align: middle;'><b>" & strheadertext23 & "</b>&nbsp;" & strHomeMgr & "</td></tr><tr class='black12'><td><b>Formation:</b>&nbsp;" & strHomeFormation & "</td></tr></table>")
            'home team lineup
            strBuilder.Append("<table width='100%' id='Lineup_Home'><tr><td class='tblhdrwhite' colspan='2'>" & strheadertext20 & "</td></tr>")
            If Not arrHomePlayerList Is Nothing Then
                If arrHomePlayerList.Count > 0 Then
                    For i As Integer = 0 To arrHomePlayerList.Count - 1
                        arrHomePlayerStrings = arrHomePlayerList(i).ToString.Split(CChar("`"))

                        For k As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                            If DsMainEvents.Tables(2).Rows(k)("PLAYER_IN").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Or DsMainEvents.Tables(2).Rows(k)("OUT").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Then
                                strPeriod = DsMainEvents.Tables(2).Rows(k)("PERIOD").ToString
                                strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(2).Rows(k)("TIME").ToString), CInt(strPeriod))
                                intMin = getMinutes(strTime)
                            End If
                        Next

                        If (arrHomePlayerStrings(1).ToString() <> "-") Then
                            s = arrHomePlayerStrings(1).Substring(0, arrHomePlayerStrings(1).IndexOf("~")).ToString
                            strBuilder.Append("<tr class='black12'><td width='10%'>" & arrHomePlayerStrings(0).ToString & "</td><td width='90%'>" & s)
                            For j As Integer = 0 To DsMainEvents.Tables(0).Rows.Count - 1
                                If (DsMainEvents.Tables(0).Rows(j).Item("goalscorer").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append(" &nbsp; <img src=" & ast1 & ">")
                                End If
                            Next
                            For j As Integer = 0 To DsMainEvents.Tables(1).Rows.Count - 1
                                If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "1") Then
                                    strBuilder.Append(" &nbsp; <img src=" & asyellocard1 & ">")
                                End If
                                If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And (DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "2" Or DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "3")) Then
                                    strBuilder.Append(" &nbsp; <img src=" & asredcard1 & ">")
                                End If
                            Next
                            For j As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                                If (DsMainEvents.Tables(2).Rows(j).Item("player_in").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append(" &nbsp; <img src=" & ashplayerin1 & "> (" & CStr(intMin) & ")")
                                End If
                                If (DsMainEvents.Tables(2).Rows(j).Item("out").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append(" &nbsp; <img src=" & ashplayerout1 & "> (" & CStr(intMin) & ")")
                                End If
                            Next
                            strBuilder.Append("</td></tr>")
                        End If
                    Next
                End If
            End If
            strBuilder.Append("</table>") 'end of home team lineup
            'home team bench
            arrHomePlayerStrings = Nothing
            arrHomePlayerList = GetSubsituationsByGoalKeeper(DsPlayersLineUp, DsMainEvents, "HOME")
            strBuilder.Append("<table width='100%' id='Bench_Home'><tr><td class='tblhdrwhite' colspan='2'>" & strheadertext21 & "</td></tr>")
            If Not arrHomePlayerList Is Nothing Then
                If arrHomePlayerList.Count > 0 Then
                    For i As Integer = 0 To arrHomePlayerList.Count - 1
                        arrHomePlayerStrings = arrHomePlayerList(i).ToString.Split(CChar("`"))

                        For k As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                            If DsMainEvents.Tables(2).Rows(k)("PLAYER_IN").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Or DsMainEvents.Tables(2).Rows(k)("OUT").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Then
                                strPeriod = DsMainEvents.Tables(2).Rows(k)("PERIOD").ToString
                                strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(2).Rows(k)("TIME").ToString), CInt(strPeriod))
                                intMin = getMinutes(strTime)
                            End If
                        Next

                        s = arrHomePlayerStrings(1).Substring(0, arrHomePlayerStrings(1).IndexOf("~")).ToString
                        strBuilder.Append("<tr class='black12'><td width='10%'>" & arrHomePlayerStrings(0).ToString & "</td><td width='90%'>" & s)
                        For j As Integer = 0 To DsMainEvents.Tables(0).Rows.Count - 1
                            If (DsMainEvents.Tables(0).Rows(j).Item("goalscorer").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                strBuilder.Append(" &nbsp; <img src=" & ast1 & ">")
                            End If
                        Next
                        For j As Integer = 0 To DsMainEvents.Tables(1).Rows.Count - 1
                            If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "1") Then
                                strBuilder.Append(" &nbsp; <img src=" & asyellocard1 & ">")
                            End If
                            If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And (DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "2" Or DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "3")) Then
                                strBuilder.Append(" &nbsp; <img src=" & asredcard1 & ">")
                            End If
                        Next
                        For j As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                            If (DsMainEvents.Tables(2).Rows(j).Item("player_in").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                strBuilder.Append(" &nbsp; <img src=" & ashplayerin1 & "> (" & CStr(intMin) & ")")
                            End If
                            If (DsMainEvents.Tables(2).Rows(j).Item("out").ToString = arrHomePlayerStrings(1).Substring(arrHomePlayerStrings(1).IndexOf("~"), arrHomePlayerStrings(1).Length - arrHomePlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                strBuilder.Append(" &nbsp; <img src=" & ashplayerout1 & "> (" & CStr(intMin) & ")")
                            End If
                        Next
                        strBuilder.Append("</td></tr>")
                    Next
                End If
            End If
            strBuilder.Append("</table>") 'end of home team bench
            'home team injuries and suspensions
            strBuilder.Append("<table width='100%' id='Injuries_Home'><tr><td class='tblhdrwhite' colspan='2'>" & strheadertext22 & "</td></tr>")

            For j As Integer = 0 To dtt.Rows.Count - 1
                strBuilder.Append("<tr class='black12'><td width='10%'>" & dtt.Rows(j).Item("UNIFORM").ToString & "</td><td width='90%'>" & dtt.Rows(j).Item("PLAYER").ToString & "</td></tr>")
            Next
            strBuilder.Append("</table>") 'end of home team injuries and suspensions
            strBuilder.Append("</td>") 'end of home section ------------------------------------------

            'middle section --------------------------------------------------------------------------
            strBuilder.Append("<td valign='top' id='Center'>")
            'scoring summary
            strBuilder.Append("<table id='ScoringSummary' width='100%' class='mystyle'><tr><td class='tblhdrwhite' colspan='6'>" & strheadertext6 & "</td></tr>")
            strBuilder.Append("<tr class='tblhdrgrey'><td align='left' width='20%'><b>" & strheadertext10 & "</b></td><td align='left' width='20%'><b>" & strheadertext12 & "</b></td><td align='center' width='5%'><b>" & strheadertext7 & "</b></td><td align='center' width='15%'><b>" & strheadertext11 & "</b></td><td align='right' width='20%'><b>" & strheadertext10 & "</b></td><td align='right' width='20%'><b>" & strheadertext12 & "</b></td></tr>")

            If DsMainEvents.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To DsMainEvents.Tables(0).Rows.Count - 1
                    If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                        strScore = DsMainEvents.Tables(0).Rows(i)("OFFENSE_SCORE").ToString & "-" & DsMainEvents.Tables(0).Rows(i)("DEFENSE_SCORE").ToString
                    Else
                        strScore = DsMainEvents.Tables(0).Rows(i)("DEFENSE_SCORE").ToString & "-" & DsMainEvents.Tables(0).Rows(i)("OFFENSE_SCORE").ToString
                    End If

                    strPeriod = DsMainEvents.Tables(0).Rows(i)("Period").ToString
                    strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(0).Rows(i)("Time_Elapsed").ToString), CInt(strPeriod))
                    intMin = getMinutes(strTime)

                    If (DsMainEvents.Tables(0).Rows(i)("GoalScorer").ToString().Equals("0")) Then
                        strGoalScorer = "`"
                    Else
                        strGoalScorer = FetchPlayerNames(CInt(DsMainEvents.Tables(0).Rows(i)("GoalScorer").ToString))
                        If strGoalScorer <> "" Then
                            arrGoalScorer = strGoalScorer.Split(CChar(""))
                        Else
                            Dim DsPlayersLineUpforSubs As DataSet = m_objTeamSetup.RosterInfo.Copy()
                            Dim drs() As DataRow = Nothing
                            If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = CInt(DsPlayersLineUpforSubs.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                drs = DsPlayersLineUpforSubs.Tables(0).Select("PLAYER_ID = " & CInt(DsMainEvents.Tables(0).Rows(i)("GoalScorer").ToString) & "")
                            Else
                                drs = DsPlayersLineUpforSubs.Tables(1).Select("PLAYER_ID = " & CInt(DsMainEvents.Tables(0).Rows(i)("GoalScorer").ToString) & "")
                            End If
                            strGoalScorer = CStr(drs(0).Item("PLAYER"))
                        End If
                        If strGoalScorer <> "" Then
                            arrGoalScorer = strGoalScorer.Split(CChar("`"))
                            strGoalScorer = arrGoalScorer(1)
                            arrGoalScorer = strGoalScorer.Split(CChar("("))
                        End If
                        If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                            homeScorer = arrGoalScorer(0).Trim
                            awayScorer = "&nbsp;"
                            srtGoalType = "(" & CStr(intMin) & ") " & DsMainEvents.Tables(0).Rows(i)("EVENT_CODE_DESC").ToString
                        Else
                            awayScorer = arrGoalScorer(0).Trim
                            homeScorer = "&nbsp;"
                            srtGoalType = DsMainEvents.Tables(0).Rows(i)("EVENT_CODE_DESC").ToString & " (" & CStr(intMin) & ")"
                        End If
                    End If

                    If (DsMainEvents.Tables(0).Rows(i)("Assist").ToString.Equals("0")) Then
                        strAssist = "`"
                        awayAssist = "&nbsp;"
                        homeAssist = "&nbsp;"
                    Else
                        strAssist = FetchPlayerNames(CInt(DsMainEvents.Tables(0).Rows(i)("Assist").ToString))
                        If strAssist <> "" Then
                            arrAssist = strAssist.Split(CChar(""))
                        Else
                            Dim DsPlayersLineUpforSubs As DataSet = m_objTeamSetup.RosterInfo.Copy()
                            Dim drs() As DataRow = Nothing
                            If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = CInt(DsPlayersLineUpforSubs.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                drs = DsPlayersLineUpforSubs.Tables(0).Select("PLAYER_ID = " & CInt(DsMainEvents.Tables(0).Rows(i)("Assist").ToString) & "")
                            Else
                                drs = DsPlayersLineUpforSubs.Tables(1).Select("PLAYER_ID = " & CInt(DsMainEvents.Tables(0).Rows(i)("Assist").ToString) & "")
                            End If

                            If drs.Length > 0 Then
                                strAssist = CStr(drs(0).Item("PLAYER"))
                            End If
                        End If
                        If strAssist <> "" Then
                            arrAssist = strAssist.Split(CChar("`"))
                            strAssist = arrAssist(1)
                            arrAssist = strAssist.Split(CChar("("))
                        End If
                        If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                            homeAssist = arrAssist(0).Trim
                            awayAssist = "&nbsp;"
                        Else
                            awayAssist = arrAssist(0).Trim
                            homeAssist = "&nbsp;"
                        End If
                    End If

                    If (DsMainEvents.Tables(0).Rows(i)("Assist2").ToString.Equals("0")) Then
                        strAssist = "`"
                    Else
                        strAssist = FetchPlayerNames(CInt(DsMainEvents.Tables(0).Rows(i)("Assist2").ToString))
                        If strAssist <> "" Then
                            arrAssist = strAssist.Split(CChar(""))
                        Else
                            Dim DsPlayersLineUpforSubs As DataSet = m_objTeamSetup.RosterInfo.Copy()
                            Dim drs() As DataRow = Nothing
                            If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = CInt(DsPlayersLineUpforSubs.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                drs = DsPlayersLineUpforSubs.Tables(0).Select("PLAYER_ID = " & CInt(DsMainEvents.Tables(0).Rows(i)("Assist2").ToString) & "")
                            Else
                                drs = DsPlayersLineUpforSubs.Tables(1).Select("PLAYER_ID = " & CInt(DsMainEvents.Tables(0).Rows(i)("Assist2").ToString) & "")
                            End If

                            If drs.Length > 0 Then
                                strAssist = CStr(drs(0).Item("PLAYER"))
                            End If
                        End If
                        If strAssist <> "" Then
                            arrAssist = strAssist.Split(CChar("`"))
                            strAssist = arrAssist(1)
                            arrAssist = strAssist.Split(CChar("("))
                        End If
                        If CInt(DsMainEvents.Tables(0).Rows(i)("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                            homeAssist = homeAssist & "; " & arrAssist(0).Trim
                            awayAssist = "&nbsp;"
                        Else
                            awayAssist = awayAssist & "; " & arrAssist(0).Trim
                            homeAssist = "&nbsp;"
                        End If
                    End If

                    strBuilder.Append("<tr class='black12'><td align='left'>" & homeScorer & "</td><td align='left'>" & homeAssist & "</td><td align='center'>" & strScore & "</td><td align='center'>" & srtGoalType & "</td><td align='right'>" & awayScorer & "</td><td align='right'>" & awayAssist & "</td></tr>")
                Next
            End If
            strBuilder.Append("</table>") 'end of scoring summary

            'bookings and expulsions
            strBuilder.Append("<br /><table id='BookingsAndExpulsions' width='100%' class='mystyle'><tr><td class='tblhdrwhite' colspan='4'>" & strheadertext13 & "</td></tr><tr class='tblhdrgrey'><td align='left' width='25%'><b>" & strheadertext14 & "</b></td><td align='center' width='25%'><b>" & strheadertext15 & "</b></td><td align='center' width='25%'><b>" & strheadertext16 & "</b></td><td align='right' width='25%'><b>" & strheadertext14 & "</b></td></tr>")
            If Not IsDBNull(DsMainEvents.Tables(1)) And DsMainEvents.Tables(1).Rows.Count > 0 Then
                For i As Integer = 0 To DsMainEvents.Tables(1).Rows.Count - 1
                    strPeriod = DsMainEvents.Tables(1).Rows(i)("PERIOD").ToString
                    strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(1).Rows(i)("TIME").ToString), CInt(strPeriod))
                    intMin = getMinutes(strTime)
                    strPlayer = FetchPlayerNames(CInt(DsMainEvents.Tables(1).Rows(i)("Player_ID").ToString))
                    If strPlayer <> "" Then
                        arrPlayer = strPlayer.Split(CChar("`"))
                        strPlayer = arrPlayer(1)
                        arrPlayer = strPlayer.Split(CChar("("))
                    End If

                    strReason = DsMainEvents.Tables(1).Rows(i)("REASON1").ToString

                    If CInt(DsMainEvents.Tables(1).Rows(i)("TEAMID")) = m_objGameDetails.HomeTeamID Then
                        strCardType = "(" & CStr(intMin) & ") " & DsMainEvents.Tables(1).Rows(i)("CARDTYPE").ToString
                        strHomePlayer = arrPlayer(0).Trim
                        strAwayPlayer = "&nbsp;"
                    Else
                        strCardType = DsMainEvents.Tables(1).Rows(i)("CARDTYPE").ToString & " (" & CStr(intMin) & ")"
                        strAwayPlayer = arrPlayer(0).Trim
                        strHomePlayer = "&nbsp;"
                    End If

                    strBuilder.Append("<tr class='black12'><td align='left'>" & strHomePlayer & "</td><td align='center'>" & strCardType & "</td><td align='center'>" & strReason & "</td><td align='right'>" & strAwayPlayer & "</td></tr>")
                Next
            End If
            strBuilder.Append("</table>") 'end of bookings and expulsions


            If Not (m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2) Then
                'substitutions
                strBuilder.Append("<br /><table id='Substitutions' width='100%' class='mystyle'><tr><td class='tblhdrwhite' colspan='5'>" & strheadertext17 & "</td></tr><tr class='tblhdrgrey'><td align='left' width='20%'><b>" & strheadertext18 & "</b></td><td align='left' width='20%'><b>" & strheadertext19 & "</b></td><td align='center' width='20%'><b>" & strheadertext16 & "</b></td><td align='right' width='20%'><b>" & strheadertext18 & "</b></td><td align='right' width='20%'><b>" & strheadertext19 & "</b></td></tr>")

                If Not IsDBNull(DsMainEvents.Tables(2)) And DsMainEvents.Tables(2).Rows.Count > 0 Then
                    For i As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1

                        strPeriod = DsMainEvents.Tables(2).Rows(i)("PERIOD").ToString
                        strReasn = DsMainEvents.Tables(2).Rows(i)("SUB_REASON").ToString
                        strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(2).Rows(i)("TIME").ToString), CInt(strPeriod))
                        intMin = getMinutes(strTime)
                        arrPlayerIn = Nothing
                        arrPlayerOut = Nothing
                        strHomePlayerIn = ""
                        strHomePlayerOut = ""
                        strAwayPlayerIn = ""
                        strAwayPlayerOut = ""
                        If CInt(strPeriod) = 2 And CInt(DsMainEvents.Tables(2).Rows(i)("TIME").ToString) = 0 Then
                            intMin = intMin + 1
                        ElseIf CInt(strPeriod) = 3 And CInt(DsMainEvents.Tables(2).Rows(i)("TIME").ToString) = 0 Then
                            intMin = intMin + 1
                        End If
                        Dim strdummy As String = DsMainEvents.Tables(2).Rows(i)("PLAYER_IN").ToString
                        strPlayerIn = FetchPlayerNames(CInt(IIf(IsDBNull(DsMainEvents.Tables(2).Rows(i)("PLAYER_IN")), 0, DsMainEvents.Tables(2).Rows(i)("PLAYER_IN").ToString)))
                        strPlayerOut = FetchPlayerNames(CInt(IIf(IsDBNull(DsMainEvents.Tables(2).Rows(i)("OUT")), 0, DsMainEvents.Tables(2).Rows(i)("OUT").ToString)))

                        If strPlayerIn <> "" Then
                            arrPlayerIn = strPlayerIn.Split(CChar("`"))
                            strPlayerIn = arrPlayerIn(1)
                            arrPlayerIn = strPlayerIn.Split(CChar("("))
                        Else
                            Dim DsPlayersLineUpforSubs As DataSet = m_objTeamSetup.RosterInfo.Copy()
                            Dim drs() As DataRow = Nothing
                            If CInt(DsMainEvents.Tables(2).Rows(i)("TEAM")) = CInt(DsPlayersLineUpforSubs.Tables(0).Rows(0).Item("TEAM_ID")) Then
                                drs = DsPlayersLineUpforSubs.Tables(0).Select("PLAYER_ID = " & CInt(IIf(IsDBNull(DsMainEvents.Tables(2).Rows(i)("PLAYER_IN")), 0, DsMainEvents.Tables(2).Rows(i)("PLAYER_IN").ToString)) & "")
                            Else
                                drs = DsPlayersLineUpforSubs.Tables(1).Select("PLAYER_ID = " & CInt(IIf(IsDBNull(DsMainEvents.Tables(2).Rows(i)("PLAYER_IN")), 0, DsMainEvents.Tables(2).Rows(i)("PLAYER_IN").ToString)) & "")
                            End If

                            If drs.Length > 0 Then
                                strPlayerIn = CStr(drs(0).Item("PLAYER"))
                            End If
                            If strPlayerIn <> "" Then
                                arrPlayerIn = strPlayerIn.Split(CChar("`"))
                                strPlayerIn = arrPlayerIn(1)
                                arrPlayerIn = strPlayerIn.Split(CChar("("))
                            End If
                        End If

                        If strPlayerOut <> "" Then
                            arrPlayerOut = strPlayerOut.Split(CChar("`"))
                            strPlayerOut = arrPlayerOut(1)
                            arrPlayerOut = strPlayerOut.Split(CChar("("))
                        End If

                        If CInt(DsMainEvents.Tables(2).Rows(i)("TEAM")) = m_objGameDetails.HomeTeamID And Not arrPlayerIn Is Nothing And Not arrPlayerOut Is Nothing Then
                            strHomePlayerIn = arrPlayerIn(0).Trim
                            strAwayPlayerIn = "&nbsp;"
                            strHomePlayerOut = arrPlayerOut(0).Trim
                            strAwayPlayerOut = "&nbsp;"
                            strReasn = "(" & CStr(intMin) & ") " & strReasn
                        ElseIf Not arrPlayerIn Is Nothing And Not arrPlayerOut Is Nothing Then
                            strAwayPlayerIn = arrPlayerIn(0).Trim
                            strHomePlayerIn = "&nbsp;"
                            strAwayPlayerOut = arrPlayerOut(0).Trim
                            strHomePlayerOut = "&nbsp;"
                            strReasn = strReasn & " (" & CStr(intMin) & ")"
                        End If
                        If (strReasn = "") Then
                            strReasn = strReasn & " (" & CStr(intMin) & ")"
                        End If

                        strBuilder.Append("<tr class='black12'><td align='left'>" & strHomePlayerIn & "</td><td align='left'>" & strHomePlayerOut & "</td><td align='center'>" & strReasn & "</td><td align='right'>" & strAwayPlayerIn & "</td><td align='right'>" & strAwayPlayerOut & "</td></tr>")
                    Next
                End If
                strBuilder.Append("</table>") 'end of substitutions
            End If

            'referees
            strBuilder.Append("<br /><table id='Referees' width='100%' class='mystyle'><tr><td class='tblhdrwhite' colspan='4'>" & strheadertext24 & "</td></tr>")
            strBuilder.Append("<tr class='tblhdrgrey'><td align='left' width='25%'><b>" & strheadertext28 & "</b></td><td align='left' width='25%'><b>" & strheadertext25 & "</b></td><td align='left' width='25%'><b>" & strheadertext26 & "</b></td><td align='left' width='25%'><b>" & strheadertext29 & "</b></td></tr>")

            Dim dsOfficials As DataTable = DsMainEvents.Tables(6).Copy()
            Dim strAsstReferees As String = String.Empty
            If dsOfficials.Rows.Count > 0 Then
                If IsDBNull(dsOfficials.Rows(0)("REFEREE")) Then
                    strReferee = String.Empty
                Else
                    strReferee = dsOfficials.Rows(0)("REFEREE").ToString
                End If

                If IsDBNull(dsOfficials.Rows(0)("4thOFFICIAL")) Then
                    str4thOfficial = String.Empty
                Else
                    str4thOfficial = dsOfficials.Rows(0)("4thOFFICIAL").ToString
                End If

                If not IsDBNull(dsOfficials.Rows(0)("5thOfficial")) Then
                    str5thOfficial = dsOfficials.Rows(0)("5thOfficial").ToString
                End If

                If IsDBNull(dsOfficials.Rows(0)("ASST-REFEREE1")) Then
                    strAsstReferee = String.Empty
                Else
                    strAsstReferee = dsOfficials.Rows(0)("ASST-REFEREE1").ToString
                End If

                If IsDBNull(dsOfficials.Rows(0)("ASST-REFEREE2")) Then
                    strAsstReferee2 = String.Empty
                Else
                    strAsstReferee2 = dsOfficials.Rows(0)("ASST-REFEREE2").ToString
                End If
                ' CHECKING ASST REFEREE'S
                If Not String.IsNullOrEmpty(strAsstReferee) And Not String.IsNullOrEmpty(strAsstReferee2) Then
                    strAsstReferees = strAsstReferee.Replace("-", ", ") + "; " + strAsstReferee2.Replace("-", ", ")
                ElseIf Not String.IsNullOrEmpty(strAsstReferee) And String.IsNullOrEmpty(strAsstReferee2) Then
                    strAsstReferees = strAsstReferee.Replace("-", ", ") + strAsstReferee2.Replace("-", " ")
                ElseIf String.IsNullOrEmpty(strAsstReferee) And Not String.IsNullOrEmpty(strAsstReferee2) Then
                    strAsstReferees = strAsstReferee2.Replace("-", ", ")
                Else
                    strAsstReferees = String.Empty
                End If
            End If
            strReferee = strReferee.Replace("-", ", ")
            str4thOfficial = str4thOfficial.Replace("-", ", ")
            str5thOfficial = str5thOfficial.Replace("-", ", ")
            strBuilder.Append("<tr class='black12'><td align='left'>" & strReferee & "</td><td align='left'>" & strAsstReferees & "</td><td align='left'>" & str4thOfficial & "</td><td align='left'>" & str5thOfficial & "</td></tr>")
            strBuilder.Append("</table>") 'end of referees

            strBuilder.Append("</td>") 'end of middle section ----------------------------------------

            'away section ----------------------------------------------------------------------------
            strBuilder.Append("<td valign='top' id='Away'>")
            'away team info
            strBuilder.Append("<table width='100%' id='TeamInfo_Away'><tr><td class='tblhdrwhite'>" & strheadertext27 & "</td></tr><tr class='black12'><td style='height: 35px; vertical-align: middle;'><b>" & strheadertext23 & "</b>&nbsp;" & strAwayMgr & "</td></tr><tr class='black12'><td><b>Formation:</b>&nbsp;" & strAwayFormation & "</td></tr></table>")
            'away team lineup
            strBuilder.Append("<table width='100%' id='Lineup_Away'><tr><td class='tblhdrwhite' colspan='2'>" & strheadertext20 & "</td></tr>")
            If Not arrAwayPlayerList Is Nothing Then
                If arrAwayPlayerList.Count > 0 Then
                    For i As Integer = 0 To arrAwayPlayerList.Count - 1
                        arrAwayPlayerStrings = arrAwayPlayerList(i).ToString.Split(CChar("`"))

                        For k As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                            If DsMainEvents.Tables(2).Rows(k)("PLAYER_IN").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Or DsMainEvents.Tables(2).Rows(k)("OUT").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Then
                                strPeriod = DsMainEvents.Tables(2).Rows(k)("PERIOD").ToString
                                strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(2).Rows(k)("TIME").ToString), CInt(strPeriod))
                                intMin = getMinutes(strTime)
                            End If
                        Next

                        If (arrAwayPlayerList(1).ToString() <> "-") Then
                            s = arrAwayPlayerStrings(1).Substring(0, arrAwayPlayerStrings(1).IndexOf("~")).ToString
                            strBuilder.Append("<tr class='black12'><td width='90%' align='right'>")
                            For j As Integer = 0 To DsMainEvents.Tables(0).Rows.Count - 1
                                If (DsMainEvents.Tables(0).Rows(j).Item("goalscorer").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append("<img src=" & ast1 & "> &nbsp;")
                                End If
                            Next

                            For j As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                                If (DsMainEvents.Tables(2).Rows(j).Item("player_in").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append("(" & CStr(intMin) & ") <img src=" & asaplayerin1 & "> &nbsp;")
                                End If
                                If (DsMainEvents.Tables(2).Rows(j).Item("out").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append("(" & CStr(intMin) & ") <img src=" & asaplayerout1 & "> &nbsp;")
                                End If
                            Next

                            For j As Integer = 0 To DsMainEvents.Tables(1).Rows.Count - 1
                                If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "1") Then
                                    strBuilder.Append("<img src=" & asyellocard1 & "> &nbsp;")
                                End If
                                If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And (DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "2" Or DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "3")) Then
                                    strBuilder.Append("<img src=" & asredcard1 & "> &nbsp;")
                                End If
                            Next

                            strBuilder.Append(s & "</td><td width='10%' align='right'>" & arrAwayPlayerStrings(0).ToString & "</td>")
                        End If
                    Next
                End If
            End If
            strBuilder.Append("</table>") 'end of away team lineup
            'away team bench
            arrAwayPlayerStrings = Nothing
            arrAwayPlayerList = GetSubsituationsByGoalKeeper(DsPlayersLineUp, DsMainEvents, "AWAY")
            strBuilder.Append("<table width='100%' id='Bench_Away'><tr><td class='tblhdrwhite' colspan='2'>" & strheadertext21 & "</td></tr>")
            If Not arrAwayPlayerList Is Nothing Then
                If arrAwayPlayerList.Count > 0 Then
                    For i As Integer = 0 To arrAwayPlayerList.Count - 1
                        arrAwayPlayerStrings = arrAwayPlayerList(i).ToString.Split(CChar("`"))

                        For k As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                            If DsMainEvents.Tables(2).Rows(k)("PLAYER_IN").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Or DsMainEvents.Tables(2).Rows(k)("OUT").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim Then
                                strPeriod = DsMainEvents.Tables(2).Rows(k)("PERIOD").ToString
                                strTime = CalculateTimeElapseAfter(CInt(DsMainEvents.Tables(2).Rows(k)("TIME").ToString), CInt(strPeriod))
                                intMin = getMinutes(strTime)
                            End If
                        Next

                        If (arrHomePlayerStrings(1).ToString() <> "-") Then
                            s = arrAwayPlayerStrings(1).Substring(0, arrAwayPlayerStrings(1).IndexOf("~")).ToString
                            strBuilder.Append("<tr class='black12'><td width='90%' align='right'>")
                            For j As Integer = 0 To DsMainEvents.Tables(0).Rows.Count - 1
                                If (DsMainEvents.Tables(0).Rows(j).Item("goalscorer").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append("<img src=" & ast1 & "> &nbsp;")
                                End If
                            Next

                            For j As Integer = 0 To DsMainEvents.Tables(2).Rows.Count - 1
                                If (DsMainEvents.Tables(2).Rows(j).Item("player_in").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append("(" & CStr(intMin) & ") <img src=" & asaplayerin1 & "> &nbsp;")
                                End If
                                If (DsMainEvents.Tables(2).Rows(j).Item("out").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim) Then
                                    strBuilder.Append("(" & CStr(intMin) & ") <img src=" & asaplayerout1 & "> &nbsp;")
                                End If
                            Next

                            For j As Integer = 0 To DsMainEvents.Tables(1).Rows.Count - 1
                                If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "1") Then
                                    strBuilder.Append("<img src=" & asyellocard1 & "> &nbsp;")
                                End If
                                If (DsMainEvents.Tables(1).Rows(j).Item("player_id").ToString = arrAwayPlayerStrings(1).Substring(arrAwayPlayerStrings(1).IndexOf("~"), arrAwayPlayerStrings(1).Length - arrAwayPlayerStrings(1).IndexOf("~")).ToString.Replace("~", "").Trim And (DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "2" Or DsMainEvents.Tables(1).Rows(j).Item("BOOKING_TYPE_ID").ToString = "3")) Then
                                    strBuilder.Append("<img src=" & asredcard1 & "> &nbsp;")
                                End If
                            Next

                            strBuilder.Append(s & "</td><td width='10%' align='right'>" & arrAwayPlayerStrings(0).ToString & "</td>")
                        End If
                    Next
                End If
            End If
            strBuilder.Append("</table>") 'end of away team bench
            'away team injuries and suspensions
            strBuilder.Append("<table width='100%' id='Injuries_Away'><tr><td class='tblhdrwhite' colspan='2'>" & strheadertext22 & "</td></tr>")

            For j As Integer = 0 To dtt2.Rows.Count - 1
                strBuilder.Append("<tr class='black12'><td width='90%' align='right'>" & dtt2.Rows(j).Item("PLAYER").ToString & "</td><td width='10%' align='right'>" & dtt2.Rows(j).Item("UNIFORM").ToString & "</td></tr>")
            Next

            strBuilder.Append("</table>") 'end of away team injuries and suspensions
            strBuilder.Append("</td></tr>") 'end of away section -------------------------------------
            strBuilder.Append("</table>")
            strBuilder.Append("</body>")
            strBuilder.Append("</html>")

            wbReport.DocumentText = strBuilder.ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try

            If CurrPeriod = 1 Then
                Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
            ElseIf CurrPeriod = 2 Then
                Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
            ElseIf CurrPeriod = 3 Then
                Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
            ElseIf CurrPeriod = 4 Then
                Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
            End If

            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            Dim StrTime As String = "00:00"
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)

                        'DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        StrTime = "90+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 3 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        StrTime = "105+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 4 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        StrTime = "120+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 5 Then
                    StrTime = "120"
                End If
                'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
            Else
                If CurrPeriod = 1 Or CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        StrTime = "45+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

                    End If
                Else
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        StrTime = "15+" + CStr(DiffTime) + ":" + CStr(DiffSec)
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                End If
            End If
            Return StrTime
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetAssistingPlayerName(ByVal PlayerName As String) As String
        Try

            If String.IsNullOrEmpty(PlayerName) Then
                Return "-"
            Else
                Return PlayerName
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetBoxScoreReport(ByVal intGameCode As Integer, ByVal intFeedNumber As Integer, ByVal LanguageID As Integer) As DataSet
        Try
            Return m_objBoxScore.GetBoxScoreReport(intGameCode, intFeedNumber, LanguageID)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function GetTeamName(ByVal strTeamId As String) As String
        Try
            If strTeamId = Nothing Or String.IsNullOrEmpty(strTeamId) Then
                Return Nothing
            End If
            If (CInt(strTeamId) = m_objGameDetails.HomeTeamID) Then
                Return m_objGameDetails.HomeTeam
            Else
                Return m_objGameDetails.AwayTeam
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetTeamLineUPByGoalKeeper(ByVal ds As DataSet, ByVal strTeam As String) As ArrayList
        Dim teamlist As New ArrayList()
        Dim strPlayerUniform As String = String.Empty
        Try
            'If ds Is Nothing Then
            '    Exit Function
            'End If
            If ds.Tables.Count <= 0 Then
                teamlist = Nothing
                Return teamlist
            End If

            If Not IsDBNull(ds.Tables(strTeam)) And ds.Tables(strTeam).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(strTeam).Rows.Count - 1
                    If Not IsDBNull(ds.Tables(strTeam).Rows(i)("STARTING_POSITION")) Then
                        If CInt(ds.Tables(strTeam).Rows(i)("STARTING_POSITION").ToString) <= 11 And CInt(ds.Tables(strTeam).Rows(i)("STARTING_POSITION").ToString) > 0 Then
                            Dim strPlayer As String = ds.Tables(strTeam).Rows(i)("PLAYER").ToString()
                            If IsDBNull(ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER")) Then
                                strPlayerUniform = "--"
                            Else
                                strPlayerUniform = ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER").ToString()
                                If strPlayerUniform.Length = 1 Then
                                    'strPlayerUniform = "&nbsp;&nbsp;" + strPlayerUniform
                                    strPlayerUniform = strPlayerUniform
                                Else
                                    strPlayerUniform = strPlayerUniform
                                End If
                            End If

                            If (strPlayer.Contains("(GK)")) Then
                                'teamlist.Add(strPlayerUniform + "." + strPlayer)
                                teamlist.Add(strPlayerUniform + "`" + strPlayer + " ~ " + ds.Tables(strTeam).Rows(i)("PLAYER_id").ToString())

                            End If
                        End If
                    End If ' END OF DBNULL CHECK.
                Next

                For i As Integer = 0 To ds.Tables(strTeam).Rows.Count - 1
                    If Not IsDBNull(ds.Tables(strTeam).Rows(i)("STARTING_POSITION")) And CInt(ds.Tables(strTeam).Rows(i)("STARTING_POSITION").ToString) <= 11 And CInt(ds.Tables(strTeam).Rows(i)("STARTING_POSITION").ToString) <> 0 Then
                        Dim strPlayer As String = ds.Tables(strTeam).Rows(i)("PLAYER").ToString()
                        If IsDBNull(ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER")) Then
                            strPlayerUniform = "--"
                        Else
                            strPlayerUniform = ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER").ToString()
                            If strPlayerUniform.Length = 1 Then
                                'strPlayerUniform = "&nbsp;&nbsp;" + strPlayerUniform
                                strPlayerUniform = strPlayerUniform
                            Else
                                If strPlayerUniform.Contains("0") Then
                                    strPlayerUniform = " " + strPlayerUniform
                                Else
                                    strPlayerUniform = " " + strPlayerUniform
                                End If

                            End If

                        End If

                        If Not (strPlayer.Contains("(GK)")) Then
                            'teamlist.Add(strPlayerUniform + "." + strPlayer)
                            teamlist.Add(strPlayerUniform + "`" + strPlayer + " ~ " + ds.Tables(strTeam).Rows(i)("PLAYER_id").ToString())
                        End If
                    End If
                Next
            End If ' END ROWCOUNT

            If teamlist.Count > 0 Then
                Return teamlist
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function GetSubsituationsByGoalKeeper(ByVal ds As DataSet, ByVal dsSubplayer As DataSet, ByVal strTeam As String) As ArrayList
        Dim teamlist As New ArrayList()
        Dim strPlayerUniform As String = String.Empty
        Dim strPlayerID As String = String.Empty
        Dim strPlayerName As String = String.Empty
        Dim strTeamID As String = String.Empty
        Dim strTeamType As String = String.Empty
        Try
            If ds.Tables.Count <= 0 Then
                teamlist = Nothing
                Return teamlist
            End If
            If dsSubplayer.Tables.Count > 0 Then

                'If IsDBNull(dsSubplayer.Tables(2)) Or dsSubplayer.Tables(2) Is Nothing Then
                'Else
                '    For i As Integer = 0 To dsSubplayer.Tables(2).Rows.Count - 1
                '        strTeamID = dsSubplayer.Tables(2).Rows(i)("TEAM").ToString
                '        strTeamType = GetTeamType(strTeamID)
                '        If strTeam.Contains(strTeamType) Then
                '            strPlayerID = dsSubplayer.Tables(2).Rows(i)("OUT").ToString
                '            strPlayerName = FetchPlayerNames(CInt(dsSubplayer.Tables(2).Rows(i)("OUT").ToString))
                '            teamlist.Add(strPlayerName)
                '        End If
                '    Next
                'End If
            End If
            If Not IsDBNull(ds.Tables(strTeam)) And ds.Tables(strTeam).Rows.Count > 1 Then
                For i As Integer = 0 To ds.Tables(strTeam).Rows.Count - 1
                    If Not IsDBNull(ds.Tables(strTeam).Rows(i)("STARTING_POSITION")) Then

                        If CInt(ds.Tables(strTeam).Rows(i)("STARTING_POSITION").ToString) > 11 Then

                            Dim strPlayer As String = ds.Tables(strTeam).Rows(i)("PLAYER").ToString()
                            If IsDBNull(ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER")) Then
                                strPlayerUniform = "--"
                            Else
                                strPlayerUniform = ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER").ToString()
                                If strPlayerUniform.Length = 1 Then
                                    'strPlayerUniform = "&nbsp;&nbsp;" + strPlayerUniform
                                    strPlayerUniform = strPlayerUniform
                                Else
                                    If strPlayerUniform.Contains("0") Then

                                    Else
                                        strPlayerUniform = " " + strPlayerUniform
                                    End If

                                End If

                            End If

                            If (strPlayer.Contains("(GK)")) Then
                                teamlist.Add(strPlayerUniform + "`" + strPlayer + "~" + ds.Tables(strTeam).Rows(i)("player_id").ToString())
                            End If
                        End If ' END OF BENCHPALYER CHECK
                    End If ' END OF NULL CHECK
                Next

                For i As Integer = 0 To ds.Tables(strTeam).Rows.Count - 1
                    If Not IsDBNull(ds.Tables(strTeam).Rows(i)("STARTING_POSITION")) Then
                        If CInt(ds.Tables(strTeam).Rows(i)("STARTING_POSITION").ToString) > 11 Then
                            Dim strPlayer As String = ds.Tables(strTeam).Rows(i)("PLAYER").ToString()
                            If IsDBNull(ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER")) Then
                                strPlayerUniform = "--"
                            Else
                                strPlayerUniform = ds.Tables(strTeam).Rows(i)("DISPLAY_UNIFORM_NUMBER").ToString()
                                If strPlayerUniform.Length = 1 Then
                                    'strPlayerUniform = "&nbsp;&nbsp;" + strPlayerUniform
                                    strPlayerUniform = strPlayerUniform
                                Else
                                    strPlayerUniform = " " + strPlayerUniform
                                End If ' END OF UNIFORM CHECK.
                            End If

                            If Not (strPlayer.Contains("(GK)")) Then
                                teamlist.Add(strPlayerUniform + "`" + strPlayer + "~" + ds.Tables(strTeam).Rows(i)("player_id").ToString())
                            End If
                        End If ' END OF BERNCH
                    End If ' END OF NULL CHECK.
                Next
            End If ' END ROWCOUNT

            If teamlist.Count > 0 Then
                Return teamlist
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function GetTeamFormation(ByVal dsFormation As DataSet, ByVal intTeamID As Integer) As String
        Try
            Dim DR() As DataRow
            If dsFormation.Tables(3).Rows.Count > 0 Then
                DR = dsFormation.Tables(3).Select("TEAM_ID = " & intTeamID)
                If IsDBNull(DR) Or DR Is Nothing Or DR.Length = 0 Then
                    Return Nothing
                Else
                    Return "(" & DR(0).Item(1).ToString & ")"
                End If
            End If
            Return Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetHalfTimeScores(ByVal dsHalfTimeScore As DataSet) As String
        Dim strFirstHalfScores As String = String.Empty
        Dim strSecondHalfScores As String = String.Empty
        Dim drTeamID() As DataRow
        If dsHalfTimeScore.Tables(4).Rows.Count > 0 Then
            drTeamID = dsHalfTimeScore.Tables(4).Select("Team_ID = " & m_objGameDetails.HomeTeamID)
            If drTeamID.Length > 0 Then
                strFirstHalfScores = dsHalfTimeScore.Tables(4).Rows(0)("OFFENSE_SCORE").ToString + "-" + dsHalfTimeScore.Tables(4).Rows(0)("DEFENSE_SCORE").ToString
            Else
                strFirstHalfScores = dsHalfTimeScore.Tables(4).Rows(0)("DEFENSE_SCORE").ToString + "-" + dsHalfTimeScore.Tables(4).Rows(0)("OFFENSE_SCORE").ToString
            End If
            'strFirstHalfScores = dsHalfTimeScore.Tables(4).Rows(0)(1).ToString + "-" + dsHalfTimeScore.Tables(4).Rows(0)(0).ToString
        End If

        'If dsHalfTimeScore.Tables(5).Rows.Count > 0 Then
        '    drTeamID = dsHalfTimeScore.Tables(5).Select("Team_ID=" & m_objGameDetails.HomeTeamID)
        '    If drTeamID.Length > 0 Then
        '        strSecondHalfScores = dsHalfTimeScore.Tables(5).Rows(0)("OFFENSE_SCORE").ToString + "-" + dsHalfTimeScore.Tables(5).Rows(0)("DEFENSE_SCORE").ToString
        '    Else
        '        strSecondHalfScores = dsHalfTimeScore.Tables(5).Rows(0)("DEFENSE_SCORE").ToString + "-" + dsHalfTimeScore.Tables(5).Rows(0)("OFFENSE_SCORE").ToString

        '    End If
        '    'strSecondHalfScores = dsHalfTimeScore.Tables(5).Rows(0)(0).ToString + "-" + dsHalfTimeScore.Tables(5).Rows(0)(1).ToString
        'End If

        m_dsScores = m_objBoxScore.GetPenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.CoverageLevel)
        Dim tempHomeScore As Integer = 0
        Dim tempAwayScore As Integer = 0

        If m_dsScores.Tables.Count > 0 Then

            If m_dsScores.Tables(2).Rows.Count > 0 Then
                If CInt(m_dsScores.Tables(2).Rows(m_dsScores.Tables(2).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                    m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(2).Rows(m_dsScores.Tables(2).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(2).Rows(m_dsScores.Tables(2).Rows.Count - 1).Item("DEFENSE_SCORE"))
                Else
                    m_objGameDetails.AwayScore = CInt(m_dsScores.Tables(2).Rows(m_dsScores.Tables(2).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    m_objGameDetails.HomeScore = CInt(m_dsScores.Tables(2).Rows(m_dsScores.Tables(2).Rows.Count - 1).Item("DEFENSE_SCORE"))
                End If
            End If

            If m_dsScores.Tables(0).Rows.Count > 0 Then
                If CDbl(m_dsScores.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                    If m_dsScores.Tables(1).Rows.Count > 0 Then
                        If CInt(m_dsScores.Tables(1).Rows(m_dsScores.Tables(1).Rows.Count - 1).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                            tempHomeScore = CInt(m_dsScores.Tables(1).Rows(m_dsScores.Tables(1).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            tempAwayScore = CInt(m_dsScores.Tables(1).Rows(m_dsScores.Tables(1).Rows.Count - 1).Item("DEFENSE_SCORE"))
                        Else
                            tempAwayScore = CInt(m_dsScores.Tables(1).Rows(m_dsScores.Tables(1).Rows.Count - 1).Item("OFFENSE_SCORE"))
                            tempHomeScore = CInt(m_dsScores.Tables(1).Rows(m_dsScores.Tables(1).Rows.Count - 1).Item("DEFENSE_SCORE"))
                        End If

                        m_objGameDetails.PenaltyShootOutHomeScore = m_objGameDetails.HomeScore - tempHomeScore
                        m_objGameDetails.PenaltyShootOutAwayScore = m_objGameDetails.AwayScore - tempAwayScore

                        strSecondHalfScores = CStr(tempHomeScore) & " - " & " " & CStr(tempAwayScore)

                    End If
                Else
                    strSecondHalfScores = CStr(m_objGameDetails.HomeScore) & " - " & CStr(m_objGameDetails.AwayScore)
                End If
            End If
        End If

        If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 0 Then
            Return strFirstHalfScores
        Else
            Return strSecondHalfScores + " (" + strFirstHalfScores + ")"
        End If

    End Function

    Private Function Get24HrFormat(ByVal dt As DateTime) As String
        Try

            Return Format(dt, "h:mm:ss")
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub btnPrint_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Function GetTeamType(ByVal strTeamID As String) As String
        If Not String.IsNullOrEmpty(strTeamID) And m_objGameDetails.HomeTeamID.ToString.Trim().Equals(strTeamID.Trim()) Then
            Return "HOME"
        End If
        If Not String.IsNullOrEmpty(strTeamID) And m_objGameDetails.AwayTeamID.ToString.Trim().Equals(strTeamID.Trim()) Then
            Return "AWAY"
        End If
        Return String.Empty
    End Function
    Public Function GetMgrName(ByVal intTEamID As Integer, ByVal dsMgrInfo As DataSet) As String
        Try
            Dim DR() As DataRow
            Dim strMgr As String = Nothing
            If dsMgrInfo.Tables.Count > 0 Then
                If dsMgrInfo.Tables(7).Rows.Count > 0 Then
                    DR = dsMgrInfo.Tables(7).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID)

                    If IsDBNull(DR) Or DR Is Nothing Or DR.Length = 0 Then
                        strMgr += "" + ";"
                    Else
                        strMgr += DR(0).Item(1).ToString + ";"
                    End If

                    DR = dsMgrInfo.Tables(7).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID)

                    If IsDBNull(DR) Or DR Is Nothing Or DR.Length = 0 Then
                        strMgr += " " + ";"
                    Else
                        strMgr += DR(0).Item(1).ToString + ";"
                    End If  'END OF ROWCOUNT

                End If ' END OF  TABLE COUNT CHECK

            End If

            Return strMgr
        Catch ex As Exception
            Throw ex

        End Try
        Return Nothing
    End Function

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            PrintDialog1.Document = PrintDocument1
            PrintDialog1.AllowSomePages = True
            PrintDocument1.DefaultPageSettings.Landscape = True
            If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim pritcopy As Integer = DirectCast(DirectCast(PrintDialog1, System.Windows.Forms.PrintDialog).PrinterSettings, System.Drawing.Printing.PrinterSettings).Copies
                For i As Integer = 0 To pritcopy - 1
                    wbReport.Print()
                Next
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnSave_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim path As String
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            If wbReport.DocumentText = Nothing Then
                'essageDialog.Show("No Document is Present", Me.Text)
                Exit Sub
            Else
                path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                SaveFileDialog1.InitialDirectory = path
                SaveFileDialog1.Filter = "HTML Files|*.htm"
                If SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fileName As String = SaveFileDialog1.FileName
                    Dim sw As StreamWriter
                    If File.Exists(fileName) = False Then
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(strBuilder)
                        sw.Flush()
                        sw.Close()
                    Else
                        File.Delete(fileName)
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(strBuilder)
                        sw.Flush()
                        sw.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub wbReport_DocumentCompleted(ByVal sender As System.Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles wbReport.DocumentCompleted

    End Sub

    Private Function CreateDataTable() As DataTable
        Try
            Dim dtPlayerLineUP As New DataTable()

            Dim dcplayerTimeElapsedColoumn As DataColumn

            dcplayerTimeElapsedColoumn = New DataColumn()
            dcplayerTimeElapsedColoumn.DataType = Type.[GetType]("System.String")
            dcplayerTimeElapsedColoumn.ColumnName = "Player_ID"
            dtPlayerLineUP.Columns.Add(dcplayerTimeElapsedColoumn)

            dcplayerTimeElapsedColoumn = New DataColumn()
            dcplayerTimeElapsedColoumn.DataType = Type.[GetType]("System.String")
            dcplayerTimeElapsedColoumn.ColumnName = "Period"
            dtPlayerLineUP.Columns.Add(dcplayerTimeElapsedColoumn)

            dcplayerTimeElapsedColoumn = New DataColumn()
            dcplayerTimeElapsedColoumn.DataType = Type.[GetType]("System.String")
            dcplayerTimeElapsedColoumn.ColumnName = "Player_IN"
            dtPlayerLineUP.Columns.Add(dcplayerTimeElapsedColoumn)

            dcplayerTimeElapsedColoumn = New DataColumn()
            dcplayerTimeElapsedColoumn.DataType = Type.[GetType]("System.String")
            dcplayerTimeElapsedColoumn.ColumnName = "Player_Out"
            dtPlayerLineUP.Columns.Add(dcplayerTimeElapsedColoumn)

            dcplayerTimeElapsedColoumn = New DataColumn()
            dcplayerTimeElapsedColoumn.DataType = Type.[GetType]("System.String")
            dcplayerTimeElapsedColoumn.ColumnName = "Elapsed_Time"
            dtPlayerLineUP.Columns.Add(dcplayerTimeElapsedColoumn)

            Return dtPlayerLineUP
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalCulatePlayerMinsAndBuildDt() As DataTable
        Dim dtPlayerMins As DataTable = CreateDataTable()
        If dtPlayerMins.Rows.Count > 0 Then
            For i = 0 To dtPlayerMins.Rows.Count - 1

            Next
            Return Nothing
        End If
        Return Nothing

    End Function

    Private Function FecthRedCardAndSubsitution() As DataSet
        Try
            Return m_objBoxScore.FetchPlayerSubsitutionAndRedCardEvents(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetInAndOutTime() As String
        Try
            Dim strTotalTime As String = String.Empty
            Dim strPlayerSubsitutionList As String = String.Empty
            Dim dsSubAndRedCardEvents As DataSet = FecthRedCardAndSubsitution()
            Dim dtPlayerElapsedTime As DataTable = CreateDataTable()
            Dim dsPlayerRoosterList As DataSet = m_objTeamSetup.RosterInfo.Copy()
            Dim intPlayerID As Integer = -1
            Dim intPlayerLineUp As Integer = -1
            ' Dim strSelect As String = "PLAYER_OUT_ID = '" & strPlayerID & "' AND STARTING_POSTION = '" & StrPlayerLineUp & "' "
            Dim strSelect As String = "PLAYER_OUT_ID = '" & intPlayerID & "'"
            Dim strSelectInTime As String = "OFFENSIVE_PLAYER_ID = '" & intPlayerLineUp & "'"
            Dim intStartPostion As Integer = 0
            Dim strInTime As String = String.Empty
            Dim strOutTime As String = String.Empty
            Dim drTimeElapsed As DataRow = Nothing
            Dim DR As DataRow() = Nothing

            If dsPlayerRoosterList.Tables.Count > 0 Then
                If dsPlayerRoosterList.Tables("HOME").Rows.Count > 0 Then
                    For i As Integer = 0 To dsPlayerRoosterList.Tables("HOME").Rows.Count - 1
                        intPlayerID = CInt(dsPlayerRoosterList.Tables("HOME").Rows(i)("PLAYER_ID").ToString())
                        intPlayerLineUp = CInt(dsPlayerRoosterList.Tables("HOME").Rows(i)("STARTING_POSITION").ToString())
                        If Not IsDBNull(dsPlayerRoosterList.Tables("HOME").Rows(i)("STARTING_POSITION")) Then
                            intStartPostion = CInt(dsPlayerRoosterList.Tables("HOME").Rows(i)("STARTING_POSITION").ToString())

                            If intStartPostion < 11 Then

                                dtPlayerElapsedTime.Rows.Add(intPlayerID.ToString, m_objGameDetails.CurrentPeriod, "0", "0", frmMain.UdcRunningClock1.URCCurrentTime)

                            End If

                            If intStartPostion > 11 Or intStartPostion = 0 Then

                                If dsSubAndRedCardEvents.Tables.Count > 0 Then

                                    If dsSubAndRedCardEvents.Tables(0).Rows.Count > 0 Then
                                        DR = dsSubAndRedCardEvents.Tables(0).Select("PLAYER_OUT_ID = '" & intPlayerID & "'")
                                        If DR.Length > 0 Then
                                            If Not IsDBNull(DR(0).Item("TIME_ELAPSED")) Then
                                                strOutTime = DR(0).Item("TIME_ELAPSED").ToString()
                                            End If ' END TIME ELAPSED
                                        End If ' END OF LENGTH COUNT.
                                    End If ' End IF
                                    DR = Nothing
                                    DR = dsSubAndRedCardEvents.Tables(0).Select("OFFENSIVE_PLAYER_ID = '" & intPlayerID & "'")

                                    If DR.Length > 0 Then

                                        If Not IsDBNull(DR(0).Item("TIME_ELAPSED")) Then

                                            strInTime = DR(0).Item("TIME_ELAPSED").ToString()
                                        End If ' END TIME ELAPSED
                                    End If ' END OF LENGTH COUNT.
                                End If  ' END OF TABLE COUNT DSSUB

                            End If ' END OF START POSITION

                            If Not String.IsNullOrEmpty(strInTime) And Not String.IsNullOrEmpty(strOutTime) Then
                                strTotalTime = Convert.ToString(CInt(strInTime) - CInt(strOutTime))
                            End If

                            If i < dsSubAndRedCardEvents.Tables(0).Rows.Count - 1 Then
                                dtPlayerElapsedTime.Rows.Add(intPlayerID.ToString, dsSubAndRedCardEvents.Tables(0).Rows(i)("PERIOD").ToString, strInTime, strOutTime, strTotalTime)
                            End If

                        End If ' END OF PLAYER ROOSTER LIST

                    Next ' END FOR
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function getMinutes(ByVal strTime As String) As Integer
        Try
            Dim elapsedMin As Integer
            Dim arr As Array = strTime.Split(CChar(":"))

            'If CInt(arr.GetValue(1)) >= 30 Then
            '    elapsedMin = CInt(arr.GetValue(0)) + 1
            'Else
            '    elapsedMin = CInt(arr.GetValue(0))
            'End If

            If CInt(arr.GetValue(1)) > 0 And CInt(arr.GetValue(1)) <= 59 Then
                elapsedMin = CInt(arr.GetValue(0)) + 1
            Else
                elapsedMin = CInt(arr.GetValue(0))
            End If

            Return elapsedMin
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub GetCurrentPlayersAfterRestart()
        Try
            'CLEARS THE ROSTER TABLES PALCED IN THE PROPERTY DATSTET
            If Not m_objTeamSetup.RosterInfo Is Nothing Then
                m_objTeamSetup.RosterInfo.Tables.Clear()
            End If

            'FETCH ROSTERS FOR THAT PARTICULAR GAME
            Dim DsPlayers As DataSet = m_objModule1Main.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objLoginDetails.LanguageID)
            If DsPlayers.Tables(0).Rows.Count > 0 Then
                DsPlayers.Tables(0).TableName = "Home"
                DsPlayers.Tables(1).TableName = "Away"

                'CREATING TWO MORE TABLES WHICH IS USED TO HOLD THE CURRENT PLAYERS
                'PLAYING IN THE FIELD
                Dim DsCurrentPlayer As DataSet
                m_objTeamSetup.RosterInfo = DsPlayers.Copy()
                DsCurrentPlayer = DsPlayers.Copy()
                DsCurrentPlayer.Tables(0).TableName = "HomeCurrent"
                DsCurrentPlayer.Tables(1).TableName = "AwayCurrent"

                'Getting the current players from red card and sub events
                Dim DsSubEvents As DataSet = m_objModule1Main.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                If DsSubEvents.Tables(0).Rows.Count > 0 Then
                    For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                        Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                            Case 7, 22
                                For intTableCount As Integer = 0 To DsCurrentPlayer.Tables.Count - 1
                                    If Not IsDBNull(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) Then
                                        Dim DrPlayerOut() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) & "")
                                        If DrPlayerOut.Length > 0 Then
                                            If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = 7 Then
                                                'RED CARD
                                                DrPlayerOut(0).BeginEdit()
                                                DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                                DrPlayerOut(0).EndEdit()
                                                DrPlayerOut(0).AcceptChanges()
                                            Else
                                                'SUBSTITUTION
                                                Dim DrPlayerIn() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) & "")
                                                If DrPlayerIn.Length > 0 Then
                                                    DrPlayerIn(0).BeginEdit()
                                                    DrPlayerIn(0).Item("STARTING_POSITION") = DrPlayerOut(0).Item("STARTING_POSITION")
                                                    DrPlayerIn(0).EndEdit()

                                                    DrPlayerOut(0).BeginEdit()
                                                    DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                                    DrPlayerOut(0).EndEdit()

                                                    DrPlayerIn(0).AcceptChanges()
                                                End If
                                            End If
                                        End If
                                    End If
                                Next
                        End Select
                    Next
                End If

                'FILLS THE ROSTERS INTO THE PROPERTY DATASET
                m_objTeamSetup.RosterInfo.Merge(DsCurrentPlayer)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh.Text, Nothing, 1, 0)
            CustomReports()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class