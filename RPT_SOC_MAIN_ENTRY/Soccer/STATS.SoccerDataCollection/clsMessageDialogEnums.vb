﻿#Region " Comments "
'------------------------------------------------------------------------------------------------------------------------------------------------------
' File name     :   clsMessageDialogEnums.vb
' Author        :   Fiaz Ahmed
' Created on    :   30th July, 2008
' Description   :   This file contains all enums required for invoking Message Dialog(see frmMessageDialog.vb).
'------------------------------------------------------------------------------------------------------------------------------------------------------
' MODIFICATION LOG
'------------------------------------------------------------------------------------------------------------------------------------------------------
' ID           |Modified by            |Modified on     |Description
'--------------|-----------------------|----------------|----------------------------------------------------------------------------------------------
'              |                       |                |
'              |                       |                |
'------------------------------------------------------------------------------------------------------------------------------------------------------
#End Region

#Region " Enums "
Public Enum MessageDialogButtons
    AbortRetryIgnore
    OK
    OKCancel
    RetryCancel
    YesNo
    YesNoCancel
End Enum

Public Enum MessageDialogIcon
    Asterisk
    [Error]
    Exclamation
    Hand
    Information
    None
    Question
    Warning
End Enum
#End Region