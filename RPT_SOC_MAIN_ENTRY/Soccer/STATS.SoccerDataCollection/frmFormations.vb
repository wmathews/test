﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmFormations
' Author        : Shirley Ranjini S
' Created Date  : 11-05-09
' Description   : This form is used for the selection of starting Lineups players for Home and Away Team
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmFormations
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objFormations As STATS.SoccerBL.clsSubFormations = STATS.SoccerBL.clsSubFormations.GetInstance()
    Private m_objUtility As New clsUtility
    Dim intTeamID As Integer
    Dim intPeriod As Integer
    Dim m_DsFormations As DataSet
    Private m_blnIsFormationComboLoaded As Boolean = False
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Dim intPlayerInID As Integer
    Private lsupport As New languagesupport

    Private message1 As String = "Please click on Formation Apply/Reset button for the changes to take effect"
    Private message2 As String = "Please select the current formation."
    Private message3 As String = "Please select the formation specification."
    Private message4 As String = "Please fill the blanks with players playing OnField"

    Private Sub frmFormations_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CInt(m_objGameDetails.languageid) <> 1 Then
                message1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message1)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                lblFormation.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblFormation.Text)
                Label3.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label3.Text)
                message1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message2)
                message2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message2)
                message3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message3)
                message4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message4)

            End If
            If intTeamID = m_objGameDetails.HomeTeamID Then
                radTeam.Text = m_objGameDetails.HomeTeamAbbrev
                lblTeamName.Text = m_objGameDetails.HomeTeam
            Else
                radTeam.Text = m_objGameDetails.AwayTeam
                lblTeamName.Text = m_objGameDetails.AwayTeam
            End If
            FetchTeamLogo(intTeamID)
            LoadFormations()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Fetching logo for the selected Team
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamLogo(ByVal TeamID As Integer)
        Try
            Dim v_memLogo As MemoryStream
            picTeamLogo.Image = Nothing
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, TeamID)
            If v_memLogo IsNot Nothing Then
                picTeamLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub LoadFormations()
        Try

            m_DsFormations = m_objFormations.GetFormationCoach(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intTeamID)
            clsUtility.LoadControl(cmbFormation, clsUtility.SelectTypeDisplay.NONE, m_DsFormations.Tables(0), "FORMATION", "FORMATION_ID")
            m_blnIsFormationComboLoaded = True

            If m_DsFormations.Tables(2).Rows.Count > 0 Then
                If Not IsDBNull(m_DsFormations.Tables(2).Rows(0).Item("START_FORMATION_ID")) Then
                    cmbFormation.SelectedValue = m_DsFormations.Tables(2).Rows(0).Item("START_FORMATION_ID")
                Else
                    cmbFormation.SelectedIndex = -1
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Sub New(ByVal Team_ID As Integer, ByVal Period As Integer, ByVal player_inID As Integer)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        intTeamID = Team_ID

        intPeriod = Period

        intPlayerInID = player_inID
        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub LoadFormationSpecification(ByVal FormationID As Integer)
        Try
            Dim dsFormationspec As New DataSet
            m_blnIsFormationComboLoaded = False

            dsFormationspec = m_objFormations.GetFormationSpecification(FormationID, m_objGameDetails.languageid)
            clsUtility.LoadControl(cmbFormationSpecification, clsUtility.SelectTypeDisplay.NONE, dsFormationspec.Tables(0), "FORMATION_SPEC", "FORMATION_SPEC_ID")
            If cmbFormationSpecification.Items.Count - 1 > 0 Then
                If FormationID = 0 Then
                    cmbFormationSpecification.SelectedIndex = 0
                Else
                    cmbFormationSpecification.SelectedIndex = 1
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbFormation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFormation.SelectedIndexChanged
        Try
            If m_blnIsFormationComboLoaded = True Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbFormation.Text, lblFormation.Text, 1, 0)
                If cmbFormation.Items.Count > 0 Then
                    If cmbFormation.SelectedValue IsNot Nothing Then

                        LoadFormationSpecification(CInt(cmbFormation.SelectedValue))

                        m_blnIsFormationComboLoaded = True
                        If m_DsFormations.Tables(2).Rows.Count > 0 Then
                            If Not IsDBNull(m_DsFormations.Tables(2).Rows(0).Item("FORMATION_SPEC_ID")) = True Then
                                cmbFormationSpecification.SelectedValue = m_DsFormations.Tables(2).Rows(0).Item("FORMATION_SPEC_ID")
                            Else
                                cmbFormationSpecification.SelectedIndex = 1
                            End If

                        End If
                    End If
                End If

                RefreshFormationDisplay()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RefreshFormationDisplay()
        Try
            UdcSoccerFormation1.USFFormation = cmbFormation.Text
            UdcSoccerFormation1.USFFormationSpecification = cmbFormationSpecification.Text
            'Dim DsRostersInfo As DataSet = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID)
            'If DsRostersInfo.Tables.Count > 0 Then
            '    DsRostersInfo.Tables(0).TableName = "Home"
            '    DsRostersInfo.Tables(1).TableName = "Away"
            'End If
            'UdcSoccerFormation1.USFRoster = DsRostersInfo.Tables(GetTableName)
            Dim strTableName As String = ""
            If GetTableName() = "Home" Then
                strTableName = "HomeCurrent"
            Else
                strTableName = "AwayCurrent"
            End If

            Dim DRS() As DataRow = m_objTeamSetup.RosterInfo.Tables(strTableName).Select("STARTING_POSITION < 12  and starting_position <> 0") 'OR PLAYER_ID = " & intPlayerInID & "")
            Dim dt As DataTable = m_objTeamSetup.RosterInfo.Tables(GetTableName).Clone()
            For Each dr As DataRow In DRS
                dt.ImportRow(dr)
            Next
            UdcSoccerFormation1.USFRoster = dt

            Dim dtStartingLineup As New DataTable
            Dim drStartingLineup As DataRow
            dtStartingLineup.Columns.Add("LINEUP")
            dtStartingLineup.Columns(0).DataType = GetType(Integer)
            dtStartingLineup.Columns.Add("PLAYER_ID")
            dtStartingLineup.Columns(1).DataType = GetType(Integer)
            dtStartingLineup.Columns.Add("PLAYER")
            dtStartingLineup.Columns.Add("UNIFORM")
            dtStartingLineup.Columns.Add("LAST_NAME")
            dtStartingLineup.Columns.Add("MONIKER")
            dtStartingLineup.Columns.Add("POSITION_ABBREV")

            DRS = m_objTeamSetup.RosterInfo.Tables(strTableName).Select("starting_position < 12 and starting_position <> 0")
            For Each dr As DataRow In DRS
                drStartingLineup = dtStartingLineup.NewRow
                drStartingLineup("LINEUP") = dr.Item("STARTING_POSITION")
                drStartingLineup("PLAYER_ID") = dr.Item("PLAYER_ID")
                drStartingLineup("PLAYER") = dr.Item("PLAYER")
                drStartingLineup("UNIFORM") = dr.Item("UNIFORM")
                dtStartingLineup.Rows.Add(drStartingLineup)
            Next

            Dim dtBench As New DataTable
            dtBench.Columns.Add("LINEUP")
            dtBench.Columns(0).DataType = GetType(Integer)
            dtBench.Columns.Add("PLAYER_ID")
            dtBench.Columns(1).DataType = GetType(Integer)
            dtBench.Columns.Add("PLAYER")
            dtBench.Columns.Add("UNIFORM")
            dtBench.Columns.Add("LAST_NAME")
            dtBench.Columns.Add("MONIKER")
            dtBench.Columns.Add("POSITION_ABBREV")

            UdcSoccerFormation1.USFStartingLineup = dtStartingLineup
            UdcSoccerFormation1.USFBench = dtBench

            UdcSoccerFormation1.USFRefreshFormationDisplay()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetTableName() As String
        Try
            Dim drs() As DataRow

            drs = m_objTeamSetup.RosterInfo.Tables(0).Select("TEAM_ID = " & intTeamID & "")
            If drs.Length > 0 Then
                Return "Home"
            Else
                Return "Away"
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub cmbFormationSpecification_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFormationSpecification.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbFormationSpecification.Text, lblFormation.Text, 1, 0)
            If UdcSoccerFormation1.btnApply.Enabled = True Then
                MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If m_blnIsFormationComboLoaded = True And cmbFormationSpecification.SelectedIndex <> -1 Then
                RefreshFormationDisplay()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim drPosition() As DataRow
            If cmbFormation.SelectedIndex < 0 Then
                MessageDialog.Show(message2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            If cmbFormationSpecification.SelectedIndex < 0 Then
                MessageDialog.Show(message3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            Dim dtLineups As DataTable = UdcSoccerFormation1.USFStartingLineup

            Dim strTableName As String = ""
            If GetTableName() = "Home" Then
                strTableName = "HomeCurrent"
            Else
                strTableName = "AwayCurrent"
            End If

            Dim DRS() As DataRow = m_objTeamSetup.RosterInfo.Tables(strTableName).Select("STARTING_POSITION < 12  and starting_position <> 0")

            If dtLineups.Rows.Count <> DRS.Length Then
                MessageDialog.Show(message4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If

            Dim m_dsOnfield As DataSet
            m_dsOnfield = m_objFormations.GetOnfieldValue(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intTeamID, intPeriod)

            'Dim drs() As DataRow = m_objTeamSetup.RosterInfo.Tables(strTableName).Select("starting_position < 12 and starting_position <> 0")
            DRS = dtLineups.Select("LINEUP < 12")
            For Each dr As DataRow In DRS
                Dim drPBPData As DataRow
                drPBPData = m_objGameDetails.PBP.Tables(1).NewRow()
                drPBPData("ONFIELD_ID") = m_dsOnfield.Tables(0).Rows(0).Item("ONFIELD")
                drPBPData("GAME_CODE") = m_objGameDetails.GameCode
                drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drPBPData("ONFIELD_SEQ") = dr("LINEUP")
                drPBPData("TEAM_ID") = intTeamID
                drPBPData("PLAYER_ID") = dr("PLAYER_ID")
                drPosition = m_objTeamSetup.RosterInfo.Tables(strTableName).Select("PLAYER_ID = " & CInt(dr("PLAYER_ID")) & "")
                If drPosition.Length > 0 Then
                    drPBPData("POSITION") = IIf(IsDBNull(drPosition(0).Item("POSITION_ID_1")) = True, "0", drPosition(0).Item("POSITION_ID_1"))
                Else
                    drPBPData("POSITION") = DBNull.Value
                End If

                drPBPData("FORMATION_ID") = cmbFormation.SelectedValue
                drPBPData("FORMATION_SPEC_ID") = cmbFormationSpecification.SelectedValue
                drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
                drPBPData("PROCESSED") = "N"
                m_objGameDetails.PBP.Tables(1).Rows.Add(drPBPData)
            Next

            DRS = m_dsOnfield.Tables(1).Select("TEAM_ID <> " & intTeamID & "")
            For Each dr As DataRow In DRS
                Dim drPBPData As DataRow
                drPBPData = m_objGameDetails.PBP.Tables(1).NewRow()
                drPBPData("ONFIELD_ID") = m_dsOnfield.Tables(0).Rows(0).Item("ONFIELD")
                drPBPData("GAME_CODE") = m_objGameDetails.GameCode
                drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drPBPData("ONFIELD_SEQ") = CInt(m_objGameDetails.PBP.Tables(1).Rows(m_objGameDetails.PBP.Tables(1).Rows.Count - 1).Item("ONFIELD_SEQ")) + 1
                drPBPData("TEAM_ID") = CInt(dr("TEAM_ID"))
                drPBPData("PLAYER_ID") = dr("PLAYER_ID")
                drPBPData("POSITION") = IIf(IsDBNull(dr("POSITION")) = True, DBNull.Value, dr("POSITION"))
                drPBPData("FORMATION_ID") = IIf(IsDBNull(dr("FORMATION_ID")) = True, DBNull.Value, dr("FORMATION_ID"))
                drPBPData("FORMATION_SPEC_ID") = IIf(IsDBNull(dr("FORMATION_SPEC_ID")) = True, DBNull.Value, dr("FORMATION_SPEC_ID"))
                drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
                drPBPData("PROCESSED") = "N"
                m_objGameDetails.PBP.Tables(1).Rows.Add(drPBPData)
            Next
            m_objGameDetails.PBP.Tables(1).AcceptChanges()
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerFormation1_USFApplyClick(ByVal sender As Object, ByVal e As SFEventArgs) Handles UdcSoccerFormation1.USFApplyClick
        Try
            If e.USFStartingLineup.Rows.Count > 0 Then
                Dim m_dtLineups As DataTable
                m_dtLineups = e.USFStartingLineup.Copy()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UdcSoccerFormation1_USFResetClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles UdcSoccerFormation1.USFResetClick
        Try
            RefreshFormationDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class