﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class udcSoccerGoal
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picNet = New System.Windows.Forms.PictureBox
        Me.picSoccerGoal = New System.Windows.Forms.PictureBox
        CType(Me.picNet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSoccerGoal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picNet
        '
        Me.picNet.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Net
        Me.picNet.Location = New System.Drawing.Point(94, 22)
        Me.picNet.Margin = New System.Windows.Forms.Padding(0)
        Me.picNet.Name = "picNet"
        Me.picNet.Size = New System.Drawing.Size(100, 50)
        Me.picNet.TabIndex = 1
        Me.picNet.TabStop = False
        '
        'picSoccerGoal
        '
        Me.picSoccerGoal.BackColor = System.Drawing.Color.Lavender
        Me.picSoccerGoal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picSoccerGoal.Location = New System.Drawing.Point(0, 0)
        Me.picSoccerGoal.Name = "picSoccerGoal"
        Me.picSoccerGoal.Size = New System.Drawing.Size(190, 64)
        Me.picSoccerGoal.TabIndex = 0
        Me.picSoccerGoal.TabStop = False
        '
        'udcSoccerGoal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.picNet)
        Me.Controls.Add(Me.picSoccerGoal)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximumSize = New System.Drawing.Size(190, 64)
        Me.MinimumSize = New System.Drawing.Size(190, 64)
        Me.Name = "udcSoccerGoal"
        Me.Size = New System.Drawing.Size(190, 64)
        CType(Me.picNet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSoccerGoal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picSoccerGoal As System.Windows.Forms.PictureBox
    Friend WithEvents picNet As System.Windows.Forms.PictureBox

End Class
