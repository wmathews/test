﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddDemoManager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.btnIgnore = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lblLeagueId = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.CmbLeagueID = New System.Windows.Forms.ComboBox()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.lvwManager = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblTeam = New System.Windows.Forms.Label()
        Me.CmbTeam = New System.Windows.Forms.ComboBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(258, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.BackColor = System.Drawing.Color.Transparent
        Me.lblLastName.Location = New System.Drawing.Point(10, 132)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(60, 15)
        Me.lblLastName.TabIndex = 6
        Me.lblLastName.Text = "Last name:"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.BackColor = System.Drawing.Color.Transparent
        Me.lblFirstName.Location = New System.Drawing.Point(10, 105)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(61, 15)
        Me.lblFirstName.TabIndex = 4
        Me.lblFirstName.Text = "First name:"
        '
        'btnIgnore
        '
        Me.btnIgnore.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnIgnore.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnIgnore.Enabled = False
        Me.btnIgnore.ForeColor = System.Drawing.Color.White
        Me.btnIgnore.Location = New System.Drawing.Point(265, 168)
        Me.btnIgnore.Name = "btnIgnore"
        Me.btnIgnore.Size = New System.Drawing.Size(53, 25)
        Me.btnIgnore.TabIndex = 12
        Me.btnIgnore.Text = "Ignore"
        Me.btnIgnore.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Enabled = False
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(147, 168)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(53, 25)
        Me.btnDelete.TabIndex = 10
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnApply
        '
        Me.btnApply.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnApply.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnApply.ForeColor = System.Drawing.Color.White
        Me.btnApply.Location = New System.Drawing.Point(206, 168)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(53, 25)
        Me.btnApply.TabIndex = 11
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 414)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(345, 21)
        Me.sstMain.TabIndex = 15
        Me.sstMain.Text = "StatusStrip1"
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-167, -3)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(514, 20)
        Me.picTopBar.TabIndex = 294
        Me.picTopBar.TabStop = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.Enabled = False
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(88, 168)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(53, 25)
        Me.btnEdit.TabIndex = 9
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(104, 104)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(226, 22)
        Me.txtFirstName.TabIndex = 5
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(104, 131)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(226, 22)
        Me.txtLastName.TabIndex = 7
        '
        'lblLeagueId
        '
        Me.lblLeagueId.AutoSize = True
        Me.lblLeagueId.BackColor = System.Drawing.Color.Transparent
        Me.lblLeagueId.Location = New System.Drawing.Point(10, 51)
        Me.lblLeagueId.Name = "lblLeagueId"
        Me.lblLeagueId.Size = New System.Drawing.Size(57, 15)
        Me.lblLeagueId.TabIndex = 0
        Me.lblLeagueId.Text = "League ID"
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnAdd.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(29, 168)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(53, 25)
        Me.btnAdd.TabIndex = 8
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'CmbLeagueID
        '
        Me.CmbLeagueID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbLeagueID.FormattingEnabled = True
        Me.CmbLeagueID.Location = New System.Drawing.Point(104, 48)
        Me.CmbLeagueID.Name = "CmbLeagueID"
        Me.CmbLeagueID.Size = New System.Drawing.Size(226, 23)
        Me.CmbLeagueID.TabIndex = 1
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-163, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(514, 28)
        Me.picReporterBar.TabIndex = 295
        Me.picReporterBar.TabStop = False
        '
        'lvwManager
        '
        Me.lvwManager.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvwManager.AutoArrange = False
        Me.lvwManager.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwManager.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvwManager.FullRowSelect = True
        Me.lvwManager.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwManager.HideSelection = False
        Me.lvwManager.Location = New System.Drawing.Point(12, 203)
        Me.lvwManager.MultiSelect = False
        Me.lvwManager.Name = "lvwManager"
        Me.lvwManager.ShowItemToolTips = True
        Me.lvwManager.Size = New System.Drawing.Size(321, 159)
        Me.lvwManager.TabIndex = 13
        Me.lvwManager.UseCompatibleStateImageBehavior = False
        Me.lvwManager.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Manager"
        Me.ColumnHeader1.Width = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Team"
        Me.ColumnHeader2.Width = 150
        '
        'lblTeam
        '
        Me.lblTeam.AutoSize = True
        Me.lblTeam.BackColor = System.Drawing.Color.Transparent
        Me.lblTeam.Location = New System.Drawing.Point(10, 78)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(34, 15)
        Me.lblTeam.TabIndex = 2
        Me.lblTeam.Text = "Team"
        '
        'CmbTeam
        '
        Me.CmbTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbTeam.FormattingEnabled = True
        Me.CmbTeam.Location = New System.Drawing.Point(104, 76)
        Me.CmbTeam.Name = "CmbTeam"
        Me.CmbTeam.Size = New System.Drawing.Size(226, 23)
        Me.CmbTeam.TabIndex = 3
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 370)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(345, 46)
        Me.picButtonBar.TabIndex = 14
        '
        'frmAddDemoManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(345, 435)
        Me.Controls.Add(Me.lblTeam)
        Me.Controls.Add(Me.CmbTeam)
        Me.Controls.Add(Me.lblLastName)
        Me.Controls.Add(Me.lblFirstName)
        Me.Controls.Add(Me.btnIgnore)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.txtFirstName)
        Me.Controls.Add(Me.txtLastName)
        Me.Controls.Add(Me.lblLeagueId)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.CmbLeagueID)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.lvwManager)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "frmAddDemoManager"
        Me.Text = "Add Manager"
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents btnIgnore As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents lblLeagueId As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents CmbLeagueID As System.Windows.Forms.ComboBox
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents lvwManager As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents CmbTeam As System.Windows.Forms.ComboBox
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
