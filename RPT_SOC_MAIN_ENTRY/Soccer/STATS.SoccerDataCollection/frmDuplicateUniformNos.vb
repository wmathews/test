﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmDuplicateUniformNumber
' Author        : Shravani
' Created Date  : 15-04-10
' Description   :

'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region
Public Class frmDuplicateUniformNos

#Region " Constants & Variables "

    Private m_objUtilAudit As New clsAuditLog
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objValidation As New clsValidation
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Dim m_intTeamChecked As Integer
    Dim m_strUniformNumber As String
    Private m_dsRostersInfo As DataSet = Nothing
    Private m_objDuplicateUN As clsDuplicateUniformNos = clsDuplicateUniformNos.GetInstance()
    Private m_dsPlayers As New DataSet
    Private blnCheck As Boolean = False
    Private lsupport As New languagesupport
    Private message1 As String = "Please enter uniform number"
    Private message2 As String = "Player uniform updated successfully"
    Private message3 As String = "Please enter only numbers"
    Private message4 As String = "Uniform Number already Exists..."

#End Region

    Public Sub New(ByVal TeamChecked As Integer, ByVal UniformNumber As String)
        m_intTeamChecked = TeamChecked
        m_strUniformNumber = UniformNumber
        InitializeComponent()
    End Sub

    Private Sub frmDuplicateUniformNos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                message1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message1)
                message2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message2)
                message3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message3)
                message4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message4)
            End If
            lblMsg.Text = "The following player(s) already have the uniform number " & m_strUniformNumber & vbNewLine & "Change the unifrom number of player(s) listed below or click cancel."
            AddDgviewColumns()
            FillGrid()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub AddDgviewColumns()
        Try
            If dgvPlayerUniformNumbers.Columns.Count > 0 Then
                dgvPlayerUniformNumbers.Columns.Clear()
            End If

            Dim ColUN As New DataGridViewTextBoxColumn()
            'set column's DataPropertyName, HeaderText, Name
            ColUN.HeaderText = "Uniform"
            ColUN.Width = 100
            ColUN.ReadOnly = False
            ColUN.MaxInputLength = 3
            ColUN.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvPlayerUniformNumbers.Columns.Add(ColUN)

            ' Set DataGridView Combo Column for playerID field
            Dim ColPlayer As New DataGridViewTextBoxColumn
            'DataGridView Combo ValueMember field has name "playerID"
            ColPlayer.HeaderText = "Player Name"
            ColPlayer.Width = 200
            ColPlayer.ReadOnly = True
            dgvPlayerUniformNumbers.Columns.Add(ColPlayer)

            Dim ColPlayerID As New DataGridViewTextBoxColumn
            'DataGridView Combo ValueMember field has name "playerID"
            ColPlayerID.HeaderText = "PlayerID"
            ColPlayerID.Width = 150
            ColPlayerID.ReadOnly = False
            ColPlayerID.Visible = False
            dgvPlayerUniformNumbers.Columns.Add(ColPlayerID)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            m_dsRostersInfo = m_objDuplicateUN.GetAllRosters(m_objGameDetails.GameCode, m_objGameDetails.languageid)
            Dim drs() As DataRow
            If Not m_dsRostersInfo Is Nothing Then
                m_dsRostersInfo.Tables(0).TableName = "Home"
                m_dsRostersInfo.Tables(1).TableName = "Away"
                If m_intTeamChecked = m_objGameDetails.HomeTeamID Then
                    If (m_strUniformNumber.Trim.Length = 1) Then
                        If (m_strUniformNumber = "0") Then
                            drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                        Else
                            Dim strUniform1 As String = "0" & m_strUniformNumber.Trim()
                            Dim strUniform2 As String = "00" & m_strUniformNumber.Trim
                            drs = m_dsRostersInfo.Tables("Home").Select("(DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "')")
                        End If
                    ElseIf (m_strUniformNumber.Trim.Length = 2) Then
                        If (m_strUniformNumber = "00") Then
                            drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                        Else
                            If (m_strUniformNumber.Substring(0, 1) = "0") Then
                                Dim strUniform1 As String = m_strUniformNumber.Substring(1, 1)
                                Dim strUniform2 As String = "00" & m_strUniformNumber.Substring(1)
                                drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                            Else
                                Dim strUniform2 As String = "0" & m_strUniformNumber
                                drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                            End If
                        End If
                    ElseIf (m_strUniformNumber.Trim.Length = 3) Then
                        If (m_strUniformNumber.Substring(0, 1) = "0") Then
                            If (m_strUniformNumber.Substring(0, 2) = "00") Then
                                drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & m_strUniformNumber.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber.Substring(1) & "' ")
                            Else
                                drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & m_strUniformNumber.Substring(1) & "' ")
                            End If
                        Else
                            drs = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "' ")
                        End If
                    End If
                    'Dim drs() As DataRow = m_dsRostersInfo.Tables("Home").Select("DISPLAY_UNIFORM_NUMBER = '" & m_strUniformNumber.ToString & "'")
                    If drs.Length > 0 Then
                        'dgvPlayerUniformNumbers.Rows.Clear()
                        If drs.Length > 0 Then
                            dgvPlayerUniformNumbers.Rows.Add(drs.Length)
                        End If
                        For i As Integer = 0 To drs.Length - 1
                            dgvPlayerUniformNumbers.Rows(i).Cells(0).Value = drs(i).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                            dgvPlayerUniformNumbers.Rows(i).Cells(1).Value = drs(i).Item("PLAYER").ToString()
                            dgvPlayerUniformNumbers.Rows(i).Cells(2).Value = drs(i).Item("PLAYER_ID").ToString()
                        Next
                        blnCheck = True
                    End If
                ElseIf m_intTeamChecked = m_objGameDetails.AwayTeamID Then

                    If (m_strUniformNumber.Trim.Length = 1) Then
                        If (m_strUniformNumber = "0") Then
                            drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                        Else
                            Dim strUniform1 As String = "0" & m_strUniformNumber.Trim()
                            Dim strUniform2 As String = "00" & m_strUniformNumber.Trim
                            drs = m_dsRostersInfo.Tables("Away").Select("(DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "')")
                        End If
                    ElseIf (m_strUniformNumber.Trim.Length = 2) Then
                        If (m_strUniformNumber = "00") Then
                            drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                        Else
                            If (m_strUniformNumber.Substring(0, 1) = "0") Then
                                Dim strUniform1 As String = m_strUniformNumber.Substring(1, 1)
                                Dim strUniform2 As String = "00" & m_strUniformNumber.Substring(1)
                                drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                            Else
                                Dim strUniform2 As String = "0" & m_strUniformNumber
                                drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                            End If
                        End If
                    ElseIf (m_strUniformNumber.Trim.Length = 3) Then
                        If (m_strUniformNumber.Substring(0, 1) = "0") Then
                            If (m_strUniformNumber.Substring(0, 2) = "00") Then
                                drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & m_strUniformNumber.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber.Substring(1) & "' ")
                            Else
                                drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & m_strUniformNumber.Substring(1) & "' ")
                            End If
                        Else
                            drs = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER= '" & m_strUniformNumber & "' ")
                        End If
                    End If

                    'Dim drs() As DataRow = m_dsRostersInfo.Tables("Away").Select("DISPLAY_UNIFORM_NUMBER = '" & m_strUniformNumber & "'")
                    If drs.Length > 0 Then
                        'dgvPlayerUniformNumbers.Rows.Clear()
                        If drs.Length > 0 Then
                            dgvPlayerUniformNumbers.Rows.Add(drs.Length)
                        End If
                        For i As Integer = 0 To drs.Length - 1
                            dgvPlayerUniformNumbers.Rows(i).Cells(0).Value = drs(i).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                            dgvPlayerUniformNumbers.Rows(i).Cells(1).Value = drs(i).Item("PLAYER").ToString()
                            dgvPlayerUniformNumbers.Rows(i).Cells(2).Value = drs(i).Item("PLAYER_ID").ToString()
                        Next
                        blnCheck = True
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            Dim DRHOME() As DataRow
            Dim DRAWAY() As DataRow
            For i As Integer = 0 To dgvPlayerUniformNumbers.Rows.Count - 1
                If CLng(dgvPlayerUniformNumbers.Rows(i).Cells(2).Value) <> Nothing Then
                    If dgvPlayerUniformNumbers.Rows(i).Cells(0).Value Is Nothing Then
                        MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    ElseIf dgvPlayerUniformNumbers.Rows(i).Cells(0).Value.ToString = "" Then
                        MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Exit Sub
                    End If
                    DRHOME = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID = " & dgvPlayerUniformNumbers.Rows(i).Cells(2).Value.ToString & "")
                    If DRHOME.Length > 0 Then
                        DRHOME(0).BeginEdit()
                        DRHOME(0).Item("DISPLAY_UNIFORM_NUMBER") = IIf(dgvPlayerUniformNumbers.Rows(i).Cells(0).Value Is Nothing, DBNull.Value, dgvPlayerUniformNumbers.Rows(i).Cells(0).Value)
                        DRHOME(0).EndEdit()
                    End If
                    DRAWAY = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID = " & dgvPlayerUniformNumbers.Rows(i).Cells(2).Value.ToString & "")
                    If DRAWAY.Length > 0 Then
                        DRAWAY(0).BeginEdit()
                        DRAWAY(0).Item("DISPLAY_UNIFORM_NUMBER") = IIf(dgvPlayerUniformNumbers.Rows(i).Cells(0).Value Is Nothing, DBNull.Value, dgvPlayerUniformNumbers.Rows(i).Cells(0).Value)
                        DRAWAY(0).EndEdit()
                    End If
                End If
            Next

            'XML CONVERTION AND UPDATION INTO DB
            Dim strXmlPlayerData As String = ""
            'CONVERTING INTO XML DATA
            m_dsRostersInfo.Tables(0).Merge(m_dsRostersInfo.Tables(1))
            m_dsRostersInfo.Tables.RemoveAt(1)
            m_dsRostersInfo.DataSetName = "PlayerpData"
            strXmlPlayerData = m_dsRostersInfo.GetXml()
            strXmlPlayerData = strXmlPlayerData.Replace("'", "''")

            'UPDATING THE LOCAL SQL
            m_objDuplicateUN.UpdateRosters(m_objGameDetails.GameCode, strXmlPlayerData)
            If m_strUniformNumber <> dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value.ToString Then
                MessageDialog.Show(message2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
            Me.Close()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub dgvPlayerUniformNumbers_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPlayerUniformNumbers.CellValueChanged
        Try
            Dim chkRes As Boolean = True
            If blnCheck = True Then
                If Not dgvPlayerUniformNumbers.CurrentCell Is Nothing Then
                    If dgvPlayerUniformNumbers.CurrentCell.ColumnIndex = 0 Then
                        'updates the starting position's selected
                        If dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value IsNot Nothing And dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value IsNot Nothing Then
                            If dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value.ToString <> "" Then
                                chkRes = clsValidation.ValidateNumeric(dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value.ToString)
                                If chkRes = False Then
                                    MessageDialog.Show(message3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value = ""
                                    Exit Sub
                                End If
                                If ValidateUniformNumber() = False Then
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                End If
                dgvPlayerUniformNumbers.AllowUserToAddRows = False
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function ValidateUniformNumber() As Boolean
        Try
            Dim drs() As DataRow = Nothing

            If dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value IsNot Nothing And dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value IsNot Nothing Then
                Dim StrUniform As String = dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value.ToString
                If (Not StrUniform Is Nothing) Then
                    If StrUniform <> "" Then
                        If m_intTeamChecked = m_objGameDetails.HomeTeamID Then
                            If (StrUniform.Trim.Length = 1) Then
                                If (StrUniform = "0") Then
                                    drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    Dim strUniform1 As String = "0" & StrUniform.Trim()
                                    Dim strUniform2 As String = "00" & StrUniform.Trim
                                    drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND (DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "')")
                                End If
                            ElseIf (StrUniform.Trim.Length = 2) Then
                                If (StrUniform = "00") Then
                                    drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    If (StrUniform.Substring(0, 1) = "0") Then
                                        Dim strUniform1 As String = StrUniform.Substring(1, 1)
                                        Dim strUniform2 As String = "00" & StrUniform.Substring(1)
                                        drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    Else
                                        Dim strUniform2 As String = "0" & StrUniform
                                        drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    End If
                                End If
                            ElseIf (StrUniform.Trim.Length = 3) Then
                                If (StrUniform.Substring(0, 1) = "0") Then
                                    If (StrUniform.Substring(0, 2) = "00") Then
                                        drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & StrUniform.Substring(1) & "' ")
                                    Else
                                        drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(1) & "' ")
                                    End If
                                Else
                                    drs = m_dsRostersInfo.Tables("Home").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' ")
                                End If

                            End If
                        Else
                            If (StrUniform.Trim.Length = 1) Then
                                If (StrUniform = "0") Then
                                    drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    Dim strUniform1 As String = "0" & StrUniform.Trim()
                                    Dim strUniform2 As String = "00" & StrUniform.Trim
                                    drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND (DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' " & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "')")
                                End If
                            ElseIf (StrUniform.Trim.Length = 2) Then
                                If (StrUniform = "00") Then
                                    drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER=0 Or DISPLAY_UNIFORM_NUMBER=00 ")
                                Else
                                    If (StrUniform.Substring(0, 1) = "0") Then
                                        Dim strUniform1 As String = StrUniform.Substring(1, 1)
                                        Dim strUniform2 As String = "00" & StrUniform.Substring(1)
                                        drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & strUniform1 & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    Else
                                        Dim strUniform2 As String = "0" & StrUniform
                                        drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & strUniform2 & "' ")
                                    End If
                                End If
                            ElseIf (StrUniform.Trim.Length = 3) Then
                                If (StrUniform.Substring(0, 1) = "0") Then
                                    If (StrUniform.Substring(0, 2) = "00") Then
                                        drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(2) & "'" & "OR DISPLAY_UNIFORM_NUMBER= '" & StrUniform.Substring(1) & "' ")
                                    Else
                                        drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "'" & "Or DISPLAY_UNIFORM_NUMBER='" & StrUniform.Substring(1) & "' ")
                                    End If
                                Else
                                    drs = m_dsRostersInfo.Tables("Away").Select("PLAYER_ID <> '" & CStr(dgvPlayerUniformNumbers.CurrentRow.Cells(2).Value) & "' AND DISPLAY_UNIFORM_NUMBER= '" & StrUniform & "' ")
                                End If
                            End If
                        End If
                        If (drs.Length >= 1) Then
                            MessageDialog.Show(message4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            dgvPlayerUniformNumbers.CurrentRow.Cells(0).Value = ""
                            Return False
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class