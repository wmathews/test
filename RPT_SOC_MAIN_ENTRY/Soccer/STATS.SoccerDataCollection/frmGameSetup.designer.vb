﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGameSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGameSetup))
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.lblOfficials = New System.Windows.Forms.Label()
        Me.pnlOfficial = New System.Windows.Forms.Panel()
        Me.txtRating6 = New System.Windows.Forms.TextBox()
        Me.txtRating5 = New System.Windows.Forms.TextBox()
        Me.lbl6thOfficial = New System.Windows.Forms.Label()
        Me.cmb6thOfficial = New System.Windows.Forms.ComboBox()
        Me.lblRating = New System.Windows.Forms.Label()
        Me.txtRating3 = New System.Windows.Forms.TextBox()
        Me.txtRating2 = New System.Windows.Forms.TextBox()
        Me.txtRating1 = New System.Windows.Forms.TextBox()
        Me.cmb5thOfficial = New System.Windows.Forms.ComboBox()
        Me.lbl5thOfficial = New System.Windows.Forms.Label()
        Me.cmbLinesman = New System.Windows.Forms.ComboBox()
        Me.lblLinesman = New System.Windows.Forms.Label()
        Me.CmbLinesman1 = New System.Windows.Forms.ComboBox()
        Me.lblLinesman1 = New System.Windows.Forms.Label()
        Me.cmb4thOfficial = New System.Windows.Forms.ComboBox()
        Me.lbl4thOfficial = New System.Windows.Forms.Label()
        Me.cmbReferee = New System.Windows.Forms.ComboBox()
        Me.lblReferee = New System.Windows.Forms.Label()
        Me.pnlMatchInformation = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblHTeamName = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbhomestartET = New System.Windows.Forms.ComboBox()
        Me.cmbCorF = New System.Windows.Forms.ComboBox()
        Me.txtTemp = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbWeather = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMatchRating = New System.Windows.Forms.TextBox()
        Me.lblMatchRating = New System.Windows.Forms.Label()
        Me.mtxtDelay = New System.Windows.Forms.MaskedTextBox()
        Me.txtAttendance = New System.Windows.Forms.TextBox()
        Me.txtKickoff = New System.Windows.Forms.TextBox()
        Me.cmbHomeStart = New System.Windows.Forms.ComboBox()
        Me.lblHomeStart = New System.Windows.Forms.Label()
        Me.cmbVenue = New System.Windows.Forms.ComboBox()
        Me.lblVenue = New System.Windows.Forms.Label()
        Me.lblAttendance = New System.Windows.Forms.Label()
        Me.lblDelay = New System.Windows.Forms.Label()
        Me.lblKickOff = New System.Windows.Forms.Label()
        Me.lblMatchInformation = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblHomeTeam = New System.Windows.Forms.Label()
        Me.lblAwayTeam = New System.Windows.Forms.Label()
        Me.picHomeLogo = New System.Windows.Forms.PictureBox()
        Me.picAwayLogo = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAddOfficial = New System.Windows.Forms.Button()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.lblhomestartET = New System.Windows.Forms.Label()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOfficial.SuspendLayout()
        Me.pnlMatchInformation.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = CType(resources.GetObject("picReporterBar.BackgroundImage"), System.Drawing.Image)
        Me.picReporterBar.Location = New System.Drawing.Point(-1, 10)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(698, 18)
        Me.picReporterBar.TabIndex = 239
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = CType(resources.GetObject("picTopBar.BackgroundImage"), System.Drawing.Image)
        Me.picTopBar.Location = New System.Drawing.Point(-1, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(698, 13)
        Me.picTopBar.TabIndex = 238
        Me.picTopBar.TabStop = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = CType(resources.GetObject("sstMain.BackgroundImage"), System.Drawing.Image)
        Me.sstMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 337)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(637, 14)
        Me.sstMain.TabIndex = 7
        Me.sstMain.Text = "StatusStrip1"
        '
        'lblOfficials
        '
        Me.lblOfficials.AutoSize = True
        Me.lblOfficials.Location = New System.Drawing.Point(12, 75)
        Me.lblOfficials.Name = "lblOfficials"
        Me.lblOfficials.Size = New System.Drawing.Size(47, 13)
        Me.lblOfficials.TabIndex = 1
        Me.lblOfficials.Text = "Officials:"
        '
        'pnlOfficial
        '
        Me.pnlOfficial.BackColor = System.Drawing.Color.Beige
        Me.pnlOfficial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOfficial.Controls.Add(Me.txtRating6)
        Me.pnlOfficial.Controls.Add(Me.txtRating5)
        Me.pnlOfficial.Controls.Add(Me.lbl6thOfficial)
        Me.pnlOfficial.Controls.Add(Me.cmb6thOfficial)
        Me.pnlOfficial.Controls.Add(Me.lblRating)
        Me.pnlOfficial.Controls.Add(Me.txtRating3)
        Me.pnlOfficial.Controls.Add(Me.txtRating2)
        Me.pnlOfficial.Controls.Add(Me.txtRating1)
        Me.pnlOfficial.Controls.Add(Me.cmb5thOfficial)
        Me.pnlOfficial.Controls.Add(Me.lbl5thOfficial)
        Me.pnlOfficial.Controls.Add(Me.cmbLinesman)
        Me.pnlOfficial.Controls.Add(Me.lblLinesman)
        Me.pnlOfficial.Controls.Add(Me.CmbLinesman1)
        Me.pnlOfficial.Controls.Add(Me.lblLinesman1)
        Me.pnlOfficial.Controls.Add(Me.cmb4thOfficial)
        Me.pnlOfficial.Controls.Add(Me.lbl4thOfficial)
        Me.pnlOfficial.Controls.Add(Me.cmbReferee)
        Me.pnlOfficial.Controls.Add(Me.lblReferee)
        Me.pnlOfficial.Location = New System.Drawing.Point(15, 90)
        Me.pnlOfficial.Name = "pnlOfficial"
        Me.pnlOfficial.Size = New System.Drawing.Size(297, 173)
        Me.pnlOfficial.TabIndex = 2
        '
        'txtRating6
        '
        Me.txtRating6.Location = New System.Drawing.Point(240, 143)
        Me.txtRating6.MaxLength = 2
        Me.txtRating6.Name = "txtRating6"
        Me.txtRating6.Size = New System.Drawing.Size(49, 20)
        Me.txtRating6.TabIndex = 17
        '
        'txtRating5
        '
        Me.txtRating5.Location = New System.Drawing.Point(240, 118)
        Me.txtRating5.MaxLength = 2
        Me.txtRating5.Name = "txtRating5"
        Me.txtRating5.Size = New System.Drawing.Size(49, 20)
        Me.txtRating5.TabIndex = 16
        '
        'lbl6thOfficial
        '
        Me.lbl6thOfficial.AutoSize = True
        Me.lbl6thOfficial.Location = New System.Drawing.Point(12, 145)
        Me.lbl6thOfficial.Name = "lbl6thOfficial"
        Me.lbl6thOfficial.Size = New System.Drawing.Size(60, 13)
        Me.lbl6thOfficial.TabIndex = 15
        Me.lbl6thOfficial.Text = "6th Official:"
        '
        'cmb6thOfficial
        '
        Me.cmb6thOfficial.BackColor = System.Drawing.Color.White
        Me.cmb6thOfficial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb6thOfficial.FormattingEnabled = True
        Me.cmb6thOfficial.Location = New System.Drawing.Point(77, 142)
        Me.cmb6thOfficial.Name = "cmb6thOfficial"
        Me.cmb6thOfficial.Size = New System.Drawing.Size(160, 21)
        Me.cmb6thOfficial.TabIndex = 14
        '
        'lblRating
        '
        Me.lblRating.AutoSize = True
        Me.lblRating.Location = New System.Drawing.Point(240, 0)
        Me.lblRating.Name = "lblRating"
        Me.lblRating.Size = New System.Drawing.Size(38, 13)
        Me.lblRating.TabIndex = 13
        Me.lblRating.Text = "Rating"
        '
        'txtRating3
        '
        Me.txtRating3.Location = New System.Drawing.Point(240, 66)
        Me.txtRating3.MaxLength = 2
        Me.txtRating3.Name = "txtRating3"
        Me.txtRating3.Size = New System.Drawing.Size(49, 20)
        Me.txtRating3.TabIndex = 12
        '
        'txtRating2
        '
        Me.txtRating2.Location = New System.Drawing.Point(240, 42)
        Me.txtRating2.MaxLength = 2
        Me.txtRating2.Name = "txtRating2"
        Me.txtRating2.Size = New System.Drawing.Size(49, 20)
        Me.txtRating2.TabIndex = 11
        '
        'txtRating1
        '
        Me.txtRating1.Location = New System.Drawing.Point(240, 17)
        Me.txtRating1.MaxLength = 2
        Me.txtRating1.Name = "txtRating1"
        Me.txtRating1.Size = New System.Drawing.Size(49, 20)
        Me.txtRating1.TabIndex = 10
        '
        'cmb5thOfficial
        '
        Me.cmb5thOfficial.BackColor = System.Drawing.Color.White
        Me.cmb5thOfficial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb5thOfficial.FormattingEnabled = True
        Me.cmb5thOfficial.Location = New System.Drawing.Point(77, 117)
        Me.cmb5thOfficial.Name = "cmb5thOfficial"
        Me.cmb5thOfficial.Size = New System.Drawing.Size(160, 21)
        Me.cmb5thOfficial.TabIndex = 9
        '
        'lbl5thOfficial
        '
        Me.lbl5thOfficial.AutoSize = True
        Me.lbl5thOfficial.Location = New System.Drawing.Point(12, 120)
        Me.lbl5thOfficial.Name = "lbl5thOfficial"
        Me.lbl5thOfficial.Size = New System.Drawing.Size(60, 13)
        Me.lbl5thOfficial.TabIndex = 8
        Me.lbl5thOfficial.Text = "5th Official:"
        '
        'cmbLinesman
        '
        Me.cmbLinesman.BackColor = System.Drawing.Color.White
        Me.cmbLinesman.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLinesman.FormattingEnabled = True
        Me.cmbLinesman.Location = New System.Drawing.Point(77, 42)
        Me.cmbLinesman.Name = "cmbLinesman"
        Me.cmbLinesman.Size = New System.Drawing.Size(160, 21)
        Me.cmbLinesman.TabIndex = 3
        '
        'lblLinesman
        '
        Me.lblLinesman.AutoSize = True
        Me.lblLinesman.Location = New System.Drawing.Point(12, 46)
        Me.lblLinesman.Name = "lblLinesman"
        Me.lblLinesman.Size = New System.Drawing.Size(55, 13)
        Me.lblLinesman.TabIndex = 2
        Me.lblLinesman.Text = "Linesman:"
        '
        'CmbLinesman1
        '
        Me.CmbLinesman1.BackColor = System.Drawing.Color.White
        Me.CmbLinesman1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbLinesman1.FormattingEnabled = True
        Me.CmbLinesman1.Location = New System.Drawing.Point(77, 67)
        Me.CmbLinesman1.Name = "CmbLinesman1"
        Me.CmbLinesman1.Size = New System.Drawing.Size(160, 21)
        Me.CmbLinesman1.TabIndex = 5
        '
        'lblLinesman1
        '
        Me.lblLinesman1.AutoSize = True
        Me.lblLinesman1.Location = New System.Drawing.Point(12, 70)
        Me.lblLinesman1.Name = "lblLinesman1"
        Me.lblLinesman1.Size = New System.Drawing.Size(55, 13)
        Me.lblLinesman1.TabIndex = 4
        Me.lblLinesman1.Text = "Linesman:"
        '
        'cmb4thOfficial
        '
        Me.cmb4thOfficial.BackColor = System.Drawing.Color.White
        Me.cmb4thOfficial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb4thOfficial.FormattingEnabled = True
        Me.cmb4thOfficial.Location = New System.Drawing.Point(77, 92)
        Me.cmb4thOfficial.Name = "cmb4thOfficial"
        Me.cmb4thOfficial.Size = New System.Drawing.Size(160, 21)
        Me.cmb4thOfficial.TabIndex = 7
        '
        'lbl4thOfficial
        '
        Me.lbl4thOfficial.AutoSize = True
        Me.lbl4thOfficial.Location = New System.Drawing.Point(12, 95)
        Me.lbl4thOfficial.Name = "lbl4thOfficial"
        Me.lbl4thOfficial.Size = New System.Drawing.Size(60, 13)
        Me.lbl4thOfficial.TabIndex = 6
        Me.lbl4thOfficial.Text = "4th Official:"
        '
        'cmbReferee
        '
        Me.cmbReferee.BackColor = System.Drawing.Color.White
        Me.cmbReferee.FormattingEnabled = True
        Me.cmbReferee.Location = New System.Drawing.Point(77, 16)
        Me.cmbReferee.Name = "cmbReferee"
        Me.cmbReferee.Size = New System.Drawing.Size(160, 21)
        Me.cmbReferee.TabIndex = 1
        '
        'lblReferee
        '
        Me.lblReferee.AutoSize = True
        Me.lblReferee.Location = New System.Drawing.Point(12, 21)
        Me.lblReferee.Name = "lblReferee"
        Me.lblReferee.Size = New System.Drawing.Size(48, 13)
        Me.lblReferee.TabIndex = 0
        Me.lblReferee.Text = "Referee:"
        '
        'pnlMatchInformation
        '
        Me.pnlMatchInformation.BackColor = System.Drawing.Color.Beige
        Me.pnlMatchInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMatchInformation.Controls.Add(Me.Label5)
        Me.pnlMatchInformation.Controls.Add(Me.lblHTeamName)
        Me.pnlMatchInformation.Controls.Add(Me.Label4)
        Me.pnlMatchInformation.Controls.Add(Me.Label3)
        Me.pnlMatchInformation.Controls.Add(Me.cmbhomestartET)
        Me.pnlMatchInformation.Controls.Add(Me.cmbCorF)
        Me.pnlMatchInformation.Controls.Add(Me.txtTemp)
        Me.pnlMatchInformation.Controls.Add(Me.Label2)
        Me.pnlMatchInformation.Controls.Add(Me.cmbWeather)
        Me.pnlMatchInformation.Controls.Add(Me.Label1)
        Me.pnlMatchInformation.Controls.Add(Me.txtMatchRating)
        Me.pnlMatchInformation.Controls.Add(Me.lblMatchRating)
        Me.pnlMatchInformation.Controls.Add(Me.mtxtDelay)
        Me.pnlMatchInformation.Controls.Add(Me.txtAttendance)
        Me.pnlMatchInformation.Controls.Add(Me.txtKickoff)
        Me.pnlMatchInformation.Controls.Add(Me.cmbHomeStart)
        Me.pnlMatchInformation.Controls.Add(Me.lblHomeStart)
        Me.pnlMatchInformation.Controls.Add(Me.cmbVenue)
        Me.pnlMatchInformation.Controls.Add(Me.lblVenue)
        Me.pnlMatchInformation.Controls.Add(Me.lblAttendance)
        Me.pnlMatchInformation.Controls.Add(Me.lblDelay)
        Me.pnlMatchInformation.Controls.Add(Me.lblKickOff)
        Me.pnlMatchInformation.Location = New System.Drawing.Point(327, 90)
        Me.pnlMatchInformation.Name = "pnlMatchInformation"
        Me.pnlMatchInformation.Size = New System.Drawing.Size(297, 200)
        Me.pnlMatchInformation.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 180)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "defends the"
        '
        'lblHTeamName
        '
        Me.lblHTeamName.AutoSize = True
        Me.lblHTeamName.BackColor = System.Drawing.Color.Chartreuse
        Me.lblHTeamName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHTeamName.Location = New System.Drawing.Point(132, 159)
        Me.lblHTeamName.Name = "lblHTeamName"
        Me.lblHTeamName.Size = New System.Drawing.Size(0, 13)
        Me.lblHTeamName.TabIndex = 25
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(155, 180)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "goal"
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Location = New System.Drawing.Point(0, 147)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(297, 1)
        Me.Label3.TabIndex = 23
        '
        'cmbhomestartET
        '
        Me.cmbhomestartET.BackColor = System.Drawing.Color.White
        Me.cmbhomestartET.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbhomestartET.DropDownWidth = 226
        Me.cmbhomestartET.Enabled = False
        Me.cmbhomestartET.FormattingEnabled = True
        Me.cmbhomestartET.Location = New System.Drawing.Point(88, 175)
        Me.cmbhomestartET.Name = "cmbhomestartET"
        Me.cmbhomestartET.Size = New System.Drawing.Size(64, 21)
        Me.cmbhomestartET.TabIndex = 22
        Me.cmbhomestartET.Visible = False
        '
        'cmbCorF
        '
        Me.cmbCorF.BackColor = System.Drawing.Color.White
        Me.cmbCorF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCorF.DropDownWidth = 30
        Me.cmbCorF.FormattingEnabled = True
        Me.cmbCorF.Items.AddRange(New Object() {"F", "C"})
        Me.cmbCorF.Location = New System.Drawing.Point(255, 66)
        Me.cmbCorF.Name = "cmbCorF"
        Me.cmbCorF.Size = New System.Drawing.Size(31, 21)
        Me.cmbCorF.TabIndex = 20
        '
        'txtTemp
        '
        Me.txtTemp.Location = New System.Drawing.Point(223, 67)
        Me.txtTemp.MaxLength = 3
        Me.txtTemp.Name = "txtTemp"
        Me.txtTemp.Size = New System.Drawing.Size(33, 20)
        Me.txtTemp.TabIndex = 19
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(184, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Temp:"
        '
        'cmbWeather
        '
        Me.cmbWeather.BackColor = System.Drawing.Color.White
        Me.cmbWeather.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWeather.FormattingEnabled = True
        Me.cmbWeather.Location = New System.Drawing.Point(81, 67)
        Me.cmbWeather.Name = "cmbWeather"
        Me.cmbWeather.Size = New System.Drawing.Size(100, 21)
        Me.cmbWeather.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Weather:"
        '
        'txtMatchRating
        '
        Me.txtMatchRating.Location = New System.Drawing.Point(235, 93)
        Me.txtMatchRating.MaxLength = 2
        Me.txtMatchRating.Name = "txtMatchRating"
        Me.txtMatchRating.Size = New System.Drawing.Size(51, 20)
        Me.txtMatchRating.TabIndex = 15
        '
        'lblMatchRating
        '
        Me.lblMatchRating.AutoSize = True
        Me.lblMatchRating.Location = New System.Drawing.Point(184, 95)
        Me.lblMatchRating.Name = "lblMatchRating"
        Me.lblMatchRating.Size = New System.Drawing.Size(41, 13)
        Me.lblMatchRating.TabIndex = 14
        Me.lblMatchRating.Text = "Rating:"
        '
        'mtxtDelay
        '
        Me.mtxtDelay.Location = New System.Drawing.Point(81, 92)
        Me.mtxtDelay.Mask = "00:00"
        Me.mtxtDelay.Name = "mtxtDelay"
        Me.mtxtDelay.Size = New System.Drawing.Size(62, 20)
        Me.mtxtDelay.TabIndex = 7
        Me.mtxtDelay.Text = "0000"
        Me.mtxtDelay.ValidatingType = GetType(Date)
        '
        'txtAttendance
        '
        Me.txtAttendance.Location = New System.Drawing.Point(81, 118)
        Me.txtAttendance.MaxLength = 6
        Me.txtAttendance.Name = "txtAttendance"
        Me.txtAttendance.Size = New System.Drawing.Size(62, 20)
        Me.txtAttendance.TabIndex = 5
        '
        'txtKickoff
        '
        Me.txtKickoff.Location = New System.Drawing.Point(81, 42)
        Me.txtKickoff.Name = "txtKickoff"
        Me.txtKickoff.ReadOnly = True
        Me.txtKickoff.Size = New System.Drawing.Size(205, 20)
        Me.txtKickoff.TabIndex = 3
        '
        'cmbHomeStart
        '
        Me.cmbHomeStart.BackColor = System.Drawing.Color.White
        Me.cmbHomeStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbHomeStart.DropDownWidth = 226
        Me.cmbHomeStart.FormattingEnabled = True
        Me.cmbHomeStart.Location = New System.Drawing.Point(87, 175)
        Me.cmbHomeStart.Name = "cmbHomeStart"
        Me.cmbHomeStart.Size = New System.Drawing.Size(64, 21)
        Me.cmbHomeStart.TabIndex = 9
        '
        'lblHomeStart
        '
        Me.lblHomeStart.AutoSize = True
        Me.lblHomeStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeStart.Location = New System.Drawing.Point(12, 159)
        Me.lblHomeStart.Name = "lblHomeStart"
        Me.lblHomeStart.Size = New System.Drawing.Size(115, 13)
        Me.lblHomeStart.TabIndex = 8
        Me.lblHomeStart.Text = "To start the match,"
        '
        'cmbVenue
        '
        Me.cmbVenue.BackColor = System.Drawing.Color.White
        Me.cmbVenue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVenue.FormattingEnabled = True
        Me.cmbVenue.Location = New System.Drawing.Point(81, 16)
        Me.cmbVenue.Name = "cmbVenue"
        Me.cmbVenue.Size = New System.Drawing.Size(205, 21)
        Me.cmbVenue.TabIndex = 1
        '
        'lblVenue
        '
        Me.lblVenue.AutoSize = True
        Me.lblVenue.Location = New System.Drawing.Point(12, 21)
        Me.lblVenue.Name = "lblVenue"
        Me.lblVenue.Size = New System.Drawing.Size(41, 13)
        Me.lblVenue.TabIndex = 0
        Me.lblVenue.Text = "Venue:"
        '
        'lblAttendance
        '
        Me.lblAttendance.AutoSize = True
        Me.lblAttendance.Location = New System.Drawing.Point(12, 120)
        Me.lblAttendance.Name = "lblAttendance"
        Me.lblAttendance.Size = New System.Drawing.Size(65, 13)
        Me.lblAttendance.TabIndex = 4
        Me.lblAttendance.Text = "Attendance:"
        '
        'lblDelay
        '
        Me.lblDelay.AutoSize = True
        Me.lblDelay.Location = New System.Drawing.Point(12, 95)
        Me.lblDelay.Name = "lblDelay"
        Me.lblDelay.Size = New System.Drawing.Size(37, 13)
        Me.lblDelay.TabIndex = 6
        Me.lblDelay.Text = "Delay:"
        '
        'lblKickOff
        '
        Me.lblKickOff.AutoSize = True
        Me.lblKickOff.Location = New System.Drawing.Point(12, 46)
        Me.lblKickOff.Name = "lblKickOff"
        Me.lblKickOff.Size = New System.Drawing.Size(43, 13)
        Me.lblKickOff.TabIndex = 2
        Me.lblKickOff.Text = "Kickoff:"
        '
        'lblMatchInformation
        '
        Me.lblMatchInformation.AutoSize = True
        Me.lblMatchInformation.Location = New System.Drawing.Point(324, 75)
        Me.lblMatchInformation.Name = "lblMatchInformation"
        Me.lblMatchInformation.Size = New System.Drawing.Size(94, 13)
        Me.lblMatchInformation.TabIndex = 3
        Me.lblMatchInformation.Text = "Match information:"
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(469, 8)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 22)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(550, 8)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 22)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblHomeTeam)
        Me.Panel3.Controls.Add(Me.lblAwayTeam)
        Me.Panel3.Controls.Add(Me.picHomeLogo)
        Me.Panel3.Controls.Add(Me.picAwayLogo)
        Me.Panel3.Controls.Add(Me.Panel1)
        Me.Panel3.Location = New System.Drawing.Point(-24, 28)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(676, 44)
        Me.Panel3.TabIndex = 0
        '
        'lblHomeTeam
        '
        Me.lblHomeTeam.AutoSize = True
        Me.lblHomeTeam.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblHomeTeam.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHomeTeam.ForeColor = System.Drawing.Color.White
        Me.lblHomeTeam.Location = New System.Drawing.Point(125, 11)
        Me.lblHomeTeam.Name = "lblHomeTeam"
        Me.lblHomeTeam.Size = New System.Drawing.Size(140, 24)
        Me.lblHomeTeam.TabIndex = 0
        Me.lblHomeTeam.Text = "Home team Vs."
        '
        'lblAwayTeam
        '
        Me.lblAwayTeam.AutoSize = True
        Me.lblAwayTeam.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblAwayTeam.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAwayTeam.ForeColor = System.Drawing.Color.White
        Me.lblAwayTeam.Location = New System.Drawing.Point(429, 11)
        Me.lblAwayTeam.Name = "lblAwayTeam"
        Me.lblAwayTeam.Size = New System.Drawing.Size(107, 24)
        Me.lblAwayTeam.TabIndex = 1
        Me.lblAwayTeam.Text = "Away team "
        '
        'picHomeLogo
        '
        Me.picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picHomeLogo.Location = New System.Drawing.Point(42, 1)
        Me.picHomeLogo.Name = "picHomeLogo"
        Me.picHomeLogo.Size = New System.Drawing.Size(40, 40)
        Me.picHomeLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picHomeLogo.TabIndex = 279
        Me.picHomeLogo.TabStop = False
        '
        'picAwayLogo
        '
        Me.picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picAwayLogo.Location = New System.Drawing.Point(596, 1)
        Me.picAwayLogo.Name = "picAwayLogo"
        Me.picAwayLogo.Size = New System.Drawing.Size(40, 40)
        Me.picAwayLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAwayLogo.TabIndex = 278
        Me.picAwayLogo.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Panel1.Location = New System.Drawing.Point(106, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(470, 42)
        Me.Panel1.TabIndex = 280
        '
        'btnAddOfficial
        '
        Me.btnAddOfficial.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnAddOfficial.BackgroundImage = CType(resources.GetObject("btnAddOfficial.BackgroundImage"), System.Drawing.Image)
        Me.btnAddOfficial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddOfficial.ForeColor = System.Drawing.Color.White
        Me.btnAddOfficial.Location = New System.Drawing.Point(16, 8)
        Me.btnAddOfficial.Name = "btnAddOfficial"
        Me.btnAddOfficial.Size = New System.Drawing.Size(94, 22)
        Me.btnAddOfficial.TabIndex = 284
        Me.btnAddOfficial.Text = "Add Officials ..."
        Me.btnAddOfficial.UseVisualStyleBackColor = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = CType(resources.GetObject("picButtonBar.BackgroundImage"), System.Drawing.Image)
        Me.picButtonBar.Controls.Add(Me.btnAddOfficial)
        Me.picButtonBar.Controls.Add(Me.btnCancel)
        Me.picButtonBar.Controls.Add(Me.btnSave)
        Me.picButtonBar.Location = New System.Drawing.Point(-1, 300)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(645, 52)
        Me.picButtonBar.TabIndex = 285
        '
        'lblhomestartET
        '
        Me.lblhomestartET.AutoSize = True
        Me.lblhomestartET.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblhomestartET.Location = New System.Drawing.Point(339, 252)
        Me.lblhomestartET.Name = "lblhomestartET"
        Me.lblhomestartET.Size = New System.Drawing.Size(119, 13)
        Me.lblhomestartET.TabIndex = 286
        Me.lblhomestartET.Text = "To start Extra time, "
        '
        'frmGameSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(637, 351)
        Me.Controls.Add(Me.lblhomestartET)
        Me.Controls.Add(Me.pnlMatchInformation)
        Me.Controls.Add(Me.lblMatchInformation)
        Me.Controls.Add(Me.pnlOfficial)
        Me.Controls.Add(Me.lblOfficials)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmGameSetup"
        Me.Text = "Soccer Data Collection - Game Setup"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOfficial.ResumeLayout(False)
        Me.pnlOfficial.PerformLayout()
        Me.pnlMatchInformation.ResumeLayout(False)
        Me.pnlMatchInformation.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents lblOfficials As System.Windows.Forms.Label
    Friend WithEvents pnlOfficial As System.Windows.Forms.Panel
    Friend WithEvents cmbReferee As System.Windows.Forms.ComboBox
    Friend WithEvents lblReferee As System.Windows.Forms.Label
    Friend WithEvents cmb5thOfficial As System.Windows.Forms.ComboBox
    Friend WithEvents lbl5thOfficial As System.Windows.Forms.Label
    Friend WithEvents cmbLinesman As System.Windows.Forms.ComboBox
    Friend WithEvents lblLinesman As System.Windows.Forms.Label
    Friend WithEvents CmbLinesman1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLinesman1 As System.Windows.Forms.Label
    Friend WithEvents cmb4thOfficial As System.Windows.Forms.ComboBox
    Friend WithEvents lbl4thOfficial As System.Windows.Forms.Label
    Friend WithEvents pnlMatchInformation As System.Windows.Forms.Panel
    Friend WithEvents cmbHomeStart As System.Windows.Forms.ComboBox
    Friend WithEvents lblHomeStart As System.Windows.Forms.Label
    Friend WithEvents lblVenue As System.Windows.Forms.Label
    Friend WithEvents lblAttendance As System.Windows.Forms.Label
    Friend WithEvents lblDelay As System.Windows.Forms.Label
    Friend WithEvents lblKickOff As System.Windows.Forms.Label
    Friend WithEvents lblMatchInformation As System.Windows.Forms.Label
    Friend WithEvents txtKickoff As System.Windows.Forms.TextBox
    Friend WithEvents txtAttendance As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblHomeTeam As System.Windows.Forms.Label
    Friend WithEvents lblAwayTeam As System.Windows.Forms.Label
    Friend WithEvents picHomeLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picAwayLogo As System.Windows.Forms.PictureBox
    Friend WithEvents mtxtDelay As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnAddOfficial As System.Windows.Forms.Button
    Friend WithEvents cmbVenue As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents txtRating3 As System.Windows.Forms.TextBox
    Friend WithEvents txtRating2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRating1 As System.Windows.Forms.TextBox
    Friend WithEvents lblRating As System.Windows.Forms.Label
    Friend WithEvents lblMatchRating As System.Windows.Forms.Label
    Friend WithEvents txtMatchRating As System.Windows.Forms.TextBox
    Friend WithEvents cmbWeather As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTemp As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbCorF As System.Windows.Forms.ComboBox
    Friend WithEvents lbl6thOfficial As System.Windows.Forms.Label
    Friend WithEvents cmb6thOfficial As System.Windows.Forms.ComboBox
    Friend WithEvents cmbhomestartET As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblHTeamName As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblhomestartET As System.Windows.Forms.Label
    Friend WithEvents txtRating6 As System.Windows.Forms.TextBox
    Friend WithEvents txtRating5 As System.Windows.Forms.TextBox
End Class
