﻿Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text
Imports System.Threading

Public Class frmSportVU
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objSportVU As clsSportVU = clsSportVU.GetInstance()
    Private m_objUtility As New clsUtility
    Private MessageDialog As New frmMessageDialog
    Private m_dsEventData As New DataSet
    Private thrSportVUData As Threading.Thread = New Threading.Thread(AddressOf BindListView)
    Public Delegate Sub SetListViewDelegate(ByVal ds As DataSet)
    Private lsupport As New languagesupport
    Dim strmessage As String = "Currently there is no SprortVUData"

    Private Sub frmSportVU_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            CreateSportVuThread()
            tmrRefreshData.Start()
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "TimeElapsed")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "EventID")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "TeamAbbrev")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "PlayerName")
                lvwSportVU.Columns(0).Text = g4
                lvwSportVU.Columns(1).Text = g5
                lvwSportVU.Columns(2).Text = g6
                lvwSportVU.Columns(3).Text = g7
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub BindListView(ByVal Value As DataSet)
        Try
            If lvwSportVU.InvokeRequired = True Then
                lvwSportVU.Invoke(New SetListViewDelegate(AddressOf BindData), Value)
            Else
                BindData()
            End If
            'Thread.Sleep(10000)
            'CreateSportVuThread()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CreateSportVuThread()
        Try
            If m_objGameDetails.IsSportVUAvailable = True Then
                'CURRENTLY WE ARE GETTING FEED ONLY FOR SJ Vs DC GAME
                If thrSportVUData Is Nothing Then
                    thrSportVUData = New Threading.Thread(AddressOf GetData)
                    thrSportVUData.Start()
                ElseIf thrSportVUData.ThreadState = Threading.ThreadState.Stopped Then
                    thrSportVUData.Abort()
                    thrSportVUData = New Threading.Thread(AddressOf GetData)
                    thrSportVUData.Start()

                ElseIf thrSportVUData.ThreadState = Threading.ThreadState.Unstarted Then
                    thrSportVUData.Abort()
                    thrSportVUData = New Threading.Thread(AddressOf GetData)
                    thrSportVUData.Start()
                ElseIf ThreadState.Running = Threading.ThreadState.Running Then
                    thrSportVUData = New Threading.Thread(AddressOf GetData)
                End If
            End If
        Catch ex As Exception
            thrSportVUData.Abort()
            Throw ex
        End Try
    End Sub

    Private Sub GetData()
        Try
            m_dsEventData = m_objSportVU.GetAllSportVUData(m_objGameDetails.GameCode, m_objGameDetails.ModuleID)
            BindListView(m_dsEventData)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Private Sub BindData()
        Try
            lvwSportVU.Items.Clear()
            If m_objGameDetails.ModuleID = 1 Then
                lvwSportVU.Columns(1).Width = 100
            ElseIf m_objGameDetails.ModuleID = 3 Then
                lvwSportVU.Columns(1).Width = 0
            End If
            If Not m_dsEventData Is Nothing Then
                If m_dsEventData.Tables.Count > 0 Then
                    If m_dsEventData.Tables(0).Rows.Count > 0 Then
                        For intRowCount = 0 To m_dsEventData.Tables(0).Rows.Count - 1
                            Dim lvwGameSchedule As New ListViewItem(m_dsEventData.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsEventData.Tables(0).Rows(intRowCount).Item("EVENT").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsEventData.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsEventData.Tables(0).Rows(intRowCount).Item("PLAYER_NAME").ToString)
                            lvwSportVU.Items.Add(lvwGameSchedule)
                        Next
                    End If
                End If
            Else
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            tmrRefreshData.Stop()
            Me.Close()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub tmrRefreshData_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrRefreshData.Tick
        Try
            Dim ds As New DataSet
            If m_dsEventData IsNot Nothing Then
                If m_dsEventData.Tables.Count > 0 Then
                    If m_dsEventData.Tables(0).Rows.Count > 0 Then
                        Dim OldUID As String = m_dsEventData.Tables(0).Rows(0).Item("unique_id").ToString
                        ds = m_objSportVU.GetMaxUniqueID(m_objGameDetails.ModuleID)
                        If Not ds Is Nothing Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                Dim dr() As DataRow = ds.Tables(0).Select("UNIQUE_ID > " & OldUID)
                                If dr.Length > 0 Then
                                    CreateSportVuThread()
                                End If
                            End If
                        End If
                    Else
                        CreateSportVuThread()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    'Private Sub RefreshData()
    '    Try
    '        Dim ds As New DataSet
    '        Dim OldUID As String = m_dsEventData.Tables(0).Rows(0).Item("unique_id").ToString
    '        ds = m_objSportVU.GetMaxUniqueID(m_objGameDetails.ModuleID)
    '        If Not ds Is Nothing Then
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                Dim dr() As DataRow = ds.Tables(0).Select("UNIQUE_ID > " & OldUID)
    '                If dr.Length > 0 Then
    '                    CreateSportVuThread()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Private thrSportVUData As Threading.Thread = New Threading.Thread(AddressOf GetData)
    'Private Sub frmSportVU_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        Me.Icon = frmMain.Icon
    '        'BindData()
    '        CreateSportVuThread()
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub CreateSportVuThread()
    '    Try
    '        'If m_objGameDetails.IsSportVUAvailable = True Then
    '        'CURRENTLY WE ARE GETTING FEED ONLY FOR SJ Vs DC GAME
    '        If thrSportVUData Is Nothing Then
    '            thrSportVUData = New Threading.Thread(AddressOf GetData)
    '            thrSportVUData.Start()
    '        ElseIf thrSportVUData.ThreadState = Threading.ThreadState.Stopped Then
    '            thrSportVUData.Abort()
    '            thrSportVUData = New Threading.Thread(AddressOf GetData)
    '            thrSportVUData.Start()

    '        ElseIf thrSportVUData.ThreadState = Threading.ThreadState.Unstarted Then
    '            thrSportVUData.Abort()
    '            thrSportVUData = New Threading.Thread(AddressOf GetData)
    '            thrSportVUData.Start()
    '        End If

    '        'End If
    '    Catch ex As Exception
    '        thrSportVUData.Abort()
    '        Throw ex
    '    End Try
    'End Sub
    'Private Sub GetData()
    '    Try
    '        'While (True)
    '        '    m_dsEventData = m_objSportVU.GetAllSportVUData(m_objGameDetails.GameCode)

    '        '    Application.DoEvents()
    '        '    'thrSportVUData.Abort()
    '        '    BindData()

    '        '    Application.DoEvents()
    '        '    Thread.Sleep(1000)
    '        '    BindData()
    '        '    CreateSportVuThread()
    '        'End While

    '        m_dsEventData = m_objSportVU.GetAllSportVUData(m_objGameDetails.GameCode)
    '        ' If thrSportVUData.ThreadState = ThreadState.Running Then
    '        'thrSportVUData.Abort()
    '        'End If
    '        BindData()

    '        Application.DoEvents()
    '        Thread.Sleep(1000)
    '        CreateSportVuThread()
    '    Catch ex As Exception
    '        Throw ex
    '        thrSportVUData.Abort()
    '    Finally

    '    End Try
    'End Sub

    'Private Sub BindData()
    '    Try
    '        lvwSportVU.Items.Clear()
    '        If Not m_dsEventData Is Nothing Then
    '            If m_dsEventData.Tables.Count > 0 Then
    '                If m_dsEventData.Tables(0).Rows.Count > 0 Then
    '                    For intRowCount = 0 To m_dsEventData.Tables(0).Rows.Count - 1
    '                        Dim lvwGameSchedule As New ListViewItem(m_dsEventData.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED").ToString)
    '                        lvwGameSchedule.SubItems.Add(m_dsEventData.Tables(0).Rows(intRowCount).Item("EVENT").ToString)
    '                        lvwGameSchedule.SubItems.Add(m_dsEventData.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
    '                        lvwGameSchedule.SubItems.Add(m_dsEventData.Tables(0).Rows(intRowCount).Item("PLAYER_NAME").ToString)
    '                        lvwSportVU.Items.Add(lvwGameSchedule)
    '                    Next
    '                End If
    '            End If
    '        Else
    '            MessageDialog.Show("Currently there is no SprortVUData", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    Try
    '        Me.Close()
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub



End Class