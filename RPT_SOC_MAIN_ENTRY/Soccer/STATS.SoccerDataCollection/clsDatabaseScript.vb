﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsDatabaseScript
' Author        : Sandeep Kumar Nanda
' Created Date  : 17 February 2010
' Description   : This class will execute the script.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By           | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                       |                   |
'               |                       |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

#Region " IMPORTS STATEMENTS"
Imports System.IO
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Security.Permissions
Imports System.Security.Cryptography
#End Region

Public Class clsDatabaseScript

#Region " DECLARATION"
    Private strFolderName As String
    Private strDatabaseName As String
    Private strScriptName As String

    Private Shared m_strPassPhrase As String = "MyPriv@Password!$$"    '---- any text string is good here
    Private Shared m_strHashAlgorithm As String = "MD5"                '--- we are doing MD5 encryption - can be "SHA1"
    Private Shared m_strPasswordIterations As Integer = 2              '--- can be any number
    Private Shared m_strInitVector As String = "@1B2c3D4e5F6g7H8"      '--- must be 16 bytes
    Private Shared m_intKeySize As Integer = 256
#End Region

#Region " USER DEFINED PUBLIC METHOD EXPOSED TO OUTSIDE"
    ''' <summary>
    ''' This method will execute and install database
    ''' </summary>
    ''' <param name="FolderName">Enter the Folder Name you want to specify </param>
    ''' <param name="DatabaseName">Enter the Database Name you want to create</param>
    ''' <param name="ScriptName">Enter the Script Name you want to execute</param>
    ''' <param name="SplashScreen">Enter the Splash Screen object you want to run during the installation process</param>
    ''' <remarks></remarks>
    Public Sub ExecuteScript(ByVal FolderName As String, ByVal DatabaseName As String, ByVal ScriptName As String, ByVal SplashScreen As Form)
        'FolderName: SoccerDataCollection
        'DatabaseName: Soccer
        'ScriptName: Soccer_Script
        'SplashScreen: SplashScreen object

        If GetCheckScript(FolderName, DatabaseName) = True Then
            SplashScreen.Show()
            DropDataBaseIfExist(FolderName, DatabaseName)
            CreateSpecifiedFolder(FolderName)
            CreateNewDataBase(DatabaseName, FolderName)
            CreateDataBaseAttribute(ScriptName)
            UpdateCheckScript(FolderName)
        End If
    End Sub
#End Region

#Region " USER DEFINED PRIVATE METHOD"

    'STATS/SANDEEP: GET THE APPLICATION PATH
    Private Function GetApplicationPath() As String
        Try
            Return System.AppDomain.CurrentDomain.BaseDirectory.Substring(0, 3)
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function

    'STATS/SANDEEP: CHECK WHETHER TO RUN SCRIPT OR NOT
    Private Function GetCheckScript() As Boolean
        Try
            If ConfigurationManager.AppSettings("CheckScript").ToUpper = "Compulsory".ToUpper Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: CHECK WHETHER TO RUN SCRIPT OR NOT
    Private Function GetCheckScript(ByVal FolderName As String, ByVal DatabaseName As String) As Boolean
        Try
            If Not Directory.Exists(GetApplicationPath() & FolderName & "\DUMP") Then
                Directory.CreateDirectory(GetApplicationPath() & FolderName & "\DUMP")
                Dim oWrite As System.IO.StreamWriter
                oWrite = IO.File.CreateText(GetApplicationPath() & FolderName & "\DUMP\Default.txt")
                oWrite.WriteLine(EncryptText("0.0.0.0"))
                oWrite.Close()
                Return True
            Else
                If Not System.IO.File.Exists(GetApplicationPath() & FolderName & "\DUMP\Default.txt") Then
                    Dim oWrite As System.IO.StreamWriter
                    oWrite = IO.File.CreateText(GetApplicationPath() & FolderName & "\DUMP\Default.txt")
                    oWrite.WriteLine(EncryptText("0.0.0.0"))
                    oWrite.Close()
                    Return True
                Else
                    Dim strNewVersion As String = String.Empty
                    Dim strOldVersion As String = String.Empty
                    'strNewVersion = String.Format("{0}", My.Application.Info.Version.ToString)
                    strNewVersion = String.Format("{0}", Application.ProductVersion.ToString)
                    'MessageBox.Show("New version number published is : " & strNewVersion, "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    Dim fs As FileStream
                    fs = New FileStream(GetApplicationPath() & FolderName & "\DUMP\Default.txt", FileMode.Open, FileAccess.Read)
                    'MessageBox.Show("Deafult file is opened to read the decrypted message", "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    Dim d As New StreamReader(fs)
                    d.BaseStream.Seek(0, SeekOrigin.Begin)
                    While d.Peek() > -1
                        strOldVersion &= d.ReadLine()
                    End While
                    d.Close()

                    'MessageBox.Show("Old version number :" & DecryptText(strOldVersion).ToUpper.Trim(), "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    If strNewVersion.ToUpper.Trim() = DecryptText(strOldVersion).ToUpper.Trim() Then
                        If Not Directory.Exists(GetApplicationPath() & FolderName & "\Database\") Then
                            Return True
                        Else
                            If System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf") = True And System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf") = True Then
                                'MessageBox.Show("new script is not generated" & strNewVersion, "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                Return False
                            Else
                                'MessageBox.Show("new script is generated" & strNewVersion, "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                Return True
                            End If
                        End If
                    Else
                        Return True
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: UPDATE THE CONFIG FILE BACK TO OPTIONAL.
    Private Function UpdateCheckScript(ByVal FolderName As String) As Boolean
        Try
            If Not Directory.Exists(GetApplicationPath() & FolderName & "\DUMP") Then
                Directory.CreateDirectory(GetApplicationPath() & FolderName & "\DUMP")
                Dim oWrite As System.IO.StreamWriter
                oWrite = IO.File.CreateText(GetApplicationPath() & FolderName & "\DUMP\Default.txt")
                oWrite.WriteLine(EncryptText(String.Format("{0}", Application.ProductVersion.ToString)))
                oWrite.Close()
            Else
                If Not System.IO.File.Exists(GetApplicationPath() & FolderName & "\DUMP\Default.txt") Then
                    Dim oWrite As System.IO.StreamWriter
                    oWrite = IO.File.CreateText(GetApplicationPath() & FolderName & "\DUMP\Default.txt")
                    oWrite.WriteLine(EncryptText(String.Format("{0}", Application.ProductVersion.ToString)))
                    oWrite.Close()
                Else
                    IO.File.Delete(GetApplicationPath() & FolderName & "\DUMP\Default.txt")
                    'MessageBox.Show("Default.txt file Deleted ", "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Dim oWrite As System.IO.StreamWriter
                    oWrite = IO.File.CreateText(GetApplicationPath() & FolderName & "\DUMP\Default.txt")
                    oWrite.WriteLine(EncryptText(String.Format("{0}", Application.ProductVersion.ToString)))
                    oWrite.Close()
                    'MessageBox.Show("Creating a new file and updating the new version info to the Default.txt file ", "Script", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: UPDATE THE CONFIG FILE BACK TO OPTIONAL.
    Private Function UpdateCheckScript() As Boolean
        Try
            Dim strDBReset As String = String.Empty
            strDBReset = ConfigurationManager.AppSettings("CheckScript")

            If strDBReset.ToUpper = "Compulsory".ToUpper Then
                Dim config As System.Configuration.Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                config.AppSettings.Settings.Item("CheckScript").Value = "Optional"
                config.Save(ConfigurationSaveMode.Modified)
                ConfigurationManager.RefreshSection("appSettings")
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: DROP THE EXISTING DATABASE IF ALREADY EXIST TO CREATE A NEW DATABASE.
    Private Function DropDataBaseIfExist(ByVal FolderName As String, ByVal DatabaseName As String) As Boolean
        Try
            Dim sqlFile As String = ""
            Dim sqlCmd As New SqlCommand

            Dim sqlConn As New SqlConnection("Data Source=.\SQLEXPRESS;Initial Catalog=master;Integrated Security=True")
            sqlConn.Open()

            sqlFile = "USE [master] "
            sqlCmd.Connection = sqlConn
            sqlCmd.CommandType = CommandType.Text
            sqlCmd.CommandText = sqlFile
            sqlCmd.ExecuteNonQuery()

            If System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf") = True Then
                Try
                    sqlFile = "IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" & DatabaseName & "')"
                    sqlFile = sqlFile & "DROP DATABASE [" & DatabaseName & "]"
                    sqlCmd.Connection = sqlConn
                    sqlCmd.CommandType = CommandType.Text
                    sqlCmd.CommandText = sqlFile
                    sqlCmd.ExecuteNonQuery()
                Catch ex As SqlException
                    ' FORCE DROPPING OF REMOTEDATABASE
                    sqlFile = "IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" & DatabaseName & "')"
                    sqlFile = sqlFile & "ALTER DATABASE [" & DatabaseName & "] "
                    sqlFile = sqlFile & "SET OFFLINE "
                    sqlFile = sqlFile & "WITH ROLLBACK IMMEDIATE "
                    sqlCmd.Connection = sqlConn
                    sqlCmd.CommandType = CommandType.Text
                    sqlCmd.CommandText = sqlFile
                    sqlCmd.ExecuteNonQuery()

                    sqlFile = "IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" & DatabaseName & "')"
                    sqlFile = sqlFile & "DROP DATABASE [" & DatabaseName & "]"
                    sqlCmd.Connection = sqlConn
                    sqlCmd.CommandType = CommandType.Text
                    sqlCmd.CommandText = sqlFile
                    sqlCmd.ExecuteNonQuery()
                End Try

                ' Granting permission for files to delete - Added by Chandramohan
                If System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf") = True Then
                    'Shell("takeown /F " & GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf")
                    Application.DoEvents()
                    ' Granting permission for files to delete - Added by Chandramohan - Code ends here
                    System.IO.File.Delete(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf")
                End If
                If System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf") = True Then
                    'Shell("takeown /F " & GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf")
                    Application.DoEvents()
                    System.IO.File.Delete(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf")
                End If
            Else
                Try
                    sqlFile = "IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" & DatabaseName & "')"
                    sqlFile = sqlFile & "DROP DATABASE [" & DatabaseName & "]"
                    sqlCmd.Connection = sqlConn
                    sqlCmd.CommandType = CommandType.Text
                    sqlCmd.CommandText = sqlFile
                    sqlCmd.ExecuteNonQuery()
                Catch ex As SqlException
                    Try
                        ' FORCE DROPPING OF REMOTEDATABASE
                        sqlFile = "IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" & DatabaseName & "')"
                        sqlFile = sqlFile & "ALTER DATABASE [" & DatabaseName & "] "
                        sqlFile = sqlFile & "SET OFFLINE "
                        sqlFile = sqlFile & "WITH ROLLBACK IMMEDIATE "
                        sqlCmd.Connection = sqlConn
                        sqlCmd.CommandType = CommandType.Text
                        sqlCmd.CommandText = sqlFile
                        sqlCmd.ExecuteNonQuery()

                        sqlFile = "IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" & DatabaseName & "')"
                        sqlFile = sqlFile & "DROP DATABASE [" & DatabaseName & "]"
                        sqlCmd.Connection = sqlConn
                        sqlCmd.CommandType = CommandType.Text
                        sqlCmd.CommandText = sqlFile
                        sqlCmd.ExecuteNonQuery()
                    Catch ex1 As Exception

                    End Try
                End Try

                ' Granting permission for files to delete - Added by Chandramohan
                If System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf") = True Then
                    'Shell("takeown /F " & GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf")
                    Application.DoEvents()
                    ' Granting permission for files to delete - Added by Chandramohan - Code ends here
                    System.IO.File.Delete(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf")
                End If
                If System.IO.File.Exists(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf") = True Then
                    'Shell("takeown /F " & GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf")
                    Application.DoEvents()
                    System.IO.File.Delete(GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: CREATE A NEW DATABASE
    Private Function CreateNewDataBase(ByVal DatabaseName As String, ByVal FolderName As String) As Boolean
        Try
            Dim sqlFile As String = ""
            Dim sqlCmd As New SqlCommand

            Dim sqlConn As New SqlConnection("Data Source=.\SQLEXPRESS;Initial Catalog=master;Integrated Security=True")
            sqlConn.Open()

            sqlFile = "CREATE DATABASE " & DatabaseName & " ON PRIMARY "
            sqlFile = sqlFile & "(NAME = " & DatabaseName & ", "
            sqlFile = sqlFile & "FILENAME = '" & GetApplicationPath() & FolderName & "\Database\" & DatabaseName & ".mdf', "
            sqlFile = sqlFile & "SIZE = 5MB, MAXSIZE = 100MB, FILEGROWTH = 10%) "
            sqlFile = sqlFile & "LOG ON (NAME = " & DatabaseName & "_Log, "
            sqlFile = sqlFile & "FILENAME = '" & GetApplicationPath() & FolderName & "\Database\" & DatabaseName & "_log.ldf', "
            sqlFile = sqlFile & "SIZE = 5MB, "
            sqlFile = sqlFile & "MAXSIZE = 100MB, "
            sqlFile = sqlFile & "FILEGROWTH = 10%)"
            sqlCmd.Connection = sqlConn
            sqlCmd.CommandType = CommandType.Text
            sqlCmd.CommandText = sqlFile
            sqlCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: EXECUTE ALL THE DATABASE ATTRIBUTE LIKE CREATE TABLE OR CREATE PROCEDURE
    Private Function CreateDataBaseAttribute(ByVal ScriptName As String) As Boolean
        Dim transaction As SqlTransaction
        Try
            Dim sqlFile As String
            Dim sql As String = ""

            sqlFile = Application.StartupPath & "\resources\" & ScriptName & ".sql"

            Dim sqlConn As New SqlConnection("Data Source=.\SQLEXPRESS;Initial Catalog=master;Integrated Security=True")
            sqlConn.Open()

            Using strm As FileStream = System.IO.File.OpenRead(sqlFile)
                Dim reader As New StreamReader(strm)
                sql = reader.ReadToEnd()
            End Using

            Dim regex As New Regex("^GO\s", RegexOptions.IgnoreCase Or RegexOptions.Multiline)
            Dim lines As String() = regex.Split(sql)

            transaction = sqlConn.BeginTransaction()
            Using cmd As SqlCommand = sqlConn.CreateCommand()
                cmd.Connection = sqlConn
                cmd.Transaction = transaction

                For Each line As String In lines
                    If line.Length > 0 Then
                        cmd.CommandText = line
                        cmd.CommandType = CommandType.Text
                        Application.DoEvents()
                        Try
                            cmd.ExecuteNonQuery()
                        Catch generatedExceptionName As SqlException
                            transaction.Rollback()
                            Throw
                        End Try
                    End If
                Next
            End Using

            transaction.Commit()

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'STATS/SANDEEP: CREATE THE FOLDER UNDER WHICH DATABASE WILL CREATE
    Private Function CreateSpecifiedFolder(ByVal FolderName As String) As Boolean
        Try
            If Not Directory.Exists(GetApplicationPath() & FolderName & "\Database\") Then
                Directory.CreateDirectory(GetApplicationPath() & FolderName & "\Database\")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function EncryptText(ByVal plainText As String) As String

        Dim strReturn As String = String.Empty
        Dim p_strSaltValue As String = "P@SSW@RD@09"
        Try
            Dim initVectorBytes As Byte()
            initVectorBytes = System.Text.Encoding.ASCII.GetBytes(m_strInitVector)

            Dim saltValueBytes As Byte()
            saltValueBytes = System.Text.Encoding.ASCII.GetBytes(p_strSaltValue)

            Dim plainTextBytes As Byte()
            plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText)

            Dim password As Rfc2898DeriveBytes

            password = New Rfc2898DeriveBytes(m_strPassPhrase, saltValueBytes, m_strPasswordIterations)

            Dim keyBytes As Byte()
            Dim intKeySize As Integer = 0

            intKeySize = CType((m_intKeySize / 8), Integer)

            keyBytes = password.GetBytes(intKeySize)

            Dim symmetricKey As System.Security.Cryptography.RijndaelManaged
            symmetricKey = New System.Security.Cryptography.RijndaelManaged

            symmetricKey.Mode = System.Security.Cryptography.CipherMode.CBC

            Dim encryptor As System.Security.Cryptography.ICryptoTransform
            encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)

            Dim memoryStream As System.IO.MemoryStream
            memoryStream = New System.IO.MemoryStream

            Dim cryptoStream As System.Security.Cryptography.CryptoStream
            cryptoStream = New System.Security.Cryptography.CryptoStream(memoryStream, encryptor, System.Security.Cryptography.CryptoStreamMode.Write)
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)

            cryptoStream.FlushFinalBlock()

            Dim cipherTextBytes As Byte()
            cipherTextBytes = memoryStream.ToArray()

            memoryStream.Close()
            cryptoStream.Close()

            Dim cipherText As String
            cipherText = Convert.ToBase64String(cipherTextBytes)

            strReturn = cipherText

        Catch ex As Exception
            strReturn = Nothing
        End Try

        Return strReturn

    End Function

    Private Function DecryptText(ByVal cipherText As String) As String

        Dim strReturn As String = String.Empty
        Dim p_strSaltValue As String = "P@SSW@RD@09"

        Try

            Dim initVectorBytes As Byte()
            initVectorBytes = System.Text.Encoding.ASCII.GetBytes(m_strInitVector)

            Dim saltValueBytes As Byte()
            saltValueBytes = System.Text.Encoding.ASCII.GetBytes(p_strSaltValue)

            Dim cipherTextBytes As Byte()
            cipherTextBytes = Convert.FromBase64String(cipherText)

            Dim password As Rfc2898DeriveBytes

            password = New Rfc2898DeriveBytes(m_strPassPhrase, saltValueBytes, m_strPasswordIterations)

            Dim keyBytes As Byte()
            Dim intKeySize As Integer

            intKeySize = CType((m_intKeySize / 8), Integer)

            keyBytes = password.GetBytes(intKeySize)

            Dim symmetricKey As System.Security.Cryptography.RijndaelManaged
            symmetricKey = New System.Security.Cryptography.RijndaelManaged

            symmetricKey.Mode = System.Security.Cryptography.CipherMode.CBC

            Dim decryptor As System.Security.Cryptography.ICryptoTransform
            decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

            Dim memoryStream As System.IO.MemoryStream
            memoryStream = New System.IO.MemoryStream(cipherTextBytes)

            Dim cryptoStream As System.Security.Cryptography.CryptoStream
            cryptoStream = New System.Security.Cryptography.CryptoStream(memoryStream, decryptor, System.Security.Cryptography.CryptoStreamMode.Read)

            Dim plainTextBytes As Byte()
            ReDim plainTextBytes(cipherTextBytes.Length)

            Dim decryptedByteCount As Integer
            decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length)

            memoryStream.Close()
            cryptoStream.Close()

            Dim plainText As String
            plainText = System.Text.Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount)

            strReturn = plainText

        Catch ex As Exception
            strReturn = Nothing
        End Try

        Return strReturn

    End Function

#End Region

End Class