#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "

Imports System.Configuration
Imports STATS.SoccerBL
Imports STATS.Utility

Imports Microsoft.Win32
#End Region

Public NotInheritable Class frmAbout

#Region " Constants and Variables "
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private lsupport As New languagesupport
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Dim strmessage As String = "Warning: This computer program is protected by copyright"
    Dim strmessage1 As String = "law and international treaties."
    Dim strmessage2 As String = "Unauthorized reproduction or distribution of this program,"
    Dim strmessage3 As String = "or any portion of it, may result in severe civil and criminal penalties,"
    Dim strmessage4 As String = "and will be prosecuted to the maximum."
#End Region

#Region " Event Handlers "
    Private Sub frmAbout_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAbout", Nothing, 1, 0)
            Dim ApplicationTitle As String
            Dim strServerName As String = "Web Server"

            ' Set the title of the form.
            If My.Application.Info.Title <> "" Then
                ApplicationTitle = My.Application.Info.Title
            Else
                ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
            End If
            Me.Text = String.Format("About {0}", ApplicationTitle)

            ' Initialize all of the text displayed on the About Box.
            ' TODO: Customize the application's assembly information in the "Application" pane of the project
            '    properties dialog (under the "Project" menu).
            Me.lblProductName.Text = My.Application.Info.ProductName
            Me.lblVersion.Text = String.Format("Version {0}", Application.ProductVersion.ToString)
            Me.lblCopyright.Text = My.Application.Info.Copyright



            Select Case ConfigurationSettings.AppSettings("Environment")
                Case "TESTRAC"
                    strServerName = "RODGERS (T)"
                Case "PRODUCTION"
                    strServerName = "DAWSON (P)"
                Case "STAGERAC"
                    strServerName = "MOSS (S)"
                Case "UATRAC"
                    strServerName = "MOSSUA (U)"
                Case Else
                    strServerName = "Unknown server"
            End Select
            lblDownloadServer.Text = "Downloaded from " & strServerName

            If CInt(m_objclsGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), Me.Text)
                lblProductName.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblProductName.Text)
                lblVersion.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblVersion.Text)
                lblDownloadServer.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblDownloadServer.Text)
                lblCopyright.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblCopyright.Text)
                lblRightsReserved.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblRightsReserved.Text)
                btnOk.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnOk.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strmessage4)

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOk.Text, Nothing, 1, 0)
        Me.Close()
    End Sub

#End Region

End Class