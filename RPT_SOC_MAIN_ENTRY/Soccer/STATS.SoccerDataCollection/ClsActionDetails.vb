﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : ClsActionDetails
' Author        : Shirley
' Created Date  : 11 Dec 2016
' Description   : This class contains information specific to all actions for Tier 6 (Merging PBP & Touches).
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'Wilson         | 17-12-2015          | Added new property and constructor |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class ClsActionDetails

    Shared _myInstance As ClsActionDetails

#Region " Shared methods "
    Public Shared Function GetInstance() As ClsActionDetails
        If _myInstance Is Nothing Then
            _myInstance = New ClsActionDetails()
        End If
        Return _myInstance
    End Function
#End Region

    Public _teamId? As Integer
    Public Property TeamId() As Nullable(Of Integer)
        Get
            Return _teamId
        End Get
        Set(value As Nullable(Of Integer))
            _teamId = value
        End Set
    End Property

    Public _event? As Integer
    Public Property EventId() As Nullable(Of Integer)
        Get
            Return _event
        End Get
        Set(value As Nullable(Of Integer))
            _event = value
        End Set
    End Property
    Public _bookingTypeId? As Integer
    Public Property BookingTypeId() As Nullable(Of Integer)
        Get
            Return _bookingTypeId
        End Get
        Set(value As Nullable(Of Integer))
            _bookingTypeId = value
        End Set
    End Property

    Private _eventDescription As String
    Public Property EventDescription() As String
        Get
            Return _eventDescription
        End Get
        Set(value As String)
            _eventDescription = value
        End Set
    End Property

    Public _player? As Integer
    Public Property PlayerId() As Nullable(Of Integer)
        Get
            Return _player
        End Get
        Set(value As Nullable(Of Integer))
            _player = value
        End Set
    End Property
    Public _subPlayer? As Integer
    Public Property subPlayerId() As Nullable(Of Integer)
        Get
            Return _subPlayer
        End Get
        Set(value As Nullable(Of Integer))
            _subPlayer = value
        End Set
    End Property
    Public _defPlayer? As Integer
    Public Property defPlayerId() As Nullable(Of Integer)
        Get
            Return _defPlayer
        End Get
        Set(value As Nullable(Of Integer))
            _defPlayer = value
        End Set
    End Property
    Private _customFormationHome As Dictionary(Of Integer, String)
    Public Property CustomFormationHome() As Dictionary(Of Integer, String)
        Get
            Return _customFormationHome
        End Get
        Set(ByVal value As Dictionary(Of Integer, String))
            _customFormationHome = value
        End Set
    End Property

    Private _customFormationAway As Dictionary(Of Integer, String)
    Public Property CustomFormationAway() As Dictionary(Of Integer, String)
        Get
            Return _customFormationAway
        End Get
        Set(ByVal value As Dictionary(Of Integer, String))
            _customFormationAway = value
        End Set
    End Property

    Public _playerX? As Integer
    Public Property PlayerX() As Nullable(Of Integer)
        Get
            Return _playerX
        End Get
        Set(value As Nullable(Of Integer))
            _playerX = value
        End Set
    End Property

    Public _playerY? As Integer
    Public Property PlayerY() As Nullable(Of Integer)
        Get
            Return _playerY
        End Get
        Set(value As Nullable(Of Integer))
            _playerY = value
        End Set
    End Property

    Public _kepperZoneX? As Integer
    Public Property KeeperZoneX() As Nullable(Of Integer)
        Get
            Return _kepperZoneX
        End Get
        Set(value As Nullable(Of Integer))
            _kepperZoneX = value
        End Set
    End Property

    Public _kepperZoneY? As Integer
    Public Property KepperZoneY() As Nullable(Of Integer)
        Get
            Return _kepperZoneY
        End Get
        Set(value As Nullable(Of Integer))
            _kepperZoneY = value
        End Set
    End Property

    Public _ballY? As Decimal
    Public Property BallY() As Nullable(Of Decimal)
        Get
            Return _ballY
        End Get
        Set(value As Nullable(Of Decimal))
            _ballY = value
        End Set
    End Property

    Public _ballZ? As Decimal
    Public Property BallZ() As Nullable(Of Decimal)
        Get
            Return _ballZ
        End Get
        Set(value As Nullable(Of Decimal))
            _ballZ = value
        End Set
    End Property

    Public _handY? As Decimal
    Public Property HandY() As Nullable(Of Decimal)
        Get
            Return _handY
        End Get
        Set(value As Nullable(Of Decimal))
            _handY = value
        End Set
    End Property

    Public _handZ? As Decimal
    Public Property HandZ() As Nullable(Of Decimal)
        Get
            Return _handZ
        End Get
        Set(value As Nullable(Of Decimal))
            _handZ = value
        End Set
    End Property

    Public _dsPBP As DataSet
    Public Property PzPBP() As DataSet
        Get
            Return _dsPBP
        End Get
        Set(ByVal value As DataSet)
            _dsPBP = value
        End Set
    End Property
    Private _TimeofEvent As String
    Public Property TimeofEvent() As String
        Get
            Return _TimeofEvent
        End Get
        Set(value As String)
            _TimeofEvent = value
        End Set
    End Property
    Private _EditMode As Boolean = False
    Public Property IsEditMode() As Boolean
        Get
            Return _EditMode
        End Get
        Set(value As Boolean)
            _EditMode = value
        End Set
    End Property
    Public _kepperZoneId? As Integer
    Public Property KepperZoneId() As Nullable(Of Integer)
        Get
            Return _kepperZoneId
        End Get
        Set(value As Nullable(Of Integer))
            _kepperZoneId = value
        End Set
    End Property

    Public _goalZoneId? As Integer
    Public Property GoalZoneId() As Nullable(Of Integer)
        Get
            Return _goalZoneId
        End Get
        Set(value As Nullable(Of Integer))
            _goalZoneId = value
        End Set
    End Property

    Public _outOfBoundsResId? As Integer
    Public Property OutOfBoundsResId() As Nullable(Of Integer)
        Get
            Return _outOfBoundsResId
        End Get
        Set(value As Nullable(Of Integer))
            _outOfBoundsResId = value
        End Set
    End Property

    Public _outOfBoundsResDesc As String
    Public Property OutOfBoundsResDesc() As String
        Get
            Return _outOfBoundsResDesc
        End Get
        Set(value As String)
            _outOfBoundsResDesc = value
        End Set
    End Property
    Private _NetworkDisconnect As Boolean = False
    Public Property IsNetworkDisconnected() As Boolean
        Get
            Return _NetworkDisconnect
        End Get
        Set(value As Boolean)
            _NetworkDisconnect = value
        End Set
    End Property
    Public Sub New()

    End Sub
    Public Sub New(pTeamId? As Integer, pEventId? As Integer, pPlayerId? As Integer)
        TeamId = pTeamId
        EventId = pEventId
        PlayerId = pPlayerId
    End Sub

    Public Class TimeElapsed
        Private _SetMinutes As Int32
        Public Property SetMinutes() As Int32
            Get
                Return _SetMinutes
            End Get
            Set(value As Int32)
                _SetMinutes = value
            End Set
        End Property

        Private _checkminutes As Int32

        Public Property Checkminutes() As Int32
            Get
                Return _checkminutes
            End Get
            Set(value As Int32)
                _checkminutes = value
            End Set
        End Property

        Private _displayMinutes As Int32
        Public Property Displayminutes() As Int32
            Get
                Return _displayMinutes
            End Get
            Set(value As Int32)
                _displayMinutes = value
            End Set
        End Property
        Public Sub New(pSetMinutes As Integer, pCheckminutes As Integer, pDisplayminutes As Integer)
            SetMinutes = pSetMinutes
            Checkminutes = pCheckminutes
            Displayminutes = pDisplayminutes
        End Sub
    End Class
End Class