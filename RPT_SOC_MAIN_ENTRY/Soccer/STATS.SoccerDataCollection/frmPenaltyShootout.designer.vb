﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPenaltyShootout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvPenaltyShootout = New System.Windows.Forms.ListView()
        Me.SeqNumber = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Sequence = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Score = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Team = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Result = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Player = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GoalZone = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.KeeperZone = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.pnlSave = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lstSubType = New System.Windows.Forms.ListBox()
        Me.lstKeeperZone = New System.Windows.Forms.ListBox()
        Me.UdcSoccerGoal2 = New STATS.SoccerDataCollection.udcSoccerGoal()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblKeeperZone = New System.Windows.Forms.Label()
        Me.lblGoalZone = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lstPlayer = New System.Windows.Forms.ListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lstType = New System.Windows.Forms.ListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lstResult = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lstTeam = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSequence = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lstGoalZone = New System.Windows.Forms.ListBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.UdcSoccerGoal1 = New STATS.SoccerDataCollection.udcSoccerGoal()
        Me.sstPenaltyShootout = New System.Windows.Forms.StatusStrip()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.pnlClockBar = New System.Windows.Forms.Panel()
        Me.lklAwayteam = New System.Windows.Forms.LinkLabel()
        Me.lklHometeam = New System.Windows.Forms.LinkLabel()
        Me.lblScoreAway = New System.Windows.Forms.Label()
        Me.lblScoreHome = New System.Windows.Forms.Label()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.picHomeLogo = New System.Windows.Forms.PictureBox()
        Me.picAwayLogo = New System.Windows.Forms.PictureBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.pnlSave.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClockBar.SuspendLayout()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvPenaltyShootout
        '
        Me.lvPenaltyShootout.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.SeqNumber, Me.Sequence, Me.Score, Me.Team, Me.Result, Me.Type, Me.Player, Me.GoalZone, Me.KeeperZone})
        Me.lvPenaltyShootout.FullRowSelect = True
        Me.lvPenaltyShootout.Location = New System.Drawing.Point(15, 355)
        Me.lvPenaltyShootout.MultiSelect = False
        Me.lvPenaltyShootout.Name = "lvPenaltyShootout"
        Me.lvPenaltyShootout.Size = New System.Drawing.Size(800, 202)
        Me.lvPenaltyShootout.TabIndex = 284
        Me.lvPenaltyShootout.UseCompatibleStateImageBehavior = False
        Me.lvPenaltyShootout.View = System.Windows.Forms.View.Details
        '
        'SeqNumber
        '
        Me.SeqNumber.Text = "Sequence Number"
        Me.SeqNumber.Width = 0
        '
        'Sequence
        '
        Me.Sequence.Text = "Seq"
        Me.Sequence.Width = 36
        '
        'Score
        '
        Me.Score.Text = "Score"
        Me.Score.Width = 49
        '
        'Team
        '
        Me.Team.Text = "Team"
        Me.Team.Width = 110
        '
        'Result
        '
        Me.Result.Text = "Result"
        Me.Result.Width = 93
        '
        'Type
        '
        Me.Type.Text = "Type"
        Me.Type.Width = 100
        '
        'Player
        '
        Me.Player.Text = "Player"
        Me.Player.Width = 120
        '
        'GoalZone
        '
        Me.GoalZone.Text = "Goal Zone"
        Me.GoalZone.Width = 114
        '
        'KeeperZone
        '
        Me.KeeperZone.Text = "Keeper Zone"
        Me.KeeperZone.Width = 110
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 337)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 15)
        Me.Label2.TabIndex = 283
        Me.Label2.Text = "Saved shoot-outs:"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(821, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 288
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(820, 505)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 25)
        Me.btnEdit.TabIndex = 294
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(820, 534)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 291
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'pnlSave
        '
        Me.pnlSave.BackColor = System.Drawing.Color.Lavender
        Me.pnlSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSave.Controls.Add(Me.Label12)
        Me.pnlSave.Controls.Add(Me.lstSubType)
        Me.pnlSave.Controls.Add(Me.lstKeeperZone)
        Me.pnlSave.Controls.Add(Me.UdcSoccerGoal2)
        Me.pnlSave.Controls.Add(Me.Panel1)
        Me.pnlSave.Controls.Add(Me.Label7)
        Me.pnlSave.Controls.Add(Me.lstPlayer)
        Me.pnlSave.Controls.Add(Me.Label6)
        Me.pnlSave.Controls.Add(Me.lstType)
        Me.pnlSave.Controls.Add(Me.Label5)
        Me.pnlSave.Controls.Add(Me.lstResult)
        Me.pnlSave.Controls.Add(Me.Label4)
        Me.pnlSave.Controls.Add(Me.lstTeam)
        Me.pnlSave.Controls.Add(Me.Label3)
        Me.pnlSave.Controls.Add(Me.txtSequence)
        Me.pnlSave.Controls.Add(Me.Label1)
        Me.pnlSave.Controls.Add(Me.btnCancel)
        Me.pnlSave.Controls.Add(Me.btnSave)
        Me.pnlSave.Controls.Add(Me.Panel2)
        Me.pnlSave.Controls.Add(Me.Panel4)
        Me.pnlSave.Controls.Add(Me.Panel5)
        Me.pnlSave.Controls.Add(Me.Panel3)
        Me.pnlSave.Location = New System.Drawing.Point(0, 73)
        Me.pnlSave.Name = "pnlSave"
        Me.pnlSave.Size = New System.Drawing.Size(900, 264)
        Me.pnlSave.TabIndex = 305
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(337, 13)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(79, 15)
        Me.Label12.TabIndex = 338
        Me.Label12.Text = "Sub Sub Type:"
        '
        'lstSubType
        '
        Me.lstSubType.FormattingEnabled = True
        Me.lstSubType.IntegralHeight = False
        Me.lstSubType.ItemHeight = 15
        Me.lstSubType.Location = New System.Drawing.Point(338, 31)
        Me.lstSubType.Name = "lstSubType"
        Me.lstSubType.Size = New System.Drawing.Size(150, 216)
        Me.lstSubType.TabIndex = 337
        '
        'lstKeeperZone
        '
        Me.lstKeeperZone.FormattingEnabled = True
        Me.lstKeeperZone.IntegralHeight = False
        Me.lstKeeperZone.ItemHeight = 15
        Me.lstKeeperZone.Location = New System.Drawing.Point(700, 102)
        Me.lstKeeperZone.Name = "lstKeeperZone"
        Me.lstKeeperZone.Size = New System.Drawing.Size(106, 146)
        Me.lstKeeperZone.TabIndex = 332
        '
        'UdcSoccerGoal2
        '
        Me.UdcSoccerGoal2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UdcSoccerGoal2.Location = New System.Drawing.Point(500, 184)
        Me.UdcSoccerGoal2.MaximumSize = New System.Drawing.Size(190, 64)
        Me.UdcSoccerGoal2.MinimumSize = New System.Drawing.Size(190, 64)
        Me.UdcSoccerGoal2.Name = "UdcSoccerGoal2"
        Me.UdcSoccerGoal2.Size = New System.Drawing.Size(190, 64)
        Me.UdcSoccerGoal2.TabIndex = 330
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.OldLace
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblKeeperZone)
        Me.Panel1.Controls.Add(Me.lblGoalZone)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Location = New System.Drawing.Point(611, 83)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(84, 98)
        Me.Panel1.TabIndex = 319
        '
        'lblKeeperZone
        '
        Me.lblKeeperZone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblKeeperZone.ForeColor = System.Drawing.Color.Blue
        Me.lblKeeperZone.Location = New System.Drawing.Point(-1, 80)
        Me.lblKeeperZone.Name = "lblKeeperZone"
        Me.lblKeeperZone.Size = New System.Drawing.Size(84, 17)
        Me.lblKeeperZone.TabIndex = 331
        Me.lblKeeperZone.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblGoalZone
        '
        Me.lblGoalZone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGoalZone.ForeColor = System.Drawing.Color.Blue
        Me.lblGoalZone.Location = New System.Drawing.Point(-1, -1)
        Me.lblGoalZone.Name = "lblGoalZone"
        Me.lblGoalZone.Size = New System.Drawing.Size(84, 17)
        Me.lblGoalZone.TabIndex = 330
        Me.lblGoalZone.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial Unicode MS", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(7, 57)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(69, 12)
        Me.Label11.TabIndex = 322
        Me.Label11.Text = "7.32m X 2.44m"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 42)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 15)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Dimension:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(19, 29)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 15)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "GOAL"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.MintCream
        Me.Label7.Location = New System.Drawing.Point(497, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 15)
        Me.Label7.TabIndex = 315
        Me.Label7.Text = "Goal Zone:"
        '
        'lstPlayer
        '
        Me.lstPlayer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstPlayer.FormattingEnabled = True
        Me.lstPlayer.IntegralHeight = False
        Me.lstPlayer.ItemHeight = 15
        Me.lstPlayer.Location = New System.Drawing.Point(178, 31)
        Me.lstPlayer.Name = "lstPlayer"
        Me.lstPlayer.Size = New System.Drawing.Size(150, 216)
        Me.lstPlayer.TabIndex = 314
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(175, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 15)
        Me.Label6.TabIndex = 313
        Me.Label6.Text = "Player:"
        '
        'lstType
        '
        Me.lstType.FormattingEnabled = True
        Me.lstType.ItemHeight = 15
        Me.lstType.Location = New System.Drawing.Point(15, 198)
        Me.lstType.Name = "lstType"
        Me.lstType.Size = New System.Drawing.Size(150, 49)
        Me.lstType.TabIndex = 312
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 180)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 15)
        Me.Label5.TabIndex = 311
        Me.Label5.Text = "Type:"
        '
        'lstResult
        '
        Me.lstResult.FormattingEnabled = True
        Me.lstResult.ItemHeight = 15
        Me.lstResult.Location = New System.Drawing.Point(15, 128)
        Me.lstResult.Name = "lstResult"
        Me.lstResult.Size = New System.Drawing.Size(150, 49)
        Me.lstResult.TabIndex = 310
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 15)
        Me.Label4.TabIndex = 309
        Me.Label4.Text = "Result:"
        '
        'lstTeam
        '
        Me.lstTeam.FormattingEnabled = True
        Me.lstTeam.ItemHeight = 15
        Me.lstTeam.Location = New System.Drawing.Point(15, 58)
        Me.lstTeam.Name = "lstTeam"
        Me.lstTeam.Size = New System.Drawing.Size(150, 49)
        Me.lstTeam.TabIndex = 308
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 15)
        Me.Label3.TabIndex = 307
        Me.Label3.Text = "Team:"
        '
        'txtSequence
        '
        Me.txtSequence.Location = New System.Drawing.Point(77, 10)
        Me.txtSequence.Name = "txtSequence"
        Me.txtSequence.Size = New System.Drawing.Size(43, 22)
        Me.txtSequence.TabIndex = 306
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 305
        Me.Label1.Text = "Sequence:"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(817, 199)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 229
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(817, 228)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 228
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.MintCream
        Me.Panel2.Controls.Add(Me.lstGoalZone)
        Me.Panel2.Location = New System.Drawing.Point(494, 13)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(118, 168)
        Me.Panel2.TabIndex = 333
        '
        'lstGoalZone
        '
        Me.lstGoalZone.FormattingEnabled = True
        Me.lstGoalZone.ItemHeight = 15
        Me.lstGoalZone.Location = New System.Drawing.Point(6, 19)
        Me.lstGoalZone.Name = "lstGoalZone"
        Me.lstGoalZone.Size = New System.Drawing.Size(106, 139)
        Me.lstGoalZone.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Bisque
        Me.Panel4.Location = New System.Drawing.Point(494, 162)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(201, 89)
        Me.Panel4.TabIndex = 335
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Bisque
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Location = New System.Drawing.Point(611, 83)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(201, 168)
        Me.Panel5.TabIndex = 336
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Bisque
        Me.Label8.Location = New System.Drawing.Point(86, 2)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 15)
        Me.Label8.TabIndex = 332
        Me.Label8.Text = "Keeper Zone:"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.MintCream
        Me.Panel3.Controls.Add(Me.UdcSoccerGoal1)
        Me.Panel3.Location = New System.Drawing.Point(611, 13)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(201, 87)
        Me.Panel3.TabIndex = 334
        '
        'UdcSoccerGoal1
        '
        Me.UdcSoccerGoal1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UdcSoccerGoal1.Location = New System.Drawing.Point(3, 3)
        Me.UdcSoccerGoal1.MaximumSize = New System.Drawing.Size(190, 64)
        Me.UdcSoccerGoal1.MinimumSize = New System.Drawing.Size(190, 64)
        Me.UdcSoccerGoal1.Name = "UdcSoccerGoal1"
        Me.UdcSoccerGoal1.Size = New System.Drawing.Size(190, 64)
        Me.UdcSoccerGoal1.TabIndex = 327
        '
        'sstPenaltyShootout
        '
        Me.sstPenaltyShootout.AutoSize = False
        Me.sstPenaltyShootout.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstPenaltyShootout.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstPenaltyShootout.Location = New System.Drawing.Point(0, 608)
        Me.sstPenaltyShootout.Name = "sstPenaltyShootout"
        Me.sstPenaltyShootout.Size = New System.Drawing.Size(899, 16)
        Me.sstPenaltyShootout.TabIndex = 306
        Me.sstPenaltyShootout.Text = "StatusStrip1"
        '
        'picReporterBar
        '
        Me.picReporterBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-1, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(934, 21)
        Me.picReporterBar.TabIndex = 239
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(0, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(933, 15)
        Me.picTopBar.TabIndex = 238
        Me.picTopBar.TabStop = False
        '
        'pnlClockBar
        '
        Me.pnlClockBar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnlClockBar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.pnlClockBar.Controls.Add(Me.lklAwayteam)
        Me.pnlClockBar.Controls.Add(Me.lklHometeam)
        Me.pnlClockBar.Controls.Add(Me.lblScoreAway)
        Me.pnlClockBar.Controls.Add(Me.lblScoreHome)
        Me.pnlClockBar.Controls.Add(Me.lblPeriod)
        Me.pnlClockBar.Location = New System.Drawing.Point(72, 33)
        Me.pnlClockBar.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlClockBar.Name = "pnlClockBar"
        Me.pnlClockBar.Size = New System.Drawing.Size(733, 40)
        Me.pnlClockBar.TabIndex = 307
        '
        'lklAwayteam
        '
        Me.lklAwayteam.ActiveLinkColor = System.Drawing.Color.White
        Me.lklAwayteam.AutoSize = True
        Me.lklAwayteam.DisabledLinkColor = System.Drawing.Color.White
        Me.lklAwayteam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklAwayteam.ForeColor = System.Drawing.Color.White
        Me.lklAwayteam.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lklAwayteam.LinkColor = System.Drawing.Color.White
        Me.lklAwayteam.Location = New System.Drawing.Point(563, 11)
        Me.lklAwayteam.Name = "lklAwayteam"
        Me.lklAwayteam.Size = New System.Drawing.Size(108, 25)
        Me.lklAwayteam.TabIndex = 22
        Me.lklAwayteam.TabStop = True
        Me.lklAwayteam.Text = "Away team"
        Me.lklAwayteam.VisitedLinkColor = System.Drawing.Color.White
        '
        'lklHometeam
        '
        Me.lklHometeam.ActiveLinkColor = System.Drawing.Color.White
        Me.lklHometeam.AutoSize = True
        Me.lklHometeam.DisabledLinkColor = System.Drawing.Color.White
        Me.lklHometeam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklHometeam.ForeColor = System.Drawing.Color.White
        Me.lklHometeam.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lklHometeam.LinkColor = System.Drawing.Color.White
        Me.lklHometeam.Location = New System.Drawing.Point(26, 11)
        Me.lklHometeam.Name = "lklHometeam"
        Me.lklHometeam.Size = New System.Drawing.Size(112, 25)
        Me.lklHometeam.TabIndex = 0
        Me.lklHometeam.TabStop = True
        Me.lklHometeam.Text = "Home team"
        Me.lklHometeam.VisitedLinkColor = System.Drawing.Color.White
        '
        'lblScoreAway
        '
        Me.lblScoreAway.AutoSize = True
        Me.lblScoreAway.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreAway.ForeColor = System.Drawing.Color.White
        Me.lblScoreAway.Location = New System.Drawing.Point(464, 11)
        Me.lblScoreAway.Name = "lblScoreAway"
        Me.lblScoreAway.Size = New System.Drawing.Size(23, 25)
        Me.lblScoreAway.TabIndex = 17
        Me.lblScoreAway.Text = "0"
        '
        'lblScoreHome
        '
        Me.lblScoreHome.AutoSize = True
        Me.lblScoreHome.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreHome.ForeColor = System.Drawing.Color.White
        Me.lblScoreHome.Location = New System.Drawing.Point(216, 11)
        Me.lblScoreHome.Name = "lblScoreHome"
        Me.lblScoreHome.Size = New System.Drawing.Size(23, 25)
        Me.lblScoreHome.TabIndex = 16
        Me.lblScoreHome.Text = "0"
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblPeriod.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.ForeColor = System.Drawing.Color.White
        Me.lblPeriod.Location = New System.Drawing.Point(298, 11)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(112, 18)
        Me.lblPeriod.TabIndex = 6
        Me.lblPeriod.Text = "Penalty Shoot-out"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picHomeLogo
        '
        Me.picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picHomeLogo.Location = New System.Drawing.Point(16, 33)
        Me.picHomeLogo.Name = "picHomeLogo"
        Me.picHomeLogo.Size = New System.Drawing.Size(40, 40)
        Me.picHomeLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picHomeLogo.TabIndex = 308
        Me.picHomeLogo.TabStop = False
        '
        'picAwayLogo
        '
        Me.picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picAwayLogo.Location = New System.Drawing.Point(830, 33)
        Me.picAwayLogo.Name = "picAwayLogo"
        Me.picAwayLogo.Size = New System.Drawing.Size(40, 40)
        Me.picAwayLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAwayLogo.TabIndex = 309
        Me.picAwayLogo.TabStop = False
        '
        'picButtonBar
        '
        Me.picButtonBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(-1, 563)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(934, 60)
        Me.picButtonBar.TabIndex = 310
        '
        'frmPenaltyShootout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(899, 624)
        Me.Controls.Add(Me.picAwayLogo)
        Me.Controls.Add(Me.picHomeLogo)
        Me.Controls.Add(Me.pnlClockBar)
        Me.Controls.Add(Me.sstPenaltyShootout)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lvPenaltyShootout)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.pnlSave)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmPenaltyShootout"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Soccer Data Collection - Penalty Shoot-out"
        Me.pnlSave.ResumeLayout(False)
        Me.pnlSave.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClockBar.ResumeLayout(False)
        Me.pnlClockBar.PerformLayout()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents lvPenaltyShootout As System.Windows.Forms.ListView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Sequence As System.Windows.Forms.ColumnHeader
    Friend WithEvents Team As System.Windows.Forms.ColumnHeader
    Friend WithEvents Score As System.Windows.Forms.ColumnHeader
    Friend WithEvents Type As System.Windows.Forms.ColumnHeader
    Friend WithEvents Player As System.Windows.Forms.ColumnHeader
    Friend WithEvents Result As System.Windows.Forms.ColumnHeader
    Friend WithEvents GoalZone As System.Windows.Forms.ColumnHeader
    Friend WithEvents KeeperZone As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents pnlSave As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lstPlayer As System.Windows.Forms.ListBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lstType As System.Windows.Forms.ListBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lstResult As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lstTeam As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSequence As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents sstPenaltyShootout As System.Windows.Forms.StatusStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents UdcSoccerGoal1 As STATS.SoccerDataCollection.udcSoccerGoal
    Friend WithEvents lstKeeperZone As System.Windows.Forms.ListBox
    Friend WithEvents UdcSoccerGoal2 As STATS.SoccerDataCollection.udcSoccerGoal
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblGoalZone As System.Windows.Forms.Label
    Friend WithEvents lblKeeperZone As System.Windows.Forms.Label
    Friend WithEvents lstGoalZone As System.Windows.Forms.ListBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lstSubType As System.Windows.Forms.ListBox
    Friend WithEvents pnlClockBar As System.Windows.Forms.Panel
    Friend WithEvents lklAwayteam As System.Windows.Forms.LinkLabel
    Friend WithEvents lklHometeam As System.Windows.Forms.LinkLabel
    Friend WithEvents lblScoreAway As System.Windows.Forms.Label
    Friend WithEvents lblScoreHome As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents picHomeLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picAwayLogo As System.Windows.Forms.PictureBox
    Friend WithEvents SeqNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
