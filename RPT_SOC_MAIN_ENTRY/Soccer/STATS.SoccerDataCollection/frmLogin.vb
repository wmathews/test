﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.Globalization
Imports IWshRuntimeLibrary
Imports System.IO
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Imports System.Security.Permissions
Imports System.Security.Cryptography

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmLogin
' Author        : RaviKrishna,Shravani
' Created Date  : 28th April,2009
' Description   : This form validates the user credentials,downloads data from Oracle database to local SQL Server database and fills the game
'                 details assigned to the user.
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :ppt
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmLogin

#Region " Constants & Variables "
    Private m_objUtility As New clsUtility
    Private m_objLogin As New clsLogin
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objclsSelectGame As clsSelectGame = clsSelectGame.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_langid As clsLoginDetails
    Private lsupport As New languagesupport
    Private m_dsGames As New DataSet
    Private m_dsDisplayGames As New DataSet
    Private m_dsUserDetails As New DataSet
    Private m_blnLoginSuccess As Boolean = False

    Private m_objUtilAudit As New clsAuditLog
    Private m_blnCheck As Boolean = False
    Private MessageDialog As New frmMessageDialog

    Private m_objclsModule1PBP As clsModule1PBP = clsModule1PBP.GetInstance()
    Private boolDisplayFontMessage As Boolean = False
    'Demo
    Private m_dsDemoGame As New DataSet
    Public cmbval As Int32
    Public lagid As Int32

    Private Shared m_strPassPhrase As String = "MyPriv@Password!$$"    '---- any text string is good here
    Private Shared m_strHashAlgorithm As String = "MD5"                '--- we are doing MD5 encryption - can be "SHA1"
    Private Shared m_strPasswordIterations As Integer = 2              '--- can be any number
    Private Shared m_strInitVector As String = "@1B2c3D4e5F6g7H8"      '--- must be 16 bytes
    Private Shared m_intKeySize As Integer = 256
    Dim temparr(5) As String
    Dim stlangcheck As String
    Private Shared m_ClickCount As Integer = 0
    Dim str As String = String.Empty
    Dim strlanguagename As String = String.Empty
    Dim objProperties As ClsProperty = ClsProperty.GetAccess()
    Dim networkConnection As Boolean
#End Region

#Region " User Methods "

    ''' <summary>
    ''' Enable the Login controls and fills the Language combo
    ''' </summary>
    ''' <param name="Language">DataTable which contains LanguageTable Data</param>
    ''' <remarks></remarks>
    Private Sub EnableLogin(ByVal Language As DataTable)
        Try
            clsUtility.LoadControl(cmbLanguage, clsUtility.SelectTypeDisplay.BLANK, Language, "LANGUAGE_DESC", "LANGUAGE_ID")
            cmbLanguage.SelectedValue = 1
            txtUsername.Text = ""
            txtPassword.Text = ""
            txtUsername.Focus()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Hide the login controls while showing the game details in listview
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub HideLoginControls()
        Try
            lblLanguage.Visible = False
            cmbLanguage.Visible = False
            lblMode.Visible = False
            radDemo.Visible = False
            radLive.Visible = False
            lblPassword.Visible = False
            txtPassword.Visible = False
            lblUsername.Visible = False
            txtUsername.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'SANDEEP ADDED THIS CODE FOR VISTA 64 BIT ISSUE.
    Private Function Is64BitSystem() As Boolean
        Try
            Dim strRegistryPath As String = Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Microsoft SQL Server\SQLEXPRESS\Setup", "SQLPath", Nothing).ToString()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Set the properties of  Login Details Class
    ''' </summary>
    ''' <param name="LoginDetails">A DataTable which contains logindetails</param>
    ''' <param name="Status">A Boolean variable which shows game availability for the logged in user</param>
    ''' <remarks></remarks>
    Private Sub FillLoginDetails(ByVal LoginDetails As DataTable, ByVal Status As Boolean)

        If radLive.Checked = True Then
            m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE
        Else
            m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO
        End If
        m_objLoginDetails.UserId = CInt(LoginDetails.Rows(0).Item("USER_ID"))
        m_objLoginDetails.UserFirstName = CStr(LoginDetails.Rows(0).Item("FIRST_NAME"))
        m_objLoginDetails.UserLastName = CStr(LoginDetails.Rows(0).Item("LAST_NAME"))
        m_objLoginDetails.UserLoginName = CStr(LoginDetails.Rows(0).Item("LOGIN_NAME"))

        If (LoginDetails.Rows(0).Item("USER_TYPE").ToString = "REPORTER") Then
            m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter
        Else
            m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS
        End If

        'm_objLoginDetails.LoginDate = CDate(LoginDetails.Rows(0).Item("LOGIN_DATE"))
        'Arindam 21-May-09 Date issue reported by Floris
        m_objLoginDetails.LoginDate = Format(LoginDetails.Rows(0).Item("LOGIN_DATE"), "yyyy-MM-dd hh:mm:ss tt")
        m_objLoginDetails.LanguageID = CInt(cmbLanguage.SelectedValue)
        m_objLoginDetails.GameAvailability = Status
        m_objLoginDetails.Language = cmbLanguage.Text

        If radLive.Checked = True Then
            m_objLoginDetails.strSortOrder = "Position"
        Else
            m_objLoginDetails.strSortOrder = "Last Name"
        End If
        'jan 2012(shirley) added mainly stats utility class to have a reporterid
        'to be used to create a audit log file.
        objProperties.UserID = m_objLoginDetails.UserId

    End Sub

    ''' <summary>
    ''' Download and fill the Reporter Data from Oracle to local SQL Tables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillLocalSQLTables(ByVal StrGameCode As String)
        Try
            'Dim intCoverageLevel As Integer = 0

            If (m_objGameDetails.IsNewDemomode = True) Then
                m_objLoginDetails.UserId = -1
                If m_objLogin.DownloadSoccerDataDemo(StrGameCode, m_objGameDetails.ModuleID, m_objGameDetails.CoverageLevel, m_objGameDetails.LeagueID, m_objGameDetails.languageid) = False Then
                    MessageDialog.Show("Error inserting data from Oracle to Sql tables.., Please try again", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Error)
                    Application.Exit()
                End If
            Else
                If m_objLogin.DownloadSoccerData(m_objLoginDetails.UserId, m_objGameDetails.languageid, StrGameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID, m_objGameDetails.ReporterRole, m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel) = False Then
                    MessageDialog.Show("Error inserting data from Oracle to Sql tables.., Please try again", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Error)
                    Application.Exit()
                End If
            End If

            ''GET THE MAX COVERGAE LEVEL, EXAMPLE IF WE ARE DOWNLOADING DIFFERENT TIERS SAY 1,2,3 GETS THE TIER 3 AND DOWNLOADS THE MULTILINGUAL DATA
            'Dim DsFeedCoverageInfo As New DataSet
            'DsFeedCoverageInfo = m_objLogin.GetFeedCoverageInfo(m_objGameDetails.GameCode, m_objLoginDetails.UserId, m_objGameDetails.FeedNumber)
            'If DsFeedCoverageInfo.Tables.Count > 0 Then
            '    intCoverageLevel = CInt(DsFeedCoverageInfo.Tables(0).Rows(0).Item("COVERAGELEVEL"))
            'End If

            If (m_objGameDetails.languageid <> 1) Then
                If m_objLogin.DownloadSoccermultilingualData(m_objGameDetails.languageid, m_objGameDetails.ModuleID, m_objGameDetails.CoverageLevel, StrGameCode, m_objLoginDetails.UserId) = False Then
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub
    Private Sub BindDomoGames()
        m_dsGames = m_dsUserDetails
        lvwGames.Items.Clear()
        Dim intRowCount As Integer
        Dim StrGameDate As String = ""
        If Not m_dsGames Is Nothing Then
            Dim drs() As DataRow
            m_dsDisplayGames = m_dsGames.Clone()
            For intTableCount As Integer = 0 To m_dsGames.Tables.Count - 1
                If intTableCount = 2 Then
                    drs = m_dsGames.Tables(intTableCount).Select("SERIAL_NO=1")
                    For Each DR As DataRow In drs
                        m_dsDisplayGames.Tables(intTableCount).ImportRow(DR)
                    Next
                Else
                    drs = m_dsGames.Tables(intTableCount).Select()
                    For Each DR As DataRow In drs
                        m_dsDisplayGames.Tables(intTableCount).ImportRow(DR)
                    Next
                End If
            Next

            If (m_dsDisplayGames.Tables(2).Rows.Count > 0) Then
                For intRowCount = 0 To m_dsDisplayGames.Tables(2).Rows.Count - 1
                    Dim lvwGameSchedule As New ListViewItem(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("GAME_CODE").ToString)
                    'displaying the gamedate based on his local time Zone
                    Dim Dt As DateTime = DateTimeHelper.GetDate(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                    If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                        StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                        lvwGameSchedule.SubItems.Add(StrGameDate)
                    Else
                        StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                        lvwGameSchedule.SubItems.Add(StrGameDate)
                    End If
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("Competition").ToString)
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("HOMETEAM").ToString)
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("AWAYTEAM").ToString)
                    ''''''
                    Dim Tier As String = ""
                    If m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_A") IsNot DBNull.Value Then
                        Tier = m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_A").ToString
                    ElseIf m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_B") IsNot DBNull.Value Then
                        Tier = m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_B").ToString
                    End If

                    If Tier = "0" Or Tier = "-1" Then
                        Continue For
                    End If
                    lvwGameSchedule.SubItems.Add(Tier)
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("Task").ToString)
                    lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("MODULE_ID").ToString)
                    lvwGameSchedule.SubItems.Add("")
                    lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("SERIAL_NO").ToString)

                    lvwGames.Items.Add(lvwGameSchedule)
                    lvwGames.Columns(7).Width = 0
                    lvwGames.Columns(8).Width = 0
                    lvwGames.Columns(9).Width = 0
                    lvwGames.Columns(10).Width = 0
                    lvwGames.Columns(11).Width = 0
                Next
                lblTimeZone.Location = New Point(160, 265)
                radFeedA.Checked = True
            End If
        End If

    End Sub
    ''' <summary>
    ''' Load the games into Listview assigned to the Reporter
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindGames()
        Try

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                lvwGames.Items.Clear()
                'Dim dsDemoGame As New DataSet
                m_dsDemoGame = m_objLogin.GetSamepleGameDisplay()
                If Not m_dsDemoGame Is Nothing Then
                    If (m_dsDemoGame.Tables(1).Rows.Count > 0) Then
                        For intRowCount = 0 To m_dsDemoGame.Tables(1).Rows.Count - 1
                            Dim lvwGameSchedule As New ListViewItem(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("GAME_CODE").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("GAME_DATE").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Competition").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("HOMETEAM").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("AWAYTEAM").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Tier").ToString)
                            lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Task").ToString)
                            ' lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(1).Rows(intRowCount).Item("Languageid").ToString)

                            lvwGameSchedule.SubItems.Add("")
                            lvwGameSchedule.SubItems.Add("")
                            lvwGameSchedule.SubItems.Add("")
                            lvwGameSchedule.SubItems.Add("")
                            lvwGames.Items.Add(lvwGameSchedule)
                        Next
                    Else
                        Dim lvwGameSchedule As New ListViewItem(m_dsDemoGame.Tables(0).Rows(0).Item("GAME_CODE").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("GAME_DATE").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Competition").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("HOMETEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("AWAYTEAM").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Tier").ToString)
                        lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Task").ToString)
                        'lvwGameSchedule.SubItems.Add(m_dsDemoGame.Tables(0).Rows(0).Item("Languageid").ToString)

                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGameSchedule.SubItems.Add("")
                        lvwGames.Items.Add(lvwGameSchedule)
                    End If
                Else
                    MessageDialog.Show("Currently there is no game available", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)

                End If
            Else
                m_dsGames = m_dsUserDetails
                lvwGames.Items.Clear()
                Dim intRowCount As Integer
                Dim StrGameDate As String = ""
                If Not m_dsGames Is Nothing Then
                    If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                        If (m_dsGames.Tables(2).Rows.Count > 0) Then
                            For intRowCount = 0 To m_dsGames.Tables(2).Rows.Count - 1
                                Dim lvwGameSchedule As New ListViewItem(m_dsGames.Tables(2).Rows(intRowCount).Item("GAME_CODE").ToString)
                                'Dim Dt As DateTime = CDate(m_dsGames.Tables(2).Rows(intRowCount).Item("GAME_DATE").ToString)
                                'Dim Dt As DateTime = DateTimeHelper.GetDate(m_dsGames.Tables(2).Rows(intRowCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                                'lvwGameSchedule.SubItems.Add(Dt.ToString("dd-MMM-yyyy hh:mm tt"))

                                'displaying the gamedate based on his local time Zone
                                Dim Dt As DateTime = DateTimeHelper.GetDate(m_dsGames.Tables(2).Rows(intRowCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                                If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                                    StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                                    lvwGameSchedule.SubItems.Add(StrGameDate)
                                Else
                                    StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                                    lvwGameSchedule.SubItems.Add(StrGameDate)
                                End If
                                lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("Competition").ToString)
                                lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("HOMETEAM").ToString)
                                lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("AWAYTEAM").ToString)
                                Dim Tier As String = ""
                                If m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_A") IsNot DBNull.Value Then
                                    Tier = m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_A").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_B") IsNot DBNull.Value Then
                                    Tier = m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_B").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_C") IsNot DBNull.Value Then
                                    Tier = m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_C").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_D") IsNot DBNull.Value Then
                                    Tier = m_dsGames.Tables(2).Rows(intRowCount).Item("TIER_D").ToString
                                End If
                                lvwGameSchedule.SubItems.Add(Tier)

                                lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("Task").ToString)

                                If CDbl(m_dsGames.Tables(2).Rows(intRowCount).Item("MODULE_ID").ToString) = 4 Then
                                    If m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_A") IsNot DBNull.Value Or m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_B") IsNot DBNull.Value Then
                                        lvwGameSchedule.SubItems.Add("Primary")
                                    End If
                                    If m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_C") IsNot DBNull.Value Or m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_D") IsNot DBNull.Value Then
                                        lvwGameSchedule.SubItems.Add("Training")
                                    End If
                                Else
                                    Select Case CInt(m_dsGames.Tables(2).Rows(intRowCount).Item("SERIAL_NO"))
                                        Case 1
                                            If Not IsDBNull(m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_A")) Or Not IsDBNull(m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_B")) Then
                                                lvwGameSchedule.SubItems.Add("Primary")
                                            Else
                                                lvwGameSchedule.SubItems.Add("Primary (Training)")
                                            End If
                                        Case 2, 3, 4, 5, 6
                                            If Not IsDBNull(m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_A")) Or Not IsDBNull(m_dsGames.Tables(2).Rows(intRowCount).Item("REPORTER_ROLE_B")) Then
                                                lvwGameSchedule.SubItems.Add("Assister")
                                            Else
                                                lvwGameSchedule.SubItems.Add("Assister (Training)")
                                            End If
                                    End Select

                                End If
                                Dim Channel As String = ""

                                If m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_A") IsNot DBNull.Value Then
                                    Channel = m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_A").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_B") IsNot DBNull.Value Then
                                    Channel = m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_B").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_C") IsNot DBNull.Value Then
                                    Channel = m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_C").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_D") IsNot DBNull.Value Then
                                    Channel = m_dsGames.Tables(2).Rows(intRowCount).Item("CHANNEL_D").ToString
                                End If
                                lvwGameSchedule.SubItems.Add(Channel)
                                'lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("DESK").ToString)
                                Dim Desk As String = ""

                                If m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_A") IsNot DBNull.Value Then
                                    Desk = m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_A").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_B") IsNot DBNull.Value Then
                                    Desk = m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_B").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_C") IsNot DBNull.Value Then
                                    Desk = m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_C").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_D") IsNot DBNull.Value Then
                                    Desk = m_dsGames.Tables(2).Rows(intRowCount).Item("DESK_D").ToString
                                End If
                                lvwGameSchedule.SubItems.Add(Desk)
                                lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("MODULE_ID").ToString)

                                Dim Language As String = ""

                                If m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_A") IsNot DBNull.Value Then
                                    Language = m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_A").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_B") IsNot DBNull.Value Then
                                    Language = m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_B").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_C") IsNot DBNull.Value Then
                                    Language = m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_C").ToString
                                ElseIf m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_D") IsNot DBNull.Value Then
                                    Language = m_dsGames.Tables(2).Rows(intRowCount).Item("LANGUAGE_ID_D").ToString
                                End If
                                lvwGameSchedule.SubItems.Add(Language)
                                lvwGameSchedule.SubItems.Add(m_dsGames.Tables(2).Rows(intRowCount).Item("SERIAL_NO").ToString)
                                Dim CntFeeds As Integer = 0
                                If m_dsGames.Tables(2).Rows(intRowCount).Item("FEED_A").ToString IsNot DBNull.Value Then
                                    CntFeeds = +1
                                End If
                                If m_dsGames.Tables(2).Rows(intRowCount).Item("FEED_B").ToString IsNot DBNull.Value Then
                                    CntFeeds = +1
                                End If
                                If m_dsGames.Tables(2).Rows(intRowCount).Item("FEED_C").ToString IsNot DBNull.Value Then
                                    CntFeeds = +1
                                End If
                                If m_dsGames.Tables(2).Rows(intRowCount).Item("FEED_D").ToString IsNot DBNull.Value Then
                                    CntFeeds = +1
                                End If

                                If CntFeeds > 1 Then
                                    m_objGameDetails.IsMultipleFeedsAssigned = True
                                Else
                                    m_objGameDetails.IsMultipleFeedsAssigned = False
                                End If
                                Dim GameCode As Integer
                                Dim ModuleID As Integer
                                Dim CurrGameCode As Integer
                                Dim CurrModuleID As Integer
                                lvwGameSchedule.SubItems.Add(Dt.ToString)
                                If lvwGames.Items.Count = 0 Then
                                    lvwGames.Items.Add(lvwGameSchedule)
                                Else
                                    Dim lvitem As ListViewItem = lvwGames.Items(lvwGames.Items.Count - 1)
                                    GameCode = CInt(lvitem.SubItems(0).Text)
                                    ModuleID = CInt(lvitem.SubItems(10).Text)
                                    CurrGameCode = CInt(m_dsGames.Tables(2).Rows(intRowCount).Item("GAME_CODE"))
                                    CurrModuleID = CInt(m_dsGames.Tables(2).Rows(intRowCount).Item("MODULE_ID"))
                                    If CurrGameCode = GameCode And CurrModuleID = ModuleID Then

                                    Else
                                        lvwGames.Items.Add(lvwGameSchedule)
                                    End If

                                End If
                                lvwGames.Columns(5).Width = 60
                                lvwGames.Columns(6).Width = 75
                                lvwGames.Columns(7).Width = 174
                                lvwGames.Columns(8).Width = 140
                            Next
                        End If
                    Else
                        'oPs login
                        Dim drs() As DataRow
                        m_dsDisplayGames = m_dsGames.Clone()
                        For intTableCount As Integer = 0 To m_dsGames.Tables.Count - 1
                            If intTableCount = 2 Then
                                drs = m_dsGames.Tables(intTableCount).Select("SERIAL_NO=1")
                                For Each DR As DataRow In drs
                                    m_dsDisplayGames.Tables(intTableCount).ImportRow(DR)
                                Next
                            Else
                                drs = m_dsGames.Tables(intTableCount).Select()
                                For Each DR As DataRow In drs
                                    m_dsDisplayGames.Tables(intTableCount).ImportRow(DR)
                                Next
                            End If
                        Next

                        If (m_dsDisplayGames.Tables(2).Rows.Count > 0) Then
                            For intRowCount = 0 To m_dsDisplayGames.Tables(2).Rows.Count - 1
                                Dim lvwGameSchedule As New ListViewItem(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("GAME_CODE").ToString)
                                'displaying the gamedate based on his local time Zone
                                Dim Dt As DateTime = DateTimeHelper.GetDate(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("GAME_DATE").ToString, DateTimeHelper.InputFormat.CurrentFormat)
                                If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                                    StrGameDate = Dt.Subtract(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                                    lvwGameSchedule.SubItems.Add(StrGameDate)
                                Else
                                    StrGameDate = Dt.Add(m_objGameDetails.SysDateDiff).ToString("dd-MMM-yyyy hh:mm tt")
                                    lvwGameSchedule.SubItems.Add(StrGameDate)
                                End If
                                lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("Competition").ToString)
                                lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("HOMETEAM").ToString)
                                lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("AWAYTEAM").ToString)
                                ''''''
                                Dim Tier As String = ""
                                If m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_A") IsNot DBNull.Value Then
                                    Tier = m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_A").ToString
                                ElseIf m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_B") IsNot DBNull.Value Then
                                    Tier = m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("COVERAGE_LEVEL_B").ToString
                                End If

                                If Tier = "0" Or Tier = "-1" Then
                                    Continue For
                                End If
                                lvwGameSchedule.SubItems.Add(Tier)
                                ''''''
                                'lvwGameSchedule.SubItems.Add("")
                                lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("Task").ToString)
                                lvwGameSchedule.SubItems.Add("")
                                lvwGameSchedule.SubItems.Add("")
                                lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("MODULE_ID").ToString)
                                lvwGameSchedule.SubItems.Add("")
                                lvwGameSchedule.SubItems.Add(m_dsDisplayGames.Tables(2).Rows(intRowCount).Item("SERIAL_NO").ToString)

                                lvwGames.Items.Add(lvwGameSchedule)
                                'lvwGames.Columns(5).Width = 0
                                'lvwGames.Columns(6).Width = 0
                                lvwGames.Columns(7).Width = 0
                                lvwGames.Columns(8).Width = 0
                                lvwGames.Columns(9).Width = 0
                                lvwGames.Columns(10).Width = 0
                                lvwGames.Columns(11).Width = 0
                            Next
                            lblTimeZone.Location = New Point(160, 265)
                            radFeedA.Checked = True
                        End If
                    End If
                Else
                    MessageDialog.Show("Currently there is no game(s) available", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' Set the properties of game details class
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGameDetails()
        Try
            Dim StrGameCodes As String = String.Empty

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                If lvwGames.SelectedItems.Count > 0 Then
                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                        Dim dsDemoGame As New DataSet
                        dsDemoGame = m_objLogin.GetSamepleGameDisplay()
                        m_objGameDetails.GameCode = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("GAME_CODE").ToString)
                        m_objGameDetails.GameDate = CDate(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("Kickoff"))
                        m_objGameDetails.ModuleID = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("MODULE_ID").ToString)
                        m_objGameDetails.ModuleName = dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("Task").ToString
                        m_objGameDetails.CoverageLevel = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("TIER").ToString)
                        m_objGameDetails.AwayTeamID = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("AWAYTEAMID").ToString)
                        m_objGameDetails.AwayTeamAbbrev = dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("AWAYABBREV").ToString
                        m_objGameDetails.AwayTeam = dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("AWAYTEAM").ToString
                        m_objGameDetails.HomeTeamID = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("HOMETEAMID").ToString)
                        m_objGameDetails.HomeTeamAbbrev = dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("HOMEABBREV").ToString
                        m_objGameDetails.HomeTeam = dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("HOMETEAM").ToString
                        m_objGameDetails.LeagueID = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                        m_objGameDetails.LeagueName = dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("Competition").ToString
                        If Not IsDBNull(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("FIELD_ID")) Then
                            m_objGameDetails.FieldID = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("FIELD_ID").ToString)
                        End If
                        m_objGameDetails.FeedNumber = CInt(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("FEEDNUMBER").ToString)
                        m_objGameDetails.ReporterRole = Convert.ToString(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("REPORTER_ROLE")) & Convert.ToInt32(dsDemoGame.Tables(1).Rows(0).Item("SERIAL_NO"))
                        m_objGameDetails.SerialNo = Convert.ToInt32(dsDemoGame.Tables(1).Rows(lvwitm.Index).Item("SERIAL_NO"))
                        m_objGameDetails.ReporterRoleDisplay = GetReporterRoleDescription()
                        m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                    Next
                End If
            Else

                If lvwGames.Items.Count > 0 Then
                    If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                        For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                            lvwGames.Items(lvwitm.Index).Selected = True
                            lvwGames.Select()
                            Dim intGameCode As String = lvwitm.SubItems(0).Text
                            Dim intModuleID As String = lvwitm.SubItems(10).Text
                            Dim intSerialNo As String = lvwitm.SubItems(12).Text

                            Dim drs() As DataRow
                            drs = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                            If drs IsNot Nothing Then
                                If drs.Length > 0 Then
                                    m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                                    m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                                    m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)
                                    If m_objGameDetails.ModuleID = 2 Then
                                        StrGameCodes = StrGameCodes & "," & m_objGameDetails.GameCode
                                    End If
                                    m_objGameDetails.ModuleName = drs(0).Item("Task").ToString
                                    m_objGameDetails.AwayTeamID = CInt(drs(0).Item("AWAYTEAMID").ToString)
                                    m_objGameDetails.AwayTeamAbbrev = drs(0).Item("AWAYABBREV").ToString
                                    m_objGameDetails.AwayTeam = drs(0).Item("AWAYTEAM").ToString
                                    m_objGameDetails.HomeTeamID = CInt(drs(0).Item("HOMETEAMID").ToString)
                                    m_objGameDetails.HomeTeamAbbrev = drs(0).Item("HOMEABBREV").ToString
                                    m_objGameDetails.HomeTeam = drs(0).Item("HOMETEAM").ToString
                                    m_objGameDetails.LeagueID = CInt(drs(0).Item("LEAGUE_ID").ToString)
                                    m_objGameDetails.LeagueName = drs(0).Item("Competition").ToString

                                    If Not IsDBNull(drs(0).Item("FIELD_ID")) Then
                                        m_objGameDetails.FieldID = CInt(drs(0).Item("FIELD_ID").ToString)
                                    End If
                                    If Not IsDBNull(drs(0).Item("FEED_A")) Then
                                        m_objGameDetails.FeedNumber = CInt(drs(0).Item("FEED_A"))
                                        m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_A").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                        m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_A"))
                                    ElseIf Not IsDBNull(drs(0).Item("FEED_B")) Then
                                        m_objGameDetails.FeedNumber = 2
                                        m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_B").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                        m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_B"))
                                    ElseIf Not IsDBNull(drs(0).Item("FEED_C")) Then
                                        m_objGameDetails.FeedNumber = 3
                                        m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_C").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                        m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_C"))
                                    ElseIf Not IsDBNull(drs(0).Item("FEED_D")) Then
                                        m_objGameDetails.FeedNumber = 4
                                        m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE_D").ToString & Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                        m_objGameDetails.CoverageLevel = CInt(drs(0).Item("TIER_D"))
                                    End If
                                    m_objGameDetails.ReporterRoleSerial = CType(m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1), Integer?)

                                    'GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                                    m_objGameDetails.SerialNo = Convert.ToInt32(drs(0).Item("SERIAL_NO"))
                                    m_objGameDetails.ReporterRoleDisplay = GetReporterRoleDescription()
                                    lagid = 1

                                    If m_objGameDetails.SerialNo = 1 Then
                                        m_objGameDetails.IsPrimaryReporter = True
                                    Else
                                        m_objGameDetails.IsPrimaryReporter = False
                                    End If

                                    'chandrasekhar added this code for reporter validation
                                    If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter And (m_objGameDetails.FeedNumber = 1 Or m_objGameDetails.FeedNumber = 2) And m_objGameDetails.CoverageLevel > 3 Then
                                        Dim reporterLogs As DataTable = m_objLogin.GetReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString).Tables(0)
                                        If (reporterLogs.Rows.Count > 0) Then
                                            If MessageDialog.Show(reporterLogs.Rows(0).Item("REPORTER_NAME").ToString & " already logged in this Game from " & reporterLogs.Rows(0).Item("COMPUTER_ID").ToString & " at " & reporterLogs.Rows(0).Item("SELECT_TIME").ToString & ". Do you want to proceed anyway? Click YES to continue.", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.Yes Then
                                                m_objLogin.DeleteReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString)
                                                m_objLogin.InsertReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString, m_objLoginDetails.UserLastName)
                                                Me.DialogResult = Windows.Forms.DialogResult.OK
                                            Else
                                                Me.DialogResult = Windows.Forms.DialogResult.Cancel
                                                Application.Exit()
                                            End If
                                        Else
                                            m_objLogin.InsertReporterData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, System.Environment.GetEnvironmentVariable("COMPUTERNAME").ToString, m_objLoginDetails.UserLastName)

                                            Me.DialogResult = Windows.Forms.DialogResult.OK
                                        End If
                                    End If

                                End If
                            End If
                        Next
                    Else
                        For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                            Dim intGameCode As String = lvwitm.SubItems(0).Text
                            Dim intModuleID As String = lvwitm.SubItems(9).Text
                            Dim intSerialNo As String = lvwitm.SubItems(11).Text

                            lvwGames.Items(lvwitm.Index).Selected = True
                            lvwGames.Select()

                            Dim drs() As DataRow = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                            If drs.Length > 0 Then
                                m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                                m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                                m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)

                                m_objGameDetails.ModuleName = drs(0).Item("Task").ToString
                                m_objGameDetails.AwayTeamID = CInt(drs(0).Item("AWAYTEAMID").ToString)
                                m_objGameDetails.AwayTeamAbbrev = drs(0).Item("AWAYABBREV").ToString
                                m_objGameDetails.AwayTeam = drs(0).Item("AWAYTEAM").ToString
                                m_objGameDetails.HomeTeamID = CInt(drs(0).Item("HOMETEAMID").ToString)
                                m_objGameDetails.HomeTeamAbbrev = drs(0).Item("HOMEABBREV").ToString
                                m_objGameDetails.HomeTeam = drs(0).Item("HOMETEAM").ToString
                                m_objGameDetails.LeagueID = CInt(drs(0).Item("LEAGUE_ID").ToString)
                                m_objGameDetails.LeagueName = drs(0).Item("Competition").ToString
                                If Not IsDBNull(drs(0).Item("FIELD_ID")) Then
                                    m_objGameDetails.FieldID = CInt(drs(0).Item("FIELD_ID").ToString)
                                End If
                                If (m_objGameDetails.IsNewDemomode = True) Then
                                    m_objGameDetails.CoverageLevel = CInt(drs(0).Item("COVERAGE_LEVEL_A").ToString)
                                End If

                                If radFeedA.Checked = True Then
                                    m_objGameDetails.FeedNumber = 1
                                ElseIf radFeedB.Checked = True Then
                                    m_objGameDetails.FeedNumber = 2
                                End If
                                m_objGameDetails.ReporterRoleDisplay = "Operation Desk" ' CStr(IIf(CInt(m_dsGames.Tables(2).Rows(lvwitm.Index).Item("SERIAL_NO").ToString) = 1, "MAIN", "ASSISTER"))
                                m_objGameDetails.ReporterRole = drs(0).Item("REPORTER_ROLE").ToString
                                m_objGameDetails.SerialNo = Convert.ToInt32(drs(0).Item("SERIAL_NO"))

                                lagid = 1
                            End If
                        Next
                    End If

                    If m_objGameDetails.ModuleID = 2 And m_objLoginDetails.UserType <> clsLoginDetails.m_enmUserType.OPS Then
                        StrGameCodes = StrGameCodes.Substring(1)
                    Else
                        StrGameCodes = CStr(m_objGameDetails.GameCode)
                    End If

                    'T1 WORKFLOW STARTS HERE
                    Dim StrReporterRole As String = ""
                    Dim m_dsDataCountForRestart As DataSet
                    Dim m_dsRestartData As DataSet

                    'Dim intTabcnt As Integer = 1
                    'If m_objGameDetails.ModuleID = 3 Or m_objGameDetails.ModuleID = 4 Then
                    '    intTabcnt = 0
                    'Else
                    '    intTabcnt = 1
                    'End If

                    If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) = "1" Then
                        'FOR PRIMARY REPORTER CHECK IF THE DATA EXISTS IN LOCAL SQL
                        'COMMENTED BY SHRAVANI
                        'm_dsDataCountForRestart = m_objLogin.GetLocalSqlDataCountForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        'TO CHECK THE SELECTED GAMES DATA AVAILABLE OR NOT IN LOCAL DATABASE - SHRAVANI
                        m_dsDataCountForRestart = m_objLogin.GetLocalSqlDataCountForRestart(StrGameCodes, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                            If MessageDialog.Show("Do you want to Reload the game state from STATS Server? Click YES to restore the game from STATS Server or Click NO to restore from this Computer.", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Information) = Windows.Forms.DialogResult.No Then
                                'restore from Local SQL.
                                sstMain.Items(0).Text = "The game is reloading..."
                                m_objGameDetails.IsRestartGame = True
                                m_dsRestartData = m_objLogin.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                                m_objGameDetails.RestartData = m_dsRestartData
                                m_objGeneral.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, 0, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")
                            Else
                                'Download from oracle
                                'WRITE CODE IF SOME OTHER GAME EXITS IN HIS LOCAL SQL..
                                m_objGameDetails.IsRestartGame = False
                                'DOWNLOAD FROM ORACLE AND FILL THE LOCAL SQL TABLES
                                sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                                Application.DoEvents()
                                FillLocalSQLTables(StrGameCodes)
                                'DOWNLOAD ORACLE SP FILLS THE PBP TABLE
                                'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                                m_dsDataCountForRestart = m_objLogin.GetLocalSqlDataCountForRestart(StrGameCodes, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                                If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                                    'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                                    sstMain.Items(0).Text = "The game is reloading..."
                                    MessageDialog.Show("The selected game data will be reloaded.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    m_objGameDetails.IsRestartGame = True
                                    m_dsRestartData = m_objLogin.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                                    m_objGameDetails.RestartData = m_dsRestartData
                                End If
                                InsertCurrentGame()
                                If m_objLoginDetails.UserId = -1 Then
                                    GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                                End If
                            End If

                        Else
                            'WRITE CODE IF SOME OTHER GAME EXITS IN HIS LOCAL SQL..
                            m_objGameDetails.IsRestartGame = False
                            'DOWNLOAD FROM ORACLE AND FILL THE LOCAL SQL TABLES
                            sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                            Application.DoEvents()
                            FillLocalSQLTables(StrGameCodes)

                            'DOWNLOAD ORACLE SP FILLS THE PBP TABLE
                            'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                            m_dsDataCountForRestart = m_objLogin.GetLocalSqlDataCountForRestart(StrGameCodes, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                            If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                                'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                                sstMain.Items(0).Text = "The game is reloading..."
                                MessageDialog.Show("The selected game data will be reloaded.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                m_objGameDetails.IsRestartGame = True
                                m_dsRestartData = m_objLogin.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                                m_objGameDetails.RestartData = m_dsRestartData
                            End If
                            InsertCurrentGame()
                            If m_objLoginDetails.UserId = -1 Then
                                GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                            End If
                        End If

                    Else 'FOR SECONDARY REPORTER (ASSISTERS)
                        m_objGameDetails.IsRestartGame = False
                        'DOWNLOAD AND FILL THE LOCAL SQL TABLES
                        sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                        Application.DoEvents()
                        FillLocalSQLTables(StrGameCodes)
                        'DOWNLOAD ORACLE SP FILLS THE PBP TABLE
                        'CHECK IF THE GAME DATA EXISTS IN LOCAL SQL
                        m_dsDataCountForRestart = m_objLogin.GetLocalSqlDataCountForRestart(StrGameCodes, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                        If CInt(m_dsDataCountForRestart.Tables(0).Rows(0).Item("ReturnValue")) = 1 Then
                            'IF DATA IS AVAILABLE IN SQL, SET THE PROPERTIES AND REFESH THE SCREENS
                            sstMain.Items(0).Text = "The Game is reloading..."
                            MessageDialog.Show("The selected game data will be reloaded.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            m_objGameDetails.IsRestartGame = True
                            m_dsRestartData = m_objLogin.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                            m_objGameDetails.RestartData = m_dsRestartData
                        End If

                        InsertCurrentGame()
                        'Added by Arindam on 17-Jan:: This is required to release lock for Q3...
                        m_objGameDetails.AssistersLastEntryStatus = True
                        If m_objLoginDetails.UserId = -1 Then
                            GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                        End If
                    End If
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "GAMECODE - " & m_objGameDetails.GameCode & ", " & "REPORTER NAME - " & m_objLoginDetails.UserLoginName & ", " & "REPORTER ROLE - " & m_objGameDetails.ReporterRole, 1, 0)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GameSelected, m_objGameDetails.AwayTeam & " @ " & m_objGameDetails.HomeTeam & ", League:" & m_objGameDetails.LeagueName & ",  " & m_objGameDetails.ModuleName, 1, 0)

                End If
                'Module 2 Tier 2 Fetch all players in prop dataset
                'HOME AND AWAY TEAM ROSTERS AND FETCHED AND STORED IN A DATASET
                m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
                'End If
                If m_objLoginDetails.UserType <> clsLoginDetails.m_enmUserType.OPS And m_objGameDetails.CoverageLevel < 3 Then
                    m_objGeneral.InsertTeamGameForLowTiers(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                End If
            End If

            'Multilingual data to dictionary
            If (m_objGameDetails.languageid <> 1 And m_objGameDetails.ModuleID <> 4) Then
                m_objGameDetails.MultilingualData = (From a In m_objGeneral.GetMultilingual(m_objGameDetails.languageid).Tables(0)
                                              Select New With
                                              {
                                                  .Key = a.Field(Of String)("ENGLISH"),
                                                  .Val = a.Field(Of String)("MULTILINGUAL")
                                              }).AsEnumerable().ToDictionary(Function(k) k.Key, Function(v) v.Val)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub FillGameDetailsOffline()
        Try
            Dim StrGameCodes As String = String.Empty

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then

                Dim dsGameDet As New DataSet
                dsGameDet = m_objLogin.GetGameDetails()

                For Each dr As DataRow In dsGameDet.Tables(0).Rows
                    m_objGameDetails.GameCode = CInt(dr.Item("GAME_CODE").ToString)
                    m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(dr.Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                    m_objGameDetails.ModuleID = CInt(dr.Item("MODULE_ID").ToString)
                    If m_objGameDetails.ModuleID = 2 Then
                        StrGameCodes = StrGameCodes & "," & m_objGameDetails.GameCode
                    End If
                    m_objGameDetails.ModuleName = dr.Item("Task").ToString
                    m_objGameDetails.AwayTeamID = CInt(dr.Item("AWAYTEAMID").ToString)
                    m_objGameDetails.AwayTeamAbbrev = dr.Item("AWAYABBREV").ToString
                    m_objGameDetails.AwayTeam = dr.Item("AWAYTEAM").ToString
                    m_objGameDetails.HomeTeamID = CInt(dr.Item("HOMETEAMID").ToString)
                    m_objGameDetails.HomeTeamAbbrev = dr.Item("HOMEABBREV").ToString
                    m_objGameDetails.HomeTeam = dr.Item("HOMETEAM").ToString
                    m_objGameDetails.LeagueID = CInt(dr.Item("LEAGUE_ID").ToString)
                    m_objGameDetails.LeagueName = dr.Item("Competition").ToString


                    m_objGameDetails.FieldID = CInt(dr.Item("FIELD_ID").ToString)


                    m_objGameDetails.FeedNumber = CInt(dr.Item("FEED_NUMBER"))
                    m_objGameDetails.ReporterRole = dr.Item("REPORTER_ROLE").ToString
                    m_objGameDetails.CoverageLevel = CInt(dr.Item("TIER"))

                    m_objGameDetails.ReporterRoleSerial = CType(m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1), Integer?)

                    'GetFeedCoverageInfo(m_objGameDetails.GameCode, -1, m_objGameDetails.FeedNumber)
                    m_objGameDetails.SerialNo = Convert.ToInt32(dr.Item("SERIAL_NO"))
                    m_objGameDetails.ReporterRoleDisplay = GetReporterRoleDescription()
                    lagid = 1

                    If m_objGameDetails.SerialNo = 1 Then
                        m_objGameDetails.IsPrimaryReporter = True
                    Else
                        m_objGameDetails.IsPrimaryReporter = False
                    End If
                Next


                If m_objGameDetails.ModuleID = 2 And m_objLoginDetails.UserType <> clsLoginDetails.m_enmUserType.OPS Then
                    StrGameCodes = StrGameCodes.Substring(1)
                Else
                    StrGameCodes = CStr(m_objGameDetails.GameCode)
                End If

                'T1 WORKFLOW STARTS HERE
                Dim StrReporterRole As String = ""
                ' Dim m_dsDataCountForRestart As DataSet
                Dim m_dsRestartData As DataSet

                sstMain.Items(0).Text = "The game is reloading..."
                m_objGameDetails.IsRestartGame = True
                m_dsRestartData = m_objLogin.GetLocalSqlDataForRestart(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ModuleID)
                m_objGameDetails.RestartData = m_dsRestartData
                m_objGeneral.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, 0, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")

                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "GAMECODE - " & m_objGameDetails.GameCode & ", " & "REPORTER NAME - " & m_objLoginDetails.UserLoginName & ", " & "REPORTER ROLE - " & m_objGameDetails.ReporterRole, 1, 0)
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GameSelected, m_objGameDetails.AwayTeam & " @ " & m_objGameDetails.HomeTeam & ", League:" & m_objGameDetails.LeagueName & ",  " & m_objGameDetails.ModuleName, 1, 0)


                'Module 2 Tier 2 Fetch all players in prop dataset
                'HOME AND AWAY TEAM ROSTERS AND FETCHED AND STORED IN A DATASET
                m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objGameDetails.languageid)
                'End If
                If m_objLoginDetails.UserType <> clsLoginDetails.m_enmUserType.OPS And m_objGameDetails.CoverageLevel < 3 Then
                    m_objGeneral.InsertTeamGameForLowTiers(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                End If
            End If

            'Multilingual data to dictionary
            If (m_objGameDetails.languageid <> 1 And m_objGameDetails.ModuleID <> 4) Then
                m_objGameDetails.MultilingualData = (From a In m_objGeneral.GetMultilingual(m_objGameDetails.languageid).Tables(0)
                                              Select New With
                                              {
                                                  .Key = a.Field(Of String)("ENGLISH"),
                                                  .Val = a.Field(Of String)("MULTILINGUAL")
                                              }).AsEnumerable().ToDictionary(Function(k) k.Key, Function(v) v.Val)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetReporterRoleDescription() As String
        Dim reporterRoleDesc As String = ""
        Select Case m_objGameDetails.SerialNo
            Case 1
                reporterRoleDesc = "Primary"
            Case 2
                reporterRoleDesc = "Assister"
            Case 3
                reporterRoleDesc = "Home"
            Case 4
                reporterRoleDesc = "Away"
            Case 5
                reporterRoleDesc = "QC1"
            Case 6
                reporterRoleDesc = "QC2"
        End Select
        Return reporterRoleDesc
    End Function
    ''' <summary>
    ''' INSERT THE SELECTED GAME INTO THE CURRENT GAME SQL TABLE
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertCurrentGame()
        Try
            'INSERTION INTO CURRENT GAME TABLE
            If m_objGameDetails.ModuleID = 2 Then
                Dim dsMultipleGames As DataSet
                'FOR MODULE 2 INSERTING ALL THE GAMES WITH +/- 24 HRS TO CURRENT GAME TABLE
                'FETCHING MULTIPLE GAMES ASSIGNED TO THE SAME REPORTER WITH DURATION +/- 24 HRS.
                ' dsMultipleGames = m_objLogin.GetMultipleGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserType, CDate(m_objGameDetails.GameDate))
                dsMultipleGames = m_objGeneral.GetGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserId)
                'INSERTION INTO CURRENT GAME INTO CURRENT GAME SQL TABLE
                m_objLogin.InsertMultipleGames(dsMultipleGames, m_objGameDetails.GameCode, m_objLoginDetails.UserId)
            Else
                'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                If (m_objGameDetails.IsNewDemomode = True) Then
                    m_objLogin.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameCode, 0, 0, "A1", m_objGameDetails.ModuleID, 0, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "INSERT")
                Else
                    m_objLogin.InsertCurrentGame(m_objGameDetails.GameCode, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, 0, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "INSERT")
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ChangeFormBackground()
        Try
            Me.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\\LoginBackground.jpg")
            Select Case ConfigurationSettings.AppSettings("Environment")
                Case "TESTRAC"
                    Me.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\\LoginBackgroundTest.jpg")
                Case "PRODUCTION"
                    Me.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\\LoginBackgroundProd.jpg")
                Case "UATRAC"
                    Me.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\\LoginBackgroundUAT.jpg")
                Case "STAGERAC"
                    Me.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\\LoginBackgroundStage.jpg")
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Event handlers "

    Private Sub frmLogin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            ' m_objLogin.DeleteReporterExistance(m_objLoginDetails.UserId)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Login Load where Database gets created
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmLogin", Nothing, 1, 0)
            Dim CApplicationPath As String = Application.StartupPath
            Me.Icon = New Icon(CApplicationPath & "\\Resources\\Soccer.ico", 256, 256)

            ChangeFormBackground()
            networkConnection = m_objGeneral.CheckNetConnection()
            If networkConnection Then
                If System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed = True Then
                    If System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CheckForUpdate = True Then
                        MessageDialog.Show("New update available! Please download the latest version of application from download site.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Me.Close()
                        Exit Sub
                    End If
                Else
                    MessageDialog.Show("Network connection is not available.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If
            'STATS/SANDEEP: BELOW CODE WRITTEN BY SANDEEP TO EXECUTE SCRIPT
            Dim splForm As New frmSplash
            Dim objDatabaseScript As New clsDatabaseScript
            Dim DsLanguage As DataSet
            Dim dsLoginLanu As DataSet
            objDatabaseScript.ExecuteScript("SoccerDataCollection", "Soccer", "Soccer_Script", splForm)

            If networkConnection Then
                DsLanguage = m_objLogin.GetLanguage()
            Else   'FPLRS-139
                DsLanguage = m_objLogin.GetSqlLanguage()
            End If
            dsLoginLanu = DsLanguage.Clone()
            If (DsLanguage.Tables.Count > 0) Then
                If (DsLanguage.Tables(0).Rows.Count > 0) Then
                    Dim drs() As DataRow = DsLanguage.Tables(0).Select(" LOGIN_LANG = 'T' ")
                    For Each dr In drs
                        dsLoginLanu.Tables(0).ImportRow(dr)
                    Next
                    EnableLogin(dsLoginLanu.Tables(0))
                Else
                    MessageDialog.Show("Languages cannot be loaded. Try restarting the application.", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Error)
                End If
            End If
            splForm.Close()
            'AUDIT TRIAL ADDED BY SHRAVANI
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ApplicationLoaded, "Soccer Data Collection ", 1, 0)

            txtUsername.Text = ""
            txtPassword.Text = ""
            txtUsername.Focus()

            grpDate.SendToBack()
            grpDate.Hide()
            Dim strServerName As String = ""
            strServerName = m_objLogin.GetServerName()

            'ADDED BY SANDEEP TO PUT THE DESKTOP SHORTCUT ONLY FOR PRODUCTION
            If strServerName.Contains("reporterapp.stats.com") Or strServerName.Contains("reporterbup.stats.com") Then
                Dim UserProfile As String = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
                Dim boolShortcut As Boolean
                boolShortcut = IsShortcutExtant()
                If (boolShortcut = False) Then
                    Dim shortCut As IWshRuntimeLibrary.IWshShortcut
                    Dim WshShell As New IWshShell_Class
                    shortCut = CType(WshShell.CreateShortcut(UserProfile & "\STATS Soccer Software.lnk"), IWshRuntimeLibrary.IWshShortcut)
                    Dim strfilepath1 As String
                    strfilepath1 = Application.StartupPath
                    strfilepath1 += "\Resources\Soccer.ICO"
                    With shortCut
                        .TargetPath = "http://reportersw.stats.com/"
                        .WindowStyle = 1
                        .Description = "STATS Soccer Software"
                        .IconLocation = strfilepath1
                        .WorkingDirectory = UserProfile
                        .Save()
                    End With
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Function IsShortcutExtant() As Boolean ' Created by Ravi - For Creating Desktop Shortcut
        Dim WshShell As New IWshShell_Class
        Dim deskTopDir As String = CType(WshShell.SpecialFolders.Item("Desktop"), String)
        If IO.File.Exists(deskTopDir & "\STATS Soccer Software.lnk") Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Handles the verification of userdetails,
    ''' Downloads the data from Oracle and insert to local SQL tables
    ''' If login success, games will be displayed in the listview assigned to the reporter
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            Dim rptCount As Integer
            btnLogin.Enabled = False
            Me.Cursor = Cursors.WaitCursor
            Dim blnGameStatus As Boolean = False
            Dim m_dsRptDetails As New DataSet
            Select Case btnLogin.Text
                Case "Login"

                    Select Case m_objLoginDetails.GameMode
                        Case clsLoginDetails.m_enmGameMode.DEMO

                            Me.Text = "Select game"
                            btnLogin.Text = "OK"
                            If (MessageDialog.Show("Do you want to select live games for demo, press Yes to continue or No to add a new game manually.", "Login", MessageDialogButtons.YesNo, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Yes) Then
                                m_objGameDetails.IsNewDemomode = True
                            End If
                            Application.DoEvents()
                            If m_objGameDetails.IsNewDemomode = True Then
                                Me.Cursor = Cursors.WaitCursor
                                ' btnAddGame.Visible = False
                                Label1.Visible = False
                                cmbLeague.Visible = True
                                cmbLeague.Enabled = True
                                Dim dsLeague As DataSet = GetAllLeagues()
                                Dim drResultSet As DataRow
                                Dim dsLoadData As New DataSet
                                dsLoadData = dsLeague.Copy()
                                drResultSet = dsLoadData.Tables(0).NewRow()
                                drResultSet(0) = 0
                                drResultSet(1) = "All"
                                cmbLeague.DataSource = dsLoadData.Tables(0)
                                dsLoadData.Tables(0).Rows.InsertAt(drResultSet, 0)
                                cmbLeague.ValueMember = dsLoadData.Tables(0).Columns(0).ToString()
                                cmbLeague.DisplayMember = dsLoadData.Tables(0).Columns(1).ToString()
                                cmbLeague.SelectedValue = 0
                                lvwGames.Height = lvwGames.Height '- grpDate.Height
                                lvwGames.Top = grpDate.Bottom
                                grpDate.Left = lvwGames.Left
                                grpDate.BringToFront()
                                grpDate.Visible = True
                                m_objLoginDetails.strSortOrder = "Position"
                            End If
                            If (m_objGameDetails.IsNewDemomode = True) Then
                                Dim ar_from_date, ar_to_date As Date
                                ar_from_date = Now.Date
                                ar_to_date = Now.Date
                                m_objGameDetails.FromDate = "" & Format(ar_from_date, Format(ar_from_date, "MM-dd-yyyy")) & " 12:00:00 AM"
                                m_objGameDetails.ToDate = "" & Format(ar_to_date, Format(ar_to_date, "MM-dd-yyyy")) & " 11:59:59 PM"
                                m_dsUserDetails = m_objLogin.FillDemoGames(m_objGameDetails.FromDate, m_objGameDetails.ToDate, 0)

                                cmbNoDays.Visible = False
                                cmbNoDays.Enabled = False
                                txtDays.Visible = False

                                'multilingual code
                                '---------------------------------------------------------------------------------------------------------
                                Dim strlbltimezone As String = "All times are displayed in"
                                Dim strtime_zone As String = "time zone."
                                If CInt(m_objGameDetails.languageid) <> 1 Then
                                    Me.Text = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), Me.Text)

                                    Dim g4 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "GameCode")
                                    Dim g5 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Kickoff")
                                    Dim g6 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Competition")
                                    Dim g7 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Home Team")
                                    Dim g8 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Away Team")
                                    Dim g9 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Tier")
                                    Dim g10 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Task")
                                    Dim g11 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "ModuleID")
                                    Dim g12 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "SerialNo")
                                    strlbltimezone = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), strlbltimezone)
                                    strtime_zone = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), strtime_zone)
                                    lvwGames.Columns(0).Text = g4
                                    lvwGames.Columns(1).Text = g5
                                    lvwGames.Columns(2).Text = g6
                                    lvwGames.Columns(3).Text = g7
                                    lvwGames.Columns(4).Text = g8
                                    lvwGames.Columns(6).Text = g9
                                    lvwGames.Columns(7).Text = g10
                                    lvwGames.Columns(8).Text = g11
                                    btnDelete.Text = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), btnDelete.Text)
                                    btnCancel.Text = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), btnCancel.Text)
                                    '  btnLogin.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnLogin.Text)
                                End If

                                lnkupcomming.Visible = False
                                Me.BackgroundImage = Nothing

                                'btnDelete.Visible = True
                                radFeedA.Visible = False
                                radFeedB.Visible = False

                                picTopBar.Visible = True
                                picReporterBar.Visible = True
                                'picCompanyLogo.Visible = True
                                picButtonBar.Visible = True
                                lvwGames.Visible = True

                                Me.Controls.Remove(btnLogin)
                                Me.Controls.Remove(btnCancel)
                                picButtonBar.Controls.Add(btnLogin)
                                picButtonBar.Controls.Add(btnCancel)
                                btnLogin.BackColor = Color.FromArgb(58, 111, 229)
                                btnCancel.BackColor = Color.FromArgb(255, 84, 52)
                                btnCancel.Left = picButtonBar.Width - btnCancel.Width - 10 'lvwGames.Left + lvwGames.Width - btnCancel.Width
                                btnCancel.Top = 8 'picButtonBar.Top + 9

                                btnLogin.Left = btnCancel.Left - btnLogin.Width - 5
                                btnLogin.Top = btnCancel.Top
                                lvwGames.Height = lvwGames.Height - grpDate.Height
                                m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE
                                m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS
                                BindDomoGames()
                                m_objGameDetails.TimeZoneName = "CST"
                                lblTimeZone.Visible = True
                                lblTimeZone.Text = strlbltimezone + " " + m_objGameDetails.TimeZoneName & " " + strtime_zone
                                lvwGames.Columns(1).Text = "Kickoff (" + m_objGameDetails.TimeZoneName + ")"
                                HideLoginControls()
                                Me.Cursor = Cursors.Default
                                Exit Sub
                            End If

                            HideLoginControls()
                            Me.Text = "Select game"
                            btnLogin.Text = "OK"

                            Me.BackgroundImage = Nothing
                            btnDelete.Visible = False
                            btnAddGame.Visible = True
                            lnkupcomming.Visible = False

                            picTopBar.Visible = True
                            picReporterBar.Visible = True
                            'picCompanyLogo.Visible = True
                            picButtonBar.Visible = True
                            lvwGames.Visible = True

                            Me.Controls.Remove(btnLogin)
                            Me.Controls.Remove(btnCancel)
                            picButtonBar.Controls.Add(btnLogin)
                            picButtonBar.Controls.Add(btnCancel)
                            btnLogin.BackColor = Color.FromArgb(58, 111, 229)
                            btnCancel.BackColor = Color.FromArgb(255, 84, 52)

                            btnCancel.Left = picButtonBar.Width - btnCancel.Width - 10 'lvwGames.Left + lvwGames.Width - btnCancel.Width
                            btnCancel.Top = 8 'picButtonBar.Top + 9
                            btnLogin.Left = btnCancel.Left - btnLogin.Width - 5
                            btnLogin.Top = btnCancel.Top

                            m_objLoginDetails.UserId = 9999
                            m_objLoginDetails.UserFirstName = "Demo"
                            m_objLoginDetails.UserLastName = "Reporter"
                            m_objLoginDetails.UserLoginName = "R_DemoReporter"

                            sstMain.Items(0).Text = "Loading data from server...this may take few minutes."
                            Application.DoEvents()
                            InsertDemoData()
                            m_objLoginDetails.IsDemoDatadownloaded = True
                            Me.Cursor = Cursors.Default

                            BindGames()
                            m_objGameDetails.languageid = CInt(cmbLanguage.SelectedValue.ToString())
                        Case clsLoginDetails.m_enmGameMode.LIVE
                            Application.DoEvents()
                            sstMain.Items(0).Text = "Validating username and password..."
                            'lblStatus.Text = "Validating username and password..."
                            Application.DoEvents()

                            Dim strlanguagename As String
                            If (txtUsername.Text.Trim.Length > 0) And (txtPassword.Text.Trim.Length > 0) Then
                                '-----------------------------For language check Available in system -------------------------------------------------------------------------------------
                                m_objGameDetails.languageid = CInt(cmbLanguage.SelectedValue.ToString())
                                cmbval = CInt(cmbLanguage.SelectedValue)
                                systemlanguagecheck(cmbval)
                                '-------------------------------------------------  Language code END -------------------------------------------------------------------------------------
                                If (stlangcheck = "set" Or cmbval = 1) Then
                                    networkConnection = m_objGeneral.CheckNetConnection()
                                    If networkConnection = False Then  'FPLRS-139	
                                        rptCount = m_objLogin.ValidateReporter(txtUsername.Text.Trim)
                                        If rptCount > 0 Then
                                            m_dsRptDetails = m_objLogin.GetReporterDetails(txtUsername.Text.Trim)
                                            FillLoginDetails(m_dsRptDetails.Tables(0), blnGameStatus)
                                            m_blnLoginSuccess = True
                                        Else
                                            Me.Cursor = Cursors.Default
                                            MessageDialog.Show("Please enter proper username/password", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                            txtUsername.Text = ""
                                            txtPassword.Text = ""
                                            cmbNoDays.Visible = False
                                            cmbNoDays.Enabled = False
                                            txtDays.Visible = False
                                            txtUsername.Focus()
                                        End If
                                    Else
                                        m_dsUserDetails = m_objLogin.ValidateUserDetails(txtUsername.Text.Trim, txtPassword.Text.Trim, m_objGameDetails.languageid, CInt(cmbNoDays.Text))
                                        m_objGameDetails.ReporterGames = m_dsUserDetails
                                        If (m_dsUserDetails.Tables.Count > 0) Then
                                            If (m_dsUserDetails.Tables("UserDetails").Rows.Count > 0) Then
                                                Application.DoEvents()
                                                'lblStatus.Text = "Loading data from server...this may take few minutes."
                                                Application.DoEvents()
                                                'Checking for the Availability of the game

                                                Dim dtOracleTime As DateTime
                                                Dim strTimeDiff As TimeSpan

                                                dtOracleTime = DateTimeHelper.GetDate(CStr(m_dsUserDetails.Tables(0).Rows(0)("LOGIN_DATE")), DateTimeHelper.InputFormat.CurrentFormat)
                                                strTimeDiff = DateTime.Now - dtOracleTime
                                                m_objLoginDetails.USIndiaTimeDiff = strTimeDiff

                                                '11/11/2009 Time Zone Getting the Diff
                                                If m_dsUserDetails.Tables("DATES").Rows.Count > 0 Then
                                                    Dim strOraTime As DateTime = DateTimeHelper.GetDate(CStr(m_dsUserDetails.Tables("DATES").Rows(0).Item("SYSDATE")), DateTimeHelper.InputFormat.CurrentFormat)
                                                    Dim StrConvDate As DateTime = DateTimeHelper.GetDate(CStr(m_dsUserDetails.Tables("DATES").Rows(0).Item("DATE_OUT")), DateTimeHelper.InputFormat.CurrentFormat)

                                                    m_objGameDetails.SysDateDiff = strOraTime - StrConvDate
                                                    m_objGameDetails.TimeZoneName = m_dsUserDetails.Tables("DATES").Rows(0).Item("TIME_ZONE_NAME").ToString
                                                End If

                                                If (m_dsUserDetails.Tables("Status").Rows.Count > 0) Then
                                                    If CInt(m_dsUserDetails.Tables("Status").Rows(0).Item("STATUS")) = 2 Then
                                                        'NO GAMES ARE ASSIGNED TO THE REPORTER
                                                        blnGameStatus = False
                                                        MessageDialog.Show("There are no games assigned to " & txtUsername.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                                    ElseIf CInt(m_dsUserDetails.Tables("Status").Rows(0).Item("STATUS")) = 3 Then
                                                        'REPORTER ALREDAY LOGGED IN
                                                        If MessageDialog.Show(txtUsername.Text & " is already logged in. Do you want to ignore this warning and proceed anyway?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                                                            Exit Try
                                                        Else
                                                            'INDICATES THE REPORTER IS ABOUT TO OVERRIDE (MIGHT BE A SYSTEM CRASH LAST TIME)
                                                            'SHOW REPORTER ASSIGNMENT ON THE SCREEN
                                                            FillLoginDetails(m_dsUserDetails.Tables("UserDetails"), blnGameStatus)
                                                            m_blnLoginSuccess = True
                                                        End If
                                                    ElseIf CInt(m_dsUserDetails.Tables("Status").Rows(0).Item("STATUS")) = 0 Then
                                                        'SUCCESSFUL LOGIN
                                                        blnGameStatus = True
                                                        'ASSIGNING THE VALUES TO THE PROPERTY VARIABLES
                                                        FillLoginDetails(m_dsUserDetails.Tables("UserDetails"), blnGameStatus)
                                                        m_blnLoginSuccess = True
                                                    End If
                                                End If
                                                'AUDIT TRIAL
                                                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Login, User Name :" & txtUsername.Text, 1, 0)
                                            End If
                                        Else
                                            Me.Cursor = Cursors.Default
                                            MessageDialog.Show("Please enter proper username/password", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                            txtUsername.Text = ""
                                            txtPassword.Text = ""
                                            cmbNoDays.Visible = False
                                            cmbNoDays.Enabled = False
                                            txtDays.Visible = False
                                            txtUsername.Focus()

                                        End If
                                    End If
                                Else
                                    ' MessageDialog.Show("Multi Lingual support is not available in this version!", "Login", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    MessageDialog.Show("selected Language Is Not Available In Your System", "Login", MessageDialogButtons.OK, MessageDialogIcon.Warning)

                                End If
                            ElseIf txtUsername.Text.Trim = "" Then
                                Me.Cursor = Cursors.Default
                                MessageDialog.Show("Please, Enter user name", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                txtUsername.Focus()
                            ElseIf txtPassword.Text.Trim = "" Then
                                Me.Cursor = Cursors.Default
                                MessageDialog.Show("Please, Enter password", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                txtPassword.Focus()
                            Else
                                Me.Cursor = Cursors.Default
                                MessageDialog.Show("Incorrect Username/Password. Try Again", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                txtUsername.Focus()
                                Exit Try
                            End If

                            'Dim langse As String = Application.CurrentInputLanguage.Culture.Name.ToString
                            'only if login succeeds, do the following
                            If m_blnLoginSuccess = True And networkConnection Then
                                HideLoginControls()
                                Me.Text = "Select game"
                                btnLogin.Text = "OK"

                                cmbNoDays.Visible = False
                                cmbNoDays.Enabled = False
                                txtDays.Visible = False

                                'multilingual code
                                '---------------------------------------------------------------------------------------------------------
                                Dim strlbltimezone As String = "All times are displayed in"
                                Dim strtime_zone As String = "time zone."
                                If CInt(m_objGameDetails.languageid) <> 1 Then
                                    Me.Text = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), Me.Text)

                                    Dim g4 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "GameCode")
                                    Dim g5 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Kickoff")
                                    Dim g6 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Competition")
                                    Dim g7 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Home Team")
                                    Dim g8 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Away Team")
                                    Dim g9 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Tier")
                                    Dim g10 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "Task")
                                    Dim g11 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "ModuleID")
                                    Dim g12 As String = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), "SerialNo")
                                    strlbltimezone = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), strlbltimezone)
                                    strtime_zone = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), strtime_zone)
                                    lvwGames.Columns(0).Text = g4
                                    lvwGames.Columns(1).Text = g5
                                    lvwGames.Columns(2).Text = g6
                                    lvwGames.Columns(3).Text = g7
                                    lvwGames.Columns(4).Text = g8
                                    lvwGames.Columns(6).Text = g9
                                    lvwGames.Columns(7).Text = g10
                                    lvwGames.Columns(8).Text = g11
                                    btnDelete.Text = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), btnDelete.Text)
                                    btnCancel.Text = lsupport.LanguageTranslateora(CInt(m_objGameDetails.languageid), btnCancel.Text)
                                    'btnLogin.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnLogin.Text)
                                End If
                                '------------------------

                                lnkupcomming.Visible = False
                                'Me.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\\login_bg.jpg")
                                Me.BackgroundImage = Nothing
                                If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then
                                    'commented for prod publish
                                    If ConfigurationSettings.AppSettings("Environment") <> "PRODUCTION" Then
                                        btnDelete.Visible = True
                                    End If
                                    radFeedA.Visible = False
                                    radFeedB.Visible = False
                                Else
                                    btnDelete.Visible = False
                                    radFeedA.Visible = True
                                    radFeedB.Visible = True
                                End If
                                picTopBar.Visible = True
                                picReporterBar.Visible = True
                                'picCompanyLogo.Visible = True
                                picButtonBar.Visible = True

                                lvwGames.Visible = True

                                Me.Controls.Remove(btnLogin)
                                Me.Controls.Remove(btnCancel)
                                picButtonBar.Controls.Add(btnLogin)
                                picButtonBar.Controls.Add(btnCancel)
                                btnLogin.BackColor = Color.FromArgb(58, 111, 229)
                                btnCancel.BackColor = Color.FromArgb(255, 84, 52)
                                btnCancel.Left = picButtonBar.Width - btnCancel.Width - 10 'lvwGames.Left + lvwGames.Width - btnCancel.Width
                                btnCancel.Top = 8 'picButtonBar.Top + 9

                                btnLogin.Left = btnCancel.Left - btnLogin.Width - 5
                                btnLogin.Top = btnCancel.Top
                                If m_dsUserDetails.Tables.Count > 0 Then
                                    'BIND THE GAMES SO AS TO DISPLAY IN THE SELECT GAME SCREEN
                                    BindGames()
                                End If
                                lblTimeZone.Visible = True
                                lblTimeZone.Text = strlbltimezone + " " + m_objGameDetails.TimeZoneName & " " + strtime_zone
                                lvwGames.Columns(1).Text = "Kickoff (" & m_objGameDetails.TimeZoneName & ")"
                            End If
                    End Select     'm_objLoginDetails.GameMode
                    'FPLRS -139
                    If m_blnLoginSuccess = True And networkConnection = False Then
                        If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                            FillGameDetailsOffline()
                            Me.Cursor = Cursors.Default
                            Me.DialogResult = Windows.Forms.DialogResult.OK
                            'GET RULES DATA AND PUTS IN PROPERTY RULES DATASET
                            m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                        End If

                    End If

                Case "OK"
                    Select Case m_objLoginDetails.GameMode

                        Case clsLoginDetails.m_enmGameMode.DEMO

                            If lvwGames.Items.Count > 0 Then
                                If lvwGames.SelectedItems.Count > 0 Then
                                    lvwGames.Select()
                                    FillGameDetails()
                                    m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                                    m_objGameDetails.IsDemoGameSelected = True
                                Else
                                    Me.Cursor = Cursors.Default
                                    MessageDialog.Show("Please, Select the game", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    Exit Try
                                End If
                            End If
                            Me.Cursor = Cursors.Default
                            Me.DialogResult = Windows.Forms.DialogResult.OK
                        Case clsLoginDetails.m_enmGameMode.LIVE
                            If ((cmbval <> lagid) And (lagid <> 0)) Then
                                assignmentlanguagecheck()
                                If lvwGames.Items.Count > 0 Then
                                    If lvwGames.SelectedItems.Count > 0 Then
                                        'AUDIT TRIAL
                                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnLogin.Text, 1, 0)
                                        FillGameDetails()
                                        'If m_objGameDetails.ModuleID = 1 Or m_objGameDetails.ModuleID = 2 Then
                                        '    UpdateXMLSortOrder()
                                        'End If

                                        Me.Cursor = Cursors.Default
                                        Me.DialogResult = Windows.Forms.DialogResult.OK
                                    Else
                                        Me.Cursor = Cursors.Default
                                        MessageDialog.Show("Please, Select the game", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                        Exit Try
                                    End If
                                End If
                            Else
                                If lvwGames.Items.Count > 0 Then
                                    If lvwGames.SelectedItems.Count > 0 Then
                                        'AUDIT TRIAL
                                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnLogin.Text, 1, 0)
                                        'Insert a dashboard alert once the reporter has logged-in and seleted the game
                                        FillGameDetails()
                                        m_objLogin.InsertDashboardAlert(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole)
                                        Me.Cursor = Cursors.Default
                                        Me.DialogResult = Windows.Forms.DialogResult.OK
                                    Else
                                        Me.Cursor = Cursors.Default
                                        MessageDialog.Show("Please, Select the game", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                        Exit Try
                                    End If
                                End If
                            End If
                            'GET RULES DATA AND PUTS IN PROPERTY RULES DATASET
                            m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
                    End Select
            End Select

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageDialog.Show(ex, Me.Text)
        Finally
            Me.Cursor = Cursors.Default
            Application.DoEvents()
            sstMain.Items(0).Text = ""
            'lblStatus.Text = ""
            Application.DoEvents()
            btnLogin.Enabled = True
        End Try
    End Sub
#Region "Language Check"
#Region "Final Check based on avilable language in system , language seleted in login,language assigned for a game"
    Public Sub assignmentlanguagecheck()
        Try
            systemlanguagecheck(lagid)
            If ((cmbval <> lagid) And (lagid <> 0)) Then
                If (stlangcheck = "notset") Then
                    MessageDialog.Show("Assigned Language Is Not Available In Your System.It Automatically changes  language seleted in login", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    m_objGameDetails.languageid = CInt(cmbval)
                    m_objLoginDetails.LanguageID = CInt(cmbval)
                Else
                    If (m_objGameDetails.IsNewDemomode = False) Then
                        MessageDialog.Show("Language selected Differs .It Automatically changes To Assigned Language ", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                    m_objGameDetails.languageid = CInt(lagid)
                    m_objLoginDetails.LanguageID = CInt(lagid)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region
#Region "Check the language avilablke in system"
    Public Sub systemlanguagecheck(ByVal languageid As Integer)
        Try
            str = CStr(lsupport.langselect(CInt(languageid)))
            strlanguagename = CultureInfo.GetCultureInfo("" + str + "").DisplayName.ToString()
            strlanguagename = strlanguagename.Substring(0, strlanguagename.IndexOf("("))
            lanfunc(languageid)
            For i As Integer = 0 To 4
                Dim ss As String = Application.CurrentInputLanguage.Culture.Name.ToString
                If (temparr(i) = ss) Then
                    stlangcheck = "set"
                    Exit For
                Else
                    stlangcheck = "notset"
                End If
            Next
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region
#Region "Getting the language short form based on language id"
    Public Sub lanfunc(ByVal ch As Integer)
        Try
            For i As Integer = 0 To 3
                temparr(i) = ""
            Next
            If (ch = 2) Then
                temparr(0) = "es-ES"
                temparr(1) = "es-GT"
            ElseIf (ch = 3) Then
                temparr(0) = "zh-TW"
                temparr(1) = "zh-CN"
                temparr(2) = "zh-HK"
            ElseIf (ch = 4) Then
                temparr(0) = "ja-JP"
            ElseIf (ch = 5) Then
                temparr(0) = "de-DE"
            ElseIf (ch = 6) Then
                temparr(0) = "fr-FR"
            ElseIf (ch = 7) Then
                temparr(0) = "pt-BR"
            ElseIf (ch = 8) Then
                temparr(0) = "it-IT"
            ElseIf (ch = 9) Then
                temparr(0) = "nl-NL"
                temparr(1) = "nl-BE"
            ElseIf (ch = 10) Then
                temparr(0) = "ar-IQ"
            ElseIf (ch = 13) Then
                temparr(0) = "es-ES"
                temparr(1) = "es-GT"
                temparr(2) = "es-MX"
            ElseIf (ch = 16) Then
                temparr(0) = "ru-RU"
            ElseIf (ch = 1) Then
                temparr(0) = "en-US"
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
#End Region
#End Region

    Private Sub lvwGames_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwGames.Click
        Try
            lagid = 0

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                If lvwGames.SelectedItems.Count > 0 Then
                    If lvwGames.Items.Count = 1 Then
                        lvwGames.Items(0).Selected = True
                    End If
                    lvwGames.Select()
                    m_objGameDetails.GameCode = CInt(m_dsDemoGame.Tables(0).Rows(0).Item("GAME_CODE").ToString)
                    m_objGameDetails.GameDate = CDate(m_dsDemoGame.Tables(0).Rows(0).Item("GAME_DATE"))
                    'If (m_dsDemoGame.Tables(0).Rows(0).Item("GLanguageid").ToString() <> "") Then
                    '    lagid = CInt(m_dsDemoGame.Tables(0).Rows(0).Item("GLanguageid").ToString)
                    'End If
                    'm_objGameDetails.ModuleID = CInt(m_dsDemoGame.Tables(0).Rows(lvwitm.Index).Item("MODULE_ID").ToString)
                End If
            Else
                If lvwGames.Items.Count > 0 Then
                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems

                        If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.Reporter Then

                            Dim intGameCode As String = lvwitm.SubItems(0).Text
                            Dim intModuleID As String = lvwitm.SubItems(10).Text
                            Dim intSerialNo As String = lvwitm.SubItems(12).Text

                            If ((lvwitm.SubItems(11).Text <> "") And lvwitm.SubItems(11).Text <> Nothing) Then
                                lagid = CInt(lvwitm.SubItems(11).Text)
                            End If

                            Dim drs() As DataRow
                            drs = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                            If drs IsNot Nothing Then
                                If drs.Length > 0 Then
                                    m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                                    m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                                    m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)
                                End If
                            End If

                        ElseIf m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then

                            Dim intGameCode As String = lvwitm.SubItems(0).Text
                            Dim intModuleID As String = lvwitm.SubItems(9).Text
                            Dim intSerialNo As String = lvwitm.SubItems(11).Text

                            lvwGames.Items(lvwitm.Index).Selected = True
                            lvwGames.Select()

                            Dim drs() As DataRow = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                            If drs.Length > 0 Then
                                m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)
                                m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(drs(0).Item("Kickoff")), DateTimeHelper.InputFormat.CurrentFormat)
                                m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)
                                m_objGameDetails.SerialNo = CInt(drs(0).Item("SERIAL_NO").ToString)
                                lagid = 1
                            End If

                            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                                Dim dr() As DataRow = m_dsGames.Tables(2).Select("Game_code = " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND REPORTER_ID_B IS NOT NULL ")
                                If dr.Length > 0 Then
                                    radFeedB.Enabled = True
                                Else
                                    radFeedB.Enabled = False
                                End If
                            End If
                        End If
                    Next
                End If

                If m_objLoginDetails.UserType <> clsLoginDetails.m_enmUserType.OPS Then
                    If m_objGameDetails.ModuleID = 2 Then
                        For i As Integer = 0 To lvwGames.Items.Count - 1
                            Dim dt As DateTime = CDate(lvwGames.Items(i).SubItems(13).Text)
                            If (dt.Date = CDate(m_objGameDetails.GameDate).Date) Then
                                If CDbl(lvwGames.Items(i).SubItems(10).Text) = 2 Then
                                    lvwGames.Items(i).Selected = True
                                End If
                            End If
                        Next
                        lvwGames.Select()
                    End If
                End If

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function GetAllLeagues() As DataSet
        Try
            Return m_objclsSelectGame.GetAllLeagues()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Label1.Visible = True
        lblTimeZone.Visible = False
        Label1.Text = "Loading games please wait...."
        Application.DoEvents()
        Dim ar_from_date, ar_to_date As Date
        ar_from_date = dtpFrom.Value
        ar_to_date = dtpTo.Value
        m_objGameDetails.FromDate = "" & Format(ar_from_date, Format(ar_from_date, "MM-dd-yyyy")) & " 12:00:00 AM"
        m_objGameDetails.ToDate = "" & Format(ar_to_date, Format(ar_to_date, "MM-dd-yyyy")) & " 11:59:59 PM"
        m_dsUserDetails = m_objLogin.FillDemoGames(m_objGameDetails.FromDate, m_objGameDetails.ToDate, CShort(cmbLeague.SelectedValue))
        If (m_dsUserDetails.Tables("Games").Rows.Count = 0) Then
            MessageDialog.Show("No games available in selected date range", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
        End If
        'BIND THE GAMES SO AS TO DISPLAY IN THE SELECT GAME SCREEN
        BindDomoGames()
        Label1.Visible = False
        lblTimeZone.Visible = True
        Label1.Text = ""
        Application.DoEvents()
    End Sub
    ''' <summary>
    ''' Closes the login form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' DELETE THE SELECTED GAME DETAILS FROM SQL AND ORACLE
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvwGames.Items.Count > 0 Then
                If MessageDialog.Show("Are you sure to delete the exisiting game data?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    For Each lvwitm As ListViewItem In lvwGames.SelectedItems
                        lvwGames.Items(lvwitm.Index).Selected = True
                        lvwGames.Select()

                        Dim intGameCode As String = lvwitm.SubItems(0).Text
                        Dim intModuleID As String = lvwitm.SubItems(10).Text
                        Dim intSerialNo As String = lvwitm.SubItems(12).Text

                        Dim drs() As DataRow
                        drs = m_dsGames.Tables(2).Select("GAME_CODE= " & intGameCode & " AND MODULE_ID = " & intModuleID & " AND SERIAL_NO = " & intSerialNo)

                        If drs.Length > 0 Then
                            m_objGameDetails.GameCode = CInt(drs(0).Item("GAME_CODE").ToString)

                            If drs(0).Item("FEED_A") IsNot DBNull.Value Then
                                m_objGameDetails.FeedNumber = CInt(drs(0).Item("FEED_A").ToString)
                            ElseIf drs(0).Item("FEED_B") IsNot DBNull.Value Then
                                m_objGameDetails.FeedNumber = CInt(drs(0).Item("FEED_B").ToString)
                            ElseIf drs(0).Item("FEED_C") IsNot DBNull.Value Then
                                m_objGameDetails.FeedNumber = CInt(drs(0).Item("FEED_C").ToString)
                            ElseIf drs(0).Item("FEED_D") IsNot DBNull.Value Then
                                m_objGameDetails.FeedNumber = CInt(drs(0).Item("FEED_D").ToString)
                            End If

                            m_objGameDetails.ModuleID = CInt(drs(0).Item("MODULE_ID").ToString)
                        End If
                        m_objLogin.DeleteCurrentGameData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    Next
                    MessageDialog.Show("Deleted Successfully!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' selects combo item
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLanguage.SelectedIndexChanged
        Try
            'AUDIT TRIAL
            If (cmbLanguage.SelectedValue.ToString() <> Nothing) And (cmbLanguage.SelectedValue.ToString() <> "System.Data.DataRowView") Then
                If m_blnCheck = True Then
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbLanguage.Text, lblLanguage.Text, 1, 0)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radDemo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDemo.CheckedChanged
        Try
            If radDemo.Checked = True Then
                m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO
                cmbLanguage.SelectedIndex = 0
                cmbLanguage.Enabled = False
                txtUsername.Text = ""
                txtUsername.Enabled = False
                txtPassword.Text = ""
                txtPassword.Enabled = False
                lnkupcomming.Visible = False
                cmbNoDays.Visible = False
                cmbNoDays.Enabled = False
                txtDays.Visible = False

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radLive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLive.CheckedChanged
        Try
            If radLive.Checked = True Then
                m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE
                cmbLanguage.Enabled = True
                txtUsername.Enabled = True
                txtPassword.Enabled = True
                lnkupcomming.Visible = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnAddGame_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGame.Click
        Try
            Dim objAddGame As New frmAddDemoGame
            objAddGame.ShowDialog()
            BindGames()
            FillGameDetails()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub GetFeedCoverageInfo(ByVal GameCode As Integer, ByVal ReporterID As Integer, ByVal intFeedNumber As Integer)
        Try
            Dim DsFeedCoverageInfo As New DataSet
            DsFeedCoverageInfo = m_objLogin.GetFeedCoverageInfo(GameCode, ReporterID, intFeedNumber)
            If DsFeedCoverageInfo.Tables.Count > 0 Then
                m_objGameDetails.CoverageLevel = CInt(DsFeedCoverageInfo.Tables(0).Rows(0).Item("COVERAGELEVEL"))
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Private Sub lnkupcomming_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkupcomming.LinkClicked
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lnkupcomming.Text, Nothing, 1, 0)
            m_objLoginDetails.UserId = 9999
            m_objLoginDetails.UserFirstName = "Demo"
            m_objLoginDetails.UserLastName = "Reporter"
            m_objLoginDetails.UserLoginName = "R_DemoReporter"
            If txtUsername.Text.Trim = "" Then
                MessageDialog.Show("Please, Enter user name", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtUsername.Focus()
            ElseIf txtPassword.Text.Trim = "" Then
                MessageDialog.Show("Please, Enter password", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtPassword.Focus()
            ElseIf (txtUsername.Text.Trim.Length > 0) And (txtPassword.Text.Trim.Length > 0) Then
                Application.DoEvents()
                m_dsUserDetails = m_objLogin.ValidateUserDetails(txtUsername.Text.Trim, txtPassword.Text.Trim, m_objGameDetails.languageid, CInt(cmbNoDays.Text))
                If (m_dsUserDetails.Tables.Count > 0) Then
                    If (m_dsUserDetails.Tables("Status").Rows.Count > 0) Then
                        If CInt(m_dsUserDetails.Tables("Status").Rows(0).Item("STATUS")) = 2 Then
                            'NO GAMES ARE ASSIGNED TO THE REPORTER
                            MessageDialog.Show("There are no games assigned to " & txtUsername.Text, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        Else
                            FillLoginDetails(m_dsUserDetails.Tables("UserDetails"), True)
                            Dim objr As New frmUpComingAssignments(m_objLoginDetails.UserId, m_objLoginDetails.UserFirstName & " " & m_objLoginDetails.UserLastName)
                            'Dim objr As New frmUpComingAssignments(CInt(m_dsUserDetails.Tables(0).Rows(0).ItemArray(0).ToString), txtUsername.Text.Trim)
                            objr.ShowDialog()

                        End If
                    End If
                Else
                    MessageDialog.Show("Please enter proper username/password", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    txtUsername.Text = ""
                    txtPassword.Text = ""
                    txtUsername.Focus()
                End If

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtUsername_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUsername.GotFocus
        Try
            Application.DoEvents()
            If boolDisplayFontMessage = False Then
                Dim strMessage As String = "This software needs Arial Unicode font to accurately display various screen elements. To install this font, refer the help page of reporter download web site. Restart the application after installing the font."
                If clsPrerequisiteCheck.IsFontAvailable("ARIALUNI.TTF") = False Then
                    MessageDialog.Show(strMessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                boolDisplayFontMessage = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
#End Region

#Region " DEMO "
    Private Sub InsertDemoData()
        Try
            m_objLogin.DownloadSoccerDemoData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub txtUsername_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUsername.LostFocus
        Try
            Application.DoEvents()
            If txtUsername.Text.Trim.ToLower = "r_arin" Or txtUsername.Text.Trim.ToLower = "r_upen" Or txtUsername.Text.Trim.ToLower = "r_dan" Or txtUsername.Text.Trim.ToLower = "r_hopfinger" Or txtUsername.Text.Trim.ToLower = "r_shirley" Or txtUsername.Text.Trim.ToLower = "r_wrusso" Or txtUsername.Text.Trim.ToLower = "r_snoordermeer" Or txtUsername.Text.Trim.ToLower = "r_dverwaijen" Or txtUsername.Text.Trim.ToLower = "r_monroy" Or txtUsername.Text.Trim.ToLower = "r_flamis" Or txtUsername.Text.Trim.ToLower = "r_pfeiffer" Or txtUsername.Text.Trim.ToLower = "r_mangurten" Or txtUsername.Text.Trim.ToLower = "r_koramania15" Then
                cmbNoDays.Visible = True
                cmbNoDays.Enabled = True
                cmbNoDays.SelectedIndex = 0
                txtDays.Visible = True
            Else
                cmbNoDays.Text = "1"
                cmbNoDays.Visible = False
                cmbNoDays.Enabled = False
                txtDays.Visible = False
            End If

        Catch ex As Exception
        End Try
    End Sub
End Class