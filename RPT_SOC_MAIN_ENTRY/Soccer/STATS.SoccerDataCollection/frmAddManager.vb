﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmAddManager
' Author        : Shravani
' Created Date  : 03-JUL-09
' Description   : This form is used to add the Coach for Teams
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmAddManager

#Region " Constants & Variables "
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objValidation As New clsValidation
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objAddManager As New clsAddManager
    Private m_intTeamID As Integer
    Private m_strNupID As String
    Private m_dsManager As DataSet
    Private strSelButton As String
    Private MessageDialog As New frmMessageDialog
    Private StrTeamChecked As String
    Private lsupport As New languagesupport
    Dim strmessage As String = "Manager Added Successfully"
    Dim strmessage1 As String = "Manager updated Successfully"
    Dim strmessage2 As String = "Manager Last Name is Not Entered"
    Dim strmessage3 As String = "Selected Manager is deleted successfully"
    Dim strmessage4 As String = "This Manager can not be deleted. The official"
    Dim strmessage5 As String = "is already assigned to the game thru Team set-up screen"
    Dim strmessage6 As String = "Please Apply or Ignore the changes"
    Dim strmessage7 As String = "Please select the manager" 'New Message
    Dim strmessage8 As String = "Are You Sure want to delete this Manager?"

#End Region

    Private Sub frmAddManager_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmAddManager", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            'ASSIGNING HOME AND AWAY TEAM NAMES TO THE RADIO BUTTONS
            'radHomeTeam.Text = m_objGameDetails.HomeTeam
            'radAwayTeam.Text = m_objGameDetails.AwayTeam

            If m_objGameDetails.HomeTeamAbbrev.Length > 12 Then
                radHomeTeam.Text = m_objGameDetails.HomeTeamAbbrev.Substring(0, 12)
            Else
                radHomeTeam.Text = m_objGameDetails.HomeTeamAbbrev
            End If

            If m_objGameDetails.AwayTeamAbbrev.Length > 12 Then
                radAwayTeam.Text = m_objGameDetails.AwayTeamAbbrev.Substring(0, 12)
            Else
                radAwayTeam.Text = m_objGameDetails.AwayTeamAbbrev
            End If

            'BY DEFAULT SELECT THE HOME TEAM RADIO BUTTON
            'radHomeTeam.Checked = True
            'radAwayTeam.Checked = False
            'lblTeam.Text = radHomeTeam.Text
            If StrTeamChecked = "Home" Then
                radHomeTeam.Checked = True
                radAwayTeam.Checked = False
                lblTeam.Text = radHomeTeam.Text
            ElseIf StrTeamChecked = "Away" Then
                radHomeTeam.Checked = False
                radAwayTeam.Checked = True
                lblTeam.Text = radAwayTeam.Text
            End If

            EnableDisableButton(True, False, False, False, False)
            EnableDisableTextBox(False, False, False)
            lvwManager.Enabled = True

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                lblFirstName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblFirstName.Text)
                lblLastName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblLastName.Text)

                btnAdd.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAdd.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnApply.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnApply.Text)
                btnIgnore.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnIgnore.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                radHomeTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radHomeTeam.Text)
                radAwayTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radAwayTeam.Text)
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Manager")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
                strmessage5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage5)
                strmessage6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage6)
                strmessage7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage7)
                strmessage8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage8)
                lvwManager.Columns(0).Text = g1
                lvwManager.Columns(1).Text = g2

            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radHomeTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radHomeTeam.CheckedChanged
        Try
            If radHomeTeam.Checked = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radHomeTeam.Text, 1, 0)

                lblTeam.Text = radHomeTeam.Text.Trim
                FetchTeamID(radHomeTeam.Text)
                'LOGO TO DISPLAY
                FetchTeamLogo(m_intTeamID)

                LoadExistingManagers()

            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radAwayTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAwayTeam.CheckedChanged
        Try
            If radAwayTeam.Checked = True Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radAwayTeam.Text, 1, 0)
                lblTeam.Text = radAwayTeam.Text.Trim
                FetchTeamID(radAwayTeam.Text)
                'LOGO TO DISPLAY
                FetchTeamLogo(m_intTeamID)

                LoadExistingManagers()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnIgnore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnIgnore.Text, 1, 0)
            strSelButton = "Add"
            EnableDisableButton(True, False, False, False, False)
            EnableDisableTextBox(False, False, True)
            ClearTextBoxes()
            btnAdd.Focus()
            radAwayTeam.Enabled = True
            radHomeTeam.Enabled = True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lvwManager_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwManager.Click
        Try
            ListviewSelectedItemDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Try
                If (strSelButton = "Save" Or strSelButton = "Update") Then
                    MessageDialog.Show(strmessage6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    btnApply.Focus()
                Else
                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, 1, 0)
                    Me.Close()
                    Me.DialogResult = Windows.Forms.DialogResult.Yes
                End If

            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAdd.Text, 1, 0)
            strSelButton = "Save"
            EnableDisableButton(False, False, False, True, True)
            EnableDisableTextBox(True, True, False)
            txtFirstName.Focus()
            ClearTextBoxes()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, 1, 0)
            strSelButton = "Update"
            EnableDisableButton(False, False, False, True, True)
            EnableDisableTextBox(True, True, False)
            If radAwayTeam.Checked = True Then
                radHomeTeam.Enabled = False
            ElseIf radHomeTeam.Checked = True Then
                radAwayTeam.Enabled = False
            End If

            txtFirstName.Focus()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, 1, 0)
            If lvwManager.SelectedItems.Count > 0 Then
                DeleteManager()
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, True)
                ClearTextBoxes()
                strSelButton = "Add"
                lvwManager.Focus()
            Else
                MessageDialog.Show(strmessage7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnApply.Text, 1, 0)

            If (RequireFieldValidation()) Then
                InsertEditManager()
                LoadExistingManagers()
                strSelButton = "Add"
                ClearTextBoxes()
                EnableDisableButton(True, False, False, False, False)
                EnableDisableTextBox(False, False, True)
                btnAdd.Focus()
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub txtFirstName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFirstName.KeyPress
        Try
            'If Char.IsLetter(e.KeyChar) <> True And Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
            '    e.Handled = True
            'End If
            If Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtLastName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLastName.KeyPress
        Try
            'If Char.IsLetter(e.KeyChar) <> True And Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
            '    e.Handled = True
            'End If
            If Asc(e.KeyChar) <> 8 And Char.IsDigit(e.KeyChar) <> False Then
                e.Handled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

#Region "user defined functions"

    'FETCHING FROM DATABASE AND STORE IN A DATASET
    Private Sub LoadExistingManagers()
        Try
            m_dsManager = m_objAddManager.SelectAddManager(m_objGameDetails.GameCode, m_intTeamID, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")))
            lvwManager.Items.Clear()

            If Not m_dsManager.Tables(0) Is Nothing Then
                For Each drNewManager As DataRow In m_dsManager.Tables(0).Rows
                    Dim lvItem As New ListViewItem
                    If CStr(drNewManager("MONIKER")) <> "" Then
                        lvItem = New ListViewItem(drNewManager("LAST_NAME").ToString() + ", " + drNewManager("MONIKER").ToString())
                    Else
                        lvItem = New ListViewItem(drNewManager("LAST_NAME").ToString())
                    End If
                    lvItem.SubItems.Add(drNewManager("TEAM_ABBREV").ToString)
                    lvItem.SubItems.Add(drNewManager("COACH_ID").ToString)
                    lvwManager.Items.Add(lvItem)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHING LOGO FOR SELECTED TEAM
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamLogo(ByVal TeamID As Integer)
        Try
            Dim v_memLogo As MemoryStream
            picTeamLogo.Image = Nothing
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, TeamID)
            If v_memLogo IsNot Nothing Then
                picTeamLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHING TEAM ID BY PASSING TEAM NAME AS PARAMETER
    ''' </summary>
    ''' <param name="Teamname"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamID(ByVal Teamname As String)
        Try
            If Teamname = m_objGameDetails.HomeTeamAbbrev Then
                m_intTeamID = m_objGameDetails.HomeTeamID
            Else
                m_intTeamID = m_objGameDetails.AwayTeamID
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableButton(ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean)
        Try
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
            btnDelete.Enabled = blnButtonDelete
            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableTextBox(ByVal blnTextBoxMoniker As Boolean, ByVal blnTextBoxLastName As Boolean, ByVal blnListViewManager As Boolean)
        Try
            txtFirstName.Enabled = blnTextBoxMoniker
            txtLastName.Enabled = blnTextBoxLastName
            lvwManager.Enabled = blnListViewManager
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearTextBoxes()
        Try
            txtFirstName.Text = ""
            txtLastName.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DeleteManager()
        Try
            Dim dsOfficial As New DataSet
            If (MessageDialog.Show(strmessage8, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                m_strNupID = lvwManager.SelectedItems(0).SubItems(2).Text
                dsOfficial = m_objAddManager.InsertManager(m_strNupID, m_intTeamID, txtFirstName.Text, txtLastName.Text, m_objGameDetails.ReporterRole, m_objGameDetails.LeagueID, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")), "DELETE", m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                If dsOfficial.Tables.Count > 0 Then
                    If CDbl(dsOfficial.Tables(0).Rows(0).Item(0).ToString) = 1 Then
                        MessageDialog.Show(strmessage4 + " " + strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Else
                        MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        LoadExistingManagers()
                    End If
                End If
                ClearTextBoxes()
                Me.DialogResult = Windows.Forms.DialogResult.Yes
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ListviewSelectedItemDisplay()
        Try
            EnableDisableButton(True, True, True, False, False)
            EnableDisableTextBox(False, False, True)
            SelectedListviewDisplay()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SelectedListviewDisplay()
        Try
            Dim intNupID As Integer
            Dim listselectDatarow() As DataRow
            For Each lvwitm As ListViewItem In lvwManager.SelectedItems
                intNupID = lvwitm.SubItems(2).Text
            Next
            listselectDatarow = m_dsManager.Tables(0).Select("COACH_ID='" & intNupID & "'")
            If listselectDatarow.Length > 0 Then
                txtFirstName.Text = listselectDatarow(0).Item(1).ToString()
                txtLastName.Text = listselectDatarow(0).Item(0).ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function RequireFieldValidation() As Boolean
        Try

            'If txtFirstName.Text = "" Then
            '    essageDialog.Show("Manager First Name is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            '    txtFirstName.Focus()
            '    Return False
            'Else
            If txtLastName.Text = "" Then
                MessageDialog.Show(strmessage2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtLastName.Focus()
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub InsertEditManager()
        Try
            If strSelButton = "Save" Then
                m_strNupID = "98" & Format(Convert.ToInt32(m_intTeamID), "00000") & "00"
                m_objAddManager.InsertManager(m_strNupID, m_intTeamID, txtFirstName.Text, txtLastName.Text, m_objGameDetails.ReporterRole, m_objGameDetails.LeagueID, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")), "ADD", m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Me.DialogResult = Windows.Forms.DialogResult.Yes
            ElseIf strSelButton = "Update" Then
                m_strNupID = lvwManager.SelectedItems(0).SubItems(2).Text
                m_objAddManager.InsertManager(m_strNupID, m_intTeamID, txtFirstName.Text, txtLastName.Text, m_objGameDetails.ReporterRole, m_objGameDetails.LeagueID, CChar(IIf(m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE, "N", "Y")), "EDIT", m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Me.DialogResult = Windows.Forms.DialogResult.Yes
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Public Sub New(ByVal TeamChecked As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        StrTeamChecked = TeamChecked
        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class