﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports STATS.Utility
Imports System.IO
#End Region

Public Class frmAddTeam

#Region " Constants & Variables "
    Private m_objAddTeam As clsAddTeam = clsAddTeam.GetInstance()
    Private m_blnIsLeagueComboLoaded As Boolean = False
    Private m_strCheckButtonOption As String
    Private m_intTeamID As Integer
    Private m_intLeagueID As Integer
    Private m_strTeamName As String
    Private m_strTeamNickName As String
    Private m_strTeamAbbrev As String
    Private m_dsAddTeam As New DataSet
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private lsupport As New languagesupport
    Private message1 As String = "Team Details Added Successfully"
    Private message2 As String = "Team Details Updated Successfully"
    Private message3 As String = "Please Apply or Ignore the changes"
    Private message4 As String = "Are you sure you want to delete?"
    Private message5 As String = "Cannot Delete the Team, Unless Game is Deleted"
    Private message6 As String = "Team Deleted Successfully"
    Private message7 As String = "Team Name is Not Entered"
    Private message8 As String = "Team Abbreviation is Not Entered"
    Private message9 As String = "Select a Leauge to Which the Team Belongs"
    Private message10 As String = "Please select valid league"
    Private message11 As String = "Currently there is no Team available"

#End Region

#Region " Event Handlers"

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmAddTeam_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAddTeam", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            LoadLeagueCombo()
            LoadTeamDetails()
            lvwTeam.Enabled = True
            EnableDisableButton(False, False, False, True, False)
            EnableDisableComboandTextBox(False, False, False, False)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                Label8.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label8.Text)
                Label1.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label1.Text)
                Label3.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label3.Text)
                btnAdd.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAdd.Text)
                btnEdit.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnEdit.Text)
                btnDelete.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnDelete.Text)
                btnApply.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnApply.Text)
                btnIgnore.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnIgnore.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnAddPlayer.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnAddPlayer.Text)
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Name")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Abbrevation")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "League")
                lvwTeam.Columns(0).Text = g1
                lvwTeam.Columns(1).Text = g2
                lvwTeam.Columns(2).Text = g3
                message1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message1)
                message2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message2)
                message3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message3)
                message5 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message5)
                message6 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message6)
                message7 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message7)
                message8 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message8)
                message9 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message9)
                message10 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message10)
                message11 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message11)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAdd.Text, Nothing, 1, 0)
            ClearTextboxes()
            m_strCheckButtonOption = "Save"
            lvwTeam.Enabled = False
            EnableDisableButton(True, True, False, False, False)
            EnableDisableComboandTextBox(True)
            CmbLeague.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        lvwTeam.Enabled = True
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnApply.Text, Nothing, 1, 0)
            If (RequireFieldValidation()) Then
                If (m_strCheckButtonOption = "Save") Then
                    m_objAddTeam.InsertAddTeam(CInt(CmbLeague.SelectedValue.ToString), txtTeamName.Text.Trim(), "", txtTeamAbbrev.Text.Trim())
                    LoadTeamDetails()
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                ElseIf (m_strCheckButtonOption = "Update") Then
                    m_objAddTeam.UpdateAddTeam(m_intTeamID, CInt((CmbLeague.SelectedValue.ToString)), txtTeamName.Text.Trim(), "", txtTeamAbbrev.Text.Trim())
                    LoadTeamDetails()
                    MessageDialog.Show(message2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
                'LoadTeamDetails()
                EnableDisableComboandTextBox(False)
                CmbLeague.Focus()
                EnableDisableButton(False, False, False, True, False)
                m_strCheckButtonOption = "Add"
                ClearTextboxes()
                btnAdd.Focus()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show(message3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnEdit.Text, Nothing, 1, 0)
            lvwTeam.Enabled = False
            EnableDisableComboandTextBox(True, True, True, True)
            m_strCheckButtonOption = "Update"
            CmbLeague.Focus()
            EnableDisableButton(True, True, False, False, False)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnDelete.Text, Nothing, 1, 0)
            Dim teamid As Integer
            Dim dsTeamCnt As New DataSet
            lvwTeam.Enabled = False
            If (MessageDialog.Show(message4, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                teamid = m_intTeamID
                dsTeamCnt = m_objAddTeam.DeleteAddTeam(m_intTeamID)

                If CDbl(dsTeamCnt.Tables(0).Rows(0).Item(0).ToString) > 0 Then
                    MessageDialog.Show(message5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    CmbLeague.Focus()
                    CmbLeague.SelectedIndex = -1
                    EnableDisableButton(False, False, True, True, True)
                    lvwTeam.Enabled = True
                Else
                    EnableDisableComboandTextBox(False, False, False, False)
                    EnableDisableButton(False, False, False, True, False)
                    LoadTeamDetails()
                    MessageDialog.Show(message6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    ClearTextboxes()
                    m_strCheckButtonOption = "Add"
                    lvwTeam.Enabled = True
                End If
            Else
                EnableDisableButton(False, False, True, True, True)
                lvwTeam.Enabled = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lvwTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwTeam.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListBoxClicked, lvwTeam.Text, Nothing, 1, 0)
            EnableDisableButton(False, False, True, True, True)
            For Each lvwitm As ListViewItem In lvwTeam.SelectedItems
                m_intLeagueID = CInt(m_dsAddTeam.Tables(0).Rows(lvwitm.Index).Item("LEAGUE_ID").ToString)
                m_intTeamID = CInt(m_dsAddTeam.Tables(0).Rows(lvwitm.Index).Item("TEAM_ID").ToString)
            Next
            SelectedListviewDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmAddTeam_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If (m_strCheckButtonOption = "Save" Or m_strCheckButtonOption = "Update") Then
                MessageDialog.Show(message3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                btnIgnore.Focus()
                e.Cancel = True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnIgnore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnIgnore.Text, Nothing, 1, 0)
            m_strCheckButtonOption = "Add"
            ClearTextboxes()
            EnableDisableComboandTextBox(False)
            EnableDisableButton(False, False, False, True, False)
            lvwTeam.Enabled = True
            btnAdd.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region " UserDefined Functions "

    Private Sub LoadLeagueCombo()
        Try
            Dim dtLeague As New DataTable
            Dim dsLeague As New DataSet
            dsLeague = m_objAddTeam.GetLeague()
            dtLeague = dsLeague.Tables(0).Copy
            LoadControl(CmbLeague, dtLeague, "LEAGUE_ABBREV", "LEAGUE_ID")
            m_blnIsLeagueComboLoaded = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub EnableDisableButton(ByVal blnButtonApply As Boolean, ByVal blnButtonIgnore As Boolean, ByVal blnButtonDelete As Boolean, ByVal blnButtonadd As Boolean, ByVal blnButtonEdit As Boolean)
        Try
            btnApply.Enabled = blnButtonApply
            btnIgnore.Enabled = blnButtonIgnore
            btnDelete.Enabled = blnButtonDelete
            btnAdd.Enabled = blnButtonadd
            btnEdit.Enabled = blnButtonEdit
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableComboandTextBox(ByVal blnComboLeague As Boolean, ByVal blnTeamName As Boolean, ByVal blnTeamNickName As Boolean, ByVal blnTeamAbbrev As Boolean)
        Try
            CmbLeague.Enabled = blnComboLeague
            txtTeamName.Enabled = blnTeamName
            txtTeamAbbrev.Enabled = blnTeamAbbrev
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearTextboxes()
        Try
            txtTeamName.Text = ""
            txtTeamAbbrev.Text = ""
            CmbLeague.SelectedIndex = -1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableDisableComboandTextBox(ByVal type As Boolean)
        Try
            txtTeamName.Enabled = type
            txtTeamAbbrev.Enabled = type
            CmbLeague.Enabled = type
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadTeamDetails()
        Try
            m_dsAddTeam = m_objAddTeam.SelectAddTeam()
            lvwTeam.Items.Clear()
            If Not m_dsAddTeam.Tables(0) Is Nothing Then
                For Each drTeam As DataRow In m_dsAddTeam.Tables(0).Rows
                    Dim lvItem As New ListViewItem(drTeam.ItemArray(2).ToString() & "  " & drTeam.ItemArray(3).ToString()) 'TeamName
                    lvItem.SubItems.Add(drTeam.ItemArray(4).ToString)
                    lvItem.SubItems.Add(drTeam.ItemArray(0).ToString)
                    lvwTeam.Items.Add(lvItem)
                Next
                lvwTeam.Items(0).Selected = True
                lvwTeam.EnsureVisible(0)
                Application.DoEvents()
            Else
                MessageDialog.Show(message11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Me.Close()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Checking The mandatory Field are Filled or not

    Public Function RequireFieldValidation() As Boolean
        Try
            Dim clsvalid As New Utility.clsValidation
            If (Not clsvalid.ValidateEmpty(txtTeamName.Text)) Then
                MessageDialog.Show(message7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtTeamName.Focus()
                Return False
                'ElseIf (Not clsvalid.ValidateEmpty(txtTeamNickName.Text)) Then
                '    essageDialog.Show("Team Nick Name is Not Entered", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                '    txtTeamNickName.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(txtTeamAbbrev.Text)) Then
                MessageDialog.Show(message8, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                txtTeamAbbrev.Focus()
                Return False
            ElseIf (Not clsvalid.ValidateEmpty(CmbLeague.Text)) Then
                MessageDialog.Show(message9, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbLeague.Focus()
                CmbLeague.SelectedIndex = -1
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Function
    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString())
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SelectedListviewDisplay()
        Dim teamid As Integer
        Try
            Dim listselectDatarow() As DataRow
            For Each lvwitm As ListViewItem In lvwTeam.SelectedItems
                teamid = m_intTeamID
            Next
            listselectDatarow = m_dsAddTeam.Tables(0).Select("TEAM_ID='" & teamid & "'")
            If listselectDatarow.Length > 0 Then
                CmbLeague.SelectedValue = listselectDatarow(0).ItemArray(6).ToString()
                txtTeamName.Text = listselectDatarow(0).ItemArray(2).ToString()
                txtTeamAbbrev.Text = listselectDatarow(0).ItemArray(4).ToString()
                EnableDisableButton(False, False, True, True, True)
            End If
            EnableDisableComboandTextBox(False)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub btnAddPlayer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPlayer.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnAddPlayer.Text, Nothing, 1, 0)
            Dim objAddPlayer As New frmAddDemoPlayer
            objAddPlayer.ShowDialog()
            If m_blnIsLeagueComboLoaded = True Then
                LoadTeamDetails()
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbLeague_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbLeague.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, CmbLeague.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtTeamAbbrev_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTeamAbbrev.Leave
        Try
            If (txtTeamAbbrev.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTeamAbbrev.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtTeamName_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTeamName.Leave
        Try
            If (txtTeamName.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTeamName.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbLeague_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbLeague.Validated
        Try
            If CmbLeague.Text <> "" And CmbLeague.SelectedValue Is Nothing Then
                MessageDialog.Show(message10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                CmbLeague.Text = ""
                CmbLeague.Focus()
                Exit Try
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class