﻿Imports System.Windows.Forms.Design
Imports STATS.SoccerBL
Imports STATS.Utility
Imports Microsoft.Win32
Imports System.IO
Imports System.Text
Public Class frmTeamSelection

    Private ReadOnly _objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private _objActionDetails As ClsActionDetails = ClsActionDetails.GetInstance()

    Private Sub frmTeamSelection_Load(sender As Object, e As EventArgs) Handles Me.Load
        btnAwayTeam.Text = _objGameDetails.AwayTeam
        btnAwayTeam.Tag = _objGameDetails.AwayTeamID
        btnHomeTeam.Text = _objGameDetails.HomeTeam
        btnHomeTeam.Tag = _objGameDetails.HomeTeamID
        Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
         If _objGameDetails.languageid <> 1 Then
                Dim lsupport As New languagesupport
                lsupport.FormLanguageTranslate(Me)
            End If
    End Sub

    Private Sub TeamSelect_Click(sender As Object, e As EventArgs) Handles btnAwayTeam.Click, btnHomeTeam.Click
        _objActionDetails.TeamId = CType(DirectCast(sender, Windows.Forms.Control), Button).Tag
        _objActionDetails.EventId = Nothing
        Me.Close()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class