﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPenaltyShootoutPz
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstDefence = New System.Windows.Forms.ListBox()
        Me.picAwayLogo = New System.Windows.Forms.PictureBox()
        Me.pnlClockBar = New System.Windows.Forms.Panel()
        Me.lklAwayteam = New System.Windows.Forms.LinkLabel()
        Me.lklHometeam = New System.Windows.Forms.LinkLabel()
        Me.lblScoreAway = New System.Windows.Forms.Label()
        Me.lblScoreHome = New System.Windows.Forms.Label()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.picHomeLogo = New System.Windows.Forms.PictureBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.pnlSave = New System.Windows.Forms.Panel()
        Me.lstShotType = New System.Windows.Forms.ListBox()
        Me.lstPlayer = New System.Windows.Forms.ListBox()
        Me.lblMissType = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.lstType = New System.Windows.Forms.ListBox()
        Me.lblBodyPart = New System.Windows.Forms.Label()
        Me.lstBodyPart = New System.Windows.Forms.ListBox()
        Me.lstResult = New System.Windows.Forms.ListBox()
        Me.LblResult = New System.Windows.Forms.Label()
        Me.lblDefendingPlayer = New System.Windows.Forms.Label()
        Me.lblShootingPlayer = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.rdoHome = New System.Windows.Forms.RadioButton()
        Me.rdoAway = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pnlGoalframe = New System.Windows.Forms.Panel()
        Me.UdcSoccerGoalFrame1 = New STATS.SoccerDataCollection.udcSoccerGoalFrame()
        Me.pnlInfo = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblControlCaption = New System.Windows.Forms.Label()
        Me.btnZoom = New System.Windows.Forms.Button()
        Me.picGFOrangeBlink = New System.Windows.Forms.PictureBox()
        Me.picGFGreenBlink = New System.Windows.Forms.PictureBox()
        Me.lblWidth = New System.Windows.Forms.Label()
        Me.picGFOrange = New System.Windows.Forms.PictureBox()
        Me.picGFGreen = New System.Windows.Forms.PictureBox()
        Me.lblClickLocationOrange = New System.Windows.Forms.Label()
        Me.lblClickLocationGreen = New System.Windows.Forms.Label()
        Me.lblMoveZ = New System.Windows.Forms.Label()
        Me.lblMoveY = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.dgvPBP = New System.Windows.Forms.DataGridView()
        Me.txtSequence = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblAlert = New System.Windows.Forms.Label()
        Me.grpdirection = New System.Windows.Forms.GroupBox()
        Me.rdoRight = New System.Windows.Forms.RadioButton()
        Me.rdoLeft = New System.Windows.Forms.RadioButton()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClockBar.SuspendLayout()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSave.SuspendLayout()
        Me.pnlGoalframe.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        CType(Me.picGFOrangeBlink, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picGFGreenBlink, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picGFOrange, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picGFGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPBP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpdirection.SuspendLayout()
        Me.SuspendLayout()
        '
        'lstDefence
        '
        Me.lstDefence.FormattingEnabled = True
        Me.lstDefence.HorizontalScrollbar = True
        Me.lstDefence.IntegralHeight = False
        Me.lstDefence.Location = New System.Drawing.Point(193, 21)
        Me.lstDefence.Name = "lstDefence"
        Me.lstDefence.Size = New System.Drawing.Size(150, 195)
        Me.lstDefence.TabIndex = 337
        '
        'picAwayLogo
        '
        Me.picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picAwayLogo.Location = New System.Drawing.Point(997, 35)
        Me.picAwayLogo.Name = "picAwayLogo"
        Me.picAwayLogo.Size = New System.Drawing.Size(45, 39)
        Me.picAwayLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAwayLogo.TabIndex = 321
        Me.picAwayLogo.TabStop = False
        '
        'pnlClockBar
        '
        Me.pnlClockBar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnlClockBar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.pnlClockBar.Controls.Add(Me.lklAwayteam)
        Me.pnlClockBar.Controls.Add(Me.lklHometeam)
        Me.pnlClockBar.Controls.Add(Me.lblScoreAway)
        Me.pnlClockBar.Controls.Add(Me.lblScoreHome)
        Me.pnlClockBar.Controls.Add(Me.lblPeriod)
        Me.pnlClockBar.Location = New System.Drawing.Point(59, 35)
        Me.pnlClockBar.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlClockBar.Name = "pnlClockBar"
        Me.pnlClockBar.Size = New System.Drawing.Size(915, 40)
        Me.pnlClockBar.TabIndex = 319
        '
        'lklAwayteam
        '
        Me.lklAwayteam.ActiveLinkColor = System.Drawing.Color.White
        Me.lklAwayteam.AutoSize = True
        Me.lklAwayteam.DisabledLinkColor = System.Drawing.Color.White
        Me.lklAwayteam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklAwayteam.ForeColor = System.Drawing.Color.White
        Me.lklAwayteam.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lklAwayteam.LinkColor = System.Drawing.Color.White
        Me.lklAwayteam.Location = New System.Drawing.Point(662, 8)
        Me.lklAwayteam.Name = "lklAwayteam"
        Me.lklAwayteam.Size = New System.Drawing.Size(108, 25)
        Me.lklAwayteam.TabIndex = 22
        Me.lklAwayteam.TabStop = True
        Me.lklAwayteam.Text = "Away team"
        Me.lklAwayteam.VisitedLinkColor = System.Drawing.Color.White
        '
        'lklHometeam
        '
        Me.lklHometeam.ActiveLinkColor = System.Drawing.Color.White
        Me.lklHometeam.AutoSize = True
        Me.lklHometeam.DisabledLinkColor = System.Drawing.Color.White
        Me.lklHometeam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklHometeam.ForeColor = System.Drawing.Color.White
        Me.lklHometeam.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lklHometeam.LinkColor = System.Drawing.Color.White
        Me.lklHometeam.Location = New System.Drawing.Point(26, 8)
        Me.lklHometeam.Name = "lklHometeam"
        Me.lklHometeam.Size = New System.Drawing.Size(112, 25)
        Me.lklHometeam.TabIndex = 0
        Me.lklHometeam.TabStop = True
        Me.lklHometeam.Text = "Home team"
        Me.lklHometeam.VisitedLinkColor = System.Drawing.Color.White
        '
        'lblScoreAway
        '
        Me.lblScoreAway.AutoSize = True
        Me.lblScoreAway.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreAway.ForeColor = System.Drawing.Color.White
        Me.lblScoreAway.Location = New System.Drawing.Point(497, 8)
        Me.lblScoreAway.Name = "lblScoreAway"
        Me.lblScoreAway.Size = New System.Drawing.Size(23, 25)
        Me.lblScoreAway.TabIndex = 17
        Me.lblScoreAway.Text = "0"
        '
        'lblScoreHome
        '
        Me.lblScoreHome.AutoSize = True
        Me.lblScoreHome.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreHome.ForeColor = System.Drawing.Color.White
        Me.lblScoreHome.Location = New System.Drawing.Point(249, 8)
        Me.lblScoreHome.Name = "lblScoreHome"
        Me.lblScoreHome.Size = New System.Drawing.Size(23, 25)
        Me.lblScoreHome.TabIndex = 16
        Me.lblScoreHome.Text = "0"
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblPeriod.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.ForeColor = System.Drawing.Color.White
        Me.lblPeriod.Location = New System.Drawing.Point(331, 11)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(112, 18)
        Me.lblPeriod.TabIndex = 6
        Me.lblPeriod.Text = "Penalty Shoot-out"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picTopBar
        '
        Me.picTopBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-17, 2)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(1066, 15)
        Me.picTopBar.TabIndex = 311
        Me.picTopBar.TabStop = False
        '
        'picHomeLogo
        '
        Me.picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picHomeLogo.Location = New System.Drawing.Point(-2, 35)
        Me.picHomeLogo.Name = "picHomeLogo"
        Me.picHomeLogo.Size = New System.Drawing.Size(45, 39)
        Me.picHomeLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picHomeLogo.TabIndex = 320
        Me.picHomeLogo.TabStop = False
        '
        'picButtonBar
        '
        Me.picButtonBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(-18, 596)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(1067, 38)
        Me.picButtonBar.TabIndex = 322
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(975, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 288
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'picReporterBar
        '
        Me.picReporterBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-18, 14)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(1067, 21)
        Me.picReporterBar.TabIndex = 312
        Me.picReporterBar.TabStop = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(793, 485)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 228
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'pnlSave
        '
        Me.pnlSave.BackColor = System.Drawing.Color.Lavender
        Me.pnlSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSave.Controls.Add(Me.lstShotType)
        Me.pnlSave.Controls.Add(Me.lstPlayer)
        Me.pnlSave.Controls.Add(Me.lblMissType)
        Me.pnlSave.Controls.Add(Me.lblType)
        Me.pnlSave.Controls.Add(Me.lstType)
        Me.pnlSave.Controls.Add(Me.lblBodyPart)
        Me.pnlSave.Controls.Add(Me.lstBodyPart)
        Me.pnlSave.Controls.Add(Me.lstResult)
        Me.pnlSave.Controls.Add(Me.LblResult)
        Me.pnlSave.Controls.Add(Me.lstDefence)
        Me.pnlSave.Controls.Add(Me.lblDefendingPlayer)
        Me.pnlSave.Controls.Add(Me.lblShootingPlayer)
        Me.pnlSave.Controls.Add(Me.Label5)
        Me.pnlSave.Location = New System.Drawing.Point(2, 104)
        Me.pnlSave.Name = "pnlSave"
        Me.pnlSave.Size = New System.Drawing.Size(628, 222)
        Me.pnlSave.TabIndex = 318
        '
        'lstShotType
        '
        Me.lstShotType.FormattingEnabled = True
        Me.lstShotType.IntegralHeight = False
        Me.lstShotType.Location = New System.Drawing.Point(473, 21)
        Me.lstShotType.Name = "lstShotType"
        Me.lstShotType.Size = New System.Drawing.Size(150, 195)
        Me.lstShotType.TabIndex = 354
        '
        'lstPlayer
        '
        Me.lstPlayer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstPlayer.FormattingEnabled = True
        Me.lstPlayer.HorizontalScrollbar = True
        Me.lstPlayer.IntegralHeight = False
        Me.lstPlayer.Location = New System.Drawing.Point(5, 21)
        Me.lstPlayer.Name = "lstPlayer"
        Me.lstPlayer.Size = New System.Drawing.Size(183, 195)
        Me.lstPlayer.TabIndex = 353
        '
        'lblMissType
        '
        Me.lblMissType.AutoSize = True
        Me.lblMissType.Location = New System.Drawing.Point(478, 5)
        Me.lblMissType.Name = "lblMissType"
        Me.lblMissType.Size = New System.Drawing.Size(55, 13)
        Me.lblMissType.TabIndex = 352
        Me.lblMissType.Text = "Miss Type"
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Location = New System.Drawing.Point(348, 158)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(31, 13)
        Me.lblType.TabIndex = 351
        Me.lblType.Text = "Type"
        '
        'lstType
        '
        Me.lstType.FormattingEnabled = True
        Me.lstType.Location = New System.Drawing.Point(348, 173)
        Me.lstType.Name = "lstType"
        Me.lstType.Size = New System.Drawing.Size(121, 43)
        Me.lstType.TabIndex = 312
        '
        'lblBodyPart
        '
        Me.lblBodyPart.AutoSize = True
        Me.lblBodyPart.Location = New System.Drawing.Point(348, 95)
        Me.lblBodyPart.Name = "lblBodyPart"
        Me.lblBodyPart.Size = New System.Drawing.Size(53, 13)
        Me.lblBodyPart.TabIndex = 350
        Me.lblBodyPart.Text = "Body Part"
        '
        'lstBodyPart
        '
        Me.lstBodyPart.FormattingEnabled = True
        Me.lstBodyPart.Location = New System.Drawing.Point(348, 110)
        Me.lstBodyPart.Name = "lstBodyPart"
        Me.lstBodyPart.Size = New System.Drawing.Size(121, 43)
        Me.lstBodyPart.TabIndex = 340
        '
        'lstResult
        '
        Me.lstResult.FormattingEnabled = True
        Me.lstResult.Location = New System.Drawing.Point(348, 21)
        Me.lstResult.Name = "lstResult"
        Me.lstResult.Size = New System.Drawing.Size(121, 69)
        Me.lstResult.TabIndex = 310
        '
        'LblResult
        '
        Me.LblResult.AutoSize = True
        Me.LblResult.Location = New System.Drawing.Point(348, 5)
        Me.LblResult.Name = "LblResult"
        Me.LblResult.Size = New System.Drawing.Size(37, 13)
        Me.LblResult.TabIndex = 349
        Me.LblResult.Text = "Result"
        '
        'lblDefendingPlayer
        '
        Me.lblDefendingPlayer.AutoSize = True
        Me.lblDefendingPlayer.Location = New System.Drawing.Point(196, 5)
        Me.lblDefendingPlayer.Name = "lblDefendingPlayer"
        Me.lblDefendingPlayer.Size = New System.Drawing.Size(112, 13)
        Me.lblDefendingPlayer.TabIndex = 348
        Me.lblDefendingPlayer.Text = "Defending Player (GK)"
        '
        'lblShootingPlayer
        '
        Me.lblShootingPlayer.AutoSize = True
        Me.lblShootingPlayer.Location = New System.Drawing.Point(5, 5)
        Me.lblShootingPlayer.Name = "lblShootingPlayer"
        Me.lblShootingPlayer.Size = New System.Drawing.Size(81, 13)
        Me.lblShootingPlayer.TabIndex = 315
        Me.lblShootingPlayer.Text = "Shooting Player"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(490, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(0, 13)
        Me.Label5.TabIndex = 311
        '
        'rdoHome
        '
        Me.rdoHome.AutoSize = True
        Me.rdoHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.rdoHome.Location = New System.Drawing.Point(137, 79)
        Me.rdoHome.Name = "rdoHome"
        Me.rdoHome.Size = New System.Drawing.Size(71, 22)
        Me.rdoHome.TabIndex = 343
        Me.rdoHome.TabStop = True
        Me.rdoHome.Text = "Home"
        Me.rdoHome.UseVisualStyleBackColor = True
        '
        'rdoAway
        '
        Me.rdoAway.AutoSize = True
        Me.rdoAway.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.rdoAway.Location = New System.Drawing.Point(407, 79)
        Me.rdoAway.Name = "rdoAway"
        Me.rdoAway.Size = New System.Drawing.Size(65, 22)
        Me.rdoAway.TabIndex = 342
        Me.rdoAway.TabStop = True
        Me.rdoAway.Text = "Away"
        Me.rdoAway.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 18)
        Me.Label3.TabIndex = 307
        Me.Label3.Text = "Shooting Team"
        '
        'pnlGoalframe
        '
        Me.pnlGoalframe.BackColor = System.Drawing.Color.LightBlue
        Me.pnlGoalframe.Controls.Add(Me.UdcSoccerGoalFrame1)
        Me.pnlGoalframe.Controls.Add(Me.pnlInfo)
        Me.pnlGoalframe.Location = New System.Drawing.Point(632, 86)
        Me.pnlGoalframe.Name = "pnlGoalframe"
        Me.pnlGoalframe.Size = New System.Drawing.Size(397, 235)
        Me.pnlGoalframe.TabIndex = 323
        '
        'UdcSoccerGoalFrame1
        '
        Me.UdcSoccerGoalFrame1.Location = New System.Drawing.Point(-423, 3)
        Me.UdcSoccerGoalFrame1.Name = "UdcSoccerGoalFrame1"
        Me.UdcSoccerGoalFrame1.Size = New System.Drawing.Size(1242, 184)
        Me.UdcSoccerGoalFrame1.TabIndex = 312
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.Label2)
        Me.pnlInfo.Controls.Add(Me.lblControlCaption)
        Me.pnlInfo.Controls.Add(Me.btnZoom)
        Me.pnlInfo.Controls.Add(Me.picGFOrangeBlink)
        Me.pnlInfo.Controls.Add(Me.picGFGreenBlink)
        Me.pnlInfo.Controls.Add(Me.lblWidth)
        Me.pnlInfo.Controls.Add(Me.picGFOrange)
        Me.pnlInfo.Controls.Add(Me.picGFGreen)
        Me.pnlInfo.Controls.Add(Me.lblClickLocationOrange)
        Me.pnlInfo.Controls.Add(Me.lblClickLocationGreen)
        Me.pnlInfo.Controls.Add(Me.lblMoveZ)
        Me.pnlInfo.Controls.Add(Me.lblMoveY)
        Me.pnlInfo.Location = New System.Drawing.Point(0, 187)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(397, 47)
        Me.pnlInfo.TabIndex = 313
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "meters"
        '
        'lblControlCaption
        '
        Me.lblControlCaption.AutoSize = True
        Me.lblControlCaption.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblControlCaption.Location = New System.Drawing.Point(52, 3)
        Me.lblControlCaption.Name = "lblControlCaption"
        Me.lblControlCaption.Size = New System.Drawing.Size(70, 15)
        Me.lblControlCaption.TabIndex = 0
        Me.lblControlCaption.Text = "Goal frame"
        '
        'btnZoom
        '
        Me.btnZoom.Location = New System.Drawing.Point(307, 11)
        Me.btnZoom.Name = "btnZoom"
        Me.btnZoom.Size = New System.Drawing.Size(75, 23)
        Me.btnZoom.TabIndex = 15
        Me.btnZoom.Text = "Wide View"
        Me.btnZoom.UseVisualStyleBackColor = True
        '
        'picGFOrangeBlink
        '
        Me.picGFOrangeBlink.Location = New System.Drawing.Point(191, 27)
        Me.picGFOrangeBlink.Name = "picGFOrangeBlink"
        Me.picGFOrangeBlink.Size = New System.Drawing.Size(14, 15)
        Me.picGFOrangeBlink.TabIndex = 14
        Me.picGFOrangeBlink.TabStop = False
        '
        'picGFGreenBlink
        '
        Me.picGFGreenBlink.Location = New System.Drawing.Point(191, 9)
        Me.picGFGreenBlink.Name = "picGFGreenBlink"
        Me.picGFGreenBlink.Size = New System.Drawing.Size(14, 13)
        Me.picGFGreenBlink.TabIndex = 13
        Me.picGFGreenBlink.TabStop = False
        '
        'lblWidth
        '
        Me.lblWidth.AutoSize = True
        Me.lblWidth.Location = New System.Drawing.Point(58, 18)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(47, 13)
        Me.lblWidth.TabIndex = 1
        Me.lblWidth.Text = "54 X 7.6"
        '
        'picGFOrange
        '
        Me.picGFOrange.Location = New System.Drawing.Point(191, 28)
        Me.picGFOrange.Name = "picGFOrange"
        Me.picGFOrange.Size = New System.Drawing.Size(14, 13)
        Me.picGFOrange.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picGFOrange.TabIndex = 12
        Me.picGFOrange.TabStop = False
        '
        'picGFGreen
        '
        Me.picGFGreen.Location = New System.Drawing.Point(191, 9)
        Me.picGFGreen.Name = "picGFGreen"
        Me.picGFGreen.Size = New System.Drawing.Size(14, 13)
        Me.picGFGreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picGFGreen.TabIndex = 11
        Me.picGFGreen.TabStop = False
        '
        'lblClickLocationOrange
        '
        Me.lblClickLocationOrange.Location = New System.Drawing.Point(210, 25)
        Me.lblClickLocationOrange.Name = "lblClickLocationOrange"
        Me.lblClickLocationOrange.Size = New System.Drawing.Size(101, 15)
        Me.lblClickLocationOrange.TabIndex = 7
        Me.lblClickLocationOrange.Text = "-"
        '
        'lblClickLocationGreen
        '
        Me.lblClickLocationGreen.Location = New System.Drawing.Point(210, 7)
        Me.lblClickLocationGreen.Name = "lblClickLocationGreen"
        Me.lblClickLocationGreen.Size = New System.Drawing.Size(101, 15)
        Me.lblClickLocationGreen.TabIndex = 6
        Me.lblClickLocationGreen.Text = "-"
        '
        'lblMoveZ
        '
        Me.lblMoveZ.AutoSize = True
        Me.lblMoveZ.Location = New System.Drawing.Point(140, 25)
        Me.lblMoveZ.Name = "lblMoveZ"
        Me.lblMoveZ.Size = New System.Drawing.Size(17, 13)
        Me.lblMoveZ.TabIndex = 5
        Me.lblMoveZ.Text = "Z:"
        '
        'lblMoveY
        '
        Me.lblMoveY.AutoSize = True
        Me.lblMoveY.Location = New System.Drawing.Point(140, 7)
        Me.lblMoveY.Name = "lblMoveY"
        Me.lblMoveY.Size = New System.Drawing.Size(17, 13)
        Me.lblMoveY.TabIndex = 4
        Me.lblMoveY.Text = "Y:"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(891, 485)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 229
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnDelete.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(846, 538)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 316
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'dgvPBP
        '
        Me.dgvPBP.AllowUserToAddRows = False
        Me.dgvPBP.AllowUserToDeleteRows = False
        Me.dgvPBP.AllowUserToResizeColumns = False
        Me.dgvPBP.AllowUserToResizeRows = False
        Me.dgvPBP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPBP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPBP.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPBP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPBP.Location = New System.Drawing.Point(1, 333)
        Me.dgvPBP.MultiSelect = False
        Me.dgvPBP.Name = "dgvPBP"
        Me.dgvPBP.ReadOnly = True
        Me.dgvPBP.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvPBP.RowHeadersVisible = False
        Me.dgvPBP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPBP.Size = New System.Drawing.Size(688, 259)
        Me.dgvPBP.TabIndex = 324
        '
        'txtSequence
        '
        Me.txtSequence.Location = New System.Drawing.Point(855, 435)
        Me.txtSequence.Name = "txtSequence"
        Me.txtSequence.ReadOnly = True
        Me.txtSequence.Size = New System.Drawing.Size(84, 20)
        Me.txtSequence.TabIndex = 345
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(790, 438)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 344
        Me.Label1.Text = "Sequence:"
        '
        'LblAlert
        '
        Me.LblAlert.AutoSize = True
        Me.LblAlert.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAlert.ForeColor = System.Drawing.Color.Red
        Me.LblAlert.Location = New System.Drawing.Point(820, 381)
        Me.LblAlert.Name = "LblAlert"
        Me.LblAlert.Size = New System.Drawing.Size(85, 20)
        Me.LblAlert.TabIndex = 346
        Me.LblAlert.Text = "Edit Mode "
        '
        'grpdirection
        '
        Me.grpdirection.Controls.Add(Me.rdoRight)
        Me.grpdirection.Controls.Add(Me.rdoLeft)
        Me.grpdirection.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpdirection.Location = New System.Drawing.Point(708, 328)
        Me.grpdirection.Name = "grpdirection"
        Me.grpdirection.Size = New System.Drawing.Size(297, 49)
        Me.grpdirection.TabIndex = 347
        Me.grpdirection.TabStop = False
        Me.grpdirection.Text = "Side of field"
        '
        'rdoRight
        '
        Me.rdoRight.AutoSize = True
        Me.rdoRight.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdoRight.Location = New System.Drawing.Point(191, 15)
        Me.rdoRight.Name = "rdoRight"
        Me.rdoRight.Size = New System.Drawing.Size(65, 24)
        Me.rdoRight.TabIndex = 1
        Me.rdoRight.TabStop = True
        Me.rdoRight.Text = "Right"
        Me.rdoRight.UseVisualStyleBackColor = True
        '
        'rdoLeft
        '
        Me.rdoLeft.AutoSize = True
        Me.rdoLeft.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdoLeft.Location = New System.Drawing.Point(11, 16)
        Me.rdoLeft.Name = "rdoLeft"
        Me.rdoLeft.Size = New System.Drawing.Size(55, 24)
        Me.rdoLeft.TabIndex = 0
        Me.rdoLeft.TabStop = True
        Me.rdoLeft.Text = "Left"
        Me.rdoLeft.UseVisualStyleBackColor = True
        '
        'frmPenaltyShootoutPz
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1032, 624)
        Me.Controls.Add(Me.grpdirection)
        Me.Controls.Add(Me.LblAlert)
        Me.Controls.Add(Me.txtSequence)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvPBP)
        Me.Controls.Add(Me.pnlGoalframe)
        Me.Controls.Add(Me.picAwayLogo)
        Me.Controls.Add(Me.pnlClockBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.picHomeLogo)
        Me.Controls.Add(Me.picButtonBar)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.rdoHome)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.rdoAway)
        Me.Controls.Add(Me.pnlSave)
        Me.Controls.Add(Me.Label3)
        Me.Name = "frmPenaltyShootoutPz"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Soccer Data Collection - Penalty Shoot-out"
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClockBar.ResumeLayout(False)
        Me.pnlClockBar.PerformLayout()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picHomeLogo,System.ComponentModel.ISupportInitialize).EndInit
        Me.picButtonBar.ResumeLayout(false)
        CType(Me.picReporterBar,System.ComponentModel.ISupportInitialize).EndInit
        Me.pnlSave.ResumeLayout(false)
        Me.pnlSave.PerformLayout
        Me.pnlGoalframe.ResumeLayout(false)
        Me.pnlInfo.ResumeLayout(false)
        Me.pnlInfo.PerformLayout
        CType(Me.picGFOrangeBlink,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picGFGreenBlink,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picGFOrange,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picGFGreen,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.dgvPBP,System.ComponentModel.ISupportInitialize).EndInit
        Me.grpdirection.ResumeLayout(false)
        Me.grpdirection.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents lstDefence As System.Windows.Forms.ListBox
    Friend WithEvents picAwayLogo As System.Windows.Forms.PictureBox
    Friend WithEvents pnlClockBar As System.Windows.Forms.Panel
    Friend WithEvents lklAwayteam As System.Windows.Forms.LinkLabel
    Friend WithEvents lklHometeam As System.Windows.Forms.LinkLabel
    Friend WithEvents lblScoreAway As System.Windows.Forms.Label
    Friend WithEvents lblScoreHome As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents picHomeLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents pnlSave As System.Windows.Forms.Panel
    Friend WithEvents lstType As System.Windows.Forms.ListBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lstResult As System.Windows.Forms.ListBox
    Friend WithEvents rdoHome As System.Windows.Forms.RadioButton
    Friend WithEvents rdoAway As System.Windows.Forms.RadioButton
    Friend WithEvents lstBodyPart As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pnlGoalframe As System.Windows.Forms.Panel
    Friend WithEvents UdcSoccerGoalFrame1 As STATS.SoccerDataCollection.udcSoccerGoalFrame
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblControlCaption As System.Windows.Forms.Label
    Friend WithEvents btnZoom As System.Windows.Forms.Button
    Friend WithEvents picGFOrangeBlink As System.Windows.Forms.PictureBox
    Friend WithEvents picGFGreenBlink As System.Windows.Forms.PictureBox
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents picGFOrange As System.Windows.Forms.PictureBox
    Friend WithEvents picGFGreen As System.Windows.Forms.PictureBox
    Friend WithEvents lblClickLocationOrange As System.Windows.Forms.Label
    Friend WithEvents lblClickLocationGreen As System.Windows.Forms.Label
    Friend WithEvents lblMoveZ As System.Windows.Forms.Label
    Friend WithEvents lblMoveY As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents dgvPBP As System.Windows.Forms.DataGridView
    Friend WithEvents lblDefendingPlayer As System.Windows.Forms.Label
    Friend WithEvents lblShootingPlayer As System.Windows.Forms.Label
    Friend WithEvents lblBodyPart As System.Windows.Forms.Label
    Friend WithEvents LblResult As System.Windows.Forms.Label
    Friend WithEvents lblMissType As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents lstPlayer As System.Windows.Forms.ListBox
    Friend WithEvents lstShotType As System.Windows.Forms.ListBox
    Friend WithEvents txtSequence As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblAlert As System.Windows.Forms.Label
    Friend WithEvents grpdirection As System.Windows.Forms.GroupBox
    Friend WithEvents rdoRight As System.Windows.Forms.RadioButton
    Friend WithEvents rdoLeft As System.Windows.Forms.RadioButton
End Class
