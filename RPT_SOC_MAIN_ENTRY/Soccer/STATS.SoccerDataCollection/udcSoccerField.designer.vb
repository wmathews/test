﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class udcSoccerField
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picSoccerField = New System.Windows.Forms.PictureBox
        CType(Me.picSoccerField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picSoccerField
        '
        Me.picSoccerField.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.picSoccerField.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picSoccerField.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picSoccerField.Location = New System.Drawing.Point(0, 0)
        Me.picSoccerField.Margin = New System.Windows.Forms.Padding(0)
        Me.picSoccerField.Name = "picSoccerField"
        Me.picSoccerField.Size = New System.Drawing.Size(300, 200)
        Me.picSoccerField.TabIndex = 3
        Me.picSoccerField.TabStop = False
        '
        'udcSoccerField
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.picSoccerField)
        Me.Name = "udcSoccerField"
        Me.Size = New System.Drawing.Size(300, 200)
        CType(Me.picSoccerField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picSoccerField As System.Windows.Forms.PictureBox

End Class
