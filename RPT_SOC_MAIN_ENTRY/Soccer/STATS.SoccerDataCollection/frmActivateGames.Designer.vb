﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivateGames
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnActivateGame = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnActivateGame
        '
        Me.btnActivateGame.BackColor = System.Drawing.Color.YellowGreen
        Me.btnActivateGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActivateGame.Font = New System.Drawing.Font("Arial Unicode MS", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActivateGame.Location = New System.Drawing.Point(0, 0)
        Me.btnActivateGame.Name = "btnActivateGame"
        Me.btnActivateGame.Size = New System.Drawing.Size(356, 119)
        Me.btnActivateGame.TabIndex = 5
        Me.btnActivateGame.Text = "Activate Game"
        Me.btnActivateGame.UseVisualStyleBackColor = False
        '
        'frmActivateGames
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(356, 119)
        Me.Controls.Add(Me.btnActivateGame)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmActivateGames"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnActivateGame As System.Windows.Forms.Button
End Class
