﻿
#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports System.Drawing.Graphics
Imports System.Drawing.Imaging
Imports System.Drawing.Design
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : udcSoccerFormation
' Author        : Fiaz Ahmed
' Created Date  : 18th August 2009
' Description   : This user control represents soccer formation.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class udcSoccerFormation

#Region " Constants & Variables "
    Private m_strFormation As String
    Private m_strFormationSpecification As String
    Private m_dtRoster As New DataTable
    Private m_dtStartingLineup As DataTable
    Private m_dtBench As DataTable
    Private m_clrShirt As Color
    Private m_clrShort As Color
    Private m_intLineY(5) As Integer
    Private m_blnPrintMode As Boolean

    Private m_pnlPanel As Panel

    Private m_dtFormationMarks As DataTable
    Private m_objUtility As New STATS.Utility.clsUtility

    Const CONTROL_WIDTH As Integer = 99
    Const CONTROL_HEIGHT As Integer = 32
    Const LABEL_HEIGHT As Integer = 17

    'for drag-and-drop
    Private indexOfItemUnderMouseToDrag As Integer
    Private indexOfItemUnderMouseToDrop As Integer

    Private dragBoxFromMouseDown As Rectangle
    Private screenOffset As Point

    Private MyNoDropCursor As Cursor
    Private MyNormalCursor As Cursor
    Private lsupport As New languagesupport
    Private blnDirtyFlag As Boolean = False
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Public Event USFResetClick(ByVal sender As Object, ByVal e As EventArgs)
    Public Event USFApplyClick(ByVal sender As Object, ByVal e As SFEventArgs)
    Public Event USFDragDrop(ByVal sender As Object, ByVal e As SFDDEventArgs)
#End Region

#Region " Event handlers "
    Private Sub udcSoccerFormation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnApply.Enabled = False
            btnReset.Enabled = False
            DefineFormation()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub pnlField_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlField.Paint
        Try
            Dim pnLine As New Pen(Color.SeaGreen)

            For i As Integer = 0 To m_intLineY.Length - 1
                If m_intLineY(i) > 0 Then
                    pnLine.DashStyle = Drawing2D.DashStyle.Dash
                    e.Graphics.DrawLine(pnLine, 0, m_intLineY(i), pnlField.Width, m_intLineY(i))
                End If
            Next
            If CInt(m_objGameDetails.languageid) <> 1 Then
                btnApply.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnApply.Text)
                btnReset.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnReset.Text)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'raise USFResetClick event here
            RaiseEvent USFResetClick(sender, e)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            'raise USFApplyClick event here
            Dim MyEventArgs As SFEventArgs
            Dim dtStartingLineup As DataTable = m_dtStartingLineup.Copy
            MyEventArgs = New SFEventArgs(dtStartingLineup)
            RaiseEvent USFApplyClick(sender, MyEventArgs)

            'if succesful, disable the buttons
            btnApply.Enabled = False
            btnReset.Enabled = False
            blnDirtyFlag = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Public properties "
    Public Property USFFormation() As String
        Get
            Return m_strFormation
        End Get
        Set(ByVal value As String)
            m_strFormation = value
        End Set
    End Property

    Public Property USFFormationSpecification() As String
        Get
            Return m_strFormationSpecification
        End Get
        Set(ByVal value As String)
            m_strFormationSpecification = value
        End Set
    End Property

    Public WriteOnly Property USFRoster() As DataTable
        Set(ByVal value As DataTable)
            If Not value Is Nothing Then
                m_dtRoster = value.Copy
            End If
        End Set
    End Property

    'Public WriteOnly Property USFStartingLineup() As DataTable
    '    Set(ByVal value As DataTable)
    '        m_dtStartingLineup = value.Copy
    '    End Set
    'End Property
    Public Property USFStartingLineup() As DataTable
        Get
            Return m_dtStartingLineup
        End Get
        Set(ByVal value As DataTable)
            If Not value Is Nothing Then
                m_dtStartingLineup = value.Copy
            End If
        End Set
    End Property

    Public WriteOnly Property USFBench() As DataTable
        Set(ByVal value As DataTable)
            If Not value Is Nothing Then
                m_dtBench = value.Copy
            End If
        End Set
    End Property

    Public WriteOnly Property USFShirtColor() As Color
        Set(ByVal value As Color)
            m_clrShirt = value
        End Set
    End Property

    Public WriteOnly Property USFShortColor() As Color
        Set(ByVal value As Color)
            m_clrShort = value
        End Set
    End Property

    Public WriteOnly Property USFPrintMode() As Boolean
        Set(ByVal value As Boolean)
            m_blnPrintMode = value
        End Set
    End Property
#End Region

#Region " Private methods "
    Private Sub DefineFormation()
        Try
            Dim drMark As DataRow
            Dim intHOffset As Integer = CInt(CONTROL_WIDTH / 2)
            Dim intVOffset As Integer = CInt(CONTROL_HEIGHT / 2)

            Dim X1of1 As Integer = CInt(pnlField.Width / 2) - intHOffset

            Dim X2of2 As Integer = CInt((pnlField.Width / 2) / 2) - intHOffset
            Dim X1of2 As Integer = X2of2 + CInt(pnlField.Width / 2)

            Dim X3of3 As Integer = CInt((pnlField.Width / 3) / 2) - intHOffset
            Dim X2of3 As Integer = X3of3 + CInt(pnlField.Width / 3)
            Dim X1of3 As Integer = X2of3 + CInt(pnlField.Width / 3)

            Dim X4of4 As Integer = CInt((pnlField.Width / 4) / 2) - intHOffset
            Dim X3of4 As Integer = X4of4 + CInt(pnlField.Width / 4)
            Dim X2of4 As Integer = X3of4 + CInt(pnlField.Width / 4)
            Dim X1of4 As Integer = X2of4 + CInt(pnlField.Width / 4)

            Dim X5of5 As Integer = CInt((pnlField.Width / 5) / 2) - intHOffset
            Dim X4of5 As Integer = X5of5 + CInt(pnlField.Width / 5)
            Dim X2of5 As Integer = X1of1 + CInt(pnlField.Width / 5)
            Dim X1of5 As Integer = X2of5 + CInt(pnlField.Width / 5)

            Dim Y4of4 As Integer = CInt((pnlField.Height / 4) / 2) - intVOffset
            Dim Y3of4 As Integer = Y4of4 + CInt(pnlField.Height / 4)
            Dim Y2of4 As Integer = Y3of4 + CInt(pnlField.Height / 4)
            Dim Y1of4 As Integer = Y2of4 + CInt(pnlField.Height / 4)

            Dim Y5of5 As Integer = CInt((pnlField.Height / 5) / 2) - intVOffset
            Dim Y4of5 As Integer = Y5of5 + CInt(pnlField.Height / 5)
            Dim Y3of5 As Integer = Y4of5 + CInt(pnlField.Height / 5)
            Dim Y2of5 As Integer = Y3of5 + CInt(pnlField.Height / 5)
            Dim Y1of5 As Integer = Y2of5 + CInt(pnlField.Height / 5)

            Dim Y6of6 As Integer = CInt((pnlField.Height / 6) / 2) - intVOffset
            Dim Y5of6 As Integer = Y6of6 + CInt(pnlField.Height / 6)
            Dim Y4of6 As Integer = Y5of6 + CInt(pnlField.Height / 6)
            Dim Y3of6 As Integer = Y4of6 + CInt(pnlField.Height / 6)
            Dim Y2of6 As Integer = Y3of6 + CInt(pnlField.Height / 6)
            Dim Y1of6 As Integer = Y2of6 + CInt(pnlField.Height / 6)

            Dim Y7of7 As Integer = CInt((pnlField.Height / 7) / 2) - intVOffset
            Dim Y6of7 As Integer = Y7of7 + CInt(pnlField.Height / 7)
            Dim Y5of7 As Integer = Y6of7 + CInt(pnlField.Height / 7)
            Dim Y4of7 As Integer = Y5of7 + CInt(pnlField.Height / 7)
            Dim Y3of7 As Integer = Y4of7 + CInt(pnlField.Height / 7)
            Dim Y2of7 As Integer = Y3of7 + CInt(pnlField.Height / 7)
            Dim Y1of7 As Integer = Y2of7 + CInt(pnlField.Height / 7)

            Dim Y8of8 As Integer = CInt((pnlField.Height / 8) / 2) - intVOffset
            Dim Y7of8 As Integer = Y8of8 + CInt(pnlField.Height / 8)
            Dim Y6of8 As Integer = Y7of8 + CInt(pnlField.Height / 8)
            Dim Y5of8 As Integer = Y6of8 + CInt(pnlField.Height / 8)
            Dim Y4of8 As Integer = Y5of8 + CInt(pnlField.Height / 8)
            Dim Y3of8 As Integer = Y4of8 + CInt(pnlField.Height / 8)
            Dim Y2of8 As Integer = Y3of8 + CInt(pnlField.Height / 8)
            Dim Y1of8 As Integer = Y2of8 + CInt(pnlField.Height / 8)

            m_dtFormationMarks = New DataTable

            m_dtFormationMarks.Columns.Add("Formation")
            m_dtFormationMarks.Columns.Add("FormationSpecification")

            'for player positions
            For i As Int16 = 1 To 11
                m_dtFormationMarks.Columns.Add("X" & i.ToString)
                m_dtFormationMarks.Columns.Add("Y" & i.ToString)
            Next

            'for dotted line positions
            For i As Int16 = 1 To 5
                m_dtFormationMarks.Columns.Add("Line" & i.ToString & "Y")
            Next

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-3"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of4
            drMark("X2") = X1of3
            drMark("Y2") = Y2of4
            drMark("X3") = X1of1
            drMark("Y3") = Y2of4
            drMark("X4") = X3of3
            drMark("Y4") = Y2of4
            drMark("X5") = X1of4
            drMark("Y5") = Y3of4
            drMark("X6") = X2of4
            drMark("Y6") = Y3of4
            drMark("X7") = X3of4
            drMark("Y7") = Y3of4
            drMark("X8") = X4of4
            drMark("Y8") = Y3of4
            drMark("X9") = X1of3
            drMark("Y9") = Y4of4
            drMark("X10") = X1of1
            drMark("Y10") = Y4of4
            drMark("X11") = X3of3
            drMark("Y11") = Y4of4
            drMark("Line1Y") = Y2of4 + CONTROL_HEIGHT + CInt((Y1of4 - (Y2of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of4 + CONTROL_HEIGHT + CInt((Y2of4 - (Y3of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of4 + CONTROL_HEIGHT + CInt((Y3of4 - (Y4of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-3"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of3
            drMark("Y2") = Y3of6
            drMark("X3") = X1of1
            drMark("Y3") = Y2of6
            drMark("X4") = X3of3
            drMark("Y4") = Y3of6
            drMark("X5") = X1of4
            drMark("Y5") = Y4of6
            drMark("X6") = X2of4
            drMark("Y6") = Y4of6
            drMark("X7") = X3of4
            drMark("Y7") = Y4of6
            drMark("X8") = X4of4
            drMark("Y8") = Y4of6
            drMark("X9") = X1of3
            drMark("Y9") = Y5of6
            drMark("X10") = X1of1
            drMark("Y10") = Y6of6
            drMark("X11") = X3of3
            drMark("Y11") = Y5of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-3"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of3
            drMark("Y2") = Y3of7
            drMark("X3") = X1of1
            drMark("Y3") = Y2of7
            drMark("X4") = X3of3
            drMark("Y4") = Y3of7
            drMark("X5") = X1of4
            drMark("Y5") = Y5of7
            drMark("X6") = X2of4
            drMark("Y6") = Y4of7
            drMark("X7") = X3of4
            drMark("Y7") = Y4of7
            drMark("X8") = X4of4
            drMark("Y8") = Y5of7
            drMark("X9") = X1of3
            drMark("Y9") = Y7of7
            drMark("X10") = X1of1
            drMark("Y10") = Y6of7
            drMark("X11") = X3of3
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-3"
            drMark("FormationSpecification") = "5"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of8
            drMark("X2") = X1of3
            drMark("Y2") = Y3of8
            drMark("X3") = X1of1
            drMark("Y3") = Y2of8
            drMark("X4") = X3of3
            drMark("Y4") = Y3of8
            drMark("X5") = X2of5
            drMark("Y5") = Y5of8
            drMark("X6") = X1of1
            drMark("Y6") = Y4of8
            drMark("X7") = X1of1
            drMark("Y7") = Y6of8
            drMark("X8") = X4of5
            drMark("Y8") = Y5of8
            drMark("X9") = X1of3
            drMark("Y9") = Y8of8
            drMark("X10") = X1of1
            drMark("Y10") = Y7of8
            drMark("X11") = X3of3
            drMark("Y11") = Y8of8
            drMark("Line1Y") = Y2of8 + CONTROL_HEIGHT + CInt((Y1of8 - (Y2of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of8 + CONTROL_HEIGHT + CInt((Y3of8 - (Y4of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y7of8 + CONTROL_HEIGHT + CInt((Y6of8 - (Y7of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-3"
            drMark("FormationSpecification") = "4"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of3
            drMark("Y2") = Y2of5
            drMark("X3") = X1of1
            drMark("Y3") = Y2of5
            drMark("X4") = X3of3
            drMark("Y4") = Y2of5
            drMark("X5") = X1of4
            drMark("Y5") = Y3of5
            drMark("X6") = X2of4
            drMark("Y6") = Y3of5
            drMark("X7") = X3of4
            drMark("Y7") = Y3of5
            drMark("X8") = X4of4
            drMark("Y8") = Y3of5
            drMark("X9") = X1of5
            drMark("Y9") = Y5of5
            drMark("X10") = X1of1
            drMark("Y10") = Y4of5
            drMark("X11") = X5of5
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of4
            drMark("Y2") = Y3of6
            drMark("X3") = X2of4
            drMark("Y3") = Y2of6
            drMark("X4") = X3of4
            drMark("Y4") = Y2of6
            drMark("X5") = X4of4
            drMark("Y5") = Y3of6
            drMark("X6") = X1of4
            drMark("Y6") = Y5of6
            drMark("X7") = X2of4
            drMark("Y7") = Y4of6
            drMark("X8") = X3of4
            drMark("Y8") = Y4of6
            drMark("X9") = X4of4
            drMark("Y9") = Y5of6
            drMark("X10") = X2of4
            drMark("Y10") = Y6of6
            drMark("X11") = X3of4
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-2"
            drMark("FormationSpecification") = "6"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of3
            drMark("Y6") = Y5of7
            drMark("X7") = X1of1
            drMark("Y7") = Y4of7
            drMark("X8") = X1of1
            drMark("Y8") = Y6of7
            drMark("X9") = X3of3
            drMark("Y9") = Y5of7
            drMark("X10") = X2of4
            drMark("Y10") = Y7of7
            drMark("X11") = X3of4
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-2"
            drMark("FormationSpecification") = "7"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of8
            drMark("X2") = X1of3
            drMark("Y2") = Y3of8
            drMark("X3") = X1of1
            drMark("Y3") = Y2of8
            drMark("X4") = X1of1
            drMark("Y4") = Y4of8
            drMark("X5") = X3of3
            drMark("Y5") = Y3of8
            drMark("X6") = X1of3
            drMark("Y6") = Y6of8
            drMark("X7") = X1of1
            drMark("Y7") = Y5of8
            drMark("X8") = X1of1
            drMark("Y8") = Y7of8
            drMark("X9") = X3of3
            drMark("Y9") = Y6of8
            drMark("X10") = X2of4
            drMark("Y10") = Y8of8
            drMark("X11") = X3of4
            drMark("Y11") = Y8of8
            drMark("Line1Y") = Y2of8 + CONTROL_HEIGHT + CInt((Y1of8 - (Y2of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line2Y") = Y5of8 + CONTROL_HEIGHT + CInt((Y4of8 - (Y5of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line3Y") = Y8of8 + CONTROL_HEIGHT + CInt((Y7of8 - (Y8of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-2"
            drMark("FormationSpecification") = "8"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of3
            drMark("Y2") = Y3of6
            drMark("X3") = X1of1
            drMark("Y3") = Y2of6
            drMark("X4") = X1of1
            drMark("Y4") = Y4of6
            drMark("X5") = X3of3
            drMark("Y5") = Y3of6
            drMark("X6") = X1of4
            drMark("Y6") = Y5of6
            drMark("X7") = X2of4
            drMark("Y7") = Y5of6
            drMark("X8") = X3of4
            drMark("Y8") = Y5of6
            drMark("X9") = X4of4
            drMark("Y9") = Y5of6
            drMark("X10") = X2of4
            drMark("Y10") = Y6of6
            drMark("X11") = X3of4
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line2Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-3-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of4
            drMark("X2") = X1of5
            drMark("Y2") = Y2of4
            drMark("X3") = X2of5
            drMark("Y3") = Y2of4
            drMark("X4") = X1of1
            drMark("Y4") = Y2of4
            drMark("X5") = X4of5
            drMark("Y5") = Y2of4
            drMark("X6") = X5of5
            drMark("Y6") = Y2of4
            drMark("X7") = X1of3
            drMark("Y7") = Y3of4
            drMark("X8") = X1of1
            drMark("Y8") = Y3of4
            drMark("X9") = X3of3
            drMark("Y9") = Y3of4
            drMark("X10") = X2of5
            drMark("Y10") = Y4of4
            drMark("X11") = X4of5
            drMark("Y11") = Y4of4
            drMark("Line1Y") = Y2of4 + CONTROL_HEIGHT + CInt((Y1of4 - (Y2of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of4 + CONTROL_HEIGHT + CInt((Y2of4 - (Y3of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of4 + CONTROL_HEIGHT + CInt((Y3of4 - (Y4of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-3-2"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of5
            drMark("Y2") = Y3of6
            drMark("X3") = X2of5
            drMark("Y3") = Y2of6
            drMark("X4") = X1of1
            drMark("Y4") = Y3of6
            drMark("X5") = X4of5
            drMark("Y5") = Y2of6
            drMark("X6") = X5of5
            drMark("Y6") = Y3of6
            drMark("X7") = X2of5
            drMark("Y7") = Y4of6
            drMark("X8") = X1of1
            drMark("Y8") = Y5of6
            drMark("X9") = X4of5
            drMark("Y9") = Y4of6
            drMark("X10") = X2of4
            drMark("Y10") = Y6of6
            drMark("X11") = X3of4
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-3-2"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of5
            drMark("Y2") = Y3of6
            drMark("X3") = X2of5
            drMark("Y3") = Y2of6
            drMark("X4") = X1of1
            drMark("Y4") = Y2of6
            drMark("X5") = X4of5
            drMark("Y5") = Y2of6
            drMark("X6") = X5of5
            drMark("Y6") = Y3of6
            drMark("X7") = X2of5
            drMark("Y7") = Y5of6
            drMark("X8") = X1of1
            drMark("Y8") = Y4of6
            drMark("X9") = X4of5
            drMark("Y9") = Y5of6
            drMark("X10") = X2of4
            drMark("Y10") = Y6of6
            drMark("X11") = X3of4
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-5-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of4
            drMark("X2") = X1of3
            drMark("Y2") = Y2of4
            drMark("X3") = X1of1
            drMark("Y3") = Y2of4
            drMark("X4") = X3of3
            drMark("Y4") = Y2of4
            drMark("X5") = X1of5
            drMark("Y5") = Y3of4
            drMark("X6") = X2of5
            drMark("Y6") = Y3of4
            drMark("X7") = X1of1
            drMark("Y7") = Y3of4
            drMark("X8") = X4of5
            drMark("Y8") = Y3of4
            drMark("X9") = X5of5
            drMark("Y9") = Y3of4
            drMark("X10") = X2of4
            drMark("Y10") = Y4of4
            drMark("X11") = X3of4
            drMark("Y11") = Y4of4
            drMark("Line1Y") = Y2of4 + CONTROL_HEIGHT + CInt((Y1of4 - (Y2of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of4 + CONTROL_HEIGHT + CInt((Y2of4 - (Y3of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of4 + CONTROL_HEIGHT + CInt((Y3of4 - (Y4of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-5-2"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X2of5
            drMark("Y2") = Y2of6
            drMark("X3") = X1of1
            drMark("Y3") = Y3of6
            drMark("X4") = X4of5
            drMark("Y4") = Y2of6
            drMark("X5") = X1of5
            drMark("Y5") = Y5of6
            drMark("X6") = X2of5
            drMark("Y6") = Y4of6
            drMark("X7") = X1of1
            drMark("Y7") = Y5of6
            drMark("X8") = X4of5
            drMark("Y8") = Y4of6
            drMark("X9") = X5of5
            drMark("Y9") = Y5of6
            drMark("X10") = X2of4
            drMark("Y10") = Y6of6
            drMark("X11") = X3of4
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-5-2"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X2of5
            drMark("Y2") = Y3of6
            drMark("X3") = X1of1
            drMark("Y3") = Y2of6
            drMark("X4") = X4of5
            drMark("Y4") = Y3of6
            drMark("X5") = X1of5
            drMark("Y5") = Y5of6
            drMark("X6") = X2of5
            drMark("Y6") = Y4of6
            drMark("X7") = X1of1
            drMark("Y7") = Y4of6
            drMark("X8") = X4of5
            drMark("Y8") = Y4of6
            drMark("X9") = X5of5
            drMark("Y9") = Y5of6
            drMark("X10") = X2of4
            drMark("Y10") = Y6of6
            drMark("X11") = X3of4
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-5-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of4
            drMark("X2") = X1of4
            drMark("Y2") = Y2of4
            drMark("X3") = X2of4
            drMark("Y3") = Y2of4
            drMark("X4") = X3of4
            drMark("Y4") = Y2of4
            drMark("X5") = X4of4
            drMark("Y5") = Y2of4
            drMark("X6") = X1of5
            drMark("Y6") = Y3of4
            drMark("X7") = X2of5
            drMark("Y7") = Y3of4
            drMark("X8") = X1of1
            drMark("Y8") = Y3of4
            drMark("X9") = X4of5
            drMark("Y9") = Y3of4
            drMark("X10") = X5of5
            drMark("Y10") = Y3of4
            drMark("X11") = X1of1
            drMark("Y11") = Y4of4
            drMark("Line1Y") = Y2of4 + CONTROL_HEIGHT + CInt((Y1of4 - (Y2of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of4 + CONTROL_HEIGHT + CInt((Y2of4 - (Y3of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of4 + CONTROL_HEIGHT + CInt((Y3of4 - (Y4of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-5-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of4
            drMark("Y2") = Y3of6
            drMark("X3") = X2of4
            drMark("Y3") = Y2of6
            drMark("X4") = X3of4
            drMark("Y4") = Y2of6
            drMark("X5") = X4of4
            drMark("Y5") = Y3of6
            drMark("X6") = X1of5
            drMark("Y6") = Y5of6
            drMark("X7") = X2of5
            drMark("Y7") = Y4of6
            drMark("X8") = X1of1
            drMark("Y8") = Y5of6
            drMark("X9") = X4of5
            drMark("Y9") = Y4of6
            drMark("X10") = X5of5
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-5-1"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of4
            drMark("Y2") = Y3of6
            drMark("X3") = X2of4
            drMark("Y3") = Y2of6
            drMark("X4") = X3of4
            drMark("Y4") = Y2of6
            drMark("X5") = X4of4
            drMark("Y5") = Y3of6
            drMark("X6") = X1of5
            drMark("Y6") = Y5of6
            drMark("X7") = X2of5
            drMark("Y7") = Y4of6
            drMark("X8") = X1of1
            drMark("Y8") = Y4of6
            drMark("X9") = X4of5
            drMark("Y9") = Y4of6
            drMark("X10") = X5of5
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-5-1"
            drMark("FormationSpecification") = "5"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of3
            drMark("Y2") = Y3of7
            drMark("X3") = X1of1
            drMark("Y3") = Y2of7
            drMark("X4") = X1of1
            drMark("Y4") = Y4of7
            drMark("X5") = X3of3
            drMark("Y5") = Y3of7
            drMark("X6") = X1of5
            drMark("Y6") = Y6of7
            drMark("X7") = X2of5
            drMark("Y7") = Y5of7
            drMark("X8") = X1of1
            drMark("Y8") = Y6of7
            drMark("X9") = X4of5
            drMark("Y9") = Y5of7
            drMark("X10") = X5of5
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-3"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of4
            drMark("X2") = X1of4
            drMark("Y2") = Y2of4
            drMark("X3") = X2of4
            drMark("Y3") = Y2of4
            drMark("X4") = X3of4
            drMark("Y4") = Y2of4
            drMark("X5") = X4of4
            drMark("Y5") = Y2of4
            drMark("X6") = X1of3
            drMark("Y6") = Y3of4
            drMark("X7") = X1of1
            drMark("Y7") = Y3of4
            drMark("X8") = X3of3
            drMark("Y8") = Y3of4
            drMark("X9") = X1of3
            drMark("Y9") = Y4of4
            drMark("X10") = X1of1
            drMark("Y10") = Y4of4
            drMark("X11") = X3of3
            drMark("Y11") = Y4of4
            drMark("Line1Y") = Y2of4 + CONTROL_HEIGHT + CInt((Y1of4 - (Y2of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of4 + CONTROL_HEIGHT + CInt((Y2of4 - (Y3of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of4 + CONTROL_HEIGHT + CInt((Y3of4 - (Y4of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-3"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of3
            drMark("Y6") = Y4of7
            drMark("X7") = X1of1
            drMark("Y7") = Y5of7
            drMark("X8") = X3of3
            drMark("Y8") = Y4of7
            drMark("X9") = X1of3
            drMark("Y9") = Y6of7
            drMark("X10") = X1of1
            drMark("Y10") = Y7of7
            drMark("X11") = X3of3
            drMark("Y11") = Y6of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-3"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of3
            drMark("Y6") = Y5of7
            drMark("X7") = X1of1
            drMark("Y7") = Y4of7
            drMark("X8") = X3of3
            drMark("Y8") = Y5of7
            drMark("X9") = X1of3
            drMark("Y9") = Y7of7
            drMark("X10") = X1of1
            drMark("Y10") = Y6of7
            drMark("X11") = X3of3
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-3"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of8
            drMark("X2") = X1of3
            drMark("Y2") = Y3of8
            drMark("X3") = X1of1
            drMark("Y3") = Y2of8
            drMark("X4") = X1of1
            drMark("Y4") = Y4of8
            drMark("X5") = X3of3
            drMark("Y5") = Y3of8
            drMark("X6") = X1of3
            drMark("Y6") = Y6of8
            drMark("X7") = X1of1
            drMark("Y7") = Y5of8
            drMark("X8") = X3of3
            drMark("Y8") = Y6of8
            drMark("X9") = X1of3
            drMark("Y9") = Y8of8
            drMark("X10") = X1of1
            drMark("Y10") = Y7of8
            drMark("X11") = X3of3
            drMark("Y11") = Y8of8
            drMark("Line1Y") = Y2of8 + CONTROL_HEIGHT + CInt((Y1of8 - (Y2of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y5of8 + CONTROL_HEIGHT + CInt((Y4of8 - (Y5of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y7of8 + CONTROL_HEIGHT + CInt((Y6of8 - (Y7of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-3"
            drMark("FormationSpecification") = "4"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of4
            drMark("Y2") = Y3of6
            drMark("X3") = X2of4
            drMark("Y3") = Y2of6
            drMark("X4") = X3of4
            drMark("Y4") = Y2of6
            drMark("X5") = X4of4
            drMark("Y5") = Y3of6
            drMark("X6") = X1of3
            drMark("Y6") = Y4of6
            drMark("X7") = X1of1
            drMark("Y7") = Y4of6
            drMark("X8") = X3of3
            drMark("Y8") = Y4of6
            drMark("X9") = X1of5
            drMark("Y9") = Y6of6
            drMark("X10") = X1of1
            drMark("Y10") = Y5of6
            drMark("X11") = X5of5
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-4-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5
            drMark("X6") = X1of1
            drMark("Y6") = Y3of5
            drMark("X7") = X1of4
            drMark("Y7") = Y4of5
            drMark("X8") = X2of4
            drMark("Y8") = Y4of5
            drMark("X9") = X3of4
            drMark("Y9") = Y4of5
            drMark("X10") = X4of4
            drMark("Y10") = Y4of5
            drMark("X11") = X1of1
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-4-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of1
            drMark("Y6") = Y4of7
            drMark("X7") = X1of4
            drMark("Y7") = Y6of7
            drMark("X8") = X2of4
            drMark("Y8") = Y5of7
            drMark("X9") = X3of4
            drMark("Y9") = Y5of7
            drMark("X10") = X4of4
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-4-1"
            drMark("FormationSpecification") = "5"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of1
            drMark("Y6") = Y4of7
            drMark("X7") = X1of4
            drMark("Y7") = Y5of7
            drMark("X8") = X2of4
            drMark("Y8") = Y6of7
            drMark("X9") = X3of4
            drMark("Y9") = Y6of7
            drMark("X10") = X4of4
            drMark("Y10") = Y5of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-2-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5
            drMark("X6") = X1of3
            drMark("Y6") = Y3of5
            drMark("X7") = X1of1
            drMark("Y7") = Y3of5
            drMark("X8") = X3of3
            drMark("Y8") = Y3of5
            drMark("X9") = X1of2
            drMark("Y9") = Y4of5
            drMark("X10") = X2of2
            drMark("Y10") = Y4of5
            drMark("X11") = X1of1
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-2-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of3
            drMark("Y6") = Y4of7
            drMark("X7") = X1of1
            drMark("Y7") = Y5of7
            drMark("X8") = X3of3
            drMark("Y8") = Y4of7
            drMark("X9") = X1of2
            drMark("Y9") = Y6of7
            drMark("X10") = X2of2
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-1-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5
            drMark("X6") = X1of4
            drMark("Y6") = Y3of5
            drMark("X7") = X2of4
            drMark("Y7") = Y3of5
            drMark("X8") = X3of4
            drMark("Y8") = Y3of5
            drMark("X9") = X4of4
            drMark("Y9") = Y3of5
            drMark("X10") = X1of1
            drMark("Y10") = Y4of5
            drMark("X11") = X1of1
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-1-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of4
            drMark("Y6") = Y5of7
            drMark("X7") = X2of4
            drMark("Y7") = Y4of7
            drMark("X8") = X3of4
            drMark("Y8") = Y4of7
            drMark("X9") = X4of4
            drMark("Y9") = Y5of7
            drMark("X10") = X1of1
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            'TOPZ-515 <-----4-4-1-1-----5
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-4-1-1"
            drMark("FormationSpecification") = "5"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of8
            drMark("X2") = X1of4
            drMark("Y2") = Y3of8
            drMark("X3") = X2of4
            drMark("Y3") = Y2of8
            drMark("X4") = X3of4
            drMark("Y4") = Y2of8
            drMark("X5") = X4of4
            drMark("Y5") = Y3of8
            drMark("X6") = X1of3
            drMark("Y6") = Y5of8
            drMark("X7") = X2of3
            drMark("Y7") = Y4of8
            drMark("X8") = X3of3
            drMark("Y8") = Y5of8
            drMark("X9") = X2of3
            drMark("Y9") = Y6of8
            drMark("X10") = X1of1
            drMark("Y10") = Y7of8
            drMark("X11") = X1of1
            drMark("Y11") = Y8of8
            drMark("Line1Y") = Y2of8 + CONTROL_HEIGHT + CInt((Y1of8 - (Y2of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of8 + CONTROL_HEIGHT + CInt((Y3of8 - (Y4of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y7of8 + CONTROL_HEIGHT + CInt((Y6of8 - (Y7of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y8of8 + CONTROL_HEIGHT + CInt((Y7of8 - (Y8of8 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-2-3"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5
            drMark("X6") = X1of1
            drMark("Y6") = Y3of5
            drMark("X7") = X1of2
            drMark("Y7") = Y4of5
            drMark("X8") = X2of2
            drMark("Y8") = Y4of5
            drMark("X9") = X1of3
            drMark("Y9") = Y5of5
            drMark("X10") = X1of1
            drMark("Y10") = Y5of5
            drMark("X11") = X3of3
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-2-3"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of1
            drMark("Y6") = Y4of7
            drMark("X7") = X1of2
            drMark("Y7") = Y5of7
            drMark("X8") = X2of2
            drMark("Y8") = Y5of7
            drMark("X9") = X1of3
            drMark("Y9") = Y6of7
            drMark("X10") = X1of1
            drMark("Y10") = Y7of7
            drMark("X11") = X3of3
            drMark("Y11") = Y6of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-4-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of4
            drMark("X2") = X1of5
            drMark("Y2") = Y2of4
            drMark("X3") = X2of5
            drMark("Y3") = Y2of4
            drMark("X4") = X1of1
            drMark("Y4") = Y2of4
            drMark("X5") = X4of5
            drMark("Y5") = Y2of4
            drMark("X6") = X5of5
            drMark("Y6") = Y2of4
            drMark("X7") = X1of4
            drMark("Y7") = Y3of4
            drMark("X8") = X2of4
            drMark("Y8") = Y3of4
            drMark("X9") = X3of4
            drMark("Y9") = Y3of4
            drMark("X10") = X4of4
            drMark("Y10") = Y3of4
            drMark("X11") = X1of1
            drMark("Y11") = Y4of4
            drMark("Line1Y") = Y2of4 + CONTROL_HEIGHT + CInt((Y1of4 - (Y2of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of4 + CONTROL_HEIGHT + CInt((Y2of4 - (Y3of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of4 + CONTROL_HEIGHT + CInt((Y3of4 - (Y4of4 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-4-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of5
            drMark("Y2") = Y3of6
            drMark("X3") = X2of5
            drMark("Y3") = Y2of6
            drMark("X4") = X1of1
            drMark("Y4") = Y3of6
            drMark("X5") = X4of5
            drMark("Y5") = Y2of6
            drMark("X6") = X5of5
            drMark("Y6") = Y3of6
            drMark("X7") = X1of4
            drMark("Y7") = Y5of6
            drMark("X8") = X2of4
            drMark("Y8") = Y4of6
            drMark("X9") = X3of4
            drMark("Y9") = Y4of6
            drMark("X10") = X4of4
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-4-1"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of5
            drMark("Y2") = Y3of6
            drMark("X3") = X2of5
            drMark("Y3") = Y2of6
            drMark("X4") = X1of1
            drMark("Y4") = Y2of6
            drMark("X5") = X4of5
            drMark("Y5") = Y2of6
            drMark("X6") = X5of5
            drMark("Y6") = Y3of6
            drMark("X7") = X1of4
            drMark("Y7") = Y5of6
            drMark("X8") = X2of4
            drMark("Y8") = Y4of6
            drMark("X9") = X3of4
            drMark("Y9") = Y4of6
            drMark("X10") = X4of4
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-4-1"
            drMark("FormationSpecification") = "5"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of5
            drMark("Y2") = Y3of7
            drMark("X3") = X2of5
            drMark("Y3") = Y2of7
            drMark("X4") = X1of1
            drMark("Y4") = Y3of7
            drMark("X5") = X4of5
            drMark("Y5") = Y2of7
            drMark("X6") = X5of5
            drMark("Y6") = Y3of7
            drMark("X7") = X1of3
            drMark("Y7") = Y5of7
            drMark("X8") = X1of1
            drMark("Y8") = Y4of7
            drMark("X9") = X1of1
            drMark("Y9") = Y6of7
            drMark("X10") = X3of3
            drMark("Y10") = Y5of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = 0
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-3-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5
            drMark("X6") = X1of1
            drMark("Y6") = Y3of5
            drMark("X7") = X1of3
            drMark("Y7") = Y4of5
            drMark("X8") = X1of1
            drMark("Y8") = Y4of5
            drMark("X9") = X3of3
            drMark("Y9") = Y4of5
            drMark("X10") = X1of2
            drMark("Y10") = Y5of5
            drMark("X11") = X2of2
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-3-2"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of1
            drMark("Y6") = Y4of7
            drMark("X7") = X1of3
            drMark("Y7") = Y5of7
            drMark("X8") = X1of1
            drMark("Y8") = Y6of7
            drMark("X9") = X3of3
            drMark("Y9") = Y5of7
            drMark("X10") = X1of2
            drMark("Y10") = Y7of7
            drMark("X11") = X2of2
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-3-2"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of1
            drMark("Y6") = Y4of7
            drMark("X7") = X1of3
            drMark("Y7") = Y6of7
            drMark("X8") = X1of1
            drMark("Y8") = Y5of7
            drMark("X9") = X3of3
            drMark("Y9") = Y6of7
            drMark("X10") = X1of2
            drMark("Y10") = Y7of7
            drMark("X11") = X2of2
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-2-3-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5
            drMark("X6") = X1of2
            drMark("Y6") = Y3of5
            drMark("X7") = X2of2
            drMark("Y7") = Y3of5
            drMark("X8") = X1of3
            drMark("Y8") = Y4of5
            drMark("X9") = X1of1
            drMark("Y9") = Y4of5
            drMark("X10") = X3of3
            drMark("Y10") = Y4of5
            drMark("X11") = X1of1
            drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-2-3-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of2
            drMark("Y6") = Y4of7
            drMark("X7") = X2of2
            drMark("Y7") = Y4of7
            drMark("X8") = X1of3
            drMark("Y8") = Y5of7
            drMark("X9") = X1of1
            drMark("Y9") = Y6of7
            drMark("X10") = X3of3
            drMark("Y10") = Y5of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-2-3-1"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of2
            drMark("Y6") = Y4of7
            drMark("X7") = X2of2
            drMark("Y7") = Y4of7
            drMark("X8") = X1of3
            drMark("Y8") = Y6of7
            drMark("X9") = X1of1
            drMark("Y9") = Y5of7
            drMark("X10") = X3of3
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-2-3-1"
            drMark("FormationSpecification") = "4"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of2
            drMark("Y6") = Y4of7
            drMark("X7") = X2of2
            drMark("Y7") = Y4of7
            drMark("X8") = X1of5
            drMark("Y8") = Y6of7
            drMark("X9") = X1of1
            drMark("Y9") = Y5of7
            drMark("X10") = X5of5
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            '4-1-3-2 - new formation added by Jon in Dec 2010 (4-2-3-1)
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-3-1-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5

            drMark("X2") = X1of4
            drMark("Y2") = Y2of5
            drMark("X3") = X2of4
            drMark("Y3") = Y2of5
            drMark("X4") = X3of4
            drMark("Y4") = Y2of5
            drMark("X5") = X4of4
            drMark("Y5") = Y2of5

            drMark("X6") = X1of3
            drMark("Y6") = Y3of5
            drMark("X7") = X2of3
            drMark("Y7") = Y3of5
            drMark("X8") = X3of3
            drMark("Y8") = Y3of5

            drMark("X9") = X1of1
            drMark("Y9") = Y4of5

            drMark("X10") = X1of2
            drMark("Y10") = Y5of5
            drMark("X11") = X2of2
            drMark("Y11") = Y5of5
            'drMark("X11") = X1of1
            'drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)


            '3-5-1-1 - new formation added by Arindam in 20-Nov 2011 
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-5-1-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of3
            drMark("Y2") = Y2of5
            drMark("X3") = X1of1
            drMark("Y3") = Y2of5
            drMark("X4") = X3of3
            drMark("Y4") = Y2of5
            drMark("X5") = X1of5
            drMark("Y5") = Y3of5
            drMark("X6") = X2of5
            drMark("Y6") = Y3of5
            drMark("X7") = X1of1
            drMark("Y7") = Y3of5
            drMark("X8") = X4of5
            drMark("Y8") = Y3of5
            drMark("X9") = X5of5
            drMark("Y9") = Y3of5

            drMark("X10") = X1of1
            drMark("Y10") = Y4of5
            drMark("X11") = X1of1
            drMark("Y11") = Y5of5
            'drMark("X11") = X1of1
            'drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            'TOPZ-515 --------------- begin
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-5-1-1"
            drMark("FormationSpecification") = "2"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of3
            drMark("Y2") = Y2of6
            drMark("X3") = X2of3
            drMark("Y3") = Y2of6
            drMark("X4") = X3of3
            drMark("Y4") = Y2of6
            drMark("X5") = X1of5
            drMark("Y5") = Y3of6
            drMark("X6") = X2of5
            drMark("Y6") = Y4of6
            drMark("X7") = X1of1
            drMark("Y7") = Y4of6
            drMark("X8") = X4of5
            drMark("Y8") = Y4of6
            drMark("X9") = X5of5
            drMark("Y9") = Y3of6
            drMark("X10") = X1of1
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6 '''
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of6 + CONTROL_HEIGHT + CInt((Y2of6 - (Y3of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-5-1-1"
            drMark("FormationSpecification") = "3"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of3
            drMark("Y2") = Y2of6
            drMark("X3") = X2of3
            drMark("Y3") = Y2of6
            drMark("X4") = X3of3
            drMark("Y4") = Y2of6
            drMark("X5") = X1of5
            drMark("Y5") = Y3of6
            drMark("X6") = X2of5
            drMark("Y6") = Y4of6
            drMark("X7") = X1of1
            drMark("Y7") = Y3of6
            drMark("X8") = X4of5
            drMark("Y8") = Y4of6
            drMark("X9") = X5of5
            drMark("Y9") = Y3of6
            drMark("X10") = X1of1
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6 '''
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of6 + CONTROL_HEIGHT + CInt((Y2of6 - (Y3of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)
            'TOPZ-515 --------------- end


            ''Arindam 3-1-4-2 and 3-4-1-2 
            
            '3-4-1-2 - new formation added by Arindam in 20-Nov 2011 
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-1-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5

            drMark("X2") = X1of3
            drMark("Y2") = Y2of5
            drMark("X3") = X1of1
            drMark("Y3") = Y2of5
            drMark("X4") = X3of3
            drMark("Y4") = Y2of5

            drMark("X5") = X1of4
            drMark("Y5") = Y3of5
            drMark("X6") = X2of4
            drMark("Y6") = Y3of5
            drMark("X7") = X3of4
            drMark("Y7") = Y3of5
            drMark("X8") = X4of4
            drMark("Y8") = Y3of5

            drMark("X9") = X1of1
            drMark("Y9") = Y4of5

            drMark("X10") = X1of2
            drMark("Y10") = Y5of5
            drMark("X11") = X2of2
            drMark("Y11") = Y5of5

            'drMark("X11") = X1of1
            'drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            ''

            'TOPZ-515 <-----3-2-4-1-----4
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-2-4-1"
            drMark("FormationSpecification") = "4"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of3
            drMark("Y2") = Y2of5
            drMark("X3") = X2of3
            drMark("Y3") = Y2of5
            drMark("X4") = X3of3
            drMark("Y4") = Y2of5
            drMark("X5") = X1of4
            drMark("Y5") = Y3of5
            drMark("X6") = X4of4
            drMark("Y6") = Y3of5
            drMark("X7") = X1of4
            drMark("Y7") = Y4of5
            drMark("X8") = X2of4
            drMark("Y8") = Y4of5
            drMark("X9") = X3of4
            drMark("Y9") = Y4of5
            drMark("X10") = X4of4
            drMark("Y10") = Y4of5
            drMark("X11") = X1of1
            drMark("Y11") = Y5of5 '''
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            '3-4-2-1 - new formation added by Arindam in 20-Nov 2011 
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-4-2-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5

            drMark("X2") = X1of3
            drMark("Y2") = Y2of5
            drMark("X3") = X1of1
            drMark("Y3") = Y2of5
            drMark("X4") = X3of3
            drMark("Y4") = Y2of5

            drMark("X5") = X1of4
            drMark("Y5") = Y3of5
            drMark("X6") = X2of4
            drMark("Y6") = Y3of5
            drMark("X7") = X3of4
            drMark("Y7") = Y3of5
            drMark("X8") = X4of4
            drMark("Y8") = Y3of5

            drMark("X9") = X1of2
            drMark("Y9") = Y4of5
            drMark("X10") = X2of2
            drMark("Y10") = Y4of5

            drMark("X11") = X1of1
            drMark("Y11") = Y5of5
            'drMark("X11") = X1of1
            'drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            '3-1-4-2 - new formation added by Arindam in 20-Nov 2011 
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-1-4-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of5
            drMark("X2") = X1of3
            drMark("Y2") = Y2of5
            drMark("X3") = X1of1
            drMark("Y3") = Y2of5
            drMark("X4") = X3of3
            drMark("Y4") = Y2of5

            drMark("X5") = X1of1
            drMark("Y5") = Y3of5

            drMark("X6") = X1of4
            drMark("Y6") = Y4of5
            drMark("X7") = X2of4
            drMark("Y7") = Y4of5
            drMark("X8") = X3of4
            drMark("Y8") = Y4of5
            drMark("X9") = X4of4
            drMark("Y9") = Y4of5

            drMark("X10") = X1of2
            drMark("Y10") = Y5of5
            drMark("X11") = X2of2
            drMark("Y11") = Y5of5
            'drMark("X11") = X1of1
            'drMark("Y11") = Y5of5
            drMark("Line1Y") = Y2of5 + CONTROL_HEIGHT + CInt((Y1of5 - (Y2of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of5 + CONTROL_HEIGHT + CInt((Y2of5 - (Y3of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of5 + CONTROL_HEIGHT + CInt((Y3of5 - (Y4of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of5 + CONTROL_HEIGHT + CInt((Y4of5 - (Y5of5 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            'TOSOCRS-277 ---------------- begin
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-2-1-2"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of4
            drMark("Y2") = Y3of7
            drMark("X3") = X2of4
            drMark("Y3") = Y2of7
            drMark("X4") = X3of4
            drMark("Y4") = Y2of7
            drMark("X5") = X4of4
            drMark("Y5") = Y3of7
            drMark("X6") = X1of3
            drMark("Y6") = Y5of7
            drMark("X7") = X1of1
            drMark("Y7") = Y4of7
            drMark("X8") = X1of1
            drMark("Y8") = Y6of7
            drMark("X9") = X3of3
            drMark("Y9") = Y5of7
            drMark("X10") = X2of4
            drMark("Y10") = Y7of7
            drMark("X11") = X3of4
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of7 + CONTROL_HEIGHT + CInt((Y3of7 - (Y4of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y6of7 + CONTROL_HEIGHT + CInt((Y5of7 - (Y6of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "4-1-2-1-2"
            drMark("FormationSpecification") = "7"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of8
            drMark("X2") = X1of3
            drMark("Y2") = Y3of8
            drMark("X3") = X1of1
            drMark("Y3") = Y2of8
            drMark("X4") = X1of1
            drMark("Y4") = Y4of8
            drMark("X5") = X3of3
            drMark("Y5") = Y3of8
            drMark("X6") = X1of3
            drMark("Y6") = Y6of8
            drMark("X7") = X1of1
            drMark("Y7") = Y5of8
            drMark("X8") = X1of1
            drMark("Y8") = Y7of8
            drMark("X9") = X3of3
            drMark("Y9") = Y6of8
            drMark("X10") = X2of4
            drMark("Y10") = Y8of8
            drMark("X11") = X3of4
            drMark("Y11") = Y8of8
            drMark("Line1Y") = Y2of8 + CONTROL_HEIGHT + CInt((Y1of8 - (Y2of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line2Y") = Y5of8 + CONTROL_HEIGHT + CInt((Y4of8 - (Y5of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line3Y") = Y6of8 + CONTROL_HEIGHT + CInt((Y5of8 - (Y6of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line4Y") = Y7of8 + CONTROL_HEIGHT + CInt((Y6of8 - (Y7of8 + CONTROL_HEIGHT)) / 2) - 1
            drMark("Line5Y") = Y8of8 + CONTROL_HEIGHT + CInt((Y7of8 - (Y8of8 + CONTROL_HEIGHT)) / 2) - 1
            m_dtFormationMarks.Rows.Add(drMark)
            'TOSOCRS-277 ---------------- end

            'TOPZ-515 --------------- begin
            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-3-3-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of7
            drMark("X2") = X1of3
            drMark("Y2") = Y2of7
            drMark("X3") = X2of3
            drMark("Y3") = Y2of7
            drMark("X4") = X3of3
            drMark("Y4") = Y2of7
            drMark("X5") = X1of3
            drMark("Y5") = Y4of7
            drMark("X6") = X2of3
            drMark("Y6") = Y3of7
            drMark("X7") = X3of3
            drMark("Y7") = Y4of7
            drMark("X8") = X1of3
            drMark("Y8") = Y6of7
            drMark("X9") = X2of3
            drMark("Y9") = Y5of7
            drMark("X10") = X3of3
            drMark("Y10") = Y6of7
            drMark("X11") = X1of1
            drMark("Y11") = Y7of7
            drMark("Line1Y") = Y2of7 + CONTROL_HEIGHT + CInt((Y1of7 - (Y2of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of7 + CONTROL_HEIGHT + CInt((Y2of7 - (Y3of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of7 + CONTROL_HEIGHT + CInt((Y4of7 - (Y5of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y7of7 + CONTROL_HEIGHT + CInt((Y6of7 - (Y7of7 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-3-2-1-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of3
            drMark("Y2") = Y2of6
            drMark("X3") = X2of3
            drMark("Y3") = Y2of6
            drMark("X4") = X3of3
            drMark("Y4") = Y2of6
            drMark("X5") = X1of3
            drMark("Y5") = Y3of6
            drMark("X6") = X2of3
            drMark("Y6") = Y3of6
            drMark("X7") = X3of3
            drMark("Y7") = Y3of6
            drMark("X8") = X1of2
            drMark("Y8") = Y4of6
            drMark("X9") = X2of2
            drMark("Y9") = Y4of6
            drMark("X10") = X1of1
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of6 + CONTROL_HEIGHT + CInt((Y2of6 - (Y3of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "3-3-2-1-1"
            drMark("FormationSpecification") = "4"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of3
            drMark("Y2") = Y2of6
            drMark("X3") = X2of3
            drMark("Y3") = Y2of6
            drMark("X4") = X3of3
            drMark("Y4") = Y2of6
            drMark("X5") = X1of3
            drMark("Y5") = Y3of6
            drMark("X6") = X2of3
            drMark("Y6") = Y3of6
            drMark("X7") = X3of3
            drMark("Y7") = Y3of6
            drMark("X8") = X1of4
            drMark("Y8") = Y4of6
            drMark("X9") = X4of4
            drMark("Y9") = Y4of6
            drMark("X10") = X1of1
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y3of6 + CONTROL_HEIGHT + CInt((Y2of6 - (Y3of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            m_dtFormationMarks.Rows.Add(drMark)

            drMark = m_dtFormationMarks.NewRow()
            drMark("Formation") = "5-1-3-1"
            drMark("FormationSpecification") = "1"
            drMark("X1") = X1of1
            drMark("Y1") = Y1of6
            drMark("X2") = X1of5
            drMark("Y2") = Y3of6
            drMark("X3") = X2of5
            drMark("Y3") = Y2of6
            drMark("X4") = X1of1
            drMark("Y4") = Y2of6
            drMark("X5") = X4of5
            drMark("Y5") = Y2of6
            drMark("X6") = X5of5
            drMark("Y6") = Y3of6
            drMark("X7") = X1of1
            drMark("Y7") = Y4of6
            drMark("X8") = X1of3
            drMark("Y8") = Y5of6
            drMark("X9") = X2of3
            drMark("Y9") = Y5of6
            drMark("X10") = X3of3
            drMark("Y10") = Y5of6
            drMark("X11") = X1of1
            drMark("Y11") = Y6of6
            drMark("Line1Y") = Y2of6 + CONTROL_HEIGHT + CInt((Y1of6 - (Y2of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line2Y") = Y4of6 + CONTROL_HEIGHT + CInt((Y3of6 - (Y4of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line3Y") = Y5of6 + CONTROL_HEIGHT + CInt((Y4of6 - (Y5of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line4Y") = Y6of6 + CONTROL_HEIGHT + CInt((Y5of6 - (Y6of6 + CONTROL_HEIGHT)) / 2)
            drMark("Line5Y") = 0
            m_dtFormationMarks.Rows.Add(drMark)
            'TOPZ-515 ----------------- end
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Public methods "
    Public Function USFGetBitmap() As Bitmap
        Try

            'create a bitmap of the user control in it's current stage
            Dim bmpMe As New Bitmap(Me.Width, Me.Height)
            Me.DrawToBitmap(bmpMe, New Rectangle(0, 0, bmpMe.Width, bmpMe.Height))
            'clip the image to display formations without list box
            'Dim bmpRet As Bitmap = bmpMe.Clone(New Rectangle(0, 0, bmpMe.Width - (Me.Width - pnlField.Width), bmpMe.Height), bmpMe.PixelFormat)
            Dim bmpRet As Bitmap = bmpMe.Clone(New Rectangle(0, 0, bmpMe.Width - (Me.Width - pnlField.Width), bmpMe.Height), bmpMe.PixelFormat)

            Return bmpRet
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub USFRefreshFormationDisplay()
        Try
            Dim strLastName As String
            Dim strMoniker As String

            'clear all previous markings and list
            pnlField.Controls.Clear()
            'lstPlayers.DataSource = Nothing
            lstPlayers.Items.Clear()
            ReDim m_intLineY(0)
            m_intLineY(0) = 0
            pnlField.Refresh()

            If m_blnPrintMode = True Then
                pnlField.BackColor = Color.MintCream
            Else
                pnlField.BackColor = Color.MediumSeaGreen
            End If

            'display only if both formation and formation specification is available
            If Not m_strFormation Is Nothing And Not m_strFormationSpecification Is Nothing Then
                If Not m_strFormation.Trim = "" And Not m_strFormationSpecification.Trim = "" Then

                    Dim ptXY As Point

                    ' Dim drMarks() As DataRow = m_dtFormationMarks.Select("Formation = '" & m_strFormation & "' and FormationSpecification = '" & m_strFormationSpecification.ToUpper & "'")
                    Dim drMarks() As DataRow = m_dtFormationMarks.Select("Formation = '" & m_strFormation & "' and FormationSpecification = '" & m_strFormationSpecification.ToString & "'")

                    'display player holders
                    If drMarks.Length > 0 Then
                        For i As Integer = 1 To 11
                            ptXY.X = CInt(drMarks(0)("X" & i.ToString))
                            ptXY.Y = CInt(drMarks(0)("Y" & i.ToString))

                            ReDim m_intLineY(5)
                            For j As Integer = 1 To 5
                                If CInt(drMarks(0)("Line" & j.ToString & "Y")) > 0 Then
                                    m_intLineY(j - 1) = CInt(drMarks(0)("Line" & j.ToString & "Y"))
                                End If
                            Next
                            pnlField.Refresh()

                            Dim pnlPlayerPanel As New Panel
                            pnlPlayerPanel.AllowDrop = True 'for drag-and-drop
                            pnlPlayerPanel.Name = "pnlPlayer" & i.ToString
                            pnlPlayerPanel.BackColor = Color.Gainsboro
                            pnlPlayerPanel.Height = CONTROL_HEIGHT
                            pnlPlayerPanel.Width = CONTROL_WIDTH
                            pnlPlayerPanel.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
                            pnlField.Controls.Add(pnlPlayerPanel)
                            pnlPlayerPanel.Location = ptXY

                            Dim lblTopLabel As New Label
                            lblTopLabel.Name = "lblTop" & i.ToString
                            lblTopLabel.Height = LABEL_HEIGHT
                            lblTopLabel.Width = CONTROL_WIDTH
                            lblTopLabel.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
                            lblTopLabel.TextAlign = ContentAlignment.TopCenter
                            If Not m_clrShirt.IsEmpty Then
                                lblTopLabel.BackColor = m_clrShirt
                                lblTopLabel.ForeColor = clsUtility.GetTextColor(m_clrShirt)
                                If lblTopLabel.ForeColor = Color.White And m_blnPrintMode = True Then
                                    lblTopLabel.Font = New Font(lblTopLabel.Font, FontStyle.Bold)
                                End If
                            Else
                                lblTopLabel.BackColor = pnlField.BackColor
                            End If
                            pnlPlayerPanel.Controls.Add(lblTopLabel)
                            lblTopLabel.Left = -1
                            lblTopLabel.Top = -1

                            Dim lblBottomLabel As New Label
                            lblBottomLabel.AutoSize = True
                            lblBottomLabel.Name = "lblBottom" & i.ToString
                            lblBottomLabel.MaximumSize = New System.Drawing.Size(CONTROL_WIDTH, 0)
                            lblBottomLabel.TextAlign = ContentAlignment.TopCenter
                            If Not m_clrShort.IsEmpty Then
                                lblBottomLabel.BackColor = m_clrShort
                                lblBottomLabel.ForeColor = clsUtility.GetTextColor(m_clrShort)
                                If lblBottomLabel.ForeColor = Color.White And m_blnPrintMode = True Then
                                    lblBottomLabel.Font = New Font(lblBottomLabel.Font, FontStyle.Bold)
                                End If
                            End If
                            pnlPlayerPanel.Controls.Add(lblBottomLabel)
                            lblBottomLabel.Left = -1
                            lblBottomLabel.Top = CONTROL_HEIGHT - LABEL_HEIGHT

                            pnlPlayerPanel.BackColor = m_clrShort
                            AddHandler pnlPlayerPanel.DragOver, AddressOf PanelDragOver
                            AddHandler pnlPlayerPanel.DragDrop, AddressOf PanelDragDrop

                            AddHandler lblTopLabel.MouseDown, AddressOf PanelMouseDown
                            AddHandler lblBottomLabel.MouseDown, AddressOf PanelMouseDown
                            AddHandler lblTopLabel.MouseUp, AddressOf PanelMouseUp
                            AddHandler lblBottomLabel.MouseUp, AddressOf PanelMouseUp
                            AddHandler lblTopLabel.MouseMove, AddressOf PanelMouseMove
                            AddHandler lblBottomLabel.MouseMove, AddressOf PanelMouseMove
                            AddHandler lblTopLabel.QueryContinueDrag, AddressOf PanelQueryContinueDrag
                            AddHandler lblBottomLabel.QueryContinueDrag, AddressOf PanelQueryContinueDrag
                        Next
                    End If

                    If Not m_dtRoster Is Nothing Then
                        'update player info in starting lineup datatable 
                        If Not m_dtStartingLineup Is Nothing Then
                            For Each drSL As DataRow In m_dtStartingLineup.Rows
                                For Each drRoster As DataRow In m_dtRoster.Rows
                                    If drSL("PLAYER_ID").Equals(drRoster("PLAYER_ID")) Then
                                        drSL("LAST_NAME") = drRoster("LAST_NAME")
                                        drSL("MONIKER") = drRoster("MONIKER")
                                        drSL("POSITION_ABBREV") = drRoster("POSITION_ABBREV")
                                    End If
                                Next
                            Next

                            'remove the players already in lineup from the player list
                            For Each drSL As DataRow In m_dtStartingLineup.Rows
                                For Each drRoster As DataRow In m_dtRoster.Rows
                                    If drSL("PLAYER_ID").Equals(drRoster("PLAYER_ID")) Then
                                        drRoster.Delete()
                                        drRoster.AcceptChanges()
                                        Exit For
                                    End If
                                Next
                            Next

                            'remove the players already in bench from the player list
                            For Each drBench As DataRow In m_dtBench.Rows
                                For Each drRoster As DataRow In m_dtRoster.Rows
                                    If drBench("PLAYER_ID").Equals(drRoster("PLAYER_ID")) Then
                                        drRoster.Delete()
                                        drRoster.AcceptChanges()
                                        Exit For
                                    End If
                                Next
                            Next

                            'display players
                            For i As Integer = 1 To m_dtStartingLineup.Rows.Count
                                For Each pnl As Control In pnlField.Controls
                                    If pnl.Name = "pnlPlayer" & m_dtStartingLineup.Rows(i - 1)("LINEUP").ToString Then
                                        For Each lbl As Control In pnl.Controls
                                            If lbl.Name.Contains("lblTop") Then
                                                lbl.Text = m_dtStartingLineup.Rows(i - 1)("UNIFORM").ToString & Space(1) & "(" & m_dtStartingLineup.Rows(i - 1)("POSITION_ABBREV").ToString & ")"
                                            ElseIf lbl.Name.Contains("lblBottom") Then
                                                strLastName = m_dtStartingLineup.Rows(i - 1)("LAST_NAME").ToString
                                                If m_dtStartingLineup.Rows(i - 1)("MONIKER").ToString() = "" Then
                                                    strMoniker = ""
                                                Else
                                                    strMoniker = m_dtStartingLineup.Rows(i - 1)("MONIKER").ToString.Chars(0)
                                                End If

                                                'strMoniker = m_dtStartingLineup.Rows(i - 1)("MONIKER").ToString.Chars(0)
                                                lbl.Text = strLastName & Space(1) & strMoniker
                                                lbl.Left = CInt(lbl.Parent.Width / 2 - lbl.Width / 2)

                                                'trim last name if the label text overflows
                                                While lbl.Height > 20
                                                    strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                                    lbl.Text = strLastName & "." & Space(1) & strMoniker
                                                    lbl.Left = 0
                                                End While
                                            End If
                                        Next
                                    End If
                                Next
                            Next
                        End If

                        For Each dr As DataRow In m_dtRoster.Rows
                            lstPlayers.Items.Add(dr("PLAYER"))
                        Next

                        btnApply.Enabled = False
                        btnReset.Enabled = False
                        blnDirtyFlag = False
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Drag-and-drop "
    '-------------------- Listbox to Panels -------------------------------------------------------------------------------
    Private Sub PanelDragOver(ByVal sender As Object, ByVal e As DragEventArgs)
        Try
            ' Determine whether string data exists in the drop data. If not, then
            ' the drop effect reflects that the drop cannot occur.
            If Not (e.Data.GetDataPresent(GetType(System.String))) Then

                e.Effect = DragDropEffects.None
                Return
            End If

            ' Set the effect based upon the KeyState.
            If ((e.KeyState And (8 + 32)) = (8 + 32) And _
                (e.AllowedEffect And DragDropEffects.Link) = DragDropEffects.Link) Then
                ' KeyState 8 + 32 = CTL + ALT

                ' Link drag-and-drop effect.
                e.Effect = DragDropEffects.Link

            ElseIf ((e.KeyState And 32) = 32 And _
                (e.AllowedEffect And DragDropEffects.Link) = DragDropEffects.Link) Then

                ' ALT KeyState for link.
                e.Effect = DragDropEffects.Link

            ElseIf ((e.KeyState And 4) = 4 And _
                (e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move) Then

                ' SHIFT KeyState for move.
                e.Effect = DragDropEffects.Move

            ElseIf ((e.KeyState And 8) = 8 And _
                (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy) Then

                ' CTL KeyState for copy.
                e.Effect = DragDropEffects.Copy

            ElseIf ((e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move) Then

                ' By default, the drop action should be move, if allowed.
                e.Effect = DragDropEffects.Move

            Else
                e.Effect = DragDropEffects.None
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PanelDragDrop(ByVal sender As Object, ByVal e As DragEventArgs)
        Try
            Dim strTopLabelText As String = ""
            Dim strLastName As String = ""
            Dim strMoniker As String
            Dim intLineup As Integer
            Dim blnFromListbox As Boolean
            Dim drSourceData As DataRow()

            ' Ensures that the list item index is contained in the data.

            If (e.Data.GetDataPresent(GetType(System.String))) Then

                Dim item As Object = CType(e.Data.GetData(GetType(System.String)), System.Object)
                If IsNumeric(item.ToString) = True Then
                    blnFromListbox = False
                    drSourceData = m_dtStartingLineup.Select("LINEUP =" & item.ToString)
                    strTopLabelText = drSourceData(0)("UNIFORM").ToString & Space(1) & "(" & drSourceData(0)("POSITION_ABBREV").ToString & ")"
                    strLastName = drSourceData(0)("LAST_NAME").ToString
                    If drSourceData(0)("MONIKER").ToString() = "" Then
                        strMoniker = ""
                    Else
                        strMoniker = drSourceData(0)("MONIKER").ToString.Chars(0)
                    End If
                Else
                    blnFromListbox = True
                    strTopLabelText = m_dtRoster.Rows(lstPlayers.SelectedIndex)("UNIFORM").ToString & Space(1) & "(" & m_dtRoster.Rows(lstPlayers.SelectedIndex)("POSITION_ABBREV").ToString & ")"
                    strLastName = m_dtRoster.Rows(lstPlayers.SelectedIndex)("LAST_NAME").ToString
                    If m_dtRoster.Rows(lstPlayers.SelectedIndex)("MONIKER").ToString() = "" Then
                        strMoniker = ""
                    Else
                        strMoniker = m_dtRoster.Rows(lstPlayers.SelectedIndex)("MONIKER").ToString.Chars(0)
                    End If
                End If

                ' Perform drag-and-drop, depending upon the effect.
                If (e.Effect = DragDropEffects.Copy Or _
                    e.Effect = DragDropEffects.Move) Then

                    ' Insert the item.
                    Dim pnl As Panel = CType(sender, Panel)
                    intLineup = CInt(pnl.Name.Replace("pnlPlayer", "").Trim)

                    For Each ctrl As Control In pnl.Controls
                        If ctrl.Name.Contains("lbl") Then
                            Dim lblOther As Label = CType(ctrl, Label)
                            If lblOther.Name.Contains("lblTop") Then
                                lblOther.Text = strTopLabelText
                            ElseIf lblOther.Name.Contains("lblBottom") Then
                                lblOther.Text = strLastName & Space(1) & strMoniker
                                lblOther.Left = CInt(lblOther.Parent.Width / 2 - lblOther.Width / 2)

                                'trim last name if the label text overflows
                                While lblOther.Height > 20
                                    strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                    lblOther.Text = strLastName & "." & Space(1) & strMoniker
                                    lblOther.Left = 0
                                End While

                                'remove the replaced player info from startinglineup datatable and insert into roster if player existed in panel
                                Dim drReplacedPlayer As DataRow() = m_dtStartingLineup.Select("LINEUP =" & intLineup)

                                If drReplacedPlayer.Length > 0 Then
                                    Dim drRoster As DataRow = m_dtRoster.NewRow
                                    drRoster("PLAYER_ID") = drReplacedPlayer(0)("PLAYER_ID")
                                    drRoster("PLAYER") = drReplacedPlayer(0)("PLAYER")
                                    drRoster("UNIFORM") = drReplacedPlayer(0)("UNIFORM")
                                    drRoster("LAST_NAME") = drReplacedPlayer(0)("LAST_NAME")
                                    drRoster("MONIKER") = drReplacedPlayer(0)("MONIKER")
                                    drRoster("POSITION_ABBREV") = drReplacedPlayer(0)("POSITION_ABBREV")
                                    m_dtRoster.Rows.Add(drRoster)

                                    lstPlayers.Items.Add(drReplacedPlayer(0)("PLAYER"))

                                    drReplacedPlayer(0).Delete()
                                End If

                                Dim drStartingLineup As DataRow = m_dtStartingLineup.NewRow
                                drStartingLineup("LINEUP") = intLineup

                                If blnFromListbox = True Then
                                    'insert the dragged player info into startinglineup datatable and remove from roster datatable
                                    drStartingLineup("PLAYER_ID") = m_dtRoster.Rows(lstPlayers.SelectedIndex)("PLAYER_ID")
                                    drStartingLineup("PLAYER") = m_dtRoster.Rows(lstPlayers.SelectedIndex)("PLAYER")
                                    drStartingLineup("UNIFORM") = m_dtRoster.Rows(lstPlayers.SelectedIndex)("UNIFORM")
                                    drStartingLineup("LAST_NAME") = m_dtRoster.Rows(lstPlayers.SelectedIndex)("LAST_NAME")
                                    drStartingLineup("MONIKER") = m_dtRoster.Rows(lstPlayers.SelectedIndex)("MONIKER")
                                    drStartingLineup("POSITION_ABBREV") = m_dtRoster.Rows(lstPlayers.SelectedIndex)("POSITION_ABBREV")

                                    m_dtRoster.Rows.RemoveAt(lstPlayers.SelectedIndex)

                                    'raise USFDragDrop event here
                                    Dim dde As New SFDDEventArgs(CInt(drStartingLineup("PLAYER_ID")), drStartingLineup("PLAYER").ToString, drStartingLineup("POSITION_ABBREV").ToString, SFDDEventArgs.DDControl.List, SFDDEventArgs.DDControl.Field)
                                    RaiseEvent USFDragDrop(sender, dde)
                                Else
                                    'insert the dragged player info into startinglineup datatable and remove from it's earlier row in the same table
                                    drStartingLineup("PLAYER_ID") = drSourceData(0)("PLAYER_ID")
                                    drStartingLineup("PLAYER") = drSourceData(0)("PLAYER")
                                    drStartingLineup("UNIFORM") = drSourceData(0)("UNIFORM")
                                    drStartingLineup("LAST_NAME") = drSourceData(0)("LAST_NAME")
                                    drStartingLineup("MONIKER") = drSourceData(0)("MONIKER")
                                    drStartingLineup("POSITION_ABBREV") = drSourceData(0)("POSITION_ABBREV")

                                    drSourceData(0).Delete()

                                    'raise USFDragDrop event here
                                    Dim dde As New SFDDEventArgs(CInt(drStartingLineup("PLAYER_ID")), drStartingLineup("PLAYER").ToString, drStartingLineup("POSITION_ABBREV").ToString, SFDDEventArgs.DDControl.List, SFDDEventArgs.DDControl.Field)
                                    RaiseEvent USFDragDrop(sender, dde)
                                End If
                                m_dtStartingLineup.Rows.Add(drStartingLineup)
                            End If
                        End If
                        blnDirtyFlag = True
                    Next
                End If
            End If

            btnApply.Enabled = blnDirtyFlag
            btnReset.Enabled = blnDirtyFlag
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lstPlayers_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstPlayers.MouseDown
        Try
            ' Get the index of the item the mouse is below.
            indexOfItemUnderMouseToDrag = lstPlayers.IndexFromPoint(e.X, e.Y)

            If (indexOfItemUnderMouseToDrag <> ListBox.NoMatches) Then

                ' Remember the point where the mouse down occurred. The DragSize indicates
                ' the size that the mouse can move before a drag event should be started.                
                Dim dragSize As Size = SystemInformation.DragSize

                ' Create a rectangle using the DragSize, with the mouse position being
                ' at the center of the rectangle.
                dragBoxFromMouseDown = New Rectangle(New Point(CInt(e.X - (dragSize.Width / 2)), _
                                                                CInt(e.Y - (dragSize.Height / 2))), dragSize)
                lstPlayers.AllowDrop = False
            Else
                ' Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lstPlayers_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstPlayers.MouseMove
        If ((e.Button And MouseButtons.Left) = MouseButtons.Left) Then

            ' If the mouse moves outside the rectangle, start the drag.
            If (Rectangle.op_Inequality(dragBoxFromMouseDown, Rectangle.Empty) And _
                Not dragBoxFromMouseDown.Contains(e.X, e.Y)) Then

                ' Creates custom cursors for the drag-and-drop operation.
                Try
                    MyNormalCursor = New Cursor("3dwarro.cur")
                    MyNoDropCursor = New Cursor("3dwno.cur")

                Catch
                    ' An error occurred while attempting to load the cursors so use
                    ' standard cursors.
                Finally
                    ' The screenOffset is used to account for any desktop bands 
                    ' that may be at the top or left side of the screen when 
                    ' determining when to cancel the drag drop operation.
                    screenOffset = SystemInformation.WorkingArea.Location

                    ' Proceed with the drag-and-drop, passing in the list item.                    
                    Dim dropEffect As DragDropEffects = lstPlayers.DoDragDrop(lstPlayers.Items(indexOfItemUnderMouseToDrag), _
                                                                                  DragDropEffects.All Or DragDropEffects.Link)

                    ' If the drag operation was a move then remove the item.
                    If (dropEffect = DragDropEffects.Move) Then
                        lstPlayers.Items.RemoveAt(indexOfItemUnderMouseToDrag)

                        ' Select the previous item in the list as long as the list has an item.
                        If (indexOfItemUnderMouseToDrag > 0) Then
                            lstPlayers.SelectedIndex = indexOfItemUnderMouseToDrag - 1

                        ElseIf (lstPlayers.Items.Count > 0) Then
                            ' Selects the first item.
                            lstPlayers.SelectedIndex = 0
                        End If
                    End If

                    ' Dispose the cursors since they are no longer needed.
                    If (Not MyNormalCursor Is Nothing) Then _
                        MyNormalCursor.Dispose()

                    If (Not MyNoDropCursor Is Nothing) Then _
                        MyNoDropCursor.Dispose()
                End Try

            End If
        End If
    End Sub

    Private Sub lstPlayers_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstPlayers.MouseUp
        Try
            ' Reset the drag rectangle when the mouse button is raised.
            dragBoxFromMouseDown = Rectangle.Empty
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lstPlayers_QueryContinueDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.QueryContinueDragEventArgs) Handles lstPlayers.QueryContinueDrag
        Try
            ' Cancel the drag if the mouse moves off the form.
            Dim lb As ListBox = CType(sender, System.Windows.Forms.ListBox)

            If (lb IsNot Nothing) Then

                Dim f As Form = lb.FindForm()

                ' Cancel the drag if the mouse moves off the form. The screenOffset
                ' takes into account any desktop bands that may be at the top or left
                ' side of the screen.
                If (((Control.MousePosition.X - screenOffset.X) < f.DesktopBounds.Left) Or _
                    ((Control.MousePosition.X - screenOffset.X) > f.DesktopBounds.Right) Or _
                    ((Control.MousePosition.Y - screenOffset.Y) < f.DesktopBounds.Top) Or _
                    ((Control.MousePosition.Y - screenOffset.Y) > f.DesktopBounds.Bottom)) Then

                    e.Action = DragAction.Cancel
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '-------------------- Panels to Listbox -------------------------------------------------------------------------------
    Private Sub PanelMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim intLineup As Integer
            Dim strLineup As String

            Dim lbl As Label = CType(sender, Label)
            strLineup = lbl.Name
            strLineup = strLineup.Replace("lblTop", "").Trim
            strLineup = strLineup.Replace("lblBottom", "").Trim
            intLineup = CInt(strLineup)

            ' Check if the panel conains player
            Dim drSourcePlayer As DataRow() = m_dtStartingLineup.Select("LINEUP =" & intLineup)

            If drSourcePlayer.Length > 0 Then

                ' Remember the point where the mouse down occurred. The DragSize indicates
                ' the size that the mouse can move before a drag event should be started.                
                Dim dragSize As Size = SystemInformation.DragSize

                ' Create a rectangle using the DragSize, with the mouse position being
                ' at the center of the rectangle.
                dragBoxFromMouseDown = New Rectangle(New Point(CInt(e.X - (dragSize.Width / 2)), _
                                                                CInt(e.Y - (dragSize.Height / 2))), dragSize)
                lstPlayers.AllowDrop = True
            Else
                ' Reset the rectangle if the panel doesnot contain player.
                dragBoxFromMouseDown = Rectangle.Empty
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PanelMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            ' Reset the drag rectangle when the mouse button is raised.
            dragBoxFromMouseDown = Rectangle.Empty
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PanelMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        If ((e.Button And MouseButtons.Left) = MouseButtons.Left) Then
            Dim intLineup As Integer
            Dim strLineup As String

            Dim lbl As Label = CType(sender, Label)
            strLineup = lbl.Name
            strLineup = strLineup.Replace("lblTop", "").Trim
            strLineup = strLineup.Replace("lblBottom", "").Trim
            intLineup = CInt(strLineup)

            ' If the mouse moves outside the rectangle, start the drag.
            If (Rectangle.op_Inequality(dragBoxFromMouseDown, Rectangle.Empty) And _
                Not dragBoxFromMouseDown.Contains(e.X, e.Y)) Then

                ' Creates custom cursors for the drag-and-drop operation.
                Try
                    MyNormalCursor = New Cursor("3dwarro.cur")
                    MyNoDropCursor = New Cursor("3dwno.cur")

                Catch
                    ' An error occurred while attempting to load the cursors so use
                    ' standard cursors.
                Finally
                    ' The screenOffset is used to account for any desktop bands 
                    ' that may be at the top or left side of the screen when 
                    ' determining when to cancel the drag drop operation.
                    screenOffset = SystemInformation.WorkingArea.Location

                    ' Proceed with the drag-and-drop, passing in the panel/player info.                    
                    Dim dropEffect As DragDropEffects = lbl.DoDragDrop(strLineup, DragDropEffects.All Or DragDropEffects.Link)

                    ' If the drag operation was a move then remove the player info from panel labels.
                    If (dropEffect = DragDropEffects.Move) Then
                        For Each ctrl As Control In lbl.Parent.Controls
                            ctrl.Text = ""
                        Next
                    End If

                    ' Dispose the cursors since they are no longer needed.
                    If (Not MyNormalCursor Is Nothing) Then _
                        MyNormalCursor.Dispose()

                    If (Not MyNoDropCursor Is Nothing) Then _
                        MyNoDropCursor.Dispose()
                End Try

            End If
        End If
    End Sub

    Private Sub PanelQueryContinueDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.QueryContinueDragEventArgs)
        Try
            ' Cancel the drag if the mouse moves off the form.
            Dim lbl As Label = CType(sender, Label)

            If (lbl IsNot Nothing) Then

                Dim f As Form = lbl.FindForm()

                ' Cancel the drag if the mouse moves off the form. The screenOffset
                ' takes into account any desktop bands that may be at the top or left
                ' side of the screen.
                If (((Control.MousePosition.X - screenOffset.X) < f.DesktopBounds.Left) Or _
                    ((Control.MousePosition.X - screenOffset.X) > f.DesktopBounds.Right) Or _
                    ((Control.MousePosition.Y - screenOffset.Y) < f.DesktopBounds.Top) Or _
                    ((Control.MousePosition.Y - screenOffset.Y) > f.DesktopBounds.Bottom)) Then

                    e.Action = DragAction.Cancel
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lstPlayers_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lstPlayers.DragOver
        Try
            ' Determine whether string data exists in the drop data. If not, then
            ' the drop effect reflects that the drop cannot occur.
            If Not (e.Data.GetDataPresent(GetType(System.String))) Then

                e.Effect = DragDropEffects.None
                Return
            End If

            ' Set the effect based upon the KeyState.
            If ((e.KeyState And (8 + 32)) = (8 + 32) And _
                (e.AllowedEffect And DragDropEffects.Link) = DragDropEffects.Link) Then
                ' KeyState 8 + 32 = CTL + ALT

                ' Link drag-and-drop effect.
                e.Effect = DragDropEffects.Link

            ElseIf ((e.KeyState And 32) = 32 And _
                (e.AllowedEffect And DragDropEffects.Link) = DragDropEffects.Link) Then

                ' ALT KeyState for link.
                e.Effect = DragDropEffects.Link

            ElseIf ((e.KeyState And 4) = 4 And _
                (e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move) Then

                ' SHIFT KeyState for move.
                e.Effect = DragDropEffects.Move

            ElseIf ((e.KeyState And 8) = 8 And _
                (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy) Then

                ' CTL KeyState for copy.
                e.Effect = DragDropEffects.Copy

            ElseIf ((e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move) Then

                ' By default, the drop action should be move, if allowed.
                e.Effect = DragDropEffects.Move

            Else
                e.Effect = DragDropEffects.None
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lstPlayers_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lstPlayers.DragDrop
        Try
            'Dim strLastName As String
            'Dim strMoniker As String
            Dim intLineup As Integer

            ' Ensures that the list item index is contained in the data.

            If (e.Data.GetDataPresent(GetType(System.String))) Then

                Dim item As Object = CType(e.Data.GetData(GetType(System.String)), System.Object)

                ' Perform drag-and-drop, depending upon the effect.
                If (e.Effect = DragDropEffects.Copy Or _
                    e.Effect = DragDropEffects.Move) Then

                    ' Insert the item.
                    intLineup = CInt(item.ToString)

                    Dim drPanelPlayer As DataRow() = m_dtStartingLineup.Select("LINEUP =" & intLineup)
                    If drPanelPlayer.Length > 0 Then
                        lstPlayers.Items.Add(drPanelPlayer(0)("PLAYER"))
                    End If

                    Dim drRoster As DataRow = m_dtRoster.NewRow
                    drRoster("PLAYER_ID") = drPanelPlayer(0)("PLAYER_ID")
                    drRoster("PLAYER") = drPanelPlayer(0)("PLAYER")
                    drRoster("UNIFORM") = drPanelPlayer(0)("UNIFORM")
                    drRoster("LAST_NAME") = drPanelPlayer(0)("LAST_NAME")
                    drRoster("MONIKER") = drPanelPlayer(0)("MONIKER")
                    drRoster("POSITION_ABBREV") = drPanelPlayer(0)("POSITION_ABBREV")
                    m_dtRoster.Rows.Add(drRoster)

                    drPanelPlayer(0).Delete()
                    blnDirtyFlag = True

                    'raise USFDragDrop event here
                    Dim dde As New SFDDEventArgs(CInt(drRoster("PLAYER_ID")), drRoster("PLAYER").ToString, drRoster("POSITION_ABBREV").ToString, SFDDEventArgs.DDControl.Field, SFDDEventArgs.DDControl.List)
                    RaiseEvent USFDragDrop(sender, dde)
                End If
            End If

            btnApply.Enabled = blnDirtyFlag
            btnReset.Enabled = blnDirtyFlag
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class

Public Class SFEventArgs
    Inherits System.EventArgs

#Region " Constants & Variables "
    Private m_dtStartingLineup As DataTable
#End Region

#Region " Public properties "
    Public ReadOnly Property USFStartingLineup() As DataTable
        Get
            Return m_dtStartingLineup
        End Get
    End Property
#End Region

#Region " Public Methods "
    Public Sub New(ByVal StartingLineup As DataTable)
        Try
            m_dtStartingLineup = StartingLineup
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class

Public Class SFDDEventArgs
    Inherits System.EventArgs

#Region " Constants & Variables "
    Private m_intPlayerId As Integer
    Private m_strPlayerName As String
    Private m_strPosition As String
    Private m_source As DDControl
    Private m_destination As DDControl

    Public Enum DDControl As Integer
        List
        Field
    End Enum
#End Region

#Region " Public properties "
    Public ReadOnly Property USFPlayerId() As Integer
        Get
            Return m_intPlayerId
        End Get
    End Property

    Public ReadOnly Property USFPlayerName() As String
        Get
            Return m_strPlayerName
        End Get
    End Property

    Public ReadOnly Property USFPosition() As String
        Get
            Return m_strPosition
        End Get
    End Property

    Public ReadOnly Property USFSource() As DDControl
        Get
            Return m_source
        End Get
    End Property

    Public ReadOnly Property USFDestination() As DDControl
        Get
            Return m_destination
        End Get
    End Property
#End Region

#Region " Public Methods "
    Public Sub New(ByVal PlayerId As Integer, ByVal PlayerName As String, ByVal Position As String, ByVal Source As DDControl, ByVal Destination As DDControl)
        Try
            m_intPlayerId = PlayerId
            m_strPlayerName = PlayerName
            m_strPosition = Position
            m_source = Source
            m_destination = Destination
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class