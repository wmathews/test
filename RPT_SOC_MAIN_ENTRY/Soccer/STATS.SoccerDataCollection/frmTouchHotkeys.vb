#Region " Options "
Option Explicit On
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public NotInheritable Class frmTouchHotkeys
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private lsupport As New languagesupport
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOk.Text, Nothing, 1, 0)
        Me.Close()
    End Sub

    Private Sub frmTouchHotkeys_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAdandon", Nothing, 1, 0)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                Dim cs As String = DirectCast(ListView1.Items(0), System.Windows.Forms.ListViewItem).SubItems(1).Text



                'DirectCast(ListView1.Items(0),System.Windows.Forms.ListViewItem).SubItems(1).Text




                ListView1.Items(0).SubItems(0).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), ListView1.Items(0).SubItems(0).Text)

                DirectCast(ListView1.Items(0), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(0), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(1), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(1), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(2), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(2), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(3), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(3), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(4), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(4), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(5), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(5), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(6), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(6), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(7), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(7), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(8), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(8), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(9), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(9), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView1.Items(10), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView1.Items(10), System.Windows.Forms.ListViewItem).SubItems(1).Text)

                DirectCast(ListView2.Items(0), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView2.Items(0), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView2.Items(1), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView2.Items(1), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView2.Items(2), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView2.Items(2), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView2.Items(3), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView2.Items(3), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView2.Items(4), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView2.Items(4), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                DirectCast(ListView2.Items(5), System.Windows.Forms.ListViewItem).SubItems(1).Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), DirectCast(ListView2.Items(5), System.Windows.Forms.ListViewItem).SubItems(1).Text)
                btnOk.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnOk.Text)

            End If

        Catch ex As Exception

        End Try
    End Sub

End Class
