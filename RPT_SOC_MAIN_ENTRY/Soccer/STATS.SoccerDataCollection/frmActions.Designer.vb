﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmActions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnFoul = New System.Windows.Forms.Button()
        Me.btnThrowIn = New System.Windows.Forms.Button()
        Me.btnFreeKick = New System.Windows.Forms.Button()
        Me.btnOffSide = New System.Windows.Forms.Button()
        Me.btnShot = New System.Windows.Forms.Button()
        Me.btnGoalKick = New System.Windows.Forms.Button()
        Me.btnCorner = New System.Windows.Forms.Button()
        Me.btnBooking = New System.Windows.Forms.Button()
        Me.btnSubstitute = New System.Windows.Forms.Button()
        Me.btnOOB = New System.Windows.Forms.Button()
        Me.btnObstacle = New System.Windows.Forms.Button()
        Me.btnHomeAction = New System.Windows.Forms.Button()
        Me.btnAwayAction = New System.Windows.Forms.Button()
        Me.pnlAwaySub = New System.Windows.Forms.Panel()
        Me.btnAwaySub32 = New System.Windows.Forms.Button()
        Me.btnAwaySub31 = New System.Windows.Forms.Button()
        Me.btnAwaySub30 = New System.Windows.Forms.Button()
        Me.btnAwaySub29 = New System.Windows.Forms.Button()
        Me.btnAwaySub28 = New System.Windows.Forms.Button()
        Me.btnAwaySub27 = New System.Windows.Forms.Button()
        Me.btnAwaySub26 = New System.Windows.Forms.Button()
        Me.btnAwaySub25 = New System.Windows.Forms.Button()
        Me.btnAwaySub24 = New System.Windows.Forms.Button()
        Me.btnAwaySub23 = New System.Windows.Forms.Button()
        Me.btnAwaySub22 = New System.Windows.Forms.Button()
        Me.btnAwaySub21 = New System.Windows.Forms.Button()
        Me.btnAwaySub20 = New System.Windows.Forms.Button()
        Me.btnAwaySub19 = New System.Windows.Forms.Button()
        Me.btnAwaySub18 = New System.Windows.Forms.Button()
        Me.btnAwaySub17 = New System.Windows.Forms.Button()
        Me.btnAwaySub16 = New System.Windows.Forms.Button()
        Me.btnAwaySub15 = New System.Windows.Forms.Button()
        Me.btnAwaySub14 = New System.Windows.Forms.Button()
        Me.btnAwaySub13 = New System.Windows.Forms.Button()
        Me.btnAwaySub12 = New System.Windows.Forms.Button()
        Me.btnAwaySub11 = New System.Windows.Forms.Button()
        Me.btnAwaySub10 = New System.Windows.Forms.Button()
        Me.btnAwaySub9 = New System.Windows.Forms.Button()
        Me.btnAwaySub8 = New System.Windows.Forms.Button()
        Me.btnAwaySub7 = New System.Windows.Forms.Button()
        Me.btnAwaySub6 = New System.Windows.Forms.Button()
        Me.btnAwaySub5 = New System.Windows.Forms.Button()
        Me.btnAwaySub4 = New System.Windows.Forms.Button()
        Me.btnAwaySub3 = New System.Windows.Forms.Button()
        Me.btnAwaySub2 = New System.Windows.Forms.Button()
        Me.btnAwaySub1 = New System.Windows.Forms.Button()
        Me.pnlHomeSub = New System.Windows.Forms.Panel()
        Me.btnHomeSub32 = New System.Windows.Forms.Button()
        Me.btnHomeSub31 = New System.Windows.Forms.Button()
        Me.btnHomeSub30 = New System.Windows.Forms.Button()
        Me.btnHomeSub29 = New System.Windows.Forms.Button()
        Me.btnHomeSub28 = New System.Windows.Forms.Button()
        Me.btnHomeSub27 = New System.Windows.Forms.Button()
        Me.btnHomeSub26 = New System.Windows.Forms.Button()
        Me.btnHomeSub25 = New System.Windows.Forms.Button()
        Me.btnHomeSub24 = New System.Windows.Forms.Button()
        Me.btnHomeSub23 = New System.Windows.Forms.Button()
        Me.btnHomeSub22 = New System.Windows.Forms.Button()
        Me.btnHomeSub21 = New System.Windows.Forms.Button()
        Me.btnHomeSub20 = New System.Windows.Forms.Button()
        Me.btnHomeSub19 = New System.Windows.Forms.Button()
        Me.btnHomeSub18 = New System.Windows.Forms.Button()
        Me.btnHomeSub17 = New System.Windows.Forms.Button()
        Me.btnHomeSub16 = New System.Windows.Forms.Button()
        Me.btnHomeSub15 = New System.Windows.Forms.Button()
        Me.btnHomeSub14 = New System.Windows.Forms.Button()
        Me.btnHomeSub13 = New System.Windows.Forms.Button()
        Me.btnHomeSub12 = New System.Windows.Forms.Button()
        Me.btnHomeSub11 = New System.Windows.Forms.Button()
        Me.btnHomeSub10 = New System.Windows.Forms.Button()
        Me.btnHomeSub9 = New System.Windows.Forms.Button()
        Me.btnHomeSub8 = New System.Windows.Forms.Button()
        Me.btnHomeSub7 = New System.Windows.Forms.Button()
        Me.btnHomeSub6 = New System.Windows.Forms.Button()
        Me.btnHomeSub5 = New System.Windows.Forms.Button()
        Me.btnHomeSub4 = New System.Windows.Forms.Button()
        Me.btnHomeSub3 = New System.Windows.Forms.Button()
        Me.btnHomeSub2 = New System.Windows.Forms.Button()
        Me.btnHomeSub1 = New System.Windows.Forms.Button()
        Me.pnlEvents = New System.Windows.Forms.Panel()
        Me.pnlSubEvents = New System.Windows.Forms.Panel()
        Me.dgvPBP = New System.Windows.Forms.DataGridView()
        Me.cmnuTimeline = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteActionToolStripItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnlGoalLblLeft = New System.Windows.Forms.Panel()
        Me.lblGoalAway = New System.Windows.Forms.Label()
        Me.pnlGoalEvents = New System.Windows.Forms.Panel()
        Me.btnOwn = New System.Windows.Forms.Button()
        Me.btnPenalty = New System.Windows.Forms.Button()
        Me.btnNormal = New System.Windows.Forms.Button()
        Me.picYellowRed = New System.Windows.Forms.PictureBox()
        Me.picRed = New System.Windows.Forms.PictureBox()
        Me.picYellow = New System.Windows.Forms.PictureBox()
        Me.grpBooking = New System.Windows.Forms.GroupBox()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.grpOutBounds = New System.Windows.Forms.GroupBox()
        Me.BtnCornerOut = New System.Windows.Forms.Button()
        Me.btnGoalKickOut = New System.Windows.Forms.Button()
        Me.btnThrow = New System.Windows.Forms.Button()
        Me.picCloseOOB = New System.Windows.Forms.PictureBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnRefreshTime1 = New System.Windows.Forms.Button()
        Me.btnRefreshTime2 = New System.Windows.Forms.Button()
        Me.btnHomePass = New System.Windows.Forms.Button()
        Me.btnAwayPass = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.txtTime = New System.Windows.Forms.MaskedTextBox()
        Me.UdcSoccerPlayerPositionsHome = New STATS.SoccerDataCollection.udcSoccerPlayerPositions()
        Me.UdcSoccerPlayerPositionsAway = New STATS.SoccerDataCollection.udcSoccerPlayerPositions()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblEventProp01 = New System.Windows.Forms.Label()
        Me.lstEventProp01 = New System.Windows.Forms.ListBox()
        Me.lblEventProp02 = New System.Windows.Forms.Label()
        Me.lstEventProp02 = New System.Windows.Forms.ListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.pnlAwaySub.SuspendLayout()
        Me.pnlHomeSub.SuspendLayout()
        Me.pnlEvents.SuspendLayout()
        Me.pnlSubEvents.SuspendLayout()
        CType(Me.dgvPBP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuTimeline.SuspendLayout()
        Me.pnlGoalLblLeft.SuspendLayout()
        Me.pnlGoalEvents.SuspendLayout()
        CType(Me.picYellowRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picYellow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBooking.SuspendLayout()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpOutBounds.SuspendLayout()
        CType(Me.picCloseOOB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnFoul
        '
        Me.btnFoul.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFoul.Location = New System.Drawing.Point(0, 84)
        Me.btnFoul.Name = "btnFoul"
        Me.btnFoul.Size = New System.Drawing.Size(91, 26)
        Me.btnFoul.TabIndex = 14
        Me.btnFoul.Tag = "8"
        Me.btnFoul.Text = "Foul"
        Me.btnFoul.UseVisualStyleBackColor = True
        '
        'btnThrowIn
        '
        Me.btnThrowIn.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnThrowIn.Location = New System.Drawing.Point(0, 117)
        Me.btnThrowIn.Name = "btnThrowIn"
        Me.btnThrowIn.Size = New System.Drawing.Size(91, 26)
        Me.btnThrowIn.TabIndex = 12
        Me.btnThrowIn.Tag = "47"
        Me.btnThrowIn.Text = "Throw In"
        Me.btnThrowIn.UseVisualStyleBackColor = True
        '
        'btnFreeKick
        '
        Me.btnFreeKick.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnFreeKick.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFreeKick.Location = New System.Drawing.Point(0, 152)
        Me.btnFreeKick.Name = "btnFreeKick"
        Me.btnFreeKick.Size = New System.Drawing.Size(91, 26)
        Me.btnFreeKick.TabIndex = 11
        Me.btnFreeKick.Tag = "9"
        Me.btnFreeKick.Text = "Free Kick"
        Me.btnFreeKick.UseVisualStyleBackColor = False
        '
        'btnOffSide
        '
        Me.btnOffSide.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOffSide.Location = New System.Drawing.Point(95, 117)
        Me.btnOffSide.Name = "btnOffSide"
        Me.btnOffSide.Size = New System.Drawing.Size(91, 26)
        Me.btnOffSide.TabIndex = 15
        Me.btnOffSide.Tag = "16"
        Me.btnOffSide.Text = "Offside"
        Me.btnOffSide.UseVisualStyleBackColor = True
        '
        'btnShot
        '
        Me.btnShot.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnShot.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShot.Location = New System.Drawing.Point(95, 84)
        Me.btnShot.Name = "btnShot"
        Me.btnShot.Size = New System.Drawing.Size(91, 26)
        Me.btnShot.TabIndex = 16
        Me.btnShot.Tag = "19"
        Me.btnShot.Text = "Shot"
        Me.btnShot.UseVisualStyleBackColor = False
        '
        'btnGoalKick
        '
        Me.btnGoalKick.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalKick.Location = New System.Drawing.Point(95, 152)
        Me.btnGoalKick.Name = "btnGoalKick"
        Me.btnGoalKick.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalKick.TabIndex = 19
        Me.btnGoalKick.Tag = "46"
        Me.btnGoalKick.Text = "Goal Kick"
        Me.btnGoalKick.UseVisualStyleBackColor = True
        '
        'btnCorner
        '
        Me.btnCorner.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCorner.Location = New System.Drawing.Point(52, 184)
        Me.btnCorner.Name = "btnCorner"
        Me.btnCorner.Size = New System.Drawing.Size(91, 26)
        Me.btnCorner.TabIndex = 17
        Me.btnCorner.Tag = "5"
        Me.btnCorner.Text = "Corner"
        Me.btnCorner.UseVisualStyleBackColor = True
        '
        'btnBooking
        '
        Me.btnBooking.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnBooking.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBooking.Location = New System.Drawing.Point(4, 1)
        Me.btnBooking.Name = "btnBooking"
        Me.btnBooking.Size = New System.Drawing.Size(91, 26)
        Me.btnBooking.TabIndex = 22
        Me.btnBooking.Tag = "0"
        Me.btnBooking.Text = "Booking"
        Me.btnBooking.UseVisualStyleBackColor = False
        '
        'btnSubstitute
        '
        Me.btnSubstitute.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubstitute.Location = New System.Drawing.Point(98, 1)
        Me.btnSubstitute.Name = "btnSubstitute"
        Me.btnSubstitute.Size = New System.Drawing.Size(91, 26)
        Me.btnSubstitute.TabIndex = 23
        Me.btnSubstitute.Tag = "22"
        Me.btnSubstitute.Text = "Substitute"
        Me.btnSubstitute.UseVisualStyleBackColor = True
        '
        'btnOOB
        '
        Me.btnOOB.BackColor = System.Drawing.Color.DimGray
        Me.btnOOB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnOOB.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOOB.ForeColor = System.Drawing.Color.Black
        Me.btnOOB.Location = New System.Drawing.Point(0, 2)
        Me.btnOOB.Name = "btnOOB"
        Me.btnOOB.Size = New System.Drawing.Size(188, 47)
        Me.btnOOB.TabIndex = 24
        Me.btnOOB.Tag = "67"
        Me.btnOOB.Text = "Out of Bounds"
        Me.btnOOB.UseVisualStyleBackColor = False
        '
        'btnObstacle
        '
        Me.btnObstacle.BackColor = System.Drawing.Color.DarkGray
        Me.btnObstacle.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnObstacle.Location = New System.Drawing.Point(52, 53)
        Me.btnObstacle.Name = "btnObstacle"
        Me.btnObstacle.Size = New System.Drawing.Size(91, 26)
        Me.btnObstacle.TabIndex = 26
        Me.btnObstacle.Tag = "68"
        Me.btnObstacle.Text = "Obstacle"
        Me.btnObstacle.UseVisualStyleBackColor = False
        '
        'btnHomeAction
        '
        Me.btnHomeAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeAction.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeAction.Location = New System.Drawing.Point(214, 1)
        Me.btnHomeAction.Name = "btnHomeAction"
        Me.btnHomeAction.Size = New System.Drawing.Size(191, 54)
        Me.btnHomeAction.TabIndex = 82
        Me.btnHomeAction.Tag = "63"
        Me.btnHomeAction.Text = "Control"
        Me.btnHomeAction.UseVisualStyleBackColor = False
        '
        'btnAwayAction
        '
        Me.btnAwayAction.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwayAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwayAction.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwayAction.Location = New System.Drawing.Point(608, 1)
        Me.btnAwayAction.Name = "btnAwayAction"
        Me.btnAwayAction.Size = New System.Drawing.Size(191, 54)
        Me.btnAwayAction.TabIndex = 109
        Me.btnAwayAction.Tag = "63"
        Me.btnAwayAction.Text = "Control"
        Me.btnAwayAction.UseVisualStyleBackColor = False
        '
        'pnlAwaySub
        '
        Me.pnlAwaySub.AutoScroll = True
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub32)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub31)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub30)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub29)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub28)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub27)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub26)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub25)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub24)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub23)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub22)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub21)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub20)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub19)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub18)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub17)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub16)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub15)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub14)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub13)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub12)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub11)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub10)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub9)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub8)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub7)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub6)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub5)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub4)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub3)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub2)
        Me.pnlAwaySub.Controls.Add(Me.btnAwaySub1)
        Me.pnlAwaySub.Location = New System.Drawing.Point(669, 286)
        Me.pnlAwaySub.Name = "pnlAwaySub"
        Me.pnlAwaySub.Size = New System.Drawing.Size(337, 84)
        Me.pnlAwaySub.TabIndex = 128
        '
        'btnAwaySub32
        '
        Me.btnAwaySub32.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub32.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub32.Location = New System.Drawing.Point(234, 290)
        Me.btnAwaySub32.Name = "btnAwaySub32"
        Me.btnAwaySub32.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub32.TabIndex = 150
        Me.btnAwaySub32.UseVisualStyleBackColor = False
        '
        'btnAwaySub31
        '
        Me.btnAwaySub31.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub31.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub31.Location = New System.Drawing.Point(156, 290)
        Me.btnAwaySub31.Name = "btnAwaySub31"
        Me.btnAwaySub31.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub31.TabIndex = 149
        Me.btnAwaySub31.UseVisualStyleBackColor = False
        '
        'btnAwaySub30
        '
        Me.btnAwaySub30.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub30.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub30.Location = New System.Drawing.Point(78, 290)
        Me.btnAwaySub30.Name = "btnAwaySub30"
        Me.btnAwaySub30.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub30.TabIndex = 148
        Me.btnAwaySub30.UseVisualStyleBackColor = False
        '
        'btnAwaySub29
        '
        Me.btnAwaySub29.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub29.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub29.Location = New System.Drawing.Point(0, 290)
        Me.btnAwaySub29.Name = "btnAwaySub29"
        Me.btnAwaySub29.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub29.TabIndex = 147
        Me.btnAwaySub29.UseVisualStyleBackColor = False
        '
        'btnAwaySub28
        '
        Me.btnAwaySub28.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub28.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub28.Location = New System.Drawing.Point(234, 248)
        Me.btnAwaySub28.Name = "btnAwaySub28"
        Me.btnAwaySub28.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub28.TabIndex = 146
        Me.btnAwaySub28.UseVisualStyleBackColor = False
        '
        'btnAwaySub27
        '
        Me.btnAwaySub27.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub27.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub27.Location = New System.Drawing.Point(156, 248)
        Me.btnAwaySub27.Name = "btnAwaySub27"
        Me.btnAwaySub27.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub27.TabIndex = 145
        Me.btnAwaySub27.UseVisualStyleBackColor = False
        '
        'btnAwaySub26
        '
        Me.btnAwaySub26.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub26.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub26.Location = New System.Drawing.Point(78, 248)
        Me.btnAwaySub26.Name = "btnAwaySub26"
        Me.btnAwaySub26.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub26.TabIndex = 144
        Me.btnAwaySub26.UseVisualStyleBackColor = False
        '
        'btnAwaySub25
        '
        Me.btnAwaySub25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub25.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub25.Location = New System.Drawing.Point(0, 248)
        Me.btnAwaySub25.Name = "btnAwaySub25"
        Me.btnAwaySub25.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub25.TabIndex = 143
        Me.btnAwaySub25.UseVisualStyleBackColor = False
        '
        'btnAwaySub24
        '
        Me.btnAwaySub24.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub24.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub24.Location = New System.Drawing.Point(234, 206)
        Me.btnAwaySub24.Name = "btnAwaySub24"
        Me.btnAwaySub24.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub24.TabIndex = 142
        Me.btnAwaySub24.UseVisualStyleBackColor = False
        '
        'btnAwaySub23
        '
        Me.btnAwaySub23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub23.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub23.Location = New System.Drawing.Point(156, 206)
        Me.btnAwaySub23.Name = "btnAwaySub23"
        Me.btnAwaySub23.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub23.TabIndex = 141
        Me.btnAwaySub23.UseVisualStyleBackColor = False
        '
        'btnAwaySub22
        '
        Me.btnAwaySub22.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub22.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub22.Location = New System.Drawing.Point(78, 206)
        Me.btnAwaySub22.Name = "btnAwaySub22"
        Me.btnAwaySub22.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub22.TabIndex = 140
        Me.btnAwaySub22.UseVisualStyleBackColor = False
        '
        'btnAwaySub21
        '
        Me.btnAwaySub21.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub21.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub21.Location = New System.Drawing.Point(0, 206)
        Me.btnAwaySub21.Name = "btnAwaySub21"
        Me.btnAwaySub21.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub21.TabIndex = 139
        Me.btnAwaySub21.UseVisualStyleBackColor = False
        '
        'btnAwaySub20
        '
        Me.btnAwaySub20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub20.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub20.Location = New System.Drawing.Point(234, 164)
        Me.btnAwaySub20.Name = "btnAwaySub20"
        Me.btnAwaySub20.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub20.TabIndex = 138
        Me.btnAwaySub20.UseVisualStyleBackColor = False
        '
        'btnAwaySub19
        '
        Me.btnAwaySub19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub19.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub19.Location = New System.Drawing.Point(156, 164)
        Me.btnAwaySub19.Name = "btnAwaySub19"
        Me.btnAwaySub19.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub19.TabIndex = 137
        Me.btnAwaySub19.UseVisualStyleBackColor = False
        '
        'btnAwaySub18
        '
        Me.btnAwaySub18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub18.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub18.Location = New System.Drawing.Point(78, 164)
        Me.btnAwaySub18.Name = "btnAwaySub18"
        Me.btnAwaySub18.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub18.TabIndex = 136
        Me.btnAwaySub18.UseVisualStyleBackColor = False
        '
        'btnAwaySub17
        '
        Me.btnAwaySub17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub17.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub17.Location = New System.Drawing.Point(0, 164)
        Me.btnAwaySub17.Name = "btnAwaySub17"
        Me.btnAwaySub17.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub17.TabIndex = 135
        Me.btnAwaySub17.UseVisualStyleBackColor = False
        '
        'btnAwaySub16
        '
        Me.btnAwaySub16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub16.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub16.Location = New System.Drawing.Point(234, 124)
        Me.btnAwaySub16.Name = "btnAwaySub16"
        Me.btnAwaySub16.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub16.TabIndex = 134
        Me.btnAwaySub16.UseVisualStyleBackColor = False
        '
        'btnAwaySub15
        '
        Me.btnAwaySub15.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub15.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub15.Location = New System.Drawing.Point(156, 124)
        Me.btnAwaySub15.Name = "btnAwaySub15"
        Me.btnAwaySub15.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub15.TabIndex = 133
        Me.btnAwaySub15.UseVisualStyleBackColor = False
        '
        'btnAwaySub14
        '
        Me.btnAwaySub14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub14.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub14.Location = New System.Drawing.Point(78, 124)
        Me.btnAwaySub14.Name = "btnAwaySub14"
        Me.btnAwaySub14.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub14.TabIndex = 132
        Me.btnAwaySub14.UseVisualStyleBackColor = False
        '
        'btnAwaySub13
        '
        Me.btnAwaySub13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub13.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub13.Location = New System.Drawing.Point(0, 124)
        Me.btnAwaySub13.Name = "btnAwaySub13"
        Me.btnAwaySub13.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub13.TabIndex = 131
        Me.btnAwaySub13.UseVisualStyleBackColor = False
        '
        'btnAwaySub12
        '
        Me.btnAwaySub12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub12.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub12.Location = New System.Drawing.Point(234, 83)
        Me.btnAwaySub12.Name = "btnAwaySub12"
        Me.btnAwaySub12.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub12.TabIndex = 130
        Me.btnAwaySub12.UseVisualStyleBackColor = False
        '
        'btnAwaySub11
        '
        Me.btnAwaySub11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub11.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub11.Location = New System.Drawing.Point(156, 83)
        Me.btnAwaySub11.Name = "btnAwaySub11"
        Me.btnAwaySub11.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub11.TabIndex = 129
        Me.btnAwaySub11.UseVisualStyleBackColor = False
        '
        'btnAwaySub10
        '
        Me.btnAwaySub10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub10.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub10.Location = New System.Drawing.Point(78, 83)
        Me.btnAwaySub10.Name = "btnAwaySub10"
        Me.btnAwaySub10.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub10.TabIndex = 128
        Me.btnAwaySub10.UseVisualStyleBackColor = False
        '
        'btnAwaySub9
        '
        Me.btnAwaySub9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub9.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub9.Location = New System.Drawing.Point(0, 83)
        Me.btnAwaySub9.Name = "btnAwaySub9"
        Me.btnAwaySub9.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub9.TabIndex = 127
        Me.btnAwaySub9.UseVisualStyleBackColor = False
        '
        'btnAwaySub8
        '
        Me.btnAwaySub8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub8.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub8.Location = New System.Drawing.Point(234, 41)
        Me.btnAwaySub8.Name = "btnAwaySub8"
        Me.btnAwaySub8.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub8.TabIndex = 126
        Me.btnAwaySub8.UseVisualStyleBackColor = False
        '
        'btnAwaySub7
        '
        Me.btnAwaySub7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub7.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub7.Location = New System.Drawing.Point(156, 41)
        Me.btnAwaySub7.Name = "btnAwaySub7"
        Me.btnAwaySub7.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub7.TabIndex = 125
        Me.btnAwaySub7.UseVisualStyleBackColor = False
        '
        'btnAwaySub6
        '
        Me.btnAwaySub6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub6.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub6.Location = New System.Drawing.Point(78, 41)
        Me.btnAwaySub6.Name = "btnAwaySub6"
        Me.btnAwaySub6.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub6.TabIndex = 124
        Me.btnAwaySub6.UseVisualStyleBackColor = False
        '
        'btnAwaySub5
        '
        Me.btnAwaySub5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub5.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub5.Location = New System.Drawing.Point(0, 41)
        Me.btnAwaySub5.Name = "btnAwaySub5"
        Me.btnAwaySub5.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub5.TabIndex = 123
        Me.btnAwaySub5.UseVisualStyleBackColor = False
        '
        'btnAwaySub4
        '
        Me.btnAwaySub4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub4.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub4.Location = New System.Drawing.Point(234, 1)
        Me.btnAwaySub4.Name = "btnAwaySub4"
        Me.btnAwaySub4.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub4.TabIndex = 122
        Me.btnAwaySub4.UseVisualStyleBackColor = False
        '
        'btnAwaySub3
        '
        Me.btnAwaySub3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub3.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub3.Location = New System.Drawing.Point(156, 1)
        Me.btnAwaySub3.Name = "btnAwaySub3"
        Me.btnAwaySub3.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub3.TabIndex = 121
        Me.btnAwaySub3.UseVisualStyleBackColor = False
        '
        'btnAwaySub2
        '
        Me.btnAwaySub2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub2.Location = New System.Drawing.Point(78, 1)
        Me.btnAwaySub2.Name = "btnAwaySub2"
        Me.btnAwaySub2.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub2.TabIndex = 120
        Me.btnAwaySub2.UseVisualStyleBackColor = False
        '
        'btnAwaySub1
        '
        Me.btnAwaySub1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwaySub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwaySub1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwaySub1.Location = New System.Drawing.Point(0, 1)
        Me.btnAwaySub1.Name = "btnAwaySub1"
        Me.btnAwaySub1.Size = New System.Drawing.Size(79, 41)
        Me.btnAwaySub1.TabIndex = 119
        Me.btnAwaySub1.UseVisualStyleBackColor = False
        '
        'pnlHomeSub
        '
        Me.pnlHomeSub.AutoScroll = True
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub32)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub31)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub30)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub29)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub28)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub27)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub26)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub25)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub24)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub23)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub22)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub21)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub20)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub19)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub18)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub17)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub16)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub15)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub14)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub13)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub12)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub11)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub10)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub9)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub8)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub7)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub6)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub5)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub4)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub3)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub2)
        Me.pnlHomeSub.Controls.Add(Me.btnHomeSub1)
        Me.pnlHomeSub.Location = New System.Drawing.Point(12, 287)
        Me.pnlHomeSub.Name = "pnlHomeSub"
        Me.pnlHomeSub.Size = New System.Drawing.Size(342, 84)
        Me.pnlHomeSub.TabIndex = 129
        '
        'btnHomeSub32
        '
        Me.btnHomeSub32.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub32.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub32.Location = New System.Drawing.Point(242, 294)
        Me.btnHomeSub32.Name = "btnHomeSub32"
        Me.btnHomeSub32.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub32.TabIndex = 150
        Me.btnHomeSub32.UseVisualStyleBackColor = False
        '
        'btnHomeSub31
        '
        Me.btnHomeSub31.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub31.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub31.Location = New System.Drawing.Point(159, 294)
        Me.btnHomeSub31.Name = "btnHomeSub31"
        Me.btnHomeSub31.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub31.TabIndex = 149
        Me.btnHomeSub31.UseVisualStyleBackColor = False
        '
        'btnHomeSub30
        '
        Me.btnHomeSub30.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub30.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub30.Location = New System.Drawing.Point(79, 294)
        Me.btnHomeSub30.Name = "btnHomeSub30"
        Me.btnHomeSub30.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub30.TabIndex = 148
        Me.btnHomeSub30.UseVisualStyleBackColor = False
        '
        'btnHomeSub29
        '
        Me.btnHomeSub29.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub29.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub29.Location = New System.Drawing.Point(0, 294)
        Me.btnHomeSub29.Name = "btnHomeSub29"
        Me.btnHomeSub29.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub29.TabIndex = 147
        Me.btnHomeSub29.UseVisualStyleBackColor = False
        '
        'btnHomeSub28
        '
        Me.btnHomeSub28.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub28.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub28.Location = New System.Drawing.Point(242, 252)
        Me.btnHomeSub28.Name = "btnHomeSub28"
        Me.btnHomeSub28.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub28.TabIndex = 146
        Me.btnHomeSub28.UseVisualStyleBackColor = False
        '
        'btnHomeSub27
        '
        Me.btnHomeSub27.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub27.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub27.Location = New System.Drawing.Point(159, 252)
        Me.btnHomeSub27.Name = "btnHomeSub27"
        Me.btnHomeSub27.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub27.TabIndex = 145
        Me.btnHomeSub27.UseVisualStyleBackColor = False
        '
        'btnHomeSub26
        '
        Me.btnHomeSub26.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub26.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub26.Location = New System.Drawing.Point(79, 252)
        Me.btnHomeSub26.Name = "btnHomeSub26"
        Me.btnHomeSub26.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub26.TabIndex = 144
        Me.btnHomeSub26.UseVisualStyleBackColor = False
        '
        'btnHomeSub25
        '
        Me.btnHomeSub25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub25.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub25.Location = New System.Drawing.Point(0, 252)
        Me.btnHomeSub25.Name = "btnHomeSub25"
        Me.btnHomeSub25.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub25.TabIndex = 143
        Me.btnHomeSub25.UseVisualStyleBackColor = False
        '
        'btnHomeSub24
        '
        Me.btnHomeSub24.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub24.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub24.Location = New System.Drawing.Point(242, 208)
        Me.btnHomeSub24.Name = "btnHomeSub24"
        Me.btnHomeSub24.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub24.TabIndex = 142
        Me.btnHomeSub24.UseVisualStyleBackColor = False
        '
        'btnHomeSub23
        '
        Me.btnHomeSub23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub23.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub23.Location = New System.Drawing.Point(159, 208)
        Me.btnHomeSub23.Name = "btnHomeSub23"
        Me.btnHomeSub23.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub23.TabIndex = 141
        Me.btnHomeSub23.UseVisualStyleBackColor = False
        '
        'btnHomeSub22
        '
        Me.btnHomeSub22.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub22.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub22.Location = New System.Drawing.Point(79, 208)
        Me.btnHomeSub22.Name = "btnHomeSub22"
        Me.btnHomeSub22.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub22.TabIndex = 140
        Me.btnHomeSub22.UseVisualStyleBackColor = False
        '
        'btnHomeSub21
        '
        Me.btnHomeSub21.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub21.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub21.Location = New System.Drawing.Point(0, 208)
        Me.btnHomeSub21.Name = "btnHomeSub21"
        Me.btnHomeSub21.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub21.TabIndex = 139
        Me.btnHomeSub21.UseVisualStyleBackColor = False
        '
        'btnHomeSub20
        '
        Me.btnHomeSub20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub20.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub20.Location = New System.Drawing.Point(242, 166)
        Me.btnHomeSub20.Name = "btnHomeSub20"
        Me.btnHomeSub20.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub20.TabIndex = 138
        Me.btnHomeSub20.UseVisualStyleBackColor = False
        '
        'btnHomeSub19
        '
        Me.btnHomeSub19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub19.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub19.Location = New System.Drawing.Point(159, 166)
        Me.btnHomeSub19.Name = "btnHomeSub19"
        Me.btnHomeSub19.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub19.TabIndex = 137
        Me.btnHomeSub19.UseVisualStyleBackColor = False
        '
        'btnHomeSub18
        '
        Me.btnHomeSub18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub18.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub18.Location = New System.Drawing.Point(79, 166)
        Me.btnHomeSub18.Name = "btnHomeSub18"
        Me.btnHomeSub18.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub18.TabIndex = 136
        Me.btnHomeSub18.UseVisualStyleBackColor = False
        '
        'btnHomeSub17
        '
        Me.btnHomeSub17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub17.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub17.Location = New System.Drawing.Point(0, 166)
        Me.btnHomeSub17.Name = "btnHomeSub17"
        Me.btnHomeSub17.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub17.TabIndex = 135
        Me.btnHomeSub17.UseVisualStyleBackColor = False
        '
        'btnHomeSub16
        '
        Me.btnHomeSub16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub16.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub16.Location = New System.Drawing.Point(242, 126)
        Me.btnHomeSub16.Name = "btnHomeSub16"
        Me.btnHomeSub16.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub16.TabIndex = 134
        Me.btnHomeSub16.UseVisualStyleBackColor = False
        '
        'btnHomeSub15
        '
        Me.btnHomeSub15.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub15.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub15.Location = New System.Drawing.Point(159, 126)
        Me.btnHomeSub15.Name = "btnHomeSub15"
        Me.btnHomeSub15.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub15.TabIndex = 133
        Me.btnHomeSub15.UseVisualStyleBackColor = False
        '
        'btnHomeSub14
        '
        Me.btnHomeSub14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub14.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub14.Location = New System.Drawing.Point(79, 126)
        Me.btnHomeSub14.Name = "btnHomeSub14"
        Me.btnHomeSub14.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub14.TabIndex = 132
        Me.btnHomeSub14.UseVisualStyleBackColor = False
        '
        'btnHomeSub13
        '
        Me.btnHomeSub13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub13.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub13.Location = New System.Drawing.Point(0, 126)
        Me.btnHomeSub13.Name = "btnHomeSub13"
        Me.btnHomeSub13.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub13.TabIndex = 131
        Me.btnHomeSub13.UseVisualStyleBackColor = False
        '
        'btnHomeSub12
        '
        Me.btnHomeSub12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub12.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub12.Location = New System.Drawing.Point(242, 83)
        Me.btnHomeSub12.Name = "btnHomeSub12"
        Me.btnHomeSub12.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub12.TabIndex = 130
        Me.btnHomeSub12.UseVisualStyleBackColor = False
        '
        'btnHomeSub11
        '
        Me.btnHomeSub11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub11.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub11.Location = New System.Drawing.Point(159, 83)
        Me.btnHomeSub11.Name = "btnHomeSub11"
        Me.btnHomeSub11.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub11.TabIndex = 129
        Me.btnHomeSub11.UseVisualStyleBackColor = False
        '
        'btnHomeSub10
        '
        Me.btnHomeSub10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub10.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub10.Location = New System.Drawing.Point(79, 83)
        Me.btnHomeSub10.Name = "btnHomeSub10"
        Me.btnHomeSub10.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub10.TabIndex = 128
        Me.btnHomeSub10.UseVisualStyleBackColor = False
        '
        'btnHomeSub9
        '
        Me.btnHomeSub9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub9.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub9.Location = New System.Drawing.Point(0, 83)
        Me.btnHomeSub9.Name = "btnHomeSub9"
        Me.btnHomeSub9.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub9.TabIndex = 127
        Me.btnHomeSub9.UseVisualStyleBackColor = False
        '
        'btnHomeSub8
        '
        Me.btnHomeSub8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub8.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub8.Location = New System.Drawing.Point(242, 41)
        Me.btnHomeSub8.Name = "btnHomeSub8"
        Me.btnHomeSub8.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub8.TabIndex = 126
        Me.btnHomeSub8.UseVisualStyleBackColor = False
        '
        'btnHomeSub7
        '
        Me.btnHomeSub7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub7.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub7.Location = New System.Drawing.Point(159, 41)
        Me.btnHomeSub7.Name = "btnHomeSub7"
        Me.btnHomeSub7.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub7.TabIndex = 125
        Me.btnHomeSub7.UseVisualStyleBackColor = False
        '
        'btnHomeSub6
        '
        Me.btnHomeSub6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub6.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub6.Location = New System.Drawing.Point(79, 41)
        Me.btnHomeSub6.Name = "btnHomeSub6"
        Me.btnHomeSub6.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub6.TabIndex = 124
        Me.btnHomeSub6.UseVisualStyleBackColor = False
        '
        'btnHomeSub5
        '
        Me.btnHomeSub5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub5.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub5.Location = New System.Drawing.Point(0, 41)
        Me.btnHomeSub5.Name = "btnHomeSub5"
        Me.btnHomeSub5.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub5.TabIndex = 123
        Me.btnHomeSub5.UseVisualStyleBackColor = False
        '
        'btnHomeSub4
        '
        Me.btnHomeSub4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub4.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub4.Location = New System.Drawing.Point(242, 1)
        Me.btnHomeSub4.Name = "btnHomeSub4"
        Me.btnHomeSub4.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub4.TabIndex = 122
        Me.btnHomeSub4.UseVisualStyleBackColor = False
        '
        'btnHomeSub3
        '
        Me.btnHomeSub3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub3.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub3.Location = New System.Drawing.Point(159, 1)
        Me.btnHomeSub3.Name = "btnHomeSub3"
        Me.btnHomeSub3.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub3.TabIndex = 121
        Me.btnHomeSub3.UseVisualStyleBackColor = False
        '
        'btnHomeSub2
        '
        Me.btnHomeSub2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub2.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub2.Location = New System.Drawing.Point(79, 1)
        Me.btnHomeSub2.Name = "btnHomeSub2"
        Me.btnHomeSub2.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub2.TabIndex = 120
        Me.btnHomeSub2.UseVisualStyleBackColor = False
        '
        'btnHomeSub1
        '
        Me.btnHomeSub1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnHomeSub1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomeSub1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomeSub1.Location = New System.Drawing.Point(0, 1)
        Me.btnHomeSub1.Name = "btnHomeSub1"
        Me.btnHomeSub1.Size = New System.Drawing.Size(79, 41)
        Me.btnHomeSub1.TabIndex = 119
        Me.btnHomeSub1.UseVisualStyleBackColor = False
        '
        'pnlEvents
        '
        Me.pnlEvents.Controls.Add(Me.btnCorner)
        Me.pnlEvents.Controls.Add(Me.btnFoul)
        Me.pnlEvents.Controls.Add(Me.btnThrowIn)
        Me.pnlEvents.Controls.Add(Me.btnFreeKick)
        Me.pnlEvents.Controls.Add(Me.btnGoalKick)
        Me.pnlEvents.Controls.Add(Me.btnShot)
        Me.pnlEvents.Controls.Add(Me.btnOffSide)
        Me.pnlEvents.Controls.Add(Me.btnObstacle)
        Me.pnlEvents.Controls.Add(Me.btnOOB)
        Me.pnlEvents.Location = New System.Drawing.Point(414, 1)
        Me.pnlEvents.Name = "pnlEvents"
        Me.pnlEvents.Size = New System.Drawing.Size(188, 212)
        Me.pnlEvents.TabIndex = 132
        '
        'pnlSubEvents
        '
        Me.pnlSubEvents.Controls.Add(Me.btnBooking)
        Me.pnlSubEvents.Controls.Add(Me.btnSubstitute)
        Me.pnlSubEvents.Location = New System.Drawing.Point(365, 343)
        Me.pnlSubEvents.Name = "pnlSubEvents"
        Me.pnlSubEvents.Size = New System.Drawing.Size(286, 27)
        Me.pnlSubEvents.TabIndex = 133
        '
        'dgvPBP
        '
        Me.dgvPBP.AllowUserToAddRows = False
        Me.dgvPBP.AllowUserToResizeColumns = False
        Me.dgvPBP.AllowUserToResizeRows = False
        Me.dgvPBP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPBP.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPBP.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvPBP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPBP.ContextMenuStrip = Me.cmnuTimeline
        Me.dgvPBP.Location = New System.Drawing.Point(12, 379)
        Me.dgvPBP.MultiSelect = False
        Me.dgvPBP.Name = "dgvPBP"
        Me.dgvPBP.ReadOnly = True
        Me.dgvPBP.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvPBP.RowHeadersVisible = False
        Me.dgvPBP.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvPBP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPBP.Size = New System.Drawing.Size(992, 153)
        Me.dgvPBP.TabIndex = 134
        '
        'cmnuTimeline
        '
        Me.cmnuTimeline.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteActionToolStripItem})
        Me.cmnuTimeline.Name = "cmnuTimeline"
        Me.cmnuTimeline.Size = New System.Drawing.Size(146, 26)
        '
        'DeleteActionToolStripItem
        '
        Me.DeleteActionToolStripItem.Name = "DeleteActionToolStripItem"
        Me.DeleteActionToolStripItem.Size = New System.Drawing.Size(145, 22)
        Me.DeleteActionToolStripItem.Text = "Delete Action"
        '
        'pnlGoalLblLeft
        '
        Me.pnlGoalLblLeft.BackColor = System.Drawing.Color.Transparent
        Me.pnlGoalLblLeft.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lblGoalBg
        Me.pnlGoalLblLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlGoalLblLeft.Controls.Add(Me.lblGoalAway)
        Me.pnlGoalLblLeft.Location = New System.Drawing.Point(462, 285)
        Me.pnlGoalLblLeft.Name = "pnlGoalLblLeft"
        Me.pnlGoalLblLeft.Size = New System.Drawing.Size(92, 30)
        Me.pnlGoalLblLeft.TabIndex = 135
        '
        'lblGoalAway
        '
        Me.lblGoalAway.AutoSize = True
        Me.lblGoalAway.BackColor = System.Drawing.Color.Transparent
        Me.lblGoalAway.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalAway.ForeColor = System.Drawing.Color.White
        Me.lblGoalAway.Location = New System.Drawing.Point(25, 7)
        Me.lblGoalAway.Name = "lblGoalAway"
        Me.lblGoalAway.Size = New System.Drawing.Size(42, 15)
        Me.lblGoalAway.TabIndex = 1
        Me.lblGoalAway.Text = "GOAL"
        '
        'pnlGoalEvents
        '
        Me.pnlGoalEvents.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.pnlGoalEvents.Controls.Add(Me.btnOwn)
        Me.pnlGoalEvents.Controls.Add(Me.btnPenalty)
        Me.pnlGoalEvents.Controls.Add(Me.btnNormal)
        Me.pnlGoalEvents.Location = New System.Drawing.Point(366, 312)
        Me.pnlGoalEvents.Name = "pnlGoalEvents"
        Me.pnlGoalEvents.Size = New System.Drawing.Size(285, 30)
        Me.pnlGoalEvents.TabIndex = 136
        '
        'btnOwn
        '
        Me.btnOwn.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOwn.Location = New System.Drawing.Point(191, 3)
        Me.btnOwn.Name = "btnOwn"
        Me.btnOwn.Size = New System.Drawing.Size(91, 26)
        Me.btnOwn.TabIndex = 3
        Me.btnOwn.Tag = "28"
        Me.btnOwn.Text = "Own"
        Me.btnOwn.UseVisualStyleBackColor = True
        '
        'btnPenalty
        '
        Me.btnPenalty.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPenalty.Location = New System.Drawing.Point(3, 3)
        Me.btnPenalty.Name = "btnPenalty"
        Me.btnPenalty.Size = New System.Drawing.Size(91, 26)
        Me.btnPenalty.TabIndex = 1
        Me.btnPenalty.Tag = "17"
        Me.btnPenalty.Text = "Penalty"
        Me.btnPenalty.UseVisualStyleBackColor = True
        '
        'btnNormal
        '
        Me.btnNormal.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNormal.Location = New System.Drawing.Point(97, 3)
        Me.btnNormal.Name = "btnNormal"
        Me.btnNormal.Size = New System.Drawing.Size(91, 26)
        Me.btnNormal.TabIndex = 2
        Me.btnNormal.Tag = "11"
        Me.btnNormal.Text = "Normal"
        Me.btnNormal.UseVisualStyleBackColor = True
        '
        'picYellowRed
        '
        Me.picYellowRed.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.red_yellow_card
        Me.picYellowRed.Location = New System.Drawing.Point(219, 21)
        Me.picYellowRed.Name = "picYellowRed"
        Me.picYellowRed.Size = New System.Drawing.Size(103, 114)
        Me.picYellowRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picYellowRed.TabIndex = 0
        Me.picYellowRed.TabStop = False
        Me.picYellowRed.Tag = "7,Expulsion,3"
        '
        'picRed
        '
        Me.picRed.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.Red_card
        Me.picRed.Location = New System.Drawing.Point(111, 21)
        Me.picRed.Name = "picRed"
        Me.picRed.Size = New System.Drawing.Size(103, 114)
        Me.picRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picRed.TabIndex = 1
        Me.picRed.TabStop = False
        Me.picRed.Tag = "7,Expulsion,2"
        '
        'picYellow
        '
        Me.picYellow.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.Yellow_card
        Me.picYellow.Location = New System.Drawing.Point(4, 21)
        Me.picYellow.Name = "picYellow"
        Me.picYellow.Size = New System.Drawing.Size(103, 114)
        Me.picYellow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picYellow.TabIndex = 2
        Me.picYellow.TabStop = False
        Me.picYellow.Tag = "2,Caution,1"
        '
        'grpBooking
        '
        Me.grpBooking.BackColor = System.Drawing.Color.Transparent
        Me.grpBooking.Controls.Add(Me.picClose)
        Me.grpBooking.Controls.Add(Me.picYellow)
        Me.grpBooking.Controls.Add(Me.picRed)
        Me.grpBooking.Controls.Add(Me.picYellowRed)
        Me.grpBooking.Location = New System.Drawing.Point(344, 76)
        Me.grpBooking.Name = "grpBooking"
        Me.grpBooking.Size = New System.Drawing.Size(337, 143)
        Me.grpBooking.TabIndex = 3
        Me.grpBooking.TabStop = False
        Me.grpBooking.Text = "Booking Options"
        Me.grpBooking.Visible = False
        '
        'picClose
        '
        Me.picClose.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.Button_Cancel_Red
        Me.picClose.Location = New System.Drawing.Point(311, -2)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(26, 21)
        Me.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picClose.TabIndex = 3
        Me.picClose.TabStop = False
        '
        'grpOutBounds
        '
        Me.grpOutBounds.BackColor = System.Drawing.Color.Transparent
        Me.grpOutBounds.Controls.Add(Me.BtnCornerOut)
        Me.grpOutBounds.Controls.Add(Me.btnGoalKickOut)
        Me.grpOutBounds.Controls.Add(Me.btnThrow)
        Me.grpOutBounds.Controls.Add(Me.picCloseOOB)
        Me.grpOutBounds.Location = New System.Drawing.Point(304, 58)
        Me.grpOutBounds.Name = "grpOutBounds"
        Me.grpOutBounds.Size = New System.Drawing.Size(439, 155)
        Me.grpOutBounds.TabIndex = 269
        Me.grpOutBounds.TabStop = False
        Me.grpOutBounds.Text = "Out Of bounds Option"
        Me.grpOutBounds.Visible = False
        '
        'BtnCornerOut
        '
        Me.BtnCornerOut.BackColor = System.Drawing.Color.White
        Me.BtnCornerOut.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.ButtonOrange
        Me.BtnCornerOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCornerOut.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BtnCornerOut.ForeColor = System.Drawing.Color.White
        Me.BtnCornerOut.Location = New System.Drawing.Point(276, 15)
        Me.BtnCornerOut.Name = "BtnCornerOut"
        Me.BtnCornerOut.Size = New System.Drawing.Size(134, 141)
        Me.BtnCornerOut.TabIndex = 6
        Me.BtnCornerOut.Tag = "2"
        Me.BtnCornerOut.Text = "       Corner         3"
        Me.BtnCornerOut.UseVisualStyleBackColor = False
        '
        'btnGoalKickOut
        '
        Me.btnGoalKickOut.BackColor = System.Drawing.Color.White
        Me.btnGoalKickOut.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.ButtonLavender
        Me.btnGoalKickOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGoalKickOut.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnGoalKickOut.ForeColor = System.Drawing.Color.White
        Me.btnGoalKickOut.Location = New System.Drawing.Point(141, 15)
        Me.btnGoalKickOut.Name = "btnGoalKickOut"
        Me.btnGoalKickOut.Size = New System.Drawing.Size(134, 141)
        Me.btnGoalKickOut.TabIndex = 5
        Me.btnGoalKickOut.Tag = "3"
        Me.btnGoalKickOut.Text = "     Goal Kick       2"
        Me.btnGoalKickOut.UseVisualStyleBackColor = False
        '
        'btnThrow
        '
        Me.btnThrow.BackColor = System.Drawing.Color.White
        Me.btnThrow.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.ButtonBlue
        Me.btnThrow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnThrow.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnThrow.ForeColor = System.Drawing.Color.White
        Me.btnThrow.Location = New System.Drawing.Point(7, 15)
        Me.btnThrow.Name = "btnThrow"
        Me.btnThrow.Size = New System.Drawing.Size(134, 141)
        Me.btnThrow.TabIndex = 4
        Me.btnThrow.Tag = "1"
        Me.btnThrow.Text = "     Throw-In       1"
        Me.btnThrow.UseVisualStyleBackColor = False
        '
        'picCloseOOB
        '
        Me.picCloseOOB.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.Button_Cancel_Red
        Me.picCloseOOB.Location = New System.Drawing.Point(411, 8)
        Me.picCloseOOB.Name = "picCloseOOB"
        Me.picCloseOOB.Size = New System.Drawing.Size(26, 21)
        Me.picCloseOOB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picCloseOOB.TabIndex = 3
        Me.picCloseOOB.TabStop = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(455, 216)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(104, 31)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "Save Action"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnRefreshTime1
        '
        Me.btnRefreshTime1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Refresh_btn
        Me.btnRefreshTime1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefreshTime1.Location = New System.Drawing.Point(427, 219)
        Me.btnRefreshTime1.Name = "btnRefreshTime1"
        Me.btnRefreshTime1.Size = New System.Drawing.Size(24, 22)
        Me.btnRefreshTime1.TabIndex = 303
        Me.btnRefreshTime1.UseVisualStyleBackColor = True
        '
        'btnRefreshTime2
        '
        Me.btnRefreshTime2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Refresh_btn
        Me.btnRefreshTime2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefreshTime2.Location = New System.Drawing.Point(561, 220)
        Me.btnRefreshTime2.Name = "btnRefreshTime2"
        Me.btnRefreshTime2.Size = New System.Drawing.Size(24, 22)
        Me.btnRefreshTime2.TabIndex = 304
        Me.btnRefreshTime2.UseVisualStyleBackColor = True
        '
        'btnHomePass
        '
        Me.btnHomePass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHomePass.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHomePass.Location = New System.Drawing.Point(12, 1)
        Me.btnHomePass.Name = "btnHomePass"
        Me.btnHomePass.Size = New System.Drawing.Size(191, 54)
        Me.btnHomePass.TabIndex = 137
        Me.btnHomePass.Tag = "50"
        Me.btnHomePass.Text = "Pass"
        Me.btnHomePass.UseVisualStyleBackColor = False
        '
        'btnAwayPass
        '
        Me.btnAwayPass.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAwayPass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAwayPass.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwayPass.Location = New System.Drawing.Point(813, 1)
        Me.btnAwayPass.Name = "btnAwayPass"
        Me.btnAwayPass.Size = New System.Drawing.Size(191, 54)
        Me.btnAwayPass.TabIndex = 138
        Me.btnAwayPass.Tag = "50"
        Me.btnAwayPass.Text = "Pass"
        Me.btnAwayPass.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Firebrick
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(557, 343)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(91, 26)
        Me.btnCancel.TabIndex = 305
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(455, 252)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(33, 15)
        Me.lblTime.TabIndex = 306
        Me.lblTime.Text = "Time:"
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(490, 249)
        Me.txtTime.Mask = "000:00:000"
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(69, 22)
        Me.txtTime.TabIndex = 307
        '
        'UdcSoccerPlayerPositionsHome
        '
        Me.UdcSoccerPlayerPositionsHome.AllowDrop = True
        Me.UdcSoccerPlayerPositionsHome.Location = New System.Drawing.Point(12, 62)
        Me.UdcSoccerPlayerPositionsHome.Name = "UdcSoccerPlayerPositionsHome"
        Me.UdcSoccerPlayerPositionsHome.Size = New System.Drawing.Size(396, 219)
        Me.UdcSoccerPlayerPositionsHome.TabIndex = 130
        Me.UdcSoccerPlayerPositionsHome.USPLeftToRight = True
        '
        'UdcSoccerPlayerPositionsAway
        '
        Me.UdcSoccerPlayerPositionsAway.AllowDrop = True
        Me.UdcSoccerPlayerPositionsAway.Location = New System.Drawing.Point(608, 62)
        Me.UdcSoccerPlayerPositionsAway.Name = "UdcSoccerPlayerPositionsAway"
        Me.UdcSoccerPlayerPositionsAway.Size = New System.Drawing.Size(396, 219)
        Me.UdcSoccerPlayerPositionsAway.TabIndex = 131
        Me.UdcSoccerPlayerPositionsAway.USPLeftToRight = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.lblEventProp02)
        Me.GroupBox1.Controls.Add(Me.lstEventProp02)
        Me.GroupBox1.Controls.Add(Me.lblEventProp01)
        Me.GroupBox1.Controls.Add(Me.lstEventProp01)
        Me.GroupBox1.Location = New System.Drawing.Point(269, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(474, 295)
        Me.GroupBox1.TabIndex = 270
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Goal"
        Me.GroupBox1.Visible = False
        '
        'lblEventProp01
        '
        Me.lblEventProp01.AutoSize = True
        Me.lblEventProp01.Location = New System.Drawing.Point(12, 19)
        Me.lblEventProp01.Name = "lblEventProp01"
        Me.lblEventProp01.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp01.TabIndex = 30
        Me.lblEventProp01.Text = "lblEventProp01"
        Me.lblEventProp01.Visible = False
        '
        'lstEventProp01
        '
        Me.lstEventProp01.FormattingEnabled = True
        Me.lstEventProp01.ItemHeight = 15
        Me.lstEventProp01.Location = New System.Drawing.Point(12, 38)
        Me.lstEventProp01.Name = "lstEventProp01"
        Me.lstEventProp01.Size = New System.Drawing.Size(219, 214)
        Me.lstEventProp01.TabIndex = 29
        Me.lstEventProp01.Visible = False
        '
        'lblEventProp02
        '
        Me.lblEventProp02.AutoSize = True
        Me.lblEventProp02.Location = New System.Drawing.Point(248, 20)
        Me.lblEventProp02.Name = "lblEventProp02"
        Me.lblEventProp02.Size = New System.Drawing.Size(80, 15)
        Me.lblEventProp02.TabIndex = 32
        Me.lblEventProp02.Text = "lblEventProp02"
        Me.lblEventProp02.Visible = False
        '
        'lstEventProp02
        '
        Me.lstEventProp02.FormattingEnabled = True
        Me.lstEventProp02.ItemHeight = 15
        Me.lstEventProp02.Location = New System.Drawing.Point(245, 38)
        Me.lstEventProp02.Name = "lstEventProp02"
        Me.lstEventProp02.Size = New System.Drawing.Size(219, 214)
        Me.lstEventProp02.TabIndex = 31
        Me.lstEventProp02.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Firebrick
        Me.Button1.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(246, 258)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(104, 31)
        Me.Button1.TabIndex = 308
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Highlight
        Me.Button2.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(115, 257)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(104, 31)
        Me.Button2.TabIndex = 309
        Me.Button2.Text = "Save"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmActions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 539)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtTime)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.grpOutBounds)
        Me.Controls.Add(Me.pnlEvents)
        Me.Controls.Add(Me.pnlAwaySub)
        Me.Controls.Add(Me.pnlHomeSub)
        Me.Controls.Add(Me.btnRefreshTime2)
        Me.Controls.Add(Me.btnRefreshTime1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnHomePass)
        Me.Controls.Add(Me.UdcSoccerPlayerPositionsHome)
        Me.Controls.Add(Me.btnHomeAction)
        Me.Controls.Add(Me.pnlGoalEvents)
        Me.Controls.Add(Me.pnlGoalLblLeft)
        Me.Controls.Add(Me.dgvPBP)
        Me.Controls.Add(Me.pnlSubEvents)
        Me.Controls.Add(Me.grpBooking)
        Me.Controls.Add(Me.btnAwayPass)
        Me.Controls.Add(Me.UdcSoccerPlayerPositionsAway)
        Me.Controls.Add(Me.btnAwayAction)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "FrmActions"
        Me.pnlAwaySub.ResumeLayout(False)
        Me.pnlHomeSub.ResumeLayout(False)
        Me.pnlEvents.ResumeLayout(False)
        Me.pnlSubEvents.ResumeLayout(False)
        CType(Me.dgvPBP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuTimeline.ResumeLayout(False)
        Me.pnlGoalLblLeft.ResumeLayout(False)
        Me.pnlGoalLblLeft.PerformLayout()
        Me.pnlGoalEvents.ResumeLayout(False)
        CType(Me.picYellowRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picYellow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBooking.ResumeLayout(False)
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpOutBounds.ResumeLayout(False)
        CType(Me.picCloseOOB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnFoul As System.Windows.Forms.Button
    Friend WithEvents btnThrowIn As System.Windows.Forms.Button
    Friend WithEvents btnFreeKick As System.Windows.Forms.Button
    Friend WithEvents btnOffSide As System.Windows.Forms.Button
    Friend WithEvents btnShot As System.Windows.Forms.Button
    Friend WithEvents btnGoalKick As System.Windows.Forms.Button
    Friend WithEvents btnCorner As System.Windows.Forms.Button
    Friend WithEvents btnBooking As System.Windows.Forms.Button
    Friend WithEvents btnSubstitute As System.Windows.Forms.Button
    Friend WithEvents btnOOB As System.Windows.Forms.Button
    Friend WithEvents btnObstacle As System.Windows.Forms.Button
    Friend WithEvents btnHomeAction As System.Windows.Forms.Button
    Friend WithEvents btnAwayAction As System.Windows.Forms.Button
    Friend WithEvents pnlAwaySub As System.Windows.Forms.Panel
    Friend WithEvents btnAwaySub12 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub11 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub10 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub9 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub8 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub7 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub6 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub5 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub4 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub3 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub2 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub1 As System.Windows.Forms.Button
    Friend WithEvents pnlHomeSub As System.Windows.Forms.Panel
    Friend WithEvents btnHomeSub12 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub11 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub10 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub9 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub8 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub7 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub6 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub5 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub4 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub3 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub2 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub1 As System.Windows.Forms.Button
    Friend WithEvents UdcSoccerPlayerPositionsHome As STATS.SoccerDataCollection.udcSoccerPlayerPositions
    Friend WithEvents UdcSoccerPlayerPositionsAway As STATS.SoccerDataCollection.udcSoccerPlayerPositions
    Friend WithEvents pnlEvents As System.Windows.Forms.Panel
    Friend WithEvents pnlSubEvents As System.Windows.Forms.Panel
    Friend WithEvents dgvPBP As System.Windows.Forms.DataGridView
    Friend WithEvents pnlGoalLblLeft As System.Windows.Forms.Panel
    Friend WithEvents lblGoalAway As System.Windows.Forms.Label
    Friend WithEvents pnlGoalEvents As System.Windows.Forms.Panel
    Friend WithEvents btnOwn As System.Windows.Forms.Button
    Friend WithEvents btnPenalty As System.Windows.Forms.Button
    Friend WithEvents btnNormal As System.Windows.Forms.Button
    Friend WithEvents picYellowRed As System.Windows.Forms.PictureBox
    Friend WithEvents picRed As System.Windows.Forms.PictureBox
    Friend WithEvents picYellow As System.Windows.Forms.PictureBox
    Friend WithEvents grpBooking As System.Windows.Forms.GroupBox
    Friend WithEvents picClose As System.Windows.Forms.PictureBox
    Friend WithEvents cmnuTimeline As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteActionToolStripItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnRefreshTime1 As System.Windows.Forms.Button
    Friend WithEvents btnRefreshTime2 As System.Windows.Forms.Button
    Friend WithEvents btnHomePass As System.Windows.Forms.Button
    Friend WithEvents btnAwayPass As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub32 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub31 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub30 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub29 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub28 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub27 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub26 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub25 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub24 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub23 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub22 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub21 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub20 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub19 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub18 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub17 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub16 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub15 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub14 As System.Windows.Forms.Button
    Friend WithEvents btnAwaySub13 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub32 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub31 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub30 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub29 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub28 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub27 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub26 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub25 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub24 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub23 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub22 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub21 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub20 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub19 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub18 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub17 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub16 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub15 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub14 As System.Windows.Forms.Button
    Friend WithEvents btnHomeSub13 As System.Windows.Forms.Button
    Friend WithEvents grpOutBounds As System.Windows.Forms.GroupBox
    Friend WithEvents BtnCornerOut As System.Windows.Forms.Button
    Friend WithEvents btnGoalKickOut As System.Windows.Forms.Button
    Friend WithEvents btnThrow As System.Windows.Forms.Button
    Friend WithEvents picCloseOOB As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents txtTime As System.Windows.Forms.MaskedTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblEventProp01 As System.Windows.Forms.Label
    Friend WithEvents lstEventProp01 As System.Windows.Forms.ListBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblEventProp02 As System.Windows.Forms.Label
    Friend WithEvents lstEventProp02 As System.Windows.Forms.ListBox
End Class
