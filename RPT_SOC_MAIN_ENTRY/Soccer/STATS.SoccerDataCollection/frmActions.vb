﻿#Region " Imports "
Imports STATS.SoccerBL
Imports STATS.Utility
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmActions
' Author        : Wilson,Shirley
' Created Date  :
' Description   : This is the entry form for Tier 6 (Primary Reporter).
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class FrmActions

#Region " Member Variables "
    Private _objActionDetails As ClsActionDetails = ClsActionDetails.GetInstance()
    Private _objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private _objActions As ClsActions = ClsActions.GetInstance()
    Private _objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private _objGeneral As clsGeneral = clsGeneral.GetInstance()
    Private _objClsPbpTree As ClsPbpTree = ClsPbpTree.GetInstance()
    Private _objUtility As New clsUtility
    Private _objUtilAudit As New clsAuditLog
    Private _objClsPlayerData As ClsPlayerData = ClsPlayerData.GetInstance()
    Private _playerTable As New List(Of ClsPlayerData.PlayerData)
    Private ReadOnly _eventHotkeys As New Dictionary(Of Keys, Button)
    Private ReadOnly _messageDialog As New frmMessageDialog
    Private ReadOnly _eventsToShow() As Int16 = {11, 17, 28, 30, 22, 2, 7, 18}
    Private ReadOnly _bookingEvents() As Integer = {2, 7}
    Private ReadOnly _goalEvents() As Integer = {11, 28, 17}
    Private ReadOnly _shootoutEvents() As Integer = {30, 31, 41}
    Private ReadOnly _eventNonEditable() As Integer = {23, 24, 34, 62, 21, 13, 10, 65, 30, 31, 41, 58, 59} '23-HomeStartingLineups 24 -AwatStartingLineups ,34-GAMESTART,62-TIME,21-STARTPERIOD,13-ENDPERIOD,10-ENDGAME,65-DELAY_OVER, (30,31,41) shootoutEvents
    Private ReadOnly _halfTimeEvents() As Integer = {2, 7, 22}
    Private ReadOnly _paredEvents() As Integer = {71, 72, 73, 74} '50 -50
    Private ReadOnly _oobKeyCodes() As Keys = {Keys.D1, Keys.D2, Keys.D3, 1, 2, 3}
    Private _homeSubs As Integer = 0
    Private _awaySubs As Integer = 0
    Private _rowIndex As Integer
    Private _deleteKeyPressCnt As Integer
    Private eventIdForSubitems As New Dictionary(Of Integer, Integer)
    Private _defaultsGoalkeeperEvents As New List(Of Int16)
    Private _betweenHalf As Boolean = False
    Private _homeTeamOnLeft As Boolean = False
    Private _milliSec As String
    Private _pbpRowSelectedSqNo? As Decimal
#End Region

#Region " Event handlers "
    Private Sub FrmActions_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmActionsAssist", Nothing, 1, 1)
            SetDefaultValues()
            ControlsSetting()
            'Multilingual for The form controls
            If _objGameDetails.languageid <> 1 Then
                Dim lsupport As New languagesupport
                lsupport.FormLanguageTranslate(Me)
                DeleteActionToolStripItem.Text = lsupport.MultilingualDictionary(DeleteActionToolStripItem.Text)
            End If

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Hot Keys
    Private Sub FrmActions_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Try
            If (_eventHotkeys.ContainsKey(e.KeyCode)) Then
                If _oobKeyCodes.Contains(e.KeyCode) And Not grpOutBounds.Visible Then
                    Exit Sub
                End If
                e.SuppressKeyPress = True
                _eventHotkeys.Item(e.KeyCode).PerformClick()
                _deleteKeyPressCnt = 0
            End If

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Home / Away Control/Pass Button click
    Private Sub Team_Click(sender As Object, e As EventArgs) Handles btnHomeAction.Click, btnAwayAction.Click, btnHomePass.Click, btnAwayPass.Click
        Try
            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") = True And _objGameDetails.CurrentPeriod = 0 And _objGameDetails.ActivateGame = False Then Exit Sub
            Dim buttonEvent As Button = CType(DirectCast(sender, Windows.Forms.Control), Button)
            If _objGameDetails.ActivateGame = True Then
                If (buttonEvent.Tag = 63) Then 'pass event
                    _messageDialog.Show(_objGameDetails.languageid, "Please select PASS action to Start the game", "Start Half", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
                If Not frmMain.ActivateGames() Then
                    Exit Sub
                Else
                    _objActionDetails.TeamId = If(buttonEvent.Name.Contains("btnHome"), _objGameDetails.HomeTeamID, _objGameDetails.AwayTeamID)
                    _objActionDetails.EventId = Convert.ToInt16(buttonEvent.Tag)
                    SaveAction()
                    _objGameDetails.ActivateGame = False
                End If
            ElseIf _objActionDetails.IsEditMode = False Then
                If SavePreviousAction() And pnlEvents.Enabled Then 'pnlEvents.Enabled To handel OOB Save
                    AssignBtnTagToProperty(buttonEvent)
                    DisplayActionInGrid()
                    _objActionDetails.PlayerId = Nothing
                Else
                    If (Not _objActionDetails.TeamId.HasValue) Then
                        _objActionDetails.TeamId = If(buttonEvent.Name.Contains("btnHome"), _objGameDetails.HomeTeamID, _objGameDetails.AwayTeamID)
                        clsUtility.HighlightGoldButton(buttonEvent)
                        DefaultsGoalkeeper()
                        DisplayActionInGrid()
                    End If
                End If
            Else
                '@Edit mode
                If _objActionDetails.TeamId <> If(buttonEvent.Name.Contains("btnHome"), _objGameDetails.HomeTeamID, _objGameDetails.AwayTeamID) Then
                    _messageDialog.Show(_objGameDetails.languageid, "You are not allowed to change the Team in edit mode!", "Goal", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Else
                    UnhighlightActionButtons()
                    grpOutBounds.Visible = False
                    _objActionDetails.OutOfBoundsResId = Nothing
                    grpBooking.Visible = False
                    _objActionDetails.BookingTypeId = Nothing
                    AssignBtnTagToProperty(buttonEvent)
                End If
            End If
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, buttonEvent.Text.Trim().Replace(vbNewLine, " "), 1, 0)
            pnlSubEvents.Enabled = True
            pnlEvents.Enabled = True
            If Not frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                btnHomePass.Enabled = True
                btnAwayPass.Enabled = True
            End If

            pnlGoalEvents.Enabled = True
            dgvPBP.Enabled = True
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Actions(oob/obstacle)  click
    Private Sub Action_Click(sender As Object, e As EventArgs) Handles btnObstacle.Click, btnOOB.Click
        Try
            If _objGameDetails.ActivateGame = True Then
                Exit Sub
            End If
            Dim btnClick As Button = CType(DirectCast(sender, Windows.Forms.Control), Button)
            If _objActionDetails.IsEditMode = False Then
                If SavePreviousAction() Then
                    'ássign selected event to property
                    AssignBtnTagToProperty(btnClick)
                    If btnClick.Tag = 67 Then
                        DisplayOobTypes(Nothing)
                        btnThrow.Focus()
                    End If
                    'Display action in grid before saving to database.
                    DisplayActionInGrid()
                End If
            Else
                UnHighlightActions()
                ResetProperties()
                AssignBtnTagToProperty(CType(DirectCast(sender, Windows.Forms.Control), Button))
            End If
            If btnThrow.Focus() = False Then
                btnSave.Focus()
            End If

            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'own goal.
    Private Sub btnOwn_Click(sender As Object, e As EventArgs) Handles btnOwn.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            If PasstoOtherActions() And IsTeamSelected() Then
                If (_objActionDetails.IsEditMode And Not _goalEvents.Contains(_objActionDetails.EventId)) Then
                    _messageDialog.Show(_objGameDetails.languageid, "You are not allowed enter Goal Event in edit mode!", "Goal", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    btnSave.Focus()
                    Exit Sub
                End If
                AssignBtnTagToProperty(CType(DirectCast(sender, Windows.Forms.Control), Button))
                If (_objActionDetails.PlayerId.HasValue) Then 'own goal
                    _messageDialog.Show(_objGameDetails.languageid, "The player cannot be from the same team for Own Goal event. Please check!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    UnhighlightLineupPlayers()
                    _objActionDetails.PlayerId = Nothing
                    DisplayActionInGrid()
                    btnSave.Focus()
                    Exit Sub
                End If
                SaveGoalEvent()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'save goal events immediately
    Private Sub Goal_Click(sender As Object, e As EventArgs) Handles btnNormal.Click, btnPenalty.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            If PasstoOtherActions() And IsTeamSelected() Then
                If (_objActionDetails.IsEditMode And Not _goalEvents.Contains(_objActionDetails.EventId)) Then
                    _messageDialog.Show(_objGameDetails.languageid, "You are not allowed enter Goal Event in edit mode!", "Goal", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    btnSave.Focus()
                    Exit Sub
                End If
                AssignBtnTagToProperty(CType(DirectCast(sender, Windows.Forms.Control), Button))
                SaveGoalEvent()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Sub event
    Private Sub Sub_Click(sender As Object, e As EventArgs) Handles btnSubstitute.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            If PasstoOtherActions() And IsTeamSelected() Then
                AssignBtnTagToProperty(CType(DirectCast(sender, Windows.Forms.Control), Button))
                'Sub events check
                If (_objActionDetails.TeamId = _objGameDetails.HomeTeamID And _homeSubs >= 3) Or _
                   (_objActionDetails.TeamId = _objGameDetails.AwayTeamID And _awaySubs >= 3) Then
                    If _messageDialog.Show(_objGameDetails.languageid, "THREE Subs already used - Do you want to proceed?", "Substitutions", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                        UnhighlightEvent()
                        _objActionDetails.EventId = 63
                        _objActionDetails.EventDescription = "Control"
                    End If
                End If
                DisplayActionInGrid()
                btnSave.Focus()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'booking click
    Private Sub Booking_Click(sender As Object, e As EventArgs) Handles btnBooking.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            If PasstoOtherActions() And IsTeamSelected() Then
                'TOSOCRS-438 Warning when 'Booking' is entered directly after 'Free Kick-Penalty Kick'
                If _objActionDetails.IsEditMode = False Then
                    Dim dgPbpDataSource As DataTable
                    dgPbpDataSource = dgvPBP.DataSource
                    Dim prevEvent = (From events In dgPbpDataSource Where events.Field(Of Decimal?)("SEQUENCE_NUMBER") = (From pbp In dgPbpDataSource
                                        Where Not IsDBNull(pbp.Field(Of Decimal?)("SEQUENCE_NUMBER")) Select pbp.Field(Of Decimal?)("SEQUENCE_NUMBER")).Max()
                                        Select events.Field(Of Int16?)("EVENT_CODE_ID"))
                    If prevEvent(0) = 9 Then
                        _messageDialog.Show(_objGameDetails.languageid, "Bookings cannot directly follow Free Kicks. Please enter the booking before the Free Kick is taken.", "Booking", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Exit Sub
                    End If
                End If

                AssignBtnTagToProperty(CType(DirectCast(sender, Windows.Forms.Control), Button))
                grpBooking.Visible = True
                GroupBoxPictureBoxBackColor(grpBooking, Color.White)
                DisplayActionInGrid()
                grpBooking.BringToFront()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub GroupBoxButtonBackColor(grpBox As GroupBox, buttonBackColor As Color)
        For Each ctrl As Button In grpBox.Controls.OfType(Of Button)()
            ctrl.BackColor = buttonBackColor
        Next
    End Sub
    Private Sub GroupBoxPictureBoxBackColor(grpBox As GroupBox, buttonBackColor As Color)
        For Each ctrl As PictureBox In grpBox.Controls.OfType(Of PictureBox)()
            ctrl.BackColor = buttonBackColor
        Next
    End Sub
    'Other Event Buttons click
    Private Sub Event_Click(sender As Object, e As EventArgs) Handles btnFoul.Click, btnShot.Click, btnThrowIn.Click, btnOffSide.Click, btnFreeKick.Click, btnCorner.Click, btnGoalKick.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            If _objGameDetails.ActivateGame = True Then
                Exit Sub
            End If
            If PasstoOtherActions() And IsTeamSelected() Then
                If (_objActionDetails.IsEditMode And _goalEvents.Contains(_objActionDetails.EventId)) Then
                    _messageDialog.Show(_objGameDetails.languageid, "You are not allowed to change a Goal Event to some other event in edit mode!", "Goal", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Exit Sub
                End If
                AssignBtnTagToProperty(CType(DirectCast(sender, Windows.Forms.Control), Button))
                'Goalkeeper Defaults
                DefaultsGoalkeeper()

                'Clear the opponent player if Foul -> offside
                If _objActionDetails.IsEditMode And _objActionDetails.EventId <> 8 Then
                    _objActionDetails.defPlayerId = Nothing
                    If _objActionDetails.TeamId = _objGameDetails.HomeTeamID Then
                        UdcSoccerPlayerPositionsAway.USPUnhighlightButtons()
                    Else
                        UdcSoccerPlayerPositionsHome.USPUnhighlightButtons()
                    End If
                End If
                'Display action in grid before saving to database.
                DisplayActionInGrid()
            End If
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Display booking options
    Private Sub BookingOptions_Click(sender As Object, e As EventArgs) Handles picYellow.Click, picRed.Click, picYellowRed.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.PictureBoxClicked, DirectCast(sender, System.Windows.Forms.PictureBox).Tag.ToString.Split(",")(1), 1, 0)
            GroupBoxPictureBoxBackColor(grpBooking, Color.White)
            Dim picBox As PictureBox = (CType(DirectCast(sender, Windows.Forms.Control), PictureBox))
            picBox.BackColor = Color.Gray
            Dim strTag() As String
            strTag = picBox.Tag.Split(",")
            _objActionDetails.EventId = CInt(strTag(0))
            _objActionDetails.EventDescription = strTag(1)
            _objActionDetails.BookingTypeId = strTag(2)
            DisplayActionInGrid() 'Display action in grid before saving to database.
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub OutofBoundsOptions_Click(sender As Object, e As EventArgs) Handles btnThrow.Click, btnGoalKickOut.Click, BtnCornerOut.Click
        Try
            If grpOutBounds.Enabled Then 'To handel Hot key
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Tag, 1, 0)
                GroupBoxButtonBackColor(grpOutBounds, Color.White)
                Dim buttonControl As Button = (CType(DirectCast(sender, Windows.Forms.Control), Button))
                buttonControl.BackColor = Color.Gold
                _objActionDetails.OutOfBoundsResDesc = buttonControl.Text.Trim()
                _objActionDetails.OutOfBoundsResId = CInt(buttonControl.Tag)
                If Not _objActionDetails.IsEditMode Then
                    pnlSubEvents.Enabled = False
                    pnlEvents.Enabled = False
                    btnHomePass.Enabled = False
                    btnAwayPass.Enabled = False
                    pnlGoalEvents.Enabled = False
                    dgvPBP.Enabled = False
                    DisplayActionInGrid() 'Display action in grid before saving to database.
                End If
                btnSave.Focus()
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs) Handles picClose.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.PictureBoxClicked, DirectCast(sender, System.Windows.Forms.PictureBox).Name, 1, 0)
            grpBooking.Visible = False
            UnhighlightEvent()
            _objActionDetails.EventId = 63
            _objActionDetails.EventDescription = Nothing
            _objActionDetails.BookingTypeId = Nothing
            DisplayActionInGrid()
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        Finally
        End Try
    End Sub
    Private Sub picCloseOOB_Click(sender As Object, e As EventArgs) Handles picCloseOOB.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.PictureBoxClicked, DirectCast(sender, System.Windows.Forms.PictureBox).Name, 1, 0)
            _objActionDetails.OutOfBoundsResDesc = "Throw-In"
            _objActionDetails.OutOfBoundsResId = 1
            grpOutBounds.Visible = False
            If Not _objActionDetails.IsEditMode Then
                pnlSubEvents.Enabled = False
                pnlEvents.Enabled = False
                btnHomePass.Enabled = False
                btnAwayPass.Enabled = False
                pnlGoalEvents.Enabled = False
                dgvPBP.Enabled = False
                DisplayActionInGrid() 'Display action in grid before saving to database.
            End If
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        Finally
        End Try
    End Sub
    'Player Button click
    Private Sub Player_Click(sender As Object, e As USPEventArgs) Handles UdcSoccerPlayerPositionsHome.USPButtonClick, UdcSoccerPlayerPositionsAway.USPButtonClick
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Replace(vbNewLine, " "), 1, 0)
            Dim buttonPlayer As Button = CType(DirectCast(sender, Control), Button)
            If ValidatePlayer(DirectCast(buttonPlayer.Parent, udcSoccerPlayerPositions).USPTeamId) Then 'validations.
                If (_objActionDetails.EventId = 8) And (DirectCast(buttonPlayer.Parent, udcSoccerPlayerPositions).USPTeamId <> _objActionDetails.TeamId) Then  ' foul
                    _objActionDetails.defPlayerId = e.USPplayerID
                ElseIf _objActionDetails.EventId = 28 Then 'own goal
                    _objActionDetails.PlayerId = e.USPplayerID
                    If _objActionDetails.IsEditMode = False Then
                        SaveGoalEvent()
                        Exit Sub
                    End If
                Else
                    _objActionDetails.PlayerId = e.USPplayerID
                End If
            Else
                UnhighlightLineupPlayers()
                _objActionDetails.PlayerId = Nothing
                DisplayActionInGrid()
                Exit Sub
            End If

            Dim bookingStatus As Boolean = True
            If (_bookingEvents.Contains(If(_objActionDetails.EventId, 0))) Then
                If Not IsBookingValid() Then
                    bookingStatus = False
                End If
                UnhighlightSubPlayer()
            ElseIf grpBooking.Visible = True Then
                UnhighlightSubPlayer()
            End If
            If bookingStatus Then
                DisplayActionInGrid()
            End If
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Home/Away Sub Button click
    Private Sub SubPlayer_Click(sender As Object, e As EventArgs) Handles btnHomeSub1.Click, btnHomeSub2.Click, btnHomeSub3.Click, btnHomeSub4.Click, btnHomeSub5.Click, _
                                                                          btnHomeSub6.Click, btnHomeSub7.Click, btnHomeSub8.Click, btnHomeSub9.Click, btnHomeSub10.Click, _
                                                                          btnHomeSub11.Click, btnHomeSub12.Click, _
                                                                          btnHomeSub13.Click, btnHomeSub14.Click, btnHomeSub15.Click, btnHomeSub16.Click,
                                                                          btnHomeSub17.Click, btnHomeSub18.Click, btnHomeSub19.Click, btnHomeSub20.Click, btnHomeSub21.Click, btnHomeSub22.Click,
                                                                          btnHomeSub23.Click, btnHomeSub24.Click, btnHomeSub25.Click, btnHomeSub26.Click, btnHomeSub27.Click, btnHomeSub28.Click,
                                                                          btnHomeSub29.Click, btnHomeSub30.Click, btnHomeSub31.Click, btnHomeSub32.Click,
                                                                          btnAwaySub1.Click, btnAwaySub2.Click, btnAwaySub3.Click, btnAwaySub4.Click, btnAwaySub5.Click, _
                                                                          btnAwaySub6.Click, btnAwaySub7.Click, btnAwaySub8.Click, btnAwaySub9.Click, btnAwaySub10.Click, _
                                                                          btnAwaySub11.Click, btnAwaySub12.Click, btnAwaySub13.Click, btnAwaySub14.Click, btnAwaySub15.Click, btnAwaySub16.Click,
                                                                          btnAwaySub17.Click, btnAwaySub18.Click, btnAwaySub19.Click, btnAwaySub20.Click, btnAwaySub21.Click, btnAwaySub22.Click,
                                                                          btnAwaySub23.Click, btnAwaySub24.Click, btnAwaySub25.Click, btnAwaySub26.Click, btnAwaySub27.Click, btnAwaySub28.Click,
                                                                          btnAwaySub29.Click, btnAwaySub30.Click, btnAwaySub31.Click, btnAwaySub32.Click

        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            Dim buttonSubPlayer As Button = CType(DirectCast(sender, Control), Button)
            If (_objActionDetails.EventId = 22 Or _objActionDetails.EventId = 0 Or _bookingEvents.Contains(If(_objActionDetails.EventId, 0))) Then
                UnhighlightSubPlayer()
                If (_objActionDetails.EventId <> 22) Then UnhighlightLineupPlayers()
                If (If(_objActionDetails.TeamId = _objGameDetails.HomeTeamID, btnHomeAction, btnAwayAction).Name.StartsWith(buttonSubPlayer.Name.Substring(0, 7))) Then
                    If _objActionDetails.EventId = 22 Then
                        _objActionDetails.subPlayerId = CType(buttonSubPlayer.Tag, Integer?)
                    Else
                        _objActionDetails.PlayerId = CType(buttonSubPlayer.Tag, Integer?)

                    End If

                    clsUtility.HighlightGoldButton(buttonSubPlayer)
                    DisplayActionInGrid()
                Else
                    _messageDialog.Show(_objGameDetails.languageid, "The Sub player does not belong to selected Action team.", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    UnhighlightSubPlayer()
                    If (_objActionDetails.EventId <> 22) Then _objActionDetails.PlayerId = Nothing
                End If
            Else
                _messageDialog.Show(_objGameDetails.languageid, "Choosing a Sub player is not allowed for this Action!", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub FrmActions_MouseClick(sender As Object, e As MouseEventArgs) Handles MyBase.MouseClick, UdcSoccerPlayerPositionsAway.USPMouseClick, UdcSoccerPlayerPositionsHome.USPMouseClick
        Try
            _deleteKeyPressCnt = 0
            If e.Button = Windows.Forms.MouseButtons.Right Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MouseClicked, "Save Action Through Mouse Right Click", 1, 0)
                If (_objActionDetails.TeamId.HasValue) Or (_objActionDetails.EventId.HasValue) Then
                    SavePreviousAction()
                Else
                    _messageDialog.Show(_objGameDetails.languageid, "No records to save. Please check!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub RefreshTime_Click(sender As Object, e As EventArgs) Handles btnRefreshTime1.Click, btnRefreshTime2.Click
        Try
            _objActionDetails.TimeofEvent = Nothing
            _objActionDetails.TimeofEvent = frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec
            txtTime.Text = _objActionDetails.TimeofEvent
            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(8, CChar(" "))
            DisplayActionInGrid()
            btnSave.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub HighlightDgvEditedRow(ByVal rowIndex As Integer)
        Try
            dgvPBP.Rows(rowIndex).Selected = True
            dgvPBP.Rows(rowIndex).DefaultCellStyle.SelectionBackColor = Color.Yellow
            dgvPBP.Rows(rowIndex).DefaultCellStyle.SelectionForeColor = Color.Black
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'IN edit mode if any other action is selected do not highlight the action other than the action seleted for editing(in yellow)
    Private Sub dgvPBP_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPBP.CellClick
        Try
            If _objActionDetails.IsEditMode Then
                dgvPBP.ClearSelection()
                HighlightDgvEditedRow(_rowIndex)
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    'TOPZ-430 Primary Reporter-Editing Saved Actions
    Private Sub dgvPBP_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPBP.CellDoubleClick
        Try
            If (e.RowIndex = -1) Then 'To avoid Column header click
                Exit Sub
            End If

            Dim playerId? As Integer
            Dim subplayerId? As Integer
            _betweenHalf = False

            'validations
            If IsDBNull(dgvPBP.Rows(dgvPBP.Rows.Count - 1).Cells("SEQUENCE_NUMBER").Value) Then
                'save the inmemory record before proceding to edit
                If Not SavePreviousAction() Then
                    dgvPBP.Rows(dgvPBP.Rows.Count - 1).Selected = True
                    Exit Sub
                End If
            End If

            If _eventNonEditable.Contains(CInt(dgvPBP.Rows(e.RowIndex).Cells("EVENT_CODE_ID").Value)) Then
                _messageDialog.Show(_objGameDetails.languageid, "Edit Functionality Not Available", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If

            If _shootoutEvents.Contains(CInt(dgvPBP.Rows(e.RowIndex).Cells("EVENT_CODE_ID").Value)) Then
                _messageDialog.Show(_objGameDetails.languageid, "Shootout Events Canot be edited here!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                dgvPBP.ClearSelection()
                _objGameDetails.IsEditMode = False
                Exit Sub
            End If

            If _objActionDetails.IsEditMode Then
                _messageDialog.Show(_objGameDetails.languageid, "You are not allowed to edit another action without completing the previous edit!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                HighlightDgvEditedRow(_rowIndex)
                Exit Sub
            End If

            'booking/sub event is edited between half over and start period, do not enable other actions
            If _halfTimeEvents.Contains(CInt(dgvPBP.Rows(e.RowIndex).Cells("EVENT_CODE_ID").Value)) Then
                Dim dgPbpDataSource As DataTable
                dgPbpDataSource = dgvPBP.DataSource
                Dim prevEvent = (From events In dgPbpDataSource Where events.Field(Of Decimal?)("SEQUENCE_NUMBER") = (From pbp In dgPbpDataSource Where
                                 pbp.Field(Of Decimal?)("SEQUENCE_NUMBER") < dgvPBP.Rows(e.RowIndex).Cells("SEQUENCE_NUMBER").Value And _
                                 Not _halfTimeEvents.Contains(pbp.Field(Of Int16)("EVENT_CODE_ID"))
                                 Select pbp.Field(Of Decimal?)("SEQUENCE_NUMBER")).Max()
                                 Select events.Field(Of Int16?)("EVENT_CODE_ID"))
                Dim nextEvent = (From events In dgPbpDataSource Where events.Field(Of Decimal?)("SEQUENCE_NUMBER") = (From pbp In dgPbpDataSource Where
                                 pbp.Field(Of Decimal?)("SEQUENCE_NUMBER") > dgvPBP.Rows(e.RowIndex).Cells("SEQUENCE_NUMBER").Value And _
                                 Not _halfTimeEvents.Contains(pbp.Field(Of Int16)("EVENT_CODE_ID"))
                                 Select pbp.Field(Of Decimal?)("SEQUENCE_NUMBER")).Min()
                                 Select events.Field(Of Int16?)("EVENT_CODE_ID"))

                If prevEvent(0) = 13 And (nextEvent(0) = 21 Or nextEvent(0) Is Nothing) Then
                    _betweenHalf = True
                End If
            End If

            'change the backcolor to Yellow
            HighlightDgvEditedRow(e.RowIndex)
            _rowIndex = e.RowIndex

            'properties.
            _objActionDetails.IsEditMode = True

            _objGameDetails.ActivateGame = False
            If _betweenHalf Then
                EnableDisableControls(True, False)
            Else
                EnableDisableControls(True)
            End If
            lblTime.Visible = False
            txtTime.Visible = False
            _pbpRowSelectedSqNo = dgvPBP.Rows(e.RowIndex).Cells("SEQUENCE_NUMBER").Value
            _objActionDetails.EventId = CInt(dgvPBP.Rows(e.RowIndex).Cells("EVENT_CODE_ID").Value)
            _objActionDetails.TeamId = IIf(dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value.ToString() <> "", dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value, Nothing)
            playerId = IIf(dgvPBP.Rows(e.RowIndex).Cells("OFFENSIVE_PLAYER_ID").Value.ToString() <> "", dgvPBP.Rows(e.RowIndex).Cells("OFFENSIVE_PLAYER_ID").Value, Nothing)
            subplayerId = IIf(dgvPBP.Rows(e.RowIndex).Cells("PLAYER_OUT_ID").Value.ToString() <> "", dgvPBP.Rows(e.RowIndex).Cells("PLAYER_OUT_ID").Value, Nothing)
            _objActionDetails.subPlayerId = If(_objActionDetails.EventId = 22, playerId, subplayerId)
            _objActionDetails.PlayerId = If(_objActionDetails.EventId = 22, subplayerId, playerId)
            _objActionDetails.defPlayerId = IIf(dgvPBP.Rows(e.RowIndex).Cells("DEFENSIVE_PLAYER_ID").Value.ToString() <> "", dgvPBP.Rows(e.RowIndex).Cells("DEFENSIVE_PLAYER_ID").Value, Nothing)
            If _objActionDetails.EventId = 66 Or _objActionDetails.EventId = 48 Or _objActionDetails.EventId = 29 Or _objActionDetails.EventId = 4 Then
                _objGameDetails.IsEditMode = True
                If _objActionDetails.EventId = 66 Then
                    _objGameDetails.InjuryEnteredTime = dgvPBP.Rows(e.RowIndex).Cells("TIME_ELAPSED").Value.ToString()
                    _objGameDetails.CommentsTime = CInt(dgvPBP.Rows(e.RowIndex).Cells("INJURY_TIME").Value.ToString())
                    Dim objInjtime As New frmInjuryTime
                    If objInjtime.ShowDialog = Windows.Forms.DialogResult.OK Then
                        InsertPlayByPlayData()
                    End If
                ElseIf _objActionDetails.EventId = 48 Then
                    _objGameDetails.ManagerExplTime = dgvPBP.Rows(e.RowIndex).Cells("TIME_ELAPSED").Value.ToString()
                    _objGameDetails.OffensiveTeamID = _objActionDetails.TeamId
                    Dim objManagerExpulsion As New frmManagerExpulsion
                    If objManagerExpulsion.ShowDialog = Windows.Forms.DialogResult.OK Then
                        InsertPlayByPlayData()
                    End If
                ElseIf _objActionDetails.EventId = 29 Then
                    _objGameDetails.ActiveGK = dgvPBP.Rows(e.RowIndex).Cells("OFFENSIVE_PLAYER_ID").Value.ToString()
                    _objGameDetails.OffensiveTeamID = dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value.ToString()
                    _objGameDetails.CommentsTime = dgvPBP.Rows(e.RowIndex).Cells("TIME_ELAPSED").Value.ToString()
                    Dim objActiveGoalkeeper As New frmActiveGoalkeeper
                    If objActiveGoalkeeper.ShowDialog = Windows.Forms.DialogResult.OK Then
                        InsertPlayByPlayData()
                    End If
                ElseIf _objActionDetails.EventId = 4 Then
                    _objGameDetails.PredefinedCommentID = Convert.ToInt16(If(IsDBNull(dgvPBP.Rows(e.RowIndex).Cells("COMMENT_ID").Value), -1, dgvPBP.Rows(e.RowIndex).Cells("COMMENT_ID").Value))
                    _objGameDetails.CommentLanguageID = Convert.ToInt16(If(IsDBNull(dgvPBP.Rows(e.RowIndex).Cells("COMMENT_LANGUAGE").Value), 1, dgvPBP.Rows(e.RowIndex).Cells("COMMENT_LANGUAGE").Value))
                    _objGameDetails.CommentData = dgvPBP.Rows(e.RowIndex).Cells("COMMENTS").Value.ToString()
                    _objGameDetails.CommentsTime = dgvPBP.Rows(e.RowIndex).Cells("TIME_ELAPSED").Value.ToString()
                    Dim objComments As New frmComments
                    If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                        InsertPlayByPlayData()
                    End If
                End If
                _objActionDetails.IsEditMode = False
                dgvPBP.ClearSelection()
                UnHighlightActions()
                ResetProperties()
                dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row
                _objGameDetails.IsEditMode = False
                _objGameDetails.CommentsTime = Nothing
                Exit Sub
            ElseIf _objActionDetails.EventId = 59 Then
                _objGameDetails.CommentLanguageID = IIf(IsDBNull(dgvPBP.Rows(e.RowIndex).Cells("COMMENT_LANGUAGE").Value), 1, dgvPBP.Rows(e.RowIndex).Cells("COMMENT_LANGUAGE").Value)
                _objGameDetails.CommentData = IIf(IsDBNull(dgvPBP.Rows(e.RowIndex).Cells("COMMENTS").Value), "", dgvPBP.Rows(e.RowIndex).Cells("COMMENTS").Value)
                _objGameDetails.IsEditMode = True
                Dim objComments As New frmComments
                If objComments.ShowDialog = Windows.Forms.DialogResult.OK Then
                    InsertPlayByPlayData()
                ElseIf _objGameDetails.IsDelayed Then
                    EnableDisableControls(False, False)
                End If
                If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                    EnableDisableControls(False, False)
                End If
                _objActionDetails.IsEditMode = False
                dgvPBP.ClearSelection()
                dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row
                ResetProperties()
                Exit Sub
            ElseIf _objActionDetails.EventId = 58 Then
                Dim objabandon As New frmAbandon
                _objGameDetails.Reason = IIf(IsDBNull(dgvPBP.Rows(e.RowIndex).Cells("COMMENTS").Value), "", dgvPBP.Rows(e.RowIndex).Cells("COMMENTS").Value)
                _objGameDetails.IsEditMode = True
                If objabandon.ShowDialog = Windows.Forms.DialogResult.OK Then
                    InsertPlayByPlayData()
                ElseIf _objGameDetails.IsGameAbandoned Then
                    EnableDisableControls(False, False)
                End If
                If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                    EnableDisableControls(False, False)
                End If
                _objActionDetails.IsEditMode = False
                dgvPBP.ClearSelection()
                dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row
                ResetProperties()
                Exit Sub
            End If
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "UNIQUE_ID- " & dgvPBP.Rows(e.RowIndex).Cells("UNIQUE_ID").Value & " SEQUENCE_NUMBER- " & Math.Round(dgvPBP.Rows(e.RowIndex).Cells("SEQUENCE_NUMBER").Value, 3) & " EVENT_CODE- " & dgvPBP.Rows(e.RowIndex).Cells("EVENT_CODE_ID").Value & " TEAM- " & dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value & " Double Click Datagrid Cell", 1, 0)
            'Fill players on the field at that moment
            FillPlayersInUserControls()
            'highlight TeamId
            If _objActionDetails.TeamId.HasValue Then
                If _objActionDetails.EventId = 50 Then
                    clsUtility.HighlightGoldButton(If(_objActionDetails.TeamId = _objGameDetails.HomeTeamID, btnHomePass, btnAwayPass))
                Else
                    clsUtility.HighlightGoldButton(If(_objActionDetails.TeamId = _objGameDetails.HomeTeamID, btnHomeAction, btnAwayAction))
                End If
            End If
            'highlight Event
            If _bookingEvents.Contains(_objActionDetails.EventId) Then
                _objActionDetails.BookingTypeId = CInt(dgvPBP.Rows(e.RowIndex).Cells("BOOKING_TYPE_ID").Value)
                DisplayBookingTypes()
            ElseIf _objActionDetails.EventId = 67 Then
                DisplayOobTypes(CInt(dgvPBP.Rows(e.RowIndex).Cells("OUT_OF_BOUNDS_RES_ID").Value))
            Else
                HighlightActionEvent()
            End If
            'highlight player
            If _objActionDetails.PlayerId.HasValue Then
                If (_objActionDetails.TeamId = _objGameDetails.HomeTeamID And _objActionDetails.EventId <> 28) Then
                    UdcSoccerPlayerPositionsHome.USPHighlightButton(_objActionDetails.PlayerId)
                    If _objActionDetails.defPlayerId.HasValue Then
                        UdcSoccerPlayerPositionsAway.USPHighlightButton(_objActionDetails.defPlayerId)
                    End If
                    If _bookingEvents.Contains(_objActionDetails.EventId) Then
                        HighlightSubPlayerButton(pnlHomeSub, _objActionDetails.PlayerId)
                    ElseIf _objActionDetails.subPlayerId.HasValue Then
                        HighlightSubPlayerButton(pnlHomeSub, _objActionDetails.subPlayerId)
                    End If
                Else
                    UdcSoccerPlayerPositionsAway.USPHighlightButton(_objActionDetails.PlayerId)
                    If _objActionDetails.defPlayerId.HasValue Then
                        UdcSoccerPlayerPositionsHome.USPHighlightButton(_objActionDetails.defPlayerId)
                    End If
                    If _bookingEvents.Contains(_objActionDetails.EventId) Then
                        HighlightSubPlayerButton(pnlAwaySub, _objActionDetails.PlayerId)
                    ElseIf _objActionDetails.subPlayerId.HasValue Then
                        HighlightSubPlayerButton(pnlAwaySub, _objActionDetails.subPlayerId)
                    End If
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Delete Action
    Private Sub DeleteActionToolStripItem_Click(sender As Object, e As EventArgs) Handles DeleteActionToolStripItem.Click
        Try
            'To avoid Column header click
            If (dgvPBP.SelectedRows.Count = 0) Then
                Exit Sub
            End If
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ContextMenuItemSelected, DeleteActionToolStripItem.Text, 1, 0)

            'validations
            If IsDBNull(dgvPBP.Rows(dgvPBP.Rows.Count - 1).Cells("SEQUENCE_NUMBER").Value) Then
                _messageDialog.Show(_objGameDetails.languageid, "You are not allowed to delete an action until you save the current action!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                dgvPBP.Rows(dgvPBP.Rows.Count - 1).Selected = True
                Exit Sub
            End If

            If Not _objActionDetails.IsEditMode Then
                Dim timeElapsed = dgvPBP.SelectedRows(0).Cells("TIME_ELAPSED").Value
                If Not IsDeleteAllowed(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value, IIf(IsDBNull(dgvPBP.SelectedRows(0).Cells("TEAM").Value), "", dgvPBP.SelectedRows(0).Cells("TEAM").Value), IIf(IsDBNull(dgvPBP.SelectedRows(0).Cells("BY PLAYER").Value), "", dgvPBP.SelectedRows(0).Cells("BY PLAYER").Value)) Then
                    Exit Sub
                End If
                _objGeneral.DeletePlaybyPlayData(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, _objGameDetails.ReporterRoleSerial)
                Select Case dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value
                    Case 13
                        EnableDisableControls(True)
                    Case 21
                        EnableDisableControls(True, False)
                        If _objGameDetails.IsHalfTimeSwap Then
                            SwitchSide()
                        End If
                    Case 21, 10
                        EnableDisableControls(True, False)
                        _objGameDetails.IsEndGame = False
                    Case 58
                        _objGameDetails.IsGameAbandoned = False
                        If Not frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                            EnableDisableControls(True)
                            frmMain.SetClockControl(True)
                            frmMain.EnableMenuItem("AbandonToolStripMenuItem", True)
                        End If
                    Case 59
                        If Not frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                            EnableDisableControls(True)
                            frmMain.SetClockControl(True)
                        End If
                        frmMain.EnableMenuItem("DelayedToolStripMenuItem", True)
                        _objGameDetails.IsDelayed = False
                    Case 65
                        EnableDisableControls(False, False)
                        frmMain.SetClockControl(True)
                        _objGameDetails.IsDelayed = True
                        frmMain.EnableMenuItem("DelayOverToolStripMenuItem", True)
                End Select
                _objGameDetails.ActivateGame = False
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub dgvPBP_KeyUp(sender As Object, e As KeyEventArgs) Handles dgvPBP.KeyUp
        If e.KeyCode = 40 Or e.KeyCode = 38 Then
            If _objActionDetails.IsEditMode Then
                dgvPBP.ClearSelection()
                HighlightDgvEditedRow(_rowIndex)
            End If
        End If
    End Sub

    Private Sub dgvPBP_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvPBP.KeyDown
        If e.KeyCode = 46 And dgvPBP.SelectedRows.Count > 0 Then
            e.SuppressKeyPress = True
            DeleteActionToolStripItem.PerformClick()
            _deleteKeyPressCnt = _deleteKeyPressCnt + 1
        End If
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If (e.ToString = "System.EventArgs") Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.KeyDown, "Save action through Enter Key", 1, 0)
            Else
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            End If

            If (_objActionDetails.TeamId.HasValue) Or (_objActionDetails.EventId.HasValue) Then
                'FPLRS-406 goal missing due to event "doubleclick & up arrow"  that highlights the previous event in grid 
                If _objActionDetails.IsEditMode Then
                    If (_pbpRowSelectedSqNo <> dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value) Then
                        _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "InsertPlayByPlayData Index changed orig : " & _pbpRowSelectedSqNo & " changed to : " & dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, 1, 0)
                        HighlightPBPSelectedRow()
                    End If
                End If
                SavePreviousAction()
            Else
                _messageDialog.Show(_objGameDetails.languageid, "No records to save. Please check!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'TOPZ-1318
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim dtPbPsource As DataTable = dgvPBP.DataSource
            Dim recordDelete() As DataRow = dtPbPsource.Select("SEQUENCE_NUMBER Is Null")
            If recordDelete.Length > 0 Then
                For Each dr As DataRow In recordDelete
                    dr.Delete()
                    dr.AcceptChanges()
                Next
            End If

            If _objActionDetails.IsEditMode And dgvPBP.SelectedRows.Count > 0 Then
                dgvPBP.Rows(_rowIndex).DefaultCellStyle.SelectionBackColor = Color.DodgerBlue
                dgvPBP.Rows(_rowIndex).DefaultCellStyle.SelectionForeColor = Color.White
            End If
            UnHighlightActions()
            _objActionDetails.IsEditMode = False
            pnlSubEvents.Enabled = True
            pnlEvents.Enabled = True
            pnlGoalEvents.Enabled = True
            dgvPBP.Enabled = True
            ResetProperties()
            dgvPBP.ClearSelection()
            grpBooking.Visible = False
            grpOutBounds.Visible = False
            ControlsSetting()
            If dgvPBP.RowCount > 0 Then
                dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row.
            End If
            FillPlayersInUserControls()
            txtTime.Text = String.Empty
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region " User defined functions "

    Public Sub SwitchSide()
        Try
            _objGameDetails.IsHomeTeamOnLeftSwitch = _homeTeamOnLeft
            _objGameDetails.IsHomeTeamOnLeft = _homeTeamOnLeft
            _eventHotkeys.Remove(Keys.H)
            _eventHotkeys.Remove(Keys.J)
            _eventHotkeys.Remove(Keys.G)
            _eventHotkeys.Remove(Keys.K)
            If _homeTeamOnLeft Then
                btnHomePass.Location = New Point(12, 1)
                btnHomeAction.Location = New Point(214, 1)
                btnAwayAction.Location = New Point(608, 1)
                btnAwayPass.Location = New Point(813, 1)
                'hot key reassignment
                _eventHotkeys.Add(Keys.H, btnHomeAction)
                _eventHotkeys.Add(Keys.J, btnAwayAction)
                _eventHotkeys.Add(Keys.G, btnHomePass)
                _eventHotkeys.Add(Keys.K, btnAwayPass)
                UdcSoccerPlayerPositionsHome.Location = New Point(12, 62)
                UdcSoccerPlayerPositionsAway.Location = New Point(608, 62)

                UdcSoccerPlayerPositionsAway.USPLeftToRight = False
                UdcSoccerPlayerPositionsHome.USPLeftToRight = True
                pnlHomeSub.Location = New Point(12, 286)
                pnlAwaySub.Location = New Point(669, 287)
                _homeTeamOnLeft = False
            Else
                btnAwayPass.Location = New Point(12, 1)
                btnAwayAction.Location = New Point(214, 1)
                btnHomeAction.Location = New Point(608, 1)
                btnHomePass.Location = New Point(813, 1)
                'hot key reassignment
                _eventHotkeys.Add(Keys.H, btnAwayAction)
                _eventHotkeys.Add(Keys.J, btnHomeAction)
                _eventHotkeys.Add(Keys.G, btnAwayPass)
                _eventHotkeys.Add(Keys.K, btnHomePass)
                UdcSoccerPlayerPositionsAway.Location = New Point(12, 62)
                UdcSoccerPlayerPositionsHome.Location = New Point(608, 62)

                UdcSoccerPlayerPositionsAway.USPLeftToRight = True
                UdcSoccerPlayerPositionsHome.USPLeftToRight = False
                pnlHomeSub.Location = New Point(669, 286)
                pnlAwaySub.Location = New Point(12, 287)
                _homeTeamOnLeft = True
            End If
            frmMain.SetHeader2()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Save Actions that includes PBP + TOUCHES
    Public Function SaveAction() As Tuple(Of Integer, Integer?, Integer?) 'EventId,TeamId,SubOOB
        Try
            InsertPlayByPlayData()
            UnHighlightActions()
            If _objActionDetails.IsEditMode = False Then
                Dim substituteEvents() As Integer = {22, 7}
                If (substituteEvents.Contains(_objActionDetails.EventId)) Then
                    FillPlayersInUserControls()
                End If
                ''ínsert clock event after inserting the main event
                'InsertClockEvent()

                'A1->delete the row which was displayed from memory
                Dim dtPbPsource As DataTable = dgvPBP.DataSource
                Dim recordDelete() As DataRow = dtPbPsource.Select("SEQUENCE_NUMBER Is Null")
                If recordDelete.Length > 0 Then
                    For Each dr As DataRow In recordDelete
                        dr.Delete()
                        dr.AcceptChanges()
                    Next
                    dgvPBP.ClearSelection()
                End If

                Dim dtPbpEvents As DataTable = _objGeneral.LoadPrimaryRepInsertedPBPData(_objGameDetails.GameCode, _objGameDetails.FeedNumber, _objGameDetails.languageid, CInt(IIf(_objGameDetails.IsDefaultGameClock, 1, 0))).Tables(0)
                PbpDataTreeLoad(dtPbpEvents, False, True)
                'display PBP events in Main screen
                Dim recordsInsert = (From pbpSource In dtPbpEvents _
                                         Where _eventsToShow.Contains(pbpSource.Field(Of Int16)("EVENT_CODE_ID"))
                                         ).ToList()
                If recordsInsert.Count > 0 Then
                    frmMain.DisplayPbpEventsInMainScreenPz(dtPbpEvents)
                End If
                'Update the PBP Data REFRESHED = 'Y'
                _objClsPbpTree.PbpDataUpdateRefreshed(dtPbpEvents)
            Else
                _objActionDetails.IsEditMode = False
                FillPlayersInUserControls()
                ControlsSetting()
            End If
            Dim subStatus = (Not btnOOB.Enabled And _objActionDetails.EventId = 22)
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, "                            Action Saved. [Event Time] " & txtTime.Text.ToString & ", [Game Time] " & frmMain.UdcRunningClock1.URCCurrentTime.ToString & " [Period] " & If(subStatus, _objGameDetails.CurrentPeriod + 1, _objGameDetails.CurrentPeriod).ToString & " [Home Score - AwayScore]  [" & frmMain.lblScoreHome.Text & "-" & frmMain.lblScoreAway.Text & "]", 1, 1)
            Return New Tuple(Of Integer, Integer?, Integer?)(_objActionDetails.EventId, _objActionDetails.TeamId, If(_objActionDetails.OutOfBoundsResId, 0))
        Catch ex As Exception
            Throw
        Finally
            ResetProperties()
            grpBooking.Visible = False
            grpOutBounds.Visible = False
            lblTime.Visible = False
            txtTime.Visible = False
        End Try
    End Function
    Public Sub ResetProperties()
        Try
            _objActionDetails.TeamId = Nothing
            _objActionDetails.EventId = Nothing
            _objActionDetails.EventDescription = Nothing
            _objActionDetails.subPlayerId = Nothing
            _objActionDetails.PlayerId = Nothing
            _objActionDetails.BookingTypeId = Nothing
            _objActionDetails.TimeofEvent = Nothing
            _objActionDetails.defPlayerId = Nothing
            _objGameDetails.CommentsTime = Nothing
            _objActionDetails.OutOfBoundsResId = Nothing
            _objActionDetails.OutOfBoundsResDesc = Nothing
            _objGameDetails.Reason = Nothing
            _objGameDetails.CommentLanguageID = Nothing
            _objGameDetails.CommentData = Nothing
            _betweenHalf = False
            txtTime.Text = String.Empty
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'save Goal event directly to database.
    Private Sub SaveGoalEvent()
        Try
            If (_objActionDetails.IsEditMode And Not _goalEvents.Contains(_objActionDetails.EventId)) Then
                _messageDialog.Show(_objGameDetails.languageid, "You are not allowed enter Goal Event in edit mode!", "Goal", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            'Goal events Click save to DB immediately
            If _messageDialog.Show(_objGameDetails.languageid, "You are crediting a goal for " & If(_objActionDetails.TeamId = _objGameDetails.HomeTeamID, _objGameDetails.HomeTeam, _objGameDetails.AwayTeam) & ". Is this Correct?", "Goal", MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                SaveAction()
                dgvPBP.ClearSelection()
                dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.Rows.Count - 1
            Else
                UnhighlightEvent()
                If _objActionDetails.EventId = 28 Then
                    'ówn goal clear the highlighted player
                    _objActionDetails.PlayerId = Nothing
                    UnhighlightLineupPlayers()
                End If
                _objActionDetails.EventId = 63
                _objActionDetails.EventDescription = Nothing
                DisplayActionInGrid()
                btnSave.Focus()
            End If
        Catch ex As Exception
            Throw
        Finally
            _objActionDetails.IsEditMode = False
        End Try
    End Sub
    'set clock time during End period
    Private Sub SetClockTimeAtEndPeriod()
        Try
            Dim timeElapsed As New Dictionary(Of Integer, ClsActionDetails.TimeElapsed)
            timeElapsed.Add(1, New ClsActionDetails.TimeElapsed(0, 2700, 45))
            timeElapsed.Add(2, New ClsActionDetails.TimeElapsed(2700, 2700, 90))
            timeElapsed.Add(3, New ClsActionDetails.TimeElapsed(5400, 900, 105))
            timeElapsed.Add(4, New ClsActionDetails.TimeElapsed(6300, 900, 120))
            timeElapsed.Add(5, New ClsActionDetails.TimeElapsed(7200, 900, 15))
            If (timeElapsed.ContainsKey(_objGameDetails.CurrentPeriod)) Then
                If IIf(_objGameDetails.IsDefaultGameClock, CalculateTimeElapsedBefore(CInt(_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec))), CInt(_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec))) < timeElapsed.Item(_objGameDetails.CurrentPeriod).Checkminutes Then
                    If _objGameDetails.IsDefaultGameClock Then
                        frmMain.UdcRunningClock1.URCSetTime(timeElapsed.Item(_objGameDetails.CurrentPeriod).Displayminutes, 0, True)
                    Else
                        frmMain.UdcRunningClock1.URCSetTime(If(_objGameDetails.CurrentPeriod > 2, timeElapsed.Item(5).Displayminutes, timeElapsed.Item(1).Displayminutes), 0, True)
                    End If
                    _objActionDetails.TimeofEvent = frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Save to Live_soc_pbp
    Public Sub InsertPlayByPlayData()
        Try
            Dim subStatus = (Not btnOOB.Enabled And _objActionDetails.EventId = 22)
            Dim timeElapsed As Integer
            Dim timeElapsedMilliSec As Double

            If _objActionDetails.TimeofEvent Is Nothing Then
                _objActionDetails.TimeofEvent = frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec
            End If
            'main entry time edit allow in normal mode.
            If _objActionDetails.IsEditMode = False And txtTime.Text.Replace(":", "").Trim <> "" Then
                _objActionDetails.TimeofEvent = txtTime.Text.Trim
            End If

            Select Case _objActionDetails.EventId
                Case 34, 21 'Game start,start period
                    timeElapsed = CInt(_objUtility.ConvertMinuteToSecond("00:00"))
                    timeElapsedMilliSec = timeElapsed
                Case Else
                    'énd period set the clock time to default.
                    If _objActionDetails.EventId = 13 And Not _objGameDetails.IsEndGame Then SetClockTimeAtEndPeriod()
                    If _objGameDetails.IsDefaultGameClock Then 'sub event entered at half time should go as "0" with period = current period + 1
                        'TOPZ 422 Start Period/End Period - we do not format pbp tree like other Action
                        timeElapsed = If(subStatus, 0, CalculateTimeElapsedBefore(CInt(_objUtility.ConvertMinuteToSecond(If(_objActionDetails.TimeofEvent, frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec)))))
                        timeElapsedMilliSec = If(subStatus, 0, CalculateTimeElapsedBeforeMilliSec(CInt(_objUtility.ConvertMinuteToMillisecond(If(_objActionDetails.TimeofEvent, frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec)))))
                    Else
                        timeElapsed = If(subStatus, 0, CInt(_objUtility.ConvertMinuteToSecond(If(_objActionDetails.TimeofEvent, frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec))))
                        timeElapsedMilliSec = If(subStatus, 0, CInt(_objUtility.ConvertMinuteToMillisecond(If(_objActionDetails.TimeofEvent, frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec))))
                    End If
            End Select

            Dim drPbpData As DataRow = _objGameDetails.PBP.Tables(0).NewRow()
            drPbpData("GAME_CODE") = _objGameDetails.GameCode
            drPbpData("FEED_NUMBER") = _objGameDetails.FeedNumber
            drPbpData("PERIOD") = If(subStatus, _objGameDetails.CurrentPeriod + 1, _objGameDetails.CurrentPeriod) 'In Half time, if sub event is entered it should go with period +1
            drPbpData("OFFENSE_SCORE") = _objGameDetails.HomeScore
            drPbpData("DEFENSE_SCORE") = _objGameDetails.AwayScore
            drPbpData("REPORTER_ROLE") = _objGameDetails.ReporterRole
            drPbpData("SEQUENCE_NUMBER") = 0
            drPbpData("CONTINUATION") = "F"
            drPbpData("RECORD_EDITED") = "N"
            drPbpData("PROCESSED") = "N"
            drPbpData("EDIT_UID") = 0
            drPbpData("DEMO_DATA") = "N"
            drPbpData("SYSTEM_TIME") = DateTimeHelper.GetDateString((Date.Now - _objLoginDetails.USIndiaTimeDiff), DateTimeHelper.OutputFormat.ISOFormat)
            drPbpData("EVENT_CODE_ID") = _objActionDetails.EventId
            drPbpData("TIME_ELAPSED") = timeElapsed
            drPbpData("GAME_TIME_MS") = timeElapsedMilliSec ' store game time in milliseconds
            drPbpData("OFFENSIVE_PLAYER_ID") = If(_objActionDetails.subPlayerId, If(_objActionDetails.PlayerId, DBNull.Value))
            drPbpData("PLAYER_OUT_ID") = If(_objActionDetails.subPlayerId, _objActionDetails.PlayerId, If(_objActionDetails.subPlayerId, DBNull.Value))
            drPbpData("DEFENSIVE_PLAYER_ID") = If(_objActionDetails.defPlayerId, DBNull.Value)
            drPbpData("BOOKING_TYPE_ID") = If(_objActionDetails.BookingTypeId, DBNull.Value)
            If _objActionDetails.EventId = 66 Then 'INJURY_TIME
                drPbpData("INJURY_TIME") = IIf(_objGameDetails.CommentsTime > 0, _objGameDetails.CommentsTime, DBNull.Value)
            End If
            If (_objActionDetails.EventId = 48) Then
                drPbpData("MANAGER_ID") = IIf(_objGameDetails.OffensiveCoachID > 0, _objGameDetails.OffensiveCoachID, DBNull.Value)
                drPbpData("BOOKING_REASON_ID") = IIf(_objGameDetails.ExplReasonID > 0, _objGameDetails.ExplReasonID, DBNull.Value)
                _objActionDetails.TeamId = _objGameDetails.OffensiveTeamID
            ElseIf (_objActionDetails.EventId = 4) Then
                drPbpData("COMMENT_ID") = IIf(_objGameDetails.PredefinedCommentID > 0, _objGameDetails.PredefinedCommentID, DBNull.Value)
                drPbpData("COMMENT_LANGUAGE") = IIf(_objGameDetails.CommentLanguageID > 0, _objGameDetails.CommentLanguageID, DBNull.Value)
                drPbpData("COMMENTS") = _objGameDetails.CommentData
            End If
            drPbpData("TEAM_ID") = If(_objActionDetails.TeamId, DBNull.Value)
            drPbpData("COMMENTS") = IIf(_objActionDetails.EventId = 58 Or _objActionDetails.EventId = 59 Or _objActionDetails.EventId = 4, IIf(_objActionDetails.EventId = 58, _objGameDetails.Reason, _objGameDetails.CommentData), "")

            If _objActionDetails.IsEditMode Then
                drPbpData("SEQUENCE_NUMBER") = dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value
                drPbpData("PERIOD") = dgvPBP.SelectedRows(0).Cells("PERIOD").Value
                drPbpData("EDIT_UID") = dgvPBP.SelectedRows(0).Cells("UNIQUE_ID").Value
                drPbpData("SYSTEM_TIME") = dgvPBP.SelectedRows(0).Cells("SYSTEM_TIME").Value
            End If
            If _objActionDetails.EventId = 67 Then 'OOB
                _objActionDetails.OutOfBoundsResId = If(_objActionDetails.OutOfBoundsResId, 1) ''Default throw In
                drPbpData("OUT_OF_BOUNDS_RES_ID") = _objActionDetails.OutOfBoundsResId
            End If
            _objGameDetails.PBP.Tables(0).Rows.Add(drPbpData)
            _objGameDetails.PBP.DataSetName = "SoccerEventData"

            If _objActionDetails.EventId = 7 Or _objActionDetails.EventId = 22 Then
                Dim playerOut As Integer = If(_objActionDetails.subPlayerId, _objActionDetails.PlayerId, If(_objActionDetails.subPlayerId, DBNull.Value))
                If playerOut = _objGameDetails.HomeTeamCaptainId Or playerOut = _objGameDetails.AwayTeamCaptainId Then
                    _messageDialog.Show(_objGameDetails.languageid, "Team Captain has been " + IIf(_objActionDetails.EventId = 7, "sent off", "substituted") + ". Please select a new captain from Team Setup page(Setup->Team Setup) .", "Team Captain", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                End If
            End If
            'insert pbp data to SQL
            Dim drScores As New DataSet
            drScores = _objActions.InsertEditActionsPBPData(_objGameDetails.PBP.GetXml(), drPbpData("SEQUENCE_NUMBER"))
            If _objGameDetails.IsGameAbandoned Or _objGameDetails.IsDelayed Then
                EnableDisableControls(False, False)
            End If


            'Set the latest scores to Lables in main screen
            If drScores.Tables(0).Rows.Count > 0 Then
                frmMain.lblScoreHome.Text = If(drScores.Tables(0).Rows(0).Item("TEAM_ID") = _objGameDetails.HomeTeamID, drScores.Tables(0).Rows(0).Item("OFFENSE_SCORE"), drScores.Tables(0).Rows(0).Item("DEFENSE_SCORE"))
                frmMain.lblScoreAway.Text = If(drScores.Tables(0).Rows(0).Item("TEAM_ID") = _objGameDetails.AwayTeamID, drScores.Tables(0).Rows(0).Item("OFFENSE_SCORE"), drScores.Tables(0).Rows(0).Item("DEFENSE_SCORE"))
            End If

        Catch ex As Exception
            dgvPBP.ClearSelection()
            UnHighlightActions()
            Throw
        Finally
            _objGameDetails.PBP.Tables(0).Rows.Clear()
            _objActionDetails.TimeofEvent = Nothing
        End Try
    End Sub
    Private Sub AssignBtnTagToProperty(ByVal buttonEvent As Button)
        Try
            'unhighlight other event button if selected alreday
            UnhighlightEvent()
            'ássign selected event to property
            _objActionDetails.EventDescription = buttonEvent.Text
            If (buttonEvent.Name.Contains("btnHome")) Then
                _objActionDetails.TeamId = _objGameDetails.HomeTeamID
                _objActionDetails.EventDescription = If(buttonEvent.Name.Contains("btnHomePass"), "Pass", "Control")
            ElseIf (buttonEvent.Name.Contains("btnAway")) Then
                _objActionDetails.TeamId = _objGameDetails.AwayTeamID
                _objActionDetails.EventDescription = If(buttonEvent.Name.Contains("btnAwayPass"), "Pass", "Control")
            End If
            _objActionDetails.EventId = Convert.ToInt16(buttonEvent.Tag)
            clsUtility.HighlightGoldButton(buttonEvent)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub DisplayBookingTypes()
        Try
            grpBooking.Visible = True
            grpBooking.BringToFront()
            GroupBoxPictureBoxBackColor(grpBooking, Color.White)
            If _objActionDetails.BookingTypeId = 1 Then
                picYellow.BackColor = Color.Gray
            ElseIf _objActionDetails.BookingTypeId = 2 Then
                picRed.BackColor = Color.Gray
            Else
                picYellowRed.BackColor = Color.Gray
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub DisplayOobTypes(ByVal subType? As Integer)
        Try
            If Not _objActionDetails.IsEditMode Then
                dgvPBP.Enabled = False
            End If

            grpOutBounds.Visible = True
            grpOutBounds.BringToFront()
            GroupBoxButtonBackColor(grpOutBounds, Color.White)
            If subType.HasValue Then
                If subType = 1 Then
                    btnThrow.PerformClick()
                ElseIf subType = 2 Then
                    BtnCornerOut.PerformClick()
                ElseIf subType = 3 Then
                    btnGoalKickOut.PerformClick()
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'get yellow card count for player
    Private Function GetYellowCardCountforPlayer() As Integer
        Try
            Return _objGeneral.GetPlayerCntforYellowCard(_objGameDetails.GameCode, _objGameDetails.FeedNumber, clsGameDetails.YELLOWCARD, _objActionDetails.PlayerId, IIf(IsDBNull(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value), 0, dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value))
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function IsBookingValid() As Boolean
        Try
            If _objActionDetails.PlayerId IsNot Nothing Then
                Dim playerYellowCardCnt As Integer = GetYellowCardCountforPlayer()
                Select Case _objActionDetails.BookingTypeId
                    Case 3 'yellow/Red Card
                        _objActionDetails.subPlayerId = _objActionDetails.PlayerId
                        If playerYellowCardCnt = 0 Then
                            _messageDialog.Show(_objGameDetails.languageid, "You cannot enter SECOND yellow card without showing the first!", "Yellow-Red Card", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                            Return False
                        End If
                    Case 1 'yellow card
                        _objActionDetails.subPlayerId = Nothing
                        If playerYellowCardCnt = 1 Then ' This is second Yellow Card - so enter Red Card with Red-Yellow type
                            If _messageDialog.Show(_objGameDetails.languageid, "This is the players second yellow card.If he was sent off, please press CANCEL and select YELLOW/RED. If the referee made a mistake and he is still being allowed to play, press OK.", "Booking", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Cancel Then
                                Return False
                            Else
                                If _messageDialog.Show(_objGameDetails.languageid, "Are you sure the player has two yellow cards but is staying in the game.If yes, press OK. Otherwise, press CANCEL.", "Booking", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Cancel Then
                                    Return False
                                End If
                            End If
                        End If
                    Case 2 'red card
                        'if a gk is thrown out, the warn the user to choose the active goal keeper.
                        _objActionDetails.subPlayerId = _objActionDetails.PlayerId
                        If (_objClsPlayerData.GetPlayerStartingPosition(_playerTable, _objActionDetails.PlayerId)) = 1 Then
                            _messageDialog.Show(_objGameDetails.languageid, "Please set NEW GOALKEEPER through OTHERS menu(Display Active Goalkeepr)!", "Red Card", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        End If
                        Return True
                End Select
            Else
                _messageDialog.Show(_objGameDetails.languageid, "Please select the player for whom the card is issued!", "Booking", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function
    'player selection validate
    Private Function ValidatePlayer(ByVal playerTeamId As Integer) As Boolean
        Try
            'validations..
            If (_objGameDetails.IsGameAbandoned Or _objGameDetails.IsDelayed) And Not _objActionDetails.IsEditMode Then
                _messageDialog.Show(_objGameDetails.languageid, "Game is Abandoned/Delayed, you are not allowed to enter any events!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Return False
            ElseIf btnHomeAction.Enabled = False Then
                _messageDialog.Show(_objGameDetails.languageid, "Please enter Start Period to proceed further!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Return False
            ElseIf _objActionDetails.TeamId Is Nothing Then
                _messageDialog.Show(_objGameDetails.languageid, "Please choose Home/Away Control before selecting a player.", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If
            If (playerTeamId <> _objActionDetails.TeamId) Then
                If (_objActionDetails.EventId = 8) Then ' foul
                    If _objActionDetails.PlayerId.HasValue = False Then
                        _messageDialog.Show(_objGameDetails.languageid, "First enter the player who committed the foul!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        Return False
                    End If
                ElseIf (_objActionDetails.EventId = 28) Then
                    Return True
                Else
                    _messageDialog.Show(_objGameDetails.languageid, "This player does not belong to selected Action team.", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                End If
            Else
                If (_objActionDetails.EventId = 28) Then 'own goal
                    _messageDialog.Show(_objGameDetails.languageid, "The player cannot be from the same team for Own Goal event. Please check!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub HighlightActionEvent()
        Try
            ''clear the highlighted color for the buttons from all panels
            If _objActionDetails.EventId IsNot Nothing Then
                For Each higlightControl As Control In Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).Where(Function(c1) c1.Tag = _objActionDetails.EventId)
                    clsUtility.HighlightGoldButton((CType(higlightControl, Button)))
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'unhighlight event,team and player buttons when clicked..
    Public Sub UnHighlightActions()
        Try
            UnhighlightEvent()
            UnhighlightLineupPlayers()
            UnhighlightSubPlayer()
            UnhighlightActionButtons()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UnhighlightEvent()
        Try
            ''clear the highlighted color for the buttons from all panels
            For Each higlightControl As Control In Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).Where(Function(c1) c1.BackColor = Color.Gold)
                Dim darkGreyButtons() As Integer = {67, 68, 69} 'OOB,RDB & Obstacle should be Dark grey
                If (darkGreyButtons.Contains(higlightControl.Tag)) Then
                    clsUtility.SetButtonColor(CType(higlightControl, Button), Color.DarkGray, Color.Black)
                Else
                    clsUtility.SetButtonColor(CType(higlightControl, Button), Color.WhiteSmoke, clsUtility.GetTextColor(Color.WhiteSmoke))
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UnhighlightActionButtons()
        Try
            'clear the highlighted color for action buttons
            clsUtility.SetButtonColor(btnHomeAction, If(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor), clsUtility.GetTextColor(If(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor)))
            clsUtility.SetButtonColor(btnAwayAction, If(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor), clsUtility.GetTextColor(If(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor)))
            clsUtility.SetButtonColor(btnHomePass, If(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor), clsUtility.GetTextColor(If(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor)))
            clsUtility.SetButtonColor(btnAwayPass, If(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor), clsUtility.GetTextColor(If(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor)))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UnhighlightLineupPlayers()
        Try
            UdcSoccerPlayerPositionsHome.USPUnhighlightButtons()
            UdcSoccerPlayerPositionsAway.USPUnhighlightButtons()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UnhighlightSubPlayer()
        Try
            'clear the highlighted color for the buttons from all panels
            clsUtility.SetPanelButtonColor(pnlHomeSub, IIf(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor), clsUtility.GetTextColor(IIf(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor)))
            clsUtility.SetPanelButtonColor(pnlAwaySub, IIf(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor), clsUtility.GetTextColor(IIf(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor)))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Fill Starting lineups players
    Public Sub FillPlayersInUserControls()
        Try
            If _objActionDetails.IsEditMode And dgvPBP.SelectedRows.Count > 0 Then
                'Handle Red cards/substitute
                _playerTable = _objClsPlayerData.FillPlayer(_objGeneral.GetAllRosters(_objGameDetails.GameCode, _objGameDetails.languageid, CDec(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value)).Tables(0))
            Else
                _playerTable = _objClsPlayerData.FillPlayer(_objGeneral.GetAllRosters(_objGameDetails.GameCode, _objGameDetails.languageid).Tables(0))
            End If

            UdcSoccerPlayerPositionsHome.USPTeamId = _objGameDetails.HomeTeamID
            UdcSoccerPlayerPositionsHome.UspPlayerDetailsCollection = _objClsPlayerData.GetStaringPlayers(_playerTable, _objGameDetails.HomeTeamID)

            UdcSoccerPlayerPositionsAway.USPTeamId = _objGameDetails.AwayTeamID
            UdcSoccerPlayerPositionsAway.UspPlayerDetailsCollection = _objClsPlayerData.GetStaringPlayers(_playerTable, _objGameDetails.AwayTeamID)

            'set custom formation if the user had dragged and dropped the players.
            If _objActionDetails.CustomFormationHome IsNot Nothing Then
                UdcSoccerPlayerPositionsHome.USPCustomFormationDetails = _objActionDetails.CustomFormationHome
            ElseIf _objGameDetails.HomeFormationId IsNot Nothing Then
                UdcSoccerPlayerPositionsHome.USPFormation = CType(_objGameDetails.HomeFormationId, udcSoccerPlayerPositions.Formation)
            End If

            If _objActionDetails.CustomFormationAway IsNot Nothing Then
                UdcSoccerPlayerPositionsAway.USPCustomFormationDetails = _objActionDetails.CustomFormationAway
            ElseIf _objGameDetails.AwayFormationId IsNot Nothing Then
                UdcSoccerPlayerPositionsAway.USPFormation = CType(_objGameDetails.AwayFormationId, udcSoccerPlayerPositions.Formation)
            End If
            'clear the text/tag and disable the buttons
            clsUtility.ClearPanelButtonText(pnlHomeSub)
            clsUtility.ClearPanelButtonText(pnlAwaySub)
            _objGeneral.FillBenchPlayers(_objClsPlayerData.GetBenchPlayers(_playerTable, _objGameDetails.HomeTeamID).ToList(), pnlHomeSub, "btnHomeSub")
            _objGeneral.FillBenchPlayers(_objClsPlayerData.GetBenchPlayers(_playerTable, _objGameDetails.AwayTeamID).ToList(), pnlAwaySub, "btnAwaySub")

            'Subs Count
            _homeSubs = 0
            _awaySubs = 0
            Dim playersList = (From players In _playerTable Where players.Substitute = True And players.RedCard = False And players.StartingPosition Is Nothing).ToList()
            For Each player In playersList
                If player.TeamId = _objGameDetails.HomeTeamID Then
                    _homeSubs += 1
                Else
                    _awaySubs += 1
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function SavePreviousAction() As Boolean
        Dim insertStatus As Boolean = True
        Try
            Dim activeGoalKeeperCnt = (From players In _playerTable Where players.StartingPosition = 1).Count()
            If (activeGoalKeeperCnt < 2 And _objActionDetails.EventId <> 22) Then
                _messageDialog.Show(_objGameDetails.languageid, "Please set NEW GOALKEEPER through OTHERS menu(Display Active Goalkeepr)!", "Active GoalKeeper", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                insertStatus = False
            End If

            If (_objActionDetails.TeamId IsNot Nothing) Or (_objActionDetails.EventId = 67 Or _objActionDetails.EventId = 68 Or _objActionDetails.EventId = 80) Then
                ''Validations..
                If (_bookingEvents.Contains(_objActionDetails.EventId)) Then 'Booking
                    insertStatus = IsBookingValid()
                ElseIf _objActionDetails.EventId = 22 Then 'Sub event
                    If (Not _objActionDetails.PlayerId.HasValue Or Not _objActionDetails.subPlayerId.HasValue) Then
                        _messageDialog.Show(_objGameDetails.languageid, "Please select Player In and Player Out for Substitution!", "Substitution", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        insertStatus = False
                    End If
                ElseIf _objActionDetails.EventId = 0 Then 'Booking and no options are selected
                    _messageDialog.Show(_objGameDetails.languageid, "Please select the right card from Booking options!", "Booking", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    insertStatus = False
                ElseIf _objActionDetails.IsEditMode And dgvPBP.SelectedRows.Count = 0 Then
                    'A1 selected a row to edit but A2 deleted that record.
                    _messageDialog.Show(_objGameDetails.languageid, "The edited row no longer exists. Please check!", "Edit Mode", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    UnHighlightActions()
                    ResetProperties()
                    insertStatus = False
                ElseIf (frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") And _objActionDetails.EventId = 63 And Not _objActionDetails.IsEditMode) Or (_betweenHalf And _objActionDetails.EventId = 63) Then
                    _messageDialog.Show(_objGameDetails.languageid, "Please select Booking/Substituition", "Booking/Sub", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                    insertStatus = False
                ElseIf Not _objActionDetails.IsEditMode Then
                    'Time Validation
                    If txtTime.Text.Trim() = ":" Then
                        _messageDialog.Show(_objGameDetails.languageid, "Clock time is not valid. Please check!", "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        insertStatus = False
                    End If
                    'TimePeriodValidation
                    Dim selectedRowsPeriod As Integer = _objGameDetails.CurrentPeriod
                    Dim timeElapsed = If(_objGameDetails.IsDefaultGameClock, CalculateTimeElapsedBefore(CInt(_objUtility.ConvertMinuteToSecond(txtTime.Text.Trim))), CInt(_objUtility.ConvertMinuteToSecond(txtTime.Text.Trim)))
                    If (selectedRowsPeriod = 2 And timeElapsed < 0) Or (selectedRowsPeriod = 3 And timeElapsed < 0) Or (selectedRowsPeriod = 3 And timeElapsed < 0) Then
                        _messageDialog.Show(_objGameDetails.languageid, "Clock time is not valid for the selected period. Please check!", "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        insertStatus = False
                    End If
                    'clock time should be greater
                    Dim clockTime As String()
                    Dim arrTime As String()
                    Dim minutesEntered As Integer = 0
                    Dim secondsEntered As Integer = 0
                    arrTime = txtTime.Text.Split(CChar(":"))
                    clockTime = frmMain.UdcRunningClock1.URCCurrentTime.Split(CChar(":"))
                    If arrTime(0).Trim() <> Nothing Then
                        minutesEntered = arrTime(0)
                    End If
                    If arrTime(1).Trim() <> Nothing Then
                        secondsEntered = arrTime(1)
                    Else
                        _messageDialog.Show(_objGameDetails.languageid, "Clock time is not valid. Please check!", "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        insertStatus = False
                    End If
                    If (CInt(clockTime(0)) < minutesEntered) Or ((minutesEntered = CInt(clockTime(0)) And CInt(clockTime(1)) < secondsEntered)) Then
                        _messageDialog.Show(_objGameDetails.languageid, "Current Clock Time should be greater than entered time!", "Clock", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                        insertStatus = False
                    End If
                End If

                'If Validated Successfully then insert ..
                If insertStatus Then
                    If AutoSelection(SaveAction()) Then
                        insertStatus = False
                    Else
                        dgvPBP.ClearSelection()
                        dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row
                    End If
                    _objActionDetails.IsEditMode = False
                End If
            End If

            Return insertStatus
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function AutoSelection(ByVal previousValues As Tuple(Of Integer, Integer?, Integer?)) As Boolean  ''(eventId As Integer, Optional subEvent? As Integer = Nothing) As Boolean
        Dim status As Boolean
        Dim autoSelectEventId() As Nullable(Of Int16) = {8, 16, 67} 'Foul should be followed by Freekick,OOB -67
        Try
            If autoSelectEventId.Contains(previousValues.Item1) And Not _objActionDetails.IsEditMode Then
                status = True
                _objActionDetails.EventId = If(eventIdForSubitems.ContainsKey(previousValues.Item3), eventIdForSubitems.Item(previousValues.Item3), 9)
                _objActionDetails.EventDescription = CStr(Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).FirstOrDefault(Function(c1) c1.Tag = _objActionDetails.EventId).Text)
                HighlightActionEvent()
                If previousValues.Item1 <> 67 Then 'select opposite Team
                    Dim buttonTeamHighlight As Button = If(previousValues.Item2 = _objGameDetails.HomeTeamID, btnAwayAction, btnHomeAction)
                    _objActionDetails.TeamId = IIf(buttonTeamHighlight.Name.Contains("btnHome"), _objGameDetails.HomeTeamID, _objGameDetails.AwayTeamID)
                    clsUtility.HighlightGoldButton(buttonTeamHighlight)
                End If
                DisplayActionInGrid()
            End If
        Catch ex As Exception
            Throw
        End Try
        Return status
    End Function

    Private Function CalculateTimeElapsedBefore(ByVal currentElapsedTime As Integer) As Integer
        Try
            Dim timeElapsed As New Dictionary(Of Integer, Integer)
            timeElapsed.Add(1, 0)
            timeElapsed.Add(2, 2700) 'FIRSTHALF
            timeElapsed.Add(3, 5400) 'SECONDHALF
            timeElapsed.Add(4, 6300) 'FIRSTEXTRA
            timeElapsed.Add(5, 7200)
            Return If(timeElapsed.ContainsKey(_objGameDetails.CurrentPeriod), currentElapsedTime - timeElapsed.Item(_objGameDetails.CurrentPeriod), currentElapsedTime)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function CalculateTimeElapsedBeforeMilliSec(ByVal currentElapsedTime As Integer) As Integer
        Try
            Dim timeElapsed As New Dictionary(Of Integer, Integer)
            timeElapsed.Add(1, 0)
            timeElapsed.Add(2, 2700000) 'FIRSTHALF
            timeElapsed.Add(3, 5400000) 'SECONDHALF
            timeElapsed.Add(4, 6300000) 'FIRSTEXTRA
            timeElapsed.Add(5, 7200000)
            Return If(timeElapsed.ContainsKey(_objGameDetails.CurrentPeriod), currentElapsedTime - timeElapsed.Item(_objGameDetails.CurrentPeriod), currentElapsedTime)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Display play by play data in Grid
    Public Sub PbpDataTreeLoad(ByVal pbpdata As DataTable, Optional fullLoad As Boolean = False, Optional fetchPrimaryInsertedData As Boolean = False)
        Try
            _objClsPbpTree.PbpTreeLoad(pbpdata, dgvPBP, fullLoad, True, fetchPrimaryInsertedData)
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub DisplayActionInGrid()
        Try
            If (_objActionDetails.IsEditMode = False) And (_objActionDetails.TeamId IsNot Nothing Or _objActionDetails.EventId IsNot Nothing) Then
                Dim pbptree As DataTable
                pbptree = dgvPBP.DataSource
                Dim playerId? As Integer = If(_objActionDetails.subPlayerId, _objActionDetails.PlayerId)

                Dim recordDelete() As DataRow = pbptree.Select("SEQUENCE_NUMBER Is Null")
                If recordDelete.Length > 0 Then
                    For Each dr As DataRow In recordDelete
                        dr.Delete()
                        dr.AcceptChanges()
                    Next
                End If

                Dim drPbpData As DataRow = pbptree.NewRow()
                drPbpData("EVENT_CODE_ID") = _objActionDetails.EventId
                drPbpData("EVENT") = _objActionDetails.EventDescription
                drPbpData("PERIOD") = _objGameDetails.CurrentPeriod
                If _objActionDetails.PlayerId.HasValue Then
                    drPbpData("By Player") = If(playerId, _objClsPlayerData.GetPlayerName(_playerTable, _objActionDetails.PlayerId), DBNull.Value)
                End If

                If _objActionDetails.TimeofEvent Is Nothing Then
                    If _milliSec = frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec Then
                        'Application.DoEvents() 'to avoid taking same time
                        '_objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Same Millisec :" & _milliSec, 1, 0)
                    End If
                    _objActionDetails.TimeofEvent = frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec
                    _milliSec = _objActionDetails.TimeofEvent
                End If
                If txtTime.Text.Replace(":", "").Trim <> "" And txtTime.Text.TrimStart <> _objActionDetails.TimeofEvent.Trim Then
                    _objActionDetails.TimeofEvent = txtTime.Text
                End If
                drPbpData("Time") = _objActionDetails.TimeofEvent
                lblTime.Visible = True
                txtTime.Visible = True
                txtTime.Text = _objActionDetails.TimeofEvent
                txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(8, CChar(" "))

                If _objActionDetails.TeamId IsNot Nothing Then
                    drPbpData("TEAM") = If(_objActionDetails.TeamId = _objGameDetails.HomeTeamID, _objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev)
                End If
                If _objActionDetails.EventId = 22 And _objActionDetails.subPlayerId.HasValue Then
                    drPbpData("Sub Type") = _objClsPlayerData.GetPlayerName(_playerTable, _objActionDetails.subPlayerId)
                End If

                If _objActionDetails.EventId = 8 And _objActionDetails.defPlayerId.HasValue Then
                    drPbpData("Sub Type") = _objClsPlayerData.GetPlayerName(_playerTable, _objActionDetails.defPlayerId)
                End If

                drPbpData("Type") = If(_objActionDetails.OutOfBoundsResDesc, DBNull.Value)
                drPbpData("OUT_OF_BOUNDS_RES_ID") = If(_objActionDetails.OutOfBoundsResId, DBNull.Value)

                pbptree.Rows.Add(drPbpData)
                pbptree.Rows(dgvPBP.RowCount - 1).AcceptChanges()

                If dgvPBP.RowCount > 0 Then
                    dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.RowCount - 1 'Scroll to the last row.
                    dgvPBP.Rows(dgvPBP.RowCount - 1).Selected = True 'Select the last row.
                End If
                HighlightGoalInGrid()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub HighlightGoalInGrid()
        Try
            'highlight goal event
            Dim goalEventRow = (From theRow As DataGridViewRow In dgvPBP.Rows _
                              Where _goalEvents.Contains(theRow.Cells("EVENT_CODE_ID").Value)
                              Select theRow.Index).ToList()
            For Each goalEventRowIndex In goalEventRow
                dgvPBP.Rows(goalEventRowIndex).DefaultCellStyle.BackColor = Color.Green
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub SetTeamColor(ByVal homeTeam As Boolean, ByVal teamColor As Color)
        Try
            Dim textForeColor As Color = clsUtility.GetTextColor(teamColor)
            Dim pnlTeam As Panel
            If homeTeam Then
                clsUtility.SetButtonColor(btnHomeAction, teamColor, textForeColor)
                clsUtility.SetButtonColor(btnHomePass, teamColor, textForeColor)
                UdcSoccerPlayerPositionsHome.USPButtonBackColor = teamColor
                UdcSoccerPlayerPositionsHome.ForeColor = textForeColor
                _objGameDetails.HomeTeamColor = teamColor
                pnlTeam = pnlHomeSub
            Else
                clsUtility.SetButtonColor(btnAwayAction, teamColor, textForeColor)
                clsUtility.SetButtonColor(btnAwayPass, teamColor, textForeColor)
                UdcSoccerPlayerPositionsAway.USPButtonBackColor = teamColor
                UdcSoccerPlayerPositionsAway.ForeColor = textForeColor
                _objGameDetails.VisitorTeamColor = teamColor
                pnlTeam = pnlAwaySub
            End If
            clsUtility.SetPanelButtonColor(pnlTeam, teamColor, textForeColor)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub EnableDisableControls(ByVal enable As Boolean, Optional ByVal notEndPeriod As Boolean = True)
        Try
            btnAwayAction.Enabled = enable
            btnHomeAction.Enabled = enable
            pnlEvents.Enabled = enable
            btnHomePass.Enabled = notEndPeriod
            btnAwayPass.Enabled = notEndPeriod
            btnSave.Enabled = enable
            DisablePanelControls(pnlAwaySub, enable)
            DisablePanelControls(pnlHomeSub, enable)
            DisablePanelControls(pnlEvents, notEndPeriod)
            DisablePanelControls(pnlSubEvents, notEndPeriod)
            DisablePanelControls(pnlGoalEvents, notEndPeriod)
            If Not notEndPeriod And Not _objGameDetails.IsEndGame Then
                'when the user gives end period, enable booking and Subs event
                btnBooking.Enabled = True
                btnSubstitute.Enabled = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Disable controls @End period/@start period
    Private Sub DisablePanelControls(ByVal panel As Control, ByVal enableType As Boolean)
        Try
            For Each ctrl As Button In panel.Controls.OfType(Of Button)()
                If ctrl.Text.Trim <> "" Then ctrl.Enabled = enableType
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'set the custom formation which was set by drag & drop
    Public Sub SetCustomFormation()
        Try
            _objActionDetails.CustomFormationHome = UdcSoccerPlayerPositionsHome.USPCustomFormationDetails
            _objActionDetails.CustomFormationAway = UdcSoccerPlayerPositionsAway.USPCustomFormationDetails
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'validate team
    Private Function IsTeamSelected() As Boolean
        Try
            If (_objActionDetails.TeamId Is Nothing) Then
                _messageDialog.Show(_objGameDetails.languageid, "Please choose Home/Away Control Action before selecting an Event.", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                'Previous Event higlighter
                HighlightActionEvent()
                Return False
            End If
            If _paredEvents.Contains(_objActionDetails.EventId) Then
                _messageDialog.Show(_objGameDetails.languageid, "Win / Lost cannot be replaced with other actions.", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Return False
            End If

            If grpBooking.Visible Then
                Dim subHighlight = Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).Where(Function(c1) c1.Tag = _objActionDetails.PlayerId)
                If subHighlight(0) IsNot Nothing Then
                    _objActionDetails.PlayerId = Nothing
                End If
                grpBooking.Visible = False
            ElseIf grpOutBounds.Visible Then
                grpOutBounds.Visible = False
            End If
            _objActionDetails.subPlayerId = Nothing
            _objActionDetails.BookingTypeId = Nothing
            _objActionDetails.OutOfBoundsResId = Nothing
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function
    ' If pass changed to Other actions, the highlight Control 
    Private Function PasstoOtherActions() As Boolean
        Try
            If _objActionDetails.EventId = 50 And _objActionDetails.IsEditMode = False Then
                UnhighlightActionButtons()
                AssignBtnTagToProperty(IIf(_objActionDetails.TeamId = _objGameDetails.HomeTeamID, btnHomeAction, btnAwayAction))
                DisplayActionInGrid()
            End If
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub HighlightSubPlayerButton(ByVal pnlSubs As Panel, ByVal playerId As Integer)
        Try
            For Each ctrl As Button In pnlSubs.Controls.OfType(Of Button)()
                If ctrl.Tag = playerId Then
                    clsUtility.HighlightGoldButton(ctrl)
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub SetDefaultValues()
        Try
            'Hotkeys Dictionary
            _eventHotkeys.Add(Keys.O, btnObstacle)
            _eventHotkeys.Add(Keys.H, btnHomeAction)
            _eventHotkeys.Add(Keys.J, btnAwayAction)
            _eventHotkeys.Add(Keys.G, btnHomePass)
            _eventHotkeys.Add(Keys.K, btnAwayPass)
            _eventHotkeys.Add(Keys.Space, btnOOB)
            _eventHotkeys.Add(Keys.F, btnFoul)
            _eventHotkeys.Add(Keys.T, btnThrowIn)
            _eventHotkeys.Add(Keys.B, btnFreeKick)
            _eventHotkeys.Add(Keys.N, btnShot)
            _eventHotkeys.Add(Keys.Y, btnGoalKick)
            _eventHotkeys.Add(Keys.U, btnCorner)
            _eventHotkeys.Add(Keys.Enter, btnSave)
            _eventHotkeys.Add(Keys.M, btnRefreshTime1)
            _eventHotkeys.Add(Keys.NumPad1, btnThrow)
            _eventHotkeys.Add(Keys.NumPad2, btnGoalKickOut)
            _eventHotkeys.Add(Keys.NumPad3, BtnCornerOut)
            _eventHotkeys.Add(Keys.D1, btnThrow)
            _eventHotkeys.Add(Keys.D2, btnGoalKickOut)
            _eventHotkeys.Add(Keys.D3, BtnCornerOut)
            _eventHotkeys.Add(Keys.I, btnOffSide)
            _eventHotkeys.Add(Keys.Escape, btnCancel)

            btnAwayAction.Text = _objGameDetails.AwayTeam + " " + btnAwayAction.Text
            btnHomeAction.Text = _objGameDetails.HomeTeam + " " + btnHomeAction.Text
            btnAwayPass.Text = _objGameDetails.AwayTeam + " " + btnAwayPass.Text
            btnHomePass.Text = _objGameDetails.HomeTeam + " " + btnHomePass.Text

            'get Team colors from DB and set the controls
            SetTeamColor(True, If(IsDBNull(_objGameDetails.HomeTeamColor), Color.WhiteSmoke, _objGameDetails.HomeTeamColor))
            SetTeamColor(False, If(IsDBNull(_objGameDetails.VisitorTeamColor), Color.WhiteSmoke, _objGameDetails.VisitorTeamColor))
            FillPlayersInUserControls()

            eventIdForSubitems.Add(1, 47) 'Throw In
            eventIdForSubitems.Add(2, 5)  ' Corner
            eventIdForSubitems.Add(3, 46) 'Goal Kick

            Dim defaultsGoalkeeperEvent =
                      (From defaultsGoalkeeper In _objGameDetails.RulesData.Tables(0).AsEnumerable()
                            Where defaultsGoalkeeper.Field(Of Int16?)("Listbox_Default_Selection") = 0 _
                              AndAlso defaultsGoalkeeper.Field(Of String)("PBPColumnName") = "OFFENSIVE_PLAYER_ID"
                        Select defaultsGoalkeeper.Field(Of Int16)("EventID")).ToList()

            For Each GoalkeeperEvent In defaultsGoalkeeperEvent
                _defaultsGoalkeeperEvents.Add(GoalkeeperEvent)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ControlsSetting()
        Try
            If (_objGameDetails.CurrentPeriod = 0) Then
                EnableDisableControls(False, True)  'if the game is in pregamestatus,then diable all controls till start period is provided/ End game is provided.
            ElseIf _objGameDetails.IsDelayed Or _objGameDetails.IsGameAbandoned Or _objGameDetails.IsEndGame Then
                EnableDisableControls(False, False)
            Else
                EnableDisableControls(True, (Not frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem"))) ''True - End period"
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Function IsDeleteAllowed(ByVal eventId As Integer, ByVal teamName As String, ByVal playerName As String) As Boolean
        Try
            Dim returnStatus As Boolean = True
            Dim messageText As String = Nothing
            Select Case eventId
                Case 11, 17, 28
                    messageText = "You are about to delete a Goal for " + teamName + ". This will change the score. Are you sure?"
                Case 2   'Booking
                    messageText = "You are about to delete a Booking for " + playerName + "(" + teamName + "). Are you sure?"
                Case 22  'Sub
                    messageText = "You are about to delete a Substitution for " + teamName + ". This will change the lineup. Are you sure?"
                Case 18  'Miss pen
                    messageText = "You are about to delete a Missed Penalty Shot for " + playerName + "(" + teamName + "). Are you sure?"
                Case 23, 24 'Lineups
                    returnStatus = False
                Case 13, 21, 34 'Start Half,game start, Game Delay
                    If (dgvPBP.Rows.Count - 1) >= (dgvPBP.SelectedRows(0).Index + 1) Or (_objGameDetails.CurrentPeriod = 1 And eventId = 21) Then
                        _messageDialog.Show(_objGameDetails.languageid, "Cannot delete this event", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        returnStatus = False
                    End If
                Case 72 '50-50 Ball Lost
                    _messageDialog.Show(_objGameDetails.languageid, "Cannot delete 50-50 Ball Lost events", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
                Case 74 'Aerial Duel Lost
                    _messageDialog.Show(_objGameDetails.languageid, "Cannot delete Aerial Duel Lost events", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
                Case 59 'Delayed, Delay Over
                    _messageDialog.Show(_objGameDetails.languageid, "Match Delayed can not be deleted. To resume the game please select 'Delay Over' in the 'Others' drop down", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
                Case 65 'Delay Over
                    _messageDialog.Show(_objGameDetails.languageid, "Match Delay Over can not be deleted", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
            End Select
            If messageText IsNot Nothing Then
                If _messageDialog.Show(_objGameDetails.languageid, messageText, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    returnStatus = False
                End If
            End If
            If _deleteKeyPressCnt >= 5 Then
                If _messageDialog.Show(_objGameDetails.languageid, "You have deleted 5 straight actions Click YES to continue.", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    returnStatus = False
                End If
            End If
            Return returnStatus
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub DefaultsGoalkeeper()
        If Not _objActionDetails.PlayerId.HasValue Then
            If (_defaultsGoalkeeperEvents.Contains(_objActionDetails.EventId)) Then
                If (_objActionDetails.TeamId = _objGameDetails.HomeTeamID) Then
                    _objActionDetails.PlayerId = UdcSoccerPlayerPositionsHome.btnGoalie.Tag
                    UdcSoccerPlayerPositionsHome.USPHighlightButton(_objActionDetails.PlayerId)
                Else
                    _objActionDetails.PlayerId = UdcSoccerPlayerPositionsAway.btnGoalie.Tag
                    UdcSoccerPlayerPositionsAway.USPHighlightButton(_objActionDetails.PlayerId)
                End If
            End If
        End If
    End Sub
    ' 'FPLRS-406 goal missing due to event "doubleclick & up arrow"  that highlights the previous event in grid 
    Private Sub HighlightPBPSelectedRow()
        Try
            Dim dgPbpDataSource As DataTable
            dgPbpDataSource = dgvPBP.DataSource
            Dim recordSelected = dgPbpDataSource.AsEnumerable().
                                 Select(Function(r, i) New With {.Row = r, .Index = i}).
                                 Where(Function(x) Not IsDBNull(x.Row.Field(Of Decimal?)("SEQUENCE_NUMBER")) AndAlso x.Row.Field(Of Decimal?)("SEQUENCE_NUMBER") = _pbpRowSelectedSqNo)
            If recordSelected.Any() Then
                'map the action based on SNO and select the action.
                Dim indexSelected As Integer = recordSelected.First().Index
                dgvPBP.Rows(indexSelected).Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region


End Class