﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class frmAbandon

#Region " Constants & Variables "
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private lsupport As New languagesupport
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Dim strmessage As String = "Please enter the Reason for Abandon"
#End Region

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOK.Text, Nothing, 1, 0)
            If txtReason.Text = "" Then
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            Else
                m_objGameDetails.Reason = txtReason.Text
            End If

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
            txtReason.Text = ""
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmAbandon_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAdandon", Nothing, 1, 0)

            If CInt(m_objclsGameDetails.languageid) <> 1 Then
                lblReason.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblReason.Text)
                btnOK.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnOK.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnCancel.Text)
                Me.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), Me.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strmessage)
            End If

            'm_objUtilAudit.AuditLog(Nothing, Nothing, Nothing, Nothing, Nothing, 0,1 )
            If m_objGameDetails.IsEditMode Then
                'cmbTeam.SelectedValue = m_objGameDetails.OffensiveTeamID
                txtReason.SelectedText = m_objGameDetails.Reason
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub txtReason_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReason.Leave
        Try
            If (txtReason.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtReason.Text, Nothing, 1, 0)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class