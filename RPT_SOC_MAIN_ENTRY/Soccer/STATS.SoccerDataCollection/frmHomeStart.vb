﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmHomeStart
' Author        : Shravani
' Created Date  : 07 Jan 2010
' Description   :
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmHomeStart

    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objHomeStart As clsHomeStart = clsHomeStart.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private lsupport As New languagesupport
    Dim strHomeStart As String = String.Empty
    Dim strHomeStartET As String = String.Empty
    Private message1 As String = "Enter correct home start!"
    Private message2 As String = "Enter correct home start (Extra Time)!"

    Private Sub frmHomeStart_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Icon = frmMain.Icon
            Me.CenterToParent()
            'LOAD HOMESTART COMBO
            cmbHomeStart.Items.Clear()
            cmbHomeStart.Items.Add("")
            cmbHomeStart.Items.Add("Left")
            cmbHomeStart.Items.Add("Right")
            cmbHomeStart.SelectedIndex = 0
            lblTeamName.Text = m_objGameDetails.HomeTeam.ToString()

            Dim DtHomeStart As DataSet
            DtHomeStart = m_objGeneral.GetTouchesHomeStartSide(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)

            '7Jan 2009 - Added by shirley
            'if the value is already available in DB, display the same.
            If m_objGameDetails.CurrentPeriod < 3 Then
                'lblHomeStartET.Enabled = False
                cmbHomeStartET.Visible = False
                cmbHomeStartET.Enabled = False
                lbltouchstartET.Visible = False
                lblHomeStart.Visible = True
                If DtHomeStart.Tables.Count > 0 Then
                    If DtHomeStart.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = False Then
                            If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim <> "" Then
                                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "L" Then
                                    cmbHomeStart.SelectedIndex = 1
                                ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "R" Then
                                    cmbHomeStart.SelectedIndex = 2
                                End If
                            Else
                                cmbHomeStart.SelectedIndex = 0
                            End If
                        End If

                    Else
                        cmbHomeStart.SelectedIndex = 0
                    End If

                Else
                    cmbHomeStart.SelectedIndex = 0
                End If
            Else
                lbltouchstartET.Visible = True
                lblHomeStart.Visible = False
                'lblHomeStartET.Enabled = True
                cmbHomeStart.Visible = False
                cmbHomeStartET.Visible = True
                cmbHomeStartET.Enabled = True
                cmbHomeStartET.Items.Clear()
                cmbHomeStartET.Items.Add("")
                cmbHomeStartET.Items.Add("Left")
                cmbHomeStartET.Items.Add("Right")
                cmbHomeStartET.SelectedIndex = 0
                If DtHomeStart.Tables.Count > 0 Then
                    If DtHomeStart.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = False Then
                            If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")).Trim <> "" Then
                                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = "L" Then
                                    cmbHomeStartET.SelectedIndex = 1
                                ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH_ET")) = "R" Then
                                    cmbHomeStartET.SelectedIndex = 2
                                End If
                            Else
                                cmbHomeStartET.SelectedIndex = 0
                            End If
                        End If
                        If IsDBNull(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = False Then
                            If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")).Trim <> "" Then
                                If CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "L" Then
                                    cmbHomeStart.SelectedIndex = 1
                                ElseIf CStr(DtHomeStart.Tables(0).Rows(0).Item("HOME_START_SIDE_TOUCH")) = "R" Then
                                    cmbHomeStart.SelectedIndex = 2
                                End If
                            Else
                                cmbHomeStart.SelectedIndex = 0
                            End If
                        End If
                    Else
                        cmbHomeStart.SelectedIndex = 0
                    End If

                Else
                    cmbHomeStart.SelectedIndex = 0
                End If
            End If

            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                lblTeamName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblTeamName.Text)
                message1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message1)
                message2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), message2)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If m_objGameDetails.CurrentPeriod > 2 Then
                If cmbHomeStartET.Text.Trim = "" Then
                    MessageDialog.Show(message2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
            Else
                If cmbHomeStart.Text.Trim = "" Then
                    MessageDialog.Show(message1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
            End If
            If cmbHomeStart.Text = "Right" Then
                strHomeStart = "R"
                m_objGameDetails.IsHomeTeamOnLeft = False
            ElseIf cmbHomeStart.Text = "Left" Then
                strHomeStart = "L"
                m_objGameDetails.IsHomeTeamOnLeft = True
            ElseIf cmbHomeStart.Text = "" Then
                strHomeStart = String.Empty
            End If

            If cmbHomeStartET.Text = "Right" Then
                strHomeStartET = "R"
                m_objGameDetails.IsHomeTeamOnLeft = False
            ElseIf cmbHomeStartET.Text = "Left" Then
                strHomeStartET = "L"
                m_objGameDetails.IsHomeTeamOnLeft = True
            ElseIf cmbHomeStartET.Text = "" Then
                strHomeStartET = String.Empty
            End If

            Me.DialogResult = Windows.Forms.DialogResult.OK
            m_objGameDetails.isHomeDirectionLeft = m_objGameDetails.IsHomeTeamOnLeft

            m_objHomeStart.InsertUpdateGameSetup(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID, strHomeStart, strHomeStartET)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class