﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.IO
Imports Microsoft.Win32
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmDisplaySubs
' Author        :
' Created Date  :
' Description   : This form acts as a container for the entry forms of all soccer modules
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmDisplaySubs

    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_intTeamID As Integer
    Private m_dsPlayersChecked As DataSet
    Private m_objUtilAudit As New clsAuditLog
    Private lsupport As New languagesupport

    Private Sub frmDisplaySubs_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAdandon", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)

            'ASSIGNING HOME AND AWAY TEAM NAMES TO THE RADIO BUTTONS
            radHomeTeam.Text = m_objGameDetails.HomeTeam
            radAwayTeam.Text = m_objGameDetails.AwayTeam

            m_dsPlayersChecked = m_objclsTeamSetup.RosterInfo.Copy()
            If m_dsPlayersChecked.Tables.Count > 1 Then
                m_dsPlayersChecked.Tables(0).Columns.Add("Checked")
                m_dsPlayersChecked.Tables(1).Columns.Add("Checked")
                radHomeTeam.Checked = True
                radAwayTeam.Checked = False

                lblTeamName.Text = radHomeTeam.Text
            End If
            'BY DEFAULT SELECT THE HOME TEAM RADIO BUTTON
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                radAwayTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radAwayTeam.Text)
                radHomeTeam.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), radHomeTeam.Text)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub radAwayTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAwayTeam.CheckedChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radHomeTeam.Text, Nothing, 1, 0)
            lblTeamName.Text = radAwayTeam.Text.Trim
            UpdatePlayersChecked()

            FetchTeamID(radAwayTeam.Text)
            FetchTeamLogo(m_intTeamID)

            LoadSubPlayers()
            RetriveCheckedPlayers()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub RetriveCheckedPlayers()
        Try
            Dim drs() As DataRow = m_dsPlayersChecked.Tables(GetTableName).Select("TEAM_ID =" & m_intTeamID & " and CHECKED ='Y'")
            If drs.Length > 0 Then
                For i As Integer = 0 To ChkLstSubs.Items.Count - 1
                    For Each dr As DataRow In drs
                        If DirectCast(ChkLstSubs.Items(i), System.Data.DataRowView).Item(0).ToString = dr.Item("Player_id").ToString Then
                            ChkLstSubs.SetItemChecked(i, True)
                        End If
                    Next
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radHomeTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radHomeTeam.CheckedChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.RadioButtonClicked, radHomeTeam.Text, Nothing, 1, 0)
            lblTeamName.Text = radHomeTeam.Text.Trim
            UpdatePlayersChecked()

            FetchTeamID(radHomeTeam.Text)
            FetchTeamLogo(m_intTeamID)
            LoadSubPlayers()
            RetriveCheckedPlayers()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub UpdatePlayersChecked()
        Try
            Dim drs() As DataRow
            UpdateRostersLineuptoNull()
            For intCheckedItems As Integer = 0 To ChkLstSubs.CheckedItems.Count - 1
                drs = m_dsPlayersChecked.Tables(GetTableName).Select("TEAM_ID = " & m_intTeamID & "   AND PLAYER_ID = '" & DirectCast(ChkLstSubs.CheckedItems.Item(intCheckedItems), System.Data.DataRowView).Item("Player_id").ToString & "'")
                If drs.Length > 0 Then
                    drs(0).BeginEdit()
                    drs(0).Item("CHECKED") = "Y"
                    drs(0).EndEdit()
                End If
            Next
            m_dsPlayersChecked.AcceptChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetTableName() As String
        Try
            Dim drs() As DataRow
            drs = m_dsPlayersChecked.Tables(0).Select("TEAM_ID = " & m_intTeamID & "")
            If drs.Length > 0 Then
                Return "Home"
            Else
                Return "Away"
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub UpdateRostersLineuptoNull()
        Try
            Dim dr() As DataRow
            Dim StrTablename As String = GetTableName()
            If m_dsPlayersChecked.Tables.Count > 0 Then
                If m_dsPlayersChecked.Tables(StrTablename).Rows.Count > 0 Then
                    dr = m_dsPlayersChecked.Tables(StrTablename).Select("CHECKED IS NOT NULL")
                    If dr.Length > 0 Then
                        For intRow As Integer = 0 To dr.Length - 1
                            dr(intRow).BeginEdit()
                            dr(intRow).Item("CHECKED") = System.DBNull.Value
                            dr(intRow).EndEdit()
                        Next
                        m_dsPlayersChecked.Tables(StrTablename).AcceptChanges()
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' FETCHING LOGO FOR SELECTED TEAM
    ''' </summary>
    ''' <param name="TeamID"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamLogo(ByVal TeamID As Integer)
        Try
            Dim v_memLogo As MemoryStream
            picTeamLogo.Image = Nothing
            v_memLogo = m_objGeneral.GetTeamLogo(m_objGameDetails.LeagueID, TeamID)
            If v_memLogo IsNot Nothing Then
                picTeamLogo.Image = New Bitmap(v_memLogo)
                v_memLogo = Nothing
            Else  'TOSOCRS-338
                picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHING TEAM ID BY PASSING TEAM NAME AS PARAMETER
    ''' </summary>
    ''' <param name="Teamname"></param>
    ''' <remarks></remarks>
    Private Sub FetchTeamID(ByVal Teamname As String)
        Try
            If Teamname = m_objGameDetails.HomeTeam Then
                m_intTeamID = m_objGameDetails.HomeTeamID
            Else
                m_intTeamID = m_objGameDetails.AwayTeamID
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSubPlayers()
        Try
            Dim drs() As DataRow
            Dim DsSubPlayers As DataSet
            Dim dsTemp As DataSet
            ChkLstSubs.DataSource = Nothing
            ChkLstSubs.Items.Clear()

            If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then
                DsSubPlayers = m_objclsTeamSetup.RosterInfo.Copy()
                dsTemp = DsSubPlayers.Clone()
                For intTableCount As Integer = 2 To DsSubPlayers.Tables.Count - 1
                    'get only substituted players
                    drs = DsSubPlayers.Tables(intTableCount).Select("TEAM_ID = " & m_intTeamID & " AND STARTING_POSITION = 0")
                    If drs.Length > 0 Then
                        For Each DR As DataRow In drs
                            dsTemp.Tables(intTableCount).ImportRow(DR)
                        Next
                        'binding the substituted players
                        ChkLstSubs.DataSource = dsTemp.Tables(intTableCount)
                        ChkLstSubs.DisplayMember = dsTemp.Tables(intTableCount).Columns("PLAYER_ID").ToString
                        ChkLstSubs.DisplayMember = dsTemp.Tables(intTableCount).Columns("PLAYER").ToString
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, Nothing, 1, 0)
            Dim drs() As DataRow
            Dim drsChecked() As DataRow
            If m_objclsTeamSetup.RosterInfo.Tables.Count > 0 Then

                UpdatePlayersChecked()

                For intTablecount As Integer = 0 To m_dsPlayersChecked.Tables.Count - 3
                    drsChecked = m_dsPlayersChecked.Tables(intTablecount).Select("CHECKED ='Y'")
                    If drsChecked.Length > 0 Then
                        For Each DR As DataRow In drsChecked
                            For inttablecnt As Integer = 2 To m_objclsTeamSetup.RosterInfo.Tables.Count - 1
                                drs = m_objclsTeamSetup.RosterInfo.Tables(inttablecnt).Select("TEAM_ID = " & CInt(DR.Item("TEAM_ID")) & " AND STARTING_POSITION = 0  AND PLAYER_ID = '" & DR.Item("Player_id").ToString & "'")
                                If drs.Length > 0 Then
                                    drs(0).BeginEdit()
                                    Dim drLineups() As DataRow = m_objclsTeamSetup.RosterInfo.Tables(inttablecnt).Select("TEAM_ID = " & CInt(DR.Item("TEAM_ID")) & " AND STARTING_POSITION > 30", "STARTING_POSITION DESC")
                                    If drLineups.Length > 0 Then
                                        drs(0).Item("STARTING_POSITION") = CInt(drLineups(0).Item("STARTING_POSITION")) + 1
                                    Else
                                        drs(0).Item("STARTING_POSITION") = 31
                                    End If
                                    drs(0).EndEdit()
                                End If
                            Next
                        Next
                    End If
                Next

                m_objclsTeamSetup.RosterInfo.AcceptChanges()
            End If
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

End Class