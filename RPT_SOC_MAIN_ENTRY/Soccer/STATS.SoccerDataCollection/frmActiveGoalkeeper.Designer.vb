﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActiveGoalkeeper
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ChkLstSubs = New System.Windows.Forms.CheckedListBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblTeamName = New System.Windows.Forms.Label()
        Me.sstAddGame = New System.Windows.Forms.StatusStrip()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.picTeamLogo = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.radAwayTeam = New System.Windows.Forms.RadioButton()
        Me.radHomeTeam = New System.Windows.Forms.RadioButton()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.txtTimeMod2 = New System.Windows.Forms.MaskedTextBox()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.txtTime = New System.Windows.Forms.MaskedTextBox()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChkLstSubs
        '
        Me.ChkLstSubs.FormattingEnabled = True
        Me.ChkLstSubs.Location = New System.Drawing.Point(17, 112)
        Me.ChkLstSubs.Name = "ChkLstSubs"
        Me.ChkLstSubs.Size = New System.Drawing.Size(337, 184)
        Me.ChkLstSubs.TabIndex = 373
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(199, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 365
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'lblTeamName
        '
        Me.lblTeamName.AutoSize = True
        Me.lblTeamName.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeamName.ForeColor = System.Drawing.Color.White
        Me.lblTeamName.Location = New System.Drawing.Point(17, 12)
        Me.lblTeamName.Name = "lblTeamName"
        Me.lblTeamName.Size = New System.Drawing.Size(115, 25)
        Me.lblTeamName.TabIndex = 14
        Me.lblTeamName.Text = "Team name"
        '
        'sstAddGame
        '
        Me.sstAddGame.AutoSize = False
        Me.sstAddGame.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstAddGame.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstAddGame.Location = New System.Drawing.Point(0, 374)
        Me.sstAddGame.Name = "sstAddGame"
        Me.sstAddGame.Size = New System.Drawing.Size(366, 18)
        Me.sstAddGame.TabIndex = 372
        Me.sstAddGame.Text = "StatusStrip1"
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(280, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 361
        Me.btnClose.Text = "Cancel"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'picTeamLogo
        '
        Me.picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picTeamLogo.Location = New System.Drawing.Point(17, 50)
        Me.picTeamLogo.Name = "picTeamLogo"
        Me.picTeamLogo.Size = New System.Drawing.Size(45, 45)
        Me.picTeamLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picTeamLogo.TabIndex = 370
        Me.picTeamLogo.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Panel2.Controls.Add(Me.radAwayTeam)
        Me.Panel2.Controls.Add(Me.lblTeamName)
        Me.Panel2.Controls.Add(Me.radHomeTeam)
        Me.Panel2.Location = New System.Drawing.Point(76, 38)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(294, 61)
        Me.Panel2.TabIndex = 369
        '
        'radAwayTeam
        '
        Me.radAwayTeam.ForeColor = System.Drawing.Color.White
        Me.radAwayTeam.Location = New System.Drawing.Point(182, 32)
        Me.radAwayTeam.Name = "radAwayTeam"
        Me.radAwayTeam.Size = New System.Drawing.Size(106, 17)
        Me.radAwayTeam.TabIndex = 341
        Me.radAwayTeam.TabStop = True
        Me.radAwayTeam.Text = "Away team name"
        Me.radAwayTeam.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.radAwayTeam.UseVisualStyleBackColor = True
        '
        'radHomeTeam
        '
        Me.radHomeTeam.ForeColor = System.Drawing.Color.White
        Me.radHomeTeam.Location = New System.Drawing.Point(182, 9)
        Me.radHomeTeam.Name = "radHomeTeam"
        Me.radHomeTeam.Size = New System.Drawing.Size(108, 17)
        Me.radHomeTeam.TabIndex = 340
        Me.radHomeTeam.TabStop = True
        Me.radHomeTeam.Text = "Home team name"
        Me.radHomeTeam.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.radHomeTeam.UseVisualStyleBackColor = True
        '
        'PictureBox6
        '
        Me.PictureBox6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox6.Location = New System.Drawing.Point(0, 19)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(385, 18)
        Me.PictureBox6.TabIndex = 368
        Me.PictureBox6.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.PictureBox7.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(569, 20)
        Me.PictureBox7.TabIndex = 367
        Me.PictureBox7.TabStop = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnSave)
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(-1, 335)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(381, 40)
        Me.picButtonBar.TabIndex = 374
        '
        'txtTimeMod2
        '
        Me.txtTimeMod2.Location = New System.Drawing.Point(80, 306)
        Me.txtTimeMod2.Mask = "000"
        Me.txtTimeMod2.Name = "txtTimeMod2"
        Me.txtTimeMod2.Size = New System.Drawing.Size(30, 20)
        Me.txtTimeMod2.TabIndex = 376
        Me.txtTimeMod2.Visible = False
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.lblTime.Location = New System.Drawing.Point(14, 309)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(36, 15)
        Me.lblTime.TabIndex = 375
        Me.lblTime.Text = "Time :"
        Me.lblTime.Visible = False
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(80, 306)
        Me.txtTime.Mask = "000:00"
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(47, 20)
        Me.txtTime.TabIndex = 377
        '
        'frmActiveGoalkeeper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(366, 392)
        Me.Controls.Add(Me.txtTime)
        Me.Controls.Add(Me.txtTimeMod2)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.ChkLstSubs)
        Me.Controls.Add(Me.sstAddGame)
        Me.Controls.Add(Me.picTeamLogo)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.picButtonBar)
        Me.Name = "frmActiveGoalkeeper"
        Me.Text = "Active Goalkeeper"
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChkLstSubs As System.Windows.Forms.CheckedListBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblTeamName As System.Windows.Forms.Label
    Friend WithEvents sstAddGame As System.Windows.Forms.StatusStrip
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents picTeamLogo As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents radAwayTeam As System.Windows.Forms.RadioButton
    Friend WithEvents radHomeTeam As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents txtTimeMod2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents txtTime As System.Windows.Forms.MaskedTextBox
End Class
