﻿#Region "Options"
Option Strict On
Option Explicit On
#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : clsGameDetails
' Author        : Ravi Krishna
' Created Date  : 24 April 2009
' Description   : This class contains information specific to the Games assigned for the user.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class clsGameDetails

#Region "Constants & Variables"

    Shared m_myInstance As clsGameDetails
    Private m_intGameCode As Integer
    Private m_intGameCodeMod2 As Integer = 0
    Private m_dtmGameDate As Date
    Private m_strAwayTeam As String = String.Empty
    Private m_strHomeTeam As String = String.Empty
    Private m_strAwayTeamAbbrev As String = String.Empty
    Private m_strHomeTeamAbbrev As String = String.Empty
    Private m_intAwayTeamID As Integer
    Private m_intHomeTeamID As Integer
    Private m_intLeagueID As Integer
    Private m_strLeagueName As String = String.Empty
    Private m_intFeedNumber As Integer
    Private m_intCoverageLevel As Integer
    Private m_intModuleID As Integer
    Private m_intClientID As Integer
    Private m_strReporterRole As String
    Private m_intFieldID As Integer
    Private m_strFieldName As String = String.Empty
    Private m_intCurrentPeriod As Integer
    Private m_intSerialNo As Integer
    Private m_strReporterRoleDisplay As String = String.Empty
    Private m_dsGamesetup As DataSet
    Private m_intAwayScore As Integer = 0
    Private m_intHomeScore As Integer = 0
    Private m_intCurrentScore As Integer = 0
    Private m_dsPBP As DataSet
    Private m_intPeriod As Integer
    Private m_dtDelayTime As Date
    Private m_intOffensiveTeamID As Integer
    Private m_intTTeamID As Integer

    Private m_intDefensiveTeamID As Integer
    Private m_intOffensiveCoachID As Integer
    Private m_intDefensiveCoachID As Integer
    Private m_strOffensiveTeam As String
    Private m_strDefensiveTeam As String
    Private m_blnEditMode As Boolean = False
    Private m_blnHomeDirection As Boolean = True
    Private m_blnIsHomeTeamOnLeft As Boolean = True

    Private m_blnIsHomeTeamOnLeftSwitch As Boolean = True
    Private m_strSortOrder As String = "NAME"
    Private m_blnIsEditMode As Boolean = False
    Private m_blnIsLineupChk As Boolean = False
    Private m_blnIsInsertMode As Boolean = False
    Private m_blnIsReplaceMode As Boolean = False
    Private m_blnEditFromMain As Boolean = False
    Private m_blnMatchDelayed As Boolean = False
    Private m_blnSwitchSide As Boolean = False
    Private m_blnIsRestartGame As Boolean = False
    Private m_blnIsReporterRemoved As Boolean = False
    Private m_blnIsHalfTimeSwap As Boolean = False
    Private m_dsRestartData As DataSet
    Private m_StrModuleName As String
    Private m_IsListBoxLoaded As Boolean = False
    Private m_clrHomeTeamColor As System.Drawing.Color = Color.WhiteSmoke
    Private m_clrVisitorTeamColor As System.Drawing.Color = Color.WhiteSmoke
    Private m_IsEndGame As Boolean = False
    Private m_blnIsContinuationEvent As Boolean = False
    Private m_blnIsStartLineupsInserted As Boolean = False
    Private m_strComments As String
    Private m_blnIsDefaultGameClock As Boolean = True
    Private m_clrHomeTeamFormation As String = String.Empty
    Private m_clrAwayTeamFormation As String = String.Empty
    Private m_IsPairedEvent As Boolean = False
    Private m_intExplTime As Integer = 0
    Private m_intExplReason As Integer = 0
    Private m_Channel As String = String.Empty
    Private m_Desk As String = String.Empty
    Private m_blnXMLSort As Boolean = False
    Private m_blnIsDemoGameSelected As Boolean = False
    Private m_blnIsPrimaryReporterDown As Boolean = False
    Private m_blnAssistersLastEntryStatus As Boolean = True
    Private m_strReason As String = String.Empty
    Private m_dtSysDateDiff As TimeSpan
    Private m_strTimeZone As String = String.Empty
    Private m_intLastSelGameCode As Integer
    Private m_dsTouchesData As New DataSet
    Private m_RulesData As DataSet
    Private m_blnFoulAssociated As Boolean
    Private m_DecFouledSNO As Decimal
    Private m_intPenaltyShootOutAwayScore As Integer
    Private m_intPenaltyShootOutHomeScore As Integer
    Private m_intCommentsTime As Integer = 0
    Private m_blnIsScoreWindow As Boolean = False
    Private m_blnIsDetailedWindow As Boolean = False
    Private m_intActiveGK As Integer
    Private m_blnIsMultipleFeedsAssigned As Boolean = False
    Private m_IsPrimaryReporter As Boolean = False
    Private m_IsInjuryTimeEdit As Boolean = False

    'SportVU
    Private m_OpticalData As DataSet
    Private m_OpticalEvent As Integer
    Private m_OpticalPBPSaved As DataSet
    Private m_OpticalTouchEvent As Integer
    Private m_IsOpticalFeedClosed As Boolean = False
    Private m_IsSportVUAvailable As Boolean = False
    Private m_OpticalTouchSaved As DataSet
    Private m_OpticalTouchData As DataSet
    Private m_langid As Integer
    Private m_IsSportDataIgnore As Boolean = False
    Private m_intFieldWidth As Integer
    Private m_intFieldHeight As Integer
    Private m_dsEditedEvents As DataSet
    Private m_dsPlayers As DataSet
    Private m_FormState As m_enumFormState
    Private m_dsReporterGames As DataSet
    Private m_PredefinedCommentID As Integer
    Private m_intCommentLanguageID As Integer
    Private m_strDirectory As String
    Private m_strAuditFileName As String
    Private m_blnMinGoalEntry As Boolean = False
    'start 26-11-13
    Private m_donotshow As String = "Y"
    'end 26-11-13
    Private m_FromDate As String
    Private m_ToDate As String
    Private m_strUserModeDemo As Boolean
    Private m_InjuryEnteredTime As String
    Private m_StartingLineupsEditUid As Integer = 0
    Private m_StartingLineups As DataSet
    Private m_activateGame As Boolean = False
    Private m_blnIsETHomeTeamOnLeft As Boolean = True
    Private m_blnGameAbandoned As Boolean = False
    Private _multilingual As New Dictionary(Of String, String)
    Private m_intHomeTeamCaptainID As Integer
    Private m_intAwayTeamCaptainID As Integer
    Private m_blnIsTouchFinalized As Boolean = False
    Private m_AppPath As String



    Public Enum m_enumFormState
        MainForm
        PBPForm
        ActionsForm
    End Enum

#Region "Event Code Constants"

    Public Const ONTARGET As Integer = 20 'Shot on Goal
    Public Const OFFTARGET As Integer = 19 'Shot
    Public Const CORNER As Integer = 5
    Public Const CROSS As Integer = 6
    Public Const OFFSIDE As Integer = 16
    Public Const SUBSTITUTE As Integer = 22

    Public Const GOAL As Integer = 11
    Public Const PENALTY As Integer = 17
    Public Const OWNGOAL As Integer = 28

    Public Const FREEKICK As Integer = 9 'Not yet decided, hence assigning -1 - Ravi Krishna - as on 01/06

    Public Const FOUL As Integer = 8

    'Public Const FREE_KICK As Integer = 8

    Public Const LEFT_FLANK_ATTACK As Integer = 42
    Public Const CENTRAL_ATTACK As Integer = 43
    Public Const RIGHT_FLANK_ATTACK As Integer = 44
    Public Const LONG_BALL_ATTACK As Integer = 45

    Public Const GOAL_KICK As Integer = 46

    Public Const MISSED_PENALTY As Integer = 18

    Public Const GOALIE_CHANGE As Integer = 29

    Public Const THROW_IN As Integer = 47
    Public Const PASS As Integer = 50
    Public Const RUN_WITH_BALL As Integer = 51
    Public Const TACKLE As Integer = 52
    Public Const DEFLECTION As Integer = 53

    Public Const YELLOWCARD As Integer = 2 'EVENT DESC FOR YELLOW CARD IS CAUTION
    Public Const REDCARD As Integer = 7    'EVENT DESC FOR RED CARD IS EXPULSION
    Public Const YELLOWRED As Integer = 0  'NOT YET DECIDED

    Public Const DEFEN_ACTION As Integer = 49

    Public Const GAMESTART As Integer = 34
    Public Const STARTPERIOD As Integer = 21
    Public Const ENDPERIOD As Integer = 13
    Public Const ENDGAME As Integer = 10

    Public Const HomeStartingLineups As Integer = 23
    Public Const AwatStartingLineups As Integer = 24
    Public Const COMMENTS As Integer = 4
    Public Const SHOOTOUT_SAVE As Integer = 31
    Public Const SHOOTOUT_GOAL As Integer = 30
    Public Const SHOOTOUT_MISSED As Integer = 41
    'Manager Expulsion
    Public Const MANAGEREXPULSION As Integer = 48

    'Clock Events
    Public Const CLOCK_START As Integer = 54
    Public Const CLOCK_STOP As Integer = 55
    Public Const CLOCK_INC As Integer = 56
    Public Const CLOCK_DEC As Integer = 57

    'DELAY/ABANDONED
    Public Const DELAYED As Integer = 59
    Public Const DELAY_OVER As Integer = 65 'RM 8695
    Public Const INJURY_TIME As Integer = 66
    Public Const ABANDONED As Integer = 58
    Public Const GAMEINTERRUPTION As Integer = 80

    Public Const YD10 As Integer = 60

    Public Const Backpass As Integer = 61
    Public Const TIME As Integer = 62

#End Region

#End Region

#Region " Shared methods "

    Public Shared Function GetInstance() As clsGameDetails
        If m_myInstance Is Nothing Then
            m_myInstance = New clsGameDetails
        End If
        Return m_myInstance
    End Function

#End Region

#Region "Public Properties"

    Public Property GameCode() As Integer
        Get
            Return m_intGameCode
        End Get
        Set(ByVal value As Integer)
            m_intGameCode = value
        End Set
    End Property

    Public Property GameCodeMod2() As Integer
        Get
            Return m_intGameCodeMod2
        End Get
        Set(ByVal value As Integer)
            m_intGameCodeMod2 = value
        End Set
    End Property

    Public Property GameDate() As Date
        Get
            Return m_dtmGameDate
        End Get
        Set(ByVal value As Date)
            m_dtmGameDate = value
        End Set
    End Property

    Public Property AwayTeam() As String
        Get
            Return m_strAwayTeam
        End Get
        Set(ByVal value As String)
            m_strAwayTeam = value
        End Set
    End Property

    Public Property InjuryEnteredTime() As String
        Get
            Return m_InjuryEnteredTime
        End Get
        Set(ByVal value As String)
            m_InjuryEnteredTime = value
        End Set
    End Property

    Public Property HomeTeam() As String
        Get
            Return m_strHomeTeam
        End Get
        Set(ByVal value As String)
            m_strHomeTeam = value
        End Set
    End Property

    Public Property EditFromMain() As Boolean
        Get
            Return m_blnEditFromMain
        End Get
        Set(ByVal value As Boolean)
            m_blnEditFromMain = value
        End Set
    End Property

    Public Property isSwitchSide() As Boolean
        Get
            Return m_blnSwitchSide
        End Get
        Set(ByVal value As Boolean)
            m_blnSwitchSide = value
        End Set
    End Property

    Public Property AllowMinGoalEntry() As Boolean
        Get
            Return m_blnMinGoalEntry
        End Get
        Set(ByVal value As Boolean)
            m_blnMinGoalEntry = value
        End Set
    End Property

    Public Property AwayTeamAbbrev() As String
        Get
            Return m_strAwayTeamAbbrev
        End Get
        Set(ByVal value As String)
            m_strAwayTeamAbbrev = value
        End Set
    End Property

    Public Property HomeTeamAbbrev() As String
        Get
            Return m_strHomeTeamAbbrev
        End Get
        Set(ByVal value As String)
            m_strHomeTeamAbbrev = value
        End Set
    End Property

    Public Property AwayTeamID() As Integer
        Get
            Return m_intAwayTeamID
        End Get
        Set(ByVal value As Integer)
            m_intAwayTeamID = value
        End Set
    End Property

    Public Property HomeTeamID() As Integer
        Get
            Return m_intHomeTeamID
        End Get
        Set(ByVal value As Integer)
            m_intHomeTeamID = value
        End Set
    End Property

    Public Property LeagueID() As Integer
        Get
            Return m_intLeagueID
        End Get
        Set(ByVal value As Integer)
            m_intLeagueID = value
        End Set
    End Property

    Public Property LeagueName() As String
        Get
            Return m_strLeagueName
        End Get
        Set(ByVal value As String)
            m_strLeagueName = value
        End Set
    End Property

    Public Property FeedNumber() As Integer
        Get
            Return m_intFeedNumber
        End Get
        Set(ByVal value As Integer)
            m_intFeedNumber = value
        End Set
    End Property

    Public Property CoverageLevel() As Integer
        Get
            Return m_intCoverageLevel
        End Get
        Set(ByVal value As Integer)
            m_intCoverageLevel = value
        End Set
    End Property

    Public Property ModuleID() As Integer
        Get
            Return m_intModuleID
        End Get
        Set(ByVal value As Integer)
            m_intModuleID = value
        End Set
    End Property

    Public Property ClientID() As Integer
        Get
            Return m_intClientID
        End Get
        Set(ByVal value As Integer)
            m_intClientID = value
        End Set
    End Property

    Public Property ReporterRole() As String
        Get
            Return m_strReporterRole
        End Get
        Set(ByVal value As String)
            m_strReporterRole = value
        End Set
    End Property

    Public Property FieldID() As Integer
        Get
            Return m_intFieldID
        End Get
        Set(ByVal value As Integer)
            m_intFieldID = value
        End Set
    End Property

    Public Property FieldName() As String
        Get
            Return m_strFieldName
        End Get
        Set(ByVal value As String)
            m_strFieldName = value
        End Set
    End Property

    Public Property CurrentPeriod() As Int32
        Get
            Return m_intCurrentPeriod
        End Get
        Set(ByVal value As Int32)
            m_intCurrentPeriod = value
        End Set
    End Property

    Public Property SerialNo() As Int32
        Get
            Return m_intSerialNo
        End Get
        Set(ByVal value As Int32)
            m_intSerialNo = value
        End Set
    End Property

    Public Property ReporterRoleDisplay() As String
        Get
            Return m_strReporterRoleDisplay
        End Get
        Set(ByVal value As String)
            m_strReporterRoleDisplay = value
        End Set
    End Property

    Public Property GameSetup() As DataSet
        Get
            Return m_dsGamesetup
        End Get
        Set(ByVal value As DataSet)
            m_dsGamesetup = value
        End Set
    End Property

    Public Property AwayScore() As Int32
        Get
            Return m_intAwayScore
        End Get
        Set(ByVal value As Int32)
            m_intAwayScore = value
        End Set
    End Property

    Public Property HomeScore() As Int32
        Get
            Return m_intHomeScore
        End Get
        Set(ByVal value As Int32)
            m_intHomeScore = value
        End Set
    End Property

    Public Property CurrentScore() As Int32
        Get
            Return m_intCurrentScore
        End Get
        Set(ByVal value As Int32)
            m_intCurrentScore = value
        End Set
    End Property

    Public Property PBP() As DataSet
        Get
            Return m_dsPBP
        End Get
        Set(ByVal value As DataSet)
            m_dsPBP = value
        End Set
    End Property

    Public Property Period() As Int32
        Get
            Return m_intPeriod
        End Get
        Set(ByVal value As Int32)
            m_intPeriod = value
        End Set
    End Property

    Public Property DelayTime() As Date
        Get
            Return m_dtDelayTime
        End Get
        Set(ByVal value As Date)
            m_dtDelayTime = value
        End Set
    End Property
    Public Property OffensiveTeamID() As Integer
        Get
            Return m_intOffensiveTeamID
        End Get
        Set(ByVal value As Integer)
            m_intOffensiveTeamID = value
        End Set
    End Property
    Public Property TouchesTeamID() As Integer
        Get
            Return m_intTTeamID
        End Get
        Set(ByVal value As Integer)
            m_intTTeamID = value
        End Set
    End Property

    Public Property OffensiveCoachID() As Integer
        Get
            Return m_intOffensiveCoachID
        End Get
        Set(ByVal value As Integer)
            m_intOffensiveCoachID = value
        End Set
    End Property

    Public Property ActiveGK() As Integer
        Get
            Return m_intActiveGK
        End Get
        Set(ByVal value As Integer)
            m_intActiveGK = value
        End Set
    End Property

    Public Property DefensiveCoachID() As Integer
        Get
            Return m_intDefensiveCoachID
        End Get
        Set(ByVal value As Integer)
            m_intDefensiveCoachID = value
        End Set
    End Property

    Public Property DefensiveTeamID() As Integer
        Get
            Return m_intDefensiveTeamID
        End Get
        Set(ByVal value As Integer)
            m_intDefensiveTeamID = value
        End Set
    End Property

    Public Property OffensiveTeam() As String
        Get
            Return m_strOffensiveTeam
        End Get
        Set(ByVal value As String)
            m_strOffensiveTeam = value
        End Set
    End Property

    Public Property DefensiveTeam() As String
        Get
            Return m_strDefensiveTeam
        End Get
        Set(ByVal value As String)
            m_strDefensiveTeam = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return m_blnEditMode
        End Get
        Set(ByVal value As Boolean)
            m_blnEditMode = value
        End Set
    End Property

    Public Property IsDelayed() As Boolean
        Get
            Return m_blnMatchDelayed
        End Get
        Set(ByVal value As Boolean)
            m_blnMatchDelayed = value
        End Set
    End Property
    Public Property IsGameAbandoned() As Boolean
        Get
            Return m_blnGameAbandoned
        End Get
        Set(ByVal value As Boolean)
            m_blnGameAbandoned = value
        End Set
    End Property

    Public Property isHomeDirectionLeft() As Boolean
        Get
            Return m_blnHomeDirection
        End Get
        Set(ByVal value As Boolean)
            m_blnHomeDirection = value
        End Set
    End Property

    Public Property IsHomeTeamOnLeft() As Boolean
        Get
            Return m_blnIsHomeTeamOnLeft
        End Get
        Set(ByVal value As Boolean)
            m_blnIsHomeTeamOnLeft = value
        End Set
    End Property

    Public Property IsHomeTeamOnLeftSwitch() As Boolean
        Get
            Return m_blnIsHomeTeamOnLeftSwitch
        End Get
        Set(ByVal value As Boolean)
            m_blnIsHomeTeamOnLeftSwitch = value
        End Set
    End Property

    Public Property SortOrder() As String
        Get
            Return m_strSortOrder
        End Get
        Set(ByVal value As String)
            m_strSortOrder = value
        End Set
    End Property

    Public Property IsEditMode() As Boolean
        Get
            Return m_blnIsEditMode
        End Get
        Set(ByVal value As Boolean)
            m_blnIsEditMode = value
        End Set
    End Property

    Public Property IsLineupChecked() As Boolean
        Get
            Return m_blnIsLineupChk
        End Get
        Set(ByVal value As Boolean)
            m_blnIsLineupChk = value
        End Set
    End Property

    'Public Property IsEndGame() As Boolean
    '    Get
    '        Return m_blnIsEndGame
    '    End Get
    '    Set(ByVal value As Boolean)
    '        m_blnIsEndGame = value
    '    End Set
    'End Property

    Public Property IsInsertMode() As Boolean
        Get
            Return m_blnIsInsertMode
        End Get
        Set(ByVal value As Boolean)
            m_blnIsInsertMode = value
        End Set
    End Property

    Public Property IsReplaceMode() As Boolean
        Get
            Return m_blnIsReplaceMode
        End Get
        Set(ByVal value As Boolean)
            m_blnIsReplaceMode = value
        End Set
    End Property

    Public Property ModuleName() As String
        Get
            Return m_StrModuleName
        End Get
        Set(ByVal value As String)
            m_StrModuleName = value
        End Set
    End Property

    Public Property IsListBoxLoaded() As Boolean
        Get
            Return m_IsListBoxLoaded
        End Get
        Set(ByVal value As Boolean)
            m_IsListBoxLoaded = value
        End Set
    End Property

    Public Property HomeTeamColor() As System.Drawing.Color
        Get
            Return m_clrHomeTeamColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            m_clrHomeTeamColor = value
        End Set
    End Property

    Public Property VisitorTeamColor() As System.Drawing.Color
        Get
            Return m_clrVisitorTeamColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            m_clrVisitorTeamColor = value
        End Set
    End Property

    Public Property IsRestartGame() As Boolean
        Get
            Return m_blnIsRestartGame
        End Get
        Set(ByVal value As Boolean)
            m_blnIsRestartGame = value
        End Set
    End Property

    Public Property IsReporterRemoved() As Boolean
        Get
            Return m_blnIsReporterRemoved
        End Get
        Set(ByVal value As Boolean)
            m_blnIsReporterRemoved = value
        End Set
    End Property

    Public Property RestartData() As DataSet
        Get
            Return m_dsRestartData
        End Get
        Set(ByVal value As DataSet)
            m_dsRestartData = value
        End Set
    End Property

    Public Property IsEndGame() As Boolean
        Get
            Return m_IsEndGame
        End Get
        Set(ByVal value As Boolean)
            m_IsEndGame = value
        End Set
    End Property

    Public Property OpticalData() As DataSet
        Get
            Return m_OpticalData
        End Get
        Set(ByVal value As DataSet)
            m_OpticalData = value
        End Set
    End Property

    Public Property OpticalEvent() As Integer
        Get
            Return m_OpticalEvent
        End Get
        Set(ByVal value As Integer)
            m_OpticalEvent = value
        End Set
    End Property

    Public Property OpticalPBPAccepted() As DataSet
        Get
            Return m_OpticalPBPSaved
        End Get
        Set(ByVal value As DataSet)
            m_OpticalPBPSaved = value
        End Set
    End Property

    Public Property IsSportVUAvailable() As Boolean
        Get
            Return m_IsSportVUAvailable
        End Get
        Set(ByVal value As Boolean)
            m_IsSportVUAvailable = value
        End Set
    End Property

    Public Property IsContinuationEvent() As Boolean
        Get
            Return m_blnIsContinuationEvent
        End Get
        Set(ByVal value As Boolean)
            m_blnIsContinuationEvent = value
        End Set

    End Property

    Public Property IsLineupsInserted() As Boolean
        Get
            Return m_blnIsStartLineupsInserted
        End Get
        Set(ByVal value As Boolean)
            m_blnIsStartLineupsInserted = value
        End Set

    End Property

    Public Property CommentData() As String
        Get
            Return m_strComments
        End Get
        Set(ByVal value As String)
            m_strComments = value
        End Set
    End Property

    Public Property ManagerExplTime() As Integer
        Get
            Return m_intExplTime
        End Get
        Set(ByVal value As Integer)
            m_intExplTime = value
        End Set
    End Property

    Public Property ExplReasonID() As Integer
        Get
            Return m_intExplReason
        End Get
        Set(ByVal value As Integer)
            m_intExplReason = value
        End Set
    End Property

    Public Property IsDefaultGameClock() As Boolean
        Get
            Return m_blnIsDefaultGameClock
        End Get
        Set(ByVal value As Boolean)
            m_blnIsDefaultGameClock = value
        End Set

    End Property

    Public Property IsHalfTimeSwap() As Boolean
        Get
            Return m_blnIsHalfTimeSwap
        End Get
        Set(ByVal value As Boolean)
            m_blnIsHalfTimeSwap = value
        End Set

    End Property

    Public Property HomeTeamFormation() As String
        Get
            Return m_clrHomeTeamFormation
        End Get
        Set(ByVal value As String)
            m_clrHomeTeamFormation = value
        End Set
    End Property

    Public Property AwayTeamFormation() As String
        Get
            Return m_clrAwayTeamFormation
        End Get
        Set(ByVal value As String)
            m_clrAwayTeamFormation = value
        End Set
    End Property

    Public Property IsPairedEvent() As Boolean
        Get
            Return m_IsPairedEvent

        End Get
        Set(ByVal value As Boolean)
            m_IsPairedEvent = value
        End Set

    End Property

    Public Property Channel() As String
        Get
            Return m_Channel
        End Get
        Set(ByVal value As String)
            m_Channel = value
        End Set
    End Property
    Public Property Desk() As String
        Get
            Return m_Desk
        End Get
        Set(ByVal value As String)
            m_Desk = value
        End Set
    End Property

    Public Property IsDemoGameSelected() As Boolean
        Get
            Return m_blnIsDemoGameSelected
        End Get
        Set(ByVal value As Boolean)
            m_blnIsDemoGameSelected = value
        End Set

    End Property

    Public Property IsPrimaryReporterDown() As Boolean
        Get
            Return m_blnIsPrimaryReporterDown
        End Get
        Set(ByVal value As Boolean)
            m_blnIsPrimaryReporterDown = value
        End Set

    End Property

    Public Property AssistersLastEntryStatus() As Boolean
        Get
            Return m_blnAssistersLastEntryStatus
        End Get
        Set(ByVal value As Boolean)
            m_blnAssistersLastEntryStatus = value
        End Set

    End Property

    Public Property OpticalTouchData() As DataSet
        Get
            Return m_OpticalTouchData
        End Get
        Set(ByVal value As DataSet)
            m_OpticalTouchData = value
        End Set
    End Property

    Public Property OpticalTouchEvent() As Integer
        Get
            Return m_OpticalTouchEvent
        End Get
        Set(ByVal value As Integer)
            m_OpticalTouchEvent = value
        End Set
    End Property

    Public Property languageid() As Integer
        Get
            Return m_langid

        End Get
        Set(ByVal value As Integer)
            m_langid = value
        End Set
    End Property

    Public Property OpticalTouchAccepted() As DataSet
        Get
            Return m_OpticalTouchSaved
        End Get
        Set(ByVal value As DataSet)
            m_OpticalTouchSaved = value
        End Set
    End Property

    Public Property Reason() As String
        Get
            Return m_strReason
        End Get
        Set(ByVal value As String)
            m_strReason = value
        End Set
    End Property

    Public Property SysDateDiff() As TimeSpan
        Get
            Return m_dtSysDateDiff
        End Get
        Set(ByVal value As TimeSpan)
            m_dtSysDateDiff = value
        End Set
    End Property

    Public Property TimeZoneName() As String
        Get
            Return m_strTimeZone
        End Get
        Set(ByVal value As String)
            m_strTimeZone = value
        End Set
    End Property

    Public Property GameLastPlayed() As Integer
        Get
            Return m_intLastSelGameCode
        End Get
        Set(ByVal value As Integer)
            m_intLastSelGameCode = value
        End Set
    End Property
    Public Property IsSportDataIgnore() As Boolean
        Get
            Return m_IsSportDataIgnore
        End Get
        Set(ByVal value As Boolean)
            m_IsSportDataIgnore = value
        End Set
    End Property

    Public Property FieldWidth() As Integer
        Get
            Return m_intFieldWidth
        End Get
        Set(ByVal value As Integer)
            m_intFieldWidth = value
        End Set
    End Property

    Public Property FieldHeight() As Integer
        Get
            Return m_intFieldHeight
        End Get
        Set(ByVal value As Integer)
            m_intFieldHeight = value
        End Set
    End Property

    Public Property EditedEvents() As DataSet
        Get
            Return m_dsEditedEvents
        End Get
        Set(ByVal value As DataSet)
            m_dsEditedEvents = value
        End Set
    End Property

    Public Property TouchesData() As DataSet
        Get
            Return m_dsTouchesData
        End Get
        Set(ByVal value As DataSet)
            m_dsTouchesData = value
        End Set
    End Property

    Public Property RulesData() As DataSet
        Get
            Return m_RulesData
        End Get
        Set(ByVal value As DataSet)
            m_RulesData = value
        End Set
    End Property

    Public Property IsFoulAssociated() As Boolean
        Get
            Return m_blnFoulAssociated
        End Get
        Set(ByVal value As Boolean)
            m_blnFoulAssociated = value
        End Set
    End Property

    Public Property FouledSNO() As Decimal
        Get
            Return m_DecFouledSNO
        End Get
        Set(ByVal value As Decimal)
            m_DecFouledSNO = value
        End Set
    End Property

    Public Property TeamRosters() As DataSet
        Get
            Return m_dsPlayers
        End Get
        Set(ByVal value As DataSet)
            m_dsPlayers = value
        End Set
    End Property

    Public Property PenaltyShootOutAwayScore() As Integer
        Get
            Return m_intPenaltyShootOutAwayScore
        End Get
        Set(ByVal value As Integer)
            m_intPenaltyShootOutAwayScore = value
        End Set
    End Property

    Public Property PenaltyShootOutHomeScore() As Integer
        Get
            Return m_intPenaltyShootOutHomeScore
        End Get
        Set(ByVal value As Integer)
            m_intPenaltyShootOutHomeScore = value
        End Set
    End Property

    Public Property CommentsTime() As Integer
        Get
            Return m_intCommentsTime
        End Get
        Set(ByVal value As Integer)
            m_intCommentsTime = value
        End Set
    End Property

    Public Property FormState() As m_enumFormState
        Get
            Return m_FormState
        End Get
        Set(ByVal value As m_enumFormState)
            m_FormState = value
        End Set
    End Property

    Public Property IsScoreWindow() As Boolean
        Get
            Return m_blnIsScoreWindow
        End Get
        Set(ByVal value As Boolean)
            m_blnIsScoreWindow = value
        End Set
    End Property

    Public Property IsDetailedWindow() As Boolean
        Get
            Return m_blnIsDetailedWindow
        End Get
        Set(ByVal value As Boolean)
            m_blnIsDetailedWindow = value
        End Set
    End Property

    Public Property ReporterGames() As DataSet
        Get
            Return m_dsReporterGames
        End Get
        Set(ByVal value As DataSet)
            m_dsReporterGames = value
        End Set
    End Property

    Public Property IsMultipleFeedsAssigned() As Boolean
        Get
            Return m_blnIsMultipleFeedsAssigned
        End Get
        Set(ByVal value As Boolean)
            m_blnIsMultipleFeedsAssigned = value
        End Set
    End Property

    Public Property IsPrimaryReporter() As Boolean
        Get
            Return m_IsPrimaryReporter
        End Get
        Set(ByVal value As Boolean)
            m_IsPrimaryReporter = value
        End Set
    End Property
    Public Property PredefinedCommentID() As Integer
        Get
            Return m_PredefinedCommentID
        End Get
        Set(ByVal value As Integer)
            m_PredefinedCommentID = value
        End Set
    End Property
    Public Property CommentLanguageID() As Integer
        Get
            Return m_intCommentLanguageID
        End Get
        Set(ByVal value As Integer)
            m_intCommentLanguageID = value
        End Set
    End Property
    'get and set audit diretcory
    Public Property AuditDirectory() As String
        Get
            Return m_strDirectory
        End Get
        Set(ByVal value As String)
            m_strDirectory = value
        End Set
    End Property

    'get and set audit trail file original diretcory
    Public Property AuditFileName() As String
        Get
            Return m_strAuditFileName
        End Get
        Set(ByVal value As String)
            m_strAuditFileName = value
        End Set
    End Property

    'start 26-11-13
    Public Property donotshow() As String
        Get
            Return m_donotshow
        End Get
        Set(ByVal value As String)
            m_donotshow = value
        End Set
    End Property
    'end 26-11-13

#End Region

    Public Property XMLSort() As Boolean
        Get
            Return m_blnXMLSort
        End Get
        Set(ByVal value As Boolean)
            m_blnXMLSort = value
        End Set
    End Property

    Public ReadOnly Property MAXBOX() As Integer
        Get
            Return CInt(System.Configuration.ConfigurationManager.AppSettings("MAXBOX"))
        End Get
    End Property

    Public Property FromDate() As String

        Get

            Return m_FromDate
        End Get
        Set(ByVal value As String)

            m_FromDate = value
        End Set
    End Property
    Public Property ToDate() As String

        Get

            Return m_ToDate
        End Get
        Set(ByVal value As String)

            m_ToDate = value
        End Set
    End Property
    ' TO GET AND SET USER MODE
    Public Property IsNewDemomode() As Boolean
        Get
            Return m_strUserModeDemo
        End Get
        Set(ByVal value As Boolean)
            m_strUserModeDemo = value
        End Set
    End Property

    ' TO GET AND SET USER MODE
    Public Property IsInjuryTimeEdited() As Boolean
        Get
            Return m_IsInjuryTimeEdit
        End Get
        Set(ByVal value As Boolean)
            m_IsInjuryTimeEdit = value
        End Set
    End Property

    Public Property StartingLineupsEditUid() As Integer
        Get
            Return m_StartingLineupsEditUid
        End Get
        Set(ByVal value As Integer)
            m_StartingLineupsEditUid = value
        End Set
    End Property

    Public Property StartingLineups() As DataSet
        Get
            Return m_StartingLineups
        End Get
        Set(ByVal value As DataSet)
            m_StartingLineups = value
        End Set
    End Property
    Private _HomeFormationId? As Integer
    Public Property HomeFormationId() As Nullable(Of Integer)
        Get
            Return _HomeFormationId
        End Get
        Set(value As Nullable(Of Integer))
            _HomeFormationId = value
        End Set
    End Property

    Private _AwayFormationId? As Integer
    Public Property AwayFormationId() As Nullable(Of Integer)
        Get
            Return _AwayFormationId
        End Get
        Set(value As Nullable(Of Integer))
            _AwayFormationId = value
        End Set
    End Property
    Private _reporterRoleSerial? As Integer
    Public Property ReporterRoleSerial() As Nullable(Of Integer)
        Get
            Return _reporterRoleSerial
        End Get
        Set(value As Nullable(Of Integer))
            _reporterRoleSerial = value
        End Set
    End Property
    Public Property ActivateGame() As Boolean
        Get
            Return m_activateGame
        End Get
        Set(ByVal value As Boolean)
            m_activateGame = value
        End Set
    End Property
    Public Property IsETHomeTeamOnLeft() As Boolean
        Get
            Return m_blnIsETHomeTeamOnLeft
        End Get
        Set(ByVal value As Boolean)
            m_blnIsETHomeTeamOnLeft = value
        End Set
    End Property
    Public Property MultilingualData() As Dictionary(Of String,String)
        Get
            Return _multilingual
        End Get
        Set(ByVal value As  Dictionary(Of String,String))
            _multilingual = value
        End Set
    End Property
    Public Property HomeTeamCaptainId() As Integer
        Get
            Return m_intHomeTeamCaptainID
        End Get
        Set(ByVal value As Integer)
            m_intHomeTeamCaptainID = value
        End Set
    End Property
    Public Property AwayTeamCaptainId() As Integer
        Get
            Return m_intAwayTeamCaptainID
        End Get
        Set(ByVal value As Integer)
            m_intAwayTeamCaptainID = value
        End Set
    End Property
    Public Property IsTouchFinalized() As Boolean
        Get
            Return m_blnIsTouchFinalized
        End Get
        Set(ByVal value As Boolean)
            m_blnIsTouchFinalized = value
        End Set
    End Property
    Public Property AppPath() As String
        Get
            Return m_AppPath
        End Get
        Set(ByVal value As String)
            m_AppPath = value
        End Set
    End Property


#Region " User methods "

#End Region

End Class