﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.lblComments = New System.Windows.Forms.Label()
        Me.lblTeam = New System.Windows.Forms.Label()
        Me.cmbTeam = New System.Windows.Forms.ComboBox()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.txtTimeMod2 = New System.Windows.Forms.MaskedTextBox()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.grpComments = New System.Windows.Forms.GroupBox()
        Me.cmbComments = New System.Windows.Forms.ComboBox()
        Me.cmbLanguage = New System.Windows.Forms.ComboBox()
        Me.lblPredefinedComments = New System.Windows.Forms.Label()
        Me.lblLanguage = New System.Windows.Forms.Label()
        Me.mtxtDelay1 = New System.Windows.Forms.MaskedTextBox()
        Me.lblDelay = New System.Windows.Forms.Label()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.grpComments.SuspendLayout()
        Me.SuspendLayout()
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 346)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(319, 17)
        Me.sstMain.TabIndex = 266
        Me.sstMain.Text = "StatusStrip1"
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(0, 16)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(466, 19)
        Me.picReporterBar.TabIndex = 283
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(0, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(514, 17)
        Me.picTopBar.TabIndex = 282
        Me.picTopBar.TabStop = False
        '
        'lblComments
        '
        Me.lblComments.AutoSize = True
        Me.lblComments.BackColor = System.Drawing.Color.Transparent
        Me.lblComments.Location = New System.Drawing.Point(16, 182)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(159, 15)
        Me.lblComments.TabIndex = 2
        Me.lblComments.Text = "Other comments (English only):"
        '
        'lblTeam
        '
        Me.lblTeam.AutoSize = True
        Me.lblTeam.BackColor = System.Drawing.Color.Transparent
        Me.lblTeam.Location = New System.Drawing.Point(10, 148)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(66, 15)
        Me.lblTeam.TabIndex = 0
        Me.lblTeam.Text = "Team name:"
        '
        'cmbTeam
        '
        Me.cmbTeam.BackColor = System.Drawing.Color.White
        Me.cmbTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTeam.FormattingEnabled = True
        Me.cmbTeam.Location = New System.Drawing.Point(84, 144)
        Me.cmbTeam.Name = "cmbTeam"
        Me.cmbTeam.Size = New System.Drawing.Size(206, 23)
        Me.cmbTeam.TabIndex = 1
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(20, 204)
        Me.txtComments.MaxLength = 255
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(270, 57)
        Me.txtComments.TabIndex = 3
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnOK.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOK.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.White
        Me.btnOK.Location = New System.Drawing.Point(147, 10)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 25)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(228, 10)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnCancel)
        Me.picButtonBar.Controls.Add(Me.btnOK)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 299)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(419, 49)
        Me.picButtonBar.TabIndex = 6
        '
        'txtTimeMod2
        '
        Me.txtTimeMod2.Location = New System.Drawing.Point(84, 268)
        Me.txtTimeMod2.Mask = "000"
        Me.txtTimeMod2.Name = "txtTimeMod2"
        Me.txtTimeMod2.Size = New System.Drawing.Size(30, 22)
        Me.txtTimeMod2.TabIndex = 5
        Me.txtTimeMod2.Visible = False
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.lblTime.Location = New System.Drawing.Point(18, 271)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(36, 15)
        Me.lblTime.TabIndex = 4
        Me.lblTime.Text = "Time :"
        Me.lblTime.Visible = False
        '
        'grpComments
        '
        Me.grpComments.Controls.Add(Me.cmbComments)
        Me.grpComments.Controls.Add(Me.cmbLanguage)
        Me.grpComments.Controls.Add(Me.lblPredefinedComments)
        Me.grpComments.Controls.Add(Me.lblLanguage)
        Me.grpComments.Location = New System.Drawing.Point(12, 43)
        Me.grpComments.Name = "grpComments"
        Me.grpComments.Size = New System.Drawing.Size(291, 86)
        Me.grpComments.TabIndex = 285
        Me.grpComments.TabStop = False
        Me.grpComments.Text = "Predefined comments"
        '
        'cmbComments
        '
        Me.cmbComments.BackColor = System.Drawing.Color.White
        Me.cmbComments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbComments.FormattingEnabled = True
        Me.cmbComments.Location = New System.Drawing.Point(72, 50)
        Me.cmbComments.Name = "cmbComments"
        Me.cmbComments.Size = New System.Drawing.Size(206, 23)
        Me.cmbComments.TabIndex = 4
        '
        'cmbLanguage
        '
        Me.cmbLanguage.BackColor = System.Drawing.Color.White
        Me.cmbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLanguage.FormattingEnabled = True
        Me.cmbLanguage.Location = New System.Drawing.Point(72, 21)
        Me.cmbLanguage.Name = "cmbLanguage"
        Me.cmbLanguage.Size = New System.Drawing.Size(206, 23)
        Me.cmbLanguage.TabIndex = 3
        '
        'lblPredefinedComments
        '
        Me.lblPredefinedComments.AutoSize = True
        Me.lblPredefinedComments.BackColor = System.Drawing.Color.Transparent
        Me.lblPredefinedComments.Location = New System.Drawing.Point(6, 48)
        Me.lblPredefinedComments.Name = "lblPredefinedComments"
        Me.lblPredefinedComments.Size = New System.Drawing.Size(61, 15)
        Me.lblPredefinedComments.TabIndex = 2
        Me.lblPredefinedComments.Text = "Comments:"
        '
        'lblLanguage
        '
        Me.lblLanguage.AutoSize = True
        Me.lblLanguage.BackColor = System.Drawing.Color.Transparent
        Me.lblLanguage.Location = New System.Drawing.Point(6, 18)
        Me.lblLanguage.Name = "lblLanguage"
        Me.lblLanguage.Size = New System.Drawing.Size(58, 15)
        Me.lblLanguage.TabIndex = 1
        Me.lblLanguage.Text = "Language:"
        '
        'mtxtDelay1
        '
        Me.mtxtDelay1.Location = New System.Drawing.Point(228, 268)
        Me.mtxtDelay1.Mask = "00:00"
        Me.mtxtDelay1.Name = "mtxtDelay1"
        Me.mtxtDelay1.Size = New System.Drawing.Size(62, 22)
        Me.mtxtDelay1.TabIndex = 286
        Me.mtxtDelay1.Text = "0000"
        Me.mtxtDelay1.ValidatingType = GetType(Date)
        '
        'lblDelay
        '
        Me.lblDelay.AutoSize = True
        Me.lblDelay.Location = New System.Drawing.Point(163, 271)
        Me.lblDelay.Name = "lblDelay"
        Me.lblDelay.Size = New System.Drawing.Size(38, 15)
        Me.lblDelay.TabIndex = 287
        Me.lblDelay.Text = "Delay:"
        '
        'frmComments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(319, 363)
        Me.Controls.Add(Me.lblDelay)
        Me.Controls.Add(Me.mtxtDelay1)
        Me.Controls.Add(Me.grpComments)
        Me.Controls.Add(Me.txtTimeMod2)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.txtComments)
        Me.Controls.Add(Me.cmbTeam)
        Me.Controls.Add(Me.lblComments)
        Me.Controls.Add(Me.lblTeam)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmComments"
        Me.Text = "Comments"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.grpComments.ResumeLayout(False)
        Me.grpComments.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cmbTeam As System.Windows.Forms.ComboBox
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents txtTimeMod2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents grpComments As System.Windows.Forms.GroupBox
    Friend WithEvents lblPredefinedComments As System.Windows.Forms.Label
    Friend WithEvents lblLanguage As System.Windows.Forms.Label
    Friend WithEvents cmbComments As System.Windows.Forms.ComboBox
    Friend WithEvents cmbLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents mtxtDelay1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblDelay As System.Windows.Forms.Label
End Class
