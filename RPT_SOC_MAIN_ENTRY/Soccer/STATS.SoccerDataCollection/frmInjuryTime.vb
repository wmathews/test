﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
Imports System.Globalization
#End Region

Public Class frmInjuryTime
    Private m_objUtility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private lsupport As New languagesupport
    Private m_objclsComments As clsComments = clsComments.GetInstance()

    'Constants...
    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

    'Text messages..
    Dim strmessage1 As String = "Enter the correct time!"
    Dim strmessage2 As String = "Please check the Injury time you entered" 'TOSOCRS-30 [Validation and logging for Injury time]

    Private Sub frmInjuryTime_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "FrmAdandon", Nothing, 1, 0)
            Me.CenterToParent()
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)

            If CInt(m_objGameDetails.languageid) <> 1 Then
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
            End If
            If m_objGameDetails.IsEditMode = False Then
                ''TOSOCRS-82 Incorrect PBP sequence due to insert of wrong injury time
                If m_objGameDetails.IsInsertMode = True Then
                    txtClockTime.Text = m_objGameDetails.InjuryEnteredTime
                    txtClockTime.Text = txtClockTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
                Else
                    txtClockTime.Text = frmMain.UdcRunningClock1.URCCurrentTime
                    txtClockTime.Text = txtClockTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
                End If
            Else
                txtClockTime.Text = clsUtility.ConvertSecondToMinute(CDbl(m_objGameDetails.InjuryEnteredTime), False)
                txtClockTime.Text = txtClockTime.Text.Replace(":", "").PadLeft(5, CChar(" "))

                txtInjTime.Text = CStr(m_objGameDetails.CommentsTime)
                txtInjTime.Text = txtInjTime.Text.PadLeft(3, CChar(" "))
            End If
            If m_objGameDetails.ModuleID = 3 Then
                txtClockTime.Enabled = False
            Else
                txtClockTime.Enabled = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Try
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
                Me.Close()
            Catch ex As Exception
                MessageDialog.Show(ex, Me.Text)
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtClockTime_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtClockTime.Validated
        Try
            txtClockTime.Text = txtClockTime.Text.Replace(":", "").PadLeft(5, CChar(" "))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            Dim EditedTime As Integer
            If txtInjTime.Text.Trim = "" Then
                MessageDialog.Show(strmessage1, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                txtInjTime.Focus()
                Exit Sub
            End If

            Dim strCurrTime() As String
            strCurrTime = txtClockTime.Text.Split(CChar(":"))
            If (strCurrTime(0).Trim = "" And strCurrTime(1).Trim = "") Then
                MessageDialog.Show(strmessage1, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                txtClockTime.Focus()
                Exit Sub
            End If

            If CInt(txtInjTime.Text) > 166 Then 'TOSOCRS-30 [Validation and logging for Injury time]
                MessageDialog.Show(strmessage2, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                txtClockTime.Focus()
                Exit Sub
            End If

            EditedTime = CInt(txtInjTime.Text)
            m_objGameDetails.CommentsTime = EditedTime

            If m_objGameDetails.IsEditMode Then
                If CInt(m_objGameDetails.InjuryEnteredTime) <> CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtClockTime.Text))) Then
                    m_objGameDetails.IsInjuryTimeEdited = True
                End If
                'ElseIf m_objGameDetails.IsInsertMode = False Then
                '    If m_objGameDetails.InjuryEnteredTime IsNot Nothing Then
                '       If CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(m_objGameDetails.InjuryEnteredTime))) <> CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtClockTime.Text))) Then
                '            m_objGameDetails.IsInjuryTimeEdited = True
                '        End If
                '    End If
            End If

            If strCurrTime(0).Trim = "" Then
                txtClockTime.Text = "000:".Trim & strCurrTime(1).Trim
            ElseIf strCurrTime(1).Trim = "" Then
                txtClockTime.Text = strCurrTime(0).Trim & ":000".Trim
            End If

            m_objGameDetails.InjuryEnteredTime = txtClockTime.Text

            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Injury Time : " + txtInjTime.Text + " " + btnOK.Text, Nothing, 1, 0)

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 0 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 1 Then
                    Return CStr(CInt(ElapsedTime / 60))
                ElseIf CurrPeriod = 2 Then
                    Return CStr(CInt((FIRSTHALF + ElapsedTime) / 60))
                ElseIf CurrPeriod = 3 Then
                    Return CStr(CInt((SECONDHALF + ElapsedTime) / 60))
                ElseIf CurrPeriod = 4 Then
                    Return CStr(CInt((FIRSTEXTRA + ElapsedTime) / 60))
                End If
            End If

            If m_objGameDetails.IsDefaultGameClock = False And m_objGameDetails.ModuleID = 2 Then
                Return CStr(CInt(ElapsedTime / 60))
            End If

            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer) As Integer
        Try
            If m_objGameDetails.CurrentPeriod = 1 Then
                Return CurrentElapsedTime
            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF
            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA
            ElseIf m_objGameDetails.CurrentPeriod = 5 Then
                Return CurrentElapsedTime
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class