﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports Microsoft.Win32
Imports System.IO
#End Region

Public Class NetworkDisconnect

#Region "Constants Variable"
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
#End Region

    Private Sub NetworkDisconnect_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Me.CenterToScreen()
            chkDoNotShow.Checked = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


    Private Sub chkShow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDoNotShow.CheckedChanged
        Try
            'Me.Master_AuditTrailHelper.AuditLog(sender, " CheckedChanged", 1, 1)
            If chkDoNotShow.CheckState = CheckState.Checked Then
                m_objGameDetails.donotshow = "N"
            Else
                m_objGameDetails.donotshow = "Y"
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Me.Master_AuditTrailHelper.AuditLog(sender, "Clicked", 1, 1)
            Me.Close()

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub


End Class