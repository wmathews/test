﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddOfficial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlLeagueId = New System.Windows.Forms.Panel()
        Me.lblLeagueId = New System.Windows.Forms.Label()
        Me.CmbLeagueID = New System.Windows.Forms.ComboBox()
        Me.pnlOfficial = New System.Windows.Forms.Panel()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.btnIgnore = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.lvwOfficial = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.pnlLeagueId.SuspendLayout()
        Me.pnlOfficial.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlLeagueId
        '
        Me.pnlLeagueId.Controls.Add(Me.lblLeagueId)
        Me.pnlLeagueId.Controls.Add(Me.CmbLeagueID)
        Me.pnlLeagueId.Location = New System.Drawing.Point(-1, 37)
        Me.pnlLeagueId.Name = "pnlLeagueId"
        Me.pnlLeagueId.Size = New System.Drawing.Size(346, 37)
        Me.pnlLeagueId.TabIndex = 0
        '
        'lblLeagueId
        '
        Me.lblLeagueId.AutoSize = True
        Me.lblLeagueId.BackColor = System.Drawing.Color.Transparent
        Me.lblLeagueId.Location = New System.Drawing.Point(17, 10)
        Me.lblLeagueId.Name = "lblLeagueId"
        Me.lblLeagueId.Size = New System.Drawing.Size(57, 15)
        Me.lblLeagueId.TabIndex = 0
        Me.lblLeagueId.Text = "League ID"
        '
        'CmbLeagueID
        '
        Me.CmbLeagueID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbLeagueID.FormattingEnabled = True
        Me.CmbLeagueID.Location = New System.Drawing.Point(107, 7)
        Me.CmbLeagueID.Name = "CmbLeagueID"
        Me.CmbLeagueID.Size = New System.Drawing.Size(224, 23)
        Me.CmbLeagueID.TabIndex = 1
        '
        'pnlOfficial
        '
        Me.pnlOfficial.Controls.Add(Me.lblLastName)
        Me.pnlOfficial.Controls.Add(Me.lblFirstName)
        Me.pnlOfficial.Controls.Add(Me.btnApply)
        Me.pnlOfficial.Controls.Add(Me.btnIgnore)
        Me.pnlOfficial.Controls.Add(Me.btnEdit)
        Me.pnlOfficial.Controls.Add(Me.txtFirstName)
        Me.pnlOfficial.Controls.Add(Me.btnDelete)
        Me.pnlOfficial.Controls.Add(Me.txtLastName)
        Me.pnlOfficial.Controls.Add(Me.btnAdd)
        Me.pnlOfficial.Controls.Add(Me.lvwOfficial)
        Me.pnlOfficial.Controls.Add(Me.Panel1)
        Me.pnlOfficial.Location = New System.Drawing.Point(-1, 73)
        Me.pnlOfficial.Name = "pnlOfficial"
        Me.pnlOfficial.Size = New System.Drawing.Size(346, 359)
        Me.pnlOfficial.TabIndex = 1
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.BackColor = System.Drawing.Color.Transparent
        Me.lblLastName.Location = New System.Drawing.Point(17, 36)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(60, 15)
        Me.lblLastName.TabIndex = 2
        Me.lblLastName.Text = "Last name:"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.BackColor = System.Drawing.Color.Transparent
        Me.lblFirstName.Location = New System.Drawing.Point(17, 8)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(61, 15)
        Me.lblFirstName.TabIndex = 0
        Me.lblFirstName.Text = "First name:"
        '
        'btnApply
        '
        Me.btnApply.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnApply.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnApply.ForeColor = System.Drawing.Color.White
        Me.btnApply.Location = New System.Drawing.Point(208, 78)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(53, 25)
        Me.btnApply.TabIndex = 7
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = False
        '
        'btnIgnore
        '
        Me.btnIgnore.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnIgnore.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnIgnore.Enabled = False
        Me.btnIgnore.ForeColor = System.Drawing.Color.White
        Me.btnIgnore.Location = New System.Drawing.Point(267, 78)
        Me.btnIgnore.Name = "btnIgnore"
        Me.btnIgnore.Size = New System.Drawing.Size(53, 25)
        Me.btnIgnore.TabIndex = 8
        Me.btnIgnore.Text = "Ignore"
        Me.btnIgnore.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.Enabled = False
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(90, 78)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(53, 25)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(107, 8)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(224, 22)
        Me.txtFirstName.TabIndex = 1
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Enabled = False
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(149, 78)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(53, 25)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(107, 34)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(224, 22)
        Me.txtLastName.TabIndex = 3
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.btnAdd.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(31, 78)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(53, 25)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'lvwOfficial
        '
        Me.lvwOfficial.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvwOfficial.AutoArrange = False
        Me.lvwOfficial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvwOfficial.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvwOfficial.FullRowSelect = True
        Me.lvwOfficial.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwOfficial.HideSelection = False
        Me.lvwOfficial.Location = New System.Drawing.Point(12, 112)
        Me.lvwOfficial.MultiSelect = False
        Me.lvwOfficial.Name = "lvwOfficial"
        Me.lvwOfficial.ShowItemToolTips = True
        Me.lvwOfficial.Size = New System.Drawing.Size(323, 187)
        Me.lvwOfficial.TabIndex = 9
        Me.lvwOfficial.UseCompatibleStateImageBehavior = False
        Me.lvwOfficial.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Official"
        Me.ColumnHeader1.Width = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "League"
        Me.ColumnHeader2.Width = 0
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Location = New System.Drawing.Point(0, 307)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(348, 50)
        Me.Panel1.TabIndex = 11
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(260, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 427)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(345, 18)
        Me.sstMain.TabIndex = 2
        Me.sstMain.Text = "StatusStrip1"
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-169, 14)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(514, 24)
        Me.picReporterBar.TabIndex = 264
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-169, -1)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(514, 17)
        Me.picTopBar.TabIndex = 263
        Me.picTopBar.TabStop = False
        '
        'frmAddOfficial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(345, 445)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.pnlOfficial)
        Me.Controls.Add(Me.pnlLeagueId)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "frmAddOfficial"
        Me.Text = "Add New Officials"
        Me.pnlLeagueId.ResumeLayout(False)
        Me.pnlLeagueId.PerformLayout()
        Me.pnlOfficial.ResumeLayout(False)
        Me.pnlOfficial.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents pnlLeagueId As System.Windows.Forms.Panel
    Friend WithEvents lblLeagueId As System.Windows.Forms.Label
    Friend WithEvents CmbLeagueID As System.Windows.Forms.ComboBox
    Friend WithEvents pnlOfficial As System.Windows.Forms.Panel
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnIgnore As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lvwOfficial As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
