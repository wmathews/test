﻿#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmModule1
' Author        :
' Created Date  :
' Description   : This is the entry form for module 1. This form will be contained within frmMain at runtime.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmModule1Main

#Region "Member Variables"

    Private m_CurrentEventCode As Integer
    Private m_CurrentEvent As String
    Private m_dsSavePBPDataXML As New DataSet
    Private m_dsScores As New DataSet
    Private m_TimeEditMode As Boolean = False
    Private dictionaryListView As New Dictionary(Of Integer, ClsPbpTree.ListBoxColumnData)

#End Region

#Region "CONSTANTS AND VARIABLES"
    Private m_objUtility As New clsUtility
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objGameSetup As clsGameSetup = clsGameSetup.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objModule1Main As clsModule1Main = clsModule1Main.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objActionDetails As ClsActionDetails = ClsActionDetails.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private m_objModule1PBP As New frmModule1PBP

    Private dsMultiplegames As New DataSet
    Private dsDisplayMasterData As New DataSet
    Private m_dsDemoGames As New DataSet
    Private m_dsGames As New DataSet
    Private lsupport As New languagesupport
    Private Const PLAYER_PANEL_TOP As Integer = 6
    Private Const LEFT_PLAYER_PANEL_LEFT As Integer = 5  '12
    Private Const RIGHT_PLAYER_PANEL_LEFT As Integer = 865  '825

    Private Const LEFT_EVENTS_LVW As Integer = 4
    Private Const RIGHT_EVENTS_LVW As Integer = 346

    Private Const LEFT_LINK_EDIT As Integer = 303
    Private Const RIGHT_LINK_EDIT As Integer = 645

    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900

    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200

    Private MessageDialog As New frmMessageDialog

    Dim strmessage As String = "Currently there is no game(s) available"

#End Region

#Region "EVENTS HANDLING"

    Private Sub frmModule1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmModule1", Nothing, 1, 1)
            'add code for Multilingual -------------------------------------------------------------------------------------------------------

            If CInt(m_objclsGameDetails.languageid) <> 1 Then
                Dim TEMLENINT As Integer
                Dim myFont As System.Drawing.Font
                myFont = New System.Drawing.Font(" Arial Unicode MS", 6.5, FontStyle.Bold)
                ' Me.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), Me.Text)
                lblHomeBench.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblHomeBench.Text)
                lblMatchInfo.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblMatchInfo.Text)
                lblHomeManager.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblHomeManager.Text)
                lblVisitBench.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblVisitBench.Text)
                lblVisitManager.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblVisitManager.Text)
                lblOfficials.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblOfficials.Text)
                lblHomeGoals.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblHomeGoals.Text)
                lblHomeBookingExpulsions.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblHomeBookingExpulsions.Text)
                lblHomeSubstitutions.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblHomeSubstitutions.Text)
                lblMissedHomePenalties.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblMissedHomePenalties.Text)
                lblVisitGoals.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblVisitGoals.Text)
                lblVisitBookingExpulsions.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblVisitBookingExpulsions.Text)
                lblVisitSubstitutions.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblVisitSubstitutions.Text)
                lblMissedVisitPenalties.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblMissedVisitPenalties.Text)
                btnSwitchGames.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnSwitchGames.Text)
                TEMLENINT = lklOfficials.Text.Length
                lklOfficials.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklOfficials.Text)
                lklOfficials.Font = myFont
                'If TEMLENINT < lklOfficials.Text.Length Then
                ' myFont = New System.Drawing.Font(" Arial Unicode MS", 6.5, FontStyle.Bold)
                '    lklOfficials.Font = myFont
                'End If

                lklNewHomeGoal.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewHomeGoal.Text)
                lklNewHomeBooking.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewHomeBooking.Text)
                lklNewHomeSub.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewHomeSub.Text)
                lklNewHomePenalty.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewHomePenalty.Text)
                lklNewVisitGoal.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewVisitGoal.Text)
                lklNewVisitBooking.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewVisitBooking.Text)
                lklNewVisitSub.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewVisitSub.Text)
                lklNewVisitPenalty.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklNewVisitPenalty.Text)

                lklEditHomeGoals.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditHomeGoals.Text)
                lklEditHomeGoals.Font = myFont
                lklEditHomeBookings.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditHomeBookings.Text)
                lklEditHomeBookings.Font = myFont
                lklEditHomeSubstitutions.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditHomeSubstitutions.Text)
                lklEditHomeSubstitutions.Font = myFont
                lklEditHomePenalty.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditHomePenalty.Text)
                lklEditHomePenalty.Font = myFont
                lklMatchInfo.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklMatchInfo.Text)
                lklMatchInfo.Font = myFont
                lklEditVisitGoals.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditVisitGoals.Text)
                lklEditVisitGoals.Font = myFont

                lklEditVisitBookings.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditVisitBookings.Text)
                lklEditVisitBookings.Font = myFont
                lklEditVisitSubstitutions.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditVisitSubstitutions.Text)
                lklEditVisitSubstitutions.Font = myFont
                lklEditVisitPenalty.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lklEditVisitPenalty.Text)
                lklEditVisitPenalty.Font = myFont
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Referee:")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Linesman:")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Linesman:")
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "4th official:")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "5th official:")
                lvwOfficials.Columns(0).Text = g1
                lvwOfficials.Columns(1).Text = g2

                'lvwOfficials.Items(1).SubItems(1).Text.ToString()

                For i As Integer = 0 To 5
                    lvwOfficials.Items(i).SubItems(0).Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lvwOfficials.Items(i).SubItems(0).Text)
                Next
                For i As Integer = 0 To 4
                    lvwMatchInformation.Items(i).SubItems(0).Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lvwMatchInformation.Items(i).SubItems(0).Text)
                Next
                Dim gL1 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Kickoff:")
                Dim gL2 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Venue:")
                Dim gL3 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Attendance:")
                Dim gL4 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Delay:")
                Dim gL5 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Home start:")
                lvwMatchInformation.Columns(0).Text = gL1
                lvwMatchInformation.Columns(1).Text = gL2
                'lvwMatchInformation.Columns(2).Text = gL3
                'lvwMatchInformation.Columns(3).Text = gL4
                'lvwMatchInformation.Columns(4).Text = gL5
                Dim gLS1 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "SlNo")
                Dim gLS2 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Time")
                Dim gLS3 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Period")
                Dim gLS4 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Type")
                Dim gLS5 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Scorer")
                Dim gLS6 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Score")
                Dim gLS7 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Player")
                Dim gLS8 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Reason")
                Dim gLS9 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Card")
                Dim gLS10 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Player missed")
                Dim gLS11 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Saved")
                Dim gLS12 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Caused by")
                Dim gLS13 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Player out")
                Dim gLS14 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Player in")

                lvwHomeGoals.Columns(0).Text = gLS1
                lvwHomeGoals.Columns(1).Text = gLS2
                lvwHomeGoals.Columns(2).Text = gLS3
                lvwHomeGoals.Columns(3).Text = gLS4
                lvwHomeGoals.Columns(4).Text = gLS5
                lvwHomeGoals.Columns(5).Text = gLS6

                lvwHomeBookings.Columns(0).Text = gLS1
                lvwHomeBookings.Columns(1).Text = gLS2
                lvwHomeBookings.Columns(2).Text = gLS3
                lvwHomeBookings.Columns(3).Text = gLS7
                lvwHomeBookings.Columns(4).Text = gLS8
                lvwHomeBookings.Columns(5).Text = gLS9

                lvwHomeSubstitutions.Columns(0).Text = gLS1
                lvwHomeSubstitutions.Columns(1).Text = gLS2
                lvwHomeSubstitutions.Columns(2).Text = gLS3
                lvwHomeSubstitutions.Columns(3).Text = gLS13
                lvwHomeSubstitutions.Columns(4).Text = gLS14
                lvwHomeSubstitutions.Columns(5).Text = gLS8

                lvwHomePenalties.Columns(0).Text = gLS1
                lvwHomePenalties.Columns(1).Text = gLS2
                lvwHomePenalties.Columns(2).Text = gLS3
                lvwHomePenalties.Columns(3).Text = gLS10
                lvwHomePenalties.Columns(4).Text = gLS11
                lvwHomePenalties.Columns(5).Text = gLS12

                lvwVisitGoals.Columns(0).Text = gLS1
                lvwVisitGoals.Columns(1).Text = gLS2
                lvwVisitGoals.Columns(2).Text = gLS3
                lvwVisitGoals.Columns(3).Text = gLS4
                lvwVisitGoals.Columns(4).Text = gLS5
                lvwVisitGoals.Columns(5).Text = gLS6

                lvwVisitBookings.Columns(0).Text = gLS1
                lvwVisitBookings.Columns(1).Text = gLS2
                lvwVisitBookings.Columns(2).Text = gLS3
                lvwVisitBookings.Columns(3).Text = gLS7
                lvwVisitBookings.Columns(4).Text = gLS8
                lvwVisitBookings.Columns(5).Text = gLS9

                lvwVisitSubstitutions.Columns(0).Text = gLS1
                lvwVisitSubstitutions.Columns(1).Text = gLS2
                lvwVisitSubstitutions.Columns(2).Text = gLS3
                lvwVisitSubstitutions.Columns(3).Text = gLS13
                lvwVisitSubstitutions.Columns(4).Text = gLS14
                lvwVisitSubstitutions.Columns(5).Text = gLS8

                lvwVisitPenalties.Columns(0).Text = gLS1
                lvwVisitPenalties.Columns(1).Text = gLS2
                lvwVisitPenalties.Columns(2).Text = gLS3
                lvwVisitPenalties.Columns(3).Text = gLS10
                lvwVisitPenalties.Columns(4).Text = gLS11
                lvwVisitPenalties.Columns(5).Text = gLS12
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
            End If
            '----------------------------------------------------------------------------------------------------------------------------
            'Arindam commented on 26-Jun-09. Otherwise it is changing the START/END period state...
            If m_objGameDetails.IsRestartGame = False Then
                setEventButton()
            End If
            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                BindGames()
                pnlStats.Height = pnlVisit.Height
            End If

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                BindDemoGames()
                ClearMainScreenControls()
                If (m_objGameDetails.IsNewDemomode = True) Then
                    btnSwitchGames.Visible = False
                Else
                    btnSwitchGames.Visible = True
                End If

                frmMain.pnlEntryForm.Enabled = True
            Else
                If m_objGameDetails.ModuleID = 2 Then
                    If m_objGameDetails.CoverageLevel = 2 Or m_objGameDetails.CoverageLevel = 1 Then
                        'frmMain.lklHometeam.Enabled = False
                        frmMain.lklHometeam.Links(0).Enabled = False
                        frmMain.lklHometeam.LinkBehavior = LinkBehavior.NeverUnderline
                        'frmMain.lklAwayteam.Enabled = False
                        frmMain.lklAwayteam.Links(0).Enabled = False
                        frmMain.lklAwayteam.LinkBehavior = LinkBehavior.NeverUnderline
                        frmMain.EnableMenuItem("TeamSetupToolStripMenuItem", False)
                        frmMain.EnableMenuItem("SortPlayersToolStripMenuItem", False)
                        frmMain.EnableMenuItem("SortByUniformToolStripMenuItem", False) 'Switch Side
                        frmMain.EnableMenuItem("SortByLastNameToolStripMenuItem", False) 'Player Sort
                        frmMain.EnableMenuItem("SortByPositionToolStripMenuItem", False)
                        frmMain.EnableMenuItem("SwitchSidesToolStripMenuItem", False)

                        frmMain.EnableMenuItem("InputTeamStatsToolStripMenuItem", False) 'Switch Side
                        frmMain.EnableMenuItem("ReportFormationToolStripMenuItem", False) 'Player Sort
                        frmMain.EnableMenuItem("AddOfficialToolStripMenuItem", False)
                        frmMain.EnableMenuItem("AddManagerToolStripMenuItem", False)
                        If m_objclsGameDetails.ModuleID = 2 Then
                            frmMain.EnableMenuItem("AbandonToolStripMenuItem", False)
                            ' frmMain.EnableMenuItem("DelayedToolStripMenuItem", False)
                        End If

                        frmMain.EnableMenuItem("PlayerStatsToolStripMenuItem", False)

                    End If
                    BindMod2MultipleGames()
                    ClearMainScreenControls()
                    If (m_objGameDetails.IsNewDemomode = True) Then
                        btnSwitchGames.Visible = False
                    Else
                        btnSwitchGames.Visible = True
                    End If

                    frmMain.pnlEntryForm.Enabled = True
                ElseIf m_objGameDetails.ModuleID = 1 Then
                    'Jira TOSOCRS-158 Disable Delay Over menu when first time logged in
                    If m_objGameDetails.CurrentPeriod = 0 Then
                        Dim dsPBP As New DataSet
                        dsPBP = m_objGeneral.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                        Dim dr() As DataRow = dsPBP.Tables(0).Select("EVENT_CODE_ID = 59 ")
                        If dr.Length > 0 Then
                            frmMain.EnableMenuItem("DelayOverToolStripMenuItem", True)
                        Else
                            frmMain.EnableMenuItem("DelayOverToolStripMenuItem", False)
                        End If
                    End If
                End If
                'FILLS THE MASTER TABLES (BOOKING REASON,TYPE AND SUBSTITUTED REASON)
                'WHICH IS USED TO DISPLAY IN MAIN SCREEN
                dsDisplayMasterData = m_objModule1Main.FetchMainScreenMasterData(m_objGameDetails.languageid)
                UpdateManager()
                'switch sides to match the PBP page
                If m_objGameDetails.IsRestartGame = False Then
                    SetSides()
                End If

                'If m_objGameDetails.ModuleID = 1 Then
                pnlStats.Height = pnlVisit.Height
                frmMain.tmrRefresh.Enabled = True
                frmMain.EnableMenuItem("SetTeamColorToolStripMenuItem", False)
                frmMain.EnableMenuItem("HomeTeamColorToolStripMenuItem", False)
                frmMain.EnableMenuItem("VisitorTeamColorToolStripMenuItem", False)
            End If
            AddEventFieldsInMaintoDictionary()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklNewHomeGoal_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewHomeGoal.LinkClicked
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewHomeGoal.Text & " - " & m_objGameDetails.HomeTeam, lblHomeGoals.Text.Trim(), 1, 0)
            'GOAL HOME EVENT
            'HIGHLIGHTS THE GOAL EVENT FROM HOME TEAM
            gpgoaltypes.Visible = True
            lblteam.Text = "home"
            'm_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            'frmMain.btnGotoPBP.Visible = False
            'frmMain.btnGotoMain.Visible = True
            'frmMain.SetChildForm()
            'frmMain.SetMainFormSize()
            'If m_objGameDetails.IsHomeTeamOnLeft Then
            '    frmMain.FiringEventsFromMain("GOALHOME", "NEW")
            'Else
            '    frmMain.FiringEventsFromMain("GOALVISIT", "NEW")
            'End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally

        End Try
    End Sub

    Private Sub btnCancelGoaltype_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelGoaltype.Click
        Try
            gpgoaltypes.Visible = False
            radPenalty.Checked = False
            radNormalgoal.Checked = False
            radOwngoal.Checked = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnokgoaltype_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnokgoaltype.Click
        Try
            Dim Goaltype As String
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()

            If radPenalty.Checked Then
                Goaltype = "Penalty"
            ElseIf radNormalgoal.Checked Then
                Goaltype = "Normal"
            ElseIf radOwngoal.Checked Then
                Goaltype = "Own"
            End If

            If m_objGameDetails.IsHomeTeamOnLeft Then
                If lblteam.Text = "visit" Then
                    frmMain.FiringEventsFromMain(Goaltype & "VISIT", "NEW")
                Else
                    frmMain.FiringEventsFromMain(Goaltype & "HOME", "NEW")
                End If
            Else
                If lblteam.Text = "visit" Then
                    frmMain.FiringEventsFromMain(Goaltype & "HOME", "NEW")
                Else
                    frmMain.FiringEventsFromMain(Goaltype & "VISIT", "NEW")
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            gpgoaltypes.Visible = False
        End Try
    End Sub

    Private Sub lklNewVisitGoal_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewVisitGoal.LinkClicked
        Try 'GOAL VISIT EVENT
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewVisitGoal.Text & " - " & m_objGameDetails.AwayTeam, lblHomeGoals.Text.Trim(), 1, 0)
            gpgoaltypes.Visible = True
            lblteam.Text = "visit"
            'm_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            'frmMain.btnGotoPBP.Visible = False
            'frmMain.btnGotoMain.Visible = True
            'frmMain.SetChildForm()
            'frmMain.SetMainFormSize()
            'If m_objGameDetails.IsHomeTeamOnLeft Then
            '    frmMain.FiringEventsFromMain("GOALVISIT", "NEW")
            'Else
            '    frmMain.FiringEventsFromMain("GOALHOME", "NEW")
            'End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        Finally

        End Try
    End Sub

    Private Sub lklNewHomeBooking_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewHomeBooking.LinkClicked
        Try 'BOOKING HOME EVENT
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewHomeBooking.Text & " - " & m_objGameDetails.HomeTeam, lblHomeBookingExpulsions.Text.Trim(), 1, 0)
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()
            If m_objGameDetails.IsHomeTeamOnLeft Then
                frmMain.FiringEventsFromMain("BOOKINGHOME", "NEW")
            Else
                frmMain.FiringEventsFromMain("BOOKINGVISIT", "NEW")
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklNewVisitBooking_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewVisitBooking.LinkClicked
        Try 'BOOKING VISIT EVENT
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewVisitBooking.Text & " - " & m_objGameDetails.AwayTeam, lblHomeBookingExpulsions.Text.Trim(), 1, 0)
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()
            If m_objGameDetails.IsHomeTeamOnLeft Then
                frmMain.FiringEventsFromMain("BOOKINGVISIT", "NEW")
            Else
                frmMain.FiringEventsFromMain("BOOKINGHOME", "NEW")
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditSubstitutions_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditHomeSubstitutions.LinkClicked
        Try
            If lvwHomeSubstitutions.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditHomeSubstitutions.Text, lblHomeSubstitutions.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True

                frmMain.FiringEventsFromMain("SUB", "EDIT", CDec(lvwHomeSubstitutions.SelectedItems.Item(0).Text))
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

    Private Sub lklEditGoals_LinkClicked1(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditHomeGoals.LinkClicked
        Try
            If lvwHomeGoals.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditHomeSubstitutions.Text, lblHomeSubstitutions.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("GOAL", "EDIT", CDec(lvwHomeGoals.SelectedItems.Item(0).Text))
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditPenalty_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditHomePenalty.LinkClicked

        Try
            If lvwHomePenalties.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditHomeSubstitutions.Text, lblHomeSubstitutions.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("MISSPEN", "EDIT", CDec(lvwHomePenalties.SelectedItems.Item(0).Text))
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditBookings_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditHomeBookings.LinkClicked
        Try
            If lvwHomeBookings.SelectedItems.Count > 0 Then
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditHomeSubstitutions.Text, lblHomeSubstitutions.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("BOOK", "EDIT", CDec(lvwHomeBookings.SelectedItems.Item(0).Text))
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditVisitBookings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lklEditVisitBookings.Click
        Try
            If lvwVisitBookings.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditVisitBookings.Text, lklEditVisitBookings.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("BOOK", "EDIT", CDec(lvwVisitBookings.SelectedItems.Item(0).Text))
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditVisitGoals_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditVisitGoals.LinkClicked
        Try
            If lvwVisitGoals.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditVisitGoals.Text, lklEditVisitGoals.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("GOAL", "EDIT", CDec(lvwVisitGoals.SelectedItems.Item(0).Text))
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditVisitSubstitutions_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditVisitSubstitutions.LinkClicked
        Try
            If lvwVisitSubstitutions.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditVisitSubstitutions.Text, lklEditVisitSubstitutions.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("SUB", "EDIT", CDec(lvwVisitSubstitutions.SelectedItems.Item(0).Text))
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklEditVisitPenalty_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEditVisitPenalty.LinkClicked
        Try
            If lvwVisitPenalties.SelectedItems.Count > 0 Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklEditVisitSubstitutions.Text, lklEditVisitSubstitutions.Text.Trim(), 1, 0)
                m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
                frmMain.btnGotoPBP.Visible = False
                frmMain.btnGotoMain.Visible = True
                frmMain.SetChildForm()
                frmMain.SetMainFormSize()
                m_objGameDetails.EditFromMain = True
                frmMain.FiringEventsFromMain("MISSPEN", "EDIT", CDec(lvwVisitPenalties.SelectedItems.Item(0).Text))
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklNewHomeSub_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewHomeSub.LinkClicked
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewHomeSub.Text & "-" & m_objGameDetails.HomeTeam, lblHomeSubstitutions.Text.Trim(), 1, 0)
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()
            If m_objGameDetails.IsHomeTeamOnLeft Then
                frmMain.FiringEventsFromMain("SUBHOME", "NEW")
            Else
                frmMain.FiringEventsFromMain("SUBVISIT", "NEW")
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklNewVisitSub_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewVisitSub.LinkClicked
        Try 'SUBSTITUTE VISIT EVENT
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewVisitSub.Text & "-" & m_objGameDetails.AwayTeam, lblHomeSubstitutions.Text.Trim(), 1, 0)
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()
            m_objModule1Main = Nothing
            If m_objGameDetails.IsHomeTeamOnLeft Then
                frmMain.FiringEventsFromMain("SUBVISIT", "NEW")
            Else
                frmMain.FiringEventsFromMain("SUBHOME", "NEW")
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklNewHomePenalty_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewHomePenalty.LinkClicked
        Try 'PENALTY HOME
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewHomePenalty.Text & "-" & m_objGameDetails.HomeTeam, lblMissedHomePenalties.Text.Trim(), 1, 0)
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()
            If m_objGameDetails.IsHomeTeamOnLeft Then
                frmMain.FiringEventsFromMain("PENALTYHOME", "NEW")
            Else
                frmMain.FiringEventsFromMain("PENALTYVISIT", "NEW")
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklNewVisitPenalty_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNewVisitPenalty.LinkClicked
        Try  'PENALTY VISIT

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklNewVisitPenalty.Text & "-" & m_objGameDetails.AwayTeam, lblMissedHomePenalties.Text.Trim(), 1, 0)
            m_objGameDetails.FormState = clsGameDetails.m_enumFormState.PBPForm
            frmMain.btnGotoPBP.Visible = False
            frmMain.btnGotoMain.Visible = True
            frmMain.SetChildForm()
            frmMain.SetMainFormSize()
            If m_objGameDetails.IsHomeTeamOnLeft Then
                frmMain.FiringEventsFromMain("PENALTYVISIT", "NEW")
            Else
                frmMain.FiringEventsFromMain("PENALTYHOME", "NEW")
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub lklOfficials_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklOfficials.LinkClicked
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklOfficials.Text, lblOfficials.Text.Trim(), 1, 0)
            Dim objGameSetup As New frmGameSetup
            objGameSetup.ShowDialog()
            InsertOfficialAndMatchInfo()
            If m_objGameDetails.Reason <> "" Then
                InsertPlayByPlayData(clsGameDetails.DELAYED, "")
            End If
            'If objGameSetup.ShowDialog = Windows.Forms.DialogResult.OK Then
            '    SwitchSides()
            '    frmMain.SetHeader2()
            '    InsertOfficialAndMatchInfo()
            'End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' Opens the Game Setup form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub lklMatchInfo_LinkClicked_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklMatchInfo.LinkClicked
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.LinkLabelClicked, lklMatchInfo.Text, lblMatchInfo.Text.Trim(), 1, 0)
            Dim objGameSetup As New frmGameSetup
            objGameSetup.ShowDialog()
            InsertOfficialAndMatchInfo()
            'If objGameSetup.ShowDialog = Windows.Forms.DialogResult.OK Then
            '    SwitchSides()
            '    frmMain.SetHeader2()
            '    InsertOfficialAndMatchInfo()
            'End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

#Region "PRIVATE FUNCTIONS"

    Public Sub ClearListviews()
        Try
            'CELARS THE ITEMS IN THE LISTBOX
            lvwHomeGoals.Items.Clear()
            lvwHomeSubstitutions.Items.Clear()
            lvwHomeBookings.Items.Clear()
            lvwHomePenalties.Items.Clear()
            lvwVisitGoals.Items.Clear()
            lvwVisitSubstitutions.Items.Clear()
            lvwVisitBookings.Items.Clear()
            lvwVisitPenalties.Items.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' DISPLAY EVENTS SUCH AS GOAL,PENALTY,SUB IN THE MAIN SCREEN LISTBOX
    ''' </summary>
    ''' <param name="DsEvents"></param>
    ''' <remarks></remarks>
    Private Sub DisplayPBPEventsInMain(ByVal DsEvents As DataSet)
        Try
            If DsEvents.Tables(0).Rows.Count > 0 Then
                Dim ElapsedTime As Integer
                Dim StrTime As String = "00:00"
                Dim CurrPeriod As Integer

                ClearListviews()

                'DISPLAY GOAL EVENTS IN MAIN SCREEN LISTBOX
                For i As Integer = 0 To DsEvents.Tables(0).Rows.Count - 1

                    Select Case CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID"))
                        Case clsGameDetails.GOAL, clsGameDetails.PENALTY, clsGameDetails.OWNGOAL, clsGameDetails.SHOOTOUT_GOAL
                            Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
                            If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
                                CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
                                ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                If ElapsedTime > 0 Then
                                    If m_objGameDetails.ModuleID = 1 Then
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                    Else
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                    End If
                                    'ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                End If
                                StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                            End If
                            lvwitem.SubItems.Add(StrTime)
                            lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
                            lvwitem.SubItems.Add(GetDescription("EVENT_DESC", CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID"))))
                            If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 28 Then
                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
                                Else
                                    lvwitem.SubItems.Add("")
                                End If
                            Else
                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
                                Else
                                    lvwitem.SubItems.Add("")
                                End If
                            End If
                            lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("OFFENSE_SCORE").ToString & "-" & DsEvents.Tables(0).Rows(i).Item("DEFENSE_SCORE").ToString)
                            If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwHomeGoals.Items.Add(lvwitem)
                                Else
                                    lvwVisitGoals.Items.Add(lvwitem)
                                End If

                            Else
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwVisitGoals.Items.Add(lvwitem)
                                Else
                                    lvwHomeGoals.Items.Add(lvwitem)
                                End If
                            End If

                        Case 22
                            'FILLING SUBSITITION EVENTS
                            Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
                            If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
                                'CONVERTING TO MINUTES
                                CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
                                ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                If ElapsedTime = 0 Then
                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 60
                                Else
                                    If m_objGameDetails.ModuleID = 1 Then
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                    Else
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                    End If
                                    'ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                End If
                                StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                            End If
                            lvwitem.SubItems.Add(StrTime)
                            lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
                                lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
                                lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID")) Then
                                lvwitem.SubItems.Add(GetDescription("SUB_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwHomeSubstitutions.Items.Add(lvwitem)
                                Else
                                    lvwVisitSubstitutions.Items.Add(lvwitem)
                                End If
                            Else
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwVisitSubstitutions.Items.Add(lvwitem)
                                Else
                                    lvwHomeSubstitutions.Items.Add(lvwitem)
                                End If
                            End If
                        Case 2, 7
                            'FILLING BOOKINGS(YELLOW/RED CARDS)
                            Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
                            If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
                                CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
                                ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                If ElapsedTime > 0 Then
                                    If m_objGameDetails.ModuleID = 1 Then
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                    Else
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                    End If
                                    'ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                End If
                                StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                            End If
                            lvwitem.SubItems.Add(StrTime)
                            lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
                            If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 2 Then
                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
                                Else
                                    lvwitem.SubItems.Add("")
                                End If
                            Else
                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
                                Else
                                    lvwitem.SubItems.Add("")
                                End If
                            End If
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID")) Then
                                lvwitem.SubItems.Add(GetDescription("BOOKING_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID")) Then
                                lvwitem.SubItems.Add(GetDescription("BOOKING_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwHomeBookings.Items.Add(lvwitem)
                                Else
                                    lvwVisitBookings.Items.Add(lvwitem)
                                End If
                            Else
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwVisitBookings.Items.Add(lvwitem)
                                Else
                                    lvwHomeBookings.Items.Add(lvwitem)
                                End If
                            End If
                        Case 18
                            'FILLING MISSED PENALTY EVENTS
                            Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
                            If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
                                CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
                                ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                If ElapsedTime > 0 Then
                                    If m_objGameDetails.ModuleID = 1 Then
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
                                    Else
                                        ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                    End If
                                    'ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED")) + 30
                                End If
                                StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                            End If
                            lvwitem.SubItems.Add(StrTime)
                            lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
                                lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID")) Then
                                lvwitem.SubItems.Add(GetDescription("SHOT_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("CAUSED_BY_PLAYER_ID")) Then
                                lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("CAUSED_BY_PLAYER_ID"))))
                            Else
                                lvwitem.SubItems.Add("")
                            End If
                            If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwHomePenalties.Items.Add(lvwitem)
                                Else
                                    lvwVisitPenalties.Items.Add(lvwitem)
                                End If
                            Else
                                If m_objGameDetails.IsHomeTeamOnLeft Then
                                    lvwVisitPenalties.Items.Add(lvwitem)
                                Else
                                    lvwHomePenalties.Items.Add(lvwitem)
                                End If
                            End If
                    End Select
                Next
            End If

            'arranging the list views based on the no of events in each boxes
            ArrangeListViews()

            'COLN 0 HOLDS SEQUECNE NUMBER WHICH CAN BE USED LATER FOR EDITING PURPOSE
            lvwHomeGoals.Columns(0).Width = 0
            lvwHomeSubstitutions.Columns(0).Width = 0
            lvwHomeBookings.Columns(0).Width = 0
            lvwHomePenalties.Columns(0).Width = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' FETCHING THE REASON FOR THE SUBSTITION PLAYER BY PASSING SUB_REASON_ID
    ''' </summary>
    ''' <remarks></remarks>
    Private Function GetDescription(ByVal Str As String, ByVal ID As Integer) As String
        Try
            Dim Drs() As DataRow
            If dsDisplayMasterData.Tables.Count > 0 Then
                Select Case Str
                    Case "SUB_REASON"
                        Drs = dsDisplayMasterData.Tables("SUB_REASON").Select("SUB_REASON_ID = " & ID & "")
                        Return CStr(Drs(0).Item("SUB_REASON"))
                    Case "BOOKING_TYPE"
                        Drs = dsDisplayMasterData.Tables("BOOKING_TYPE").Select("BOOKING_TYPE_ID = " & ID & "")
                        Return CStr(Drs(0).Item("BOOKING_TYPE"))
                    Case "BOOKING_REASON"
                        Drs = dsDisplayMasterData.Tables("BOOKING_REASON").Select("BOOKING_REASON_ID = " & ID & "")
                        Return CStr(Drs(0).Item("BOOKING_REASON"))
                    Case "SHOT_TYPE"
                        Drs = dsDisplayMasterData.Tables("SHOT_TYPE").Select("SHOT_TYPE_ID = " & ID & "")
                        Return CStr(Drs(0).Item("SHOT_TYPE"))
                    Case "EVENT_DESC"
                        Drs = dsDisplayMasterData.Tables("EVENT_CODE").Select("EVENT_CODE_ID = " & ID & "")
                        Return CStr(Drs(0).Item("EVENT_CODE_DESC"))
                End Select
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FUNCTION TO RETRIVE PLAYER NAME BY PASSING PLAYER ID AS PARAMETER
    ''' </summary>
    ''' <param name="PlayerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Private Function FetchPlayerNames(ByVal PlayerID As Integer) As String
        Try
            Dim StrPlayer As String = ""
            If m_objGameDetails.CoverageLevel = 2 Then
                If m_objGameDetails.TeamRosters.Tables.Count > 0 Then
                    Dim DsPlayers As DataSet = m_objGameDetails.TeamRosters.Copy()
                    Dim drs() As DataRow = Nothing
                    drs = DsPlayers.Tables(0).Select("PLAYER_ID = " & PlayerID & "")
                    If drs.Length > 0 Then
                        StrPlayer = CStr(drs(0).Item("PLAYER"))
                    Else
                        drs = DsPlayers.Tables(1).Select("PLAYER_ID = " & PlayerID & "")
                        If drs.Length > 0 Then
                            StrPlayer = CStr(drs(0).Item("PLAYER"))
                        End If
                    End If
                End If
            Else
                If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                    Dim DsPlayers As DataSet = m_objTeamSetup.RosterInfo.Copy()
                    Dim drs() As DataRow = Nothing
                    drs = DsPlayers.Tables("Home").Select("PLAYER_ID = " & PlayerID & "")
                    If drs.Length > 0 Then
                        StrPlayer = CStr(drs(0).Item("PLAYER"))
                    Else
                        drs = DsPlayers.Tables("Away").Select("PLAYER_ID = " & PlayerID & "")
                        If drs.Length > 0 Then
                            StrPlayer = CStr(drs(0).Item("PLAYER"))
                        End If
                    End If
                End If
            End If

            Return StrPlayer
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FILLING OF HOME TEAM PLAYERS
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LabelHomeTeamTagged()
        Try
            Dim intLblCntr As Integer = 0
            Dim lbl As Label
            Dim lblPos As Label
            Dim strDisplayUniform As String
            Dim strLastName As String
            Dim strMoniker As String
            Dim strPositionAbbrev As String

            Dim hthome As New Hashtable
            Dim hthomesub As New Hashtable
            Dim dsPlayers As New DataSet

            SetBenchPlayerslablesVisibility()
            If m_objTeamSetup.RosterInfo IsNot Nothing Then
                If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                    If Not m_objTeamSetup.RosterInfo.Tables("Home") Is Nothing Then
                        If m_objTeamSetup.RosterInfo.Tables("Home").Rows.Count > 0 Then

                            dsPlayers = m_objTeamSetup.RosterInfo.Copy()
                            hthome.Add(1, Me.lblPlayer1Home)
                            hthome.Add(2, Me.lblPlayer2Home)
                            hthome.Add(3, Me.lblPlayer3Home)
                            hthome.Add(4, Me.lblPlayer4Home)
                            hthome.Add(5, Me.lblPlayer5Home)
                            hthome.Add(6, Me.lblPlayer6Home)
                            hthome.Add(7, Me.lblPlayer7Home)
                            hthome.Add(8, Me.lblPlayer8Home)
                            hthome.Add(9, Me.lblPlayer9Home)
                            hthome.Add(10, Me.lblPlayer10Home)
                            hthome.Add(11, Me.lblPlayer11Home)
                            hthome.Add(12, Me.lblPosition1Home)
                            hthome.Add(13, Me.lblPosition2Home)
                            hthome.Add(14, Me.lblPosition3Home)
                            hthome.Add(15, Me.lblPosition4Home)
                            hthome.Add(16, Me.lblPosition5Home)
                            hthome.Add(17, Me.lblPosition6Home)
                            hthome.Add(18, Me.lblPosition7Home)
                            hthome.Add(19, Me.lblPosition8Home)
                            hthome.Add(20, Me.lblPosition9Home)
                            hthome.Add(21, Me.lblPosition10Home)
                            hthome.Add(22, Me.lblPosition11Home)

                            If dsPlayers Is Nothing Then
                                Exit Sub
                            End If

                            Dim drs() As DataRow

                            Dim strSortOrder As String = ""
                            If m_objLoginDetails.strSortOrder = "Uniform" Then
                                strSortOrder = "SORT_NUMBER"
                                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", True)
                                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                            ElseIf m_objLoginDetails.strSortOrder = "Position" Then
                                strSortOrder = "STARTING_POSITION"
                                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", True)

                            ElseIf m_objLoginDetails.strSortOrder = "Last Name" Then
                                strSortOrder = "LAST_NAME"
                                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", True)
                                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                            End If

                            drs = dsPlayers.Tables("Home").Select("STARTING_POSITION < 12", "" & strSortOrder & " ASC")
                            If drs.Length > 0 Then
                                For Each dr As DataRow In drs
                                    strDisplayUniform = dr.Item("DISPLAY_UNIFORM_NUMBER").ToString
                                    strLastName = dr.Item("LAST_NAME").ToString

                                    If dr.Item("MONIKER").ToString = "" Then
                                        strMoniker = ""
                                    Else
                                        strMoniker = dr.Item("MONIKER").ToString.Chars(0)
                                    End If

                                    strPositionAbbrev = dr.Item("POSITION_ABBREV").ToString
                                    intLblCntr += 1
                                    lbl = CType(hthome.Item(intLblCntr), Label)
                                    lblPos = CType(hthome.Item(intLblCntr + 11), Label)
                                    If strDisplayUniform = "" Then
                                        lbl.Text = "       " & strLastName & Space(1) & strMoniker
                                        lblPos.Text = strPositionAbbrev
                                    Else
                                        If strDisplayUniform.Length = 1 Then
                                            lbl.Text = " " & strDisplayUniform & ".   " & strLastName & Space(1) & strMoniker
                                            lblPos.Text = strPositionAbbrev
                                        Else
                                            lbl.Text = " " & strDisplayUniform & ". " & strLastName & Space(1) & strMoniker
                                            lblPos.Text = strPositionAbbrev
                                        End If

                                    End If
                                    lbl.Tag = dr.Item("PLAYER_ID").ToString()
                                    lbl.Visible = True
                                    lblPos.Visible = True
                                    While lbl.PreferredWidth > lbl.Width
                                        strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                        If strDisplayUniform = "" Then
                                            lbl.Text = "       " & strLastName & "." & Space(1) & strMoniker
                                        ElseIf strDisplayUniform.Length = 1 Then
                                            lbl.Text = " " & strDisplayUniform & ".   " & strLastName & "." & Space(1) & strMoniker
                                        Else
                                            lbl.Text = " " & strDisplayUniform & ". " & strLastName & "." & Space(1) & strMoniker
                                        End If
                                    End While
                                Next
                            End If

                            intLblCntr = 0

                            hthomesub.Add(1, Me.lblSub1Home)
                            hthomesub.Add(2, Me.lblSub2Home)
                            hthomesub.Add(3, Me.lblSub3Home)
                            hthomesub.Add(4, Me.lblSub4Home)
                            hthomesub.Add(5, Me.lblSub5Home)
                            hthomesub.Add(6, Me.lblSub6Home)
                            hthomesub.Add(7, Me.lblSub7Home)
                            hthomesub.Add(8, Me.lblSub8Home)
                            hthomesub.Add(9, Me.lblSub9Home)
                            hthomesub.Add(10, Me.lblSub10Home)
                            hthomesub.Add(11, Me.lblSub11Home)
                            hthomesub.Add(12, Me.lblSub12Home)
                            hthomesub.Add(13, Me.lblSub13Home)
                            hthomesub.Add(14, Me.lblSub14Home)
                            hthomesub.Add(15, Me.lblSub15Home)
                            hthomesub.Add(16, Me.lblSub16Home)
                            hthomesub.Add(17, Me.lblSub17Home)
                            hthomesub.Add(18, Me.lblSub18Home)
                            hthomesub.Add(19, Me.lblSub19Home)
                            hthomesub.Add(20, Me.lblSub20Home)
                            hthomesub.Add(21, Me.lblSub21Home)
                            hthomesub.Add(22, Me.lblSub22Home)
                            hthomesub.Add(23, Me.lblSub23Home)
                            hthomesub.Add(24, Me.lblSub24Home)
                            hthomesub.Add(25, Me.lblSub25Home)
                            hthomesub.Add(26, Me.lblSub26Home)
                            hthomesub.Add(27, Me.lblSub27Home)
                            hthomesub.Add(28, Me.lblSub28Home)
                            hthomesub.Add(29, Me.lblSub29Home)
                            hthomesub.Add(30, Me.lblSub30Home)
                            hthomesub.Add(31, Me.lblSubPosition1Home)
                            hthomesub.Add(32, Me.lblSubPosition2Home)
                            hthomesub.Add(33, Me.lblSubPosition3Home)
                            hthomesub.Add(34, Me.lblSubPosition4Home)
                            hthomesub.Add(35, Me.lblSubPosition5Home)
                            hthomesub.Add(36, Me.lblSubPosition6Home)
                            hthomesub.Add(37, Me.lblSubPosition7Home)
                            hthomesub.Add(38, Me.lblSubPosition8Home)
                            hthomesub.Add(39, Me.lblSubPosition9Home)
                            hthomesub.Add(40, Me.lblSubPosition10Home)
                            hthomesub.Add(41, Me.lblSubPosition11Home)
                            hthomesub.Add(42, Me.lblSubPosition12Home)
                            hthomesub.Add(43, Me.lblSubPosition13Home)
                            hthomesub.Add(44, Me.lblSubPosition14Home)
                            hthomesub.Add(45, Me.lblSubPosition15Home)
                            hthomesub.Add(46, Me.lblSubPosition16Home)
                            hthomesub.Add(47, Me.lblSubPosition17Home)
                            hthomesub.Add(48, Me.lblSubPosition18Home)
                            hthomesub.Add(49, Me.lblSubPosition19Home)
                            hthomesub.Add(50, Me.lblSubPosition20Home)
                            hthomesub.Add(51, Me.lblSubPosition21Home)
                            hthomesub.Add(52, Me.lblSubPosition22Home)
                            hthomesub.Add(53, Me.lblSubPosition23Home)
                            hthomesub.Add(54, Me.lblSubPosition24Home)
                            hthomesub.Add(55, Me.lblSubPosition25Home)
                            hthomesub.Add(56, Me.lblSubPosition26Home)
                            hthomesub.Add(57, Me.lblSubPosition27Home)
                            hthomesub.Add(58, Me.lblSubPosition28Home)
                            hthomesub.Add(59, Me.lblSubPosition29Home)
                            hthomesub.Add(60, Me.lblSubPosition30Home)

                            If m_objTeamSetup.RosterInfo.Tables("Home").Rows.Count > 0 Then
                                drs = m_objTeamSetup.RosterInfo.Tables("Home").Select("STARTING_POSITION > 11", "STARTING_POSITION ASC")
                                If drs.Length > 0 Then
                                    Dim intLoopEnd As Integer = CInt(drs(drs.Length - 1).Item("STARTING_POSITION"))
                                    Dim intBenPlayerCount As Integer = intLoopEnd - 11
                                    If intBenPlayerCount > 7 Then
                                        pnlHomeBench.AutoScroll = True
                                    Else
                                        pnlHomeBench.AutoScroll = False
                                    End If
                                Else
                                    pnlHomeBench.AutoScroll = False
                                End If

                                drs = dsPlayers.Tables("Home").Select("STARTING_POSITION > 11", "" & strSortOrder & " ASC")
                                If drs.Length > 0 Then
                                    For Each dr As DataRow In drs
                                        'If intLblCntr < 30 Then
                                        strDisplayUniform = dr.Item("DISPLAY_UNIFORM_NUMBER").ToString
                                        strLastName = dr.Item("LAST_NAME").ToString
                                        If dr.Item("MONIKER").ToString = "" Then
                                            strMoniker = ""
                                        Else
                                            strMoniker = dr.Item("MONIKER").ToString.Chars(0)
                                        End If

                                        strPositionAbbrev = dr.Item("POSITION_ABBREV").ToString
                                        intLblCntr += 1
                                        lbl = CType(hthomesub.Item(intLblCntr), Label)
                                        lblPos = CType(hthomesub.Item(intLblCntr + 30), Label)

                                        If strDisplayUniform = "" Then
                                            lbl.Text = "       " & strLastName & Space(1) & strMoniker
                                            lblPos.Text = strPositionAbbrev
                                        Else
                                            If strDisplayUniform.Length = 1 Then
                                                lbl.Text = " " & strDisplayUniform & ".   " & strLastName & Space(1) & strMoniker
                                                lblPos.Text = strPositionAbbrev
                                            Else
                                                lbl.Text = " " & strDisplayUniform & ". " & strLastName & Space(1) & strMoniker
                                                lblPos.Text = strPositionAbbrev
                                            End If
                                        End If
                                        lbl.Tag = dr.Item("PLAYER_ID").ToString()
                                        lbl.Visible = True
                                        lblPos.Visible = True
                                        While lbl.PreferredWidth > lbl.Width
                                            strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                            If strDisplayUniform = "" Then
                                                lbl.Text = "       " & strLastName & "." & Space(1) & strMoniker
                                            ElseIf strDisplayUniform.Length = 1 Then
                                                lbl.Text = " " & strDisplayUniform & ".   " & strLastName & "." & Space(1) & strMoniker
                                            Else
                                                lbl.Text = " " & strDisplayUniform & ". " & strLastName & "." & Space(1) & strMoniker
                                            End If
                                        End While
                                        'End If

                                    Next
                                End If
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FILLING OF VISITING TEAM PLAYERS
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LabelAwayTeamTagged()
        Try
            Dim intLblCntr As Integer = 0
            Dim lbl As Label
            Dim lblPos As Label
            Dim strDisplayUniform As String
            Dim strLastName As String
            Dim strMoniker As String
            Dim strPositionAbbrev As String

            Dim htVisit As New Hashtable
            Dim htVisitsub As New Hashtable
            Dim dsPlayers As New DataSet

            If m_objTeamSetup.RosterInfo IsNot Nothing Then
                If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                    If Not m_objTeamSetup.RosterInfo.Tables("Away") Is Nothing Then
                        If m_objTeamSetup.RosterInfo.Tables("Away").Rows.Count > 0 Then
                            dsPlayers = m_objTeamSetup.RosterInfo.Copy()
                            htVisit.Add(1, Me.lblPlayer1Visit)
                            htVisit.Add(2, Me.lblPlayer2Visit)
                            htVisit.Add(3, Me.lblPlayer3Visit)
                            htVisit.Add(4, Me.lblPlayer4Visit)
                            htVisit.Add(5, Me.lblPlayer5Visit)
                            htVisit.Add(6, Me.lblPlayer6Visit)
                            htVisit.Add(7, Me.lblPlayer7Visit)
                            htVisit.Add(8, Me.lblPlayer8Visit)
                            htVisit.Add(9, Me.lblPlayer9Visit)
                            htVisit.Add(10, Me.lblPlayer10Visit)
                            htVisit.Add(11, Me.lblPlayer11Visit)
                            htVisit.Add(12, Me.lblPosition1Visit)
                            htVisit.Add(13, Me.lblPosition2Visit)
                            htVisit.Add(14, Me.lblPosition3Visit)
                            htVisit.Add(15, Me.lblPosition4Visit)
                            htVisit.Add(16, Me.lblPosition5Visit)
                            htVisit.Add(17, Me.lblPosition6Visit)
                            htVisit.Add(18, Me.lblPosition7Visit)
                            htVisit.Add(19, Me.lblPosition8Visit)
                            htVisit.Add(20, Me.lblPosition9Visit)
                            htVisit.Add(21, Me.lblPosition10Visit)
                            htVisit.Add(22, Me.lblPosition11Visit)

                            If dsPlayers Is Nothing Then
                                Exit Sub
                            End If

                            Dim drs() As DataRow

                            Dim strSortOrder As String = ""
                            If m_objLoginDetails.strSortOrder = "Uniform" Then

                                strSortOrder = "SORT_NUMBER"
                            ElseIf m_objLoginDetails.strSortOrder = "Position" Then
                                strSortOrder = "STARTING_POSITION"

                            ElseIf m_objLoginDetails.strSortOrder = "Last Name" Then
                                strSortOrder = "LAST_NAME"
                            End If

                            drs = dsPlayers.Tables("Away").Select("STARTING_POSITION < 12", "" & strSortOrder & " ASC")
                            If drs.Length > 0 Then
                                For Each dr As DataRow In drs
                                    strDisplayUniform = dr.Item("DISPLAY_UNIFORM_NUMBER").ToString
                                    strLastName = dr.Item("LAST_NAME").ToString
                                    If dr.Item("MONIKER").ToString = "" Then
                                        strMoniker = ""
                                    Else
                                        strMoniker = dr.Item("MONIKER").ToString.Chars(0)
                                    End If

                                    strPositionAbbrev = dr.Item("POSITION_ABBREV").ToString
                                    intLblCntr += 1
                                    lbl = CType(htVisit.Item(intLblCntr), Label)
                                    lbl.Tag = dr.Item("PLAYER_ID").ToString()
                                    lbl.Visible = True
                                    lblPos = CType(htVisit.Item(intLblCntr + 11), Label)
                                    If strDisplayUniform = "" Then
                                        lbl.Text = "       " & strLastName & Space(1) & strMoniker
                                        lblPos.Text = strPositionAbbrev
                                    ElseIf strDisplayUniform.Length = 1 Then
                                        lbl.Text = " " & strDisplayUniform & ".   " & strLastName & Space(1) & strMoniker
                                        lblPos.Text = strPositionAbbrev
                                    Else
                                        lbl.Text = " " & strDisplayUniform & ". " & strLastName & Space(1) & strMoniker
                                        lblPos.Text = strPositionAbbrev
                                    End If

                                    While lbl.PreferredWidth > lbl.Width
                                        strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                        If strDisplayUniform = "" Then
                                            lbl.Text = "       " & strLastName & "." & Space(1) & strMoniker
                                        ElseIf strDisplayUniform.Length = 1 Then
                                            lbl.Text = " " & strDisplayUniform & ".   " & strLastName & "." & Space(1) & strMoniker
                                        Else
                                            lbl.Text = " " & strDisplayUniform & ". " & strLastName & "." & Space(1) & strMoniker
                                        End If
                                    End While
                                Next
                            End If

                            intLblCntr = 0
                            htVisitsub.Add(1, Me.lblSub1Visit)
                            htVisitsub.Add(2, Me.lblSub2Visit)
                            htVisitsub.Add(3, Me.lblSub3Visit)
                            htVisitsub.Add(4, Me.lblSub4Visit)
                            htVisitsub.Add(5, Me.lblSub5Visit)
                            htVisitsub.Add(6, Me.lblSub6Visit)
                            htVisitsub.Add(7, Me.lblSub7Visit)
                            htVisitsub.Add(8, Me.lblSub8Visit)
                            htVisitsub.Add(9, Me.lblSub9Visit)
                            htVisitsub.Add(10, Me.lblSub10Visit)
                            htVisitsub.Add(11, Me.lblSub11Visit)
                            htVisitsub.Add(12, Me.lblSub12Visit)
                            htVisitsub.Add(13, Me.lblSub13Visit)
                            htVisitsub.Add(14, Me.lblSub14Visit)
                            htVisitsub.Add(15, Me.lblSub15Visit)
                            htVisitsub.Add(16, Me.lblSub16Visit)
                            htVisitsub.Add(17, Me.lblSub17Visit)
                            htVisitsub.Add(18, Me.lblSub18Visit)
                            htVisitsub.Add(19, Me.lblSub19Visit)
                            htVisitsub.Add(20, Me.lblSub20Visit)
                            htVisitsub.Add(21, Me.lblSub21Visit)
                            htVisitsub.Add(22, Me.lblSub22Visit)
                            htVisitsub.Add(23, Me.lblSub23Visit)
                            htVisitsub.Add(24, Me.lblSub24Visit)
                            htVisitsub.Add(25, Me.lblSub25Visit)
                            htVisitsub.Add(26, Me.lblSub26Visit)
                            htVisitsub.Add(27, Me.lblSub27Visit)
                            htVisitsub.Add(28, Me.lblSub28Visit)
                            htVisitsub.Add(29, Me.lblSub29Visit)
                            htVisitsub.Add(30, Me.lblSub30Visit)
                            htVisitsub.Add(31, Me.lblSubPosition1Visit)
                            htVisitsub.Add(32, Me.lblSubPosition2Visit)
                            htVisitsub.Add(33, Me.lblSubPosition3Visit)
                            htVisitsub.Add(34, Me.lblSubPosition4Visit)
                            htVisitsub.Add(35, Me.lblSubPosition5Visit)
                            htVisitsub.Add(36, Me.lblSubPosition6Visit)
                            htVisitsub.Add(37, Me.lblSubPosition7Visit)
                            htVisitsub.Add(38, Me.lblSubPosition8Visit)
                            htVisitsub.Add(39, Me.lblSubPosition9Visit)
                            htVisitsub.Add(40, Me.lblSubPosition10Visit)
                            htVisitsub.Add(41, Me.lblSubPosition11Visit)
                            htVisitsub.Add(42, Me.lblSubPosition12Visit)
                            htVisitsub.Add(43, Me.lblSubPosition13Visit)
                            htVisitsub.Add(44, Me.lblSubPosition14Visit)
                            htVisitsub.Add(45, Me.lblSubPosition15Visit)
                            htVisitsub.Add(46, Me.lblSubPosition16Visit)
                            htVisitsub.Add(47, Me.lblSubPosition17Visit)
                            htVisitsub.Add(48, Me.lblSubPosition18Visit)
                            htVisitsub.Add(49, Me.lblSubPosition19Visit)
                            htVisitsub.Add(50, Me.lblSubPosition20Visit)
                            htVisitsub.Add(51, Me.lblSubPosition21Visit)
                            htVisitsub.Add(52, Me.lblSubPosition22Visit)
                            htVisitsub.Add(53, Me.lblSubPosition23Visit)
                            htVisitsub.Add(54, Me.lblSubPosition24Visit)
                            htVisitsub.Add(55, Me.lblSubPosition25Visit)
                            htVisitsub.Add(56, Me.lblSubPosition26Visit)
                            htVisitsub.Add(57, Me.lblSubPosition27Visit)
                            htVisitsub.Add(58, Me.lblSubPosition28Visit)
                            htVisitsub.Add(59, Me.lblSubPosition29Visit)
                            htVisitsub.Add(60, Me.lblSubPosition30Visit)

                            If m_objTeamSetup.RosterInfo.Tables("Away").Rows.Count > 0 Then
                                drs = m_objTeamSetup.RosterInfo.Tables("Away").Select("STARTING_POSITION > 11", "STARTING_POSITION ASC")
                                If drs.Length > 0 Then
                                    Dim intLoopEnd As Integer = CInt(drs(drs.Length - 1).Item("STARTING_POSITION"))
                                    Dim intBenPlayerCount As Integer = intLoopEnd - 11
                                    If intBenPlayerCount > 7 Then
                                        pnlVisitBench.AutoScroll = True
                                    Else
                                        pnlVisitBench.AutoScroll = False
                                    End If
                                Else
                                    pnlVisitBench.AutoScroll = False
                                End If

                                drs = dsPlayers.Tables("Away").Select("STARTING_POSITION > 11", "" & strSortOrder & " ASC")
                                If drs.Length > 0 Then
                                    For Each dr As DataRow In drs
                                        strDisplayUniform = dr.Item("DISPLAY_UNIFORM_NUMBER").ToString
                                        strLastName = dr.Item("LAST_NAME").ToString
                                        If dr.Item("MONIKER").ToString = "" Then
                                            strMoniker = ""
                                        Else
                                            strMoniker = dr.Item("MONIKER").ToString.Chars(0)
                                        End If

                                        strPositionAbbrev = dr.Item("POSITION_ABBREV").ToString
                                        intLblCntr += 1
                                        lbl = CType(htVisitsub.Item(intLblCntr), Label)
                                        lbl.Tag = dr.Item("PLAYER_ID").ToString()
                                        lbl.Visible = True
                                        lblPos = CType(htVisitsub.Item(intLblCntr + 30), Label)
                                        lblPos.Visible = True

                                        If strDisplayUniform = "" Then
                                            lbl.Text = "       " & strLastName & Space(1) & strMoniker
                                            lblPos.Text = strPositionAbbrev
                                        ElseIf strDisplayUniform.Length = 1 Then
                                            lbl.Text = " " & strDisplayUniform & ".   " & strLastName & Space(1) & strMoniker
                                            lblPos.Text = strPositionAbbrev
                                        Else
                                            lbl.Text = " " & strDisplayUniform & ". " & strLastName & Space(1) & strMoniker
                                            lblPos.Text = strPositionAbbrev
                                        End If

                                        While lbl.PreferredWidth > lbl.Width
                                            strLastName = strLastName.Substring(0, strLastName.Length - 1)
                                            If strDisplayUniform = "" Then
                                                lbl.Text = "       " & strLastName & "." & Space(1) & strMoniker
                                            ElseIf strDisplayUniform.Length = 1 Then
                                                lbl.Text = " " & strDisplayUniform & ".   " & strLastName & "." & Space(1) & strMoniker
                                            Else
                                                lbl.Text = " " & strDisplayUniform & ". " & strLastName & "." & Space(1) & strMoniker
                                            End If
                                        End While
                                    Next
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetBenchPlayerslablesVisibility()
        Try
            Me.lblSub8Visit.Visible = False
            Me.lblSub9Visit.Visible = False
            Me.lblSub10Visit.Visible = False
            Me.lblSub11Visit.Visible = False
            Me.lblSub12Visit.Visible = False
            Me.lblSub13Visit.Visible = False
            Me.lblSub14Visit.Visible = False
            Me.lblSub15Visit.Visible = False
            Me.lblSub17Visit.Visible = False
            Me.lblSub18Visit.Visible = False
            Me.lblSub19Visit.Visible = False
            Me.lblSub20Visit.Visible = False
            Me.lblSub21Visit.Visible = False
            Me.lblSub22Visit.Visible = False
            Me.lblSub23Visit.Visible = False
            Me.lblSub24Visit.Visible = False
            Me.lblSub25Visit.Visible = False
            Me.lblSub26Visit.Visible = False
            Me.lblSub27Visit.Visible = False
            Me.lblSub28Visit.Visible = False
            Me.lblSub29Visit.Visible = False
            Me.lblSub30Visit.Visible = False

            Me.lblSubPosition8Visit.Visible = False
            Me.lblSubPosition9Visit.Visible = False
            Me.lblSubPosition10Visit.Visible = False
            Me.lblSubPosition11Visit.Visible = False
            Me.lblSubPosition12Visit.Visible = False
            Me.lblSubPosition13Visit.Visible = False
            Me.lblSubPosition14Visit.Visible = False
            Me.lblSubPosition15Visit.Visible = False
            Me.lblSubPosition17Visit.Visible = False
            Me.lblSubPosition18Visit.Visible = False
            Me.lblSubPosition19Visit.Visible = False
            Me.lblSubPosition20Visit.Visible = False
            Me.lblSubPosition21Visit.Visible = False
            Me.lblSubPosition22Visit.Visible = False
            Me.lblSubPosition23Visit.Visible = False
            Me.lblSubPosition24Visit.Visible = False
            Me.lblSubPosition25Visit.Visible = False
            Me.lblSubPosition26Visit.Visible = False
            Me.lblSubPosition27Visit.Visible = False
            Me.lblSubPosition28Visit.Visible = False
            Me.lblSubPosition29Visit.Visible = False
            Me.lblSubPosition30Visit.Visible = False

            Me.lblSub8Home.Visible = False
            Me.lblSub9Home.Visible = False
            Me.lblSub10Home.Visible = False
            Me.lblSub11Home.Visible = False
            Me.lblSub12Home.Visible = False
            Me.lblSub13Home.Visible = False
            Me.lblSub14Home.Visible = False
            Me.lblSub15Home.Visible = False
            Me.lblSub17Home.Visible = False
            Me.lblSub18Home.Visible = False
            Me.lblSub19Home.Visible = False
            Me.lblSub20Home.Visible = False
            Me.lblSub21Home.Visible = False
            Me.lblSub22Home.Visible = False
            Me.lblSub23Home.Visible = False
            Me.lblSub24Home.Visible = False
            Me.lblSub25Home.Visible = False
            Me.lblSub26Home.Visible = False
            Me.lblSub27Home.Visible = False
            Me.lblSub28Home.Visible = False
            Me.lblSub29Home.Visible = False
            Me.lblSub30Home.Visible = False

            Me.lblSubPosition8Home.Visible = False
            Me.lblSubPosition9Home.Visible = False
            Me.lblSubPosition10Home.Visible = False
            Me.lblSubPosition11Home.Visible = False
            Me.lblSubPosition12Home.Visible = False
            Me.lblSubPosition13Home.Visible = False
            Me.lblSubPosition14Home.Visible = False
            Me.lblSubPosition15Home.Visible = False
            Me.lblSubPosition17Home.Visible = False
            Me.lblSubPosition18Home.Visible = False
            Me.lblSubPosition19Home.Visible = False
            Me.lblSubPosition20Home.Visible = False
            Me.lblSubPosition21Home.Visible = False
            Me.lblSubPosition22Home.Visible = False
            Me.lblSubPosition23Home.Visible = False
            Me.lblSubPosition24Home.Visible = False
            Me.lblSubPosition25Home.Visible = False
            Me.lblSubPosition26Home.Visible = False
            Me.lblSubPosition27Home.Visible = False
            Me.lblSubPosition28Home.Visible = False
            Me.lblSubPosition29Home.Visible = False
            Me.lblSubPosition30Home.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' CLEARS THE TEXT EXITS IN THE LABELS
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearVisitTeamLabeltext()
        Try
            lblPlayer1Visit.Text = ""
            lblPlayer2Visit.Text = ""
            lblPlayer3Visit.Text = ""
            lblPlayer4Visit.Text = ""
            lblPlayer5Visit.Text = ""
            lblPlayer6Visit.Text = ""
            lblPlayer7Visit.Text = ""
            lblPlayer8Visit.Text = ""
            lblPlayer9Visit.Text = ""
            lblPlayer10Visit.Text = ""
            lblPlayer11Visit.Text = ""

            lblPosition1Visit.Text = ""
            lblPosition2Visit.Text = ""
            lblPosition3Visit.Text = ""
            lblPosition4Visit.Text = ""
            lblPosition5Visit.Text = ""
            lblPosition6Visit.Text = ""
            lblPosition7Visit.Text = ""
            lblPosition8Visit.Text = ""
            lblPosition9Visit.Text = ""
            lblPosition10Visit.Text = ""
            lblPosition11Visit.Text = ""

            lblSub1Visit.Text = ""
            lblSub2Visit.Text = ""
            lblSub3Visit.Text = ""
            lblSub4Visit.Text = ""
            lblSub5Visit.Text = ""
            lblSub6Visit.Text = ""
            lblSub7Visit.Text = ""
            lblSub8Visit.Text = ""
            lblSub9Visit.Text = ""
            lblSub10Visit.Text = ""

            lblSub11Visit.Text = ""
            lblSub12Visit.Text = ""
            lblSub13Visit.Text = ""
            lblSub14Visit.Text = ""
            lblSub15Visit.Text = ""
            lblSub16Visit.Text = ""
            lblSub17Visit.Text = ""
            lblSub18Visit.Text = ""
            lblSub19Visit.Text = ""
            lblSub20Visit.Text = ""

            lblSub21Visit.Text = ""
            lblSub22Visit.Text = ""
            lblSub23Visit.Text = ""
            lblSub24Visit.Text = ""
            lblSub25Visit.Text = ""
            lblSub26Visit.Text = ""
            lblSub27Visit.Text = ""
            lblSub28Visit.Text = ""
            lblSub29Visit.Text = ""
            lblSub30Visit.Text = ""

            lblSubPosition1Visit.Text = ""
            lblSubPosition2Visit.Text = ""
            lblSubPosition3Visit.Text = ""
            lblSubPosition4Visit.Text = ""
            lblSubPosition5Visit.Text = ""
            lblSubPosition6Visit.Text = ""
            lblSubPosition7Visit.Text = ""
            lblSubPosition8Visit.Text = ""
            lblSubPosition9Visit.Text = ""
            lblSubPosition10Visit.Text = ""
            lblSubPosition11Visit.Text = ""
            lblSubPosition12Visit.Text = ""
            lblSubPosition13Visit.Text = ""
            lblSubPosition14Visit.Text = ""
            lblSubPosition15Visit.Text = ""
            lblSubPosition16Visit.Text = ""
            lblSubPosition17Visit.Text = ""
            lblSubPosition18Visit.Text = ""
            lblSubPosition19Visit.Text = ""
            lblSubPosition20Visit.Text = ""
            lblSubPosition21Visit.Text = ""
            lblSubPosition22Visit.Text = ""
            lblSubPosition23Visit.Text = ""
            lblSubPosition24Visit.Text = ""
            lblSubPosition25Visit.Text = ""
            lblSubPosition26Visit.Text = ""
            lblSubPosition27Visit.Text = ""
            lblSubPosition28Visit.Text = ""
            lblSubPosition29Visit.Text = ""
            lblSubPosition30Visit.Text = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' CLEARS THE TEXT EXITS IN THE LABELS
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearHomeTeamLabeltext()
        Try
            lblPlayer1Home.Text = ""
            lblPlayer2Home.Text = ""
            lblPlayer3Home.Text = ""
            lblPlayer4Home.Text = ""
            lblPlayer5Home.Text = ""
            lblPlayer6Home.Text = ""
            lblPlayer7Home.Text = ""
            lblPlayer8Home.Text = ""
            lblPlayer9Home.Text = ""
            lblPlayer10Home.Text = ""
            lblPlayer11Home.Text = ""

            lblPosition1Home.Text = ""
            lblPosition2Home.Text = ""
            lblPosition3Home.Text = ""
            lblPosition4Home.Text = ""
            lblPosition5Home.Text = ""
            lblPosition6Home.Text = ""
            lblPosition7Home.Text = ""
            lblPosition8Home.Text = ""
            lblPosition9Home.Text = ""
            lblPosition10Home.Text = ""
            lblPosition11Home.Text = ""

            lblSub1Home.Text = ""
            lblSub2Home.Text = ""
            lblSub3Home.Text = ""
            lblSub4Home.Text = ""
            lblSub5Home.Text = ""
            lblSub6Home.Text = ""
            lblSub7Home.Text = ""
            lblSub8Home.Text = ""
            lblSub9Home.Text = ""
            lblSub10Home.Text = ""

            lblSub11Home.Text = ""
            lblSub12Home.Text = ""
            lblSub13Home.Text = ""
            lblSub14Home.Text = ""
            lblSub15Home.Text = ""
            lblSub16Home.Text = ""
            lblSub17Home.Text = ""
            lblSub18Home.Text = ""
            lblSub19Home.Text = ""
            lblSub20Home.Text = ""

            lblSub21Home.Text = ""
            lblSub22Home.Text = ""
            lblSub23Home.Text = ""
            lblSub24Home.Text = ""
            lblSub25Home.Text = ""
            lblSub26Home.Text = ""
            lblSub27Home.Text = ""
            lblSub28Home.Text = ""
            lblSub29Home.Text = ""
            lblSub30Home.Text = ""

            lblSubPosition1Home.Text = ""
            lblSubPosition2Home.Text = ""
            lblSubPosition3Home.Text = ""
            lblSubPosition4Home.Text = ""
            lblSubPosition5Home.Text = ""
            lblSubPosition6Home.Text = ""
            lblSubPosition7Home.Text = ""
            lblSubPosition8Home.Text = ""
            lblSubPosition9Home.Text = ""
            lblSubPosition10Home.Text = ""
            lblSubPosition11Home.Text = ""
            lblSubPosition12Home.Text = ""
            lblSubPosition13Home.Text = ""
            lblSubPosition14Home.Text = ""
            lblSubPosition15Home.Text = ""
            lblSubPosition16Home.Text = ""
            lblSubPosition17Home.Text = ""
            lblSubPosition18Home.Text = ""
            lblSubPosition19Home.Text = ""
            lblSubPosition20Home.Text = ""
            lblSubPosition21Home.Text = ""
            lblSubPosition22Home.Text = ""
            lblSubPosition23Home.Text = ""
            lblSubPosition24Home.Text = ""
            lblSubPosition25Home.Text = ""
            lblSubPosition26Home.Text = ""
            lblSubPosition27Home.Text = ""
            lblSubPosition28Home.Text = ""
            lblSubPosition29Home.Text = ""
            lblSubPosition30Home.Text = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetTeamButtonColor()
        Try
            'TEAMS COLOR
            Dim dsTeamColor As New DataSet
            dsTeamColor = m_objModule1Main.GetTeamColors(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            Dim clrHome, clrAway As Color
            If dsTeamColor.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(dsTeamColor.Tables(0).Rows(0).Item("HOME_COLOR")) Then
                    clrHome = System.Drawing.Color.FromArgb(CInt(dsTeamColor.Tables(0).Rows(0).Item("HOME_COLOR")))
                    m_objModule1PBP.SetTeamColor(True, clrHome)
                    frmMain.SetTeamColor(True, clrHome)
                Else
                    frmMain.SetTeamColor(True, Color.Empty)
                End If
                If Not IsDBNull(dsTeamColor.Tables(0).Rows(0).Item("AWAY_COLOR")) Then
                    clrAway = System.Drawing.Color.FromArgb(CInt(dsTeamColor.Tables(0).Rows(0).Item("AWAY_COLOR")))
                    m_objModule1PBP.SetTeamColor(False, clrAway)
                    frmMain.SetTeamColor(False, clrAway)
                Else
                    frmMain.SetTeamColor(False, Color.Empty)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' CLEARS THE CONTROLS IN THE MAIN SCREEN WHICH IS USED TO DISPLAY THE DATA
    ''' WHEN MULTIPLE GAME SELECTION IS ALLOWED
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearMainScreenControls()
        Try
            lvwHomeBookings.Items.Clear()
            lvwHomePenalties.Items.Clear()
            lvwHomeSubstitutions.Items.Clear()
            lvwHomeGoals.Items.Clear()
            lvwVisitBookings.Items.Clear()
            lvwVisitPenalties.Items.Clear()
            lvwVisitSubstitutions.Items.Clear()
            lvwVisitGoals.Items.Clear()
            ClearHomeTeamLabeltext()
            ClearVisitTeamLabeltext()
            ClearOfficialAndMatchInfo()
            If m_objGameDetails.ModuleID = 2 And m_objGameDetails.CoverageLevel = 2 Then
                lblHomeSubstitutions.Visible = False
                lblVisitSubstitutions.Visible = False
                lblMissedHomePenalties.Visible = False
                lblMissedVisitPenalties.Visible = False

                lvwHomeSubstitutions.Visible = False
                lvwVisitSubstitutions.Visible = False
                lvwHomePenalties.Visible = False
                lvwVisitPenalties.Visible = False

                lklNewHomeSub.Visible = False
                lklEditHomeSubstitutions.Visible = False
                lklNewVisitSub.Visible = False
                lklEditVisitSubstitutions.Visible = False
                lklNewHomePenalty.Visible = False
                lklNewVisitPenalty.Visible = False
                lklEditHomePenalty.Visible = False
                lklEditVisitPenalty.Visible = False
            Else
                lblHomeSubstitutions.Visible = True
                lblVisitSubstitutions.Visible = True
                lblMissedHomePenalties.Visible = True
                lblMissedVisitPenalties.Visible = True

                lvwHomeSubstitutions.Visible = True
                lvwVisitSubstitutions.Visible = True
                lvwHomePenalties.Visible = True
                lvwVisitPenalties.Visible = True

                lklNewHomeSub.Visible = True
                lklEditHomeSubstitutions.Visible = True
                lklNewVisitSub.Visible = True
                lklEditVisitSubstitutions.Visible = True
                lklNewHomePenalty.Visible = True
                lklNewVisitPenalty.Visible = True
                lklEditHomePenalty.Visible = True
                lklEditVisitPenalty.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Clears the existing text in the Listviews of Official and Match
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearOfficialAndMatchInfo()
        Try
            lvwOfficials.Items(0).SubItems(1).Text = ""
            lvwOfficials.Items(1).SubItems(1).Text = ""
            lvwOfficials.Items(2).SubItems(1).Text = ""
            lvwOfficials.Items(3).SubItems(1).Text = ""
            lvwOfficials.Items(4).SubItems(1).Text = ""
            lvwMatchInformation.Items(0).SubItems(1).Text = ""
            lvwMatchInformation.Items(1).SubItems(1).Text = ""
            lvwMatchInformation.Items(2).SubItems(1).Text = ""
            lvwMatchInformation.Items(3).SubItems(1).Text = ""
            lvwMatchInformation.Items(4).SubItems(1).Text = ""
            If m_objGameDetails.ModuleID = 2 Then
                lvwMatchInformation.Items(4).SubItems(0).Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Diplays  the Official and Match info on the Main Form
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InsertOfficialAndMatchInfo()
        Try
            Dim dsOfficialandMatch As New DataSet
            dsOfficialandMatch = m_objGameSetup.GetOfficialAndMatchInfo(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.LeagueID)
            If Not dsOfficialandMatch Is Nothing Then
                If dsOfficialandMatch.Tables(0).Rows.Count > 0 Then
                    lvwOfficials.Items(0).SubItems(1).Text = dsOfficialandMatch.Tables(0).Rows(0).Item("REFEREE").ToString
                    lvwOfficials.Items(1).SubItems(1).Text = dsOfficialandMatch.Tables(0).Rows(0).Item("LINESMAN").ToString
                    lvwOfficials.Items(2).SubItems(1).Text = dsOfficialandMatch.Tables(0).Rows(0).Item("LINESMAN1").ToString
                    lvwOfficials.Items(3).SubItems(1).Text = dsOfficialandMatch.Tables(0).Rows(0).Item("FORTHOFFICIAL").ToString
                    lvwOfficials.Items(4).SubItems(1).Text = dsOfficialandMatch.Tables(0).Rows(0).Item("FIFTHOFICIAL").ToString
                    lvwOfficials.Items(5).SubItems(1).Text = dsOfficialandMatch.Tables(0).Rows(0).Item("SIXTHOFICIAL").ToString
                End If
                If dsOfficialandMatch.Tables(1).Rows.Count > 0 Then
                    If Not IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("KICKOFF")) Then
                        Dim dt As DateTime = CDate(dsOfficialandMatch.Tables(1).Rows(0).Item("KICKOFF").ToString)
                        If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                            lvwMatchInformation.Items(0).SubItems(1).Text = dt.Subtract(m_objGameDetails.SysDateDiff).ToString("hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                        Else
                            lvwMatchInformation.Items(0).SubItems(1).Text = dt.Add(m_objGameDetails.SysDateDiff).ToString("hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                        End If

                        ' lvwMatchInformation.Items(0).SubItems(1).Text = dsOfficialandMatch.Tables(1).Rows(0).Item("KICKOFF").ToString
                        lvwMatchInformation.Items(1).SubItems(1).Text = dsOfficialandMatch.Tables(1).Rows(0).Item("FIELD_NAME").ToString

                        If Not dsOfficialandMatch.Tables(1).Rows(0).Item("FIELD_NAME").ToString Is DBNull.Value Then
                            m_objGameDetails.FieldName = dsOfficialandMatch.Tables(1).Rows(0).Item("FIELD_NAME").ToString
                        End If
                        Dim strAttendence As String = String.Empty
                        If Not IsDBNull(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")) Then
                            strAttendence = CInt(dsOfficialandMatch.Tables(1).Rows(0).Item("ATTENDANCE")).ToString("N")
                            strAttendence = strAttendence.Substring(0, Len(strAttendence) - 3)
                        End If

                        lvwMatchInformation.Items(2).SubItems(1).Text = strAttendence

                        Dim TsTimeSpan As TimeSpan
                        Dim strDelayTime As String
                        'If m_objGameDetails.IsRestartGame = True Or m_objGameDetails.ModuleID = 2 Then
                        m_objGameDetails.DelayTime = CDate(CDate(m_objGameDetails.GameDate).Date & " " & CDate(dsOfficialandMatch.Tables(1).Rows(0).Item("KICKOFF")))
                        'End If
                        TsTimeSpan = m_objGameDetails.DelayTime.Subtract(CDate(m_objGameDetails.GameDate))
                        If TsTimeSpan.Hours <> 0 And TsTimeSpan.Minutes <> 0 Then
                            strDelayTime = TsTimeSpan.Hours & " Hr " & TsTimeSpan.Minutes & " Min"
                        ElseIf TsTimeSpan.Hours <> 0 And TsTimeSpan.Minutes = 0 Then
                            strDelayTime = TsTimeSpan.Hours & " Hr "
                        ElseIf TsTimeSpan.Hours = 0 And TsTimeSpan.Minutes <> 0 Then
                            strDelayTime = TsTimeSpan.Minutes & " Min"
                        Else
                            strDelayTime = ""
                        End If
                        lvwMatchInformation.Items(3).SubItems(1).Text = strDelayTime

                        If dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE").ToString = CChar("L") Then
                            lvwMatchInformation.Items(4).SubItems(1).Text = "Left"
                            m_objGameDetails.IsHomeTeamOnLeft = True
                            'm_objGameDetails.IsHomeTeamOnLeftSwitch = True
                        ElseIf dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE").ToString = CChar("R") Then
                            lvwMatchInformation.Items(4).SubItems(1).Text = "Right"
                            If m_objGameDetails.IsRestartGame = True Or m_objclsGameDetails.CurrentPeriod = 0 Then
                                m_objGameDetails.IsHomeTeamOnLeft = False
                                m_objGameDetails.isHomeDirectionLeft = False
                            End If
                        Else
                            lvwMatchInformation.Items(4).SubItems(1).Text = ""
                            m_objGameDetails.IsHomeTeamOnLeft = True
                        End If

                        If m_objGameDetails.ModuleID = 2 Then
                            lvwMatchInformation.Items(4).SubItems(1).Text = ""
                            lvwMatchInformation.Items(4).Text = ""
                        End If

                        If m_objGameDetails.IsRestartGame = True Or m_objGameDetails.ModuleID = 2 Then
                            If dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE").ToString = CChar("L") Then
                                m_objGameDetails.IsHomeTeamOnLeft = True
                                'm_objGameDetails.IsHomeTeamOnLeftSwitch = True
                            Else
                                m_objGameDetails.IsHomeTeamOnLeft = False
                                'm_objGameDetails.IsHomeTeamOnLeftSwitch = False
                            End If
                        End If
                    End If
                End If
                If dsOfficialandMatch.Tables(2).Rows.Count > 0 Then 'Arindam 21-Mar-12 multi reporter
                    If Not IsDBNull(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")) Then
                        If CStr(dsOfficialandMatch.Tables(2).Rows(0).Item("HOME_START_SIDE_ET")).Trim <> "" Then
                            If dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE").ToString = CChar("L") Then
                                lvwMatchInformation.Items(4).SubItems(1).Text = "Left"
                                'If m_objGameDetails.IsRestartGame = True Then
                                m_objGameDetails.IsHomeTeamOnLeft = True
                                ' m_objGameDetails.IsHomeTeamOnLeftSwitch = True
                                'End If
                                'm_objGameDetails.IsHomeTeamOnLeftSwitch = True
                            ElseIf dsOfficialandMatch.Tables(1).Rows(0).Item("HOME_START_SIDE").ToString = CChar("R") Then
                                lvwMatchInformation.Items(4).SubItems(1).Text = "Right"
                                If m_objGameDetails.IsRestartGame = True Then
                                    m_objGameDetails.IsHomeTeamOnLeft = False
                                    m_objGameDetails.isHomeDirectionLeft = False
                                    'm_objGameDetails.IsHomeTeamOnLeftSwitch = False
                                End If
                                'Else
                                '    lvwMatchInformation.Items(4).SubItems(1).Text = ""
                                '    m_objGameDetails.IsHomeTeamOnLeft = True
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetSides()
        Try

            If m_objGameDetails.IsHomeTeamOnLeft = True Then
                If m_objGameDetails.IsHomeTeamOnLeftSwitch = False And pnlHome.Left = LEFT_PLAYER_PANEL_LEFT Then
                    pnlHome.Left = RIGHT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = LEFT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = RIGHT_EVENTS_LVW
                    lvwHomeGoals.Left = RIGHT_EVENTS_LVW
                    lvwHomePenalties.Left = RIGHT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = RIGHT_EVENTS_LVW

                    lklNewHomeGoal.Left = RIGHT_EVENTS_LVW
                    lklNewHomeBooking.Left = RIGHT_EVENTS_LVW
                    lklNewHomePenalty.Left = RIGHT_EVENTS_LVW
                    lklNewHomeSub.Left = RIGHT_EVENTS_LVW

                    lklEditHomeGoals.Left = RIGHT_LINK_EDIT
                    lklEditHomeBookings.Left = RIGHT_LINK_EDIT
                    lklEditHomePenalty.Left = RIGHT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = RIGHT_LINK_EDIT

                    lblHomeSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedHomePenalties.Left = RIGHT_EVENTS_LVW
                    lblHomeGoals.Left = RIGHT_EVENTS_LVW

                    lvwVisitBookings.Left = LEFT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lvwVisitGoals.Left = LEFT_EVENTS_LVW
                    lvwVisitPenalties.Left = LEFT_EVENTS_LVW

                    lklNewVisitGoal.Left = LEFT_EVENTS_LVW
                    lklNewVisitBooking.Left = LEFT_EVENTS_LVW
                    lklNewVisitPenalty.Left = LEFT_EVENTS_LVW
                    lklNewVisitSub.Left = LEFT_EVENTS_LVW

                    lklEditVisitGoals.Left = LEFT_LINK_EDIT
                    lklEditVisitBookings.Left = LEFT_LINK_EDIT
                    lklEditVisitPenalty.Left = LEFT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = LEFT_LINK_EDIT

                    lblVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = LEFT_EVENTS_LVW
                    lblVisitGoals.Left = LEFT_EVENTS_LVW

                ElseIf m_objGameDetails.IsHomeTeamOnLeftSwitch = True Then

                    pnlHome.Left = LEFT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = RIGHT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = LEFT_EVENTS_LVW
                    lvwHomeGoals.Left = LEFT_EVENTS_LVW
                    lvwHomePenalties.Left = LEFT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = LEFT_EVENTS_LVW

                    lklNewHomeGoal.Left = LEFT_EVENTS_LVW
                    lklNewHomeBooking.Left = LEFT_EVENTS_LVW
                    lklNewHomePenalty.Left = LEFT_EVENTS_LVW
                    lklNewHomeSub.Left = LEFT_EVENTS_LVW

                    lklEditHomeGoals.Left = LEFT_LINK_EDIT
                    lklEditHomeBookings.Left = LEFT_LINK_EDIT
                    lklEditHomePenalty.Left = LEFT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = LEFT_LINK_EDIT

                    lblHomeSubstitutions.Left = LEFT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedHomePenalties.Left = LEFT_EVENTS_LVW

                    lvwVisitBookings.Left = RIGHT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lvwVisitGoals.Left = RIGHT_EVENTS_LVW
                    lvwVisitPenalties.Left = RIGHT_EVENTS_LVW

                    lklNewVisitGoal.Left = RIGHT_EVENTS_LVW
                    lklNewVisitBooking.Left = RIGHT_EVENTS_LVW
                    lklNewVisitPenalty.Left = RIGHT_EVENTS_LVW
                    lklNewVisitSub.Left = RIGHT_EVENTS_LVW

                    lklEditVisitGoals.Left = RIGHT_LINK_EDIT
                    lklEditVisitBookings.Left = RIGHT_LINK_EDIT
                    lklEditVisitPenalty.Left = RIGHT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = RIGHT_LINK_EDIT

                    lblVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = RIGHT_EVENTS_LVW
                    lblVisitGoals.Left = RIGHT_EVENTS_LVW

                End If
            Else
                If m_objGameDetails.IsHomeTeamOnLeftSwitch = False And pnlHome.Left = LEFT_PLAYER_PANEL_LEFT Then

                    pnlHome.Left = RIGHT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = LEFT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = LEFT_EVENTS_LVW
                    lvwHomeGoals.Left = LEFT_EVENTS_LVW
                    lvwHomePenalties.Left = LEFT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = LEFT_EVENTS_LVW

                    lklNewHomeGoal.Left = LEFT_EVENTS_LVW
                    lklNewHomeBooking.Left = LEFT_EVENTS_LVW
                    lklNewHomePenalty.Left = LEFT_EVENTS_LVW
                    lklNewHomeSub.Left = LEFT_EVENTS_LVW

                    lklEditHomeGoals.Left = LEFT_LINK_EDIT
                    lklEditHomeBookings.Left = LEFT_LINK_EDIT
                    lklEditHomePenalty.Left = LEFT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = LEFT_LINK_EDIT

                    lblHomeSubstitutions.Left = LEFT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedHomePenalties.Left = LEFT_EVENTS_LVW

                    lvwVisitBookings.Left = RIGHT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lvwVisitGoals.Left = RIGHT_EVENTS_LVW
                    lvwVisitPenalties.Left = RIGHT_EVENTS_LVW

                    lklNewVisitGoal.Left = RIGHT_EVENTS_LVW
                    lklNewVisitBooking.Left = RIGHT_EVENTS_LVW
                    lklNewVisitPenalty.Left = RIGHT_EVENTS_LVW
                    lklNewVisitSub.Left = RIGHT_EVENTS_LVW

                    lklEditVisitGoals.Left = RIGHT_LINK_EDIT
                    lklEditVisitBookings.Left = RIGHT_LINK_EDIT
                    lklEditVisitPenalty.Left = RIGHT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = RIGHT_LINK_EDIT

                    lblVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = RIGHT_EVENTS_LVW
                    lblVisitGoals.Left = RIGHT_EVENTS_LVW

                ElseIf m_objGameDetails.IsHomeTeamOnLeftSwitch = True Then
                    pnlHome.Left = LEFT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = RIGHT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = RIGHT_EVENTS_LVW
                    lvwHomeGoals.Left = RIGHT_EVENTS_LVW
                    lvwHomePenalties.Left = RIGHT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = RIGHT_EVENTS_LVW

                    lklNewHomeGoal.Left = RIGHT_EVENTS_LVW
                    lklNewHomeBooking.Left = RIGHT_EVENTS_LVW
                    lklNewHomePenalty.Left = RIGHT_EVENTS_LVW
                    lklNewHomeSub.Left = RIGHT_EVENTS_LVW

                    lklEditHomeGoals.Left = RIGHT_LINK_EDIT
                    lklEditHomeBookings.Left = RIGHT_LINK_EDIT
                    lklEditHomePenalty.Left = RIGHT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = RIGHT_LINK_EDIT

                    lblHomeSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedHomePenalties.Left = RIGHT_EVENTS_LVW
                    lblHomeGoals.Left = RIGHT_EVENTS_LVW

                    lvwVisitBookings.Left = LEFT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lvwVisitGoals.Left = LEFT_EVENTS_LVW
                    lvwVisitPenalties.Left = LEFT_EVENTS_LVW

                    lklNewVisitGoal.Left = LEFT_EVENTS_LVW
                    lklNewVisitBooking.Left = LEFT_EVENTS_LVW
                    lklNewVisitPenalty.Left = LEFT_EVENTS_LVW
                    lklNewVisitSub.Left = LEFT_EVENTS_LVW

                    lklEditVisitGoals.Left = LEFT_LINK_EDIT
                    lklEditVisitBookings.Left = LEFT_LINK_EDIT
                    lklEditVisitPenalty.Left = LEFT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = LEFT_LINK_EDIT

                    lblVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = LEFT_EVENTS_LVW
                    lblVisitGoals.Left = LEFT_EVENTS_LVW

                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ArrangeListViews()
        Try
            Dim intVerticalDistance As Integer = 5
            Dim intVerticalFreeSpace As Integer

            lvwOfficials.Scrollable = True
            lvwMatchInformation.Scrollable = True
            lvwHomeGoals.Scrollable = True
            lvwVisitGoals.Scrollable = True
            lvwHomeBookings.Scrollable = True
            lvwVisitBookings.Scrollable = True
            lvwHomeSubstitutions.Scrollable = True
            lvwVisitSubstitutions.Scrollable = True
            lvwHomePenalties.Scrollable = True
            lvwVisitPenalties.Scrollable = True

            'set the height of all list views to avoid scrolling
            For Each ctrl As Control In pnlStats.Controls
                If ctrl.GetType.Equals(GetType(ListView)) Then
                    Dim lvwTemp As ListView
                    lvwTemp = CType(ctrl, ListView)
                    If lvwTemp.Items.Count > 0 Then
                        lvwTemp.Items(lvwTemp.Items.Count - 1).EnsureVisible()
                        While lvwTemp.TopItem Is Nothing
                            lvwTemp.Height = lvwTemp.Height + 1
                        End While
                        While Not lvwTemp.TopItem.Equals(lvwTemp.Items(0))
                            lvwTemp.Height = lvwTemp.Height + 1
                        End While
                    End If
                End If
            Next

            'equalize the height of home and visit listviews
            If lvwOfficials.Height > lvwMatchInformation.Height Then
                lvwMatchInformation.Height = lvwOfficials.Height
            ElseIf lvwMatchInformation.Height > lvwOfficials.Height Then
                lvwOfficials.Height = lvwMatchInformation.Height
            End If

            If lvwHomeGoals.Height > lvwVisitGoals.Height Then
                lvwVisitGoals.Height = lvwHomeGoals.Height
            ElseIf lvwVisitGoals.Height > lvwHomeGoals.Height Then
                lvwHomeGoals.Height = lvwVisitGoals.Height
            End If

            If lvwHomeBookings.Height > lvwVisitBookings.Height Then
                lvwVisitBookings.Height = lvwHomeBookings.Height
            ElseIf lvwVisitBookings.Height > lvwHomeBookings.Height Then
                lvwHomeBookings.Height = lvwVisitBookings.Height
            End If

            If lvwHomeSubstitutions.Height > lvwVisitSubstitutions.Height Then
                lvwVisitSubstitutions.Height = lvwHomeSubstitutions.Height
            ElseIf lvwVisitSubstitutions.Height > lvwHomeSubstitutions.Height Then
                lvwHomeSubstitutions.Height = lvwVisitSubstitutions.Height
            End If

            If lvwHomePenalties.Height > lvwVisitPenalties.Height Then
                lvwVisitPenalties.Height = lvwHomePenalties.Height
            ElseIf lvwVisitPenalties.Height > lvwHomePenalties.Height Then
                lvwHomePenalties.Height = lvwVisitPenalties.Height
            End If

            'if the sum of listview heights is less than panel height, increase listview heights
            intVerticalFreeSpace = lvwOfficials.Height + lvwHomeGoals.Height + lvwHomeBookings.Height + lvwHomeSubstitutions.Height + lvwHomePenalties.Height
            intVerticalFreeSpace = intVerticalFreeSpace + lblOfficials.Height + lblHomeGoals.Height + lblHomeBookingExpulsions.Height + lblHomeSubstitutions.Height + lblMissedHomePenalties.Height
            intVerticalFreeSpace = intVerticalFreeSpace + intVerticalDistance * 6
            intVerticalFreeSpace = pnlStats.Height - intVerticalFreeSpace

            If intVerticalFreeSpace > intVerticalDistance Then
                intVerticalFreeSpace = CInt(intVerticalFreeSpace / 4)
                lvwHomeGoals.Height = lvwHomeGoals.Height + intVerticalFreeSpace
                lvwVisitGoals.Height = lvwVisitGoals.Height + intVerticalFreeSpace
                lvwHomeBookings.Height = lvwHomeBookings.Height + intVerticalFreeSpace
                lvwVisitBookings.Height = lvwVisitBookings.Height + intVerticalFreeSpace
                lvwHomeSubstitutions.Height = lvwHomeSubstitutions.Height + intVerticalFreeSpace
                lvwVisitSubstitutions.Height = lvwVisitSubstitutions.Height + intVerticalFreeSpace
                lvwHomePenalties.Height = lvwHomePenalties.Height + intVerticalFreeSpace
                lvwVisitPenalties.Height = lvwVisitPenalties.Height + intVerticalFreeSpace
            End If

            'set the listview positions
            lblHomeGoals.Top = lvwOfficials.Top + lvwOfficials.Height + intVerticalDistance
            lklNewHomeGoal.Top = lblHomeGoals.Top
            lklEditHomeGoals.Top = lblHomeGoals.Top
            lvwHomeGoals.Top = lblHomeGoals.Top + lblHomeGoals.Height

            lblVisitGoals.Top = lblHomeGoals.Top
            lklNewVisitGoal.Top = lblVisitGoals.Top
            lklEditVisitGoals.Top = lblVisitGoals.Top
            lvwVisitGoals.Top = lblVisitGoals.Top + lblVisitGoals.Height

            lblHomeBookingExpulsions.Top = lvwHomeGoals.Top + lvwHomeGoals.Height + intVerticalDistance
            lklNewHomeBooking.Top = lblHomeBookingExpulsions.Top
            lklEditHomeBookings.Top = lblHomeBookingExpulsions.Top
            lvwHomeBookings.Top = lblHomeBookingExpulsions.Top + lblHomeBookingExpulsions.Height

            lblVisitBookingExpulsions.Top = lblHomeBookingExpulsions.Top
            lklNewVisitBooking.Top = lblVisitBookingExpulsions.Top
            lklEditVisitBookings.Top = lblVisitBookingExpulsions.Top
            lvwVisitBookings.Top = lblVisitBookingExpulsions.Top + lblVisitBookingExpulsions.Height

            lblHomeSubstitutions.Top = lvwHomeBookings.Top + lvwHomeBookings.Height + intVerticalDistance
            lklNewHomeSub.Top = lblHomeSubstitutions.Top
            lklEditHomeSubstitutions.Top = lblHomeSubstitutions.Top
            lvwHomeSubstitutions.Top = lblHomeSubstitutions.Top + lblHomeSubstitutions.Height

            lblVisitSubstitutions.Top = lblHomeSubstitutions.Top
            lklNewVisitSub.Top = lblVisitSubstitutions.Top
            lklEditVisitSubstitutions.Top = lblVisitSubstitutions.Top
            lvwVisitSubstitutions.Top = lblVisitSubstitutions.Top + lblVisitSubstitutions.Height

            lblMissedHomePenalties.Top = lvwHomeSubstitutions.Top + lvwHomeSubstitutions.Height + intVerticalDistance
            lklNewHomePenalty.Top = lblMissedHomePenalties.Top
            lklEditHomePenalty.Top = lblMissedHomePenalties.Top
            lvwHomePenalties.Top = lblMissedHomePenalties.Top + lblMissedHomePenalties.Height

            lblMissedVisitPenalties.Top = lblMissedHomePenalties.Top
            lklNewVisitPenalty.Top = lblMissedVisitPenalties.Top
            lklEditVisitPenalty.Top = lblMissedVisitPenalties.Top
            lvwVisitPenalties.Top = lblMissedVisitPenalties.Top + lblMissedVisitPenalties.Height

            lvwOfficials.Scrollable = False
            lvwMatchInformation.Scrollable = False
            lvwHomeGoals.Scrollable = False
            lvwVisitGoals.Scrollable = False
            lvwHomeBookings.Scrollable = False
            lvwVisitBookings.Scrollable = False
            lvwHomeSubstitutions.Scrollable = False
            lvwVisitSubstitutions.Scrollable = False
            lvwHomePenalties.Scrollable = False
            lvwVisitPenalties.Scrollable = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Public methods "

    ''' <summary>
    ''' FILLING HOME AND AWAY TEAM PLAYERS IN THE LABELS
    '''  </summary>
    ''' <remarks></remarks>
    Public Sub LabelsTagged()
        Try
            ClearHomeTeamLabeltext()
            ClearVisitTeamLabeltext()
            LabelHomeTeamTagged()
            LabelAwayTeamTagged()
            UpdateManager()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Inserts the Official and Match info
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InsertOfficialsandMatchInfo()
        Try
            ClearOfficialAndMatchInfo()
            InsertOfficialAndMatchInfo()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub GetCurrentPlayersAfterRestart()
        Try
            'CLEARS THE ROSTER TABLES PALCED IN THE PROPERTY DATSTET
            If Not m_objTeamSetup.RosterInfo Is Nothing Then
                m_objTeamSetup.RosterInfo.Tables.Clear()
            End If

            If m_objGameDetails.CoverageLevel = 2 Then
                m_objGameDetails.TeamRosters = m_objGeneral.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objclsGameDetails.languageid)
            Else
                'FETCH ROSTERS FOR THAT PARTICULAR GAME
                Dim DsPlayers As DataSet = m_objModule1Main.GetRosterInfo(m_objGameDetails.GameCode, m_objGameDetails.LeagueID, m_objclsGameDetails.languageid)
                If DsPlayers.Tables(0).Rows.Count > 0 Then
                    DsPlayers.Tables(0).TableName = "Home"
                    DsPlayers.Tables(1).TableName = "Away"

                    'CREATING TWO MORE TABLES WHICH IS USED TO HOLD THE CURRENT PLAYERS
                    'PLAYING IN THE FIELD
                    Dim DsCurrentPlayer As DataSet
                    m_objTeamSetup.RosterInfo = DsPlayers.Copy()
                    DsCurrentPlayer = DsPlayers.Copy()
                    DsCurrentPlayer.Tables(0).TableName = "HomeCurrent"
                    DsCurrentPlayer.Tables(1).TableName = "AwayCurrent"

                    'Getting the current players from red card and sub events
                    Dim DsSubEvents As DataSet = m_objModule1Main.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    If DsSubEvents.Tables(0).Rows.Count > 0 Then
                        For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                            Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                                Case 7, 22
                                    For intTableCount As Integer = 0 To DsCurrentPlayer.Tables.Count - 1
                                        If Not IsDBNull(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) Then
                                            Dim DrPlayerOut() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) & "")
                                            If DrPlayerOut.Length > 0 Then
                                                If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = 7 Then
                                                    'RED CARD
                                                    DrPlayerOut(0).BeginEdit()
                                                    DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                                    DrPlayerOut(0).EndEdit()
                                                    DrPlayerOut(0).AcceptChanges()
                                                Else
                                                    'SUBSTITUTION
                                                    Dim DrPlayerIn() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) & "")
                                                    If DrPlayerIn.Length > 0 Then
                                                        DrPlayerIn(0).BeginEdit()
                                                        DrPlayerIn(0).Item("STARTING_POSITION") = DrPlayerOut(0).Item("STARTING_POSITION")
                                                        DrPlayerIn(0).EndEdit()

                                                        DrPlayerOut(0).BeginEdit()
                                                        DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                                        DrPlayerOut(0).EndEdit()

                                                        DrPlayerIn(0).AcceptChanges()
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Next
                            End Select
                        Next
                    End If

                    'FILLS THE ROSTERS INTO THE PROPERTY DATASET
                    m_objTeamSetup.RosterInfo.Merge(DsCurrentPlayer)
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub RestartExistingGame()
        Try

            'CLEARS THE CONTROLS
            If m_objGameDetails.ModuleID <> 3 Then
                ClearMainScreenControls()
                'getting current players after restart
                GetCurrentPlayersAfterRestart()
                'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
                LabelHomeTeamTagged()
                LabelAwayTeamTagged()
                UpdateManager()
                'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
                FetchMainScreenPBPEvents()
                'GAME SETUP INFO
                InsertOfficialAndMatchInfo()
                'PERIOD,SCORES,TEAM NAMES DISPLAY IS DONE IN THIS FUNCTION
                frmMain.FillFormControls()
                frmMain.ResetClockAndScores()
                SetTeamButtonColor()
                SetUniformColor()
                frmMain.GetPenaltyShootoutScore()
                CheckforAbandoned()
            Else
                frmMain.FillFormControls()
                SetTeamButtonColor()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CheckforAbandoned()
        Try
            Dim abanData As DataSet = m_objGeneral.getDataForEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, clsGameDetails.ABANDONED, m_objGameDetails.ModuleID)

            If abanData.Tables(0).Rows.Count > 0 Then
                lklNewHomeBooking.Enabled = False
                lklNewHomeGoal.Enabled = False
                lklNewHomePenalty.Enabled = False
                lklNewHomeSub.Enabled = False
                lklNewVisitBooking.Enabled = False
                lklNewVisitGoal.Enabled = False
                lklNewVisitPenalty.Enabled = False
                lklNewVisitSub.Enabled = False
                If frmMain.UdcRunningClock1.URCIsRunning Then
                    frmMain.UdcRunningClock1.URCToggle()
                End If
                If frmMain.UdcRunningClock1.URCIsRunning Then
                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                Else
                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                End If

                frmMain.SetClockControl(False)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub FillMainMatchPlayers()
        Try
            'getting current players after restart
            GetCurrentPlayersAfterRestart()

            ClearHomeTeamLabeltext()
            ClearVisitTeamLabeltext()
            'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
            LabelHomeTeamTagged()
            LabelAwayTeamTagged()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FillPlayersInLabel(ByVal dtplayers As DataTable, ByVal pnlPlayers As Panel, ByVal team As String, ByVal typePlayer As String)
        Try
            clsUtility.ClearPanelLabelText(pnlPlayers)
            Dim lblPlayer As Label
            Dim lblPlayerPosition As Label
            Dim playerOrder As Integer = 1
            Dim playerCollection = (From player In dtplayers.AsEnumerable()
                                       Where player.Field(Of String)("TEAM") = team _
                                       AndAlso player.Field(Of String)("TypePlayer") = typePlayer
                                       Order By player.Field(Of String)(m_objLoginDetails.strSortOrder)
                                       Select player = player.Field(Of String)("player"),
                                               positionAbbrevation = player.Field(Of String)("Position_Abbreviation")
                                       ).ToList()
            For Each plyrr In playerCollection
                lblPlayer = CType(pnlPlayers.Controls.Item("lbl" & typePlayer & playerOrder & team), Label)
                lblPlayerPosition = CType(pnlPlayers.Controls.Item(lblPlayer.Name.Replace("lblPlayer", "lblPosition").Replace("lblSub", "lblSubPosition")), Label)
                lblPlayer.Text = plyrr.player
                lblPlayerPosition.Text = plyrr.positionAbbrevation
                playerOrder = playerOrder + 1
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub FillPlayersInMainScreenw(ByVal dtplayers As DataTable)
        Try
            FillPlayersInLabel(dtplayers, pnlHome, "Home", "Player")
            FillPlayersInLabel(dtplayers, pnlHomeBench, "Home", "Sub")
            FillPlayersInLabel(dtplayers, pnlVisit, "Visit", "Player")
            FillPlayersInLabel(dtplayers, pnlVisitBench, "Visit", "Sub")
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Sub FillPlayersInMainScreen(ByVal dtplayers As DataTable)
        Try
            For Each ctrl As Label In pnlHome.Controls.OfType(Of Label)()
                ctrl.Text = String.Empty
            Next

            For Each ctrl As Label In pnlVisit.Controls.OfType(Of Label)()
                ctrl.Text = String.Empty
            Next

            Dim strSortOrder As String = ""
            If m_objLoginDetails.strSortOrder = "Uniform" Then
                strSortOrder = "SORT_NUMBER"
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", True)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
            ElseIf m_objLoginDetails.strSortOrder = "Position" Then
                strSortOrder = "STARTING_POSITION"
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", True)
            ElseIf m_objLoginDetails.strSortOrder = "Last Name" Then
                strSortOrder = "LAST_NAME"
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", True)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
            End If

            'home
            Dim drsHome() As DataRow = dtplayers.Select("STARTING_POSITION < 12 and TEAM_ID = " & m_objGameDetails.HomeTeamID, "" & strSortOrder & " ASC")
            If drsHome.Length > 0 Then
                DisplayPlayersInMainScreen(drsHome, "Home")
            End If

            'home sub
            Dim drsHomeSub() As DataRow = dtplayers.Select("STARTING_POSITION > 11 and TEAM_ID = " & m_objGameDetails.HomeTeamID, "" & strSortOrder & " ASC")
            If drsHomeSub.Length > 0 Then
                DisplayPlayersInMainScreen(drsHomeSub, "HomeSub")
            End If

            'away
            Dim drsAway() As DataRow = dtplayers.Select("STARTING_POSITION < 12 and TEAM_ID = " & m_objGameDetails.AwayTeamID, "" & strSortOrder & " ASC")
            If drsHome.Length > 0 Then
                DisplayPlayersInMainScreen(drsAway, "Visit")
            End If

            'away sub
            Dim drsAwaySub() As DataRow = dtplayers.Select("STARTING_POSITION > 11 and TEAM_ID = " & m_objGameDetails.AwayTeamID, "" & strSortOrder & " ASC")
            If drsHomeSub.Length > 0 Then
                DisplayPlayersInMainScreen(drsAwaySub, "VisitSub")
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function DisplayPlayersInMainScreen(ByVal drs() As DataRow, ByVal Source As String) As Boolean
        Try
            Dim intLblCntr As Integer = 0
            Dim lbl As Label
            Dim lblPos As Label
            Dim strLastName As String, intLoopEnd As Integer

            For Each dr As DataRow In drs
                intLblCntr += 1
                Select Case Source
                    Case "Home"
                        lbl = CType(pnlHome.Controls.Item("lblPlayer" & intLblCntr & "Home"), Label)
                        lblPos = CType(pnlHome.Controls.Item("lblPosition" & intLblCntr & "Home"), Label)
                    Case "HomeSub"
                        lbl = CType(pnlHomeBench.Controls.Item("lblSub" & intLblCntr & "Home"), Label)
                        lblPos = CType(pnlHomeBench.Controls.Item("lblSubPosition" & intLblCntr & "Home"), Label)
                        intLoopEnd = CInt(drs(drs.Length - 1).Item("STARTING_POSITION"))
                        Dim intBenPlayerCount As Integer = intLoopEnd - 11
                        If intBenPlayerCount > 7 Then
                            pnlHomeBench.AutoScroll = True
                        Else
                            pnlHomeBench.AutoScroll = False
                        End If
                    Case "Visit"
                        lbl = CType(pnlVisit.Controls.Item("lblPlayer" & intLblCntr & "Visit"), Label)
                        lblPos = CType(pnlVisit.Controls.Item("lblPosition" & intLblCntr & "Visit"), Label)
                    Case "VisitSub"
                        lbl = CType(pnlVisitBench.Controls.Item("lblSub" & intLblCntr & "Visit"), Label)
                        lblPos = CType(pnlVisitBench.Controls.Item("lblSubPosition" & intLblCntr & "Visit"), Label)
                        intLoopEnd = CInt(drs(drs.Length - 1).Item("STARTING_POSITION"))
                        Dim intBenPlayerCount As Integer = intLoopEnd - 11
                        If intBenPlayerCount > 7 Then
                            pnlVisitBench.AutoScroll = True
                        Else
                            pnlVisitBench.AutoScroll = False
                        End If
                End Select

                If dr.Item("DISPLAY_UNIFORM_NUMBER").ToString = "" Then
                    lbl.Text = "       " & dr.Item("LAST_NAME").ToString & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                    lblPos.Text = dr.Item("POSITION_ABBREV").ToString
                Else
                    If dr.Item("DISPLAY_UNIFORM_NUMBER").ToString.Length = 1 Then
                        lbl.Text = " " & dr.Item("DISPLAY_UNIFORM_NUMBER").ToString & ".   " & dr.Item("LAST_NAME").ToString & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                        lblPos.Text = dr.Item("POSITION_ABBREV").ToString
                    Else
                        lbl.Text = " " & dr.Item("DISPLAY_UNIFORM_NUMBER").ToString & ". " & dr.Item("LAST_NAME").ToString & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                        lblPos.Text = dr.Item("POSITION_ABBREV").ToString
                    End If
                End If
                lbl.Tag = dr.Item("PLAYER_ID").ToString()
                lbl.Visible = True
                lblPos.Visible = True
                If lbl.PreferredWidth > lbl.Width Then
                    strLastName = dr.Item("LAST_NAME").ToString.Substring(0, dr.Item("LAST_NAME").ToString.Length - 1)
                    If dr.Item("DISPLAY_UNIFORM_NUMBER").ToString = "" Then
                        lbl.Text = "       " & strLastName & "." & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                    ElseIf dr.Item("DISPLAY_UNIFORM_NUMBER").ToString.Length = 1 Then
                        lbl.Text = " " & dr.Item("DISPLAY_UNIFORM_NUMBER").ToString & ".   " & strLastName & "." & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                    Else
                        lbl.Text = " " & dr.Item("DISPLAY_UNIFORM_NUMBER").ToString & ". " & strLastName & "." & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                    End If
                    'TOPZ-1477 Player Name Missing on Main Page
                    If lbl.PreferredWidth > lbl.Width Then
                        If strLastName.Contains("-") Then
                            Dim lastname As String() = strLastName.Split("-")
                            lbl.Text = " " & dr.Item("DISPLAY_UNIFORM_NUMBER").ToString & ". " & lastname(0) & "." & Space(1) & If(dr.Item("MONIKER").ToString = "", "", dr.Item("MONIKER").ToString.Chars(0))
                        End If
                    End If
                End If
            Next
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub FillGameInfoInMainScreen(ByVal dtGameSetupInfo As DataTable)
        Try
            Dim endPeriod As Boolean = False
            lvwOfficials.Items(0).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("REFEREE").ToString
            lvwOfficials.Items(1).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("LINESMAN").ToString
            lvwOfficials.Items(2).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("LINESMAN1").ToString
            lvwOfficials.Items(3).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("FORTHOFFICIAL").ToString
            lvwOfficials.Items(4).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("FIFTHOFICIAL").ToString
            lvwOfficials.Items(5).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("SIXTHOFICIAL").ToString

            If Not IsDBNull(dtGameSetupInfo.Rows(0).Item("KICKOFF")) Then
                Dim dt As DateTime = CDate(dtGameSetupInfo.Rows(0).Item("KICKOFF").ToString)
                If m_objGameDetails.SysDateDiff.Ticks < 0 Then
                    lvwMatchInformation.Items(0).SubItems(1).Text = dt.Subtract(m_objGameDetails.SysDateDiff).ToString("hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                Else
                    lvwMatchInformation.Items(0).SubItems(1).Text = dt.Add(m_objGameDetails.SysDateDiff).ToString("hh:mm tt") & Space(1) & m_objGameDetails.TimeZoneName
                End If
            End If
            lvwMatchInformation.Items(1).SubItems(1).Text = dtGameSetupInfo.Rows(0).Item("FIELD_NAME").ToString

            Dim strAttendence As String = String.Empty
            If Not IsDBNull(dtGameSetupInfo.Rows(0).Item("ATTENDANCE")) Then
                strAttendence = CInt(dtGameSetupInfo.Rows(0).Item("ATTENDANCE")).ToString("N")
                strAttendence = strAttendence.Substring(0, Len(strAttendence) - 3)
            End If
            lvwMatchInformation.Items(2).SubItems(1).Text = strAttendence
            Dim TsTimeSpan As TimeSpan
            Dim strDelayTime As String
            If IsDBNull(dtGameSetupInfo.Rows(0).Item("KICKOFF")) Then
                m_objGameDetails.DelayTime = CDate(CDate(m_objGameDetails.GameDate).Date)
            Else
                m_objGameDetails.DelayTime = CDate(CDate(m_objGameDetails.GameDate).Date & " " & CDate(dtGameSetupInfo.Rows(0).Item("KICKOFF")))
            End If

            TsTimeSpan = m_objGameDetails.DelayTime.Subtract(CDate(m_objGameDetails.GameDate))
            If TsTimeSpan.Hours > 0 Or TsTimeSpan.Minutes > 0 Then
                If TsTimeSpan.Hours <> 0 And TsTimeSpan.Minutes <> 0 Then
                    strDelayTime = TsTimeSpan.Hours & " Hr " & TsTimeSpan.Minutes & " Min"
                ElseIf TsTimeSpan.Hours <> 0 And TsTimeSpan.Minutes = 0 Then
                    strDelayTime = TsTimeSpan.Hours & " Hr "
                ElseIf TsTimeSpan.Hours = 0 And TsTimeSpan.Minutes <> 0 Then
                    strDelayTime = TsTimeSpan.Minutes & " Min"
                Else
                    strDelayTime = ""
                End If
                lvwMatchInformation.Items(3).SubItems(1).Text = strDelayTime
            End If
            'Home Start Side
            FillHomeStartSide(Convert.ToString(dtGameSetupInfo.Rows(0).Item("HOME_START_SIDE")), endPeriod)
            endPeriod = m_objGameDetails.CurrentPeriod = 2 And frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem")
            'ET Home Start Side
            If m_objGameDetails.CurrentPeriod > 2 Or endPeriod Then
                FillHomeStartSide(Convert.ToString(dtGameSetupInfo.Rows(0).Item("HOME_START_SIDE_ET")), endPeriod)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub FillHomeStartSide(ByVal startSide As String, ByVal endPeriod As Boolean)
        If startSide = CChar("L") Then
            lvwMatchInformation.Items(4).SubItems(1).Text = "Left"
            If m_objGameDetails.CurrentPeriod > 2 Or endPeriod Then
                m_objGameDetails.IsETHomeTeamOnLeft = True
            Else
                m_objGameDetails.IsHomeTeamOnLeft = True
            End If
            m_objGameDetails.IsHomeTeamOnLeftSwitch = True
        ElseIf startSide = CChar("R") Then
            lvwMatchInformation.Items(4).SubItems(1).Text = "Right"
            If m_objGameDetails.CurrentPeriod > 2 Or endPeriod Then
                m_objGameDetails.IsETHomeTeamOnLeft = False
            Else
                m_objGameDetails.IsHomeTeamOnLeft = False
            End If
            m_objGameDetails.IsHomeTeamOnLeftSwitch = False
        Else
            lvwMatchInformation.Items(4).SubItems(1).Text = ""
            m_objGameDetails.IsHomeTeamOnLeft = True
            m_objGameDetails.IsETHomeTeamOnLeft = True
        End If
    End Sub
    Public Sub FillCoachesInMainScreen(ByVal dtCoachInfo As DataTable)
        Try
            lblManagerHome.Text = ""
            lblManagerAway.Text = ""
            For i As Integer = 0 To dtCoachInfo.Rows.Count - 1
                If m_objGameDetails.HomeTeamID = CInt(dtCoachInfo.Rows(i).Item("TEAM_ID")) Then
                    lblManagerHome.Text = CStr(dtCoachInfo.Rows(i).Item("COACH_NAME"))
                    If lblManagerHome.PreferredWidth > lblManagerHome.Width Then
                        lblManagerHome.Text = CStr(dtCoachInfo.Rows(i).Item("LAST_NAME")) & "," & Space(1) & CStr(IIf(dtCoachInfo.Rows(i).Item("MONIKER").ToString = "", "", dtCoachInfo.Rows(i).Item("MONIKER").ToString.Chars(0)))
                    End If
                    If Not IsDBNull(dtCoachInfo.Rows(i).Item("START_FORMATION_ID")) Then
                        m_objGameDetails.HomeFormationId = CInt(dtCoachInfo.Rows(i).Item("START_FORMATION_ID"))
                    End If
                Else
                    lblManagerAway.Text = CStr(dtCoachInfo.Rows(i).Item("COACH_NAME"))
                    If lblManagerAway.PreferredWidth > lblManagerAway.Width Then
                        lblManagerAway.Text = CStr(dtCoachInfo.Rows(i).Item("LAST_NAME")) & "," & Space(1) & CStr(IIf(dtCoachInfo.Rows(i).Item("MONIKER").ToString = "", "", dtCoachInfo.Rows(i).Item("MONIKER").ToString.Chars(0)))
                    End If
                    If Not IsDBNull(dtCoachInfo.Rows(i).Item("START_FORMATION_ID")) Then
                        m_objGameDetails.AwayFormationId = CInt(dtCoachInfo.Rows(i).Item("START_FORMATION_ID"))
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FillMainMatchOfficials()
        Try
            'GAME SETUP INFO
            InsertOfficialAndMatchInfo()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FillMainMatchManagers()
        Try
            UpdateManager()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub RefreshData()
        Try
            'CLEARS THE CONTROLS
            ClearMainScreenControls()

            'getting current players after restart
            GetCurrentPlayersAfterRestart()

            'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
            LabelHomeTeamTagged()
            LabelAwayTeamTagged()

            UpdateManager()

            'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
            FetchMainScreenPBPEvents()
            'GAME SETUP INFO
            InsertOfficialAndMatchInfo()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' FETCHING THE MAIN SCREEN PBP EVENTS SUCH AS GOAL,EXPULSION,PENALTY AND BOOKINGS SO AS TO
    ''' DISPLAY IN THE RESPECTIVE LIST BOXES
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FetchMainScreenPBPEvents()
        Try
            Dim DsMainEvents As DataSet
            Dim drs() As DataRow
            DsMainEvents = m_objModule1Main.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            Dim DsPBPEvents As DataSet = DsMainEvents.Clone()
            'Dim drs() As DataRow = DsMainEvents.Tables(0).Select("", "SEQUENCE_NUMBER DESC")
            If DsMainEvents.Tables(0).Rows.Count > 0 Then
                If m_objclsGameDetails.CoverageLevel <= 2 Then
                    If DsMainEvents.Tables(0).Rows(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
                        drs = DsMainEvents.Tables(0).Select("EVENT_CODE_ID NOT IN (30,41)", "SEQUENCE_NUMBER DESC")
                    Else
                        drs = DsMainEvents.Tables(0).Select("", "SEQUENCE_NUMBER DESC")
                    End If
                Else
                    drs = DsMainEvents.Tables(0).Select("", "SEQUENCE_NUMBER DESC")
                End If

                For Each dr As DataRow In drs
                    DsPBPEvents.Tables(0).ImportRow(dr)
                Next

                'DISPLAYS EVENTS IN THE MAIN SCREEEN LISTBOX
                DisplayPBPEventsInMain(DsPBPEvents)
            End If

            'setEventButton()
            'HIGHLIGHT THE SELECTED MODULE 1 GAME EVERYTIME...IN THE MAIN SCREEN
            'If m_objGameDetails.ModuleID = 2 Then
            '    'For i As Integer = 0 To lvwMultiplegames.Items.Count - 1
            '    '    If m_objGameDetails.GameCode = CInt(lvwMultiplegames.Items(i).SubItems(0).Text) Then
            '    '        lvwMultiplegames.Items(i).Selected = True
            '    '        lvwMultiplegames.Select()
            '    '        Me.lvwMultiplegames.EnsureVisible(i)
            '    '        Exit For
            '    '    End If
            '    'Next
            '    For Each ctrl As Control In Me.pnlDisplaygames.Controls
            '        If ctrl.Visible = True And TypeOf ctrl Is RadioButton Then
            '            Dim rad As RadioButton = TryCast(ctrl, RadioButton)
            '            If CInt(ctrl.Tag) = m_objGameDetails.GameCode Then
            '                rad.Checked = True
            '            End If
            '        End If
            '    Next
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SwitchSides()
        Try
            pnlHome.Top = PLAYER_PANEL_TOP
            If m_objGameDetails.IsHomeTeamOnLeft Then
                If m_objGameDetails.IsHomeTeamOnLeftSwitch = False Then
                    pnlHome.Left = LEFT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = RIGHT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = LEFT_EVENTS_LVW
                    lvwHomeGoals.Left = LEFT_EVENTS_LVW
                    lvwHomePenalties.Left = LEFT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = LEFT_EVENTS_LVW

                    lklNewHomeGoal.Left = LEFT_EVENTS_LVW
                    lklNewHomeBooking.Left = LEFT_EVENTS_LVW
                    lklNewHomePenalty.Left = LEFT_EVENTS_LVW
                    lklNewHomeSub.Left = LEFT_EVENTS_LVW

                    lklEditHomeGoals.Left = LEFT_LINK_EDIT
                    lklEditHomeBookings.Left = LEFT_LINK_EDIT
                    lklEditHomePenalty.Left = LEFT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = LEFT_LINK_EDIT

                    lblHomeGoals.Left = LEFT_EVENTS_LVW
                    lblHomeSubstitutions.Left = LEFT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedHomePenalties.Left = LEFT_EVENTS_LVW

                    lvwVisitBookings.Left = RIGHT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lvwVisitGoals.Left = RIGHT_EVENTS_LVW
                    lvwVisitPenalties.Left = RIGHT_EVENTS_LVW

                    lklNewVisitGoal.Left = RIGHT_EVENTS_LVW
                    lklNewVisitBooking.Left = RIGHT_EVENTS_LVW
                    lklNewVisitPenalty.Left = RIGHT_EVENTS_LVW
                    lklNewVisitSub.Left = RIGHT_EVENTS_LVW

                    lklEditVisitGoals.Left = RIGHT_LINK_EDIT
                    lklEditVisitBookings.Left = RIGHT_LINK_EDIT
                    lklEditVisitPenalty.Left = RIGHT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = RIGHT_LINK_EDIT

                    lblVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = RIGHT_EVENTS_LVW
                    lblVisitGoals.Left = RIGHT_EVENTS_LVW

                    m_objGameDetails.IsHomeTeamOnLeftSwitch = True
                    LabelHomeTeamTagged()
                    LabelAwayTeamTagged()

                Else

                    pnlHome.Left = RIGHT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = LEFT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = RIGHT_EVENTS_LVW
                    lvwHomeGoals.Left = RIGHT_EVENTS_LVW
                    lvwHomePenalties.Left = RIGHT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = RIGHT_EVENTS_LVW

                    lklNewHomeGoal.Left = RIGHT_EVENTS_LVW
                    lklNewHomeBooking.Left = RIGHT_EVENTS_LVW
                    lklNewHomePenalty.Left = RIGHT_EVENTS_LVW
                    lklNewHomeSub.Left = RIGHT_EVENTS_LVW

                    lklEditHomeGoals.Left = RIGHT_LINK_EDIT
                    lklEditHomeBookings.Left = RIGHT_LINK_EDIT
                    lklEditHomePenalty.Left = RIGHT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = RIGHT_LINK_EDIT

                    lblHomeSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedHomePenalties.Left = RIGHT_EVENTS_LVW
                    lblHomeGoals.Left = RIGHT_EVENTS_LVW

                    lvwVisitBookings.Left = LEFT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lvwVisitGoals.Left = LEFT_EVENTS_LVW
                    lvwVisitPenalties.Left = LEFT_EVENTS_LVW

                    lklNewVisitGoal.Left = LEFT_EVENTS_LVW
                    lklNewVisitBooking.Left = LEFT_EVENTS_LVW
                    lklNewVisitPenalty.Left = LEFT_EVENTS_LVW
                    lklNewVisitSub.Left = LEFT_EVENTS_LVW

                    lklEditVisitGoals.Left = LEFT_LINK_EDIT
                    lklEditVisitBookings.Left = LEFT_LINK_EDIT
                    lklEditVisitPenalty.Left = LEFT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = LEFT_LINK_EDIT

                    lblVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = LEFT_EVENTS_LVW
                    lblVisitGoals.Left = LEFT_EVENTS_LVW

                    m_objGameDetails.IsHomeTeamOnLeftSwitch = False
                    LabelHomeTeamTagged()
                    LabelAwayTeamTagged()
                End If
            Else
                If m_objGameDetails.IsHomeTeamOnLeftSwitch = False Then

                    pnlHome.Left = LEFT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = RIGHT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = RIGHT_EVENTS_LVW
                    lvwHomeGoals.Left = RIGHT_EVENTS_LVW
                    lvwHomePenalties.Left = RIGHT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = RIGHT_EVENTS_LVW

                    lklNewHomeGoal.Left = RIGHT_EVENTS_LVW
                    lklNewHomeBooking.Left = RIGHT_EVENTS_LVW
                    lklNewHomePenalty.Left = RIGHT_EVENTS_LVW
                    lklNewHomeSub.Left = RIGHT_EVENTS_LVW

                    lklEditHomeGoals.Left = RIGHT_LINK_EDIT
                    lklEditHomeBookings.Left = RIGHT_LINK_EDIT
                    lklEditHomePenalty.Left = RIGHT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = RIGHT_LINK_EDIT

                    lblHomeSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedHomePenalties.Left = RIGHT_EVENTS_LVW
                    lblHomeGoals.Left = RIGHT_EVENTS_LVW

                    lvwVisitBookings.Left = LEFT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lvwVisitGoals.Left = LEFT_EVENTS_LVW
                    lvwVisitPenalties.Left = LEFT_EVENTS_LVW

                    lklNewVisitGoal.Left = LEFT_EVENTS_LVW
                    lklNewVisitBooking.Left = LEFT_EVENTS_LVW
                    lklNewVisitPenalty.Left = LEFT_EVENTS_LVW
                    lklNewVisitSub.Left = LEFT_EVENTS_LVW

                    lklEditVisitGoals.Left = LEFT_LINK_EDIT
                    lklEditVisitBookings.Left = LEFT_LINK_EDIT
                    lklEditVisitPenalty.Left = LEFT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = LEFT_LINK_EDIT

                    lblVisitSubstitutions.Left = LEFT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = LEFT_EVENTS_LVW
                    lblVisitGoals.Left = LEFT_EVENTS_LVW

                    m_objGameDetails.IsHomeTeamOnLeftSwitch = True
                    LabelHomeTeamTagged()
                    LabelAwayTeamTagged()
                Else
                    pnlHome.Left = RIGHT_PLAYER_PANEL_LEFT
                    pnlVisit.Left = LEFT_PLAYER_PANEL_LEFT

                    lvwHomeBookings.Left = LEFT_EVENTS_LVW
                    lvwHomeGoals.Left = LEFT_EVENTS_LVW
                    lvwHomePenalties.Left = LEFT_EVENTS_LVW
                    lvwHomeSubstitutions.Left = LEFT_EVENTS_LVW

                    lklNewHomeGoal.Left = LEFT_EVENTS_LVW
                    lklNewHomeBooking.Left = LEFT_EVENTS_LVW
                    lklNewHomePenalty.Left = LEFT_EVENTS_LVW
                    lklNewHomeSub.Left = LEFT_EVENTS_LVW

                    lklEditHomeGoals.Left = LEFT_LINK_EDIT
                    lklEditHomeBookings.Left = LEFT_LINK_EDIT
                    lklEditHomePenalty.Left = LEFT_LINK_EDIT
                    lklEditHomeSubstitutions.Left = LEFT_LINK_EDIT

                    lblHomeGoals.Left = LEFT_EVENTS_LVW
                    lblHomeSubstitutions.Left = LEFT_EVENTS_LVW
                    lblHomeBookingExpulsions.Left = LEFT_EVENTS_LVW
                    lblMissedHomePenalties.Left = LEFT_EVENTS_LVW

                    lvwVisitBookings.Left = RIGHT_EVENTS_LVW
                    lvwVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lvwVisitGoals.Left = RIGHT_EVENTS_LVW
                    lvwVisitPenalties.Left = RIGHT_EVENTS_LVW

                    lklNewVisitGoal.Left = RIGHT_EVENTS_LVW
                    lklNewVisitBooking.Left = RIGHT_EVENTS_LVW
                    lklNewVisitPenalty.Left = RIGHT_EVENTS_LVW
                    lklNewVisitSub.Left = RIGHT_EVENTS_LVW

                    lklEditVisitGoals.Left = RIGHT_LINK_EDIT
                    lklEditVisitBookings.Left = RIGHT_LINK_EDIT
                    lklEditVisitPenalty.Left = RIGHT_LINK_EDIT
                    lklEditVisitSubstitutions.Left = RIGHT_LINK_EDIT

                    lblVisitSubstitutions.Left = RIGHT_EVENTS_LVW
                    lblVisitBookingExpulsions.Left = RIGHT_EVENTS_LVW
                    lblMissedVisitPenalties.Left = RIGHT_EVENTS_LVW
                    lblVisitGoals.Left = RIGHT_EVENTS_LVW

                    m_objGameDetails.IsHomeTeamOnLeftSwitch = False
                    LabelHomeTeamTagged()
                    LabelAwayTeamTagged()
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Sub DisplayPBPMain()
    '    Try
    '        Dim dsPBP As DataSet
    '        dsPBP = m_objModule1PBP.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
    '        DisplayInListView(dsPBP)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Public Sub setEventButton()
        Try
            If (m_objGameDetails.CurrentPeriod > 0) And frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") Then
                'lklNewHomeBooking.Enabled = False
                lklNewHomeGoal.Enabled = False
                lklNewHomePenalty.Enabled = False
                'lklNewHomeSub.Enabled = False
                'lklNewVisitBooking.Enabled = False
                lklNewVisitGoal.Enabled = False
                lklNewVisitPenalty.Enabled = False
                'lklNewVisitSub.Enabled = False
                lklEditHomeBookings.Enabled = True
                lklEditHomeGoals.Enabled = True
                lklEditHomePenalty.Enabled = True
                lklEditHomeSubstitutions.Enabled = True

                lklEditVisitBookings.Enabled = True
                lklEditVisitGoals.Enabled = True
                lklEditVisitPenalty.Enabled = True
                lklEditVisitSubstitutions.Enabled = True
            End If
            If m_objGameDetails.CurrentPeriod = 0 Then
                lklNewHomeBooking.Enabled = False
                lklNewHomeGoal.Enabled = False
                lklNewHomePenalty.Enabled = False
                lklNewHomeSub.Enabled = False
                lklNewVisitBooking.Enabled = False
                lklNewVisitGoal.Enabled = False
                lklNewVisitPenalty.Enabled = False
                lklNewVisitSub.Enabled = False
                lklEditHomeBookings.Enabled = False
                lklEditHomeGoals.Enabled = False
                lklEditHomePenalty.Enabled = False
                lklEditHomeSubstitutions.Enabled = False

                lklEditVisitBookings.Enabled = False
                lklEditVisitGoals.Enabled = False
                lklEditVisitPenalty.Enabled = False
                lklEditVisitSubstitutions.Enabled = False
            End If
            If m_objGameDetails.CurrentPeriod = 4 Then
                Dim dsEndPeriodCount As DataSet
                'Dim endPeriodCount As Integer
                dsEndPeriodCount = m_objGeneral.GetPeriodEndCount(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, clsGameDetails.ENDPERIOD)
                If CInt(dsEndPeriodCount.Tables(0).Rows(0).Item("PeriodCount")) >= m_objGameDetails.CurrentPeriod Then
                    lklNewHomeBooking.Enabled = True
                    lklNewHomeGoal.Enabled = True
                    lklNewHomePenalty.Enabled = True
                    lklNewHomeSub.Enabled = True
                    lklNewVisitBooking.Enabled = True
                    lklNewVisitGoal.Enabled = True
                    lklNewVisitPenalty.Enabled = True
                    lklNewVisitSub.Enabled = True
                End If
            End If

            If (m_objGameDetails.CurrentPeriod > 0) And m_objGameDetails.ModuleID = 2 And frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") Then
                lklNewHomeBooking.Enabled = True
                lklNewHomeGoal.Enabled = True
                lklNewHomePenalty.Enabled = True
                lklNewHomeSub.Enabled = True
                lklNewVisitBooking.Enabled = True
                lklNewVisitGoal.Enabled = True
                lklNewVisitPenalty.Enabled = True
                lklNewVisitSub.Enabled = True
                lklEditHomeBookings.Enabled = True
                lklEditHomeGoals.Enabled = True
                lklEditHomePenalty.Enabled = True
                lklEditHomeSubstitutions.Enabled = True

                lklEditVisitBookings.Enabled = True
                lklEditVisitGoals.Enabled = True
                lklEditVisitPenalty.Enabled = True
                lklEditVisitSubstitutions.Enabled = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub InsertPlayByPlayData(ByVal EventID As Integer, ByVal Source As String)

        Dim dr As DataRow
        Dim intUniqueID As Integer
        Dim decSeqNo As Decimal
        Dim dsTempData As New DataSet
        Dim strTimeElapsed As Integer
        Dim drPBPData As DataRow
        Dim intTeamID As Integer
        Dim intAwayScore As Integer
        Dim intHomeScore As Integer
        Dim intOldAwayScore As Integer
        Dim dtTimeDiff As DateTime
        Try
            'To get the latest Score of the Teams
            If m_CurrentEventCode <> clsGameDetails.GAMESTART Then
                m_dsScores = m_objModule1Main.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                If m_dsScores.Tables(0).Rows.Count > 0 Then
                    intTeamID = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID"))
                    intAwayScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    intHomeScore = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                End If
            End If

            drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
            intUniqueID = m_objModule1Main.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            decSeqNo = Convert.ToDecimal(intUniqueID)
            drPBPData("GAME_CODE") = m_objGameDetails.GameCode
            drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
            drPBPData("UNIQUE_ID") = intUniqueID
            drPBPData("SEQUENCE_NUMBER") = decSeqNo
            drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod

            dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
            drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)

            drPBPData("RECORD_EDITED") = "N"
            drPBPData("EDIT_UID") = 0
            drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole
            drPBPData("PROCESSED") = "N"
            drPBPData("CONTINUATION") = "F"

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                drPBPData("DEMO_DATA") = "Y"
            Else
                drPBPData("DEMO_DATA") = "N"
            End If

            If EventID = clsGameDetails.CLOCK_INC Or EventID = clsGameDetails.CLOCK_DEC Or EventID = clsGameDetails.CLOCK_STOP Or EventID = clsGameDetails.CLOCK_START Then
                m_CurrentEventCode = EventID
                m_CurrentEvent = "Clock"
                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                If m_objGameDetails.IsDefaultGameClock Then
                    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                Else
                    drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                End If

                If m_objGameDetails.ModuleID = 3 Then
                    drPBPData("GAME_TIME_MS") = If(m_objGameDetails.IsDefaultGameClock, CalculateTimeElapsedBeforeInMilliSec(CInt(m_objUtility.ConvertMinuteToMillisecond(frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec))), CInt(m_objUtility.ConvertMinuteToMillisecond(frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec)))
                End If

                drPBPData("EVENT_CODE_ID") = EventID
                If (EventID = clsGameDetails.CLOCK_START And Source = "CLOCKSTART") Or (EventID = clsGameDetails.CLOCK_STOP And Source = "CLOCKSTOP") Then
                    drPBPData("ORIG_SEQ") = decSeqNo - 1
                End If
                drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore

            End If
           
            If EventID = clsGameDetails.HomeStartingLineups Then
                m_CurrentEventCode = EventID
                m_CurrentEvent = "Starting Lineups - Home"
                Dim TimeElapsed As Integer
                'shirley Sep 9 2010
                If m_objGameDetails.CoverageLevel > 3 Then
                    If m_objGameDetails.IsDefaultGameClock Then
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                    Else
                        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                    End If
                Else
                    'strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                    'drPBPData("TIME_ELAPSED") = strTimeElapsed
                    TimeElapsed = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    drPBPData("TIME_ELAPSED") = TimeElapsed
                End If
                drPBPData("GAME_TIME_MS") = TimeElapsed
                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore

            End If

            If EventID = clsGameDetails.AwatStartingLineups Then
                m_CurrentEventCode = EventID
                m_CurrentEvent = "Starting Lineups - Away"
                'strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                'drPBPData("TIME_ELAPSED") = strTimeElapsed
                'shirley Sep 9 2010
                Dim TimeElapsed As Integer
                If m_objGameDetails.CoverageLevel > 3 Then
                    If m_objGameDetails.IsDefaultGameClock Then
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                    Else
                        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                    End If
                Else
                    TimeElapsed = m_objGeneral.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    drPBPData("TIME_ELAPSED") = TimeElapsed
                End If
                drPBPData("GAME_TIME_MS") = TimeElapsed
                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("TEAM_ID") = m_objGameDetails.AwayTeamID
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
            End If

            If EventID = clsGameDetails.GAMESTART Then
                m_CurrentEventCode = EventID
                m_CurrentEvent = "Game Start"
                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                drPBPData("TIME_ELAPSED") = strTimeElapsed
                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
            End If

            If EventID = clsGameDetails.STARTPERIOD Then
                m_CurrentEvent = "Start Half"
                m_CurrentEventCode = EventID
                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                If m_objGameDetails.ModuleID = 1 Then
                    drPBPData("TIME_ELAPSED") = strTimeElapsed
                Else
                    If m_objGameDetails.CurrentPeriod = 1 Then
                        drPBPData("TIME_ELAPSED") = strTimeElapsed
                    Else
                        drPBPData("TIME_ELAPSED") = strTimeElapsed
                    End If
                End If
                drPBPData("EVENT_CODE_ID") = EventID
                If m_objGameDetails.CurrentPeriod = 1 Then
                    drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                    drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                    drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                Else
                    drPBPData("TEAM_ID") = intTeamID
                    drPBPData("OFFENSE_SCORE") = intHomeScore
                    drPBPData("DEFENSE_SCORE") = intAwayScore
                End If
            End If
            'End Period
            If EventID = clsGameDetails.ENDPERIOD Then
                m_CurrentEvent = "End Half"
                m_CurrentEventCode = EventID
                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                ''
                Dim CurrentMainClockTime As Integer
                If m_objGameDetails.IsDefaultGameClock Then
                    CurrentMainClockTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                Else
                    CurrentMainClockTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                End If

                'If m_objGameDetails.ModuleID = 2 Then
                If m_objGameDetails.CurrentPeriod = 1 Or m_objGameDetails.CurrentPeriod = 2 Then
                    Dim DsLastEventDetails As DataSet
                    Dim currTime As Integer

                    DsLastEventDetails = m_objModule1PBP.getLastEvent()
                    If m_objGameDetails.ModuleID = 1 Then
                        currTime = CurrentMainClockTime
                    Else
                        currTime = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                    End If
                    'drPBPData("TIME_ELAPSED") = FIRSTHALF
                    If currTime < FIRSTHALF Then
                        'Set the Main Clock to FIRST HALF
                        If m_objGameDetails.IsDefaultGameClock Then
                            If m_objGameDetails.CurrentPeriod = 1 Then
                                frmMain.UdcRunningClock1.URCSetTime(45, 0, True)

                            Else
                                frmMain.UdcRunningClock1.URCSetTime(90, 0, True)
                            End If
                        Else
                            frmMain.UdcRunningClock1.URCSetTime(45, 0, True)
                        End If
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                    Else
                        If m_objGameDetails.IsDefaultGameClock Then
                            If m_objGameDetails.ModuleID = 1 Then
                                drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                            Else

                                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 90
                            End If

                        Else
                            If m_objGameDetails.ModuleID = 1 Then
                                ' sep 09 2010 shirley
                                'drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                                drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                            Else
                                'Dim DsLastEventDetails As DataSet
                                'DsLastEventDetails = m_objModule1PBP.getLastEvent()
                                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 90
                            End If
                        End If
                    End If
                ElseIf m_objGameDetails.CurrentPeriod = 3 Or m_objGameDetails.CurrentPeriod = 4 Then
                    'drPBPData("TIME_ELAPSED") = EXTRATIME
                    Dim DsLastEventDetails As DataSet
                    Dim currTime As Integer

                    DsLastEventDetails = m_objModule1PBP.getLastEvent()
                    If m_objGameDetails.ModuleID = 1 Then
                        currTime = CurrentMainClockTime
                    Else
                        currTime = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                    End If
                    If currTime < EXTRATIME Then
                        'Set the Main Clock to FIRST HALF
                        If m_objGameDetails.IsDefaultGameClock Then
                            If m_objGameDetails.CurrentPeriod = 3 Then
                                frmMain.UdcRunningClock1.URCSetTime(105, 0, True)
                            Else
                                frmMain.UdcRunningClock1.URCSetTime(120, 0, True)
                            End If
                        Else
                            frmMain.UdcRunningClock1.URCSetTime(15, 0, True)
                        End If
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                    Else
                        If m_objGameDetails.IsDefaultGameClock Then
                            If m_objGameDetails.ModuleID = 1 Then
                                drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                            Else
                                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                            End If

                        Else
                            If m_objGameDetails.ModuleID = 1 Then
                                ' sep 09 2010
                                'drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                                drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                            Else
                                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED")) + 30
                            End If
                        End If
                    End If
                End If
                'If m_objGameDetails.IsDefaultGameClock Then
                '    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                'Else
                '    drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                'End If

                'drPBPData("TIME_ELAPSED") = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TIME_ELAPSED"))
                'End If
                'Else
                '    If m_objGameDetails.IsDefaultGameClock Then
                '        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                '    Else
                '        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                '    End If
                'End If
                'End If
                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("TEAM_ID") = intTeamID
                drPBPData("OFFENSE_SCORE") = intHomeScore
                drPBPData("DEFENSE_SCORE") = intAwayScore
                m_objModule1PBP.lvPBP.Enabled = True
            End If

            'End Game
            If EventID = clsGameDetails.ENDGAME Then
                m_CurrentEvent = "Game END"
                m_CurrentEventCode = EventID
                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond("00:00"))
                'If m_objGameDetails.IsDefaultGameClock Then
                'If m_objGameDetails.ModuleID = 1 Then
                '    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                'Else
                Dim DsLastEventDetails As DataSet
                DsLastEventDetails = m_objModule1PBP.getLastEvent()
                drPBPData("TIME_ELAPSED") = CInt(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                'If m_objGameDetails.CurrentPeriod = 2 Then
                '    drPBPData("TIME_ELAPSED") = FIRSTHALF
                'ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                '    drPBPData("TIME_ELAPSED") = EXTRATIME
                'End If
                'End If
                'Else
                '    If m_objGameDetails.ModuleID = 1 Then
                '        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                '        'drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                '    Else
                '        If m_objGameDetails.CurrentPeriod = 2 Then
                '            drPBPData("TIME_ELAPSED") = FIRSTHALF
                '        ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                '            drPBPData("TIME_ELAPSED") = EXTRATIME
                '        End If
                '    End If

                'End If
                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
            End If

            If EventID = clsGameDetails.COMMENTS Or EventID = clsGameDetails.ABANDONED Or EventID = clsGameDetails.DELAY_OVER Or EventID = clsGameDetails.DELAYED Or EventID = clsGameDetails.GOALIE_CHANGE Or EventID = clsGameDetails.INJURY_TIME Or EventID = clsGameDetails.GAMEINTERRUPTION Then
                m_CurrentEvent = "Comments"
                m_CurrentEventCode = EventID

                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))

                If m_objGameDetails.IsDefaultGameClock Then
                    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                Else
                    drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                End If

                '''''''''''''
                If m_objGameDetails.ModuleID = 1 Then

                    If EventID = clsGameDetails.INJURY_TIME Then
                        'Rm 8695
                        drPBPData("INJURY_TIME") = m_objGameDetails.CommentsTime 'Used for Injury Time.
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(m_objGameDetails.InjuryEnteredTime)))
                    Else

                    End If
                    If m_objGameDetails.IsDefaultGameClock Then
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                    Else
                        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                    End If
                Else
                    Dim TimeElapsed As Integer = m_objModule1Main.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                    Dim EditedTime As Integer
                    If m_objGameDetails.IsDefaultGameClock Then
                        If EventID = clsGameDetails.COMMENTS Or EventID = clsGameDetails.GOALIE_CHANGE Or EventID = clsGameDetails.DELAYED Then
                            EditedTime = CalculateTimeElapsedBefore(m_objGameDetails.CommentsTime - 30)
                        End If
                    Else
                        If EventID = clsGameDetails.COMMENTS Then
                            EditedTime = m_objGameDetails.CommentsTime
                        End If
                    End If

                    If EditedTime < TimeElapsed Then 'TIme EDIT
                        Dim DsPBPDataSet As DataSet
                        'Dim currentTime As Integer
                        'currentTime = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text)))
                        m_TimeEditMode = True
                    End If
                    drPBPData("TIME_ELAPSED") = EditedTime
                    If (m_objGameDetails.CoverageLevel = 6 And EventID = clsGameDetails.GOALIE_CHANGE) Then
                        drPBPData("TIME_ELAPSED") = m_objGameDetails.CommentsTime
                    End If
                End If

                ''''''''''''

                drPBPData("EVENT_CODE_ID") = EventID
                If (m_CurrentEventCode = clsGameDetails.COMMENTS Or m_CurrentEventCode = clsGameDetails.DELAYED) And m_objGameDetails.OffensiveTeamID = -1 Then
                    If intTeamID = 0 Then
                        drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                    Else
                        drPBPData("TEAM_ID") = intTeamID
                    End If

                ElseIf (m_CurrentEventCode = clsGameDetails.DELAYED) And m_objGameDetails.OffensiveTeamID = 0 Then
                    If intTeamID <> 0 Then
                        drPBPData("TEAM_ID") = intTeamID
                    Else
                        drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                    End If

                Else
                    drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                End If

                ''

                If intTeamID = m_objGameDetails.HomeTeamID Then
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    Else
                        Dim OldAwayScore As Integer
                        OldAwayScore = intAwayScore
                        intAwayScore = intHomeScore
                        intHomeScore = OldAwayScore
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    End If
                Else
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        Dim OldAwayScore As Integer
                        OldAwayScore = intAwayScore
                        intAwayScore = intHomeScore
                        intHomeScore = OldAwayScore
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    Else
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    End If
                End If

                'drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                'drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore
                If EventID = clsGameDetails.COMMENTS Or EventID = clsGameDetails.DELAYED Then
                    drPBPData("COMMENTS") = m_objGameDetails.CommentData
                    If m_objGameDetails.PredefinedCommentID = -1 Then
                        drPBPData("COMMENT_ID") = DBNull.Value
                    Else
                        drPBPData("COMMENT_ID") = m_objGameDetails.PredefinedCommentID
                    End If
                    'COMMENTED BY SHRAVANI
                    drPBPData("COMMENT_LANGUAGE") = m_objGameDetails.CommentLanguageID
                ElseIf EventID = clsGameDetails.GOALIE_CHANGE Then
                    drPBPData("OFFENSIVE_PLAYER_ID") = m_objGameDetails.ActiveGK
                    drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                Else
                    drPBPData("COMMENTS") = m_objGameDetails.Reason
                    If intTeamID <> 0 Then
                        drPBPData("TEAM_ID") = intTeamID
                    Else
                        drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                    End If

                End If

            End If
            If EventID = clsGameDetails.MANAGEREXPULSION Then
                m_CurrentEvent = "Manager Expulsion"
                m_CurrentEventCode = EventID
                strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))

                If m_objGameDetails.IsDefaultGameClock Then
                    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(m_objGameDetails.ManagerExplTime)
                Else
                    drPBPData("TIME_ELAPSED") = m_objGameDetails.ManagerExplTime
                End If
                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                ''

                If intTeamID = m_objGameDetails.HomeTeamID Then
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    Else
                        Dim OldAwayScore As Integer
                        OldAwayScore = intAwayScore
                        intAwayScore = intHomeScore
                        intHomeScore = OldAwayScore
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    End If
                Else
                    If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                        Dim OldAwayScore As Integer
                        OldAwayScore = intAwayScore
                        intAwayScore = intHomeScore
                        intHomeScore = OldAwayScore
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    Else
                        drPBPData("DEFENSE_SCORE") = intAwayScore
                        drPBPData("OFFENSE_SCORE") = intHomeScore
                    End If
                End If

                drPBPData("MANAGER_ID") = m_objGameDetails.OffensiveCoachID
                drPBPData("BOOKING_REASON_ID") = m_objGameDetails.ExplReasonID
            End If

            'Arindam 6-Dec-2011 added code for TIME event
            If EventID = clsGameDetails.TIME Then
                m_CurrentEvent = "Time Event"
                m_CurrentEventCode = EventID
                If m_objGameDetails.ModuleID = 1 Then
                    If m_objGameDetails.IsDefaultGameClock Then
                        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                    Else
                        drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)) * 60
                    End If
                    'Else
                    '    'If m_objGameDetails.IsDefaultGameClock Then
                    '    '    Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                    '    '    If timElap >= 60 Then
                    '    '        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore((CInt(txtTimeMod2.Text) * 60) - 30)
                    '    '    Else
                    '    '        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                    '    '    End If
                    '    '    'drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(txtTimeMod2.Text) * 60)
                    '    'Else
                    '    '    Dim timElap As Integer = CInt(txtTimeMod2.Text) * 60
                    '    '    If timElap >= 60 Then
                    '    '        drPBPData("TIME_ELAPSED") = (CInt(txtTimeMod2.Text) * 60) - 30
                    '    '    Else
                    '    '        drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                    '    '    End If
                    '    '    'drPBPData("TIME_ELAPSED") = CInt(txtTimeMod2.Text) * 60
                    '    'End If
                End If

                drPBPData("EVENT_CODE_ID") = EventID
                drPBPData("OFFENSE_SCORE") = m_objGameDetails.HomeScore
                drPBPData("DEFENSE_SCORE") = m_objGameDetails.AwayScore

                'm_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)

            End If
            If EventID = clsGameDetails.INJURY_TIME Then
                drPBPData("INJURY_TIME") = m_objGameDetails.CommentsTime 'Used for Injury Time.
                drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(m_objGameDetails.InjuryEnteredTime)))
                drPBPData("TEAM_ID") = DBNull.Value
            ElseIf EventID = clsGameDetails.ABANDONED Or EventID = clsGameDetails.GAMEINTERRUPTION Or EventID = clsGameDetails.DELAYED Or EventID = clsGameDetails.DELAY_OVER Then
                drPBPData("TEAM_ID") = IIf(EventID = clsGameDetails.DELAYED Or EventID = clsGameDetails.DELAY_OVER, drPBPData("TEAM_ID"), DBNull.Value)
                If m_objGameDetails.IsDefaultGameClock Then
                    drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime)))
                Else
                    drPBPData("TIME_ELAPSED") = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                End If
            End If
            'ACTIVE GOALKEEPER
            'If EventID = clsGameDetails.GOALIE_CHANGE Then
            '    m_CurrentEvent = "Active GoalKeeper"
            '    m_CurrentEventCode = EventID
            '    strTimeElapsed = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))

            '    If m_objGameDetails.IsDefaultGameClock Then
            '        drPBPData("TIME_ELAPSED") = CalculateTimeElapsedBefore(m_objGameDetails.ManagerExplTime)
            '    Else
            '        drPBPData("TIME_ELAPSED") = m_objGameDetails.ManagerExplTime
            '    End If
            '    drPBPData("EVENT_CODE_ID") = EventID
            '    drPBPData("TEAM_ID") = m_objGameDetails.OffensiveTeamID
            '    ''

            '    If intTeamID = m_objGameDetails.HomeTeamID Then
            '        If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
            '            drPBPData("DEFENSE_SCORE") = intAwayScore
            '            drPBPData("OFFENSE_SCORE") = intHomeScore
            '        Else
            '            Dim OldAwayScore As Integer
            '            OldAwayScore = intAwayScore
            '            intAwayScore = intHomeScore
            '            intHomeScore = OldAwayScore
            '            drPBPData("DEFENSE_SCORE") = intAwayScore
            '            drPBPData("OFFENSE_SCORE") = intHomeScore
            '        End If
            '    Else
            '        If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
            '            Dim OldAwayScore As Integer
            '            OldAwayScore = intAwayScore
            '            intAwayScore = intHomeScore
            '            intHomeScore = OldAwayScore
            '            drPBPData("DEFENSE_SCORE") = intAwayScore
            '            drPBPData("OFFENSE_SCORE") = intHomeScore
            '        Else
            '            drPBPData("DEFENSE_SCORE") = intAwayScore
            '            drPBPData("OFFENSE_SCORE") = intHomeScore
            '        End If
            '    End If

            '    drPBPData("MANAGER_ID") = m_objGameDetails.OffensiveCoachID
            '    drPBPData("BOOKING_REASON_ID") = m_objGameDetails.ExplReasonID
            'End If

            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next
            m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)

            Dim dsTempPBPData As New DataSet
            m_objGameDetails.PBP.DataSetName = "SoccerEventData"
            dsTempPBPData = m_objGameDetails.PBP
            Dim strPBPXMLData As String = dsTempPBPData.GetXml()
            Dim dsPBP As New DataSet

            'IF ACTIVE GOAL KEEPER ALREADY EXISTS FOR THE TEAM, UPDATE THAT RECORD TO 'Y' AND THE INSERTS THE NEW RECORD
            If m_CurrentEventCode = clsGameDetails.GOALIE_CHANGE Then
                m_objModule1Main.UpdateActiveGK(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.OffensiveTeamID, m_objGameDetails.languageid, m_objGameDetails.CoverageLevel)
            End If

            dsPBP = m_objModule1Main.InsertPBPDataToSQL(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, "N", m_objclsGameDetails.languageid, m_objGameDetails.ModuleID)

            'Display in Listview taking from the live_soc_pbp table after the insertion of the event
            'DisplayInListView(dsPBP)

            'IF CURRENT REPORTER IS AN ASSISTER THEN LOCK THE SYSTEM TILL THE CONFIRMATION FROM PRIMARY REPORTER
            If m_objGameDetails.ReporterRole.Substring(m_objGameDetails.ReporterRole.Length - 1) <> "1" Then
                m_objGeneral.UpdateLastEntryStatus(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, True)
                m_objGameDetails.AssistersLastEntryStatus = False
            End If

            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 5 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDEXTRA + ElapsedTime).ToString), False)
                End If
            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer) As Integer
        Try
            If m_objGameDetails.CurrentPeriod = 1 Then
                Return CurrentElapsedTime
            ElseIf m_objGameDetails.CurrentPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF
            ElseIf m_objGameDetails.CurrentPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF
            ElseIf m_objGameDetails.CurrentPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapsedBeforeInMilliSec(ByVal CurrentElapsedTime As Integer) As Integer
        Try
            Dim timeElapsed As New Dictionary(Of Integer, Integer)
            timeElapsed.Add(1, 0)
            timeElapsed.Add(2, 2700000) 'FIRSTHALF
            timeElapsed.Add(3, 5400000) 'SECONDHALF
            timeElapsed.Add(4, 6300000) 'FIRSTEXTRA
            timeElapsed.Add(5, 7200000)
            Return If(timeElapsed.ContainsKey(m_objGameDetails.CurrentPeriod), CurrentElapsedTime - timeElapsed.Item(m_objGameDetails.CurrentPeriod), CurrentElapsedTime)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Sub SetUniformColor()
        Try
            Dim dsUniformColor As New DataSet
            dsUniformColor = m_objModule1Main.GetUniformColors(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)

            If dsUniformColor.Tables(0).Rows.Count > 0 Then
                Dim dr() As DataRow = dsUniformColor.Tables(0).Select("TEAM_ID = " & m_objGameDetails.HomeTeamID)
                If dr.Length > 0 Then
                    If dr(0).Item("SHIRT_COLOR").ToString() <> "0" And Not IsDBNull(dr(0).Item("SHIRT_COLOR")) Then
                        m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(dr(0).Item("SHIRT_COLOR").ToString)
                    End If
                    If dr(0).Item("SHORT_COLOR").ToString() <> "0" And Not IsDBNull(dr(0).Item("SHORT_COLOR")) Then
                        m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(dr(0).Item("SHORT_COLOR").ToString)
                    End If
                End If

                Dim drAway() As DataRow = dsUniformColor.Tables(0).Select("TEAM_ID = " & m_objGameDetails.AwayTeamID)
                If drAway.Length > 0 Then
                    If drAway(0).Item("SHIRT_COLOR").ToString() <> "0" And Not IsDBNull(drAway(0).Item("SHIRT_COLOR")) Then
                        m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(drAway(0).Item("SHIRT_COLOR").ToString)
                    End If
                    If drAway(0).Item("SHORT_COLOR").ToString() <> "0" And Not IsDBNull(drAway(0).Item("SHORT_COLOR")) Then
                        m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(drAway(0).Item("SHORT_COLOR").ToString)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function correctRoster() As Boolean
        Try
            If m_objTeamSetup.RosterInfo.Tables("Home").Rows.Count > 0 And m_objTeamSetup.RosterInfo.Tables("Away").Rows.Count > 0 Then
                Return True
            End If
            Return False
        Catch ex As Exception

        End Try
    End Function

#End Region

#Region " Module2 Event Handlers and User Defined Function "

    ''' <summary>
    ''' SETS THE PROPERTY FOR THE MODULE 2 GAME SELECTED IN THE MAIN SCREEN
    ''' </summary>
    ''' <remarks></remarks>
    '''
    Public Sub FillSelectedGameDetails()
        Try
            'REFRESHING THE MAIN SCREEN CONTROLS FOR EVERY GAME SELECTION IN THE MAIN SCREEN
            BindGames()
            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                If m_dsGames.Tables.Count > 0 Then
                    For i As Integer = 0 To m_dsGames.Tables(0).Rows.Count - 1
                        If CInt(m_dsGames.Tables(0).Rows(i).Item("GAME_CODE")) = m_objGameDetails.GameCode Then
                            Dim intOldGameCode As Integer = m_objGameDetails.GameLastPlayed
                            m_objGameDetails.GameCode = CInt(m_dsGames.Tables(0).Rows(i).Item("GAME_CODE").ToString)
                            m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(m_dsGames.Tables(0).Rows(i).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                            m_objGameDetails.ModuleID = CInt(m_dsGames.Tables(0).Rows(i).Item("MODULE_ID").ToString)
                            'm_objGameDetails.CoverageLevel = CInt(m_dsGames.Tables(0).Rows(i).Item("TIER").ToString)
                            m_objGameDetails.AwayTeamID = CInt(m_dsGames.Tables(0).Rows(i).Item("AWAYTEAMID").ToString)
                            m_objGameDetails.AwayTeamAbbrev = m_dsGames.Tables(0).Rows(i).Item("AWAYABBREV").ToString
                            m_objGameDetails.AwayTeam = m_dsGames.Tables(0).Rows(i).Item("AWAYTEAM").ToString
                            m_objGameDetails.HomeTeamID = CInt(m_dsGames.Tables(0).Rows(i).Item("HOMETEAMID").ToString)
                            m_objGameDetails.HomeTeamAbbrev = m_dsGames.Tables(0).Rows(i).Item("HOMEABBREV").ToString
                            m_objGameDetails.HomeTeam = m_dsGames.Tables(0).Rows(i).Item("HOMETEAM").ToString
                            m_objGameDetails.LeagueID = CInt(m_dsGames.Tables(0).Rows(i).Item("LEAGUE_ID").ToString)
                            m_objGameDetails.LeagueName = m_dsGames.Tables(0).Rows(i).Item("Competition").ToString
                            m_objGameDetails.FieldID = CInt(m_dsGames.Tables(0).Rows(i).Item("FIELD_ID").ToString)

                            'm_objGameDetails.FieldName = dsMultiplegames.Tables(0).Rows(i).Item("FIELDNAME").ToString
                            'm_objGameDetails.FeedNumber = 1 'CInt(m_dsGames.Tables(0).Rows(i).Item("FEEDNUMBER").ToString)
                            m_objGameDetails.ReporterRoleDisplay = "Operation Desk"
                            m_objGameDetails.ReporterRole = "OPS" ' Convert.ToString(m_dsGames.Tables(0).Rows(i).Item("REPORTER_ROLE")) & Convert.ToInt32(m_dsGames.Tables(0).Rows(i).Item("SERIAL_NO"))
                            m_objGameDetails.SerialNo = Convert.ToInt32(m_dsGames.Tables(0).Rows(i).Item("SERIAL_NO"))

                            'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                            Dim intTimeElapsed As Integer = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                            m_objModule1Main.InsertCurrentGame(m_objGameDetails.GameCode, intOldGameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, intTimeElapsed, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")

                            'CLEARS THE CONTROLS
                            ClearMainScreenControls()

                            'getting the current players for the selected game
                            GetCurrentPlayersAfterRestart()

                            SortPlayers()
                            'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
                            LabelHomeTeamTagged()
                            LabelAwayTeamTagged()
                            'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
                            FetchMainScreenPBPEvents()
                            'GAME SETUP INFO
                            InsertOfficialAndMatchInfo()
                            'PERIOD,SCORES,TEAM NAMES DISPLAY IS DONE IN THIS FUNCTION
                            frmMain.FillFormControls()
                            frmMain.ResetClockAndScores()
                            SetTeamButtonColor()
                            UpdateManager()
                            frmMain.GetPenaltyShootoutScore()
                            Exit For
                        End If
                    Next
                End If
            Else
                If m_objGameDetails.ModuleID = 2 Then
                    'MODULE 2 (MULTIPLE GAMES)
                    For i As Integer = 0 To dsMultiplegames.Tables(0).Rows.Count - 1
                        If CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE")) = m_objGameDetails.GameCode Then
                            Dim intOldGameCode As Integer = m_objGameDetails.GameLastPlayed
                            m_objGameDetails.GameCode = CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE").ToString)
                            m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(dsMultiplegames.Tables(0).Rows(i).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                            m_objGameDetails.ModuleID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("MODULE_ID").ToString)
                            m_objGameDetails.CoverageLevel = CInt(dsMultiplegames.Tables(0).Rows(i).Item("TIER").ToString)
                            m_objGameDetails.AwayTeamID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAMID").ToString)
                            m_objGameDetails.AwayTeamAbbrev = dsMultiplegames.Tables(0).Rows(i).Item("AWAYABBREV").ToString
                            m_objGameDetails.AwayTeam = dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAM").ToString
                            m_objGameDetails.HomeTeamID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAMID").ToString)
                            m_objGameDetails.HomeTeamAbbrev = dsMultiplegames.Tables(0).Rows(i).Item("HOMEABBREV").ToString
                            m_objGameDetails.HomeTeam = dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAM").ToString
                            m_objGameDetails.LeagueID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("LEAGUE_ID").ToString)
                            m_objGameDetails.LeagueName = dsMultiplegames.Tables(0).Rows(i).Item("Competition").ToString

                            If dsMultiplegames.Tables(0).Rows(i).Item("FIELD_ID") IsNot DBNull.Value Then
                                m_objGameDetails.FieldID = CInt(dsMultiplegames.Tables(0).Rows(i).Item("FIELD_ID").ToString)
                            End If
                            'm_objGameDetails.FieldName = dsMultiplegames.Tables(0).Rows(i).Item("FIELDNAME").ToString
                            m_objGameDetails.FeedNumber = CInt(dsMultiplegames.Tables(0).Rows(i).Item("FEEDNUMBER").ToString)
                            m_objGameDetails.ReporterRoleDisplay = CStr(IIf(CInt(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO").ToString) = 1, "MAIN", "ASSISTER"))
                            m_objGameDetails.ReporterRole = Convert.ToString(dsMultiplegames.Tables(0).Rows(i).Item("REPORTER_ROLE")) & Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))
                            m_objGameDetails.SerialNo = Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))

                            'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                            Dim intTimeElapsed As Integer = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                            m_objModule1Main.InsertCurrentGame(m_objGameDetails.GameCode, intOldGameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, intTimeElapsed, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")

                            If m_objGameDetails.CoverageLevel < 3 Then
                                m_objGeneral.InsertTeamGameForLowTiers(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)
                            End If

                            If m_objGameDetails.CoverageLevel >= 2 Then
                                'CLEARS THE CONTROLS
                                frmMain.SetChildForm()
                                frmMain.SetMainFormSize()
                                ClearMainScreenControls()
                                'getting the current players for the selected game
                                GetCurrentPlayersAfterRestart()
                                SortPlayers()
                                'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
                                LabelsTagged()
                                'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
                                FetchMainScreenPBPEvents()
                                'GAME SETUP INFO
                                InsertOfficialAndMatchInfo()
                                'PERIOD,SCORES,TEAM NAMES DISPLAY IS DONE IN THIS FUNCTION
                                frmMain.FillFormControls()
                                frmMain.SetAllMenuItemsVisbleFalse()
                                frmMain.SetMainFormMenu()
                                frmMain.ResetClockAndScores()
                                SetTeamButtonColor()
                                UpdateManager()
                                frmMain.GetPenaltyShootoutScore()
                            Else
                                'frmMain.SetChildForm()
                                frmMain.SetMainFormSize()
                                frmMain.FillFormControls()
                                'frmMain.m_objModule2Score.LoadTeam(m_objGameDetails.GameCode)
                                'frmMain.m_objModule2Score.FillListView()
                                frmMain.ResetClockAndScores()
                                frmMain.SetMenuItemsforModule2Score()
                                SetTeamButtonColor()
                                frmMain.GetPenaltyShootoutScore()
                                frmMain.SetChildForm()
                            End If
                            Exit For
                        End If
                    Next
                Else

                    ClearMainScreenControls()

                    'getting the current players for the selected game
                    GetCurrentPlayersAfterRestart()

                    SortPlayers()
                    'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
                    LabelHomeTeamTagged()
                    LabelAwayTeamTagged()
                    'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
                    FetchMainScreenPBPEvents()
                    'GAME SETUP INFO
                    InsertOfficialAndMatchInfo()
                    'PERIOD,SCORES,TEAM NAMES DISPLAY IS DONE IN THIS FUNCTION
                    frmMain.FillFormControls()
                    frmMain.ResetClockAndScores()
                    SetTeamButtonColor()
                    UpdateManager()
                    frmMain.GetPenaltyShootoutScore()

                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Function SortPlayers() As DataSet
        Try
            Dim DsPlayers As New DataSet
            If Not m_objTeamSetup.RosterInfo Is Nothing Then
                If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
                    DsPlayers = m_objTeamSetup.RosterInfo.Copy()
                End If
                m_objTeamSetup.RosterInfo.Tables.Clear()
            End If
            Select Case m_objLoginDetails.strSortOrder
                Case "Uniform"
                    frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", True)
                    frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                    frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                    If DsPlayers IsNot Nothing Then
                        If DsPlayers.Tables.Count > 0 Then
                            For i As Integer = 0 To DsPlayers.Tables.Count - 1
                                If DsPlayers.Tables(i).Rows.Count > 0 Then
                                    If Not DsPlayers.Tables(i).Columns.Contains("SORT_NUMBER") Then
                                        Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                                        dcHome.DataType = System.Type.GetType("System.Int32")
                                        DsPlayers.Tables(i).Columns.Add(dcHome)
                                        For Each drHome As DataRow In DsPlayers.Tables(i).Rows
                                            If IsDBNull(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = True Then
                                                drHome.Item("SORT_NUMBER") = 30000
                                            Else
                                                drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                            End If
                                        Next
                                    End If

                                    Dim dvHome As New DataView(DsPlayers.Tables(i))
                                    Dim dtSortHome As New DataTable
                                    dtSortHome = DsPlayers.Tables(i).Clone
                                    dtSortHome.TableName = DsPlayers.Tables(i).TableName
                                    Dim drArrHome As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION<=11", "SORT_NUMBER ASC")
                                    For Each drSortHome As DataRow In drArrHome
                                        dtSortHome.ImportRow(drSortHome)
                                    Next
                                    Dim drArrHomeBench As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION>11", "SORT_NUMBER ASC")
                                    For Each drSortHomeBench As DataRow In drArrHomeBench
                                        dtSortHome.ImportRow(drSortHomeBench)
                                    Next
                                    m_objTeamSetup.RosterInfo.Tables.Add(dtSortHome)
                                End If
                            Next
                        End If
                    End If

                Case "Position"
                    frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                    frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                    frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", True)

                    If DsPlayers IsNot Nothing Then
                        If DsPlayers.Tables.Count > 0 Then
                            For i As Integer = 0 To DsPlayers.Tables.Count - 1
                                If DsPlayers.Tables(i).Rows.Count > 0 Then
                                    Dim dvHome As New DataView(DsPlayers.Tables(i))
                                    Dim dtSortHome As New DataTable
                                    dtSortHome = DsPlayers.Tables(i).Clone
                                    dtSortHome.TableName = DsPlayers.Tables(i).TableName
                                    Dim drArrHome As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION<=11", "STARTING_POSITION ASC")
                                    For Each drSortHome As DataRow In drArrHome
                                        dtSortHome.ImportRow(drSortHome)
                                    Next
                                    Dim drArrHomeBench As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION>11", "STARTING_POSITION ASC")
                                    For Each drSortHomeBench As DataRow In drArrHomeBench
                                        dtSortHome.ImportRow(drSortHomeBench)
                                    Next
                                    m_objTeamSetup.RosterInfo.Tables.Add(dtSortHome)
                                End If
                            Next
                        End If
                    End If

                Case "Last Name"
                    frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                    frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", True)
                    frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                    If DsPlayers IsNot Nothing Then
                        If DsPlayers.Tables.Count > 0 Then
                            For i As Integer = 0 To DsPlayers.Tables.Count - 1
                                If DsPlayers.Tables(i).Rows.Count > 0 Then
                                    Dim dvHome As New DataView(DsPlayers.Tables(i))
                                    Dim dtSortHome As New DataTable
                                    dtSortHome = DsPlayers.Tables(i).Clone
                                    dtSortHome.TableName = DsPlayers.Tables(i).TableName
                                    Dim drArrHome As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION<=11", "LAST_NAME ASC")
                                    For Each drSortHome As DataRow In drArrHome
                                        dtSortHome.ImportRow(drSortHome)
                                    Next
                                    Dim drArrHomeBench As DataRow() = DsPlayers.Tables(i).Select("STARTING_POSITION>11", "LAST_NAME ASC")
                                    For Each drSortHomeBench As DataRow In drArrHomeBench
                                        dtSortHome.ImportRow(drSortHomeBench)
                                    Next
                                    m_objTeamSetup.RosterInfo.Tables.Add(dtSortHome)
                                End If
                            Next
                        End If
                    End If
            End Select

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    ''' <summary>
    ''' LOADS MODULE2 GAMES ASSIGNED TO THE REPORTER (GAMES WITH +/- 24 HRS IS TAKEN INTO CONSIDERATION)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindMod2MultipleGames()
        Try
            'GETS MODULE 2 GAMES ASSIGNED TO THAT REPORTER (GAME DATE IS THE GAME DATE SELECTED FROM THE SELECT GAME SCREEN)
            'DATE DIFF IS CHECKED WITH THE GAME DATE PASSED AS PARAMETER AND DISPLAYS THE GAMES IN THE LISTBOX
            dsMultiplegames = m_objModule1Main.GetMultipleGameDetails(m_objLoginDetails.LanguageID, m_objLoginDetails.UserId)
            'pnlDisplaygames.Visible = True
            If Not dsMultiplegames Is Nothing Then
                If (dsMultiplegames.Tables(0).Rows.Count > 0) Then
                    'For intRowCount = 0 To dsMultiplegames.Tables(0).Rows.Count - 1
                    '    Me.pnlDisplaygames.Controls.Item("radGame" & Format(intRowCount, "00")).Visible = True
                    '    Me.pnlDisplaygames.Controls.Item("radGame" & Format(intRowCount, "00")).Tag = dsMultiplegames.Tables(0).Rows(intRowCount).Item("GAME_CODE").ToString
                    '    Me.pnlDisplaygames.Controls.Item("radGame" & Format(intRowCount, "00")).Text = dsMultiplegames.Tables(0).Rows(intRowCount).Item("HOMEABBREV").ToString & " Vs " & dsMultiplegames.Tables(0).Rows(intRowCount).Item("AWAYABBREV").ToString & " - " & CDate(dsMultiplegames.Tables(0).Rows(intRowCount).Item("GAME_DATE").ToString).ToShortTimeString
                    'Next
                End If
            Else
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Sub UpdateManager()
        Try
            Dim dsForCoach As New DataSet

            dsForCoach = m_objModule1Main.GetFormationCoach(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, "MAIN", m_objclsGameDetails.languageid)

            If dsForCoach.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsForCoach.Tables(0).Rows.Count - 1
                    If m_objGameDetails.HomeTeamID = CInt(dsForCoach.Tables(0).Rows(i).Item("TEAM_ID")) Then
                        lblManagerHome.Text = CStr(dsForCoach.Tables(0).Rows(i).Item("COACH_NAME"))
                        If lblManagerHome.PreferredWidth > lblManagerHome.Width Then
                            lblManagerHome.Text = CStr(dsForCoach.Tables(0).Rows(i).Item("LAST_NAME")) & "," & Space(1) & CStr(IIf(dsForCoach.Tables(0).Rows(i).Item("MONIKER").ToString = "", "", dsForCoach.Tables(0).Rows(i).Item("MONIKER").ToString.Chars(0)))
                        End If
                    Else
                        lblManagerAway.Text = CStr(dsForCoach.Tables(0).Rows(i).Item("COACH_NAME"))
                        If lblManagerAway.PreferredWidth > lblManagerAway.Width Then
                            lblManagerAway.Text = CStr(dsForCoach.Tables(0).Rows(i).Item("LAST_NAME")) & "," & Space(1) & CStr(IIf(dsForCoach.Tables(0).Rows(i).Item("MONIKER").ToString = "", "", dsForCoach.Tables(0).Rows(i).Item("MONIKER").ToString.Chars(0)))
                        End If
                    End If
                Next
            Else
                lblManagerHome.Text = ""
                lblManagerAway.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Commented Functions for future Ref"

    'Public Sub FillSelectedGameInfo(ByVal GameCode As Integer)
    '    Try
    '        For i As Integer = 0 To dsMultiplegames.Tables(0).Rows.Count - 1
    '            If CInt(dsMultiplegames.Tables(0).Rows(i).Item("GAME_CODE")) = GameCode Then
    '                lstGameInfo.Visible = True
    '                lstGameInfo.Items(0).SubItems(1).Text = CStr(CDate(dsMultiplegames.Tables(0).Rows(i).Item("KICKOFF").ToString))
    '                lstGameInfo.Items(1).SubItems(1).Text = dsMultiplegames.Tables(0).Rows(i).Item("HOMETEAM").ToString
    '                lstGameInfo.Items(2).SubItems(1).Text = dsMultiplegames.Tables(0).Rows(i).Item("AWAYTEAM").ToString

    '                Dim drSchedule() As DataRow
    '                drSchedule = dsMultiplegames.Tables(1).Select("Game_Code = " & GameCode & "")
    '                If drSchedule.Length > 0 Then
    '                    If drSchedule(0).Item("FIELDNAME").ToString() IsNot DBNull.Value Then
    '                        lstGameInfo.Items(3).SubItems(1).Text = drSchedule(0).Item("FIELDNAME").ToString()
    '                    Else
    '                        Dim drGame() As DataRow
    '                        drGame = dsMultiplegames.Tables(2).Select("Game_Code =" & GameCode & "")
    '                        If drGame.Length > 0 Then
    '                            If drGame(0).Item("FIELDNAME").ToString() IsNot DBNull.Value Then
    '                                lstGameInfo.Items(3).SubItems(1).Text = drGame(0).Item("FIELDNAME").ToString()
    '                            Else
    '                                lstGameInfo.Items(3).SubItems(1).Text = ""
    '                            End If
    '                        End If
    '                    End If
    '                Else
    '                    Dim drGame() As DataRow
    '                    drGame = dsMultiplegames.Tables(2).Select("Game_Code =" & GameCode & "")
    '                    If drGame.Length > 0 Then
    '                        If drGame(0).Item("FIELDNAME").ToString() IsNot DBNull.Value Then
    '                            lstGameInfo.Items(3).SubItems(1).Text = drGame(0).Item("FIELDNAME").ToString()
    '                        Else
    '                            lstGameInfo.Items(3).SubItems(1).Text = ""
    '                        End If
    '                    End If
    '                End If
    '                'lstGameInfo.Items(3).SubItems(1).Text = dsMultiplegames.Tables(0).Rows(i).Item("FIELDNAME").ToString
    '                FillSelectedGameDetails(GameCode)
    '                Exit For
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub radGame00_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame00.Checked = True Then
    '            If radGame00.BackColor <> Color.Yellow Then
    '                radGame00.BackColor = Color.Red
    '            End If
    '            'radGame00.BackColor = Color.Red
    '            FillSelectedGameInfo(CInt(radGame00.Tag))
    '        Else
    '            If radGame00.BackColor <> Color.Yellow Then
    '                radGame00.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame01_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame01.Checked = True Then
    '            If radGame01.BackColor <> Color.Yellow Then
    '                radGame01.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame01.Tag))
    '        Else
    '            If radGame01.BackColor <> Color.Yellow Then
    '                radGame01.BackColor = Color.Empty
    '            End If

    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame02_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame02.Checked = True Then
    '            If radGame02.BackColor <> Color.Yellow Then
    '                radGame02.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame02.Tag))
    '        Else
    '            If radGame02.BackColor <> Color.Yellow Then
    '                radGame02.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub
    'Private Sub radGame03_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame03.Checked = True Then
    '            If radGame03.BackColor <> Color.Yellow Then
    '                radGame03.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame03.Tag))
    '        Else
    '            If radGame03.BackColor <> Color.Yellow Then
    '                radGame03.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame04_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame04.Checked = True Then
    '            If radGame04.BackColor <> Color.Yellow Then
    '                radGame04.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame04.Tag))
    '        Else
    '            If radGame04.BackColor <> Color.Yellow Then
    '                radGame04.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame05_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame05.Checked = True Then
    '            If radGame05.BackColor <> Color.Yellow Then
    '                radGame05.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame05.Tag))
    '        Else
    '            If radGame05.BackColor <> Color.Yellow Then
    '                radGame05.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame06_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame06.Checked = True Then
    '            If radGame06.BackColor <> Color.Yellow Then
    '                radGame06.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame06.Tag))
    '        Else
    '            If radGame06.BackColor <> Color.Yellow Then
    '                radGame06.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame07_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame07.Checked = True Then
    '            If radGame07.BackColor <> Color.Yellow Then
    '                radGame07.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame07.Tag))
    '        Else
    '            If radGame07.BackColor <> Color.Yellow Then
    '                radGame07.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame08_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame08.Checked = True Then
    '            If radGame08.BackColor <> Color.Yellow Then
    '                radGame08.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame08.Tag))
    '        Else
    '            If radGame08.BackColor <> Color.Yellow Then
    '                radGame08.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame09_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame09.Checked = True Then
    '            If radGame09.BackColor <> Color.Yellow Then
    '                radGame09.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame09.Tag))
    '        Else
    '            If radGame09.BackColor <> Color.Yellow Then
    '                radGame09.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame10_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame10.Checked = True Then
    '            If radGame10.BackColor <> Color.Yellow Then
    '                radGame10.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame10.Tag))
    '        Else
    '            If radGame10.BackColor <> Color.Yellow Then
    '                radGame10.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame11_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame11.Checked = True Then
    '            If radGame11.BackColor <> Color.Yellow Then
    '                radGame11.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame11.Tag))
    '        Else
    '            If radGame11.BackColor <> Color.Yellow Then
    '                radGame11.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame12_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame12.Checked = True Then
    '            If radGame12.BackColor <> Color.Yellow Then
    '                radGame12.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame12.Tag))
    '        Else
    '            If radGame12.BackColor <> Color.Yellow Then
    '                radGame12.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame13_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame13.Checked = True Then
    '            If radGame13.BackColor <> Color.Yellow Then
    '                radGame13.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame13.Tag))
    '        Else
    '            If radGame13.BackColor <> Color.Yellow Then
    '                radGame13.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame14_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame14.Checked = True Then
    '            If radGame14.BackColor <> Color.Yellow Then
    '                radGame14.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame14.Tag))
    '        Else
    '            If radGame14.BackColor <> Color.Yellow Then
    '                radGame14.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame15_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame15.Checked = True Then
    '            If radGame15.BackColor <> Color.Yellow Then
    '                radGame15.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame15.Tag))
    '        Else
    '            If radGame15.BackColor <> Color.Yellow Then
    '                radGame15.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame16_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame16.Checked = True Then
    '            If radGame16.BackColor <> Color.Yellow Then
    '                radGame16.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame16.Tag))
    '        Else
    '            If radGame16.BackColor <> Color.Yellow Then
    '                radGame16.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame17_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame17.Checked = True Then
    '            If radGame17.BackColor <> Color.Yellow Then
    '                radGame17.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame17.Tag))
    '        Else
    '            If radGame17.BackColor <> Color.Yellow Then
    '                radGame17.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame18_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame18.Checked = True Then
    '            If radGame18.BackColor <> Color.Yellow Then
    '                radGame18.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame18.Tag))
    '        Else
    '            If radGame18.BackColor <> Color.Yellow Then
    '                radGame18.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame19_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame19.Checked = True Then
    '            If radGame19.BackColor <> Color.Yellow Then
    '                radGame19.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame19.Tag))
    '        Else
    '            If radGame19.BackColor <> Color.Yellow Then
    '                radGame19.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    'Private Sub radGame20_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If radGame20.Checked = True Then
    '            If radGame20.BackColor <> Color.Yellow Then
    '                radGame20.BackColor = Color.Red
    '            End If
    '            FillSelectedGameInfo(CInt(radGame20.Tag))
    '        Else
    '            If radGame20.BackColor <> Color.Yellow Then
    '                radGame20.BackColor = Color.Empty
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub
    '''' <summary>
    '''' DISPLAY EVENTS SUCH AS GOAL,PENALTY,SUB IN THE MAIN SCREEN LISTBOX
    '''' </summary>
    '''' <param name="DsEvents"></param>
    '''' <remarks></remarks>
    'Private Sub DisplayPBPEventsInMain(ByVal DsEvents As DataSet)
    '    Try
    '        If DsEvents.Tables(0).Rows.Count > 0 Then
    '            Dim ElapsedTime As Integer
    '            Dim StrTime As String = "00:00"
    '            Dim CurrPeriod As Integer

    '            'CELARS THE ITEMS IN THE LISTBOX
    '            lvwHomeGoals.Items.Clear()
    '            lvwHomeSubstitutions.Items.Clear()
    '            lvwHomeBookings.Items.Clear()
    '            lvwHomePenalties.Items.Clear()

    '            'DISPLAY GOAL EVENTS IN MAIN SCREEN LISTBOX
    '            For i As Integer = 0 To DsEvents.Tables(0).Rows.Count - 1
    '                Select Case CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID"))
    '                    Case clsGameDetails.GOAL, clsGameDetails.PENALTY, clsGameDetails.OWNGOAL, clsGameDetails.SHOOTOUT_GOAL
    '                        'FILLING GOAL EVENTS
    '                        Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
    '                        If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                ''Time Conversion
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 28 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("OFFENSE_SCORE").ToString & "-" & DsEvents.Tables(0).Rows(i).Item("DEFENSE_SCORE").ToString)
    '                            Else
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("DEFENSE_SCORE").ToString & "-" & DsEvents.Tables(0).Rows(i).Item("OFFENSE_SCORE").ToString)
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 28 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                            End If
    '                        Else
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("DEFENSE_SCORE").ToString & "-" & DsEvents.Tables(0).Rows(i).Item("OFFENSE_SCORE").ToString)
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 28 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                            Else
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 28 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("OFFENSE_SCORE").ToString & "-" & DsEvents.Tables(0).Rows(i).Item("DEFENSE_SCORE").ToString)
    '                            End If

    '                        End If
    '                        lvwHomeGoals.Items.Add(lvwitem)
    '                    Case 22
    '                        'FILLING SUBSITITION EVENTS
    '                        Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
    '                        If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SUB_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                            Else
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SUB_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                            End If
    '                        Else
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SUB_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                lvwitem.SubItems.Add(StrTime)
    '                            Else
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SUB_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("SUB_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                            End If
    '                        End If
    '                        lvwHomeSubstitutions.Items.Add(lvwitem)
    '                    Case 2, 7
    '                        'FILLING BOOKINGS(YELLOW/RED CARDS)
    '                        Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
    '                        If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 2 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                            Else
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 2 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                lvwitem.SubItems.Add(StrTime)
    '                            End If
    '                        Else
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 2 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                lvwitem.SubItems.Add(StrTime)
    '                            Else
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If CInt(DsEvents.Tables(0).Rows(i).Item("EVENT_CODE_ID")) = 2 Then
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID")) Then
    '                                        lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("PLAYER_OUT_ID"))))
    '                                    Else
    '                                        lvwitem.SubItems.Add("")
    '                                    End If
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_REASON", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_REASON_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("BOOKING_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("BOOKING_TYPE_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                            End If
    '                        End If
    '                        lvwHomeBookings.Items.Add(lvwitem)
    '                    Case 18
    '                        'FILLING MISSED PENALTY EVENTS
    '                        Dim lvwitem As New ListViewItem(DsEvents.Tables(0).Rows(i).Item("SEQUENCE_NUMBER").ToString)
    '                        If CInt(DsEvents.Tables(0).Rows(i).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SHOT_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                            Else
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SHOT_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID"))))
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                lvwitem.SubItems.Add(StrTime)
    '                            End If
    '                        Else
    '                            If m_objGameDetails.IsHomeTeamOnLeft Then
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                lvwitem.SubItems.Add("")
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SHOT_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID"))))
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                lvwitem.SubItems.Add(StrTime)
    '                            Else
    '                                If Not IsDBNull(CDbl(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))) Then
    '                                    'CONVERTING TO MINUTES
    '                                    CurrPeriod = CInt(DsEvents.Tables(0).Rows(i).Item("PERIOD"))
    '                                    ElapsedTime = CInt(DsEvents.Tables(0).Rows(i).Item("TIME_ELAPSED"))
    '                                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                                End If
    '                                lvwitem.SubItems.Add(StrTime)
    '                                lvwitem.SubItems.Add(DsEvents.Tables(0).Rows(i).Item("PERIOD").ToString)
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("OFFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID")) Then
    '                                    lvwitem.SubItems.Add(GetDescription("SHOT_TYPE", CInt(DsEvents.Tables(0).Rows(i).Item("SHOT_TYPE_ID"))))
    '                                End If
    '                                If Not IsDBNull(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID")) Then
    '                                    lvwitem.SubItems.Add(FetchPlayerNames(CInt(DsEvents.Tables(0).Rows(i).Item("DEFENSIVE_PLAYER_ID"))))
    '                                Else
    '                                    lvwitem.SubItems.Add("")
    '                                End If
    '                            End If
    '                        End If
    '                        lvwHomePenalties.Items.Add(lvwitem)
    '                End Select
    '            Next
    '        End If

    Public Sub FillSelectedDemoGameDetails(ByVal GameCode As Integer)
        Try
            m_dsDemoGames = m_objModule1Main.GetDemoGameDetails()
            If m_dsDemoGames.Tables(1).Rows.Count > 0 Then
                For i As Integer = 0 To m_dsDemoGames.Tables(1).Rows.Count - 1
                    If CInt(m_dsDemoGames.Tables(1).Rows(i).Item("GAME_CODE")) = GameCode Then
                        Dim intOldGameCode As Integer = m_objGameDetails.GameCode
                        m_objGameDetails.GameCode = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("GAME_CODE").ToString)
                        m_objGameDetails.GameDate = DateTimeHelper.GetDate(CStr(m_dsDemoGames.Tables(1).Rows(i).Item("Kickoff")), DateTimeHelper.InputFormat.ISOFormat)
                        m_objGameDetails.ModuleID = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("MODULE_ID").ToString)
                        m_objGameDetails.CoverageLevel = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("TIER").ToString)
                        m_objGameDetails.AwayTeamID = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("AWAYTEAMID").ToString)
                        m_objGameDetails.AwayTeamAbbrev = m_dsDemoGames.Tables(1).Rows(i).Item("AWAYABBREV").ToString
                        m_objGameDetails.AwayTeam = m_dsDemoGames.Tables(1).Rows(i).Item("AWAYTEAM").ToString
                        m_objGameDetails.HomeTeamID = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("HOMETEAMID").ToString)
                        m_objGameDetails.HomeTeamAbbrev = m_dsDemoGames.Tables(1).Rows(i).Item("HOMEABBREV").ToString
                        m_objGameDetails.HomeTeam = m_dsDemoGames.Tables(1).Rows(i).Item("HOMETEAM").ToString
                        m_objGameDetails.LeagueID = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("LEAGUE_ID").ToString)
                        m_objGameDetails.LeagueName = m_dsDemoGames.Tables(1).Rows(i).Item("Competition").ToString
                        If m_dsDemoGames.Tables(1).Rows(i).Item("FIELD_ID") IsNot DBNull.Value Then
                            m_objGameDetails.FieldID = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("FIELD_ID").ToString)
                        End If
                        'm_objGameDetails.FieldName = dsMultiplegames.Tables(0).Rows(i).Item("FIELDNAME").ToString
                        m_objGameDetails.FeedNumber = CInt(m_dsDemoGames.Tables(1).Rows(i).Item("FEEDNUMBER").ToString)
                        'm_objGameDetails.ReporterRoleDisplay = CStr(IIf(CInt(m_dsDemoGames.Tables(1).Rows(i).Item("SERIAL_NO").ToString) = 1, "MAIN", "ASSISTER"))
                        ' m_objGameDetails.ReporterRole = Convert.ToString(m_dsDemoGames.Tables(1).Rows(i).Item("REPORTER_ROLE")) & Convert.ToInt32(dsMultiplegames.Tables(0).Rows(i).Item("SERIAL_NO"))
                        'm_objGameDetails.SerialNo = Convert.ToInt32(m_dsDemoGames.Tables(1).Rows(i).Item("SERIAL_NO"))

                        'INSERTING THE SELECETED GAME INTO CURRENT GAME SQL TABLE
                        Dim intTimeElapsed As Integer = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
                        ' m_objModule1Main.InsertCurrentGame(m_objGameDetails.GameCode, intOldGameCode, m_objGameDetails.FeedNumber, m_objLoginDetails.UserId, m_objGameDetails.ReporterRole, m_objGameDetails.ModuleID, intTimeElapsed, Format(Date.Now, "yyyy-MM-dd hh:mm:ss"), "UPDATE")

                        'CLEARS THE CONTROLS
                        ClearMainScreenControls()

                        'getting the current players for the selected game
                        GetCurrentPlayersAfterRestart()

                        SortPlayers()
                        'LABLES ARE CLEARED AND ROSTERS ARE DISPLAYED FOR THE SELECTED GAME
                        LabelHomeTeamTagged()
                        LabelAwayTeamTagged()
                        'PBP EVENTS ARE FETCEHD FROM DB TO DISPLAY IN THE MAIN SCREEN LIST BOXES
                        FetchMainScreenPBPEvents()
                        'GAME SETUP INFO
                        InsertOfficialAndMatchInfo()
                        'PERIOD,SCORES,TEAM NAMES DISPLAY IS DONE IN THIS FUNCTION
                        frmMain.FillFormControls()
                        frmMain.ResetClockAndScores()
                        SetTeamButtonColor()

                        UpdateManager()

                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

#End Region

    Public Sub btnSwitchGames_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSwitchGames.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSwitchGames.Text, Nothing, 1, 0)
            Dim objSelectGame As New frmSelectGame
            If objSelectGame.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            End If
            'RM 8028 CPU Spike when switching between games..
            GC.Collect()

            If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.LIVE Then
                FillSelectedGameDetails()
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GameSelected, m_objGameDetails.AwayTeam & " @ " & m_objGameDetails.HomeTeam & ", League:" & m_objGameDetails.LeagueName & ",  " & m_objGameDetails.ModuleName, 1, 0)
                'TO GET RULES DATA
                m_objGameDetails.RulesData = m_objGeneral.GetRulesData(m_objGameDetails.LeagueID, m_objGameDetails.CoverageLevel)
            Else
                FillSelectedDemoGameDetails(m_objGameDetails.GameCode)
            End If
            m_objGameDetails.IsLineupChecked = False
            frmMain.SetMarqueeText("")

            If m_objGameDetails.CoverageLevel = 2 Or m_objGameDetails.CoverageLevel = 1 Then
                frmMain.lklHometeam.Links(0).Enabled = False
                frmMain.lklAwayteam.Links(0).Enabled = False
                frmMain.lklHometeam.LinkBehavior = LinkBehavior.NeverUnderline
                frmMain.lklAwayteam.LinkBehavior = LinkBehavior.NeverUnderline
            Else
                frmMain.lklAwayteam.Links(0).Enabled = True
                frmMain.lklHometeam.Links(0).Enabled = True
                frmMain.lklHometeam.LinkBehavior = LinkBehavior.HoverUnderline
                frmMain.lklAwayteam.LinkBehavior = LinkBehavior.HoverUnderline
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindDemoGames()
        Try
            m_dsDemoGames = m_objModule1Main.GetDemoGameDetails()
            If Not m_dsDemoGames Is Nothing Then
                If (m_dsDemoGames.Tables(1).Rows.Count > 0) Then
                End If
            Else
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub BindGames()
        Try
            If m_objLoginDetails.UserType = clsLoginDetails.m_enmUserType.OPS Then
                m_dsGames = m_objModule1Main.GetGameDetails(m_objLoginDetails.LanguageID, 1)
                If Not m_dsGames Is Nothing Then
                    If (m_dsGames.Tables(0).Rows.Count > 0) Then
                    End If
                Else
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    'Enable the panel link Lables
    Public Sub EnableLinkLabels(ByVal Enable As Boolean)
        Try
            For Each ctrl As LinkLabel In pnlStats.Controls.OfType(Of LinkLabel)()
                ctrl.Enabled = Enable
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub AddEventFieldsInMaintoDictionary()
        Try

            dictionaryListView.Add(11, New ClsPbpTree.ListBoxColumnData(lvwHomeGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(17, New ClsPbpTree.ListBoxColumnData(lvwHomeGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(28, New ClsPbpTree.ListBoxColumnData(lvwHomeGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(30, New ClsPbpTree.ListBoxColumnData(lvwHomeGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(2, New ClsPbpTree.ListBoxColumnData(lvwHomeBookings, "By Player", "BOOKING_REASON", "Event"))
            dictionaryListView.Add(7, New ClsPbpTree.ListBoxColumnData(lvwHomeBookings, "By Player", "BOOKING_REASON", "Event"))
            dictionaryListView.Add(22, New ClsPbpTree.ListBoxColumnData(lvwHomeSubstitutions, "Sub Type", "By Player", "Type"))
            dictionaryListView.Add(18, New ClsPbpTree.ListBoxColumnData(lvwHomePenalties, "By Player", "Type", "Type"))
            dictionaryListView.Add(11000, New ClsPbpTree.ListBoxColumnData(lvwVisitGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(17000, New ClsPbpTree.ListBoxColumnData(lvwVisitGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(28000, New ClsPbpTree.ListBoxColumnData(lvwVisitGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(30000, New ClsPbpTree.ListBoxColumnData(lvwVisitGoals, "Event", "By Player", "OFFENSE_DEFENSE_SCORE"))
            dictionaryListView.Add(2000, New ClsPbpTree.ListBoxColumnData(lvwVisitBookings, "By Player", "BOOKING_REASON", "Event"))
            dictionaryListView.Add(7000, New ClsPbpTree.ListBoxColumnData(lvwVisitBookings, "By Player", "BOOKING_REASON", "Event"))
            dictionaryListView.Add(22000, New ClsPbpTree.ListBoxColumnData(lvwVisitSubstitutions, "Sub Type", "By Player", "Type"))
            dictionaryListView.Add(18000, New ClsPbpTree.ListBoxColumnData(lvwVisitPenalties, "By Player", "Type", "Type"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub DisplayPbpEventsInMainPz(ByVal pbpdata As DataTable)
        Try
            If pbpdata.Rows.Count > 0 Then
                'GOAL -11 , PENALTY -17 , OWNGOAL -28,SHOOTOUT_GOAL -30
                'FILLING SUBSITITION EVENTS -- 22
                'FILLING BOOKINGS(YELLOW/RED CARDS)  2, 7
                'FILLING MISSED PENALTY EVENTS  18
                Dim addItemtoList As Boolean
                Dim eventsToShow() As Int16 = {11, 17, 28, 30, 22, 2, 7, 18}
                Dim recordsInsert = (From pbpSource In pbpdata _
                                         Where eventsToShow.Contains(pbpSource.Field(Of Int16)("EVENT_CODE_ID"))
                                         ).ToList()

                'To remove the row
                Dim ignoreListview() As String = {"lvwOfficials", "lvwMatchInformation"}
                Dim listviewcControl As New Dictionary(Of Decimal, Tuple(Of String, Integer))
                Dim lstcontrols = From lst In pnlStats.Controls.OfType(Of ListView)() Where Not ignoreListview.Contains(lst.Name)

                For Each listviewControl In lstcontrols
                    For Each listItem As ListViewItem In listviewControl.Items
                        Try
                            listviewcControl.Add(CDec(listItem.Tag), New Tuple(Of String, Integer)(CStr(listviewControl.Name), CInt(listItem.Index)))
                        Catch ex As Exception
                        End Try
                    Next
                Next
                Dim recordsRemove = (From pbpSource In pbpdata _
                                      Join list In listviewcControl.AsEnumerable() On list.Key Equals (pbpSource.Field(Of Decimal)("SEQUENCE_NUMBER")) _
                                      Where Not eventsToShow.Contains(pbpSource.Field(Of Int16)("EVENT_CODE_ID")) AndAlso pbpSource.Field(Of Int32?)("EDIT_UID") > 0 _
                                            ).ToList()

                For Each row In recordsRemove
                    Dim lstEvent As ListView = CType(pnlStats.Controls.Item(row.list.Value.Item1.ToString()), ListView)
                    lstEvent.Items(CInt(row.list.Value.Item2)).Remove()
                Next

                '--------------------------------
                For Each r In recordsInsert
                    Dim teamNumber1 As Integer = 1
                    If (r.Field(Of Int32)("TEAM_ID") = m_objGameDetails.AwayTeamID) Then
                        teamNumber1 = 1000
                    End If
                    Dim lstColumnData As ClsPbpTree.ListBoxColumnData = dictionaryListView.Item(CInt(r.Field(Of Int16)("EVENT_CODE_ID") * teamNumber1))
                    Dim lstEvent As ListView = lstColumnData.ListEvent
                    Dim listViewItemIndex As Integer = 0
                    addItemtoList = True

                    If (r.Field(Of Decimal)("SEQUENCE_NUMBER") = -1 Or r.Field(Of Int32)("EDIT_UID") > 0) Then 'Delete/remove the existing record in PBP tree
                        For Each listItem As ListViewItem In lstEvent.Items
                            If (CStr(lstEvent.Items(listViewItemIndex).Tag) = CStr(r.Field(Of Object)("SEQUENCE_NUMBER"))) Or _
                                   (CInt(lstEvent.Items(listViewItemIndex).Text) = CInt(r.Field(Of Object)("EDIT_UID"))) Then
                                addItemtoList = False
                                If r.Field(Of Decimal)("SEQUENCE_NUMBER") = -1 Then
                                    listItem.Remove()
                                Else
                                    lstEvent.Items(listViewItemIndex).SubItems(0).Text = r.Field(Of Int32)("UNIQUE_ID").ToString()
                                    lstEvent.Items(listViewItemIndex).SubItems(1).Text = r.Field(Of String)("Time").ToString()
                                    lstEvent.Items(listViewItemIndex).SubItems(2).Text = r.Field(Of Byte)("Period").ToString()
                                    lstEvent.Items(listViewItemIndex).SubItems(3).Text = CStr(r.Field(Of Object)(lstColumnData.Column1))
                                    lstEvent.Items(listViewItemIndex).SubItems(4).Text = CStr(r.Field(Of Object)(lstColumnData.Column2))
                                    lstEvent.Items(listViewItemIndex).SubItems(5).Text = CStr(r.Field(Of Object)(lstColumnData.Column3))
                                End If
                            End If
                            listViewItemIndex = listViewItemIndex + 1
                            If lstEvent.Items.Count <= listViewItemIndex Then
                                Exit For
                            End If
                        Next
                    End If

                    Try
                        If r.Field(Of Int32)("EDIT_UID") > 0 Then ''team replace
                            If (r.Field(Of Int32)("TEAM_ID") = m_objGameDetails.HomeTeamID) Then
                                teamNumber1 = 1000
                            Else
                                teamNumber1 = 1
                            End If

                            Dim lstColumnDataOpp As ClsPbpTree.ListBoxColumnData = dictionaryListView.Item(CInt(r.Field(Of Int16)("EVENT_CODE_ID") * teamNumber1))
                            Dim lstEventOpp As ListView = lstColumnDataOpp.ListEvent
                            Dim deleteItems = From li As ListViewItem In lstEventOpp.Items Where CDec(li.Tag) = r.Field(Of Decimal)("SEQUENCE_NUMBER") _
                                   Select li
                            For Each listItem In deleteItems
                                listItem.Remove()
                            Next
                        End If
                    Catch ex As Exception
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, "Main Screen team replace catch exception", 1, 0)
                    End Try


                    If addItemtoList And r.Field(Of Decimal)("SEQUENCE_NUMBER") <> -1 Then
                        'Add list view items.
                        Dim lstEventAdd As New ListViewItem
                        lstEventAdd = lstEvent.Items.Add(r.Field(Of Int32)("UNIQUE_ID").ToString())
                        With lstEventAdd
                            .SubItems.Add(r.Field(Of String)("Time").ToString())
                            .SubItems.Add(r.Field(Of Byte)("Period").ToString())
                            .SubItems.Add(CStr(r.Field(Of Object)(lstColumnData.Column1)))
                            .SubItems.Add(CStr(r.Field(Of Object)(lstColumnData.Column2)))
                            .SubItems.Add(CStr(r.Field(Of Object)(lstColumnData.Column3)))
                            .SubItems.Add(CStr(r.Field(Of Object)("SEQUENCE_NUMBER")))
                            .Tag = CStr(r.Field(Of Object)("SEQUENCE_NUMBER"))
                        End With
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, "Main Screen Listview " & lstEvent.Name & " Unique_id : " & r.Field(Of Int32)("UNIQUE_ID").ToString() & " Sequence_number : " & Math.Round(r.Field(Of Object)("SEQUENCE_NUMBER"), 3) & " Time : " & r.Field(Of String)("Time").ToString(), 1, 0)
                    End If
                Next
                ArrangeListViews()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub



End Class