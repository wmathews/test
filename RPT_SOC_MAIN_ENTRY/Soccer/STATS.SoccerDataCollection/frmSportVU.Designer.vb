﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSportVU
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.lvwSportVU = New System.Windows.Forms.ListView()
        Me.TimeElapsed = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.EventID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TeamAbbrev = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PlayerName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnClose = New System.Windows.Forms.Button()
        Me.tmrRefreshData = New System.Windows.Forms.Timer(Me.components)
        Me.picButtonBar = New System.Windows.Forms.Panel()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 363)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(467, 18)
        Me.sstMain.TabIndex = 290
        Me.sstMain.Text = "StatusStrip1"
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-85, 14)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(621, 24)
        Me.picReporterBar.TabIndex = 292
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-85, -1)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(621, 17)
        Me.picTopBar.TabIndex = 291
        Me.picTopBar.TabStop = False
        '
        'lvwSportVU
        '
        Me.lvwSportVU.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.TimeElapsed, Me.EventID, Me.TeamAbbrev, Me.PlayerName})
        Me.lvwSportVU.FullRowSelect = True
        Me.lvwSportVU.Location = New System.Drawing.Point(9, 46)
        Me.lvwSportVU.Name = "lvwSportVU"
        Me.lvwSportVU.Size = New System.Drawing.Size(450, 270)
        Me.lvwSportVU.TabIndex = 295
        Me.lvwSportVU.UseCompatibleStateImageBehavior = False
        Me.lvwSportVU.View = System.Windows.Forms.View.Details
        '
        'TimeElapsed
        '
        Me.TimeElapsed.Text = "Time Elapsed"
        Me.TimeElapsed.Width = 100
        '
        'EventID
        '
        Me.EventID.Text = "Event"
        Me.EventID.Width = 100
        '
        'TeamAbbrev
        '
        Me.TeamAbbrev.Text = "Team Abbrev"
        Me.TeamAbbrev.Width = 100
        '
        'PlayerName
        '
        Me.PlayerName.Text = "Player Name"
        Me.PlayerName.Width = 100
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(384, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 296
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'tmrRefreshData
        '
        Me.tmrRefreshData.Interval = 3000
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 325)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(536, 41)
        Me.picButtonBar.TabIndex = 297
        '
        'frmSportVU
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(467, 381)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.lvwSportVU)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmSportVU"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "SportVU"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents lvwSportVU As System.Windows.Forms.ListView
    Friend WithEvents TimeElapsed As System.Windows.Forms.ColumnHeader
    Friend WithEvents EventID As System.Windows.Forms.ColumnHeader
    Friend WithEvents TeamAbbrev As System.Windows.Forms.ColumnHeader
    Friend WithEvents PlayerName As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents tmrRefreshData As System.Windows.Forms.Timer
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
