﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
'STATS.SoccerDAL
Imports System.Text
Imports System.Drawing.Graphics
Imports System.Drawing.Imaging
Imports System.Drawing.Design

#End Region
#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmTeamSetup
' Author        : wilson
' Created Date  : 11-05-09
' Description   : This form is used for the reporting  players for Home and Away Team
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmFormationReport
#Region "Constants & Variables"
    Private m_objBLTeamSetup As New STATS.SoccerBL.clsTeamSetup
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private DsRostersInfo As DataSet = Nothing
    Private MessageDialog As New frmMessageDialog
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Dim m_strBuilder As StringBuilder
    Private m_objTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Dim bmp As New Bitmap(Width, Height)
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private dsTeamGame As DataSet = Nothing
    Dim intTeamID As Integer
    Dim hintTeamID As Integer
    Dim forstr As String
    Private m_HomeFormation As Integer
    Private m_AwayFormation As Integer
    Public fr1, fr2 As String
    Dim frmm1, frmm2 As String
    Dim formahm As String
    Dim formaaw As String
    Dim stag As String = String.Empty
    Private m_Assignments As New clsUpComingAssignments
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Dim gformat As String
    Private lsupport As New languagesupport

    Dim strForamtion1 As String = "Formation Report for "

#End Region
    Private Sub FrmFormationreport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmFormationReport", Nothing, 1, 0)
            strForamtion1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strForamtion1)
            loadreport()
            wbReport.Focus()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub loadreport()
        Try
            Dim objfrmTeamSetup As New frmTeamSetup("Home")
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnPrint.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPrint.Text)
                btnRefresh.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnRefresh.Text)
            End If
            intTeamID = m_objclsGameDetails.AwayTeamID
            hintTeamID = m_objclsGameDetails.HomeTeamID
            dsTeamGame = m_objBLTeamSetup.GetFormationCoach(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTeamID)
            dsTeamGame.Tables(0).TableName = "Formation"
            dsTeamGame.Tables(1).TableName = "Coach"
            dsTeamGame.Tables(3).TableName = "FormationSpec"
            dsTeamGame.Tables(4).TableName = "UniformImages"
            dsTeamGame.Tables("Formation").Columns.Add("HOME")
            dsTeamGame.Tables("Formation").Columns.Add("AWAY")
            UpdateFormationCoach()
            DsRostersInfo = m_objBLTeamSetup.GetRosterInfo(m_objclsGameDetails.GameCode, m_objclsGameDetails.LeagueID, m_objclsGameDetails.languageid)
            If DsRostersInfo.Tables.Count > 0 Then
                DsRostersInfo.Tables(0).TableName = "Home"
                DsRostersInfo.Tables(1).TableName = "Away"
            End If
            GetShirtAndShortColors(m_objGameDetails.HomeTeamID)
            GetShirtAndShortColors(m_objGameDetails.AwayTeamID)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub imagecall()
        Try
            If (UdcSoccerFormation1.Created = True) Then
                stag = ""
                UdcSoccerFormation1.USFPrintMode = True
                RefreshFormationDisplay("Home")
                bmp = UdcSoccerFormation1.USFGetBitmap()
                Dim he As Integer = bmp.Height
                Dim wid As Integer = bmp.Width
                Dim ss As String = Application.StartupPath() + "\" + "Resources\" + "home.bmp"
                takeSnapshot(bmp, he, wid, "" + ss + "")
                RefreshFormationDisplay("Away")
                bmp = UdcSoccerFormation1.USFGetBitmap()
                Dim he1 As Integer = bmp.Height
                Dim wid1 As Integer = bmp.Width
                Dim ss1 As String = Application.StartupPath() + "\" + "Resources\" + "away.bmp"
                'Dim ss1 As String = Application.StartupPath() + "\" + "away.bmp"
                takeSnapshot(bmp, he1, wid1, "" + ss1 + "")
                m_strBuilder = New StringBuilder
                Dim st As String = Application.StartupPath() + "\" + "Resources\" + "away.bmp"
                Dim ast As String = "" & " "" " + st + """ "
                Dim st1 As String = Application.StartupPath() + "\" + "Resources\" + "home.bmp"
                Dim ast1 As String = "" & " "" " + st1 + """ "
                Dim homstring As String = m_objGameDetails.HomeTeam
                Dim awstring As String = m_objGameDetails.AwayTeam
                Dim hmfrm As String
                Dim hmawr As String
                If (formaaw <> "" Or fr2 <> "") Then
                    hmfrm = "( " + formaaw + "  :  " + fr2 + " )"
                    If (fr2 = "") Then
                        hmfrm = hmfrm.Replace(":", "")
                    End If
                End If

                If (formahm <> "" Or fr1 <> "") Then
                    hmawr = "( " + formahm + "  :  " + fr1 + " )"
                    If (fr1 = "") Then
                        hmawr = hmawr.Replace(":", "")
                    End If
                End If

                Dim networkConnection = m_objGeneral.CheckNetConnection()
                If networkConnection Then
                    Dim dsreportergmformat As New DataSet
                    dsreportergmformat = m_Assignments.GetReporterSettings(m_objLoginDetails.UserId)

                    If (dsreportergmformat.Tables(0).Rows.Count = 1) Then
                        gformat = dsreportergmformat.Tables(0).Rows(0).ItemArray(3).ToString
                    End If
                End If
                wbReport.Refresh()
                If (gformat = "AT@HT") Then
                    wbReport.DocumentText = "<html> <center> <h4> " & strForamtion1 + awstring + "  @  " + homstring + " </h4> <br/><table><tr><td><B><center> " + awstring + "  " + hmfrm + "   </b> </center><tr><td><img src=" + ast + " border=" & "2" & "></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td><B><center>" + homstring + " " + hmawr + " </b></td></tr><tr><td><img src=" + ast1 + "  border=" & "2" & "></td></tr></table><center></panel></html>"
                Else
                    wbReport.DocumentText = "<html> <center> <h4>" & strForamtion1 + homstring + "  -  " + awstring + " </h4> <br/><table><tr><td><B><center> " + homstring + "  " + hmawr + "   </b> </center><tr><td><img src=" + ast1 + " border=" & "2" & "></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td><B><center>" + awstring + " " + hmfrm + " </b></td></tr><tr><td><img src=" + ast + "  border=" & "2" & "></td></tr></table><center></panel></html>"

                End If

                stag = "yes"
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub UpdateFormationCoach()
        Try
            Dim DR() As DataRow
            For j As Integer = 0 To dsTeamGame.Tables("table2").Rows.Count - 1
                If (dsTeamGame.Tables("table2").Rows(j).ItemArray(0).ToString = intTeamID) Then
                    If (dsTeamGame.Tables("table2").Rows(j).Item("start_formation_id").ToString <> "") Then

                        m_HomeFormation = CInt(dsTeamGame.Tables("table2").Rows(j).Item("start_formation_id").ToString)
                    End If
                End If
            Next
            DR = dsTeamGame.Tables("Formation").Select("FORMATION_ID = " & m_HomeFormation)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("AWAY") = m_objGameDetails.HomeTeamID
                'DR(0).Item("HOME") = m_objGameDetails.HomeTeamID
                DR(0).EndEdit()
            End If
            dsTeamGame.AcceptChanges()
            For k As Integer = 0 To dsTeamGame.Tables("table2").Rows.Count - 1
                If (dsTeamGame.Tables("table2").Rows(k).ItemArray(0).ToString = hintTeamID) Then
                    If (dsTeamGame.Tables("table2").Rows(k).Item("start_formation_id").ToString <> "") Then
                        m_AwayFormation = dsTeamGame.Tables("table2").Rows(k).Item("start_formation_id").ToString
                    End If

                End If
            Next
            DR = dsTeamGame.Tables("Formation").Select("FORMATION_ID = " & m_AwayFormation)
            If DR.Length > 0 Then
                DR(0).BeginEdit()
                DR(0).Item("HOME") = m_objGameDetails.AwayTeamID
                'DR(0).Item("AWAY") = m_objGameDetails.AwayTeamID
                DR(0).EndEdit()
            End If
            dsTeamGame.AcceptChanges()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            PrintDialog1.Document = PrintDocument1
            PrintDialog1.AllowSomePages = True
            PrintDocument1.DefaultPageSettings.Landscape = True
            If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim pritcopy As Integer = DirectCast(DirectCast(PrintDialog1, System.Windows.Forms.PrintDialog).PrinterSettings, System.Drawing.Printing.PrinterSettings).Copies
                For i As Integer = 0 To pritcopy - 1
                    wbReport.Print()
                Next
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ' wbReport.DocumentText = "<html><b>sdfdsf</b><table border=3 width=500 height=500><tr><td style=" & "position: fixed" & "><img src=""D:\Soccer Working Folder\RPT_SOC_MAIN_ENTRY\trunk\Soccer\STATS.SoccerDataCollection\bin\Debug\Resources\home.bmp""></td></tr>\n\n\n\n\n<tr><td></td></tr></table></panel></html>"
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh.Text, Nothing, 1, 0)
            loadreport()
            imagecall()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    'Public Function takeSnapshot(ByVal wb As WebBrowser, ByRef width As Integer, ByRef height As Integer, ByRef imgName As String) As Boolean
    Public Function takeSnapshot(ByVal wb As Bitmap, ByRef width As Integer, ByRef height As Integer, ByRef imgName As String) As Boolean
        Try

            'image from the Bitmap
            'Dim bmp As New Bitmap(width, height)
            Dim original As Image = bmp
            'Graphics object for formatting the image
            Dim rec As New Rectangle(0, 0, width, height)
            'wb.DrawToBitmap(bmp, rec)

            'Dim gfx As Graphics = Graphics.FromImage(original)
            ''gfx.CompositingQuality = Drawing2D.CompositingQuality.Default
            ''gfx.SmoothingMode = Drawing2D.SmoothingMode.Default
            ''gfx.InterpolationMode = Drawing2D.InterpolationMode.Default
            'gfx.DrawImage(original, rec)
            'gfx.CompositingQuality = CompositingQuality.HighQuality
            'gfx.SmoothingMode = SmoothingMode.HighQuality
            'gfx.InterpolationMode = InterpolationMode.HighQualityBicubic
            'draw the image
            'save as PNG
            'NOTE: Can change this to the desired format
            original.Save(imgName, ImageFormat.Bmp)
            'close and clean-up
            ' gfx.Dispose()
            bmp.Dispose()
            original.Dispose()
            'PrintDocument1.Print()
            Return True
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim path As String
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, 1, 0)
            If wbReport.DocumentText = Nothing Then
                'essageDialog.Show("No Document is Present", Me.Text)
                Exit Sub
            Else
                path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                SaveFileDialog1.InitialDirectory = path
                SaveFileDialog1.Filter = "HTML Files|*.htm"
                If SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fileName As String = SaveFileDialog1.FileName
                    Dim sw As StreamWriter
                    If File.Exists(fileName) = False Then
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(wbReport.DocumentText)
                        sw.Flush()
                        sw.Close()
                    Else
                        File.Delete(fileName)
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(wbReport.DocumentText)
                        sw.Flush()
                        sw.Close()
                    End If
                End If
            End If

            'm_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 0)
            'm_strBuilder = New StringBuilder
            'm_strBuilder.Append("<html><body>")
            'm_strBuilder.Append("<center><IMG SRC=" & "rpt.bmp" & "></center>")
            'm_strBuilder.Append("<b> </b>")
            'm_strBuilder.Append("<body></html>")
            'WebBrowser1.DocumentText = m_strBuilder.ToString
            'If wbReport.DocumentText = Nothing Then
            '    'essageDialog.Show("No Document is Present", Me.Text)
            '    Exit Sub
            'Else
            '    'Dim x As Integer = Me.Location.X

            '    'Dim y As Integer = Me.Location.Y

            '    'Me.win
            '    ' Me.SetBounds(Me.Location.X, Me.Location.Y, 771, 910)

            '    'wbReport.ScrollBarsEnabled = False
            '    ' Me.WindowState = FormWindowState.Maximized
            '    ' wbReport.ScrollBarsEnabled = False

            '    path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            '    SaveFileDialog1.InitialDirectory = path
            '    SaveFileDialog1.Filter = "HTML Files|*.htm"
            '    If SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            '        Dim fileName As String = SaveFileDialog1.FileName
            '        Dim fi As New FileInfo(SaveFileDialog1.FileName)
            '        Dim sw As StreamWriter
            '        Dim rpath = fi.DirectoryName
            '        If File.Exists(fileName) = False Then
            '        Else
            '            File.Delete(fileName)
            '        End If

            '        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
            '        sw.Write(m_strBuilder)
            '        sw.Flush()
            '        sw.Close()
            '        ' Dim st1 As String = "c:\rpt.bmp"
            '        ' loadreport()
            '        'wbReport.Refresh()
            '        'Dim bmp1 As New Bitmap(1250, wbReport.Height)
            '        Dim bmp1 As New Bitmap(wbReport.Width - 17, 841)
            '        wbReport.Width = 1250
            '        wbReport.Height = 841

            '        'Dim bmp1 As New Bitmap(wbReport.Width - 17, 841)
            '        'Dim bmp1 As New Bitmap(771, 910)
            '        Dim original1 As Image = bmp1
            '        'Dim rec As New Rectangle(0, 0, 771, 910)
            '        Dim rec As New Rectangle(0, 0, wbReport.Width, wbReport.Height)
            '        wbReport.DrawToBitmap(bmp1, rec)
            '        Dim gfx As Graphics = Graphics.FromImage(original1)
            '        gfx.CompositingQuality = Drawing2D.CompositingQuality.GammaCorrected
            '        gfx.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
            '        gfx.InterpolationMode = Drawing2D.InterpolationMode.High
            '        gfx.DrawImage(original1, rec)
            '        Dim imgName1 As String = rpath + "\rpt.bmp"
            '        original1.Save(imgName1, ImageFormat.Bmp)
            '        gfx.Dispose()
            '        bmp.Dispose()
            '        original1.Dispose()
            '        Me.WindowState = FormWindowState.Normal
            '        'sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
            '        'sw.Write(m_strBuilder)
            '        'sw.Flush()
            '        'sw.Close()

            '    End If
            '    wbReport.ScrollBarsEnabled = True
            'End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)

        End Try
    End Sub
    Private Sub RefreshFormationDisplay(ByVal team As String)
        Try
            forstr = ""
            For z As Integer = 0 To dsTeamGame.Tables("Formation").Rows.Count - 1
                If (dsTeamGame.Tables("Formation").Rows(z).Item(team).ToString <> "") Then
                    forstr = dsTeamGame.Tables("Formation").Rows(z).ItemArray(1).ToString
                End If
            Next
            UdcSoccerFormation1.USFFormation = forstr
            If (team = "Home") Then

                For x As Integer = 0 To dsTeamGame.Tables("table2").Rows.Count - 1
                    If (dsTeamGame.Tables("table2").Rows(x).Item("Team_id") = m_objGameDetails.HomeTeamID) Then
                        If (dsTeamGame.Tables("table2").Rows(x).Item("start_formation_id").ToString <> "") Then
                            frmm1 = dsTeamGame.Tables("table2").Rows(x).Item("start_formation_id")
                        End If
                        If (dsTeamGame.Tables("table2").Rows(x).Item("formation_spec_id").ToString <> "") Then
                            frmm2 = dsTeamGame.Tables("table2").Rows(x).Item("formation_spec_id")
                        End If

                    End If
                Next
                For z As Integer = 0 To dsTeamGame.Tables("formationspec").Rows.Count - 1
                    If (dsTeamGame.Tables("formationspec").Rows(z).Item("formation_spec_id") = frmm2 And dsTeamGame.Tables("formationspec").Rows(z).Item("formation_id") = frmm1) Then
                        formahm = dsTeamGame.Tables("formationspec").Rows(z).Item("formation_spec_id")

                    End If
                Next
                UdcSoccerFormation1.USFFormationSpecification = formahm
            Else

                For x As Integer = 0 To dsTeamGame.Tables("table2").Rows.Count - 1
                    If (dsTeamGame.Tables("table2").Rows(x).Item("Team_id") = m_objGameDetails.AwayTeamID) Then
                        If (dsTeamGame.Tables("table2").Rows(x).Item("start_formation_id").ToString <> "") Then
                            frmm1 = dsTeamGame.Tables("table2").Rows(x).Item("start_formation_id")
                        End If
                        If (dsTeamGame.Tables("table2").Rows(x).Item("formation_spec_id").ToString <> "") Then
                            frmm2 = dsTeamGame.Tables("table2").Rows(x).Item("formation_spec_id")
                        End If
                    End If
                Next
                For z As Integer = 0 To dsTeamGame.Tables("formationspec").Rows.Count - 1
                    If (dsTeamGame.Tables("formationspec").Rows(z).Item("formation_spec_id") = frmm2 And dsTeamGame.Tables("formationspec").Rows(z).Item("formation_id") = frmm1) Then
                        formaaw = dsTeamGame.Tables("formationspec").Rows(z).Item("formation_spec_id")
                    End If
                Next
                UdcSoccerFormation1.USFFormationSpecification = formaaw
            End If
            UdcSoccerFormation1.USFRoster = DsRostersInfo.Tables(team)
            Dim dtStartingLineup As New DataTable
            Dim drStartingLineup As DataRow
            dtStartingLineup.Columns.Add("LINEUP")
            dtStartingLineup.Columns(0).DataType = GetType(Integer)
            dtStartingLineup.Columns.Add("PLAYER_ID")
            dtStartingLineup.Columns(1).DataType = GetType(Integer)
            dtStartingLineup.Columns.Add("PLAYER")
            dtStartingLineup.Columns.Add("UNIFORM")
            dtStartingLineup.Columns.Add("LAST_NAME")
            dtStartingLineup.Columns.Add("MONIKER")
            dtStartingLineup.Columns.Add("POSITION_ABBREV")
            Dim dtBench As New DataTable
            Dim drBench As DataRow
            If (team = "Home") Then
                fr1 = forstr
                'UdcSoccerFormation1.USFShirtColor = Color.MediumSeaGreen
                UdcSoccerFormation1.USFShirtColor = m_objTeamSetup.HomeShirtColor
                UdcSoccerFormation1.USFShortColor = m_objTeamSetup.HomeShortColor

                'If (m_objTeamSetup.HomeShirtColor.ToString = "Color [A=255, R=0, G=0, B=0]") Then

                '    UdcSoccerFormation1.USFShirtColor = Color.MediumSeaGreen
                'End If
                'If (m_objTeamSetup.HomeShortColor.ToString = "Color [A=255, R=0, G=0, B=0]") Then

                '    UdcSoccerFormation1.USFShortColor = Color.MediumSeaGreen
                'End If

                If (m_objTeamSetup.HomeShirtColor = Color.Empty) Then

                    UdcSoccerFormation1.USFShirtColor = Color.MediumSeaGreen
                End If
                If (m_objTeamSetup.HomeShortColor = Color.Empty) Then

                    UdcSoccerFormation1.USFShortColor = Color.MediumSeaGreen
                End If

                UdcSoccerFormation1.USFRoster = DsRostersInfo.Tables("Home")
                'For i As Integer = 0 To DsRostersInfo.Tables("Away").Rows.Count - 1
                For i As Integer = 0 To DsRostersInfo.Tables("Home").Rows.Count - 1
                    drStartingLineup = dtStartingLineup.NewRow
                    drStartingLineup("LINEUP") = DsRostersInfo.Tables("Home").Rows(i).Item("starting_position")
                    drStartingLineup("PLAYER_ID") = DsRostersInfo.Tables("Home").Rows(i).Item("PLAYER_ID")
                    drStartingLineup("PLAYER") = DsRostersInfo.Tables("Home").Rows(i).Item("PLAYER")
                    drStartingLineup("UNIFORM") = DsRostersInfo.Tables("Home").Rows(i).Item("DISPLAY_UNIFORM_NUMBER")
                    drStartingLineup("LAST_NAME") = DsRostersInfo.Tables("Home").Rows(i).Item("LAST_NAME")
                    drStartingLineup("MONIKER") = DsRostersInfo.Tables("Home").Rows(i).Item("MONIKER")
                    drStartingLineup("POSITION_ABBREV") = DsRostersInfo.Tables("Home").Rows(i).Item("POSITION_ABBREV")
                    dtStartingLineup.Rows.Add(drStartingLineup)
                Next
                dtBench.Columns.Add("LINEUP")
                dtBench.Columns(0).DataType = GetType(Integer)
                dtBench.Columns.Add("PLAYER_ID")
                dtBench.Columns(1).DataType = GetType(Integer)
                dtBench.Columns.Add("PLAYER")
                dtBench.Columns.Add("UNIFORM")
                dtBench.Columns.Add("LAST_NAME")
                dtBench.Columns.Add("MONIKER")
                dtBench.Columns.Add("POSITION_ABBREV")
                For i As Integer = 0 To DsRostersInfo.Tables("Home").Rows.Count - 1
                    ' If (DsRostersInfo.Tables("Away").Rows(0).ItemArray(8).ToString <> "0") Then
                    If (DsRostersInfo.Tables("Home").Rows(0).ItemArray(8).ToString <> "0") Then
                        drBench = dtBench.NewRow
                        drBench("LINEUP") = DsRostersInfo.Tables("Home").Rows(i).Item("starting_position")
                        drBench("PLAYER_ID") = DsRostersInfo.Tables("Home").Rows(i).Item("PLAYER_ID")
                        drBench("PLAYER") = DsRostersInfo.Tables("Home").Rows(i).Item("PLAYER")
                        drBench("UNIFORM") = DsRostersInfo.Tables("Home").Rows(i).Item("DISPLAY_UNIFORM_NUMBER")
                        drStartingLineup("LAST_NAME") = DsRostersInfo.Tables("Home").Rows(i).Item("LAST_NAME")
                        drStartingLineup("MONIKER") = DsRostersInfo.Tables("Home").Rows(i).Item("MONIKER")
                        drStartingLineup("POSITION_ABBREV") = DsRostersInfo.Tables("Home").Rows(i).Item("POSITION_ABBREV")
                        dtBench.Rows.Add(drBench)
                    End If
                Next
            Else
                fr2 = forstr

                UdcSoccerFormation1.USFShirtColor = m_objTeamSetup.AwayShirtColor
                UdcSoccerFormation1.USFShortColor = m_objTeamSetup.AwayShortColor

                'If (m_objTeamSetup.AwayShirtColor.ToString = "Color [A=255, R=0, G=0, B=0]") Then

                '    UdcSoccerFormation1.USFShirtColor = Color.MediumSeaGreen
                'End If
                'If (m_objTeamSetup.AwayShortColor.ToString = "Color [A=255, R=0, G=0, B=0]") Then

                '    UdcSoccerFormation1.USFShortColor = Color.MediumSeaGreen
                'End If

                If (m_objTeamSetup.AwayShirtColor = Color.Empty) Then

                    UdcSoccerFormation1.USFShirtColor = Color.MediumSeaGreen
                End If
                If (m_objTeamSetup.AwayShortColor = Color.Empty) Then

                    UdcSoccerFormation1.USFShortColor = Color.MediumSeaGreen
                End If

                UdcSoccerFormation1.USFRoster = DsRostersInfo.Tables("Home")
                For i As Integer = 0 To DsRostersInfo.Tables("Away").Rows.Count - 1
                    drStartingLineup = dtStartingLineup.NewRow
                    drStartingLineup("LINEUP") = DsRostersInfo.Tables("Away").Rows(i).Item("starting_position")
                    drStartingLineup("PLAYER_ID") = DsRostersInfo.Tables("Away").Rows(i).Item("PLAYER_ID")
                    drStartingLineup("PLAYER") = DsRostersInfo.Tables("Away").Rows(i).Item("PLAYER")
                    drStartingLineup("UNIFORM") = DsRostersInfo.Tables("Away").Rows(i).Item("DISPLAY_UNIFORM_NUMBER")
                    drStartingLineup("LAST_NAME") = DsRostersInfo.Tables("Away").Rows(i).Item("LAST_NAME")
                    drStartingLineup("MONIKER") = DsRostersInfo.Tables("Away").Rows(i).Item("MONIKER")
                    drStartingLineup("POSITION_ABBREV") = DsRostersInfo.Tables("Away").Rows(i).Item("POSITION_ABBREV")
                    dtStartingLineup.Rows.Add(drStartingLineup)
                Next
                dtBench.Columns.Add("LINEUP")
                dtBench.Columns(0).DataType = GetType(Integer)
                dtBench.Columns.Add("PLAYER_ID")
                dtBench.Columns(1).DataType = GetType(Integer)
                dtBench.Columns.Add("PLAYER")
                dtBench.Columns.Add("UNIFORM")
                dtBench.Columns.Add("LAST_NAME")
                dtBench.Columns.Add("MONIKER")
                dtBench.Columns.Add("POSITION_ABBREV")
                For i As Integer = 0 To DsRostersInfo.Tables("Away").Rows.Count - 1
                    If (DsRostersInfo.Tables("Away").Rows(0).ItemArray(8).ToString <> "0") Then
                        drBench = dtBench.NewRow
                        drBench("LINEUP") = DsRostersInfo.Tables("Away").Rows(i).Item("starting_position")
                        drBench("PLAYER_ID") = DsRostersInfo.Tables("Away").Rows(i).Item("PLAYER_ID")
                        drBench("PLAYER") = DsRostersInfo.Tables("Away").Rows(i).Item("PLAYER")
                        drBench("UNIFORM") = DsRostersInfo.Tables("Away").Rows(i).Item("DISPLAY_UNIFORM_NUMBER")
                        drStartingLineup("LAST_NAME") = DsRostersInfo.Tables("Away").Rows(i).Item("LAST_NAME")
                        drStartingLineup("MONIKER") = DsRostersInfo.Tables("Away").Rows(i).Item("MONIKER")
                        drStartingLineup("POSITION_ABBREV") = DsRostersInfo.Tables("Away").Rows(i).Item("POSITION_ABBREV")
                        dtBench.Rows.Add(drBench)
                    End If
                Next
            End If
            UdcSoccerFormation1.USFStartingLineup = dtStartingLineup
            UdcSoccerFormation1.USFBench = dtBench
            UdcSoccerFormation1.USFRefreshFormationDisplay()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub GetCurrentPlayersAfterRestart()
        Try
            'Getting the current players from red card and sub events
            Dim DsSubEvents As DataSet = m_objGeneral.FetchPBPEventsToDisplay(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If DsSubEvents.Tables(0).Rows.Count > 0 Then
                For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                    Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                        Case 7, 22
                            For intTableCount As Integer = 2 To m_objTeamSetup.RosterInfo.Tables.Count - 1
                                Dim DrPlayerOut() As DataRow = m_objTeamSetup.RosterInfo.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) & "")
                                If DrPlayerOut.Length > 0 Then
                                    If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = 7 Then
                                        'RED CARD
                                        DrPlayerOut(0).BeginEdit()
                                        DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                        DrPlayerOut(0).EndEdit()
                                        DrPlayerOut(0).AcceptChanges()
                                    Else
                                        'SUBSTITUTION
                                        Dim DrPlayerIn() As DataRow = m_objTeamSetup.RosterInfo.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) & "")
                                        If DrPlayerIn.Length > 0 Then
                                            DrPlayerIn(0).BeginEdit()
                                            DrPlayerIn(0).Item("STARTING_POSITION") = DrPlayerOut(0).Item("STARTING_POSITION")
                                            DrPlayerIn(0).EndEdit()

                                            DrPlayerOut(0).BeginEdit()
                                            DrPlayerOut(0).Item("STARTING_POSITION") = 0
                                            DrPlayerOut(0).EndEdit()

                                            DrPlayerIn(0).AcceptChanges()
                                        End If
                                    End If
                                End If
                            Next
                    End Select
                Next
            End If

            m_objTeamSetup.RosterInfo.AcceptChanges()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            'loadreport()
            'Timer1.Stop()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub FrmFormationreport_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try

            imagecall()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub picTopBar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picTopBar.Click

    End Sub

    Private Sub picReporterBar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picReporterBar.Click

    End Sub

    Private Sub picCompanyLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub wbReport_PreviewKeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles wbReport.PreviewKeyDown
        If (e.KeyCode.ToString = "F5") Then
            If (btnRefresh.Enabled.ToString() = "True") Then
                btnRefresh_Click(sender, e)
            End If
        End If
    End Sub
    Private Sub GetShirtAndShortColors(ByVal intTeamID)
        Dim dsForCoach As New DataSet
        dsForCoach = m_objBLTeamSetup.GetFormationCoach(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, intTeamID)
        If dsForCoach.Tables(2).Rows.Count > 0 Then

            If CInt(dsForCoach.Tables(2).Rows(0).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                If dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString() = "" Then
                    m_objTeamSetup.HomeShirtColor = Color.Empty
                Else
                    m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString)
                End If

                If dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString() = "" Then
                    m_objTeamSetup.HomeShortColor = Color.Empty
                Else
                    m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString)
                End If

                If dsForCoach.Tables(2).Rows.Count > 1 Then
                    If dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString() = "" Then
                        m_objTeamSetup.AwayShirtColor = Color.Empty
                    Else
                        m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString)
                    End If

                    If dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString() = "" Then
                        m_objTeamSetup.AwayShortColor = Color.Empty
                    Else
                        m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString)
                    End If
                End If
            Else
                If dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString() = "" Then
                    m_objTeamSetup.AwayShirtColor = Color.Empty
                Else
                    m_objTeamSetup.AwayShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHIRT_COLOR").ToString)
                End If

                If dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString() = "" Then
                    m_objTeamSetup.AwayShortColor = Color.Empty
                Else
                    m_objTeamSetup.AwayShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(0).Item("SHORT_COLOR").ToString)
                End If

                If dsForCoach.Tables(2).Rows.Count > 1 Then
                    If dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString() = "" Then
                        m_objTeamSetup.HomeShirtColor = Color.Empty
                    Else
                        m_objTeamSetup.HomeShirtColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHIRT_COLOR").ToString)
                    End If

                    If dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString() = "" Then
                        m_objTeamSetup.HomeShortColor = Color.Empty
                    Else
                        m_objTeamSetup.HomeShortColor = clsUtility.ConvertHexadecimalStringToColor(dsForCoach.Tables(2).Rows(1).Item("SHORT_COLOR").ToString)
                    End If
                End If
            End If
        End If
    End Sub
End Class