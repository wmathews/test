﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmManagerExpulsion
' Author        : Shravani
' Created Date  : 06-08-09
' Description   :
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmManagerExpulsion

#Region " Constants & Variables "
    Private m_objUtility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_blnIsComboLoaded As Boolean = False
    Private m_objModule1PBP As New frmModule1PBP
    Private m_objModule1Main As New frmModule1Main
    Private m_objManagerExpl As New STATS.SoccerBL.clsManagerExpulsion
    Private m_ExplManagerName As String
    Private MessageDialog As New frmMessageDialog

    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200
    Private lsupport As New languagesupport
    Public strmessage As String = "Select a Team/Manager! " & " ," & " Manager Expulsion"
    Public strmessage1 As String = "Current time should be greater than entered time!"
    Public strmessage2 As String = "Enter the correct time!" & ", " & "Manager Expulsion&"
    Public strmessage3 As String = "No manager to expel!" & ", " & " Manager Expulsion"
    Private strmessage4 As String = "Manager Expulsion"
    Private strmessage5 As String = "Manager expulsion already exist for this team"
    Private _objActions As ClsActions = ClsActions.GetInstance()
    Private _objGeneral As clsGeneral = clsGeneral.GetInstance()
#End Region

    ''' <summary>
    ''' Handles the Load event of the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmComments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim TEMLENINT As Integer
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmComments", Nothing, 1, 0)
            Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)
            Me.CenterToParent()
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                TEMLENINT = Label1.Text.Length
                Label1.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label1.Text)
                linebreakrdo(Label1, TEMLENINT)
                lblReason.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblReason.Text)
                lblHomeTeamName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblHomeTeamName.Text)
                Label2.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Label2.Text)
                btnOK.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnOK.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                strmessage2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage2)
                strmessage3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage3)
                strmessage4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage4)
            End If

            'LOAD TEAM COMBO
            LoadTeamCombo()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub linebreakrdo(ByVal lbtemp As Label, ByVal temp As Int32)
        If (lbtemp.Text.Length > temp) Then
            lbtemp.Text = lbtemp.Text.Substring(0, temp) + Environment.NewLine + lbtemp.Text.Substring(temp, lbtemp.Text.Length - temp)
        End If
    End Sub

    ''' <summary>
    ''' Loads team combo
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadTeamCombo()
        Try
            ''Dim dsTeam As New DataSet
            ''Dim dtTeam As DataTable
            ''dsTeam = m_objclsComments.LoadTeam(m_objGameDetails.GameCode, m_objGameDetails.LeagueID)
            ''dtTeam = dsTeam.Tables(0).Copy
            ''clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.BLANK, dtTeam, "TEAMNAME", "TEAM_ID")
            'cmbTeam.SelectedValue = m_objGameDetails.HomeTeamID
            'm_blnIsComboLoaded = True

            Dim dtTeam As New DataTable
            Dim dtReason As DataTable
            Dim dsReasonInfo As DataSet
            Dim TeamID As Integer
            Dim intRowCount As Integer
            Dim dsManager As DataSet
            Dim CoachID As Integer

            CoachID = m_objGameDetails.OffensiveCoachID

            dtTeam.Columns.Add("TEAM_ID")
            dtTeam.Columns.Add("TEAM_NAME")

            'Dim drTeam As DataRow
            ''HOME TEAM
            'drTeam = dtTeam.NewRow()
            'drTeam("TEAM_ID") = m_objGameDetails.HomeTeamID
            'drTeam("TEAM_NAME") = m_objGameDetails.HomeTeam
            'dtTeam.Rows.Add(drTeam)

            ''AWAY TEAM
            'drTeam = dtTeam.NewRow()
            'drTeam("TEAM_ID") = m_objGameDetails.AwayTeamID
            'drTeam("TEAM_NAME") = m_objGameDetails.AwayTeam
            'dtTeam.Rows.Add(drTeam)
            'clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.NONE, dtTeam, "TEAM_NAME", "TEAM_ID")
            dsManager = m_objManagerExpl.GetFormationCoach(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, "LIVE", m_objGameDetails.languageid)
            If dsManager.Tables(0).Rows.Count = 0 And m_objGameDetails.IsEditMode = False Then
                'Both Managers already expelled
                '  MessageDialog.Show(strmessage10, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                MessageDialog.Show(strmessage3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Me.DialogResult = Windows.Forms.DialogResult.No
                Exit Sub
            End If

            'Fill the teams
            '
            If dsManager.Tables(0).Rows.Count = 2 Then
                Dim drTeam As DataRow
                'HOME TEAM
                drTeam = dtTeam.NewRow()
                drTeam("TEAM_ID") = m_objGameDetails.HomeTeamID
                drTeam("TEAM_NAME") = m_objGameDetails.HomeTeam
                dtTeam.Rows.Add(drTeam)

                'AWAY TEAM
                drTeam = dtTeam.NewRow()
                drTeam("TEAM_ID") = m_objGameDetails.AwayTeamID
                drTeam("TEAM_NAME") = m_objGameDetails.AwayTeam
                dtTeam.Rows.Add(drTeam)
                clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.NONE, dtTeam, "TEAM_NAME", "TEAM_ID")
            ElseIf dsManager.Tables(0).Rows.Count = 1 Then
                If CInt(dsManager.Tables(0).Rows(0).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                    Dim drTeam As DataRow
                    'HOME TEAM
                    drTeam = dtTeam.NewRow()
                    drTeam("TEAM_ID") = m_objGameDetails.HomeTeamID
                    drTeam("TEAM_NAME") = m_objGameDetails.HomeTeam
                    dtTeam.Rows.Add(drTeam)
                    clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.NONE, dtTeam, "TEAM_NAME", "TEAM_ID")
                    cmbTeam.SelectedValue = drTeam("TEAM_ID")
                Else
                    Dim drTeam As DataRow
                    drTeam = dtTeam.NewRow()
                    drTeam("TEAM_ID") = m_objGameDetails.AwayTeamID

                    drTeam("TEAM_NAME") = m_objGameDetails.AwayTeam
                    dtTeam.Rows.Add(drTeam)
                    clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.NONE, dtTeam, "TEAM_NAME", "TEAM_ID")
                    cmbTeam.SelectedValue = drTeam("TEAM_ID")
                End If
                txtManagerExpl.Text = dsManager.Tables(0).Rows(0).Item("COACH_NAME")
            End If
            '
            'clsUtility.LoadControl(CmbReason, clsUtility.SelectTypeDisplay.BLANK, dtReason, "BOOKING_REASON", "BOOKING_REASON_ID")

            TeamID = CInt(cmbTeam.SelectedValue)

            If m_objGameDetails.IsEditMode = False Or m_objGameDetails.OffensiveCoachID = 0 Then
                For intRowCount = 0 To dsManager.Tables(0).Rows.Count - 1
                    If TeamID = CInt(dsManager.Tables(0).Rows(intRowCount).Item("TEAM_ID")) Then
                        txtManagerExpl.Text = dsManager.Tables(0).Rows(intRowCount).Item("COACH_NAME")
                        m_objGameDetails.OffensiveCoachID = CInt(dsManager.Tables(0).Rows(intRowCount).Item("COACH_ID"))
                        CoachID = m_objGameDetails.OffensiveCoachID
                        Continue For
                    End If
                Next
            End If

            'Get Expel Reason
            dsReasonInfo = m_objManagerExpl.GetReasonData(m_objGameDetails.languageid)

            dtReason = dsReasonInfo.Tables(0).Copy

            clsUtility.LoadControl(CmbReason, clsUtility.SelectTypeDisplay.BLANK, dtReason, "BOOKING_REASON", "BOOKING_REASON_ID")
            If m_objGameDetails.IsEditMode Then
                Dim drTeam As DataRow
                Dim dsManagerName As DataSet
                'HOME TEAM
                drTeam = dtTeam.NewRow()
                drTeam("TEAM_ID") = m_objGameDetails.OffensiveTeamID
                intRowCount = 0
                'If m_objGameDetails.OffensiveTeamID = m_objGameDetails.HomeTeamID Then
                '    drTeam("TEAM_NAME") = m_objGameDetails.HomeTeam
                'Else
                '    drTeam("TEAM_NAME") = m_objGameDetails.AwayTeam
                'End If
                'dtTeam.Rows.Add(drTeam)
                clsUtility.LoadControl(cmbTeam, clsUtility.SelectTypeDisplay.NONE, dtTeam, "TEAM_NAME", "TEAM_ID")
                cmbTeam.SelectedValue = drTeam("TEAM_ID")
                CmbReason.SelectedValue = m_objGameDetails.ExplReasonID

                dsManagerName = m_objManagerExpl.GetFormationCoach(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CoachID, "EDIT", m_objGameDetails.languageid)
                m_ExplManagerName = CStr(dsManagerName.Tables(0).Rows(intRowCount).Item("COACH_NAME"))
                txtManagerExpl.Text = m_ExplManagerName
                txtTime.Text = CalculateTimeElapseAfter(m_objGameDetails.ManagerExplTime, m_objGameDetails.CurrentPeriod)
                'need to add the Manager name here
            Else
                txtTime.Text = frmMain.UdcRunningClock1.URCCurrentTime
            End If
            If (m_objGameDetails.CoverageLevel = 6) Then
                txtTime.Enabled = False
            End If
            m_blnIsComboLoaded = True

            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(5, CChar(" "))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTeam.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbTeam.Text, Nothing, 1, 0)
            Dim TeamID As Integer
            Dim intRowCount As Integer
            Dim dsManager As DataSet
            If m_blnIsComboLoaded Then
                TeamID = CInt(cmbTeam.SelectedValue)
                If TeamID = 0 Then
                    txtManagerExpl.Text = ""
                    Exit Sub
                End If
                dsManager = m_objManagerExpl.GetFormationCoach(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, 0, "LIVE", m_objGameDetails.languageid)

                For intRowCount = 0 To dsManager.Tables(0).Rows.Count - 1
                    If TeamID = CInt(dsManager.Tables(0).Rows(intRowCount).Item("TEAM_ID")) Then
                        txtManagerExpl.Text = dsManager.Tables(0).Rows(intRowCount).Item("COACH_NAME")
                        m_objGameDetails.OffensiveCoachID = CInt(dsManager.Tables(0).Rows(intRowCount).Item("COACH_ID"))
                        Exit Sub
                    End If
                Next
                If m_objGameDetails.OffensiveTeamID = cmbTeam.SelectedValue And m_objGameDetails.IsEditMode Then
                    txtManagerExpl.Text = m_ExplManagerName
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbTeam_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTeam.SelectedValueChanged
        'Try
        '    Dim TeamID As Integer
        '    Dim intRowCount As Integer
        '    Dim dsManager As DataSet
        '    If cmbTeam.Text <> "" Then
        '        TeamID = CInt(cmbTeam.SelectedValue)
        '        dsManager = m_objManagerExpl.GetFormationCoach(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, TeamID)
        '        For intRowCount = 0 To dsManager.Tables.Count - 1
        '            If TeamID = CInt(dsManager.Tables(1).Rows(intRowCount).Item("TEAM_ID")) Then
        '                txtManagerExpl.Text = CInt(dsManager.Tables(1).Rows(1).Item("COACH_ID"))
        '                Exit Sub
        '            End If
        '        Next

        '    End If
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOK.Text, Nothing, 1, 0)
            Dim CurrentClockTime As Integer
            Dim EditedTime As Integer
            CurrentClockTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
            EditedTime = CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text))
            If txtTime.Text.Replace(" :", "").Trim = "" Then
                MessageDialog.Show(strmessage2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            ElseIf EditedTime > CurrentClockTime Then
                MessageDialog.Show(strmessage1, strmessage4, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            Else

            End If
            'If txtComments.Text = "" Then
            '    essageDialog.Show("Please enter Comments", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            '    Exit Sub
            'Else
            '    m_objGameDetails.CommentData = txtComments.Text
            'End If
            If (m_objGameDetails.CoverageLevel = 6) Then
                Dim dtPbpEvents As DataTable = _objGeneral.GetPZActionreport(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CInt(IIf(m_objGameDetails.IsDefaultGameClock, 1, 0))).Tables(0)
                'Allow manager expulsion only once for a team
                Dim recordsInsert = (From pbpSource In dtPbpEvents _
                                         Where pbpSource.Field(Of Int16)("EVENT_CODE_ID") = 48 And pbpSource.Field(Of Int32)("TEAM_ID") = CInt(cmbTeam.SelectedValue) And pbpSource.Field(Of Int16)("TIME_ELAPSED") <> CInt(EditedTime)
                                         ).ToList()
                If recordsInsert.Count > 0 Then
                    MessageDialog.Show(strmessage5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Try
                End If
            End If


            If CInt(cmbTeam.SelectedValue) > 0 Then
                m_objGameDetails.OffensiveTeamID = CInt(cmbTeam.SelectedValue)
            Else
                MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                Exit Sub
            End If
            m_objGameDetails.ManagerExplTime = EditedTime
            m_objGameDetails.ExplReasonID = CmbReason.SelectedValue
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            If m_objGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                End If
            End If
            Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub txtManagerExpl_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtManagerExpl.Leave
        Try
            If (txtManagerExpl.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtManagerExpl.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtTime_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTime.Leave
        Try
            If (txtTime.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTime.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub CmbReason_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbReason.SelectedIndexChanged
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, CmbReason.Text, Nothing, 1, 0)
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
End Class