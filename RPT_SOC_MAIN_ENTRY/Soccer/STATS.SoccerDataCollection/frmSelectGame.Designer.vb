﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwGames = New System.Windows.Forms.ListView()
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SerialNo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTimeZone = New System.Windows.Forms.Label()
        Me.radFeedB = New System.Windows.Forms.RadioButton()
        Me.radFeedA = New System.Windows.Forms.RadioButton()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.radFeedD = New System.Windows.Forms.RadioButton()
        Me.radFeedC = New System.Windows.Forms.RadioButton()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sstMain.SuspendLayout()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvwGames
        '
        Me.lvwGames.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader9, Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader10, Me.SerialNo})
        Me.lvwGames.FullRowSelect = True
        Me.lvwGames.Location = New System.Drawing.Point(12, 52)
        Me.lvwGames.MultiSelect = False
        Me.lvwGames.Name = "lvwGames"
        Me.lvwGames.Size = New System.Drawing.Size(584, 212)
        Me.lvwGames.TabIndex = 12
        Me.lvwGames.UseCompatibleStateImageBehavior = False
        Me.lvwGames.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "GameCode"
        Me.ColumnHeader9.Width = 0
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Kickoff"
        Me.ColumnHeader1.Width = 140
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Competition"
        Me.ColumnHeader2.Width = 55
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Home Team"
        Me.ColumnHeader3.Width = 100
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Away Team"
        Me.ColumnHeader4.Width = 100
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Tier"
        Me.ColumnHeader5.Width = 40
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Task"
        Me.ColumnHeader6.Width = 130
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "ModuleID"
        Me.ColumnHeader10.Width = 0
        '
        'SerialNo
        '
        Me.SerialNo.Text = "SerialNo"
        Me.SerialNo.Width = 0
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(548, 10)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 265
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'picReporterBar
        '
        Me.picReporterBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-3, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(637, 21)
        Me.picReporterBar.TabIndex = 267
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-3, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(637, 15)
        Me.picTopBar.TabIndex = 266
        Me.picTopBar.TabStop = False
        '
        'btnOk
        '
        Me.btnOk.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnOk.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOk.ForeColor = System.Drawing.Color.White
        Me.btnOk.Location = New System.Drawing.Point(467, 10)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 25)
        Me.btnOk.TabIndex = 269
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.sstMain.Location = New System.Drawing.Point(0, 367)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(609, 16)
        Me.sstMain.TabIndex = 270
        Me.sstMain.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Margin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 0)
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblTimeZone
        '
        Me.lblTimeZone.AutoSize = True
        Me.lblTimeZone.BackColor = System.Drawing.Color.Black
        Me.lblTimeZone.ForeColor = System.Drawing.Color.White
        Me.lblTimeZone.Location = New System.Drawing.Point(12, 305)
        Me.lblTimeZone.Name = "lblTimeZone"
        Me.lblTimeZone.Size = New System.Drawing.Size(58, 15)
        Me.lblTimeZone.TabIndex = 272
        Me.lblTimeZone.Text = "Time Zone"
        '
        'radFeedB
        '
        Me.radFeedB.BackColor = System.Drawing.Color.Black
        Me.radFeedB.ForeColor = System.Drawing.Color.LimeGreen
        Me.radFeedB.Location = New System.Drawing.Point(82, 282)
        Me.radFeedB.Name = "radFeedB"
        Me.radFeedB.Size = New System.Drawing.Size(69, 19)
        Me.radFeedB.TabIndex = 274
        Me.radFeedB.TabStop = True
        Me.radFeedB.Text = "Feed B"
        Me.radFeedB.UseVisualStyleBackColor = False
        Me.radFeedB.Visible = False
        '
        'radFeedA
        '
        Me.radFeedA.AutoSize = True
        Me.radFeedA.BackColor = System.Drawing.Color.Black
        Me.radFeedA.ForeColor = System.Drawing.Color.LimeGreen
        Me.radFeedA.Location = New System.Drawing.Point(12, 282)
        Me.radFeedA.Name = "radFeedA"
        Me.radFeedA.Size = New System.Drawing.Size(69, 19)
        Me.radFeedA.TabIndex = 273
        Me.radFeedA.TabStop = True
        Me.radFeedA.Text = "Feed A   "
        Me.radFeedA.UseVisualStyleBackColor = False
        Me.radFeedA.Visible = False
        '
        'picButtonBar
        '
        Me.picButtonBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.Label1)
        Me.picButtonBar.Controls.Add(Me.btnCancel)
        Me.picButtonBar.Controls.Add(Me.btnOk)
        Me.picButtonBar.Location = New System.Drawing.Point(-27, 324)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(642, 71)
        Me.picButtonBar.TabIndex = 275
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(181, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 15)
        Me.Label1.TabIndex = 270
        '
        'radFeedD
        '
        Me.radFeedD.BackColor = System.Drawing.Color.Black
        Me.radFeedD.ForeColor = System.Drawing.Color.LimeGreen
        Me.radFeedD.Location = New System.Drawing.Point(223, 282)
        Me.radFeedD.Name = "radFeedD"
        Me.radFeedD.Size = New System.Drawing.Size(69, 19)
        Me.radFeedD.TabIndex = 280
        Me.radFeedD.TabStop = True
        Me.radFeedD.Text = "Feed D"
        Me.radFeedD.UseVisualStyleBackColor = False
        Me.radFeedD.Visible = False
        '
        'radFeedC
        '
        Me.radFeedC.BackColor = System.Drawing.Color.Black
        Me.radFeedC.ForeColor = System.Drawing.Color.LimeGreen
        Me.radFeedC.Location = New System.Drawing.Point(152, 282)
        Me.radFeedC.Name = "radFeedC"
        Me.radFeedC.Size = New System.Drawing.Size(69, 19)
        Me.radFeedC.TabIndex = 279
        Me.radFeedC.TabStop = True
        Me.radFeedC.Text = "Feed C"
        Me.radFeedC.UseVisualStyleBackColor = False
        Me.radFeedC.Visible = False
        '
        'frmSelectGame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(609, 383)
        Me.Controls.Add(Me.radFeedD)
        Me.Controls.Add(Me.radFeedC)
        Me.Controls.Add(Me.radFeedB)
        Me.Controls.Add(Me.radFeedA)
        Me.Controls.Add(Me.lblTimeZone)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.lvwGames)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmSelectGame"
        Me.Text = "Select Game"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sstMain.ResumeLayout(False)
        Me.sstMain.PerformLayout()
        Me.picButtonBar.ResumeLayout(False)
        Me.picButtonBar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lvwGames As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTimeZone As System.Windows.Forms.Label
    Friend WithEvents radFeedB As System.Windows.Forms.RadioButton
    Friend WithEvents radFeedA As System.Windows.Forms.RadioButton
    Friend WithEvents SerialNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents radFeedD As System.Windows.Forms.RadioButton
    Friend WithEvents radFeedC As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
