﻿Imports System.Windows.Forms.Design
Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text

Public Class FrmActionsAssist

#Region " Member Variables "
    Private ReadOnly _messageDialog As New frmMessageDialog
    Private ReadOnly _objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private _lblDetails As Label
    Private _lstEventDetails As ListBox
    Private ReadOnly _objGeneral As clsGeneral = clsGeneral.GetInstance()
    Private _objActionDetails As ClsActionDetails = ClsActionDetails.GetInstance()
    Private _objClsGetEventData As ClsGetEventData = ClsGetEventData.GetInstance()
    Private _objClsPlayerData As ClsPlayerData = ClsPlayerData.GetInstance()
    Private _objClsPbpTree As ClsPbpTree = ClsPbpTree.GetInstance()
    Private _objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private _playerTable As New List(Of ClsPlayerData.PlayerData)
    Private _objActionsAssist As ClsActionsAssist = ClsActionsAssist.GetInstance()
    Private _autoInsertEvent As New Dictionary(Of Integer, Integer)
    Private _eventHotkeys As New Dictionary(Of Keys, Button)
    Private _objUtility As New clsUtility
    Private _objUtilAudit As New clsAuditLog
    Private _previousPlayerId? As Integer
    Private _previousTeamId? As Integer
    Private _previousRow? As Integer
    Private ReadOnly _eventDependency As New List(Of ClsGetEventData.EventDataDependecy)
    Private _insertMode As Boolean = False
    Private ReadOnly _eventCoordinatesData As New Dictionary(Of Integer, Dictionary(Of Integer, udcSoccerField.enMarkLocation))
    Private ReadOnly _eventCoordinates As New Dictionary(Of Integer, Tuple(Of udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation, Boolean))
    Private ReadOnly _eventCoordinatesDataTeam As New Dictionary(Of Integer, Dictionary(Of String, Tuple(Of Int32, udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation)))
    Private ReadOnly _goalEvents() As Integer = {11, 28, 17}
    Private ReadOnly _paredEvents() As Integer = {71, 72, 73, 74} '50 -50
    Private ReadOnly _eventsWithoutTeam() As Integer = {67, 68, 66}
    Private ReadOnly _eventNonEditable() As Integer = {23, 24, 34, 62, 21, 13, 10, 65, 30, 31, 41, 58, 59} '23-HomeStartingLineups 24 -AwatStartingLineups ,34-GAMESTART,62-TIME,21-STARTPERIOD,13-ENDPERIOD,10-ENDGAME,65-DELAY_OVER, (30,31,41) shootoutEvents
    Private ReadOnly _assisterRepSlNo() As Integer = {2, 5, 6}
    Private ReadOnly _touchRepSlNo() As Integer = {3, 4}
    Private _txtAssignedTime As String
    Private ReadOnly _eventQcFilter As New Dictionary(Of Integer, Tuple(Of String, Integer()))
    Private ReadOnly _previousEventCoordinates As New Dictionary(Of Integer, String)
    Private _previousFieldX? As Decimal
    Private _previousFieldY? As Decimal
    Private _deleteKeyPressCnt As Integer
    Private _mnuitem As New ToolStripMenuItem
    Private _blnKey As Boolean = False
    Private strBuilder As New StringBuilder
    Private lsupport As New languagesupport
    Private _previousGridTeamId? As Integer = Nothing 'TOPZ-1301
    Private _goalEventSeqNum As Decimal
    Private _customFormationHome As New Dictionary(Of Integer, String)
    Private _customFormationAway As New Dictionary(Of Integer, String)
    Private _pbpRowSelectedSqNo? As Decimal
#End Region
#Region " Event handlers "

    Private Sub frmActionsAssist_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            SetDefaultValues()
            'Multilingual for The form controls
            If _objGameDetails.languageid <> 1 Then
                lsupport.FormLanguageTranslate(Me)
                'ContextMenuStrip Multilingual
                DeleteActionToolStripItem.Text = lsupport.MultilingualDictionary(DeleteActionToolStripItem.Text)
                InsertToolStripItem.Text = lsupport.MultilingualDictionary(InsertToolStripItem.Text)
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    'Event Button click
    Private Sub Event_Click(sender As Object, e As EventArgs) Handles btnFoul.Click, btnShot.Click, btnThrowIn.Click, btnOffSide.Click, _
                                                                      btnFreeKick.Click, btnGoalKick.Click, btnCorner.Click, _
                                                                      btnSubstitute.Click, btnBooking.Click, btnPass.Click, btnSaves.Click, _
                                                                      btn50BW.Click, btnBlocks.Click, btnGKAction.Click,
                                                                      btnAirWon.Click, btnChance.Click, btnTackle.Click, _
                                                                      btnClearance.Click, btnOOB.Click, btnControl.Click, btnObstacle.Click, _
                                                                      btnUncontrolled.Click

        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            'Property setting
            ResetPanelEventProperties()
            UdcSoccerPlayerPositions1.USPUnhighlightButtons()
            If (dgvPBP.SelectedRows.Count > 0) Then
                Dim buttonEvent As Button = CType(DirectCast(sender, Windows.Forms.Control), Button)
                _objActionDetails.EventId = CInt(buttonEvent.Tag)
                If (Not _goalEvents.Contains(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value) And (_eventsWithoutTeam.Contains(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value) = _eventsWithoutTeam.Contains(_objActionDetails.EventId)) And (Not _paredEvents.Contains(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value)) Or _insertMode) Then
                    buttonEvent.BackColor = Color.Olive
                    lblEvent.Text = buttonEvent.Text
                    LoadEventsbox(_objActionDetails.EventId, _objActionDetails.TeamId)
                    If _previousPlayerId > -1 Then
                        UdcSoccerPlayerPositions1.USPHighlightButton(_previousPlayerId)
                    End If
                    PnlEventPropertiesVisible()
                Else
                    _messageDialog.Show(_objGameDetails.languageid, "Replacing the scoring events/ Win event / Lost event / Obstacle / Out of Bounds / InjuryTime to a Team specific event not available", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    _objActionDetails.EventId = Nothing
                    dgvPBP.ClearSelection()
                    UnHighlightButtonColor(Color.Gold)
                    UdcSoccerPlayerPositions1.USPUnhighlightButtons()
                End If
            End If
            dgvPBP.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    'Event Button click
    Private Sub Event_ClickScoring(sender As Object, e As EventArgs) Handles btnOwnGoal.Click, btnNormalGoal.Click, btnPenalty.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            'Property setting
            ResetPanelEventProperties()
            If (dgvPBP.SelectedRows.Count > 0) Then
                If _goalEvents.Contains(IIf(_objActionDetails.EventId Is Nothing, 0, _objActionDetails.EventId)) Or _insertMode Then
                    Dim buttonEvent As Button = CType(DirectCast(sender, Windows.Forms.Control), Button)
                    _objActionDetails.EventId = CInt(buttonEvent.Tag)
                    If Not GoalEventValidation(_objActionDetails.TeamId) Then
                        Exit Sub
                    End If
                    lblEvent.Text = buttonEvent.Text
                    LoadEventsbox(_objActionDetails.EventId, _objActionDetails.TeamId)
                    PnlEventPropertiesVisible()
                    UdcSoccerGoalFrame1.USGFConfineToGoal = True
                Else
                    _messageDialog.Show(_objGameDetails.languageid, "Replace to scoring Event Not Available", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub dgvPBP_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPBP.CellClick
        Try
            If (e.RowIndex = -1) Then 'To avoid Column header click
                Exit Sub
            End If
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "UNIQUE_ID- " & dgvPBP.Rows(e.RowIndex).Cells("UNIQUE_ID").Value & " SEQUENCE_NUMBER- " & Math.Round(dgvPBP.Rows(e.RowIndex).Cells("SEQUENCE_NUMBER").Value, 3) & " EVENT- " & dgvPBP.Rows(e.RowIndex).Cells("EVENT").Value & " TEAM- " & dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value & " Click Datagrid cell", 1, 0)

            'Property setting
            ResetPanelEventProperties()
            _insertMode = False
            If (_assisterRepSlNo.Contains(_objGameDetails.ReporterRoleSerial)) Then btnReplace.Visible = True
            clsUtility.SetButtonColor(btnInsert, Color.FromArgb(112, 151, 236))
            _objActionDetails.EventId = CInt(dgvPBP.Rows(e.RowIndex).Cells("EVENT_CODE_ID").Value)
            lblEvent.Text = dgvPBP.Rows(e.RowIndex).Cells("Event").Value.ToString()
            lblEvent.Visible = True
            _objActionDetails.TeamId = CInt(IIf(dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value.ToString() <> "", dgvPBP.Rows(e.RowIndex).Cells("TEAM_ID").Value, 0))

            'Unhighlight Event Button
            UnHighlightButtonColor(Color.Gold)

            If (TimeLineValidation()) Then 'Validation to avoid list fill
                ClearSelectedAction()
                Exit Sub
            End If

            '50 - 50 Events time Edit not allowed 71,72,73,74
            If _paredEvents.Contains(_objActionDetails.EventId) Then
                txtTime.Enabled = False
                btnRefreshTime.Enabled = False
                btnIncTime.Enabled = False
                btnDecTime.Enabled = False
            Else
                btnRefreshTime.Enabled = True
                btnIncTime.Enabled = True
                btnDecTime.Enabled = True
                txtTime.Enabled = True
            End If

            'Handel substitute
            _playerTable = _objClsPlayerData.FillPlayer(_objGeneral.GetAllRosters(_objGameDetails.GameCode, _objGameDetails.languageid, CDec(dgvPBP.Rows(e.RowIndex).Cells("SEQUENCE_NUMBER").Value)).Tables(0))

            'Event higlighter
            Dim goldButton = Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).FirstOrDefault(Function(c1) c1.Tag = _objActionDetails.EventId)
            If goldButton IsNot Nothing Then
                clsUtility.HighlightGoldButton(goldButton)
            End If

            'TOPZ-1301----begin
            If UdcSoccerPlayerPositions1.USPFormation = udcSoccerPlayerPositions.Formation.f_Custom Then
                If _previousGridTeamId = _objGameDetails.HomeTeamID Then
                    '_objActionDetails.CustomFormationHome = UdcSoccerPlayerPositions1.USPCustomFormationDetails
                    _customFormationHome.Clear()
                    For Each entry As KeyValuePair(Of Integer, String) In UdcSoccerPlayerPositions1.USPCustomFormationDetails
                        _customFormationHome.Add(entry.Key, entry.Value)
                    Next
                Else
                    '_objActionDetails.CustomFormationAway = UdcSoccerPlayerPositions1.USPCustomFormationDetails
                    _customFormationAway.Clear()
                    For Each entry As KeyValuePair(Of Integer, String) In UdcSoccerPlayerPositions1.USPCustomFormationDetails
                        _customFormationAway.Add(entry.Key, entry.Value)
                    Next
                End If
            End If

            If _previousGridTeamId <> _objActionDetails.TeamId Or _previousGridTeamId Is Nothing Then
                'set custom formation if the user had dragged and dropped the players.
                If _objActionDetails.TeamId = _objGameDetails.HomeTeamID Then
                    ' If _objActionDetails.CustomFormationHome IsNot Nothing Then
                    If _customFormationHome IsNot Nothing And _customFormationHome.Count > 0 Then
                        'UdcSoccerPlayerPositions1.USPCustomFormationDetails = _objActionDetails.CustomFormationHome
                        UdcSoccerPlayerPositions1.USPCustomFormationDetails = _customFormationHome
                    Else
                        If _objGameDetails.HomeFormationId IsNot Nothing Then
                            UdcSoccerPlayerPositions1.USPFormation = CType(_objGameDetails.HomeFormationId, udcSoccerPlayerPositions.Formation)
                        ElseIf _objGameDetails.HomeFormationId Is Nothing Then
                            UdcSoccerPlayerPositions1.USPFormation = UdcSoccerPlayerPositions1.USPDefaultFormation
                        End If
                    End If
                ElseIf _objActionDetails.TeamId = _objGameDetails.AwayTeamID Then
                    'If _objActionDetails.CustomFormationAway IsNot Nothing Then
                    If _customFormationAway IsNot Nothing And _customFormationAway.Count > 0 Then
                        'UdcSoccerPlayerPositions1.USPCustomFormationDetails = _objActionDetails.CustomFormationAway
                        UdcSoccerPlayerPositions1.USPCustomFormationDetails = _customFormationAway
                    Else
                        If _objGameDetails.AwayFormationId IsNot Nothing Then
                            UdcSoccerPlayerPositions1.USPFormation = CType(_objGameDetails.AwayFormationId, udcSoccerPlayerPositions.Formation)
                        ElseIf _objGameDetails.AwayFormationId Is Nothing Then
                            UdcSoccerPlayerPositions1.USPFormation = UdcSoccerPlayerPositions1.USPDefaultFormation
                        End If
                    End If
                End If
            End If
            'TOPZ-1301----end

            clsUtility.ClearPanelButtonText(pnlsubstitute)
            _objGeneral.FillBenchPlayers(_objClsPlayerData.GetBenchPlayers(_playerTable, _objActionDetails.TeamId).ToList(), pnlsubstitute, "btnSub")
            'set the edited record time

            'To handel format 45 + 1:42 in to txtTime
            Dim strformatedTime() As String
            Dim milliSec As Integer
            strformatedTime = dgvPBP.Rows(e.RowIndex).Cells("time").Value.ToString().Split("+")
            milliSec = CInt(dgvPBP.Rows(e.RowIndex).Cells("Game_Time_MS").Value) - (CInt(dgvPBP.Rows(e.RowIndex).Cells("Time_Elapsed").Value) * 1000)

            If strformatedTime.Length > 1 Then
                txtTime.Text = (CInt(strformatedTime(0)) + CInt(strformatedTime(1).Substring(0, strformatedTime(1).IndexOf(":")))).ToString() + strformatedTime(1).Substring(strformatedTime(1).IndexOf(":") + 0, 3) + ":" + milliSec.ToString.PadLeft(3, CChar("0"))
            Else
                txtTime.Text = strformatedTime(0) + ":" + milliSec.ToString.PadLeft(3, CChar("0"))
            End If

            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(8, CChar(" "))

            'to compare if it is a TimeEdit
            _txtAssignedTime = txtTime.Text

            'Load the event List box
            UdcSoccerPlayerPositions1.USPUnhighlightButtons()
            LoadEventsbox(_objActionDetails.EventId, _objActionDetails.TeamId)

            'Display time
            Controls.Add(pnlSave)
            pnlSave.Location = New Point(415, 396)
            'set home/visit team color
            If (_objActionDetails.TeamId = _objGameDetails.HomeTeamID) Then
                SetTeamColor(_objGameDetails.HomeTeamColor)
            Else
                SetTeamColor(_objGameDetails.VisitorTeamColor)
            End If
            _previousGridTeamId = _objActionDetails.TeamId    'TOPZ-1301
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub dgvPBP_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPBP.CellDoubleClick
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "Double Click Datagrid Cell", 1, 0)
            'FPLRS-227 goal missing due to event doubleclick and click on down arrow which highlights the next event in grid 
            If _pbpRowSelectedSqNo <> dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "Double Click Datagrid Cell, RowSelected : " & _pbpRowSelectedSqNo & " RowModified : " & Math.Round(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, 3), 1, 0)
                HighlightPBPSelectedRow()
            End If

            If Not _insertMode And (dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value = 58 Or dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value = 59 Or dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value = 65) Then
                _messageDialog.Show(_objGameDetails.languageid, "Only primary can edit/delete this event", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            PnlEventPropertiesVisible()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    'Event Button click
    Private Sub lstEventProp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstEventProp01.SelectedIndexChanged, lstEventProp02.SelectedIndexChanged, lstEventProp03.SelectedIndexChanged, _
                                                                                            lstEventProp04.SelectedIndexChanged, lstEventProp05.SelectedIndexChanged, lstEventProp06.SelectedIndexChanged, _
                                                                                            lstEventProp07.SelectedIndexChanged, lstEventProp08.SelectedIndexChanged, lstEventProp09.SelectedIndexChanged, _
                                                                                            lstEventProp10.SelectedIndexChanged, lstEventProp11.SelectedIndexChanged
        Try
            Dim resetBox As Boolean
            If _objGameDetails.IsListBoxLoaded <> True Then
                Dim listBox = CType(DirectCast(sender, Windows.Forms.Control), ListBox)
                If CStr(listBox.SelectedValue) <> Nothing Then
                    If listBox.SelectedValue.ToString() <> "STATS.SoccerBL.ClsGetEventData+EventDataValues" Then
                        resetBox = LoadEventsboxDependency(CInt(listBox.Name.Replace("lstEventProp", "")), listBox.SelectedValue, _objActionDetails.EventId)
                    End If
                    If listBox.Tag = "OFFENSIVE_PLAYER_ID" Then
                        UdcSoccerPlayerPositions1.USPHighlightButton(listBox.SelectedValue)
                    End If
                    ''Multiple dependency (Example Foot(Only on Type 'Cross' AND "Body Part-Foot"))
                    Dim eventMultipleDependency = (From dependency In _eventDependency.AsEnumerable() Where dependency.EventId = _objActionDetails.EventId Order By dependency.BoxEnable
                                        Select dependency).ToList()
                    Dim boxEnableNumber As Integer

                    For Each Dependency In eventMultipleDependency
                        Dim listBoxToShow = CType(pnlEventProperties.Controls.Item("lstEventProp" & Format(CInt(Dependency.BoxEnable), "00")), ListBox)
                        Dim hidestatus As Boolean
                        If Dependency.BoxEnable <> boxEnableNumber Then ''reset diffrent box
                            hidestatus = False
                        End If
                        boxEnableNumber = Dependency.BoxEnable
                        Dim lblToHide = CType(pnlEventProperties.Controls.Item(listBoxToShow.Name.Replace("lst", "lbl")), Label)
                        Dim boxDependency1 = CType(pnlEventProperties.Controls.Item("lstEventProp" & Format(CInt(Dependency.BoxDependency1), "00")), ListBox)
                        Dim boxDependency2 = CType(pnlEventProperties.Controls.Item("lstEventProp" & Format(CInt(Dependency.BoxDependency2), "00")), ListBox)
                        If boxDependency1.SelectedValue = Dependency.BoxDependencyData1 AndAlso boxDependency2.SelectedValue = Dependency.BoxDependencyData2 Then
                            hidestatus = True
                        ElseIf Not hidestatus Then
                            hidestatus = False
                        End If
                        listBoxToShow.Visible = hidestatus
                        lblToHide.Visible = hidestatus
                    Next

                    'Collect X/Y Coordinates for 'Obstacle'
                    If (_eventCoordinatesData.ContainsKey(_objActionDetails.EventId)) Then
                        CurrentCoordinatesActions(CInt(listBox.SelectedValue))
                    End If

                    'plot last touch value in field
                    If _previousEventCoordinates.ContainsKey(_objActionDetails.EventId) Then
                        If (listBox.Tag = _previousEventCoordinates.Item(_objActionDetails.EventId)) Then
                            If _previousFieldX.HasValue Then
                                SetFieldPoints(1, _previousFieldX, _previousFieldY)
                                If _objActionDetails.EventId = 9 And (listBox.SelectedValue = 4 Or listBox.SelectedValue = 2 Or listBox.SelectedValue = 14) Then 'TOPZ - 2044
                                    UdcSoccerField1.Enabled = False
                                Else
                                    UdcSoccerField1.Enabled = True
                                End If
                            End If
                        End If
                    End If

                    If (_eventCoordinatesDataTeam.ContainsKey(_objActionDetails.EventId)) Then
                        If (_eventCoordinatesDataTeam.Item(_objActionDetails.EventId).ContainsKey(listBox.Tag)) Then
                            UdcSoccerField1.USFClearMarks()
                            UdcSoccerGoalFrame1.USGFClearMarks()
                            _previousFieldX = Nothing
                            _previousFieldY = Nothing

                            If _eventCoordinatesDataTeam.Item(_objActionDetails.EventId).Item(listBox.Tag).Item1 = CInt(listBox.SelectedValue) And _objActionDetails.EventId = 9 Then

                                Dim enumValue As udcSoccerField.enMarkLocation
                                Dim currentCoordinates As Point
                                If (UdcSoccerField1.USFHomeTeamOnLeft) Then
                                    enumValue = _eventCoordinatesDataTeam.Item(_objActionDetails.EventId).Item(listBox.Tag).Item2
                                Else
                                    enumValue = _eventCoordinatesDataTeam.Item(_objActionDetails.EventId).Item(listBox.Tag).Item3
                                End If
                                currentCoordinates = UdcSoccerField1.USFSetMark(enumValue, IIf(_objActionDetails.PlayerX.HasValue, _objActionDetails.PlayerX, 0), IIf(_objActionDetails.PlayerY.HasValue, _objActionDetails.PlayerY, 0))
                                _objActionDetails.PlayerX = currentCoordinates.X
                                _objActionDetails.PlayerY = currentCoordinates.Y
                                If enumValue = 6 Or enumValue = 5 Then 'TOPZ - 2044
                                    ''UdcSoccerField1.Enabled = False
                                    _previousFieldX = _objActionDetails.PlayerX
                                    _previousFieldY = _objActionDetails.PlayerY
                                End If

                            ElseIf _objActionDetails.EventId = 5 Then     'TOPZ - 2043
                                If (UdcSoccerField1.USFHomeTeamOnLeft) Then
                                    UdcSoccerField1.USFSetMark(105, IIf(CInt(listBox.SelectedValue) = 1, 0, 70))
                                    _objActionDetails.PlayerX = 105
                                    _objActionDetails.PlayerY = IIf(CInt(listBox.SelectedValue) = 1, 0, 70)
                                Else
                                    UdcSoccerField1.USFSetMark(0, IIf(CInt(listBox.SelectedValue) = 1, 70, 0))
                                    _objActionDetails.PlayerX = 0
                                    _objActionDetails.PlayerY = IIf(CInt(listBox.SelectedValue) = 1, 70, 0)
                                End If

                            End If
                            resetBox = True
                        End If
                    End If



                    'Reset the ListBox
                    If resetBox Then
                        AlignListboxes()
                    End If
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            ResetPanelEventProperties()
            If dgvPBP.SelectedRows.Count > 0 Then
                dgvPBP_CellClick(dgvPBP, New DataGridViewCellEventArgs(0, dgvPBP.SelectedRows(0).Index))
            End If
            _insertMode = False
            If _assisterRepSlNo.Contains(_objGameDetails.ReporterRoleSerial) Then btnReplace.Visible = True
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            InsertPlayByPlayData()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub btnReplace_Click(sender As Object, e As EventArgs) Handles btnReplace.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Trim().Replace(vbNewLine, " "), 1, 0)
            If dgvPBP.SelectedRows.Count > 0 Then
                Dim eventNonEditable() As Integer = {23, 24, 34, 62, 21, 13, 10, 65, 67, 68, 11, 28, 17, 71, 72, 73, 74} '23-HomeStartingLineups 24 -AwatStartingLineups ,34-GAMESTART,62-TIME,21-STARTPERIOD,13-ENDPERIOD,10-ENDGAME,65-DELAY_OVER,67-OOB,68-OBSTACLE
                If (eventNonEditable.Contains(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value)) Then 'Validation to avoid list fill
                    _messageDialog.Show(_objGameDetails.languageid, "Replace Functionality Not Available", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    ClearSelectedAction()
                    Exit Sub
                End If
                _objActionDetails.TeamId = If(dgvPBP.SelectedRows(0).Cells("TEAM_ID").Value = _objGameDetails.HomeTeamID, _objGameDetails.AwayTeamID, _objGameDetails.HomeTeamID)
                LoadEventsbox(_objActionDetails.EventId, _objActionDetails.TeamId)
                PnlEventPropertiesVisible()
            End If
            dgvPBP.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Event list DoubleClick
    Private Sub lstEventProp_DoubleClick(sender As Object, e As EventArgs) Handles lstEventProp01.DoubleClick, lstEventProp02.DoubleClick, lstEventProp03.DoubleClick, _
                                                                                            lstEventProp04.DoubleClick, lstEventProp05.DoubleClick, lstEventProp06.DoubleClick, _
                                                                                            lstEventProp07.DoubleClick, lstEventProp08.DoubleClick, lstEventProp09.DoubleClick, _
                                                                                            lstEventProp10.DoubleClick, lstEventProp11.DoubleClick
        Try
            Dim lstEventDetails = CType(DirectCast(sender, Windows.Forms.Control), ListBox)
            If lstEventDetails.Text <> Nothing Then
                lstEventDetails.SelectedIndex = -1
                If lstEventDetails.Tag = "OFFENSIVE_PLAYER_ID" Then
                    UdcSoccerPlayerPositions1.USPUnhighlightButtons()
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub FrmActionsAssist_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If (_eventHotkeys.ContainsKey(e.KeyCode)) Then
            e.SuppressKeyPress = True
            _eventHotkeys.Item(e.KeyCode).PerformClick()
            _deleteKeyPressCnt = 0
        End If
    End Sub

    Private Sub SaveEvent_MouseRightClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick, pnlEventProperties.MouseClick, pnlSave.MouseClick
        _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MouseClicked, "Save Action Through Mouse Right click", 1, 0)
        _deleteKeyPressCnt = 0
        If e.Button = Windows.Forms.MouseButtons.Right Then
            InsertPlayByPlayData()
        End If
    End Sub

    Private Sub IncDecTime(ByVal sender As Object, ByVal e As EventArgs) Handles btnIncTime.Click, btnDecTime.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Name, 1, 0)
            Dim btnSource As Button
            Dim arrTime As String()
            Dim intMin As Integer
            Dim intSec As Integer
            Dim intMilliSec As Integer
            btnSource = CType(sender, Button)
            If txtTime.Text.Replace(" :", "").Trim = "" Then
                Exit Try
            End If
            'get minutes and seconds
            arrTime = txtTime.Text.Split(CChar(":"))

            If arrTime(0).Trim() <> Nothing Then
                intMin = CInt(arrTime(0))
            End If

            If arrTime(1).Trim() <> Nothing Then
                intSec = CInt(arrTime(1))
            End If

            If arrTime(2).Trim() <> Nothing Then
                intMilliSec = CInt(arrTime(2))
            End If


            'increment/decrement seconds by 1
            Select Case btnSource.Name
                Case "btnIncTime", "btnIncTime1"
                    If intSec = 59 Then
                        intMin = intMin + 1
                        intSec = 0
                    Else
                        intSec = intSec + 1
                    End If
                Case "btnDecTime", "btnDecTime1"
                    If intSec = 0 Then
                        If intMin = 0 Then
                            Exit Try
                        Else
                            intMin = intMin - 1
                            intSec = 59
                        End If
                    Else
                        intSec = intSec - 1
                    End If
            End Select

            'set the incremented/decremented time
            txtTime.Text = (CStr(intMin).PadLeft(2, CChar("0")) & CStr(intSec).PadLeft(2, CChar("0")) & CStr(intMilliSec).PadLeft(3, CChar("0"))).PadLeft(8, CChar(" "))

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub RefreshTime_Click(sender As Object, e As EventArgs) Handles btnRefreshTime.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Name, 1, 0)
            txtTime.Text = frmMain.UdcRunningClock1.URCCurrentTimeInMilliSec
            txtTime.Text = txtTime.Text.Replace(":", "").PadLeft(8, CChar(" "))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Player_Click(sender As Object, e As USPEventArgs) Handles UdcSoccerPlayerPositions1.USPButtonClick
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text.Replace(vbNewLine, " "), 1, 0)
            If _objActionDetails.EventId Is Nothing Then
                UdcSoccerPlayerPositions1.USPUnhighlightButtons()
                _messageDialog.Show(_objGameDetails.languageid, "Select an event first", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If

            Dim playerlistbox As ListBox = pnlEventProperties.Controls.OfType(Of ListBox)().FirstOrDefault(Function(c) c.Tag = "OFFENSIVE_PLAYER_ID")
            If playerlistbox IsNot Nothing Then
                playerlistbox.SelectedValue = e.USPplayerID
            End If
            dgvPBP.Focus()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try

    End Sub
    Private Sub btnFilters_Click(sender As Object, e As EventArgs) Handles btnFilters.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, DirectCast(sender, System.Windows.Forms.Button).Text, 1, 0)
            frmMain.tmrRefresh.Stop()
            Dim dsRefreshData As New DataSet   'Full data Load
            dsRefreshData = _objGeneral.RefreshPZPbpData(_objGameDetails.GameCode, _objGameDetails.FeedNumber, _objGameDetails.languageid, CInt(IIf(_objGameDetails.IsDefaultGameClock, 1, 0)), True, CInt(_objGameDetails.ReporterRoleSerial), 0)
            PbpDataTreeLoad(dsRefreshData.Tables(0), True)
            ClearSelectedAction()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        Finally
            frmMain.tmrRefresh.Start()
        End Try
    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click, InsertToolStripItem.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "Insert", 1, 0)
            If dgvPBP.SelectedRows.Count > 0 Then
                If _assisterRepSlNo.Contains(_objGameDetails.ReporterRoleSerial) Then
                    frmTeamSelection.ShowDialog()
                ElseIf _objGameDetails.ReporterRoleSerial = 3 Then
                    _objActionDetails.TeamId = _objGameDetails.HomeTeamID
                    _objActionDetails.EventId = Nothing
                ElseIf _objGameDetails.ReporterRoleSerial = 4 Then
                    _objActionDetails.TeamId = _objGameDetails.AwayTeamID
                    _objActionDetails.EventId = Nothing
                End If

                If _objActionDetails.EventId Is Nothing Then
                    _insertMode = True
                    btnReplace.Visible = False
                    clsUtility.SetButtonColor(btnInsert, Color.Gold)
                    UnHighlightButtonColor(Color.Gold)
                    UdcSoccerPlayerPositions1.USPUnhighlightButtons()
                    'fill the player position
                    UdcSoccerPlayerPositions1.USPTeamId = _objActionDetails.TeamId
                    UdcSoccerPlayerPositions1.UspPlayerDetailsCollection = _objClsPlayerData.GetStaringPlayers(_playerTable, _objActionDetails.TeamId)
                    'fill sub players
                    clsUtility.ClearPanelButtonText(pnlsubstitute)
                    _objGeneral.FillBenchPlayers(_objClsPlayerData.GetBenchPlayers(_playerTable, _objActionDetails.TeamId).ToList(), pnlsubstitute, "btnSub")
                End If
            End If
            dgvPBP.Focus()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DeleteActionToolStripItem_Click(sender As Object, e As EventArgs) Handles DeleteActionToolStripItem.Click
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.MouseClicked, "Delete", 1, 0)
            'To avoid Column header click
            If (dgvPBP.SelectedRows.Count = 0) Then
                Exit Sub
            End If

            If Not IsDeleteAllowed(dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value, IIf(IsDBNull(dgvPBP.SelectedRows(0).Cells("TEAM").Value), "", dgvPBP.SelectedRows(0).Cells("TEAM").Value), IIf(IsDBNull(dgvPBP.SelectedRows(0).Cells("BY PLAYER").Value), "", dgvPBP.SelectedRows(0).Cells("BY PLAYER").Value)) Then
                Exit Sub
            End If
            dgvPBP.SelectedRows(0).Cells("PROCESSED").Value = "N" 'update the status so that the same row not allowed to be deleted
            _objGeneral.DeletePlaybyPlayData(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, _objGameDetails.ReporterRoleSerial)
            ClearSelectedAction()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub dgvPBP_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvPBP.KeyDown
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "keyDown Datagrid Cell", 1, 0)
            If e.KeyCode = 46 And dgvPBP.SelectedRows.Count > 0 Then
                e.SuppressKeyPress = True
                DeleteActionToolStripItem.PerformClick()
                _deleteKeyPressCnt = _deleteKeyPressCnt + 1
            ElseIf e.KeyCode = 33 Or e.KeyCode = 34 Or e.KeyCode = 9 Then 'page up/down and tab
                e.SuppressKeyPress = True
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub dgvPBP_KeyUp(sender As Object, e As KeyEventArgs) Handles dgvPBP.KeyUp
        Try
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "keyUp Datagrid Cell", 1, 0)
            If (e.KeyCode = 38 Or e.KeyCode = 40 Or e.KeyCode = 13) And dgvPBP.SelectedRows.Count > 0 Then  'Up/Down arrow ,enter key
                dgvPBP_CellClick(dgvPBP, New DataGridViewCellEventArgs(0, dgvPBP.SelectedRows(0).Index))
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'TOPZ-266 Key Moments for New T6 Software
    Private Sub dgvPBP_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvPBP.CellMouseDown
        Try
            If e.Button = MouseButtons.Right AndAlso e.RowIndex > -1 Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.GridRowSelected, "CellMouseDown Datagrid Cell", 1, 0)
                If (dgvPBP.SelectedRows.Count = 0) Then
                    Exit Sub
                End If
                _goalEventSeqNum = 0
                If (TimeLineValidation()) Then 'Validation to avoid list fill
                    ClearSelectedAction()
                    Exit Sub
                End If
                'delete Keymoment option
                If dgvPBP.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow Then
                    Dim dgPbpDataSource As DataTable
                    dgPbpDataSource = dgvPBP.DataSource
                    Dim keyMomentsMinimum As List(Of Decimal) = _
                                                                (From pbp In dgPbpDataSource _
                                                                Where pbp.Field(Of Int16?)("KEY_MOMENT_ID") > 0 _
                                                                Group pbp By key = pbp!KEY_MOMENT_ID Into Group _
                                                                Let minSequence = Group.Min(Function(m) m.Field(Of Decimal)("SEQUENCE_NUMBER"))
                                                                Select minSequence).ToList()
                    Dim keyMomentStartAction = From theRow As DataGridViewRow In dgvPBP.Rows _
                                               Where keyMomentsMinimum.Contains(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value)
                    If keyMomentStartAction.Count > 0 Then
                        _mnuitem.Name = "Delete KeyMoment"
                        _mnuitem.Text = "Delete KeyMoment"
                        If Not cmnuTimeline.Items.ContainsKey("Delete KeyMoment") Then
                            AddHandler (_mnuitem.Click), AddressOf KeyMomentMenuItem_Click
                            cmnuTimeline.Items.Add(_mnuitem)
                            _blnKey = True
                        End If
                    End If
                    Exit Sub
                End If

                'Start Keymoment
                If IsDBNull(dgvPBP.SelectedRows(0).Cells("KEY_MOMENT_ID").Value) And Not IsDBNull(dgvPBP.SelectedRows(0).Cells("TEAM_ID").Value) Then
                    _mnuitem.Name = "Start KeyMoment"
                    _mnuitem.Text = "Start KeyMoment"
                    If Not cmnuTimeline.Items.ContainsKey("Start KeyMoment") Then
                        Dim dgPbpDataSource As DataTable
                        dgPbpDataSource = dgvPBP.DataSource
                        'get goal sequence no
                        Dim goalEvent = (From goalEv In dgPbpDataSource
                                         Where goalEv.Field(Of Decimal?)("SEQUENCE_NUMBER") > dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value AndAlso _
                                         goalEv.Field(Of Byte?)("PERIOD") = dgvPBP.SelectedRows(0).Cells("PERIOD").Value AndAlso _
                                        _goalEvents.Contains(goalEv.Field(Of Int16)("EVENT_CODE_ID")) _
                                         Order By goalEv.Field(Of Decimal?)("SEQUENCE_NUMBER")).FirstOrDefault()
                        If goalEvent IsNot Nothing Then
                            If goalEvent.Item("TEAM_ID") = dgvPBP.SelectedRows(0).Cells("TEAM_ID").Value Then
                                'çheck if any key moments lies inbetween and if any goal event for opp team lies then dont give "Start option"
                                Dim pbpData = From Events In dgPbpDataSource _
                                              Where Events.Field(Of Decimal?)("SEQUENCE_NUMBER") > dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value AndAlso _
                                                    Events.Field(Of Decimal?)("SEQUENCE_NUMBER") < goalEvent.Item("SEQUENCE_NUMBER") AndAlso _
                                                    Events.Field(Of Int16?)("KEY_MOMENT_ID") IsNot Nothing

                                _goalEventSeqNum = goalEvent.Item("SEQUENCE_NUMBER")
                                If goalEvent.Item("PROCESSED") = "N" Then
                                    _messageDialog.Show(_objGameDetails.languageid, "Key moment for this goal is in process - please wait for the changes to take place!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                    Exit Sub
                                End If
                                If pbpData.Count = 0 Then
                                    AddHandler (_mnuitem.Click), AddressOf KeyMomentMenuItem_Click
                                    cmnuTimeline.Items.Add(_mnuitem)
                                    _blnKey = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub KeyMomentMenuItem_Click(sender As Object, e As EventArgs)
        Try
            If _blnKey Then
                _objActionsAssist.InsertDeleteKeyMomentData(dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, _objGameDetails.ReporterRole, sender.name)
                If _goalEventSeqNum <> 0 Then
                    Dim keyMomentsRowIndex = (From theRow As DataGridViewRow In dgvPBP.Rows _
                                             Where theRow.Cells("SEQUENCE_NUMBER").Value >= dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value AndAlso
                                             theRow.Cells("SEQUENCE_NUMBER").Value <= _goalEventSeqNum
                                             Select theRow.Index).ToList()
                    For Each keyMomentIndex In keyMomentsRowIndex
                        dgvPBP.Rows(keyMomentIndex).Cells("PROCESSED").Value = "N"
                    Next

                End If
                If dgvPBP.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow Then
                    Dim keyMomentsRowIndex = (From theRow As DataGridViewRow In dgvPBP.Rows _
                                            Where IIf(IsDBNull(theRow.Cells("KEY_MOMENT_ID").Value), -1, theRow.Cells("KEY_MOMENT_ID").Value) = dgvPBP.SelectedRows(0).Cells("KEY_MOMENT_ID").Value
                                            Select theRow.Index).ToList()
                    For Each keyMomentIndex In keyMomentsRowIndex
                        dgvPBP.Rows(keyMomentIndex).Cells("PROCESSED").Value = "N"
                    Next
                End If
                ClearSelectedAction()
                _messageDialog.Show(_objGameDetails.languageid, "keymoment " & IIf(sender.name = "Start KeyMoment", "added", "deleted") & " Successfully!", Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                _blnKey = False

            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

#End Region

#Region " User Defined Functions"
    Private Sub SetDefaultValues()
        Try

            'Auto Event Dictionary for Win/loss & 50/50
            _autoInsertEvent.Add(71, 72)
            _autoInsertEvent.Add(73, 74)

            'Hotkeys Dictionary
            _eventHotkeys.Add(Keys.Q, btnChance)
            _eventHotkeys.Add(Keys.W, btnClearance)
            _eventHotkeys.Add(Keys.E, btnGKAction)
            _eventHotkeys.Add(Keys.R, btnSaves)
            _eventHotkeys.Add(Keys.T, btnThrowIn)
            _eventHotkeys.Add(Keys.A, btnBlocks)
            _eventHotkeys.Add(Keys.S, btnTackle)
            _eventHotkeys.Add(Keys.D, btnPass)
            _eventHotkeys.Add(Keys.B, btnFreeKick)
            _eventHotkeys.Add(Keys.F, btnFoul)
            _eventHotkeys.Add(Keys.N, btnShot)
            _eventHotkeys.Add(Keys.Z, btnAirWon)
            _eventHotkeys.Add(Keys.X, btn50BW)
            _eventHotkeys.Add(Keys.C, btnControl)
            _eventHotkeys.Add(Keys.V, btnUncontrolled)
            _eventHotkeys.Add(Keys.Y, btnGoalKick)
            _eventHotkeys.Add(Keys.U, btnCorner)
            _eventHotkeys.Add(Keys.O, btnObstacle)
            _eventHotkeys.Add(Keys.Space, btnOOB)
            _eventHotkeys.Add(Keys.Enter, btnSave)
            _eventHotkeys.Add(Keys.I, btnOffSide)

            'free Kick
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 12, 2, 1, 7, 6)) '12	Cross Description, 2	Sub Type, Direct	1, 7 Type, Cross	6
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 12, 2, 2, 7, 6)) '12	Cross Description, 2	Sub Type, Indirect	1, 7 Type, Cross	6
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 9, 2, 1, 7, 4)) '9	Description, 2	Sub Type, Direct	1, 5	Type, 4 Shot On Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 9, 2, 2, 7, 4)) '9	Description, 2	Sub Type, Indirect	1, 5	Type, 4 Shot On Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 9, 2, 1, 7, 5)) '9	Description, 2	Sub Type, Direct	1, 5	Type, 5	Shot Off Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 9, 2, 2, 7, 5)) '9	Description, 2	Sub Type, Direct	1, 5	Type, 5	Shot Off Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 11, 2, 1, 7, 4)) '11	Chance, 2	Sub Type, Direct	1, 5	Type, 4	Shot On Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 11, 2, 1, 7, 5)) '11	Chance, 2	Sub Type, Indirect	2, 5	Type, 5	Shot Off Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 11, 2, 2, 7, 4)) '11	Chance, 2	Sub Type, Indirect	1, 5	Type, 4	Shot On Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 11, 2, 2, 7, 5)) '11	Chance, 2	Sub Type, Indirect	1, 5	Type, 5	Shot Off Target

            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 6, 2, 3, 5, 2)) '6	Shot Description, 2	Sub Type, Penalty Kick	3, 5	Type, 2	Pass
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 6, 2, 3, 5, 4)) '6	Shot Description, 2	Sub Type, Penalty Kick	3, 5	Type, 4 Shot On Target
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(9, 6, 2, 3, 5, 5)) '6	Shot Description, 2	Sub Type, Penalty Kick	3, 5	Type, 5	Shot Off Target

            'Pass
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(50, 5, 2, 3, 3, 1)) '5	Foot, 2	Type, Cross	3, 3  Body Part, 1	Foot

            '50 -50 win
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(71, 6, 2, 2, 4, 3)) '6 Cross Description 2	Type, Pass	3, 4  Pass Type, 4	Cross
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(71, 5, 2, 2, 4, 3)) '5 Foot Description 2	Type, Pass	3, 4  Pass Type, 4	Cross


            '50 -50 Aerial Duel Won
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(73, 6, 2, 2, 4, 3)) '6 Cross Description 2	Type, Pass	3, 4  Pass Type, 4	Cross
            _eventDependency.Add(New ClsGetEventData.EventDataDependecy(73, 5, 2, 2, 4, 3)) '5 Foot Description 2	Type, Pass	3, 4  Pass Type, 4	Cross

            Dim dictionaryDataIdCoordinates As New Dictionary(Of Integer, udcSoccerField.enMarkLocation)
            dictionaryDataIdCoordinates.Add(1, udcSoccerField.enMarkLocation.LeftPost)  'RightPost To player direction it will be Opposite
            dictionaryDataIdCoordinates.Add(2, udcSoccerField.enMarkLocation.RightPost) 'LeftPost
            dictionaryDataIdCoordinates.Add(3, udcSoccerField.enMarkLocation.Crossbar)
            _eventCoordinatesData.Add(68, dictionaryDataIdCoordinates)

            Dim freeKickSubType As New Dictionary(Of String, Tuple(Of Int32, udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation))
            freeKickSubType.Add("FK_TYPE_ID", New Tuple(Of Int32, udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation)(3, udcSoccerField.enMarkLocation.PenaltyMarkRightHalf, udcSoccerField.enMarkLocation.PenaltyMarkLeftHalf)) 'Penalty Kick
            _eventCoordinatesDataTeam.Add(9, freeKickSubType)
            Dim cornerCrossType As New Dictionary(Of String, Tuple(Of Int32, udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation))
            cornerCrossType.Add("CORNER_CROSS_TYPE", New Tuple(Of Int32, udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation)(1, 1, 2))
            _eventCoordinatesDataTeam.Add(5, cornerCrossType)

            '' _eventCoordinates.Add(18, New Tuple(Of udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation, Boolean)(udcSoccerField.enMarkLocation.PenaltyMarkRightHalf, udcSoccerField.enMarkLocation.PenaltyMarkLeftHalf, True)) 'Miss Kick good
            'oppsite side of  the team  (17,18)'right first and  the left
            _eventCoordinates.Add(17, New Tuple(Of udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation, Boolean)(udcSoccerField.enMarkLocation.PenaltyMarkRightHalf, udcSoccerField.enMarkLocation.PenaltyMarkLeftHalf, True)) 'penality kick good
            _eventCoordinates.Add(47, New Tuple(Of udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation, Boolean)(udcSoccerField.enMarkLocation.Sideline, udcSoccerField.enMarkLocation.Sideline, True)) 'Throw Ins
            _eventCoordinates.Add(67, New Tuple(Of udcSoccerField.enMarkLocation, udcSoccerField.enMarkLocation, Boolean)(udcSoccerField.enMarkLocation.SidelineAndGoalline, udcSoccerField.enMarkLocation.SidelineAndGoalline, False)) ' Out of Bounds

            _eventQcFilter.Add(9, New Tuple(Of String, Integer())("FK_RESULT_ID", {4, 5}))
            _eventQcFilter.Add(73, New Tuple(Of String, Integer())("INTERCEPTION_TYPE_ID", {5, 6}))
            _eventQcFilter.Add(71, New Tuple(Of String, Integer())("INTERCEPTION_TYPE_ID", {5, 6}))
            _previousEventCoordinates.Add(19, "SHOT_RESULT")
            _previousEventCoordinates.Add(9, "FK_RESULT_ID")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub ResetPanelEventProperties()
        Try
            pnlEventProperties.Hide()
            For Each listControl As Control In pnlEventProperties.Controls
                listControl.Hide()
                listControl.Tag = Nothing
            Next
            btnViewGoal.Enabled = False
            UdcSoccerGoalFrame1.Enabled = False
            UdcSoccerField1.Enabled = False
            pnlEventProperties.SendToBack()
            UdcSoccerGoalFrame1.USGFClearMarks()
            UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Mark1Only
            UdcSoccerGoalFrame1.USGFClearMarks()
            UdcSoccerField1.USFClearMarks()
            pnlGoalframe.Visible = True
            pnlField.Visible = True
            pnlSave.Visible = True
            UnHighlightButtonColor(Color.Olive)
            Controls.Add(pnlSave)
            pnlSave.Location = New Point(426, 419)
            UdcSoccerGoalFrame1.USGFConfineToGoal = False
            _objActionDetails.PlayerX = Nothing
            _objActionDetails.PlayerY = Nothing
            _objActionDetails.KeeperZoneX = Nothing
            _objActionDetails.KepperZoneY = Nothing
            _objActionDetails.HandY = Nothing
            _objActionDetails.HandZ = Nothing
            _objActionDetails.GoalZoneId = Nothing
            _objActionDetails.KepperZoneId = Nothing
            cmnuTimeline.Items.RemoveByKey("Start KeyMoment")
            cmnuTimeline.Items.RemoveByKey("Delete KeyMoment")
            lblInjuryTime.Visible = False
            txtInjTime.Visible = False
            picFieldBlinkGreen.Visible = False
            picFieldGreen.Visible = False
            picFieldBlinkOrange.Visible = False
            picFieldOrange.Visible = False
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub ClearSelectedAction()
        Try
            _objActionDetails.EventId = Nothing
            _objActionDetails.TeamId = Nothing
            UdcSoccerPlayerPositions1.USPUnhighlightButtons()
            UnHighlightButtonColor(Color.Gold)
            dgvPBP.Focus()
            dgvPBP.ClearSelection()
            txtTime.Clear()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Public Sub SetFieldPoints(ByVal mark As Integer, ByVal fieldX? As Decimal, ByVal fieldY? As Decimal)
        If fieldX IsNot Nothing And fieldY IsNot Nothing And mark <= CInt(UdcSoccerField1.USFMaximumMarksAllowed) Then
            UdcSoccerField1.USFNextMark = mark
            UdcSoccerField1.USFSetMark(fieldX, fieldY)
            If mark = 1 Then
                _objActionDetails.PlayerX = fieldX
                _objActionDetails.PlayerY = fieldY
            ElseIf mark = 2 Then
                _objActionDetails.KeeperZoneX = fieldX
                _objActionDetails.KepperZoneY = fieldY
            End If
        End If
    End Sub
    Public Sub SetGoalFramePoints(ByVal fieldX? As Decimal, ByVal fieldY? As Decimal)
        If fieldX IsNot Nothing And fieldY IsNot Nothing Then
            Dim goalFramePoints As PointF
            goalFramePoints = GetUnmappedCoordinateValues(fieldX, fieldY, Not (UdcSoccerField1.USFHomeTeamOnLeft))
            UdcSoccerGoalFrame1.USGFSetMark(goalFramePoints.X, goalFramePoints.Y)
        End If
    End Sub

    Private Sub AlignListboxes()
        Try
            Dim lst As ListBox
            Dim lbl As Label
            Dim intNextLeft As Integer = 0
            Dim intNextTop As Integer = 0
            Const horizMargin As Integer = 5
            Const vertMargin As Integer = 5
            Const horizGap As Integer = 5
            Const vertGap As Integer = 5
            Const lblBoxGap As Integer = 1

            For i As Integer = 1 To 15   'increase max value of i to match the listbox count
                lst = CType(pnlEventProperties.Controls("lstEventProp" & i.ToString.PadLeft(2, "0"c)), ListBox)
                'set the width for listbox
                lst.Width = IIf(lst.PreferredSize.Width < 135, 135, lst.PreferredSize.Width)
                lbl = CType(pnlEventProperties.Controls("lblEventProp" & i.ToString.PadLeft(2, "0"c)), Label)
                If lst.Visible = True Then
                    If intNextLeft = 0 Then
                        lbl.Top = vertMargin
                        lst.Top = lbl.Top + lbl.Height + lblBoxGap
                        lbl.Left = horizMargin
                        lst.Left = horizMargin
                        intNextLeft = lst.Left + lst.Width + horizGap
                        intNextTop = lbl.Top
                    Else
                        If intNextLeft + lst.Width + horizMargin > pnlEventProperties.Width Then
                            'if control exceeds panel width
                            lbl.Top = intNextTop + lbl.Height + lblBoxGap + lst.Height + vertGap
                            lst.Top = lbl.Top + lbl.Height + lblBoxGap
                            lbl.Left = horizMargin
                            lst.Left = horizMargin
                            intNextLeft = lst.Left + lst.Width + horizGap
                            intNextTop = lbl.Top
                        Else
                            lbl.Top = intNextTop
                            lst.Top = lbl.Top + lbl.Height + lblBoxGap
                            lbl.Left = intNextLeft
                            lst.Left = intNextLeft
                            intNextLeft = lst.Left + lst.Width + horizGap
                        End If
                    End If
                End If
            Next

            'position the save panel
            pnlSave.Left = 425
            If intNextLeft >= 425 Then
                pnlSave.Top = intNextTop + lblEventProp01.Height + lblBoxGap + lstEventProp01.Height + vertGap
            Else
                pnlSave.Top = intNextTop + lstEventProp01.Height - pnlSave.Height
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub PbpDataTreeLoad(ByVal pbpdata As DataTable, Optional fullLoad As Boolean = False)
        Try

            If chkInc.Checked Or chkHome.Checked Or chkAway.Checked Or chkOwnAction.Checked Or chkQC.Checked Or chk1sthalf.Checked Or chk2ndhalf.Checked Then
                Dim eventIdQc() As Nullable(Of Int16) = {7, 11, 17, 28, 19, 75, 78, 75, 16, 8, 2, 22, 18, 5, 30} 'QC Actions
                Dim eventIds() As Nullable(Of Int16) = {0}

                Dim teamFilter As Boolean
                Dim teamId? As Integer

                If chkHome.Checked And chkAway.Checked Then
                    teamFilter = False
                ElseIf (chkHome.Checked Or chkAway.Checked Or chkOwnAction.Checked) Then
                    teamId = GetTeamFilter(False)
                    teamFilter = True
                End If

                If Not chkOwnAction.Checked Then
                    eventIds = {23, 24, 34, 13, 21, 10}
                End If

                'Filter done on LINQ
                Dim pbpDataRows = (
                    From pbp In pbpdata.AsEnumerable().OfType(Of DataRow)().Where _
                              (Function(row) _
                                              ((row.Field(Of Object)("TEAM_ID") = teamId) Or teamFilter = False) _
                                              AndAlso ((eventIdQc.Contains(row.Field(Of Int16?)("event_code_id")) Or (chkQC.Checked And GetQcFilterRow(row)) Or chkQC.Checked = False)) _
                                              AndAlso (Not chk1sthalf.Checked Or row.Field(Of Byte?)("Period") <= 1) _
                                              AndAlso (Not chk2ndhalf.Checked Or row.Field(Of Byte?)("Period") > 1) _
                                           Or (chkOwnAction.Checked AndAlso eventIds.Contains(row.Field(Of Int16?)("event_code_id"))) _
                                           Or (chk1sthalf.Checked AndAlso chk2ndhalf.Checked) _
                                           Or ((row.Field(Of Int32?)("EDIT_UID") > 0 Or row.Field(Of Decimal)("SEQUENCE_NUMBER") = -1) AndAlso Not fullLoad)
                              ))
                'PBP tree Loading
                If pbpDataRows.Any() Then
                    ''AndAlso ((row.Field(Of String)("Type") Is Nothing Or row.Field(Of String)("By Player") Is Nothing Or row.Field(Of String)("Sub Type") Is Nothing Or row.Field(Of String)("Sub Sub Type") Is Nothing) Or chkInc.Checked = False) _
                    If chkInc.Checked And fullLoad Then
                        _objClsPbpTree.PbpTreeLoad(GetIncomplete(pbpDataRows.CopyToDataTable()), dgvPBP, fullLoad)
                    Else
                        _objClsPbpTree.PbpTreeLoad(pbpDataRows.CopyToDataTable(), dgvPBP, fullLoad)
                    End If
                ElseIf fullLoad Then
                    _objClsPbpTree.PbpTreeLoad(pbpdata.Clone(), dgvPBP, True) 'Clear the Grid When no data exist
                End If
            Else 'To avoid LINQ
                _objClsPbpTree.PbpTreeLoad(pbpdata, dgvPBP, fullLoad)
            End If

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    'Filter Inc
    Public Function GetIncomplete(ByVal pbpData As DataTable) As DataTable
        Dim pbpDataRows = (
          From pbp In pbpData.AsEnumerable().OfType(Of DataRow)().Where _
                    (Function(row) GetIncompleteRow(row)))
        If pbpDataRows.Any() Then
            Return pbpDataRows.CopyToDataTable()
        Else
            Return pbpData.Clone()
        End If
    End Function
    Public Function GetIncompleteRow(ByVal pbpDataRow As DataRow) As Boolean

        Dim incompleteStatus As Boolean
        Dim dependencyList As New List(Of String)()
        dependencyList.Add("ASSISTING_PLAYER_ID")
        dependencyList.Add("SECOND_ASSISTER_ID")
        dependencyList.Add("Field Orange")
        Try
            For Each rulesListBox In _objClsGetEventData.GetEventPbpColumns(_objGameDetails.RulesData.Tables(0), pbpDataRow.Field(Of Int16)("EVENT_CODE_ID"))
                Dim eventDependency = _objClsGetEventData.GetEventDependencyInc(rulesListBox.DataId(), IIf(IsDBNull(pbpDataRow.Field(Of Object)(rulesListBox.DataDesc())), -1, pbpDataRow.Field(Of Object)(rulesListBox.DataDesc())), _objGameDetails.RulesData.Tables(0), _objGameDetails.RulesData.Tables(4), _objGameDetails.LeagueID, _objGameDetails.CoverageLevel, pbpDataRow.Field(Of Int16)("EVENT_CODE_ID"))
                For Each rulesdependencyList In eventDependency
                    dependencyList.Add(rulesdependencyList.PbpColumnName)
                Next
                If (pbpDataRow.Field(Of Object)(rulesListBox.DataDesc()) Is Nothing) And Not dependencyList.Contains(rulesListBox.DataDesc()) Then
                    incompleteStatus = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
        Return incompleteStatus
    End Function
    Public Function GetQcFilterRow(ByVal pbpDataRow As DataRow) As Boolean
        Dim qcFilterRowStatus As Boolean
        Try
            If (_eventQcFilter.ContainsKey(pbpDataRow.Field(Of Int16)("EVENT_CODE_ID"))) Then
                If _eventQcFilter.Item(pbpDataRow.Field(Of Int16)("EVENT_CODE_ID")).Item2.Contains(pbpDataRow.Field(Of Object)(_eventQcFilter.Item(pbpDataRow.Field(Of Int16)("EVENT_CODE_ID")).Item1)) Then
                    qcFilterRowStatus = True
                End If
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
        Return qcFilterRowStatus
    End Function

    Public Function GetTeamFilter(Optional ByVal checkboxOption As Boolean = False) As Integer
        Dim teamFilter As Integer = 0
        Try
            If _objGameDetails.ReporterRoleSerial = 3 And (chkOwnAction.Checked Or checkboxOption) Or chkHome.Checked Then  'chkOwnAction only when the Display own Action is checked
                teamFilter = _objGameDetails.HomeTeamID
            ElseIf _objGameDetails.ReporterRoleSerial = 4 And (chkOwnAction.Checked Or checkboxOption) Or chkAway.Checked Then
                teamFilter = _objGameDetails.AwayTeamID
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
        Return teamFilter
    End Function

    'KeyMoment Highlight
    Public Function HighlightKeyMomentDataInGrid()
        Try
            'highlight keymoment event
            Dim keyMomentsRowIndex = (From theRow As DataGridViewRow In dgvPBP.Rows _
                              Where Not IsDBNull(theRow.Cells("KEY_MOMENT_ID").Value)
                              Select theRow.Index).ToList()
            For Each keyMomentIndex In keyMomentsRowIndex
                dgvPBP.Rows(keyMomentIndex).DefaultCellStyle.BackColor = Color.Yellow
            Next

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Function
    Public Sub LoadEventsbox(ByVal eventId As Integer, ByVal teamId? As Integer)
        Try
            pnlEventProperties.Visible = False 'default false
            _objGameDetails.IsListBoxLoaded = True
            'set arrow in field
            Dim periodSelected As Integer = dgvPBP.SelectedRows(0).Cells("PERIOD").Value

            If (_objActionDetails.TeamId = _objGameDetails.HomeTeamID And (periodSelected = 1 Or periodSelected = 3)) Or (_objActionDetails.TeamId = _objGameDetails.AwayTeamID And (periodSelected = 2 Or periodSelected = 4)) Then
                UdcSoccerField1.USFHomeTeamOnLeft = IIf(periodSelected > 2, _objGameDetails.IsETHomeTeamOnLeft, _objGameDetails.IsHomeTeamOnLeft)
            Else
                UdcSoccerField1.USFHomeTeamOnLeft = IIf(periodSelected > 2, IIf(_objGameDetails.IsETHomeTeamOnLeft, False, True), IIf(_objGameDetails.IsHomeTeamOnLeft, False, True))
            End If

            If _objActionDetails.EventId = 28 Then ''own Goal opposite Direction
                UdcSoccerField1.USFHomeTeamOnLeft = Not (UdcSoccerField1.USFHomeTeamOnLeft)
            End If
            'Get Event data to fill the box
            Dim eventData = _objClsGetEventData.GetEventData(_objGameDetails.RulesData.Tables(0), _objGameDetails.RulesData.Tables(1), _objGameDetails.LeagueID, _objGameDetails.CoverageLevel, eventId)
            For Each rulesListBox In eventData
                Dim defaultSelection As Integer = IIf(rulesListBox.DefaultSelection.HasValue, rulesListBox.DefaultSelection, -1)
                Dim dataDesc As String = rulesListBox.AttributeDesc
                Dim userControls() As String = {"Goal Zone", "Keeper Zone", "Field Green", "Field Orange"}
                If (userControls.Contains(dataDesc)) Then 'User Control Enable
                    UserControlsLoad(dataDesc, True)
                ElseIf dataDesc = "INJURY_TIME" Then
                    lblInjuryTime.Visible = True
                    txtInjTime.Visible = True
                    If dgvPBP.SelectedRows(0).Cells("INJURY_TIME").Value IsNot DBNull.Value Then
                        txtInjTime.Text = CStr(dgvPBP.SelectedRows(0).Cells("INJURY_TIME").Value)
                        txtInjTime.Text = txtInjTime.Text.PadLeft(3, CChar(" "))
                    End If
                    txtInjTime.Focus()
                ElseIf dataDesc = "Manager" Or dataDesc = "Comments" Then
                    txtTime.Focus()
                Else
                    _lblDetails = CType(pnlEventProperties.Controls.Item("lblEventProp" & Format(rulesListBox.AttributeId, "00")), Label)
                    _lblDetails.Text = rulesListBox.AttributeDesc
                    _lblDetails.Visible = True
                    _lstEventDetails = CType(pnlEventProperties.Controls.Item("lstEventProp" & Format(rulesListBox.AttributeId, "00")), ListBox)
                    If Not _insertMode Then
                        Dim gridListValue = (From d In dgvPBP.SelectedRows.OfType(Of DataGridViewRow)()
                                                                       Select d.Cells(rulesListBox.PbpColumnName).Value).ToList()
                        If (gridListValue.First()) IsNot DBNull.Value Then
                            defaultSelection = CInt(gridListValue.First())
                        End If
                    End If
                    'Check Player to fill or event data(sub events) to fill in box
                    If rulesListBox.EventDataValue.FirstOrDefault().DataId < -1 Then ''player fill
                        Dim playerfill() As String = {"FIELD OFF", "BENCH ALL", "ALL", "FIELD ALL", "BENCH OFF", "FIELD DEF", "ALL OFF", "ALL DEF", "Field", "ALL REDCARD"}
                        If (playerfill.Contains(rulesListBox.EventDataValue.FirstOrDefault().DataDesc.ToString())) Then 'player fill
                            Dim eventPlayerData = _objClsPlayerData.GetEventPlayerData(_playerTable.ToList(), If((teamId = _objGameDetails.HomeTeamID), 1, 2), rulesListBox.EventDataValue.FirstOrDefault().DataDesc)
                            'For Automatically Fill in Player
                            If defaultSelection = -1 And _previousTeamId = teamId And _previousRow + 1 = dgvPBP.SelectedRows(0).Index And Not _insertMode Then
                                defaultSelection = _previousPlayerId
                            ElseIf (dataDesc = "By Player" Or dataDesc = "Player") Then
                                _previousPlayerId = -1
                            End If

                            clsUtility.LoadControl(_lstEventDetails, eventPlayerData.ConvertAll(Function(x) DirectCast(x, Object)), "Player", "PlayerId", rulesListBox.PbpColumnName, defaultSelection)
                            If defaultSelection = 0 And (dataDesc = "By Player" Or dataDesc = "Player") Then
                                _previousPlayerId = If(_lstEventDetails.SelectedValue, DBNull.Value)
                            End If
                        End If
                    Else
                        clsUtility.LoadControl(_lstEventDetails, rulesListBox.EventDataValue.ConvertAll(Function(x) DirectCast(x, Object)), "DataDesc", "DataId", rulesListBox.PbpColumnName, defaultSelection) 'Default selection done for insert mode too  IIf(_insertMode, -1, defaultSelection))
                    End If
                End If
            Next

            _objGameDetails.IsListBoxLoaded = False

            'replace with own goal - don't display player
            If eventId = 28 Then
                Dim playerTable As New List(Of ClsPlayerData.PlayerData)
                UdcSoccerPlayerPositions1.UspPlayerDetailsCollection = playerTable
                UdcSoccerPlayerPositions1.USPUnhighlightButtons()
            End If

            Dim lstEvent As ListBox  'To Fire the Selected List box Manually ( This make  the Depedency to work when all the Box are Loaded)
            Dim eventDependency = _objClsGetEventData.GetEventDependency(_objGameDetails.RulesData.Tables(0), _objGameDetails.RulesData.Tables(4), _objGameDetails.LeagueID, _objGameDetails.CoverageLevel, eventId)
            For Each rulesListBox In eventDependency 'Hide list based on selection value
                lstEvent = CType(pnlEventProperties.Controls.Item("lstEventProp" & Format(CInt(rulesListBox), "00")), ListBox)
                lstEventProp_SelectedIndexChanged(lstEvent, New EventArgs())
            Next

            UdcSoccerField1.USFClearMarks()
            If Not _insertMode Then
                If (_previousRow + 1 <> dgvPBP.SelectedRows(0).Index) Then
                    _previousPlayerId = Nothing
                    _previousTeamId = Nothing
                    _previousFieldX = Nothing
                    _previousFieldY = Nothing
                End If

                If dgvPBP.SelectedRows(0).Cells("PLAYER_X").Value IsNot DBNull.Value Then
                    SetFieldPoints(1, CDec(dgvPBP.SelectedRows(0).Cells("PLAYER_X").Value), CDec(dgvPBP.SelectedRows(0).Cells("PLAYER_Y").Value))
                ElseIf _previousFieldX.HasValue And UdcSoccerField1.Enabled And _previousRow.HasValue Then
                    SetFieldPoints(1, _previousFieldX, _previousFieldY)
                ElseIf _previousFieldX.HasValue And Not _previousRow.HasValue And eventId = 9 Then 'freekich followed by foul
                    SetFieldPoints(1, _previousFieldX, _previousFieldY)
                End If

                If dgvPBP.SelectedRows(0).Cells("KEEPER_X").Value IsNot DBNull.Value And eventId <> 17 Then 'TOSOCRS -437
                    SetFieldPoints(2, CDec(dgvPBP.SelectedRows(0).Cells("KEEPER_X").Value), CDec(dgvPBP.SelectedRows(0).Cells("KEEPER_Y").Value))
                End If

                UdcSoccerGoalFrame1.USGFClearMarks()

                If dgvPBP.SelectedRows(0).Cells("BALL_Y").Value IsNot DBNull.Value Then
                    SetGoalFramePoints(CDec(dgvPBP.SelectedRows(0).Cells("BALL_Y").Value), CDec(dgvPBP.SelectedRows(0).Cells("BALL_Z").Value))
                End If
                If dgvPBP.SelectedRows(0).Cells("HAND_Y").Value IsNot DBNull.Value Then
                    SetGoalFramePoints(CDec(dgvPBP.SelectedRows(0).Cells("HAND_Y").Value), CDec(dgvPBP.SelectedRows(0).Cells("HAND_Z").Value))
                End If
                _objActionDetails.HandY = If(dgvPBP.SelectedRows(0).Cells("HAND_Y").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("HAND_Y").Value)
                _objActionDetails.HandZ = If(dgvPBP.SelectedRows(0).Cells("HAND_Z").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("HAND_Z").Value)
                _objActionDetails.BallY = If(dgvPBP.SelectedRows(0).Cells("BALL_Y").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("BALL_Y").Value)
                _objActionDetails.BallZ = If(dgvPBP.SelectedRows(0).Cells("BALL_Z").Value Is DBNull.Value, Nothing, dgvPBP.SelectedRows(0).Cells("BALL_Z").Value)

                If Not dgvPBP.SelectedRows(0).Cells("KEEPERZONE_ID").Value Is DBNull.Value Then
                    _objActionDetails.KepperZoneId = CInt(dgvPBP.SelectedRows(0).Cells("KEEPERZONE_ID").Value)
                End If
                If Not dgvPBP.SelectedRows(0).Cells("GOALZONE_ID").Value Is DBNull.Value Then
                    _objActionDetails.GoalZoneId = CInt(dgvPBP.SelectedRows(0).Cells("GOALZONE_ID").Value)
                End If
            End If
            'Player fill in user control
            Dim disablefillPlayerEvents() As Integer = {28, 22, 48, 29, 4} 'except own goal, Substitution 22,Manager Expulsion,Goalie Change,Comments
            If ((Not disablefillPlayerEvents.Contains(_objActionDetails.EventId)) And _objActionDetails.TeamId <> 0) Then
                UdcSoccerPlayerPositions1.USPTeamId = _objActionDetails.TeamId
                UdcSoccerPlayerPositions1.UspPlayerDetailsCollection = _objClsPlayerData.GetStaringPlayers(_playerTable, _objActionDetails.TeamId)
                UdcSoccerPlayerPositions1.Enabled = True
            Else
                Dim playerTable As New List(Of ClsPlayerData.PlayerData)
                UdcSoccerPlayerPositions1.UspPlayerDetailsCollection = playerTable
                UdcSoccerPlayerPositions1.USPUnhighlightButtons()
                UdcSoccerPlayerPositions1.Enabled = False
            End If

            If dgvPBP.SelectedRows(0).Cells("OFFENSIVE_PLAYER_ID").Value IsNot DBNull.Value And Not _insertMode Then
                UdcSoccerPlayerPositions1.USPHighlightButton(dgvPBP.SelectedRows(0).Cells("OFFENSIVE_PLAYER_ID").Value)
            ElseIf _previousPlayerId > -1 Then
                UdcSoccerPlayerPositions1.USPHighlightButton(_previousPlayerId)
            End If

            UdcSoccerGoalFrame1.USGFConfineToGoal = False
            UdcSoccerField1.USFConfineToLeftPenaltyBox = False
            UdcSoccerField1.USFConfineToRightPenaltyBox = False

            If _goalEvents.Contains(_objActionDetails.EventId) Then
                UdcSoccerGoalFrame1.USGFConfineToGoal = True
            End If

            If _objActionDetails.EventId = 46 And UdcSoccerField1.USFHomeTeamOnLeft Then 'Goal Kicks
                UdcSoccerField1.USFConfineToLeftPenaltyBox = True
            ElseIf _objActionDetails.EventId = 46 Then 'Goal Kicks
                UdcSoccerField1.USFConfineToRightPenaltyBox = True
            End If
            EventCoordinates()
            If CInt(_objGameDetails.languageid) <> 1 Then   'Multilingual for panel
                lsupport.ControlGroup(pnlEventProperties)
            End If

            If _objActionDetails.PlayerX.HasValue Then
                picFieldBlinkGreen.Visible = False
            End If

            If _objActionDetails.KeeperZoneX.HasValue Then
                picFieldBlinkOrange.Visible = False
            End If

            If _objActionDetails.BallY Then
                picGFGreenBlink.Visible = False
            End If

            If _objActionDetails.HandY Then
                picGFOrangeBlink.Visible = False
            End If
            _pbpRowSelectedSqNo = dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Public Function LoadEventsboxDependency(ByVal listBoxNo As Integer, ByVal listBoxSelectedValue As Integer, ByVal eventId As Integer) As Boolean
        Try
            ''List box Hide and reset/List box Player diplay change and reset
            ''-100 list box Hide, other negative values player display change, + values  data change
            Dim eventDependency = _objClsGetEventData.GetEventDependencyData(listBoxNo, listBoxSelectedValue, _objGameDetails.RulesData.Tables(0), _objGameDetails.RulesData.Tables(4), _objGameDetails.LeagueID, _objGameDetails.CoverageLevel, eventId)
            Dim lstEvent As ListBox
            Dim listBoxNumberField As New ArrayList
            Dim listBoxNumber As New ArrayList
            Dim lblEvent As Label
            Dim resetBox As Boolean
            Dim checkGoalFrame = (From dependecy In eventDependency Where dependecy.AttributeDesc = "Goal Zone" Or dependecy.AttributeDesc = "Keeper Zone").Count()
            If checkGoalFrame > 0 Then
                UdcSoccerGoalFrame1.USGFClearMarks()
                btnViewGoal.Enabled = True
                UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Mark1Only
            End If
            Dim checkForField = (From dependecy In eventDependency Where dependecy.AttributeDesc = "Field Green" Or dependecy.AttributeDesc = "Field Orange").Count()
            If checkForField > 0 Then
                UdcSoccerField1.USFClearMarks()
                UdcSoccerField1.USFMaximumMarksAllowed = 1
            End If

            Dim userControls() As String = {"Goal Zone", "Keeper Zone", "Field Green", "Field Orange"}
            For Each rulesListBox In eventDependency 'Hide list based on selection value
                lstEvent = CType(pnlEventProperties.Controls.Item("lstEventProp" & Format(CInt(rulesListBox.AttributeEntityDetailId), "00")), ListBox)
                lblEvent = CType(pnlEventProperties.Controls.Item("lblEventProp" & Format(CInt(rulesListBox.AttributeEntityDetailId), "00")), Label)
                If userControls.Contains(rulesListBox.AttributeDesc) Then 'User Control Enable
                    pnlField.Visible = True
                    If (From dependecy In eventDependency Where dependecy.AttributeEntityId = listBoxNo AndAlso dependecy.AttributeEntityDetailId = rulesListBox.AttributeEntityDetailId AndAlso dependecy.AttributeEntityDataId = listBoxSelectedValue AndAlso dependecy.AttributeData = "FALSE").Any() Then
                        UserControlsLoad(rulesListBox.AttributeDesc, False)
                        listBoxNumberField.Add(rulesListBox.AttributeEntityDetailId)
                    ElseIf CInt(rulesListBox.AttributeEntityDataId) <> listBoxSelectedValue And rulesListBox.AttributeData = "FALSE" And listBoxNumberField.Contains(rulesListBox.AttributeEntityDetailId) = False Then
                        UserControlsLoad(rulesListBox.AttributeDesc, True)
                    End If
                ElseIf (From dependecy In eventDependency Where dependecy.AttributeEntityId = listBoxNo AndAlso dependecy.AttributeEntityDetailId = rulesListBox.AttributeEntityDetailId AndAlso dependecy.AttributeEntityDataId = listBoxSelectedValue AndAlso dependecy.AttributeData = "FALSE").Any() Then
                    lstEvent.Visible = False
                    lblEvent.Visible = False
                    listBoxNumber.Add(rulesListBox.AttributeEntityDetailId)
                    resetBox = True
                ElseIf CInt(rulesListBox.AttributeEntityDataId) <> listBoxSelectedValue And rulesListBox.AttributeData = "FALSE" And listBoxNumber.Contains(rulesListBox.AttributeEntityDetailId) = False Then '-- Reset the box  ''lstEvent1.Name.ToString() <> lstEvent.Name.ToString()
                    lstEvent.Visible = True
                    lblEvent.Visible = True
                    resetBox = True
                End If
            Next
            Return resetBox
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try

    End Function

    Public Sub UserControlsLoad(ByVal dataDesc As String, ByVal enableControl As Boolean)
        If dataDesc = "Goal Zone" Then
            UdcSoccerGoalFrame1.Enabled = enableControl
            If UdcSoccerGoalFrame1.USGFMarksAllowed <> udcSoccerGoalFrame.Marks.Both Then
                UdcSoccerGoalFrame1.USGFClearMarks()
                UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Mark1Only
            End If
            btnViewGoal.Enabled = True
            pnlGoalframe.Visible = True
        ElseIf dataDesc = "Keeper Zone" And enableControl Then
            UdcSoccerGoalFrame1.Enabled = enableControl
            btnViewGoal.Enabled = enableControl
            UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Both
            pnlGoalframe.Visible = True
        ElseIf dataDesc = "Keeper Zone" And Not enableControl Then
            UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Mark1Only
        ElseIf dataDesc = "Field Green" Then
            UdcSoccerField1.Enabled = enableControl
            UdcSoccerField1.USFMaximumMarksAllowed = 1
            UdcSoccerField1.USFEnableOneClickRelocation = True
        ElseIf dataDesc = "Field Orange" And enableControl Then
            UdcSoccerField1.Enabled = enableControl
            UdcSoccerField1.USFMaximumMarksAllowed = 2
            UdcSoccerField1.USFEnableOneClickRelocation = False
        ElseIf dataDesc = "Field Orange" And Not enableControl Then
            UdcSoccerField1.USFMaximumMarksAllowed = 1
            UdcSoccerField1.USFEnableOneClickRelocation = True
        End If

        If UdcSoccerField1.USFMaximumMarksAllowed = 2 Then
            picFieldBlinkGreen.Visible = True
            picFieldGreen.Visible = True
            picFieldBlinkOrange.Visible = True
            picFieldOrange.Visible = True
        ElseIf UdcSoccerField1.USFMaximumMarksAllowed = 1 Then
            picFieldBlinkGreen.Visible = True
            picFieldGreen.Visible = True
            picFieldBlinkOrange.Visible = False
            picFieldOrange.Visible = False
        Else
            picFieldBlinkGreen.Visible = False
            picFieldGreen.Visible = False
            picFieldBlinkOrange.Visible = False
            picFieldOrange.Visible = False
        End If

        If UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Both Then
            picGFGreenBlink.Visible = True
            picGFGreen.Visible = True
            picGFOrangeBlink.Visible = True
            picGFOrange.Visible = True
        ElseIf UdcSoccerGoalFrame1.USGFMarksAllowed = udcSoccerGoalFrame.Marks.Mark1Only Then
            picGFGreenBlink.Visible = True
            picGFGreen.Visible = True
            picGFOrangeBlink.Visible = False
            picGFOrange.Visible = False
        Else
            picGFGreenBlink.Visible = False
            picGFGreen.Visible = False
            picGFOrangeBlink.Visible = False
            picGFOrange.Visible = False
        End If

    End Sub
    Function GoalEventValidation(ByVal teamId As Integer) As Boolean
        Try
            Dim returnStatus As Boolean = True
            If _insertMode Then
                If _messageDialog.Show(_objGameDetails.languageid, "You are crediting a goal for " + IIf(teamId = _objGameDetails.HomeTeamID, _objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev) + ". This will change the score. Make sure Primary reporter does not add any actions until the score updates take place. Please wait till the score reflects on your screen. Are you sure you want to insert this goal?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    clsUtility.SetButtonColor(btnInsert, Color.FromArgb(112, 151, 236))
                    dgvPBP_CellClick(dgvPBP, New DataGridViewCellEventArgs(0, dgvPBP.SelectedRows(0).Index))
                    returnStatus = False
                End If
            Else
                If dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value = 11 Or dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value = 17 Or dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value = 28 Then
                    If _messageDialog.Show(_objGameDetails.languageid, "You are changing the goal type. Are you sure?", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                        dgvPBP_CellClick(dgvPBP, New DataGridViewCellEventArgs(0, dgvPBP.SelectedRows(0).Index))
                        returnStatus = False
                    End If
                End If

            End If
            Return returnStatus
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Save to Live_soc_pbp
    Public Sub InsertPlayByPlayData()
        Try
            'FPLRS-227 goal missing due to event "doubleclick & down arrow" and "TimeEdit & cellMouseClick" that highlights the next event in grid 
            If _insertMode = False And (_pbpRowSelectedSqNo <> dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value) Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "InsertPlayByPlayData Index changed orig : " & _pbpRowSelectedSqNo & " changed to : " & dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, 1, 0)
                HighlightPBPSelectedRow()
            End If

            'Insert validation
            Dim validation As Tuple(Of Boolean, String) = InsertValidation()
            If validation.Item1 Then
                If validation.Item2 IsNot Nothing Then
                    _messageDialog.Show(_objGameDetails.languageid, validation.Item2, Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                Exit Sub
            End If
            'TOSOCRS-425 Warning Message for Shots Beyond Half Line
            Dim _validatePlottingEvents() As Integer = {11, 28, 17, 19}
            If _validatePlottingEvents.Contains(_objActionDetails.EventId) Then
                If (UdcSoccerField1.USFHomeTeamOnLeft And _objActionDetails.PlayerX < 51) Or (Not UdcSoccerField1.USFHomeTeamOnLeft And _objActionDetails.PlayerX > 51) Then
                    If _messageDialog.Show(_objGameDetails.languageid, "The location of the shot you have entered is behind the halfway line. Is this correct? If yes, press Ok. Otherwise, press CANCEL.", "Field Plotting (Green dot)", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Cancel Then
                        Exit Sub
                    End If
                End If
            End If


            If _objActionDetails.EventId = 28 Then 'Own Goal with an assist FPLRS-329
                Dim listBoxPlayer = Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of ListBox)()).FirstOrDefault(Function(c1) c1.Tag = "ASSISTING_PLAYER_ID")
                If (listBoxPlayer.SelectedValue <> Nothing) Then
                    If _messageDialog.Show(_objGameDetails.languageid, "You are saving an Own Goal with an assist. This is VERY RARE. Would you still like to save the Own Goal? If yes, press Ok. Otherwise, press CANCEL.", "Own Goal with an assist", MessageDialogButtons.OKCancel, MessageDialogIcon.Warning) = Windows.Forms.DialogResult.Cancel Then
                        Exit Sub
                    End If
                End If
            End If

            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "InsertPlayByPlayData : Validated successfully.", Nothing, 1, 0)
            Dim drPbpData As DataRow = _objActionDetails.PzPBP.Tables(0).NewRow()
            InsertDefaultValueData(drPbpData, _objActionDetails.EventId)

            If drPbpData("TIME_ELAPSED") < 0 Then
                _messageDialog.Show(_objGameDetails.languageid, "Clock time is not valid. Please check!", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If

            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "InsertDefaultValueData : Default values are assigned.", Nothing, 1, 0)
            InsertListBoxValue(drPbpData, Nothing)
            drPbpData("SEQUENCE_NUMBER") = dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value
            drPbpData("EDIT_UID") = IIf(_insertMode, 0, dgvPBP.SelectedRows(0).Cells("UNIQUE_ID").Value)
            drPbpData("OFFENSE_SCORE") = dgvPBP.SelectedRows(0).Cells("OFFENSE_SCORE").Value
            drPbpData("DEFENSE_SCORE") = dgvPBP.SelectedRows(0).Cells("DEFENSE_SCORE").Value
            drPbpData("TEAM_ID") = If(_objActionDetails.TeamId = 0, DBNull.Value, _objActionDetails.TeamId)

            'Filed Green
            drPbpData("PLAYER_X") = If(_objActionDetails.PlayerX, DBNull.Value)
            drPbpData("PLAYER_Y") = If(_objActionDetails.PlayerY, DBNull.Value)

            'Filed Orange  
            If _objActionDetails.EventId = 17 Then 'TOSOCRS -437
                If (UdcSoccerField1.USFHomeTeamOnLeft) Then
                    drPbpData("KEEPER_X") = 105
                Else
                    drPbpData("KEEPER_X") = 0
                End If
            Else
                drPbpData("KEEPER_X") = If(_objActionDetails.KeeperZoneX, DBNull.Value)
                drPbpData("KEEPER_Y") = If(_objActionDetails.KepperZoneY, DBNull.Value)
            End If

            'Goal Zone green
            drPbpData("BALL_Y") = If(_objActionDetails.BallY, DBNull.Value)
            drPbpData("BALL_Z") = If(_objActionDetails.BallZ, DBNull.Value)
            'Goal Zone orange
            drPbpData("HAND_Y") = If(_objActionDetails.HandY, DBNull.Value)
            drPbpData("HAND_Z") = If(_objActionDetails.HandZ, DBNull.Value)

            drPbpData("KEEPERZONE_ID") = If(_objActionDetails.KepperZoneId, DBNull.Value)
            drPbpData("GOALZONE_ID") = If(_objActionDetails.GoalZoneId, DBNull.Value)
            drPbpData("INJURY_TIME") = If(txtInjTime.Visible, CInt(txtInjTime.Text), DBNull.Value)
            drPbpData("SUCCESS") = If(_objActionDetails.EventId = 52 Or _objActionDetails.EventId = 78, "Y", DBNull.Value)
            'Comments, Manager Expultion And Active Goal Keeper
            drPbpData("COMMENT_LANGUAGE") = If(dgvPBP.SelectedRows(0).Cells("COMMENT_LANGUAGE").Value, DBNull.Value)
            drPbpData("MODIFY_TIME") = DateTimeHelper.GetDateString((Date.Now - _objLoginDetails.USIndiaTimeDiff), DateTimeHelper.OutputFormat.ISOFormat)
            'Çaution and Expulsion
            If _objActionDetails.EventId = 2 AndAlso Not IsDBNull(drPbpData("BOOKING_TYPE_ID")) AndAlso drPbpData("BOOKING_TYPE_ID") > 1 Then
                drPbpData("EVENT_CODE_ID") = 7 'Expulsion Event for Red card/ yellow red card based on BOOKING_TYPE_ID selection  , PLAYER_OUT_ID
            ElseIf _objActionDetails.EventId = 7 AndAlso Not IsDBNull(drPbpData("BOOKING_TYPE_ID")) AndAlso drPbpData("BOOKING_TYPE_ID") < 2 Then
                drPbpData("EVENT_CODE_ID") = 2 'Caution Event when or Red card/ yellow red card chnaged to yellow based on BOOKING_TYPE_ID selection,OFFENSIVE_PLAYER_ID
                drPbpData("OFFENSIVE_PLAYER_ID") = IIf(IsDBNull(drPbpData("OFFENSIVE_PLAYER_ID")), drPbpData("PLAYER_OUT_ID"), drPbpData("OFFENSIVE_PLAYER_ID"))
                drPbpData("PLAYER_OUT_ID") = DBNull.Value
            ElseIf _objActionDetails.EventId = 29 Then
                drPbpData("OFFENSIVE_PLAYER_ID") = If(dgvPBP.SelectedRows(0).Cells("OFFENSIVE_PLAYER_ID").Value, DBNull.Value)
            ElseIf _objActionDetails.EventId = 48 Then
                drPbpData("BOOKING_REASON_ID") = If(dgvPBP.SelectedRows(0).Cells("BOOKING_REASON_ID").Value, DBNull.Value)
                drPbpData("MANAGER_ID") = If(dgvPBP.SelectedRows(0).Cells("MANAGER_ID").Value, DBNull.Value)
            ElseIf _objActionDetails.EventId = 4 Then
                drPbpData("COMMENT_ID") = If(dgvPBP.SelectedRows(0).Cells("COMMENT_ID").Value, DBNull.Value)
                drPbpData("COMMENTS") = If(dgvPBP.SelectedRows(0).Cells("COMMENTS").Value, DBNull.Value)
            End If

            'red card assign playerout as offensive player
            If (drPbpData("EVENT_CODE_ID") = 7) Then
                drPbpData("OFFENSIVE_PLAYER_ID") = IIf(IsDBNull(drPbpData("PLAYER_OUT_ID")), drPbpData("OFFENSIVE_PLAYER_ID"), drPbpData("PLAYER_OUT_ID"))
                drPbpData("PLAYER_OUT_ID") = IIf(IsDBNull(drPbpData("PLAYER_OUT_ID")), drPbpData("OFFENSIVE_PLAYER_ID"), drPbpData("PLAYER_OUT_ID"))
            End If

            _objActionDetails.PzPBP.Tables(0).Rows.Add(drPbpData)

            ''Auto Insert for 50-50 Loss 72/AerialLoss 74 (win 71 / 73)
            If (_autoInsertEvent.ContainsKey(_objActionDetails.EventId) And (_objActionDetails.EventId <> dgvPBP.SelectedRows(0).Cells("EVENT_CODE_ID").Value)) Then
                Dim drPbpData1 As DataRow = _objActionDetails.PzPBP.Tables(0).NewRow()
                InsertDefaultValueData(drPbpData1, _autoInsertEvent.Item(_objActionDetails.EventId))
                'Aerial lost/5/50 loss credit it for opponent team
                drPbpData1("PLAYER_X") = If(_objActionDetails.PlayerX, DBNull.Value)
                drPbpData1("PLAYER_Y") = If(_objActionDetails.PlayerY, DBNull.Value)
                drPbpData1("OFFENSE_SCORE") = dgvPBP.SelectedRows(0).Cells("DEFENSE_SCORE").Value
                drPbpData1("DEFENSE_SCORE") = dgvPBP.SelectedRows(0).Cells("OFFENSE_SCORE").Value
                drPbpData1("TEAM_ID") = If((_objActionDetails.TeamId = _objGameDetails.HomeTeamID), _objGameDetails.AwayTeamID, _objGameDetails.HomeTeamID)
                _objActionDetails.PzPBP.Tables(0).Rows.Add(drPbpData1)
            End If

            _objActionDetails.PzPBP.DataSetName = "SoccerEventData"
            _objActionDetails.PzPBP.Tables(0).TableName = "PBP"

            'IsTimeEdited
            Dim isTimeEdited As Boolean
            If (_txtAssignedTime <> txtTime.Text) Then
                For row As Integer = 0 To _objActionDetails.PzPBP.Tables(0).Rows.Count - 1
                    _objActionDetails.PzPBP.Tables(0).Rows(row).Item("CONTINUATION") = "T"
                Next
                isTimeEdited = True
            End If


            'Edit pbp data
            Dim drScores As New DataSet
            drScores = _objActionsAssist.EditPlaybyPlaydata(_objActionDetails.PzPBP.GetXml(), dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value, IIf(isTimeEdited, CChar("Y"), CChar("N")), IIf(_insertMode, CChar("Y"), CChar("N")))
            'Network Disconnect Check
            If (drScores.Tables(0).Rows.Count > 0) And _objActionDetails.IsNetworkDisconnected = False Then
                Dim dtTimeDiff As DateTime
                Dim span As TimeSpan
                Dim endTime As DateTime = DateTime.Parse(drScores.Tables(0).Rows(0).Item("SYSTEM_TIME").ToString)
                dtTimeDiff = Date.Now - _objLoginDetails.USIndiaTimeDiff
                Dim startTime As DateTime = DateTime.Parse(DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat))
                span = startTime.Subtract(endTime)
                If (span.Days > 0 Or span.Hours > 0 Or span.Minutes >= 2) Then
                    ''TOSOCRS-452 --------------------------------begin
                    frmMain.lblNetwork.Enabled = True
                    frmMain.lblNetwork.Visible = True
                    frmMain.lblNetwork.Image = System.Drawing.Image.FromFile(Application.StartupPath & "\\Resources\\NetworkDisconnect.gif ")
                    frmMain.lblNetworkTop.Enabled = True
                    frmMain.lblNetworkTop.Visible = True
                    frmMain.lblNetworkTop.Image = System.Drawing.Image.FromFile(Application.StartupPath & "\\Resources\\NetworkDisconnect.gif ")
                    _objActionDetails.IsNetworkDisconnected = True
                    ''TOSOCRS-452 --------------------------------end
                End If
            End If

            'update the edited record to Processed = 'N' to prevent editing it continuosly.
            If Not _insertMode Then dgvPBP.SelectedRows(0).Cells("PROCESSED").Value = "N"
            _insertMode = False
            If _assisterRepSlNo.Contains(_objGameDetails.ReporterRoleSerial) Then btnReplace.Visible = True

            'For Automatically Fill in Player
            _previousPlayerId = Nothing
            _previousTeamId = Nothing
            _previousFieldX = Nothing
            _previousFieldY = Nothing
            _previousRow = Nothing
            If (_objActionDetails.EventId = 63) And Not IsDBNull(drPbpData("OFFENSIVE_PLAYER_ID")) Then 'Control
                _previousPlayerId = drPbpData("OFFENSIVE_PLAYER_ID")
                _previousTeamId = _objActionDetails.TeamId
                _previousRow = dgvPBP.SelectedRows(0).Index
            End If

            If _objActionDetails.EventId = 63 Or _objActionDetails.EventId = 50 Or _objActionDetails.EventId = 8 Then 'Free Kick
                If _objActionDetails.PlayerX.HasValue Then
                    _previousFieldX = _objActionDetails.PlayerX.Value
                    _previousFieldY = _objActionDetails.PlayerY.Value
                End If
            End If
            Dim subStatus = (Not btnOOB.Enabled And _objActionDetails.EventId = 22)
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, "                            Action Saved. [Event Time]" & txtTime.Text.ToString & ", [Game Time]" & frmMain.UdcRunningClock1.URCCurrentTime.ToString & "[Period] " & If(subStatus, _objGameDetails.CurrentPeriod + 1, _objGameDetails.CurrentPeriod).ToString & " [Home Score - AwayScore]  [" & frmMain.lblScoreHome.Text & "-" & frmMain.lblScoreAway.Text & "]", 1, 1)

            'Rest  the Controls
            ResetPanelEventProperties()

            Dim selectedRowIndex? As Integer
            selectedRowIndex = dgvPBP.SelectedRows(0).Index + 1
            'A2/ A3  Next row selection  to Own team
            Dim teamFilter As String = CStr(GetTeamFilter(True))
            If (selectedRowIndex <= dgvPBP.Rows.Count - 1) Then
                If _touchRepSlNo.Contains(_objGameDetails.ReporterRoleSerial) And teamFilter <> dgvPBP.Rows(selectedRowIndex).Cells("TEAM_ID").Value.ToString() Then
                    selectedRowIndex = Nothing
                    For rowSelection As Integer = dgvPBP.SelectedRows(0).Index + 1 To dgvPBP.Rows.Count - 1
                        If (teamFilter = dgvPBP.Rows(rowSelection).Cells("TEAM_ID").Value.ToString() Or dgvPBP.Rows(rowSelection).Cells("TEAM_ID").Value.ToString() = Nothing) Then
                            selectedRowIndex = rowSelection
                            Exit For
                        End If
                    Next
                End If
            End If
            If selectedRowIndex = dgvPBP.Rows.Count Then
                ClearSelectedAction()
                Exit Sub
            End If

            If selectedRowIndex <= dgvPBP.Rows.Count - 1 Then
                If Not _eventNonEditable.Contains(CInt(dgvPBP.Rows(selectedRowIndex).Cells("EVENT_CODE_ID").Value)) Then
                    dgvPBP.Rows(selectedRowIndex).Selected = True
                    dgvPBP_CellClick(dgvPBP, New DataGridViewCellEventArgs(0, dgvPBP.SelectedRows(0).Index))
                    If dgvPBP.SelectedRows.Count > 0 Then
                        dgvPBP.FirstDisplayedScrollingRowIndex = dgvPBP.SelectedRows(0).Index
                    End If
                End If
            End If
        Catch ex As Exception
            If dgvPBP.SelectedRows.Count = 0 Then
                _messageDialog.Show(_objGameDetails.languageid, "The selected timeline is either deleted/edited by another reporter at the same time. Please re-select once again!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Warning)
                ResetPanelEventProperties()
            Else
                Throw
            End If
        Finally
            _objActionDetails.PzPBP.Tables(0).Rows.Clear()
            _objActionDetails.PzPBP.Tables(2).Rows.Clear()
        End Try
    End Sub
    'FPLRS-227 goal missing issue due to "Double click & Down-arrow" and "TimeEdit enter & MouseClick keys"
    Private Sub HighlightPBPSelectedRow()
        Try
            Dim dgPbpDataSource As DataTable
            dgPbpDataSource = dgvPBP.DataSource
            Dim recordSelected = dgPbpDataSource.AsEnumerable().
                                 Select(Function(r, i) New With {.Row = r, .Index = i}).
                                 Where(Function(x) Not IsDBNull(x.Row.Field(Of Decimal?)("SEQUENCE_NUMBER")) AndAlso x.Row.Field(Of Decimal?)("SEQUENCE_NUMBER") = _pbpRowSelectedSqNo)
            If recordSelected.Any() Then
                'map the action based on SNO and select the action.
                Dim indexSelected As Integer = recordSelected.First().Index
                dgvPBP.Rows(indexSelected).Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    'called set Home/Away team color
    Public Sub SetTeamColor(ByVal teamColor As Color)
        Try
            Dim textForeColor As Color = clsUtility.GetTextColor(teamColor)
            UdcSoccerPlayerPositions1.USPButtonBackColor = teamColor
            UdcSoccerPlayerPositions1.ForeColor = textForeColor
            clsUtility.SetPanelButtonColor(pnlsubstitute, teamColor, textForeColor)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function CalculateTimeElapsedBefore(ByVal currentElapsedTime As Integer) As Integer
        Try
            Dim timeElapsed As New Dictionary(Of Integer, Integer)
            timeElapsed.Add(1, 0)
            timeElapsed.Add(2, 2700) 'FIRSTHALF
            timeElapsed.Add(3, 5400) 'SECONDHALF
            timeElapsed.Add(4, 6300) 'FIRSTEXTRA
            timeElapsed.Add(5, 7200)
            If dgvPBP.SelectedRows.Count > 0 Then
                Return If(timeElapsed.ContainsKey(dgvPBP.SelectedRows(0).Cells("PERIOD").Value), currentElapsedTime - timeElapsed.Item(dgvPBP.SelectedRows(0).Cells("PERIOD").Value), currentElapsedTime)
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function CalculateTimeElapsedBeforeMilliSec(ByVal currentElapsedTime As Integer) As Integer
        Try
            Dim timeElapsed As New Dictionary(Of Integer, Integer)
            timeElapsed.Add(1, 0)
            timeElapsed.Add(2, 2700000) 'FIRSTHALF
            timeElapsed.Add(3, 5400000) 'SECONDHALF
            timeElapsed.Add(4, 6300000) 'FIRSTEXTRA
            timeElapsed.Add(5, 7200000)
            If dgvPBP.SelectedRows.Count > 0 Then
                Return If(timeElapsed.ContainsKey(dgvPBP.SelectedRows(0).Cells("PERIOD").Value), currentElapsedTime - timeElapsed.Item(dgvPBP.SelectedRows(0).Cells("PERIOD").Value), currentElapsedTime)
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub InsertDefaultValueData(ByVal drPbpData As DataRow, ByVal eventId As Integer)
        Try
            'Default  Values
            drPbpData("GAME_CODE") = _objGameDetails.GameCode
            drPbpData("FEED_NUMBER") = _objGameDetails.FeedNumber
            drPbpData("REPORTER_ROLE") = _objGameDetails.ReporterRole
            drPbpData("CONTINUATION") = "F"
            drPbpData("RECORD_EDITED") = "N"
            drPbpData("PROCESSED") = "N"
            drPbpData("UNIQUE_ID") = 1
            drPbpData("EDIT_UID") = 0
            drPbpData("DEMO_DATA") = "N"
            drPbpData("PERIOD") = dgvPBP.SelectedRows(0).Cells("PERIOD").Value
            drPbpData("EVENT_CODE_ID") = eventId
            drPbpData("TIME_ELAPSED") = If(_objGameDetails.IsDefaultGameClock, CalculateTimeElapsedBefore(CInt(_objUtility.ConvertMinuteToSecond(txtTime.Text))), CInt(_objUtility.ConvertMinuteToSecond(txtTime.Text)))
            drPbpData("GAME_TIME_MS") = If(_objGameDetails.IsDefaultGameClock, CalculateTimeElapsedBeforeMilliSec(CInt(_objUtility.ConvertMinuteToMillisecond(txtTime.Text))), CInt(_objUtility.ConvertMinuteToMillisecond(txtTime.Text)))
            Dim dtTimeDiff As DateTime
            dtTimeDiff = CDate(dgvPBP.SelectedRows(0).Cells("SYSTEM_TIME").Value.ToString())
            drPbpData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub InsertListBoxValue(ByVal drPbpData As DataRow, ByVal pbpColumnName As String)
        Try
            'List box Selected Values based on Rules PBP column to datarow
            strBuilder.Clear()
            pnlEventProperties.Visible = True 'Visible true to loop the controls
            For Each listControl As Control In pnlEventProperties.Controls.OfType(Of ListBox)()
                Dim lstEventDetails As ListBox
                lstEventDetails = CType(listControl, ListBox)
                If lstEventDetails.Visible And (lstEventDetails.Tag = pbpColumnName Or pbpColumnName = Nothing) Then
                    strBuilder.Append(lstEventDetails.Name & ": " & lstEventDetails.Tag & " -> " & lstEventDetails.Text & vbNewLine & "                            ")
                    drPbpData(lstEventDetails.Tag) = If(lstEventDetails.SelectedValue, DBNull.Value)
                End If
            Next
            If (strBuilder.ToString <> "") Then
                _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemClicked, strBuilder.ToString, Nothing, 1, 0)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Function InsertValidation() As Tuple(Of Boolean, String)
        Try
            'dont allow assister to save any action if primary is down.
            If _objGameDetails.IsPrimaryReporterDown Then
                Return New Tuple(Of Boolean, String)(True, "You are not allowed to Insert/Update Events - Primary Reporter is Down!")
            End If
            'Time Validation
            If txtTime.Text.Trim().Replace(":", "").Trim = "" Then
                Return New Tuple(Of Boolean, String)(True, "Clock time is not valid. Please check!")
            End If

            'insert mode validations
            If _insertMode Then
                'TimePeriodValidation
                Dim timeElapsed = If(_objGameDetails.IsDefaultGameClock And dgvPBP.SelectedRows(0).Cells("PERIOD").Value > 1, CalculateTimeElapsedBefore(CInt(_objUtility.ConvertMinuteToSecond(txtTime.Text))), CInt(_objUtility.ConvertMinuteToSecond(txtTime.Text)))
                Dim selectedRowsPeriod As Integer = 0
                If dgvPBP.SelectedRows.Count > 0 Then
                    selectedRowsPeriod = dgvPBP.SelectedRows(0).Cells("PERIOD").Value
                End If
                If (selectedRowsPeriod = 2 And timeElapsed < 0) Or (selectedRowsPeriod = 3 And timeElapsed < 0) Or (selectedRowsPeriod = 3 And timeElapsed < 0) Then
                    Return New Tuple(Of Boolean, String)(True, "Clock time is not valid for the selected period. Please check!")
                End If

                'CheckInsertProperEvent
                If _objActionDetails.EventId Is Nothing Then
                    If _messageDialog.Show(_objGameDetails.languageid, "You are in INSERT mode, click YES to select an event or click NO to exit from INSERT mode.", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Return New Tuple(Of Boolean, String)(True, Nothing)
                    Else
                        clsUtility.SetButtonColor(btnInsert, Color.FromArgb(112, 151, 236))
                        dgvPBP_CellClick(dgvPBP, New DataGridViewCellEventArgs(0, dgvPBP.SelectedRows(0).Index))
                        Return New Tuple(Of Boolean, String)(True, Nothing)
                    End If
                End If
            End If

            'ListBoxValidation player validation
            Dim offensivePlayer As Tuple(Of Boolean, Integer?) = ListBoxDataCheck("OFFENSIVE_PLAYER_ID")
            If offensivePlayer.Item1 Then
                Return New Tuple(Of Boolean, String)(True, "Select mandatory data point (Player)!")
            End If

            'Event ID is Empty
            If Not _objActionDetails.EventId.HasValue Then
                Return New Tuple(Of Boolean, String)(True, "Select Time line / Event is Empty ")
            End If

            'Event Injury time
            If _objActionDetails.EventId = 66 Then
                If txtInjTime.Text.Trim = "" Then
                    txtInjTime.Focus()
                    Return New Tuple(Of Boolean, String)(True, "Enter the correct time!")
                ElseIf CInt(txtInjTime.Text) > 166 Then
                    If _messageDialog.Show(_objGameDetails.languageid, "Please check the Injury time you entered", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Return New Tuple(Of Boolean, String)(False, Nothing)
                    Else
                        Return New Tuple(Of Boolean, String)(True, Nothing)
                    End If
                End If
            ElseIf _objActionDetails.EventId = 22 Then
                'Player In and Out are mandatory
                Dim checkSubPlayerOut As Tuple(Of Boolean, Integer?) = ListBoxDataCheck("PLAYER_OUT_ID")
                If checkSubPlayerOut.Item1 Then
                    Return New Tuple(Of Boolean, String)(True, "Select Player Out for Subsitution!")
                Else
                    'if the player is already used in Sub then dont allow.
                    Dim dgPbpDataSource As DataTable
                    dgPbpDataSource = dgvPBP.DataSource
                    Dim pbpData = From actions In dgPbpDataSource _
                                          Where actions.Field(Of Decimal?)("SEQUENCE_NUMBER") > dgvPBP.SelectedRows(0).Cells("SEQUENCE_NUMBER").Value AndAlso _
                                                (actions.Field(Of Int32?)("OFFENSIVE_PLAYER_ID") = checkSubPlayerOut.Item2 Or _
                                                actions.Field(Of Int32?)("ASSISTING_PLAYER_ID") = checkSubPlayerOut.Item2 Or _
                                                actions.Field(Of Int32?)("DEFENSIVE_PLAYER_ID") = checkSubPlayerOut.Item2 Or _
                                                actions.Field(Of Int32?)("PLAYER_OUT_ID") = checkSubPlayerOut.Item2)
                                                Order By actions.Field(Of Decimal?)("SEQUENCE_NUMBER")
                    If pbpData.Count > 0 Then
                        Return New Tuple(Of Boolean, String)(True, "Player is already used in other actions - Substitute is not allowed")
                    End If
                End If
            End If
            'Time Validation
            Dim clockTime As String()
            Dim arrTime As String()
            Dim minutesEntered As Integer = 0
            Dim secondsEntered As Integer = 0

            arrTime = txtTime.Text.Split(CChar(":"))
            clockTime = frmMain.UdcRunningClock1.URCCurrentTime.Split(CChar(":"))
            If arrTime(0).Trim() <> Nothing Then
                minutesEntered = arrTime(0)
            End If
            If arrTime(1).Trim() <> Nothing Then
                secondsEntered = arrTime(1)
            End If

            If ((CInt(clockTime(0)) < minutesEntered) Or ((minutesEntered = CInt(clockTime(0)) And CInt(clockTime(1)) < secondsEntered))) And (CInt(dgvPBP.SelectedRows(0).Cells("PERIOD").Value) = CInt(_objGameDetails.CurrentPeriod)) Then
                Return New Tuple(Of Boolean, String)(True, "Current Clock Time should be greater than entered time!")
            End If
            Dim assistingPlayer As Tuple(Of Boolean, Integer?) = ListBoxDataCheck("ASSISTING_PLAYER_ID")
            Dim secondAssistingPlayer As Tuple(Of Boolean, Integer?) = ListBoxDataCheck("SECOND_ASSISTER_ID")

            'same player selection
            Dim duplicateplayer As Boolean = False
            If (offensivePlayer.Item2 = assistingPlayer.Item2 And Not offensivePlayer.Item1) Then
                duplicateplayer = True
            ElseIf (offensivePlayer.Item2 = secondAssistingPlayer.Item2 And Not offensivePlayer.Item1) Then
                duplicateplayer = True
            ElseIf (assistingPlayer.Item2 = secondAssistingPlayer.Item2 And Not secondAssistingPlayer.Item1) Then
                duplicateplayer = True
            End If

            If (duplicateplayer) Then
                Return New Tuple(Of Boolean, String)(True, "Same player selected!")
            End If
            'Else no failure
            Return New Tuple(Of Boolean, String)(False, "Select Time line / Event is Empty ")
        Catch ex As Exception
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "InsertValidation Failed dgvRowsCount : " & dgvPBP.SelectedRows.Count, Nothing, 1, 0)
            Throw
        End Try
    End Function
    Function ListBoxDataCheck(listBoxTag As String) As Tuple(Of Boolean, Integer?)
        Dim status As Boolean
        Dim selectedValue As Integer?
        Try
            Dim listBoxPlayer = Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of ListBox)()).FirstOrDefault(Function(c1) c1.Tag = listBoxTag)
            If (listBoxPlayer IsNot Nothing) Then
                status = listBoxPlayer.SelectedValue Is Nothing
                selectedValue = listBoxPlayer.SelectedValue
            End If
        Catch ex As Exception
            _objUtilAudit.AuditLog(_objGameDetails.HomeTeamAbbrev, _objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "ListBoxDataCheck Failed!.", Nothing, 1, 0)
            Throw
        End Try
        Return New Tuple(Of Boolean, Integer?)(status, selectedValue)
    End Function
    Public Sub PnlEventPropertiesVisible()
        pnlEventProperties.Visible = True 'Visible child control
        If pnlEventProperties.Controls.OfType(Of ListBox)().Count(Function(c1) c1.Visible) > 0 Then
            pnlEventProperties.BringToFront()
            pnlEventProperties.Visible = True
            btnCancel.Visible = True
            pnlEventProperties.Controls.Add(pnlSave)
            pnlSave.Location = New Point(120, 406)
            'Reset the ListBox
            AlignListboxes()
            'Setting points
            btnSave.Focus()
        Else
            pnlEventProperties.Visible = False
        End If
    End Sub
    Private Sub UnHighlightButtonColor(buttonBackColor As Color)
        Dim unHiglight = Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).FirstOrDefault(Function(c1) c1.BackColor = buttonBackColor)
        Controls.OfType(Of Panel)().SelectMany(Function(c) c.Controls.OfType(Of Button)()).Where(Function(c1) c1.BackColor = buttonBackColor)
        If unHiglight IsNot Nothing Then
            clsUtility.SetButtonColor(unHiglight, Color.WhiteSmoke)
        End If
    End Sub
    Function TimeLineValidation() As Boolean
        Try
            Dim validation As Boolean
            Dim teamFilter As Integer = GetTeamFilter(True)
            If dgvPBP.SelectedRows(0).Cells("PROCESSED").Value = "N" Then 'edit of the same record continuosly
                validation = True
                _messageDialog.Show(_objGameDetails.languageid, "Last edit of this action is getting processed - please wait before editing again!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            ElseIf (_eventNonEditable.Contains(_objActionDetails.EventId)) Then 'Validation to avoid list fill
                validation = True
                _messageDialog.Show(_objGameDetails.languageid, "Edit Functionality Not Available", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            ElseIf (teamFilter > 0 And _objActionDetails.TeamId > 0) And (teamFilter <> _objActionDetails.TeamId) And _touchRepSlNo.Contains(_objGameDetails.ReporterRoleSerial) Then 'Home Reporter is only able to edit Home Actions as well as "Out of Bounds" and "Obstacle"
                validation = True
                _messageDialog.Show(_objGameDetails.languageid, "This Action belongs to the " + IIf(_objGameDetails.ReporterRoleSerial = 3, "Away", "Home") + " reporter", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If
            Return validation
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Collect X/Y Coordinates for 'Obstacle'
    Function CurrentCoordinatesActions(selectedValue As Integer?) As Boolean
        Try
            Dim currentCoordinates As Point
            If (_eventCoordinatesData.Item(_objActionDetails.EventId).ContainsKey(selectedValue)) Then
                UdcSoccerField1.USFClearMarks()
                currentCoordinates = UdcSoccerField1.USFSetMark(_eventCoordinatesData.Item(_objActionDetails.EventId).Item(selectedValue), IIf(_objActionDetails.PlayerX.HasValue, _objActionDetails.PlayerX, IIf(_objGameDetails.IsHomeTeamOnLeft, 0, 80)), IIf(_objActionDetails.PlayerY.HasValue, _objActionDetails.PlayerY, 0), False)
                _objActionDetails.PlayerX = currentCoordinates.X
                _objActionDetails.PlayerY = currentCoordinates.Y
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Function IsDeleteAllowed(ByVal eventId As Integer, ByVal teamName As String, ByVal playerName As String) As Boolean
        Try
            Dim returnStatus As Boolean = True
            Dim messageText As String = Nothing
            Select Case eventId
                Case 11, 17, 28
                    messageText = "You are about to delete a Goal for " + teamName + ". This will change the score. Make sure Primary reporter does not add any actions until the score updates take place. Are you sure?"
                Case 2   'Booking
                    messageText = "You are about to delete a Booking for " + playerName + "(" + teamName + "). Are you sure?"
                Case 22  'Sub
                    messageText = "You are about to delete a Substitution for " + teamName + ". This will change the lineup. Are you sure?"
                Case 18  'Miss pen
                    messageText = "You are about to delete a Missed Penalty Shot for " + playerName + "(" + teamName + "). Are you sure?"
                Case 23, 24 'Lineups, game start
                    returnStatus = False
                Case 13, 21, 34, 10
                    _messageDialog.Show(_objGameDetails.languageid, "Assister cannot delete GAME START/Start-End Period/End Game events", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
                Case 58, 59, 65
                    _messageDialog.Show(_objGameDetails.languageid, "Assister cannot delete suspended and delay events", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
                Case 72
                    _messageDialog.Show(_objGameDetails.languageid, "Cannot delete 50-50 Ball Lost events, To delete the lost action you need to delete the win action", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
                Case 74
                    _messageDialog.Show("Cannot delete Aerial Duel Lost events, To delete the lost action you need to delete the win action", Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    returnStatus = False
            End Select
            If messageText IsNot Nothing Then
                If _messageDialog.Show(messageText, Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    returnStatus = False
                End If
            End If
            If dgvPBP.SelectedRows(0).Cells("PROCESSED").Value = "N" Then 'delete of the same record continuosly
                _messageDialog.Show(_objGameDetails.languageid, "Last delete of this action is getting processed - please wait before deleting again!", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                returnStatus = False
            End If

            If _deleteKeyPressCnt >= 5 Then
                If _messageDialog.Show(_objGameDetails.languageid, "You have deleted 5 straight actions Click YES to continue.", Me.Text, MessageDialogButtons.YesNo, MessageDialogIcon.Question) = Windows.Forms.DialogResult.No Then
                    returnStatus = False
                End If
            End If
            Return returnStatus

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EventCoordinates()
        If _eventCoordinates.ContainsKey(If(_objActionDetails.EventId, 0)) Then
            Dim currentCoordinates As Point
            UdcSoccerField1.USFClearMarks()
            Dim enumValue As udcSoccerField.enMarkLocation
            If (UdcSoccerField1.USFHomeTeamOnLeft) Then
                enumValue = _eventCoordinates.Item(_objActionDetails.EventId).Item1
            Else
                enumValue = _eventCoordinates.Item(_objActionDetails.EventId).Item2
            End If

            If (_eventCoordinates.Item(_objActionDetails.EventId).Item3 Or _objActionDetails.PlayerX.HasValue Or _objActionDetails.PlayerY.HasValue) Then
                currentCoordinates = UdcSoccerField1.USFSetMark(enumValue, IIf(_objActionDetails.PlayerX.HasValue, _objActionDetails.PlayerX, 0), IIf(_objActionDetails.PlayerY.HasValue, _objActionDetails.PlayerY, 0))
                _objActionDetails.PlayerX = currentCoordinates.X
                _objActionDetails.PlayerY = currentCoordinates.Y
            End If
        End If
    End Sub
    Public Function FinalValidation() As List(Of String)
        Dim dsRefreshData As New DataSet
        Dim messagestring As New List(Of String)
        chkHome.Checked = False
        chkAway.Checked = False
        chkOwnAction.Checked = False
        chkQC.Checked = False
        chk1sthalf.Checked = False
        chk2ndhalf.Checked = False
        chkInc.Checked = True

        If _objGameDetails.ReporterRoleSerial <> 2 Then
            chkOwnAction.Checked = True
        End If
        'Full data Load
        dsRefreshData = _objGeneral.RefreshPZPbpData(_objGameDetails.GameCode, _objGameDetails.FeedNumber, _objGameDetails.languageid, CInt(IIf(_objGameDetails.IsDefaultGameClock, 1, 0)), True, CInt(_objGameDetails.ReporterRoleSerial), 0)
        PbpDataTreeLoad(dsRefreshData.Tables(0), True)

        If _objGameDetails.ReporterRoleSerial = 2 Then
            messagestring = FinalValidationTeam(dsRefreshData.Tables(0), _objGameDetails.HomeTeamID)
            For Each Msg In (FinalValidationTeam(dsRefreshData.Tables(0), _objGameDetails.AwayTeamID))
                messagestring.Add(Msg)
            Next
        Else
            messagestring = FinalValidationTeam(dsRefreshData.Tables(0), GetTeamFilter(False))
        End If
        Return messagestring
    End Function

    Public Function FinalValidationTeam(pbpdata As DataTable, teamId As Integer) As List(Of String)

        Dim strTeam As String = _objGameDetails.AwayTeamAbbrev
        Dim stropponentTeam As String
        Dim opponentTeamId As Integer


        If teamId = _objGameDetails.HomeTeamID Then
            strTeam = _objGameDetails.HomeTeamAbbrev
            stropponentTeam = _objGameDetails.AwayTeamAbbrev
            opponentTeamId = _objGameDetails.AwayTeamID
        Else
            stropponentTeam = _objGameDetails.HomeTeamAbbrev
            opponentTeamId = _objGameDetails.HomeTeamID
        End If

        Dim messagestring As New List(Of String)

        If dgvPBP.RowCount > 0 Then
            messagestring.Add("There are " + strTeam + " actions that are missing required data. Would you still like to Finalize?")
        End If

        'LINQ Get Penalty 
        Dim penaltyData = (From pbp In pbpdata.AsEnumerable() _
                          Where (pbp.Field(Of Object)("TEAM_ID") = teamId Or _objGameDetails.ReporterRoleSerial = 2) _
                            AndAlso pbp.Field(Of Object)("EVENT_CODE_ID") = 17 _
                            AndAlso pbp.Field(Of Decimal)("SEQUENCE_NUMBER") <> -1 _
                          Select pbp.Field(Of Object)("SNo")).ToList()


        If penaltyData.Any() Then
            For Each Penalty In penaltyData
                Dim previousPenaltyData = (From pbp In pbpdata.AsEnumerable() _
                                           Where pbp.Field(Of Object)("SNo") = CInt(Penalty) - 1 _
                                           Select pbp).FirstOrDefault
                If (previousPenaltyData.Field(Of Object)("EVENT_CODE_ID") <> 9) Or (previousPenaltyData.Field(Of Object)("FK_TYPE_ID") <> 3) And (previousPenaltyData.Field(Of Object)("TEAM_ID") = teamId) Then
                    messagestring.Add("The " + strTeam + " has more ""Penalty Goals"" than ""Penalty Kick Attempts"" or Please check to make sure the Free Kick action before the Penalty Goal has a sub-type of Penalty. Would you still like to finalize?")
                    Exit For
                End If
            Next
        End If

        Dim eventsCheck() As Integer = {11, 17, 75, 5, 9, 19, 71, 73} 'Normal Goal, Penalty Goal, save, 5 -  CORNER_CROSS_DESC, 19 - SHOT_RESULT, 71 - WIN_TYPE_ID, 73 - WIN_TYPE_ID

        'LINQ Get Goal/ Save /shot all team  
        Dim eventsCheckData = (From pbp In pbpdata.AsEnumerable() _
                              Where eventsCheck.Contains(pbp.Field(Of Object)("EVENT_CODE_ID")) _
                               AndAlso pbp.Field(Of Decimal)("SEQUENCE_NUMBER") <> -1 _
                               Select pbp).ToList()

        'Filter to get count for each event 
        Dim goalCount As Integer = (From goalData In eventsCheckData Where (goalData.Field(Of Int16)("EVENT_CODE_ID") = 11 Or goalData.Field(Of Int16)("EVENT_CODE_ID") = 17) AndAlso goalData.Field(Of Object)("TEAM_ID") = teamId).Count()

        Dim shotOntargerCount As Integer = (From shotOntarger In eventsCheckData _
                                            Where ((shotOntarger.Field(Of Int16)("EVENT_CODE_ID") = 5 AndAlso shotOntarger.Field(Of Object)("CORNER_CROSS_DESC") = 13) Or _
                                                    (shotOntarger.Field(Of Int16)("EVENT_CODE_ID") = 9 AndAlso shotOntarger.Field(Of Object)("FK_RESULT_ID") = 4) Or _
                                                    (shotOntarger.Field(Of Int16)("EVENT_CODE_ID") = 19 AndAlso shotOntarger.Field(Of Object)("SHOT_RESULT") = 50) Or _
                                                    ((shotOntarger.Field(Of Int16)("EVENT_CODE_ID") = 71 Or shotOntarger.Field(Of Int16)("EVENT_CODE_ID") = 73) AndAlso shotOntarger.Field(Of Object)("WIN_TYPE_ID") = 4) _
                                                    ) _
                                            AndAlso shotOntarger.Field(Of Object)("TEAM_ID") = teamId _
                                            ).Count()
        'opponentTeamId Save count
        Dim saveCount As Integer = (From saveData In eventsCheckData Where saveData.Field(Of Int16)("EVENT_CODE_ID") = 75 AndAlso saveData.Field(Of Object)("TEAM_ID") = opponentTeamId).Count()

        'validation Count  (shot - goal) != save) 
        If Math.Abs(shotOntargerCount - goalCount) <> saveCount Then
            messagestring.Add("The Saves (" + saveCount.ToString + ") for " + stropponentTeam + " do not equal the Shots on Target (" + shotOntargerCount.ToString + ") - Goals (" + goalCount.ToString + ") for " + strTeam + ".This is very rare but can happen.Would you still like to finalize?")
        End If
        Return messagestring
    End Function

    Public Sub finalLoadAll()
        Dim messagestring(1) As String
        Dim dsRefreshData As New DataSet   'Full data Load
        dsRefreshData = _objGeneral.RefreshPZPbpData(_objGameDetails.GameCode, _objGameDetails.FeedNumber, _objGameDetails.languageid, CInt(IIf(_objGameDetails.IsDefaultGameClock, 1, 0)), True, CInt(_objGameDetails.ReporterRoleSerial), 0)
        Dim teamId As Integer = GetTeamFilter(False)
        Dim eventValidate() As Nullable(Of Int16) = {17, 9} ' Penalty, Free Kick 
        Dim seq As Integer = 0
        Dim strTeam As String = "Away"
        chkHome.Checked = False
        chkAway.Checked = False
        chkOwnAction.Checked = False
        chkQC.Checked = False
        chk1sthalf.Checked = False
        chk2ndhalf.Checked = False
        chkInc.Checked = False
        chkOwnAction.Checked = True
        PbpDataTreeLoad(dsRefreshData.Tables(0), True)
    End Sub

#End Region

#Region "Goal Frame"

    Private Sub btnViewGoal_Click(sender As Object, e As EventArgs) Handles btnViewGoal.Click
        Try
            pnlGoalframe.Visible = True
            pnlField.Visible = False
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub btnViewField_Click(sender As Object, e As EventArgs) Handles btnViewField.Click
        Try
            pnlField.Visible = True
            pnlGoalframe.Visible = False
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub UdcSoccerField1_USFRightClick(sender As Object, e As USFEventArgs) Handles UdcSoccerField1.USFRightClick
        Try
            InsertPlayByPlayData()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub UdcSoccerGoalFrame1_USGFRightClick(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFRightClick
        Try
            InsertPlayByPlayData()
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
    Private Sub UdcSoccerField1_USFClick(sender As Object, e As USFEventArgs) Handles UdcSoccerField1.USFClick
        Try
            If e.USFMarks.Rows.Count > 1 Then
                'Green
                _objActionDetails.PlayerX = e.USFMarks.Rows(0).Item("X").ToString()
                _objActionDetails.PlayerY = e.USFMarks.Rows(0).Item("Y").ToString()
                'orange
                If Not (e.USFMarks.Rows(1).Item("X") Is DBNull.Value) Then
                    _objActionDetails.KeeperZoneX = e.USFMarks.Rows(1).Item("X").ToString()
                End If

                If Not (e.USFMarks.Rows(1).Item("Y") Is DBNull.Value) Then
                    _objActionDetails.KepperZoneY = e.USFMarks.Rows(1).Item("Y").ToString()
                End If
                If (_objActionDetails.EventId = 63 Or _objActionDetails.EventId = 50) Then
                    InsertPlayByPlayData()
                    Exit Sub
                End If
            End If
            If (_eventCoordinatesData.ContainsKey(If(_objActionDetails.EventId, 0))) Then
                Dim obstaclelistbox As ListBox = pnlEventProperties.Controls.OfType(Of ListBox)().FirstOrDefault(Function(c) c.Tag = "OBSTACLE_TYPE_ID")
                CurrentCoordinatesActions(CInt(obstaclelistbox.SelectedValue))
            Else
                EventCoordinates()
            End If

            If btnViewGoal.Enabled And UdcSoccerField1.USFNextMark = 1 And UdcSoccerGoalFrame1.Enabled Then
                btnViewGoal_Click(sender, e)
            End If


            If _objActionDetails.PlayerX.HasValue Then
                picFieldBlinkGreen.Visible = False
            End If

            If _objActionDetails.KeeperZoneX.HasValue Then
                picFieldBlinkOrange.Visible = False
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub UdcSoccerGoalFrame1_USGFClick(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFClick
        Dim pointYz As PointF
        If e.USGFmark1.IsEmpty Then
            lblClickLocationGreen.Text = ""
            picGFGreenBlink.Visible = True
            picGFGreen.Visible = False
            _objActionDetails.BallY = Nothing
            _objActionDetails.BallZ = Nothing
            _objActionDetails.GoalZoneId = Nothing
        Else
            'green
            pointYz = GetMappedCoordinateValues(e.USGFmark1.X, e.USGFmark1.Y, Not (UdcSoccerField1.USFHomeTeamOnLeft))
            lblClickLocationGreen.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            picGFGreen.Visible = True
            _objActionDetails.BallY = pointYz.X
            _objActionDetails.BallZ = pointYz.Y
            _objActionDetails.GoalZoneId = e.USGFmark1zone
        End If

        If e.USGFmark2.IsEmpty Then
            lblClickLocationOrange.Text = ""
            picGFOrange.Visible = False
            _objActionDetails.HandY = Nothing
            _objActionDetails.HandZ = Nothing
            _objActionDetails.KepperZoneId = Nothing
        Else
            'Orange
            pointYz = GetMappedCoordinateValues(e.USGFmark2.X, e.USGFmark2.Y, Not (UdcSoccerField1.USFHomeTeamOnLeft))
            lblClickLocationOrange.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            picGFOrange.Visible = True
            _objActionDetails.HandY = pointYz.X
            _objActionDetails.HandZ = pointYz.Y
            _objActionDetails.KepperZoneId = If(e.USGFmark2zone = 0, Nothing, e.USGFmark2zone)
        End If

        If _objActionDetails.BallY.HasValue Then
            picGFGreenBlink.Visible = False
        End If

        If _objActionDetails.HandY.HasValue And picGFOrange.Visible Then
            picGFOrangeBlink.Visible = False
        End If


    End Sub

    Private Sub UdcSoccerGoalFrame1_USGFMarkRemoved(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFMarkRemoved
        Try
            Dim pointYz As PointF
            If e.USGFmark1.IsEmpty Then
                lblClickLocationGreen.Text = ""
            Else
                pointYz = GetMappedCoordinateValues(e.USGFmark1.X, e.USGFmark1.Y, Not (UdcSoccerField1.USFHomeTeamOnLeft))
                _objActionDetails.BallY = Nothing
                _objActionDetails.BallZ = Nothing
                _objActionDetails.GoalZoneId = e.USGFmark1zone
                lblClickLocationGreen.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            End If

            If e.USGFmark2.IsEmpty Then
                lblClickLocationOrange.Text = ""
            Else
                pointYz = GetMappedCoordinateValues(e.USGFmark2.X, e.USGFmark2.Y, Not (UdcSoccerField1.USFHomeTeamOnLeft))
                _objActionDetails.HandY = Nothing
                _objActionDetails.HandZ = Nothing
                _objActionDetails.KepperZoneId = Nothing
                lblClickLocationOrange.Text = "Y: " & pointYz.X & ", Z:" & pointYz.Y
            End If


            If picGFGreen.Visible = True And e.USGFmark1.IsEmpty Then
                picGFGreenBlink.Visible = True
            End If

            If picGFOrange.Visible = True And e.USGFmark2.IsEmpty Then
                picGFOrangeBlink.Visible = True
            End If

        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub UdcSoccerGoalFrame1_USGFMouseMove(sender As Object, e As USGFEventArgs) Handles UdcSoccerGoalFrame1.USGFMouseMove
        Try
            Dim pointYz As PointF
            pointYz = GetMappedCoordinateValues(e.USGFx, e.USGFy, Not (UdcSoccerField1.USFHomeTeamOnLeft))

            lblMoveY.Text = "Y: " & pointYz.X
            lblMoveZ.Text = "Z: " & pointYz.Y
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    'TOPZ-296
    Private Sub btnZoom_Click(sender As Object, e As EventArgs) Handles btnZoom.Click
        Try
            If btnZoom.Text = "Wide View" Then
                UdcSoccerGoalFrame1.Width = pnlGoalframe.Width
                UdcSoccerGoalFrame1.Left = 0
                UdcSoccerGoalFrame1.Top = pnlGoalframe.Height - pnlInfo.Height - UdcSoccerGoalFrame1.Height
                btnZoom.Text = "Closer View"
            Else
                UdcSoccerGoalFrame1.Height = 184
                UdcSoccerGoalFrame1.Width = 1242
                UdcSoccerGoalFrame1.Top = 0
                UdcSoccerGoalFrame1.Left = -423
                btnZoom.Text = "Wide View"
            End If
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub

    Private Sub UdcSoccerField1_USFMouseMove(sender As Object, e As USFEventArgs) Handles UdcSoccerField1.USFMouseMove
        Try
            lblFieldX.Text = "X: " & e.USFx
            lblFieldY.Text = "Y: " & e.USFy
        Catch ex As Exception
            _messageDialog.Show(ex, Text)
        End Try
    End Sub
#End Region

    Private Function GetMappedCoordinateValues(dblX As Double, dblY As Double, Optional blnLeftGoal As Boolean = True) As PointF
        Try
            Dim pntReturn As PointF
            Const fieldHeight As Integer = 70

            If blnLeftGoal = True Then
                dblX = (fieldHeight / 2) - dblX
            Else
                dblX = (fieldHeight / 2) + dblX
            End If

            pntReturn.X = Math.Round(dblX, 2)
            pntReturn.Y = Math.Round(dblY, 2)
            Return pntReturn
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function GetUnmappedCoordinateValues(dblX As Double, dblY As Double, Optional blnLeftGoal As Boolean = True) As PointF
        Try
            Dim pntReturn As PointF
            Const fieldHeight As Integer = 70

            If blnLeftGoal = True Then
                dblX = (fieldHeight / 2) - dblX
            Else
                dblX = dblX - (fieldHeight / 2)
            End If

            pntReturn.X = Math.Round(dblX, 2)
            pntReturn.Y = Math.Round(dblY, 2)
            Return pntReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub UdcSoccerField1_USFMarkRemoved(sender As Object, e As USFEventArgs) Handles UdcSoccerField1.USFMarkRemoved
        Try

            If e.USFMarks.Rows(1).Item("X") Is DBNull.Value Then
                _objActionDetails.PlayerX = Nothing
                _objActionDetails.PlayerY = Nothing
            End If

            If e.USFMarks.Rows(1).Item("X") Is DBNull.Value Then
                _objActionDetails.KepperZoneY = Nothing
                _objActionDetails.KeeperZoneX = Nothing
            End If

            If picFieldGreen.Visible = True And e.USFMarks.Rows(0).Item("X") Is DBNull.Value Then
                picFieldBlinkGreen.Visible = True
            End If

            If picFieldOrange.Visible = True And e.USFMarks.Rows(1).Item("X") Is DBNull.Value Then
                picFieldBlinkOrange.Visible = True
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class