﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModule1PBP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Event")
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Period")
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Team")
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Time")
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Type")
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("By Player")
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("SubType")
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("SubSubType")
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Area")
        Dim ListViewItem10 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("SequenceNumber")
        Dim ListViewItem11 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("EventID")
        Dim ListViewItem12 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("UniqueID")
        Dim ListViewItem13 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Team_ID")
        Me.btnSubstituteHome = New System.Windows.Forms.Button
        Me.btnKeyMomentHome = New System.Windows.Forms.Button
        Me.btnGoalThreatHome = New System.Windows.Forms.Button
        Me.btnGoalKickHome = New System.Windows.Forms.Button
        Me.btnOffSideHome = New System.Windows.Forms.Button
        Me.btnFoulHome = New System.Windows.Forms.Button
        Me.btnCrossHome = New System.Windows.Forms.Button
        Me.btnCornerHome = New System.Windows.Forms.Button
        Me.btnOffTargetHome = New System.Windows.Forms.Button
        Me.btnBookingHome = New System.Windows.Forms.Button
        Me.btnOnTargetHome = New System.Windows.Forms.Button
        Me.btnGoalHome = New System.Windows.Forms.Button
        Me.pnlSave = New System.Windows.Forms.Panel
        Me.btnSwitchGames = New System.Windows.Forms.Button
        Me.btnTime = New System.Windows.Forms.Button
        Me.btnDecTime = New System.Windows.Forms.Button
        Me.btnIncTime = New System.Windows.Forms.Button
        Me.btnRefreshTime = New System.Windows.Forms.Button
        Me.picOpticalFeed = New System.Windows.Forms.PictureBox
        Me.txtTimeMod2 = New System.Windows.Forms.MaskedTextBox
        Me.txtTime = New System.Windows.Forms.MaskedTextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnSubstituteAway = New System.Windows.Forms.Button
        Me.btnKeyMomentAway = New System.Windows.Forms.Button
        Me.btnGoalThreatAway = New System.Windows.Forms.Button
        Me.btnGoalKickAway = New System.Windows.Forms.Button
        Me.btnOffSideAway = New System.Windows.Forms.Button
        Me.btnFoulAway = New System.Windows.Forms.Button
        Me.btnCrossAway = New System.Windows.Forms.Button
        Me.btnCornerAway = New System.Windows.Forms.Button
        Me.btnOffTargetAway = New System.Windows.Forms.Button
        Me.btnBookingAway = New System.Windows.Forms.Button
        Me.btnOnTargetAway = New System.Windows.Forms.Button
        Me.btnGoalAway = New System.Windows.Forms.Button
        Me.lvPBP = New System.Windows.Forms.ListView
        Me.EventID = New System.Windows.Forms.ColumnHeader
        Me.Period = New System.Windows.Forms.ColumnHeader
        Me.Team = New System.Windows.Forms.ColumnHeader
        Me.Time = New System.Windows.Forms.ColumnHeader
        Me.Type = New System.Windows.Forms.ColumnHeader
        Me.Player = New System.Windows.Forms.ColumnHeader
        Me.SubType = New System.Windows.Forms.ColumnHeader
        Me.SubSubType = New System.Windows.Forms.ColumnHeader
        Me.Area = New System.Windows.Forms.ColumnHeader
        Me.SequenceNumber = New System.Windows.Forms.ColumnHeader
        Me.EventCodeID = New System.Windows.Forms.ColumnHeader
        Me.UniqueID = New System.Windows.Forms.ColumnHeader
        Me.Team_ID = New System.Windows.Forms.ColumnHeader
        Me.Paired = New System.Windows.Forms.ColumnHeader
        Me.lblSavedEvents = New System.Windows.Forms.Label
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnReplace = New System.Windows.Forms.Button
        Me.btnInsert = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.picOrangeBlink = New System.Windows.Forms.PictureBox
        Me.picBlinkGreen = New System.Windows.Forms.PictureBox
        Me.btnRepeatLast = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.picMark2 = New System.Windows.Forms.PictureBox
        Me.picMark1 = New System.Windows.Forms.PictureBox
        Me.lblSelection2 = New System.Windows.Forms.Label
        Me.lblSelection1 = New System.Windows.Forms.Label
        Me.lblFieldY = New System.Windows.Forms.Label
        Me.lblFieldX = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.pnlChildEvents = New System.Windows.Forms.Panel
        Me.lblEventDetails14 = New System.Windows.Forms.Label
        Me.lblEventDetails13 = New System.Windows.Forms.Label
        Me.dblstEventDetails14 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails13 = New System.Windows.Forms.ListBox
        Me.lblEventDetails12 = New System.Windows.Forms.Label
        Me.dblstEventDetails12 = New System.Windows.Forms.ListBox
        Me.lblEventDetails11 = New System.Windows.Forms.Label
        Me.lblEventDetails10 = New System.Windows.Forms.Label
        Me.lblEventDetails08 = New System.Windows.Forms.Label
        Me.dblstEventDetails11 = New System.Windows.Forms.ListBox
        Me.lblEventDetails06 = New System.Windows.Forms.Label
        Me.lblEventDetails09 = New System.Windows.Forms.Label
        Me.lblEventDetails07 = New System.Windows.Forms.Label
        Me.lblEventDetails05 = New System.Windows.Forms.Label
        Me.lblEventDetails04 = New System.Windows.Forms.Label
        Me.lblEventDetails03 = New System.Windows.Forms.Label
        Me.lblEventDetails02 = New System.Windows.Forms.Label
        Me.lblEventDetails01 = New System.Windows.Forms.Label
        Me.dblstEventDetails10 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails09 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails08 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails07 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails06 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails05 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails04 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails03 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails02 = New System.Windows.Forms.ListBox
        Me.dblstEventDetails01 = New System.Windows.Forms.ListBox
        Me.pnlHome1 = New System.Windows.Forms.Panel
        Me.pnlHome2 = New System.Windows.Forms.Panel
        Me.btnMissPenaltyHome = New System.Windows.Forms.Button
        Me.pnlVisit1 = New System.Windows.Forms.Panel
        Me.pnlVisit2 = New System.Windows.Forms.Panel
        Me.btnMissPenaltyVisit = New System.Windows.Forms.Button
        Me.pnlHome3 = New System.Windows.Forms.Panel
        Me.btnBackpassHome = New System.Windows.Forms.Button
        Me.btn10YDHome = New System.Windows.Forms.Button
        Me.btnThrowInHome = New System.Windows.Forms.Button
        Me.btnFreeKickHome = New System.Windows.Forms.Button
        Me.btnDefActHome = New System.Windows.Forms.Button
        Me.pnlVisit3 = New System.Windows.Forms.Panel
        Me.btnBackpassVisit = New System.Windows.Forms.Button
        Me.btn10YDVisit = New System.Windows.Forms.Button
        Me.btnThrowInVisit = New System.Windows.Forms.Button
        Me.btnFreeKickAway = New System.Windows.Forms.Button
        Me.btnDefActVisit = New System.Windows.Forms.Button
        Me.btnExpand = New System.Windows.Forms.Button
        Me.tlpPBP = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlGoalsLeft = New System.Windows.Forms.Panel
        Me.btnOwnHome = New System.Windows.Forms.Button
        Me.btnNormalHome = New System.Windows.Forms.Button
        Me.btnPenaltyHome = New System.Windows.Forms.Button
        Me.pnlGoalRight = New System.Windows.Forms.Panel
        Me.btnOwnAway = New System.Windows.Forms.Button
        Me.btnNormalAway = New System.Windows.Forms.Button
        Me.btnPenaltyAway = New System.Windows.Forms.Button
        Me.pnlGoalLblLeft = New System.Windows.Forms.Panel
        Me.lblGoalAway = New System.Windows.Forms.Label
        Me.pnlGoalLblRight = New System.Windows.Forms.Panel
        Me.lblGoalHome = New System.Windows.Forms.Label
        Me.UdcSoccerField1 = New STATS.SoccerDataCollection.udcSoccerField
        Me.pnlSave.SuspendLayout()
        CType(Me.picOpticalFeed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.picOrangeBlink, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBlinkGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMark2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMark1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlChildEvents.SuspendLayout()
        Me.pnlHome1.SuspendLayout()
        Me.pnlHome2.SuspendLayout()
        Me.pnlVisit1.SuspendLayout()
        Me.pnlVisit2.SuspendLayout()
        Me.pnlHome3.SuspendLayout()
        Me.pnlVisit3.SuspendLayout()
        Me.pnlGoalsLeft.SuspendLayout()
        Me.pnlGoalRight.SuspendLayout()
        Me.pnlGoalLblLeft.SuspendLayout()
        Me.pnlGoalLblRight.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSubstituteHome
        '
        Me.btnSubstituteHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSubstituteHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubstituteHome.Location = New System.Drawing.Point(0, 62)
        Me.btnSubstituteHome.Name = "btnSubstituteHome"
        Me.btnSubstituteHome.Size = New System.Drawing.Size(91, 26)
        Me.btnSubstituteHome.TabIndex = 2
        Me.btnSubstituteHome.Text = "Substitute"
        Me.btnSubstituteHome.UseVisualStyleBackColor = True
        '
        'btnKeyMomentHome
        '
        Me.btnKeyMomentHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnKeyMomentHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeyMomentHome.Location = New System.Drawing.Point(0, 62)
        Me.btnKeyMomentHome.Name = "btnKeyMomentHome"
        Me.btnKeyMomentHome.Size = New System.Drawing.Size(91, 26)
        Me.btnKeyMomentHome.TabIndex = 2
        Me.btnKeyMomentHome.Text = "Key Mmt."
        Me.btnKeyMomentHome.UseVisualStyleBackColor = True
        '
        'btnGoalThreatHome
        '
        Me.btnGoalThreatHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalThreatHome.Enabled = False
        Me.btnGoalThreatHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalThreatHome.Location = New System.Drawing.Point(0, 155)
        Me.btnGoalThreatHome.Name = "btnGoalThreatHome"
        Me.btnGoalThreatHome.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalThreatHome.TabIndex = 5
        Me.btnGoalThreatHome.Text = "Goal threat"
        Me.btnGoalThreatHome.UseVisualStyleBackColor = True
        '
        'btnGoalKickHome
        '
        Me.btnGoalKickHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalKickHome.Enabled = False
        Me.btnGoalKickHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalKickHome.Location = New System.Drawing.Point(0, 155)
        Me.btnGoalKickHome.Name = "btnGoalKickHome"
        Me.btnGoalKickHome.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalKickHome.TabIndex = 5
        Me.btnGoalKickHome.Text = "Goal Kick"
        Me.btnGoalKickHome.UseVisualStyleBackColor = True
        '
        'btnOffSideHome
        '
        Me.btnOffSideHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOffSideHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOffSideHome.Location = New System.Drawing.Point(0, 124)
        Me.btnOffSideHome.Name = "btnOffSideHome"
        Me.btnOffSideHome.Size = New System.Drawing.Size(91, 26)
        Me.btnOffSideHome.TabIndex = 4
        Me.btnOffSideHome.Text = "Offside"
        Me.btnOffSideHome.UseVisualStyleBackColor = True
        '
        'btnFoulHome
        '
        Me.btnFoulHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFoulHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFoulHome.Location = New System.Drawing.Point(0, 124)
        Me.btnFoulHome.Name = "btnFoulHome"
        Me.btnFoulHome.Size = New System.Drawing.Size(91, 26)
        Me.btnFoulHome.TabIndex = 4
        Me.btnFoulHome.Text = "Foul"
        Me.btnFoulHome.UseVisualStyleBackColor = True
        '
        'btnCrossHome
        '
        Me.btnCrossHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCrossHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCrossHome.Location = New System.Drawing.Point(0, 93)
        Me.btnCrossHome.Name = "btnCrossHome"
        Me.btnCrossHome.Size = New System.Drawing.Size(91, 26)
        Me.btnCrossHome.TabIndex = 3
        Me.btnCrossHome.Text = "Cross"
        Me.btnCrossHome.UseVisualStyleBackColor = True
        '
        'btnCornerHome
        '
        Me.btnCornerHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCornerHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCornerHome.Location = New System.Drawing.Point(0, 62)
        Me.btnCornerHome.Name = "btnCornerHome"
        Me.btnCornerHome.Size = New System.Drawing.Size(91, 26)
        Me.btnCornerHome.TabIndex = 2
        Me.btnCornerHome.Text = "Corner"
        Me.btnCornerHome.UseVisualStyleBackColor = True
        '
        'btnOffTargetHome
        '
        Me.btnOffTargetHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOffTargetHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOffTargetHome.Location = New System.Drawing.Point(0, 31)
        Me.btnOffTargetHome.Name = "btnOffTargetHome"
        Me.btnOffTargetHome.Size = New System.Drawing.Size(91, 26)
        Me.btnOffTargetHome.TabIndex = 1
        Me.btnOffTargetHome.Text = "Off Target"
        Me.btnOffTargetHome.UseVisualStyleBackColor = True
        '
        'btnBookingHome
        '
        Me.btnBookingHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnBookingHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnBookingHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBookingHome.Location = New System.Drawing.Point(0, 31)
        Me.btnBookingHome.Name = "btnBookingHome"
        Me.btnBookingHome.Size = New System.Drawing.Size(91, 26)
        Me.btnBookingHome.TabIndex = 1
        Me.btnBookingHome.Text = "Booking"
        Me.btnBookingHome.UseVisualStyleBackColor = False
        '
        'btnOnTargetHome
        '
        Me.btnOnTargetHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOnTargetHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOnTargetHome.Location = New System.Drawing.Point(0, 0)
        Me.btnOnTargetHome.Name = "btnOnTargetHome"
        Me.btnOnTargetHome.Size = New System.Drawing.Size(91, 26)
        Me.btnOnTargetHome.TabIndex = 0
        Me.btnOnTargetHome.Text = "On Target"
        Me.btnOnTargetHome.UseVisualStyleBackColor = True
        '
        'btnGoalHome
        '
        Me.btnGoalHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGoalHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalHome.Location = New System.Drawing.Point(0, 0)
        Me.btnGoalHome.Name = "btnGoalHome"
        Me.btnGoalHome.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalHome.TabIndex = 0
        Me.btnGoalHome.Text = "Goal"
        Me.btnGoalHome.UseVisualStyleBackColor = False
        Me.btnGoalHome.Visible = False
        '
        'pnlSave
        '
        Me.pnlSave.BackColor = System.Drawing.Color.Lavender
        Me.pnlSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSave.Controls.Add(Me.btnSwitchGames)
        Me.pnlSave.Controls.Add(Me.btnTime)
        Me.pnlSave.Controls.Add(Me.btnDecTime)
        Me.pnlSave.Controls.Add(Me.btnIncTime)
        Me.pnlSave.Controls.Add(Me.btnRefreshTime)
        Me.pnlSave.Controls.Add(Me.picOpticalFeed)
        Me.pnlSave.Controls.Add(Me.txtTimeMod2)
        Me.pnlSave.Controls.Add(Me.txtTime)
        Me.pnlSave.Controls.Add(Me.btnCancel)
        Me.pnlSave.Controls.Add(Me.btnSave)
        Me.pnlSave.Location = New System.Drawing.Point(868, 221)
        Me.pnlSave.Name = "pnlSave"
        Me.pnlSave.Size = New System.Drawing.Size(124, 206)
        Me.pnlSave.TabIndex = 7
        '
        'btnSwitchGames
        '
        Me.btnSwitchGames.BackColor = System.Drawing.Color.DimGray
        Me.btnSwitchGames.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSwitchGames.ForeColor = System.Drawing.Color.White
        Me.btnSwitchGames.Location = New System.Drawing.Point(26, 5)
        Me.btnSwitchGames.Name = "btnSwitchGames"
        Me.btnSwitchGames.Size = New System.Drawing.Size(95, 27)
        Me.btnSwitchGames.TabIndex = 300
        Me.btnSwitchGames.Text = "Switch Games"
        Me.btnSwitchGames.UseVisualStyleBackColor = False
        Me.btnSwitchGames.Visible = False
        '
        'btnTime
        '
        Me.btnTime.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.btnTime.Font = New System.Drawing.Font("Arial Unicode MS", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTime.ForeColor = System.Drawing.Color.PeachPuff
        Me.btnTime.Location = New System.Drawing.Point(35, 34)
        Me.btnTime.Name = "btnTime"
        Me.btnTime.Size = New System.Drawing.Size(75, 56)
        Me.btnTime.TabIndex = 258
        Me.btnTime.Text = "Time"
        Me.btnTime.UseVisualStyleBackColor = False
        Me.btnTime.Visible = False
        '
        'btnDecTime
        '
        Me.btnDecTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.time_down
        Me.btnDecTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDecTime.Location = New System.Drawing.Point(97, 108)
        Me.btnDecTime.Name = "btnDecTime"
        Me.btnDecTime.Size = New System.Drawing.Size(24, 24)
        Me.btnDecTime.TabIndex = 257
        Me.btnDecTime.UseVisualStyleBackColor = True
        '
        'btnIncTime
        '
        Me.btnIncTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.time_up
        Me.btnIncTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnIncTime.Location = New System.Drawing.Point(74, 108)
        Me.btnIncTime.Name = "btnIncTime"
        Me.btnIncTime.Size = New System.Drawing.Size(24, 24)
        Me.btnIncTime.TabIndex = 256
        Me.btnIncTime.UseVisualStyleBackColor = True
        '
        'btnRefreshTime
        '
        Me.btnRefreshTime.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Refresh_btn
        Me.btnRefreshTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefreshTime.Location = New System.Drawing.Point(1, 108)
        Me.btnRefreshTime.Name = "btnRefreshTime"
        Me.btnRefreshTime.Size = New System.Drawing.Size(24, 24)
        Me.btnRefreshTime.TabIndex = 4
        Me.btnRefreshTime.UseVisualStyleBackColor = True
        '
        'picOpticalFeed
        '
        Me.picOpticalFeed.Location = New System.Drawing.Point(20, -1)
        Me.picOpticalFeed.Name = "picOpticalFeed"
        Me.picOpticalFeed.Size = New System.Drawing.Size(92, 91)
        Me.picOpticalFeed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picOpticalFeed.TabIndex = 255
        Me.picOpticalFeed.TabStop = False
        '
        'txtTimeMod2
        '
        Me.txtTimeMod2.Location = New System.Drawing.Point(61, 106)
        Me.txtTimeMod2.Mask = "000"
        Me.txtTimeMod2.Name = "txtTimeMod2"
        Me.txtTimeMod2.Size = New System.Drawing.Size(24, 22)
        Me.txtTimeMod2.TabIndex = 0
        '
        'txtTime
        '
        Me.txtTime.Location = New System.Drawing.Point(26, 109)
        Me.txtTime.Mask = "000:00"
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(47, 22)
        Me.txtTime.TabIndex = 3
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(37, 145)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(37, 172)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnSubstituteAway
        '
        Me.btnSubstituteAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSubstituteAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubstituteAway.Location = New System.Drawing.Point(0, 62)
        Me.btnSubstituteAway.Name = "btnSubstituteAway"
        Me.btnSubstituteAway.Size = New System.Drawing.Size(91, 26)
        Me.btnSubstituteAway.TabIndex = 2
        Me.btnSubstituteAway.Text = "Substitute"
        Me.btnSubstituteAway.UseVisualStyleBackColor = True
        '
        'btnKeyMomentAway
        '
        Me.btnKeyMomentAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnKeyMomentAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeyMomentAway.Location = New System.Drawing.Point(0, 62)
        Me.btnKeyMomentAway.Name = "btnKeyMomentAway"
        Me.btnKeyMomentAway.Size = New System.Drawing.Size(91, 26)
        Me.btnKeyMomentAway.TabIndex = 2
        Me.btnKeyMomentAway.Text = "Key Mmt."
        Me.btnKeyMomentAway.UseVisualStyleBackColor = True
        '
        'btnGoalThreatAway
        '
        Me.btnGoalThreatAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalThreatAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalThreatAway.Location = New System.Drawing.Point(0, 155)
        Me.btnGoalThreatAway.Name = "btnGoalThreatAway"
        Me.btnGoalThreatAway.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalThreatAway.TabIndex = 5
        Me.btnGoalThreatAway.Text = "Goal threat"
        Me.btnGoalThreatAway.UseVisualStyleBackColor = True
        '
        'btnGoalKickAway
        '
        Me.btnGoalKickAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalKickAway.Enabled = False
        Me.btnGoalKickAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalKickAway.Location = New System.Drawing.Point(0, 155)
        Me.btnGoalKickAway.Name = "btnGoalKickAway"
        Me.btnGoalKickAway.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalKickAway.TabIndex = 5
        Me.btnGoalKickAway.Text = "Goal Kick"
        Me.btnGoalKickAway.UseVisualStyleBackColor = True
        '
        'btnOffSideAway
        '
        Me.btnOffSideAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOffSideAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOffSideAway.Location = New System.Drawing.Point(0, 124)
        Me.btnOffSideAway.Name = "btnOffSideAway"
        Me.btnOffSideAway.Size = New System.Drawing.Size(91, 26)
        Me.btnOffSideAway.TabIndex = 4
        Me.btnOffSideAway.Text = "Offside"
        Me.btnOffSideAway.UseVisualStyleBackColor = True
        '
        'btnFoulAway
        '
        Me.btnFoulAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFoulAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFoulAway.Location = New System.Drawing.Point(0, 124)
        Me.btnFoulAway.Name = "btnFoulAway"
        Me.btnFoulAway.Size = New System.Drawing.Size(91, 26)
        Me.btnFoulAway.TabIndex = 4
        Me.btnFoulAway.Text = "Foul"
        Me.btnFoulAway.UseVisualStyleBackColor = True
        '
        'btnCrossAway
        '
        Me.btnCrossAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCrossAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCrossAway.Location = New System.Drawing.Point(0, 93)
        Me.btnCrossAway.Name = "btnCrossAway"
        Me.btnCrossAway.Size = New System.Drawing.Size(91, 26)
        Me.btnCrossAway.TabIndex = 3
        Me.btnCrossAway.Text = "Cross"
        Me.btnCrossAway.UseVisualStyleBackColor = True
        '
        'btnCornerAway
        '
        Me.btnCornerAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCornerAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCornerAway.Location = New System.Drawing.Point(0, 62)
        Me.btnCornerAway.Name = "btnCornerAway"
        Me.btnCornerAway.Size = New System.Drawing.Size(91, 26)
        Me.btnCornerAway.TabIndex = 2
        Me.btnCornerAway.Text = "Corner"
        Me.btnCornerAway.UseVisualStyleBackColor = True
        '
        'btnOffTargetAway
        '
        Me.btnOffTargetAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOffTargetAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOffTargetAway.Location = New System.Drawing.Point(0, 31)
        Me.btnOffTargetAway.Name = "btnOffTargetAway"
        Me.btnOffTargetAway.Size = New System.Drawing.Size(91, 26)
        Me.btnOffTargetAway.TabIndex = 1
        Me.btnOffTargetAway.Text = "Off Target"
        Me.btnOffTargetAway.UseVisualStyleBackColor = True
        '
        'btnBookingAway
        '
        Me.btnBookingAway.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnBookingAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnBookingAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBookingAway.Location = New System.Drawing.Point(0, 31)
        Me.btnBookingAway.Name = "btnBookingAway"
        Me.btnBookingAway.Size = New System.Drawing.Size(91, 26)
        Me.btnBookingAway.TabIndex = 1
        Me.btnBookingAway.Text = "Booking"
        Me.btnBookingAway.UseVisualStyleBackColor = False
        '
        'btnOnTargetAway
        '
        Me.btnOnTargetAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOnTargetAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOnTargetAway.Location = New System.Drawing.Point(0, 0)
        Me.btnOnTargetAway.Name = "btnOnTargetAway"
        Me.btnOnTargetAway.Size = New System.Drawing.Size(91, 26)
        Me.btnOnTargetAway.TabIndex = 0
        Me.btnOnTargetAway.Text = "On Target"
        Me.btnOnTargetAway.UseVisualStyleBackColor = True
        '
        'btnGoalAway
        '
        Me.btnGoalAway.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGoalAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGoalAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoalAway.Location = New System.Drawing.Point(0, 0)
        Me.btnGoalAway.Name = "btnGoalAway"
        Me.btnGoalAway.Size = New System.Drawing.Size(91, 26)
        Me.btnGoalAway.TabIndex = 0
        Me.btnGoalAway.Text = "Goal"
        Me.btnGoalAway.UseVisualStyleBackColor = False
        Me.btnGoalAway.Visible = False
        '
        'lvPBP
        '
        Me.lvPBP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.EventID, Me.Period, Me.Team, Me.Time, Me.Type, Me.Player, Me.SubType, Me.SubSubType, Me.Area, Me.SequenceNumber, Me.EventCodeID, Me.UniqueID, Me.Team_ID, Me.Paired})
        Me.lvPBP.FullRowSelect = True
        ListViewItem1.Tag = "Event"
        Me.lvPBP.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6, ListViewItem7, ListViewItem8, ListViewItem9, ListViewItem10, ListViewItem11, ListViewItem12, ListViewItem13})
        Me.lvPBP.Location = New System.Drawing.Point(12, 447)
        Me.lvPBP.MultiSelect = False
        Me.lvPBP.Name = "lvPBP"
        Me.lvPBP.Size = New System.Drawing.Size(857, 89)
        Me.lvPBP.TabIndex = 9
        Me.lvPBP.UseCompatibleStateImageBehavior = False
        Me.lvPBP.View = System.Windows.Forms.View.Details
        '
        'EventID
        '
        Me.EventID.Text = "Event"
        Me.EventID.Width = 125
        '
        'Period
        '
        Me.Period.Text = "Period"
        Me.Period.Width = 48
        '
        'Team
        '
        Me.Team.Text = "Team"
        Me.Team.Width = 50
        '
        'Time
        '
        Me.Time.Text = "Time"
        '
        'Type
        '
        Me.Type.Text = "Type"
        Me.Type.Width = 80
        '
        'Player
        '
        Me.Player.Text = "By Player"
        Me.Player.Width = 110
        '
        'SubType
        '
        Me.SubType.Text = "Sub Type"
        Me.SubType.Width = 110
        '
        'SubSubType
        '
        Me.SubSubType.Text = "Sub Sub Type"
        Me.SubSubType.Width = 112
        '
        'Area
        '
        Me.Area.DisplayIndex = 9
        Me.Area.Text = "Area"
        Me.Area.Width = 82
        '
        'SequenceNumber
        '
        Me.SequenceNumber.DisplayIndex = 8
        Me.SequenceNumber.Text = "SequenceNumber"
        Me.SequenceNumber.Width = 0
        '
        'EventCodeID
        '
        Me.EventCodeID.Text = "EventCodeID"
        Me.EventCodeID.Width = 0
        '
        'UniqueID
        '
        Me.UniqueID.Text = "UniqueID"
        Me.UniqueID.Width = 0
        '
        'Team_ID
        '
        Me.Team_ID.Text = "Team_ID"
        Me.Team_ID.Width = 0
        '
        'Paired
        '
        Me.Paired.Text = "Paired"
        Me.Paired.Width = 49
        '
        'lblSavedEvents
        '
        Me.lblSavedEvents.AutoSize = True
        Me.lblSavedEvents.Location = New System.Drawing.Point(9, 431)
        Me.lblSavedEvents.Name = "lblSavedEvents"
        Me.lblSavedEvents.Size = New System.Drawing.Size(77, 15)
        Me.lblSavedEvents.TabIndex = 8
        Me.lblSavedEvents.Text = "Saved events:"
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(906, 513)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 14
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnReplace
        '
        Me.btnReplace.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnReplace.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnReplace.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReplace.ForeColor = System.Drawing.Color.Black
        Me.btnReplace.Location = New System.Drawing.Point(906, 486)
        Me.btnReplace.Name = "btnReplace"
        Me.btnReplace.Size = New System.Drawing.Size(75, 25)
        Me.btnReplace.TabIndex = 13
        Me.btnReplace.Text = "Replace"
        Me.btnReplace.UseVisualStyleBackColor = False
        '
        'btnInsert
        '
        Me.btnInsert.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnInsert.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnInsert.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInsert.ForeColor = System.Drawing.Color.Black
        Me.btnInsert.Location = New System.Drawing.Point(906, 459)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(75, 25)
        Me.btnInsert.TabIndex = 12
        Me.btnInsert.Text = "Insert"
        Me.btnInsert.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(112, Byte), Integer), CType(CType(151, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.btnEdit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnEdit.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(906, 432)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 25)
        Me.btnEdit.TabIndex = 11
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.picOrangeBlink)
        Me.Panel1.Controls.Add(Me.picBlinkGreen)
        Me.Panel1.Controls.Add(Me.btnRepeatLast)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.picMark2)
        Me.Panel1.Controls.Add(Me.picMark1)
        Me.Panel1.Controls.Add(Me.lblSelection2)
        Me.Panel1.Controls.Add(Me.lblSelection1)
        Me.Panel1.Controls.Add(Me.lblFieldY)
        Me.Panel1.Controls.Add(Me.lblFieldX)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(303, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(52, 221)
        Me.Panel1.TabIndex = 15
        '
        'picOrangeBlink
        '
        Me.picOrangeBlink.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePinBlink
        Me.picOrangeBlink.Location = New System.Drawing.Point(17, 159)
        Me.picOrangeBlink.Name = "picOrangeBlink"
        Me.picOrangeBlink.Size = New System.Drawing.Size(14, 15)
        Me.picOrangeBlink.TabIndex = 14
        Me.picOrangeBlink.TabStop = False
        '
        'picBlinkGreen
        '
        Me.picBlinkGreen.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPinBlink
        Me.picBlinkGreen.Location = New System.Drawing.Point(17, 124)
        Me.picBlinkGreen.Name = "picBlinkGreen"
        Me.picBlinkGreen.Size = New System.Drawing.Size(14, 13)
        Me.picBlinkGreen.TabIndex = 13
        Me.picBlinkGreen.TabStop = False
        '
        'btnRepeatLast
        '
        Me.btnRepeatLast.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.RepeatLast
        Me.btnRepeatLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRepeatLast.Location = New System.Drawing.Point(12, 192)
        Me.btnRepeatLast.Name = "btnRepeatLast"
        Me.btnRepeatLast.Size = New System.Drawing.Size(24, 24)
        Me.btnRepeatLast.TabIndex = 8
        Me.btnRepeatLast.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "70m"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "X"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "105m"
        '
        'picMark2
        '
        Me.picMark2.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.OrangePin
        Me.picMark2.Location = New System.Drawing.Point(17, 160)
        Me.picMark2.Name = "picMark2"
        Me.picMark2.Size = New System.Drawing.Size(14, 13)
        Me.picMark2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMark2.TabIndex = 12
        Me.picMark2.TabStop = False
        '
        'picMark1
        '
        Me.picMark1.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.GreenPin
        Me.picMark1.Location = New System.Drawing.Point(17, 124)
        Me.picMark1.Name = "picMark1"
        Me.picMark1.Size = New System.Drawing.Size(14, 13)
        Me.picMark1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMark1.TabIndex = 11
        Me.picMark1.TabStop = False
        '
        'lblSelection2
        '
        Me.lblSelection2.Location = New System.Drawing.Point(2, 176)
        Me.lblSelection2.Name = "lblSelection2"
        Me.lblSelection2.Size = New System.Drawing.Size(45, 15)
        Me.lblSelection2.TabIndex = 7
        Me.lblSelection2.Text = "-"
        Me.lblSelection2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblSelection1
        '
        Me.lblSelection1.Location = New System.Drawing.Point(-1, 138)
        Me.lblSelection1.Name = "lblSelection1"
        Me.lblSelection1.Size = New System.Drawing.Size(52, 15)
        Me.lblSelection1.TabIndex = 6
        Me.lblSelection1.Text = "-"
        Me.lblSelection1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFieldY
        '
        Me.lblFieldY.AutoSize = True
        Me.lblFieldY.Location = New System.Drawing.Point(6, 95)
        Me.lblFieldY.Name = "lblFieldY"
        Me.lblFieldY.Size = New System.Drawing.Size(17, 15)
        Me.lblFieldY.TabIndex = 5
        Me.lblFieldY.Text = "Y:"
        '
        'lblFieldX
        '
        Me.lblFieldX.AutoSize = True
        Me.lblFieldX.Location = New System.Drawing.Point(6, 80)
        Me.lblFieldX.Name = "lblFieldX"
        Me.lblFieldX.Size = New System.Drawing.Size(17, 15)
        Me.lblFieldX.TabIndex = 4
        Me.lblFieldX.Text = "X:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Field"
        '
        'pnlChildEvents
        '
        Me.pnlChildEvents.AutoScroll = True
        Me.pnlChildEvents.BackColor = System.Drawing.Color.Lavender
        Me.pnlChildEvents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails14)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails13)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails14)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails13)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails12)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails12)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails11)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails10)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails08)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails11)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails06)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails09)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails07)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails05)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails04)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails03)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails02)
        Me.pnlChildEvents.Controls.Add(Me.lblEventDetails01)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails10)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails09)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails08)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails07)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails06)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails05)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails04)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails03)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails02)
        Me.pnlChildEvents.Controls.Add(Me.dblstEventDetails01)
        Me.pnlChildEvents.Location = New System.Drawing.Point(0, 221)
        Me.pnlChildEvents.Name = "pnlChildEvents"
        Me.pnlChildEvents.Size = New System.Drawing.Size(869, 206)
        Me.pnlChildEvents.TabIndex = 6
        '
        'lblEventDetails14
        '
        Me.lblEventDetails14.AutoSize = True
        Me.lblEventDetails14.Location = New System.Drawing.Point(1641, 1)
        Me.lblEventDetails14.Name = "lblEventDetails14"
        Me.lblEventDetails14.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails14.TabIndex = 26
        Me.lblEventDetails14.Text = "lblEventDetails14"
        Me.lblEventDetails14.Visible = False
        '
        'lblEventDetails13
        '
        Me.lblEventDetails13.AutoSize = True
        Me.lblEventDetails13.Location = New System.Drawing.Point(1518, 1)
        Me.lblEventDetails13.Name = "lblEventDetails13"
        Me.lblEventDetails13.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails13.TabIndex = 25
        Me.lblEventDetails13.Text = "lblEventDetails13"
        Me.lblEventDetails13.Visible = False
        '
        'dblstEventDetails14
        '
        Me.dblstEventDetails14.FormattingEnabled = True
        Me.dblstEventDetails14.ItemHeight = 15
        Me.dblstEventDetails14.Location = New System.Drawing.Point(1642, 16)
        Me.dblstEventDetails14.Name = "dblstEventDetails14"
        Me.dblstEventDetails14.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails14.TabIndex = 24
        Me.dblstEventDetails14.Visible = False
        '
        'dblstEventDetails13
        '
        Me.dblstEventDetails13.FormattingEnabled = True
        Me.dblstEventDetails13.ItemHeight = 15
        Me.dblstEventDetails13.Location = New System.Drawing.Point(1515, 16)
        Me.dblstEventDetails13.Name = "dblstEventDetails13"
        Me.dblstEventDetails13.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails13.TabIndex = 23
        Me.dblstEventDetails13.Visible = False
        '
        'lblEventDetails12
        '
        Me.lblEventDetails12.AutoSize = True
        Me.lblEventDetails12.Location = New System.Drawing.Point(1389, 1)
        Me.lblEventDetails12.Name = "lblEventDetails12"
        Me.lblEventDetails12.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails12.TabIndex = 11
        Me.lblEventDetails12.Text = "lblEventDetails12"
        Me.lblEventDetails12.Visible = False
        '
        'dblstEventDetails12
        '
        Me.dblstEventDetails12.FormattingEnabled = True
        Me.dblstEventDetails12.ItemHeight = 15
        Me.dblstEventDetails12.Location = New System.Drawing.Point(1389, 16)
        Me.dblstEventDetails12.Name = "dblstEventDetails12"
        Me.dblstEventDetails12.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails12.TabIndex = 22
        Me.dblstEventDetails12.Visible = False
        '
        'lblEventDetails11
        '
        Me.lblEventDetails11.AutoSize = True
        Me.lblEventDetails11.Location = New System.Drawing.Point(1263, 1)
        Me.lblEventDetails11.Name = "lblEventDetails11"
        Me.lblEventDetails11.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails11.TabIndex = 10
        Me.lblEventDetails11.Text = "lblEventDetails11"
        Me.lblEventDetails11.Visible = False
        '
        'lblEventDetails10
        '
        Me.lblEventDetails10.AutoSize = True
        Me.lblEventDetails10.Location = New System.Drawing.Point(1137, 1)
        Me.lblEventDetails10.Name = "lblEventDetails10"
        Me.lblEventDetails10.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails10.TabIndex = 9
        Me.lblEventDetails10.Text = "lblEventDetails10"
        Me.lblEventDetails10.Visible = False
        '
        'lblEventDetails08
        '
        Me.lblEventDetails08.AutoSize = True
        Me.lblEventDetails08.Location = New System.Drawing.Point(885, 1)
        Me.lblEventDetails08.Name = "lblEventDetails08"
        Me.lblEventDetails08.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails08.TabIndex = 7
        Me.lblEventDetails08.Text = "lblEventDetails08"
        Me.lblEventDetails08.Visible = False
        '
        'dblstEventDetails11
        '
        Me.dblstEventDetails11.FormattingEnabled = True
        Me.dblstEventDetails11.ItemHeight = 15
        Me.dblstEventDetails11.Location = New System.Drawing.Point(1263, 16)
        Me.dblstEventDetails11.Name = "dblstEventDetails11"
        Me.dblstEventDetails11.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails11.TabIndex = 21
        Me.dblstEventDetails11.Visible = False
        '
        'lblEventDetails06
        '
        Me.lblEventDetails06.AutoSize = True
        Me.lblEventDetails06.Location = New System.Drawing.Point(633, 1)
        Me.lblEventDetails06.Name = "lblEventDetails06"
        Me.lblEventDetails06.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails06.TabIndex = 5
        Me.lblEventDetails06.Text = "lblEventDetails06"
        Me.lblEventDetails06.Visible = False
        '
        'lblEventDetails09
        '
        Me.lblEventDetails09.AutoSize = True
        Me.lblEventDetails09.Location = New System.Drawing.Point(1011, 1)
        Me.lblEventDetails09.Name = "lblEventDetails09"
        Me.lblEventDetails09.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails09.TabIndex = 8
        Me.lblEventDetails09.Text = "lblEventDetails09"
        Me.lblEventDetails09.Visible = False
        '
        'lblEventDetails07
        '
        Me.lblEventDetails07.AutoSize = True
        Me.lblEventDetails07.Location = New System.Drawing.Point(759, 1)
        Me.lblEventDetails07.Name = "lblEventDetails07"
        Me.lblEventDetails07.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails07.TabIndex = 6
        Me.lblEventDetails07.Text = "lblEventDetails07"
        Me.lblEventDetails07.Visible = False
        '
        'lblEventDetails05
        '
        Me.lblEventDetails05.AutoSize = True
        Me.lblEventDetails05.Location = New System.Drawing.Point(507, 1)
        Me.lblEventDetails05.Name = "lblEventDetails05"
        Me.lblEventDetails05.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails05.TabIndex = 4
        Me.lblEventDetails05.Text = "lblEventDetails05"
        Me.lblEventDetails05.Visible = False
        '
        'lblEventDetails04
        '
        Me.lblEventDetails04.AutoSize = True
        Me.lblEventDetails04.Location = New System.Drawing.Point(381, 1)
        Me.lblEventDetails04.Name = "lblEventDetails04"
        Me.lblEventDetails04.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails04.TabIndex = 3
        Me.lblEventDetails04.Text = "lblEventDetails04"
        Me.lblEventDetails04.Visible = False
        '
        'lblEventDetails03
        '
        Me.lblEventDetails03.AutoSize = True
        Me.lblEventDetails03.Location = New System.Drawing.Point(255, 1)
        Me.lblEventDetails03.Name = "lblEventDetails03"
        Me.lblEventDetails03.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails03.TabIndex = 2
        Me.lblEventDetails03.Text = "lblEventDetails03"
        Me.lblEventDetails03.Visible = False
        '
        'lblEventDetails02
        '
        Me.lblEventDetails02.AutoSize = True
        Me.lblEventDetails02.Location = New System.Drawing.Point(129, 1)
        Me.lblEventDetails02.Name = "lblEventDetails02"
        Me.lblEventDetails02.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails02.TabIndex = 1
        Me.lblEventDetails02.Text = "lblEventDetails02"
        Me.lblEventDetails02.Visible = False
        '
        'lblEventDetails01
        '
        Me.lblEventDetails01.AutoSize = True
        Me.lblEventDetails01.Location = New System.Drawing.Point(3, 1)
        Me.lblEventDetails01.Name = "lblEventDetails01"
        Me.lblEventDetails01.Size = New System.Drawing.Size(90, 15)
        Me.lblEventDetails01.TabIndex = 0
        Me.lblEventDetails01.Text = "lblEventDetails01"
        Me.lblEventDetails01.Visible = False
        '
        'dblstEventDetails10
        '
        Me.dblstEventDetails10.FormattingEnabled = True
        Me.dblstEventDetails10.ItemHeight = 15
        Me.dblstEventDetails10.Location = New System.Drawing.Point(1137, 16)
        Me.dblstEventDetails10.Name = "dblstEventDetails10"
        Me.dblstEventDetails10.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails10.TabIndex = 20
        Me.dblstEventDetails10.Visible = False
        '
        'dblstEventDetails09
        '
        Me.dblstEventDetails09.FormattingEnabled = True
        Me.dblstEventDetails09.ItemHeight = 15
        Me.dblstEventDetails09.Location = New System.Drawing.Point(1011, 16)
        Me.dblstEventDetails09.Name = "dblstEventDetails09"
        Me.dblstEventDetails09.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails09.TabIndex = 19
        Me.dblstEventDetails09.Visible = False
        '
        'dblstEventDetails08
        '
        Me.dblstEventDetails08.FormattingEnabled = True
        Me.dblstEventDetails08.ItemHeight = 15
        Me.dblstEventDetails08.Location = New System.Drawing.Point(885, 16)
        Me.dblstEventDetails08.Name = "dblstEventDetails08"
        Me.dblstEventDetails08.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails08.TabIndex = 18
        Me.dblstEventDetails08.Visible = False
        '
        'dblstEventDetails07
        '
        Me.dblstEventDetails07.FormattingEnabled = True
        Me.dblstEventDetails07.ItemHeight = 15
        Me.dblstEventDetails07.Location = New System.Drawing.Point(759, 16)
        Me.dblstEventDetails07.Name = "dblstEventDetails07"
        Me.dblstEventDetails07.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails07.TabIndex = 17
        Me.dblstEventDetails07.Visible = False
        '
        'dblstEventDetails06
        '
        Me.dblstEventDetails06.FormattingEnabled = True
        Me.dblstEventDetails06.ItemHeight = 15
        Me.dblstEventDetails06.Location = New System.Drawing.Point(633, 16)
        Me.dblstEventDetails06.Name = "dblstEventDetails06"
        Me.dblstEventDetails06.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails06.TabIndex = 16
        Me.dblstEventDetails06.Visible = False
        '
        'dblstEventDetails05
        '
        Me.dblstEventDetails05.FormattingEnabled = True
        Me.dblstEventDetails05.ItemHeight = 15
        Me.dblstEventDetails05.Location = New System.Drawing.Point(507, 16)
        Me.dblstEventDetails05.Name = "dblstEventDetails05"
        Me.dblstEventDetails05.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails05.TabIndex = 15
        Me.dblstEventDetails05.Visible = False
        '
        'dblstEventDetails04
        '
        Me.dblstEventDetails04.FormattingEnabled = True
        Me.dblstEventDetails04.ItemHeight = 15
        Me.dblstEventDetails04.Location = New System.Drawing.Point(381, 16)
        Me.dblstEventDetails04.Name = "dblstEventDetails04"
        Me.dblstEventDetails04.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails04.TabIndex = 14
        Me.dblstEventDetails04.Visible = False
        '
        'dblstEventDetails03
        '
        Me.dblstEventDetails03.FormattingEnabled = True
        Me.dblstEventDetails03.ItemHeight = 15
        Me.dblstEventDetails03.Location = New System.Drawing.Point(255, 16)
        Me.dblstEventDetails03.Name = "dblstEventDetails03"
        Me.dblstEventDetails03.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails03.TabIndex = 13
        Me.dblstEventDetails03.Visible = False
        '
        'dblstEventDetails02
        '
        Me.dblstEventDetails02.FormattingEnabled = True
        Me.dblstEventDetails02.ItemHeight = 15
        Me.dblstEventDetails02.Location = New System.Drawing.Point(129, 16)
        Me.dblstEventDetails02.Name = "dblstEventDetails02"
        Me.dblstEventDetails02.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails02.TabIndex = 12
        Me.dblstEventDetails02.Visible = False
        '
        'dblstEventDetails01
        '
        Me.dblstEventDetails01.FormattingEnabled = True
        Me.dblstEventDetails01.ItemHeight = 15
        Me.dblstEventDetails01.Location = New System.Drawing.Point(3, 16)
        Me.dblstEventDetails01.Name = "dblstEventDetails01"
        Me.dblstEventDetails01.Size = New System.Drawing.Size(120, 169)
        Me.dblstEventDetails01.TabIndex = 11
        Me.dblstEventDetails01.Visible = False
        '
        'pnlHome1
        '
        Me.pnlHome1.Controls.Add(Me.btnOnTargetHome)
        Me.pnlHome1.Controls.Add(Me.btnOffTargetHome)
        Me.pnlHome1.Controls.Add(Me.btnCornerHome)
        Me.pnlHome1.Controls.Add(Me.btnCrossHome)
        Me.pnlHome1.Controls.Add(Me.btnOffSideHome)
        Me.pnlHome1.Controls.Add(Me.btnGoalThreatHome)
        Me.pnlHome1.Location = New System.Drawing.Point(12, 36)
        Me.pnlHome1.Name = "pnlHome1"
        Me.pnlHome1.Size = New System.Drawing.Size(91, 181)
        Me.pnlHome1.TabIndex = 0
        '
        'pnlHome2
        '
        Me.pnlHome2.Controls.Add(Me.btnGoalHome)
        Me.pnlHome2.Controls.Add(Me.btnMissPenaltyHome)
        Me.pnlHome2.Controls.Add(Me.btnBookingHome)
        Me.pnlHome2.Controls.Add(Me.btnFoulHome)
        Me.pnlHome2.Controls.Add(Me.btnGoalKickHome)
        Me.pnlHome2.Controls.Add(Me.btnSubstituteHome)
        Me.pnlHome2.Location = New System.Drawing.Point(109, 36)
        Me.pnlHome2.Name = "pnlHome2"
        Me.pnlHome2.Size = New System.Drawing.Size(91, 181)
        Me.pnlHome2.TabIndex = 1
        '
        'btnMissPenaltyHome
        '
        Me.btnMissPenaltyHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnMissPenaltyHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnMissPenaltyHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMissPenaltyHome.Location = New System.Drawing.Point(0, 93)
        Me.btnMissPenaltyHome.Name = "btnMissPenaltyHome"
        Me.btnMissPenaltyHome.Size = New System.Drawing.Size(91, 26)
        Me.btnMissPenaltyHome.TabIndex = 3
        Me.btnMissPenaltyHome.Text = "Miss Pen"
        Me.btnMissPenaltyHome.UseVisualStyleBackColor = False
        '
        'pnlVisit1
        '
        Me.pnlVisit1.Controls.Add(Me.btnOnTargetAway)
        Me.pnlVisit1.Controls.Add(Me.btnOffTargetAway)
        Me.pnlVisit1.Controls.Add(Me.btnCornerAway)
        Me.pnlVisit1.Controls.Add(Me.btnCrossAway)
        Me.pnlVisit1.Controls.Add(Me.btnOffSideAway)
        Me.pnlVisit1.Controls.Add(Me.btnGoalThreatAway)
        Me.pnlVisit1.Location = New System.Drawing.Point(694, 36)
        Me.pnlVisit1.Name = "pnlVisit1"
        Me.pnlVisit1.Size = New System.Drawing.Size(91, 181)
        Me.pnlVisit1.TabIndex = 3
        '
        'pnlVisit2
        '
        Me.pnlVisit2.Controls.Add(Me.btnGoalAway)
        Me.pnlVisit2.Controls.Add(Me.btnMissPenaltyVisit)
        Me.pnlVisit2.Controls.Add(Me.btnFoulAway)
        Me.pnlVisit2.Controls.Add(Me.btnGoalKickAway)
        Me.pnlVisit2.Controls.Add(Me.btnSubstituteAway)
        Me.pnlVisit2.Controls.Add(Me.btnBookingAway)
        Me.pnlVisit2.Location = New System.Drawing.Point(791, 36)
        Me.pnlVisit2.Name = "pnlVisit2"
        Me.pnlVisit2.Size = New System.Drawing.Size(91, 181)
        Me.pnlVisit2.TabIndex = 4
        '
        'btnMissPenaltyVisit
        '
        Me.btnMissPenaltyVisit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnMissPenaltyVisit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnMissPenaltyVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMissPenaltyVisit.Location = New System.Drawing.Point(0, 93)
        Me.btnMissPenaltyVisit.Name = "btnMissPenaltyVisit"
        Me.btnMissPenaltyVisit.Size = New System.Drawing.Size(91, 26)
        Me.btnMissPenaltyVisit.TabIndex = 3
        Me.btnMissPenaltyVisit.Text = "Miss Pen"
        Me.btnMissPenaltyVisit.UseVisualStyleBackColor = False
        '
        'pnlHome3
        '
        Me.pnlHome3.Controls.Add(Me.btnBackpassHome)
        Me.pnlHome3.Controls.Add(Me.btn10YDHome)
        Me.pnlHome3.Controls.Add(Me.btnThrowInHome)
        Me.pnlHome3.Controls.Add(Me.btnFreeKickHome)
        Me.pnlHome3.Controls.Add(Me.btnDefActHome)
        Me.pnlHome3.Controls.Add(Me.btnKeyMomentHome)
        Me.pnlHome3.Location = New System.Drawing.Point(206, 36)
        Me.pnlHome3.Name = "pnlHome3"
        Me.pnlHome3.Size = New System.Drawing.Size(91, 181)
        Me.pnlHome3.TabIndex = 2
        '
        'btnBackpassHome
        '
        Me.btnBackpassHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnBackpassHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackpassHome.Location = New System.Drawing.Point(0, 155)
        Me.btnBackpassHome.Name = "btnBackpassHome"
        Me.btnBackpassHome.Size = New System.Drawing.Size(91, 26)
        Me.btnBackpassHome.TabIndex = 5
        Me.btnBackpassHome.Text = "Backpass"
        Me.btnBackpassHome.UseVisualStyleBackColor = True
        '
        'btn10YDHome
        '
        Me.btn10YDHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btn10YDHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn10YDHome.Location = New System.Drawing.Point(1, 124)
        Me.btn10YDHome.Name = "btn10YDHome"
        Me.btn10YDHome.Size = New System.Drawing.Size(91, 26)
        Me.btn10YDHome.TabIndex = 4
        Me.btn10YDHome.Text = "10 YD"
        Me.btn10YDHome.UseVisualStyleBackColor = True
        '
        'btnThrowInHome
        '
        Me.btnThrowInHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnThrowInHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnThrowInHome.Location = New System.Drawing.Point(0, 93)
        Me.btnThrowInHome.Name = "btnThrowInHome"
        Me.btnThrowInHome.Size = New System.Drawing.Size(91, 26)
        Me.btnThrowInHome.TabIndex = 3
        Me.btnThrowInHome.Text = "Throw In"
        Me.btnThrowInHome.UseVisualStyleBackColor = True
        '
        'btnFreeKickHome
        '
        Me.btnFreeKickHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnFreeKickHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFreeKickHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFreeKickHome.Location = New System.Drawing.Point(0, 31)
        Me.btnFreeKickHome.Name = "btnFreeKickHome"
        Me.btnFreeKickHome.Size = New System.Drawing.Size(91, 26)
        Me.btnFreeKickHome.TabIndex = 1
        Me.btnFreeKickHome.Text = "Free Kick"
        Me.btnFreeKickHome.UseVisualStyleBackColor = False
        '
        'btnDefActHome
        '
        Me.btnDefActHome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnDefActHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDefActHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDefActHome.Location = New System.Drawing.Point(0, 0)
        Me.btnDefActHome.Name = "btnDefActHome"
        Me.btnDefActHome.Size = New System.Drawing.Size(91, 26)
        Me.btnDefActHome.TabIndex = 0
        Me.btnDefActHome.Text = "Defn Actn"
        Me.btnDefActHome.UseVisualStyleBackColor = False
        '
        'pnlVisit3
        '
        Me.pnlVisit3.Controls.Add(Me.btnBackpassVisit)
        Me.pnlVisit3.Controls.Add(Me.btn10YDVisit)
        Me.pnlVisit3.Controls.Add(Me.btnThrowInVisit)
        Me.pnlVisit3.Controls.Add(Me.btnFreeKickAway)
        Me.pnlVisit3.Controls.Add(Me.btnDefActVisit)
        Me.pnlVisit3.Controls.Add(Me.btnKeyMomentAway)
        Me.pnlVisit3.Location = New System.Drawing.Point(888, 37)
        Me.pnlVisit3.Name = "pnlVisit3"
        Me.pnlVisit3.Size = New System.Drawing.Size(91, 180)
        Me.pnlVisit3.TabIndex = 5
        '
        'btnBackpassVisit
        '
        Me.btnBackpassVisit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnBackpassVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackpassVisit.Location = New System.Drawing.Point(1, 154)
        Me.btnBackpassVisit.Name = "btnBackpassVisit"
        Me.btnBackpassVisit.Size = New System.Drawing.Size(91, 26)
        Me.btnBackpassVisit.TabIndex = 6
        Me.btnBackpassVisit.Text = "Backpass"
        Me.btnBackpassVisit.UseVisualStyleBackColor = True
        '
        'btn10YDVisit
        '
        Me.btn10YDVisit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btn10YDVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn10YDVisit.Location = New System.Drawing.Point(0, 124)
        Me.btn10YDVisit.Name = "btn10YDVisit"
        Me.btn10YDVisit.Size = New System.Drawing.Size(91, 26)
        Me.btn10YDVisit.TabIndex = 4
        Me.btn10YDVisit.Text = "10 YD"
        Me.btn10YDVisit.UseVisualStyleBackColor = True
        '
        'btnThrowInVisit
        '
        Me.btnThrowInVisit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnThrowInVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnThrowInVisit.Location = New System.Drawing.Point(0, 93)
        Me.btnThrowInVisit.Name = "btnThrowInVisit"
        Me.btnThrowInVisit.Size = New System.Drawing.Size(91, 26)
        Me.btnThrowInVisit.TabIndex = 3
        Me.btnThrowInVisit.Text = "Throw In"
        Me.btnThrowInVisit.UseVisualStyleBackColor = True
        '
        'btnFreeKickAway
        '
        Me.btnFreeKickAway.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnFreeKickAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnFreeKickAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFreeKickAway.Location = New System.Drawing.Point(0, 31)
        Me.btnFreeKickAway.Name = "btnFreeKickAway"
        Me.btnFreeKickAway.Size = New System.Drawing.Size(91, 26)
        Me.btnFreeKickAway.TabIndex = 1
        Me.btnFreeKickAway.Text = "Free Kick"
        Me.btnFreeKickAway.UseVisualStyleBackColor = False
        '
        'btnDefActVisit
        '
        Me.btnDefActVisit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnDefActVisit.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDefActVisit.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDefActVisit.Location = New System.Drawing.Point(0, 0)
        Me.btnDefActVisit.Name = "btnDefActVisit"
        Me.btnDefActVisit.Size = New System.Drawing.Size(91, 26)
        Me.btnDefActVisit.TabIndex = 0
        Me.btnDefActVisit.Text = "Defn Actn"
        Me.btnDefActVisit.UseVisualStyleBackColor = False
        '
        'btnExpand
        '
        Me.btnExpand.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.btnExpand.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExpand.Location = New System.Drawing.Point(867, 446)
        Me.btnExpand.Name = "btnExpand"
        Me.btnExpand.Size = New System.Drawing.Size(19, 20)
        Me.btnExpand.TabIndex = 10
        Me.btnExpand.Text = "▲"
        Me.btnExpand.UseVisualStyleBackColor = False
        '
        'pnlGoalsLeft
        '
        Me.pnlGoalsLeft.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.pnlGoalsLeft.Controls.Add(Me.btnOwnHome)
        Me.pnlGoalsLeft.Controls.Add(Me.btnNormalHome)
        Me.pnlGoalsLeft.Controls.Add(Me.btnPenaltyHome)
        Me.pnlGoalsLeft.Location = New System.Drawing.Point(12, 3)
        Me.pnlGoalsLeft.Name = "pnlGoalsLeft"
        Me.pnlGoalsLeft.Size = New System.Drawing.Size(285, 30)
        Me.pnlGoalsLeft.TabIndex = 17
        '
        'btnOwnHome
        '
        Me.btnOwnHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOwnHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOwnHome.Location = New System.Drawing.Point(192, 2)
        Me.btnOwnHome.Name = "btnOwnHome"
        Me.btnOwnHome.Size = New System.Drawing.Size(91, 26)
        Me.btnOwnHome.TabIndex = 3
        Me.btnOwnHome.Text = "Own"
        Me.btnOwnHome.UseVisualStyleBackColor = True
        '
        'btnNormalHome
        '
        Me.btnNormalHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnNormalHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNormalHome.Location = New System.Drawing.Point(97, 2)
        Me.btnNormalHome.Name = "btnNormalHome"
        Me.btnNormalHome.Size = New System.Drawing.Size(91, 26)
        Me.btnNormalHome.TabIndex = 2
        Me.btnNormalHome.Text = "Normal"
        Me.btnNormalHome.UseVisualStyleBackColor = True
        '
        'btnPenaltyHome
        '
        Me.btnPenaltyHome.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnPenaltyHome.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPenaltyHome.Location = New System.Drawing.Point(1, 2)
        Me.btnPenaltyHome.Name = "btnPenaltyHome"
        Me.btnPenaltyHome.Size = New System.Drawing.Size(91, 26)
        Me.btnPenaltyHome.TabIndex = 1
        Me.btnPenaltyHome.Text = "Penalty"
        Me.btnPenaltyHome.UseVisualStyleBackColor = True
        '
        'pnlGoalRight
        '
        Me.pnlGoalRight.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.pnlGoalRight.Controls.Add(Me.btnOwnAway)
        Me.pnlGoalRight.Controls.Add(Me.btnNormalAway)
        Me.pnlGoalRight.Controls.Add(Me.btnPenaltyAway)
        Me.pnlGoalRight.Location = New System.Drawing.Point(694, 3)
        Me.pnlGoalRight.Name = "pnlGoalRight"
        Me.pnlGoalRight.Size = New System.Drawing.Size(285, 30)
        Me.pnlGoalRight.TabIndex = 18
        '
        'btnOwnAway
        '
        Me.btnOwnAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnOwnAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOwnAway.Location = New System.Drawing.Point(192, 2)
        Me.btnOwnAway.Name = "btnOwnAway"
        Me.btnOwnAway.Size = New System.Drawing.Size(91, 26)
        Me.btnOwnAway.TabIndex = 6
        Me.btnOwnAway.Text = "Own"
        Me.btnOwnAway.UseVisualStyleBackColor = True
        '
        'btnNormalAway
        '
        Me.btnNormalAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnNormalAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNormalAway.Location = New System.Drawing.Point(97, 2)
        Me.btnNormalAway.Name = "btnNormalAway"
        Me.btnNormalAway.Size = New System.Drawing.Size(91, 26)
        Me.btnNormalAway.TabIndex = 5
        Me.btnNormalAway.Text = "Normal"
        Me.btnNormalAway.UseVisualStyleBackColor = True
        '
        'btnPenaltyAway
        '
        Me.btnPenaltyAway.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnPenaltyAway.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPenaltyAway.Location = New System.Drawing.Point(1, 2)
        Me.btnPenaltyAway.Name = "btnPenaltyAway"
        Me.btnPenaltyAway.Size = New System.Drawing.Size(91, 26)
        Me.btnPenaltyAway.TabIndex = 4
        Me.btnPenaltyAway.Text = "Penalty"
        Me.btnPenaltyAway.UseVisualStyleBackColor = True
        '
        'pnlGoalLblLeft
        '
        Me.pnlGoalLblLeft.BackColor = System.Drawing.Color.Transparent
        Me.pnlGoalLblLeft.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lblGoalBg
        Me.pnlGoalLblLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlGoalLblLeft.Controls.Add(Me.lblGoalAway)
        Me.pnlGoalLblLeft.Location = New System.Drawing.Point(108, 31)
        Me.pnlGoalLblLeft.Name = "pnlGoalLblLeft"
        Me.pnlGoalLblLeft.Size = New System.Drawing.Size(92, 29)
        Me.pnlGoalLblLeft.TabIndex = 18
        '
        'lblGoalAway
        '
        Me.lblGoalAway.AutoSize = True
        Me.lblGoalAway.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalAway.ForeColor = System.Drawing.Color.White
        Me.lblGoalAway.Location = New System.Drawing.Point(25, 5)
        Me.lblGoalAway.Name = "lblGoalAway"
        Me.lblGoalAway.Size = New System.Drawing.Size(42, 15)
        Me.lblGoalAway.TabIndex = 1
        Me.lblGoalAway.Text = "GOAL"
        '
        'pnlGoalLblRight
        '
        Me.pnlGoalLblRight.BackColor = System.Drawing.Color.Transparent
        Me.pnlGoalLblRight.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lblGoalBg
        Me.pnlGoalLblRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlGoalLblRight.Controls.Add(Me.lblGoalHome)
        Me.pnlGoalLblRight.Location = New System.Drawing.Point(791, 31)
        Me.pnlGoalLblRight.Name = "pnlGoalLblRight"
        Me.pnlGoalLblRight.Size = New System.Drawing.Size(92, 29)
        Me.pnlGoalLblRight.TabIndex = 19
        '
        'lblGoalHome
        '
        Me.lblGoalHome.AutoSize = True
        Me.lblGoalHome.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalHome.ForeColor = System.Drawing.Color.White
        Me.lblGoalHome.Location = New System.Drawing.Point(24, 5)
        Me.lblGoalHome.Name = "lblGoalHome"
        Me.lblGoalHome.Size = New System.Drawing.Size(42, 15)
        Me.lblGoalHome.TabIndex = 0
        Me.lblGoalHome.Text = "GOAL"
        '
        'UdcSoccerField1
        '
        Me.UdcSoccerField1.Location = New System.Drawing.Point(355, 1)
        Me.UdcSoccerField1.Name = "UdcSoccerField1"
        Me.UdcSoccerField1.Size = New System.Drawing.Size(330, 220)
        Me.UdcSoccerField1.TabIndex = 16
        Me.UdcSoccerField1.USFAwayTeamName = ""
        Me.UdcSoccerField1.USFDisplayArrow = True
        Me.UdcSoccerField1.USFDisplayTeamName = True
        Me.UdcSoccerField1.USFDisplayTouchInstructions = False
        Me.UdcSoccerField1.USFEventText = ""
        Me.UdcSoccerField1.USFHomeTeamName = ""
        Me.UdcSoccerField1.USFHomeTeamOnLeft = True
        Me.UdcSoccerField1.USFMaximumMarksAllowed = 2
        Me.UdcSoccerField1.USFNextMark = 1
        '
        'frmModule1PBP
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(992, 541)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlGoalLblRight)
        Me.Controls.Add(Me.pnlGoalLblLeft)
        Me.Controls.Add(Me.pnlGoalRight)
        Me.Controls.Add(Me.pnlGoalsLeft)
        Me.Controls.Add(Me.UdcSoccerField1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnExpand)
        Me.Controls.Add(Me.pnlVisit3)
        Me.Controls.Add(Me.pnlHome3)
        Me.Controls.Add(Me.pnlVisit2)
        Me.Controls.Add(Me.pnlVisit1)
        Me.Controls.Add(Me.pnlHome1)
        Me.Controls.Add(Me.pnlHome2)
        Me.Controls.Add(Me.pnlChildEvents)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.btnReplace)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblSavedEvents)
        Me.Controls.Add(Me.lvPBP)
        Me.Controls.Add(Me.pnlSave)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmModule1PBP"
        Me.pnlSave.ResumeLayout(False)
        Me.pnlSave.PerformLayout()
        CType(Me.picOpticalFeed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picOrangeBlink, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBlinkGreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMark2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMark1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlChildEvents.ResumeLayout(False)
        Me.pnlChildEvents.PerformLayout()
        Me.pnlHome1.ResumeLayout(False)
        Me.pnlHome2.ResumeLayout(False)
        Me.pnlVisit1.ResumeLayout(False)
        Me.pnlVisit2.ResumeLayout(False)
        Me.pnlHome3.ResumeLayout(False)
        Me.pnlVisit3.ResumeLayout(False)
        Me.pnlGoalsLeft.ResumeLayout(False)
        Me.pnlGoalRight.ResumeLayout(False)
        Me.pnlGoalLblLeft.ResumeLayout(False)
        Me.pnlGoalLblLeft.PerformLayout()
        Me.pnlGoalLblRight.ResumeLayout(False)
        Me.pnlGoalLblRight.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSubstituteHome As System.Windows.Forms.Button
    Friend WithEvents btnKeyMomentHome As System.Windows.Forms.Button
    Friend WithEvents btnGoalThreatHome As System.Windows.Forms.Button
    Friend WithEvents btnGoalKickHome As System.Windows.Forms.Button
    Friend WithEvents btnOffSideHome As System.Windows.Forms.Button
    Friend WithEvents btnFoulHome As System.Windows.Forms.Button
    Friend WithEvents btnCrossHome As System.Windows.Forms.Button
    Friend WithEvents btnCornerHome As System.Windows.Forms.Button
    Friend WithEvents btnOffTargetHome As System.Windows.Forms.Button
    Friend WithEvents btnBookingHome As System.Windows.Forms.Button
    Friend WithEvents btnOnTargetHome As System.Windows.Forms.Button
    Friend WithEvents btnGoalHome As System.Windows.Forms.Button
    Friend WithEvents pnlSave As System.Windows.Forms.Panel
    Friend WithEvents btnSubstituteAway As System.Windows.Forms.Button
    Friend WithEvents btnKeyMomentAway As System.Windows.Forms.Button
    Friend WithEvents btnGoalThreatAway As System.Windows.Forms.Button
    Friend WithEvents btnGoalKickAway As System.Windows.Forms.Button
    Friend WithEvents btnOffSideAway As System.Windows.Forms.Button
    Friend WithEvents btnFoulAway As System.Windows.Forms.Button
    Friend WithEvents btnCrossAway As System.Windows.Forms.Button
    Friend WithEvents btnCornerAway As System.Windows.Forms.Button
    Friend WithEvents btnOffTargetAway As System.Windows.Forms.Button
    Friend WithEvents btnBookingAway As System.Windows.Forms.Button
    Friend WithEvents btnOnTargetAway As System.Windows.Forms.Button
    Friend WithEvents btnGoalAway As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lvPBP As System.Windows.Forms.ListView
    Friend WithEvents lblSavedEvents As System.Windows.Forms.Label
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnReplace As System.Windows.Forms.Button
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents Team As System.Windows.Forms.ColumnHeader
    Friend WithEvents Time As System.Windows.Forms.ColumnHeader
    Friend WithEvents Type As System.Windows.Forms.ColumnHeader
    Friend WithEvents Player As System.Windows.Forms.ColumnHeader
    Friend WithEvents SubType As System.Windows.Forms.ColumnHeader
    Friend WithEvents SubSubType As System.Windows.Forms.ColumnHeader
    Friend WithEvents Area As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblFieldY As System.Windows.Forms.Label
    Friend WithEvents lblFieldX As System.Windows.Forms.Label
    Friend WithEvents lblSelection2 As System.Windows.Forms.Label
    Friend WithEvents lblSelection1 As System.Windows.Forms.Label
    Friend WithEvents EventID As System.Windows.Forms.ColumnHeader

    Friend WithEvents pnlChildEvents As System.Windows.Forms.Panel
    Friend WithEvents lblEventDetails01 As System.Windows.Forms.Label
    Friend WithEvents dblstEventDetails01 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventDetails10 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails08 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails06 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails09 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails07 As System.Windows.Forms.Label
    Friend WithEvents dblstEventDetails10 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails09 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails08 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails07 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails06 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventDetails05 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails04 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails03 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails02 As System.Windows.Forms.Label
    Friend WithEvents dblstEventDetails05 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails04 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails03 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails02 As System.Windows.Forms.ListBox
    Friend WithEvents picMark2 As System.Windows.Forms.PictureBox
    Friend WithEvents picMark1 As System.Windows.Forms.PictureBox
    Friend WithEvents SequenceNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents EventCodeID As System.Windows.Forms.ColumnHeader
    Friend WithEvents UniqueID As System.Windows.Forms.ColumnHeader
    Friend WithEvents Team_ID As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlHome1 As System.Windows.Forms.Panel
    Friend WithEvents pnlHome2 As System.Windows.Forms.Panel
    Friend WithEvents pnlVisit1 As System.Windows.Forms.Panel
    Friend WithEvents pnlVisit2 As System.Windows.Forms.Panel
    Friend WithEvents pnlHome3 As System.Windows.Forms.Panel
    Friend WithEvents btnMissPenaltyHome As System.Windows.Forms.Button
    Friend WithEvents btnDefActHome As System.Windows.Forms.Button
    Friend WithEvents pnlVisit3 As System.Windows.Forms.Panel
    Friend WithEvents btnMissPenaltyVisit As System.Windows.Forms.Button
    Friend WithEvents btnDefActVisit As System.Windows.Forms.Button
    Friend WithEvents Period As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnFreeKickHome As System.Windows.Forms.Button
    Friend WithEvents btnFreeKickAway As System.Windows.Forms.Button
    Friend WithEvents btnThrowInHome As System.Windows.Forms.Button
    Friend WithEvents btnThrowInVisit As System.Windows.Forms.Button
    Friend WithEvents txtTime As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Paired As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnExpand As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblEventDetails11 As System.Windows.Forms.Label
    Friend WithEvents dblstEventDetails11 As System.Windows.Forms.ListBox
    Friend WithEvents txtTimeMod2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnRepeatLast As System.Windows.Forms.Button
    Friend WithEvents tlpPBP As System.Windows.Forms.ToolTip
    Friend WithEvents UdcSoccerField1 As STATS.SoccerDataCollection.udcSoccerField
    Friend WithEvents picOpticalFeed As System.Windows.Forms.PictureBox
    Friend WithEvents btnRefreshTime As System.Windows.Forms.Button
    Friend WithEvents btn10YDHome As System.Windows.Forms.Button
    Friend WithEvents btn10YDVisit As System.Windows.Forms.Button
    Friend WithEvents btnBackpassHome As System.Windows.Forms.Button
    Friend WithEvents btnBackpassVisit As System.Windows.Forms.Button
    Friend WithEvents lblEventDetails12 As System.Windows.Forms.Label
    Friend WithEvents dblstEventDetails12 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventDetails13 As System.Windows.Forms.Label
    Friend WithEvents dblstEventDetails14 As System.Windows.Forms.ListBox
    Friend WithEvents dblstEventDetails13 As System.Windows.Forms.ListBox
    Friend WithEvents lblEventDetails14 As System.Windows.Forms.Label
    Friend WithEvents btnIncTime As System.Windows.Forms.Button
    Friend WithEvents btnDecTime As System.Windows.Forms.Button
    Friend WithEvents picBlinkGreen As System.Windows.Forms.PictureBox
    Friend WithEvents picOrangeBlink As System.Windows.Forms.PictureBox
    Friend WithEvents btnTime As System.Windows.Forms.Button
    Friend WithEvents btnSwitchGames As System.Windows.Forms.Button
    Friend WithEvents pnlGoalsLeft As System.Windows.Forms.Panel
    Friend WithEvents pnlGoalRight As System.Windows.Forms.Panel
    Friend WithEvents pnlGoalLblLeft As System.Windows.Forms.Panel
    Friend WithEvents pnlGoalLblRight As System.Windows.Forms.Panel
    Friend WithEvents lblGoalAway As System.Windows.Forms.Label
    Friend WithEvents lblGoalHome As System.Windows.Forms.Label
    Friend WithEvents btnPenaltyHome As System.Windows.Forms.Button
    Friend WithEvents btnOwnHome As System.Windows.Forms.Button
    Friend WithEvents btnNormalHome As System.Windows.Forms.Button
    Friend WithEvents btnOwnAway As System.Windows.Forms.Button
    Friend WithEvents btnNormalAway As System.Windows.Forms.Button
    Friend WithEvents btnPenaltyAway As System.Windows.Forms.Button
End Class
