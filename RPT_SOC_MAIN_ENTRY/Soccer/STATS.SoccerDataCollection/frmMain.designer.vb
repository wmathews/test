﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnsMain = New System.Windows.Forms.MenuStrip()
        Me.pnlEntryForm = New System.Windows.Forms.Panel()
        Me.lblLanguage = New System.Windows.Forms.Label()
        Me.lblMode = New System.Windows.Forms.Label()
        Me.lblReporterPipe = New System.Windows.Forms.Label()
        Me.pnlClockBar = New System.Windows.Forms.Panel()
        Me.picAwayColorBottom = New System.Windows.Forms.PictureBox()
        Me.picAwayColorTop = New System.Windows.Forms.PictureBox()
        Me.picHomeColorBottom = New System.Windows.Forms.PictureBox()
        Me.picHomeColorTop = New System.Windows.Forms.PictureBox()
        Me.lklAwayteam = New System.Windows.Forms.LinkLabel()
        Me.lklHometeam = New System.Windows.Forms.LinkLabel()
        Me.mtxtClockEdit = New System.Windows.Forms.MaskedTextBox()
        Me.btnSecondDown = New System.Windows.Forms.Button()
        Me.btnSecondUp = New System.Windows.Forms.Button()
        Me.lblScoreAway = New System.Windows.Forms.Label()
        Me.lblScoreHome = New System.Windows.Forms.Label()
        Me.btnClock = New System.Windows.Forms.Button()
        Me.btnMinuteDown = New System.Windows.Forms.Button()
        Me.btnMinuteUp = New System.Windows.Forms.Button()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.btnGotoMain = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnGotoPBP = New System.Windows.Forms.Button()
        Me.tmrClockIncrement = New System.Windows.Forms.Timer(Me.components)
        Me.ttClockEdit = New System.Windows.Forms.ToolTip(Me.components)
        Me.cdlgTeam = New System.Windows.Forms.ColorDialog()
        Me.picOpticalFeed = New System.Windows.Forms.PictureBox()
        Me.pnlFooterInfo = New System.Windows.Forms.Panel()
        Me.lblSportVu = New System.Windows.Forms.Label()
        Me.lblStatsConnectionStatus = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblStatsConnection = New System.Windows.Forms.Label()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picAwayLogo = New System.Windows.Forms.PictureBox()
        Me.picHomeLogo = New System.Windows.Forms.PictureBox()
        Me.pnlReporter = New System.Windows.Forms.Panel()
        Me.picEditMode = New System.Windows.Forms.PictureBox()
        Me.lblReporter = New System.Windows.Forms.LinkLabel()
        Me.picCompanyLogo = New System.Windows.Forms.PictureBox()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.tmrRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.picRptr1 = New System.Windows.Forms.PictureBox()
        Me.picRptr2 = New System.Windows.Forms.PictureBox()
        Me.picRptr4 = New System.Windows.Forms.PictureBox()
        Me.picRptr3 = New System.Windows.Forms.PictureBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.tmrRefreshGameInfo = New System.Windows.Forms.Timer(Me.components)
        Me.picRptr5 = New System.Windows.Forms.PictureBox()
        Me.picRptr6 = New System.Windows.Forms.PictureBox()
        Me.lblNetwork = New System.Windows.Forms.Label()
        Me.UdcRunningClock1 = New STATS.SoccerDataCollection.udcRunningClock()
        Me.lblNetworkTop = New System.Windows.Forms.Label()
        Me.pnlClockBar.SuspendLayout()
        CType(Me.picAwayColorBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picAwayColorTop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picHomeColorBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picHomeColorTop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picOpticalFeed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFooterInfo.SuspendLayout()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlReporter.SuspendLayout()
        CType(Me.picEditMode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCompanyLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRptr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRptr2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRptr4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRptr3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        CType(Me.picRptr5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRptr6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnsMain
        '
        Me.mnsMain.BackColor = System.Drawing.Color.Transparent
        Me.mnsMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mnsMain.Location = New System.Drawing.Point(0, 0)
        Me.mnsMain.Name = "mnsMain"
        Me.mnsMain.Size = New System.Drawing.Size(890, 24)
        Me.mnsMain.TabIndex = 0
        Me.mnsMain.Text = "MenuStrip1"
        '
        'pnlEntryForm
        '
        Me.pnlEntryForm.Location = New System.Drawing.Point(-3, 107)
        Me.pnlEntryForm.Name = "pnlEntryForm"
        Me.pnlEntryForm.Size = New System.Drawing.Size(890, 393)
        Me.pnlEntryForm.TabIndex = 1
        '
        'lblLanguage
        '
        Me.lblLanguage.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblLanguage.ForeColor = System.Drawing.Color.Thistle
        Me.lblLanguage.Location = New System.Drawing.Point(-1, 41)
        Me.lblLanguage.Name = "lblLanguage"
        Me.lblLanguage.Size = New System.Drawing.Size(85, 19)
        Me.lblLanguage.TabIndex = 236
        Me.lblLanguage.Text = " English"
        Me.lblLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMode
        '
        Me.lblMode.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblMode.ForeColor = System.Drawing.Color.Gold
        Me.lblMode.Location = New System.Drawing.Point(664, 42)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(58, 20)
        Me.lblMode.TabIndex = 233
        Me.lblMode.Text = "Live Mode"
        Me.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReporterPipe
        '
        Me.lblReporterPipe.BackColor = System.Drawing.Color.Black
        Me.lblReporterPipe.ForeColor = System.Drawing.Color.Gray
        Me.lblReporterPipe.Location = New System.Drawing.Point(651, 40)
        Me.lblReporterPipe.Name = "lblReporterPipe"
        Me.lblReporterPipe.Size = New System.Drawing.Size(16, 20)
        Me.lblReporterPipe.TabIndex = 234
        Me.lblReporterPipe.Text = " | "
        Me.lblReporterPipe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlClockBar
        '
        Me.pnlClockBar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.pnlClockBar.Controls.Add(Me.picAwayColorBottom)
        Me.pnlClockBar.Controls.Add(Me.picAwayColorTop)
        Me.pnlClockBar.Controls.Add(Me.picHomeColorBottom)
        Me.pnlClockBar.Controls.Add(Me.picHomeColorTop)
        Me.pnlClockBar.Controls.Add(Me.lklAwayteam)
        Me.pnlClockBar.Controls.Add(Me.lklHometeam)
        Me.pnlClockBar.Controls.Add(Me.mtxtClockEdit)
        Me.pnlClockBar.Controls.Add(Me.UdcRunningClock1)
        Me.pnlClockBar.Controls.Add(Me.btnSecondDown)
        Me.pnlClockBar.Controls.Add(Me.btnSecondUp)
        Me.pnlClockBar.Controls.Add(Me.lblScoreAway)
        Me.pnlClockBar.Controls.Add(Me.lblScoreHome)
        Me.pnlClockBar.Controls.Add(Me.btnClock)
        Me.pnlClockBar.Controls.Add(Me.btnMinuteDown)
        Me.pnlClockBar.Controls.Add(Me.btnMinuteUp)
        Me.pnlClockBar.Controls.Add(Me.lblPeriod)
        Me.pnlClockBar.Location = New System.Drawing.Point(85, 60)
        Me.pnlClockBar.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlClockBar.Name = "pnlClockBar"
        Me.pnlClockBar.Size = New System.Drawing.Size(718, 46)
        Me.pnlClockBar.TabIndex = 237
        '
        'picAwayColorBottom
        '
        Me.picAwayColorBottom.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.team_border
        Me.picAwayColorBottom.Location = New System.Drawing.Point(454, 36)
        Me.picAwayColorBottom.Name = "picAwayColorBottom"
        Me.picAwayColorBottom.Size = New System.Drawing.Size(243, 6)
        Me.picAwayColorBottom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAwayColorBottom.TabIndex = 26
        Me.picAwayColorBottom.TabStop = False
        '
        'picAwayColorTop
        '
        Me.picAwayColorTop.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.team_border
        Me.picAwayColorTop.Location = New System.Drawing.Point(452, 5)
        Me.picAwayColorTop.Name = "picAwayColorTop"
        Me.picAwayColorTop.Size = New System.Drawing.Size(243, 6)
        Me.picAwayColorTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAwayColorTop.TabIndex = 25
        Me.picAwayColorTop.TabStop = False
        '
        'picHomeColorBottom
        '
        Me.picHomeColorBottom.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.team_border
        Me.picHomeColorBottom.Location = New System.Drawing.Point(12, 36)
        Me.picHomeColorBottom.Name = "picHomeColorBottom"
        Me.picHomeColorBottom.Size = New System.Drawing.Size(243, 6)
        Me.picHomeColorBottom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picHomeColorBottom.TabIndex = 24
        Me.picHomeColorBottom.TabStop = False
        '
        'picHomeColorTop
        '
        Me.picHomeColorTop.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.team_border
        Me.picHomeColorTop.Location = New System.Drawing.Point(10, 5)
        Me.picHomeColorTop.Name = "picHomeColorTop"
        Me.picHomeColorTop.Size = New System.Drawing.Size(243, 6)
        Me.picHomeColorTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picHomeColorTop.TabIndex = 23
        Me.picHomeColorTop.TabStop = False
        '
        'lklAwayteam
        '
        Me.lklAwayteam.ActiveLinkColor = System.Drawing.Color.White
        Me.lklAwayteam.AutoSize = True
        Me.lklAwayteam.DisabledLinkColor = System.Drawing.Color.White
        Me.lklAwayteam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklAwayteam.ForeColor = System.Drawing.Color.White
        Me.lklAwayteam.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lklAwayteam.LinkColor = System.Drawing.Color.White
        Me.lklAwayteam.Location = New System.Drawing.Point(563, 11)
        Me.lklAwayteam.Name = "lklAwayteam"
        Me.lklAwayteam.Size = New System.Drawing.Size(108, 25)
        Me.lklAwayteam.TabIndex = 22
        Me.lklAwayteam.TabStop = True
        Me.lklAwayteam.Text = "Away team"
        Me.lklAwayteam.VisitedLinkColor = System.Drawing.Color.White
        '
        'lklHometeam
        '
        Me.lklHometeam.ActiveLinkColor = System.Drawing.Color.White
        Me.lklHometeam.AutoSize = True
        Me.lklHometeam.DisabledLinkColor = System.Drawing.Color.White
        Me.lklHometeam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklHometeam.ForeColor = System.Drawing.Color.White
        Me.lklHometeam.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lklHometeam.LinkColor = System.Drawing.Color.White
        Me.lklHometeam.Location = New System.Drawing.Point(26, 11)
        Me.lklHometeam.Name = "lklHometeam"
        Me.lklHometeam.Size = New System.Drawing.Size(112, 25)
        Me.lklHometeam.TabIndex = 0
        Me.lklHometeam.TabStop = True
        Me.lklHometeam.Text = "Home team"
        Me.lklHometeam.VisitedLinkColor = System.Drawing.Color.White
        '
        'mtxtClockEdit
        '
        Me.mtxtClockEdit.Location = New System.Drawing.Point(154, 43)
        Me.mtxtClockEdit.Name = "mtxtClockEdit"
        Me.mtxtClockEdit.Size = New System.Drawing.Size(100, 22)
        Me.mtxtClockEdit.TabIndex = 21
        Me.mtxtClockEdit.Visible = False
        '
        'btnSecondDown
        '
        Me.btnSecondDown.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnSecondDown.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.down
        Me.btnSecondDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSecondDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSecondDown.Font = New System.Drawing.Font("Arial Unicode MS", 6.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSecondDown.ForeColor = System.Drawing.Color.White
        Me.btnSecondDown.Location = New System.Drawing.Point(366, 23)
        Me.btnSecondDown.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSecondDown.Name = "btnSecondDown"
        Me.btnSecondDown.Size = New System.Drawing.Size(22, 22)
        Me.btnSecondDown.TabIndex = 19
        Me.btnSecondDown.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSecondDown.UseVisualStyleBackColor = False
        '
        'btnSecondUp
        '
        Me.btnSecondUp.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnSecondUp.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.up
        Me.btnSecondUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSecondUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSecondUp.Font = New System.Drawing.Font("Arial Unicode MS", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSecondUp.ForeColor = System.Drawing.Color.White
        Me.btnSecondUp.Location = New System.Drawing.Point(366, 2)
        Me.btnSecondUp.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSecondUp.Name = "btnSecondUp"
        Me.btnSecondUp.Size = New System.Drawing.Size(22, 22)
        Me.btnSecondUp.TabIndex = 18
        Me.btnSecondUp.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSecondUp.UseVisualStyleBackColor = False
        '
        'lblScoreAway
        '
        Me.lblScoreAway.AutoSize = True
        Me.lblScoreAway.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreAway.ForeColor = System.Drawing.Color.White
        Me.lblScoreAway.Location = New System.Drawing.Point(464, 11)
        Me.lblScoreAway.Name = "lblScoreAway"
        Me.lblScoreAway.Size = New System.Drawing.Size(23, 25)
        Me.lblScoreAway.TabIndex = 17
        Me.lblScoreAway.Text = "0"
        '
        'lblScoreHome
        '
        Me.lblScoreHome.AutoSize = True
        Me.lblScoreHome.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreHome.ForeColor = System.Drawing.Color.White
        Me.lblScoreHome.Location = New System.Drawing.Point(216, 11)
        Me.lblScoreHome.Name = "lblScoreHome"
        Me.lblScoreHome.Size = New System.Drawing.Size(23, 25)
        Me.lblScoreHome.TabIndex = 16
        Me.lblScoreHome.Text = "0"
        '
        'btnClock
        '
        Me.btnClock.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnClock.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.run
        Me.btnClock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClock.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClock.Font = New System.Drawing.Font("Arial Unicode MS", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClock.ForeColor = System.Drawing.Color.White
        Me.btnClock.Location = New System.Drawing.Point(394, 2)
        Me.btnClock.Margin = New System.Windows.Forms.Padding(0)
        Me.btnClock.Name = "btnClock"
        Me.btnClock.Size = New System.Drawing.Size(38, 43)
        Me.btnClock.TabIndex = 12
        Me.btnClock.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnClock.UseVisualStyleBackColor = False
        '
        'btnMinuteDown
        '
        Me.btnMinuteDown.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnMinuteDown.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.down
        Me.btnMinuteDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMinuteDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinuteDown.Font = New System.Drawing.Font("Arial Unicode MS", 6.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMinuteDown.ForeColor = System.Drawing.Color.White
        Me.btnMinuteDown.Location = New System.Drawing.Point(268, 23)
        Me.btnMinuteDown.Margin = New System.Windows.Forms.Padding(0)
        Me.btnMinuteDown.Name = "btnMinuteDown"
        Me.btnMinuteDown.Size = New System.Drawing.Size(22, 22)
        Me.btnMinuteDown.TabIndex = 10
        Me.btnMinuteDown.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMinuteDown.UseVisualStyleBackColor = False
        '
        'btnMinuteUp
        '
        Me.btnMinuteUp.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnMinuteUp.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.up
        Me.btnMinuteUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMinuteUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinuteUp.Font = New System.Drawing.Font("Arial Unicode MS", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMinuteUp.ForeColor = System.Drawing.Color.White
        Me.btnMinuteUp.Location = New System.Drawing.Point(268, 2)
        Me.btnMinuteUp.Margin = New System.Windows.Forms.Padding(0)
        Me.btnMinuteUp.Name = "btnMinuteUp"
        Me.btnMinuteUp.Size = New System.Drawing.Size(22, 22)
        Me.btnMinuteUp.TabIndex = 9
        Me.btnMinuteUp.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMinuteUp.UseVisualStyleBackColor = False
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblPeriod.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.ForeColor = System.Drawing.Color.White
        Me.lblPeriod.Location = New System.Drawing.Point(293, 2)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(70, 18)
        Me.lblPeriod.TabIndex = 6
        Me.lblPeriod.Text = "Pre-Game"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnGotoMain
        '
        Me.btnGotoMain.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGotoMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.btnGotoMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGotoMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGotoMain.ForeColor = System.Drawing.Color.White
        Me.btnGotoMain.Location = New System.Drawing.Point(684, 13)
        Me.btnGotoMain.Name = "btnGotoMain"
        Me.btnGotoMain.Size = New System.Drawing.Size(116, 25)
        Me.btnGotoMain.TabIndex = 261
        Me.btnGotoMain.Text = "Go to main page >>"
        Me.btnGotoMain.UseVisualStyleBackColor = False
        Me.btnGotoMain.Visible = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnClose.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClose.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(806, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 260
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnGotoPBP
        '
        Me.btnGotoPBP.BackColor = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.btnGotoPBP.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnGotoPBP.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGotoPBP.ForeColor = System.Drawing.Color.White
        Me.btnGotoPBP.Location = New System.Drawing.Point(684, 12)
        Me.btnGotoPBP.Name = "btnGotoPBP"
        Me.btnGotoPBP.Size = New System.Drawing.Size(116, 25)
        Me.btnGotoPBP.TabIndex = 263
        Me.btnGotoPBP.Text = "Go to PBP page >>"
        Me.btnGotoPBP.UseVisualStyleBackColor = False
        '
        'tmrClockIncrement
        '
        '
        'picOpticalFeed
        '
        Me.picOpticalFeed.Enabled = False
        Me.picOpticalFeed.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.Camera
        Me.picOpticalFeed.Location = New System.Drawing.Point(404, 41)
        Me.picOpticalFeed.Name = "picOpticalFeed"
        Me.picOpticalFeed.Size = New System.Drawing.Size(26, 19)
        Me.picOpticalFeed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picOpticalFeed.TabIndex = 264
        Me.picOpticalFeed.TabStop = False
        Me.picOpticalFeed.Visible = False
        '
        'pnlFooterInfo
        '
        Me.pnlFooterInfo.BackColor = System.Drawing.Color.Transparent
        Me.pnlFooterInfo.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.pnlFooterInfo.Controls.Add(Me.lblSportVu)
        Me.pnlFooterInfo.Controls.Add(Me.lblStatsConnectionStatus)
        Me.pnlFooterInfo.Controls.Add(Me.Label17)
        Me.pnlFooterInfo.Controls.Add(Me.lblStatsConnection)
        Me.pnlFooterInfo.Location = New System.Drawing.Point(4, 508)
        Me.pnlFooterInfo.Name = "pnlFooterInfo"
        Me.pnlFooterInfo.Size = New System.Drawing.Size(335, 30)
        Me.pnlFooterInfo.TabIndex = 262
        '
        'lblSportVu
        '
        Me.lblSportVu.AutoSize = True
        Me.lblSportVu.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblSportVu.ForeColor = System.Drawing.Color.Crimson
        Me.lblSportVu.Location = New System.Drawing.Point(244, 6)
        Me.lblSportVu.Name = "lblSportVu"
        Me.lblSportVu.Size = New System.Drawing.Size(82, 15)
        Me.lblSportVu.TabIndex = 263
        Me.lblSportVu.Text = ": Not supported"
        '
        'lblStatsConnectionStatus
        '
        Me.lblStatsConnectionStatus.AutoSize = True
        Me.lblStatsConnectionStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblStatsConnectionStatus.ForeColor = System.Drawing.Color.Crimson
        Me.lblStatsConnectionStatus.Location = New System.Drawing.Point(116, 6)
        Me.lblStatsConnectionStatus.Name = "lblStatsConnectionStatus"
        Me.lblStatsConnectionStatus.Size = New System.Drawing.Size(43, 15)
        Me.lblStatsConnectionStatus.TabIndex = 262
        Me.lblStatsConnectionStatus.Text = ": Active"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label17.ForeColor = System.Drawing.Color.LightGray
        Me.Label17.Location = New System.Drawing.Point(198, 6)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(46, 15)
        Me.Label17.TabIndex = 260
        Me.Label17.Text = "SportVu"
        '
        'lblStatsConnection
        '
        Me.lblStatsConnection.AutoSize = True
        Me.lblStatsConnection.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblStatsConnection.ForeColor = System.Drawing.Color.LightGray
        Me.lblStatsConnection.Location = New System.Drawing.Point(1, 6)
        Me.lblStatsConnection.Name = "lblStatsConnection"
        Me.lblStatsConnection.Size = New System.Drawing.Size(115, 15)
        Me.lblStatsConnection.TabIndex = 259
        Me.lblStatsConnection.Text = " Connection to STATS"
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 553)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(890, 16)
        Me.sstMain.TabIndex = 259
        Me.sstMain.Text = "StatusStrip1"
        '
        'picAwayLogo
        '
        Me.picAwayLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picAwayLogo.Location = New System.Drawing.Point(824, 61)
        Me.picAwayLogo.Name = "picAwayLogo"
        Me.picAwayLogo.Size = New System.Drawing.Size(45, 45)
        Me.picAwayLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAwayLogo.TabIndex = 239
        Me.picAwayLogo.TabStop = False
        '
        'picHomeLogo
        '
        Me.picHomeLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picHomeLogo.Location = New System.Drawing.Point(21, 61)
        Me.picHomeLogo.Name = "picHomeLogo"
        Me.picHomeLogo.Size = New System.Drawing.Size(45, 45)
        Me.picHomeLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picHomeLogo.TabIndex = 238
        Me.picHomeLogo.TabStop = False
        '
        'pnlReporter
        '
        Me.pnlReporter.BackColor = System.Drawing.Color.Transparent
        Me.pnlReporter.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.pnlReporter.Controls.Add(Me.picEditMode)
        Me.pnlReporter.Controls.Add(Me.lblReporter)
        Me.pnlReporter.Location = New System.Drawing.Point(464, 39)
        Me.pnlReporter.Name = "pnlReporter"
        Me.pnlReporter.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.pnlReporter.Size = New System.Drawing.Size(196, 21)
        Me.pnlReporter.TabIndex = 235
        '
        'picEditMode
        '
        Me.picEditMode.Location = New System.Drawing.Point(-25, 3)
        Me.picEditMode.Name = "picEditMode"
        Me.picEditMode.Size = New System.Drawing.Size(17, 17)
        Me.picEditMode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picEditMode.TabIndex = 31
        Me.picEditMode.TabStop = False
        '
        'lblReporter
        '
        Me.lblReporter.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblReporter.AutoSize = True
        Me.lblReporter.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblReporter.LinkColor = System.Drawing.Color.DarkTurquoise
        Me.lblReporter.Location = New System.Drawing.Point(3, 3)
        Me.lblReporter.Name = "lblReporter"
        Me.lblReporter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReporter.Size = New System.Drawing.Size(101, 15)
        Me.lblReporter.TabIndex = 30
        Me.lblReporter.TabStop = True
        Me.lblReporter.Text = "Chuck Miller (Main)"
        Me.lblReporter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picCompanyLogo
        '
        Me.picCompanyLogo.BackColor = System.Drawing.Color.Transparent
        Me.picCompanyLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.MainLogo
        Me.picCompanyLogo.Location = New System.Drawing.Point(806, 22)
        Me.picCompanyLogo.Name = "picCompanyLogo"
        Me.picCompanyLogo.Size = New System.Drawing.Size(84, 36)
        Me.picCompanyLogo.TabIndex = 232
        Me.picCompanyLogo.TabStop = False
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-134, 39)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(1024, 21)
        Me.picReporterBar.TabIndex = 231
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-134, 24)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(1024, 15)
        Me.picTopBar.TabIndex = 230
        Me.picTopBar.TabStop = False
        '
        'tmrRefresh
        '
        Me.tmrRefresh.Interval = 500
        '
        'picRptr1
        '
        Me.picRptr1.BackColor = System.Drawing.SystemColors.ControlText
        Me.picRptr1.Image = CType(resources.GetObject("picRptr1.Image"), System.Drawing.Image)
        Me.picRptr1.Location = New System.Drawing.Point(724, 41)
        Me.picRptr1.Name = "picRptr1"
        Me.picRptr1.Size = New System.Drawing.Size(19, 19)
        Me.picRptr1.TabIndex = 265
        Me.picRptr1.TabStop = False
        '
        'picRptr2
        '
        Me.picRptr2.BackColor = System.Drawing.SystemColors.ControlText
        Me.picRptr2.Image = CType(resources.GetObject("picRptr2.Image"), System.Drawing.Image)
        Me.picRptr2.Location = New System.Drawing.Point(745, 41)
        Me.picRptr2.Name = "picRptr2"
        Me.picRptr2.Size = New System.Drawing.Size(19, 19)
        Me.picRptr2.TabIndex = 266
        Me.picRptr2.TabStop = False
        '
        'picRptr4
        '
        Me.picRptr4.BackColor = System.Drawing.SystemColors.ControlText
        Me.picRptr4.Image = CType(resources.GetObject("picRptr4.Image"), System.Drawing.Image)
        Me.picRptr4.Location = New System.Drawing.Point(787, 41)
        Me.picRptr4.Name = "picRptr4"
        Me.picRptr4.Size = New System.Drawing.Size(19, 19)
        Me.picRptr4.TabIndex = 268
        Me.picRptr4.TabStop = False
        '
        'picRptr3
        '
        Me.picRptr3.BackColor = System.Drawing.SystemColors.ControlText
        Me.picRptr3.Image = CType(resources.GetObject("picRptr3.Image"), System.Drawing.Image)
        Me.picRptr3.Location = New System.Drawing.Point(766, 41)
        Me.picRptr3.Name = "picRptr3"
        Me.picRptr3.Size = New System.Drawing.Size(19, 19)
        Me.picRptr3.TabIndex = 267
        Me.picRptr3.TabStop = False
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnClose)
        Me.picButtonBar.Controls.Add(Me.btnGotoPBP)
        Me.picButtonBar.Controls.Add(Me.btnGotoMain)
        Me.picButtonBar.Location = New System.Drawing.Point(-1, 500)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(891, 60)
        Me.picButtonBar.TabIndex = 269
        '
        'tmrRefreshGameInfo
        '
        Me.tmrRefreshGameInfo.Interval = 400
        '
        'picRptr5
        '
        Me.picRptr5.BackColor = System.Drawing.SystemColors.ControlText
        Me.picRptr5.Image = CType(resources.GetObject("picRptr5.Image"), System.Drawing.Image)
        Me.picRptr5.Location = New System.Drawing.Point(806, 41)
        Me.picRptr5.Name = "picRptr5"
        Me.picRptr5.Size = New System.Drawing.Size(19, 19)
        Me.picRptr5.TabIndex = 270
        Me.picRptr5.TabStop = False
        '
        'picRptr6
        '
        Me.picRptr6.BackColor = System.Drawing.SystemColors.ControlText
        Me.picRptr6.Image = CType(resources.GetObject("picRptr6.Image"), System.Drawing.Image)
        Me.picRptr6.Location = New System.Drawing.Point(825, 41)
        Me.picRptr6.Name = "picRptr6"
        Me.picRptr6.Size = New System.Drawing.Size(19, 19)
        Me.picRptr6.TabIndex = 271
        Me.picRptr6.TabStop = False
        '
        'lblNetwork
        '
        Me.lblNetwork.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNetwork.BackColor = System.Drawing.Color.Red
        Me.lblNetwork.Enabled = False
        Me.lblNetwork.ForeColor = System.Drawing.Color.White
        Me.lblNetwork.Location = New System.Drawing.Point(-65, 554)
        Me.lblNetwork.Name = "lblNetwork"
        Me.lblNetwork.Size = New System.Drawing.Size(1021, 15)
        Me.lblNetwork.TabIndex = 272
        Me.lblNetwork.Text = "Connection to server has been lost. Actions created now will be saved once connection " & _
  "has been repaired. If you have internet try Help -> Restart Datasync " & _
  "to fix connection. "

        Me.lblNetwork.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblNetwork.Visible = False
        '
        'UdcRunningClock1
        '
        Me.UdcRunningClock1.BackColor = System.Drawing.SystemColors.Control
        Me.UdcRunningClock1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UdcRunningClock1.Location = New System.Drawing.Point(305, 23)
        Me.UdcRunningClock1.Name = "UdcRunningClock1"
        Me.UdcRunningClock1.Size = New System.Drawing.Size(40, 20)
        Me.UdcRunningClock1.TabIndex = 20
        Me.UdcRunningClock1.URCBackColor = System.Drawing.SystemColors.Control
        Me.UdcRunningClock1.URCPauseForeColor = System.Drawing.Color.Empty
        Me.UdcRunningClock1.URCRunForeColor = System.Drawing.Color.Empty
        Me.UdcRunningClock1.URCStartForeColor = System.Drawing.SystemColors.ControlText
        '
        'lblNetworkTop
        '
        Me.lblNetworkTop.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNetworkTop.BackColor = System.Drawing.Color.Red
        Me.lblNetworkTop.Enabled = False
        Me.lblNetworkTop.ForeColor = System.Drawing.Color.White
        Me.lblNetworkTop.Location = New System.Drawing.Point(-65, 22)
        Me.lblNetworkTop.Name = "lblNetworkTop"
        Me.lblNetworkTop.Size = New System.Drawing.Size(1021, 15)
        Me.lblNetworkTop.TabIndex = 274
        Me.lblNetworkTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblNetworkTop.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(890, 569)
        Me.Controls.Add(Me.picRptr6)
        Me.Controls.Add(Me.picRptr5)
        Me.Controls.Add(Me.picCompanyLogo)
        Me.Controls.Add(Me.lblNetworkTop)
        Me.Controls.Add(Me.lblNetwork)
        Me.Controls.Add(Me.pnlEntryForm)
        Me.Controls.Add(Me.pnlFooterInfo)
        Me.Controls.Add(Me.picRptr1)
        Me.Controls.Add(Me.picRptr2)
        Me.Controls.Add(Me.picRptr4)
        Me.Controls.Add(Me.picRptr3)
        Me.Controls.Add(Me.picOpticalFeed)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picAwayLogo)
        Me.Controls.Add(Me.picHomeLogo)
        Me.Controls.Add(Me.lblLanguage)
        Me.Controls.Add(Me.pnlReporter)
        Me.Controls.Add(Me.lblMode)
        Me.Controls.Add(Me.lblReporterPipe)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.pnlClockBar)
        Me.Controls.Add(Me.mnsMain)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.mnsMain
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Soccer data collection"
        Me.pnlClockBar.ResumeLayout(False)
        Me.pnlClockBar.PerformLayout()
        CType(Me.picAwayColorBottom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picAwayColorTop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picHomeColorBottom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picHomeColorTop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picOpticalFeed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFooterInfo.ResumeLayout(False)
        Me.pnlFooterInfo.PerformLayout()
        CType(Me.picAwayLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picHomeLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlReporter.ResumeLayout(False)
        Me.pnlReporter.PerformLayout()
        CType(Me.picEditMode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCompanyLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRptr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRptr2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRptr4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRptr3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        CType(Me.picRptr5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRptr6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnsMain As System.Windows.Forms.MenuStrip
    Friend WithEvents pnlEntryForm As System.Windows.Forms.Panel
    Friend WithEvents picAwayLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picHomeLogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblLanguage As System.Windows.Forms.Label
    Friend WithEvents pnlReporter As System.Windows.Forms.Panel
    Friend WithEvents picEditMode As System.Windows.Forms.PictureBox
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents lblReporterPipe As System.Windows.Forms.Label
    Friend WithEvents picCompanyLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents pnlClockBar As System.Windows.Forms.Panel
    Friend WithEvents btnSecondDown As System.Windows.Forms.Button
    Friend WithEvents btnSecondUp As System.Windows.Forms.Button
    Friend WithEvents lblScoreAway As System.Windows.Forms.Label
    Friend WithEvents lblScoreHome As System.Windows.Forms.Label
    Friend WithEvents btnClock As System.Windows.Forms.Button
    Friend WithEvents btnMinuteDown As System.Windows.Forms.Button
    Friend WithEvents btnMinuteUp As System.Windows.Forms.Button
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents btnGotoMain As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents pnlFooterInfo As System.Windows.Forms.Panel
    Friend WithEvents lblSportVu As System.Windows.Forms.Label
    Friend WithEvents lblStatsConnectionStatus As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblStatsConnection As System.Windows.Forms.Label
    Friend WithEvents btnGotoPBP As System.Windows.Forms.Button
    Friend WithEvents UdcRunningClock1 As STATS.SoccerDataCollection.udcRunningClock
    Friend WithEvents tmrClockIncrement As System.Windows.Forms.Timer
    Friend WithEvents mtxtClockEdit As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ttClockEdit As System.Windows.Forms.ToolTip
    Friend WithEvents cdlgTeam As System.Windows.Forms.ColorDialog
    Friend WithEvents picOpticalFeed As System.Windows.Forms.PictureBox
    Friend WithEvents lklHometeam As System.Windows.Forms.LinkLabel
    Friend WithEvents lklAwayteam As System.Windows.Forms.LinkLabel
    Friend WithEvents tmrRefresh As System.Windows.Forms.Timer
    Friend WithEvents picRptr1 As System.Windows.Forms.PictureBox
    Friend WithEvents picRptr2 As System.Windows.Forms.PictureBox
    Friend WithEvents picRptr4 As System.Windows.Forms.PictureBox
    Friend WithEvents picRptr3 As System.Windows.Forms.PictureBox
    Friend WithEvents lblReporter As System.Windows.Forms.LinkLabel
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents picHomeColorTop As System.Windows.Forms.PictureBox
    Friend WithEvents picHomeColorBottom As System.Windows.Forms.PictureBox
    Friend WithEvents picAwayColorBottom As System.Windows.Forms.PictureBox
    Friend WithEvents picAwayColorTop As System.Windows.Forms.PictureBox
    Friend WithEvents tmrRefreshGameInfo As System.Windows.Forms.Timer
    Friend WithEvents picRptr5 As System.Windows.Forms.PictureBox
    Friend WithEvents picRptr6 As System.Windows.Forms.PictureBox
    Friend WithEvents lblNetwork As System.Windows.Forms.Label
    Friend WithEvents lblNetworkTop As System.Windows.Forms.Label
End Class
