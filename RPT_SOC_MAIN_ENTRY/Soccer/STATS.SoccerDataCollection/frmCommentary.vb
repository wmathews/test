﻿#Region " Options "
Option Explicit On
Option Strict On
#End Region

#Region " Imports "
Imports STATS.Utility
Imports STATS.SoccerBL
Imports System.Data

#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmCommentary
' Author        : Dijo Davis
' Created Date  : 29-04-09
' Description   : This is the entry form for commentary module. This form will be contained within frmMain at runtime.
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmCommentary

#Region "Constants & Variables"
    Private m_objUtility As New clsUtility
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objclsCommentary As STATS.SoccerBL.clsCommentary = clsCommentary.GetInstance()
    Private m_objclsLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private m_blnlvwValidate As Boolean = False
    Private m_dsCommentaryData As New DataSet()
    Private m_decSequenceNo As String = "-1"
    Private m_intCurrentPeriod As Integer
    Private m_dtHome As DataTable
    Private m_dtAway As DataTable
    Private m_blnHomeLineUp As Boolean = False
    Private m_blnVisitLineUp As Boolean = False
    Private m_blnMouseMoveOn As Boolean = False
    Private m_strTooltipText As String = ""
    Private blnSkipSEChanged As Boolean = False

    Private m_strTxtHeadline As String = String.Empty
    Private m_strTxtTime As String = String.Empty
    Private m_strTxtComment As String = String.Empty

    Private dsHomeTeam As New DataSet
    Private dsAwayTeam As New DataSet

    Private Const PLAYER_PANEL_TOP As Integer = 6
    Private Const LEFT_PLAYER_PANEL_LEFT As Integer = 22
    Private Const RIGHT_PLAYER_PANEL_LEFT As Integer = 757
    Private Const BENCH_PANEL_TOP As Integer = 316
    Private Const RIGHT_BENCH_PANEL_LEFT As Integer = 709
    Private Const m_intPeriodBreak As Integer = 15
    Private Const m_intPeriodDuration As Integer = 45

    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const FIRSTEXTRA As Integer = 6300
    Private Const SECONDEXTRA As Integer = 7200
    Private lsupport As New languagesupport
    Dim st1 As String
    Dim st2 As String
    Dim st3 As String
    Dim st4 As String
    Dim st5 As String
    Dim st6 As String
    Private Dsref As DataSet
    Private MessageDialog As New frmMessageDialog

    Dim temp As Int32
    Dim intPrevUniqueID As Integer
    Dim m1 As String = "Please Save/Clear to continue"
    Dim m2 As String = "Please enter a numeric value"
    Dim m3 As String = "Please enter correct time"
    Dim m4 As String = "Please enter a valid text"
    Dim m5 As String = "Please enter a numeric value between 1-90"
    Dim m6 As String = "Please enter data in required fields - HeadLine,Comment"
    Dim m7 As String = "Please set a time below current time"
    Dim M8 As String = "Time should be minimum 106 minutes (2nd Extra Time)!"
    Dim M9 As String = "Time should be minimum 91 minutes (1st Extra Time)!"
    Dim M10 As String = "Time should be minimum 46 minutes (2nd Half)!"
    Dim M11 As String = "Selcet valid Commentary Type!"

    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()

#End Region

#Region "Event Handling"
    ''' <summary>
    ''' Used to populate and initialize controls
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmCommentary_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmCommentary", Nothing, 1, 0)
            'For Multilingual
            '------------------------------------------------------------------------------------------------------
            Dim dsReporterAssignment As DataSet
            dsReporterAssignment = m_objGeneral.getAssignmentForCommentary(m_objGameDetails.GameCode, m_objclsLoginDetails.UserId, m_objGameDetails.ModuleID, m_objclsGameDetails.SerialNo)

            If dsReporterAssignment.Tables(0).Rows.Count > 0 Then
                m_objclsGameDetails.ClientID = CInt(dsReporterAssignment.Tables(0).Rows(0).Item("CLIENT_ID"))
                '----------- Arindam get comment type data -----------------'

                Dim dsCommentaryType As New DataSet

                dsCommentaryType = m_objclsCommentary.GetCommType(m_objclsGameDetails.languageid, m_objclsGameDetails.ClientID)
                If dsCommentaryType IsNot Nothing Then
                    Dim dtCommType As DataTable
                    Dim ds As New DataSet
                    dtCommType = dsCommentaryType.Tables(0).Copy
                    dtCommType.TableName = "Table1"
                    ds.Tables.Add(dtCommType)

                    Dsref = ds.Copy()

                    PopulateDropdowns(Dsref)
                End If
            End If

            If CInt(m_objclsGameDetails.languageid) <> 1 Then
                lblTime.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblTime.Text)
                lblHeadline.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblHeadline.Text)
                Label4.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), Label4.Text)
                lblPlayByPlay.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), lblPlayByPlay.Text)
                btnRefreshPlayers.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnRefreshPlayers.Text)
                btnClear.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnClear.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnSave.Text)
                frmMain.lblMode.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.lblMode.Text)
                'frmMain.Label15.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.Label15.Text)
                frmMain.lblStatsConnectionStatus.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.lblStatsConnectionStatus.Text)
                frmMain.btnClose.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.btnClose.Text)
                frmMain.lblSportVu.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.lblSportVu.Text)
                frmMain.lblStatsConnection.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.lblStatsConnection.Text)
                frmMain.Label17.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), frmMain.Label17.Text)
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Game time")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Period")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Headline")
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Comment")
                lvwCommentary.Columns(0).Text = g1
                lvwCommentary.Columns(1).Text = g2
                lvwCommentary.Columns(2).Text = g3
                lvwCommentary.Columns(3).Text = g4
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Event")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Type")
                Dim g7 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "Player")
                Dim g8 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "SubType")
                Dim g9 As String = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), "SubSubType")
                m1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m1)
                m2 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m2)
                m3 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m3)
                m4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m4)
                m5 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m5)
                m6 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m6)
                m7 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), m7)
                lvwPBP.Columns(0).Text = lblTime.Text.Replace(":", "")
                lvwPBP.Columns(1).Text = g5
                lvwPBP.Columns(2).Text = g6
                lvwPBP.Columns(3).Text = g7
                lvwPBP.Columns(4).Text = g8
                lvwPBP.Columns(5).Text = "Sub" + g8

                '------------------------------------------------------------------------------------------------------

            End If
            m_blnlvwValidate = False
            btnHomeTeam.Text = m_objclsGameDetails.HomeTeam.Trim()
            btnHomeTeam.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnHomeTeam.Text)
            btnVisitTeam.Text = m_objclsGameDetails.AwayTeam.Trim()
            btnVisitTeam.Text = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), btnVisitTeam.Text)
            tmrPBP.Enabled = True
            tmrPBP.Start()
            If m_objclsGameDetails.IsRestartGame = False Then
                BindControls()
            End If
            'BindControls()
            txtTime.Focus()
            frmMain.tmrRefresh.Enabled = True 'RM 7353
            frmMain.lklHometeam.Links(0).Enabled = False
            frmMain.lklAwayteam.Links(0).Enabled = False
            frmMain.lklHometeam.LinkBehavior = LinkBehavior.NeverUnderline
            frmMain.lklAwayteam.LinkBehavior = LinkBehavior.NeverUnderline

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Application.Exit()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer1.MouseClick
        Try
            'AUDIT TRIAL
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer1.Tag.ToString())

            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer1.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer1.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If

                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer2.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer2.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer2.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer2.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer3.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer3.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer3.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer3.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer4.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer4.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer4.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer4.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer5_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer5.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer5.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer5.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer5.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer6_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer6.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer6.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer6.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer6.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer7.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer7.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer7.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer7.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer8.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer8.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer8.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer8.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer9_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer9.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer9.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer9.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer9.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer10.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer10.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer10.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer10.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomePlayer11_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer11.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer11.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer11.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer11.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub1.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub1.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub1.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub1.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub2.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub2.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub2.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub2.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub3.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub3.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub3.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub3.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub4.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub4.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub4.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub4.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub5_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub5.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub5.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub5.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub5.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub6_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub6.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub6.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub6.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub6.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub7.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub7.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub7.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub7.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub8.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub8.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub8.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub8.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub9_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub9.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub9.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub9.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub9.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub10.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub10.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub10.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub10.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub11_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub11.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub11.Tag.ToString())
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub11.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub11.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub12_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub12.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub12.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub12.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub12.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub13_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub13.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub13.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub13.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub13.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub14_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub14.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub14.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub14.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub14.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub15.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub15.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub15.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub15.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub16_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub16.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub16.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub16.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub16.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub17_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub17.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub17.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub17.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub17.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub18_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub18.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub18.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub18.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub18.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub19_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub19.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub19.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub19.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub19.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub20_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub20.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub20.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub20.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub20.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub21_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub21.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub21.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub21.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub21.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub22_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub22.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub22.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub22.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub22.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub23.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub23.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub23.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub23.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub24.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub24.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub24.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub24.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub25_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub25.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub25.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub25.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub25.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub26_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub26.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub26.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub26.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub26.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub27_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub27.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub27.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub27.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub27.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub28_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub28.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub28.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub28.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub28.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub29_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub29.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub29.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub29.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub29.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeSub30_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeSub30.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomeSub30.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeSub30.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnHomeSub30.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer1.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer1.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer1.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer1.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer2.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer2.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer2.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer2.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer3.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer3.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer3.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer3.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    '''  To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer4.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer4.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer4.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer4.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer5_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer5.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer5.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer5.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer5.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    '''  To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer6_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer6.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer6.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer6.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer6.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer7.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer7.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer7.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer7.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer8.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer8.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer8.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer8.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer9_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer9.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer9.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer9.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer9.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer10.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer10.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer10.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer10.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitPlayer11_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer11.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer11.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer11.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer11.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub1.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub1.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub1.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub1.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub2.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub2.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub2.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub2.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub3.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub3.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub3.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub3.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub4.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub4.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub4.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub4.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub5_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub5.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub5.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub5.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub5.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub6_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub6.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub6.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub6.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub6.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub7.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub7.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub7.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub7.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub8.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub8.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub8.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub8.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub9_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub9.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub9.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub9.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub9.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub10.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub10.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub10.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub10.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub11_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub11.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub11.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub11.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub11.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub12_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub12.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub12.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub12.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub12.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub13_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub13.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub13.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub13.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub13.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub14_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub14.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub14.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub14.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub14.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub15.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub15.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub15.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub15.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub16_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub16.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub16.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub16.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub16.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub17_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub17.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub17.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub17.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub17.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub18_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub18.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub18.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub18.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub18.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub19_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub19.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub19.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub19.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub19.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub20_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub20.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub20.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub20.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub20.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub21_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub21.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub21.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub21.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub21.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub22_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub22.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub22.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub22.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub22.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub23.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub23.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub23.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub23.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub24.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub24.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub24.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub24.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub25_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub25.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub25.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer1.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub25.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub26_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub26.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub26.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub26.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub26.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub27_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub27.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub27.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub27.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub27.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub28_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub28.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub28.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub28.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub28.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub29_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub29.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub29.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub29.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub29.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate player name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitSub30_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitSub30.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitSub30.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitSub30.Text.Trim.Replace(vbNewLine, " "), 1, 0)

            If Not String.IsNullOrEmpty(btnVisitSub30.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate home team name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeTeam_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeTeam.MouseClick
        Try
            'Dim arrlst As New ArrayList
            'arrlst = GetNameandID(btnHomeTeam.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeTeam.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomeTeam.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(btnHomeTeam.Text.Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(btnHomeTeam.Text.Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate visit team name in comment text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitTeam_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitTeam.MouseClick
        Try
            'Dim arrlst As New ArrayList
            'arrlst = GetNameandID(btnVisitTeam.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitTeam.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitTeam.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(btnVisitTeam.Text.Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(btnVisitTeam.Text.Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate visit team coach
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnVisitCoach_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitCoach.MouseClick
        Try
            'Dim arrlst As New ArrayList
            'arrlst = GetNameandID(btnVisitCoach.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitCoach.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitCoach.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(btnVisitCoach.Tag.ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(btnVisitCoach.Tag.ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To populate home team coach
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnHomeCoach_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomeCoach.MouseClick
        Try
            'Dim arrlst As New ArrayList
            'arrlst = GetNameandID(btnHomeCoach.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomeCoach.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomeCoach.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(btnHomeCoach.Tag.ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(btnHomeCoach.Tag.ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To clear controls
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClear.Text, 1, 0)

            ClearTextboxes()
            m_objclsGameDetails.IsEditMode = False
            frmMain.ChangeTheme(False)
            Me.ChangeTheme(False)

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    '''  To populate controls with the selected comentary data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lvwCommentary_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwCommentary.DoubleClick
        Try
            If lvwCommentary.SelectedItems.Count > 0 And m_objclsGameDetails.IsEditMode = False Then
                If Not String.IsNullOrEmpty(txtHeadline.Text.Trim()) Or Not String.IsNullOrEmpty(txtComment.Text.Trim()) Or Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                    MessageDialog.Show(m1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                If m_blnlvwValidate Then
                    txtTime.Enabled = True
                    SelectedListViewDisplay()
                    m_objclsGameDetails.IsEditMode = True
                    frmMain.ChangeTheme(True)
                    Me.ChangeTheme(True)
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To save or update commentary data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strSystemTime As String = Format(Date.Now - m_objclsLoginDetails.USIndiaTimeDiff, "yyyy-MM-dd hh:mm:ss tt")
            If Not String.IsNullOrEmpty(txtHeadline.Text.Trim()) And Not String.IsNullOrEmpty(txtComment.Text.Trim()) Then
                Dim chkRes As Boolean = True
                If Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                    chkRes = clsValidation.ValidateNumeric(txtTime.Text.Trim())
                    If chkRes Then
                        chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, txtTime.Text.Trim(), 0, 150)
                    Else
                        MessageDialog.Show(m2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                Else
                    If m_objclsGameDetails.CurrentPeriod > 0 And frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") = True Then
                        If m_objclsGameDetails.IsEditMode Then
                            If m_intCurrentPeriod <> 0 Then
                                MessageDialog.Show(m3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                                Exit Sub
                            End If
                        Else
                            MessageDialog.Show(m3, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                            Exit Sub
                        End If

                    End If
                End If

                If txtTime.Text <> "" Then
                    If m_objGameDetails.IsDefaultGameClock = True Then
                        If m_objclsGameDetails.IsEditMode And m_intCurrentPeriod = 0 Then

                        Else
                            If m_objGameDetails.CurrentPeriod = 2 And Convert.ToInt32(txtTime.Text.Trim) < 46 Then
                                If m_objGameDetails.CurrentPeriod = m_intCurrentPeriod Then
                                    MessageDialog.Show(M10, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If
                            End If
                            If Convert.ToInt32(txtTime.Text.Trim) < 91 And m_objGameDetails.CurrentPeriod = 3 Then
                                If m_objGameDetails.CurrentPeriod = m_intCurrentPeriod Then
                                    MessageDialog.Show(M9, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If
                            End If
                            If Convert.ToInt32(txtTime.Text.Trim) < 106 And m_objGameDetails.CurrentPeriod = 4 Then
                                If m_objGameDetails.CurrentPeriod = m_intCurrentPeriod Then
                                    MessageDialog.Show(M8, "Save", MessageDialogButtons.OK, MessageDialogIcon.Warning)
                                    Exit Sub
                                End If

                            End If
                        End If

                    End If
                End If

                ' If chkRes Then
                ''   chkRes = clsValidation.ValidateAlphaNumeric(txtHeadline.Text.Trim())
                ''  ElseIf m_objclsGameDetails.CurrentPeriod <> 0 Then
                ''     essageDialog.Show("Please enter a numeric value between 0-90", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                ''   End If

                ' If chkRes Then
                '    chkRes = clsValidation.ValidateAlphaNumeric(txtComment.Text.Trim())
                ' Else
                '   essageDialog.Show("Please enter a valid text", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                ' End If
                If Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then

                    If chkRes Then
                        chkRes = ValidateCurrentTime()
                    Else
                        MessageDialog.Show(m4, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                End If
                Dim intPeriod As Integer = 0
                If frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") Then
                    intPeriod = m_objclsGameDetails.CurrentPeriod
                End If

                'if

                'm_objclsGameDetails.CurrentPeriod = 1 'Hardcoded by Dijo for time being on 29-04-09
                If chkRes Then
                    Dim intTime As Int32 = 0
                    If Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                        'intTime = Convert.ToInt32(txtTime.Text.Trim())
                        If m_objclsGameDetails.IsDefaultGameClock Then
                            If m_objclsGameDetails.IsEditMode Then
                                intTime = CalculateTimeElapsedBefore(Convert.ToInt32(txtTime.Text.Trim()) * 60, m_intCurrentPeriod)
                            Else
                                intTime = CalculateTimeElapsedBefore(Convert.ToInt32(txtTime.Text.Trim()) * 60, m_objclsGameDetails.CurrentPeriod)
                            End If

                        Else
                            intTime = Convert.ToInt32(txtTime.Text.Trim()) * 60
                        End If
                    Else
                        intTime = -1
                    End If
                    If Not frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") And m_objclsGameDetails.IsEditMode = False And m_objclsGameDetails.CurrentPeriod = 0 Then
                        intTime = -1
                    End If
                    If m_objclsGameDetails.IsEditMode And m_intCurrentPeriod = 0 Then
                        intTime = -1
                        intPeriod = m_intCurrentPeriod
                    End If
                    If intTime > 0 Then
                        intTime = intTime - 30
                    Else
                        'If m_objclsGameDetails.CurrentPeriod > 0 Then
                        '    If m_objclsGameDetails.IsEditMode Then
                        '        'intTime = Convert.ToInt32(txtTime.Text.Trim()) * 60
                        '    Else
                        '        CommLastEvt = m_objclsCommentary.GetLastCommentary(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)
                        '        intTime = CInt(CommLastEvt.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                        '    End If
                        'End If
                    End If

                    If m_objclsGameDetails.IsEditMode Then
                        intPeriod = m_intCurrentPeriod
                    Else
                        intPeriod = m_objclsGameDetails.CurrentPeriod
                    End If

                    If m_decSequenceNo = "-1" Then
                        m_objclsCommentary.AddCommentary(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsLoginDetails.LanguageID, intPeriod, intTime, txtHeadline.Text.Trim(), txtComment.Text.Trim(), CInt(cmbCommentType.SelectedValue), strSystemTime, m_objGameDetails.SerialNo)
                    Else
                        m_objclsCommentary.UpdateCommentary(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, m_objclsLoginDetails.LanguageID, m_intCurrentPeriod, intTime, txtHeadline.Text.Trim(), txtComment.Text.Trim(), CInt(cmbCommentType.SelectedValue), CType(m_decSequenceNo, Decimal), m_objGameDetails.SerialNo)
                    End If

                    'AUDIT TRIAL
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Period] " & m_objGameDetails.CurrentPeriod, 1, 0)
                    m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, "[Event entered at] " & txtTime.Text, 1, 0)
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, 1, 1)

                End If
                ClearTextboxes()
                BindCommentaryData()
                m_decSequenceNo = "-1"
                m_intCurrentPeriod = -1
                frmMain.ChangeTheme(False)
                Me.ChangeTheme(False)
                m_objclsGameDetails.IsEditMode = False

            Else
                MessageDialog.Show(m6, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To populate controls with the selected comentary data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub EditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditToolStripMenuItem.Click
        Try
            If lvwCommentary.SelectedItems.Count > 0 Then
                If Not String.IsNullOrEmpty(txtHeadline.Text.Trim()) Or Not String.IsNullOrEmpty(txtComment.Text.Trim()) Or Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                    MessageDialog.Show(m1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    Exit Sub
                End If
                'AUDIT TRIAL
                m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ContextMenuItemSelected, EditToolStripMenuItem.Text, 1, 0)

                txtTime.Enabled = True
                m_objclsGameDetails.IsEditMode = True
                SelectedListViewDisplay()
                frmMain.ChangeTheme(True)
                Me.ChangeTheme(True)
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To delete commentry data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Try
            If Not String.IsNullOrEmpty(txtHeadline.Text.Trim()) Or Not String.IsNullOrEmpty(txtComment.Text.Trim()) Or Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                MessageDialog.Show(m1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            If lvwCommentary.SelectedItems.Count > 0 Then
                For Each lvwitm As ListViewItem In lvwCommentary.SelectedItems
                    m_decSequenceNo = lvwitm.SubItems(4).Text.Trim()
                Next
            End If

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ContextMenuItemSelected, DeleteToolStripMenuItem.Text, 1, 0)

            'If m_decSequenceNo <> 0 Then
            '    m_objclsCommentary.DeleteCommentary(m_decSequenceNo)
            '    BindCommentaryData()
            '    m_decSequenceNo = -1
            '    m_intCurrentPeriod = -1
            '    ClearTextboxes()
            'End If

            '''' Delete Copied from Touches
            If m_decSequenceNo <> "-1" Then
                Dim dsDeleteDataPrev As DataSet
                Dim dsDeleteDataNext As DataSet
                Dim dsDeleteDataCurrent As DataSet
                'Dim PeriodPrev As Integer
                'Dim PeriodCurrent As Integer
                Dim TimeElapsed As Integer

                ''Get Prev, Current and Next event
                '' If Next is NULL - YOU DELETED THE LAST EVENT
                ''If Period of Current and Prev are different, You just deleted START PERIOD
                ''Enable START PERIOD, REDUCE COUNT OF CURRENT PERIOD AND SET MAIN CLOCK WITH PREV TIME
                ''BASED ON NEW PERIOD, SET MAIN CLOCK PERIOD AND CLOCK SHOULD BE STOPPED
                ''If Both PERIOD differ, then

                dsDeleteDataNext = m_objclsCommentary.GetCommRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "NEXT")

                If dsDeleteDataNext.Tables(0).Rows.Count = 0 Then
                    dsDeleteDataPrev = m_objclsCommentary.GetCommRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "PREV")
                    dsDeleteDataCurrent = m_objclsCommentary.GetCommRecordBasedOnSeqNumber(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, CType(m_decSequenceNo, Decimal), "EQUAL")

                    If dsDeleteDataPrev.Tables(0).Rows.Count = 0 And CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) <> 0 Then
                        'Special Case - last record deleted
                        Dim Period As Integer
                        Period = CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD"))
                        If Period = 1 Then
                            frmMain.lblPeriod.Text = "1st Half"
                        ElseIf Period = 2 Then
                            frmMain.lblPeriod.Text = "2nd Half"
                        ElseIf Period = 3 Then
                            frmMain.lblPeriod.Text = "1st ET"
                        ElseIf Period = 4 Then
                            frmMain.lblPeriod.Text = "2nd ET"
                        End If
                        frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                        frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                        frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                        Dim StrTime As String = "00:00"
                        Dim strNewTime() As String

                        'Set Main Clock with Prev Seq Time ELapsed
                        StrTime = CalculateTimeElapseAfter(0, Period)
                        strNewTime = StrTime.Split(CChar(":"))
                        frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), 0)
                        m_objclsCommentary.DeleteCommentary(CType(m_decSequenceNo, Decimal))
                        'If frmMain.UdcRunningClock1.URCIsRunning = False Then
                        '    frmMain.UdcRunningClock1.URCToggle()
                        '    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                        'End If
                        DisableControls(True)
                    Else
                        If dsDeleteDataPrev.Tables(0).Rows.Count > 0 Then
                            If CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) <> CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")) Then
                                m_objclsCommentary.DeleteCommentary(CType(m_decSequenceNo, Decimal))
                                If CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")) = 0 Then

                                    m_objclsGameDetails.CurrentPeriod = CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) - 1
                                    ''Set time
                                    Dim StrTime As String = "00:00"
                                    Dim strNewTime() As String

                                    'Set Main Clock with Prev Seq Time ELapsed
                                    If m_objclsGameDetails.CurrentPeriod <> 0 And CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")) = 0 Then
                                        'this is where we deleted the last event and the previous event is a comment in haltime
                                        StrTime = CalculateTimeElapseAfter(0, CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")))
                                        frmMain.lblPeriod.Text = "Halftime"
                                        frmMain.lblPeriod.ForeColor = Color.Red
                                    Else
                                        StrTime = CalculateTimeElapseAfter(0, CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")))
                                    End If

                                    strNewTime = StrTime.Split(CChar(":"))

                                    frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), 0)

                                    If frmMain.UdcRunningClock1.URCIsRunning = True Then
                                        frmMain.UdcRunningClock1.URCToggle()
                                        frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\run.gif")
                                    End If
                                    frmMain.btnClock.Enabled = False
                                    frmMain.btnMinuteDown.Enabled = False
                                    frmMain.btnMinuteUp.Enabled = False
                                    frmMain.btnSecondDown.Enabled = False
                                    frmMain.btnSecondUp.Enabled = False
                                    frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                    frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                    frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                                Else
                                    TimeElapsed = CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                                    frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                    frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                    frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                                    If CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) = 0 Then
                                        m_objclsGameDetails.CurrentPeriod = CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD"))
                                    Else
                                        m_objclsGameDetails.CurrentPeriod = CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) - 1
                                    End If
                                    ''
                                    Dim StrTime As String = "00:00"
                                    Dim strNewTime() As String

                                    'Set Main Clock with Prev Seq Time ELapsed
                                    StrTime = CalculateTimeElapseAfter(TimeElapsed, CInt(dsDeleteDataPrev.Tables(0).Rows(0).Item("PERIOD")))
                                    strNewTime = StrTime.Split(CChar(":"))

                                    frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), 0)

                                    If m_objclsGameDetails.CurrentPeriod = 1 Then
                                        frmMain.lblPeriod.Text = "1st Half"
                                    ElseIf m_objclsGameDetails.CurrentPeriod = 2 Then
                                        frmMain.lblPeriod.Text = "2nd Half"
                                    ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                                        frmMain.lblPeriod.Text = "1st ET"
                                    ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                                        frmMain.lblPeriod.Text = "2nd ET"
                                    End If
                                    If m_objclsGameDetails.CurrentPeriod <> 0 And m_objclsGameDetails.IsHalfTimeSwap Then
                                        SwitchSides()
                                        frmMain.SetHeader2()
                                    End If
                                    frmMain.btnClock.Enabled = True
                                    frmMain.btnMinuteDown.Enabled = True
                                    frmMain.btnMinuteUp.Enabled = True
                                    frmMain.btnSecondDown.Enabled = True
                                    frmMain.btnSecondUp.Enabled = True
                                    If frmMain.UdcRunningClock1.URCIsRunning = False Then
                                        frmMain.UdcRunningClock1.URCToggle()
                                        frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                    End If
                                    txtTime.Enabled = True
                                End If

                            Else
                                m_objclsCommentary.DeleteCommentary(CType(m_decSequenceNo, Decimal))
                                If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") And CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) <> 0 Then
                                    DisableControls(True)
                                    frmMain.btnClock.Enabled = True
                                    frmMain.btnMinuteDown.Enabled = True
                                    frmMain.btnMinuteUp.Enabled = True
                                    frmMain.btnSecondDown.Enabled = True
                                    frmMain.btnSecondUp.Enabled = True
                                    If frmMain.UdcRunningClock1.URCIsRunning = False Then
                                        frmMain.UdcRunningClock1.URCToggle()
                                        frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                    End If
                                    frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                    frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                    frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                                    If m_objclsGameDetails.CurrentPeriod = 1 Then
                                        frmMain.lblPeriod.Text = "1st Half"
                                    ElseIf m_objclsGameDetails.CurrentPeriod = 2 Then
                                        frmMain.lblPeriod.Text = "2nd Half"
                                    ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                                        frmMain.lblPeriod.Text = "1st ET"
                                    ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                                        frmMain.lblPeriod.Text = "2nd ET"
                                    End If
                                End If
                                txtTime.Enabled = True
                            End If

                            ''
                        Else
                            m_objclsCommentary.DeleteCommentary(CType(m_decSequenceNo, Decimal))
                            If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") And CInt(dsDeleteDataCurrent.Tables(0).Rows(0).Item("PERIOD")) <> 0 Then
                                DisableControls(True)
                                frmMain.btnClock.Enabled = True
                                frmMain.btnMinuteDown.Enabled = True
                                frmMain.btnMinuteUp.Enabled = True
                                frmMain.btnSecondDown.Enabled = True
                                frmMain.btnSecondUp.Enabled = True
                                If frmMain.UdcRunningClock1.URCIsRunning = False Then
                                    frmMain.UdcRunningClock1.URCToggle()
                                    frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                End If
                                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)
                                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                                frmMain.EnableMenuItem("EndGameToolStripMenuItem", False)
                            End If
                        End If
                    End If
                    'deleted "FIRST" records from next period
                Else
                    m_objclsCommentary.DeleteCommentary(CType(m_decSequenceNo, Decimal))
                End If
                BindCommentaryData()
                m_decSequenceNo = "-1"
                m_intCurrentPeriod = -1
                ClearTextboxes()

            End If
            ''''End Copy

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To display current time in minutes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTime.Click
        Try
            If m_objclsGameDetails.IsEditMode = False Then
                If frmMain.isMenuItemEnabled("StartPeriodToolStripMenuItem") = False Then
                    If m_objclsGameDetails.CurrentPeriod = 5 Then
                        txtTime.Text = "120"
                        'txtTime.Enabled = False
                    Else
                        If frmMain.isMenuItemEnabled("EndPeriodToolStripMenuItem") = False And frmMain.isMenuItemEnabled("EndGameToolStripMenuItem") = True Then
                            CalculateTime()
                            txtTime.Enabled = False
                        Else
                            txtTime.Enabled = True
                            CalculateTime()
                        End If

                    End If

                Else
                    CalculateTime()
                    txtTime.Enabled = False
                End If
            Else
                If m_intCurrentPeriod = 0 Then
                    txtTime.Enabled = False
                Else
                    txtTime.Enabled = True
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To validate time
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTime.KeyPress
        Try

            If Asc(e.KeyChar) <> 8 Then
                Dim chkRes = True
                chkRes = clsValidation.ValidateNumeric(Convert.ToString(e.KeyChar))
                If chkRes = False Then
                    txtTime.Text = String.Empty
                    MessageDialog.Show(m2, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    e.Handled = True
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' To validate time
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub txtTime_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTime.LostFocus
    '    Try
    '        If Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
    '            Dim chkRes As Boolean = True
    '            chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, txtTime.Text.Trim(), 1, 150)
    '            If chkRes = False And m_objclsGameDetails.CurrentPeriod <> 0 Then
    '                txtTime.Text = String.Empty
    '                essageDialog.Show("Please enter a numeric value between 1-90", Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
    '            Else
    '                ValidateCurrentTime()
    '            End If
    '        End If

    '    Catch ex As Exception
    '        MessageDialog.Show(ex, Me.Text)
    '    End Try
    'End Sub

    Private Sub lvwCommentary_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwCommentary.MouseLeave
        Try
            'm_blnMouseMoveOn = False
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    ''' Showing tooltip of commentary
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lvwCommentary_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvwCommentary.MouseMove
        Try
            Dim lvCommentInfo As ListViewHitTestInfo = lvwCommentary.HitTest(e.Location)
            If (lvCommentInfo.SubItem IsNot Nothing) Then
                If m_strTooltipText <> lvCommentInfo.SubItem.Text Then
                    m_blnMouseMoveOn = False
                    tltpCommentary.RemoveAll()
                End If
                'tltpCommentary.Show(lvCommentInfo.SubItem.Text.ToString(), lvwCommentary, New Point(e.Location.X + 20, e.Location.Y + 20), 2000)
                If m_blnMouseMoveOn = False Then
                    tltpCommentary.Active = True
                    'tltpCommentary.SetToolTip(lvwCommentary, lvCommentInfo.SubItem.Text.ToString())
                    ' Added by Chandramohan to customize tooltip
                    tltpCommentary.SetToolTip(lvwCommentary, filltooltiptext(e))
                    ' code ends for the customization of tooltip
                    tltpCommentary.AutoPopDelay = 30000
                    m_strTooltipText = lvCommentInfo.SubItem.Text
                    m_blnMouseMoveOn = True
                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ' Added by Chandramohan
    ' Function to resize tooltip and customize tooltip for lengthier text
    Private Function filltooltiptext(ByVal e As System.Windows.Forms.MouseEventArgs) As String
        Try
            Dim lvTouchInfo As ListViewHitTestInfo = lvwCommentary.HitTest(e.Location)
            Dim strTooltiptext As String = ""
            Dim intcharloop As Integer = 0
            Dim intCharCount As Integer = 0

            For intcharloop = 0 To lvTouchInfo.SubItem.Text.ToString.Length - 1
                If intCharCount >= 100 Then
                    Do While lvTouchInfo.SubItem.Text.Trim.Substring(intcharloop, 1) <> Chr(32)
                        strTooltiptext = strTooltiptext & lvTouchInfo.SubItem.Text.Trim.Substring(intcharloop, 1)
                        If intcharloop < lvTouchInfo.SubItem.Text.Trim.Length - 1 Then
                            intcharloop = intcharloop + 1
                        Else
                            Exit Do
                        End If
                    Loop

                    If intcharloop < lvTouchInfo.SubItem.Text.Trim.Length - 1 Then
                        strTooltiptext = strTooltiptext & vbCrLf & lvTouchInfo.SubItem.Text.Trim.Substring(intcharloop, 1)
                        intCharCount = 0
                    End If
                Else
                    strTooltiptext = strTooltiptext & lvTouchInfo.SubItem.Text.Trim.Substring(intcharloop, 1)
                    intCharCount = intCharCount + 1
                End If
            Next intcharloop

            Return strTooltiptext
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' To populate listview with an interval of 5sec.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tmrPBP_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrPBP.Tick
        Try
            tmrPBP.Enabled = False
            tmrPBP.Stop()
            BindPBPData()
            GetCurrentPlayersAfterRestart()
            If m_objclsGameDetails.IsRestartGame = False Then
                BindCoachInfo()
            End If

        Catch ex As Exception
            'we need to handle error here.
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, "*************ERROR OCCURED ON REFRESH(Commentary) " & ex.Message & vbNewLine, 1, 1)
        Finally
            tmrPBP.Enabled = True
            tmrPBP.Start()
        End Try
    End Sub

    ''' <summary>
    ''' To populate listview with PBP data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub picRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picRefresh.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.PictureBoxClicked, "Refresh", 1, 0)

            BindPBPData()
            GetCurrentPlayersAfterRestart()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    ''' <summary>
    ''' To refresh players Home/Away
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnRefreshPlayers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshPlayers.Click
        Try
            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefreshPlayers.Text, 1, 0)
            BindCoachInfo()
            'BindTeamRostersByLineUp()
            'SortTeamPlayers()
            GetCurrentPlayersAfterRestart()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub txtHeadline_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHeadline.Validated
        Try
            'Audit trial added by Shravani
            If txtHeadline.Text <> m_strTxtHeadline Then
                If txtHeadline.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtHeadline.Text, lblHeadline.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtHeadline.Text & "' '", lblHeadline.Text, 1, 0)
                End If
            End If
            m_strTxtHeadline = txtHeadline.Text

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtTime_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTime.Validated
        Try

            If Not String.IsNullOrEmpty(txtTime.Text.Trim()) Then
                Dim chkRes As Boolean = True
                chkRes = clsValidation.ValidateRange(clsValidation.RangeValidation.INTEGER, txtTime.Text.Trim(), 1, 150)
                If chkRes = False And m_objclsGameDetails.CurrentPeriod <> 0 Then
                    txtTime.Text = String.Empty
                    MessageDialog.Show(m5, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                Else
                    ValidateCurrentTime()
                End If
            End If

            If txtTime.Text <> m_strTxtTime Then
                If txtTime.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTime.Text, lblTime.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTime.Text & "' '", lblTime.Text, 1, 0)
                End If
            End If
            m_strTxtTime = txtTime.Text
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtComment_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtComment.Validated
        Try
            If txtComment.Text <> m_strTxtComment Then
                If txtComment.Text <> Nothing Then
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtComment.Text, Label4.Text, 1, 0)
                Else
                    m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtComment.Text & "' '", lblTime.Text, 1, 0)
                End If
            End If
            m_strTxtComment = txtComment.Text

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try

    End Sub

#End Region

#Region "User Defined Functions"
    ''' <summary>
    ''' To populate controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindControls()
        Try
            BindTeamRosters()
            SortTeamPlayers()
            BindCoachInfo()
            'BindCommentaryData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' To Populate Home Team and Visit Team Players
    ''' </summary>
    ''' <remarks>First checking TeamSetup Dataset and fills controls. If Dataset is null fill from local SQL Server Roster table</remarks>
    Private Sub BindTeamRosters()
        Try

            Dim dsLineUp As DataSet = m_objclsTeamSetup.RosterInfo()
            Dim blnChkTeam As Boolean = False

            If dsLineUp IsNot Nothing Then
                If dsLineUp.Tables.Count > 0 Then
                    If dsLineUp.Tables("Home").Rows.Count > 0 And dsLineUp.Tables("Away").Rows.Count > 0 Then
                        blnChkTeam = True
                        ButtonsHomeTeamTagged(dsLineUp.Tables("Home"), True)
                        ButtonsVisitTeamTagged(dsLineUp.Tables("Away"), True)

                        pnlHome.AutoScroll = False
                        pnlVisit.AutoScroll = False
                        pnlHome.Height = 305
                        pnlVisit.Height = 305
                        pnlHomeBench.Visible = True
                        pnlVisitBench.Visible = True

                        m_blnHomeLineUp = True
                        m_blnVisitLineUp = True
                    End If
                Else
                    pnlHome.AutoScroll = True
                    pnlVisit.AutoScroll = True
                    pnlHome.Height = 430
                    pnlVisit.Height = 430
                    pnlHomeBench.Visible = False
                    pnlVisitBench.Visible = False
                End If
            End If

            If blnChkTeam = False Then
                'Home Team Roster
                'Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.HomeTeamID)
                dsHomeTeam = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
                If dsHomeTeam IsNot Nothing Then
                    If dsHomeTeam.Tables.Count > 0 Then
                        dsHomeTeam.Tables(0).TableName = "Home"
                        If dsHomeTeam.Tables("Home").Rows.Count > 0 Then
                            ButtonsHomeTeamTagged(dsHomeTeam.Tables("Home"), False)
                            m_blnHomeLineUp = False
                        End If
                    End If
                End If

                'Away TeamRoster
                'Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.AwayTeamID)
                dsAwayTeam = m_objGeneral.GetTeamRostersforCommentaryandTouches(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
                If dsAwayTeam IsNot Nothing Then
                    If dsAwayTeam.Tables.Count > 0 Then
                        dsAwayTeam.Tables(0).TableName = "Away"
                        If dsAwayTeam.Tables("Away").Rows.Count > 0 Then
                            ButtonsVisitTeamTagged(dsAwayTeam.Tables("Away"), False)
                            m_blnVisitLineUp = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To populate Home Team and Visit Team Players by LineUps
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindTeamRostersByLineUp()
        Try
            Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
            If dsHomeTeam IsNot Nothing Then
                If dsHomeTeam.Tables.Count > 0 Then
                    dsHomeTeam.Tables(0).TableName = "Home"
                    If dsHomeTeam.Tables("Home").Rows.Count > 0 Then
                        ButtonsHomeTeamTagged(dsHomeTeam.Tables("Home"), True)
                        m_blnHomeLineUp = True
                    End If
                End If
            End If

            'Away TeamRoster
            Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
            If dsAwayTeam IsNot Nothing Then
                If dsAwayTeam.Tables.Count > 0 Then
                    dsAwayTeam.Tables(0).TableName = "Away"
                    If dsAwayTeam.Tables("Away").Rows.Count > 0 Then
                        ButtonsVisitTeamTagged(dsAwayTeam.Tables("Away"), True)
                        m_blnVisitLineUp = True
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '' Arindam 23-Aug
    Private Sub PopulateDropdowns(ByVal OriginalDataset As DataSet, Optional ByVal Combo As ComboBox = Nothing)
        Try
            blnSkipSEChanged = True

            Dim RunningDataset As DataSet = OriginalDataset.Copy
            Dim strPrevSelectedValue As String = ""

            If Not Combo Is Nothing Then
                If Not Combo.Name = "cmbCommentType" Then
                    If Not cmbCommentType.SelectedValue Is Nothing Then
                        strPrevSelectedValue = cmbCommentType.SelectedValue.ToString
                    Else
                        strPrevSelectedValue = ""
                    End If

                    cmbCommentType.DataSource = Nothing
                    Dim dr() As DataRow

                    'cmbReferee.DataSource = RunningDataset.Tables(0)
                    'cmbReferee.DisplayMember = RunningDataset.Tables(0).Columns(1).ToString
                    'cmbReferee.ValueMember = RunningDataset.Tables(0).Columns(0).ToString
                    LoadControl(cmbCommentType, RunningDataset.Tables(0), "COMMENTARY_TYPE", "COMMENTARY_DESC")

                    If strPrevSelectedValue = "" Then
                        cmbCommentType.SelectedIndex = -1
                    Else
                        cmbCommentType.SelectedValue = strPrevSelectedValue
                    End If
                End If

            Else

                'cmbReferee.DataSource = RunningDataset.Tables(0)
                'cmbReferee.DisplayMember = RunningDataset.Tables(0).Columns(1).ToString
                'cmbReferee.ValueMember = RunningDataset.Tables(0).Columns(0).ToString
                LoadControl(cmbCommentType, RunningDataset.Tables(0), "COMMENTARY_TYPE", "COMMENTARY_DESC")
                cmbCommentType.SelectedIndex = -1

            End If

            blnSkipSEChanged = False
        Catch ex As Exception
            blnSkipSEChanged = False
            Throw ex
        End Try
    End Sub

    Private Sub LoadControl(ByVal Combo As ComboBox, ByVal GameSetup As DataTable, ByVal DisplayColumnName As String, ByVal DataColumnName As String)
        Try
            Dim drTempRow As DataRow
            Combo.DataSource = Nothing
            Combo.Items.Clear()

            Dim acscRosterData As New AutoCompleteStringCollection()

            Dim intLoop As Integer
            For intLoop = 0 To GameSetup.Rows.Count - 1
                acscRosterData.Add(GameSetup.Rows(intLoop)(1).ToString().Trim)
            Next

            Dim dtLoadData As New DataTable

            dtLoadData = GameSetup.Copy()
            drTempRow = dtLoadData.NewRow()
            drTempRow(0) = 0
            drTempRow(1) = ""
            dtLoadData.Rows.InsertAt(drTempRow, 0)
            Combo.DataSource = dtLoadData
            Combo.ValueMember = dtLoadData.Columns(0).ToString()
            Combo.DisplayMember = dtLoadData.Columns(1).ToString()

            Combo.DropDownStyle = ComboBoxStyle.DropDown
            Combo.AutoCompleteSource = AutoCompleteSource.CustomSource
            Combo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Combo.AutoCompleteCustomSource = acscRosterData

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' To assign tags to Home Team Player Buttons
    ''' </summary>
    ''' <param name="dtHomeTeamRoster"></param>
    ''' <remarks></remarks>
    'Private Sub ButtonsHomeTeamTagged(ByVal dtHomeTeamRoster As DataTable)
    '    Try
    '        Dim intBtnCntr As Integer = 1
    '        Dim btn As Button
    '        Dim hthome As New Hashtable
    '        Dim hthomesub As New Hashtable
    '        Dim htaway As New Hashtable
    '        Dim htvisitingsub As New Hashtable
    '        Dim strPlayerLastName As String
    '        Dim dtHomeLineUpRosters As New DataTable

    '        'Clear buttons text of home team
    '        ClearHomeButtonText()

    '        hthome.Add(1, Me.btnHomePlayer1)
    '        hthome.Add(2, Me.btnHomePlayer2)
    '        hthome.Add(3, Me.btnHomePlayer3)
    '        hthome.Add(4, Me.btnHomePlayer4)
    '        hthome.Add(5, Me.btnHomePlayer5)
    '        hthome.Add(6, Me.btnHomePlayer6)
    '        hthome.Add(7, Me.btnHomePlayer7)
    '        hthome.Add(8, Me.btnHomePlayer8)
    '        hthome.Add(9, Me.btnHomePlayer9)
    '        hthome.Add(10, Me.btnHomePlayer10)
    '        hthome.Add(11, Me.btnHomePlayer11)

    '        If dtHomeTeamRoster Is Nothing Then
    '            Exit Sub
    '        End If
    '        Dim strSortOrder As String = ""
    '        If m_objclsLoginDetails.strSortOrder = "Uniform" Then
    '            strSortOrder = "SORT_NUMBER"
    '        ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
    '            strSortOrder = "STARTING_POSITION"
    '        ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
    '            strSortOrder = "LAST_NAME"
    '        End If

    '        If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then

    '            For intBtnCntr = 1 To 11
    '                btn = CType(hthome.Item(intBtnCntr), Button)
    '                If dtHomeTeamRoster.Rows.Count >= intBtnCntr Then
    '                    Dim strFullName As String = String.Empty
    '                    If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
    '                        strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
    '                    End If
    '                    If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & " - " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        Else
    '                            strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        End If
    '                    End If
    '                    If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & ", " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        Else
    '                            strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        End If
    '                    End If

    '                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                        btn.Text = strFullName
    '                    End If

    '                    btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                End If
    '            Next
    '        Else

    '            Dim dtrowHomeLineUps As DataRow()
    '            If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
    '                dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
    '            Else
    '                dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
    '            End If

    '            dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
    '            For Each drHomeLineUps As DataRow In dtrowHomeLineUps
    '                dtHomeLineUpRosters.ImportRow(drHomeLineUps)
    '            Next
    '            For intBtnCntr = 1 To 11
    '                btn = CType(hthome.Item(intBtnCntr), Button)
    '                If dtHomeLineUpRosters.Rows.Count >= intBtnCntr Then
    '                    Dim strFullName As String = String.Empty
    '                    If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
    '                        strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
    '                    End If
    '                    If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & " - " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        Else
    '                            strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        End If
    '                    End If
    '                    If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & ", " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        Else
    '                            strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        End If
    '                    End If

    '                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                        btn.Text = strFullName
    '                    End If

    '                    btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                End If
    '            Next
    '        End If

    '        hthomesub.Add(1, Me.btnHomeSub1)
    '        hthomesub.Add(2, Me.btnHomeSub2)
    '        hthomesub.Add(3, Me.btnHomeSub3)
    '        hthomesub.Add(4, Me.btnHomeSub4)
    '        hthomesub.Add(5, Me.btnHomeSub5)
    '        hthomesub.Add(6, Me.btnHomeSub6)
    '        hthomesub.Add(7, Me.btnHomeSub7)
    '        hthomesub.Add(8, Me.btnHomeSub8)
    '        hthomesub.Add(9, Me.btnHomeSub9)
    '        hthomesub.Add(10, Me.btnHomeSub10)
    '        hthomesub.Add(11, Me.btnHomeSub11)
    '        hthomesub.Add(12, Me.btnHomeSub12)
    '        hthomesub.Add(13, Me.btnHomeSub13)
    '        hthomesub.Add(14, Me.btnHomeSub14)
    '        hthomesub.Add(15, Me.btnHomeSub15)
    '        hthomesub.Add(16, Me.btnHomeSub16)
    '        hthomesub.Add(17, Me.btnHomeSub17)
    '        hthomesub.Add(18, Me.btnHomeSub18)
    '        hthomesub.Add(19, Me.btnHomeSub19)
    '        hthomesub.Add(20, Me.btnHomeSub20)
    '        hthomesub.Add(21, Me.btnHomeSub21)
    '        hthomesub.Add(22, Me.btnHomeSub22)
    '        hthomesub.Add(23, Me.btnHomeSub23)
    '        hthomesub.Add(24, Me.btnHomeSub24)
    '        hthomesub.Add(25, Me.btnHomeSub25)
    '        hthomesub.Add(26, Me.btnHomeSub26)
    '        hthomesub.Add(27, Me.btnHomeSub27)
    '        hthomesub.Add(28, Me.btnHomeSub28)
    '        hthomesub.Add(29, Me.btnHomeSub29)
    '        hthomesub.Add(30, Me.btnHomeSub30)

    '        If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then
    '            If dtHomeTeamRoster.Rows.Count > 11 Then
    '                For intBtnCntr = 12 To dtHomeTeamRoster.Rows.Count
    '                    btn = CType(hthomesub.Item(intBtnCntr - 11), Button)
    '                    strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                    If strPlayerLastName.Length > 6 Then
    '                        strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
    '                    End If
    '                    If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
    '                        btn.Text = strPlayerLastName
    '                    Else
    '                        btn.Text = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
    '                    End If
    '                    btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                    btn.Visible = True
    '                Next
    '            End If
    '        Else

    '            Dim dtrowHomeLineUps As DataRow()
    '            If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
    '                dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
    '            Else
    '                dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
    '            End If
    '            dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
    '            For Each drHomeLineUps As DataRow In dtrowHomeLineUps
    '                dtHomeLineUpRosters.ImportRow(drHomeLineUps)
    '            Next
    '            For intBtnCntr = 1 To dtHomeLineUpRosters.Rows.Count
    '                btn = CType(hthomesub.Item(intBtnCntr), Button)
    '                strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                If strPlayerLastName.Length > 6 Then
    '                    strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
    '                End If
    '                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
    '                    btn.Text = strPlayerLastName
    '                Else
    '                    btn.Text = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
    '                End If
    '                btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                btn.Visible = True
    '            Next
    '        End If

    '        m_dtHome = dtHomeTeamRoster
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub ButtonsHomeTeamTagged(ByVal dtHomeTeamRoster As DataTable, ByVal blnLineup As Boolean)
        Try
            Dim intBtnCntr As Integer = 1
            Dim btn As Button
            Dim hthome As New Hashtable
            Dim hthomesub As New Hashtable
            Dim htaway As New Hashtable
            Dim htvisitingsub As New Hashtable
            Dim strPlayerLastName As String
            Dim dtHomeLineUpRosters As New DataTable

            'Clear buttons text of home team
            ClearHomeButtonText()

            If blnLineup = False Then

                hthome.Add(1, Me.btnHomePlayer1)
                hthome.Add(2, Me.btnHomePlayer2)
                hthome.Add(3, Me.btnHomePlayer3)
                hthome.Add(4, Me.btnHomePlayer4)
                hthome.Add(5, Me.btnHomePlayer5)
                hthome.Add(6, Me.btnHomePlayer6)
                hthome.Add(7, Me.btnHomePlayer7)
                hthome.Add(8, Me.btnHomePlayer8)
                hthome.Add(9, Me.btnHomePlayer9)
                hthome.Add(10, Me.btnHomePlayer10)
                hthome.Add(11, Me.btnHomePlayer11)
                hthome.Add(12, Me.btnHomePlayer12)
                hthome.Add(13, Me.btnHomePlayer13)
                hthome.Add(14, Me.btnHomePlayer14)
                hthome.Add(15, Me.btnHomePlayer15)
                hthome.Add(16, Me.btnHomePlayer16)
                hthome.Add(17, Me.btnHomePlayer17)
                hthome.Add(18, Me.btnHomePlayer18)
                hthome.Add(19, Me.btnHomePlayer19)
                hthome.Add(20, Me.btnHomePlayer20)
                hthome.Add(21, Me.btnHomePlayer21)
                hthome.Add(22, Me.btnHomePlayer22)
                hthome.Add(23, Me.btnHomePlayer23)
                hthome.Add(24, Me.btnHomePlayer24)
                hthome.Add(25, Me.btnHomePlayer25)
                hthome.Add(26, Me.btnHomePlayer26)
                hthome.Add(27, Me.btnHomePlayer27)
                hthome.Add(28, Me.btnHomePlayer28)
                hthome.Add(29, Me.btnHomePlayer29)
                hthome.Add(30, Me.btnHomePlayer30)
                hthome.Add(31, Me.btnHomePlayer31)
                hthome.Add(32, Me.btnHomePlayer32)
                hthome.Add(33, Me.btnHomePlayer33)
                hthome.Add(34, Me.btnHomePlayer34)
                hthome.Add(35, Me.btnHomePlayer35)
                hthome.Add(36, Me.btnHomePlayer36)
                hthome.Add(37, Me.btnHomePlayer37)
                hthome.Add(38, Me.btnHomePlayer38)
                hthome.Add(39, Me.btnHomePlayer39)
                hthome.Add(40, Me.btnHomePlayer40)
                hthome.Add(41, Me.btnHomePlayer41)

                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    For intBtnCntr = 1 To dtHomeTeamRoster.Rows.Count
                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            If dtHomeTeamRoster.Rows.Count >= intBtnCntr Then

                                Dim strFullName As String = String.Empty
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                st1 = ""
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    st1 = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    st1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st1)
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & st1
                                        'strFullName = strFullName & " - " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = st1
                                        'strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                st2 = ""
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    st2 = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    st2 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st2)
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & st2
                                        'strFullName = strFullName & ", " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = st2
                                        'strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If
                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                    btn.Tag = st2 + " " + st1 + "*" + st2 + " " + st1 + "*" + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                Else
                                    btn.Tag = st2 + " " + st1 + "*" + st2 + " " + st1 + "*" + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                    'btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    'btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                            End If
                            btn.Visible = True
                        End If
                    Next

                    If dtHomeTeamRoster.Rows.Count > 17 Then
                        pnlHome.AutoScroll = True
                    Else
                        pnlHome.AutoScroll = False
                    End If
                    m_dtHome = dtHomeTeamRoster
                End If
            Else   ' if lineups availabale

                hthome.Add(1, Me.btnHomePlayer1)
                hthome.Add(2, Me.btnHomePlayer2)
                hthome.Add(3, Me.btnHomePlayer3)
                hthome.Add(4, Me.btnHomePlayer4)
                hthome.Add(5, Me.btnHomePlayer5)
                hthome.Add(6, Me.btnHomePlayer6)
                hthome.Add(7, Me.btnHomePlayer7)
                hthome.Add(8, Me.btnHomePlayer8)
                hthome.Add(9, Me.btnHomePlayer9)
                hthome.Add(10, Me.btnHomePlayer10)
                hthome.Add(11, Me.btnHomePlayer11)

                If dtHomeTeamRoster Is Nothing Then
                    Exit Sub
                End If
                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then

                    For intBtnCntr = 1 To 11
                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            If dtHomeTeamRoster.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                st1 = ""
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    st1 = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    st1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st1)
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & st1
                                        'strFullName = strFullName & " - " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = st1
                                        'strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                st2 = ""
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    st2 = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    st2 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st2)
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & st2
                                        'strFullName = strFullName & ", " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = st2
                                        'strFullName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                btn.Tag = st2 + " " + st1 + "*" + st2 + " " + st1 + "*" + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            End If
                        End If
                    Next
                Else

                    Dim dtrowHomeLineUps As DataRow()
                    If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If

                    dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
                    For Each drHomeLineUps As DataRow In dtrowHomeLineUps
                        dtHomeLineUpRosters.ImportRow(drHomeLineUps)
                    Next
                    For intBtnCntr = 1 To 11
                        btn = CType(hthome.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            st3 = ""
                            If dtHomeLineUpRosters.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                st3 = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                st3 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st3)
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & " - " & st3
                                        'strFullName = strFullName & " - " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = st3
                                        'strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                st4 = ""
                                If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    st4 = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    st4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st4)
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & st4
                                        'strFullName = strFullName & ", " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = st4
                                        'strFullName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                btn.Tag = st4 + " " + st3 + "*" + st4 + " " + st3 + "*" + dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            End If
                        End If
                    Next
                End If
                hthomesub.Add(1, Me.btnHomeSub1)
                hthomesub.Add(2, Me.btnHomeSub2)
                hthomesub.Add(3, Me.btnHomeSub3)
                hthomesub.Add(4, Me.btnHomeSub4)
                hthomesub.Add(5, Me.btnHomeSub5)
                hthomesub.Add(6, Me.btnHomeSub6)
                hthomesub.Add(7, Me.btnHomeSub7)
                hthomesub.Add(8, Me.btnHomeSub8)
                hthomesub.Add(9, Me.btnHomeSub9)
                hthomesub.Add(10, Me.btnHomeSub10)
                hthomesub.Add(11, Me.btnHomeSub11)
                hthomesub.Add(12, Me.btnHomeSub12)
                hthomesub.Add(13, Me.btnHomeSub13)
                hthomesub.Add(14, Me.btnHomeSub14)
                hthomesub.Add(15, Me.btnHomeSub15)
                hthomesub.Add(16, Me.btnHomeSub16)
                hthomesub.Add(17, Me.btnHomeSub17)
                hthomesub.Add(18, Me.btnHomeSub18)
                hthomesub.Add(19, Me.btnHomeSub19)
                hthomesub.Add(20, Me.btnHomeSub20)
                hthomesub.Add(21, Me.btnHomeSub21)
                hthomesub.Add(22, Me.btnHomeSub22)
                hthomesub.Add(23, Me.btnHomeSub23)
                hthomesub.Add(24, Me.btnHomeSub24)
                hthomesub.Add(25, Me.btnHomeSub25)
                hthomesub.Add(26, Me.btnHomeSub26)
                hthomesub.Add(27, Me.btnHomeSub27)
                hthomesub.Add(28, Me.btnHomeSub28)
                hthomesub.Add(29, Me.btnHomeSub29)
                hthomesub.Add(30, Me.btnHomeSub30)

                If Not dtHomeTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    If dtHomeTeamRoster.Rows.Count > 11 Then
                        For intBtnCntr = 12 To dtHomeTeamRoster.Rows.Count
                            btn = CType(hthomesub.Item(intBtnCntr - 11), Button)
                            st5 = ""
                            If btn IsNot Nothing Then
                                strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                st5 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strPlayerLastName)
                                strPlayerLastName = st5
                                If strPlayerLastName.Length > 6 Then
                                    st5 = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6)
                                    st5 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st5)
                                    strPlayerLastName = st5 & "."
                                    'strPlayerLastName = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                                End If
                                st6 = ""
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                    btn.Text = strPlayerLastName
                                Else
                                    st6 = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                    st6 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st6)
                                    btn.Text = st6 + vbNewLine + strPlayerLastName
                                    'btn.Text = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                End If
                                Dim temmoniker = ""
                                If dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    temmoniker = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                End If
                                temmoniker = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker)
                                btn.Tag = temmoniker + " " + st5 + "*" + temmoniker + " " + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString() + "*" + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'btn.Tag = dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                btn.Visible = True
                            End If
                        Next
                    End If
                Else

                    Dim dtrowHomeLineUps As DataRow()
                    Dim st7 As String
                    Dim st8 As String
                    If Not dtHomeTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowHomeLineUps = dtHomeTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    dtHomeLineUpRosters = dtHomeTeamRoster.Clone()
                    For Each drHomeLineUps As DataRow In dtrowHomeLineUps
                        dtHomeLineUpRosters.ImportRow(drHomeLineUps)
                    Next
                    For intBtnCntr = 1 To dtHomeLineUpRosters.Rows.Count
                        btn = CType(hthomesub.Item(intBtnCntr), Button)
                        st7 = ""
                        If btn IsNot Nothing Then

                            st7 = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            st7 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), st7)
                            strPlayerLastName = st7
                            'strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            If strPlayerLastName.Length > 6 Then
                                strPlayerLastName = st7
                                'strPlayerLastName = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                            End If
                            If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                btn.Text = strPlayerLastName
                            Else
                                btn.Text = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                            End If
                            Dim temmoniker = ""
                            If dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                temmoniker = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                            End If

                            btn.Tag = temmoniker + " " + st7 + "*" + temmoniker + " " + dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString() + "*" + dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                            'btn.Tag = dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtHomeLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            btn.Visible = True
                        End If
                    Next
                End If
                m_dtHome = dtHomeTeamRoster
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To clear button text for visit team players
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearVisitButtonText()

        For Each ctrl As Control In Me.Controls("pnlVisitBench").Controls
            If ctrl.Name <> "btnVisitTeam" Then
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    CType(ctrl, Button).Text = ""
                    CType(ctrl, Button).Visible = False
                End If
            End If
        Next

        btnVisitPlayer1.Text = ""
        btnVisitPlayer2.Text = ""
        btnVisitPlayer3.Text = ""
        btnVisitPlayer4.Text = ""
        btnVisitPlayer5.Text = ""
        btnVisitPlayer6.Text = ""
        btnVisitPlayer7.Text = ""
        btnVisitPlayer8.Text = ""
        btnVisitPlayer9.Text = ""
        btnVisitPlayer10.Text = ""
        btnVisitPlayer11.Text = ""

        btnVisitSub1.Visible = True
        btnVisitSub2.Visible = True
        btnVisitSub3.Visible = True
        btnVisitSub4.Visible = True
        btnVisitSub5.Visible = True
        btnVisitSub6.Visible = True
        btnVisitSub7.Visible = True
        btnVisitSub8.Visible = True
        btnVisitSub9.Visible = True
    End Sub

    ''' <summary>
    ''' To clear button text for home team players
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearHomeButtonText()
        For Each ctrl As Control In Me.Controls("pnlHomeBench").Controls
            If ctrl.Name <> "btnHomeTeam" Then
                If ctrl.GetType().ToString = "System.Windows.Forms.Button" Then
                    CType(ctrl, Button).Text = ""
                    CType(ctrl, Button).Visible = False
                End If
            End If
        Next

        btnHomePlayer1.Text = ""
        btnHomePlayer2.Text = ""
        btnHomePlayer3.Text = ""
        btnHomePlayer4.Text = ""
        btnHomePlayer5.Text = ""
        btnHomePlayer6.Text = ""
        btnHomePlayer7.Text = ""
        btnHomePlayer8.Text = ""
        btnHomePlayer9.Text = ""
        btnHomePlayer10.Text = ""
        btnHomePlayer11.Text = ""

        btnHomeSub1.Visible = True
        btnHomeSub2.Visible = True
        btnHomeSub3.Visible = True
        btnHomeSub4.Visible = True
        btnHomeSub5.Visible = True
        btnHomeSub6.Visible = True
        btnHomeSub7.Visible = True
        btnHomeSub8.Visible = True
        btnHomeSub9.Visible = True

    End Sub

    '''' <summary>
    '''' To assign tags to Visit Team Player Buttons
    '''' </summary>
    '''' <param name="dtAwayTeamRoster"></param>
    '''' <remarks></remarks>
    'Private Sub ButtonsVisitTeamTagged(ByVal dtAwayTeamRoster As DataTable)
    '    Try
    '        Dim intBtnCntr As Integer = 1
    '        Dim btn As Button
    '        Dim htVisit As New Hashtable
    '        Dim htVisitsub As New Hashtable
    '        Dim htaway As New Hashtable
    '        Dim htvisitingsub As New Hashtable
    '        Dim strPlayerLastName As String
    '        Dim dtVisitLineUpRosters As New DataTable
    '        'Clear buttons text of Visit team
    '        ClearVisitButtonText()

    '        htVisit.Add(1, Me.btnVisitPlayer1)
    '        htVisit.Add(2, Me.btnVisitPlayer2)
    '        htVisit.Add(3, Me.btnVisitPlayer3)
    '        htVisit.Add(4, Me.btnVisitPlayer4)
    '        htVisit.Add(5, Me.btnVisitPlayer5)
    '        htVisit.Add(6, Me.btnVisitPlayer6)
    '        htVisit.Add(7, Me.btnVisitPlayer7)
    '        htVisit.Add(8, Me.btnVisitPlayer8)
    '        htVisit.Add(9, Me.btnVisitPlayer9)
    '        htVisit.Add(10, Me.btnVisitPlayer10)
    '        htVisit.Add(11, Me.btnVisitPlayer11)

    '        If dtAwayTeamRoster Is Nothing Then
    '            Exit Sub
    '        End If
    '        Dim strSortOrder As String = ""
    '        If m_objclsLoginDetails.strSortOrder = "Uniform" Then
    '            strSortOrder = "SORT_NUMBER"
    '        ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
    '            strSortOrder = "STARTING_POSITION"
    '        ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
    '            strSortOrder = "LAST_NAME"
    '        End If

    '        If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then

    '            For intBtnCntr = 1 To 11
    '                btn = CType(htVisit.Item(intBtnCntr), Button)
    '                If dtAwayTeamRoster.Rows.Count >= intBtnCntr Then
    '                    Dim strFullName As String = String.Empty
    '                    If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
    '                        strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
    '                    End If
    '                    If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & " - " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        Else
    '                            strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        End If
    '                    End If
    '                    If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & ", " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        Else
    '                            strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        End If
    '                    End If

    '                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                        btn.Text = strFullName
    '                    End If
    '                    btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                End If
    '            Next
    '        Else

    '            Dim dtrowVisitLineUps As DataRow()
    '            If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
    '                dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
    '            Else
    '                dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
    '            End If
    '            dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
    '            For Each drVisitLineUps As DataRow In dtrowVisitLineUps
    '                dtVisitLineUpRosters.ImportRow(drVisitLineUps)
    '            Next
    '            For intBtnCntr = 1 To 11
    '                btn = CType(htVisit.Item(intBtnCntr), Button)
    '                If dtVisitLineUpRosters.Rows.Count >= intBtnCntr Then
    '                    Dim strFullName As String = String.Empty
    '                    If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
    '                        strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
    '                    End If
    '                    If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & " - " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        Else
    '                            strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                        End If
    '                    End If
    '                    If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
    '                        If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                            strFullName = strFullName & ", " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        Else
    '                            strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
    '                        End If
    '                    End If

    '                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
    '                        btn.Text = strFullName
    '                    End If
    '                    btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                End If
    '            Next
    '        End If

    '        htVisitsub.Add(1, Me.btnVisitSub1)
    '        htVisitsub.Add(2, Me.btnVisitSub2)
    '        htVisitsub.Add(3, Me.btnVisitSub3)
    '        htVisitsub.Add(4, Me.btnVisitSub4)
    '        htVisitsub.Add(5, Me.btnVisitSub5)
    '        htVisitsub.Add(6, Me.btnVisitSub6)
    '        htVisitsub.Add(7, Me.btnVisitSub7)
    '        htVisitsub.Add(8, Me.btnVisitSub8)
    '        htVisitsub.Add(9, Me.btnVisitSub9)
    '        htVisitsub.Add(10, Me.btnVisitSub10)
    '        htVisitsub.Add(11, Me.btnVisitSub11)
    '        htVisitsub.Add(12, Me.btnVisitSub12)
    '        htVisitsub.Add(13, Me.btnVisitSub13)
    '        htVisitsub.Add(14, Me.btnVisitSub14)
    '        htVisitsub.Add(15, Me.btnVisitSub15)
    '        htVisitsub.Add(16, Me.btnVisitSub16)
    '        htVisitsub.Add(17, Me.btnVisitSub17)
    '        htVisitsub.Add(18, Me.btnVisitSub18)
    '        htVisitsub.Add(19, Me.btnVisitSub19)
    '        htVisitsub.Add(20, Me.btnVisitSub20)
    '        htVisitsub.Add(21, Me.btnVisitSub21)
    '        htVisitsub.Add(22, Me.btnVisitSub22)
    '        htVisitsub.Add(23, Me.btnVisitSub23)
    '        htVisitsub.Add(24, Me.btnVisitSub24)
    '        htVisitsub.Add(25, Me.btnVisitSub25)
    '        htVisitsub.Add(26, Me.btnVisitSub26)
    '        htVisitsub.Add(27, Me.btnVisitSub27)
    '        htVisitsub.Add(28, Me.btnVisitSub28)
    '        htVisitsub.Add(29, Me.btnVisitSub29)
    '        htVisitsub.Add(30, Me.btnVisitSub30)
    '        If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then

    '            If dtAwayTeamRoster.Rows.Count > 11 Then
    '                For intBtnCntr = 12 To dtAwayTeamRoster.Rows.Count
    '                    btn = CType(htVisitsub.Item(intBtnCntr - 11), Button)
    '                    strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                    If strPlayerLastName.Length > 6 Then
    '                        strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
    '                    End If
    '                    If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
    '                        btn.Text = strPlayerLastName
    '                    Else
    '                        btn.Text = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
    '                    End If
    '                    btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                    btn.Visible = True
    '                Next
    '            End If
    '        Else

    '            Dim dtrowVisitLineUps As DataRow()
    '            If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
    '                dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
    '            Else
    '                dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
    '            End If
    '            dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
    '            For Each drVisitLineUps As DataRow In dtrowVisitLineUps
    '                dtVisitLineUpRosters.ImportRow(drVisitLineUps)
    '            Next
    '            For intBtnCntr = 1 To dtVisitLineUpRosters.Rows.Count
    '                btn = CType(htVisitsub.Item(intBtnCntr), Button)
    '                strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                If strPlayerLastName.Length > 6 Then
    '                    strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
    '                End If
    '                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
    '                    btn.Text = strPlayerLastName
    '                Else
    '                    btn.Text = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
    '                End If
    '                btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
    '                btn.Visible = True
    '            Next
    '        End If
    '        m_dtAway = dtAwayTeamRoster
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub ButtonsVisitTeamTagged(ByVal dtAwayTeamRoster As DataTable, ByVal blnLineUp As Boolean)
        Try
            Dim intBtnCntr As Integer = 1
            Dim btn As Button
            Dim htVisit As New Hashtable
            Dim htVisitsub As New Hashtable
            Dim htaway As New Hashtable
            Dim htvisitingsub As New Hashtable
            Dim strPlayerLastName As String
            Dim dtVisitLineUpRosters As New DataTable
            'Clear buttons text of Visit team
            ClearVisitButtonText()
            If blnLineUp = False Then

                htVisit.Add(1, Me.btnVisitPlayer1)
                htVisit.Add(2, Me.btnVisitPlayer2)
                htVisit.Add(3, Me.btnVisitPlayer3)
                htVisit.Add(4, Me.btnVisitPlayer4)
                htVisit.Add(5, Me.btnVisitPlayer5)
                htVisit.Add(6, Me.btnVisitPlayer6)
                htVisit.Add(7, Me.btnVisitPlayer7)
                htVisit.Add(8, Me.btnVisitPlayer8)
                htVisit.Add(9, Me.btnVisitPlayer9)
                htVisit.Add(10, Me.btnVisitPlayer10)
                htVisit.Add(11, Me.btnVisitPlayer11)
                htVisit.Add(12, Me.btnVisitPlayer12)
                htVisit.Add(13, Me.btnVisitPlayer13)
                htVisit.Add(14, Me.btnVisitPlayer14)
                htVisit.Add(15, Me.btnVisitPlayer15)
                htVisit.Add(16, Me.btnVisitPlayer16)
                htVisit.Add(17, Me.btnVisitPlayer17)
                htVisit.Add(18, Me.btnVisitPlayer18)
                htVisit.Add(19, Me.btnVisitPlayer19)
                htVisit.Add(20, Me.btnVisitPlayer20)
                htVisit.Add(21, Me.btnVisitPlayer21)
                htVisit.Add(22, Me.btnVisitPlayer22)
                htVisit.Add(23, Me.btnVisitPlayer23)
                htVisit.Add(24, Me.btnVisitPlayer24)
                htVisit.Add(25, Me.btnVisitPlayer25)
                htVisit.Add(26, Me.btnVisitPlayer26)
                htVisit.Add(27, Me.btnVisitPlayer27)
                htVisit.Add(28, Me.btnVisitPlayer28)
                htVisit.Add(29, Me.btnVisitPlayer29)
                htVisit.Add(30, Me.btnVisitPlayer30)
                htVisit.Add(31, Me.btnVisitPlayer31)
                htVisit.Add(32, Me.btnVisitPlayer32)
                htVisit.Add(33, Me.btnVisitPlayer33)
                htVisit.Add(34, Me.btnVisitPlayer34)
                htVisit.Add(35, Me.btnVisitPlayer35)
                htVisit.Add(36, Me.btnVisitPlayer36)
                htVisit.Add(37, Me.btnVisitPlayer37)
                htVisit.Add(38, Me.btnVisitPlayer38)
                htVisit.Add(39, Me.btnVisitPlayer39)
                htVisit.Add(40, Me.btnVisitPlayer40)
                htVisit.Add(41, Me.btnVisitPlayer41)

                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then
                    For intBtnCntr = 1 To dtAwayTeamRoster.Rows.Count
                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then

                            If dtAwayTeamRoster.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                Dim stf1 As String
                                Dim stl1 As String

                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                stf1 = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                stf1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stf1)
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then

                                        strFullName = strFullName & " - " & stf1
                                        'strFullName = strFullName & " - " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        strFullName = stf1
                                        'strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then

                                    stl1 = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    stl1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stl1)

                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & stl1
                                        'strFullName = strFullName & ", " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = stl1
                                        ' strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If
                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                    btn.Tag = stl1 + " " + stf1 + "*" + stl1 + " " + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString() + "*" + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                Else
                                    'btn.Tag = stl1 & " " & stf1
                                    btn.Tag = stl1 + " " + stf1 + "*" + stl1 + " " + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString() + "*" + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                                    'btn.Tag = st2 + " " + st1 + "-" + st2 + " " + st1 + "-" + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                    'btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    'btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                            End If
                            btn.Visible = True
                        End If
                    Next

                    If dtAwayTeamRoster.Rows.Count > 20 Then
                        pnlVisit.AutoScroll = True
                    Else
                        pnlVisit.AutoScroll = False
                    End If
                    m_dtAway = dtAwayTeamRoster
                End If
            Else
                htVisit.Add(1, Me.btnVisitPlayer1)
                htVisit.Add(2, Me.btnVisitPlayer2)
                htVisit.Add(3, Me.btnVisitPlayer3)
                htVisit.Add(4, Me.btnVisitPlayer4)
                htVisit.Add(5, Me.btnVisitPlayer5)
                htVisit.Add(6, Me.btnVisitPlayer6)
                htVisit.Add(7, Me.btnVisitPlayer7)
                htVisit.Add(8, Me.btnVisitPlayer8)
                htVisit.Add(9, Me.btnVisitPlayer9)
                htVisit.Add(10, Me.btnVisitPlayer10)
                htVisit.Add(11, Me.btnVisitPlayer11)

                If dtAwayTeamRoster Is Nothing Then
                    Exit Sub
                End If
                Dim strSortOrder As String = ""
                If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                    strSortOrder = "SORT_NUMBER"
                ElseIf m_objclsLoginDetails.strSortOrder = "Position" Then
                    strSortOrder = "STARTING_POSITION"
                ElseIf m_objclsLoginDetails.strSortOrder = "Last Name" Then
                    strSortOrder = "LAST_NAME"
                End If

                If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then

                    For intBtnCntr = 1 To 11
                        Dim stf3_1 As String
                        Dim stl3_1 As String
                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then

                            If dtAwayTeamRoster.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then

                                        strFullName = strFullName & " - " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        stl3_1 = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                        stl3_1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stl3_1)
                                        strFullName = stl3_1
                                        'strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        Dim temmoniker1 = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                        temmoniker1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker1)
                                        strFullName = strFullName & ", " & temmoniker1
                                        'strFullName = strFullName & ", " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        Dim temmoniker2 = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                        temmoniker2 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker2)
                                        strFullName = temmoniker2
                                        'strFullName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If

                                Dim temmoniker = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                temmoniker = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker)
                                btn.Tag = temmoniker + " " + strPlayerLastName + "*" + temmoniker + " " + strPlayerLastName + "*" + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'btn.Tag = st2 + " " + st1 + "-" + st2 + " " + st1 + "-" + dtHomeTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                                'btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            End If
                        End If
                    Next
                Else

                    Dim dtrowVisitLineUps As DataRow()

                    If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION < 12 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
                    For Each drVisitLineUps As DataRow In dtrowVisitLineUps
                        dtVisitLineUpRosters.ImportRow(drVisitLineUps)
                    Next
                    For intBtnCntr = 1 To 11
                        Dim stf4_1 As String
                        Dim stl4_1 As String
                        btn = CType(htVisit.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            If dtVisitLineUpRosters.Rows.Count >= intBtnCntr Then
                                Dim strFullName As String = String.Empty
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() <> "0" Then
                                    strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                                End If
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        stf4_1 = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                        stf4_1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stf4_1)
                                        strFullName = strFullName & " - " & stf4_1
                                        ' strFullName = strFullName & " - " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    Else
                                        stl4_1 = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                        stl4_1 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stl4_1)
                                        strFullName = stl4_1
                                        ' strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                    End If
                                End If
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    Dim temmoniker33 = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    temmoniker33 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker33)
                                    If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                        strFullName = strFullName & ", " & temmoniker33
                                        'strFullName = strFullName & ", " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    Else
                                        strFullName = temmoniker33
                                        'strFullName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(strFullName.Trim()) Then
                                    btn.Text = strFullName
                                End If
                                Dim tem = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                tem = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), tem)
                                Dim temmoniker = ""
                                If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    temmoniker = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                End If

                                temmoniker = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker)
                                btn.Tag = temmoniker + " " + tem + "*" + temmoniker + " " + tem + "*" + dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()
                                'btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            End If
                        End If
                    Next
                End If

                htVisitsub.Add(1, Me.btnVisitSub1)
                htVisitsub.Add(2, Me.btnVisitSub2)
                htVisitsub.Add(3, Me.btnVisitSub3)
                htVisitsub.Add(4, Me.btnVisitSub4)
                htVisitsub.Add(5, Me.btnVisitSub5)
                htVisitsub.Add(6, Me.btnVisitSub6)
                htVisitsub.Add(7, Me.btnVisitSub7)
                htVisitsub.Add(8, Me.btnVisitSub8)
                htVisitsub.Add(9, Me.btnVisitSub9)
                htVisitsub.Add(10, Me.btnVisitSub10)
                htVisitsub.Add(11, Me.btnVisitSub11)
                htVisitsub.Add(12, Me.btnVisitSub12)
                htVisitsub.Add(13, Me.btnVisitSub13)
                htVisitsub.Add(14, Me.btnVisitSub14)
                htVisitsub.Add(15, Me.btnVisitSub15)
                htVisitsub.Add(16, Me.btnVisitSub16)
                htVisitsub.Add(17, Me.btnVisitSub17)
                htVisitsub.Add(18, Me.btnVisitSub18)
                htVisitsub.Add(19, Me.btnVisitSub19)
                htVisitsub.Add(20, Me.btnVisitSub20)
                htVisitsub.Add(21, Me.btnVisitSub21)
                htVisitsub.Add(22, Me.btnVisitSub22)
                htVisitsub.Add(23, Me.btnVisitSub23)
                htVisitsub.Add(24, Me.btnVisitSub24)
                htVisitsub.Add(25, Me.btnVisitSub25)
                htVisitsub.Add(26, Me.btnVisitSub26)
                htVisitsub.Add(27, Me.btnVisitSub27)
                htVisitsub.Add(28, Me.btnVisitSub28)
                htVisitsub.Add(29, Me.btnVisitSub29)
                htVisitsub.Add(30, Me.btnVisitSub30)
                If Not dtAwayTeamRoster.Columns.Contains("STARTING_POSITION") Then

                    If dtAwayTeamRoster.Rows.Count > 11 Then
                        Dim stf3 As String
                        Dim stl3 As String
                        For intBtnCntr = 12 To dtAwayTeamRoster.Rows.Count
                            btn = CType(htVisitsub.Item(intBtnCntr - 11), Button)
                            If btn IsNot Nothing Then

                                strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                If strPlayerLastName.Length > 6 Then
                                    strPlayerLastName = strPlayerLastName.Substring(0, 6) & "."
                                    'strPlayerLastName = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                                End If
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                    btn.Text = strPlayerLastName
                                Else
                                    btn.Text = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                                End If
                                Dim temmoniker = ""
                                If dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                    temmoniker = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                                End If
                                temmoniker = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker)
                                'btn.Tag = temmoniker + " " + strPlayerLastName
                                btn.Tag = temmoniker + " " + strPlayerLastName + "*" + temmoniker + " " + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString() + "*" + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                                'btn.Tag = dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                                btn.Visible = True
                            End If
                        Next
                    End If
                Else
                    Dim dtrowVisitLineUps As DataRow()
                    Dim stf4 As String
                    Dim stl4 As String
                    If Not dtAwayTeamRoster.Columns.Contains("SORT_NUMBER") And strSortOrder = "SORT_NUMBER" Then
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "STARTING_POSITION ASC")
                    Else
                        dtrowVisitLineUps = dtAwayTeamRoster.Select("STARTING_POSITION > 11 AND STARTING_POSITION > 0", "" & strSortOrder & " ASC")
                    End If
                    dtVisitLineUpRosters = dtAwayTeamRoster.Clone()
                    For Each drVisitLineUps As DataRow In dtrowVisitLineUps
                        dtVisitLineUpRosters.ImportRow(drVisitLineUps)
                    Next
                    For intBtnCntr = 1 To dtVisitLineUpRosters.Rows.Count
                        btn = CType(htVisitsub.Item(intBtnCntr), Button)
                        If btn IsNot Nothing Then
                            stl4 = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            stl4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stl4)
                            strPlayerLastName = stl4
                            'strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            If strPlayerLastName.Length > 6 Then
                                strPlayerLastName = strPlayerLastName.Substring(0, 6)
                                'strPlayerLastName = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString().Substring(0, 6) & "."
                            End If
                            If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() = "0" Then
                                btn.Text = strPlayerLastName
                            Else
                                btn.Text = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("DISPLAY_UNIFORM_NUMBER").ToString() + vbNewLine + strPlayerLastName
                            End If
                            Dim temmoniker = ""
                            If dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER") IsNot DBNull.Value Then
                                temmoniker = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString()
                            End If
                            temmoniker = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmoniker)
                            'btn.Tag = temmoniker + " " + strPlayerLastName
                            btn.Tag = temmoniker + " " + strPlayerLastName + "*" + temmoniker + " " + dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString() + "*" + dtAwayTeamRoster.Rows(intBtnCntr - 1).Item("PLAYER_ID").ToString()

                            'btn.Tag = dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("MONIKER").ToString() & " " & dtVisitLineUpRosters.Rows(intBtnCntr - 1).Item("LAST_NAME").ToString()
                            btn.Visible = True
                        End If
                    Next
                End If
                m_dtAway = dtAwayTeamRoster
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To clear controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearTextboxes()
        Try
            txtTime.Text = String.Empty
            txtHeadline.Text = String.Empty
            txtComment.Text = String.Empty
            m_decSequenceNo = "-1"
            m_intCurrentPeriod = -1
            txtTime.Enabled = True
            cmbCommentType.SelectedValue = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' To populate existing commentary data in listview
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub BindCommentaryData()
        Try
            lvwCommentary.Items.Clear()
            m_dsCommentaryData = m_objclsCommentary.GetCommentary(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber)

            If m_dsCommentaryData IsNot Nothing Then
                If m_dsCommentaryData.Tables.Count > 0 Then
                    If m_dsCommentaryData.Tables(0).Rows.Count > 0 Then
                        'start 26-11-13
                        frmModule1PBP.IsRecordsProcessing(m_dsCommentaryData.Tables(0))
                        'end 26-11-13

                        Dim strPeriod As String = String.Empty
                        Dim strTimeElapsed As String = String.Empty
                        Dim intPreExistPeriod As Integer = 0
                        Dim CurrPeriod As Integer
                        Dim ElapsedTime As Integer
                        Dim intMaxPeriod As Integer = Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(0).Item("PERIOD"))
                        'If m_objclsGameDetails.IsEndGame = True Then
                        For i = 0 To m_dsCommentaryData.Tables(0).Rows.Count - 1
                            If Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(i).Item("PERIOD")) > intMaxPeriod Then
                                intMaxPeriod = Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(i).Item("PERIOD"))
                            End If
                        Next
                        'End If
                        For intRowCount = 0 To m_dsCommentaryData.Tables(0).Rows.Count - 1
                            strPeriod = String.Empty
                            CurrPeriod = CInt(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD"))

                            If m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED") Is DBNull.Value Then
                                strTimeElapsed = String.Empty
                            Else
                                ElapsedTime = CInt(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED").ToString())
                                If ElapsedTime > 0 And CurrPeriod <> 0 Then
                                    ElapsedTime = ElapsedTime + 30
                                    strTimeElapsed = TimeAfterRegularInterval(ElapsedTime, CurrPeriod)
                                Else
                                    If intMaxPeriod = 4 Then

                                        strTimeElapsed = "120"
                                    End If
                                    If intMaxPeriod = 3 Then

                                        strTimeElapsed = "105"
                                    End If
                                    If intMaxPeriod = 2 Then

                                        strTimeElapsed = "90"
                                    End If
                                    If intMaxPeriod = 1 Then

                                        strTimeElapsed = "45"
                                    End If

                                End If

                            End If

                            If intPreExistPeriod = 0 And Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) = 0 Then
                                If intMaxPeriod = 4 Then
                                    strPeriod = "End Regulation"
                                    strTimeElapsed = "120"
                                End If
                                If intMaxPeriod = 3 Then
                                    strPeriod = "Extra Time Halftime"
                                    strTimeElapsed = "105"
                                End If
                                If intMaxPeriod = 2 Then
                                    strPeriod = "End Regulation"
                                    strTimeElapsed = "90"
                                End If
                                If intMaxPeriod = 1 Then
                                    strPeriod = "Halftime"
                                    strTimeElapsed = "45"
                                End If
                                If intMaxPeriod = 0 Then
                                    strPeriod = "Pre Game"
                                End If
                            ElseIf intPreExistPeriod = 1 And Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) = 0 Then
                                strPeriod = "Pre Game"
                            ElseIf intPreExistPeriod = 2 And Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) = 0 Then
                                strPeriod = "Halftime"
                                strTimeElapsed = "45"
                            ElseIf intPreExistPeriod = 3 And Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) = 0 Then
                                strPeriod = "End Regulation"
                                strTimeElapsed = "90"
                            ElseIf intPreExistPeriod = 4 And Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) = 0 Then
                                strPeriod = "Extra Time Halftime"
                                strTimeElapsed = "105"
                            Else
                                strPeriod = m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD").ToString
                            End If
                            If intPreExistPeriod = 0 And m_objclsGameDetails.IsEndGame = True And Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) = 0 Then
                                strPeriod = "Post Game"
                            End If
                            Dim lvwCommentaryData As New ListViewItem(strTimeElapsed)
                            lvwCommentaryData.UseItemStyleForSubItems = False

                            If Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD")) <> 0 Then
                                intPreExistPeriod = Convert.ToInt32(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("PERIOD"))
                            End If
                            lvwCommentaryData.SubItems.Add(strPeriod)

                            Dim headlineItem As ListViewItem.ListViewSubItem = lvwCommentaryData.SubItems.Add(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("HEADLINE").ToString())
                            headlineItem.Font = New System.Drawing.Font(lvwCommentary.Font, System.Drawing.FontStyle.Bold)
                            Dim commentItem As ListViewItem.ListViewSubItem = lvwCommentaryData.SubItems.Add(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("COMMENTARY").ToString)
                            commentItem.Tag = m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("COMMENTARY").ToString
                            lvwCommentaryData.SubItems.Add(m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                            lvwCommentaryData.ToolTipText = m_dsCommentaryData.Tables(0).Rows(intRowCount).Item("COMMENTARY").ToString
                            lvwCommentary.Items.Add(lvwCommentaryData)
                            m_blnlvwValidate = True
                        Next
                        'DISCUSS WITH DIJO - THIS CODE IS CAUSING PROBLEM!!!!!!!
                        If m_objclsGameDetails.IsRestartGame Then
                            'Setting the period property implemented by shirley 29/07/2009
                            Dim drCommData() As DataRow = m_dsCommentaryData.Tables(0).Select("PERIOD > 0", "SEQUENCE_NUMBER DESC")
                            If drCommData.Length > 0 Then
                                m_objclsGameDetails.CurrentPeriod = Convert.ToInt32(drCommData(0).Item("PERIOD"))
                            Else
                                m_objclsGameDetails.CurrentPeriod = 0
                                frmMain.UdcRunningClock1.URCSetTime(0, 0, True)
                                frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", True)
                                frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", False)
                                Exit Sub
                            End If
                            'setting the gameclock
                            drCommData = m_dsCommentaryData.Tables(0).Select("PERIOD = " & m_objclsGameDetails.CurrentPeriod & " AND TIME_ELAPSED IS NOT NULL", "SEQUENCE_NUMBER DESC")
                            If drCommData.Length > 0 Then
                                Dim strNewTime() As String
                                strNewTime = CalculateTimeElapseAfter(CInt(drCommData(0).Item("TIME_ELAPSED")), m_objclsGameDetails.CurrentPeriod).Split(CChar(":"))
                                frmMain.UdcRunningClock1.URCSetTime(CShort(strNewTime(0)), 0)
                            End If
                            If frmMain.UdcRunningClock1.URCIsRunning Then
                                frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                            Else
                                frmMain.btnClock.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "\\Resources\pause.gif")
                                frmMain.UdcRunningClock1.URCToggle()
                            End If

                            'setting menu items
                            frmMain.EnableMenuItem("StartPeriodToolStripMenuItem", False)
                            frmMain.EnableMenuItem("EndPeriodToolStripMenuItem", True)

                        End If
                    Else
                        lvwCommentary.Items.Clear()
                    End If
                Else
                    lvwCommentary.Items.Clear()
                End If
            Else
                lvwCommentary.Items.Clear()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To populate controls with the selected commentary data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SelectedListViewDisplay()
        Try
            Dim ElapsedTime As Integer
            If m_dsCommentaryData IsNot Nothing Then
                If m_dsCommentaryData.Tables.Count > 0 Then
                    If m_dsCommentaryData.Tables(0).Rows.Count > 0 Then
                        If lvwCommentary.SelectedItems.Count > 0 Then
                            Dim listselectDatarow() As DataRow
                            For Each lvwitm As ListViewItem In lvwCommentary.SelectedItems
                                m_decSequenceNo = lvwitm.SubItems(4).Text.Trim()
                            Next
                            txtTime.Text = ""
                            listselectDatarow = m_dsCommentaryData.Tables(0).Select("SEQUENCE_NUMBER='" & m_decSequenceNo & "'")
                            If listselectDatarow.Length > 0 Then
                                m_intCurrentPeriod = Convert.ToInt32(listselectDatarow(0).Item("PERIOD")) ' Dijo Changed on 15 June, 2009
                                If IsDBNull(listselectDatarow(0).Item("TIME_ELAPSED")) = False Then
                                    ElapsedTime = CInt(listselectDatarow(0).Item("TIME_ELAPSED"))
                                    If ElapsedTime > 0 And m_intCurrentPeriod <> 0 Then
                                        ElapsedTime = ElapsedTime + 30
                                    End If
                                    If ElapsedTime > 0 And m_intCurrentPeriod <> 0 Then
                                        txtTime.Enabled = True
                                        txtTime.Text = CalculateTimeElapseAfter(ElapsedTime, m_intCurrentPeriod)
                                    Else
                                        txtTime.Text = CStr(CInt(ElapsedTime / 60))
                                        txtTime.Enabled = False
                                    End If
                                    'txtTime.Text = CalculateTimeElapseAfter(ElapsedTime, m_intCurrentPeriod)
                                End If

                                txtHeadline.Text = listselectDatarow(0).Item("HEADLINE").ToString()
                                txtComment.Text = listselectDatarow(0).Item("COMMENTARY").ToString()
                                If IsDBNull(listselectDatarow(0).Item("COMMENTARY_TYPE")) = False Then
                                    cmbCommentType.SelectedValue = CInt(listselectDatarow(0).Item("COMMENTARY_TYPE").ToString())
                                Else
                                    cmbCommentType.SelectedValue = 0
                                End If

                            End If
                            'AUDIT TRIAL
                            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemDoubleClicked, lvwCommentary.SelectedItems(0).Text & " - " & lvwCommentary.SelectedItems(0).SubItems(1).Text & " - " & lvwCommentary.SelectedItems(0).SubItems(2).Text & " - " & lvwCommentary.SelectedItems(0).SubItems(3).Text, 1, 0)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub SelectedListViewDisplay()
    '    Try
    '        If m_dsCommentaryData IsNot Nothing Then
    '            If m_dsCommentaryData.Tables.Count > 0 Then
    '                If m_dsCommentaryData.Tables(0).Rows.Count > 0 Then
    '                    If lvwCommentary.SelectedItems.Count > 0 Then
    '                        Dim listselectDatarow() As DataRow
    '                        For Each lvwitm As ListViewItem In lvwCommentary.SelectedItems
    '                            m_decSequenceNo = Convert.ToDecimal(lvwitm.SubItems(4).Text.Trim())
    '                        Next
    '                        listselectDatarow = m_dsCommentaryData.Tables(0).Select("SEQUENCE_NUMBER=" & m_decSequenceNo)
    '                        If listselectDatarow.Length > 0 Then
    '                            txtTime.Text = listselectDatarow(0).Item("TIME_ELAPSED").ToString()
    '                            txtHeadline.Text = listselectDatarow(0).Item("HEADLINE").ToString()
    '                            txtComment.Text = listselectDatarow(0).Item("COMMENTARY").ToString()
    '                            m_intCurrentPeriod = Convert.ToInt32(listselectDatarow(0).Item("PERIOD")) ' Dijo Changed on 15 June, 2009
    '                        End If
    '                        'AUDIT TRIAL
    '                        m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ListViewItemDoubleClicked, lvwCommentary.SelectedItems(0).Text & " - " & lvwCommentary.SelectedItems(0).SubItems(1).Text & " - " & lvwCommentary.SelectedItems(0).SubItems(2).Text & " - " & lvwCommentary.SelectedItems(0).SubItems(3).Text, 1, 0)
    '                    End If
    '                End If
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''' <summary>
    ''' To calculate time in minutes
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CalculateTime()
        Try
            'If frmMain.UdcRunningClock1.URCIsRunning Then
            Dim chrSep(1) As Char
            chrSep(1) = Convert.ToChar(":")
            Dim strArrCurrentTime As String() = frmMain.UdcRunningClock1.URCCurrentTime.Split(chrSep)

            If m_objclsGameDetails.CurrentPeriod = 0 Then
                txtTime.Text = Convert.ToString(Convert.ToInt32(strArrCurrentTime(0)))
            Else
                txtTime.Text = Convert.ToString(Convert.ToInt32(strArrCurrentTime(0)) + 1)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function ValidateCurrentTime() As Boolean
        Try
            Dim chkRes As Boolean = True
            Dim strActualValue As String = String.Empty
            Dim chrSep(1) As Char
            chrSep(1) = Convert.ToChar(":")
            Dim strArrCurrentTime As String() = frmMain.UdcRunningClock1.URCCurrentTime.Split(chrSep)
            strActualValue = Convert.ToString(Convert.ToInt32(strArrCurrentTime(0)) + 1)

            ''
            'Dim CurrentClockTime As Integer
            'Dim EventTime As Integer

            'CurrentClockTime = CInt(m_objUtility.ConvertMinuteToSecond(frmMain.UdcRunningClock1.URCCurrentTime))
            'EventTime = CInt(m_objUtility.ConvertMinuteToSecond(txtTime.Text))

            'If m_objclsGameDetails.IsEditMode Then
            '    If CurrentClockTime < EventTime And m_CurrentPeriod = m_objclsGameDetails.CurrentPeriod Then
            '        essageDialog.Show("Current Clock Time should be greater than entered time!")
            '        txtTime.Text = String.Empty
            '        Return False
            '    End If
            '    If IsEditTimeAllowed(EventTime) = False Then
            '        txtTime.Text = String.Empty
            '        Return False
            '    End If
            'End If

            ''

            If Not String.IsNullOrEmpty(strActualValue) Then
                If m_objclsGameDetails.IsEditMode Then
                    If Convert.ToInt32(txtTime.Text) > Convert.ToInt32(strActualValue) And m_intCurrentPeriod = m_objclsGameDetails.CurrentPeriod Then
                        chkRes = False
                        MessageDialog.Show(m7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        txtTime.Text = String.Empty
                    End If
                Else
                    If Convert.ToInt32(txtTime.Text) > Convert.ToInt32(strActualValue) Then
                        chkRes = False
                        MessageDialog.Show(m7, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                        txtTime.Text = String.Empty
                    End If
                End If
            End If
            Return chkRes
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To fetch and display PBP data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindPBPData()
        Try
            Dim dsPBP As New DataSet
            Dim TeamID As Integer

            dsPBP = m_objclsCommentary.GetPBPDataForCommentary(m_objclsGameDetails.GameCode, m_objclsGameDetails.languageid)
            If dsPBP IsNot Nothing Then
                dsPBP.DataSetName = "PBP"
                If dsPBP.Tables.Count > 0 Then
                    dsPBP.Tables(0).TableName = "PBPData"
                    If dsPBP.Tables("PBPData").Rows.Count > 0 Then
                        lvwPBP.Items.Clear()
                        For intRowCount = 0 To dsPBP.Tables("PBPData").Rows.Count - 1
                            'If CInt(dsPBP.Tables("PBPData").Rows(intRowCount).Item("EVENT_CODE_ID")) = 62 Then
                            '    Continue For
                            'End If
                            Dim StrTime As String = "00:00"

                            StrTime = TimeAfterRegularInterval(CInt(dsPBP.Tables("PBPData").Rows(intRowCount).Item("TIME_ELAPSED")), CInt(dsPBP.Tables("PBPData").Rows(intRowCount).Item("PERIOD")))
                            Dim lvwPBPData As New ListViewItem(StrTime)

                            'Dim lvwPBPData As New ListViewItem(clsUtility.ConvertSecondToMinute(Convert.ToDouble(), False))
                            lvwPBPData.SubItems.Add(dsPBP.Tables("PBPData").Rows(intRowCount).Item("EVENT_CODE_DESC").ToString)
                            lvwPBPData.SubItems.Add(dsPBP.Tables("PBPData").Rows(intRowCount).Item("SHOT_TYPE").ToString)
                            lvwPBPData.SubItems.Add(dsPBP.Tables("PBPData").Rows(intRowCount).Item("LAST_NAME").ToString())
                            lvwPBPData.SubItems.Add(dsPBP.Tables("PBPData").Rows(intRowCount).Item("SHOT_DESC").ToString)
                            lvwPBPData.SubItems.Add(dsPBP.Tables("PBPData").Rows(intRowCount).Item("SHOT_RESULT").ToString)
                            lvwPBP.Items.Add(lvwPBPData)
                        Next
                        If dsPBP.Tables("PBPData").Rows(0).Item("TEAM_ID") IsNot DBNull.Value Then
                            TeamID = CInt(dsPBP.Tables("PBPData").Rows(0).Item("TEAM_ID"))
                            If TeamID = m_objclsGameDetails.HomeTeamID Then
                                m_objclsGameDetails.HomeScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE"))
                                m_objclsGameDetails.AwayScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE"))

                                frmMain.lblScoreHome.Text = dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE").ToString()
                                frmMain.lblScoreAway.Text = dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE").ToString()
                            Else

                                m_objclsGameDetails.HomeScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE"))
                                m_objclsGameDetails.AwayScore = CInt(dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE"))

                                frmMain.lblScoreAway.Text = dsPBP.Tables("PBPData").Rows(0).Item("OFFENSE_SCORE").ToString()
                                frmMain.lblScoreHome.Text = dsPBP.Tables("PBPData").Rows(0).Item("DEFENSE_SCORE").ToString()

                            End If

                        End If

                        'If dsPBP.Tables("PBPData").Rows(dsPBP.Tables("PBPData").Rows.Count - 1).Item("DEFENSE_SCORE") IsNot DBNull.Value Then

                        'End If
                        lvwPBP.Items(0).EnsureVisible()
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To bind Coach Data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindCoachInfo()
        Try
            btnHomeCoach.Enabled = True
            btnHomeCoach.Visible = True
            btnVisitCoach.Enabled = True
            btnVisitCoach.Visible = True
            btnHomeCoach.Text = String.Empty
            btnVisitCoach.Text = String.Empty
            Dim strHomeCoachName As String = String.Empty
            Dim strVisitCoachName As String = String.Empty
            Dim strHomeCoachTag As String = String.Empty
            Dim strVisitCoachTag As String = String.Empty
            Dim dsCoachInfo As New DataSet
            Dim stcf4 As String
            Dim stcl4 As String
            Dim strcs As String = "(Coach/Manager)"
            strcs = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strcs)
            dsCoachInfo = m_objclsCommentary.GetCoachInfoForCommentary(m_objclsGameDetails.GameCode, m_objclsGameDetails.languageid)
            If dsCoachInfo IsNot Nothing Then
                If dsCoachInfo.Tables.Count > 0 Then
                    If dsCoachInfo.Tables("CoachData").Rows.Count > 0 Then
                        For intCoachDataCount = 0 To dsCoachInfo.Tables("CoachData").Rows.Count - 1
                            If Convert.ToInt32(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("TEAM_ID")) = m_objclsGameDetails.HomeTeamID Then
                                If dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME") IsNot DBNull.Value Then
                                    stcl4 = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                    stcl4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stcl4)
                                    strHomeCoachName = stcl4
                                    'strHomeCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                End If
                                If dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER") IsNot DBNull.Value Then
                                    If String.IsNullOrEmpty(strHomeCoachName.Trim()) Then
                                        stcf4 = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                        stcf4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), stcf4)
                                        strHomeCoachName = stcf4
                                        strHomeCoachName = strHomeCoachName
                                        'strHomeCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                    Else
                                        Dim homMonk As String = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                        homMonk = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), homMonk)
                                        strHomeCoachName = strHomeCoachName + ", " + homMonk
                                        'strHomeCoachName = strHomeCoachName & ", " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                    End If
                                    strHomeCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                    strHomeCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strHomeCoachTag)
                                End If
                                If dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If String.IsNullOrEmpty(strHomeCoachTag.Trim()) Then
                                        strHomeCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                        strHomeCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strHomeCoachTag)
                                    Else
                                        strHomeCoachTag = strHomeCoachTag & " " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                        strHomeCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strHomeCoachTag)
                                    End If

                                End If
                            End If
                            If Convert.ToInt32(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("TEAM_ID")) = m_objclsGameDetails.AwayTeamID Then
                                If dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME") IsNot DBNull.Value Then
                                    strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                    strVisitCoachName = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachName)
                                    'strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                End If
                                If dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER") IsNot DBNull.Value Then
                                    If String.IsNullOrEmpty(strHomeCoachName.Trim()) Then
                                        strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                        strVisitCoachName = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachName)
                                        ' strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                    Else
                                        Dim temmonk As String = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                        temmonk = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmonk)
                                        strVisitCoachName = strVisitCoachName + ", " + temmonk
                                        'strVisitCoachName = strVisitCoachName & ", " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                    End If
                                    Dim temmonk2 As String = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                    temmonk2 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmonk2)
                                    strVisitCoachTag = temmonk2
                                    'strVisitCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("MONIKER"))
                                End If
                                If dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME") IsNot DBNull.Value Then
                                    If String.IsNullOrEmpty(strVisitCoachTag.Trim()) Then
                                        strVisitCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                        strVisitCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachTag)
                                        'strVisitCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                    Else
                                        Dim temmonk3 As String = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                        temmonk3 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmonk3)
                                        strVisitCoachTag = strVisitCoachTag + " " + temmonk3
                                        'strVisitCoachTag = strVisitCoachTag & " " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(intCoachDataCount).Item("LAST_NAME"))
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            If Not String.IsNullOrEmpty(strHomeCoachName.Trim()) Then
                btnHomeCoach.Text = strHomeCoachName & " (Coach/Manager)"
                btnHomeCoach.Tag = strHomeCoachTag
            Else
                dsCoachInfo = m_objclsCommentary.GetInitialCoachInfoForCommentary(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
                If dsCoachInfo IsNot Nothing Then
                    If dsCoachInfo.Tables.Count > 0 Then
                        If dsCoachInfo.Tables("CoachData").Rows.Count > 0 Then
                            If dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME") IsNot DBNull.Value Then
                                strHomeCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                            End If
                            If dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER") IsNot DBNull.Value Then
                                If String.IsNullOrEmpty(strHomeCoachName.Trim()) Then
                                    'strHomeCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                    strHomeCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                    strHomeCoachName = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strHomeCoachName)
                                Else
                                    strHomeCoachName = strHomeCoachName & ", " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                End If
                                strHomeCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                strHomeCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strHomeCoachTag)
                                'strHomeCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                            End If
                            If dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME") IsNot DBNull.Value Then
                                If String.IsNullOrEmpty(strHomeCoachTag.Trim()) Then
                                    strHomeCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                    strHomeCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strHomeCoachTag)
                                    '  strHomeCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                Else
                                    Dim temmonk3 As String = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                    temmonk3 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmonk3)
                                    strHomeCoachTag = strHomeCoachTag + " " + temmonk3
                                    'strHomeCoachTag = strHomeCoachTag & " " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                End If
                            End If
                        End If
                    End If
                End If
                If Not String.IsNullOrEmpty(strHomeCoachName.Trim()) Then
                    btnHomeCoach.Text = strHomeCoachName + " " + strcs
                    'btnHomeCoach.Text = strHomeCoachName & " (Coach/Manager)"
                    btnHomeCoach.Tag = strHomeCoachTag
                End If
            End If
            If Not String.IsNullOrEmpty(strVisitCoachName.Trim()) Then
                btnVisitCoach.Text = strVisitCoachName + " " + strcs
                btnVisitCoach.Tag = strVisitCoachTag
                'btnVisitCoach.Text = strVisitCoachName & " (Coach/Manager)"
                ' btnVisitCoach.Tag = strVisitCoachTag
            Else
                dsCoachInfo = m_objclsCommentary.GetInitialCoachInfoForCommentary(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
                If dsCoachInfo IsNot Nothing Then
                    If dsCoachInfo.Tables.Count > 0 Then
                        If dsCoachInfo.Tables("CoachData").Rows.Count > 0 Then
                            If dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME") IsNot DBNull.Value Then
                                strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                strVisitCoachName = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachName)
                                'strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                            End If
                            If dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER") IsNot DBNull.Value Then
                                If String.IsNullOrEmpty(strHomeCoachName.Trim()) Then
                                    'strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                    strVisitCoachName = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                    strVisitCoachName = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachName)
                                Else
                                    strVisitCoachName = strVisitCoachName & ", " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                    strVisitCoachName = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachName)
                                End If
                                strVisitCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("MONIKER"))
                                strVisitCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachTag)
                            End If
                            If dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME") IsNot DBNull.Value Then
                                If String.IsNullOrEmpty(strVisitCoachTag.Trim()) Then
                                    strVisitCoachTag = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                    strVisitCoachTag = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), strVisitCoachTag)
                                Else
                                    Dim temmonk4 As String = Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))
                                    temmonk4 = lsupport.LanguageTranslate(CInt(m_objclsGameDetails.languageid), temmonk4)
                                    strVisitCoachTag = strVisitCoachTag + " " + temmonk4
                                    'strVisitCoachTag = strVisitCoachTag & " " & Convert.ToString(dsCoachInfo.Tables("CoachData").Rows(0).Item("LAST_NAME"))

                                End If
                            End If
                        End If
                    End If
                End If

                If Not String.IsNullOrEmpty(strVisitCoachName.Trim()) Then
                    btnVisitCoach.Text = strVisitCoachName + " " + strcs
                    '  btnVisitCoach.Text = strVisitCoachName & " (Coach/Manager)"
                    btnVisitCoach.Tag = strVisitCoachTag
                    'btnVisitCoach.Text = strVisitCoachName & " (Coach/Manager)"
                    'btnVisitCoach.Tag = strVisitCoachTag
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ChangeTheme(ByVal EditMode As Boolean)
        Try
            If EditMode Then
                Me.BackColor = Color.LightGoldenrodYellow
                pnlHome.BackColor = Color.LightGoldenrodYellow
                pnlVisit.BackColor = Color.LightGoldenrodYellow
                pnlHomeBench.BackColor = Color.LightGoldenrodYellow
                pnlVisitBench.BackColor = Color.LightGoldenrodYellow
                pnlCommentaryEntry.BackColor = Color.Beige
                pnlPBP.BackColor = Color.Khaki
            Else
                Me.BackColor = Color.WhiteSmoke
                pnlHome.BackColor = Color.WhiteSmoke
                pnlVisit.BackColor = Color.WhiteSmoke
                pnlHomeBench.BackColor = Color.WhiteSmoke
                pnlVisitBench.BackColor = Color.WhiteSmoke
                pnlCommentaryEntry.BackColor = Color.Lavender
                pnlPBP.BackColor = Color.LightSteelBlue
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
    '    Try
    '        Dim StrTime As String = "00"
    '        If m_objclsGameDetails.IsDefaultGameClock Then
    '            If CurrPeriod = 1 Then
    '                If ElapsedTime > FIRSTHALF Then
    '                    Dim DiffTime As Integer
    '                    DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
    '                    StrTime = "45+" + CStr(DiffTime)
    '                Else
    '                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                End If
    '            ElseIf CurrPeriod = 2 Then
    '                If ElapsedTime > FIRSTHALF Then
    '                    Dim DiffTime As Integer
    '                    DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
    '                    StrTime = "90+" + CStr(DiffTime)
    '                Else
    '                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                End If
    '            ElseIf CurrPeriod = 3 Then
    '                If ElapsedTime > EXTRATIME Then
    '                    Dim DiffTime As Integer
    '                    DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
    '                    StrTime = "105+" + CStr(DiffTime)
    '                Else
    '                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                End If
    '            ElseIf CurrPeriod = 4 Then
    '                If ElapsedTime > EXTRATIME Then
    '                    Dim DiffTime As Integer
    '                    DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
    '                    StrTime = "120+" + CStr(DiffTime)
    '                Else
    '                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                End If
    '            ElseIf CurrPeriod = 5 Then
    '                StrTime = "120"
    '            End If
    '            'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
    '        Else
    '            If CurrPeriod = 1 Or CurrPeriod = 2 Then
    '                If ElapsedTime > FIRSTHALF Then
    '                    Dim DiffTime As Integer
    '                    DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
    '                    StrTime = "45+" + CStr(DiffTime)
    '                Else
    '                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

    '                End If
    '            Else
    '                If ElapsedTime > EXTRATIME Then
    '                    Dim DiffTime As Integer
    '                    DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
    '                    StrTime = "15+" + CStr(DiffTime)
    '                Else
    '                    StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
    '                End If
    '            End If
    '        End If
    '        Dim strCurrTime() As String
    '        strCurrTime = StrTime.Split(CChar(":"))
    '        Return strCurrTime(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Function TimeAfterRegularInterval(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try
            Dim StrTime As String = "00:00"
            If m_objclsGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 0 Then
                    If ElapsedTime > 0 Then
                        If ElapsedTime = FIRSTHALF Then
                            StrTime = "45"
                        ElseIf ElapsedTime = SECONDHALF Then
                            StrTime = "90"
                        ElseIf ElapsedTime = FIRSTEXTRA Then
                            StrTime = "105"
                        ElseIf ElapsedTime = SECONDEXTRA Then
                            StrTime = "120"
                        End If
                    End If
                ElseIf CurrPeriod = 1 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffTime > 0 Then
                            StrTime = "45+" + CStr(DiffTime)
                        Else
                            StrTime = "45"
                        End If

                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)

                        If DiffTime > 0 Then
                            StrTime = "90+" + CStr(DiffTime)
                        Else
                            StrTime = "90"
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 3 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffTime > 0 Then
                            StrTime = "105+" + CStr(DiffTime)
                        Else
                            StrTime = "105"
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 4 Then
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffTime > 0 Then
                            StrTime = "120+" + CStr(DiffTime)
                        Else
                            StrTime = "120"
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                ElseIf CurrPeriod = 5 Then
                    StrTime = "120"
                End If
                'StrTime = CalculateTimeElapseAfter(ElapsedTime, CInt(m_dsTouchData.Tables("Touches").Rows(intRowCount).Item("PERIOD")))
            Else
                If CurrPeriod = 1 Or CurrPeriod = 2 Then
                    If ElapsedTime > FIRSTHALF Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - FIRSTHALF) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - FIRSTHALF) / 60)
                        If DiffTime > 0 Then
                            StrTime = "45+" + CStr(DiffTime)
                        Else
                            StrTime = "45"
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)

                    End If
                Else
                    If ElapsedTime > EXTRATIME Then
                        Dim DiffTime As Integer
                        Dim DiffSec As Integer
                        DiffSec = CInt((ElapsedTime - EXTRATIME) Mod 60)
                        ElapsedTime = ElapsedTime - DiffSec
                        DiffTime = CInt((ElapsedTime - EXTRATIME) / 60)
                        If DiffTime > 0 Then
                            StrTime = "15+" + CStr(DiffTime)
                        Else
                            StrTime = "15"
                        End If
                    Else
                        StrTime = CalculateTimeElapseAfter(ElapsedTime, CurrPeriod)
                    End If
                End If
            End If
            Return StrTime
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
        Try

            Dim strCurrTime() As String
            Dim StrTime As String = "00"
            'strCurrTime = StrTime.Split(CChar(":"))
            'Return strCurrTime(0)
            If m_objclsGameDetails.IsDefaultGameClock Then
                If CurrPeriod = 1 Then
                    StrTime = clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                ElseIf CurrPeriod = 2 Then
                    StrTime = clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 3 Then
                    StrTime = clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
                ElseIf CurrPeriod = 4 Then
                    StrTime = clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
                Else
                    StrTime = clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
                End If
            Else
                StrTime = clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
            End If
            strCurrTime = StrTime.Split(CChar(":"))
            Return strCurrTime(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Function CalculateTimeElapseAfter(ByVal ElapsedTime As Integer, ByVal CurrPeriod As Integer) As String
    '    Try
    '        If m_objclsGameDetails.IsDefaultGameClock Then
    '            If CurrPeriod = 1 Then
    '                Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
    '            ElseIf CurrPeriod = 2 Then
    '                Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTHALF + ElapsedTime).ToString), False)
    '            ElseIf CurrPeriod = 3 Then
    '                Return clsUtility.ConvertSecondToMinute(CDbl((SECONDHALF + ElapsedTime).ToString), False)
    '            ElseIf CurrPeriod = 4 Then
    '                Return clsUtility.ConvertSecondToMinute(CDbl((FIRSTEXTRA + ElapsedTime).ToString), False)
    '            End If
    '        End If
    '        Return clsUtility.ConvertSecondToMinute(CDbl(ElapsedTime.ToString), False)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Function CalculateTimeElapsedBefore(ByVal CurrentElapsedTime As Integer, ByVal CurrPeriod As Integer) As Integer
        Try
            If CurrPeriod = 1 Then
                Return CurrentElapsedTime
            ElseIf CurrPeriod = 2 Then
                Return CurrentElapsedTime - FIRSTHALF
            ElseIf CurrPeriod = 3 Then
                Return CurrentElapsedTime - SECONDHALF
            ElseIf CurrPeriod = 4 Then
                Return CurrentElapsedTime - FIRSTEXTRA
            ElseIf CurrPeriod = 5 Then
                Return CurrentElapsedTime
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function getHalfTime() As Integer
        Try
            If m_objclsGameDetails.CurrentPeriod = 1 Then
                Return FIRSTHALF
            ElseIf m_objclsGameDetails.CurrentPeriod = 2 Then
                Return FIRSTHALF
            ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                Return EXTRATIME
            ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                Return EXTRATIME
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' To set period label for T1 workflow
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetPeriod()
        Try
            If m_objclsGameDetails.CurrentPeriod = 1 Then
                frmMain.lblPeriod.Text = "1st Half"

            ElseIf m_objclsGameDetails.CurrentPeriod = 2 Then
                frmMain.lblPeriod.Text = "2nd Half"

            ElseIf m_objclsGameDetails.CurrentPeriod = 3 Then
                frmMain.lblPeriod.Text = "1st ET"

            ElseIf m_objclsGameDetails.CurrentPeriod = 4 Then
                frmMain.lblPeriod.Text = "2nd ET"

            ElseIf m_objclsGameDetails.CurrentPeriod = 5 Then
                frmMain.lblPeriod.Text = "Shootout"
            End If
            frmMain.SetClockControl(True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Public Methods "
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub SwitchSides()
        Try
            pnlHome.Top = PLAYER_PANEL_TOP
            pnlHomeBench.Top = BENCH_PANEL_TOP
            pnlVisitBench.Top = BENCH_PANEL_TOP

            If m_objclsGameDetails.IsHomeTeamOnLeft = True Then
                pnlHome.Left = RIGHT_PLAYER_PANEL_LEFT
                pnlVisit.Left = LEFT_PLAYER_PANEL_LEFT
                pnlHomeBench.Left = RIGHT_BENCH_PANEL_LEFT
                pnlVisitBench.Left = LEFT_PLAYER_PANEL_LEFT

                m_objclsGameDetails.IsHomeTeamOnLeft = False
            Else
                pnlHome.Left = LEFT_PLAYER_PANEL_LEFT
                pnlVisit.Left = RIGHT_PLAYER_PANEL_LEFT
                pnlHomeBench.Left = LEFT_PLAYER_PANEL_LEFT
                pnlVisitBench.Left = RIGHT_BENCH_PANEL_LEFT

                m_objclsGameDetails.IsHomeTeamOnLeft = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetTeamColor(ByVal HomeTeam As Boolean, ByVal TeamColor As System.Drawing.Color)
        Try
            Dim clrTextColor As System.Drawing.Color
            clrTextColor = clsUtility.GetTextColor(TeamColor)

            If HomeTeam = True Then
                m_objGeneral.UpdateTeamColor(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, True, TeamColor.ToArgb)
                m_objclsGameDetails.HomeTeamColor = TeamColor
                For Each obj As Control In pnlHome.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
                For Each obj As Control In pnlHomeBench.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
            Else
                m_objGeneral.UpdateTeamColor(m_objclsGameDetails.GameCode, m_objclsGameDetails.FeedNumber, False, TeamColor.ToArgb)
                m_objclsGameDetails.VisitorTeamColor = TeamColor
                For Each obj As Control In pnlVisit.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
                For Each obj As Control In pnlVisitBench.Controls
                    obj.BackColor = TeamColor
                    obj.ForeColor = clrTextColor
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function IsRefreshNeeded() As Boolean
        Try
            Dim drs() As DataRow
            Dim dsHome As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
            Dim dsAway As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
            Dim blnChkTeam As Boolean = False

            If dsHome IsNot Nothing Or dsAway IsNot Nothing Then
                If dsHome.Tables.Count > 0 Or dsAway.Tables.Count > 0 Then
                    If dsHome.Tables(0).Rows.Count > 0 Or dsAway.Tables(0).Rows.Count > 0 Then
                        blnChkTeam = True
                        m_blnHomeLineUp = True
                        m_blnVisitLineUp = True
                        pnlHome.AutoScroll = False
                        pnlVisit.AutoScroll = False
                        pnlHome.Height = 305
                        pnlVisit.Height = 305
                        pnlHomeBench.Visible = True
                        pnlVisitBench.Visible = True
                    End If
                Else
                    pnlHome.AutoScroll = True
                    pnlVisit.AutoScroll = True
                    pnlHome.Height = 430
                    pnlVisit.Height = 430
                    pnlHomeBench.Visible = False
                    pnlVisitBench.Visible = False
                End If
            End If

            'roster comparison
            Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
            'TOSOCRS-192 START
            If dsHomeTeam.Tables.Count > 0 Then
                If Not m_dtHome Is Nothing Then
                    If (dsHomeTeam.Tables(0).Rows.Count <> m_dtHome.Rows.Count) And (dsHomeTeam.Tables(0).Rows.Count > 0) Then
                        Return True
                    End If
                    If dsHomeTeam.Tables(0).Rows.Count > 0 Then
                        Dim drsHome() As DataRow = dsHomeTeam.Tables(0).Select("STARTING_POSITION > 11")
                        If drsHome.Length > 0 And btnHomeSub1.Text = "" Then
                            Return True
                        End If
                    End If
                End If

                If dsHomeTeam.Tables(0).Rows.Count > 0 Then
                    If IsLineupsModified() = True Then

                        Return True
                        Exit Function
                    End If
                    'TOSOCRS-192 END
                    If Not m_dtHome Is Nothing Then
                        For i As Integer = 0 To dsHomeTeam.Tables(0).Rows.Count - 1
                            drs = m_dtHome.Select("PLAYER_ID = " & dsHomeTeam.Tables(0).Rows(i).Item("PLAYER_ID").ToString & "")
                            If drs.Length = 0 Then
                                Return True
                            Else
                                If drs(0).Item("DISPLAY_UNIFORM_NUMBER").ToString <> dsHomeTeam.Tables(0).Rows(i).Item("DISPLAY_UNIFORM_NUMBER").ToString Then
                                    Return True
                                End If
                            End If
                        Next
                    End If
                Else
                    Return False
                End If
            End If

            'Away TeamRoster
            Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
            If dsAwayTeam.Tables.Count > 0 Then
                If Not m_dtAway Is Nothing Then
                    If (dsAwayTeam.Tables(0).Rows.Count <> m_dtAway.Rows.Count) And (dsAwayTeam.Tables(0).Rows.Count > 0) Then
                        Return True
                    End If
                End If

                If dsAwayTeam.Tables(0).Rows.Count > 0 Then
                    Dim drsaway() As DataRow = dsAwayTeam.Tables(0).Select("STARTING_POSITION > 11")
                    If drsaway.Length > 0 And btnVisitSub1.Text = "" Then
                        Return True
                    End If
                End If

                If dsAwayTeam.Tables(0).Rows.Count > 0 Then
                    If Not m_dtAway Is Nothing Then
                        For i As Integer = 0 To dsAwayTeam.Tables(0).Rows.Count - 1
                            drs = m_dtAway.Select("PLAYER_ID = " & CInt(dsAwayTeam.Tables(0).Rows(i).Item("PLAYER_ID")) & "")
                            If drs.Length = 0 Then
                                Return True
                            Else
                                If drs(0).Item("DISPLAY_UNIFORM_NUMBER").ToString <> dsAwayTeam.Tables(0).Rows(i).Item("DISPLAY_UNIFORM_NUMBER").ToString Then
                                    Return True
                                End If
                            End If
                        Next
                    End If
                Else
                    Return False
                End If
            End If
            'Check for PBP events
            Dim m_dsEvents As DataSet = m_objGeneral.RefreshPlayers(intPrevUniqueID, m_objclsGameDetails.GameCode)
            If m_dsEvents.Tables(0).Rows.Count > 0 Then
                If m_dsEvents.Tables(1).Rows.Count > 0 Then
                    intPrevUniqueID = CInt(m_dsEvents.Tables(1).Rows(0).Item("UNIQUE_ID"))
                End If
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    'TOSOCRS-192 START
    Private Function IsLineupsModified() As Boolean
        Try
            Dim DsExistingPlayers As New DataSet
            Dim drsExisting() As DataRow
            Dim drsNew() As DataRow
            If Not m_dtHome Is Nothing And Not m_dtAway Is Nothing Then
                DsExistingPlayers.Tables.Add(m_dtHome.Copy)
                DsExistingPlayers.Tables.Add(m_dtAway.Copy)

                Dim dsHomeTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.HomeTeamID, m_objclsGameDetails.languageid)
                Dim dsAwayTeam As DataSet = m_objGeneral.GetTeamRostersforCommentaryandTouchesByLineUps(m_objclsGameDetails.AwayTeamID, m_objclsGameDetails.languageid)
                If dsHomeTeam.Tables(0).Rows.Count > 0 And dsAwayTeam.Tables(0).Rows.Count > 0 Then
                    Dim DsRostersInfo As New DataSet
                    dsHomeTeam.Tables(0).TableName = "Home"
                    dsAwayTeam.Tables(0).TableName = "Away"

                    DsRostersInfo.Tables.Add(dsHomeTeam.Tables(0).Copy)
                    DsRostersInfo.Tables.Add(dsAwayTeam.Tables(0).Copy)

                    If DsExistingPlayers.Tables.Count > 0 Then
                        For intTableCnt As Integer = 0 To DsExistingPlayers.Tables.Count - 1
                            If DsExistingPlayers.Tables(intTableCnt).Columns.Contains("Starting_Position") Then
                                drsExisting = DsExistingPlayers.Tables(intTableCnt).Select("Starting_Position is not null", "Starting_Position")
                                If drsExisting.Length > 0 Then
                                    'fetching new players which are currently used
                                    drsNew = DsRostersInfo.Tables(intTableCnt).Select("Starting_Position is not null", "Starting_Position")
                                    If drsNew.Length > 0 Then
                                        For intRowCount As Integer = 0 To drsExisting.Length - 1
                                            If CInt(intRowCount) > CInt(drsNew.Length - 1) Then
                                                'issue occured when players are deselected and set to blanks
                                                Continue For
                                            End If

                                            If CInt(drsExisting(intRowCount).Item("Starting_Position")) = CInt(drsNew(intRowCount).Item("Starting_position")) Then
                                                'if lineups are matching, then check for player match
                                                If CLng(drsExisting(intRowCount).Item("Player_id")) <> CLng(drsNew(intRowCount).Item("Player_id")) Then
                                                    Return True
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function
    'TOSOCRS-192 END

    Public Sub GetCurrentPlayersAfterRestart()
        Try

            If IsRefreshNeeded() = True Then
                BindTeamRostersByLineUp()
                SortTeamPlayers()
                Dim DsCurrentPlayer As New DataSet
                DsCurrentPlayer.Merge(m_dtHome)
                DsCurrentPlayer.Merge(m_dtAway)

                'Getting the current players from red card and sub events
                Dim DsSubEvents As DataSet = m_objGeneral.FetchSubsforCommAndTouches(m_objclsGameDetails.GameCode)
                If DsSubEvents.Tables(0).Rows.Count > 0 Then
                    For intRowCount As Integer = 0 To DsSubEvents.Tables(0).Rows.Count - 1
                        Select Case CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID"))
                            Case 7, 22
                                For intTableCount As Integer = 0 To DsCurrentPlayer.Tables.Count - 1
                                    Dim DrPlayerOut() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("PLAYER_OUT_ID")) & "")
                                    If DrPlayerOut.Length > 0 Then
                                        If CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = 7 Then
                                            'RED CARD
                                            DrPlayerOut(0).BeginEdit()
                                            DrPlayerOut(0).Item("STARTING_POSITION") = 99
                                            DrPlayerOut(0).EndEdit()
                                            DrPlayerOut(0).AcceptChanges()
                                        Else
                                            'SUBSTITUTION
                                            Dim DrPlayerIn() As DataRow = DsCurrentPlayer.Tables(intTableCount).Select("TEAM_ID = " & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("TEAM_ID")) & " AND PLAYER_ID =" & CInt(DsSubEvents.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER_ID")) & "")
                                            If DrPlayerIn.Length > 0 Then
                                                DrPlayerIn(0).BeginEdit()
                                                DrPlayerIn(0).Item("STARTING_POSITION") = DrPlayerOut(0).Item("STARTING_POSITION")
                                                DrPlayerIn(0).EndEdit()

                                                DrPlayerOut(0).BeginEdit()
                                                DrPlayerOut(0).Item("STARTING_POSITION") = 99
                                                DrPlayerOut(0).EndEdit()

                                                DrPlayerIn(0).AcceptChanges()
                                            End If
                                        End If
                                    End If
                                Next
                        End Select
                    Next

                    ButtonsHomeTeamTagged(DsCurrentPlayer.Tables("Home"), True)
                    ButtonsVisitTeamTagged(DsCurrentPlayer.Tables("Away"), True)

                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' To sort team players
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SortTeamPlayers()
        Try
            If m_objclsLoginDetails.strSortOrder = "Uniform" Then
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", True)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                If m_dtHome IsNot Nothing Then
                    If m_dtHome.Rows.Count > 0 Then
                        If Not m_dtHome.Columns.Contains("SORT_NUMBER") Then
                            Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                            dcHome.DataType = System.Type.GetType("System.Int32")
                            m_dtHome.Columns.Add(dcHome)
                            For Each drHome As DataRow In m_dtHome.Rows
                                If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drHome.Item("SORT_NUMBER") = 30000
                                Else
                                    drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvHome As New DataView(m_dtHome)
                        Dim dtSortHome As New DataTable
                        dtSortHome = m_dtHome.Clone
                        If m_blnHomeLineUp = True Then
                            Dim drArrHome As DataRow() = m_dtHome.Select("STARTING_POSITION<=11", "SORT_NUMBER ASC")
                            For Each drSortHome As DataRow In drArrHome
                                dtSortHome.ImportRow(drSortHome)
                            Next
                            Dim drArrHomeBench As DataRow() = m_dtHome.Select("STARTING_POSITION>11", "SORT_NUMBER ASC")
                            For Each drSortHomeBench As DataRow In drArrHomeBench
                                dtSortHome.ImportRow(drSortHomeBench)
                            Next
                        Else
                            dvHome.Sort = "SORT_NUMBER ASC"
                            dtSortHome = dvHome.ToTable()
                        End If
                        ButtonsHomeTeamTagged(dtSortHome, True)

                    End If
                End If
                If m_dtAway IsNot Nothing Then
                    If m_dtAway.Rows.Count > 0 Then
                        If Not m_dtAway.Columns.Contains("SORT_NUMBER") Then
                            Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                            dcAway.DataType = System.Type.GetType("System.Int32")
                            m_dtAway.Columns.Add(dcAway)
                            For Each drAway As DataRow In m_dtAway.Rows
                                If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drAway.Item("SORT_NUMBER") = 30000
                                Else
                                    drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvAway As New DataView(m_dtAway)
                        Dim dtSortAway As New DataTable
                        dtSortAway = m_dtAway.Clone
                        If m_blnVisitLineUp = True Then
                            Dim drArrAway As DataRow() = m_dtAway.Select("STARTING_POSITION<=11", "SORT_NUMBER ASC")
                            For Each drSortAway As DataRow In drArrAway
                                dtSortAway.ImportRow(drSortAway)
                            Next
                            Dim drArrAwayBench As DataRow() = m_dtAway.Select("STARTING_POSITION>11", "SORT_NUMBER ASC")
                            For Each drSortAwayBench As DataRow In drArrAwayBench
                                dtSortAway.ImportRow(drSortAwayBench)
                            Next
                        Else
                            dvAway.Sort = "SORT_NUMBER ASC"
                            dtSortAway = dvAway.ToTable()
                        End If
                        ButtonsVisitTeamTagged(dtSortAway, True)
                    End If
                End If
            End If
            If m_objclsLoginDetails.strSortOrder = "Last Name" Then
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", True)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", False)
                If m_dtHome IsNot Nothing Then
                    If m_dtHome.Rows.Count > 0 Then
                        If Not m_dtHome.Columns.Contains("SORT_NUMBER") Then
                            Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                            dcHome.DataType = System.Type.GetType("System.Int32")
                            m_dtHome.Columns.Add(dcHome)
                            For Each drHome As DataRow In m_dtHome.Rows
                                If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drHome.Item("SORT_NUMBER") = 30000
                                Else
                                    drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvHome As New DataView(m_dtHome)
                        Dim dtSortHome As New DataTable
                        dtSortHome = m_dtHome.Clone
                        If m_blnHomeLineUp = True Then
                            Dim drArrHome As DataRow() = m_dtHome.Select("STARTING_POSITION<=11", "LAST_NAME ASC")
                            For Each drSortHome As DataRow In drArrHome
                                dtSortHome.ImportRow(drSortHome)
                            Next
                            Dim drArrHomeBench As DataRow() = m_dtHome.Select("STARTING_POSITION>11", "LAST_NAME ASC")
                            For Each drSortHomeBench As DataRow In drArrHomeBench
                                dtSortHome.ImportRow(drSortHomeBench)
                            Next
                        Else
                            dvHome.Sort = "LAST_NAME ASC"
                            dtSortHome = dvHome.ToTable()
                        End If
                        ButtonsHomeTeamTagged(dtSortHome, True)
                    End If
                End If
                If m_dtAway IsNot Nothing Then
                    If m_dtAway.Rows.Count > 0 Then
                        If Not m_dtAway.Columns.Contains("SORT_NUMBER") Then
                            Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                            dcAway.DataType = System.Type.GetType("System.Int32")
                            m_dtAway.Columns.Add(dcAway)
                            For Each drAway As DataRow In m_dtAway.Rows
                                If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drAway.Item("SORT_NUMBER") = 30000
                                Else
                                    drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvAway As New DataView(m_dtAway)
                        Dim dtSortAway As New DataTable
                        dtSortAway = m_dtAway.Clone
                        If m_blnVisitLineUp = True Then
                            Dim drArrAway As DataRow() = m_dtAway.Select("STARTING_POSITION<=11", "LAST_NAME ASC")
                            For Each drSortAway As DataRow In drArrAway
                                dtSortAway.ImportRow(drSortAway)
                            Next
                            Dim drArrAwayBench As DataRow() = m_dtAway.Select("STARTING_POSITION>11", "LAST_NAME ASC")
                            For Each drSortAwayBench As DataRow In drArrAwayBench
                                dtSortAway.ImportRow(drSortAwayBench)
                            Next
                        Else
                            dvAway.Sort = "LAST_NAME ASC"
                            dtSortAway = dvAway.ToTable()
                        End If
                        ButtonsVisitTeamTagged(dtSortAway, True)
                    End If
                End If
            End If
            'SHIRLEY -  IMPLEMENTED FOR POSITION
            If m_objclsLoginDetails.strSortOrder = "Position" Then
                frmMain.CheckMenuItem("SortByUniformToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByLastNameToolStripMenuItem", False)
                frmMain.CheckMenuItem("SortByPositionToolStripMenuItem", True)
                If m_dtHome IsNot Nothing Then
                    If m_dtHome.Rows.Count > 0 Then
                        If Not m_dtHome.Columns.Contains("SORT_NUMBER") Then
                            Dim dcHome As DataColumn = New DataColumn("SORT_NUMBER")
                            dcHome.DataType = System.Type.GetType("System.Int32")
                            m_dtHome.Columns.Add(dcHome)
                            For Each drHome As DataRow In m_dtHome.Rows
                                If Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drHome.Item("SORT_NUMBER") = 30000
                                Else
                                    drHome.Item("SORT_NUMBER") = Convert.ToInt32(drHome.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvHome As New DataView(m_dtHome)
                        Dim dtSortHome As New DataTable
                        dtSortHome = m_dtHome.Clone
                        If m_blnHomeLineUp = True Then
                            Dim drArrHome As DataRow() = m_dtHome.Select("STARTING_POSITION<=11", "STARTING_POSITION ASC")
                            For Each drSortHome As DataRow In drArrHome
                                dtSortHome.ImportRow(drSortHome)
                            Next
                            Dim drArrHomeBench As DataRow() = m_dtHome.Select("STARTING_POSITION>11", "STARTING_POSITION ASC")
                            For Each drSortHomeBench As DataRow In drArrHomeBench
                                dtSortHome.ImportRow(drSortHomeBench)
                            Next
                        Else
                            dvHome.Sort = "SORT_NUMBER ASC"
                            dtSortHome = dvHome.ToTable()
                        End If
                        ButtonsHomeTeamTagged(dtSortHome, True)
                    End If
                End If
                If m_dtAway IsNot Nothing Then
                    If m_dtAway.Rows.Count > 0 Then
                        If Not m_dtAway.Columns.Contains("SORT_NUMBER") Then
                            Dim dcAway As DataColumn = New DataColumn("SORT_NUMBER")
                            dcAway.DataType = System.Type.GetType("System.Int32")
                            m_dtAway.Columns.Add(dcAway)
                            For Each drAway As DataRow In m_dtAway.Rows
                                If Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER")) = 0 Then
                                    drAway.Item("SORT_NUMBER") = 30000
                                Else
                                    drAway.Item("SORT_NUMBER") = Convert.ToInt32(drAway.Item("DISPLAY_UNIFORM_NUMBER"))
                                End If
                            Next
                        End If
                        Dim dvAway As New DataView(m_dtAway)
                        Dim dtSortAway As New DataTable
                        dtSortAway = m_dtAway.Clone
                        If m_blnVisitLineUp = True Then
                            Dim drArrAway As DataRow() = m_dtAway.Select("STARTING_POSITION<=11", "STARTING_POSITION ASC")
                            For Each drSortAway As DataRow In drArrAway
                                dtSortAway.ImportRow(drSortAway)
                            Next
                            Dim drArrAwayBench As DataRow() = m_dtAway.Select("STARTING_POSITION>11", "STARTING_POSITION ASC")
                            For Each drSortAwayBench As DataRow In drArrAwayBench
                                dtSortAway.ImportRow(drSortAwayBench)
                            Next
                        Else
                            dvAway.Sort = "SORT_NUMBER ASC"
                            dtSortAway = dvAway.ToTable()
                        End If
                        ButtonsVisitTeamTagged(dtSortAway, True)
                    End If
                End If
            End If

            'BindCoachInfo()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' T1 workflow - Restart Game
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub RestartExistingGame()
        Try
            BindTeamRostersByLineUp()
            If m_blnHomeLineUp = False And m_blnVisitLineUp = False Then
                BindTeamRosters()
            End If
            SortTeamPlayers()
            BindPBPData()
            GetCurrentPlayersAfterRestart()
            BindCoachInfo()
            BindCommentaryData()
            SetPeriod()
            frmMain.FillFormControls()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DisableControls(ByVal EnableType As Boolean)
        Try
            pnlHome.Enabled = EnableType
            pnlHomeBench.Enabled = EnableType
            pnlVisit.Enabled = EnableType
            pnlVisitBench.Enabled = EnableType
            txtTime.Enabled = EnableType
            txtHeadline.Enabled = EnableType
            txtComment.Enabled = EnableType
            txtTime.Enabled = EnableType
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DisplayComments()
        Try
            BindCommentaryData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub txtTime_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTime.Leave
        Try
            If (txtTime.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtTime.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtHeadline_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHeadline.Leave
        Try
            If (txtHeadline.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtHeadline.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub txtComment_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtComment.Leave
        Try
            If (txtComment.Text <> "") Then
                m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtComment.Text, Nothing, 1, 0)
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnHomePlayer12_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer12.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer12.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer12.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer12.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnHomePlayer13_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer13.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer13.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer13.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer13.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer14_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer14.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer14.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer14.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer14.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer15.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer15.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer15.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer15.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer16_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer16.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer16.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer16.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer16.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer17_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer17.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer17.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer17.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer17.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer18_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer18.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer18.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer18.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer18.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer19_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer19.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer19.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer19.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer19.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer20_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer20.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer20.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer20.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer20.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer21_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer21.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer21.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer21.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer21.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer22_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer22.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer22.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer22.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer22.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer23.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer23.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer23.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer23.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer24.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer24.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer24.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer24.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer25_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer25.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer25.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer25.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer25.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer26_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer26.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer26.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer26.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer26.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer27_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer27.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer27.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer27.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer27.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer28_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer28.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer28.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer28.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer28.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer29_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer29.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer29.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer29.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer29.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer30_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer30.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer30.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer30.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer30.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer31_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer31.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer31.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer31.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer31.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer32_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer32.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer32.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer32.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer32.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer33_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer33.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer33.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer33.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer33.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer34_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer34.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer34.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer34.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer34.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer35_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer35.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer35.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer35.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer35.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer36_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer36.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer36.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer36.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer36.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer37_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer37.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer37.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer37.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer37.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer38_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer38.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer38.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer38.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer38.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer39_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer39.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer39.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer39.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer39.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer40_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer40.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer40.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer40.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer40.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnHomePlayer41_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnHomePlayer41.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnHomePlayer41.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnHomePlayer41.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnHomePlayer41.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnVisitPlayer12_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer12.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer12.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer12.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer12.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnVisitPlayer13_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer13.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer13.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer13.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer13.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer14_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer14.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer14.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer14.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer14.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer15.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer15.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer15.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer15.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer16_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer16.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer16.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer16.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer16.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer17_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer17.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer17.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer17.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer17.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer18_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer18.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer18.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer18.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer18.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer19_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer19.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer19.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer19.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer19.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer20_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer20.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer20.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer20.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer20.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer21_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer21.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer21.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer21.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer21.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer22_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer22.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer22.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer22.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer22.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer23.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer23.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer23.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer23.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer24.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer24.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer24.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer24.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer25_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer25.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer25.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer25.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer25.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer26_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer26.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer26.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer26.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer26.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer27_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer27.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer27.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer27.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer27.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer28_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer28.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer28.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer28.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer28.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer29_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer29.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer29.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer29.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer29.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer30_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer30.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer30.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer30.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer30.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer31_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer31.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer31.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer31.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer31.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer32_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer32.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer32.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer32.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer32.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer33_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer33.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer33.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer33.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer33.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer34_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer34.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer34.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer34.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer34.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer35_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer35.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer35.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer35.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer35.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer36_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer36.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer36.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer36.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer36.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer37_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer37.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer37.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer37.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer37.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer38_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer38.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer38.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer38.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer38.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer39_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer39.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer39.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer39.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer39.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer40_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer40.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer40.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer40.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer40.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Private Sub btnVisitPlayer41_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVisitPlayer41.MouseClick
        Try
            Dim arrlst As New ArrayList
            arrlst = GetNameandID(btnVisitPlayer41.Tag.ToString())

            'AUDIT TRIAL
            m_objUtilAudit.AuditLog(m_objclsGameDetails.HomeTeamAbbrev, m_objclsGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnVisitPlayer41.Text, 1, 0)

            If Not String.IsNullOrEmpty(btnVisitPlayer41.Text.Trim()) Then
                If Not String.IsNullOrEmpty(txtComment.Text) Then
                    txtComment.Text = txtComment.Text.Insert(txtComment.SelectionStart, Convert.ToString(arrlst(1).ToString().Trim())) + " "
                Else
                    txtComment.Text = Convert.ToString(arrlst(1).ToString().Trim()) + " "
                End If
                txtComment.Focus()
                txtComment.ScrollToCaret()
                txtComment.SelectionStart = txtComment.Text.Trim().Length + 1
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Function GetNameandID(ByVal NameID As String) As ArrayList
        Try
            Dim arr As ArrayList = New ArrayList
            Dim str() As String
            NameID = NameID.Trim()
            str = NameID.Split(CChar("*"))
            If str.Length = 3 Then
                arr.Add(str(0))
                arr.Add(str(1))
                arr.Add(str(2))
            ElseIf str.Length = 1 Then
                arr.Add(str(0))
            End If

            Return arr
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '' Arindam 23-Aug

    Private Sub cmbCommentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCommentType.SelectedIndexChanged
        Try
            Dim strComboName As String = ""
            If blnSkipSEChanged = False Then
                If cmbCommentType.Text <> "" Then
                    If (CInt(cmbCommentType.SelectedValue.ToString()) <> 0) Then
                        'AUDIT TRIAL
                        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ComboBoxSelected, cmbCommentType.Text, 1, 0)
                        'If m_objGameDetails.IsEndGame = True Or m_intEndGameCnt > 0 Then
                        '    txtRating1.Enabled = True
                        'Else
                        '    txtRating1.Enabled = False
                        '    txtRating1.Text = ""
                        'End If
                    ElseIf cmbCommentType.SelectedValue Is Nothing Then
                        MessageDialog.Show(M11, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    End If
                Else
                    'txtRating1.Enabled = False
                    'txtRating1.Text = ""
                End If
                'If Not blnSkipSEChanged Then
                '    PopulateDropdowns(Dsref, CType(sender, ComboBox))
                'End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

End Class