﻿#Region " Imports "
Imports System.IO
Imports STATS.Utility
Imports STATS.SoccerBL
#End Region

#Region " Comments "
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmValidateScore
' Author        : Shravani
' Created Date  : 23-Apr-2010
' Description   : To enter penlaty shootout score for Module2 games(Tier 1 and Tier 2)
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------
#End Region

Public Class frmPenaltyShootoutScore

#Region "Constents and Variables"
    Private m_objclsGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objPenaltyShootoutScore As clsPenaltyShootoutScore = clsPenaltyShootoutScore.GetInstance()
    Private m_objUtilAudit As New clsAuditLog
    Private MessageDialog As New frmMessageDialog
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objLoginDetails As clsLoginDetails = clsLoginDetails.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private lsupport As New languagesupport
    Private m_DsScores As New DataSet
    Private m_HomeScoreSO As Integer
    Private m_AwayScoreSO As Integer
    'Dim strmessage As String = "Invalid score!"
    Dim strmessage1 As String = "Fill the score for both teams!"
    'Dim strmessage4 As String = "Score Mismatch for"
    'Dim strmessage5 As String = "Incorrect score for BOTH teams - please correct the score first!"



    Private Const FIRSTHALF As Integer = 2700
    Private Const EXTRATIME As Integer = 900
    Private Const SECONDHALF As Integer = 5400
    Private Const SECONDEXTRA As Integer = 7200

#End Region

    Private Sub frmPenaltyShootoutScore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPenaltyShootoutScore", Nothing, 1, 0)
            Me.Icon = frmMain.Icon
            Me.CenterToParent()
            lblHomeTeamName.Text = m_objclsGameDetails.HomeTeam
            lblAwayTeamName.Text = m_objclsGameDetails.AwayTeam
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnCancel.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnCancel.Text)
                btnOK.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnOK.Text)
                lblHomeTeamName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblHomeTeamName.Text)
                lblAwayTeamName.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), lblAwayTeamName.Text)
            End If

            'FETCHING THE LAST EVENT SCORES FOR THE SELECTED GAME
            m_DsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            m_HomeScoreSO = 0
            m_AwayScoreSO = 0
            GetShootoutScore(m_DsScores)

            'ASSIGNING THE SCORES TO THE RESPECTIVE LABELS
            txtHomeScore.Text = CStr(m_HomeScoreSO)
            txtAwayScore.Text = CStr(m_AwayScoreSO)
            'Dim dsPBP As New DataSet
            'dsPBP = m_objPenaltyShootoutScore.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

            'Dim dr() As DataRow = dsPBP.Tables(0).Select(" EVENT_CODE_ID = 30")
            'If dr.Length > 0 Then
            '    If dr(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
            '        m_objGameDetails.IsScoreWindow = True
            '    Else
            '        m_objGameDetails.IsDetailedWindow = True
            '    End If
            'Else
            '    m_objGameDetails.IsDetailedWindow = False
            '    m_objGameDetails.IsScoreWindow = False
            'End If
            frmMain.GetPenaltyShootoutScore()



        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnOK.Text, Nothing, 1, 0)

            Dim HomeScore As Integer
            Dim AwayScore As Integer

            If txtHomeScore.Text = "" Or txtAwayScore.Text = "" Then
                MessageDialog.Show(strmessage1, "Penalty Shootout Score", MessageDialogButtons.OK, MessageDialogIcon.Information)
                Exit Sub
            End If
            HomeScore = CInt(txtHomeScore.Text)
            AwayScore = CInt(txtAwayScore.Text)

            Call AddPlayByPlayData()
            If InsertPlaybyPlayData() = True Then
                Dim dsPBP As New DataSet
                dsPBP = m_objPenaltyShootoutScore.GetPBPData(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

                Dim dr() As DataRow = dsPBP.Tables(0).Select(" EVENT_CODE_ID = 30")
                If dr.Length > 0 Then
                    If dr(0).Item("OFFENSIVE_PLAYER_ID") Is DBNull.Value Then
                        m_objGameDetails.IsScoreWindow = True
                        m_objGameDetails.IsDetailedWindow = False
                    Else
                        m_objGameDetails.IsDetailedWindow = True
                        m_objGameDetails.IsScoreWindow = False
                    End If
                Else
                    m_objGameDetails.IsDetailedWindow = False
                    m_objGameDetails.IsScoreWindow = False
                End If

            End If
            m_objclsGameDetails.IsEndGame = False
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnCancel.Text, Nothing, 1, 0)
            'Me.DialogResult = Windows.Forms.DialogResult.No
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtHomeScore_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHomeScore.Leave
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtHomeScore.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtAwayScore_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAwayScore.Leave
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.TextBoxEdited, txtAwayScore.Text, Nothing, 1, 0)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub AddPlayByPlayData()
        Try

            Dim drPBPData As DataRow
            Dim dtTimeDiff As DateTime
            Dim DsLastEventDetails As DataSet
            'Delete Shootout Score

            m_objPenaltyShootoutScore.UpdatePenaltyShootoutScore(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_objGameDetails.languageid)
            Dim tempUniqueId As Integer = GetUniqueID()
            Dim intHomeScore As Integer = CalculateScores(m_objGameDetails.HomeTeamID, "OFFENSE_SCORE")
            Dim intAwayScore As Integer = CalculateScores(m_objGameDetails.AwayTeamID, "OFFENSE_SCORE")
            For i As Integer = 1 To txtHomeScore.Text
                drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
                drPBPData("GAME_CODE") = m_objGameDetails.GameCode
                drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drPBPData("UNIQUE_ID") = tempUniqueId
                drPBPData("SEQUENCE_NUMBER") = Convert.ToDecimal(drPBPData("UNIQUE_ID"))
                drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
                dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
                drPBPData("ONFIELD_ID") = GetOnfieldvalue(CInt(drPBPData("PERIOD")))
                drPBPData("OPTICAL_TIMESTAMP") = DBNull.Value
                drPBPData("RECORD_EDITED") = "N"
                drPBPData("EDIT_UID") = 0
                drPBPData("PBP_STRING") = DBNull.Value
                drPBPData("MULTILINGUAL_PBP_STRING") = DBNull.Value
                drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole '& Convert.ToString(m_objGameDetails.SerialNo)
                drPBPData("PROCESSED") = "N"
                drPBPData("CONTINUATION") = "F"
                ''



                DsLastEventDetails = m_objPenaltyShootoutScore.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                ''

                If DsLastEventDetails.Tables(0).Rows.Count > 0 Then
                    drPBPData("TIME_ELAPSED") = CStr(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                Else
                    drPBPData("TIME_ELAPSED") = CStr(getTimeElapsed())
                End If

                'drPBPData("TIME_ELAPSED") = CStr(getTimeElapsed())
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    drPBPData("DEMO_DATA") = "Y"
                Else
                    drPBPData("DEMO_DATA") = "N"
                End If
                drPBPData("OFFENSIVE_PLAYER_ID") = DBNull.Value
                drPBPData("SHOT_DESC") = DBNull.Value
                drPBPData("GOALZONE_ID") = DBNull.Value
                drPBPData("KEEPERZONE_ID") = DBNull.Value
                drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                drPBPData("KEEPERZONE_ID") = DBNull.Value
                drPBPData("SHOT_RESULT") = DBNull.Value
                drPBPData("SHOOTOUT_ROUND") = DBNull.Value
                '
                drPBPData("TEAM_ID") = m_objGameDetails.HomeTeamID
                drPBPData("ORIG_SEQ") = DBNull.Value


                drPBPData("OFFENSE_SCORE") = intHomeScore + 1
                drPBPData("DEFENSE_SCORE") = intAwayScore
                m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                tempUniqueId = tempUniqueId + 1
            Next

            For i As Integer = 1 To txtAwayScore.Text
                drPBPData = m_objGameDetails.PBP.Tables(0).NewRow()
                drPBPData("GAME_CODE") = m_objGameDetails.GameCode
                drPBPData("FEED_NUMBER") = m_objGameDetails.FeedNumber
                drPBPData("UNIQUE_ID") = tempUniqueId
                drPBPData("SEQUENCE_NUMBER") = Convert.ToDecimal(drPBPData("UNIQUE_ID"))
                drPBPData("PERIOD") = m_objGameDetails.CurrentPeriod
                dtTimeDiff = Date.Now - m_objLoginDetails.USIndiaTimeDiff
                drPBPData("SYSTEM_TIME") = DateTimeHelper.GetDateString(dtTimeDiff, DateTimeHelper.OutputFormat.ISOFormat)
                drPBPData("ONFIELD_ID") = GetOnfieldvalue(CInt(drPBPData("PERIOD")))
                drPBPData("OPTICAL_TIMESTAMP") = DBNull.Value
                drPBPData("RECORD_EDITED") = "N"
                drPBPData("EDIT_UID") = 0
                drPBPData("PBP_STRING") = DBNull.Value
                drPBPData("MULTILINGUAL_PBP_STRING") = DBNull.Value
                drPBPData("REPORTER_ROLE") = m_objGameDetails.ReporterRole '& Convert.ToString(m_objGameDetails.SerialNo)
                drPBPData("PROCESSED") = "N"
                drPBPData("CONTINUATION") = "F"
                DsLastEventDetails = m_objPenaltyShootoutScore.GetLastEventDetails(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
                ''

                If DsLastEventDetails.Tables(0).Rows.Count > 0 Then
                    drPBPData("TIME_ELAPSED") = CStr(DsLastEventDetails.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                Else
                    drPBPData("TIME_ELAPSED") = CStr(getTimeElapsed())
                End If

                'drPBPData("TIME_ELAPSED") = CStr(getTimeElapsed())
                If m_objLoginDetails.GameMode = clsLoginDetails.m_enmGameMode.DEMO Then
                    drPBPData("DEMO_DATA") = "Y"
                Else
                    drPBPData("DEMO_DATA") = "N"
                End If
                drPBPData("OFFENSIVE_PLAYER_ID") = DBNull.Value
                drPBPData("SHOT_DESC") = DBNull.Value
                drPBPData("GOALZONE_ID") = DBNull.Value
                drPBPData("KEEPERZONE_ID") = DBNull.Value
                drPBPData("EVENT_CODE_ID") = clsGameDetails.SHOOTOUT_GOAL
                drPBPData("KEEPERZONE_ID") = DBNull.Value
                drPBPData("SHOT_RESULT") = DBNull.Value
                drPBPData("SHOOTOUT_ROUND") = DBNull.Value
                drPBPData("TEAM_ID") = m_objGameDetails.AwayTeamID
                drPBPData("ORIG_SEQ") = DBNull.Value
                drPBPData("OFFENSE_SCORE") = intAwayScore + 1
                drPBPData("DEFENSE_SCORE") = intHomeScore
                m_objGameDetails.PBP.Tables(0).Rows.Add(drPBPData)
                tempUniqueId = tempUniqueId + 1
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub


    Private Function CalculateScores(ByVal TeamID As Integer, ByVal Type As String) As Integer
        Try
            Dim m_dsScores As DataSet
            m_dsScores = m_objGeneral.GetScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
            If m_dsScores.Tables(0).Rows.Count > 0 Then
                If TeamID = CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("TEAM_ID")) Then
                    Select Case Type
                        Case "OFFENSE_SCORE"
                            Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                        Case "DEFENSE_SCORE"
                            Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                    End Select
                Else
                    Select Case Type
                        Case "OFFENSE_SCORE"
                            Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("DEFENSE_SCORE"))
                        Case "DEFENSE_SCORE"
                            Return CInt(m_dsScores.Tables(0).Rows(m_dsScores.Tables(0).Rows.Count - 1).Item("OFFENSE_SCORE"))
                    End Select
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function InsertPlaybyPlayData() As Boolean
        Try
            'code to save the data into SQL
            Dim dsTempPBPData As New DataSet
            Dim dsPBP As New DataSet
            m_objGameDetails.PBP.DataSetName = "SoccerEventData"
            dsTempPBPData = m_objGameDetails.PBP
            Dim strPBPXMLData As String = dsTempPBPData.GetXml()
            'If m_objGameDetails.IsEditMode Then
            'm_objPenaltyShootoutScore.UpdatePenaltyShootoutScore(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_objGameDetails.languageid)
            'Else
            m_objPenaltyShootoutScore.InsertPBPDataToSQL(strPBPXMLData, m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole, m_objGameDetails.languageid)
            'End If

            m_objPenaltyShootoutScore.UpdateTeamGamePenaltyShootoutScores(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.ReporterRole)

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        Finally
            For i As Integer = 0 To m_objGameDetails.PBP.Tables.Count - 1
                m_objGameDetails.PBP.Tables(i).Rows.Clear()
            Next
            m_objGameDetails.IsContinuationEvent = False
        End Try
    End Function
    Private Function GetUniqueID() As Integer
        Try
            Return m_objGeneral.GetUniqueID(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetOnfieldvalue(ByVal period As Integer) As Integer
        Try
            Dim intOnField As Integer = m_objGeneral.GetOnfieldValue(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, period)
            Return intOnField
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function getTimeElapsed() As Integer
        Try
            If m_objGameDetails.CurrentPeriod = 2 Then
                Return FIRSTHALF
            Else
                Return EXTRATIME
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub GetShootoutScore(ByVal Scores As DataSet)
        Try
            If Scores.Tables.Count > 0 Then
                If Scores.Tables(0).Rows.Count > 0 Then
                    Dim intRowCount As Integer
                    For intRowCount = 0 To Scores.Tables(0).Rows.Count - 1
                        If CInt(Scores.Tables(0).Rows(intRowCount).Item("EVENT_CODE_ID")) = clsGameDetails.SHOOTOUT_GOAL Then
                            If CInt(Scores.Tables(0).Rows(intRowCount).Item("TEAM_ID")) = m_objGameDetails.HomeTeamID Then
                                m_HomeScoreSO = m_HomeScoreSO + 1
                            Else
                                m_AwayScoreSO = m_AwayScoreSO + 1
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class