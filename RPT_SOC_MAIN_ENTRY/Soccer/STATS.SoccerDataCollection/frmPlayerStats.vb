﻿#Region " Options "
Option Strict On
Option Explicit On
#End Region

#Region "Imports"
Imports STATS.SoccerBL
Imports STATS.Utility
Imports System.Text
Imports System.IO

#End Region

#Region "Comments"
'----------------------------------------------------------------------------------------------------------------------------------------------
' Class name    : frmPlayerStats
' Author        : Shravani
' Created Date  : 10-Nov-09
' Description   : DIPLAYS THE PLAYER STATISTICS
'
'----------------------------------------------------------------------------------------------------------------------------------------------
' Modification Log :
'----------------------------------------------------------------------------------------------------------------------------------------------
'ID             | Modified By         | Modified date     | Description
'----------------------------------------------------------------------------------------------------------------------------------------------
'               |                     |                   |
'               |                     |                   |
'----------------------------------------------------------------------------------------------------------------------------------------------

#End Region

Public Class frmPlayerStats

    Private m_strBuilder As StringBuilder
    Private m_objPlayerStats As clsPlayerStats = clsPlayerStats.GetInstance()
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Private m_objutility As New clsUtility
    Private m_objUtilAudit As New clsAuditLog
    Private m_objTeamSetup As New clsTeamSetup
    Private lsupport As New languagesupport
    Dim strheadertext1 As String = "Goalkeeper"
    Dim strheadertext2 As String = "PLAYER STATISTICS"
    Dim strheadertext3 As String = "TEAM TOTAL"
    Dim strheadertext4 As String = "Players"



    Private Sub frmPlayerStats_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.temp, Nothing, Nothing, 0, 1)
        m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.Formname, "frmPlayerStats", Nothing, 1, 0)
        Me.Icon = New Icon(Application.StartupPath & "\\Resources\\Soccer.ico", 256, 256)

        If CInt(m_objGameDetails.languageid) <> 1 Then
            Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
            btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
            btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
            btnPrint.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnPrint.Text)
            btnRefresh.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnRefresh.Text)
            strheadertext1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext1)
            strheadertext2 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext2)
            strheadertext3 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext3)
            strheadertext4 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strheadertext4)
        End If
        If (m_objGameDetails.CoverageLevel = 6) Then
            DisplayPZPlayerStats()
        Else
            DisplayPlayerStats()
        End If
    End Sub

    Public Sub DisplayPlayerStats()
        Try
            m_strBuilder = New StringBuilder
            Dim dsPlayerStats As New DataSet
            Dim strCurrentTime() As String
            Dim intMin As Integer
            Dim intSec As Integer
            'Dim strTime As String = frmMain.UdcRunningClock1.URCCurrentTime
            'strCurrentTime = strTime.Split(CChar(":"))
            'intMin = CInt(strCurrentTime(0).ToString)

            If m_objGameDetails.ModuleID = 1 Then
                Dim strTime As String = frmMain.UdcRunningClock1.URCCurrentTime
                strCurrentTime = strTime.Split(CChar(":"))
                intMin = CInt(strCurrentTime(0).ToString)
                If m_objGameDetails.IsDefaultGameClock = False Then
                    If (m_objGameDetails.CurrentPeriod = 2) Then
                        intMin = 45 + intMin
                    ElseIf (m_objGameDetails.CurrentPeriod = 3) Then
                        intMin = 90 + intMin
                    ElseIf (m_objGameDetails.CurrentPeriod = 4) Then
                        intMin = 135 + intMin
                    Else
                        intMin = intMin
                    End If
                End If
            ElseIf m_objGameDetails.ModuleID = 2 Then
                intSec = CInt((m_objPlayerStats.GetMaxTimeElapsed(m_objGameDetails.GameCode, m_objGameDetails.CurrentPeriod)))
                If intSec > 0 Then
                    intSec = intSec + 30
                End If
                intMin = CInt(intSec / 60)
                'If intMin >= 1 Then
                '    intMin = intMin + 1
                'End If
                If (m_objGameDetails.CurrentPeriod = 2) Then
                    intMin = 45 + intMin
                ElseIf (m_objGameDetails.CurrentPeriod = 3) Then
                    intMin = 90 + intMin
                ElseIf (m_objGameDetails.CurrentPeriod = 4) Then
                    intMin = 135 + intMin
                Else
                    intMin = intMin
                End If
            End If


            m_objPlayerStats.InsertPlayerStatsToSummary(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, intMin, m_objGameDetails.CoverageLevel)

            dsPlayerStats = m_objPlayerStats.GetPlayerStats(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)
            m_strBuilder.Append("<html><body>")
            m_strBuilder.Append("<head color:Gray style=font-size:14><b>" & strheadertext2 & "</b></HEAD>")

            m_strBuilder.Append("<br><br>")
            m_strBuilder.Append("<table border=0  width=100% style=font-family:@Arial Unicode MS >")
            m_strBuilder.Append("<tr color:Gray style=font-size:14>")
            m_strBuilder.Append("<b>" & m_objGameDetails.HomeTeam.ToUpper & "</b>")
            m_strBuilder.Append("</tr>")
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("<hr>")

            'GOAL KEEPER - HOME
            m_strBuilder.Append("<table border=0 width=100% color:black style=font-family:@Arial Unicode MS;>")
            m_strBuilder.Append("<tr style=background-color:#EFEFEF;font-size:14;>")
            m_strBuilder.Append("<th width = 30% colspan=2 align=left>" & strheadertext1 & "</th>")
            m_strBuilder.Append("<th width = 10% align=right>  Min </th>")
            m_strBuilder.Append("<th width = 5% align=right>  GA </th>")
            m_strBuilder.Append("<th width = 5% align=right>  S </th>")
            m_strBuilder.Append("<th width = 5% align=right>  SOG </th>")
            m_strBuilder.Append("<th width = 5% align=right>  Sv </th>")
            m_strBuilder.Append("<th width = 5% align=right>   </th>")
            m_strBuilder.Append("<th width = 5% align=right>  </th>")
            m_strBuilder.Append("<th width = 5% align=right>   </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FC </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FS </th>")
            m_strBuilder.Append("</tr>")
            m_strBuilder.Append("<tr></tr><tr></tr><tr></tr>")
            Dim dsEvent As DataSet
            Dim minPlayed As Integer
            If dsPlayerStats.Tables(4).Rows.Count > 0 Then
                'If dsPlayerStats.Tables(4).Rows.Count > 1 Then
                '    dsEvent = m_objGeneral.GetGoalieChangeEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.HomeTeamID)
                '    'If dsEvent.Tables(0).Rows.Count > 0 Then
                '    '    Dim timSec As Integer

                '    '    timSec = (CInt(dsPlayerStats.Tables(4).Rows(1).Item("Min")) * 60) - CInt(dsEvent.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                '    '    dsPlayerStats.Tables(4).Rows(1).Item("Min") = CInt(timSec / 60)
                '    '    dsPlayerStats.Tables(4).Rows(0).Item("Min") = CInt(dsPlayerStats.Tables(4).Rows(0).Item("Min")) - CInt(dsPlayerStats.Tables(4).Rows(1).Item("Min"))
                '    'End If
                'End If

                For intR = 0 To dsPlayerStats.Tables(4).Rows.Count - 1
                    'If CInt(dsPlayerStats.Tables(3).Rows(0).Item("Min")) > 1 Then
                    '    dsPlayerStats.Tables(4).Rows(0).Item("Min") = CInt(dsPlayerStats.Tables(4).Rows(0).Item("Min")) + 1
                    'End If
                    m_strBuilder.Append("<tr  style=font-size:14;cell-padding=4;cell-spacing=4>") ' padding:0px; border-collapse:collapse; line-height:5px;
                    m_strBuilder.Append("<td width = 5% align=left> " & dsPlayerStats.Tables(4).Rows(intR).Item("DISPLAY_UNIFORM_NUMBER").ToString & "</td>")
                    m_strBuilder.Append("<td width = 25% align=left>" & dsPlayerStats.Tables(4).Rows(intR).Item("PLAYER").ToString & "</td>")
                    m_strBuilder.Append("<td width = 10% align=right>  " & CInt(dsPlayerStats.Tables(4).Rows(intR).Item("Min")).ToString & "</td>")
                    m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(4).Rows(intR).Item("GA").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(4).Rows(intR).Item("ATT").ToString & "</td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(4).Rows(intR).Item("SHOTS_ON_GOAL").ToString & "</td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(4).Rows(intR).Item("SAVES").ToString & "</td>")
                    m_strBuilder.Append("<td width = 5% align=right></td>")
                    m_strBuilder.Append("<td width = 5% align=right></td>")
                    m_strBuilder.Append("<td width = 5% align=right></td>")
                    m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(4).Rows(intR).Item("FOULS_COMMITTED").ToString & "</td>")
                    m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(4).Rows(intR).Item("FOULS_SUFFERED").ToString & " </td>")
                    m_strBuilder.Append("</tr>")
                Next
            End If
            m_strBuilder.Append("</table>")

            m_strBuilder.Append("<table border=0 width=100% color:black style=font-family:@Arial Unicode MS>")
            m_strBuilder.Append("<tr  style=font-size:14;>")
            m_strBuilder.Append("<th width = 30% colspan=2 align=left> " + strheadertext4 + "</th>")
            m_strBuilder.Append("<th width = 10% align=right>  Min </th>")
            m_strBuilder.Append("<th width = 5% align=right>  G </th>")
            m_strBuilder.Append("<th width = 5% align=right>  A </th>")
            m_strBuilder.Append("<th width = 5% align=right>  S </th>")
            m_strBuilder.Append("<th width = 5% align=right>  SOG </th>")
            m_strBuilder.Append("<th width = 5% align=right>  Crs </th>")
            m_strBuilder.Append("<th width = 5% align=right>  CK </th>")
            m_strBuilder.Append("<th width = 5% align=right>  Off </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FC </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FS </th>")
            m_strBuilder.Append("<br>")
            m_strBuilder.Append("</tr>")
            m_strBuilder.Append("<tr></tr><tr></tr><tr></tr>")
            If dsPlayerStats.Tables(0).Rows.Count > 0 Then
                For intRowCnt = 0 To dsPlayerStats.Tables(0).Rows.Count - 1
                    m_strBuilder.Append("<tr style=font-size:13px; padding:0px; border-collapse:collapse; line-height:5px;>")

                    If dsPlayerStats.Tables(4).Rows.Count > 1 Then
                        'If CInt(dsPlayerStats.Tables(0).Rows(intRowCnt).Item("PLAYER_ID")) = CInt(dsPlayerStats.Tables(4).Rows(1).Item("PLAYER_ID")) Then
                        '    dsPlayerStats.Tables(0).Rows(intRowCnt).Item("MIN") = CInt(dsPlayerStats.Tables(0).Rows(intRowCnt).Item("MIN")) - CInt(dsPlayerStats.Tables(4).Rows(1).Item("Min"))
                        'End If
                    End If
                    Dim strUniform As String = dsPlayerStats.Tables(0).Rows(intRowCnt).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                    If strUniform <> "" Then
                        If strUniform.Length = 1 Then
                            strUniform = "0" & strUniform
                        End If
                    End If
                    'm_strBuilder.Append("<td width = 5%  align=left> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("DISPLAY_UNIFORM_NUMBER").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=left> " & strUniform & " </td>")
                    m_strBuilder.Append("<td width = 25%  align=left> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("PLAYER").ToString & " </td>")
                    m_strBuilder.Append("<td width = 10%  align=right>" & CInt(dsPlayerStats.Tables(0).Rows(intRowCnt).Item("Min")).ToString & "  </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("GOALS").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("ASSISTS").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("ATT").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("SHOTS_ON_GOAL").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("CROSSES").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("CORNER_KICKS").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("OFFSIDES").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("FOULS_COMMITTED").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right>  " & dsPlayerStats.Tables(0).Rows(intRowCnt).Item("FOULS_SUFFERED").ToString & "</td>")
                    m_strBuilder.Append("</tr>")
                    'FOULS_SUFFERED

                Next
            End If

            If dsPlayerStats.Tables(1).Rows.Count > 0 Then

                m_strBuilder.Append("<tr  style=background-color:#EFEFEF;font-size:14;>")
                m_strBuilder.Append("<td width = 30% colspan=2 align=left><b> " & strheadertext3 & "</b> </td>")
                m_strBuilder.Append("<td width = 10% align=right> <b>" & CInt(dsPlayerStats.Tables(1).Rows(0).Item("Min")).ToString & "</b> </td>")
                m_strBuilder.Append("<td width = 5% align=right><b>" & dsPlayerStats.Tables(1).Rows(0).Item("GOALS").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(1).Rows(0).Item("ASSISTS").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(1).Rows(0).Item("ATT").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(1).Rows(0).Item("SHOTS_ON_GOAL").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(1).Rows(0).Item("CROSSES").ToString & " </b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(1).Rows(0).Item("CORNER_KICKS").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>  " & dsPlayerStats.Tables(1).Rows(0).Item("OFFSIDES").ToString & "</b> </td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(1).Rows(0).Item("FOULS_COMMITTED").ToString & "</b> </td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(1).Rows(0).Item("FOULS_SUFFERED").ToString & "</b> </td>")
                m_strBuilder.Append("</tr>")
            End If
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("<br><br><br><br>")

            'AWAY HEADING
            m_strBuilder.Append("<table border=0 width=100%  style=font-family:@Arial Unicode MS >")
            m_strBuilder.Append("<tr color:Gray style=font-size:14>")
            m_strBuilder.Append("<b>" & m_objGameDetails.AwayTeamAbbrev.ToUpper & "</b>")
            m_strBuilder.Append("</tr>")
            m_strBuilder.Append("</table>")

            m_strBuilder.Append("<hr>")
            'GOAL KEEPER - away
            m_strBuilder.Append("<table border=0 width=100% color:black style=font-family:@Arial Unicode MS >")
            m_strBuilder.Append("<tr  style=background-color:#EFEFEF;font-size:14;>")
            m_strBuilder.Append("<th width = 30%  colspan=2 align=left>" & strheadertext1 & "</th>")
            m_strBuilder.Append("<th width = 10% align=right>  Min </th>")
            m_strBuilder.Append("<th width = 5% align=right>  GA </th>")
            m_strBuilder.Append("<th width = 5% align=right>  S </th>")
            m_strBuilder.Append("<th width = 5% align=right>  SOG </th>")
            m_strBuilder.Append("<th width = 5% align=right>  Sv </th>")
            m_strBuilder.Append("<th width = 5% align=right>   </th>")
            m_strBuilder.Append("<th width = 5% align=right>   </th>")
            m_strBuilder.Append("<th width = 5% align=right>   </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FC </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FS </th>")
            m_strBuilder.Append("</tr>")
            m_strBuilder.Append("<tr></tr><tr></tr><tr></tr>")
            If dsPlayerStats.Tables(5).Rows.Count > 0 Then
                If dsPlayerStats.Tables(5).Rows.Count > 1 Then
                    dsEvent = m_objGeneral.GetGoalieChangeEvent(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.AwayTeamID, m_objGameDetails.CoverageLevel)
                    'If dsEvent.Tables(0).Rows.Count > 0 Then
                    '    Dim timSec As Integer

                    '    timSec = (CInt(dsPlayerStats.Tables(5).Rows(1).Item("Min")) * 60) - CInt(dsEvent.Tables(0).Rows(0).Item("TIME_ELAPSED"))
                    '    dsPlayerStats.Tables(5).Rows(1).Item("Min") = CInt(timSec / 60)
                    '    dsPlayerStats.Tables(5).Rows(0).Item("Min") = CInt(dsPlayerStats.Tables(5).Rows(0).Item("Min")) - CInt(dsPlayerStats.Tables(5).Rows(1).Item("Min"))
                    'End If
                End If
                For intRowCnt = 0 To dsPlayerStats.Tables(5).Rows.Count - 1

                    m_strBuilder.Append("<tr  style=font-size:14;>")
                    m_strBuilder.Append("<td width = 5%  align=left> " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("DISPLAY_UNIFORM_NUMBER").ToString & "  </td>")
                    m_strBuilder.Append("<td width = 25%  align=left> " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("PLAYER").ToString & "  </td>")
                    m_strBuilder.Append("<td width = 10% align=right> " & CInt(dsPlayerStats.Tables(5).Rows(intRowCnt).Item("Min")).ToString & " </td>")
                    m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("GA").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("ATT").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("SHOTS_ON_GOAL").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5% align=right>   " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("SAVES").ToString & "</td>")
                    m_strBuilder.Append("<td width = 5% align=right>   </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("FOULS_COMMITTED").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(5).Rows(intRowCnt).Item("FOULS_SUFFERED").ToString & " </td>")
                    m_strBuilder.Append("</tr>")

                Next
            End If
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("<br>")
            m_strBuilder.Append("<table border=0 width=100% color:black style=font-family:@Arial Unicode MS >")
            m_strBuilder.Append("<tr  style=background-color:#EFEFEF;font-size:14;>")
            m_strBuilder.Append("<th width = 30% colspan=2 align=left>" + strheadertext4 + " </th>")
            m_strBuilder.Append("<th width = 10% align=right>  Min </th>")
            m_strBuilder.Append("<th width = 5% align=right>  G </th>")
            m_strBuilder.Append("<th width = 5% align=right>  A </th>")
            m_strBuilder.Append("<th width = 5% align=right>  S </th>")
            m_strBuilder.Append("<th width = 5% align=right>  SOG </th>")
            m_strBuilder.Append("<th width = 5% align=right>  Crs </th>")
            m_strBuilder.Append("<th width = 5% align=right>  CK </th>")
            m_strBuilder.Append("<th width = 5% align=right>  Off </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FC </th>")
            m_strBuilder.Append("<th width = 5% align=right>  FS </th>")
            m_strBuilder.Append("</tr>")
            m_strBuilder.Append("<tr></tr><tr></tr><tr></tr>")
            If dsPlayerStats.Tables(2).Rows.Count > 0 Then
                For intRowCnt = 0 To dsPlayerStats.Tables(2).Rows.Count - 1
                    m_strBuilder.Append("<tr style=font-size:13px; padding:0px; border-collapse:collapse; line-height:5px;>")
                    'If dsPlayerStats.Tables(5).Rows.Count > 1 Then
                    '    If CInt(dsPlayerStats.Tables(2).Rows(intRowCnt).Item("PLAYER_ID")) = CInt(dsPlayerStats.Tables(5).Rows(1).Item("PLAYER_ID")) Then
                    '        dsPlayerStats.Tables(2).Rows(intRowCnt).Item("MIN") = CInt(dsPlayerStats.Tables(2).Rows(intRowCnt).Item("MIN")) - CInt(dsPlayerStats.Tables(5).Rows(1).Item("Min"))
                    '    End If
                    'End If
                    Dim strUniform As String = dsPlayerStats.Tables(2).Rows(intRowCnt).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                    If strUniform <> "" Then
                        If strUniform.Length = 1 Then
                            strUniform = "0" & strUniform
                        End If
                    End If
                    
                    'm_strBuilder.Append("<td width = 5%  align=left> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("DISPLAY_UNIFORM_NUMBER").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=left> " & strUniform & " </td>")
                    m_strBuilder.Append("<td width = 25%  align=left> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("PLAYER").ToString & " </td>")
                    m_strBuilder.Append("<td width = 10%  align=right> " & CInt(dsPlayerStats.Tables(2).Rows(intRowCnt).Item("Min")).ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("GOALS").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("ASSISTS").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("ATT").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("SHOTS_ON_GOAL").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("CROSSES").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("CORNER_KICKS").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("OFFSIDES").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("FOULS_COMMITTED").ToString & " </td>")
                    m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(2).Rows(intRowCnt).Item("FOULS_SUFFERED").ToString & " </td>")
                    m_strBuilder.Append("</tr>")
                Next
            End If

            If dsPlayerStats.Tables(3).Rows.Count > 0 Then
                
                m_strBuilder.Append("<tr  style=background-color:#EFEFEF;font-size:14;>")
                m_strBuilder.Append("<td width = 30% colspan=2 align=left><b> " & strheadertext3 & "</td>")
                m_strBuilder.Append("<td width = 10% align=right> <b>" & CInt(dsPlayerStats.Tables(3).Rows(0).Item("Min")).ToString & "</b> </td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(3).Rows(0).Item("GOALS").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(3).Rows(0).Item("ASSISTS").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(3).Rows(0).Item("ATT").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right><b> " & dsPlayerStats.Tables(3).Rows(0).Item("SHOTS_ON_GOAL").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(3).Rows(0).Item("CROSSES").ToString & "</b> </td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(3).Rows(0).Item("CORNER_KICKS").ToString & "</b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(3).Rows(0).Item("OFFSIDES").ToString & " </b></td>")
                m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(3).Rows(0).Item("FOULS_COMMITTED").ToString & "</b> </td>")
                m_strBuilder.Append("<td width = 5% align=right>  <b> " & dsPlayerStats.Tables(3).Rows(0).Item("FOULS_SUFFERED").ToString & "</b></td>")
                m_strBuilder.Append("</tr>")
            End If
            m_strBuilder.Append("</table>")
            m_strBuilder.Append("</body></html>")
            wbReport.DocumentText = m_strBuilder.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Private Function GetGoalKeeperID(ByVal TeamID As Integer) As Integer
    '    Try
    '        Dim intGoalKeeperID As Integer = -1
    '        Dim dr() As DataRow
    '        If m_objTeamSetup IsNot Nothing Then
    '            If m_objTeamSetup.RosterInfo.Tables.Count > 0 Then
    '                If TeamID = m_objGameDetails.HomeTeamID Then
    '                    dr = m_objTeamSetup.RosterInfo.Tables("HomeCurrent").Select("TEAM_ID = " & TeamID & "AND  POSITION_ID_1 =1 AND  STARTING_POSITION <> 0")
    '                Else
    '                    dr = m_objTeamSetup.RosterInfo.Tables("AwayCurrent").Select("TEAM_ID = " & TeamID & "AND  POSITION_ID_1 =1 AND  STARTING_POSITION <> 0")
    '                End If
    '                If dr.Length > 0 Then
    '                    intGoalKeeperID = CInt(dr(0).Item("PLAYER_ID"))
    '                End If
    '                Return intGoalKeeperID
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnClose.Text, Nothing, 1, 0)
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim path As String
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnSave.Text, Nothing, 1, 0)
            If wbReport.DocumentText = Nothing Then
                'essageDialog.Show("No Document is Present", Me.Text)
                Exit Sub
            Else
                path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                SaveFileDialog1.InitialDirectory = path
                SaveFileDialog1.Filter = "HTML Files|*.htm"
                If SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim fileName As String = SaveFileDialog1.FileName
                    Dim sw As StreamWriter
                    If File.Exists(fileName) = False Then
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    Else
                        File.Delete(fileName)
                        sw = New System.IO.StreamWriter(fileName, True, System.Text.Encoding.Unicode)
                        sw.Write(m_strBuilder)
                        sw.Flush()
                        sw.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnPrint.Text, Nothing, 1, 0)
            PrintDialog1.Document = PrintDocument1
            PrintDialog1.AllowSomePages = True
            PrintDocument1.DefaultPageSettings.Landscape = True
            If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                wbReport.Print()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            m_objUtilAudit.AuditLog(m_objGameDetails.HomeTeamAbbrev, m_objGameDetails.AwayTeamAbbrev, clsAuditLog.AuditOperation.ButtonClicked, btnRefresh.Text, Nothing, 1, 0)
            If (m_objGameDetails.CoverageLevel = 6) Then
                DisplayPZPlayerStats()
            Else
                DisplayPlayerStats()
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub
    Public Sub DisplayPZPlayerStats()
        Try
            m_strBuilder = New StringBuilder
           
            m_strBuilder.Append("<html><body><head color:Gray style=font-size:14><b>" & strheadertext2 & "</b></HEAD>")
          
            m_strBuilder.Append("<br><br><table border=0  width=100% style=font-family:@Arial Unicode MS >")
            m_strBuilder.Append("<tr color:Gray style=font-size:14><b>" & m_objGameDetails.HomeTeam.ToUpper & "</b></tr></table><hr>")
            PZCreateHTML(4, 0, 1)
            'AWAY HEADING
            m_strBuilder.Append("<table border=0 width=100%  style=font-family:@Arial Unicode MS >")
            m_strBuilder.Append("<tr color:Gray style=font-size:14> <b>" & m_objGameDetails.AwayTeamAbbrev.ToUpper & "</b></tr>")
            m_strBuilder.Append("</table> <hr>")
            PZCreateHTML(5, 2, 3)
            m_strBuilder.Append("</body></html>")
            wbReport.DocumentText = m_strBuilder.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function PZCreateHTML(ByVal FTable As Integer, ByVal STable As Integer, ByVal TTable As Integer) As String
        Dim dsPlayerStats As New DataSet
        Dim strCurrentTime() As String
        Dim Minute As Integer
        Dim strTime As String = frmMain.UdcRunningClock1.URCCurrentTime
        strCurrentTime = strTime.Split(CChar(":"))
        Minute = CInt(strCurrentTime(0).ToString)
        m_objPlayerStats.InsertPlayerStatsToSummary(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, If(m_objGameDetails.IsDefaultGameClock, Minute, TimeBasedOnPeriod(Minute)), m_objGameDetails.CoverageLevel)
        dsPlayerStats = m_objPlayerStats.GetPlayerStats(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, m_objGameDetails.languageid)

        'GOAL KEEPER - HOME
        m_strBuilder.Append("<table border=0 width=100% color:black style=font-family:@Arial Unicode MS;>")
        m_strBuilder.Append("<tr style=background-color:#EFEFEF;font-size:14;>")
        m_strBuilder.Append("<th width = 30% colspan=2 align=left>" & strheadertext1 & "</th>")
        m_strBuilder.Append("<th width = 15% align=right>  Min </th>")
        m_strBuilder.Append("<th width = 5% align=right>  GA </th>")
        m_strBuilder.Append("<th width = 5% align=right>  S </th>")
        m_strBuilder.Append("<th width = 5% align=right>  SOG </th>")
        m_strBuilder.Append("<th width = 5% align=right>  Sv </th>")
        m_strBuilder.Append("<th width = 5% align=right>  </th><th width = 5% align=right>   </th><th width = 5% align=right>   </th><th width = 5% align=right>   </th>")
        m_strBuilder.Append("<th width = 5% align=right>  FC </th>")
        m_strBuilder.Append("<th width = 5% align=right>  FS </th>")
        m_strBuilder.Append("</tr> <tr></tr><tr></tr><tr></tr>")
        If dsPlayerStats.Tables(FTable).Rows.Count > 0 Then
            For intR = 0 To dsPlayerStats.Tables(FTable).Rows.Count - 1
                m_strBuilder.Append("<tr  style=font-size:14;cell-padding=4;cell-spacing=4>") ' padding:0px; border-collapse:collapse; line-height:5px;
                m_strBuilder.Append("<td width = 5% align=left> " & dsPlayerStats.Tables(FTable).Rows(intR).Item("DISPLAY_UNIFORM_NUMBER").ToString & "</td>")
                m_strBuilder.Append("<td width = 25% align=left>" & dsPlayerStats.Tables(FTable).Rows(intR).Item("PLAYER").ToString & "</td>")
                m_strBuilder.Append("<td width = 15% align=right>  " & CInt(dsPlayerStats.Tables(FTable).Rows(intR).Item("Min")).ToString & "</td>")
                m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(FTable).Rows(intR).Item("GA").ToString & " </td>")
                m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(FTable).Rows(intR).Item("ATT").ToString & "</td>")
                m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(FTable).Rows(intR).Item("SHOTS_ON_GOAL").ToString & "</td>")
                m_strBuilder.Append("<td width = 5% align=right>  " & dsPlayerStats.Tables(FTable).Rows(intR).Item("SAVES").ToString & "</td>")
                m_strBuilder.Append("<td width = 5% align=right></td><td width = 5% align=right></td><td width = 5% align=right> <td width = 5% align=right></td>")
                m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(FTable).Rows(intR).Item("FOULS_COMMITTED").ToString & "</td>")
                m_strBuilder.Append("<td width = 5% align=right> " & dsPlayerStats.Tables(FTable).Rows(intR).Item("FOULS_SUFFERED").ToString & " </td>")
                m_strBuilder.Append("</tr>")
            Next
        End If
        m_strBuilder.Append("</table>")
        m_strBuilder.Append("<table border=0 width=100% color:black style=font-family:@Arial Unicode MS>")
        m_strBuilder.Append("<tr  style=font-size:14;>")
        m_strBuilder.Append("<th width = 30% colspan=2 align=left> " + strheadertext4 + "</th>")
        m_strBuilder.Append("<th width = 15% align=right>  Min </th>")
        m_strBuilder.Append("<th width = 5% align=right>  G </th>")
        m_strBuilder.Append("<th width = 5% align=right>  A </th>")
        m_strBuilder.Append("<th width = 5% align=right>  S </th>")
        m_strBuilder.Append("<th width = 5% align=right>  SOG </th>")
        m_strBuilder.Append("<th width = 5% align=right>  SOF </th>")
        m_strBuilder.Append("<th width = 5% align=right>  SB </th>")
        m_strBuilder.Append("<th width = 5% align=right>  Crs </th>")
        m_strBuilder.Append("<th width = 5% align=right>  CK </th>")
        m_strBuilder.Append("<th width = 5% align=right>  Off </th>")
        m_strBuilder.Append("<th width = 5% align=right>  FC </th>")
        m_strBuilder.Append("<th width = 5% align=right>  FS </th>")
        m_strBuilder.Append("<br> </tr> <tr></tr><tr></tr><tr></tr>")
        If dsPlayerStats.Tables(STable).Rows.Count > 0 Then
            For intRowCnt = 0 To dsPlayerStats.Tables(STable).Rows.Count - 1
                m_strBuilder.Append("<tr style=font-size:13px; padding:0px; border-collapse:collapse; line-height:5px;>")

                Dim strUniform As String = dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("DISPLAY_UNIFORM_NUMBER").ToString()
                If strUniform <> "" Then
                    If strUniform.Length = 1 Then
                        strUniform = "0" & strUniform
                    End If
                End If
                m_strBuilder.Append("<td width = 5%  align=left> " & strUniform & " </td>")
                m_strBuilder.Append("<td width = 25%  align=left> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("PLAYER").ToString & " </td>")
                m_strBuilder.Append("<td width = 10%  align=right>" & CInt(dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("Min")).ToString & "  </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("GOALS").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("ASSISTS").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("ATT").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("SHOTS_ON_GOAL").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & (CInt(dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("ATT")) - CInt(dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("SHOTS_ON_GOAL")) - CInt(dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("BLOCKED_SHOTS"))).ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("BLOCKED_SHOTS").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("CROSSES").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("CORNER_KICKS").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("OFFSIDES").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right> " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("FOULS_COMMITTED").ToString & " </td>")
                m_strBuilder.Append("<td width = 5%  align=right>  " & dsPlayerStats.Tables(STable).Rows(intRowCnt).Item("FOULS_SUFFERED").ToString & "</td>")
                m_strBuilder.Append("</tr>")
                'FOULS_SUFFERED

            Next
        End If

        If dsPlayerStats.Tables(TTable).Rows.Count > 0 Then
            m_strBuilder.Append("<tr  style=background-color:#EFEFEF;font-size:14;>")
            m_strBuilder.Append("<td width = 30% colspan=2 align=left><b> " & strheadertext3 & "</b> </td>")
            m_strBuilder.Append("<td width = 10% align=right> <b>" & CInt(dsPlayerStats.Tables(TTable).Rows(0).Item("Min")).ToString & "</b> </td>")
            m_strBuilder.Append("<td width = 5% align=right><b>" & dsPlayerStats.Tables(TTable).Rows(0).Item("GOALS").ToString & "</b></td>")
            m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(TTable).Rows(0).Item("ASSISTS").ToString & "</b></td>")
            m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(TTable).Rows(0).Item("ATT").ToString & "</b></td>")
            m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(TTable).Rows(0).Item("SHOTS_ON_GOAL").ToString & "</b></td>")
            m_strBuilder.Append("<td width = 5% align=right> <b>" & (CInt(dsPlayerStats.Tables(TTable).Rows(0).Item("ATT")) - CInt(dsPlayerStats.Tables(TTable).Rows(0).Item("SHOTS_ON_GOAL")) - CInt(dsPlayerStats.Tables(TTable).Rows(0).Item("BLOCKED_SHOTS"))).ToString & "</b> </td>")
            m_strBuilder.Append("<td width = 5% align=right> <b>" & dsPlayerStats.Tables(TTable).Rows(0).Item("BLOCKED_SHOTS").ToString & "</b></td>")
            m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(TTable).Rows(0).Item("CROSSES").ToString & "</b> </td>")
            m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(TTable).Rows(0).Item("CORNER_KICKS").ToString & "</b></td>")
            m_strBuilder.Append("<td width = 5% align=right> <b>  " & dsPlayerStats.Tables(TTable).Rows(0).Item("OFFSIDES").ToString & "</b> </td>")
            m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(TTable).Rows(0).Item("FOULS_COMMITTED").ToString & "</b> </td>")
            m_strBuilder.Append("<td width = 5% align=right> <b> " & dsPlayerStats.Tables(TTable).Rows(0).Item("FOULS_SUFFERED").ToString & "</b> </td>")
            m_strBuilder.Append("</tr>")
        End If
        m_strBuilder.Append("</table> <br><br><br><br>")

    End Function
    Private Function TimeBasedOnPeriod(ByVal Minute As Integer) As Integer
        Try
            Dim timeElapsed As New Dictionary(Of Integer, Integer)
            timeElapsed.Add(1, 0)
            timeElapsed.Add(2, 45) 'FIRSTHALF
            timeElapsed.Add(3, 90) 'SECONDHALF
            timeElapsed.Add(4, 135) 'FIRSTEXTRA
            Return Minute + timeElapsed.Item(m_objGameDetails.CurrentPeriod)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class