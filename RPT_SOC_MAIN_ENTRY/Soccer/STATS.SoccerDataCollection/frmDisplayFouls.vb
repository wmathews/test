﻿Public Class frmDisplayFouls
    Private m_objGameDetails As clsGameDetails = clsGameDetails.GetInstance
    Private m_objclsTeamSetup As clsTeamSetup = clsTeamSetup.GetInstance()
    Private m_objGeneral As STATS.SoccerBL.clsGeneral = STATS.SoccerBL.clsGeneral.GetInstance()
    Private MessageDialog As New frmMessageDialog
    Dim CurPeriod As Integer
    Dim intTeamID As Integer
    Private lsupport As New languagesupport
    Dim strmessage As String = "Please select the Foul Event to associate Booking ."
    Dim strmessage1 As String = "No Foul available to attach with this booking!"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            m_objGameDetails.FouledSNO = 0
            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub frmDisplayFouls_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim m_dsFouls As DataSet
            m_dsFouls = m_objGeneral.FetchFouls(m_objGameDetails.GameCode, m_objGameDetails.FeedNumber, CurPeriod, intTeamID, m_objGameDetails.CommentsTime)
            If CInt(m_objGameDetails.languageid) <> 1 Then
                Me.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), Me.Text)
                btnSave.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnSave.Text)
                btnClose.Text = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), btnClose.Text)
                Dim g1 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Event")
                Dim g2 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Period")
                Dim g3 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Team")
                Dim g4 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Time")
                Dim g5 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "Type")
                Dim g6 As String = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), "By Player")
                strmessage = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage)
                strmessage1 = lsupport.LanguageTranslate(CInt(m_objGameDetails.languageid), strmessage1)
                lvPBP.Columns(0).Text = g1
                lvPBP.Columns(1).Text = g2
                lvPBP.Columns(2).Text = g3
                lvPBP.Columns(3).Text = g4
                lvPBP.Columns(4).Text = g5
                lvPBP.Columns(5).Text = g6

            End If

            If m_dsFouls.Tables(0).Rows.Count > 0 Then
                DisplayFouls(m_dsFouls)
            Else
                MessageDialog.Show(strmessage1, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                m_objGameDetails.FouledSNO = 0
                Me.Close()
            End If

        Catch EX As Exception
            MessageDialog.Show(EX, Me.Text)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lvPBP.Items.Count > 0 Then
                If lvPBP.SelectedItems.Count = 0 Then
                    MessageDialog.Show(strmessage, Me.Text, MessageDialogButtons.OK, MessageDialogIcon.Information)
                    m_objGameDetails.FouledSNO = 0
                    Exit Sub
                End If
                If lvPBP.SelectedItems.Count > 0 Then
                    m_objGameDetails.FouledSNO = lvPBP.SelectedItems.Item(0).SubItems(6).Text
                End If
            Else
                m_objGameDetails.FouledSNO = 0
            End If

            Me.Close()
        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Private Sub DisplayFouls(ByVal PBP As DataSet)
        Try
            lvPBP.Items.Clear()
            Dim intRowCount As Integer

            Dim ElapsedTime As Integer
            Dim CurrPeriod As Integer
            Dim StrTime As String = "00:00"

            If Not PBP Is Nothing Then
                If (PBP.Tables(0).Rows.Count > 0) Then
                    For intRowCount = 0 To PBP.Tables(0).Rows.Count - 1
                        Dim lvwpbp As New ListViewItem("Foul")
                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("TEAM_ABBREV").ToString)
                        If Not IsDBNull(CDbl(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))) Then
                            ElapsedTime = CInt(PBP.Tables(0).Rows(intRowCount).Item("TIME_ELAPSED"))
                            StrTime = frmModule1PBP.TimeAfterRegularInterval(ElapsedTime, PBP.Tables(0).Rows(intRowCount).Item("PERIOD").ToString)
                        End If
                        If m_objGameDetails.ModuleID = 1 Then
                            lvwpbp.SubItems.Add(StrTime)
                        Else
                            Dim strCurrTime() As String
                            strCurrTime = StrTime.Split(CChar(":"))
                            lvwpbp.SubItems.Add(strCurrTime(0))
                        End If

                        ''If PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_DESC") IsNot DBNull.Value Then
                        ''    lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("EVENT_CODE_DESC").ToString) '42
                        ''End If
                        If PBP.Tables(0).Rows(intRowCount).Item("FOUL_TYPE") IsNot DBNull.Value Then
                            If CInt(PBP.Tables(0).Rows(intRowCount).Item("FOUL_TYPE_ID")) = 1 Or CInt(PBP.Tables(0).Rows(intRowCount).Item("FOUL_TYPE_ID")) = 5 Then
                            End If
                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("FOUL_TYPE").ToString)
                        Else
                            lvwpbp.SubItems.Add("")
                        End If

                        If PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER") IsNot DBNull.Value Then
                            lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("OFFENSIVE_PLAYER").ToString)
                        Else
                            lvwpbp.SubItems.Add("")
                        End If

                        lvwpbp.SubItems.Add(PBP.Tables(0).Rows(intRowCount).Item("SEQUENCE_NUMBER").ToString)
                        lvPBP.Items.Add(lvwpbp)

                    Next

                End If
            End If

        Catch ex As Exception
            MessageDialog.Show(ex, Me.Text)
        End Try
    End Sub

    Public Sub New(ByVal intPeriod As Integer, ByVal TeamID As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        CurPeriod = intPeriod

        intTeamID = TeamID
        ' Add any initialization after the InitializeComponent() call.

    End Sub

End Class