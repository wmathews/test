﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreferences
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.radAmericanFormat = New System.Windows.Forms.RadioButton()
        Me.radEuropeanFormat = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ChkHalfTimeSwap = New System.Windows.Forms.CheckBox()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 189)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(366, 14)
        Me.sstMain.TabIndex = 266
        Me.sstMain.Text = "StatusStrip1"
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(0, -312)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(376, 14)
        Me.picReporterBar.TabIndex = 264
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(0, -330)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(376, 14)
        Me.picTopBar.TabIndex = 263
        Me.picTopBar.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.PictureBox1.Location = New System.Drawing.Point(-4, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(370, 23)
        Me.PictureBox1.TabIndex = 268
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.PictureBox2.Location = New System.Drawing.Point(-4, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(370, 15)
        Me.PictureBox2.TabIndex = 267
        Me.PictureBox2.TabStop = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(201, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 272
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(282, 10)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 271
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 15)
        Me.Label1.TabIndex = 273
        Me.Label1.Text = "Game Clock:"
        '
        'radAmericanFormat
        '
        Me.radAmericanFormat.AutoSize = True
        Me.radAmericanFormat.ForeColor = System.Drawing.Color.Black
        Me.radAmericanFormat.Location = New System.Drawing.Point(121, 74)
        Me.radAmericanFormat.Name = "radAmericanFormat"
        Me.radAmericanFormat.Size = New System.Drawing.Size(99, 19)
        Me.radAmericanFormat.TabIndex = 275
        Me.radAmericanFormat.Text = "0-45  and  0-45"
        Me.radAmericanFormat.UseVisualStyleBackColor = True
        '
        'radEuropeanFormat
        '
        Me.radEuropeanFormat.AutoSize = True
        Me.radEuropeanFormat.Checked = True
        Me.radEuropeanFormat.ForeColor = System.Drawing.Color.Black
        Me.radEuropeanFormat.Location = New System.Drawing.Point(121, 52)
        Me.radEuropeanFormat.Name = "radEuropeanFormat"
        Me.radEuropeanFormat.Size = New System.Drawing.Size(105, 19)
        Me.radEuropeanFormat.TabIndex = 274
        Me.radEuropeanFormat.TabStop = True
        Me.radEuropeanFormat.Text = "0-45  and  45-90"
        Me.radEuropeanFormat.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 15)
        Me.Label2.TabIndex = 276
        Me.Label2.Text = "Half Time Swap:"
        '
        'ChkHalfTimeSwap
        '
        Me.ChkHalfTimeSwap.AutoSize = True
        Me.ChkHalfTimeSwap.Checked = True
        Me.ChkHalfTimeSwap.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkHalfTimeSwap.Location = New System.Drawing.Point(121, 112)
        Me.ChkHalfTimeSwap.Name = "ChkHalfTimeSwap"
        Me.ChkHalfTimeSwap.Size = New System.Drawing.Size(15, 14)
        Me.ChkHalfTimeSwap.TabIndex = 277
        Me.ChkHalfTimeSwap.UseVisualStyleBackColor = True
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnCancel)
        Me.picButtonBar.Controls.Add(Me.btnSave)
        Me.picButtonBar.Location = New System.Drawing.Point(0, 147)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(376, 43)
        Me.picButtonBar.TabIndex = 278
        '
        'frmPreferences
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(366, 203)
        Me.Controls.Add(Me.ChkHalfTimeSwap)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.radAmericanFormat)
        Me.Controls.Add(Me.radEuropeanFormat)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!)
        Me.Name = "frmPreferences"
        Me.Text = "Preferences"
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radAmericanFormat As System.Windows.Forms.RadioButton
    Friend WithEvents radEuropeanFormat As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ChkHalfTimeSwap As System.Windows.Forms.CheckBox
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
End Class
