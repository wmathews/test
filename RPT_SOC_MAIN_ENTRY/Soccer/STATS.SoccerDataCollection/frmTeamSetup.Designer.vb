﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTeamSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.radAwayTeam = New System.Windows.Forms.RadioButton()
        Me.radHomeTeam = New System.Windows.Forms.RadioButton()
        Me.lblTeam = New System.Windows.Forms.Label()
        Me.dgvStartingLineup = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvBench = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.cmbFormation = New System.Windows.Forms.ComboBox()
        Me.lblFormation = New System.Windows.Forms.Label()
        Me.lblInjuriesSuspensions = New System.Windows.Forms.Label()
        Me.dgInjuriesSuspensions = New System.Windows.Forms.DataGridView()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnAddPlayer = New System.Windows.Forms.Button()
        Me.btnManager = New System.Windows.Forms.Button()
        Me.lblCoach = New System.Windows.Forms.Label()
        Me.cmbCoach = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbFormationSpecification = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnShirtColor = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblShirtColor = New System.Windows.Forms.Label()
        Me.pnlKit = New System.Windows.Forms.Panel()
        Me.picKit1 = New System.Windows.Forms.PictureBox()
        Me.picKit8 = New System.Windows.Forms.PictureBox()
        Me.picKit5 = New System.Windows.Forms.PictureBox()
        Me.picKit6 = New System.Windows.Forms.PictureBox()
        Me.picKit7 = New System.Windows.Forms.PictureBox()
        Me.picKit3 = New System.Windows.Forms.PictureBox()
        Me.picKit4 = New System.Windows.Forms.PictureBox()
        Me.picKit2 = New System.Windows.Forms.PictureBox()
        Me.pnlHighlight = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblShortColor = New System.Windows.Forms.Label()
        Me.btnShortColor = New System.Windows.Forms.Button()
        Me.picTeamLogo = New System.Windows.Forms.PictureBox()
        Me.sstMain = New System.Windows.Forms.StatusStrip()
        Me.picReporterBar = New System.Windows.Forms.PictureBox()
        Me.picTopBar = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.cdlgShirtShort = New System.Windows.Forms.ColorDialog()
        Me.btnClearShirtColor = New System.Windows.Forms.Button()
        Me.btnClearShortColor = New System.Windows.Forms.Button()
        Me.lblStartLineSent = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.picButtonBar = New System.Windows.Forms.Panel()
        Me.btnShirtNoColor = New System.Windows.Forms.Button()
        Me.btnClearShirtNoColor = New System.Windows.Forms.Button()
        Me.lblShirtNoColor = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.UdcSoccerFormation1 = New STATS.SoccerDataCollection.udcSoccerFormation()
        Me.pnlHeader.SuspendLayout()
        CType(Me.dgvStartingLineup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBench, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgInjuriesSuspensions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlKit.SuspendLayout()
        CType(Me.picKit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picKit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.picButtonBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(84, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.btnCancel.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnCancel.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(921, 12)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 263
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.pnlHeader.Controls.Add(Me.radAwayTeam)
        Me.pnlHeader.Controls.Add(Me.radHomeTeam)
        Me.pnlHeader.Controls.Add(Me.lblTeam)
        Me.pnlHeader.Location = New System.Drawing.Point(72, 33)
        Me.pnlHeader.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(933, 46)
        Me.pnlHeader.TabIndex = 264
        '
        'radAwayTeam
        '
        Me.radAwayTeam.AutoSize = True
        Me.radAwayTeam.ForeColor = System.Drawing.Color.White
        Me.radAwayTeam.Location = New System.Drawing.Point(741, 22)
        Me.radAwayTeam.Name = "radAwayTeam"
        Me.radAwayTeam.Size = New System.Drawing.Size(107, 19)
        Me.radAwayTeam.TabIndex = 266
        Me.radAwayTeam.TabStop = True
        Me.radAwayTeam.Text = "Away team name"
        Me.radAwayTeam.UseVisualStyleBackColor = True
        '
        'radHomeTeam
        '
        Me.radHomeTeam.AutoSize = True
        Me.radHomeTeam.ForeColor = System.Drawing.Color.White
        Me.radHomeTeam.Location = New System.Drawing.Point(741, 5)
        Me.radHomeTeam.Name = "radHomeTeam"
        Me.radHomeTeam.Size = New System.Drawing.Size(108, 19)
        Me.radHomeTeam.TabIndex = 15
        Me.radHomeTeam.TabStop = True
        Me.radHomeTeam.Text = "Home team name"
        Me.radHomeTeam.UseVisualStyleBackColor = True
        '
        'lblTeam
        '
        Me.lblTeam.AutoSize = True
        Me.lblTeam.Font = New System.Drawing.Font("Arial Unicode MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.ForeColor = System.Drawing.Color.White
        Me.lblTeam.Location = New System.Drawing.Point(17, 9)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(115, 25)
        Me.lblTeam.TabIndex = 14
        Me.lblTeam.Text = "Team name"
        '
        'dgvStartingLineup
        '
        Me.dgvStartingLineup.AllowUserToAddRows = False
        Me.dgvStartingLineup.AllowUserToDeleteRows = False
        Me.dgvStartingLineup.AllowUserToResizeColumns = False
        Me.dgvStartingLineup.AllowUserToResizeRows = False
        Me.dgvStartingLineup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStartingLineup.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvStartingLineup.Location = New System.Drawing.Point(4, 105)
        Me.dgvStartingLineup.Name = "dgvStartingLineup"
        Me.dgvStartingLineup.RowHeadersVisible = False
        Me.dgvStartingLineup.Size = New System.Drawing.Size(259, 267)
        Me.dgvStartingLineup.TabIndex = 266
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 15)
        Me.Label1.TabIndex = 267
        Me.Label1.Text = "Starting line-up:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(273, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 15)
        Me.Label2.TabIndex = 269
        Me.Label2.Text = "Bench:"
        '
        'dgvBench
        '
        Me.dgvBench.AllowUserToAddRows = False
        Me.dgvBench.AllowUserToDeleteRows = False
        Me.dgvBench.AllowUserToResizeColumns = False
        Me.dgvBench.AllowUserToResizeRows = False
        Me.dgvBench.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBench.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvBench.Location = New System.Drawing.Point(265, 105)
        Me.dgvBench.Name = "dgvBench"
        Me.dgvBench.RowHeadersVisible = False
        Me.dgvBench.Size = New System.Drawing.Size(270, 179)
        Me.dgvBench.TabIndex = 268
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.btnSave.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnSave.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(840, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 270
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'cmbFormation
        '
        Me.cmbFormation.FormattingEnabled = True
        Me.cmbFormation.Location = New System.Drawing.Point(89, 383)
        Me.cmbFormation.Name = "cmbFormation"
        Me.cmbFormation.Size = New System.Drawing.Size(168, 23)
        Me.cmbFormation.TabIndex = 272
        '
        'lblFormation
        '
        Me.lblFormation.AutoSize = True
        Me.lblFormation.Location = New System.Drawing.Point(11, 386)
        Me.lblFormation.Name = "lblFormation"
        Me.lblFormation.Size = New System.Drawing.Size(58, 15)
        Me.lblFormation.TabIndex = 273
        Me.lblFormation.Text = "Formation:"
        '
        'lblInjuriesSuspensions
        '
        Me.lblInjuriesSuspensions.AutoSize = True
        Me.lblInjuriesSuspensions.Location = New System.Drawing.Point(14, 3)
        Me.lblInjuriesSuspensions.Name = "lblInjuriesSuspensions"
        Me.lblInjuriesSuspensions.Size = New System.Drawing.Size(131, 15)
        Me.lblInjuriesSuspensions.TabIndex = 276
        Me.lblInjuriesSuspensions.Text = "Injuries and suspensions:"
        '
        'dgInjuriesSuspensions
        '
        Me.dgInjuriesSuspensions.AllowUserToAddRows = False
        Me.dgInjuriesSuspensions.AllowUserToDeleteRows = False
        Me.dgInjuriesSuspensions.AllowUserToResizeColumns = False
        Me.dgInjuriesSuspensions.AllowUserToResizeRows = False
        Me.dgInjuriesSuspensions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgInjuriesSuspensions.Location = New System.Drawing.Point(16, 21)
        Me.dgInjuriesSuspensions.MultiSelect = False
        Me.dgInjuriesSuspensions.Name = "dgInjuriesSuspensions"
        Me.dgInjuriesSuspensions.ReadOnly = True
        Me.dgInjuriesSuspensions.RowHeadersVisible = False
        Me.dgInjuriesSuspensions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgInjuriesSuspensions.Size = New System.Drawing.Size(432, 163)
        Me.dgInjuriesSuspensions.TabIndex = 277
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(101, Byte), Integer))
        Me.btnDelete.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnDelete.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(373, 188)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 280
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnAddPlayer
        '
        Me.btnAddPlayer.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnAddPlayer.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnAddPlayer.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddPlayer.ForeColor = System.Drawing.Color.White
        Me.btnAddPlayer.Location = New System.Drawing.Point(14, 12)
        Me.btnAddPlayer.Name = "btnAddPlayer"
        Me.btnAddPlayer.Size = New System.Drawing.Size(79, 25)
        Me.btnAddPlayer.TabIndex = 283
        Me.btnAddPlayer.Text = "Add player..."
        Me.btnAddPlayer.UseVisualStyleBackColor = False
        '
        'btnManager
        '
        Me.btnManager.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(111, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnManager.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnManager.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManager.ForeColor = System.Drawing.Color.White
        Me.btnManager.Location = New System.Drawing.Point(99, 12)
        Me.btnManager.Name = "btnManager"
        Me.btnManager.Size = New System.Drawing.Size(95, 25)
        Me.btnManager.TabIndex = 284
        Me.btnManager.Text = "Add manager..."
        Me.btnManager.UseVisualStyleBackColor = False
        '
        'lblCoach
        '
        Me.lblCoach.AutoSize = True
        Me.lblCoach.Location = New System.Drawing.Point(276, 300)
        Me.lblCoach.Name = "lblCoach"
        Me.lblCoach.Size = New System.Drawing.Size(42, 15)
        Me.lblCoach.TabIndex = 286
        Me.lblCoach.Text = "Coach:"
        '
        'cmbCoach
        '
        Me.cmbCoach.FormattingEnabled = True
        Me.cmbCoach.Location = New System.Drawing.Point(364, 297)
        Me.cmbCoach.Name = "cmbCoach"
        Me.cmbCoach.Size = New System.Drawing.Size(157, 23)
        Me.cmbCoach.TabIndex = 285
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 415)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 15)
        Me.Label3.TabIndex = 288
        Me.Label3.Text = "Specification:"
        '
        'cmbFormationSpecification
        '
        Me.cmbFormationSpecification.FormattingEnabled = True
        Me.cmbFormationSpecification.Location = New System.Drawing.Point(89, 411)
        Me.cmbFormationSpecification.Name = "cmbFormationSpecification"
        Me.cmbFormationSpecification.Size = New System.Drawing.Size(168, 23)
        Me.cmbFormationSpecification.TabIndex = 289
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 462)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 15)
        Me.Label4.TabIndex = 290
        Me.Label4.Text = "Shirt color:"
        '
        'btnShirtColor
        '
        Me.btnShirtColor.BackColor = System.Drawing.Color.DarkGray
        Me.btnShirtColor.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnShirtColor.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShirtColor.ForeColor = System.Drawing.Color.Black
        Me.btnShirtColor.Location = New System.Drawing.Point(116, 458)
        Me.btnShirtColor.Name = "btnShirtColor"
        Me.btnShirtColor.Size = New System.Drawing.Size(30, 23)
        Me.btnShirtColor.TabIndex = 291
        Me.btnShirtColor.Text = "..."
        Me.btnShirtColor.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 488)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 292
        Me.Label5.Text = "Short color:"
        '
        'lblShirtColor
        '
        Me.lblShirtColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblShirtColor.ForeColor = System.Drawing.Color.DarkGray
        Me.lblShirtColor.Location = New System.Drawing.Point(72, 459)
        Me.lblShirtColor.Name = "lblShirtColor"
        Me.lblShirtColor.Size = New System.Drawing.Size(43, 21)
        Me.lblShirtColor.TabIndex = 294
        Me.lblShirtColor.Text = "(None)"
        Me.lblShirtColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlKit
        '
        Me.pnlKit.BackColor = System.Drawing.Color.DimGray
        Me.pnlKit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlKit.Controls.Add(Me.picKit1)
        Me.pnlKit.Controls.Add(Me.picKit8)
        Me.pnlKit.Controls.Add(Me.picKit5)
        Me.pnlKit.Controls.Add(Me.picKit6)
        Me.pnlKit.Controls.Add(Me.picKit7)
        Me.pnlKit.Controls.Add(Me.picKit3)
        Me.pnlKit.Controls.Add(Me.picKit4)
        Me.pnlKit.Controls.Add(Me.picKit2)
        Me.pnlKit.Controls.Add(Me.pnlHighlight)
        Me.pnlKit.Location = New System.Drawing.Point(177, 458)
        Me.pnlKit.Name = "pnlKit"
        Me.pnlKit.Size = New System.Drawing.Size(79, 157)
        Me.pnlKit.TabIndex = 297
        '
        'picKit1
        '
        Me.picKit1.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit1.Location = New System.Drawing.Point(2, 2)
        Me.picKit1.Name = "picKit1"
        Me.picKit1.Size = New System.Drawing.Size(36, 37)
        Me.picKit1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit1.TabIndex = 297
        Me.picKit1.TabStop = False
        '
        'picKit8
        '
        Me.picKit8.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit8.Location = New System.Drawing.Point(39, 116)
        Me.picKit8.Name = "picKit8"
        Me.picKit8.Size = New System.Drawing.Size(36, 37)
        Me.picKit8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit8.TabIndex = 304
        Me.picKit8.TabStop = False
        '
        'picKit5
        '
        Me.picKit5.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit5.Location = New System.Drawing.Point(2, 78)
        Me.picKit5.Name = "picKit5"
        Me.picKit5.Size = New System.Drawing.Size(36, 37)
        Me.picKit5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit5.TabIndex = 303
        Me.picKit5.TabStop = False
        '
        'picKit6
        '
        Me.picKit6.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit6.Location = New System.Drawing.Point(39, 78)
        Me.picKit6.Name = "picKit6"
        Me.picKit6.Size = New System.Drawing.Size(36, 37)
        Me.picKit6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit6.TabIndex = 302
        Me.picKit6.TabStop = False
        '
        'picKit7
        '
        Me.picKit7.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit7.Location = New System.Drawing.Point(2, 116)
        Me.picKit7.Name = "picKit7"
        Me.picKit7.Size = New System.Drawing.Size(36, 37)
        Me.picKit7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit7.TabIndex = 301
        Me.picKit7.TabStop = False
        '
        'picKit3
        '
        Me.picKit3.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit3.Location = New System.Drawing.Point(2, 40)
        Me.picKit3.Name = "picKit3"
        Me.picKit3.Size = New System.Drawing.Size(36, 37)
        Me.picKit3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit3.TabIndex = 300
        Me.picKit3.TabStop = False
        '
        'picKit4
        '
        Me.picKit4.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit4.Location = New System.Drawing.Point(39, 40)
        Me.picKit4.Name = "picKit4"
        Me.picKit4.Size = New System.Drawing.Size(36, 37)
        Me.picKit4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit4.TabIndex = 299
        Me.picKit4.TabStop = False
        '
        'picKit2
        '
        Me.picKit2.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.picKit2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picKit2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picKit2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picKit2.Location = New System.Drawing.Point(39, 2)
        Me.picKit2.Name = "picKit2"
        Me.picKit2.Size = New System.Drawing.Size(36, 37)
        Me.picKit2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picKit2.TabIndex = 298
        Me.picKit2.TabStop = False
        '
        'pnlHighlight
        '
        Me.pnlHighlight.BackColor = System.Drawing.Color.Yellow
        Me.pnlHighlight.Location = New System.Drawing.Point(38, 39)
        Me.pnlHighlight.Name = "pnlHighlight"
        Me.pnlHighlight.Size = New System.Drawing.Size(38, 39)
        Me.pnlHighlight.TabIndex = 308
        Me.pnlHighlight.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(174, 440)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(22, 15)
        Me.Label8.TabIndex = 298
        Me.Label8.Text = "Kit:"
        '
        'lblShortColor
        '
        Me.lblShortColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblShortColor.ForeColor = System.Drawing.Color.DarkGray
        Me.lblShortColor.Location = New System.Drawing.Point(72, 485)
        Me.lblShortColor.Name = "lblShortColor"
        Me.lblShortColor.Size = New System.Drawing.Size(43, 21)
        Me.lblShortColor.TabIndex = 300
        Me.lblShortColor.Text = "(None)"
        Me.lblShortColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnShortColor
        '
        Me.btnShortColor.BackColor = System.Drawing.Color.DarkGray
        Me.btnShortColor.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnShortColor.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShortColor.ForeColor = System.Drawing.Color.Black
        Me.btnShortColor.Location = New System.Drawing.Point(116, 484)
        Me.btnShortColor.Name = "btnShortColor"
        Me.btnShortColor.Size = New System.Drawing.Size(30, 23)
        Me.btnShortColor.TabIndex = 299
        Me.btnShortColor.Text = "..."
        Me.btnShortColor.UseVisualStyleBackColor = False
        '
        'picTeamLogo
        '
        Me.picTeamLogo.Image = Global.STATS.SoccerDataCollection.My.Resources.Resources.TeamwithNoLogo
        Me.picTeamLogo.Location = New System.Drawing.Point(13, 36)
        Me.picTeamLogo.Name = "picTeamLogo"
        Me.picTeamLogo.Size = New System.Drawing.Size(45, 45)
        Me.picTeamLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picTeamLogo.TabIndex = 265
        Me.picTeamLogo.TabStop = False
        '
        'sstMain
        '
        Me.sstMain.AutoSize = False
        Me.sstMain.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.bottombar
        Me.sstMain.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sstMain.Location = New System.Drawing.Point(0, 679)
        Me.sstMain.Name = "sstMain"
        Me.sstMain.Size = New System.Drawing.Size(1005, 16)
        Me.sstMain.TabIndex = 262
        Me.sstMain.Text = "StatusStrip1"
        '
        'picReporterBar
        '
        Me.picReporterBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picReporterBar.Location = New System.Drawing.Point(-1, 12)
        Me.picReporterBar.Name = "picReporterBar"
        Me.picReporterBar.Size = New System.Drawing.Size(1007, 21)
        Me.picReporterBar.TabIndex = 239
        Me.picReporterBar.TabStop = False
        '
        'picTopBar
        '
        Me.picTopBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.topbar
        Me.picTopBar.Location = New System.Drawing.Point(-1, 0)
        Me.picTopBar.Name = "picTopBar"
        Me.picTopBar.Size = New System.Drawing.Size(1007, 15)
        Me.picTopBar.TabIndex = 238
        Me.picTopBar.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Linen
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.lblInjuriesSuspensions)
        Me.Panel4.Controls.Add(Me.dgInjuriesSuspensions)
        Me.Panel4.Controls.Add(Me.btnDelete)
        Me.Panel4.Location = New System.Drawing.Point(537, 105)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(465, 219)
        Me.Panel4.TabIndex = 304
        '
        'btnClearShirtColor
        '
        Me.btnClearShirtColor.BackColor = System.Drawing.Color.DarkGray
        Me.btnClearShirtColor.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClearShirtColor.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearShirtColor.ForeColor = System.Drawing.Color.Crimson
        Me.btnClearShirtColor.Location = New System.Drawing.Point(145, 458)
        Me.btnClearShirtColor.Name = "btnClearShirtColor"
        Me.btnClearShirtColor.Size = New System.Drawing.Size(24, 23)
        Me.btnClearShirtColor.TabIndex = 306
        Me.btnClearShirtColor.Text = "X"
        Me.btnClearShirtColor.UseVisualStyleBackColor = False
        '
        'btnClearShortColor
        '
        Me.btnClearShortColor.BackColor = System.Drawing.Color.DarkGray
        Me.btnClearShortColor.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClearShortColor.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearShortColor.ForeColor = System.Drawing.Color.Crimson
        Me.btnClearShortColor.Location = New System.Drawing.Point(145, 484)
        Me.btnClearShortColor.Name = "btnClearShortColor"
        Me.btnClearShortColor.Size = New System.Drawing.Size(24, 23)
        Me.btnClearShortColor.TabIndex = 307
        Me.btnClearShortColor.Text = "X"
        Me.btnClearShortColor.UseVisualStyleBackColor = False
        '
        'lblStartLineSent
        '
        Me.lblStartLineSent.AutoSize = True
        Me.lblStartLineSent.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.lblStartLineSent.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblStartLineSent.Location = New System.Drawing.Point(331, 646)
        Me.lblStartLineSent.Name = "lblStartLineSent"
        Me.lblStartLineSent.Size = New System.Drawing.Size(24, 15)
        Me.lblStartLineSent.TabIndex = 309
        Me.lblStartLineSent.Text = "NO"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label15.ForeColor = System.Drawing.Color.LightGray
        Me.Label15.Location = New System.Drawing.Point(214, 646)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(118, 15)
        Me.Label15.TabIndex = 308
        Me.Label15.Text = "Line-up sent to Clients:"
        '
        'picButtonBar
        '
        Me.picButtonBar.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.lines
        Me.picButtonBar.Controls.Add(Me.btnAddPlayer)
        Me.picButtonBar.Controls.Add(Me.btnManager)
        Me.picButtonBar.Controls.Add(Me.btnCancel)
        Me.picButtonBar.Controls.Add(Me.btnSave)
        Me.picButtonBar.Location = New System.Drawing.Point(-1, 631)
        Me.picButtonBar.Name = "picButtonBar"
        Me.picButtonBar.Size = New System.Drawing.Size(1006, 60)
        Me.picButtonBar.TabIndex = 310
        '
        'btnShirtNoColor
        '
        Me.btnShirtNoColor.BackColor = System.Drawing.Color.DarkGray
        Me.btnShirtNoColor.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnShirtNoColor.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShirtNoColor.ForeColor = System.Drawing.Color.Black
        Me.btnShirtNoColor.Location = New System.Drawing.Point(116, 531)
        Me.btnShirtNoColor.Name = "btnShirtNoColor"
        Me.btnShirtNoColor.Size = New System.Drawing.Size(30, 23)
        Me.btnShirtNoColor.TabIndex = 312
        Me.btnShirtNoColor.Text = "..."
        Me.btnShirtNoColor.UseVisualStyleBackColor = False
        '
        'btnClearShirtNoColor
        '
        Me.btnClearShirtNoColor.BackColor = System.Drawing.Color.DarkGray
        Me.btnClearShirtNoColor.BackgroundImage = Global.STATS.SoccerDataCollection.My.Resources.Resources.Glass_effect
        Me.btnClearShirtNoColor.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearShirtNoColor.ForeColor = System.Drawing.Color.Crimson
        Me.btnClearShirtNoColor.Location = New System.Drawing.Point(145, 531)
        Me.btnClearShirtNoColor.Name = "btnClearShirtNoColor"
        Me.btnClearShirtNoColor.Size = New System.Drawing.Size(24, 23)
        Me.btnClearShirtNoColor.TabIndex = 314
        Me.btnClearShirtNoColor.Text = "X"
        Me.btnClearShirtNoColor.UseVisualStyleBackColor = False
        '
        'lblShirtNoColor
        '
        Me.lblShirtNoColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblShirtNoColor.ForeColor = System.Drawing.Color.DarkGray
        Me.lblShirtNoColor.Location = New System.Drawing.Point(72, 532)
        Me.lblShirtNoColor.Name = "lblShirtNoColor"
        Me.lblShirtNoColor.Size = New System.Drawing.Size(43, 21)
        Me.lblShirtNoColor.TabIndex = 313
        Me.lblShirtNoColor.Text = "(None)"
        Me.lblShirtNoColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 514)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 15)
        Me.Label7.TabIndex = 311
        Me.Label7.Text = "Shirt Number color:"
        '
        'UdcSoccerFormation1
        '
        Me.UdcSoccerFormation1.BackColor = System.Drawing.Color.Wheat
        Me.UdcSoccerFormation1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UdcSoccerFormation1.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UdcSoccerFormation1.Location = New System.Drawing.Point(278, 335)
        Me.UdcSoccerFormation1.Name = "UdcSoccerFormation1"
        Me.UdcSoccerFormation1.Size = New System.Drawing.Size(723, 280)
        Me.UdcSoccerFormation1.TabIndex = 305
        Me.UdcSoccerFormation1.USFFormation = Nothing
        Me.UdcSoccerFormation1.USFFormationSpecification = Nothing
        Me.UdcSoccerFormation1.USFStartingLineup = Nothing
        '
        'frmTeamSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1005, 695)
        Me.Controls.Add(Me.btnShirtNoColor)
        Me.Controls.Add(Me.btnClearShirtNoColor)
        Me.Controls.Add(Me.lblShirtNoColor)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblStartLineSent)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.btnShortColor)
        Me.Controls.Add(Me.btnShirtColor)
        Me.Controls.Add(Me.btnClearShortColor)
        Me.Controls.Add(Me.lblShirtColor)
        Me.Controls.Add(Me.btnClearShirtColor)
        Me.Controls.Add(Me.UdcSoccerFormation1)
        Me.Controls.Add(Me.lblShortColor)
        Me.Controls.Add(Me.pnlKit)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbFormationSpecification)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblCoach)
        Me.Controls.Add(Me.cmbCoach)
        Me.Controls.Add(Me.lblFormation)
        Me.Controls.Add(Me.cmbFormation)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvBench)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvStartingLineup)
        Me.Controls.Add(Me.picTeamLogo)
        Me.Controls.Add(Me.pnlHeader)
        Me.Controls.Add(Me.sstMain)
        Me.Controls.Add(Me.picReporterBar)
        Me.Controls.Add(Me.picTopBar)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.picButtonBar)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTeamSetup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Soccer Data Collection - Team Setup"
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlHeader.PerformLayout()
        CType(Me.dgvStartingLineup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBench, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgInjuriesSuspensions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlKit.ResumeLayout(False)
        Me.pnlKit.PerformLayout()
        CType(Me.picKit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picKit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTeamLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picReporterBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.picButtonBar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picReporterBar As System.Windows.Forms.PictureBox
    Friend WithEvents picTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents sstMain As System.Windows.Forms.StatusStrip
    Friend WithEvents picTeamLogo As System.Windows.Forms.PictureBox
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents radAwayTeam As System.Windows.Forms.RadioButton
    Friend WithEvents radHomeTeam As System.Windows.Forms.RadioButton
    Friend WithEvents dgvStartingLineup As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvBench As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cmbFormation As System.Windows.Forms.ComboBox
    Friend WithEvents lblFormation As System.Windows.Forms.Label
    Friend WithEvents lblInjuriesSuspensions As System.Windows.Forms.Label
    Friend WithEvents dgInjuriesSuspensions As System.Windows.Forms.DataGridView
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnAddPlayer As System.Windows.Forms.Button
    Friend WithEvents btnManager As System.Windows.Forms.Button
    Friend WithEvents lblCoach As System.Windows.Forms.Label
    Friend WithEvents cmbCoach As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbFormationSpecification As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnShirtColor As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblShirtColor As System.Windows.Forms.Label
    Friend WithEvents pnlKit As System.Windows.Forms.Panel
    Friend WithEvents picKit7 As System.Windows.Forms.PictureBox
    Friend WithEvents picKit4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblShortColor As System.Windows.Forms.Label
    Friend WithEvents btnShortColor As System.Windows.Forms.Button
    Friend WithEvents picKit8 As System.Windows.Forms.PictureBox
    Friend WithEvents picKit5 As System.Windows.Forms.PictureBox
    Friend WithEvents picKit6 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents UdcSoccerFormation1 As STATS.SoccerDataCollection.udcSoccerFormation
    Friend WithEvents cdlgShirtShort As System.Windows.Forms.ColorDialog
    Friend WithEvents btnClearShirtColor As System.Windows.Forms.Button
    Friend WithEvents btnClearShortColor As System.Windows.Forms.Button
    Friend WithEvents picKit3 As System.Windows.Forms.PictureBox
    Friend WithEvents picKit2 As System.Windows.Forms.PictureBox
    Friend WithEvents picKit1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlHighlight As System.Windows.Forms.Panel
    Friend WithEvents lblStartLineSent As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents picButtonBar As System.Windows.Forms.Panel
    Friend WithEvents btnShirtNoColor As System.Windows.Forms.Button
    Friend WithEvents btnClearShirtNoColor As System.Windows.Forms.Button
    Friend WithEvents lblShirtNoColor As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
