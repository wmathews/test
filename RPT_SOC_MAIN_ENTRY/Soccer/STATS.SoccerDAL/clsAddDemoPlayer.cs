﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsDemoAddPlayer
// Author        : Shravani
// Created Date  : 24th Aug,2009
// Description   : 
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion


namespace STATS.SoccerDAL
{
    public class clsAddDemoPlayer
    {
        #region "Member Variables"

        private static clsAddDemoPlayer m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        public static clsAddDemoPlayer GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsAddDemoPlayer();
            }
            return m_myInstance;
        }


        public DataSet GetLeague()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetLeague", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    

        public int InsertAddPlayer(int PERSON_ID, string MONIKER, string LAST_NAME, int LEAGUE_ID, int TEAM_ID, string UNIFORM, string DISPLAY_UNIFORM, int POSITION_ID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@PERSON_ID", PERSON_ID), new SqlParameter("@MONIKER", MONIKER), new SqlParameter("@LAST_NAME", LAST_NAME), new SqlParameter("@LEAGUE_ID", LEAGUE_ID), new SqlParameter("@TEAM_ID", TEAM_ID), new SqlParameter("@UNIFORM", UNIFORM), new SqlParameter("@DISPLAY_UNIFORM", DISPLAY_UNIFORM), new SqlParameter("@POSITION_ID", POSITION_ID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_InsertAddPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

        public int UpdateAddPlayer(int TeamID, int LeagueID, string Moniker, string LastName, int Person_ID, string Disply_Uniform_number, int Position_ID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Team_ID", TeamID), new SqlParameter("@League_ID", LeagueID), new SqlParameter("@Moniker", Moniker), new SqlParameter("@LastName", LastName), new SqlParameter("@Person_ID", Person_ID), new SqlParameter("@Disply_Uniform_number", Disply_Uniform_number), new SqlParameter("@Position_ID", Position_ID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_UpdateAddPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet SelectAddPlayer()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_SelectAddPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetTeamAndPosition(int LeagueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LEAGUEID", LeagueID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetTeamAndPosition", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteAddPlayer(int PlayerID,  int TeamID,int LeagueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@PlayerID", PlayerID),  new SqlParameter("@TeamID", TeamID),new SqlParameter("@LeagueID", LeagueID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_DeleteAddPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
