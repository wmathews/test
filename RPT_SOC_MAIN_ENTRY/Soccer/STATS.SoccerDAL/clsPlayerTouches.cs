﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsPlayerTouches
    {
        #region "Member Variables"

        private static clsPlayerTouches m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsPlayerTouches GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsPlayerTouches();
            }
            return m_myInstance;
        }

        #endregion
        /// <summary>
        /// GETS GAME STATISTICS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetPlayerTouches(Int64 GameCode, int FeedNumber, int language_id, int serial_no, int hTeam, int aTeam, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@language_id", language_id), new SqlParameter("@serial_no", serial_no), new SqlParameter("@touch_HteamID", hTeam), new SqlParameter("@touch_AteamID", aTeam), new SqlParameter("@period", Period) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTouches", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS GAME STATISTICS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTouchTypes()
        {
            try
            {
                //SqlParameter[] ParamValues // = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                //SqlParameter[] ParamValues;

                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SELECT_TOUCHTYPES"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// GETS GAME STATISTICS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetRosters(Int64 GameCode, int FeedNumber, Int64 teamID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", teamID) };
                //SqlParameter[] ParamValues;

                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTeamRosters_By_Lineups", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public DataSet InsertPlayerTouchestoSummary(Int64 GameCode, int FeedNumber,int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@MODULEID", ModuleID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_UPDATE_PBP_TO_STATS_PROC_SOC", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
