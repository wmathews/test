﻿
#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsTeamSetup
// Author        : Shirley Ranjini
// Created Date  : 11th May,2009
// Description   : Data Access Layer for Team Setup screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace STATS.SoccerDAL
{
    public class clsTeamSetup
    {
        #region " Constants & Variables "
            private static clsTeamSetup Instance;
            private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
        #endregion

        #region " User Methods "

            /// <summary>
            /// SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
            /// </summary>
            /// <returns></returns>
            public static clsTeamSetup GetAccess()
            {
                //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
                if (Instance == null)
                {
                    Instance = new clsTeamSetup();
                }
                return Instance;
            }

             /// <summary>
             /// FETCHES HOME AND AWAY TEAM ROSTERS FOR THE SPECIFIED GAMECODE AND LEAGUEID
             /// </summary>
             /// <param name="Gamecode"></param>
             /// <param name="LeagueID"></param>
             /// <returns>A DATASET CONTAINING 3 TABLES TABLE1 : HOME TEAM ROSTERS TABLE2 : AWAY TEAM ROSTERS TABLE 3 : TEAM NAMES WITH THEIR CORRESPONDING ID'S</returns>

            public DataSet GetRosterInfo(int Gamecode, int LeagueID, string Type, int LanguageID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@Type", Type), new SqlParameter("@Language", LanguageID) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetRosterInfo", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                /// <param name="Gamecode"></param>
                /// <param name="FeedNumber"></param>
                /// <param name="TeamID"></param>
                /// <returns></returns>
                public DataSet GetFormationCoach(int Gamecode, int FeedNumber,int TeamID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@TeamID", TeamID) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetFormationCoach", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                       
                /// <summary>
                /// UPDATES DISPLAY UN, LINEUP DATA FOR THE SELECTED PLAYERS IN STARTING LINEUPS
                /// </summary>
                /// <param name="LineupData"></param> PARAMETER WHICH CONTAINS THE XML DATA
                /// <returns></returns>
                public int UpdateRosterInfo(int GameCode,string LineupData)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@LineupData", LineupData) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Insert_LINEUP_Data_XML", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                /// <summary>
                /// GETS FORMATION AND COAHC DATA
               /// </summary>
               /// <param name="TeamID">An integer represents TeamID</param>
                /// <returns>A DATASET CONTAINING 2 TABLES TABLE1 : FORMATION TABLE2 :COACH </returns>
                public DataSet GetFormation()
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] {};
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GETFORMATION", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public DataSet GetCoach(int TeamID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", TeamID) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GETCOACH", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                public int InsertUpdateTeamGame(string TeamGameData)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FormationCoach", TeamGameData) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Insert_FormationAndCoach_Data_XML", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>
                public int GetLineUpEventCnt(Int64 GameCode, int FeedNumber,int ModuleId)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ModuleId", ModuleId) };
                        return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GET_LINEUP",ParamValues));
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                /// <summary>
                /// GET PLAYERS WHO INJURED AND SUSPENDED BASED ON TEAMID
                /// </summary>
                /// <param name="TeamID"></param>
                /// <returns></returns>
                public DataSet GetInjuredSuspendedPlayers(int TeamID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", TeamID) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_SelectInjuriesSuspensions", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// Deletes Injured or Suspended Player and Updates in Roster table as a bench player
                /// </summary>
                /// <param name="LeaguID"></param>
                /// <param name="TeamID"></param>
                /// <param name="PlayerID"></param>
                /// <returns></returns>
                public int DeleteInjuredOrSuspended(int LeaguID,int TeamID,int PlayerID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LeagueID", LeaguID), new SqlParameter("@TeamID", TeamID), new SqlParameter("@PlayerID", PlayerID) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_DeleteInjuredOrSuspendedPlayer", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// GET FORMATIONSPECIFICATION
                /// </summary>
                /// <param name="FormationID"></param>
                /// <returns></returns>
                public DataSet GetFormationSpecification(int FormationID, int language_id)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FormationID", FormationID),new SqlParameter("@language_id",language_id) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_SelectFormationSpecification", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public DataSet GetUniformPictures(int TeamID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", TeamID) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GETUNIFORMPICTURES", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public DataSet GetUniformColors(int GameCode, int FeedNumber)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETUNIFORMCOLOR", ParamValues);
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                public int GetPlayerCount(Int64 GameCode, int FeedNumber,int PlayerID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@FEEDNUMBER", FeedNumber), new SqlParameter("@PLAYERID", PlayerID) };
                        return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_MOD1_CrossCheckPlayerIDInPBP", ParamValues));

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                public int UpdateExisPBPPlayerWithNewPlayer(int GameCode, int FeedNumber, String ReporterRole, int ExisPlayerID, int NewPlayerID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@FEEDNUMBER", FeedNumber), new SqlParameter("@REPORTERROLE", ReporterRole), new SqlParameter("@EXISPLAYERID", ExisPlayerID), new SqlParameter("@NEWPLAYERID", NewPlayerID) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_UpdateExisPBPPlayerWithNewPlayer", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                public DataSet GetShirtandShortColors(int KitNo, int TeamID)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@KITNO", KitNo), new SqlParameter("@TEAMID", TeamID) };
                        return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETSHIRTANDSHORTCOLORS", ParamValues);
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public int GetEndGame(int GameCode, int FeedNumber)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] {  new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@FEEDNUMBER", FeedNumber)};
                        return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_GetEndGame", ParamValues));
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public int GetPeriodEndCount(int GameCode, int FeedNumber, int Period)
                {
                    try
                    {
                        int Count;
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period) };
                        return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPeriodEndCount", ParamValues));

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public int InsertPBPOnfield(int GameCode, int FeedNumber, string ReporterRole, string PlayerData, String FormationData)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@PLAYERDATA", PlayerData), new SqlParameter("@FORMATIONDATA", FormationData) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_INSERT_ONFIELD", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                public int UpdateLineupEvent(int GameCode, int FeedNumber, String ReporterRole,string EditedTime )
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@FEEDNUMBER", FeedNumber), new SqlParameter("@REPORTERROLE", ReporterRole), new SqlParameter("@EDITEDTIME", EditedTime) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_UPDATE_LINEUPS", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                public int UpdateLineupsAndOverwriteExisPlayerData(int GameCode, string LineupData, String ReporterRole)
                {
                    try
                    {
                        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@LineupData", LineupData), new SqlParameter("@Reporter_Role", ReporterRole) };
                        return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_PZ_Insert_Lineup_Data", ParamValues);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

        #endregion
    }
}
