﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsHomeStart
    {
        #region "Member Variables"

        private static clsHomeStart m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsHomeStart GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsHomeStart();
            }
            return m_myInstance;
        }

        #endregion
        #region "User Functions"
        /// <summary>
        /// Inserts or Updates the Gamesetup info
        /// </summary>
        /// <param name="XMLGameSetupData"></param>
        /// <returns></returns>
        public int InsertUpdateHomeStart(int GameCode, int FeedNumber, int LeagueID , string HomeStart, string HomeStartET)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@HomeStart", HomeStart), new SqlParameter("@HomeStartET", HomeStartET) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_InsertAndUpdateHomeStart", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion
    }


}
