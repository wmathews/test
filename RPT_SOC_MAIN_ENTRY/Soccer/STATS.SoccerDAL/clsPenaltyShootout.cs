﻿# region "Using"

using STATS.DataAccess;
using System;
using System.Data;
using System.Data.SqlClient;

#endregion

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsGeneral
// Author        : Dijo Davis
// Created Date  : 07-05-09
// Description   : This is the data access for general functions
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsPenaltyShootout
    {
        #region "Constants & Variables"
        private static clsPenaltyShootout Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
        #endregion "Constants & Variables"

        #region "Shared methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY
        public static clsPenaltyShootout GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT
            if (Instance == null)
            {
                Instance = new clsPenaltyShootout();
            }
            return Instance;
        }

        #endregion "Shared methods"

        public DataSet FetchShootoutInputs(int GameCode, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@LANGUAGE_ID", LanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_ShootoutInputs", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet FetchPzShootoutInputs(int GameCode, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@LANGUAGE_ID", LanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_ShootoutInputs", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetShootoutPBPData(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_ShootoutPBPData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetShootoutPzpbpData(int gameCode, int feedNumber, int languageId)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", gameCode), new SqlParameter("@FEED_NUMBER", feedNumber), new SqlParameter("@LANGUAGE_ID", languageId) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_ShootoutPBPData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPRecordBasedOnSeqNumber(int GameCode, int FeedNumber, decimal Sequence_Number, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchPBPRecordBasedOnSeqNo", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="XMLGameSetupData"></param>
        /// <returns></returns>
        public DataSet InsertPBPDataToSQL(string PBPXMLData, int GameCode, int FeedNumber, string ReporterRole, int LangID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@language_id", LangID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);

                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited) };
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int EditScoreEvent(int GameCode, int FeedNumber, Decimal SequenceNumber, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode), new SqlParameter("@Feed_Number", FeedNumber), new SqlParameter("@Sequence_Number", SequenceNumber), new SqlParameter("@Type", Type) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_EDIT_SCORE_PBP", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatePBPData(string PBPXMLData, int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, string Type, int language_id)
        {
            try
            {
                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Update_PBP_Data", ParamValues);
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@Type", Type), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PairedPBPEvents", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteScoreEvent(int GameCode, int FeedNumber, Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode), new SqlParameter("@Feed_Number", FeedNumber), new SqlParameter("@Sequence_Number", SequenceNumber) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Delete_Score_Event", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeletePBPData(int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ReporterRole", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_DeletePBPData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPData(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateTeamGamePenaltyShootoutScores(int GameCode, int FeedNumber, string ReporterRole)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_UpdateTeamGameWithPSScores", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}