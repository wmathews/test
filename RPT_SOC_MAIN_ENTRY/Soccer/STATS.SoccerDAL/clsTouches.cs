﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsTouches
// Author        : Dijo Davis
// Created Date  : 03-06-09
// Description   : This is the data access logic for Touches module.
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsTouches
    {
        private static clsTouches Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsTouches GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsTouches();
            }
            return Instance;
        }

        #region "Touches"

      

       /// <summary>
        /// To save touches data to local SQL
       /// </summary>
       /// <param name="intGameCode"></param>
       /// <param name="intFeedNumber"></param>
       /// <param name="intTimeElapsed"></param>
       /// <param name="intPeriod"></param>
       /// <param name="intPlayerID"></param>
       /// <param name="intTouchTypeID"></param>
       /// <param name="strReporterRole"></param>
       /// <param name="intXFieldZone"></param>
       /// <param name="intYFieldZone"></param>
        public void AddTouches(int intGameCode, int intFeedNumber, int intTimeElapsed, int intPeriod, int intPlayerID, int intTouchTypeID, string strReporterRole, int intXFieldZone, int intYFieldZone)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[9];

                ParamValues[0]=new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@TimeElapsed";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intTimeElapsed;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@Period";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = intPeriod;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@PlayerID";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                if (intPlayerID == 0)
                {
                    ParamValues[4].Value = DBNull.Value;
                }
                else
                {
                    ParamValues[4].Value = intPlayerID;
                }


                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@TouchTypeID";
                ParamValues[5].SqlDbType = SqlDbType.Int;
                ParamValues[5].Direction = ParameterDirection.Input;

                if (intTouchTypeID == -1)
                {
                    ParamValues[5].Value = DBNull.Value;
                }
                else
                {
                    ParamValues[5].Value = intTouchTypeID;
                }

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@ReporterRole";
                ParamValues[6].SqlDbType = SqlDbType.VarChar;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = strReporterRole;

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@XFieldZone";
                ParamValues[7].SqlDbType = SqlDbType.Int;
                ParamValues[7].Direction = ParameterDirection.Input;
                if (intXFieldZone != -1 && intYFieldZone != -1)
                {
                    ParamValues[7].Value = intXFieldZone;
                }
                else
                {
                    ParamValues[7].Value = DBNull.Value;                
                }

                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@YFieldZone";
                ParamValues[8].SqlDbType = SqlDbType.Int;
                ParamValues[8].Direction = ParameterDirection.Input;
                if (intXFieldZone != -1 && intYFieldZone != -1)
                {
                    ParamValues[8].Value = intYFieldZone;
                }
                else
                {
                    ParamValues[8].Value = DBNull.Value;
                }

                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_AddTouches", ParamValues);


            }
            catch (Exception ex)
            {
                throw ex;            
            }
        
        }

        public void AddTouches(DataSet TouchesData)
        {
            try
            {
                string strTouchesData;
                strTouchesData = TouchesData.GetXml();

                SqlParameter[] ParamValues = new SqlParameter[1];
                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@TouchesData";
                ParamValues[0].SqlDbType = SqlDbType.Xml;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = strTouchesData;

                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_AddTouchEvent", ParamValues);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// To get touch data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTouches(int intGameCode, int intFeedNumber, int intlanguage_id, int intSerialNo, int touchHTeamID, int touchATeamID, int period)
        {
            try
            {
                DataSet dsTouches = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[7];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@language_id";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intlanguage_id;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@serial_no";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = intSerialNo;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@touch_HteamID";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = touchHTeamID;

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@touch_AteamID";
                ParamValues[5].SqlDbType = SqlDbType.Int;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = touchATeamID;

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@period";
                ParamValues[6].SqlDbType = SqlDbType.Int;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = period;

                //ParamValues[7] = new SqlParameter();
                //ParamValues[7].ParameterName = "@filter";
                //ParamValues[7].SqlDbType = SqlDbType.Int;
                //ParamValues[7].Direction = ParameterDirection.Input;
                //ParamValues[7].Value = filter;

                dsTouches= SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTouches", ParamValues);
                if(dsTouches!=null)
                {
                    if (dsTouches.Tables.Count > 0)
                    {
                        dsTouches.DataSetName = "TouchesData";
                        dsTouches.Tables[0].TableName = "Touches"; 
                    }
                }

                return dsTouches;
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }


        public DataSet GetTouchesAll(int intGameCode, int intFeedNumber, int intlanguage_id, int intSerialNo)
        {
            try
            {
                DataSet dsTouches = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[4];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@language_id";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intlanguage_id;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@serial_no";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = intSerialNo;

                dsTouches= SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTouchesAll", ParamValues);
                if(dsTouches!=null)
                {
                    if (dsTouches.Tables.Count > 0)
                    {
                        dsTouches.DataSetName = "TouchesData";
                        dsTouches.Tables[0].TableName = "Touches"; 
                    }
                }

                return dsTouches;
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }

        public DataSet RefreshTouchesData(int intGameCode, int intFeedNumber, int intlanguage_id, Decimal Sequence_Number, int SerialNo)
        {
            try
            {
                DataSet dsTouches = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[5];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@language_id";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intlanguage_id;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@Sequence_Number";
                ParamValues[3].SqlDbType = SqlDbType.Decimal;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = Sequence_Number;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@SerialNo";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = SerialNo;

                dsTouches = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_RefreshTouchesData", ParamValues);
                if (dsTouches != null)
                {
                    if (dsTouches.Tables.Count > 0)
                    {
                        dsTouches.DataSetName = "TouchesData";
                        dsTouches.Tables[0].TableName = "Touches";
                    }
                }

                return dsTouches;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int GetMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD3_GET_MAX_TIMEELAPSED_TOUCHES", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetLastEventDetails(int GameCode, int FeedNumber)
        {
            try
            {
                DataSet dsTouches = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[2];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = GameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = FeedNumber;

                dsTouches = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD3_GET_MAX_EVENTDETAILS_TOUCHES", ParamValues);
                if (dsTouches != null)
                {
                    if (dsTouches.Tables.Count > 0)
                    {
                        dsTouches.DataSetName = "TouchesData";
                        dsTouches.Tables[0].TableName = "Touches";
                    }
                }

                return dsTouches;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTouchesRefresh(int intGameCode, int intFeedNumber, int intlanguage_id, int SerialNo)
        {
            try
            {
                DataSet dsTouches = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[4];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@language_id";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intlanguage_id;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@SerialNo";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = SerialNo;

                dsTouches = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTouchesRefresh", ParamValues);
                if(dsTouches!=null)
                {
                    if (dsTouches.Tables.Count > 0)
                    {
                        dsTouches.DataSetName = "TouchesData";
                        dsTouches.Tables[0].TableName = "Touches"; 
                    }
                }

                return dsTouches;
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }



        public DataSet GetPBPRecordBasedOnSeqNumber(int GameCode, int FeedNumber, decimal Sequence_Number, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchTouchRecordBasedOnSeqNo", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To update touches data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <param name="intTimeElapsed"></param>
        /// <param name="intPlayerID"></param>
        /// <param name="intTouchTypeID"></param>
        /// <param name="intXFieldZone"></param>
        /// <param name="intYFieldZone"></param>
        /// <param name="decSequenceNumber"></param>

        public int UpdateTouches(DataSet TouchData,decimal SequenceNumber,int GameCode, int FeedNumber)
        {
            try
            {
                string strTouchData;
                strTouchData = TouchData.GetXml();

                SqlParameter[] ParamValues = new SqlParameter[4];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@TouchesData";
                ParamValues[0].SqlDbType = SqlDbType.Xml;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = strTouchData;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@SequenceNumber";
                ParamValues[1].SqlDbType = SqlDbType.Decimal;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = SequenceNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@GameCode";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = GameCode;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@FeedNumber";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = FeedNumber;

                //SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_UpdateTouches", ParamValues);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_UpdateTouches", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }   
        
                    
        }


        public DataSet GetPBPDataForTouches(int intGameCode,int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetPBPDataforCommentary", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To delete touch data
        /// </summary>
        /// <param name="decSequenceNumber"></param>
        public int DeleteTouches(int GameCode, int FeedNumber, decimal decSequenceNumber,String Reporter_Role)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", decSequenceNumber), new SqlParameter("@Reporter_Role", Reporter_Role) };
                //SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_DeleteTouch", ParamValues);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_DeleteTouch", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
              
        public DataSet GetTouchRecordBasedOnSeqNumber(int GameCode, int FeedNumber, decimal Sequence_Number, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchTouchRecordBasedOnSeqNo", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      
        public DataSet GetGoalieChangeEvent(int GameCode, int FeedNumber, int TeamID)
        {
            try
            {

                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@TEAM_ID", TeamID) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetGoalieChangeEvent", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetPlayerCountInTouches(int GameCode, int FeedNumber, int PlayerID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@PLAYER_ID", PlayerID) };
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_CheckPlayerIdIfExitsInTouches", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateExisTouchesPlayerWithNewPlayer(int GameCode, int FeedNumber, String ReporterRole, int ExisPlayerID, int NewPlayerID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@EXIS_PLAYERID", ExisPlayerID), new SqlParameter("@NEW_PLAYERID", NewPlayerID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_UpdateTouchesPlayerWithNewPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion "Touches"
    }
}
