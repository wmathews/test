﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsLogin
// Author        : Shravani
// Created Date  : 28th April,2009
// Description   : 
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace STATS.SoccerDAL
{
    public class clsLogin
    {
        #region " Constants & Variables "
        private static clsLogin Instance;
        private SqlConnection m_connection;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
        #endregion

        #region " User Methods "

        /// <summary>
        /// SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        /// </summary>
        /// <returns></returns>
        public static clsLogin GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsLogin();
            }
            return Instance;
        }

        /// <summary>
        /// GETS THE SQL CONNECTION
        /// </summary>
        /// <returns></returns>
        public SqlConnection Connection()
        {

            if (m_connection == null)
            {
                m_connection = new SqlConnection(ConfigurationManager.AppSettings["strSqlCon"]);
            }
            return m_connection;
        }


        public SqlConnection ConnectionDrop()
        {

            if (m_connection == null)
            {
                m_connection = new SqlConnection(ConfigurationManager.AppSettings["strSqlDropTab"]);
            }
            return m_connection;
        }

        public int ExecuteNonQuery(String Query)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(ConnectionDrop(), CommandType.Text, Query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// DELETES THE DATA FROM SQL LANGUAGE TABLE
        /// </summary>
        /// <returns>An Integer</returns>
        public int DeleteLanguageTableData()
        {
            try
            {
                string DelQuery = "Delete  from glb_language";
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.Text, DelQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// INSERTS THE REPORTER DATA INTO LOCAL SQL TABLES 
        /// </summary>
        /// <param name="SoccerDataXML">A String represernts the XMLdata </param>
        /// <param name="LanguageID">An Integer represents the LanguageID</param>
        /// <returns>An Integer</returns>
        public int InsertDownLoaddata(string SoccerDataXML, int LanguageID, int ReporterSerialNo)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerDataXML", SoccerDataXML), new SqlParameter("@Language", LanguageID), new SqlParameter("@ReporterSerialNo", ReporterSerialNo) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Download_All_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int InsertDownMultilingualdata(string SoccerDataXML, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerDataXML", SoccerDataXML), new SqlParameter("@Language", LanguageID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Download_All_multilingual_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///  DELETES ALL TABLE DATA BEFORE INSERT
        /// </summary>
        public void DeleteAllTableData()
        {
            try
            {
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_Delete_Reporter_Data");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS THE GAME DETAILS ASSIGNED TO THE REPORTER
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// 
        /// <returns>A DataSet contains game details</returns>

        public DataSet GetGameDetails(int LanguageID, int UserType)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@UserType", UserType) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Fills the Language Table
        /// </summary>
        /// <param name="Language"></param>
        public void FillLanguageTable(DataSet Language)
        {
            try
            {
                DataSet dsSqlLanguage = new DataSet();
                SqlCommandBuilder cmdSqlBuilder;
                SqlDataAdapter daLanguage;
                if (Language.Tables[0].Rows.Count > 0)
                {

                    daLanguage = new SqlDataAdapter("SELECT LANGUAGE_ID, LANGUAGE_DESC, LOGIN_LANG, COMMENTS_LANG FROM GLB_LANGUAGE", m_objDataAccess.GetSQLConnection());
                    daLanguage.Fill(dsSqlLanguage);

                    if (dsSqlLanguage.Tables[0].Rows.Count == 0)
                    {
                        //dsSqlLanguage.Tables.Add();
                        //dsSqlLanguage.Tables[0].Columns.Add("LANGUAGE_ID");
                        //dsSqlLanguage.Tables[0].Columns.Add("LANGUAGE_DESC");

                        foreach (DataRow drLanguage in Language.Tables[0].Rows)
                        {
                            dsSqlLanguage.Tables[0].ImportRow(drLanguage);
                        }
                        dsSqlLanguage.AcceptChanges();
                        foreach (DataRow drLanguage in dsSqlLanguage.Tables[0].Rows)
                        {
                            drLanguage.SetAdded();
                        }
                    }
                    cmdSqlBuilder = new SqlCommandBuilder(daLanguage);
                    daLanguage.InsertCommand = cmdSqlBuilder.GetInsertCommand(false);
                    daLanguage.Update(dsSqlLanguage.Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Fills the TeamLogo Table
        /// </summary>
        /// <param name="TeamLogo"></param>
        public void FillTeamLogo(DataSet TeamLogo)
        {
            try
            {
                DataSet dsSqlTeamLogo = new DataSet();
                SqlCommandBuilder cmdSqlBuilder;
                SqlDataAdapter daTeamLogo;
                if (TeamLogo.Tables[0].Rows.Count > 0)
                {
                    daTeamLogo = new SqlDataAdapter("SELECT * FROM GLB_TEAM_LOGO WHERE 1=2", m_objDataAccess.GetSQLConnection());
                    daTeamLogo.Fill(dsSqlTeamLogo);

                    foreach (DataRow drTeamLogo in TeamLogo.Tables[0].Rows)
                    {
                        dsSqlTeamLogo.Tables[0].ImportRow(drTeamLogo);
                    }
                    dsSqlTeamLogo.AcceptChanges();
                    foreach (DataRow drTeamLogo in dsSqlTeamLogo.Tables[0].Rows)
                    {
                        drTeamLogo.SetAdded();
                    }
                    cmdSqlBuilder = new SqlCommandBuilder(daTeamLogo);
                    daTeamLogo.InsertCommand = cmdSqlBuilder.GetInsertCommand(false);
                    daTeamLogo.Update(dsSqlTeamLogo.Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Fills the Uniform Images Table
        /// </summary>
        /// <param name="TeamLogo"></param>
        public void FillUniformImages(DataSet UniformImages)
        {
            try
            {
                DataSet dsUniformImages = new DataSet();
                SqlCommandBuilder cmdSqlBuilder;
                SqlDataAdapter daUniformImages;
                if (UniformImages.Tables[0].Rows.Count > 0)
                {
                    daUniformImages = new SqlDataAdapter("SELECT * FROM SOCCER_UNIFORM_IMAGES WHERE 1=2", m_objDataAccess.GetSQLConnection());
                    daUniformImages.Fill(dsUniformImages);

                    foreach (DataRow drUniformImages in UniformImages.Tables[0].Rows)
                    {
                        dsUniformImages.Tables[0].ImportRow(drUniformImages);
                    }
                    dsUniformImages.AcceptChanges();
                    foreach (DataRow drUniformImages in dsUniformImages.Tables[0].Rows)
                    {
                        drUniformImages.SetAdded();
                    }
                    cmdSqlBuilder = new SqlCommandBuilder(daUniformImages);
                    daUniformImages.InsertCommand = cmdSqlBuilder.GetInsertCommand(false);
                    daUniformImages.Update(dsUniformImages.Tables[0]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To insert current game data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <param name="intReporterID"></param>
        /// <param name="strReporterRole"></param>

        public void InsertCurrentGame(int intGameCode, int intOldGameCode, int intFeedNumber, int intReporterID, string strReporterRole, int intModuleID, int TimeElapsed, String SysLocalTime, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@OldGameCode", intOldGameCode), new SqlParameter("@FeedNumber", intFeedNumber), new SqlParameter("@ReporterID", intReporterID), new SqlParameter("@ReporterRole", strReporterRole), new SqlParameter("@ModuleID", intModuleID), new SqlParameter("@TimeElapsed", TimeElapsed), new SqlParameter("@SysLocalTime", SysLocalTime), new SqlParameter("@Type", Type) };
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_InsertCurrentGame", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// FETCHES MODULE 2 GAME DETAILS ASSIGNED TO THE REPORTER
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// <returns>A DataSet contains game details</returns>

        public DataSet GetMultipleGameDetails(int LanguageID, int UserType, DateTime GameDate)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@UserType", UserType), new SqlParameter("@GameDate", GameDate) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GetMultipleGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// INSERTING MODULE 2 MULTIPLE GAMES INTO CURRENT_GAME SQL TABLE AND THE GAME THAT IS SELECTED IN SELCET GAME
        /// IS MADE ACTIVE STATUS AS "Y"
        /// </summary>
        /// <param name="Mod2Games"></param>
        /// <returns></returns>
        public int InsertMultipleGames(string @Mod2Games)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Mod2Games", @Mod2Games) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD2_InsertCurrentGame", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS THE COUNT OF RECORDS FROM SQL IF IT IS RESTARTED (MAINLY FOR T1 WORKFLOW)
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="ModuleID"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetLocalSQLDataCountForRestart(string GameCode, int FeedNumber, int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@MODULE_ID", ModuleID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetLocalDataCountForRestart", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// GETS THE PBPRECORDS FROM SQL IF IT IS RESTARTED (MAINLY FOR T1 WORKFLOW)
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="ModuleID"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetLocalSQLDataForRestart(int GameCode, int FeedNumber, int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@MODULE_ID", ModuleID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetLocalDataForRestart", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// DELETE THE SELECTED GAME DETAILS FROM SQL LIVE TABLES
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public int DeleteLiveTables(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_DeleteGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //FPLRS-139	
        public DataSet GetSqlLanguage()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_SELECTLANGUAGE", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int ValidateReporter(string Loginname)
        {
            try
            {
                int rptCount;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@ReporterLoginId", Loginname) };
                rptCount = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_ValidateReporter", ParamValues));
                return rptCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetReporterDetails(string Loginname)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@ReporterLoginId", Loginname) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_GetReporterDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetGameDetails()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_GetGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region " DEMO "
        /// <summary>
        /// INSERTS THE DEMO DATA INTO LOCAL SQL TABLES 
        /// </summary>
        /// <param name="SoccerDataXML">A String represernts the XMLdata </param>
        /// <returns>An Integer</returns>
        public int InsertDemoData(string SoccerDemoDataXML)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerDemoData", SoccerDemoDataXML) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Download_All_DemoData_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// GETSAMPLEGAME TO DISPLAY IN LISTVIEW
        /// </summary>
        /// <returns></returns>
        public DataSet GetSampleGameDisplay()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETSAMPLEGAME_DISPLAY", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet InsertSampleGame()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_CreateSampleGameData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetFeedCoverageInfo(int GameCode, int ReporterID, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@ReporterID", ReporterID), new SqlParameter("@FEED_NUMBER", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetFeedCoverageInfo", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

    }


}


