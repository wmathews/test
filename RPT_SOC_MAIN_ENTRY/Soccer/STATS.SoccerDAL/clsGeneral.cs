﻿# region "Using"

using STATS.DataAccess;
using System;
using System.Data;
using System.Data.SqlClient;

#endregion

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsGeneral
// Author        : Dijo Davis
// Created Date  : 07-05-09
// Description   : This is the data access for general functions
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsGeneral
    {
        #region "Constants & Variables"
        private static clsGeneral Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
        #endregion "Constants & Variables"

        #region "Shared methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY
        public static clsGeneral GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT
            if (Instance == null)
            {
                Instance = new clsGeneral();
            }
            return Instance;
        }

        #endregion "Shared methods"

        #region "General"

        /// <summary>
        /// To fetch team logos from database
        /// </summary>
        /// <param name="SQLQuery"></param>
        /// <returns></returns>
        public DataSet GetTeamLogo(int intLeagueID, int intTeamID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LeagueID", intLeagueID), new SqlParameter("@TeamID", intTeamID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_General_FetchTeamLogos", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get unprocessed records in all modules
        /// </summary>
        /// <returns></returns>
        public object GetUnprocessedRecords(int intGameCode, int intFeedNumber, int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", intGameCode), new SqlParameter("@Feed_Number", intFeedNumber), new SqlParameter("@ModuleID", ModuleID) };
                return SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetUnprocessedRecords", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Function to fetch Home & Away team rosters
        /// </summary>
        /// <param name="intTeamID"></param>
        /// <returns></returns>
        public DataSet GetTeamRostersforCommentaryandTouches(int intTeamID, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", intTeamID), new SqlParameter("@language_id", LanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTeamRosters", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To fetch Home & Away team rosters by LineUps
        /// </summary>
        /// <param name="intTeamID"></param>
        /// <returns></returns>
        public DataSet GetTeamRostersforCommentaryandTouchesByLineUps(int intTeamID, int intlanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", intTeamID), new SqlParameter("@LANGUAGE_ID", intlanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetTeamRosters_By_Lineups", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To return the Home/Away Team Scores from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>A Dataset containing both the Home and Away Team Scores</returns>
        public DataSet GetScores(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetScores", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD2_FetchCurrentGameTime", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTeamColor(int GameCode, int FeedNumber, bool IsHomeTeam, int TeamColor)
        {
            try
            {
                int intIsHomeTeam;
                if (IsHomeTeam == true)
                {
                    intIsHomeTeam = 1;
                }
                else
                {
                    intIsHomeTeam = 0;
                }
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@IsHomeTeam", intIsHomeTeam), new SqlParameter("@TeamColor", TeamColor) };
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_UpdateTeamColor", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To return the Unique Id from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>An integer value representing the UniqueId that need to be used</returns>
        public int GetUniqueID(int GameCode, int FeedNumber)
        {
            try
            {
                int intUniqueID;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                intUniqueID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetUniqueID", ParamValues));
                return intUniqueID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Refresh Part for Multi Reporter Communication
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet RefreshGameData(int GameCode, int FeedNumber, int ModuleID, bool IsEditMode, int language_id)
        {
            try
            {
                if (IsEditMode == true)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ModuleID", ModuleID), new SqlParameter("@IsEditMode", "T"), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_RefreshGameData", ParamValues);
                }
                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ModuleID", ModuleID), new SqlParameter("@IsEditMode", "F"), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_RefreshGameData", ParamValues);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetLastClockEvent(int GameCode, int FeedNumber, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_LAST_CLOCK_EVENT", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet DeleteCurrentGame(int GameCode, int FeedNumber, int ReporterID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ReporterID", ReporterID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_DeleteCurrentGame", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GOPAL ADDED BELOW CODE TO UPDATE LAST ENTRY STATUS OF ASSISTER
        public int UpdateLastEntryStatus(int GameCode, int FeedNumber, bool IsEntryHappened)
        {
            try
            {
                if (IsEntryHappened)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@LastEntryStatus", "N") };
                    return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "STP_UpdateLastEntryStatus", ParamValues);
                }
                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@LastEntryStatus", DBNull.Value) };
                    return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "STP_UpdateLastEntryStatus", ParamValues);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPeriodEndCount(int GameCode, int FeedNumber, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPeriodEndCount", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet PzEndPeriodCount(int GameCode, int FeedNumber, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Pz_EndPeriodCount", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPeriodEndCount_Touches(int GameCode, int FeedNumber, int Period)
        {
            try
            {
                int Count;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPeriodEndCount_Touches", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// FETCHES PBP EVENS (GOAL,EXPULSION,PENALTY AND BOOKINGS) SO AS TO DISPLAY IN THE MAIN SCREEN
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet FetchMainScreenEvents(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_FetchMainScreenPBPEvents", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet FetchSubsforCommAndTouches(int GameCode)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_FetchSubsForCommAndTouches", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet RefreshPlayers(Int64 UniqueID, Int64 GameCode)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@UNIQUEID", UniqueID), new SqlParameter("@GAMECODE", GameCode) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD3_RefreshPlayers", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GET GAME DETAILS ASSIGNED TO THE REPORTER
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// <returns>A DataSet contains game details</returns>
        public DataSet GetGameDetails(int LanguageID, int Reporter_ID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@Reporter_ID", Reporter_ID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GetMultipleGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertCurrentGame(int intGameCode, int intOldGameCode, int intFeedNumber, int intReporterID, string strReporterRole, int intModuleID, int TimeElapsed, String SysLocalTime, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@OldGameCode", intOldGameCode), new SqlParameter("@FeedNumber", intFeedNumber), new SqlParameter("@ReporterID", intReporterID), new SqlParameter("@ReporterRole", strReporterRole), new SqlParameter("@ModuleID", intModuleID), new SqlParameter("@TimeElapsed", TimeElapsed), new SqlParameter("@SysLocalTime", SysLocalTime), new SqlParameter("@Type", Type) };
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_InsertCurrentGame", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getEndGameCount(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                //return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_getEndGameCount", ParamValues);
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_getEndGameCount", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string getTouchesHomeStartSide(int GAMECODE, int FEEDNUMBER,int LEAGUEID)
        //{
        //    try
        //    {
        //        return Convert.ToString(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT HOME_START_SIDE_TOUCH FROM LIVE_SOC_GAME WHERE GAME_CODE= " + GAMECODE + " AND FEED_NUMBER= " + FEEDNUMBER + " AND LEAGUE_ID=" + LEAGUEID + ""));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public DataSet getTouchesHomeStartSide(int GAMECODE, int FEEDNUMBER, int LEAGUEID)
        {
            try
            {
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT HOME_START_SIDE_TOUCH,HOME_START_SIDE_TOUCH_ET FROM LIVE_SOC_GAME WHERE GAME_CODE= " + GAMECODE + " AND FEED_NUMBER= " + FEEDNUMBER + " AND LEAGUE_ID=" + LEAGUEID + "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPreport(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA_report", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTouchesreport(int GameCode, int FeedNumber, int langage_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Language", langage_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetTouches_Report", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPZActionreport(int GameCode, int FeedNumber, int GameClock)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@GameClockEachPeriod", GameClock) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_ACTION_REPORT", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getDataForEvent(int GameCode, int FeedNumber, int Period, int evtID, int ModuleId)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period), new SqlParameter("@Event", evtID), new SqlParameter("@ModuleID", ModuleId) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetDataForEvent", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SportVUFeedAvailability(int GameCode)
        {
            try
            {
                int intGameCnt;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode) };
                intGameCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_IsSportVUFeedAvailable", ParamValues));
                return intGameCnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getAssignmentForCommentary(int GameCode, int RepID, int ModuleID, int SerialNO)
        {
            try
            {
                //int intGameCnt;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@RepID", RepID), new SqlParameter("@ModuleID", ModuleID), new SqlParameter("@SerialNO", SerialNO) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetReportersAssignForCommentary", ParamValues);

                //intGameCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetReportersAssignForCommentary", ParamValues));
                //return intGameCnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet FetchFouls(int GameCode, int FeedNumber, int Period, int TeamID, int timeElaped)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period), new SqlParameter("@TEAM_ID", TeamID), new SqlParameter("@TIM_ELAPSED", timeElaped) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_FetchFouls", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetRosterInfo(int Gamecode, int LeagueID, string Type, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@Type", Type), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetRosterInfo", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPenaltyShootoutScores(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPenaltyShootoutScores", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPenaltyShootoutScoresPz(int gameCode, int feedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", gameCode), new SqlParameter("@FeedNumber", feedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_GetPenaltyShootoutScore", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetMultipleGameDetails(int LanguageID, int Reporter_ID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@Reporter_ID", Reporter_ID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GetMultipleGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatehomesideET(int GameCode, int FeedNumber, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_UpdateHomeSide", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS RULES DATA
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetRulesData(int LeagueID, int CoverageLevel)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LEAGUE_ID", LeagueID), new SqlParameter("@COVERAGE_LEVEL", CoverageLevel) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetRulesData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetOnfieldValue(int Gamecode, int FeedNumber, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", Gamecode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@PERIOD", Period) };
                int onfield_id = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_GetOnfieldID", ParamValues));
                return onfield_id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetNextOnfieldValue(int Gamecode, int FeedNumber, int TeamID, int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", Gamecode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@TEAM_ID", TeamID), new SqlParameter("@PERIOD", Period) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetNextOnfieldID", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPData(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxTimeElapsedTouches(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD3_GET_MAX_TIMEELAPSED_TOUCHES", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet InsertPBPDataToSQL(string PBPXMLData, int GameCode, int FeedNumber, string isEdited, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatePairedPBPEvents(string PBPXMLData, int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, string Type, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@Type", Type), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PairedPBPEvents", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPRecordBasedOnSeqNumber(int GameCode, int FeedNumber, decimal Sequence_Number, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchPBPRecordBasedOnSeqNo", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet LanguageTranslate(int intLangID)
        {
            try
            {
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT LABEL_ID,LANGUAGE_ID,LABEL_DESCRIPTION  FROM LIVE_LANGUAGE_LABEL_XLATE"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetMultilingual(int languageId)
        {
            try
            {
                SqlParameter[] paramValues = new SqlParameter[] { new SqlParameter("@LANGUAGE_ID", languageId) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetMultilingual", paramValues));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// TO GET THE COLORS THE TEAMS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTeamColors(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETTEAMCOLOR", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetGoalieChangeEvent(int GameCode, int FeedNumber, int TeamID, int CoverageLevel)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@TEAM_ID", TeamID) };
                if (CoverageLevel == 6)
                {
                    return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetGoalieChangeEvent", ParamValues));
                }
                else
                {
                    return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetGoalieChangeEvent", ParamValues));
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TO GET LAST EVENT DETAILS
        public DataSet GetLastEventDetails(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_LASTEVENT_DETAILS", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertTeamGameForLowTiers(int GameCode, int FeedNumber, string ReporterRole)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_InsertTeamGameForLowTiers", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_MAX_TIMEELAPSED", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetKeymomentsData(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetKeyMomentData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion "General"

        #region "Demo"

        public DataSet GetDemoGameDetails()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETSAMPLEGAME_DISPLAY", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion "Demo"

        #region "Prozone"

        public DataSet LoadPlaybyPlaydata(int gameCode, int feedNumber, int languageId, int gameClockEachPeriod, String refreshedOnly, int reporterSlNo, int teamId)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", gameCode), new SqlParameter("@FEED_NUMBER", feedNumber), new SqlParameter("@LANGUAGE_ID", languageId), new SqlParameter("@GameClockEachPeriod", gameClockEachPeriod), new SqlParameter("@REFRESHEDONLY", Convert.ToChar(refreshedOnly)), new SqlParameter("@REPORTERSLNO", reporterSlNo), new SqlParameter("@TEAMID", teamId) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_LoadPlaybyPlayData", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet LoadPrimaryRepInsertedPBPData(int gameCode, int feedNumber, int languageId, int gameClockEachPeriod)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", gameCode), new SqlParameter("@FEED_NUMBER", feedNumber), new SqlParameter("@LANGUAGE_ID", languageId), new SqlParameter("@GameClockEachPeriod", gameClockEachPeriod) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_LoadPrimaryRepInsertedPBPData", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet RefreshPZPbpData(int gameCode, int feedNumber, int languageId, int defaultGameClock, bool loadFullPbp, int reporterSlNo, int teamId)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", gameCode), new SqlParameter("@FEED_NUMBER", feedNumber), new SqlParameter("@LANGUAGE_ID", languageId), new SqlParameter("@DEFAULT_GAMECLOCK", defaultGameClock), new SqlParameter("@LOADFULLPBP", loadFullPbp ? "Y" : "N"), new SqlParameter("@REPORTERSLNO", reporterSlNo), new SqlParameter("@TEAMID", teamId) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_RefreshPBPData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet RefreshPZGameInfoData(int gameCode, int feedNumber, int languageId)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", gameCode), new SqlParameter("@FEED_NUMBER", feedNumber), new SqlParameter("@LANGUAGE_ID", languageId) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_RefreshGameInfoData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetAllRosters Module 3 - Tier 6 to fetch all rosters
        /// </summary>
        /// <param name="gameCode"></param>
        /// <param name="languageId"></param>
        /// <param name="sequenceNumber"></param>
        /// <returns></returns>
        public DataSet GetAllRosters(int gameCode, int languageId, decimal sequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", gameCode), new SqlParameter("@LANGUAGE_ID", languageId), new SqlParameter("@SEQUENCE_NUMBER", sequenceNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetAllRosters", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetPlayerCntforYellowCard(int GameCode, int FeedNumber, int EventID, int PlayerID, Decimal SequenceNumber)
        {
            try
            {
                int Count;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EventID", EventID), new SqlParameter("@PlayerID", PlayerID), new SqlParameter("@SequenceNumber", SequenceNumber) };
                Count = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetCardCntForPlayer", ParamValues));
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Delete PBP Actions
        public int DeletePlaybyPlayData(decimal SequenceNumber, int ReporterRoleSerial)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SEQUENCE_NUMBER", SequenceNumber), new SqlParameter("@ReporterRoleSerial", ReporterRoleSerial) };
                return (SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_DeleteActionsPBPData", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get the last record time
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public int GetActionsMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetMaxTimeElapsed", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get the last record time
        /// </summary>
        /// <param name="gameCode"></param>
        /// <param name="feedNumber"></param>
        /// <returns></returns>
        public int GetInjuryTimePeriodCount(int gameCode, int feedNumber, int period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", gameCode), new SqlParameter("@FeedNumber", feedNumber), new SqlParameter("@Period", period) };
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetInjuryTimePeriodCount", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion "Prozone"
    }
}