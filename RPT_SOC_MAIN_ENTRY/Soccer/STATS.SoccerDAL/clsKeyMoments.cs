﻿#region "Using"
using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;
#endregion "Using"

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsKeyMoments
// Author        : Ravi Krishna G
// Created Date  : 18-06-09
// Description   : This is the data access logic for Module1 PBP.
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsKeyMoments
    {

        #region "Member Variables"

        private static clsKeyMoments m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion 

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsKeyMoments GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsKeyMoments();
            }
            return m_myInstance;
        }

        #endregion

        #region "User Functions"

        /// <summary>
        /// To insert the KeyMoments to PBP Table
        /// </summary>
        /// <param name="FromUniqueID">An Integer value representing the From Unique Id</param>
        /// <param name="ToUniqueID">An Integer value representing the To Unique Id</param>
        /// <param name="KeyMomentID">An Unique integer value representing the KeyMoment</param>
        /// <param name="GameCode">An Integer value representing the Game Code</param>
        /// <param name="FeedNumber">An Integer value representing the Feed Number</param>
        /// <returns>A Dataset containing the affected rows</returns>
        public DataSet InsertKeyMomentsToPBP(Decimal FromUniqueID, Decimal ToUniqueID, int KeyMomentID, int GameCode, int FeedNumber,string Reporter_Role)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FromUniqueID", FromUniqueID), new SqlParameter("@ToUniqueID", ToUniqueID), new SqlParameter("@KeyMomentID", KeyMomentID), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) , new SqlParameter("@Reporter_Role", Reporter_Role)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_InsertKeyMoments", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To Get the unique Keymoment Id from the LIVE_PBP Table
        /// </summary>
        /// <param name="GameCode">An Integer value representing the Game Code</param>
        /// <param name="FeedNumber">An Integer value representing the Feed Number</param>
        /// <returns>An Integer representing the unique Keymoment ID</returns>
        public int GetKeyMomentID(int GameCode, int FeedNumber)
        {
            try
            {
                int intKeyMomentID;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                intKeyMomentID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_GetKeyMomentID", ParamValues));
                return intKeyMomentID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To Delete the KeyMoment associated with a set of PBP Events
        /// </summary>
        /// <param name="FromUniqueID">An Integer value representing the From Unique Id</param>
        /// <param name="ToUniqueID">An Integer value representing the To Unique Id</param>
        /// <param name="KeyMomentID">An Unique integer value representing the KeyMoment</param>
        /// <param name="GameCode">An Integer value representing the Game Code</param>
        /// <param name="FeedNumber">An Integer value representing the Feed Number</param>
        /// <returns>An Integer value representing the number of rows deleted which can be considered as Success/Failure</returns>
        public int DeleteKeyMoments(Decimal FromUniqueID, Decimal ToUniqueID, int KeyMomentID, int GameCode, int FeedNumber,string Reporter_Role)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FromUniqueID", FromUniqueID), new SqlParameter("@ToUniqueID", ToUniqueID), new SqlParameter("@KeyMomentID", KeyMomentID), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Reporter_Role", Reporter_Role) };
                return Convert.ToInt32((SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_DeleteKeyMoments", ParamValues)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To Update the KeyMoments to PBP Table
        /// </summary>
        /// <param name="FromUniqueID">An Integer value representing the From Unique Id</param>
        /// <param name="ToUniqueID">An Integer value representing the To Unique Id</param>
        /// <param name="KeyMomentID">An Unique integer value representing the KeyMoment</param>
        /// <param name="GameCode">An Integer value representing the Game Code</param>
        /// <param name="FeedNumber">An Integer value representing the Feed Number</param>
        /// <returns>A Dataset containing the affected rows</returns>
        public DataSet UpdateKeyMomentsToPBP(Decimal FromUniqueID, Decimal ToUniqueID, int KeyMomentID, int GameCode, int FeedNumber,string Reporter_Role)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FromUniqueID", FromUniqueID), new SqlParameter("@ToUniqueID", ToUniqueID), new SqlParameter("@KeyMomentID", KeyMomentID), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Reporter_Role", Reporter_Role) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_UpdateKeyMoments", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// To get KeyMoments data
        /// </summary>
        /// <param name="FromUniqueID">An Integer value representing the From Unique Id</param>
        /// <param name="ToUniqueID">An Integer value representing the To Unique Id</param>
        /// <param name="KeyMomentID">An Unique integer value representing the KeyMoment</param>
        /// <param name="GameCode">An Integer value representing the Game Code</param>
        /// <param name="FeedNumber">An Integer value representing the Feed Number</param>
        /// <returns>A Dataset containing the affected rows</returns>
        public DataSet GetKeymomentsData(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetKeyMomentData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion "User Functions"
    }
}
