﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsGameSetup
// Author        : Shravani
// Created Date  : 28th April,2009
// Description   : 
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace STATS.SoccerDAL
{
    public class clsGameSetup
    {

        #region " Constants & Variables "
        private static clsGameSetup Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
        #endregion

        /// <summary>
        /// SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        /// </summary>
        /// <returns></returns>
        public static clsGameSetup GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsGameSetup();
            }
            return Instance;
        }


        /// <summary>
        /// GETS THE GAMESETUP DETAILS 
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// <returns>A DataSet contains game details</returns>
        public DataSet SeletGameSetup(int GameCode, int FeedNumber, int LeagueID, int AwayTeamID, int HomeTeamID, int LanguageID, int Tier, int Module)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@LEAGUE_ID", LeagueID), new SqlParameter("@AWAY_TEAM_ID", AwayTeamID), new SqlParameter("@HOME_TEAM_ID", HomeTeamID), new SqlParameter("@Language", LanguageID), new SqlParameter("@Tier", Tier), new SqlParameter("@Module", Module) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_SelectGameSetup", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Inserts or Updates the Gamesetup info
        /// </summary>
        /// <param name="XMLGameSetupData"></param>
        /// <returns></returns>
        public int InsertUpdateGameSetup(string XMLGameSetupData)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GamesetUpData", XMLGameSetupData) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_InsertAndUpdateGameSetup", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     /// <summary>
     /// GETS THE OFFICIAL AND MATCH DETAILS 
     /// </summary>
     /// <param name="GameCode">An Integer represents the GameCode</param>
     /// <param name="FeedNumber">An Integer represents the FeedNumber</param>
     /// <param name="LeagueID">An Integer represents the LeagueID</param>
     /// <returns>A Dataset which contains the Official and Match Information</returns>
        public DataSet GetOfficialAndMatchInfo(int GameCode, int FeedNumber, int LeagueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@LEAGUE_ID,", LeagueID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetOfficalAndMatchInfo", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
