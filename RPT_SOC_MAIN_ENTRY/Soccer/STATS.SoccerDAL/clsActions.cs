﻿using System;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsActions

    {


        #region "Member Variables"

        private static clsActions m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY
        public static clsActions GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT
            if (m_myInstance == null)
            {
                m_myInstance = new clsActions();
            }
            return m_myInstance;
        }

        #endregion

        #region "User Functions"
        /// <summary>
        /// Inserts Actions XML data from Primary reporter to SQL.
        /// </summary>
        /// <param name="PBPXMLData"></param>
        /// <returns></returns>
        public DataSet InsertEditActionsPBPData(string PBPXMLData, Decimal SeqNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@SEQUENCE_NUMBER", SeqNumber) };
              return(SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(),CommandType.StoredProcedure,  "USP_PZ_InsertEditActionsPBPData", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      
        #endregion
    }
}
