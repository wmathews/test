﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsAddManager
// Author        : Shravani
// Created Date  : 03rd July,2009
// Description   : Data Access Layer for AddManager screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion



namespace STATS.SoccerDAL
{
    public class clsAddManager
    {


        private static clsAddManager Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsAddManager GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsAddManager();
            }
            return Instance;
        }


        /// <summary>
        /// Selects Add Newly Add Manager Data
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns></returns>
        public DataSet SelectAddManager(Int64 GameCode,int TeamID,char DemoData )
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@TEAMID", TeamID), new SqlParameter("@DEMO_DATA", DemoData) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_SelectAddManager", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public DataSet InsertManager(int CoachID, int TeamID, string Moniker, string LastName, string ReporterRole,int LeagueID,char DemoData, String Type, Int64 GameCode, int FeedNumber)
        {
            try
            {
                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@NUPID", CoachID), new SqlParameter("@TEAMID", TeamID), new SqlParameter("@Moniker", Moniker), new SqlParameter("@LastName", LastName), new SqlParameter("@ReporterRole", ReporterRole), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@Demo_Data", DemoData), new SqlParameter("@TYPE", Type) };
                SqlParameter[] ParamValues = new SqlParameter[10];
                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@NUPID";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = CoachID;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@TEAMID";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = TeamID;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@Moniker";
                ParamValues[2].SqlDbType = SqlDbType.NVarChar;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = Moniker;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@LastName";
                ParamValues[3].SqlDbType = SqlDbType.NVarChar;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = LastName;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@ReporterRole";
                ParamValues[4].SqlDbType = SqlDbType.VarChar;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = ReporterRole;

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@LeagueID";
                ParamValues[5].SqlDbType = SqlDbType.VarChar;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = LeagueID;
                               
                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@DEMO_DATA";
                ParamValues[6].SqlDbType = SqlDbType.Char;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = DemoData;

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@TYPE";
                ParamValues[7].SqlDbType = SqlDbType.VarChar;
                ParamValues[7].Direction = ParameterDirection.Input;
                ParamValues[7].Value = Type;

                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@GameCode";
                ParamValues[8].SqlDbType = SqlDbType.BigInt;
                ParamValues[8].Direction = ParameterDirection.Input;
                ParamValues[8].Value = GameCode;

                ParamValues[9] = new SqlParameter();
                ParamValues[9].ParameterName = "@FeedNumber";
                ParamValues[9].SqlDbType = SqlDbType.TinyInt;
                ParamValues[9].Direction = ParameterDirection.Input;
                ParamValues[9].Value = FeedNumber;


                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_IED_Manager", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

               
    }
}
