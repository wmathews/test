﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsSportVU
    {
        #region "Member Variables"

        private static clsSportVU myInstance;
        private clsDataAccess objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsSportVU GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (myInstance == null)
            {
                myInstance = new clsSportVU();
            }
            return myInstance;
        }

        #endregion

        /// <summary>
        /// GET OPTICAL DATA PROVIDED BY SPORTVU
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>DATASET WHICH CONTAINS OPTICAL PBP DATA</returns>
        public DataSet GetOpticalData(int GameCode, int TeamID, int EventID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@TeamID", TeamID), new SqlParameter("@EventID", EventID) }; //
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GetPBPData", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GET AN OPTICAL EVENT FROM SPORTVU TABLE
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>DATASET WHICH CONTAINS OPTICAL EVENT WITH DATA POINTS</returns>
        public DataSet GetOpticalEvent(int GameCode, int UniqueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@UniqueID", UniqueID) };
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GetPBPEvent", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// UPDATE PROCESSED OPTICAL EVENTS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>True/False</returns>
        public int UpdateProcessedOpticalPBP(int GameCode, string UniqueIDs, char SaveIgnore)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@UniqueIDs", UniqueIDs), new SqlParameter("@SaveIgnore", SaveIgnore) };
                return SqlHelper.ExecuteNonQuery(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_UpdateProcessedPBP", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// GET OPTICAL DATA PROVIDED BY SPORTVU FOR TOUCH
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>DATASET WHICH CONTAINS OPTICAL TOUCH DATA</returns>
        public DataSet GetOpticalTouchData(int GameCode,int PlayerID, int TeamID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@PlayerID", PlayerID), new SqlParameter("@TeamID", TeamID) };
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GetTouchData", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// UPDATE PROCESSED OPTICAL EVENTS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>True/False</returns>
        public int UpdateProcessedOpticalTouch(int GameCode, string UniqueIDs)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@UniqueIDs", UniqueIDs) };
                return SqlHelper.ExecuteNonQuery(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_UpdateProcessedTouch", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GET AN OPTICAL EVENT FROM SPORTVU TABLE
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>DATASET WHICH CONTAINS OPTICAL EVENT WITH DATA POINTS</returns>
        public DataSet GetOpticalTouchEvent(int GameCode, int UniqueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@UniqueID", UniqueID) };
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GettTouchEvent", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GET ALL THE SPORTVU EVENT DATA
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns>DATASET WHICH CONTAINS OPTICAL EVENT WITH DATA POINTS</returns>
        public DataSet GetAllSportVUData(int GameCode,Int16 ModuleID )
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@ModuleID", ModuleID) };
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GetAllEventData", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ModuleID"></param>
        /// <returns></returns>
        public DataSet GetMaxUniqueID(int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@ModuleID", ModuleID) };
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GetUniqueID", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To get the Field dimensions
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns></returns>

        public DataSet GetFieldDimensions(int GameCode)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode) }; 
                return SqlHelper.ExecuteDataset(objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SportVU_GetFieldDeminesions", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
