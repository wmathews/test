﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsAddTeam
// Author        : Shravani
// Created Date  : 20th Aug,2009
// Description   : 
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion
namespace STATS.SoccerDAL
{
    public class clsAddTeam
    {


        #region "Member Variables"

        private static clsAddTeam m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion 

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsAddTeam GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsAddTeam();
            }
            return m_myInstance;
        }

        public DataSet GetLeague()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetLeague", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LEAGUEID"></param>
        /// <param name="TEAMNAME"></param>
        /// <param name="TEAMNICKNAME"></param>
        /// <param name="TEAMABBREV"></param>
        /// <returns></returns>
        public int InsertAddTeam(int LEAGUEID, string @TEAMNAME, string TEAMNICKNAME, string @TEAMABBREV)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LEAGUE", LEAGUEID), new SqlParameter("@TEAM_NAME", @TEAMNAME), new SqlParameter("@TEAMNICKNAME", TEAMNICKNAME), new SqlParameter("@TEAMABBREV", TEAMABBREV) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_InsertAddTeam", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TEAMID"></param>
        /// <param name="LEAGUEID"></param>
        /// <param name="TEAMNAME"></param>
        /// <param name="TEAMNICKNAME"></param>
        /// <param name="TEAMABBREV"></param>
        /// <returns></returns>
        public int UpdateAddTeam(int TEAMID, int LEAGUEID, string @TEAMNAME, string TEAMNICKNAME, string @TEAMABBREV)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TEAMID", TEAMID), new SqlParameter("@LEAGUE", LEAGUEID), new SqlParameter("@TEAM_NAME", @TEAMNAME), new SqlParameter("@TEAMNICKNAME", TEAMNICKNAME), new SqlParameter("@TEAMABBREV", TEAMABBREV) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_UpdateAddTeam", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


       /// <summary>
       /// 
       /// </summary>
       /// <returns></returns>
        public DataSet SelectAddTeam()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_SelectAddTeam", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TeamID"></param>
        /// <returns></returns>
        public DataSet DeleteAddTeam(int TeamID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamId", TeamID)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "[USP_Demo_DeleteAddTeam]", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

    }
}
