﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsModule1PBP
// Author        : Ravi Krishna G
// Created Date  : 18-05-09
// Description   : This is the data access logic for Module1 PBP.
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsModule1PBP
    {

    #region "Member Variables"

        private static clsModule1PBP m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

    #endregion 

    #region "Shared Methods"

    //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsModule1PBP GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsModule1PBP();
            }
            return m_myInstance;
        }

    #endregion

    #region "User Functions"

        public void AddPBP()
        {
            try
            {
                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@FeedNumber", intFeedNumber), new SqlParameter("@LanguageID", intLanguageID), new SqlParameter("@Period", intPeriod), new SqlParameter("@TimeElapsed", intTimeElapsed), new SqlParameter("@HeadLine", strheadLine), new SqlParameter("@Commentary", strComment) };
                //SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_AddCommentry", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To return the Unique Id from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>An integer value representing the UniqueId that need to be used</returns>
        public int GetUniqueID(int GameCode, int FeedNumber)
        {
            try
            {
                int intUniqueID;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber)};
                intUniqueID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetUniqueID", ParamValues));
                return intUniqueID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetTableCount(string TableName, string ColumnName)
        {
            try
            {   
                DataSet dsTableCount=new DataSet();

                dsTableCount = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, "Select count(*) from " + TableName + " ");
                if (dsTableCount.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(dsTableCount.Tables[0].Rows[0][0]) != "")
                        return Convert.ToInt32(dsTableCount.Tables[0].Rows[0][0]);
                    else
                        return -1;
                }
                else
                    return -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetSortOrder(string TableName, string ColumnName, int ColumnValue)
        {
            try
            {
                DataSet dsTableCount = new DataSet();

                dsTableCount = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, "Select Sort_Order from " + TableName + " WHERE " + ColumnName + " = " + ColumnValue);

                if (dsTableCount.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(dsTableCount.Tables[0].Rows[0][0]) != "")
                        return Convert.ToInt32(dsTableCount.Tables[0].Rows[0][0]);
                    else
                        return -1;
                }
                else
                    return -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxEventID()
        {
            try
            {
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetMaxEventID"));                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber)};
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_MAX_TIMEELAPSED", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public int GetMaxTimeElapsedMain(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber)};
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_MAX_TIMEELAPSEDMAIN", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int checkForHalfTimeEvents(int GameCode, int FeedNumber, decimal Sequence_Number)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SEQUENCE_NUMBER", Sequence_Number) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_CHECKFORHALFTIMEevents", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetLastEventDetails(int GameCode, int FeedNumber)
        {
            try
            {
          
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber)};
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_LASTEVENT_DETAILS", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        
        /// <summary>
        /// To return the Home/Away Team Scores from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>A Dataset containing both the Home and Away Team Scores</returns>
        public DataSet GetScores(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return(SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetScores", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      /// <summary>
      /// Handles normal entry,Time Edit and Insert Before functionalities
      /// </summary>
      /// <param name="PBPXMLData"></param>
      /// <param name="GameCode"></param>
      /// <param name="FeedNumber"></param>
      /// <param name="ReporterRole"></param>
      /// <param name="IsTimeEdited"></param>
      /// <param name="IsInsertBefore"></param>
      /// <param name="Sequence_Number"></param>
      /// <returns></returns>
        public DataSet InsertPBPDataToSQL(string PBPXMLData,int GameCode, int FeedNumber,string  ReporterRole,bool IsTimeEdited,bool IsInsertBefore,Decimal Sequence_Number,bool IsFoulAssociated,int language_id)
        {
            try
            {
                if (IsFoulAssociated == true)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SEQUENCE_NUMBER", Sequence_Number),new SqlParameter("@language_id",language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_LinkBookingsWithFouls", ParamValues);
                }
                else if (IsTimeEdited == true)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_MainEntryTimeEdit", ParamValues);
                }
                else if (IsInsertBefore == true)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SEQUENCE_NUMBER", Sequence_Number), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_Before", ParamValues);
                }

                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole),new SqlParameter("@language_id",language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
                }
             
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateSubstitutePlayer(int LeagueID, int PlayerInID,int PlayerOutID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@PlayerInID", PlayerInID), new SqlParameter("@PlayerOutID", PlayerOutID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_UpdateSUBSTITUTEPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// FTECHING PBP RECORDS BASED ON SEQUENCE NUMBER WHICH IS MAINLY USED FOR EDITING PURPOSE
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <param name="Sequence_Number"></param> 'THE RECORD IS FETCHED BASED ON SEQUECE NUMBER BY PASSING IT AS AN INPUT
        /// <param name="Type"></param> 'REPRESENTS WHICH QUERY TO GET EXECUTED
        /// <returns></returns>
        public DataSet GetPBPRecordBasedOnSeqNumber(int GameCode, int FeedNumber,decimal Sequence_Number,String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchPBPRecordBasedOnSeqNo", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet FetchSeqNumBasedOnTime(int GameCode, int FeedNumber, int timeElapsed, int Period, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ElapsedTime", timeElapsed), new SqlParameter("@Period", Period), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchSeqNumBasedOnTime", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet checkEventHalftime(int GameCode, int FeedNumber, int Period, int evtCode)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Period", Period), new SqlParameter("@EventID", evtCode) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_checkEventHalftime", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public DataSet GetPBPRecordBasedOnTime(int GameCode, int FeedNumber, int Time_Elapsed, int Period, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ElapsedTime", Time_Elapsed), new SqlParameter("@Period", Period), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchSeqNumBasedOnTime", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPData(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber),new SqlParameter("@language_id",language_id)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getArea(int GameCode, int FeedNumber, decimal SeqNum)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SeqNum", SeqNum) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GET_AREA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataSet DeletePBPData(int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ReporterRole", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber),new SqlParameter("@language_id",language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_DeletePBPData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet DeleteBookings(int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ReporterRole", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_DeleteBookings", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPrevSeq(int GameCode, int FeedNumber, Decimal SequenceNumber, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@Type", Type) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GET_PREV_SEQUENCE", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetPlayerCount(int GameCode, int FeedNumber, Decimal SequenceNumber, int PlayerID)
        {
            try
            {
                int Count;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@PlayerID", PlayerID) };
                Count = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_PLAYER_COUNT_PREV", ParamValues));
                
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetPlayerForEvent(int GameCode, int FeedNumber, int EventID, int PlayerID)
        {
            try
            {
                int Count;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EventID", EventID), new SqlParameter("@PlayerID", PlayerID) };
                Count = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetPlayerForEvent", ParamValues));

                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the Score Event 
        /// </summary>
        /// <param name="SequenceNumber"></param>
        /// <returns></returns>
        public int DeleteScoreEvent(int GameCode, int FeedNumber,Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode), new SqlParameter("@Feed_Number", FeedNumber),new SqlParameter("@Sequence_Number", SequenceNumber) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Delete_Score_Event", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetTeamIDBasedonSeqNumber(int GameCode, int FeedNumber, Decimal SequenceNumber)
        {
            try
            {
                
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber),new SqlParameter("@SequenceNumber", SequenceNumber) };
                //TeamID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetTeamIDBasedonSeqNumber", ParamValues));
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetTeamIDBasedonSeqNumber", ParamValues);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int EditScoreEvent(int GameCode, int FeedNumber, Decimal SequenceNumber, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode), new SqlParameter("@Feed_Number", FeedNumber), new SqlParameter("@Sequence_Number", SequenceNumber), new SqlParameter("@Type", Type) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_EDIT_SCORE_PBP", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ReplaceScoreEvent(int GameCode, int FeedNumber, Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode), new SqlParameter("@Feed_Number", FeedNumber), new SqlParameter("@Sequence_Number", SequenceNumber) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_REPLACE_SCORE_EVENT", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertScoreEvent(int GameCode, int FeedNumber,String ReporterRole, Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode), new SqlParameter("@Feed_Number", FeedNumber), new SqlParameter("@Reporter_Role", ReporterRole), new SqlParameter("@Sequence_Number", SequenceNumber) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_INSERT_SCORE_EVENT", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPlayerInOut(int GameCode, int FeedNumber, Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "Usp_MOD1_GetPlayerInOut", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPDataForEdit(int GameCode, int FeedNumber, Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GETPBPDATA_EDIT", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatePBPData(string PBPXMLData, int GameCode, int FeedNumber, Decimal SequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Update_PBP_Data", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatePairedPBPEvents(string PBPXMLData, int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, string Type, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber),new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@Type", Type),new SqlParameter("@language_id",language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PairedPBPEvents", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetFormationCoach(int Gamecode, int FeedNumber, int ManagerID, String Type, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ManagerID", ManagerID), new SqlParameter("@Type", Type), new SqlParameter("@language_id",language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetCoachForExpulsion", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// FETCHES HOME AND AWAY TEAM ROSTERS FOR THE SPECIFIED GAMECODE AND LEAGUEID
        /// </summary>
        /// <param name="Gamecode"></param>
        /// <param name="LeagueID"></param>
        /// <returns>A DATASET CONTAINING 3 TABLES TABLE1 : HOME TEAM ROSTERS TABLE2 : AWAY TEAM ROSTERS TABLE 3 : TEAM NAMES WITH THEIR CORRESPONDING ID'S</returns>

        public DataSet GetRosterInfo(int Gamecode, int LeagueID, string Type,int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@Type", Type), new SqlParameter("@Language", LanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetRosterInfo", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// FETCHES PBP EVENS (GOAL,EXPULSION,PENALTY AND BOOKINGS) SO AS TO DISPLAY IN THE MAIN SCREEN
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet FetchMainScreenEvents(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_FetchMainScreenPBPEvents", ParamValues);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet TimeEditOnPBPEvents(string PBPXMLData, int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber,int LanguageID,char IsEventModified)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@LANGUAGE_ID", LanguageID), new SqlParameter("@ISEVENT_MODIFIED", IsEventModified)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_TIMEEDIT", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPMainEventData(int GameCode, int FeedNumber,  Decimal SequenceNumber, string Type)
        {
            try
            {
                if (Type == "SNO")
                {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber),  new SqlParameter("@SequenceNumber", SequenceNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetMainEventSNO", ParamValues);
                }
                else
                {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPairedEvents", ParamValues);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetPBPEvents(int GameCode, int FeedNumber,int Period, int TimeElapsed)
        {
            try
            {
                return Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT COUNT(*) FROM LIVE_SOC_PBP WHERE GAME_CODE = " + GameCode + " AND FEED_NUMBER =" + FeedNumber + " AND PERIOD = " + Period + "  AND EVENT_CODE_ID NOT IN (54,55,56,57) AND RECORD_EDITED='N' AND SEQUENCE_NUMBER <> -1 AND TIME_ELAPSED > " + TimeElapsed + ""));
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetKeyMomentStartSNO( int GameCode, int FeedNumber,  int KeyMomentID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@KEY_MOMENT_ID", KeyMomentID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetKeyMomentStartSNO", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetGoalieChangeEvent(int GameCode, int FeedNumber,int TeamID)
        {
            try
            {

                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@TEAM_ID", TeamID) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetGoalieChangeEvent", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        public DataSet UpdateActiveGK(int GameCode, int FeedNumber, int TeamID, int LangID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@@TeamID", TeamID), new SqlParameter("@language_id", LangID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_ActiveGoalKeeper", ParamValues);

                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited) };
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion

        


    }
}
