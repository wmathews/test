﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsUpComingAssignments
// Author        : Shravani
// Created Date  : 08th July,2009
// Description   : Data Access Layer for UpcomingAssignments screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion



namespace STATS.SoccerDAL
{
    public class clsUpComingAssignments
    {
        private static clsUpComingAssignments Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsUpComingAssignments GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsUpComingAssignments();
            }
            return Instance;
        }

        public DataSet LoadLeague()
        {
            try
            {
                string strqry = "SELECT LEAGUE_ID,LEAGUE_ABBREV FROM GLB_LEAGUE WHERE SPORT_ID = 8 ORDER BY LEAGUE_ID";
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, strqry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
