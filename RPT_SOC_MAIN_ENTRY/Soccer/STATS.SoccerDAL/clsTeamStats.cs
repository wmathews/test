﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsTeamStats
    {
        #region "Member Variables"

        private static clsTeamStats m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsTeamStats GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsTeamStats();
            }
            return m_myInstance;
        }

        #endregion
        /// <summary>
        /// GETS GAME STATISTICS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTeamStats(Int64 GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SELECT_TEAMSTATS", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Getperiodcount(int GameCode)
        {
            try
            {
                int intperiodD;
                string temp;
                temp = Convert.ToString(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "select max(period)from dbo.LIVE_SOC_PBP where game_code=" + GameCode + ""));
                if (temp != "" && temp != null)
                {
                    intperiodD = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "select max(period)from dbo.LIVE_SOC_PBP where game_code=" + GameCode + ""));
                }
                else
                {
                    intperiodD=0;
                }
                return intperiodD;
                //intperiodD = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "select max(period)from dbo.LIVE_SOC_PBP where game_code=" + GameCode + ""));

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTeamStastoselect(Int64 GameCode, int FeedNumber)

        {

            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_Teamstatsselect", ParamValues));
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "Teamstatsselect", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet InsertTeamStatstoSummary(Int64 GameCode, int FeedNumber, int ModuleID, int CoverageLevel)
        {
            try
            {
                if (CoverageLevel == 6)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_UPDATE_ACTION_TO_STATS_PROC_SOC", ParamValues);
                }
                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@MODULEID", ModuleID) };
                     return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_UPDATE_PBP_TO_STATS_PROC_SOC", ParamValues);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       

       
        public void InsertTeamStas(int GAME_CODE, int FEED_NUMBER, int TEAM_ID, int PERIOD, int SHOTS, int SHOTS_ON_GOAL, int WOODWORK, int OFFSIDES, int CORNER_KICKS, int FOULS_COMMITTED)
          {
              #region MyRegion

              
            try
            {      

                SqlParameter[] ParamValues = new SqlParameter[10];
                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = GAME_CODE;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = FEED_NUMBER;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@TEAM_ID";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = TEAM_ID;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@PERIOD";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = PERIOD;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@SHOTS";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = SHOTS;

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@SHOTS_ON_GOAL";
                ParamValues[5].SqlDbType = SqlDbType.Int;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = SHOTS_ON_GOAL;

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@WOODWORK";
                ParamValues[6].SqlDbType = SqlDbType.Int;                
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = WOODWORK;

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@OFFSIDES";
                ParamValues[7].SqlDbType = SqlDbType.Int;
                ParamValues[7].Direction = ParameterDirection.Input;
                ParamValues[7].Value = OFFSIDES;


                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@CORNER_KICKS";
                ParamValues[8].SqlDbType = SqlDbType.Int;
                ParamValues[8].Direction = ParameterDirection.Input;
                ParamValues[8].Value = CORNER_KICKS;


                 ParamValues[9] = new SqlParameter();
                 ParamValues[9].ParameterName = "@FOULS_COMMITTED ";               
                 ParamValues[9].SqlDbType = SqlDbType.Int;
                 ParamValues[9].Direction = ParameterDirection.Input;
                 ParamValues[9].Value = FOULS_COMMITTED;
                 SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_Addteamstats", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
              #endregion
          }



        public void InsertTeampossion(int GAME_CODE, int FEED_NUMBER, int TEAM_ID, int PERIOD, int possession)
        {
            #region MyRegion


            try
            {

                SqlParameter[] ParamValues = new SqlParameter[10];
                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = GAME_CODE;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = FEED_NUMBER;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@TEAM_ID";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = TEAM_ID;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@PERIOD";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = PERIOD;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@Possession";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = possession;

                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_AddPossession", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }


    }
}
