﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsAddDemoGame
// Author        : Shravani
// Created Date  : 20th Aug,2009
// Description   : 
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace STATS.SoccerDAL
{
    public class clsAddDemoGame
    {
        #region " Constantds and Variable"
        private static clsAddDemoGame Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
        #endregion

        /// <summary>
        /// SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        /// </summary>
        /// <returns></returns>
        public static clsAddDemoGame GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsAddDemoGame();
            }
            return Instance;
        }

        /// <summary>
        /// GETSAMPLEGAME TO DISPLAY IN LISTVIEW
        /// </summary>
        /// <returns></returns>
        public DataSet GetSampleGameDisplay()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETSAMPLEGAME_DISPLAY", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetLeague()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetLeague", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetAllComboData( int LeagueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LeagueID",LeagueID)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetTeamAndPosition", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetCntPlayer(string strqry)
        {
            try
            {
                DataSet dscnt = new DataSet();
                dscnt = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, strqry);
                return dscnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public int InsertAddGame(DateTime GameDate, int LeagueID, int AwayTeamID, int HomeTeamID, int VenueID, int CoverageLevel, int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMEDATE", GameDate),new SqlParameter("@LEAGUEID", LeagueID), new SqlParameter("@AWAYTEAMID", AwayTeamID), new SqlParameter("@HOMETEAMID", HomeTeamID), new SqlParameter("@VENUEID", VenueID), new SqlParameter("@COVERAGELEVEL", CoverageLevel), new SqlParameter("@MODULEID", ModuleID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_InsertAddGame", ParamValues);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      
        public int UpdateAddGame(int GameCode,DateTime GameDate, int LeagueID, int AwayTeamID, int HomeTeamID, int VenueID, int CoverageLevel, int ModuleID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] {new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@GAMEDATE", GameDate), new SqlParameter("@LEAGUEID", LeagueID), new SqlParameter("@AWAYTEAMID", AwayTeamID), new SqlParameter("@HOMETEAMID", HomeTeamID), new SqlParameter("@VENUEID", VenueID), new SqlParameter("@COVERAGELEVEL", CoverageLevel), new SqlParameter("@MODULEID", ModuleID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_UpdateAddGame", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteAddGame(int GameCode)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", GameCode)};
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_DeleteAddGame", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
