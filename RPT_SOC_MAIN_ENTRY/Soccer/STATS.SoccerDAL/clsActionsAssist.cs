﻿using System;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsActionsAssist
    {
        #region "Member Variables"

        private static clsActionsAssist m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY
        public static clsActionsAssist GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT
            if (m_myInstance == null)
            {
                m_myInstance = new clsActionsAssist();
            }
            return m_myInstance;
        }

        #endregion

        #region "User Functions"


        public int UpdatePbpRefresh(string uniqueIds)
        {
            try
            {
                SqlParameter[] paramValues = new SqlParameter[] { new SqlParameter("@UNIQUE_ID", uniqueIds) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_PZ_UPDATE_SOC_PBP_REFRESH", paramValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         /// <summary>
         /// Edit of records by A2,A3 & A4
         /// </summary>
         /// <param name="PBPXMLData"></param>
         /// <returns></returns>
        public DataSet EditPlaybyPlaydata(string PBPXMLData,Decimal SequenceNumber,char IsTimeEdited, char InsertMode)
        {
            try
            {
                if (InsertMode == 'Y')
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@Sequence_Number", SequenceNumber), new SqlParameter("@IsTimeEdited",IsTimeEdited) };
                    return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_InsertBeforeActionsDataXml", ParamValues));
                }
                else if (IsTimeEdited == 'Y')
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@Sequence_Number", SequenceNumber) };
                    return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_TimeEdit", ParamValues));
                }
                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@Sequence_Number", SequenceNumber) };
                    return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_EditActionsDataXml", ParamValues));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      /// <summary>
      /// Insert/Delete Key moment Data.
      /// </summary>
      /// <param name="SequenceNumber"></param>
      /// <param name="ReporterRole"></param>
      /// <param name="KeyType"></param>
      /// <returns></returns>
        public int InsertDeleteKeyMomentData(Decimal SequenceNumber, string ReporterRole, string KeyType)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SEQUENCE_NUMBER", SequenceNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@KEYTYPE", KeyType) };
                return (SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_InsertEditKeyMoment", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
