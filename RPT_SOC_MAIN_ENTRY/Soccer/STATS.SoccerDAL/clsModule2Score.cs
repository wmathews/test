﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsModule2Score
// Author        : Shravani
// Created Date  : 19-Mar-2010
// Description   : This is the data access logic for Module2Score
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"
namespace STATS.SoccerDAL
{
    public class clsModule2Score
    {


        #region "Member Variables"

        private static clsModule2Score m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsModule2Score GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsModule2Score();
            }
            return m_myInstance;
        }

        #endregion

        #region "User Methods"

        public DataSet GetTeams(int GameCode)
        {
            SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Game_Code", GameCode)};
            return(SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(),CommandType.StoredProcedure,"USP_MOD2_GetTeam",ParamValues));
        }

        /// <summary>
        /// To return the Home/Away Team Scores from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>A Dataset containing both the Home and Away Team Scores</returns>
        public DataSet GetScores(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetScores", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To return the Unique Id from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>An integer value representing the UniqueId that need to be used</returns>
        public int GetUniqueID(int GameCode, int FeedNumber)
        {
            try
            {
                int intUniqueID;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                intUniqueID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetUniqueID", ParamValues));
                return intUniqueID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Handles normal entry,Time Edit and Insert Before functionalities
        /// </summary>
        /// <param name="PBPXMLData"></param>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <param name="ReporterRole"></param>
        /// <param name="IsTimeEdited"></param>
        /// <param name="IsInsertBefore"></param>
        /// <param name="Sequence_Number"></param>
        /// <returns></returns>
        public DataSet InsertPBPDataToSQL(string PBPXMLData, int GameCode, int FeedNumber, string ReporterRole, bool IsTimeEdited, bool IsInsertBefore, Decimal Sequence_Number, int language_id)
        {
            try
            {

                if (IsTimeEdited == true)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_MainEntryTimeEdit", ParamValues);
                }
                else if (IsInsertBefore == true)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SEQUENCE_NUMBER", Sequence_Number), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_Before", ParamValues);
                }

                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// FETCHES MODULE 2 GAME DETAILS ASSIGNED TO THE REPORTER
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// <returns>A DataSet contains game details</returns>
        public DataSet GetMultipleGameDetails(int LanguageID, int Reporter_ID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@Reporter_ID", Reporter_ID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GetMultipleGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To insert current game data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <param name="intReporterID"></param>
        /// <param name="strReporterRole"></param>

        public void InsertCurrentGame(int intGameCode, int intOldGameCode, int intFeedNumber, int intReporterID, string strReporterRole, int intModuleID, int TimeElapsed, String SysLocalTime, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@OldGameCode", intOldGameCode), new SqlParameter("@FeedNumber", intFeedNumber), new SqlParameter("@ReporterID", intReporterID), new SqlParameter("@ReporterRole", strReporterRole), new SqlParameter("@ModuleID", intModuleID), new SqlParameter("@TimeElapsed", TimeElapsed), new SqlParameter("@SysLocalTime", SysLocalTime), new SqlParameter("@Type", Type) };
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_InsertCurrentGame", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetPBPData(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber),new SqlParameter("@language_id",language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_MAX_TIMEELAPSED", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// FTECHING PBP RECORDS BASED ON SEQUENCE NUMBER WHICH IS MAINLY USED FOR EDITING PURPOSE
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <param name="Sequence_Number"></param> 'THE RECORD IS FETCHED BASED ON SEQUECE NUMBER BY PASSING IT AS AN INPUT
        /// <param name="Type"></param> 'REPRESENTS WHICH QUERY TO GET EXECUTED
        /// <returns></returns>
        public DataSet GetPBPRecordBasedOnSeqNumber(int GameCode, int FeedNumber, decimal Sequence_Number, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchPBPRecordBasedOnSeqNo", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPrevSeq(int GameCode, int FeedNumber, Decimal SequenceNumber, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@Type", Type) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GET_PREV_SEQUENCE", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet DeletePBPData(int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ReporterRole", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_DeletePBPData", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPMainEventData(int GameCode, int FeedNumber, Decimal SequenceNumber, string Type)
        {
            try
            {
                if (Type == "SNO")
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetMainEventSNO", ParamValues);
                }
                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@SequenceNumber", SequenceNumber) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPairedEvents", ParamValues);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatePairedPBPEvents(string PBPXMLData, int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, string Type, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@Type", Type), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PairedPBPEvents", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet TimeEditOnPBPEvents(string PBPXMLData, int GameCode, int FeedNumber, String ReporterRole, Decimal SequenceNumber, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@SequenceNumber", SequenceNumber), new SqlParameter("@LANGUAGE_ID", LanguageID), new SqlParameter("@ISEVENT_MODIFIED", "N") };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_TIMEEDIT", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPRecordBasedOnTime(int GameCode, int FeedNumber, int Time_Elapsed, int Period, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ElapsedTime", Time_Elapsed), new SqlParameter("@Period", Period), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_FetchSeqNumBasedOnTime", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetLastEventDetails(int GameCode, int FeedNumber)
        {
            try
            {

                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_LASTEVENT_DETAILS", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
