﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;



#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsBoxScore
// Author        : Asif
// Created Date  : 16th july, 2009
// Description   : Data access for the box score form
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"
namespace STATS.SoccerDAL
{

   public class clsBoxScore
    {

       public static clsBoxScore Instance;
       private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();
       // MADE PRIVATE TO PREVENT THE INSTATIATION OF THE CLASS.

       //public clsBoxScore()
       //{

       //}

       public static clsBoxScore  GetAccess()
       {
           if (Instance == null)
           {
               Instance = new clsBoxScore();
           }
           return Instance;

       }


       public DataSet GetVenueDetails(int intGameCode, int intFeedNumber, int intLeagueID,  int LanguageID)
       {
           try
           {
               SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", intGameCode), new SqlParameter("@FEED_NUMBER", intFeedNumber), new SqlParameter("@LEAGUE_ID", intLeagueID), new SqlParameter("@language_id", LanguageID) };
               return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetVenueDetailsReport", ParamValues);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public DataSet GetBoxScoreReport(int intGameCode, int intFeedNumber, int LanguageID)
       {

           try
           {
               SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", intGameCode), new SqlParameter("@FEED_NUMBER", intFeedNumber), new SqlParameter("@language_id", LanguageID) };
               return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetBoxScoreReport", ParamValues);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public DataSet GetNewBoxScoreReport(int intGameCode, int intFeedNumber, int LanguageID, int CoverageLevel)
       {

           try
           {
               SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", intGameCode), new SqlParameter("@FEED_NUMBER", intFeedNumber), new SqlParameter("@language_id", LanguageID) };
               if (CoverageLevel == 6)
               {
                   return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_PZ_GetBoxScoreReport", ParamValues);

               }
               else
               {
                   return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetNewBoxScoreReport", ParamValues); 
               }
               
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public int GetPlayerSeconds(int intGameCode, int intFeedNumber,int intLeagueID,int intTeamID,int intPlayerID)
       {

           try
           {


               SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", intGameCode), new SqlParameter("@FEED_NUMBER", intFeedNumber), new SqlParameter("@LEAGUE_ID", intLeagueID), new SqlParameter("@TEAM_ID", intTeamID), new SqlParameter("@PLAYER_ID", intPlayerID) };
               object objPlayerMins =  SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GET_PLAYER_MINS", ParamValues);
               if (objPlayerMins != null)
               {
                   return Convert.ToInt32(objPlayerMins);
               }
               else
               {
                   return -1;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       public DataSet FetchSubsitutionAndRedCardEvents(int intGameCode, int intFeedNumber)
       {
           try
           {
               SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", intGameCode), new SqlParameter("@FEEDNUMBER", intFeedNumber) };
               return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_FetchSubsitutionAndExpulsionEvents", ParamValues);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       public DataSet GetPenaltyShootoutScores(int GameCode, int FeedNumber, int CoverageLevel)
       {
           try
           {
               SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
               if (CoverageLevel == 6)
               {
                   return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_GetPenaltyShootoutScores", ParamValues); 
               }
               else
               {
                   return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GetPenaltyShootoutScores", ParamValues);
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}
