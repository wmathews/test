﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsCommentary
// Author        : Dijo Davis
// Created Date  : 29-04-09
// Description   : This is the data access logic for commentary module.
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsCommentary
    {
        private static clsCommentary Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsCommentary GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsCommentary();
            }
            return Instance;
        }

        #region "Commentary"
        /// <summary>
        /// Function to fetch available languages
        /// </summary>
        /// <returns></returns>
        public DataSet GetLanguages()
        {
            try
            {
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetLanguages");

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        /// <summary>
        /// Function to save Commentry
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <param name="intLanguageID"></param>
        /// <param name="intPeriod"></param>
        /// <param name="intTimeElapsed"></param>
        /// <param name="strheadLine"></param>
        /// <param name="strComment"></param>
        public void AddCommentary(int intGameCode, int intFeedNumber, int intLanguageID, int intPeriod, int intTimeElapsed, string strheadLine, string strComment, int intCommentType, string strSystemTime, int Reporter_SlNo)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[10];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@LanguageID";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intLanguageID;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@Period";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = intPeriod;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@TimeElapsed";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                if (intTimeElapsed == -1)
                {
                    ParamValues[4].Value = DBNull.Value;
                }
                else
                {
                    ParamValues[4].Value = intTimeElapsed;
                }

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@HeadLine";
                ParamValues[5].SqlDbType = SqlDbType.NVarChar;
                //ParamValues[5].SqlDbType = SqlDbType.VarChar;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = strheadLine;

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@Commentary";
                //wilson changed for multi language 
                //ParamValues[6].SqlDbType = SqlDbType.VarChar;
                ParamValues[6].SqlDbType = SqlDbType.NVarChar;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = strComment;

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@CommentType";
                //wilson changed for multi language 
                //ParamValues[6].SqlDbType = SqlDbType.VarChar;
                ParamValues[7].SqlDbType = SqlDbType.Int;
                ParamValues[7].Direction = ParameterDirection.Input;
                //ParamValues[7].Value = intCommentType;
                if (intCommentType == 0)
                {
                    ParamValues[7].Value = DBNull.Value;
                }
                else
                {
                    ParamValues[7].Value = intCommentType;
                }
                

                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@SystemTime";
                ParamValues[8].SqlDbType = SqlDbType.DateTime;
                ParamValues[8].Direction = ParameterDirection.Input;
                ParamValues[8].Value = strSystemTime;

                ParamValues[9] = new SqlParameter();
                ParamValues[9].ParameterName = "@Reporter_SlNo";
                ParamValues[9].SqlDbType = SqlDbType.Int;
                ParamValues[9].Direction = ParameterDirection.Input;
                ParamValues[9].Value = Reporter_SlNo;

                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_AddCommentry", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Function to fetch commentary data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <returns></returns>

        public DataSet GetCommentary(int intGameCode, int intFeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@FeedNumber", intFeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetCommentary", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Function to delete commentary data
        /// </summary>
        /// <param name="decSequenceNumber"></param>

        public void DeleteCommentary(decimal decSequenceNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SequenceNumber", decSequenceNumber) };
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_DeleteCommentary", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Function to Edit Commentary data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <param name="intLanguageID"></param>
        /// <param name="intPeriod"></param>
        /// <param name="intTimeElapsed"></param>
        /// <param name="strheadLine"></param>
        /// <param name="strComment"></param>
        /// <param name="decSequenceNumber"></param>

        public void UpdateCommentary(int intGameCode, int intFeedNumber, int intLanguageID, int intPeriod, int intTimeElapsed, string strheadLine, string strComment, int intCommentType, decimal decSequenceNumber, int Reporter_SlNo)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[10];

                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@GameCode";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = intGameCode;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@FeedNumber";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = intFeedNumber;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@LanguageID";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = intLanguageID;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@Period";
                ParamValues[3].SqlDbType = SqlDbType.Int;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = intPeriod;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@TimeElapsed";
                ParamValues[4].SqlDbType = SqlDbType.Int;
                ParamValues[4].Direction = ParameterDirection.Input;
                if (intTimeElapsed == -1)
                {
                    ParamValues[4].Value = DBNull.Value;
                }
                else
                {
                    ParamValues[4].Value = intTimeElapsed;
                }

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@HeadLine";
                ParamValues[5].SqlDbType = SqlDbType.NVarChar;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = strheadLine;

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@Commentary";
                ParamValues[6].SqlDbType = SqlDbType.NVarChar;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = strComment;

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@CommentaryType";
                ParamValues[7].SqlDbType = SqlDbType.Int;
                ParamValues[7].Direction = ParameterDirection.Input;
                if (intCommentType == 0)
                {
                    ParamValues[7].Value = DBNull.Value;
                }
                else
                {
                    ParamValues[7].Value = intCommentType;
                }

                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@SequenceNumber";
                ParamValues[8].SqlDbType = SqlDbType.Decimal;
                ParamValues[8].Direction = ParameterDirection.Input;
                ParamValues[8].Value = decSequenceNumber;


                ParamValues[9] = new SqlParameter();
                ParamValues[9].ParameterName = "@Reporter_SlNo";
                ParamValues[9].SqlDbType = SqlDbType.Int;
                ParamValues[9].Direction = ParameterDirection.Input;
                ParamValues[9].Value = Reporter_SlNo;


                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_UpdateCommentary", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To get the PBP string
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intLanguageID"></param>
        /// <param name="intFeedNumber"></param>
        /// <returns></returns>

        public DataSet GetPBPString(int intGameCode, int intLanguageID, int intFeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@LanguageID", intLanguageID), new SqlParameter("@FeedNumber", intFeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetPBPString", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Arindam 23-Aug
        public DataSet GetCommType(int intLanguageID, int clientID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LanguageID", intLanguageID), new SqlParameter("@ClientID", clientID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetCommentaryType", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// To get PBP data for commentary
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <returns></returns>

        public DataSet GetPBPDataForCommentary(int intGameCode,int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetPBPDataforCommentary", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get Coach Info for commentary
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <returns></returns>
        public DataSet GetCoachInfoForCommentary(int intGameCode,int language_id)
        {
            try
            {
                DataSet dsCoach = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@language_id", language_id) };
                dsCoach = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetCoachInfoforCommentary", ParamValues);
                if (dsCoach != null)
                {
                    if (dsCoach.Tables.Count > 0)
                    {
                        dsCoach.Tables[0].TableName = "CoachData";
                    }
                }

                return dsCoach;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get Initial Coach Info for commentary
        /// </summary>
        /// <param name="intTeamID"></param>
        /// <returns></returns>
        public DataSet GetInitialCoachInfoForCommentary(int intTeamID,int language_id)
        {
            try
            {
                DataSet dsCoach = new DataSet();
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@TeamID", intTeamID), new SqlParameter("@language_id", language_id) };
                dsCoach = SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_GetInitialCoachInfoforCommentary", ParamValues);
                if (dsCoach != null)
                {
                    if (dsCoach.Tables.Count > 0)
                    {
                        dsCoach.Tables[0].TableName = "CoachData";
                    }
                }

                return dsCoach;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetPBPRecordBasedOnSeqNumber(int GameCode, int FeedNumber, decimal Sequence_Number, String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@Sequence_Number", Sequence_Number), new SqlParameter("@Type", Type) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD4_FetchCommRecordBasedOnSeqNo", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion "Commentry"

    }
}
