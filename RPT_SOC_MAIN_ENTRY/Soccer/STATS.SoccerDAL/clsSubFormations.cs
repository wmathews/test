﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsTeamSetup
// Author        : Shirley Ranjini
// Created Date  : 11th May,2009
// Description   : Data Access Layer for Team Setup screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace STATS.SoccerDAL
{
    public class clsSubFormations
    {

        private static clsSubFormations Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        /// <summary>
        /// SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        /// </summary>
        /// <returns></returns>
        public static clsSubFormations GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsSubFormations();
            }
            return Instance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Gamecode"></param>
        /// <param name="FeedNumber"></param>
        /// <param name="TeamID"></param>
        /// <returns></returns>
        public DataSet GetFormationCoach(int Gamecode, int FeedNumber, int TeamID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@TeamID", TeamID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetFormations", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetFormationSpecification(int FormationID, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FormationID", FormationID),new SqlParameter("@language_id",language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_SelectFormationSpecification", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetOnfieldValue(int Gamecode, int FeedNumber,int TeamID,int Period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", Gamecode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@TEAM_ID", TeamID), new SqlParameter("@PERIOD", Period) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetNextOnfieldID", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
