﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace STATS.SoccerDAL
{
    public class clsPlayerStats
    {
        #region "Member Variables"

        private static clsPlayerStats m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsPlayerStats GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsPlayerStats();
            }
            return m_myInstance;
        }
        #endregion


        public DataSet GetPlayerStats(Int64 GameCode, int FeedNumber, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@language_id", LanguageID) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SELECTPLAYERSTATS", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet InsertPlayerStatstoSummary(Int64 GameCode, int FeedNumber, int Time, int CoverageLevel)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@TIME_MIN", Time) };
                if (CoverageLevel == 6)
                {
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_INSERTPLAYERSTATS", ParamValues);
                }
                else
                {
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_INSERTPLAYERSTATS", ParamValues);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 GetMaxTimeElapsed(Int64 GameCode,int period)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@PERIOD", period) };
                return Convert.ToInt64(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GET_MAXTIMEELAPSED", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}