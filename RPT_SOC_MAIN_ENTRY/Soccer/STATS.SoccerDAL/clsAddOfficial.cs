﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsAddOfficial
// Author        : Shravani
// Created Date  : 12th June,2009
// Description   : Data Access Layer for AddOfficial screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion


namespace STATS.SoccerDAL
{
    public class clsAddOfficial
    {


        private static clsAddOfficial Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsAddOfficial GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsAddOfficial();
            }
            return Instance;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="PersonID"></param>
        /// <param name="LeagueID"></param>
        /// <param name="UniformNumber"></param>
        /// <param name="ReporterRole"></param>
        /// <param name="LastName"></param>
        /// <param name="FirstName"></param>
        /// <returns></returns>
        public int InsertUpdateOfficial(int PersonID, int LeagueID, string UniformNumber, string ReporterRole, string LastName, string FirstName, char Demo, Int64 GameCode, int FeedNumber)
        {
            try
            {
                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Person_Id", PersonID), new SqlParameter("@League_Id", LeagueID), new SqlParameter("@Uniform_number", UniformNumber), new SqlParameter("@Reporter_role", ReporterRole), new SqlParameter("@Last_Name", LastName), new SqlParameter("@Moniker", FirstName), new SqlParameter("@DEMO_DATA", Demo) };
                SqlParameter[] ParamValues = new SqlParameter[9];
                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@Person_Id";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = PersonID;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@League_Id";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = LeagueID;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@Uniform_number";
                ParamValues[2].SqlDbType = SqlDbType.VarChar;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = UniformNumber;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@ReporterRole";
                ParamValues[3].SqlDbType = SqlDbType.VarChar;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = ReporterRole;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@LAST_NAME";
                ParamValues[4].SqlDbType = SqlDbType.NVarChar;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = LastName;

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@MONIKER";
                ParamValues[5].SqlDbType = SqlDbType.NVarChar;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = FirstName;               

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@DEMO_DATA";
                ParamValues[6].SqlDbType = SqlDbType.Char;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = Demo;

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@GameCode";
                ParamValues[7].SqlDbType = SqlDbType.BigInt;
                ParamValues[7].Direction = ParameterDirection.Input;
                ParamValues[7].Value = GameCode;

                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@FeedNumber";
                ParamValues[8].SqlDbType = SqlDbType.TinyInt;
                ParamValues[8].Direction = ParameterDirection.Input;
                ParamValues[8].Value = FeedNumber;

                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_InsertUpdateAddOfficial", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet SelectAddOfficial(int League_Id, string SortOrder)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@League_Id", League_Id), new SqlParameter("@SortOrder", SortOrder) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_SelectAddOfficial", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Referee_Id"></param>
        /// <param name="League_Id"></param>
        /// <returns></returns>
        public DataSet DeleteAddOfficial(int Referee_Id, int League_Id, char Demo_Data, Int64 GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@RefereeID", Referee_Id), new SqlParameter("@LeagueID", League_Id), new SqlParameter("@Demo_Data", Demo_Data), new SqlParameter("@GAMECODE", GameCode), new SqlParameter("@FEEDNUMBER", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_DeleteAddOfficial", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS THE Uniform Number
        /// </summary>
        /// <returns></returns>
        public int GetUniform()
        {
            try
            {
                int intUniform;
                //SqlParameter[] ParamValues = new SqlParameter[] {};
                intUniform = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetUniformNumberAddOffiical"));
                return intUniform;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// GET LEAGUE FOR DEMO
        /// </summary>
        /// <returns></returns>
        public DataSet GetLeague()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetLeague", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet SelectAddOfficialDemo(string SortOrder)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SortOrder", SortOrder) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_SelectAddOfficial", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
    }
}


   	