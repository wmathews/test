﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;


namespace STATS.SoccerDAL
{
    public class clsPenaltyShootoutScore
    {
        #region "Member Variables"

        private static clsPenaltyShootoutScore m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsPenaltyShootoutScore GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsPenaltyShootoutScore();
            }
            return m_myInstance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="XMLGameSetupData"></param>
        /// <returns></returns>
        public DataSet InsertPBPDataToSQL(string PBPXMLData, int GameCode, int FeedNumber, string ReporterRole, int LangID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole), new SqlParameter("@language_id", LangID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);

                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited) };
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdatePenaltyShootoutScore(int GameCode, int FeedNumber, string ReporterRole, int LangID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@language_id", LangID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_UpdateShootOutGoal", ParamValues);

                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited) };
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetPBPData(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateTeamGamePenaltyShootoutScores(int GameCode, int FeedNumber, string ReporterRole)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@REPORTER_ROLE", ReporterRole) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_UpdateTeamGameWithPSScores", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetLastEventDetails(int GameCode, int FeedNumber)
        {
            try
            {

                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_LASTEVENT_DETAILS", ParamValues));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        #endregion
    }
}
