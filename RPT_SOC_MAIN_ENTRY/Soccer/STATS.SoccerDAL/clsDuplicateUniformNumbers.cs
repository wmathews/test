﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsDuplicateUniformNumbers
// Author        : Shravani
// Created Date  : 15th April,2010
// Description   : Data Access Layer for DuplicateUniformNumbers screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion


namespace STATS.SoccerDAL
{
    public class clsDuplicateUniformNumbers
    {
        private static clsDuplicateUniformNumbers Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsDuplicateUniformNumbers GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsDuplicateUniformNumbers();
            }
            return Instance;
        }

        public DataSet GetAllRosters(int GameCode, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@language_id", LanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetAllRosters", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// UPDATES DISPLAY UN
        /// </summary>
        /// <param name="LineupData"></param> PARAMETER WHICH CONTAINS THE XML DATA
        /// <returns></returns>
        public int UpdateRosterInfo(int GameCode, string Players)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@Players", Players) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Insert_Players_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
