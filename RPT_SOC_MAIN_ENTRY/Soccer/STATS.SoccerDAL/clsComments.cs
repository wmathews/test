﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsComments
// Author        : Shravani
// Created Date  : 15th July,2009
// Description   : Data Access Layer for Comments Screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion


namespace STATS.SoccerDAL
{


    public class clsComments
    {
        private static clsComments Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsComments GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsComments();
            }
            return Instance;
        }

        /// <summary>
        /// GETS THE PBP DATA FROM LIVE_SOC_PBP
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetPBPData(int GameCode, int FeedNumber, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber),new SqlParameter("@language_id",language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_PBP_LOAD_DATA", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_MAX_TIMEELAPSED", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getDelayData(int GameCode, int FeedNumber)
        {
            try
            {
                
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_FetchPBPRecordForDelay", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet updateDelayTime(int GameCode, int FeedNumber, DateTime DelayDate)
        {
            try
            {

                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@DelayDate", DelayDate) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_UpdateDelayTime", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public DataSet getGameSetupData(int GameCode, int FeedNumber)
        {
            try
            {

                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_FetchGameSetup", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetLanguage(string qry)
        {
            try
            {
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, qry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetPredefinedComments(string qry)
        {
            try
            {
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, qry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertUpdateGameSetupDelay(string XMLGameSetupData)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GamesetUpData", XMLGameSetupData) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_InsertAndUpdateGameSetup", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
