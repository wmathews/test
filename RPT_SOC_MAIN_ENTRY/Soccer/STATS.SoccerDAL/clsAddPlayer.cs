﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsAddPlayer
// Author        : Shravani
// Created Date  : 16th June,2009
// Description   : Data Access Layer for AddPlayer screen
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion


namespace STATS.SoccerDAL
{
    public class clsAddPlayer
    {

        private static clsAddPlayer Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsAddPlayer GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsAddPlayer();
            }
            return Instance;
        }

        public DataSet LoadPosition()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] {};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_GETPOSITION", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int InsertUpdatePlayer(long PersonID, int LeagueID, int TeamID, string LastName, string FirstName, string UniformNumber, string DisplayUniform, int PositionID, int YearLast,char DemoData)
        {
            try
            {
                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Person_Id", PersonID),new SqlParameter("@League_Id", LeagueID),new SqlParameter("@TEAM_ID", TeamID),new SqlParameter("@LAST_NAME", LastName),new SqlParameter("@MONIKER", FirstName),new SqlParameter("@UNIFORM", UniformNumber),  new SqlParameter("@DISPLAY_UNIFORM", DisplayUniform),new SqlParameter("@POSITION_ID", PositionID),new SqlParameter("@YEAR_LAST", YearLast),new SqlParameter("@DEMO_DATA", DemoData) };
                SqlParameter[] ParamValues = new SqlParameter[10];
                ParamValues[0] = new SqlParameter();
                ParamValues[0].ParameterName = "@Person_Id";
                ParamValues[0].SqlDbType = SqlDbType.BigInt;
                ParamValues[0].Direction = ParameterDirection.Input;
                ParamValues[0].Value = PersonID;

                ParamValues[1] = new SqlParameter();
                ParamValues[1].ParameterName = "@League_Id";
                ParamValues[1].SqlDbType = SqlDbType.Int;
                ParamValues[1].Direction = ParameterDirection.Input;
                ParamValues[1].Value = LeagueID;

                ParamValues[2] = new SqlParameter();
                ParamValues[2].ParameterName = "@TEAM_ID";
                ParamValues[2].SqlDbType = SqlDbType.Int;
                ParamValues[2].Direction = ParameterDirection.Input;
                ParamValues[2].Value = TeamID;

                ParamValues[3] = new SqlParameter();
                ParamValues[3].ParameterName = "@LAST_NAME";
                ParamValues[3].SqlDbType = SqlDbType.NVarChar;
                ParamValues[3].Direction = ParameterDirection.Input;
                ParamValues[3].Value = LastName;

                ParamValues[4] = new SqlParameter();
                ParamValues[4].ParameterName = "@MONIKER";
                ParamValues[4].SqlDbType = SqlDbType.NVarChar;
                ParamValues[4].Direction = ParameterDirection.Input;
                ParamValues[4].Value = FirstName;

                ParamValues[5] = new SqlParameter();
                ParamValues[5].ParameterName = "@UNIFORM";
                ParamValues[5].SqlDbType = SqlDbType.VarChar;
                ParamValues[5].Direction = ParameterDirection.Input;
                ParamValues[5].Value = UniformNumber;

                ParamValues[6] = new SqlParameter();
                ParamValues[6].ParameterName = "@DISPLAY_UNIFORM";
                ParamValues[6].SqlDbType = SqlDbType.VarChar;
                ParamValues[6].Direction = ParameterDirection.Input;
                ParamValues[6].Value = DisplayUniform;                            

                ParamValues[7] = new SqlParameter();
                ParamValues[7].ParameterName = "@POSITION_ID";
                ParamValues[7].SqlDbType = SqlDbType.Int;
                ParamValues[7].Direction = ParameterDirection.Input;
                ParamValues[7].Value = PositionID;

                ParamValues[8] = new SqlParameter();
                ParamValues[8].ParameterName = "@YEAR_LAST";
                ParamValues[8].SqlDbType = SqlDbType.Int;
                ParamValues[8].Direction = ParameterDirection.Input;
                ParamValues[8].Value = YearLast;

                ParamValues[9] = new SqlParameter();
                ParamValues[9].ParameterName = "@DEMO_DATA";
                ParamValues[9].SqlDbType = SqlDbType.Char;
                ParamValues[9].Direction = ParameterDirection.Input;
                ParamValues[9].Value = DemoData;

                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_MOD1_InsertUpdateAddPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet SelectAddPlayer(Int64 GameCode, int League_Id, string SortOrder,char DemoData)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@LEAGUE_ID", League_Id), new SqlParameter("@SortOrder", SortOrder), new SqlParameter("@DEMO_DATA", DemoData) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_SelectAddPlayer", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteAddPlayer(Int64 GameCode, int FeedNumber, Int64 PlayerID, int LeagueID, int TeamID, char DemoData, int CoverageLevel)
        {
            try
            {
                int Count;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@PlayerID", PlayerID), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@TeamID", TeamID), new SqlParameter("@DEMO_DATA", DemoData), new SqlParameter("@CoverageLevel", CoverageLevel) };
                Count = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_DeleteAddPlayer", ParamValues));
                return Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCurrentYear(int League_Id)
        {
            return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT YEAR FROM dbo.GLB_SEASON WHERE LEAGUE_ID = '" + League_Id + "' AND  CURRENT_SEASON_IND ='Y'");
        }


        public DataSet GetUnusedUniformNumber(int TeamID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] {  new SqlParameter("@TEAM_ID", TeamID)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetUnusedUniformNumbers", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
