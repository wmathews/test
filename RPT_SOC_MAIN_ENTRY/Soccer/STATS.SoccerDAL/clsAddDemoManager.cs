﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;

namespace STATS.SoccerDAL
{
    public class clsAddDemoManager
    {
        private static clsAddDemoManager Instance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();


        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsAddDemoManager GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (Instance == null)
            {
                Instance = new clsAddDemoManager();
            }
            return Instance;
        }


        /// <summary>
        /// Selects Add Newly Add Manager Data
        /// </summary>
        /// <param name="GameCode"></param>
        /// <returns></returns>
        public DataSet SelectAddManager()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] {};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_SelectAddManager", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS THE CURUUENT SEASONID
        /// </summary>
        /// <param name="LeagueID"></param>
        /// <returns></returns>
        public int GetSeasonID(int LeagueID)
        {
            try
            {
                int intSeasonID;
                intSeasonID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT SEASON_ID FROM dbo.GLB_SEASON WHERE LEAGUE_ID = " + LeagueID + " AND  CURRENT_SEASON_IND ='Y'"));
                return intSeasonID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CoachID"></param>
        /// <param name="SeasonID"></param>
        /// <param name="TeamID"></param>
        /// <param name="Sequence"></param>
        /// <param name="Moniker"></param>
        /// <param name="LastName"></param>
        /// <param name="ReporterRole"></param>
        /// <returns></returns>
        public int InsertUpdateManager(int CoachID, int SeasonID, int TeamID, int Sequence, string Moniker, string LastName, string ReporterRole)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@NupID", CoachID), new SqlParameter("@SeasonID", SeasonID), new SqlParameter("@TeamID", TeamID), new SqlParameter("@Sequence", Sequence), new SqlParameter("@Moniker", Moniker), new SqlParameter("@LastName", LastName), new SqlParameter("@ReporterRole", ReporterRole) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_InsertUpdateAddManager", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int GetSequence()
        {
            try
            {
                int intSequence;
                intSequence = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.Text, "SELECT MAX(SEQUENCE)+1 FROM SOCCER_COACH_SEASON"));
                return intSequence;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeletetManager(int CoachID,int SeasonID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@CoachID", CoachID), new SqlParameter("@SeasonID", SeasonID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_DeleteAddManager", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GET LEAGUE FOR DEMO
        /// </summary>
        /// <returns></returns>
        public DataSet GetLeague()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetLeague", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetTeam(int LeagueID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LEAGUEID", LeagueID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetTeamAndPosition", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
