﻿using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;


namespace STATS.SoccerDAL
{
    public class clsManagerExpulsion
    {
        #region "Member Variables"

        private static clsManagerExpulsion m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsManagerExpulsion GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsManagerExpulsion();
            }
            return m_myInstance;
        }

        #endregion

        #region "User Functions"

        //public DataSet GetScores(int GameCode, int FeedNumber)
        //{
        //    try
        //    {
        //        SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
        //        return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetScores", ParamValues));

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public DataSet GetFormationCoach(int Gamecode, int FeedNumber, int ManagerID, String Type, int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ManagerID", ManagerID), new SqlParameter("@Type", Type), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetCoachForExpulsion", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetReasonData(int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@language_id", language_id)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetManagerExpelReason", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
