﻿#region " Using "
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using STATS.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
#endregion

#region " Comments "
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsAddVenue
// Author        : Shravani
// Created Date  : 21th Aug,2009
// Description   : 
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace STATS.SoccerDAL
{
    public class clsAddVenue
    {

        #region "Member Variables"

        private static clsAddVenue m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion 

        public static clsAddVenue GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsAddVenue();
            }
            return m_myInstance;
        }


        public DataSet GetLeague()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_GetLeague", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LEAGUEID"></param>
        /// <param name="FIELDNAME"></param>
        /// <param name="FIELDNICKNAME"></param>
        /// <param name="CITY"></param>
        /// <param name="CAPACITY"></param>
        /// <param name="SURFACEID"></param>
        /// <returns></returns>
        public int InsertAddVenue(int LEAGUEID, string FIELDNAME, string FIELDNICKNAME, string CITY, int CAPACITY, char GRASS, char DOME)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LEAGUE_ID", LEAGUEID), new SqlParameter("@FIELD_NAME", FIELDNAME), new SqlParameter("@FIELD_NICKNAME", FIELDNICKNAME), new SqlParameter("@CITY", CITY), new SqlParameter("@CAPACITY", CAPACITY),  new SqlParameter("@GRASS",GRASS),new SqlParameter("@DOME",DOME) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_InsertAddVenue", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LEAGUEID"></param>
        /// <param name="FIELDID"></param>
        /// <param name="FIELDNAME"></param>
        /// <param name="FIELDNICKNAME"></param>
        /// <param name="CITY"></param>
        /// <param name="CAPACITY"></param>
        /// <param name="SURFACEID"></param>
        /// <returns></returns>
        public int UpdateAddVenue(int LEAGUEID, int FIELDID,string FIELDNAME, string FIELDNICKNAME, string CITY, int CAPACITY, char GRASS ,char DOME )
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@LEAGUE_ID", LEAGUEID), new SqlParameter("@FIELD_ID", FIELDID), new SqlParameter("@FIELD_NAME", FIELDNAME), new SqlParameter("@FIELD_NICKNAME", FIELDNICKNAME), new SqlParameter("@CITY", CITY), new SqlParameter("@CAPACITY", CAPACITY), new SqlParameter("@GRASS",GRASS),new SqlParameter("@DOME",DOME) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "USP_Demo_UpdateAddVenue", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet SelectAddVenue()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_Demo_SelectAddVenue", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="FIELDID"></param>
       /// <param name="LEGAUEID"></param>
       /// <returns></returns>
        public int DeleteAddVenue(int FIELDID,int LEGAUEID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@FIELD_ID", FIELDID), new SqlParameter("@LEAGUE_ID", LEGAUEID) };
                return SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), "[USP_Demo_DeleteAddVenue]", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
