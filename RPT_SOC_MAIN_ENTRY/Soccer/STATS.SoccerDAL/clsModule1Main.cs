﻿
using System;
using System.Collections.Generic;
using System.Text;
using STATS.DataAccess;
using System.Data;
using System.Data.SqlClient;

#region "Comments"
//----------------------------------------------------------------------------------------------------------------------------------------------
// Class name    : clsModule1Main
// Author        : Shirley Ranjini G
// Created Date  : 01-06-09
// Description   : This is the data access logic for Module1 Main.
//----------------------------------------------------------------------------------------------------------------------------------------------
// Modification Log :
//----------------------------------------------------------------------------------------------------------------------------------------------
//ID             | Modified By         | Modified date     | Description
//----------------------------------------------------------------------------------------------------------------------------------------------
//               |                     |                   |
//               |                     |                   |
//----------------------------------------------------------------------------------------------------------------------------------------------
#endregion "Comments"

namespace STATS.SoccerDAL
{
    public class clsModule1Main
    {
        #region "Member Variables"

        private static clsModule1Main m_myInstance;
        private clsDataAccess m_objDataAccess = clsDataAccess.GetAccess();

        #endregion

        #region "Shared Methods"

        //SINGLETON CLASS CAN BE ACCESS THROUGH SHARED MEMBER FUNCTION ONLY 
        public static clsModule1Main GetAccess()
        {
            //INSTANCE (NEW) WILL CREATE ONLY IF THERE IS NO INSTANCE CREATED BEFORE ELSE NOT 
            if (m_myInstance == null)
            {
                m_myInstance = new clsModule1Main();
            }
            return m_myInstance;
        }

        #endregion

        /// <summary>
        /// FETCHES MODULE 2 GAME DETAILS ASSIGNED TO THE REPORTER
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// <returns>A DataSet contains game details</returns>
        public DataSet GetMultipleGameDetails(int LanguageID, int Reporter_ID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@Reporter_ID", Reporter_ID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GetMultipleGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetGameCodeMod2()
        {
            try
            {
                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@UserType", UserType), new SqlParameter("@GameDate", GameDate) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_GET_GAMECODE_ENDGAME");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// FETCHES PBP EVENS (GOAL,EXPULSION,PENALTY AND BOOKINGS) SO AS TO DISPLAY IN THE MAIN SCREEN
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet FetchMainScreenEvents(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber)};
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_FetchMainScreenPBPEvents", ParamValues);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// FETCHES THE MASTER TABLES WHICH IS REQUIRED TO DIPSLAY IN THE LISTBOX (EX. SUB REASON,BOOKING REASON AND TYPES)
        /// </summary>
        /// <returns></returns>
        public DataSet FetchMainScreenMasterData(int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_FetchMainScreenMasterData",ParamValues);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To insert current game data
        /// </summary>
        /// <param name="intGameCode"></param>
        /// <param name="intFeedNumber"></param>
        /// <param name="intReporterID"></param>
        /// <param name="strReporterRole"></param>

        public void InsertCurrentGame(int intGameCode,int intOldGameCode,int intFeedNumber, int intReporterID, string strReporterRole, int intModuleID,int TimeElapsed,String SysLocalTime,String Type)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", intGameCode), new SqlParameter("@OldGameCode", intOldGameCode), new SqlParameter("@FeedNumber", intFeedNumber), new SqlParameter("@ReporterID", intReporterID), new SqlParameter("@ReporterRole", strReporterRole), new SqlParameter("@ModuleID", intModuleID), new SqlParameter("@TimeElapsed", TimeElapsed), new SqlParameter("@SysLocalTime", SysLocalTime), new SqlParameter("@Type", Type) };
                SqlHelper.ExecuteNonQuery(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_InsertCurrentGame", ParamValues);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// FETCHES HOME AND AWAY TEAM ROSTERS FOR THE SPECIFIED GAMECODE AND LEAGUEID
        /// </summary>
        /// <param name="Gamecode"></param>
        /// <param name="LeagueID"></param>
        /// <returns>A DATASET CONTAINING 3 TABLES TABLE1 : HOME TEAM ROSTERS TABLE2 : AWAY TEAM ROSTERS TABLE 3 : TEAM NAMES WITH THEIR CORRESPONDING ID'S</returns>

        public DataSet GetRosterInfo(int Gamecode, int LeagueID, string Type, int LanguageID)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@LeagueID", LeagueID), new SqlParameter("@Type", Type), new SqlParameter("@language_id", LanguageID) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetRosterInfo", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Added by Arindam 18-Jun-09

        /// <summary>
        /// To return the Unique Id from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>An integer value representing the UniqueId that need to be used</returns>
        public int GetUniqueID(int GameCode, int FeedNumber)
        {
            try
            {
                int intUniqueID;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                intUniqueID = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetUniqueID", ParamValues));
                return intUniqueID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// To return the Home/Away Team Scores from the Local SQL Server
        /// </summary>
        /// <param name="GameCode">An integer representing the gamecode</param>
        /// <param name="FeedNumber">An integer representing the feednumber</param>
        /// <returns>A Dataset containing both the Home and Away Team Scores</returns>
        public DataSet GetScores(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GetScores", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="XMLGameSetupData"></param>
        /// <returns></returns>
        public DataSet InsertPBPDataToSQL(string PBPXMLData, int GameCode, int FeedNumber, string isEdited, int language_id,int ModuleId)
        {
            try
            {
                if (ModuleId == 3)
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@SEQUENCE_NUMBER", "0") };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_InsertEditActionsPBPData", ParamValues);
                }
                else
                {
                    SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited), new SqlParameter("@language_id", language_id) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS GAME STATISTICS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTeamStats(Int64 GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return (SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_SELECT_TEAMSTATS", ParamValues));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet InsertTeamStatstoSummary(Int64 GameCode, int FeedNumber, int CoverageLevel)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                if (CoverageLevel == 6)
                {
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_PZ_UPDATE_ACTION_TO_STATS_PROC_SOC", ParamValues);
                }
                else
                {
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_UPDATE_PBP_TO_STATS_PROC_SOC", ParamValues);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// TO GET THE COLORS THE TEAMS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetTeamColors(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETTEAMCOLOR", ParamValues);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetFormationCoach(int Gamecode, int FeedNumber, int ManagerID, String Type,int language_id)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Gamecode", Gamecode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@ManagerID", ManagerID), new SqlParameter("@Type", Type), new SqlParameter("@language_id", language_id) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetCoachForExpulsion", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// TO GET THE COLORS THE TEAMS UNIFORMS
        /// </summary>
        /// <param name="GameCode"></param>
        /// <param name="FeedNumber"></param>
        /// <returns></returns>
        public DataSet GetUniformColors(int GameCode, int FeedNumber)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETUNIFORMCOLOR", ParamValues);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GETS THE GAME DETAILS ASSIGNED TO THE REPORTER
        /// </summary>
        /// <param name="LanguageID">An Integer represetns the LanguageID</param>
        /// <param name="UserType">An Integer represents the UserType</param>
        /// 
        /// <returns>A DataSet contains game details</returns>

        public DataSet GetGameDetails(int LanguageID, int UserType)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@Language_ID", LanguageID), new SqlParameter("@UserType", UserType) };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GetGameDetails", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMaxTimeElapsed(int GameCode, int FeedNumber)
        {
            try
            {
                int MaxTimeElapsed;
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber) };
                MaxTimeElapsed = Convert.ToInt32(SqlHelper.ExecuteScalar(m_objDataAccess.GetSQLConnection(), CommandType.StoredProcedure, "USP_MOD1_GET_MAX_TIMEELAPSED", ParamValues));
                return MaxTimeElapsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet UpdateActiveGK(int GameCode, int FeedNumber, int TeamID, int LangID, int CoverageLevel)
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@GAME_CODE", GameCode), new SqlParameter("@FEED_NUMBER", FeedNumber), new SqlParameter("@@TeamID", TeamID), new SqlParameter("@language_id", LangID) };
                    return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD2_ActiveGoalKeeper", ParamValues);

                //SqlParameter[] ParamValues = new SqlParameter[] { new SqlParameter("@SoccerPBPData", PBPXMLData), new SqlParameter("@GameCode", GameCode), new SqlParameter("@FeedNumber", FeedNumber), new SqlParameter("@EditedRecord", isEdited) };
                //return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_MOD1_Insert_PBP_Data_XML", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region "Demo"
        public DataSet GetDemoGameDetails()
        {
            try
            {
                SqlParameter[] ParamValues = new SqlParameter[] { };
                return SqlHelper.ExecuteDataset(m_objDataAccess.GetSQLConnection(), "USP_GETSAMPLEGAME_DISPLAY", ParamValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion "Demo"


        //END Added by Arindam 18-Jun-09
        

    }

   
}
